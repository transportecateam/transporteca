<?php

/**
 * TNT Express Connect
 *
 * @author Jose Manuel Sanchez, <josemanuelsh@hotmail.com>
 * @version 1.0
 *
 */
class TNT_ExpressConnect {

    public $customerId;
    public $customerPassword;
    protected $curlCall;

    /**
     * Builds class, registers Customer information and prepares CURL
     *
     * @cId Customer Id provided by TNT
     * @cPassword Customer Password provided by TNT
     * @cAccount Customer Account provided by TNT (Test account can be used)
     */
    function __construct($cId, $cPassword) {
        // Fills vars
        $this->customerId = $cId;
        $this->customerPassword = $cPassword;
        // Configures CURL
        $this->curlCall = curl_init();
        curl_setopt($this->curlCall, CURLOPT_POST, true);
        curl_setopt($this->curlCall, CURLOPT_HEADER, false);
        curl_setopt($this->curlCall, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlCall, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curlCall, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.13) Gecko/20101206 Ubuntu/10.10 (maverick) Firefox/3.6.13');
        curl_setopt($this->curlCall, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=ISO-8859-1', 'Authorization: Basic ' . base64_encode($this->customerId . ':' . $this->customerPassword)));
    }

    /**
     * Destroys class and closes CURL
     */
    function __destruct() {
        curl_close($this->curlCall);
    }

    /**
     * Searches tracking information by Tracking Number
     *
     * @lAlbaranes Array of Tracking Numbers
     */
    function search_albaran($lAlbaranes) {
        if (!is_array($lAlbaranes))
            $lAlbaranes = array($lAlbaranes);
        $postXML = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
        $postXML .="\n" . '<TrackRequest>';
        $postXML .="\n" . '	<SearchCriteria>';
        foreach ($lAlbaranes as $value)
            $postXML .="\n" . '		<ConsignmentNumber>' . $value . '</ConsignmentNumber>';
        $postXML .="\n" . '	</SearchCriteria>';
        $postXML .="\n" . '	<LevelOfDetail>';
        $postXML .="\n" . '		<Complete originAddress="true" destinationAddress="true" package="true" shipment="true"/>';
        $postXML .="\n" . '	</LevelOfDetail>';
        $postXML .="\n" . '</TrackRequest>';
        $resultXML = new SimpleXMLElement($this->send_xml($postXML, 'https://express.tnt.com/expressconnect/track.do?'));
        return $this->get_tracking_array($resultXML);
    }

    /**
     * Searches tracking information by Reference Number
     *
     * @lReferences Array of Reference Numbers
     */
    function search_referencia($lReferences) {
        if (!is_array($lReferences))
            $lReferences = array($lReferences);
        $postXML = '<?xml version="1.0" encoding="utf-8" standalone="no"?>';
        $postXML .="\n" . '<TrackRequest>';
        $postXML .="\n" . '	<SearchCriteria>';
        foreach ($lReferences as $value)
            $postXML .="\n" . '		<CustomerReference>' . $value . '</CustomerReference>';
        $postXML .="\n" . '	</SearchCriteria>';
        $postXML .="\n" . '	<LevelOfDetail>';
        $postXML .="\n" . '		<Complete originAddress="true" destinationAddress="true" package="true" shipment="true"/>';
        $postXML .="\n" . '	</LevelOfDetail>';
        $postXML .="\n" . '</TrackRequest>';
        $resultXML = new SimpleXMLElement($this->send_xml($postXML, 'https://express.tnt.com/expressconnect/track.do?'));
        return $this->get_tracking_array($resultXML);
    }

    /**
     * Searches tracking information by Account Number
     *
     * @raccount Account Number
     * @rcountry Country code where the Account is located
     * @rstart Start date
     * @rend End date
     */
    function search_cuenta($raccount, $rcountry, $rstart, $rend = '') {
        if (!$rend || $rend < $rstart)
            $rend = $rstart;
        $postXML = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
        $postXML .="\n" . '<TrackRequest>';
        $postXML .="\n" . '	<SearchCriteria>';
        $postXML .="\n" . '		<Account>';
        $postXML .="\n" . '			<Number>' . $raccount . '</Number>';
        $postXML .="\n" . '			<CountryCode>' . $rcountry . '</CountryCode>';
        $postXML .="\n" . '		</Account>';
        $postXML .="\n" . '		<Period>';
        $postXML .="\n" . '			<DateFrom>' . $rstart . '</DateFrom>';
        $postXML .="\n" . '			<DateTo>' . $rend . '</DateTo>';
        $postXML .="\n" . '		</Period>';
        $postXML .="\n" . '	</SearchCriteria>';
        $postXML .="\n" . '	<LevelOfDetail>';
        $postXML .="\n" . '		<Complete originAddress="true" destinationAddress="true" package="true" shipment="true"/>';
        $postXML .="\n" . '	</LevelOfDetail>';
        $postXML .="\n" . '</TrackRequest>';
        $resultXML = new SimpleXMLElement($this->send_xml($postXML, 'https://express.tnt.com/expressconnect/track.do?'));
        return $this->get_tracking_array($resultXML);
    }

    /**
     * Sends Shipment in Original Mode
     *
     * @sdata Array containing all information
     */
    function send_shipment($rsdata, $xslPath = '') {
        $postXML = '<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>';
        $postXML .="\n" . '<!DOCTYPE ESHIPPER SYSTEM "https://iconnection.tnt.com/ShipperDTD1.0/EShipperIN.dtd">';
        $postXML .="\n" . '<ESHIPPER>';
        $postXML .="\n" . '	<LOGIN>';
        $postXML .="\n" . '		<COMPANY>' . $this->customerId . '</COMPANY>';
        $postXML .="\n" . '		<PASSWORD>' . $this->customerPassword . '</PASSWORD>';
        $postXML .="\n" . '		<APPID>EC</APPID>';
        $postXML .="\n" . '		<APPVERSION>2.2</APPVERSION>';
        $postXML .="\n" . '	</LOGIN>';
        $postXML .="\n" . '	<CONSIGNMENTBATCH>';
        $postXML .="\n" . '		<SENDER>';
        $postXML .="\n" . '			<COMPANYNAME>' . $rsdata['Sender']['CompanyName'] . '</COMPANYNAME>';
        $postXML .="\n" . '			<STREETADDRESS1>' . $rsdata['Sender']['StreetAddress1'] . '</STREETADDRESS1>';
        $postXML .="\n" . '			<STREETADDRESS2>' . $rsdata['Sender']['StreetAddress2'] . '</STREETADDRESS2>';
        $postXML .="\n" . '			<STREETADDRESS3>' . $rsdata['Sender']['StreetAddress3'] . '</STREETADDRESS3>';
        $postXML .="\n" . '			<CITY>' . $rsdata['Sender']['City'] . '</CITY>';
        $postXML .="\n" . '			<PROVINCE>' . $rsdata['Sender']['Province'] . '</PROVINCE>';
        $postXML .="\n" . '			<POSTCODE>' . $rsdata['Sender']['PostCode'] . '</POSTCODE>';
        $postXML .="\n" . '			<COUNTRY>' . $rsdata['Sender']['Country'] . '</COUNTRY>';
        $postXML .="\n" . '			<ACCOUNT>' . $rsdata['Sender']['Account'] . '</ACCOUNT>';
        $postXML .="\n" . '			<VAT>' . $rsdata['Sender']['VAT'] . '</VAT>';
        $postXML .="\n" . '			<CONTACTNAME>' . $rsdata['Sender']['ContactName'] . '</CONTACTNAME>';
        $postXML .="\n" . '			<CONTACTDIALCODE>' . $rsdata['Sender']['ContactDialCode'] . '</CONTACTDIALCODE>';
        $postXML .="\n" . '			<CONTACTTELEPHONE>' . $rsdata['Sender']['ContactTelephone'] . '</CONTACTTELEPHONE>';
        $postXML .="\n" . '			<CONTACTEMAIL>' . $rsdata['Sender']['ContactEmail'] . '</CONTACTEMAIL>';
        $postXML .="\n" . '			<COLLECTION>';
        if (isset($rsdata['Collection'])) {
            $postXML .="\n" . '				<COLLECTIONADDRESS>';
            $postXML .="\n" . '					<COMPANYNAME>' . $rsdata['Collection']['CompanyName'] . '</COMPANYNAME>';
            $postXML .="\n" . '					<STREETADDRESS1>' . $rsdata['Collection']['StreetAddress1'] . '</STREETADDRESS1>';
            $postXML .="\n" . '					<STREETADDRESS2>' . $rsdata['Collection']['StreetAddress2'] . '</STREETADDRESS2>';
            $postXML .="\n" . '					<STREETADDRESS3>' . $rsdata['Collection']['StreetAddress3'] . '</STREETADDRESS3>';
            $postXML .="\n" . '					<CITY>' . $rsdata['Collection']['City'] . '</CITY>';
            $postXML .="\n" . '					<PROVINCE>' . $rsdata['Collection']['Province'] . '</PROVINCE>';
            $postXML .="\n" . '					<POSTCODE>' . $rsdata['Collection']['PostCode'] . '</POSTCODE>';
            $postXML .="\n" . '					<COUNTRY>' . $rsdata['Collection']['Country'] . '</COUNTRY>';
            $postXML .="\n" . '					<VAT>' . $rsdata['Collection']['VAT'] . '</VAT>';
            $postXML .="\n" . '					<CONTACTNAME>' . $rsdata['Collection']['ContactName'] . '</CONTACTNAME>';
            $postXML .="\n" . '					<CONTACTDIALCODE>' . $rsdata['Collection']['ContactDialCode'] . '</CONTACTDIALCODE>';
            $postXML .="\n" . '					<CONTACTTELEPHONE>' . $rsdata['Collection']['ContactTelephone'] . '</CONTACTTELEPHONE>';
            $postXML .="\n" . '                                                                                     <CONTACTEMAIL>' . $rsdata['Collection']['ContactEmail'] . '</CONTACTEMAIL>';
            $postXML .="\n" . '				</COLLECTIONADDRESS>';
        }
        $postXML .="\n" . '				<SHIPDATE>' . $rsdata['Sender']['ShipDate'] . '</SHIPDATE>';
        $postXML .="\n" . '				<PREFCOLLECTTIME>';
        if (!$rsdata['Sender']['CollectionTimeFrom'])
            $rsdata['Sender']['CollectionTimeFrom'] = '0900';
        $postXML .="\n" . '					<FROM>' . $rsdata['Sender']['CollectionTimeFrom'] . '</FROM>';
        if (!$rsdata['Sender']['CollectionTimeTo'])
            $rsdata['Sender']['CollectionTimeTo'] = '1800';
        $postXML .="\n" . '					<TO>' . $rsdata['Sender']['CollectionTimeTo'] . '</TO>';
        $postXML .="\n" . '				</PREFCOLLECTTIME>';
        $postXML .="\n" . '				<COLLINSTRUCTIONS>' . $rsdata['Sender']['CollectionInstructions'] . '</COLLINSTRUCTIONS>';
        $postXML .="\n" . '			</COLLECTION>';
        $postXML .="\n" . '		</SENDER>';
        $postXML .="\n" . '		<CONSIGNMENT>';
        $postXML .="\n" . '			<CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF>';
        $postXML .="\n" . '			<DETAILS>';
        $postXML .="\n" . '				<RECEIVER>';
        $postXML .="\n" . '					<COMPANYNAME>' . $rsdata['Receiver']['CompanyName'] . '</COMPANYNAME>';
        $postXML .="\n" . '					<STREETADDRESS1>' . $rsdata['Receiver']['StreetAddress1'] . '</STREETADDRESS1>';
        $postXML .="\n" . '					<STREETADDRESS2>' . $rsdata['Receiver']['StreetAddress2'] . '</STREETADDRESS2>';
        $postXML .="\n" . '					<STREETADDRESS3>' . $rsdata['Receiver']['StreetAddress3'] . '</STREETADDRESS3>';
        $postXML .="\n" . '					<CITY>' . $rsdata['Receiver']['City'] . '</CITY>';
        $postXML .="\n" . '					<PROVINCE>' . $rsdata['Receiver']['Province'] . '</PROVINCE>';
        $postXML .="\n" . '					<POSTCODE>' . $rsdata['Receiver']['PostCode'] . '</POSTCODE>';
        $postXML .="\n" . '					<COUNTRY>' . $rsdata['Receiver']['Country'] . '</COUNTRY>';
        $postXML .="\n" . '					<VAT>' . $rsdata['Receiver']['VAT'] . '</VAT>';
        $postXML .="\n" . '					<CONTACTNAME>' . $rsdata['Receiver']['ContactName'] . '</CONTACTNAME>';
        $postXML .="\n" . '					<CONTACTDIALCODE>' . $rsdata['Receiver']['ContactDialCode'] . '</CONTACTDIALCODE>';
        $postXML .="\n" . '					<CONTACTTELEPHONE>' . $rsdata['Receiver']['ContactTelephone'] . '</CONTACTTELEPHONE>';
        $postXML .="\n" . '					<CONTACTEMAIL>' . $rsdata['Receiver']['ContactEmail'] . '</CONTACTEMAIL>';
        $postXML .="\n" . '				</RECEIVER>';
        if (isset($rsdata['Delivery'])) {
            $postXML .="\n" . '				<DELIVERY>';
            $postXML .="\n" . '					<COMPANYNAME>' . $rsdata['Delivery']['CompanyName'] . '</COMPANYNAME>';
            $postXML .="\n" . '					<STREETADDRESS1>' . $rsdata['Delivery']['StreetAddress1'] . '</STREETADDRESS1>';
            $postXML .="\n" . '					<STREETADDRESS2>' . $rsdata['Delivery']['StreetAddress2'] . '</STREETADDRESS2>';
            $postXML .="\n" . '					<STREETADDRESS3>' . $rsdata['Delivery']['StreetAddress3'] . '</STREETADDRESS3>';
            $postXML .="\n" . '					<CITY>' . $rsdata['Delivery']['City'] . '</CITY>';
            $postXML .="\n" . '					<PROVINCE>' . $rsdata['Delivery']['Province'] . '</PROVINCE>';
            $postXML .="\n" . '					<POSTCODE>' . $rsdata['Delivery']['PostCode'] . '</POSTCODE>';
            $postXML .="\n" . '					<COUNTRY>' . $rsdata['Delivery']['Country'] . '</COUNTRY>';
            $postXML .="\n" . '					<VAT>' . $rsdata['Delivery']['VAT'] . '</VAT>';
            $postXML .="\n" . '					<CONTACTNAME>' . $rsdata['Delivery']['ContactName'] . '</CONTACTNAME>';
            $postXML .="\n" . '					<CONTACTDIALCODE>' . $rsdata['Delivery']['ContactDialCode'] . '</CONTACTDIALCODE>';
            $postXML .="\n" . '					<CONTACTTELEPHONE>' . $rsdata['Delivery']['ContactTelephone'] . '</CONTACTTELEPHONE>';
            $postXML .="\n" . '					<CONTACTEMAIL>' . $rsdata['Delivery']['ContactEmail'] . '</CONTACTEMAIL>';
            $postXML .="\n" . '				</DELIVERY>';
        }
        $postXML .="\n" . '				<CUSTOMERREF>' . $rsdata['Consignment']['CustomerReference'] . '</CUSTOMERREF>';
        $postXML .="\n" . '				<CONTYPE>' . $rsdata['Consignment']['ConsigmentType'] . '</CONTYPE>';
        $postXML .="\n" . '				<PAYMENTIND>S</PAYMENTIND>';
        $rstotalweight = 0;
        $rstotalvolume = 0;
        foreach ($rsdata['Packages'] as $rspackage) {
            $rstotalweight += $rspackage['Weight'] * $rspackage['Quantity'];
            $rstotalvolume += ( ($rspackage['Length']) * ($rspackage['Height']) * ($rspackage['Width']) ) * $rspackage['Quantity'];
        }
        $postXML .="\n" . '				<ITEMS>' . count($rsdata['Packages']) . '</ITEMS>';
        $postXML .="\n" . '				<TOTALWEIGHT>' . $rstotalweight . '</TOTALWEIGHT>';
        $postXML .="\n" . '				<TOTALVOLUME>' . $rstotalvolume . '</TOTALVOLUME>';
        $postXML .="\n" . '				<CURRENCY>' . $rsdata['Consignment']['Currency'] . '</CURRENCY>';
        $postXML .="\n" . '				<GOODSVALUE>' . $rsdata['Consignment']['GoodsValue'] . '</GOODSVALUE>';
        $postXML .="\n" . '				<INSURANCEVALUE>' . $rsdata['Consignment']['InsuranceValue'] . '</INSURANCEVALUE>';
        $postXML .="\n" . '				<SERVICE>' . $rsdata['Consignment']['Service'] . '</SERVICE>';
        $postXML .="\n" . '				<OPTION>' . $rsdata['Consignment']['Option'] . '</OPTION>';
        $postXML .="\n" . '				<DESCRIPTION>' . $rsdata['Consignment']['Description'] . '</DESCRIPTION>';
        $postXML .="\n" . '				<DELIVERYINST>' . $rsdata['Consignment']['DeliveryInstructions'] . '</DELIVERYINST>';
        foreach ($rsdata['Packages'] as $rspackage) {
            $postXML .="\n" . '				<PACKAGE>';
            $postXML .="\n" . '					<ITEMS>' . $rspackage['Quantity'] . '</ITEMS>';
            $postXML .="\n" . '					<DESCRIPTION>' . $rspackage['Description'] . '</DESCRIPTION>';
            $postXML .="\n" . '					<LENGTH>' . $rspackage['Length'] . '</LENGTH>';
            $postXML .="\n" . '					<HEIGHT>' . $rspackage['Height'] . '</HEIGHT>';
            $postXML .="\n" . '					<WIDTH>' . $rspackage['Width'] . '</WIDTH>';
            $postXML .="\n" . '					<WEIGHT>' . $rspackage['Weight'] . '</WEIGHT>';
            $postXML .="\n" . '				</PACKAGE>';
        }
        $postXML .="\n" . '			</DETAILS>';
        $postXML .="\n" . '		</CONSIGNMENT>';
        $postXML .="\n" . '	</CONSIGNMENTBATCH>';
        $postXML .="\n" . '	<ACTIVITY>';
        $postXML .="\n" . '		<CREATE>';
        $postXML .="\n" . '          <CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF>';
        $postXML .="\n" . '     </CREATE>';
        $postXML .="\n" . '		<RATE><CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF></RATE>';
        $postXML .="\n" . '		<BOOK ShowBookingRef="Y"><CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF></BOOK>';
        $postXML .="\n" . '     <SHIP><CONREF>'. $rsdata['Consignment']['UniqueReference'] .'</CONREF></SHIP>';
        $postXML .="\n" . '		<PRINT>';
        $postXML .="\n" . '			<CONNOTE><CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF></CONNOTE>';
        $postXML .="\n" . '			<LABEL><CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF></LABEL>';
        $postXML .="\n" . '			<MANIFEST><CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF></MANIFEST>';
        $postXML .="\n" . '			<INVOICE><CONREF>' . $rsdata['Consignment']['UniqueReference'] . '</CONREF></INVOICE>';
        $postXML .="\n" . '			<EMAILTO>' . $rsdata['Consignment']['EmailTo'] . '</EMAILTO>';
        $postXML .="\n" . '			<EMAILFROM>' . $rsdata['Consignment']['EmailFrom'] . '</EMAILFROM>';
        $postXML .="\n" . '		</PRINT>';
        $postXML .="\n" . '	</ACTIVITY>';
        $postXML .="\n" . '</ESHIPPER>';

        $hoy = (string) date("Y-m-d-H-i-s");
        $xmlRequest = fopen("request/$hoy.xml", "w");
        for ($escrito = 0; $escrito < strlen($postXML); $escrito += $fwrite) {
            $fwrite = fwrite($xmlRequest, $postXML);
        }
        //sleep(3);

        $resultXML = $this->send_xml($postXML, 'https://iconnection.tnt.com/ShipperGate2.asp');
        if (strpos($resultXML, 'COMPLETE:') === false) {
            // Error - Sin Respuesta
            return array('Success' => 0, 'Result' => 'No communication with TNT', 'Envio' => 'ERROR COMPLETE');
        } else {
            // Revisar si hay Errores: Regresar FALSE y XML
            $resultCode = substr($resultXML, 9 - strlen($resultXML));
            $resultXML = $this->send_xml('GET_RESULT:' . $resultCode, 'https://iconnection.tnt.com/ShipperGate2.asp');
            $no_hay_error = true;
            $fileXML = new SimpleXMLElement($resultXML);
            if ($fileXML->xpath('//ERROR')) {
                if ($fileXML->xpath('/document/RATE/ERROR')) {
                    $code_a_error = $fileXML->xpath('/document/RATE/ERROR/CODE');
                    $desc_a_error = $fileXML->xpath('/document/RATE/ERROR/DESCRIPTION');
					if(!empty($code_a_error) && !empty($desc_a_error)){
						//list(, $code_de_error) = $code_a_error[0][0];
						//list(, $desc_de_error) = $desc_a_error[0][0];
						$code_de_error = $code_a_error[0][0];
						$desc_de_error = $desc_a_error[0][0];
						if ($code_de_error != 'P13') {
							$no_hay_error = false;
						}
					}else{
						$code_de_error = 0;
						$desc_de_error = 'Connection Lost';
					}
					return array('Success' => 0, 'Result' => $code_de_error . ': ' . $desc_de_error, 'Envio' => $resultCode, 'XML_Result' => $resultXML);
                } else {
                    $no_hay_error = false;
                    return array('Success' => 0, 'Result' => $this->get_results_array($fileXML), 'Envio' => $resultCode, 'XML_Result' => $resultXML);
                }
            }
            if ($no_hay_error) {
                // No hay Error. Mete el codigo xml en posiciones del array finalArray.
                $finalArray = array('Success' => 1, 'Albaran' => '', 'Result' => $this->get_results_array($fileXML), 'Envio' => $resultCode, 'XML_Result' => $resultXML);
                $finalArray['Albaran'] = $finalArray['Result']['CREATE']['CONNUMBER'];
                $resultXML = $this->send_xml('GET_CONNOTE:' . $resultCode, 'https://iconnection.tnt.com/ShipperGate2.asp');
                $resultXML = str_replace('http://iconnection.tnt.com:81/Shipper/NewStyleSheets/', $xslPath, $resultXML);
                $resultXML = str_replace('http://iconnection.tnt.com:81', 'https://iconnection.tnt.com', $resultXML);
                $finalArray['CartaPorte'] = $resultXML;
                $resultXML = $this->send_xml('GET_LABEL:' . $resultCode, 'https://iconnection.tnt.com/ShipperGate2.asp');
                $resultXML = str_replace('http://iconnection.tnt.com:81/Shipper/NewStyleSheets/', $xslPath, $resultXML);
                $resultXML = str_replace('http://iconnection.tnt.com:81', 'https://iconnection.tnt.com', $resultXML);
                $finalArray['Etiqueta'] = $resultXML;
                $resultXML = $this->send_xml('GET_MANIFEST:' . $resultCode, 'https://iconnection.tnt.com/ShipperGate2.asp');
                $resultXML = str_replace('http://iconnection.tnt.com:81/Shipper/NewStyleSheets/', $xslPath, $resultXML);
                $resultXML = str_replace('http://iconnection.tnt.com:81', 'https://iconnection.tnt.com', $resultXML);
                $finalArray['Manifesto'] = $resultXML;
                $resultXML = $this->send_xml('GET_INVOICE:' . $resultCode, 'https://iconnection.tnt.com/ShipperGate2.asp');
                $resultXML = str_replace('http://iconnection.tnt.com:81/Shipper/NewStyleSheets/', $xslPath, $resultXML);
                $resultXML = str_replace('http://iconnection.tnt.com:81', 'https://iconnection.tnt.com', $resultXML);
                $finalArray['Factura'] = $resultXML;
                $resultXML = $this->send_xml('GET_RESULT:' . $resultCode, 'https://iconnection.tnt.com/ShipperGate2.asp');
                $finalArray['Result'] = $resultXML;
                return $finalArray;
            }
        }
    }

    /**
     * Returns the requested DataSets From TNT
     *
     * @dPostcodeMask Boolean indicates if PostcodeMask DataSet is required
     * @dCountry Boolean indicates if Country DataSet is required
     * @dCurrency Boolean indicates if Currency DataSet is required
     * @dTowngroup Boolean indicates if Towngroup DataSet is required
     * @dService Boolean indicates if Service DataSet is required
     * @dOption Boolean indicates if Option DataSet is required
     */
    function get_datasets($dPostCodeMask = true, $dCountry = true, $dCurrency = true, $dService = true, $dTownGroup = true, $dOption = true) {
        $postXML = '<?xml version="1.0" standalone="no"?>';
        $postXML .="\n" . '<!DOCTYPE SETREQUEST SYSTEM "https://iconnection.tnt.com/PriceCheckerDTD1.0/SetRequestIN.dtd">';
        $postXML .="\n" . '<SETREQUEST>';
        $postXML .="\n" . '	<LOGIN>';
        $postXML .="\n" . '		<COMPANY>' . $this->customerId . '</COMPANY>';
        $postXML .="\n" . '		<PASSWORD>' . $this->customerPassword . '</PASSWORD>';
        $postXML .="\n" . '		<APPID>PC</APPID>';
        $postXML .="\n" . '	</LOGIN>';
        $postXML .="\n" . '	<REQUEST>';
        $postXML .="\n" . '		<DATASET>';
        if ($dPostCodeMask)
            $postXML .= 'PostcodeMask ';
        if ($dCountry)
            $postXML .= 'Country ';
        if ($dCurrency)
            $postXML .= 'Currency ';
        if ($dTownGroup)
            $postXML .= 'Towngroup ';
        if ($dService)
            $postXML .= 'Service ';
        if ($dOption)
            $postXML .= 'Option ';
        $postXML .="\n" . '		</DATASET>';
        $postXML .="\n" . '	</REQUEST>';
        $postXML .="\n" . '</SETREQUEST>';
        $resultXML = new SimpleXMLElement($this->send_xml($postXML, 'https://iconnection.tnt.com/priceGate.asp'));
        return $this->get_datasets_array($resultXML);
    }

    /**
     * Checks TNT Rates
     *
     * @rsdata Array containing all information
     */
    function check_prices($rsdata, $versions = '') {
        if (!is_array($versions))
            $versions = $this->get_datasets(0, 0, 0, 0, 0, 0);
        $postXML = '<?xml version="1.0" standalone="no"?>';
        $postXML .="\n" . '<!DOCTYPE SETREQUEST SYSTEM "https://iconnection.tnt.com/PriceCheckerDTD1.0/PriceRequestIN.dtd">';
        $postXML .="\n" . '<PRICEREQUEST>';
        $postXML .="\n" . '	<LOGIN>';
        $postXML .="\n" . '		<COMPANY>' . $this->customerId . '</COMPANY>';
        $postXML .="\n" . '		<PASSWORD>' . $this->customerPassword . '</PASSWORD>';
        $postXML .="\n" . '		<APPID>PC</APPID>';
        $postXML .="\n" . '	</LOGIN>';
        $postXML .="\n" . '	<DATASETS>';
        $postXML .="\n" . '		<COUNTRY>' . $versions['VERSIONS']['COUNTRYSET'] . '</COUNTRY>';
        $postXML .="\n" . '		<CURRENCY>' . $versions['VERSIONS']['CURRENCYSET'] . '</CURRENCY>';
        $postXML .="\n" . '		<POSTCODEMASK>' . $versions['VERSIONS']['POSTCODEMASKSET'] . '</POSTCODEMASK>';
        $postXML .="\n" . '		<TOWNGROUP>' . $versions['VERSIONS']['TOWNGROUPSET'] . '</TOWNGROUP>';
        $postXML .="\n" . '		<SERVICE>' . $versions['VERSIONS']['SERVICESET'] . '</SERVICE>';
        $postXML .="\n" . '		<OPTION>' . $versions['VERSIONS']['OPTIONSET'] . '</OPTION>';
        $postXML .="\n" . '	</DATASETS>';
        foreach ($rsdata as $rspricechk) {
            $postXML .="\n" . '	<PRICECHECK>';
            $postXML .="\n" . '		<RATEID>' . $rspricechk['RateId'] . '</RATEID>';
            $postXML .="\n" . '		<ORIGINCOUNTRY>' . $rspricechk['OriginCountry'] . '</ORIGINCOUNTRY>';
            $postXML .="\n" . '		<ORIGINTOWNNAME>' . $rspricechk['OriginCity'] . '</ORIGINTOWNNAME>';
            $postXML .="\n" . '		<ORIGINPOSTCODE>' . $rspricechk['OriginPostCode'] . '</ORIGINPOSTCODE>';
            $postXML .="\n" . '		<ORIGINTOWNGROUP>' . $rspricechk['OriginTownGroup'] . '</ORIGINTOWNGROUP>';
            $postXML .="\n" . '		<DESTCOUNTRY>' . $rspricechk['DestinCountry'] . '</DESTCOUNTRY>';
            $postXML .="\n" . '		<DESTTOWNNAME>' . $rspricechk['DestinCity'] . '</DESTTOWNNAME>';
            $postXML .="\n" . '		<DESTPOSTCODE>' . $rspricechk['DestinPostCode'] . '</DESTPOSTCODE>';
            $postXML .="\n" . '		<DESTTOWNGROUP>' . $rspricechk['DestinTownGroup'] . '</DESTTOWNGROUP>';
            $postXML .="\n" . '		<CONTYPE>' . $rspricechk['ConsigmentType'] . '</CONTYPE>';
            $postXML .="\n" . '		<CURRENCY>' . $rspricechk['Currency'] . '</CURRENCY>';
            $postXML .="\n" . '		<WEIGHT>' . $rspricechk['Weight'] . '</WEIGHT>';
            $postXML .="\n" . '		<VOLUME>' . $rspricechk['Volume'] . '</VOLUME>';
            $postXML .="\n" . '		<ACCOUNT>' . $rspricechk['Account'] . '</ACCOUNT>';
            $postXML .="\n" . '		<ITEMS>' . $rspricechk['Items'] . '</ITEMS>';
            $postXML .="\n" . '		<SERVICE>' . $rspricechk['Service'] . '</SERVICE>';
            $postXML .="\n" . '	</PRICECHECK>';
        }
        $postXML .="\n" . '</PRICEREQUEST>';
        $resultXML = new SimpleXMLElement($this->send_xml($postXML, 'https://iconnection.tnt.com/priceGate.asp'));
        return $this->get_prices_array($resultXML);
    }

    /**
     * Sends CURL and gets results in string format
     *
     * @postXML XML string to send to TNT
     */
    protected function send_xml($postXML, $postURL) {
        curl_setopt($this->curlCall, CURLOPT_URL, $postURL);
        curl_setopt($this->curlCall, CURLOPT_POSTFIELDS, 'xml_in=' . urlencode($postXML));
        curl_setopt($this->curlCall, CURLOPT_SSLVERSION, 3);
        $curlResult = curl_exec($this->curlCall);
        if (curl_errno($this->curlCall)) {
            return false;
        } else {
            return $curlResult;
        }
    }

    /**
     * Returns Tracking XML in Array form
     *
     * @trackingXML XML string to convert to Array
     */
    protected function get_tracking_array($trackingXML) {
        $returnArray = array();
        foreach ($trackingXML->Consignment as $consignment) {
            $xmlAlbaran = (string) $consignment->ConsignmentNumber;
            $returnArray[$xmlAlbaran]['SummaryCode'] = (string) $consignment->SummaryCode;
            foreach ($consignment->children() as $second_gen) {
                switch ($second_gen->getName()) {
                    case 'SummaryCode':
                    case 'ConsignmentNumber':
                        break;
                    case 'StatusData':
                        $status = array();
                        foreach ($second_gen->children() as $third_gen)
                            $status[$third_gen->getName()] = (string) $third_gen;
                        $returnArray[$xmlAlbaran][$second_gen->getName()][] = $status;
                        break;
                    case 'Addresses':
                        foreach ($second_gen->children() as $third_gen)
                            foreach ($third_gen->children() as $fourth_gen)
                                $returnArray[$xmlAlbaran][$second_gen->getName()][(string) $third_gen['addressParty']][$fourth_gen->getName()] = (string) $fourth_gen;
                        break;
                    default:
                        $returnArray[$xmlAlbaran][$second_gen->getName()] = (string) $second_gen;
                        foreach ($second_gen->children() as $third_gen)
                            $returnArray[$xmlAlbaran][$second_gen->getName()][$third_gen->getName()] = (string) $third_gen;
                        break;
                }
            }
        }
        return $returnArray;
    }

    /**
     * Returns DataSets XML in Array form
     *
     * @trackingXML XML string to convert to Array
     */
    protected function get_datasets_array($datasetsXML) {
        $returnArray = array();
        foreach ($datasetsXML->DATASETS as $dataset) {
            foreach ($dataset->children() as $second_gen) {
                switch ($second_gen->getName()) {
                    case 'VERSIONS':
                        foreach ($second_gen->children() as $third_gen)
                            $returnArray[$second_gen->getName()][$third_gen->getName()] = (string) $third_gen;
                        break;
                    default:
                        $datagroup = array();
                        foreach ($second_gen->children() as $third_gen)
                            $datagroup[$third_gen->getName()] = (string) $third_gen;
                        $returnArray[$second_gen->getName()][] = $datagroup;
                        break;
                }
            }
        }
        return $returnArray;
    }

    /**
     * Returns Prices XML in Array form
     *
     * @trackingXML XML string to convert to Array
     */
    protected function get_prices_array($pricesXML) {
        $returnArray = array();
        foreach ($pricesXML->children() as $first_gen) {
            switch ($first_gen->getName()) {
                case 'DATASETS':
                    break;
                case 'PRICE':
                    $price = array();
                    foreach ($first_gen->children() as $second_gen) {
                        $price[$second_gen->getName()] = (string) $second_gen;
                    }
                    $returnArray[] = $price;
                    break;
            }
        }
        return $returnArray;
    }

    /**
     * Returns Results XML in Array form
     *
     * @resultsXML XML string to convert to Array
     */
    protected function get_results_array($resultsXML) {
        $returnArray = array();
        foreach ($resultsXML->children() as $first_gen) {
            $returnArray[$first_gen->getName()] = (string) $first_gen;
            foreach ($first_gen->children() as $second_gen) {
                $returnArray[$first_gen->getName()][$second_gen->getName()] = (string) $second_gen;
                foreach ($second_gen->children() as $third_gen)
                    $returnArray[$first_gen->getName()][$second_gen->getName()][$third_gen->getName()] = (string) $third_gen;
            }
        }
        return $returnArray;
    }

}

# ...end...