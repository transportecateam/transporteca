<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

    $kExplain = new cExplain();
    $idLandingPage='9';
    $landingPageDataAry = $kExplain->getAllNewLandingPageData($idLandingPage);
    $testimonialAry = $kExplain->getAllTestimonials($idLandingPage);
    
    $partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);
    $ctr=0;
    if(!empty($landingPageDataAry))
    {
        $landingPageData_Ary=$landingPageDataAry[0];
        foreach($landingPageData_Ary as $key=>$values)
        {
            if($key!='id' && $key!='iOrder' && $key!='iPublished')
            {
                //echo $key."key<br /><br />";
                $keyArr[$ctr]=$key;
                if($key=='iLanguage')
                {
                    $valuesArr[$ctr]=9;
                }
                else if($key=='szSeoKeywords')
                {
                    $valuesArr[$ctr]='Italian';
                }
                else if($key=='szUrl')
                {
                    $valuesArr[$ctr]='/it';
                }
                else
                {
                    $valuesArr[$ctr]=mysql_escape_custom(trim($values));
                }
                ++$ctr;
            }
        }
        
        if(!empty($keyArr))
        {
            $keyStr=implode(",",$keyArr);
            //echo $keyStr."<br /><br />";
        }
        if(!empty($valuesArr))
        {
            $valuesStr=implode("','",$valuesArr);
            $valuesStr="'".$valuesStr."'";
            //echo "'".$valuesStr."'";
        }
        
        $idNewLandingPage= $kExplain->copyNewLandingPage($keyStr,$valuesStr);
        //$idNewLandingPage='18';
        if((int)$idNewLandingPage>0)
        {
           
            if(!empty($testimonialAry))
            {
                foreach($testimonialAry as $testimonial_Ary)
                {
                     $ctr=0;
                    foreach($testimonial_Ary as $key=>$values)
                    {
                        if($key!='id')
                        {
                             $keyTestArr[$ctr]=$key;
                            if($key=='idLandingPage')
                            {
                                $valuesTestArr[$ctr]=(int)$idNewLandingPage;
                            }
                            else
                            {
                                $valuesTestArr[$ctr]=mysql_escape_custom(trim($values));
                            }
                            ++$ctr;
                        }
                    }

                    if(!empty($keyTestArr))
                    {
                        $keyTestStr=implode(",",$keyTestArr);
                    }
                    if(!empty($valuesTestArr))
                    {
                        $valuesTestStr=implode("','",$valuesTestArr);
                        $valuesTestStr="'".$valuesTestStr."'";
                    }
                    
                    $kExplain->copyTestimonialData($keyTestStr,$valuesTestStr);
                }

            }
            
            
            if(!empty($partnerLogoAry))
            {
                foreach($partnerLogoAry as $partnerLogo_Ary)
                {
                     $ctr=0;
                    foreach($partnerLogo_Ary as $key=>$values)
                    {
                        if($key!='id')
                        {
                             $keypartnerArr[$ctr]=$key;
                            if($key=='idLandingPage')
                            {
                                $valuespartnerArr[$ctr]=(int)$idNewLandingPage;
                            }
                            else
                            {
                                $valuespartnerArr[$ctr]=mysql_escape_custom(trim($values));
                            }
                            ++$ctr;
                        }
                    }

                    if(!empty($keypartnerArr))
                    {
                        $keypartnerStr=implode(",",$keypartnerArr);
                    }
                    if(!empty($valuespartnerArr))
                    {
                        $valuespartnerStr=implode("','",$valuespartnerArr);
                        $valuespartnerStr="'".$valuespartnerStr."'";
                    }
                    
                    
                   $kExplain->copyPartnerimonialData($keypartnerStr,$valuespartnerStr);
                }

            }
        }
    }
    
    ?>
    
    