<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
  
$szMetaTitle = __BOOKING_GET_RATE_META_TITLE__;
$szMetaKeywords = __BOOKING_GET_RATE_META_KEYWORDS__;
$szMetaDescription = __BOOKING_GET_RATE_META_DESCRIPTION__;
$mode='GET_RATES';
//$iDonotIncludeHiddenSearchHeader = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingDetails/";
$t_base_user = "Users/AccountPage/";
$t_base_error = "Error";
$kBooking = new cBooking();
$kConfig = new cConfig();
$kWHSSearch=new cWHSSearch();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
  
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
$postSearchAry = $kBooking->getBookingDetails($idBooking);
$idLandingPage = $postSearchAry['idLandingPage'] ;

if($idLandingPage<=0)
{
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
        $idLandingPage = 2; 
    }
    else
    {
        $idLandingPage = 1; 
    }  
} 
$userSessionData = array();
$userSessionData = get_user_session_data();
$idUserSession = $userSessionData['idUser'];

$iLanguage = getLanguageId();
 
if((int)$_REQUEST['idBooking']>0)
{
    $idBooking = (int)$_REQUEST['idBooking'] ;
    $idBooking = $idBooking ;

    $kBooking = new cBooking();
    if($idUserSession>0)
    {
        if($kBooking->isBookingBelongsToCustomer($idBooking,$idUserSession))
        {
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
            $booking_mode = 2;
        }
        else
        {
            header("Location:".__NEW_SEARCH_PAGE_URL__);
            die;
        }
    }
}
else if((int)$idBooking>0)
{
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);	

    if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
        echo display_booking_already_paid($t_base,$idBooking);
        die;
    }
}
else
{
    $redirect_url = __NEW_SEARCH_PAGE_URL__ ;
    ?>
    <div id="change_price_div">
    <?php
    display_booking_notification($redirect_url);
    echo "</div>";
}  

if(((int)$idUserSession>0) && ($_REQUEST['err']<=0))
{
    checkUserPrefferedCurrencyActive();
}
 
$kUser = new cUser();  
$kRegisterShipCon = new cRegisterShipCon(); 

//if($iLanguage==__LANGUAGE_ID_DANISH__) 
//{ 
//    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__'); 
//} 
//else 
//{
    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage); 
//} 

$customerTestimonialAry = array();
$customerTestimonialAry = $kExplain->getAllTestimonials($idLandingPage);

$partnerLogoAry = array();
$partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);
 
/*
 * This code was used when we searched everything on landing page.
 */ 
$tempResultAry = $kWHSSearch->getSearchedDataFromTempData($idBooking);
$searchResultAry = unserialize($tempResultAry['szSerializeData']); 

$iSearchResultType = $tempResultAry['iSearchResultType']; 

$cheapestServiceAry = array();
$fastestServiceAry = array();
 
//Finding out cheapest service
$searchResultArys = sortArray($searchResultAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);
$cheapestServiceAry = $searchResultArys[0];

//Finding out fastest service
$searchResultArys = sortArray($searchResultAry,'iAvailabeleDate_time',false,'fDisplayPrice',false,false,false,true);
$fastestServiceAry = $searchResultArys[0];
  
if($fastestServiceAry['idCourierProvider']<=0)
{
   $fastestServiceAry['dtAvailableDate'] =  $fastestServiceAry['dtAvailabeleDate_formated'] ;
}

$iMonth = date('m',strtotime($fastestServiceAry['dtAvailableDate'])); 
$szDeliveryDate = date('j.',strtotime($fastestServiceAry['dtAvailableDate'])); 
$szMonthName = getMonthName($iLanguage,$iMonth);
$szDeliveryDate .=" ".$szMonthName ;
 
$fCheapestRate = $cheapestServiceAry['szCurrency']." ".getPriceByLang($cheapestServiceAry['fDisplayPrice'],$iLanguage);
  
$iForwarderCount = 0;
if($iSearchResultType==1) //Both LCL and Courier services available
{
    if(!empty($searchResultAry))
    {
        foreach($searchResultAry as $res_arys)
        {							
            if(!empty($forwarderAry) && in_array($res_arys['idForwarder'],$forwarderAry))
            {
                continue;
            }
            else
            {
                $forwarderAry[] = $res_arys['idForwarder'];
                $iForwarderCount++;
            }
        }
    }
    $szFastestServiceText = t($t_base.'fields/fastest_can_deliver_on')." ".$szDeliveryDate;
    $szCheapestServiceText = t($t_base.'fields/cheapest_can_do_for')." ".$fCheapestRate;
    $szPageHeadingText = t($t_base.'fields/forwarders_ready_sharp_price');
} 
else if($iSearchResultType==2) //Only LCL services available
{
    $szFastestServiceText = t($t_base.'fields/fastest_deliver_by_sea_on')." ".$szDeliveryDate;
    $szCheapestServiceText = t($t_base.'fields/cheapest_can_do_for')." ".$fCheapestRate;
    $szPageHeadingText = t($t_base.'fields/sharp_prices_from_selected_fwd');
    $iForwarderCount = count($searchResultAry); 
}
else if($iSearchResultType==3) //Only Courier services available
{
    $szFastestServiceText = t($t_base.'fields/fastest_can_deliver_on')." ".$szDeliveryDate;
    $szCheapestServiceText = t($t_base.'fields/cheapest_airfreight_for')." ".$fCheapestRate;
    $szPageHeadingText = t($t_base.'fields/sharp_prices_from_selected_fwd');
    $iForwarderCount = count($searchResultAry); 
}  
$mode = 'GET_RATES';    

$szPageHeadingText = t($t_base.'fields/forwarders_ready_sharp_price');
$iForwarderCount = 14;

$iQuickQuote = false;
$iDefaultCalculaVal = 1;
if($postSearchAry['iQuickQuote']>0)
{
    $iQuickQuote = true;
    $iDefaultCalculaVal = 0;
}
?>  
<script type='text/javascript'>
    $().ready(function(){  
        <?php if(!$iQuickQuote){ ?>
            generate_service_list('<?php echo $szBookingRandomNum; ?>',3);
        <?php } ?>
         
        /*
         *  
        setTimeout(function(){
            $("#forwarder_count").html('<?php echo $iForwarderCount; ?>');
            $("#service-available-container").attr('style','display:block;'); 
        },'3000'); */
    
        $(".get-rate-quote-submit-button").unbind("click"); 
        $(".get-rate-quote-submit-button").attr("style","opacity:1;"); 
        $(".get-rate-quote-submit-button").click(function(){ validate_booking_quote_form('booking_quote_contact_detail_form','GET_RATES'); }); 
    });
</script>
<div id="hsbody-2" class="quote-page get-rates"> 
    <div id="all_available_service" style="display:none;"></div>
    <div id="customs_clearance_pop_right" class="help-pop right"></div> 
    <div id="customs_clearance_pop" class="help-pop"></div>
    <div id="change_price_div" style="display:none"></div>  
    <div class="hsbody-2-right">
        <div class="forwarder-ready clearfix">
            <div class="number" id='forwarder_count'>
                <!--<img src="<?=__BASE_STORE_IMAGE_URL__?>/Large_wheel.gif" alt="" />-->
                <?php echo $iForwarderCount; ?>
            </div>
            <div class="text">
                <h3 id="get-rate-page-heading"><?php echo $szPageHeadingText; ?></h3> 
                <!-- id="service-available-container" -->
                <div class="service-available"  style="display:block;">
                    <p class="get-quote-sub-heading"><?php echo t($t_base.'fields/complete_your_contact_details'); ?></p> 
                </div>
            </div> 
        </div>
        <div class="get-price" id="booking_quotes_details_container">
            <div class="clearfix">
                <div class="price-guarantee-points">
                    <h3><?php echo t($t_base.'fields/price_guarantee'); ?></h3>
                    <p>1. <?php echo t($t_base.'fields/price_guarantee_text_1'); ?></p>
                    <p>2. <?php echo t($t_base.'fields/price_guarantee_text_2'); ?></p>
                    <p class="read-more"><a href="javascript:void(0);" onclick="focus_price_guarantee('price-guarantee-container');" name='price-guarantee'><?php echo t($t_base.'fields/read_more'); ?></a></p>
                </div>
                <div class="mobile-price-guarantee-points">
                    <h3><?php echo t($t_base.'fields/fill_your_info_to_see_the_price');?></h3>
                    <div class="checkbox-mobile">
                        <span class="checkbox-image-mobile">                       
                            <?php echo t($t_base.'fields/easy_fast_no_obligations'); ?>
                        </span>
                        <span class="checkbox-image-mobile">                      
                           <?php echo t($t_base.'fields/free_access_for_current_prices'); ?>
                        </span>
                    </div>
                </div>
                <div class="price-form" id="booking-form-main-container">
                    <?php
                        echo display_booking_quotes_details_form($postSearchAry,$kRegisterShipCon,$mode);
                    ?>
                </div>
            </div>
        </div> 
        
        <div class="image-names clearfix">
            <div><?php echo t($t_base.'fields/morten_laekholm'); ?><span><?php echo t($t_base.'fields/partner_transporteca'); ?></span></div>
            <div><?php echo t($t_base.'fields/thorsten_boeck'); ?><span><?php echo t($t_base.'fields/partner_transporteca'); ?></span></div>
        </div>
    </div> 
    <input type="hidden" id="calculate_service_list_flag" name="calculate_service_list_flag" value="<?php echo $iDefaultCalculaVal; ?>">
    
</div>
<?php echo display_get_quote_get_rate_bottom_section($mode); ?> 
<?php
	echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
?> 
<?php
 $iGetRateGetQuotePageAdword='1';
  require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>				