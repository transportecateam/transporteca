<?php   
ob_start();
session_start(); 

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
   
echo phpinfo();
die; 
$kWhsSearch = new cWHSSearch();
$szGoogleMapv3APIKey = $kWhsSearch->getManageMentVariableByDescription('__GOOGLE_MAP_V3_API_KEY__'); 

$address = "2300+copenhagen+Denmark"; 

$iHttps = (int)$_GET['secure'];
try
{
    if($iHttps==1)
    {
        $url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false"; 
    }
    else
    {
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false"; 
    } 
    $c = curl_init(); 
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE); 
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, TRUE);
    curl_setopt ($c, CURLOPT_SSL_VERIFYHOST, TRUE); 
    $result = curl_exec($c);
    $err  = curl_getinfo($c,CURLINFO_HTTP_CODE); 
    curl_close($c);  
     
    if(!empty($result))
    {
       echo "API Result <br>";  
       echo $result; 
    }
    else
    {
        echo "Got CURL Error: ".curl_errno($c);
    } 
} 
catch (Exception $ex)
{
    echo "Exception in Google API";
    print_R($ex);
} 