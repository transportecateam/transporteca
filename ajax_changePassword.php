<?php
/**
 * Change Customer Account Password
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$successPasswordUpdatedFlag=false;
$t_base = "Users/AccountPage/";
$t_base_error = "Error";
$kConfig = new cConfig();
$kUser = new cUser();
if(!empty($_POST['editUserInfoArr']))
{
	if($kUser->changePassword($_POST['editUserInfoArr'],$_SESSION['user_id']))
	{
		$successPasswordUpdatedFlag=true;
	}
}
?>
<?php
if(!empty($kUser->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kUser->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }
if($successPasswordUpdatedFlag)
{
	echo "<p style=color:green>".t($t_base.'messages/password_change_successfully')."</p>";
?>
<div class="ui-fields">
	<span class="field-name"><strong><?=t($t_base.'fields/password');?></strong> <a href="javascript:void(0);" onclick="cancel_user_info(3);"><?=t($t_base.'fields/edit');?></a></span>
		<span class="field-container">*************</span>
</div>
<?php	
}else{
?>
<form name="changePassword" id="changePassword" method="post">
<h5><strong>Password</strong> </h5>		
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'fields/current_password')?></span>
	<span class="field-container"><input type="password" name="editUserInfoArr[szOldPassword]" id="szOldPassword"/></span>
</label>
<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'fields/password');?></span>
	<span class="field-container"><input type="password" name="editUserInfoArr[szNewPassword]" id="szNewPassword" onblur="closeTip('pass');" onfocus="openTip('pass');"/></span>
	<div class="field-alert"><div id="pass" style="display:none;"><?=t($t_base.'messages/password_msg');?>#)</div></div>
</label>

<label class="profile-fields">
	<span class="field-name"><?=t($t_base.'fields/con_new_password')?></span>
	<span class="field-container"><input type="password" name="editUserInfoArr[szConPassword]" id="szConPassword"/></span>
</label>
<br />
<br />
<p align="center" style="width:60%"><a href="javascript:void(0);" class="button1" onclick="changeUserPassword();"><span>Save</span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_user_info();"><span><?=t($t_base.'fields/cancel');?></span></a> </p>
</form>
<?php }?>