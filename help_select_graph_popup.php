<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH__ . "/inc/functions.php" );
include( __APP_PATH__ . "/inc/I18n.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");

$t_base = "home/homepage/";

?>
<div id="popup-bg"></div>
	<div id="popup-container">
	  <div class="help-select-graph-popup popup">
		<h5><?=t($t_base.'fields/grap_popup_title');?></h5>
		<div class="links">
			<span class="first"><?=t($t_base.'fields/Shipper_door');?></span>
			<span class="third"><?=t($t_base.'fields/origin_warehouse');?> </span>
			<span class="seventh"><?=t($t_base.'fields/destination_warehouse');?></span>
			<span class="ninth"><?=t($t_base.'fields/consigner_door');?></span>
		</div>
		<p align="center" class="shipment"><?=t($t_base.'fields/shipment_flow');?></p>
		<p align="center"><?=t($t_base.'messages/DTD');?></p>
		<p align="center"><?=t($t_base.'messages/DTW');?></p>
		<p align="center"><?=t($t_base.'messages/WTD');?></p>
		<p align="center"><?=t($t_base.'messages/WTW');?></p>
		<br />
		<br />
		<p align="center"><a href="javascript:void(0);" onclick="showHide('Transportation_pop');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
</div>
</div>
