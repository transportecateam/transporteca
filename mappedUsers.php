<?php
 ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/MultiUserAccess/";
$t_base_error = "Error";
$showpop=false;
$successflag=false;
if($_REQUEST['flag']=='NONREGISTERUSER')
{
	$szEmail=$_REQUEST['szEmail'];
	if($kUser->sendEmailNonRegisterUser($szEmail))
	{?>
		<div id="mapped_user_non_register">
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup remove-shippers-popup">
				<p class="close-icon" align="right">
				<a onclick="cancel_remove_user_popup('mapped_user_non_register','hsbody-2');" href="javascript:void(0);">
				<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
				</a>
				</p>
				<h5><strong><?=t($t_base.'title/thank_you');?></strong></h5>
				<p><?=t($t_base.'messages/non_register_thankyou_popup_msg');?></p><br/>
				<p align="center"><a href="javascript:void(0)" class="button1"  onclick="cancel_remove_user_popup('mapped_user_non_register','hsbody-2');"><span><?=t($t_base.'fields/close');?></span></a></p>
				</div>
			</div>
		</div>
	<?		
	}
}
else
{
if(!empty($_REQUEST['mappedUser']))
{
	$idCustomer = (int)$_SESSION['user_id'];
	$kUser->getUserDetails($idCustomer);
	
	// if user's profile is incomplete then do not show shipper consignee dropdowns 
	if((int)$kUser->iIncompleteProfile==0)
	{	
		//print_r($_REQUEST['mappedUser']);
		$user_profile_complete = true;
		$userDetailArr=$kUser->checkEmailAddress($_REQUEST['mappedUser']);
		//print_r($userDetailArr);
		if(!empty($userDetailArr))
		{
			$showpop=true;
			$successflag=true;	
		}
		else
		{
			$szEmail=$_REQUEST['mappedUser']['szEmail'];
			$showpop=true;
		}
	}
	else
	{
		$user_profile_complete = false;
		$showpop=true;
		$kUser->addError('mappedUser',t($t_base.'messages/incomplete_profile_error_message'));
	}
}
$flag = 0;
if(!empty($_REQUEST['mappedArr']))
{
	if($kUser->mappedUser($_REQUEST['mappedArr']))
	{
		$flag=1;	
		//echo $flag;
	}
}
}
if($showpop){
?>
<script type="text/javascript">
//disableForm('hsbody-2');
</script>
<div id="mapped_user_id">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup remove-shippers-popup">
<p class="close-icon" align="right">
<a onclick="cancel_remove_user_popup('mapped_user_id','hsbody-2');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<form name="linkedUser" id="linkedUser" method="post">
	<?php
		if(!empty($kUser->arErrorMessages)){
		?>
		<div id="regError" class="errorBox ">
		<div class="header"><?=t($t_base_error.'/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kUser->arErrorMessages as $key=>$values)
			{
			?><li><?=$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<?php }
		
		if($kUser->iAlreadyMapperUserError){
		?>
		<div id="AlreadyMapperUserError">
		<p><strong><?=t($t_base.'messages/already_exist_user_error_message_header');?></strong></p>
		<p><?=t($t_base.'messages/already_exist_user_error_message');?></p>
		</div>
		<?php }
		
		if($successflag){
		?>
		<h5><?=t($t_base.'messages/add_linked_user');?></h5>
		<p class="f-size-22"><?=t($t_base.'messages/do_you_wish_to_link');?> <?=ucwords($userDetailArr[0]['szFirstName'])?> <?=ucwords($userDetailArr[0]['szLastName'])?> <?=t($t_base.'messages/invite_user_confirmation');?></p>
		<?php }?>
		<? if($kUser->szEmailNotExists){
		?>
		<h5><strong><?=t($t_base.'messages/user_is_not_registered');?></strong></h5>
		<p class="f-size-22"><?=t($t_base.'messages/user_not_register_msg');?></p>
		<? }?>
		<br />
	<p align="center">
	<?php
	if($successflag){
		?>
		<a href="javascript:void(0)" class="button1" onclick="confirm_mapped_user();"><span><?=t($t_base.'fields/confirm');?></span></a>
		<?php }?>
	<? if($kUser->szEmailNotExists){?>
	<a href="javascript:void(0)" class="button1" id="send_non_register_user" onclick="send_email_non_registered_user('<?=$szEmail?>');"><span><?=t($t_base.'fields/send');?></span></a>
	<? } ?> 	
		<a href="javascript:void(0)" class="button1" id="cancel_button" onclick="cancel_remove_user_popup('mapped_user_id','hsbody-2');"><span><?=t($t_base.'fields/cancel');?></span></a>
		<input type="hidden" name="mappedArr[idUser]" id="idUser" value="<?=$kUser->id?>">
		<input type="hidden" name="mappedArr[mappedId]" id="mappedId" value="<?=$userDetailArr[0]['id']?>">
	</p>
	</form>
</div>
</div>
</div>
<?php	
}
 
if($flag==1)
{
	echo "SUCCESS||||";
	$kUser->getUserDetails($_SESSION['user_id']);
	if((int)$kUser->id>0)
	{
		$userMutliArr=$kUser->getMultiUserData($kUser->idGroup);
		$pendingUserAry = $kUser->getPendingInvitedUser($kUser->id,$kUser->idGroup);
	}
	?>
<form name="removeUser" id="removeUser" method="post">
<p><?=t($t_base.'title/user_linked_your_account');?></p>
	<br />
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/user_current_linkd');?></p>
		<p class="fl-40">
			<select multiple="multiple" name="userIdArr[]" id="userIdArr" onclick="get_remove_user_id();">
				<?php
				   if(!empty($userMutliArr))
				   { 
				       foreach($userMutliArr as $userMutliArrs)
				   	   {
				   	   		if($userMutliArrs['id']!=$_SESSION['user_id']){
				?>
				<option value="<?=$userMutliArrs['id']?>"><?=$userMutliArrs['szFirstName']?> <?=$userMutliArrs['szLastName']?> <?php if($userMutliArrs['iInvited']!='1') { echo "(".t($t_base.'messages/not_confirmed').")";  }?></option>
				<?php }}}?>
				
				<?php
				   if(!empty($pendingUserAry))
				   { 
				       foreach($pendingUserAry as $pendingUserArys)
				   	   {
				   	   		if($pendingUserArys['id']!=$_SESSION['user_id']){
				?>
						<option value="<?=$pendingUserArys['id']?>"><?=utf8_encode($pendingUserArys['szFirstName'])?> <?=utf8_encode($pendingUserArys['szLastName'])." ";?>(Pending)</option>
				<?php }}}?>
				
			</select>
		</p>
		<p class="fl-20" align="right"><a href="javascript:void(0)" class="button1" style="opacity:0.4;margin-top: 121px;" onclick="" id="remove"><span><?=t($t_base.'fields/remove');?></span></a></p>
	</div>
	</form>
	<?php
	echo "||||";
}
?>
<form name="mappedUser" id="mappedUser" method="post">
<p class="f-size-22"><?=t($t_base.'title/email_request_confirmation_send');?></p>
<br />
<div class="oh">
	<p class="fl-40"><?=t($t_base.'fields/email_address');?></p>
	<p class="fl-40"><input style="width: 280px;" type="text" size="38" name="mappedUser[szEmail]" id="szEmail" value="<?=$_REQUEST['mappedUser']['szEmail']?>"/></p>
	<p class="fl-20" align="right"><a href="javascript:void(0);" class="button1" onclick="check_email_address();"><span><?=t($t_base.'fields/add');?></span></a></p>
</div>
</form>