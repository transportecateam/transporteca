<?php
ob_start();
session_start();

$szMetaTitle= __DELETE_ACCOUNT_META_TITLE__;
$szMetaKeywords = __DELETE_ACCOUNT_META_KEYWORDS__;
$szMetaDescription = __DELETE_ACCOUNT_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Users/DeleteAccount/";
checkAuth();
?>
<div id="my-account-body">
<?php require_once( __APP_PATH_LAYOUT__ . "/nav.php" ); ?> 
<div class="hsbody-2-right">
        <div id="delete_user_account">
                <?php
                    require_once( __APP_PATH__ . "/ajax_deleteUserAccount.php" ); 
                ?>
        </div>
</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>