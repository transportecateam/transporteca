<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$iNewLayoutDesign = 1;

$szURL = $_SERVER['REQUEST_URI'] ;
$kExplain= new cExplain();
$kWHSSearch=new cWHSSearch();
$szLanguage = sanitize_all_html_input($_REQUEST['lang']);
$iLanguage = getLanguageId($szLanguage);

$landingPageDataArys = array();
$landingPageDataAry = array();
if(!empty($szURL))
{
    $szURL = str_replace("/","",urldecode($szURL)); 
    if(!empty($szURL))
    {
        $landingPageDataArys = $kExplain->getAllNewLandingPageData(false,$szURL);
        $landingPageDataAry = $landingPageDataArys[0];
    }
}

if(empty($landingPageDataAry))
{
    //fetching default Landing page.
    $landingPageDataArys = $kExplain->getAllNewLandingPageData(false,false,true,$iLanguage);
    $landingPageDataAry = $landingPageDataArys[0];
} 
$idLandingPage = $landingPageDataAry['id'] ; 
$_SESSION['Landing_Page_ID']=$idLandingPage;
$customerTestimonialAry = array();
$customerTestimonialAry = $kExplain->getAllTestimonials($idLandingPage);

$partnerLogoAry = array();
$partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);

if(!empty($landingPageDataAry['szMetaTitle']))
{
	$szMetaTitle = $landingPageDataAry['szMetaTitle'] ;
}
else 
{
	$szMetaTitle = __LANDING_PAGE_META_TITLE__ ;
}

if(!empty($landingPageDataAry['szMetaKeywords']))
{
	$szMetaKeywords = $landingPageDataAry['szMetaKeywords'] ;
}
else
{
	$szMetaKeywords = __LANDING_PAGE_META_KEYWORDS__;
}

if(!empty($landingPageDataAry['szMetaDescription']))
{
	$szMetaDescription = $landingPageDataAry['szMetaDescription'];
}
else
{
	$szMetaDescription = __LANDING_PAGE_META_DESCRIPTION__;
}
$szDefaultFromField = $landingPageDataAry['szTextB2'] ;

require_once(__APP_PATH_LAYOUT__."/header_new.php");
  
$t_base = "home/homepage/"; 
$kConfig=new cConfig();
$number = 1;
$kBooking = new cBooking();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum); 
if($_REQUEST['use_session']!=1)
{
    if($idBooking>0)
    {
        $redirect_url = __SELECT_SERVICES_URL__ ;
        ?>
        <div id="change_price_div">
        <?php
        display_booking_notification($redirect_url);
        echo "</div>";
    }
}

unset($_SESSION['valid_confirmation']);
$_SESSION['__BOOKING_TIMESTAMP__'] = '';
unset($_SESSION['__BOOKING_TIMESTAMP__']);


/**
* $_REQUEST['err']=1 means no record found on service page
* $_REQUEST['err']=2 means force user to login for confirming multiuser access ;
* $_REQUEST['err']=1 means no record found for this user either lcl pricing is deleted or user email is in selected forwarders non-acceptance list
**/
if($_REQUEST['err']==1)
{
    $kBooking->addExpactedServiceData($idBooking);
    //$t_base = "SelectService/";
    $landing_page_url = __LANDING_PAGE_URL__ ;
    $idExpactedService = $kBooking->idExpactedService ;
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    ?>
    <script type="text/javascript">
            addPopupScrollClass('service_not_found');
    </script>
    <div id="service_not_found" style="display:block;">	
    <?
    //echo display_service_not_found_html($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking);
    echo display_service_not_found_seach_service_popup($postSearchAry,$idExpactedService,$kBooking);

    echo "</div>";
    ?>
    <script type="text/javascript">
            addPopupScrollClass('all_available_service');
    </script>
    <?php
}
else if(($_REQUEST['err']==2) && !empty($_SESSION['multi_user_key']))
{
	$redirect_url =__BASE_URL__."/inviteConfirmation.php?confirmationKey=".trim($_SESSION['multi_user_key']);
	//echo $_SESSION['multi_user_key'] ;
	//die;
	?>
	<script type="text/javascript">
		ajaxLogin('<?=$redirect_url?>')
	</script>
	<?php
}
else if(($_REQUEST['err']==3))
{
	$t_base_mybooking = "Booking/MyBooking/";
	?>
	<div id="change_price_div">
	<?php
	echo display_rate_expire_popup($t_base_mybooking,$idBooking);
	echo '</div>';
}
else if(($_REQUEST['err']==4))
{
    $t_base_mybooking = "Booking/MyBooking/";
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 

    ?>
    <script type="text/javascript">
        display_price_changed_popup_cargo_changed('<?=$_SESSION['booking_page_url']?>','<?=$szBookingRandomNum?>');
    </script>	
    <?php
    /**
    * we are updating idForwarder,idWarehouseFrom and idWarehouseTo to prevent Multiple tab problem.
    */
    $res_ary=array();
    $res_ary['iDoNotKnowCargo']= '0';

    if(!empty($res_ary))
    {
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }
    $update_query = rtrim($update_query,",");
    if($kBooking->updateDraftBooking($update_query,$idBooking))
    {
            //echo "SUCCESS";
    }
} 
// geting all service type 
$serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');

$postSearchAry = array();
$cargoAry = array();   

if((int)$_REQUEST['idBooking']>0)
{
    //This block of code is called in repeat booking  
    $idBooking = (int)$_REQUEST['idBooking'] ;
    //echo $idBooking ;

    if($_SESSION['user_id']>0)
    {
        if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
        {
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $postSearchAry['szBookingRandomNum'] = '';
            $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
            $booking_mode = 2;
        }
        else
        {
            header("Location:".__LANDING_PAGE_URL__);
            die;
        }
    }
    else
    {
        header("Location:".__LANDING_PAGE_URL__);
        die;
    }
}
else if($idBooking>0)
{
    $kBooking = new cBooking();
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);

    if(($postSearchAry['iPaymentProcess']==1) && ($booking_mode==2))
    {
        header("Location:".__LANDING_PAGE_URL__);
        exit;
    }
    else if(($_SESSION['user_id']>0) && (!$kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id'])))
    {
        echo display_booking_invalid_user_popup($t_base_details);
        die;
    }
    else if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
        $t_base = "home/homepage/";
        echo display_booking_already_paid($t_base,$idBooking);
        die;
    }
    else if($_REQUEST['err'] !=4) // That means user donot come from cargo changed price popup
    {
        /**
        * we are updating idForwarder,idWarehouseFrom and idWarehouseTo to prevent Multiple tab problem.
        **/

        $res_ary=array();
        $res_ary['idForwarder']= '';
        $res_ary['idWarehouseFrom']= '';
        $res_ary['idWarehouseTo']= '';

        if(!empty($res_ary))
        {
                foreach($res_ary as $key=>$value)
                {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
        }
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {
                //echo "SUCCESS";
        }
    }	
}
else
{
    $_SESSION['booking_register_shipper_id']='';
    $_SESSION['booking_register_shipper_id'] = '';
    unset($_SESSION['booking_register_shipper_id']);
    unset($_SESSION['booking_register_shipper_id']);
}

if(((int)$_SESSION['user_id']>0) && ($_REQUEST['err']<=0))
{
    checkUserPrefferedCurrencyActive();
}

if(!empty($serviceTypeAry))
{
    $preload_image_str = '';
    foreach($serviceTypeAry as $serviceTypeArys)
    {
        $idServiceType = $serviceTypeArys['id'] ;
        $alt_text = $serviceTypeArys['szDescription'];

        if(!empty($preload_image_str))
        {
                $preload_image_str .=", '".display_service_type_image_search_service($postSearchAry,$idServiceType,$alt_text,false,true)."'";
        }
        else
        {
                $preload_image_str .="'".display_service_type_image_search_service($postSearchAry,$idServiceType,$alt_text,false,true)."'";
        }

        if(__SERVICE_TYPE_PTD__==$serviceTypeArys['id'])
        {
                $servicTypeIdDefault=__SERVICE_TYPE_PTD__;
                $servicTypeIdDefaultDesc=$serviceTypeArys['szDescription'];
        }
    }
}
 
$compareButtonDataAry = array();
$compareButtonDataAry = $kBooking->getCompareButtonDetails();
$iAvailableLclService = $kBooking->getAvailableLclService(); 
 
//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{ 
//    $iTotalUniqueIP = number_format((float)$compareButtonDataAry['iTotalUniqueIP'],0,'.','.');
//    $iTotalNumRows = number_format((float)$compareButtonDataAry['iTotalNumRows'],0,'.','.'); 
//    $iAvailableLclService = number_format((float)$iAvailableLclService,0,'.','.');  
//}
//else
//{
//    $iTotalUniqueIP = number_format((float)$compareButtonDataAry['iTotalUniqueIP'],0,'.',',');
//    $iTotalNumRows = number_format((float)$compareButtonDataAry['iTotalNumRows'],0,'.',','); 
//    $iAvailableLclService = number_format((float)$iAvailableLclService,0,'.',','); 
//}

$iTotalUniqueIP = number_format_custom((float)$compareButtonDataAry['iTotalUniqueIP'],$iLanguage,0);
$iTotalNumRows = number_format_custom((float)$compareButtonDataAry['iTotalNumRows'],$iLanguage,0); 
$iAvailableLclService = number_format_custom((float)$iAvailableLclService,$iLanguage,0);
  
?> 
<script type="text/javascript">
jQuery().ready(function(){	
    <?php /*if(!empty($preload_image_str)) { ?>
		preload([
		    <?=trim($preload_image_str)?>
		]);
	<?php } */?>
	$('#video').click(function() {
		jQuery.fancybox(
	    { 
		    'href' : "#video_container_div"
	    });
	    console.log("clicked");
	});	
	$('.sl-slider').bxSlider();
	setTimeout(function(){ $(".sl-slider-div").css('visibility','visible'); },'500'); 
}); 
</script> 

<div id="Transportation_pop" style="display:none;"></div>
<section id="search-freight" data-type="background" data-speed="10" class="pages" style="background-image:url('<?php echo $landingPageDataAry['szTextA1']; ?>');">    
	<article>
		<h1><?php echo $landingPageDataAry['szTextB']; ?></h1>
		<div id="transporteca_search_form_container">
			<?php 
                            echo display_new_search_form($kBooking,$postSearchAry,false,false,false,$szDefaultFromField,false,true);
			?>
		</div>
		<?php if(!empty($landingPageDataAry['szTextB3'])){ ?>
			<div class="search-description-box">
				<?php echo $landingPageDataAry['szTextB3']; ?>
			</div>
		<?php } ?>
	</article>
</section>   
<section class="what-is-in pages">
	<h2><?php echo $landingPageDataAry['szTextC']; ?></h2>
	<ul class="clearfix">
		<li class="quick">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD1']; ?>');">&nbsp;</div>
			<h3><?php echo $landingPageDataAry['szTextE1']; ?></h3>
			<p><?php echo $landingPageDataAry['szTextE2']; ?></p>
		</li>
		<li class="all-price">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD4']; ?>');">&nbsp;</div>
			<h3><?php echo $landingPageDataAry['szTextE3']; ?></h3>
			<p><?php echo $landingPageDataAry['szTextE4']; ?></p>
		</li>
		<li class="on-time">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD7']; ?>');">&nbsp;</div>
			<h3><?php echo $landingPageDataAry['szTextE5']; ?></h3>
			<p><?php echo $landingPageDataAry['szTextE6']; ?></p>
		</li>
	</ul>
</section>
<section class="how-does-work pages">
	<h2><?php echo $landingPageDataAry['szTextF1']; ?></h2>
	<ul class="clearfix">
		<li class="quick">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG1']; ?>')no-repeat 0 0;">&nbsp;</div>
			<p><?php echo $landingPageDataAry['szTextH1']; ?></p>
		</li>
		<li class="all-price">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG4']; ?>')no-repeat 0 0;">&nbsp;</div>
			<p><?php echo $landingPageDataAry['szTextH2']; ?></p>
		</li>
		<li class="on-time">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG7']; ?>')no-repeat 0 0;">&nbsp;</div>
			<p><?php echo $landingPageDataAry['szTextH3']; ?></p>
		</li>
	</ul>
</section>
<section id="partners" data-type="background" data-speed="15" class="pages" style='background-image:url("<?php echo $landingPageDataAry['szTextI1']; ?>");'>
	<article>
		<h2><?php echo $landingPageDataAry['szTextI4']; ?></h2>
		<div class="partners-number clearfix">
			<div class="professionals"><span><?php echo $iTotalUniqueIP; ?></span><br><?php echo $landingPageDataAry['szTextI5']; ?></div>
			<div class="companies"><span><?php echo $iTotalNumRows;?></span><br><?php echo $landingPageDataAry['szTextI6']; ?></div>
			<div class="routes"><span><?php echo $iAvailableLclService ; ?></span><br><?php echo $landingPageDataAry['szTextI7']; ?></div>
		</div>
		<ul class="clearfix">
		<?php
			if(!empty($partnerLogoAry))
			{ 
				$i=1;
				foreach($partnerLogoAry as $partnerLogoArys)
				{
						$idImage="img_".$i;
					?>
					<li id="<?=$idImage?>">
						 <img class="normal" src="<?php echo $partnerLogoArys['szLogoImageUrl']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
   						 <img class="hover" src="<?php echo $partnerLogoArys['szWhiteLogoImageURL']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
						 <div><div><?php echo $partnerLogoArys['szToolTipText']; ?></div></div>
					</li>
					<?php
					++$i;
				}
			}
		?>
		</ul>
	</article>
</section> 
 
<section id="testimonials" class="pages">
	<article>
		<h2><?php echo $landingPageDataAry['szTextJ1']; ?></h2>
		<div class="testimonials-container" id="testimonials-container"> 
			<ul class="sl-slider">	
				<?php 
					if(!empty($customerTestimonialAry)) 
					{
						foreach($customerTestimonialAry as $customerTestimonialArys)
						{
							?>
							<li>
								<div class="sl-slider-div" style="visibility:hidden;">
									<div class="deco"><img src="<?php echo $customerTestimonialArys['szImageURL']; ?>" alt="<?php echo $customerTestimonialArys['szImageTitle']?>" title="<?php echo $customerTestimonialArys['szImageDesc']; ?>"></div>
									<h3><?php echo $customerTestimonialArys['szHeading']; ?></h3>
									<blockquote><p><?php echo nl2br($customerTestimonialArys['szTextDesc']); ?></p><cite><?php echo $customerTestimonialArys['szTitleName']; ?></cite></blockquote>
								</div>
							</li>
							<?php
						}
					}
				?>
			</ul>  
		</div>
	</article>
</section>  

<section id="video" class="pages" style="cursor:pointer;background-image:url('<?php echo $landingPageDataAry['szTextL1']; ?>');">
	<article>
		
	</article>
</section>  
<div style="display:none;">
	<div id="video_container_div" style="width:1020px;height:580px;overflow:hidden;">	
		<?php echo $landingPageDataAry['szTextL4']?>
	</div>
</div>
<div id="all_available_service" style="display:none;"></div>
<?php
echo html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>