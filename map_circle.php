<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code));
constantApiKey();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>		
		<script src="http://maps.google.com/maps?file=api&amp;v=3.x&amp;key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true" type="text/javascript"></script>
		<script type="text/javascript">
/* Developed by: Ajay jha */
//Global variables
var map;
var bounds = new GLatLngBounds; //Circle Bounds
var map_center = new GLatLng(28.535516, 77.391026);

var Circle; //Circle object
var CirclePoints = []; //Circle drawing points
var CircleCenterMarker, CircleResizeMarker;
var circle_moving = false; //To track Circle moving
var circle_resizing = false; //To track Circle resizing
var radius = 1; //1 km
var min_radius = 0; //0.5km
var max_radius = 50000; //5km

//Circle Marker/Node icons
var redpin = new GIcon(); //Red Pushpin Icon
//redpin.image = "http://maps.google.com/mapfiles/ms/icons/red-pushpin.png";
redpin.image = '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/resize.png'?>';
redpin.iconSize = new GSize(15,16);
redpin.iconAnchor = new GPoint(10, 32);
var bluepin = new GIcon(); //Blue Pushpin Icon
//bluepin.image = "http://maps.google.com/mapfiles/ms/icons/blue-pushpin.png";
bluepin.image = '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>';
bluepin.iconSize = new GSize(22, 33);
bluepin.iconAnchor = new GPoint(10, 32);
geocoder = new GClientGeocoder();

function initialize() 
{
	 //Initialize Google Map
    if (GBrowserIsCompatible()) 
    {
        map = new GMap2(document.getElementById("map_canvas")); //New GMap object
        map.setCenter(map_center);

        var ui = new GMapUIOptions(); //Map UI options
        ui.maptypes = { normal:true, satellite:true, hybrid:true, physical:false }
        ui.zoom = {scrollwheel:true, doubleclick:true};
        ui.controls = { largemapcontrol3d:true, maptypecontrol:true, scalecontrol:true };
        map.setUI(ui); //Set Map UI options
		
		var map_center2 = new GLatLng(27.535516, 76.391026);
		var map_center1 = new GLatLng(28.535516, 77.391026);
		
        addCircleCenterMarker(map_center1);
        addCircleResizeMarker(map_center1);
        drawCircle(map_center1, radius);
        
        addCircleCenterMarker(map_center2);
        addCircleResizeMarker(map_center2);
        drawCircle(map_center2, radius);
    }
}

// Adds Circle Center marker
function addCircleCenterMarker(point) 
{
    var markerOptions = { icon: bluepin, draggable: true };
    CircleCenterMarker = new GMarker(point, markerOptions);
    map.addOverlay(CircleCenterMarker); //Add marker on the map
    GEvent.addListener(CircleCenterMarker, 'dragstart', function() { //Add drag start event
        circle_moving = true;
    });
    GEvent.addListener(CircleCenterMarker, 'drag', function(point) { //Add drag event
        drawCircle(point, radius);
    });
    GEvent.addListener(CircleCenterMarker, 'dragend', function(point) { //Add drag end event
        circle_moving = false;
        drawCircle(point, radius);
    });
}

// Adds Circle Resize marker
function addCircleResizeMarker(point) 
{
    var resize_icon = new GIcon(redpin);
    resize_icon.maxHeight = 0;
    var markerOptions = { icon: resize_icon, draggable: true };
    CircleResizeMarker = new GMarker(point, markerOptions);
    map.addOverlay(CircleResizeMarker); //Add marker on the map
    GEvent.addListener(CircleResizeMarker, 'dragstart', function() { //Add drag start event
        circle_resizing = true;
    });
    GEvent.addListener(CircleResizeMarker, 'drag', function(point) { //Add drag event
        var new_point = new GLatLng(map_center.lat(), point.lng()); //to keep resize marker on horizontal line
        var new_radius = new_point.distanceFrom(map_center) / 1000; //calculate new radius
        if (new_radius < min_radius) new_radius = min_radius;
        if (new_radius > max_radius) new_radius = max_radius;
        drawCircle(map_center, new_radius);
    });
    GEvent.addListener(CircleResizeMarker, 'dragend', function(point) { //Add drag end event
        circle_resizing = false;
        var new_point = new GLatLng(map_center.lat(), point.lng()); //to keep resize marker on horizontal line
        var new_radius = new_point.distanceFrom(map_center) / 1000; //calculate new radius
        if (new_radius < min_radius) new_radius = min_radius;
        if (new_radius > max_radius) new_radius = max_radius;
        drawCircle(map_center, new_radius);
    });
}

//Draw Circle with given radius and center
function drawCircle(center, new_radius) 
{
    //Circle Drawing Algorithm from: http://koti.mbnet.fi/ojalesa/googlepages/circle.htm

    //Number of nodes to form the circle
 
    var nodes = new_radius * 40;
    if(new_radius < 1) nodes = 40;

    //calculating km/degree
    var latConv = center.distanceFrom(new GLatLng(center.lat() + 0.1, center.lng())) / 100;
    var lngConv = center.distanceFrom(new GLatLng(center.lat(), center.lng() + 0.1)) / 100;
	document.getElementById("radious").value = new_radius.toFixed(2);
	
    CirclePoints = [];
    var step = parseInt(360 / nodes) || 10;
    var counter = 0;
    for (var i = 0; i <= 360; i += step) 
    {
        var cLat = center.lat() + (new_radius / latConv * Math.cos(i * Math.PI / 180));
        var cLng = center.lng() + (new_radius / lngConv * Math.sin(i * Math.PI / 180));
        var point = new GLatLng(cLat, cLng);
        CirclePoints.push(point);
        counter++;
    }
    CircleResizeMarker.setLatLng(CirclePoints[Math.floor(counter / 4)]); //place circle resize marker
    CirclePoints.push(CirclePoints[0]); //close the circle polygon
    if (Circle) { map.removeOverlay(Circle); } //Remove existing Circle from Map
    var fillColor = (circle_resizing || circle_moving) ? "#00FF00" : "#00FF00"; //Set Circle Fill Color
    Circle = new GPolygon(CirclePoints, '#FFFFFF', 2, 1, fillColor, 0.2); //New GPolygon object for Circle
    map.addOverlay(Circle); //Add Circle Overlay on the Map
    radius = new_radius; //Set global radius
    map_center = center; //Set global map_center
    if (!circle_resizing && !circle_moving) { //Fit the circle if it is nor moving or resizing
        fitCircle();
        //Circle drawing complete trigger function goes here
    }
}
function showAddress() 
{
	var address = document.getElementById("address").value;
	var radius_val = document.getElementById("radious").value;

	if(address=='')
	{
		alert("Please enter city name");
		document.getElementById("address").focus();
		return false;
	}
	if(isNaN(radius_val))
	{
		alert("Please enter radius");
		document.getElementById("radious").focus();
		return false;
	}
	var radius = parseFloat(radius_val);
	 if (geocoder) 
	 {
	 	geocoder.getLatLng(
		address,
	 	function(point) {
	 	if (!point) 
	 	{
	 		alert(address + " not found");
	 	} 
	 	else 
	 	{ 
	 		posset = 1;
	 		 map.clearOverlays();
	 		//map.setMapType(G_HYBRID_MAP);
			map.setCenter(point,8);
	 		zm = 1;
	 		addCircleCenterMarker(point);
	 		addCircleResizeMarker(point);
	 		drawCircle(point, radius);
	 	}
	   });
	 }
}
function computepos (point)
{
var latA = Math.abs(Math.round(value=point.y * 1000000.));
var lonA = Math.abs(Math.round(value=point.x * 1000000.));

if(value=point.y < 0)
{
	var ls = '-' + Math.floor((latA / 1000000));
}
else
{
	var ls = Math.floor((latA / 1000000));
}

var lm = Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60);
var ld = ( Math.floor(((((latA/1000000) - Math.floor(latA/1000000)) * 60) - Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60)) * 100000) *60/100000 );

if(value=point.x < 0)
{
  var lgs = '-' + Math.floor((lonA / 1000000));
}
else
{
	var lgs = Math.floor((lonA / 1000000));
}

var lgm = Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60);
var lgd = ( Math.floor(((((lonA/1000000) - Math.floor(lonA/1000000)) * 60) - Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60)) * 100000) *60/100000 );
alert(point.y.toFixed(6));
alert(point.x.toFixed(6));
document.getElementById("latbox").value=point.y.toFixed(6);
document.getElementById("lonbox").value=point.x.toFixed(6);


}

function show_address_on_submit(kEvent)
{
	var temp=(window.event)?kEvent.keyCode:kEvent.which;
	if(temp == 13)
	{
		showAddress();
	}
}

//Fits the Map to Circle bounds
function fitCircle() {
    bounds = Circle.getBounds();
    map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
}
</script> 
</head>
<body onload="initialize()" onunload="GUnload()">
<div id="map_canvas" style="width:100%; height:450px"></div>
<br/>
<br/>
 <form action="javascript:void();" methos="post" >    
      <div class="oh">
      <p class="fl-45" align="left" style="padding-top: 5px;">City Name <input type="text" onkeyup="show_address_on_submit(event)" size="30" style="width:200px;" id="address" name="address" value="<?=$address_line?>" />
      <br><br>Radious (Km) <input type="text" onkeyup="show_address_on_submit(event)" size="30" style="width:200px;" id="radious" name="radious" value="<?=$radious?>" /></p>
      <p class="fl-15" align="right">
        <a href="javascript:void(0)" align="right" onclick="showAddress()" class="button1"><span style="min-width:60px;">search</span></a>
      </p>
      </div>
        <div id="map" align="center" style="width: 700px; height: 300px"></div>
     </form> 
</body> 
</html>