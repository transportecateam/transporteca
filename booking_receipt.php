<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
///$display_abandon_message = true;

$szMetaTitle = __BOOKING_RECEIPT_META_TITLE__ ;
$szMetaKeywords = __BOOKING_RECEIPT_META_KEYWORDS__;
$szMetaDescription = __BOOKING_RECEIPT_META_DESCRIPTION__;

require_once(__APP_PATH_LAYOUT__."/header.php");
$t_base = "BookingReceipt/";
$kBooking = new cBooking(); 

if((int)$_SESSION['booking_id']<=0)
{
	$redirect_url = __HOME_PAGE_URL__ ;
	header("Location:".$redirect_url);
	die;
}
$postSearchAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']);
$cargoAry = $kBooking->getCargoDeailsByBookingId($_SESSION['booking_id']);
if(($postSearchAry['idShipperConsignee']<=0) || ($postSearchAry['idForwarder']<=0) || ($postSearchAry['idWarehouseTo']<=0) || ($postSearchAry['idWarehouseFrom']<=0))
{
	$redirect_url = __HOME_PAGE_URL__ ;
	header("Location:".$redirect_url);
	die;
	/*
	?>
	<div id="change_price_div">
	<?
	display_booking_notification($redirect_url);
	echo "</div>";
*/
} 
$kForwarder = new cForwarder();
$customerServiceEmailAry = array();
$idForwarder = $postSearchAry['idForwarder'] ;
$customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
//getAllForwarderContactsEmail
$szWareHouseFromStr = '';
$szWareHouseMapStr = '';

if(!empty($postSearchAry['szWarehouseFromName']))
{
	$szWareHouseFromStr.=$postSearchAry['szWarehouseFromName']." <br> ";
	$szWareHouseMapStr.=$postSearchAry['szWarehouseFromName']." <br>";
}
if(!empty($postSearchAry['szWarehouseFromAddress']))
{
	$szWareHouseFromStr.=$postSearchAry['szWarehouseFromAddress'];
	$szWareHouseMapStr .= $postSearchAry['szWarehouseFromAddress'];
}
if(!empty($postSearchAry['szWarehouseFromAddress2']))
{
	$szWareHouseFromStr .=", ".$postSearchAry['szWarehouseFromAddress2'];
	$szWareHouseMapStr .= ", ".$postSearchAry['szWarehouseFromAddress2'];
}
if(!empty($postSearchAry['szWarehouseFromAddress3']))
{
	$szWareHouseFromStr .=", ".$postSearchAry['szWarehouseFromAddress3'];
	$szWareHouseMapStr .= ", ".$postSearchAry['szWarehouseFromAddress3'];
}

if(!empty($postSearchAry['szWarehouseFromPostCode']) || !empty($postSearchAry['szWarehouseFromCity']))
{
	if(!empty($postSearchAry['szWarehouseFromPostCode']))
	{
		$szWareHouseFromStr.="<br> ".$postSearchAry['szWarehouseFromPostCode'];
		$szWareHouseMapStr .="<br> ".$postSearchAry['szWarehouseFromPostCode'];
	}
	
	if(!empty($postSearchAry['szWarehouseFromPostCode']) && !empty($postSearchAry['szWarehouseFromCity']))
	{
		$szWareHouseFromStr.=" ".$postSearchAry['szWarehouseFromCity']."<br> ";
		$szWareHouseMapStr .=" ".$postSearchAry['szWarehouseFromCity']."<br> ";
	}
	else if(!empty($postSearchAry['szWarehouseFromCity']))
	{
		$szWareHouseFromStr.= "<br> ".$postSearchAry['szWarehouseFromCity']."<br> ";
		$szWareHouseMapStr .= "<br> ".$postSearchAry['szWarehouseFromCity']."<br> ";
	}
	else if(!empty($postSearchAry['szWarehouseFromPostCode']))
	{
		$szWareHouseMapStr .= "<br /> ";
		$szWareHouseFromStr .= "<br /> ";
	}
}						
if(!empty($postSearchAry['szWarehouseFromCountry']) || !empty($postSearchAry['szWarehouseFromState']))
{
	if(!empty($postSearchAry['szWarehouseFromState']))
	{
		$szWareHouseFromStr.=$postSearchAry['szWarehouseFromState'];
		$szWareHouseMapStr .= $postSearchAry['szWarehouseFromState'];
	}
	
	if(!empty($postSearchAry['szWarehouseFromState']) && !empty($postSearchAry['szWarehouseFromCountry']))
	{
		$szWareHouseFromStr.=" ".$postSearchAry['szWarehouseFromCountry']."<br> ";
		$szWareHouseMapStr .=" ".$postSearchAry['szWarehouseFromCountry']."<br> ";
	}
	else if(!empty($postSearchAry['szWarehouseToCountry']))
	{
		$szWareHouseFromStr.= $postSearchAry['szWarehouseFromCountry']."<br> ";
		$szWareHouseMapStr .= $postSearchAry['szWarehouseFromCountry']."<br> ";
	}
}

$szWareHouseToStr = '';
$szWareHouseToMapStr = "";
if(!empty($postSearchAry['dtAvailable']))
{
	//$szWareHouseToStr.=date('d M Y H:i',strtotime($postSearchAry['dtAvailable']))." <br> ";
}
if(!empty($postSearchAry['szWarehouseToName']))
{
	$szWareHouseToStr.=$postSearchAry['szWarehouseToName']." <br> ";
	$szWareHouseToMapStr .=$postSearchAry['szWarehouseToName']." <br> ";
}
if(!empty($postSearchAry['szWarehouseToAddress']))
{
	$szWareHouseToStr.=$postSearchAry['szWarehouseToAddress'];
	$szWareHouseToMapStr .=$postSearchAry['szWarehouseToAddress'];
}
if(!empty($postSearchAry['szWarehouseToAddress2']))
{
	$szWareHouseToStr.=", ".$postSearchAry['szWarehouseToAddress2'];
	$szWareHouseToMapStr .=", ".$postSearchAry['szWarehouseToAddress2'];
}
if(!empty($postSearchAry['szWarehouseToAddress3']))
{
	$szWareHouseToStr.=", ".$postSearchAry['szWarehouseToAddress3'];
	$szWareHouseToMapStr .=", ".$postSearchAry['szWarehouseToAddress3'];
}
if(!empty($postSearchAry['szWarehouseToPostCode']) || !empty($postSearchAry['szWarehouseToCity']))
{
	if(!empty($postSearchAry['szWarehouseToPostCode']))
	{
		$szWareHouseToStr.="<br> ".$postSearchAry['szWarehouseToPostCode'];
		$szWareHouseToMapStr .="<br> ".$postSearchAry['szWarehouseToPostCode'];
	}
	
	if(!empty($postSearchAry['szWarehouseToPostCode']) && !empty($postSearchAry['szWarehouseToCity']))
	{
		$szWareHouseToStr.=" ".$postSearchAry['szWarehouseToCity']."<br> ";
		$szWareHouseToMapStr .=" ".$postSearchAry['szWarehouseToCity']."<br> ";
	}
	else if(!empty($postSearchAry['szWarehouseToCity']))
	{
		$szWareHouseToStr.= "<br> ".$postSearchAry['szWarehouseToCity']."<br> ";
		$szWareHouseToMapStr .= "<br> ".$postSearchAry['szWarehouseToCity']."<br> ";
	}
	else if(!empty($postSearchAry['szWarehouseToPostCode'])){
		$szWareHouseToMapStr .= "<br /> ";
	}	
}						
if(!empty($postSearchAry['szWarehouseToCountry']) || !empty($postSearchAry['szWarehouseToState']))
{
	if(!empty($postSearchAry['szWarehouseToState']))
	{
		$szWareHouseToStr.=$postSearchAry['szWarehouseToState'];
		$szWareHouseToMapStr .= $postSearchAry['szWarehouseToState'];
	}
	
	if(!empty($postSearchAry['szWarehouseToState']) && !empty($postSearchAry['szWarehouseToCountry']))
	{
		$szWareHouseToStr.=" ".$postSearchAry['szWarehouseToCountry']."<br> ";
		$szWareHouseToMapStr .=" ".$postSearchAry['szWarehouseToCountry']."<br> ";
	}
	else if(!empty($postSearchAry['szWarehouseToCountry']))
	{
		$szWareHouseToStr.= $postSearchAry['szWarehouseToCountry']."<br> ";
		$szWareHouseToMapStr .= $postSearchAry['szWarehouseToCountry']."<br> ";
	}
}

$shipperAddress = '';
if(!empty($postSearchAry['szShipperCompanyName']))
{
	$shipperAddress.=$postSearchAry['szShipperCompanyName']." <br> ";
}
if(!empty($postSearchAry['szShipperFirstName']) || $postSearchAry['szShipperLastName'])
{
	//$shipperAddress.=$postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName']." <br> ";
}
if($postSearchAry['idServiceType']!=__SERVICE_TYPE_DTD__ && $postSearchAry['idServiceType']!=__SERVICE_TYPE_DTW__)
{
	if(!empty($postSearchAry['szShipperPhone']))
	{
		$shipperAddress.=$postSearchAry['szShipperPhone']."<br>";
	}
}
if(!empty($postSearchAry['szShipperAddress_pickup']))
{
	$shipperAddress.=$postSearchAry['szShipperAddress_pickup'];
}
if(!empty($postSearchAry['szShipperAddress2_pickup']))
{
	$shipperAddress.=", ".$postSearchAry['szShipperAddress2_pickup'];
}
if(!empty($postSearchAry['szShipperAddress3_pickup']))
{
	$shipperAddress.=", ".$postSearchAry['szShipperAddress3_pickup'];
}
if(!empty($postSearchAry['szShipperPostCode_pickup']) || !empty($postSearchAry['szShipperCity_pickup']))
{
	$shipperAddress.="<br>".$postSearchAry['szShipperPostCode_pickup']." ".$postSearchAry['szShipperCity_pickup']."<br>";
}
if(!empty($postSearchAry['szShipperCountry_pickup']))
{
	$shipperAddress.=$postSearchAry['szShipperCountry_pickup']." <br> ";
}
if($postSearchAry['szPaymentStatus']=='Completed')
{
?>
	
		<div class="hsbody-half" style="width:946px;padding:0 20px 70px;position:relative;">
			<div class="hsbody-left">
				<h5><?=t($t_base.'fields/thank_you_booking');?></h5>
				<p> <?=t($t_base.'fields/booking_ref_is');?>: <?=$postSearchAry['szBookingRef']?></p><br>
				<p><?=t($t_base.'fields/booking_conf_sent_to');?>:</p>
				<p><?=$postSearchAry['szEmail']?></p><br>
				<p><?=t($t_base.'fields/for_booking_related');?> <?=$postSearchAry['szForwarderDispName']?> <?=t($t_base.'fields/customer_service');?>: 
				<?php
					$customerServiceEmailLinkAry = format_fowarder_emails($customerServiceEmailAry) ;
					$customerServiceEmailLink = $customerServiceEmailLinkAry[0];
					$customerServiceEmailStr = $customerServiceEmailLinkAry[1];
				?>	
				<a style="font-size:16px;" href="mailto:<?=$customerServiceEmailLink?>?subject=Transporteca booking number : <?=$postSearchAry['szBookingRef']?>" ><?=$customerServiceEmailStr?></a></p>
							
				<br />
				<br />
				<br />
				<br />
				<br />
				<div style="position:absolute;bottom:20px;left:115px;">
				<p align="center"><a href="<?=__BASE_URL__?>/viewInvoice/<?=$postSearchAry['id']?>/" target="_blank" class="button1"><span style="width:220px;"><?=t($t_base.'fields/view_invoice');?></span></a></p>
				<p align="center"><a href="<?=__BASE_URL__?>/viewBookingConfirmation/<?=$postSearchAry['id']?>/" target="_blank" class="button1"><span style="width:220px;"><?=t($t_base.'fields/view_confirmation');?></span></a></p>
				</div>
			</div>
			<div class="hsbody-right">
				<h5><?=t($t_base.'fields/next_step');?></h5>
				<?php
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__) // DTD=1 , DTW=2
						{
							if(($postSearchAry['szFirstName']==$postSearchAry['szShipperFirstName']) && ($postSearchAry['szLastName']==$postSearchAry['szShipperLastName']))
							{
								$shipperName =  t($t_base.'fields/you').' ';
							}
							else
							{
								$shipperName = $postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName'] ;
							}
							$next_step_str = $postSearchAry['szForwarderDispName']." ".t($t_base.'fields/will_contact')." ".$shipperName." ".t($t_base.'fields/to_arrange_for_pick_up_at').": <br>".$shipperAddress."<br>" ;
							$next_step_str .= t($t_base.'fields/arrange_pickup')." ".date('d/m/Y H:i',strtotime($postSearchAry['dtCutOff']))." ".t($t_base.'fields/or_thereafter') ;
						}
						else
						{
							$next_step_str = t($t_base.'fields/ensure_cargo').": <br> ".nl2br($szWareHouseMapStr)."<br>".t($t_base.'fields/late_by_cut_off').": ".date('d/m/Y',strtotime($postSearchAry['dtCutOff'])) ;
						}
						echo $next_step_str ;
					?>
				<br />
				<br>
				<br>
				<?php
				$kConfig = new cConfig();
				// geting  service type 
				$serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__',$postSearchAry['iBookingLanguage']);
				$serviceTypeAry=$serviceTypeAry[$postSearchAry['idServiceType']];
				// geting  service type in english
				$serviceTypeEnglishAry = $kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__',__LANGUAGE_ID_ENGLISH__);
				$serviceTypeEnglishAry=$serviceTypeEnglishAry[$postSearchAry['idServiceType']];
				$count_cargo = count($cargoAry);
				$counter_1=0;
				for($i=1;$i<=$count_cargo;$i++)
				{
					if(!empty($cargoAry))
					{
						$fLength = ceil($cargoAry[$i]['fLength']);
						$fWidth = ceil($cargoAry[$i]['fWidth']);
						$fHeight = ceil($cargoAry[$i]['fHeight']);
						$fWeight = ceil($cargoAry[$i]['fWeight']);
						$iQuantity = ceil($cargoAry[$i]['iQuantity']);
						$szCommodity = $cargoAry[$i]['szCommodity'];
						$szCargoMeasure = $cargoAry[$i]['cmdes'];
						$szWeightMeasure =  $cargoAry[$i]['wmdes'];
						$szCommodity = $cargoAry[$i]['szCommodity'];
					}							
					$szCargoDetails_str .= $i.": ".$iQuantity."x".$fLength."x".$fWidth."x".$fHeight."".$szCargoMeasure.", ".number_format((float)$fWeight)." ".$szWeightMeasure.", ".$szCommodity."<br>";
					$counter_1++;
				}
				
				$services_string = $serviceTypeAry['szDescription'] ;
				$cc_string = '';
				if((int)$postSearchAry['iOriginCC']==1)
				{
					$cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/origin');
					$cc_string_english = "and customs clearance at origin";
				}								
				if((int)$postSearchAry['iDestinationCC']==2)
				{
					if(empty($cc_string))
					{
						$cc_string = t($t_base.'fields/and')." ".t($t_base.'fields/customs_clearance_at')." ".t($t_base.'fields/destination');
						$cc_string_english = "and customs clearance at destination";
					}
					else
					{
						$cc_string.= " ".t($t_base.'fields/and')." ".t($t_base.'fields/destination');
						$cc_string_english .= " and destination";
					}
				}
				
				$services_string = $serviceTypeEnglishAry['szDescription'] ;
				$services_string =display_service_type_description($postSearchAry,__LANGUAGE_ID_ENGLISH__,true);
				$send_shipper_email = true ;
				$send_consignee_email = true ;
					//if(!empty($postSearchAry['szShipperEmail']))
					//{
						$replace_ary = array();
						$replace_ary['szBookingRef'] = $postSearchAry['szBookingRef'];
						$replace_ary['szFirstName'] = $postSearchAry['szFirstName'] ;
						$replace_ary['szLastName'] = $postSearchAry['szLastName'] ;
						
						$replace_ary['szShipperFirstName'] = $postSearchAry['szShipperFirstName'] ;
						$replace_ary['szShipperLastName'] = $postSearchAry['szShipperLastName'] ;
						
						$replace_ary['szForwarderDispName'] = $postSearchAry['szForwarderDispName'] ;
						$replace_ary['szServiceType'] = $services_string." ".$cc_string_english ;
						$replace_ary['szShipperCompany'] = $postSearchAry['szShipperCompanyName'] ;
						$replace_ary['szConsigneeCompany'] = $postSearchAry['szConsigneeCompanyName'] ;
						$replace_ary['szCargoDetails'] = $szCargoDetails_str ;
						$replace_ary['szForwarderEmail'] = $customerServiceEmailStr;
						
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTP__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTP__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__ )  // WTW=4 , WTD = 3
						{					
							$szCutoffAvailableString = "Please ensure that the cargo is delivered to: <br> ".nl2br($szWareHouseMapStr)."<br>Latest by cut-off: ".date('d/m/Y H:i',strtotime($postSearchAry['dtCutOff'])) ;
							$replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString ;
						}
						else
						{
							$szCutoffAvailableString = $postSearchAry['szForwarderDispName']." will contact you directly to arrange exact details for pick-up. You should expect to have the shipment ready for pick-up by ".date('d/m/Y H:i',strtotime($postSearchAry['dtCutOff'])).".";
							$replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString ;
						}
						
						$emailAry = createEmailMessage('__SHIPPER_CONFIRMATION_EMAIL_TEXT__', $replace_ary);
						
						$mail_body =$emailAry['szEmailBody'];
						$shipper_subject = $emailAry['szEmailSubject'];
						$shipper_mail_text = $mail_body ;
						$shipper_mail = $postSearchAry['szShipperEmail'] ;
						$send_shipper_email = true ;
						//$shipper_mail_text = $postSearchAry['szShipperEmail']."||||".$subject."||||".$mail_body;
					//}					
					//if(!empty($postSearchAry['szConsigneeEmail']))
					//{
						$replace_ary = array();
						$szCutoffAvailableString = '';
						$replace_ary['szBookingRef'] = $postSearchAry['szBookingRef'];
						$replace_ary['szConsigneeFirstName'] = $postSearchAry['szConsigneeFirstName'] ;
						$replace_ary['szConsigneeLastName'] = $postSearchAry['szConsigneeLastName'] ;
						$replace_ary['szFirstName'] = $postSearchAry['szFirstName'] ;
						$replace_ary['szLastName'] = $postSearchAry['szLastName'] ;
						
						$replace_ary['szForwarderDispName'] = $postSearchAry['szForwarderDispName'] ;
						$replace_ary['szServiceType'] = $services_string." ".$cc_string_english ;
						$replace_ary['szShipperCompany'] = $postSearchAry['szShipperCompanyName'] ;
						$replace_ary['szConsigneeCompany'] = $postSearchAry['szConsigneeCompanyName'] ;
						$replace_ary['szCargoDetails'] = $szCargoDetails_str ;
						$replace_ary['szForwarderEmail'] = $customerServiceEmailStr ;
												
						if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTP__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTP__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__ )   // WTW=4 , DTW = 3
						{							
							$szCutoffAvailableString .= "You should arrange for pick-up at ".$postSearchAry['szForwarderDispName']." on following address : <br> ".$szWareHouseToStr ;
							$replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString."<br>The cargo will be available from ".date('d M Y H:i',strtotime($postSearchAry['dtAvailable'])).".";
						}
						else
						{
							$szCutoffAvailableString = $postSearchAry['szForwarderDispName']." will contact you directly to arrange exact details for delivery. You should expect the shipment to be delivered before ".date('d M Y H:i',strtotime($postSearchAry['dtAvailable'])).".";
							$replace_ary['szCutoffAvailableString'] = $szCutoffAvailableString ;
						}
						$emailAry = createEmailMessage('__CONSIGNEE_CONFIRMATION_EMAIL_TEXT__', $replace_ary);
						
						$consignee_mail_body =$emailAry['szEmailBody'];
						$consignee_email_subject = $emailAry['szEmailSubject'];
						$consignee_mail = $postSearchAry['szConsigneeEmail'] ;
						//$consignee_mail_text = "mailto:".$postSearchAry['szConsigneeEmail']."?subject=".__CONSIGNEE_EMAIL_SUBJECT__."&body=".$mail_body ;
						
						$send_consignee_email = true;
					//}
					//$send_consignee_email = true;
					//echo $shipper_mail_text."<br><br>" ;
					//$consignee_mail_text = "javascript:void(0);";
				?>
				<br><br><br>
				<div style="position:absolute;bottom:20px;right:120px;">
				<input type="hidden" name="szShipperMailMsg"  id="szShipperMailMsg" value="<?=$shipper_mail_text?>">
				<input type="hidden" name="szConsigneeMailMsg"  id="szConsigneeMailMsg" value="<?=$consignee_mail_body?>">
				<p align="center"><a href="javascript:void(0)" <? if($send_shipper_email) {?>  onclick = "javascript:send_shipper_consignee_mail('<?=$shipper_mail?>','<?=$shipper_subject?>','Shipper')" <? }?> class="button1"><span style="width:220px;"><?=t($t_base.'fields/send_instruction_to_shipper');?></span></a></p>
				<p align="center"><a href="javascript:void(0);" <? if($send_consignee_email) {?> onclick="javascript:send_shipper_consignee_mail('<?=$consignee_mail?>','<?=$consignee_email_subject?>','Consigee')" <? }?> class="button1"><span style="width:220px;"><?=t($t_base.'fields/send_details_to_consignee');?></span></a></p>
				</div>
			</div>
		</div>
		
	<!-- Google Code for purchase Conversion Page -->
	<script type="text/javascript">
		/* <![CDATA[ */
		vargoogle_conversion_id = 1001820024;
		vargoogle_conversion_language = "en";
		vargoogle_conversion_format = "3";
		vargoogle_conversion_color = "ffffff";
		vargoogle_conversion_label = "omJXCKCouQMQ-J7a3QM";
		vargoogle_conversion_value = 0;
		/* ]]> */
	</script>
	<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
	</script>
<noscript>

<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1001820024/?value=0&amp;label=omJXCKCouQMQ-J7a3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
		
		<?php }
		else if($postSearchAry['szPaymentStatus']=='Failed')
		{?>
			<div id="hsbody">			
			<p><h5><?=t($t_base.'fields/thank_you_booking');?></h5></p>
			<p> <?=t($t_base.'fields/booking_ref_is');?>: <?=$postSearchAry['szBookingRef']?></p><br>
			<p> <? echo t($t_base.'fields/your_paid_amount_does_not_match_with_booking_amount');?>.</p><br>
		</div>
		<?php
			unset($_SESSION['amount_mismatch']);
		}
		else {?>
		<div id="hsbody">			
			<p><h5><?=t($t_base.'fields/thank_you_booking');?></h5></p>
			<p> <?=t($t_base.'fields/booking_ref_is');?>: <?=$postSearchAry['szBookingRef']?></p><br>
			<p> <? echo t($t_base.'fields/your_payment_status_is_in_pending_state');?></p><br>
		</div>		
		<? }?>
		<div id="hsbody">
			<p><?=t($t_base.'fields/another_comparison');?> <a href="<?=__HOME_PAGE_URL__?>"><?=t($t_base.'fields/compare_book');?></a></p>
			<p><?=t($t_base.'fields/overview_of_your_bookings');?> <a href="<?=__BASE_URL__.'/myBooking/'?>"><?=t($t_base.'fields/my_bookings');?></a></p>
		</div>
<?php
$invoiceUrl=__BASE_URL__."/viewInvoice/".$postSearchAry['id']."/";
$bookingConfirmationUrl=__BASE_URL__."/viewBookingConfirmation/".$postSearchAry['id']."/";

$_SESSION['booking_id'] = '';
unset($_SESSION['booking_id']);
$_SESSION['booking_register_shipper_id'] = '';
$_SESSION['booking_register_consignee_id'] = '';
unset($_SESSION['booking_register_shipper_id']);
unset($_SESSION['booking_register_consignee_id']);

require_once(__APP_PATH_LAYOUT__."/footer.php");
$kNPS = new cNPS();
if(!$kNPS->getCustomerFeedBackDay($_SESSION['user_id']))
{
?>
<script type="text/javascript">
       var elements = document.getElementsByTagName('a');
                  //alert(elements[0].href);
                  for(var i = 0, len = elements.length; i < len; i++) 
                  {
                        //alert(elements[28].href);
                        if(elements[i].href!='' && elements[i].href!='javascript:void(0)' && elements[i].href!='javascript:void(0);' && elements[i].href!='<?=$invoiceUrl?>' && elements[i].href!='<?=$bookingConfirmationUrl?>')
                        {
                        	 // alert(elements[i].href);	
                              $(elements[i]).attr('onClick','checkPageRedirectionBulkService_feedback(\''+elements[i].href+'\');');
                              $(elements[i]).attr('href','javascript:void(0);');
                  		}
                  }
</script>
<?php }?>