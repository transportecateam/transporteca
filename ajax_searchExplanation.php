<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
$kExplain = new cExplain;
static $i= 0 ;
$t_home_base = "home/";
$szSearchDefaultText = t($t_home_base.'footer/type_word_or_phrase');

if(empty($_REQUEST['searchField']) || trim($_REQUEST['searchField'])==$szSearchDefaultText)
{
	//echo "ERROR||||";
	die;
}
if(isset($_REQUEST['searchField']) && !empty($_REQUEST['searchField']) && trim($_REQUEST['searchField'])!=$szSearchDefaultText)
{	
	$searchF = sanitize_all_html_input(trim($_REQUEST['searchField']));
	$kExplain->submitSearchFields($searchF);
	$searchField = strtolower($searchF);
	
	$iLanguage = getLanguageId();
	
	//FINDING OUT MATCHING CASES FOR EXPLAIN PAGES
	$SearchExplain = $kExplain->searchExplanation($searchField,$iLanguage);
	//print_r($SearchExplain);
	$searchArr1 = array();
	if($SearchExplain != array())
	{	
		$searchArr1 = showAllListing($SearchExplain,$searchField,'szDescription','',__EXPLAIN_PAGE_URL__);
	}
	
	//FINDING OUT MATCHING CASES FOR BLOG PAGES
	
	$SearchExplain = array();
	$searchArr2 = array();
	$SearchExplain = $kExplain->serviceBlogSearch($searchField,$iLanguage);
	//print_r($SearchExplain);
	if($SearchExplain != array())
	{		
		 $searchArr2 = showAllListing($SearchExplain,$searchField,'szContent','Knowledge Centre',__BLOG_PAGE_URL__);
	}
	
	//FINDING OUT MATCHING CASES FOR FAQ
	
	$SearchExplain = array();
	$searchArr3 = array();
	$SearchExplain = $kExplain->serviceFAQSearch($searchField,$iLanguage);
	//print_r($SearchExplain);
	if($SearchExplain != array())
	{
		 $searchArr3 = showAllListing($SearchExplain,$searchField,'szDescription','FAQ',__FAQ_PAGE_URL__);
	}
	
//content for showing on left nav
	if(!isset($kExplain))
	{
		$kExplain = new cExplain;
	}
	$links = array();
	$links = $kExplain->selectPageDetailsUserEnd($iLanguage);
	if($links != array())
	{
		foreach($links as $linking)
		{
	?>	
		<li class="triangle"><a href="<?=__EXPLAIN_PAGE_URL__.$linking['szLink']?>"> <?=$linking['szPageHeading']?></a></li>
	<?	}
	}
	ECHO "|||||";
//CONTENT ENDS HERE
if(!empty($searchArr1) || !empty($searchArr2) || !empty($searchArr3) )
$newArr = array_merge($searchArr1,$searchArr2,$searchArr3);

$newSortArr = sortArray($newArr, 1, true);

echo "<h5><b>Search Results for \"".$searchF."\"</b></h5>";
if($newSortArr!= array())
{
	foreach($newSortArr as $arr)
	{
		echo $arr[0]."<br />";
	}
}
else
{
	echo "<i>Sorry, the search gave no results</i>";
}
}
?>