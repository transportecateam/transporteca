<?php
/**
 * Customer Account Confirmation 
 */
 ob_start();
session_start();
$szMetaTitle= __EMAIL_CONFIRMATION_META_TITLE__;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Users/AccountPage/";

$kBooking = new cBooking();

$szConfirmationKey = trim($_REQUEST['szRandomKey']); 

if(!empty($szConfirmationKey))
{
	$bookingSearchAry = array();
	$bookingSearchAry = $kBooking->getLinkByKey($szConfirmationKey);
	if(!empty($bookingSearchAry))
	{ 
		$szBookingRandomNum = $kBooking->getUniqueBookingKey(10);
		$szBookingRandomNum = $kBooking->isBookingRandomNumExist($szBookingRandomNum);
		
		$kConfig = new cConfig();
		$kConfig->iVolume = $bookingSearchAry['fCargoVolume'];
		$kConfig->iWeight = $bookingSearchAry['fCargoWeight'];
		$kConfig->idServiceType = $bookingSearchAry['idServiceType'];
		$kConfig->iOrigingCC = $bookingSearchAry['iOrigingCC'];
		
		$kConfig->iDestinationCC = $bookingSearchAry['iDestinationCC'];
		$kConfig->szOriginCountry = $bookingSearchAry['idOriginCountry'];
		$kConfig->idOriginPostCode = $bookingSearchAry['idOriginPostCode'];
		$kConfig->szOriginCountryName = $bookingSearchAry['szOriginCountry'];
		$kConfig->szOriginPostCode = $bookingSearchAry['szOriginPostCode'];
		$kConfig->szOriginCity = $bookingSearchAry['szOriginCity'];
		$kConfig->iOriginPostCodeSpecific = $bookingSearchAry['iOriginPostCodeSpecific'];
		
		$kConfig->szDestinationCountry = $bookingSearchAry['idDestinationCountry'];
		$kConfig->idDestinationPostCode = $bookingSearchAry['idDestinationPostCode'];
		$kConfig->szDestinationPostCode = $bookingSearchAry['szDestinationPostCode'];
		$kConfig->szDestinationCountryName = $bookingSearchAry['szDestinationCountry'];
		$kConfig->iDestinationPostCodeSpecific = $bookingSearchAry['iDestinationPostCodeSpecific'];
		$kConfig->szDestinationCity = $bookingSearchAry['szDestinationCity'];
		$kConfig->idTimingType = $bookingSearchAry['idTimingType'];
		
		$kConfig->dtTiming = $bookingSearchAry['dtTimingDate'];
		$kConfig->idCurrency = $bookingSearchAry['idCustomerCurrency'];
		$kConfig->fOriginLatitude = $bookingSearchAry['fOriginLatitude'];
		$kConfig->fOriginLongitude = $bookingSearchAry['fOriginLongitude'];
		
		$kConfig->fDestinationLatitude = $bookingSearchAry['fDestinationLatitude'];
		$kConfig->fDestinationLongitude = $bookingSearchAry['fDestinationLongitude'];
		$kConfig->fOriginLongitude = $bookingSearchAry['fOriginLongitude'];
		$kConfig->fOriginLongitude = $bookingSearchAry['fOriginLongitude']; 
		$kConfig->szBookingRandomNum = $szBookingRandomNum;
		$szReuquestPageUrl = __PRE_SERVICES_PAGE_URL__."/".$szConfirmationKey."/";
		 
		$kBooking->add($kConfig,__BOOKING_STATUS_ZERO_LEVEL__,$szReuquestPageUrl);
		
		$kConfig->loadCountry($postSearchAry['idOriginCountry']);
		$szCountryStrUrl = $kConfig->szCountryISO ;
		$kConfig->loadCountry($postSearchAry['idDestinationCountry']);
		$szCountryStrUrl .= $kConfig->szCountryISO ; 
		
		$szReuquestPageUrl =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
		 
		$_SESSION['do-not-load-booking-details'] = 1 ;
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
		 
		$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);
		$postSearchAry['iNumRecordFound'] = count($searchResultAry);
		$kBooking->addSelectServiceData($postSearchAry);
		
		
		$_SESSION['came_from_landing_page'.'_'.$szBookingRandomNum]=0;
		unset($_SESSION['came_from_landing_page']);
			
		ob_end_clean();
		header("location:".$szReuquestPageUrl);
		die;
	} 
}

include( __APP_PATH_LAYOUT__ . "/footer_new.php" );

?>