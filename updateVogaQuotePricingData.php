<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once(__APP_PATH_CLASSES__."/import.class.php");
$kImportData = new cImportData();

$resArr=$kImportData->getVogaBookingQuoteWhereForwarderAndCustomerDifferent();

if(!empty($resArr))
{
    foreach($resArr as $resArrs)
    {
        echo "--------Start --".$resArrs['szBookingRandomNum']."--------------<br />";
        $fTotalPriceForwarderCurrency = $resArrs['fTotalPriceForwarderCurrency'];
        $fForwarderTotalVat = $resArrs['fTotalVat'];
        $fForwarderCurrencyExchangeRate = $resArrs['fForwarderCurrencyExchangeRate'];
        $idForwarderCurrency = $resArrs['idForwarderCurrency'];
        $szForwarderCurrency = $resArrs['szForwarderCurrency'];
        $idCustomerCurrency = $resArrs['idCustomerCurrency'];
        $idQuotePricing = $resArrs['id'];
        
        echo "idBooking--".$resArrs['idBooking']."<br />";
        echo "fTotalPriceForwarderCurrency--".$fTotalPriceForwarderCurrency."<br />";
        echo "fTotalVat--".$fForwarderTotalVat."<br />";
        echo "fForwarderCurrencyExchangeRate--".$fForwarderCurrencyExchangeRate."<br />";
        echo "idForwarderCurrency--".$idForwarderCurrency."<br />";
        
        if($idForwarderCurrency==1)
        {
            $fTotalPriceForwarderCurrencyForReferralFeeUSD=$fTotalPriceForwarderCurrency+$fForwarderTotalVat;
        }
        else
        {
            $fTotalPriceForwarderCurrencyUSD = ($fTotalPriceForwarderCurrency*$resArrs['fForwarderCurrencyExchangeRate']);
            $fForwarderTotalVatUSD = ($fForwarderTotalVat*$resArrs['fForwarderCurrencyExchangeRate']);
            
            $fTotalPriceForwarderCurrencyForReferralFeeUSD = $fTotalPriceForwarderCurrencyUSD + $fForwarderTotalVatUSD;
        }
        echo "fTotalPriceForwarderCurrencyForReferralFeeUSD--".$fTotalPriceForwarderCurrencyForReferralFeeUSD."<br />";
        $fReferalPercentage = $resArrs['fReferalPercentage'];
        
        $fReferalFeeUSD = $fTotalPriceForwarderCurrencyForReferralFeeUSD * .01 * $fReferalPercentage;
        echo "fReferalFeeUSD--".$fReferalFeeUSD."<br />";
        
        if($idCustomerCurrency==1)
        {
            $fReferalFee_hidden = $fReferalFeeUSD;
        }
        else
        {
            $fCustomerExchangeRate = $resArrs['fCustomerExchangeRate'];
            echo "fCustomerExchangeRate--".$fCustomerExchangeRate."<br>";
            $fReferalFee_hidden = $fReferalFeeUSD /$fCustomerExchangeRate;
        }
        $fReferalFee = round((float)$fReferalFee_hidden);
        
        echo "fReferalFee_hidden--".$fReferalFee_hidden."<br />";
        echo "fReferalFee--".$fReferalFee."<br />";
        
        if($resArrs['iHandlingFeeApplicable']==1)
        {
            if($resArrs['idCurrency']==1) //USD
            {
                $fHandlingFeeCustomerCurrency = $resArrs['fTotalHandlingFeeUSD'];
            }
            else if($resArrs['fCustomerExchangeRate']>0)
            {
                $fHandlingFeeCustomerCurrency = round((float)($resArrs['fTotalHandlingFeeUSD']/$resArrs['fCustomerExchangeRate']),4);
            }
            if((float)$resArrs['fMarkupPercentage']>0){
                $fHandlingFeeCustomerCurrencyMarkup = round((float)($fHandlingFeeCustomerCurrency * $resArrs['fMarkupPercentage'] * .01),4);
            }
            
            echo "fHandlingFeeCustomerCurrencyMarkup--".$fHandlingFeeCustomerCurrencyMarkup."<br>";
        }
        
        $kImportData->updateReferalFeeInQuotePricing($idQuotePricing,$fReferalFee,$fReferalFee_hidden,$fHandlingFeeCustomerCurrencyMarkup);
        echo "<br/>--------End --".$resArrs['szBookingRandomNum']."--------------<br />";
    }
}