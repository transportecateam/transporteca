<?php
/**
 * Customer Account Confirmation 
 */
 ob_start();
session_start();
$szMetaTitle= __EMAIL_CONFIRMATION_META_TITLE__;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php" );

//error_reporting(E_ALL);
//ini_set('display_error','1');
//error_reporting(E_ALL);
 
$t_base = "Users/AccountPage/";

$kBooking = new cBooking();
$kWHSSearch = new cWHSSearch();
$kVatApplication = new cVatApplication(); 
$kQuote = new cQuote();

$idQuotePricing = $_REQUEST['quote_pricing_id'];
$szQuotePriceKey = $_REQUEST['quote_pricing_key'];
   
if(!empty($szQuotePriceKey))
{
    $bookingQuoteAry = array();
    $bookingQuoteAry = $kQuote->getQuotePricingIDbyKey($szQuotePriceKey);
    $idQuotePricing = $bookingQuoteAry['id']; 
    $idBooking = $bookingQuoteAry['idBooking'];
}

$iBookingLanguage = getLanguageId(); 
cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__; 
$kServices = new cServices();
$kServices->updateQuoteDetailsToBooking($idQuotePricing,$iBookingLanguage); 

if(!empty($kServices->szRedirectUrl))
{
    ob_end_clean(); 
    header("location:".$kServices->szRedirectUrl);
    die;
}
else
{
    ob_end_clean(); 
    header("location:".__BASE_URL__);
    die;
} 
?>