<?php
 ob_start();
session_start();
$szMetaTitle= __REGISTER_SHIPPER_CONSIGNEE_META_TITLE__;
$szMetaKeywords = __REGISTER_SHIPPER_CONSIGNEE_META_KEYWORDS__;
$szMetaDescription = __REGISTER_SHIPPER_CONSIGNEE_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );


$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Users/AccountPage/";
checkAuth();
$kRegisterShipCon = new cRegisterShipCon();
/*if($kUser->iConfirmed!='1')
{
	header('Location:'.__BASE_URL__);
	exit();
}*/
?>
<div id="my-account-body">
    <?php require_once( __APP_PATH_LAYOUT__ . "/nav.php" ); ?> 
    <div class="hsbody-2-right">
        <div id="reg_ship_consign">
            <div id="registerShipConsignList">
            <?php
            require_once( __APP_PATH__ . "/registeredShipperConsigneesList.php" ); ?>
            </div>
            <br />
            <div id="registerShipConsign">
            <?php
            require_once( __APP_PATH__ . "/registerShipConsign.php" ); ?>
            </div>
            <br />
            <br />
        </div>
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>