<?php
/**
 * Customer Login
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");

$t_base = "home/homepage/";
$mode=sanitize_all_html_input(trim($_REQUEST['mode']));

if($mode=='DELETE_SERVICE_TEMP_DATA')
{
	$kWHSSearch = new cWHSSearch();
	$szBookingRandomNum=sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum']));
	$kWHSSearch->deleteServiceTempData($szBookingRandomNum);
	$_SESSION['booking_ref_no']='';
	unset($_SESSION['booking_ref_no']);
}
else if($mode=='CHANGE_LANGUAGE_REDIRECT')
{
	$szUri = sanitize_all_html_input(trim($_REQUEST['szUri']));
	$iLanguage = sanitize_all_html_input(trim($_REQUEST['iLanguage']));
	
	if(!empty($szUri))
	{
		$specialUrlAry = array('1'=>'explain','2'=>'learn','3'=>'howitworks','4'=>'how-it-works','5'=>'company','6'=>'transportpedia','7'=>'saadan-virker-det','8'=>'virksomheden','9'=>'leksikon');
		$uriAry = explode("/",$szUri);
		$_SESSION['LANGUAGE_CHANGED_BY_USER'] = 1;
		 
		if($iLanguage==__LANGUAGE_ID_ENGLISH__)
		{	 
			$key = array_search($uriAry[2],$specialUrlAry);
			  
			$_SESSION['transporteca_language_en'] = ''; 
		  
			if($key>0)
			{ 
				echo __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
			}
			else
			{ 
				$szRequestUri = __MAIN_SITE_HOME_PAGE_SECURE_URL__.$szUri ;
				$redirect_url = str_replace(__MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__,__MAIN_SITE_HOME_PAGE_SECURE_URL__,$szRequestUri);
				echo $redirect_url;
			}
		}
		else
		{
			$_SESSION['transporteca_language_en'] = 'danish';
			  
			$szUrl = trim($uriAry[1]);
			$key = array_search($szUrl,$specialUrlAry);  
			if($key>0)
			{ 
				echo __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__."/" ;
			}
			else
			{ 
				$szRequestUri = __MAIN_SITE_HOME_PAGE_SECURE_URL__.$szUri ;
				$redirect_url = str_replace(__MAIN_SITE_HOME_PAGE_SECURE_URL__,__MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__,$szRequestUri);
				echo $redirect_url;
			}			
		}
	}
}
else
{

$idDestinationCountry = sanitize_all_html_input(trim($_REQUEST['idDestinationCountry']));
$idOriginCountry = sanitize_all_html_input(trim($_REQUEST['idOriginCountry']));
$szBookingRandomNum=sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum']));

$kConfig=new cConfig();
$page=(int)$_REQUEST['page'];
$countryNameAry=array();
$countryNameAry[0] = $idOriginCountry ;
$countryNameAry[1] = $idDestinationCountry;
$iLanguage = getLanguageId();
$orignCountrAry = $kConfig->getAllCountryInKeyValuePair(false,false,$countryNameAry,$iLanguage);

$postSearchAry['szOriginCountry'] = $orignCountrAry[$idOriginCountry]['szCountryName'];
$postSearchAry['szDestinationCountry'] = $orignCountrAry[$idDestinationCountry]['szCountryName'];

if($szBookingRandomNum!='')
{
	$_SESSION['booking_ref_no']='';
	unset($_SESSION['booking_ref_no']);
	$_SESSION['booking_ref_no']=$szBookingRandomNum;
}
else
{
	$szBookingRandomNum=$_SESSION['booking_ref_no'];
}

$kWHSSearch = new cWHSSearch();
$availableServiceAry = array();
if($idDestinationCountry!='' && $idOriginCountry!='')
{
	$kWHSSearch->getAllAvailableService($idOriginCountry,$idDestinationCountry,$szBookingRandomNum);
}

if((int)$page==1 || (int)$page==0)
 {
 	$from=0;
 	$to=100;
 }
 else
 {
 	$countervalue=$page-1;
 	$from=($countervalue*100);
 	$to=$from+100;
 	
 }
$availableServiceAry = $kWHSSearch->getAllAvailableServiceTempTable($szBookingRandomNum,$from,$to);
//print_r($availableServiceAry);
$iAvailableServiceCount = $kWHSSearch->getAllAvailableServiceTempTable($szBookingRandomNum);
$iAvailableServiceCount= count($iAvailableServiceCount);
 $kPagination  = new Pagination($iAvailableServiceCount,100,10);
 
 
 //print_r($availableServiceAry);
?>
<div id="popup-bg" class="white-bg"></div>
<div id="popup-container">	
	<div class="service-popup popup">
	<a heref="javascript:void(0);" onclick="closePopup('all_available_service','<?=$szBookingRandomNum?>');"><img class="close-icon" src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a>
	<h1><?=t($t_base.'fields/we_have')?> <?=number_format((int)$iAvailableServiceCount)?> <?=$iAvailableServiceCount==1?t($t_base.'fields/service_from'):t($t_base.'fields/services_from')?> <?=$postSearchAry['szOriginCountry']?> <?=t($t_base.'fields/to_service')?> <?=$postSearchAry['szDestinationCountry']?></h1>
	<div class="container" id="boxscroll2">
		<div class="head">
			<div class="first"><?=t($t_base.'fields/forwarder')?></div>
			<div class="second"><?=t($t_base.'fields/from')?></div>
			<div class="third"><?=t($t_base.'fields/to')?></div>
			<div class="fourth"><?=t($t_base.'fields/type')?></div>
			<div class="fifth"><?=t($t_base.'fields/frequency')?></div>
			<div class="sixth"><?=t($t_base.'fields/valid_to')?></div>
		</div>
			<?
				if(!empty($availableServiceAry))
				{
					//print_r($availableServiceAry);
					for($i=0;$i<count($availableServiceAry);++$i)
					{						
						if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_DTD__)
						{
							$szServiceType = " Door to Door";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_DTW__)
						{
							$szServiceType=" Door to Warehouse";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_WTD__)
						{
							$szServiceType=" Warehouse to Door";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_DTP__)
						{
							$szServiceType=" Door to Port";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_PTD__)
						{
							$szServiceType=" Port to Door";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_WTW__)
						{
							$szServiceType=" Warehouse to Warehouse";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_PTW__)
						{
							$szServiceType=" Port to Warehouse";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_WTP__)
						{
							$szServiceType=" Warehouse to Port";
						}
						else if($availableServiceAry[$i]['iServiceType']==__SERVICE_TYPE_PTP__)
						{
							$szServiceType=" Port to Port";
						}
			?>
				<div class="content">
					<div class="first">
						<?
							if(strlen($availableServiceAry[$i]['szForwarderName'])>15)
							{
								$availableServiceAry[$i]['szForwarderName'] = substr($availableServiceAry[$i]['szForwarderName'],0,12)."..";
							}
							if(!empty($availableServiceAry[$i]['szForwarderLogo']))
							{
								echo "<img id='source_image' alt='".$availableServiceAry[$i]['szForwarderName']."' src ='".__MAIN_SITE_HOME_PAGE_URL__."/image.php?img=".$availableServiceAry[$i]['szForwarderLogo']."&w=80&h=30'>";
							}
							else
							{
								echo "<img id='source_image' alt='".$availableServiceAry[$i]['szForwarderName']."' src ='#'>";
							}
						?>
					</div>
					<div class="second"><?=$availableServiceAry[$i]['szOriginCity']?></div>
					<div class="third"><?=$availableServiceAry[$i]['szDestinationCity']?></div>
					<div class="fourth"><?=$szServiceType?></div>
					<div class="fifth"><?=$availableServiceAry[$i]['szFrequency']?></div>
					<div class="sixth">
						<?php 
						
							$iMonth = (int)date("m",strtotime($availableServiceAry[$i]['dtValidTo']));
							$szMonthName = getMonthName($iLanguage,$iMonth);
							echo date('j.',strtotime($availableServiceAry[$i]['dtValidTo']))." ".$szMonthName;
						?>
					</div>
				</div>
						
						<?
					}
				}
			?>
	</div>
	<?
		$kPagination->paginate('serviceFoundPagination');
		if($kPagination->links_per_page>1)
		{
			echo $kPagination->renderFullNav();
		}
	?>
	<br/>
	<input type="hidden" id="szBookingRandomNum" name="szBookingRandomNum" value="<?=$szBookingRandomNum?>">
	<div align="center"><a href="javascript:void(0);" onclick="closePopup('all_available_service','<?=$szBookingRandomNum?>');" class="orange-button1"><span><?=t($t_base.'fields/close')?></span></a></div>
	</div>
</div>
<? }?>