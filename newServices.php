<?php
ob_start();
session_start();

//ini_set('display_error',1);
//error_reporting(E_ALL);
//ini_set('error_reporting',E_ALL);

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __SERVICES_PAGE_META_TITLE__;
$szMetaKeywords = __SERVICES_PAGE_META_KEYWORDS__;
$szMetaDescription = __SERVICES_PAGE_META_DESCRIPTION__;

$iNewServicePage = 1;
$iNewServicePageFlag = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
 
if(!empty($_REQUEST['booking_random_num']))
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
} 

//In case of Test Search Button click we kind of by pass login check 
$bByPassLoginCheck = true;
if(isset($_SESSION['admin_test_search_'.$szBookingRandomNum]) || $_SESSION['anonymous_user_logged_in']==1)
{
    $bByPassLoginCheck = false;
}

if(!empty($_REQUEST['booking_random_num']))
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
}
$kBooking = new cBooking();
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

$postSearchAry = array();
$postSearchAry = $kBooking->getBookingDetails($idBooking); 
$iGetQuoteFlagSearch=false;
if($postSearchAry['iGetQuotePageFlag']==1)
{
    $oldSearchHaveResultArr = array();
    $oldSearchHaveResultArr = $kBooking->getAllFileSearchLogs('','',true,$idBooking); 
    if(!empty($oldSearchHaveResultArr))
    {
        $res_upd_arr=array();
        $res_upd_arr['szOriginCountry']=$oldSearchHaveResultArr[0]['szFromLocation'];
        $res_upd_arr['szDestinationCountry']=$oldSearchHaveResultArr[0]['szToLocation'];
        $res_upd_arr['idServiceType']=$oldSearchHaveResultArr[0]['idServiceType'];
        $res_upd_arr['fCargoWeight']=$oldSearchHaveResultArr[0]['fCargoWeight'];
        $res_upd_arr['fCargoVolume']=$oldSearchHaveResultArr[0]['fCargoVolume'];
        $res_upd_arr['dtTimingDate']=$oldSearchHaveResultArr[0]['dtTimingDate'];
        $res_upd_arr['idDestinationCountry']=$oldSearchHaveResultArr[0]['idDestinationCountry'];
        $res_upd_arr['idOriginCountry']=$oldSearchHaveResultArr[0]['idOriginCountry'];
        $res_upd_arr['iGetQuotePageFlag']=0;
        $res_upd_arr['szInternalComment'] = '';
 
        if(!empty($res_upd_arr))
        {
            $update_query = '';
            foreach($res_upd_arr as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        $update_query = rtrim($update_query,",");
        $kBooking->updateDraftBooking($update_query,$idBooking);

        $postSearchAry = $kBooking->getBookingDetails($idBooking); 
        $iGetQuoteFlagSearch=true;
        $bLoadDataFromAjax = true; 
    } 
}

$iSmallShipmentFlag = $postSearchAry['iSmallShipment'];
if((int)$_SESSION['user_id']<=0 && $iSmallShipmentFlag!=1 && $postSearchAry['iQuickQuote']!=__QUICK_QUOTE_SEND_QUOTE__)
{
    /*
    * In case of small shipment or quick quote we allow user to see this even without log in. 
    */
    $postSearchAry = $kBooking->getBookingDetails($idBooking); 
    $kConfig_new = new cConfig(); 
    $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig_new->szCountryISO ;  
    $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
    $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
    
       
    $redirect_url=  __BOOKING_GET_RATES_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
    header("Location: ".$redirect_url);
    exit();
}
$t_base = "SelectService/";
constantApiKey();
  
if($postSearchAry['iQuotesStatus']>0 && $bByPassLoginCheck && $postSearchAry['iQuickQuote']!=__QUICK_QUOTE_SEND_QUOTE__)
{ 
    //It means this RFQ has already been sent so no need to send it again, we simply redirected user to landing page.
    ob_end_clean();
    header("Location:".__BASE_URL__);
    die;
}  
if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
{
    echo display_booking_already_paid($t_base,$idBooking);
    die;
}
$kWHSSearch= new cWHSSearch();
$bLoadDataFromAjax = false; 
if((int)$_SESSION['user_id']>0)
{	 
    if($postSearchAry['iAnonymusBooking']==1 && $_SESSION['user_id']!=$postSearchAry['idUser'])
    { 
        //This means this booking was creatde by Transporteca's anonymous user and it must be assigned to Logged user
        //$kWHSSearch->updateAnonymousBookingFlag($idBooking,true);
        
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking); 
    }
    else
    {
        //Updating logged in user details to booking.
        //$kWHSSearch->updateAnonymousBookingFlag($idBooking);
    }
     
    /**
    * From now we have enabled a feature that any user can see any other user's booking so we just assign booking to logged in user always no matter whoever is logged
    * 
    if(!$kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
    {
        echo display_booking_invalid_user_popup($t_base_details);
        die;
    } 
      
    * */ 
    
     //update customer currency
    if($postSearchAry['idCustomerCurrency']!=$kUser->szCurrency)
    { 
        if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
        {
            /*
            *  If currency of customer is changed in case of quick quote then we have recalculated the price based on new currency
            */
            //$kUser = new cUser();
            //$kUser->updateUserWithCurrency($postSearchAry['idCustomerCurrency']); 
        }
        else
        {
            $kWHSSearch->updateBookingWithCurrency($kUser->szCurrency,$szBookingRandomNum); 
            $bLoadDataFromAjax = true; 
            unset($_SESSION['load-booking-details-page-for-booking-id']);
            unset($_SESSION['load-booking-details-page-for-booking-id']); 
        } 
    }
} 

if($postSearchAry['iFinancialVersion']!='2')
{
    $bLoadDataFromAjax = true; 
    $res_ary=array(); 
    $res_ary['iFinancialVersion'] = 2; 

    $update_query = '';
    if(!empty($res_ary))
    { 
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }		
    if(!empty($update_query))
    {
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {
            //booking moves on to 8 step means Service options displayed.
        }
    } 
}
$szResultPageMetaTitle = t($t_base.'messages/SERVICE_PAGE_META_TITLE');
$iLanguage = getLanguageId();
$kConfig = new cConfig();
$languageArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
if(!empty($languageArr))
{
    $szLangParam = strtolower($languageArr[0]['szName']);
}
else
{
    $szLangParam = 'english';
}

if(!$bLoadDataFromAjax)
{
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__ && !empty($postSearchAry['szReferenceToken']))
    {
        cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__; 
        cPartner::$szGlobalToken = $postSearchAry['szReferenceToken']; 
        $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true); 
        $searchResultAry = unserialize($tempResultAry['szSerializeData']);  
        $szCourierCalculationLogString = $tempResultAry['szCourierCalculationLogString'];
        $iSearchResultLimit = count($searchResultAry);

        $res_ary = $searchResultAry ;
        $final_ary = sortArray($searchResultAry,'fDisplayPrice');  // sorting array ASC to fPrice  
        $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true); 
        
        
        $dtResponseSentDateTime = strtotime($postSearchAry['dtCreatedOn']); 
        $dtMinus24HoursTime = strtotime($dtMinus24HoursDateTime);
        $szDeveloperNotes .= ">. Price response sent on: ".$postSearchAry['dtCreatedOn']." Booking Request received on: ".$dtMinus24HoursDateTime."".PHP_EOL; 
        if($dtMinus24HoursTime>=$dtResponseSentDateTime)
        {
            /*
             * If the call is more than 24 hours old then we recalculate pricing
             */
            $bRecalculateFlag = true;
            $szDeveloperNotes .= ">. Requested service is more then 24 hours old so we need to recalculate booking prices again ".PHP_EOL; 
        }   
        if($bRecalculateFlag)
        {  
            $bDisplayPriceChangedPopup = false;
            /*
            * Recalculate transporteca pricing 
            */
            $bDisplayPriceChangedPopup = false;
            
            $kQuote = new cQuote();
            $szRecalculationTag = $kQuote->recalculateQuickQuotePricing($postSearchAry);
            
            if($szRecalculationTag=='WARNING')
            {
                $szTitle = t($t_base.'fields/price_updated_heading');
                $szMessage = t($t_base.'fields/forwarder_has_updated_price');
                echo "<div id='quick-quote-price-changed'>";
                success_message_popup($szTitle,$szMessage,'quick-quote-price-changed');
                echo "</div>";
            }
        } 
    }
    else 
    {
        $iShipperConsignee = $postSearchAry['iShipperConsignee'];
        $idServiceType = $postSearchAry['idServiceType'];
        
        /*
         *  1. If customer type is SHIPPER and they have searched for PTD then we recalculate service based on DTP because for Shipper do not provide PTD service
         *  2. If customer type is CONSIGNEE and they have searched for DTP then we recalculate service based on PTD because for consignee we do not provide DTP service.
        */
         
        if($iShipperConsignee==1 && $idServiceType==__SERVICE_TYPE_PTD__)
        {
            $recalculateServices = true; 
            $idNewServiceType = __SERVICE_TYPE_DTP__;
        }
        else if($iShipperConsignee==2 && $idServiceType==__SERVICE_TYPE_DTP__)
        { 
            $recalculateServices = true; 
            $idNewServiceType = __SERVICE_TYPE_PTD__;
        } 
        
        if($recalculateServices)
        {  
            $idServiceTerms = $kBooking->getServiceTerms($idNewServiceType,$iShipperConsignee);
                 
            unset($_SESSION['load-booking-details-page-for-booking-id']);
            unset($_SESSION['load-booking-details-page-for-booking-id']); 
            
            $res_ary=array(); 
            $res_ary['idServiceType'] = $idNewServiceType;
            $res_ary['idServiceTerms'] = $idServiceTerms;
            
            $update_query = '';
            if(!empty($res_ary))
            { 
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }		
            if(!empty($update_query))
            {
                $update_query = rtrim($update_query,",");
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    //booking moves on to 8 step means Service options displayed.
                }
            } 
            $searchResultAry = array();
            $bLoadDataFromAjax = true;
        } 
        else if($kBooking->check_30_minute_window($postSearchAry))
        {  
            $tempResultAry = $kWHSSearch->getSearchedDataFromTempData($idBooking);
            $searchResultAry = unserialize($tempResultAry['szSerializeData']); 
            $szCourierCalculationLogString = $tempResultAry['szCourierCalculationLogString'];
            $iSearchResultLimit = count($searchResultAry);

            $res_ary = $searchResultAry ;
            $final_ary = sortArray($searchResultAry,'fDisplayPrice');  // sorting array ASC to fPrice  
            $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true); 
        }  
    }
    
    if(!empty($searchResultAry))
    {
        $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true); 
        $res_ary=array();
        if((int)$postSearchAry['iBookingStep']<8)
        {
            $res_ary['iBookingStep'] = 8 ;
        }
        $res_ary['iBookingType'] = __BOOKING_TYPE_AUTOMATIC__;
        $res_ary['iBookingStep'] = 8 ;
        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }		
        if(!empty($update_query))
        {
            $update_query = rtrim($update_query,",");
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                //booking moves on to 8 step means Service options displayed.
            }
        } 
    }
    else
    { 
        $bLoadDataFromAjax = true;
    } 
} 

$classNameSearchMiniVersionList ="";
$classNameSearchMiniVersion ="";
if($postSearchAry['iSearchMiniVersion']==5 || $postSearchAry['iSearchMiniVersion']==6)
{
    $kCourierServices= new cCourierServices();
    $bDTDTradesFlag = false;
    if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
    { 
        $classNameSearchMiniVersionList='class="search_list_serachmini_5"';
        $classNameSearchMiniVersion='class="search-mini-container version-'.$postSearchAry['iSearchMiniVersion'].'"';
    } 
}


?>
<script type="text/javascript"> 
    $(document).ready(function(){setTimeout(function(){$("body").addClass("new-searvice-page-main-container")},"1000")}); 
</script>
<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style> 
<div id="customs_clearance_pop_right" class="help-pop right">
</div>  
<div id="all_available_service" style="display:none;"></div>
<div id="customs_clearance_pop1" style="display:none;"></div>
<div id="support_heading_bubble" style="display:none;"></div>

<section id="search-result" <?php echo $classNameSearchMiniVersionList;?>>
    <?php 
        if($bLoadDataFromAjax)
        {   
            ?>
            <div id="service-search-form-container" <?php echo $classNameSearchMiniVersion;?>>
                <?php
                    echo display_new_search_form($kBooking,$postSearchAry,false,false,true);
                ?>
            </div>
            <div id="service-listing-container">
                <h3 class="service-waiting-text"><?=t($t_base.'fields/service_waiting_text');?></h3>
            </div>
            <div id="rating_popup"></div>
            <?php
        } 
        else 
        { 
            echo display_new_services_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,false,$szCourierCalculationLogString); 
        }
    ?> 
</section> 
<script type="text/javascript">
$().ready(function(){ 
    $("#search-btn-container").addClass('search-btn-clicked');    
    <?php if($bLoadDataFromAjax){ ?>
        display_service_list_page(); 
    <?php }else{ ?>
        $("#search-btn-container").removeClass('search-btn-clicked'); 
        //load_booking_detail_field('<?=$szBookingRandomNum?>','<?php echo $szLangParam; ?>'); 
    <?php }?> 
});

function display_service_list_page()
{ 
    var iSearchMiniVersion= <?php echo $postSearchAry['iSearchMiniVersion'];?>
     
        if(parseInt(iSearchMiniVersion)==5)
        {
            $("#search-btn-container_V_"+iSearchMiniVersion).addClass('search-btn-clicked');        
        }else{
            $("#search-btn-container").addClass('search-btn-clicked');
        }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php?lang=<?php echo $szLangParam; ?>",{szBookingRandomNum:'<?=$szBookingRandomNum?>',hideDivFlag:'SIMPLE_SERVICES',flag:'NEW_LANDING_PAGE'},function(result){		
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#search-result").html(result_ary[1]);
            <?php if(!empty($_SESSION['load-booking-details-page-for-booking-id']) && $_SESSION['load-booking-details-page-for-booking-id']==$postSearchAry['szBookingRandomNum']){ } else {?>
                load_booking_detail_field('<?=$szBookingRandomNum?>','<?php echo $szLangParam; ?>'); 
            <?php }?>
             if(parseInt(iSearchMiniVersion)==5)
            {
                $("#search-btn-container_V_"+iSearchMiniVersion).removeClass('search-btn-clicked');        
            }else{
                $("#search-btn-container").removeClass('search-btn-clicked');
            } 
        }
        if(result_ary[0]=='NO_RECORD_FOUND')
        {
            $("#customs_clearance_pop1").html(result_ary[1]);
             if(parseInt(iSearchMiniVersion)==5)
            {
                $("#search-btn-container_V_"+iSearchMiniVersion).removeClass('search-btn-clicked');        
            }else{
                $("#search-btn-container").removeClass('search-btn-clicked');
            } 
        }	
        else
        {
            if(parseInt(iSearchMiniVersion)==5)
            {
                $("#search-btn-container_V_"+iSearchMiniVersion).removeClass('search-btn-clicked');        
            }else{
                $("#search-btn-container").removeClass('search-btn-clicked');
            } 
        }
        document.title = '<?php echo $szResultPageMetaTitle; ?>';	
   }); 
}
</script>

<?php
$kConfig_new = new cConfig(); 
$kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
$szOriginCountryCode = $kConfig_new->szCountryISO ;  
$kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
$szDestinationCountryCode = $kConfig_new->szCountryISO ;
?>
<form id="booking-details-form-hidden" name="booking-details-form-hidden">

</form> 
<input type="hidden" name="iServicePage" value="1" id="iServicePage">

<input type="hidden" name="szOriginCountryCode" value="<?php echo $szOriginCountryCode;?>" id="szOriginCountryCode">
<input type="hidden" name="szDestinationCountryCode" value="<?php echo $szDestinationCountryCode;?>" id="szDestinationCountryCode">
<input type="hidden" name="idCustomerCountry" value="<?php echo $postSearchAry['szCustomerCountry'];?>" id="idCustomerCountry">

<input type="hidden" name="iServicePage" value="1" id="iServicePage">
<input type="hidden" name="showBookingFieldsFlag" value="" id="showBookingFieldsFlag">
<input type="hidden" name="szServiceUniqueKey" value="" id="szServiceUniqueKey">
<input type="hidden" name="iShipperConsigneeSubmit" value="1" id="iShipperConsigneeSubmit">


<?php
$iServicePageAdword = 1 ;
echo html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");

if($iGetQuoteFlagSearch){?>
<script type="text/javascript">
update_service_listing('landing_page_form','0');
</script>
<?php
}?>