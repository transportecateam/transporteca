<?php
/**
 * Customer My Account  
 */
 ob_start();
session_start();
//error_reporting (E_ALL);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
//require_once( __APP_PATH_LAYOUT__ . "/header.php" );

$kRss = new cRSS();
header('Content-type: application/rss+xml');
echo $kRss->GetFeed();
?>