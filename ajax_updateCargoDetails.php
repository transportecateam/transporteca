<?php

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
 
$t_base = "LandingPage/";

$kBooking = new cBooking();
$kConfig = new cConfig();
$idBooking = $postSearchAry['id'];

// geting all weight measure 
$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

// geting all cargo measure 
$cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
$number = 1;
if($use_session_flag)
{
        $cargoAry = $_SESSION['cargo_details_'.$idBooking];
        $cargo_counter = count($cargoAry['iLength']);
        //echo "<br /> use session ";
}
else
{
        $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
        $cargo_counter = count($cargoAry);
        //echo "<br /> use db ";
}
if($cargo_counter<=0)
{
        $cargo_counter = 1;
}
if(!empty($cargoAry))
{
        if($use_session_flag)
        {
            $fLength = ceil($cargoAry['iLength'][$number]);
            $fWidth = ceil($cargoAry['iWidth'][$number]);
            $fHeight = ceil($cargoAry['iHeight'][$number]);
            $fWeight = ceil($cargoAry['iWeight'][$number]);
            $iQuantity = ceil($cargoAry['iQuantity'][$number]);
            $idCargo = $cargoAry['id'][$number];
            $iDangerCargo = $cargoAry['iDangerCargo'][$number];

        }
        else
        {
            $fLength = ceil($cargoAry[$number]['fLength']);
            $fWidth = ceil($cargoAry[$number]['fWidth']);
            $fHeight = ceil($cargoAry[$number]['fHeight']);
            $fWeight = ceil($cargoAry[$number]['fWeight']);
            $iQuantity = ceil($cargoAry[$number]['iQuantity']);
            $idCargo = $cargoAry[$number]['id'];
            $iDangerCargo = $cargoAry[$number]['iDangerCargo'];
        }
}
if($disabled)
{
        $disabled_str = "disabled='disabled'";
}
$szBookingRefNum = $postSearchAry['szBookingRandomNum'] ;

$kWhsSearch = new cWHSSearch();

$iLanguage = getLanguageId();
//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{
//    $szVariableName = '__IS_DANGEROUS_CARGO_DANISH__' ;
//}
//else
//{
    $szVariableName = '__IS_DANGEROUS_CARGO__' ;
//}

$szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
if(empty($szExplainUrl))
{
    $szExplainUrl = __EXPLAIN_PAGE_URL__ ;
}

$iLanguage = getLanguageId();		
if($iLanguage==__LANGUAGE_ID_DANISH__)
{
    $szYouAreDoneImageName = "done-arrow-da.png";
}
else
{
    $szYouAreDoneImageName = "done-arrow.png";
}

//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{
    $szDangor_cargo_yes = t($t_base.'fields/yes');
    $szDangor_cargo_no = t($t_base.'fields/no');
//}
//else
//{
//    $szDangor_cargo_yes = 'Yes';
//    $szDangor_cargo_no = 'No';
//} 
?>		
<script type="text/javascript">
$().ready(function(){
        <?php
        if(!$disabled && !$use_session_flag)
        {
        if((int)$postSearchAry['iDoNotKnowCargo']==1)
                {
                        ?>
                        $("#iDoNotKnow").attr('checked','checked');			
                        autofill_cargo_dimentions('REQUIREMENT_PAGE',false,'<?php echo $weightMeasureAry[0]['szDescription']?>','<?php echo $cargoMeasureAry[0]['szDescription']?>','<?php echo $szDangor_cargo_no; ?>');
                        <?php
                }
        }
        if(!$disabled)
        {
            ?>
            enable_cargo_show_option_button();
            <?php 
        } 
        ?>
        Custom.init();
        <?php  
        ?>
});
</script>
<div id="complete-box" class="active transporteca-box">
<form action="" id="cargo_details_form" method="post"> 
        <p class="cargo-heading"><?=t($t_base.'title/cargo_details_title');?></p>
        <p class="checkbox-ab">
            <input type="checkbox" class="styled" <?=$disabled_str?> name="cargodetailAry[iDoNotKnowCargo]" onclick="autofill_cargo_dimentions('REQUIREMENT_PAGE',false,'<?php echo $weightMeasureAry[0]['szDescription']?>','<?php echo $cargoMeasureAry[0]['szDescription']?>','<?php echo $szDangor_cargo_no; ?>');" value="1" id="iDoNotKnow">
            <?=t($t_base.'title/donot_know_cargo_title');?>
        </p>	
        <div class="cargo1 clearfix">
            <p class="field-name"><?=t($t_base.'title/outer_dimensions');?></p>
            <p class="input-title">
                <span><?=t($t_base.'fields/length');?></span>
                <input type="hidden" name="cargodetailAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
                <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Length',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Length');validate_cargo_fields('Length',this.id,'<?=$number?>');" name="cargodetailAry[iLength][<?=$number?>]" value="<?=$fLength?>" id= "iLength<?=$number?>" />
            </p>
            <p class="sep">x</p>
            <p class="input-title">
                <span><?=t($t_base.'fields/width');?></span>
                <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Width',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Width');validate_cargo_fields('Width',this.id,'<?=$number?>');"  name="cargodetailAry[iWidth][<?=$number?>]" value="<?=$fWidth?>" id= "iWidth<?=$number?>" />
            </p>
            <p class="sep">x</p>
            <p class="input-title">
                <span><?=t($t_base.'fields/height');?></span>
                <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Height',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Height');validate_cargo_fields('Height',this.id,'<?=$number?>');" name="cargodetailAry[iHeight][<?=$number?>]" value="<?=$fHeight?>" id= "iHeight<?=$number?>" />
            </p>
            <p class="input-title"><span>&nbsp;</span>
                <select name="cargodetailAry[idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?=$number?>"  <?=$disabled_str?> class="styled">
                    <?php
                                        if(!empty($cargoMeasureAry))
                                        {
                                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                                {
                                                        ?>
                                                                <option value="<?=$cargoMeasureArys['id']?>" <? if($cargoAry[$number]['idCargoMeasure']==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
                                                        <?php
                                                }
                                        }
                                 ?>
                        </select>
                </p>
                <p class="sep">x</p>
                <p class="input-title">
                        <span><?=t($t_base.'fields/quantity');?></span>
                        <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Quantity',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Quantity');validate_cargo_fields('Quantity',this.id,'<?=$number?>');" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?=$number?>" />
                </p>
        </div>
        <div class="cargo2 clearfix">
                <p class="field-name"><?=t($t_base.'fields/total_weight');?></p>
                <p class="input-title">
                        <input type="text" <?=$disabled_str?> pattern="[0-9]*" name="cargodetailAry[iWeight][<?=$number?>]" onblur="validate_cargo_fields('Weight',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Weight');validate_cargo_fields('Weight',this.id,'<?=$number?>');" value="<?=$fWeight?>" id= "iWeight<?=$number?>" />
                </p>
                <p class="sep">&nbsp;</p>
                <p class="input-title">
                        <select size="1" id= "idWeightMeasure<?=$number?>" <?=$disabled_str?> name="cargodetailAry[idWeightMeasure][<?=$number?>]" class="styled">
                                 <?php
                                        if(!empty($weightMeasureAry))
                                        {
                                                foreach($weightMeasureAry as $weightMeasureArys)
                                                {
                                                        ?>
                                                                <option value="<?=$weightMeasureArys['id']?>" <? if($cargoAry[$number]['idWeightMeasure']==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
                                                        <?php
                                                }
                                        }
                                 ?>
                        </select>
                </p>
                <p class="text"><?=t($t_base.'fields/dangerous_cargo');?>? <a href="<?=$szExplainUrl?>" target="__blank"><?=t($t_base.'fields/help');?>!</a></p>
                <p class="sep">&nbsp;</p>
                <p class="input-title">
                        <select size="1" <?=$disabled_str?> id= "iDangerCargo<?=$number?>"  name="cargodetailAry[iDangerCargo][<?=$number?>]" class="styled">
                                <option value="No" <? if($iDangerCargo == 'No'){?>selected<? } ?> ><?=$szDangor_cargo_no?></option>
                                <option value="Yes" <? if($iDangerCargo == 'Yes'){?>selected<? } ?>><?=$szDangor_cargo_yes?></option>
                        </select>
                </p>
        </div>

        <?php
        //echo $cargo_counter ;
        if($cargo_counter>1)
        {	
                echo "<div id='cargo'>";
                        $ctr=0;
                        for($number=2;$number<=$cargo_counter;$number++)
                        {
                                if(!empty($cargoAry))
                                {
                                        if($use_session_flag)
                                        {
                                                $fLength = ceil($cargoAry['iLength'][$number]);
                                                $fWidth = ceil($cargoAry['iWidth'][$number]);
                                                $fHeight = ceil($cargoAry['iHeight'][$number]);
                                                $fWeight = ceil($cargoAry['iWeight'][$number]);
                                                $iQuantity = ceil($cargoAry['iQuantity'][$number]);
                                                $idCargo = $cargoAry['id'][$number];
                                                $iDangerCargo = $cargoAry['iDangerCargo'][$number];
                                        }
                                        else
                                        {
                                                $fLength = ceil($cargoAry[$number]['fLength']);
                                                $fWidth = ceil($cargoAry[$number]['fWidth']);
                                                $fHeight = ceil($cargoAry[$number]['fHeight']);
                                                $fWeight = ceil($cargoAry[$number]['fWeight']);
                                                $iQuantity = ceil($cargoAry[$number]['iQuantity']);
                                                $idCargo = $cargoAry[$number]['id'];
                                                $iDangerCargo = $cargoAry[$number]['iDangerCargo'];
                                        }
                                }	
                ?>
                <div id="cargo<?=$number-1?>">
                <hr>
                <div class="cargo1 clearfix">
                <p class="field-name"><?=t($t_base.'title/outer_dimensions');?></p>
                <p class="input-title">
                        <span><?=t($t_base.'fields/length');?></span>
                        <input type="hidden" name="cargodetailAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
                        <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Length',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Length');validate_cargo_fields('Length',this.id,'<?=$number?>');" name="cargodetailAry[iLength][<?=$number?>]" value="<?=$fLength?>" id= "iLength<?=$number?>" />
                </p>
                <p class="sep">x</p>
                <p class="input-title">
                        <span><?=t($t_base.'fields/width');?></span>
                        <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Width',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Width');validate_cargo_fields('Width',this.id,'<?=$number?>');"  name="cargodetailAry[iWidth][<?=$number?>]" value="<?=$fWidth?>" id= "iWidth<?=$number?>" />
                </p>
                <p class="sep">x</p>
                <p class="input-title">
                        <span><?=t($t_base.'fields/height');?></span>
                        <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Height',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Height');validate_cargo_fields('Height',this.id,'<?=$number?>');" name="cargodetailAry[iHeight][<?=$number?>]" value="<?=$fHeight?>" id= "iHeight<?=$number?>" />
                </p>
                <p class="input-title"><span>&nbsp;</span>
                        <select name="cargodetailAry[idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?=$number?>" <?=$disabled_str?> class="styled">
                                 <?php
                                        if(!empty($cargoMeasureAry))
                                        {
                                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                                {
                                                        ?>
                                                                <option value="<?=$cargoMeasureArys['id']?>" <? if($cargoAry[$number]['idCargoMeasure']==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
                                                        <?php
                                                }
                                        }
                                 ?>
                        </select>
                </p>
                <p class="sep">x</p>
                <p class="input-title">
                        <span><?=t($t_base.'fields/quantity');?></span>
                        <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Quantity',this.id,'<?=$number?>');" onkeyup="enable_cargo_show_option_button(event,'Quantity');validate_cargo_fields('Quantity',this.id,'<?=$number?>');" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?=$number?>" />
                </p>
        </div>
        <div class="cargo2 clearfix">
                <p class="field-name"><?=t($t_base.'fields/total_weight');?></p>
                <p class="input-title">
                        <input type="text" <?=$disabled_str?> pattern="[0-9]*" onblur="validate_cargo_fields('Weight',this.id,'<?=$number?>');" name="cargodetailAry[iWeight][<?=$number?>]" onkeyup="enable_cargo_show_option_button(event,'Weight');validate_cargo_fields('Weight',this.id,'<?=$number?>');" value="<?=$fWeight?>" id= "iWeight<?=$number?>" />
                </p>
                <p class="sep">&nbsp;</p>
                <p class="input-title">
                        <select size="1" <?=$disabled_str?> id= "idWeightMeasure<?=$number?>" name="cargodetailAry[idWeightMeasure][<?=$number?>]" class="styled">
                                 <?php
                                        if(!empty($weightMeasureAry))
                                        {
                                                foreach($weightMeasureAry as $weightMeasureArys)
                                                {
                                                        ?>
                                                                <option value="<?=$weightMeasureArys['id']?>" <? if($cargoAry[$number]['idWeightMeasure']==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
                                                        <?php
                                                }
                                        }
                                 ?>
                        </select>
                </p>
                <p class="text"><?=t($t_base.'fields/dangerous_cargo');?>? <a href="<?=$szExplainUrl?>" target="__blank"><?=t($t_base.'fields/help');?>!</a></p>
                <p class="sep">&nbsp;</p>
                <p class="input-title">
                        <select size="1" id= "iDangerCargo<?=$number?>" <?=$disabled_str?> name="cargodetailAry[iDangerCargo][<?=$number?>]" class="styled">
                                <option value="No" <? if($iDangerCargo =='No' || empty($iDangerCargo)){?>selected<? } ?> ><?=$szDangor_cargo_no?></option>
                                <option value="Yes" <? if($iDangerCargo == 'Yes'){?>selected<? } ?> ><?=$szDangor_cargo_yes?></option>
                        </select>
                </p>
        </div>	
</div>
                <?php
                $ctr++;				
        }
        echo "<div id='cargo".$cargo_counter."'></div></div>";
        $remove_cargo_link_style = " style='display:block;'";
}
else
{
        $remove_cargo_link_style = " style='display:none;'";
        ?>
        <div id="cargo">
                <div id="cargo<?=$cargo_counter?>"></div>
        </div>
        <?php
}
if(!$disabled)
{
?>
<div class="clearfix" style="margin-top: -6px;">
        <p class="fl" style="width:138px;"><a href="javascript:void(0);" onclick="add_more_cargo('REQUIREMENT_PAGE')">+ <?=t($t_base.'title/add_more_cargo');?></a></p>
        <p class="red_text" style="display:none;" id="cargo_measure_error_span">&nbsp;</p>
        <p style="margin-top:6px;margin-bottom:11px;" align="right" class="fr"><a href="javascript:void(0);" id="cargo_detail_button_show_option" class="gray-button1"><span><?=t($t_base.'title/show_options');?></span></a></p>
        <p class="fr" align="right" style="margin-right:30px"><a href="javascript:void(0);" id="remove" <?=$remove_cargo_link_style?> onclick="remove_cargo('REQUIREMENT_PAGE');"><?=t($t_base.'fields/remove_cargo');?></a></p>
        <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition" value="<?=$cargo_counter?>" />
        <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1" value="<?=$cargo_counter?>" />			
</div>
<?php }?>
<input type="hidden" name="idCargoMeasure_hidden" id="idCargoMeasure_hidden" value="1">
<input type="hidden" name="idWeightMeasure_hidden" id="idWeightMeasure_hidden" value="1">
<input type="hidden" name="iDangerCargo_hidden" id="iDangerCargo_hidden" value="No">
<input type="hidden" name="mode" value="CARGO_DETAILS">
<input type="hidden" id="booking_key" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">
</form> 
</div>