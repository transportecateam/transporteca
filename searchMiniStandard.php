<?php
ob_start();
session_start(); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$iLanguage = getLanguageId(); 
$t_base = "LandingPage/";

$kWhsSearch = new cWHSSearch();   
$szReferer = $_SERVER['HTTP_REFERER']; 
$idSeoPage = $_SESSION['seo_page_id'];   

$iSearchMiniPage = $_REQUEST['search_mini_version']; 
echo "PV: ".$iSearchMiniPage;

?> 
<div id="all_available_service" style="display:none;"></div>  
<section onmouseout="hideDatePicker();">     
    <div id="transporteca_search_form_container" class="search-mini-container">
       <?php 
           echo display_new_search_form($kBooking,$postSearchAry,true,false,false,$szDefaultFromField,$idSeoPage,true,$idDefaultLandingPage,false,1);
       ?>
   </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){ 
        $(".hasDatepicker").on("blur", function(e) { $(this).datepicker("hide"); });
    }); 
</script>
    
     