<?php
ob_start();
session_start(); 
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
//include( __APP_PATH_LAYOUT__ . "/header_new.php" ); 
require_once( __APP_PATH__ . "/inc/classes/imapReader.class.php" ); 

$box = new ImapReader();
$box
   ->connect()
   ->fetchAllHeaders()
;

echo $box->count() . " emails in mailbox\n";
for ($i = 0; ($i < $box->count()); $i++)
{
    $msg = $box->get($i);
    echo "<br><br>Reception date : {$msg->date}\n";
    echo "<br>From : {$msg->from}\n";
    echo "<br>To : {$msg->to}\n";
    echo "<br>Reply to : {$msg->from}\n";
    echo "<br>Subject : {$msg->subject}\n";
    $msg = $box->fetch($msg);
    echo "<br>Number of readable contents : " . count($msg->content) . "\n";
    foreach ($msg->content as $key => $content)
    {
        echo "<br><br>\tContent  " . ($key + 1) . " :\n";
        echo "<br>\t\tContent type : {$content->mime}\n";
        echo "<br>\t\tContent charset : {$content->charset}\n";
        echo "<br>\t\tContent size : {$content->size}\n";
    }
    echo "<br><br>Number of attachments : " . count($msg->attachments) . "\n";
    foreach ($msg->attachments as $key => $attachment)
    {
        echo "<br>\tAttachment " . ($key + 1) . " :\n";
        echo "<br>\t\tAttachment type : {$attachment->type}\n";
        echo "<br>\t\tContent type : {$attachment->mime}\n";
        echo "<br>\t\tFile name : {$attachment->name}\n";
        echo "<br>\t\tFile size : {$attachment->size}\n";
    }
    echo "<br>\n";
}

echo "<br><br>Searching '*Bob*' ...\n";
$results = $box->searchBy('*Bob*', ImapReader::FROM);
foreach ($results as $result)
{
    echo "\tMatched: {$result->from} - {$result->date} - {$result->subject}\n";
}