<?php
session_start();
if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/../"));
}
require_once(__APP_PATH__ . "/inc/constants.php");  
require_once( __APP_PATH__ . "/inc/functions.php" );
/* 
*  This file is used to download Transporteca's partner API 
*/

$szFileName = __APP_PATH__."/partner/Transporteca.zip";

if(!empty($szFileName) && file_exists($szFileName))
{	
    $filename = "Transporteca.zip";
    $file = $szFileName ;

    header('Pragma: public'); 	// required
    header('Expires: 0');		// no cache 
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($file_name)).' GMT');
    header('Cache-Control: private',false);
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename=' . urlencode(basename($filename)));
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($file));	// provide file size
    header('Connection: close');
    /*
    header('Content-Description: File Transfer');
    header('Content-Type: application/zip');
    header("Content-Type: application/force-download");
    header('Content-Disposition: attachment; filename=' . urlencode(basename($filename)));
    // header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
     * 
     */
    ob_clean();
    flush();
    readfile($file);     
    exit;	
}