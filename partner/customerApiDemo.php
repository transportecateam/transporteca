<?php 
session_start();
$szMetaTitle="Transporteca API Demo";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once(__APP_PATH__ . "/inc/constants.php");  
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once(__APP_PATH_LAYOUT__ ."/header_new.php");
 
$_SESSION['partnerApiRequestAry'] = array();
if(!empty($_SESSION['partnerApiRequestAry']))
{
    $_POST['partnerApiRequestAry'] = $_SESSION['partnerApiRequestAry'] ;
}

$arrRequestTypeAry = array(); 
$arrRequestTypeAry['CUSTOMER_LOGIN'] = 'Customer login';
$arrRequestTypeAry['CUSTOMER_SIGNUP'] = "Customer signup";
$arrRequestTypeAry['CUSTOMER_UPDATE_PROFILE'] = "Update customer profile";
$arrRequestTypeAry['CUSTOMER_PROFILE_DETAILS'] = "Customer profile details";
$arrRequestTypeAry['CUSTOMER_BOOKING_HISTORY'] = "Customer booking history";
$arrRequestTypeAry['CUSTOMER_CHANGE_PASSWORD'] = "Customer change password";
$arrRequestTypeAry['CUSTOMER_FORGOT_PASSWORD'] = "Customer forgot password";
$arrRequestTypeAry['DELETE_CUSTOMER_ACCOUNT'] = "Delete Customer Account";
$arrRequestTypeAry['CUSTOMER_REGISTERED_SHIPPER'] = "Registered Shipper";
$arrRequestTypeAry['CUSTOMER_REGISTERED_CONSIGNEE'] = "Registered Consignee";
$arrRequestTypeAry['CUSTOMER_REGISTERED_SHIPPER_DETAIL'] = "Registered Shipper Details";
$arrRequestTypeAry['CUSTOMER_REGISTERED_CONSIGNEE_DETAIL']= "Registered Consignee Details";
$arrRequestTypeAry['CUSTOMER_UPDATE_SHIPPER_DETAIL']= "Update Shipper Details";
$arrRequestTypeAry['CUSTOMER_UPDATE_CONSIGNEE_DETAIL']= "Update Consignee Details";
$arrRequestTypeAry['FORWARDER_RATING']= "Forwarder Rating";
$arrRequestTypeAry['NEWS_LETTERS']= "News Letters";

?>
<div id="hsbody-2"> 
    <style type="text/css">
        .format-2 td {
            background-color: #ffffff;
            font-size: 14px;
            padding: 4px 10px;
            text-align: left;
        }
        .api_content {
            background: #f4f4f4 none repeat scroll 0 0;
            border: 1px solid #dadada;
            border-radius: 6px;
            display: table;
            font-size: 100%;
            letter-spacing: 1px;
            margin: 12px auto;
            padding: 5px 0 5px 5px;
            width: 100%;
        }

        .api_subheading_1 {
            background: #ebf3f9 none repeat scroll 0 0;
            border: 1px solid #c3d9ec;
            border-radius: 6px;
            display: block;
            margin: auto;
            overflow: auto;
            padding: 10px;
            width: 92%;
        } 
        .api_subheading {
            background: #e7f6ec none repeat scroll 0 0;
            border: 1px solid #c3e8d1;
            border-radius: 6px;
            display: block;
            margin: auto;
            padding: 10px;
            width: 95%;
        }
        .api_area_heading {
            color: #666;
            font-weight: normal;
        }
        .api_area_heading h3 span {
            display: block;
            padding: 10px 0;
        }
        .api-flo {
            float: left;
            font-size: 13px;
            font-weight: normal;
            margin-left: 16px;
            width: 85%;
        }  
        .api_txtara {
            width: 25%;
        }
        .api_hdng1 {
            color: #333;
            display: block;
            float: left;
            font: bold 13px/1.5 "Helvetica Neue",Arial,"Liberation Sans",FreeSans,sans-serif;
            padding-left: 20px;
            text-align: right;
            width: 12%;
        } 
        .api_dscrarea span {
            border: 0 solid #333;
            float: left;
            font-size: 13px;
            margin-bottom: 3px;
        }
        .api_ori_hdng {
            color: #333;
            display: block;
            float: left;
            font-size: 13px;
            font-weight: bold;
            padding-right: 10px;
            text-align: right;
            width: 15%;
        }
        .api_dp_menu_1 {
            color: #666;
            display: block;
            font-size: 13px;
            font-weight: normal;
            width: 83%;
        }
        .api_clear
        {
            clear:both;
            display:block;
        }
    </style>
    <div class="hsbody-2-right"> 
        <h2>Transporteca Customer API Demo</h2>
        <br>
        <div id="courier_service_container_div">  
            <form id="get_api_service_form" name="get_api_service_form" method="post"> 
                <div class="api_content">
                    <div class="api_subheading_1"> 
                        <!--<h2>API Request</h2>-->
                        <table cellpadding="5" cellspacing="2">
                            <tr>
                                <td style="width:5%">Mode: </td>
                                <td style="width:45%">
                                    <select  name="partnerApiRequestAry[szMethod]" id="szMode">
                                        <!--<option value="TEST" <?php echo (($_POST['partnerApiRequestAry']['szMode']=="TEST")?'selected':''); ?>>Test</option>-->
                                        <option value="LIVE" <?php echo (($_POST['partnerApiRequestAry']['szMode']=="LIVE")?'selected':''); ?>>Live</option> 
                                    </select>
                                </td> 
                                <td style="width:15%">Request Type: </td>
                                <td style="width:35%">
                                    <select  name="partnerApiRequestAry[szRequestType]" id="szRequestType" onchange="update_default_params(this.value)" style="width:100%;">
                                        <option val="">Select</option>
                                        <?php
                                            if(!empty($arrRequestTypeAry))
                                            {
                                                foreach($arrRequestTypeAry as $szRequestType=>$arrRequestTypeArys)
                                                {
                                                    ?>
                                                    <option value="<?php echo $szRequestType; ?>" <?php echo (($_POST['partnerApiRequestAry']['szRequestType']==$szRequestType)?'selected':''); ?>><?php echo $arrRequestTypeArys; ?></option>
                                                    <?php
                                                }
                                            }
                                        ?> 
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="width:100%;">
                                    <span class="api_dscrarea">
                                        <br>
                                        <textarea cols="100" style="min-height:200px;width:100%;" rows="10" name="partnerApiRequestAry[szApiRequestParams]" id="szApiRequestParams"><?php echo $_POST['partnerApiRequestAry']['szApiRequestParams']; ?></textarea> 
                                    </span>
                                </td>
                            </tr> 
                        </table> 
                    </div>
                    
                    <br>
                    <div style="text-align:center;">
                        <input type="button" name="getCourierRateAry[szSubmit]" value="Get Details" onclick="submit_partner_api_demo_form();"> 
                    </div>
                </div>
            </form>
        </div>
        <div class="api_content" id="api_response_container" style="display:none;"> 
        </div>
    </div> 
</div>
<script type="text/javascript">
    function  update_default_params(szRequestType)
    {
        var szDefaultValue = $("#request_type_"+szRequestType).val();
        $("#szApiRequestParams").val(szDefaultValue);
    }
</script>
<div id="hidden_request_params_container" style="display:none;"> 
    <textarea name="request_type_CUSTOMER_LOGIN" id="request_type_CUSTOMER_LOGIN" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerLogin","szUserName":"ajay@whiz-solutions.com","szPassword":"Test1234"}</textarea> 
    <textarea name="request_type_CUSTOMER_SIGNUP" id="request_type_CUSTOMER_SIGNUP" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerSignup","szFirstName":"Ajay","szLastName":"Jha","szEmail":"***@****.com",  "szPhone":"9911466405", "iInternationDialCode":"91", "szCompanyName":"", "szCompanyRegNo":"", "isPrivate":"Yes","szPassword":"******","szConfirmPassword":"******", "szAddress":"Bandra", "szPostCode":"400050", "szCity":"Mumbai", "szCountry":"IN","szCurrency":"USD","iAcceptNewsUpdate":"1"}</textarea>  
    <textarea name="request_type_CUSTOMER_UPDATE_PROFILE" id="request_type_CUSTOMER_UPDATE_PROFILE" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerUpdateProfile","token":"XXXXXX","szFirstName":"Ajay","szLastName":"Jha","szEmail":"***@****.com",  "szPhone":"9911466405", "iInternationDialCode":"91", "szCompanyName":"", "szCompanyRegNo":"", "isPrivate":"Yes", "szAddress":"Bandra", "szPostCode":"400050", "szCity":"Mumbai", "szCountry":"IN","szCurrency":"USD","iAcceptNewsUpdate":"1"}</textarea>   
    <textarea name="request_type_CUSTOMER_BOOKING_HISTORY" id="request_type_CUSTOMER_BOOKING_HISTORY" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerBookings","token":"XXXXXXXXXXX"}</textarea> 
    <textarea name="request_type_CUSTOMER_PROFILE_DETAILS" id="request_type_CUSTOMER_PROFILE_DETAILS" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerProileDetails","token":"XXXXXXXXXXX"}</textarea> 
    <textarea name="request_type_CUSTOMER_CHANGE_PASSWORD" id="request_type_CUSTOMER_CHANGE_PASSWORD" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerChangePassword","token":"XXXXXXXXXXX","szCurrentPassword":"XXXXXXXXXXX","szPassword":"XXXXXXXXXXX","szConfirmPassword":"XXXXXXXXXXX"}</textarea> 
    <textarea name="request_type_CUSTOMER_FORGOT_PASSWORD" id="request_type_CUSTOMER_FORGOT_PASSWORD" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerForgotPassword","szEmail":"ajay@whiz-solutions.com","szLanguage":"DK"}</textarea>
    <textarea name="request_type_DELETE_CUSTOMER_ACCOUNT" id="request_type_DELETE_CUSTOMER_ACCOUNT" cols="100" style="min-height:200px;" rows="10">{"szRequest":"deleteCustomerAccount","token":"XXXXXXXXXXX","szEmail":"ajay@whiz-solutions.com","szPassword":"Test1234"}</textarea>
    <textarea name="request_type_CUSTOMER_REGISTERED_SHIPPER" id="request_type_CUSTOMER_REGISTERED_SHIPPER" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getShippers","token":"XXXXXXXXXXX","szCompanyName":"XYZ","szPostcode":"400001","szCity":"Mumbai","szCountryCode":"IN"}</textarea>
    <textarea name="request_type_CUSTOMER_REGISTERED_CONSIGNEE" id="request_type_CUSTOMER_REGISTERED_CONSIGNEE" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getConsignees","token":"XXXXXXXXXXX","szCompanyName":"XYZ","szPostcode":"1050","szCity":"Copenhagen","szCountryCode":"DK"}</textarea>
    <textarea name="request_type_CUSTOMER_REGISTERED_SHIPPER_DETAIL" id="request_type_CUSTOMER_REGISTERED_SHIPPER_DETAIL" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getShipperDetails","token":"XXXXXXXXXXX","idShipper":""}</textarea>
    <textarea name="request_type_CUSTOMER_REGISTERED_CONSIGNEE_DETAIL" id="request_type_CUSTOMER_REGISTERED_CONSIGNEE_DETAIL" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getConsigneeDetails","token":"XXXXXXXXXXX","idConsignee":""}</textarea>
    <textarea name="request_type_CUSTOMER_UPDATE_SHIPPER_DETAIL" id="request_type_CUSTOMER_UPDATE_SHIPPER_DETAIL" cols="100" style="min-height:200px;" rows="10">{"szRequest":"updateShipperDetails","token":"XXXXXXXXXXX","idShipper":"","iRegisteredType":"","szFirstName":"","szLastName":"","szEmail":"","szCompanyName":"","iInternationalDialCode":"","szPhoneNumber":"","szAddress":"","szAddress2":"","szAddress3":"","szCity":"","szPostCode":"","szState":"","szCountry":""}</textarea>
    <textarea name="request_type_CUSTOMER_UPDATE_CONSIGNEE_DETAIL" id="request_type_CUSTOMER_UPDATE_CONSIGNEE_DETAIL" cols="100" style="min-height:200px;" rows="10">{"szRequest":"updateConsigneeDetails","token":"XXXXXXXXXXX","idConsignee":"","iRegisteredType":"","szFirstName":"","szLastName":"","szEmail":"","szCompanyName":"","iInternationalDialCode":"","szPhoneNumber":"","szAddress":"","szAddress2":"","szAddress3":"","szCity":"","szPostCode":"","szState":"","szCountry":""}</textarea>
    <textarea name="request_type_CUSTOMER_ADD_SHIPPER_DETAIL" id="request_type_CUSTOMER_ADD_SHIPPER_DETAIL" cols="100" style="min-height:200px;" rows="10">{"szRequest":"updateShipperDetails","token":"XXXXXXXXXXX","iRegisteredType":"","szFirstName":"","szLastName":"","szEmail":"","szCompanyName":"","iInternationalDialCode":"","szPhoneNumber":"","szAddress":"","szAddress2":"","szAddress3":"","szCity":"","szPostCode":"","szState":"","szCountry":""}</textarea>
    <textarea name="request_type_CUSTOMER_ADD_CONSIGNEE_DETAIL" id="request_type_CUSTOMER_ADD_CONSIGNEE_DETAIL" cols="100" style="min-height:200px;" rows="10">{"szRequest":"updateConsigneeDetails","token":"XXXXXXXXXXX","iRegisteredType":"","szFirstName":"","szLastName":"","szEmail":"","szCompanyName":"","iInternationalDialCode":"","szPhoneNumber":"","szAddress":"","szAddress2":"","szAddress3":"","szCity":"","szPostCode":"","szState":"","szCountry":""}</textarea>
    <textarea name="request_type_FORWARDER_RATING" id="request_type_FORWARDER_RATING" cols="100" style="min-height:200px;" rows="10">{"szRequest":"forwarderRating","szForwarderAlias":"SM"}</textarea>
    <textarea name="request_type_NEWS_LETTERS" id="request_type_NEWS_LETTERS" cols="100" style="min-height:200px;" rows="10">{"szRequest":"newsLettersSignup","szName":"Ashish Srivastava","szEmail":"ashish@whiz-solutions.com","szLanguage":"EN"}</textarea>
</div> 
<?php
//include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
