<!DOCTYPE html>
<?php
session_start();
if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/../"));
}
require_once(__APP_PATH__ . "/inc/constants.php");

$iGetLanguageFromRequest = 1;
$_REQUEST['lang'] = 'english';

require_once(__APP_PATH_LAYOUT__."/ajax_header.php"); 

$t_base_partner_api = "Partner/PartnerApiDoc/"; 
$t_base_partner = "Partner/"; 

$mode = $_REQUEST['mode'];
if(empty($mode))
{
    $mode = 'php';
}
$kApi = new cApi();
$errorDetailsAry = array();
$errorDetailsAry = $kApi->getAllErrorCode();
  
$yourApi=  t($t_base_partner_api.'requestAttribute/yourApi');
$requestmode =t($t_base_partner_api.'requestAttribute/mode');

if($mode == "php")
{
    $szPriceCallString='';
    $szPriceCallString1 .="\Transporteca\Transporteca::setApiKey(".$yourApi.",".$requestmode.");<br>";
    $szPriceCallString2 .="\Transporteca\Service::getPrice(array(<br>";
    $szBookingString2 .="\Transporteca\Service::createBooking(array(<br>";
    $szInsuranceCallString2 .="\Transporteca\Service::getInsuranceValue(array(<br>";
    $szValidatePriceCallString2 .="\Transporteca\Service::validatePrice(array(<br>";
    $szStandardCargoCallString2 .="\Transporteca\Service::getStandardCargo(array(<br>";
    
    $szGetLabelCallString2 .="\Transporteca\Booking::getLabels(array(<br>";
    $szConfirmDownloadCallString2 .="\Transporteca\Booking::confirmDownload(array(<br>";
    $szSubmitNpsCallString2 .="\Transporteca\Booking::submitNPS(array(<br>";
}
elseif(trim($mode) == "json")
{
    $szPriceCallString='';
    $szPriceCallString1 .="\Transporteca\Transporteca::setApiKey(\"".$yourApi."\",".$requestmode.");<br>";
    $szPriceCallString2 .="\Transporteca\Service::getPrice({<br>";
    $szBookingString2 .="\Transporteca\Service::createBooking({<br>";
    $szInsuranceCallString2 .="\Transporteca\Service::getInsuranceValue({<br>";
    $szValidatePriceCallString2 .="\Transporteca\Service::validatePrice({<br>"; 
    $szStandardCargoCallString2 .="\Transporteca\Service::getStandardCargo({<br>";
    
    $szGetLabelCallString2 .="\Transporteca\Booking::getLabels({<br>";
    $szConfirmDownloadCallString2 .="\Transporteca\Booking::confirmDownload({<br>";
    $szSubmitNpsCallString2 .="\Transporteca\Booking::submitNPS({<br>";
}
$requestAry = array();

$responseAry = array();
        
$requestAry['szRequest']='price';
$requestAry['dtDay']=5;
$requestAry['dtMonth']=1;
$requestAry['dtYear']=2016;
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][0]['szPalletType'] = '';
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = '';
$requestAry['cargo'][0]['iHeight'] = '';
$requestAry['cargo'][0]['iWeight'] = '';
$requestAry['cargo'][0]['iQuantity'] = '';
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['cargo'][1]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][1]['szPalletType'] = '';
$requestAry['cargo'][1]['iLength'] = '';
$requestAry['cargo'][1]['iWidth'] = '';
$requestAry['cargo'][1]['iHeight'] = '';
$requestAry['cargo'][1]['iWeight'] = '';
$requestAry['cargo'][1]['iQuantity'] = '';
$requestAry['cargo'][1]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][1]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']='';
$requestAry['customerType'][0] = 'PRIVATE';  
$requestAry['customerType'][1] = 'BUSINESS';  
$requestAry['szCargoDescription']="Sample Description";
$requestAry['szCommodity'] = 1;
$requestAry['fCargoValue']=10000;
$requestAry['szCargoCurrency']="USD";
$requestAry['szShipperCompanyName']="ABC Seller Company";
$requestAry['szShipperFirstName']="Johan";
$requestAry['szShipperLastName']="Miller";
$requestAry['iShipperCountryDialCode']=91;
$requestAry['iShipperPhoneNumber']=1234567890;
$requestAry['szShipperEmail']="shipper@email.com";
$requestAry['szShipperAddress']="Highstreet 19";
$requestAry['szShipperPostcode']=440000;
$requestAry['szShipperCity']="Mumbai";
$requestAry['szShipperCountry']="IN";
$requestAry['szConsigneeCompanyName']="XYZ Buyer Company";
$requestAry['szConsigneeFirstName']="Albert";
$requestAry['szConsigneeLastName']="Metthew";
$requestAry['iConsigneeCountryDialCode']=45;
$requestAry['iConsigneePhoneNumber']=1231231234;
$requestAry['szConsigneeEmail']="consignee@email.com";
$requestAry['szConsigneeAddress']="Landevejen 15, baghuset";
$requestAry['szConsigneePostcode']=3480;
$requestAry['szConsigneeCity']="Fredensborg";
$requestAry['szConsigneeCountry']="DK";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['bInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
$requestAry['szPaymentType'] = "Stripe";
$requestAry['szStripeToken'] = "tok_XXXXXXXXXXXXXXXXXXXXX";
$requestAry['szLanguage']=t($t_base_partner_api.'requestAttribute/szLanguageKey');
$requestAry['szReference']=t($t_base_partner_api.'requestAttribute/reference');  


$responseAry['code'] = 200;
$responseAry['status'] = "OK";
$responseAry['token'] = "TTPE1KV2JTD8E9RS9BLO";
$responseAry['service'][0]['szServiceID'] = "0501TTP93981";
$responseAry['service'][0]['dtTransportFinishDay'] = 26;
$responseAry['service'][0]['dtTransportFinishMonth'] = 01;
$responseAry['service'][0]['dtTransportFinishYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/onlyCatons');
$responseAry['service'][0]['szCurrency'] = "USD";
$responseAry['service'][0]['fBookingPrice'] = 510.00;
$responseAry['service'][0]['fVatAmount'] = 127.5;
$responseAry['service'][0]['bInsuranceAvailable'] = t($t_base_partner_api.'responseAttribute/yes');
$responseAry['service'][0]['fInsurancePrice'] = 24.00;
$responseAry['service'][0]['szTransportMode'] = "COURIER";
$responseAry['service'][0]['szTransportMode'] = "COURIER";
$responseAry['service'][0]['szForwarderAlias'] = "SM";

 
$bookingResponseAry['code']=200;
$bookingResponseAry['status']="OK";
$bookingResponseAry['token']="TTPE1KV2JTD8E9RS9BLO";
$bookingResponseAry['bookings'][0]['dtTransportFinishDay']=26;
$bookingResponseAry['bookings'][0]['dtTransportFinishMonth']=01;
$bookingResponseAry['bookings'][0]['dtTransportFinishYear']=2016;
$bookingResponseAry['bookings'][0]['szBookingStatus']="Confirmed";
$bookingResponseAry['bookings'][0]['szBookingRef']="1601TCL049";
$bookingResponseAry['bookings'][0]['szServiceDescription']=t($t_base_partner_api.'responseAttribute/szServiceDescriptionParcel');
$bookingResponseAry['bookings'][0]['szPackingType']=t($t_base_partner_api.'responseAttribute/onlyCatons');
$bookingResponseAry['bookings'][0]['szTransportMode']="Courier";
$bookingResponseAry['bookings'][0]['szCurrency']="USD";
$bookingResponseAry['bookings'][0]['fBookingPrice']=510.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=127.50;
$bookingResponseAry['bookings'][0]['fInsurancePrice']=24.00;
$bookingResponseAry['bookings'][0]['bCargoInsurance']=t($t_base_partner_api.'responseAttribute/yes');
$bookingResponseAry['bookings'][0]['szCarrierName']="Transporteca";
$bookingResponseAry['bookings'][0]['szCarrierAddressLine1']="Fruebjergvej 3";
$bookingResponseAry['bookings'][0]['szCarrierAddressLine2']='';
$bookingResponseAry['bookings'][0]['szCarrierAddressLine3']='';
$bookingResponseAry['bookings'][0]['szCarrierPostcode']=2100;
$bookingResponseAry['bookings'][0]['szCarrierCity']="Copenhagen OE";
$bookingResponseAry['bookings'][0]['szCarrierRegion']='';
$bookingResponseAry['bookings'][0]['szCarrierCountry']="DK";
$bookingResponseAry['bookings'][0]['szBookingContactNumber']="+45 7199 7299";
$bookingResponseAry['bookings'][0]['szBookingContactEmail']="contact@transporteca.com";
$bookingResponseAry['bookings'][0]['szCarrierCompanyRegistration']="DK34689474";
         
?>
<html lang='en'>
<head>
<meta charset='utf-8'>
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
<title>
<?php echo t($t_base_partner_api.'heading/titleHeading');?>
</title>
<meta content='Transporteca' name='author'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<meta content='website' property='og:type'>
<link rel="stylesheet" media="all" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/apidocs.css?t=<?php echo time(); ?>" />
<script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
</head>
    <body>
        <header class="line" style="height:56px;">
          <hgroup class="unit padding-left-0">
              <div class="logo" style="margin-top:8px;">
                  <a href="#"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/transporteca-logo.png" alt="Transporteca"></a>
            </div>
            <h1 style="margin-left:309px;">API Documentation</h1>
          </hgroup> 
        </header>
        <section class="line wrapper" id="api_documentation">
          <aside id="sidebar" class="unit side" style="background-color:#404041;margin-top:20px;">
            <div class="ab">
                <nav class="navigation" id="nav">
                  <ul>
                    <li class="active-section section introduction" data-hash='#introduction' onclick="heighLightSelectedMenu('introduction');">
                        <a href='#introduction'>
                            <?php echo t($t_base_partner_api.'leftMenu/introduction');?>
                        </a>
                    </li>
                    <li class="section api-lib" data-hash='#api-lib' onclick="heighLightSelectedMenu('api-lib');">
                        <a href='#api-lib'>
                            <?php echo t($t_base_partner_api.'leftMenu/api_library');?>
                        </a>
                    </li>
                    <li class="section authentication" data-hash='#authentication' onclick="heighLightSelectedMenu('authentication');">
                        <a href='#authentication'>
                            <?php echo t($t_base_partner_api.'leftMenu/authentication');?>
                        </a>
                    </li>
                    <li class="section parcels" data-hash='#parcels' onclick="heighLightSelectedMenu('parcels');">
                        <a href='#parcels'>
                            <?php echo t($t_base_partner_api.'leftMenu/parcels');?>
                        </a>
                    </li>
                    <li class="section pallets" data-hash='#pallets' onclick="heighLightSelectedMenu('pallets');">
                        <a href='#pallets'>
                            <?php echo t($t_base_partner_api.'leftMenu/pallets');?>
                        </a>
                    </li>
                    <li class="section breakBulk" data-hash='#breakBulk' onclick="heighLightSelectedMenu('breakBulk');">
                        <a href='#breakBulk'>
                            <?php echo t($t_base_partner_api.'leftMenu/breakBulk');?>
                        </a>
                    </li>
                    <li class="section palletsParcels" data-hash='#palletsParcels' onclick="heighLightSelectedMenu('palletsParcels');">
                        <a href='#palletsParcels'>
                            <?php echo t($t_base_partner_api.'leftMenu/palletsParcels');?>
                        </a>
                    </li> 
                     <li class="section standardCargo" data-hash='#standardCargo' onclick="heighLightSelectedMenu('standardCargo');">
                        <a href='#standardCargo'>
                            <?php echo t($t_base_partner_api.'leftMenu/standardCargo');?>
                        </a>
                    </li>
                    <li class="section insuranceRate" data-hash='#insuranceRate' onclick="heighLightSelectedMenu('insuranceRate');">
                        <a href='#insuranceRate'>
                            <?php echo t($t_base_partner_api.'leftMenu/insuranceRate');?>
                        </a>
                    </li>
                    <li class="section validatePrice" data-hash='#validatePrice' onclick="heighLightSelectedMenu('validatePrice');">
                        <a href='#validatePrice'>
                            <?php echo t($t_base_partner_api.'leftMenu/validatePrice');?>
                        </a>
                    </li>
                    <li class="section standardCargoList" data-hash='#standardCargoList' onclick="heighLightSelectedMenu('standardCargoList');">
                        <a href='#standardCargoList'>
                            <?php echo t($t_base_partner_api.'leftMenu/standardCargoList');?>
                        </a>
                    </li>
                    <li class="section getLabels" data-hash='#getLabels' onclick="heighLightSelectedMenu('getLabels');">
                        <a href='#getLabels'>
                            <?php echo t($t_base_partner_api.'leftMenu/getLabels');?>
                        </a>
                    </li>
                    <li class="section confirmDownload" data-hash='#confirmDownload' onclick="heighLightSelectedMenu('confirmDownload');">
                        <a href='#confirmDownload'>
                            <?php echo t($t_base_partner_api.'leftMenu/confirmDownload');?>
                        </a>
                    </li>
                    <li class="section submitNps" data-hash='#submitNps' onclick="heighLightSelectedMenu('submitNps');">
                        <a href='#submitNps'>
                            <?php echo t($t_base_partner_api.'leftMenu/submitNps');?>
                        </a>
                    </li>
                    <li class="section errorResponse" data-hash='#errorResponse' onclick="heighLightSelectedMenu('errorResponse');">
                        <a href='#errorResponse'>
                            <?php echo t($t_base_partner_api.'leftMenu/errorResponse');?>
                        </a>
                    </li>
                    <li class="section requestForQuote" data-hash='#requestForQuote' onclick="heighLightSelectedMenu('requestForQuote');">
                        <a href='#requestForQuote'>
                            <?php echo t($t_base_partner_api.'leftMenu/requestForQuote');?>
                        </a>
                    </li>
                    <li class="section parameters" data-hash='#parameters' onclick="heighLightSelectedMenu('parameters');">
                        <a href='#parameters'>
                            <?php echo t($t_base_partner_api.'leftMenu/parameters');?>
                        </a>
                    </li>
                    <li class="section returnValues" data-hash='#returnValues' onclick="heighLightSelectedMenu('returnValues');">
                        <a href='#returnValues'>
                            <?php echo t($t_base_partner_api.'leftMenu/returnValues');?>
                        </a>
                    </li>
                    <li class="section errors" data-hash='#errors' onclick="heighLightSelectedMenu('errors');">
                        <a href='#errors'>
                            <?php echo t($t_base_partner_api.'leftMenu/errors');?>
                        </a>
                    </li>
                </ul>
                </nav>
              </div>
            </aside>
            
            <div id="main" data-swiftype-index="true">
                <div id="doc-lang">
                    <div id="lang-current">
                        <p class="below0"><?php if(trim($mode) == "php"){ ?>PHP<?php }else{?>JSON<?php }?></p>
                    </div>
                    <div id="lang-nav">
                      <p class="below0">Choose Language</p>
                      <ul>
                          <li>
                            <a id="phpLanageLink" href="/api/docs" <?php if(trim($mode) == "php"){ ?>class="selected"<?php }?>>PHP</a>
                          </li>
                          <li>
                            <a id="jsonLanageLink" href="/api/docs/json/" <?php if(trim($mode) == "json"){ ?>class="selected"<?php }?> onclick="chooseAPIDocLanguage('JSON');">JSON</a>
                          </li>                 
                      </ul>
                    </div>
                  </div>
              <div class="in ab double">
                  <div class="main">
<script type="text/javascript">
            $(window).load(function(){
            // Cache selectors
            var lastId,
                topMenu = $("#nav"),
                topMenuHeight = topMenu.outerHeight()+20,
                // All list items
                menuItems = topMenu.find("a"),
                // Anchors corresponding to menu items
                scrollItems = menuItems.map(function(){
                  var item = $($(this).attr("href"));
                  if (item.length) { return item; }
                });

            // Bind click handler to menu items
            // so we can get a fancy scroll animation

            // Bind to scroll
            $(window).scroll(function(){
               // Get container scroll position
               var fromTop = $(this).scrollTop()+topMenuHeight-200;
               // Get id of current scroll item
               var cur = scrollItems.map(function(){
                 if ($(this).offset().top < fromTop)
                   return this;
               });
               // Get the id of the current element
               cur = cur[cur.length-1];
               
               var id = cur && cur.length ? cur[0].id : "";

               heighLightSelectedMenu(id);
            });
            });
</script>
                      
                    <div class="content">
                        
                    <!--START INTRODUCTION ROW-->
                    <a id="introduction"><h2>&nbsp;</h2></a>
                        <div class="namespace-summary margintop">
                            <h1><?php echo t($t_base_partner_api.'leftMenu/introduction');?></h1>
                            <div class="namespace-summary">
                                <p>
                                    <?php echo t($t_base_partner_api.'description/introduction');?>
                                </p> 
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/priceRequest');?></h3>
                                <p>
                                    <?php echo t($t_base_partner_api.'description/priceRequest');?>
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/bookingRequest');?></h3>
                                <p>
                                    <?php echo t($t_base_partner_api.'description/bookingRequest');?>
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                                <p>
                                   <?php echo t($t_base_partner_api.'description/apiEndPoint');?> 
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/requestMode');?></h3>
                                <p>
                                    <?php echo t($t_base_partner_api.'description/requestMode').": ".t($t_base_partner_api.'description/requestMode_1');?> 
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/responseCode');?></h3>
                                    <p>
                                        <?php echo t($t_base_partner_api.'description/responseCode');?> 
                                    </p>
                                <ul>
                                    <li>
                                        <strong>Code - </strong>200
                                        <div>
                                            <strong>status - </strong>OK
                                        </div>
                                    </li>
                                    <li>
                                        <strong>Code - </strong>400
                                        <div>
                                            <strong>status - </strong>Error
                                        </div>
                                    </li>
                                    <li>
                                        <strong>Code - </strong>401
                                        <div>
                                            <strong>status - </strong><?php echo t($t_base_partner_api.'description/responseCode401');?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                <!--END INTRODUCTION ROW-->

                <a id="api-lib"><h2>&nbsp;</h2></a>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/api_library');?></h1>
                    <p>
                        <?php echo t($t_base_partner_api.'description/api_library_description')." ";?><a href="<?php echo __BASE_URL__.'/api/download/'; ?>"><?php echo  t($t_base_partner_api.'description/click_here'); ?></a>
                    </p>
                </div> 
                <!--START AUTHENTICATION ROW-->
                <a id="authentication"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/authentication');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/authenticationDescription');?>
                        </p>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <ul>
                            <li>
                                <?php if(trim($mode) == "php"){ ?>
                                    \Transporteca\Transporteca::setApiKey(<?php echo t($t_base_partner_api.'requestAttribute/yourApi');?>,<?php echo t($t_base_partner_api.'requestAttribute/mode');?>); 
                                <?php }else{ ?>
                                    \Transporteca\Transporteca::setApiKey("<?php echo t($t_base_partner_api.'requestAttribute/yourApi');?>",<?php echo t($t_base_partner_api.'requestAttribute/mode');?>); 
                                <?php } ?> 
                            </li>
                        </ul>
                    </div>
                                    

                <!--END AUTHENTICATION ROW-->
                
                <!--START PARCEL ROW-->
                
                <a id="parcels"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/parcels');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/parcelDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/parcelExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong> 
<pre>
<?php
$requestAry['szCargoDescription']='';
$requestAry['szShipperCompanyName']='';
$requestAry['szShipperFirstName']='';
$requestAry['szShipperLastName']='';
$requestAry['iShipperCountryDialCode']='';
$requestAry['iShipperPhoneNumber']='';
$requestAry['szShipperEmail']='';
$requestAry['szShipperAddress']='';
$requestAry['szShipperPostcode']=440000;
$requestAry['szShipperCity']="Mumbai";
$requestAry['szShipperCountry']="IN";
$requestAry['szConsigneeCompanyName']='';
$requestAry['szConsigneeFirstName']='';
$requestAry['szConsigneeLastName']='';
$requestAry['iConsigneeCountryDialCode']='';
$requestAry['iConsigneePhoneNumber']='';
$requestAry['szConsigneeEmail']='';
$requestAry['szConsigneeAddress']='';
$requestAry['szConsigneePostcode']=3480;
$requestAry['szConsigneeCity']="Fredensborg";
$requestAry['szConsigneeCountry']="DK";
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.5;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12;
$requestAry['cargo'][1]['iQuantity'] = 2;
unset($requestAry['cargo'][0]['szPalletType']);
unset($requestAry['cargo'][1]['szPalletType']);
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>  
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<?php
$responseAry['service'][1]['szServiceID'] = "0601TTP91921";
$responseAry['service'][1]['dtTransportFinishDay'] = 07;
$responseAry['service'][1]['dtTransportFinishMonth'] = 01;
$responseAry['service'][1]['dtTransportFinishYear'] = 2016;
$responseAry['service'][1]['szPackingType'] = t($t_base_partner_api.'responseAttribute/palletCartons');
$responseAry['service'][1]['szCurrency'] = "USD";
$responseAry['service'][1]['fBookingPrice'] = 2044.00;
$responseAry['service'][1]['fVatAmount'] = 511.00;
$responseAry['service'][1]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][1]['fInsurancePrice'] = 69.00;
$responseAry['service'][1]['szTransportMode'] = "COURIER";
$responseAry['service'][1]['szForwarderAlias'] = "SM";

$responseAry['service'][2]['szServiceID'] = "0601TTP96384";
$responseAry['service'][2]['dtTransportFinishDay'] = 9;
$responseAry['service'][2]['dtTransportFinishMonth'] = 01;
$responseAry['service'][2]['dtTransportFinishYear'] = 2016;
$responseAry['service'][2]['szPackingType'] = t($t_base_partner_api.'responseAttribute/palletCartons');
$responseAry['service'][2]['szCurrency'] = "USD";
$responseAry['service'][2]['fBookingPrice'] = 1909.00;
$responseAry['service'][2]['fVatAmount'] = 477.25;
$responseAry['service'][2]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][2]['fInsurancePrice'] = 66.50;
$responseAry['service'][2]['szTransportMode'] = "COURIER";
$responseAry['service'][2]['szForwarderAlias'] = "SM";

?>
<code class="clear json">
<?php
$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<?php
$requestAry['szRequest']='booking';
$requestAry['szCargoDescription']="Sample Description";
$requestAry['fCargoValue']=10000;
$requestAry['szCargoCurrency']="USD";
$requestAry['szShipperCompanyName']="ABC Seller Company";
$requestAry['szShipperFirstName']="Johan";
$requestAry['szShipperLastName']="Miller";
$requestAry['iShipperCountryDialCode']=91;
$requestAry['iShipperPhoneNumber']=1234567890;
$requestAry['szShipperEmail']="shipper@email.com";
$requestAry['szShipperAddress']="Highstreet 19";
$requestAry['szShipperPostcode']=440000;
$requestAry['szShipperCity']="Mumbai";
$requestAry['szShipperCountry']="IN";
$requestAry['szConsigneeCompanyName']="XYZ Buyer Company";
$requestAry['szConsigneeFirstName']="Albert";
$requestAry['szConsigneeLastName']="Metthew";
$requestAry['iConsigneeCountryDialCode']=45;
$requestAry['iConsigneePhoneNumber']=1231231234;
$requestAry['szConsigneeEmail']="consignee@email.com";
$requestAry['szConsigneeAddress']="Landevejen 15, baghuset";
$requestAry['szConsigneePostcode']=3480;
$requestAry['szConsigneeCity']="Fredensborg";
$requestAry['szConsigneeCountry']="DK";
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.5;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['szServiceID']="0501TTP93981";
$requestAry['token']="TTPE1KV2JTD8E9RS9BLO"; 
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                    </div>
                <!--END PARCEL ROW-->
                
                
                <!--START PALLETS ROW-->
                <a id="pallets"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/pallets');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/palletsDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/palletExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=21;
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['szPalletType'] = "Euro";
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = '';
$requestAry['cargo'][0]['iHeight'] = 160;
$requestAry['cargo'][0]['iWeight'] = 180;
$requestAry['cargo'][0]['iQuantity'] = 2;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = '';
$requestAry['cargo'][1]['iWidth'] = '';
$requestAry['cargo'][1]['iHeight'] = 160;
$requestAry['cargo'][1]['iWeight'] = 100;
$requestAry['cargo'][1]['iQuantity'] = 1;
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']="DKK";
$requestAry['szShipperPostcode']=199;
$requestAry['szShipperCity']="Rome";
$requestAry['szShipperCountry']="IT";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['bInsuranceRequired']="No";
$requestAry['szCurrency']="DKK";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$responseAry['token']="TTPNTFS0TI2NCGXIYJRA";
$responseAry['service'][0]['szServiceID'] = "0601TTP91819";
$responseAry['service'][0]['dtTransportFinishDay'] = 03;
$responseAry['service'][0]['dtTransportFinishMonth'] = 02;
$responseAry['service'][0]['dtTransportFinishYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/euroPallet');
$responseAry['service'][0]['szCurrency'] = "DKK";
$responseAry['service'][0]['fBookingPrice'] = 2246.00;
$responseAry['service'][0]['fVatAmount'] = 561.50;
$responseAry['service'][0]['bInsuranceAvailable'] = "No, cargo value required";
$responseAry['service'][0]['fInsurancePrice'] = '';
$responseAry['service'][0]['szTransportMode'] = "COURIER";
$responseAry['service'][0]['szForwarderAlias'] = "SM";
$responseAry['service'][0]['szServiceDescription'] = "Transportation from shippers door in Mumbai to consignees door in Copenhagen and customs clearance in India and Denmark. This is also known as EXW terms.";
unset($responseAry['service'][1]);
unset($responseAry['service'][2]);
$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=21;
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['szPalletType'] = "Euro";
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = '';
$requestAry['cargo'][0]['iHeight'] = 160;
$requestAry['cargo'][0]['iWeight'] = 180;
$requestAry['cargo'][0]['iQuantity'] = 2;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = '';
$requestAry['cargo'][1]['iWidth'] = '';
$requestAry['cargo'][1]['iHeight'] = 160;
$requestAry['cargo'][1]['iWeight'] = 100;
$requestAry['cargo'][1]['iQuantity'] = 1;
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']="DKK";
$requestAry['szServiceID']="0601TTP91819";
$requestAry['token']="TTPNTFS0TI2NCGXIYJRA"; 
$requestAry['bInsuranceRequired']="No";
$requestAry['szCurrency']="DKK";


$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$bookingResponseAry['token']="TTPNTFS0TI2NCGXIYJRA";
$bookingResponseAry['bookings'][0]['dtTransportFinishDay']=03;
$bookingResponseAry['bookings'][0]['dtTransportFinishMonth']=02;
$bookingResponseAry['bookings'][0]['dtTransportFinishYear']=2016;
$bookingResponseAry['bookings'][0]['szBookingRef']="3901MCL089";
$bookingResponseAry['bookings'][0]['szPackingType']=t($t_base_partner_api.'responseAttribute/euroPallet');
$bookingResponseAry['bookings'][0]['szServiceDescription']=t($t_base_partner_api.'responseAttribute/szServiceDescriptionPallets');
$bookingResponseAry['bookings'][0]['szTransportMode']="Road transport";
$bookingResponseAry['bookings'][0]['szCurrency']="DKK";
$bookingResponseAry['bookings'][0]['fBookingPrice']=2246.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=561.50;
$bookingResponseAry['bookings'][0]['fInsurancePrice']='';
$bookingResponseAry['bookings'][0]['bCargoInsurance']=t($t_base_partner_api.'responseAttribute/no');
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                    </div>
                
                <!--END PALLETS DIV-->
                
                <!--START BREAK BULK DIV-->
                <a id="breakBulk"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/breakBulk');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/breakBulkDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/breakBulkExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=03;
$requestAry['szShipperCompanyName']='';
$requestAry['szShipperFirstName']='';
$requestAry['szShipperLastName']='';
$requestAry['iShipperCountryDialCode']='';
$requestAry['iShipperPhoneNumber']='';
$requestAry['szShipperEmail']='';
$requestAry['szShipperAddress']='';
$requestAry['szShipperPostcode']='';
$requestAry['szShipperCity']="Hong Kong";
$requestAry['szShipperCountry']="HK";
$requestAry['szConsigneeCompanyName']='';
$requestAry['szConsigneeFirstName']='';
$requestAry['szConsigneeLastName']='';
$requestAry['iConsigneeCountryDialCode']='';
$requestAry['iConsigneePhoneNumber']='';
$requestAry['szConsigneeEmail']='';
$requestAry['szConsigneeAddress']='';
$requestAry['szConsigneePostcode']='';
$requestAry['szConsigneeCity']="Fredensborg";
$requestAry['szConsigneeCountry']="DK";
$requestAry['cargo']=array();
$requestAry['cargo'][0]['szShipmentType'] = "BREAK_BULK";
$requestAry['cargo'][0]['fTotalVolume'] = 5.2;
$requestAry['cargo'][0]['fTotalWeight'] = 860;
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cbm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']="FOB";
$requestAry['fCargoValue'] = 8600;
$requestAry['szCargoCurrency']='USD';
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['bInsuranceRequired']="Yes";
$requestAry['szCurrency']='';
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$responseAry['token']="TTPAIU4MFISKRT4IDMUB";
$responseAry['service'][0]['szServiceID']="0924LTP91800";
$responseAry['service'][0]['dtTransportFinishDay'] = 12;
$responseAry['service'][0]['dtTransportFinishMonth'] = 02;
$responseAry['service'][0]['dtTransportFinishYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/palletCartons');
$responseAry['service'][0]['szCurrency'] = "USD";
$responseAry['service'][0]['fBookingPrice'] = 1286.00;
$responseAry['service'][0]['fVatAmount'] = 0;
$responseAry['service'][0]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][0]['fInsurancePrice'] = 44.75;
$responseAry['service'][0]['szTransportMode'] = "LCL";
$responseAry['service'][0]['szForwarderAlias'] = "AB";

$responseAry['service'][1]['szServiceID']="1901LLP91629";
$responseAry['service'][1]['dtTransportFinishDay'] = 19;
$responseAry['service'][1]['dtTransportFinishMonth'] = 02;
$responseAry['service'][1]['dtTransportFinishYear'] = 2016;
$responseAry['service'][1]['szPackingType'] = t($t_base_partner_api.'responseAttribute/palletCartons');
$responseAry['service'][1]['szCurrency'] = "USD";
$responseAry['service'][1]['fBookingPrice'] = 1220.00;
$responseAry['service'][1]['fVatAmount'] = 0;
$responseAry['service'][1]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][1]['fInsurancePrice'] = 44.20;
$responseAry['service'][1]['szTransportMode'] = "LCL";
$responseAry['service'][1]['szForwarderAlias'] = "AB";

unset($responseAry['service'][2]);
$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=03;
$requestAry['dtMonth']=01;
$requestAry['szCargoDescription']="Sample Description";
$requestAry['szCommodity'] = 2;
$requestAry['szShipperCompanyName']="HK Seller Company";
$requestAry['szShipperFirstName']="Johan";
$requestAry['szShipperLastName']="Miller";
$requestAry['iShipperCountryDialCode']=856;
$requestAry['iShipperPhoneNumber']=1234567890;
$requestAry['szShipperEmail']="shipper@email.com";
$requestAry['szShipperAddress']="31 city center mall";
$requestAry['szShipperPostcode']=856;
$requestAry['szShipperCity']="Hong Kong";
$requestAry['szShipperCountry']="HK";
$requestAry['szConsigneeCompanyName']="XYZ Buyer Company";
$requestAry['szConsigneeFirstName']="Albert";
$requestAry['szConsigneeLastName']="Metthew";
$requestAry['iConsigneeCountryDialCode']=45;
$requestAry['iConsigneePhoneNumber']=1231231234;
$requestAry['szConsigneeEmail']="consignee@email.com";
$requestAry['szConsigneeAddress']="Landevejen 15, baghuset";
$requestAry['szConsigneePostcode']=3480;
$requestAry['szConsigneeCity']="Fredensborg";
$requestAry['szConsigneeCountry']="DK";
$requestAry['cargo']=array();
$requestAry['cargo'][0]['szShipmentType'] = "BREAK_BULK";
$requestAry['cargo'][0]['fTotalVolume'] = 5.2;
$requestAry['cargo'][0]['fTotalWeight'] = 860;
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cbm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']="FOB";
$requestAry['fCargoValue'] = 8600;
$requestAry['szCargoCurrency']='USD';
$requestAry['szServiceID']="0924LTP91800";
$requestAry['token']="TTPAIU4MFISKRT4IDMUB";
$requestAry['bInsuranceRequired']="Yes";
$requestAry['szCurrency']='';
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$bookingResponseAry['token']="TTPAIU4MFISKRT4IDMUB";
$bookingResponseAry['bookings'][0]['dtTransportFinishDay']=12;
$bookingResponseAry['bookings'][0]['dtTransportFinishMonth']=02;
$bookingResponseAry['bookings'][0]['dtTransportFinishYear']=2016;
$bookingResponseAry['bookings'][0]['szBookingRef']="0901MML009";
$bookingResponseAry['bookings'][0]['szServiceDescription']=t($t_base_partner_api.'responseAttribute/szServiceDescriptionBreakBulk');
$bookingResponseAry['bookings'][0]['szPackingType']=t($t_base_partner_api.'responseAttribute/palletCartons');
$bookingResponseAry['bookings'][0]['szTransportMode']="Sea freight";
$bookingResponseAry['bookings'][0]['szCurrency']="USD";
$bookingResponseAry['bookings'][0]['fBookingPrice']=1286.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=0;
$bookingResponseAry['bookings'][0]['fInsurancePrice']=45.75;
$bookingResponseAry['bookings'][0]['bCargoInsurance']=t($t_base_partner_api.'responseAttribute/yes');
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                    </div>
                
                <!--END BREAK BULK DIV-->
                
                
                <!--START PALLETS OR PARCEL DIV-->
                <a id="palletsParcels"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/palletsParcels');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/parcelPalletsDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/parcelPalletsExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
unset($requestAry['cargo'][0]['szDimensionMeasure']);
unset($requestAry['cargo'][0]['szWeightMeasure']);
$requestAry['szRequest']='price';
$requestAry['dtDay']=8;
$requestAry['szCargoCurrency']="USD";
$requestAry['szShipperCompanyName']="ABC Seller Company";
$requestAry['szShipperFirstName']="Johan";
$requestAry['szShipperLastName']="Miller";
$requestAry['iShipperCountryDialCode']=91;
$requestAry['iShipperPhoneNumber']=1234567890;
$requestAry['szShipperEmail']="shipper@email.com";
$requestAry['szShipperAddress']="Highstreet 19";
$requestAry['szShipperPostcode']=440000;
$requestAry['szShipperCity']="Mumbai";
$requestAry['szShipperCountry']="IN";
$requestAry['szConsigneeCompanyName']="XYZ Buyer Company";
$requestAry['szConsigneeFirstName']="Albert";
$requestAry['szConsigneeLastName']="Metthew";
$requestAry['iConsigneeCountryDialCode']=45;
$requestAry['iConsigneePhoneNumber']=1231231234;
$requestAry['szConsigneeEmail']="consignee@email.com";
$requestAry['szConsigneeAddress']="Landevejen 15, baghuset";
$requestAry['szConsigneePostcode']=3480;
$requestAry['szConsigneeCity']="Fredensborg";
$requestAry['szConsigneeCountry']="DK";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][0]['szPalletType'] = '';
$requestAry['cargo'][0]['iLength'] = 65;
$requestAry['cargo'][0]['iWidth'] = 50;
$requestAry['cargo'][0]['iHeight'] = 50;
$requestAry['cargo'][0]['iWeight'] = 22;
$requestAry['cargo'][0]['iQuantity'] = 2;
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['cargo'][1]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][1]['szPalletType'] = '';
$requestAry['cargo'][1]['iLength'] = 40;
$requestAry['cargo'][1]['iWidth'] = 45;
$requestAry['cargo'][1]['iHeight'] = 70;
$requestAry['cargo'][1]['iWeight'] = 18;
$requestAry['cargo'][1]['iQuantity'] = 3;
$requestAry['cargo'][1]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][1]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['cargo'][2]['szShipmentType'] = "PALLET";
$requestAry['cargo'][2]['szPalletType'] = "Euro";
$requestAry['cargo'][2]['iLength'] = 120;
$requestAry['cargo'][2]['iWidth'] = 80;
$requestAry['cargo'][2]['iHeight'] = 180;
$requestAry['cargo'][2]['iWeight'] = 120;
$requestAry['cargo'][2]['iQuantity'] = 1;
$requestAry['cargo'][2]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][2]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['fCargoValue']=1558.96;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['bInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
$requestAry['szServiceTerm']='';
unset($requestAry['cargo'][0]['szPalletType']); 
unset($requestAry['cargo'][1]['szPalletType']); 
unset($requestAry['cargo'][0]['fTotalVolume']); 
unset($requestAry['cargo'][0]['fTotalWeight']); 
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$responseAry['status']= "OK";
$responseAry['token']= "TTPNKJNG5GOFPOGL6SHR";
$responseAry['service'][0]['szServiceID'] = "0601TTP92019";
$responseAry['service'][0]['dtTransportFinishDay'] = 12;
$responseAry['service'][0]['dtTransportFinishMonth'] = 01;
$responseAry['service'][0]['dtTransportFinishYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/onlyCatons');
$responseAry['service'][0]['szCurrency'] = "USD";
$responseAry['service'][0]['fBookingPrice'] = 214.00;
$responseAry['service'][0]['fVatAmount'] = 53.5;
$responseAry['service'][0]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][0]['fInsurancePrice'] = 13.00;
$responseAry['service'][0]['szTransportMode'] = "COURIER";
$responseAry['service'][0]['szForwarderAlias'] = "SM";

$responseAry['service'][1]['szServiceID'] = "0601TTP99319";
$responseAry['service'][1]['dtTransportFinishDay'] = 16;
$responseAry['service'][1]['dtTransportFinishMonth'] = 02;
$responseAry['service'][1]['dtTransportFinishYear'] = 2016;
$responseAry['service'][1]['szPackingType'] = t($t_base_partner_api.'responseAttribute/euroPallet');
$responseAry['service'][1]['szCurrency'] = "USD";
$responseAry['service'][1]['fBookingPrice'] = 147.00;
$responseAry['service'][1]['fVatAmount'] = 36.75;
$responseAry['service'][1]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][1]['fInsurancePrice'] = 12.15;
$responseAry['service'][1]['szTransportMode'] = "COURIER";
$responseAry['service'][1]['szForwarderAlias'] = "SM";

$responseAry['service'][2]['szServiceID'] = "0601TTP97729";
$responseAry['service'][2]['dtTransportFinishDay'] = 14;
$responseAry['service'][2]['dtTransportFinishMonth'] = 01;
$responseAry['service'][2]['dtTransportFinishYear'] = 2016;
$responseAry['service'][2]['szPackingType'] = t($t_base_partner_api.'responseAttribute/onlyCatons');
$responseAry['service'][2]['szCurrency'] = "USD";
$responseAry['service'][2]['fBookingPrice'] = 167.00;
$responseAry['service'][2]['fVatAmount'] = 41.75;
$responseAry['service'][2]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][2]['fInsurancePrice'] = 12.38;
$responseAry['service'][2]['szTransportMode'] = "COURIER";
$responseAry['service'][2]['szForwarderAlias'] = "SM";

$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=8;
$requestAry['dtMonth']=01;
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][0]['szPalletType'] = '';
$requestAry['cargo'][0]['iLength'] = 65;
$requestAry['cargo'][0]['iWidth'] = 50;
$requestAry['cargo'][0]['iHeight'] = 50;
$requestAry['cargo'][0]['iWeight'] = 22;
$requestAry['cargo'][0]['iQuantity'] = 2;
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['cargo'][1]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][1]['iLength'] = 40;
$requestAry['cargo'][1]['iWidth'] = 45;
$requestAry['cargo'][1]['iHeight'] = 70;
$requestAry['cargo'][1]['iWeight'] = 18;
$requestAry['cargo'][1]['iQuantity'] = 3;
$requestAry['cargo'][1]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][1]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['cargo'][2]['szShipmentType'] = "PALLET";
$requestAry['cargo'][2]['szPalletType'] = "Euro";
$requestAry['cargo'][2]['iLength'] = 120;
$requestAry['cargo'][2]['iWidth'] = 80;
$requestAry['cargo'][2]['iHeight'] = 180;
$requestAry['cargo'][2]['iWeight'] = 120;
$requestAry['cargo'][2]['iQuantity'] = 1;
$requestAry['cargo'][2]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][2]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['fCargoValue']=1558.96;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceID']="0601TTP92019";
$requestAry['token']="TTPNKJNG5GOFPOGL6SHR";
$requestAry['bInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
unset($requestAry['cargo'][0]['szPalletType']);
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
    </strong>
                        
<pre>
<code class="clear json">
<?php
$bookingResponseAry['token']="TTPNKJNG5GOFPOGL6SHR";
$bookingResponseAry['bookings'][0]['dtTransportFinishDay']=12;
$bookingResponseAry['bookings'][0]['dtTransportFinishMonth']=01;
$bookingResponseAry['bookings'][0]['dtTransportFinishYear']=2016;
$bookingResponseAry['bookings'][0]['szBookingRef']="5731ZNL080";
$bookingResponseAry['bookings'][0]['szServiceDescription']=t($t_base_partner_api.'responseAttribute/szServiceDescriptionPalletsParcels');
$bookingResponseAry['bookings'][0]['szPackingType']=t($t_base_partner_api.'responseAttribute/onlyCatons');
$bookingResponseAry['bookings'][0]['szTransportMode']="Courier";
$bookingResponseAry['bookings'][0]['szCurrency']="USD";
$bookingResponseAry['bookings'][0]['fBookingPrice']=214.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=53.5;
$bookingResponseAry['bookings'][0]['fInsurancePrice']=13;
$bookingResponseAry['bookings'][0]['bCargoInsurance']=t($t_base_partner_api.'responseAttribute/yes');
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                    </div>
                
                <!--END PALLETS OR PARCEL DIV-->
                <!--START Insurance Api DIV-->
                <a id="standardCargo"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/standardCargo');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/standardCargoDescription');?>
                        </p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
<?php
unset($requestAry['cargo'][0]['szDimensionMeasure']);
unset($requestAry['cargo'][0]['szWeightMeasure']);
unset($requestAry['cargo'][1]['szDimensionMeasure']);
unset($requestAry['cargo'][1]['szWeightMeasure']);
unset($requestAry['cargo'][2]['szDimensionMeasure']);
unset($requestAry['cargo'][2]['szWeightMeasure']);
unset($requestAry['cargo'][0]['iLength']);
unset($requestAry['cargo'][0]['iWidth']);
unset($requestAry['cargo'][0]['iHeight']);
unset($requestAry['cargo'][1]['iLength']);
unset($requestAry['cargo'][1]['iWidth']);
unset($requestAry['cargo'][1]['iHeight']);
unset($requestAry['cargo'][2]['szPalletType']);
unset($requestAry['cargo'][2]['iLength']);
unset($requestAry['cargo'][2]['iWidth']);
unset($requestAry['cargo'][2]['iHeight']);
unset($requestAry['cargo'][0]['iWeight']);
unset($requestAry['cargo'][1]['iWeight']);
unset($requestAry['cargo'][2]['iWeight']);
$requestAry['szRequest']='price';
$requestAry['dtDay']=8;
$requestAry['szCargoCurrency']="USD";
$requestAry['szShipperCompanyName']="ABC Seller Company";
$requestAry['szShipperFirstName']="Johan";
$requestAry['szShipperLastName']="Miller";
$requestAry['iShipperCountryDialCode']=91;
$requestAry['iShipperPhoneNumber']=1234567890;
$requestAry['szShipperEmail']="shipper@email.com";
$requestAry['szShipperAddress']="Highstreet 19";
$requestAry['szShipperPostcode']=440000;
$requestAry['szShipperCity']="Mumbai";
$requestAry['szShipperCountry']="IN";
$requestAry['szConsigneeCompanyName']="XYZ Buyer Company";
$requestAry['szConsigneeFirstName']="Albert";
$requestAry['szConsigneeLastName']="Metthew";
$requestAry['iConsigneeCountryDialCode']=45;
$requestAry['iConsigneePhoneNumber']=1231231234;
$requestAry['szConsigneeEmail']="consignee@email.com";
$requestAry['szConsigneeAddress']="Landevejen 15, baghuset";
$requestAry['szConsigneePostcode']=3480;
$requestAry['szConsigneeCity']="Fredensborg";
$requestAry['szConsigneeCountry']="DK";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['cargo'][0]['szShipmentType'] = "STANDARD_CARGO";
$requestAry['cargo'][0]['iStandardCargoId'] = 9;
$requestAry['cargo'][0]['iQuantity'] = 2;
$requestAry['cargo'][1]['szShipmentType'] = "STANDARD_CARGO";
$requestAry['cargo'][1]['iStandardCargoId'] = 17;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['cargo'][2]['szShipmentType'] = "STANDARD_CARGO";
$requestAry['cargo'][2]['iStandardCargoId'] = 18;
$requestAry['cargo'][2]['iQuantity'] = 2;
$requestAry['fCargoValue']=1558.96;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['bInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
$requestAry['szServiceTerm']='';
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>
        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        <pre>
<code class="clear json">
<?php
$insuranceResponseAry['status']="OK";
$insuranceResponseAry['token']="TAJE50OGIRHVRKKHHVZH";
$insuranceResponseAry['notification']='Transporteca will send the transportation quotation directly to Ajay Jha within one working day';
$insuranceResponseAry['iResponseDay']='0';
//$insuranceResponseAry['insuranceRate'][0]['fInsurancePriceUSD']="1428";
//$insuranceResponseAry['insuranceRate'][0]['fInsuranceMinPriceUSD']="14.28";

$insuranceResponse = getResponseAry($insuranceResponseAry,$mode);
echo $insuranceResponse;
?>
</code>
</pre>
</div>    
<!--END Insurance Api DIV-->
                <!--START Insurance Api DIV-->
                <a id="insuranceRate"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/insuranceRate');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/insuranceRateDescription');?>
                        </p>
                        <h3>Insurance Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
<?php
$requestInsuranceAry['szRequest']='getInsuranceValue';
$requestInsuranceAry['szOriginCountry']='IN';
$requestInsuranceAry['szDestinationCountry']='DK';
$requestInsuranceAry['szTransportMode']='COURIER';
$requestInsuranceAry['szCargoType']='GENERAL CARGO';
//$requestInsuranceAry['szGoodsInsuranceCurrency']='DKK';
$requestInsuranceAry['szForwarderCountry']='DK';
$requestInsuranceAry['szCustomerType']='PRIVATE';
$requestInsuranceAry['szPriceToken']='';
$requestInsuranceAry['szServiceID']='';
?>
<code class="clear json">
<?php
$szResponseInsuranceString = getRequestAry($requestInsuranceAry,$mode);
echo $szPriceCallString1;
echo $szInsuranceCallString2;
echo $szResponseInsuranceString;
?>
</code>
</pre>
        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        <pre>
<code class="clear json">
<?php
$insuranceResponseAry['status']="OK";
$insuranceResponseAry['token']="TAJE50OGIRHVRKKHHVZH";
$insuranceResponseAry['insuranceRate'][0]['fInsuranceUptoPrice']="10000.00";
$insuranceResponseAry['insuranceRate'][0]['szInsuranceCurrency']="DKK";
$insuranceResponseAry['insuranceRate'][0]['fInsurancePercentageRate']="5%";
$insuranceResponseAry['insuranceRate'][0]['fInsuranceMinPrice']="100";
$insuranceResponseAry['insuranceRate'][0]['fInsuranceCurrencyExchangeRate']="0.1428";
//$insuranceResponseAry['insuranceRate'][0]['fInsurancePriceUSD']="1428";
//$insuranceResponseAry['insuranceRate'][0]['fInsuranceMinPriceUSD']="14.28";

$insuranceResponse = getResponseAry($insuranceResponseAry,$mode);
echo $insuranceResponse;
?>
</code>
</pre>
</div>    
<!--END Insurance Api DIV-->
                
<!--START Validate price Api DIV-->
<a id="validatePrice"><h2>&nbsp;</h2></a>
<div id='' class="namespace-summary margintop">
    <h1><?php echo t($t_base_partner_api.'leftMenu/validatePrice');?></h1>
    <p>
        <?php echo t($t_base_partner_api.'description/validatePriceDescription');?>
    </p>
    <h3>Validate price API Call</h3>
    <strong>End point: </strong><i>/api/validatePrice</i><br/>
    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
    </strong>
    <pre>
        <?php 
         $requestAry['szRequest']='validatePrice';
         $szValidatePriceRequestString = getRequestAry($requestAry,$mode);
        ?>
        <code class="clear json"><?php echo $szPriceCallString1."".$szValidatePriceCallString2."".$szValidatePriceRequestString; ?>
        </code>
    </pre>
    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
    </strong>
    <pre>
        <?php
                $validatePriceResponseAry = array();
                $validatePriceResponseAry['status']="OK";
                $validatePriceResponseAry['token']="TAJE50OGIRHVRKKHHVZH"; 

                $insuranceResponse = getResponseAry($validatePriceResponseAry,$mode); 
                $insuranceResponse = rtrim($insuranceResponse,",");
            ?>
        <code class="clear json"><?php echo $insuranceResponse; ?></code>
    </pre>
    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleErrorResponse');?>
    </strong>
    <pre>
    <?php
            $validatePriceResponseAry = array();
            $validatePriceResponseAry['status']="Error";
            $validatePriceResponseAry['token']="TAJE50OGIRHVRKKHHVZH"; 
            $validatePriceResponseAry['errors'][0]['code'] = 'E1170';
            $validatePriceResponseAry['errors'][0]['description'] = 'Transportation price expired';

            $insuranceResponse = getResponseAry($validatePriceResponseAry,$mode); 
        ?>
        <code class="clear json"><?php echo $insuranceResponse;?></code>
    </pre>
</div>    
<!--END Validate Price Api DIV-->
<!--START Standard Cargo Api DIV-->
                <a id="standardCargoList"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/standardCargoList');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/standardCargoListDescription');?>
                        </p>
                        <h3>Insurance Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
<?php
$requestStandardCargoAry['szRequest']='getStandardCargo';
$requestStandardCargoAry['iDataSet']='1';
$requestStandardCargoAry['iLanguage']='1';
?>
<code class="clear json">
<?php
$szStandardCargoString = getRequestAry($requestStandardCargoAry,$mode);
echo $szPriceCallString1;
echo $szStandardCargoCallString2;
echo $szStandardCargoString;
?>
</code>
</pre>
        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        <pre>
<code class="clear json">
<?php
$standardResponseAry['status']="OK";
$standardResponseAry['token']="TAJE50OGIRHVRKKHHVZH";
$standardResponseAry['iResponseDay']="1";
$standardResponseAry['szCategoryID']="1";
$standardResponseAry['szCategory']="Bestseller";
$standardResponseAry['szCargoList'][0]['szStandardCagoID']="9";
$standardResponseAry['szCargoList'][0]['szType']="AJ Floor Lamp";
$standardResponseAry['szCargoList'][1]['szStandardCagoID']="17";
$standardResponseAry['szCargoList'][1]['szType']="AJ Wall Lamp";
//$insuranceResponseAry['insuranceRate'][0]['fInsurancePriceUSD']="1428";
//$insuranceResponseAry['insuranceRate'][0]['fInsuranceMinPriceUSD']="14.28";

$standardCargoResponse = getResponseAry($standardResponseAry,$mode);
echo $standardCargoResponse;
?>
</code>
</pre>
</div>    
<!--END Standard Cargo Api DIV-->

<!--START Get Label Api DIV-->
<a id="getLabels"><h2>&nbsp;</h2></a>
    <div id='' class="namespace-summary margintop">
        <h1><?php echo t($t_base_partner_api.'leftMenu/getLabels');?></h1>
        <p>
            <?php echo t($t_base_partner_api.'description/getLabelDescription');?>
        </p> 
        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
        </strong>
        <pre>
<?php
$getLabelRequestAry = array();
$getLabelRequestAry['szRequest']='getLabels';
$getLabelRequestAry['szBookingKey']='TBZBSLJIEJMC';
$getLabelRequestAry['iLanguage']='1';
$szGetLabelString = getRequestAry($getLabelRequestAry,$mode);
?>
        <code class="clear json"> <?php  echo $szPriceCallString1." ".$szGetLabelCallString2." ".$szGetLabelString; ?></code>
    </pre>
    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
    </strong>
    
<?php
$labelResponseAry['status']="OK";
$labelResponseAry['token']="TAJE50OGIRHVRKKHHVZH";
$labelResponseAry['label'][0]['szShipperCompanyName'] = "ABC Seller Company"; 
$labelResponseAry['label'][0]['szShipperFirstName'] = "Johan"; 
$labelResponseAry['label'][0]['szShipperLastName'] = "Miller"; 
$labelResponseAry['label'][0]['szShipperEmail'] = "shipper@email.com"; 
$labelResponseAry['label'][0]['iShipperDialCode'] = "45"; 
$labelResponseAry['label'][0]['szShipperPhone'] = "876768879"; 
$labelResponseAry['label'][0]['iCollection'] = "1"; 
$labelResponseAry['label'][0]['dtCollectionDay'] = date('d'); 
$labelResponseAry['label'][0]['dtCollectionMonth'] = date('m');
$labelResponseAry['label'][0]['dtCollectionYear'] = date('Y'); 
$labelResponseAry['label'][0]['szText'] = ""; 


$getLabelResponse = getResponseAry($labelResponseAry,$mode); 
    ?>
    <pre>
        <code class="clear json"><?php echo $getLabelResponse; ?></code>
    </pre>
</div>    
<!--END Get Label Api DIV--> 


<!--START Confirm Label downloadApi DIV-->
<a id="confirmDownload"><h2>&nbsp;</h2></a>
    <div id='' class="namespace-summary margintop">
        <h1><?php echo t($t_base_partner_api.'leftMenu/confirmDownload');?></h1>
        <p>
            <?php echo t($t_base_partner_api.'description/confirmDownloadDescription');?>
        </p> 
        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
        </strong>
        
<?php
$getLabelRequestAry = array();
$getLabelRequestAry['szRequest']='confirmDownload';
$getLabelRequestAry['szBookingKey']='TBZBSLJIEJMC'; 
$getLabelRequestAry['szShipperCompanyName'] = 'ABC Seller Company';
$getLabelRequestAry['szShipperFirstName'] = 'Johan';
$getLabelRequestAry['szShipperLastName'] = 'Miller';
$getLabelRequestAry['szShipperEmail'] = 'shipper@email.com';
$getLabelRequestAry['iShipperDialCode'] = '45';
$getLabelRequestAry['szShipperPhone'] = '876768879';
$getLabelRequestAry['iLanguage']='1';
$getLabelRequestAry['dtPickUpDay'] = date('d'); 
$getLabelRequestAry['dtPickUpMonth'] = date('m');
$getLabelRequestAry['dtPickUpYear'] = date('Y'); 
$szGetLabelString = getRequestAry($getLabelRequestAry,$mode);
?>
    <pre>
        <code class="clear json"> <?php  echo $szPriceCallString1." ".$szConfirmDownloadCallString2." ".$szGetLabelString; ?></code>
    </pre>
    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
    </strong>
    
<?php
$labelResponseAry = array();
$labelResponseAry['status']="OK";
$labelResponseAry['token']="TAJE50OGIRHVRKKHHVZH"; 
$labelResponseAry['label'][0]['szLabelURL'] = "https://transporteca.co.uk/viewCourierLabel/1705TDE0065/"; 
$labelResponseAry['label'][0]['szText'] = ""; 


$getLabelResponse = getResponseAry($labelResponseAry,$mode); 
    ?>
    <pre>
        <code class="clear json"><?php echo $getLabelResponse; ?></code>
    </pre>
</div>    
<!--END Confirm Label Download Api DIV--> 


<!--START Submit NPS Api DIV-->
<a id="submitNps"><h2>&nbsp;</h2></a>
<div id='' class="namespace-summary margintop">
    <h1><?php echo t($t_base_partner_api.'leftMenu/submitNps');?></h1>
    <p>
        <?php echo t($t_base_partner_api.'description/submitNPSDescription');?>
    </p> 
    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
    </strong>
        
<?php
$getLabelRequestAry = array();
$getLabelRequestAry['szRequest']='submitNPS';
$getLabelRequestAry['szEmail']='shipper@email.com'; 
$getLabelRequestAry['iScore'] = '9';
$getLabelRequestAry['szComment'] = ''; 
$szGetLabelString = getRequestAry($getLabelRequestAry,$mode);
?>
    <pre>
        <code class="clear json"> <?php  echo $szPriceCallString1." ".$szSubmitNpsCallString2." ".$szGetLabelString; ?></code>
    </pre>
    <strong>
        <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
    </strong>
    
<?php
$labelResponseAry = array();
$labelResponseAry['status']="OK";
$labelResponseAry['token']="TAJE50OGIRHVRKKHHVZH"; 
$labelResponseAry['notification'] = "Thanks! We received your valuable feedback.";   

$getLabelResponse = getResponseAry($labelResponseAry,$mode); 
    ?>
    <pre>
        <code class="clear json"><?php echo $getLabelResponse; ?></code>
    </pre>
</div>    
<!--END Standard Cargo Api DIV-->

                <!--START ERROR RESPONSE DIV-->
                <a id="errorResponse"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/errorResponse');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/errorResponseDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/parcelExample2');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<?php
$requestAry['szRequest']='price';
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']='';
$requestAry['szShipperPostcode']='';
$requestAry['szShipperCity']='';
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.5;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12;
$requestAry['cargo'][1]['iQuantity'] = '';
unset($requestAry['cargo'][0]['szPalletType']);
unset($requestAry['cargo'][1]['szPalletType']);
unset($requestAry['cargo'][2]);
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<?php if(trim($mode) == "php"){ ?>
<code class="clear json">
<span class="attribute">code</span> => <span class="value"><span class="number">200</span></span>,
<span class="attribute">status</span> => <span class="value"><span class="string">Error</span></span>,
<span class="attribute">token</span> => <span class="value"><span class="string">TTP8DVLLFVP8U60JTULC</span></span>,
<span class="attribute">errors</span> => array([0]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1054</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1054');?></span></span>
),
array([1]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1063</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1063');?></span></span>
),
array([2]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1034</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1034');?></span></span>
),
array([3]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1035</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1035');?></span></span>
)
</code>
<?php }else{ ?>
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1054"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1054');?>"</span></span>
},
{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1063"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1063');?>"</span></span>
},
{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1034"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1034');?>"</span></span>
},
{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1035"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1035');?>"</span></span>
}
</code>
<?php } ?>
</pre>
<h3>Booking Call</h3>
<strong>
    <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
</strong>   
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.5;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12.2;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['szShipperPostcode']=210000;
$requestAry['szShipperCity']="Shanghai";
$requestAry['szShipperCountry']="CN";
$requestAry['szServiceID']="0601TTP91932";
$requestAry['token']="TTPZGN5L3CCCAGTNYXCE";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre> 
<strong>
    <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
</strong>     
<pre>
<?php
if(trim($mode) == "php")
{?>
<code class="clear json">
<span class="attribute">code</span> => <span class="value"><span class="number">200</span></span>,
<span class="attribute">status</span> => <span class="value"><span class="string">Error</span></span>,
<span class="attribute">token</span> => <span class="value"><span class="string">TTPZGN5L3CCCAGTNYXCE</span></span>,
<span class="attribute">errors</span> => array([0]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1097</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1097');?></span></span>
)
</code>
<?php }else{ ?>
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTPZGN5L3CCCAGTNYXCE"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1097"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1097');?>"</span></span>
}]}
</code>
<?php } ?>
</pre>
<h3>Insurance Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
<?php
$requestInsuranceAry['szRequest']='getInsuranceValue';
$requestInsuranceAry['szOriginCountry']='';
$requestInsuranceAry['szDestinationCountry']='';
$requestInsuranceAry['szTransportMode']='';
$requestInsuranceAry['szCargoType']='';
//$requestInsuranceAry['szGoodsInsuranceCurrency']='';
$requestInsuranceAry['szForwarderCountry']='';
$requestInsuranceAry['szCustomerType']='';


?>
<code class="clear json">
<?php
$szResponseInsuranceString = getRequestAry($requestInsuranceAry,$mode);
echo $szPriceCallString1;
echo $szInsuranceCallString2;
echo $szResponseInsuranceString;
?>
</code>
</pre>
<strong>
    <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
</strong>     
<pre>
<?php
if(trim($mode) == "php")
{?>
<code class="clear json">
<span class="attribute">code</span> => <span class="value"><span class="number">200</span></span>,
<span class="attribute">status</span> => <span class="value"><span class="string">Error</span></span>,
<span class="attribute">token</span> => <span class="value"><span class="string">TTPZGN5L3CCCAGTNYXCE</span></span>,
<span class="attribute">errors</span> => array([0]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1148</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1148');?></span></span>
),
array([1]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1150</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1150');?></span></span>
),
array([2]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1152</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1152');?></span></span>
),
array([3]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1154</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1154');?></span></span>
)
</code>
<?php }else{ ?>
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTPZGN5L3CCCAGTNYXCE"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1148"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1148');?>"</span></span>
},
{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1150"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1150');?>"</span></span>
}{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1152"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1152');?>"</span></span>
}{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1154"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1154');?>"</span></span>
}]}
</code>
<?php } ?>
</pre>
                    </div>
                <!--END ERROR RESPONSE DIV-->
                
                
                <!--START PARAMETER DIV-->
                <a id="requestForQuote"><h2>&nbsp;</h2></a>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/requestForQuote');?></h1>
                    <p><?php echo t($t_base_partner_api.'description/requestForQuoteDescription1');?></p>
                    <p><?php echo t($t_base_partner_api.'description/requestForQuoteDescription2');?></p>
                    <p><?php echo t($t_base_partner_api.'description/requestForQuoteDescription4');?></p>
                    <p><?php echo t($t_base_partner_api.'description/requestForQuoteDescription3');?></p>
                    <h3><?php echo t($t_base_partner_api.'heading/pricecall');?></h3>
                     <strong>
                        <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                    </strong>
<pre>
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']='21';
$requestAry['szShipperCountry']='IT';
$requestAry['szShipperPostcode']='199';
$requestAry['szShipperCity']='Rome';
$requestAry['bInsuranceRequired']='No';
$requestAry['szCargoCurrency']='DKK';
$requestAry['szCurrency']='DKK';
$requestAry['szServiceID']=''; 
$requestAry['token']='';
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = '';
$requestAry['cargo'][0]['iHeight'] = 160;
$requestAry['cargo'][0]['iWeight'] = 180;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][0]['szPalletType'] = "Euro"; 
unset($requestAry['cargo'][1]);
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>        
<strong>
    <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
</strong>      
                    
<pre>
<?php
if(trim($mode) == "php")
{?>
<code class="clear json">
<span class="attribute">code</span> => <span class="value"><span class="number">200</span></span>,
<span class="attribute">status</span> => <span class="value"><span class="string">Error</span></span>,
<span class="attribute">token</span> => <span class="value"><span class="string">TMLITHEGC24PI7SYJNRO</span></span>,
<span class="attribute">errors</span> => array([0]=>
<span class="attribute">code</span> => <span class="value"><span class="string">E1092</span></span>,
<span class="attribute">description</span> => <span class="value"><span class="string"><?php echo t($t_base_partner.'error/E1092');?></span></span>
)
</code>
<?php }else{ ?>
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TMLITHEGC24PI7SYJNRO"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1092"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1092');?>"</span></span>
}]}
</code>
<?php } ?>
</pre>

<h3><?php echo t($t_base_partner_api.'heading/bookingcall');?></h3>
<strong>
    <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
</strong>
<pre>
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']='21';
$requestAry['szShipperCountry']='IT';
$requestAry['szShipperPostcode']='199';
$requestAry['szShipperCity']='Rome';
$requestAry['bInsuranceRequired']='No';
$requestAry['szCargoCurrency']='DKK';
$requestAry['szCurrency']='DKK';
$requestAry['szServiceID']='2222'; 
$requestAry['token']='TMLITHEGC24PI7SYJNRO';
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = '';
$requestAry['cargo'][0]['iHeight'] = 160;
$requestAry['cargo'][0]['iWeight'] = 180;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][0]['szPalletType'] = "Euro"; 
unset($requestAry['cargo'][1]);
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>
<strong>
    <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
</strong>
                        
<pre>
<?php
if(trim($mode) == "php")
{?>
<code class="clear json">
<span class="attribute">code</span> => <span class="value"><span class="number">200</span></span>,
<span class="attribute">status</span> => <span class="value"><span class="string">Error</span></span>,
<span class="attribute">token</span> => <span class="value"><span class="string">TMLITHEGC24PI7SYJNRO</span></span>,
<span class="attribute">bookings</span> => array([0]=>
<span class="attribute">notification</span> => <span class="value"><span class="string">Transporteca will send the transportation quotation directly to ABC Seller Company within one working day</span></span>
)
</code>
<?php }else{ ?>
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"OK"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TMLITHEGC24PI7SYJNRO"</span></span>,
"<span class="attribute">bookings</span>":[{
"<span class="attribute">notification</span>":<span class="value"><span class="string">"Transporteca will send the transportation quotation directly to ABC Seller Company within one working day"</span></span>
}]}
</code>
<?php } ?>
</pre>

                </div>    
                <!--START PARAMETER DIV-->
                <a id="parameters"><h2>&nbsp;</h2></a>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/parameters');?></h1>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    Request Parameter
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/requestParameter');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szRequest
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szRequest');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    Timing Parameters
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/timingParameters');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtDay
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Format: DD
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 03
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/dtDay');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation').", ".t($t_base_partner_api.'heading/numeric');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtMonth
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Format: MM
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 01
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/dtMonth');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation').", ".t($t_base_partner_api.'heading/numeric');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtYear
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Format: YYYY
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2016
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/dtYear');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation').", ".t($t_base_partner_api.'heading/numeric');?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Cargo Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/cargoParameter');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipmentType
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: PARCEL
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipmentType');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iLength
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 56
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iLength');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iWidth
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 36
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iWidth');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iHeight
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 50
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iHeight');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan').", ".t($t_base_partner_api.'heading/iHeightValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iWeight
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 100
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iWeight');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iQuantity
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iQuantity');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fTotalVolume
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 3.6
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fTotalVolume');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fTotalWeight
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2350
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fTotalWeight');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szDimensionMeasure
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: cm
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szDimensionMeasure');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szWeightMeasure
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: pounds
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szWeightMeasure');?>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szServiceTerm
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DTD
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szServiceTerm');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCargoDescription
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/freeText');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Wooden Chairs
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCargoDescription');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCommodity
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCommodity');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fCargoValue
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1356.34
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fCargoValue');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/greaterThan');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCargoCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DKK
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCargoCurrency');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iStandardCargoId
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 17,9
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iStandardCargoId');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iQuantity
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1,2...
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iQuantityText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iShipperDetailsLocked
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0 or 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iShipperDetailsLockedText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iConsigneeDetailsLocked
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0 or 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iConsigneeDetailsLockedText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iCargoDescriptionLocked
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0 or 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iCargoDescriptionLockedText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iRemovePriceGuarantee
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0 or 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iRemovePriceGuaranteeText');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Shipper Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/shipperParameter');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperCompanyName
                                    </div>
                                    <div>
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div>
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div>
                                      Example: ABC Company
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperCompanyName');?>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperFirstName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: John
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperFirstName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperLastName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Miller
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperLastName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperEmail
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: shipper@email.com
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperEmail');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/email').", ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iShipperCountryDialCode
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 86
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iShipperCountryDialCode');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/numeric');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iShipperPhoneNumber
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/freeText');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1234567890
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iShipperPhoneNumber');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperAddress
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Park Lane 19, Building 15
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperAddress');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperPostcode
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 440000
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperPostcode');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperCity
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                    
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperCity');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperLatitude
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 18.932245
                                    </div>
                                    
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperLatitude');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperLongitude
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 72.826439
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperLongitude');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperCountry
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: US
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperCountry');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Consignee Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/consigneeParameter');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeCompanyName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: XYZ Company
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeCompanyName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeFirstName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Albert
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeFirstName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeLastName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Methew
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeLastName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeEmail
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: consignee@email.com
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeEmail');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/email').", ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iConsigneeCountryDialCode
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 46
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iConsigneeCountryDialCode');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/numeric');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iConsigneePhoneNumber
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/freeText');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 123123123
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iConsigneePhoneNumber');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeAddress
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 51, Block D, West Street
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeAddress');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneePostcode
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2100
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneePostcode');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeCity
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Copenhagen
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeCity');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeLatitude
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 55.676098
                                    </div>
                                    
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeLatitude');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeLongitude
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 12.568337
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeLongitude');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeCountry
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DK
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeCountry');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Billing Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/billingParameter');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingCompanyName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: XYZ Company
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingCompanyName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingFirstName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Albert
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingFirstName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingLastName
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Methew
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingLastName');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingEmail
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: consignee@email.com
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingEmail');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/email').", ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iBillingCountryDialCode
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 46
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iBillingCountryDialCode');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/numeric');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iBillingPhoneNumber
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/freeText');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 123123123
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iBillingPhoneNumber');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingAddress
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 51, Block D, West Street
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingAddress');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/maxLength')."(255)";?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingPostcode
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2100
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingPostcode');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingCity
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Copenhagen
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingCity');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingCountry
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DK
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBillingCountry');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/reqValidation');?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Response Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/responseParameter');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szServiceID
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 3112TTP92242
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szServiceID');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        token
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        bInsuranceRequired
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Yes
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iInsuranceRequired');?>
                                    <div>
                                        <?php echo t($t_base_partner_api.'heading/validation').": ".t($t_base_partner_api.'heading/caseInsensitive');?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Other Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/othersParameter');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: USD
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCurrency');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szLanguage
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: EN
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szLanguage');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szReference
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Mr.Methew
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szReference');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        customerType
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/array');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: customerType[0] = 'PRIVATE'
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/customerType');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szPaymentType
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Transfer
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szPaymentType');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szStripeToken
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: tok_XXXXXXXXXXXXXXXXXXXX
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szStripeToken');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iShipperConsigneeType
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: "SHIPPER","CONSIGNEE","BILLING"
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iShipperConsigneeType');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iOfferType
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1,2,3,4,5,6,7,8,9,10,11,12
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/iOfferTypeText');?>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                    
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Insurance Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/insuranceResquestParameters');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szOriginCountry
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DK,IN
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szOriginCountry');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szDestinationCountry
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DK,IN
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szDestinationCountry');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportMode
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 'COURIER','LCL'
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szTransportModeForInsurance');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCargoType
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 'GENERAL CARGO'
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCargoTypeForInsurance');?>
                                </td>
                            </tr>
                            <!--<tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szGoodsInsuranceCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 'DKK'
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szGoodsInsuranceCurrencyStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fValueOfGoods
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: '100'
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fValueOfGoodsStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCustomerCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 'DKK'
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCustomerCurrencyStr');?>
                                </td>
                            </tr>
                            <tr>
                                
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fTotalPriceCustomerCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1000
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fTotalPriceCustomerCurrencyStr');?>
                                </td>
                            </tr>-->
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szForwarderCountry
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DK
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szForwarderCountryStr');?>
                                </td>
                            </tr>
                            <!--<tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBookingCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DKK
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szBookingCurrencyStr');?>
                                </td>
                            </tr>-->
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCustomerType
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 'PRIVATE'
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/customerType');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p><?php echo t($t_base_partner_api.'leftMenu/standardCargo');?> <?php echo t($t_base_partner_api.'leftMenu/parameters');?></p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/standardCargoResquestParameters');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iDataSet
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1,2,3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iDataSetText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iLanguage
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1,2,3..
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iLanguageText');?>
                                </td>
                            </tr>
                    </table>
                </div>
                <!--END PARAMETER DIV-->
                
                <!--START RETURN VALUE DIV-->
                
                <a id="returnValues"><h2>&nbsp;</h2></a>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/returnValues');?></h1>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Price Response Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/priceResponseParameters');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        token
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szServiceID
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 3112TTP92242
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szServiceID');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportStartDay
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 01
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtTransportStartDay');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportStartMonth
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 01
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtTransportStartMonth');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportStartYear
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2016
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtTransportStartYear');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportStartCity
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Mumbai
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportStartCity');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportFinishDay
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 09
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryDay');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportFinishMonth
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 01
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryMonth');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportFinishYear
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2016
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryYear');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportFinishCity
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Copenhagen
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportFinishCity');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szDtdTrade
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0 or 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szDtdTradeText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iOfferType
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1,2,3,4,5,6,7,8,9,10,11,12
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/iOfferTypeText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iResponseDay
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0 or 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'standardCargoResponse/iResponseDayText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szPackingType
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Pallets and cartons
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szPackingType');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: HKD
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCurrency');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fBookingPrice
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1324.34
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fBookingPrice');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fVatAmount
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 624.45
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fVatAmount');?>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        bInsuranceAvailable
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Yes
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/bInsuranceAvailable');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fInsurancePrice
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 190.39
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fInsurancePrice');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportMode 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: LCL
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportMode');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportIcon 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1 or 2 or 3 or 4
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportIcon');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szForwarderAlias  
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: AB
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szForwarderAlias');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szServiceDescription  
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Transportation from suppliers door in Rome to your door in Fredensborg, all inclusive. This is also known as EXW terms.
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szServiceDescription');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Booking Response Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/bookingResponseParameters');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        token
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportStartMonth
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 01
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtTransportStartMonth');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportStartYear
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2016
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtTransportStartYear');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportStartCity
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Mumbai
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportStartCity');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportFinishDay
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 12
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryDay');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportFinishMonth
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 01
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryMonth');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        dtTransportFinishYear
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2016
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryYear');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportFinishCity
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Copenhagen
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportFinishCity');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBookingStatus
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Confirmed
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingStatus');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBookingRef
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1512TSM072
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingRef');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szServiceDescription
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szServiceDescription');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportMode
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Courier
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportMode');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportIcon 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1 or 2 or 3 or 4
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportIcon');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szPackingType
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Only cartons
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szPackingType');?>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: USD
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCurrency');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fBookingPrice
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 167.50
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fBookingPrice');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fVatAmount
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 41.75
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fVatAmount');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fInsurancePrice
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 73
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fInsurancePrice');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        bInsuranceAvailable
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Yes
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/bInsuranceAvailable');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierName
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DSV
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierAddressLine1
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Fruebjergvej 3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierAddressLine1');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierAddressLine2
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierAddressLine2');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierAddressLine3
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierAddressLine3');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierPostcode
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 2100
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierPostcode');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierCity
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Copenhagen
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierCity');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierRegion
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Copenhagen West
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierRegion');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierCountry
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DK
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierCountry');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBookingContactNumber
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: +45 7199 7299
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingContactNumber');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBookingContactEmail
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: contact@transporteca.com
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingContactEmail');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCarrierCompanyRegistration
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 23 54 12
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierCompanyRegistration');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTotalPrice
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 14,522.53
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTotalPriceText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szLatestPaymentDate 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 3 May
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szLatestPaymentDateText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingEmail 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: ****@transporteca.com 
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBillingEmailText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportStartDate  
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 3. maj 
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportStartDateText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szTransportFinishDate 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 13. maj 
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportFinishDateText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szShipperCountryName 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Indien 
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szShipperCountryNameText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szConsigneeCountryName  
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Danmark
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szConsigneeCountryNameText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szBillingCountryName 
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Kina 
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBillingCountryNameText');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p>Insurance Response Parameters</p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/insuranceResponseParameters');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        token
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fInsuranceUptoPrice
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 10000
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'insurancegResponse/fInsuranceUptoPriceStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szInsuranceCurrency
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: DKK
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'insurancegResponse/szInsuranceCurrencyStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fInsuranceCurrencyExchangeRate
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0.1428
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'insurancegResponse/fInsuranceCurrencyExchangeRateStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fInsurancePercentageRate
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 5.00 %
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'insurancegResponse/fInsurancePercentageRateStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        fInsuranceMinPrice
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 100
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'insurancegResponse/fInsuranceMinPriceStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szPriceToken
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: TAJE50OGIRHVRKKHHVZH
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'insurancegResponse/szPriceTokenStr');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szServiceID
                                    </div>
                                    <div class="parmeter-margin">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0905TTPEXW66318B28
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'insurancegResponse/szServiceIDStr');?>
                                </td>
                            </tr>
                            
                          </tbody>  
                    </table>
                    <table class="params simple top30">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    <p><?php echo t($t_base_partner_api.'leftMenu/standardCargo');?> <?php echo t($t_base_partner_api.'leftMenu/response');?> <?php echo t($t_base_partner_api.'leftMenu/parameters');?></p>
                                    <?php echo t($t_base_partner_api.'parameterIntroductiveTexts/insuranceResponseParameters');?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        token
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        iResponseDay
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 0 or 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'standardCargoResponse/iResponseDayText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCategoryID
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 1
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'standardCargoResponse/szCategoryIDText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szCategory
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: Bestseller
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'standardCargoResponse/szCategoryText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szStandardCagoID
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: 9
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'standardCargoResponse/szStandardCagoIDText');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="parmeter-margin param-name">
                                        szType
                                    </div>
                                    <div class="parmeter-margin">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </div>
                                    <div class="parmeter-margin">
                                      Example: AJ Floor Lamp
                                    </div>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'standardCargoResponse/szTypeText');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>  
                </div>
                <!--END RETURN VALUE DIV-->
                
                <!--START ERROR DIV-->
                <a id="errors"><h2>&nbsp;</h2></a>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/errors');?></h1>
                    <p><?php echo t($t_base_partner_api.'description/errorCodeDescription');?></p>
                        <table class="simple top30">
                            <thead>
                                <tr>
                                    <th><?php echo t($t_base_partner_api.'heading/codeHeading');?></th>
                                    <th><?php echo t($t_base_partner_api.'heading/descriptionHeading');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(!empty($errorDetailsAry))
                                    {
                                        foreach($errorDetailsAry as $errorDetailsArys)
                                        {
                                            $szErrorCode = $errorDetailsArys['szErrorCode'];
                                            $szErrorMessage = t($t_base_partner.'error/'.$szErrorCode);
                                            if(!empty($szErrorCode) && !empty($szErrorMessage))
                                            {
                                                ?>
                                                <tr>
                                                    <td class="param-desc"><?php echo $szErrorCode; ?></td>
                                                    <td class="param-desc"><?php echo $szErrorMessage;?></td>
                                                </tr>
                                                <?php
                                            } 
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                </div>
                <!--END ERROR DIV-->
                </div>
              </div>
            </div>
        </div>
        </section>
    </body>
</html>