<?php 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) ); 
ini_set('display_errors',false); 
error_reporting(0); 

require_once(__APP_PATH__."/config.php");
 
$priceInputAry = array(); 

$priceInputAry['szRequest'] = 'price';
$priceInputAry['dtDay'] =date('d'); //DD
$priceInputAry['dtMonth'] = date('m');//MM
$priceInputAry['dtYear'] = date('Y'); //YY

/*
 * You can send cargo details (szShipmentType) in 3 types 
 * 1. PARCEL  (Lenght X Width X Height), Quantity
 * 2. PALLET  Pallet Type, (Length X Width X Height), Quantity
 * 3. BREAK_BULK (Total Weight of shipment X Total Volume shipment)
 */

$priceInputAry['cargo'][0]['szShipmentType'] = "PARCEL";
$priceInputAry['cargo'][0]['iLength'] = 100;
$priceInputAry['cargo'][0]['iWidth'] = 100;
$priceInputAry['cargo'][0]['iHeight'] = 100;
$priceInputAry['cargo'][0]['iWeight'] = 100;
$priceInputAry['cargo'][0]['iQuantity'] = 1; 
$priceInputAry['cargo'][0]['szDimensionMeasure'] ="CM"; // case insensitive, can be cm or inch, for szShipmentType BREAK_BULK szDimensionMeasure must be cbm
$priceInputAry['cargo'][0]['szWeightMeasure'] = "KG"; // case insensitive can be Kg or pounds, for szShipmentType BREAK_BULK szDimensionMeasure must be kg

$priceInputAry['cargo'][1]['szShipmentType'] = "PARCEL";
$priceInputAry['cargo'][1]['iLength'] = 120;
$priceInputAry['cargo'][1]['iWidth'] = 120;
$priceInputAry['cargo'][1]['iHeight'] = 120;
$priceInputAry['cargo'][1]['iWeight'] = 100;
$priceInputAry['cargo'][1]['iQuantity'] = 1; 
$priceInputAry['cargo'][1]['szDimensionMeasure'] ="CM";  
$priceInputAry['cargo'][1]['szWeightMeasure'] = "KG"; 

/*
 * You can use any of the following as service term
 * 1. FOB - Freight On Board or Free On Board
 * 2. EXW - Ex Works
 * 3. CFR - Cost and Freight
 * 4. DAP - Delivered At Place
 * 5. DTD - Door to door
 */

$priceInputAry['szServiceTerm'] = 'EXW';  //This is mandatory only in case of BREAK_BULK shipment
$priceInputAry['fCargoValue'] = ''; //This is purchase price of your goods
$priceInputAry['szCargoCurrency'] = ""; //This is purchase price currency of your goods

/*
* Shipper details
*/ 
$priceInputAry['szShipperPostcode'] =440000;
$priceInputAry['szShipperCity'] = "Mumbai"; 
$priceInputAry['szShipperCountry'] = "IN"; // India

/*
* Consignee details
*/ 
$priceInputAry['szConsigneePostcode'] = 2100;
$priceInputAry['szConsigneeCity'] = "Copenhagen";
$priceInputAry['szConsigneeCountry'] = "DK"; // Denmark
  
$priceInputAry['szCurrency'] = "USD"; // Currency in which you want to receive api response. default should be 'USD'
$priceInputAry['szLanguage'] = "DA"; // Language in which you need response. Values should EN or DA  
 
$szTransportecApiKey = "3f31e6d2729d90a98024468a3f3b2bf5";
$kTransporteca = \Transporteca\Transporteca::setApiKey($szTransportecApiKey,'TEST');  
 
try
{
    $transportecaResponseAry = \Transporteca\Service::getPrice($priceInputAry);
    
    echo "<br><br> Your API Request <br><br> ";
    print_R($priceInputAry);
    
    echo "<br><br> Your API Response <br><br> ";
    print_R($transportecaResponseAry);
}
catch (Error\Service $ex)
{
    echo "Exception: ".$ex->getMessage();
}
catch (Exception $e)
{
    echo "Exception: ".$e->getMessage();
}

?>
