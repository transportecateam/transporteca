<?php

namespace Transporteca\Error;

class InvalidRequest extends Base
{
    public function __construct(
        $message,
        $transportecaParam,
        $httpStatus = null,
        $httpBody = null,
        $jsonBody = null,
        $httpHeaders = null
    ) 
    {
        parent::__construct($message, $httpStatus, $httpBody, $jsonBody, $httpHeaders);
        $this->transportecaParam = $transportecaParam;
        echo $transportecaParam;
    } 
    public function getTransportecaParam()
    {
        return $this->transportecaParam;
    }
}
