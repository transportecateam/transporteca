<?php

namespace Transporteca\Error;

class Service extends Base
{
    public function __construct(
        $message,
        $transportecaParam,
        $transportecaCode,
        $httpStatus,
        $httpBody,
        $jsonBody,
        $httpHeaders = null
    ) {
        parent::__construct($message, $httpStatus, $httpBody, $jsonBody, $httpHeaders);
        $this->transportecaParam = $transportecaParam;
        $this->transportecaCode = $transportecaCode;
    }

    public function getTransportecaCode()
    {
        return $this->transportecaCode;
    }

    public function getTransportecaParam()
    {
        return $this->transportecaParam;
    }
}
