<?php

namespace Transporteca;

abstract class ApiResource
{
    private static $HEADERS_TO_PERSIST = array('Transporteca-Account' => true, 'Transporteca-Version' => true);

    public static function baseUrl()
    {
        return Transporteca::getApiBase();
    }

    /**
     * @return string The name of the class, with namespacing and underscores 
     */
    public static function className()
    {
        $class = get_called_class();
        // Useful for namespaces: Foo\Charge
        if ($postfixNamespaces = strrchr($class, '\\')) {
            $class = substr($postfixNamespaces, 1);
        }
        // Useful for underscored 'namespaces': Foo_Charge
        if ($postfixFakeNamespaces = strrchr($class, '')) {
            $class = $postfixFakeNamespaces;
        } 
        $class = str_replace('_', '', $class);
        $name = urlencode($class);
        $name = strtolower($name);
        return $name;
    }

    /**
    * @return string The endpoint URL for the given class.
    */
    public static function classUrl()
    {
        $base = static::className();
        return "/api/${base}s";
    }

    /**
     * @return string The full API URL for this API resource.
     */
    public function instanceUrl()
    {
        $id = $this['id'];
        if ($id === null) {
            $class = get_called_class();
            $message = "Could not determine which URL to request: "
               . "$class instance has invalid ID: $id";
            throw new Error\InvalidRequest($message, null);
        }
        $id = Util\Util::utf8($id);
        $base = static::classUrl();
        $extn = urlencode($id);
        return "$base/$extn";
    }

    private static function _validateParams($params = null)
    { 
        if (empty($params))
        {
            $message = "You must pass an array as the first argument to Transporteca API ";
            throw new Error\Api($message);
        }
        
        if($params && !is_array($params))
        {
            $message = "You must pass an array as the first argument to Transporteca API ";
            throw new Error\Api($message);
        }
        else if(!empty($params))
        {
            return  Util\Util::__toJson($params);
        }
    }

    protected function _request($method, $url, $params = array(), $options = null)
    {
        $opts = $this->_opts->merge($options);
        return static::_staticRequest($method, $url, $params, $opts);
    }

    protected static function _staticRequest($method, $url, $params, $options)
    {
        $opts = Util\RequestOptions::parse($options); 
        $apiKey = Transporteca::getApiKey();
        $requestor = new ApiRequestor($apiKey, static::baseUrl()); 
        list($response, $opts->apiKey) = $requestor->request($method, $url, $params, $opts->headers);
        foreach ($opts->headers as $k => $v) {
            if (!array_key_exists($k, self::$HEADERS_TO_PERSIST)) {
                unset($opts->headers[$k]);
            }
        }
        return array($response, $opts);
    } 
    protected static function _create($params = null, $options = null,$url=null)
    {
        self::_validateParams($params); 
        $base = static::baseUrl();
        if(empty($url))
        {
            $url = static::classUrl();  
        } 
        list($response, $opts) = static::_staticRequest('post', $url, $params, $options);   
        return $response;
    } 
}
