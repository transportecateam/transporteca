<?php

namespace Transporteca;

class Service extends ApiResource
{ 
    /**
     * @param array $params
     * @param array|string|null $options
     *
     * @return servie list in json object.
     */
    public static function getPrice($params = null, $options = null)
    {
        return self::_create($params, $options);
    } 
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return forwarder rating review list in json object.
    */
    public static function forwarderRating($params = null, $options = null)
    {
        $szApiEndPoint = "/api/forwarderRating/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return forwarder rating review list in json object.
    */
    public static function getInsuranceValue($params = null, $options = null)
    {
        $szApiEndPoint = "/api/getInsuranceValue/";
        return self::_create($params, $options,$szApiEndPoint);
    }
     
    public static function validatePrice($params = null, $options = null)
    {
        $szApiEndPoint = "/api/validatePrice/";
        return self::_create($params, $options,$szApiEndPoint);
    }
}
