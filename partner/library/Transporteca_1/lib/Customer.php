<?php

namespace Transporteca;

class Customer extends ApiResource
{ 
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function customerLogin($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/login/";
        return self::_create($params, $options,$szApiEndPoint);
    } 
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function customerSignup($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/signup/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function customerProfileDetails($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/profileDetails/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function bookings($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/bookings/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function activebookings($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/bookings/active/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function draftbookings($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/bookings/draft/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function holdbookings($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/bookings/hold/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function archivedtbookings($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/bookings/archived/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return notification contains password changed success message.
    * @author Transporteca <contact@transporteca.com>
    */
    public static function changePassword($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/changePassword/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    * @author Transporteca <contact@transporteca.com>
    */
    public static function forgotPassword($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/forgotPassword/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return 
    */
    public static function deleteCustomerAccount($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/deleteAccount/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    public static function updateProfile($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/updateProfile/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function getShippers($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/shippers/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function getConsignees($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/consignees/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function getShipperDetails($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/shipperDetails/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function getConsigneeDetails($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/consigneeDetails/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function updateShipperDetails($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/updateShipperDetails/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function updateConsigneeDetails($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/updateConsigneeDetails/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function addShipperDetails($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/addShipperDetails/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
    /**
    * @param array $params
    * @param array|string|null $options
    *
    * @return servie list in json object.
    */
    function addConsigneeDetails($params = null, $options = null)
    {
        $szApiEndPoint = "/api/customer/addConsigneeDetails/";
        return self::_create($params, $options,$szApiEndPoint);
    }
    
}
