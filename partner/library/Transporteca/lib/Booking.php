<?php

namespace Transporteca;

class Booking extends ApiResource
{ 
    /**
     * @param array $params
     * @param array|string|null $options
     *
     * @return servie list in json object.
     */
    public static function createBooking($params = null, $options = null)
    {
        return self::_create($params, $options);
    } 
}
