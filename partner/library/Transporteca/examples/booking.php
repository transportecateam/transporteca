<?php 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) ); 
ini_set('display_errors',false); 
error_reporting(0); 

require_once(__APP_PATH__."/config.php");
 
$bookingInputAry = array(); 

$bookingInputAry['szRequest'] = 'booking';
$bookingInputAry['dtDay'] =date('d'); //DD
$bookingInputAry['dtMonth'] = date('m');//MM
$bookingInputAry['dtYear'] = date('Y'); //YY

/*
 * You can send cargo details (szShipmentType) in 3 types 
 * 1. PARCEL (Lenght X Width X Height), Quantity
 * 2. PALLET Pallet Type, (Length X Width X Height), Quantity
 * 3. BREAK_BULK (Total Weight of shipment X Total Volume shipment)
 */

$bookingInputAry['cargo'][0]['szShipmentType'] = "PARCEL";
$bookingInputAry['cargo'][0]['iLength'] = 100;
$bookingInputAry['cargo'][0]['iWidth'] = 100;
$bookingInputAry['cargo'][0]['iHeight'] = 100;
$bookingInputAry['cargo'][0]['iWeight'] = 100;
$bookingInputAry['cargo'][0]['iQuantity'] = 1; 
$bookingInputAry['cargo'][0]['szDimensionMeasure'] ="CM"; // case insensitive, can be cm or inch, for szShipmentType BREAK_BULK szDimensionMeasure must be cbm
$bookingInputAry['cargo'][0]['szWeightMeasure'] = "KG"; // case insensitive can be Kg or pounds, for szShipmentType BREAK_BULK szDimensionMeasure must be kg

$bookingInputAry['cargo'][1]['szShipmentType'] = "PARCEL";
$bookingInputAry['cargo'][1]['iLength'] = 120;
$bookingInputAry['cargo'][1]['iWidth'] = 120;
$bookingInputAry['cargo'][1]['iHeight'] = 120;
$bookingInputAry['cargo'][1]['iWeight'] = 100;
$bookingInputAry['cargo'][1]['iQuantity'] = 1; 
$bookingInputAry['cargo'][1]['szDimensionMeasure'] ="CM";
$bookingInputAry['cargo'][1]['szWeightMeasure'] = "KG";

/*
 * You can use any of the following as service term
 * 1. FOB - Freight On Board or Free On Board
 * 2. EXW - Ex Works
 * 3. CFR - Cost and Freight
 * 4. DAP - Delivered At Place
 * 5. DTD - Door to door
 */

$bookingInputAry['szServiceTerm'] = 'EXW'; //This is mandatory only in case of BREAK_BULK shipment
$bookingInputAry['szCargoDescription'] = "Shoe racks"; //This is short description of your goods
$bookingInputAry['fCargoValue'] = 1000; //This is purchase price of your goods
$bookingInputAry['szCargoCurrency'] = "DKK"; //This is purchase price currency of your goods

/*
* Shipper details
*/
$bookingInputAry['szShipperCompanyName'] ="ABC comapny";
$bookingInputAry['szShipperFirstName'] ="Johan";
$bookingInputAry['szShipperLastName'] ="Miller";
$bookingInputAry['szShipperEmail'] ="shipper@email.com";
$bookingInputAry['iShipperCountryDialCode'] =91;
$bookingInputAry['iShipperPhoneNumber'] =1234567890;
$bookingInputAry['szShipperAddress'] ="A-49 , Phase - 1, Sector 2";
$bookingInputAry['szShipperPostcode'] =440000;
$bookingInputAry['szShipperCity'] = "Mumbai"; 
$bookingInputAry['szShipperCountry'] = "IN"; // India

/*
* Consignee details
*/
$bookingInputAry['szConsigneeCompanyName'] ="XYZ company";
$bookingInputAry['szConsigneeFirstName'] ="Albert";
$bookingInputAry['szConsigneeLastName'] ="Methew";
$bookingInputAry['szConsigneeEmail'] ="consignee@email.com";
$bookingInputAry['iConsigneeCountryDialCode'] =46;
$bookingInputAry['iConsigneePhoneNumber'] =1231231234;
$bookingInputAry['szConsigneeAddress'] ="51, Block D, West Street";
$bookingInputAry['szConsigneePostcode'] = 2100;
$bookingInputAry['szConsigneeCity'] = "Copenhagen";
$bookingInputAry['szConsigneeCountry'] = "DK"; // Denmark

$bookingInputAry['szServiceID'] = ""; // This is the unique Id which you get in Price Response as 'szServiceID'
$bookingInputAry['token'] = ""; // This is the unique token which you get in Price Response as 'token'
            
$bookingInputAry['iInsuranceRequired'] = "Yes"; // Wheather you need transportation insurance or not. Value should be 'Yes' or 'No'
$bookingInputAry['szCurrency'] = "USD"; // Currency in which you want to receive api response. default should be 'USD'
$bookingInputAry['szLanguage'] = "EN"; // Language in which you need response. Values should EN or DA
$bookingInputAry['szReference'] = "Sample Texts"; //This is your special remark what you want to add with this booking

 
$szTransportecApiKey = "YOUR_API_KEY";
$kTransporteca = \Transporteca\Transporteca::setApiKey($szTransportecApiKey,'TEST');  
 
try
{
    $transportecaResponseAry = \Transporteca\Booking::createBooking($bookingInputAry);
    
    echo "<br><br> Your API Request <br><br> ";
    print_R($bookingInputAry);
    
    echo "<br><br> Your API Response <br><br> ";
    print_R($transportecaResponseAry);
}
catch (Error\Booking $ex)
{
    echo "Exception: ".$ex->getMessage();
}
catch (Exception $e)
{
    echo "Exception: ".$ex->getMessage();
}

?>
