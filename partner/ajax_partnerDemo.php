﻿<?php
ob_start();
session_start();
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
ini_set (max_execution_time,300000);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );  
require_once(__APP_PATH__."/Transporteca/config.php");
  
$kPartner = new cPartner();    
if(!empty($_POST['partnerApiRequestAry']))
{
    $_SESSION['partnerApiRequestAry'] = $_POST['partnerApiRequestAry'] ; 
    $szMethodType = $_POST['partnerApiRequestAry']['szRequest'];  
    
    $szTransportecApiKey = "3f31e6d2729d90a98024468a3f3b2bf5";
    $szTransportecaMode = $_POST['partnerApiRequestAry']['szMethod'];
    
    if(!empty($_POST['partnerApiRequestAry']['szApiRequestParams']))
    { 
        //$encodedString = utf8_encode($_POST['partnerApiRequestAry']['szApiRequestParams']);
        $encodedString = $_POST['partnerApiRequestAry']['szApiRequestParams'];
        $apiRequestParams = json_decode($encodedString,true);   
        $szMethodType = $apiRequestParams['szRequest']; 
    }  
}
else if(!empty($_POST['getCourierRateAry']))
{
    $_SESSION['getCourierRateAry'] = $_POST['getCourierRateAry'] ;
    $_SESSION['cargo'] = $_POST['cargo'] ;
    $szMethodType = $_POST['getCourierRateAry']['szRequest']; 
    
    $szTransportecApiKey = $_POST['szApiKey'];
    $szTransportecaMode = $_POST['szMode'];

    $apiRequestParams = $kPartner->prepareRequestParams($_POST['getCourierRateAry'],$_POST['cargo']);
} 

if($szMethodType=='price')
{    
    $kTransporteca = \Transporteca\Transporteca::setApiKey($szTransportecApiKey,$szTransportecaMode);
    try
    {
        $transportecaResponseAry = \Transporteca\Service::getPrice($apiRequestParams);    
    }
    catch (\TransportecaError\Service $ex)
    { 
        $szExceptionMessage = $ex->getMessage();
    }
    catch (Exception $ex)
    {  
        $szExceptionMessage =  $ex->getMessage();
    }

    $szApiEndPoint = \Transporteca\Transporteca::$apiEndPoint;
    $szApiRequestHeaders = \Transporteca\Transporteca::$apiHeader;
    $szApiRequestParams = \Transporteca\Transporteca::$apiParams;
    $szApiResponseParams = \Transporteca\Transporteca::$apiResponse;
    $szApiResponseCode = \Transporteca\Transporteca::$apiResponseCode;
    $szApiResponseHeaders = \Transporteca\Transporteca::$apiResponseHeaders;
   
    echo "SUCCESS||||";
    echo display_api_response($szApiEndPoint,$szApiRequestHeaders,$szApiRequestParams,$szApiResponseParams,$szApiResponseCode,$szApiResponseHeaders,$szExceptionMessage,$szMethodType);
    die;
}  
else if($szMethodType=='booking' || $szMethodType=='validatePrice' || $szMethodType=='confirmPayment')
{
    $kTransporteca = \Transporteca\Transporteca::setApiKey($szTransportecApiKey,$szTransportecaMode);
    
    try
    {
        if($szMethodType=='validatePrice')
        {
            $transportecaResponseAry = \Transporteca\Service::validatePrice($apiRequestParams);   
        }
        else if($szMethodType=='confirmPayment')
        {
            $transportecaResponseAry = \Transporteca\Booking::confirmPayment($apiRequestParams);   
        }
        else
        {
            $transportecaResponseAry = \Transporteca\Booking::createBooking($apiRequestParams);   
        } 
    }
    catch (\TransportecaError\Booking $ex)
    { 
        $szExceptionMessage = $ex->getMessage();
    }
    catch (Exception $ex)
    {  
        $szExceptionMessage =  $ex->getMessage();
    }

    $szApiEndPoint = \Transporteca\Transporteca::$apiEndPoint;
    $szApiRequestHeaders = \Transporteca\Transporteca::$apiHeader;
    $szApiRequestParams = \Transporteca\Transporteca::$apiParams;
    $szApiResponseParams = \Transporteca\Transporteca::$apiResponse;
    $szApiResponseCode = \Transporteca\Transporteca::$apiResponseCode;
    $szApiResponseHeaders = \Transporteca\Transporteca::$apiResponseHeaders;
    
    echo "SUCCESS||||";
    echo display_api_response($szApiEndPoint,$szApiRequestHeaders,$szApiRequestParams,$szApiResponseParams,$szApiResponseCode,$szApiResponseHeaders,$szExceptionMessage,$szMethodType);
    die;
}
else if(!empty($szMethodType))
{
    $kTransporteca = \Transporteca\Transporteca::setApiKey($szTransportecApiKey,$szTransportecaMode);
    try
    { 
        if($szMethodType=='customerLogin')
        {
            $transportecaResponseAry = \Transporteca\Customer::customerLogin($apiRequestParams);   
        }  
        else if($szMethodType=='customerSignup')
        {
            $transportecaResponseAry = \Transporteca\Customer::customerSignup($apiRequestParams);   
        }
        else if($szMethodType=='customerUpdateProfile')
        {
            $transportecaResponseAry = \Transporteca\Customer::updateProfile($apiRequestParams);   
        } 
        else if($szMethodType=='customerChangePassword')
        {
            $transportecaResponseAry = \Transporteca\Customer::changePassword($apiRequestParams);   
        }
        else if($szMethodType=='customerForgotPassword')
        {
            $transportecaResponseAry = \Transporteca\Customer::forgotPassword($apiRequestParams);   
        }
        else if($szMethodType=='customerProileDetails')
        {
            $transportecaResponseAry = \Transporteca\Customer::customerProfileDetails($apiRequestParams);   
        }
        else if($szMethodType=='customerBookings')
        {
            $transportecaResponseAry = \Transporteca\Customer::bookings($apiRequestParams); 
        }
        else if($szMethodType=='customerActiveBookings')
        {
            $transportecaResponseAry = \Transporteca\Customer::activebookings($apiRequestParams); 
        }
        else if($szMethodType=='customerArchivedBookings')
        {
            $transportecaResponseAry = \Transporteca\Customer::archivedtbookings($apiRequestParams); 
        }
        else if($szMethodType=='customerDraftBookings')
        {
            $transportecaResponseAry = \Transporteca\Customer::draftbookings($apiRequestParams); 
        }
        else if($szMethodType=='customerHoldBookings')
        {
            $transportecaResponseAry = \Transporteca\Customer::holdbookings($apiRequestParams); 
        }
        else if($szMethodType=='deleteCustomerAccount')
        {
            $transportecaResponseAry = \Transporteca\Customer::deleteCustomerAccount($apiRequestParams); 
        }
        else if($szMethodType=='getShippers')
        {
            $transportecaResponseAry = \Transporteca\Customer::getShippers($apiRequestParams); 
        }
        else if($szMethodType=='getConsignees')
        {
            $transportecaResponseAry = \Transporteca\Customer::getConsignees($apiRequestParams); 
        }
        else if($szMethodType=='getShipperDetails')
        {
            $transportecaResponseAry = \Transporteca\Customer::getShipperDetails($apiRequestParams); 
        }
        else if($szMethodType=='getConsigneeDetails')
        {
            $transportecaResponseAry = \Transporteca\Customer::getConsigneeDetails($apiRequestParams); 
        }
        else if($szMethodType=='updateShipperDetails')
        {
            $transportecaResponseAry = \Transporteca\Customer::updateShipperDetails($apiRequestParams); 
        }
        else if($szMethodType=='updateConsigneeDetails')
        {
            $transportecaResponseAry = \Transporteca\Customer::updateConsigneeDetails($apiRequestParams); 
        }
        else if($szMethodType=='forwarderRating')
        {
            $transportecaResponseAry = \Transporteca\Service::forwarderRating($apiRequestParams);   
        } 
        else if($szMethodType=='getStandardCargo')
        {
            $transportecaResponseAry = \Transporteca\Service::getStandardCargo($apiRequestParams);   
        } 
        else if($szMethodType=='getInsuranceValue')
        {
            $transportecaResponseAry = \Transporteca\Service::getInsuranceValue($apiRequestParams);   
        }        
        else if($szMethodType=='customerEvents')
        {
            $transportecaResponseAry = \Transporteca\Customer::customerEvents($apiRequestParams); 
        }
        else if($szMethodType=='getQuote')
        {
            $transportecaResponseAry = \Transporteca\Service::getQuote($apiRequestParams);   
        } 
        else if($szMethodType=='getLabels')
        {
            $transportecaResponseAry = \Transporteca\Booking::getLabels($apiRequestParams);   
        }
        else if($szMethodType=='confirmDownload')
        {
            $transportecaResponseAry = \Transporteca\Booking::confirmDownload($apiRequestParams);   
        } 
        else if($szMethodType=='submitNPS')
        {
            $transportecaResponseAry = \Transporteca\Booking::submitNPS($apiRequestParams);   
        } 
        else if($szMethodType=='newsLettersSignup')
        {
            $transportecaResponseAry = \Transporteca\Customer::newsLettersSignup($apiRequestParams); 
        }
    }
    catch (\TransportecaError\Customer $ex)
    { 
        $szExceptionMessage = $ex->getMessage();
    }
    catch (Exception $ex)
    {  
        $szExceptionMessage =  $ex->getMessage();
    }
    
    $szApiEndPoint = \Transporteca\Transporteca::$apiEndPoint;
    $szApiRequestHeaders = \Transporteca\Transporteca::$apiHeader;
    $szApiRequestParams = \Transporteca\Transporteca::$apiParams;
    $szApiResponseParams = \Transporteca\Transporteca::$apiResponse;
    $szApiResponseCode = \Transporteca\Transporteca::$apiResponseCode;
    $szApiResponseHeaders = \Transporteca\Transporteca::$apiResponseHeaders;
    echo "SUCCESS||||";
    echo display_api_response($szApiEndPoint,$szApiRequestHeaders,$szApiRequestParams,$szApiResponseParams,$szApiResponseCode,$szApiResponseHeaders,$szExceptionMessage,$szMethodType);
    die;
}
?>