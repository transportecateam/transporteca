<?php  
session_start();
$szMetaTitle="Transporteca API Demo";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once(__APP_PATH__ . "/inc/constants.php");  
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once(__APP_PATH_LAYOUT__ ."/header_new.php");

$kConfig = new cConfig();
$allCountriesArr = array();
$allCountriesArr=$kConfig->getAllCountries(true,1,true); 

$currencyAry = array();
$currencyAry = $kConfig->getBookingCurrency(false,true); 

$serviceTermsAry = array();
$serviceTermsAry = $kConfig->getAllServiceTerms();

$palletTypesAry = array();
$palletTypesAry = $kConfig->getAllPalletTypes(false,$iLanguage);

$langArr = array();
$langArr = $kConfig->getLanguageDetails();

// geting all weight measure 
$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

// geting all cargo measure 
$cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
    
if(!empty($_SESSION['getCourierRateAry']))
{
    $_POST['getCourierRateAry'] = $_SESSION['getCourierRateAry'] ;
}
if(!empty($_SESSION['cargo']))
{
    $_POST['cargo'] = $_SESSION['cargo'];
} 
$cargoCounter = 0; 
$_POST['szApiKey'] = '3f31e6d2729d90a98024468a3f3b2bf5';
  
$kVatApplication = new cVatApplication();
$manualFeeCargoTypeArr = array();
$manualFeeCargoTypeArr = $kVatApplication->getManualFeeCaroTypeList(); 
    
?>
<div id="hsbody-2"> 
    <style type="text/css">
        .format-2 td {
            background-color: #ffffff;
            font-size: 14px;
            padding: 4px 10px;
            text-align: left;
        }
        .format-2 input{
            width:70%;
        }
        .format-2 select{
            width:75%;
        }
        .api_content {
            background: #f4f4f4 none repeat scroll 0 0;
            border: 1px solid #dadada;
            border-radius: 6px;
            display: table;
            font-size: 100%;
            letter-spacing: 1px;
            margin: 12px auto;
            padding: 5px 0 5px 5px;
            width: 100%;
        }

        .api_subheading_1 {
            background: #ebf3f9 none repeat scroll 0 0;
            border: 1px solid #c3d9ec;
            border-radius: 6px;
            display: block;
            margin: auto;
            overflow: auto;
            padding: 10px;
            width: 92%;
        } 
        .api_subheading {
            background: #e7f6ec none repeat scroll 0 0;
            border: 1px solid #c3e8d1;
            border-radius: 6px;
            display: block;
            margin: auto;
            padding: 10px;
            width: 95%;
        }
        .api_area_heading {
            color: #666;
            font-weight: normal;
        }
        .api_area_heading h3 span {
            display: block;
            padding: 10px 0;
        }
        .api-flo {
            float: left;
            font-size: 13px;
            font-weight: normal;
            margin-left: 16px;
            width: 85%;
        }  
        .api_txtara {
            width: 25%;
        }
        .api_hdng1 {
            color: #333;
            display: block;
            float: left;
            font: bold 13px/1.5 "Helvetica Neue",Arial,"Liberation Sans",FreeSans,sans-serif;
            padding-left: 20px;
            text-align: right;
            width: 12%;
        } 
        .api_dscrarea span {
            border: 0 solid #333;
            float: left;
            font-size: 13px;
            margin-bottom: 3px;
        }
        .api_ori_hdng {
            color: #333;
            display: block;
            float: left;
            font-size: 13px;
            font-weight: bold;
            padding-right: 10px;
            text-align: right;
            width: 13%;
        }
        .api_dp_menu_1 {
            color: #666;
            display: block;
            font-size: 13px;
            font-weight: normal;
            width: 85%;
        }
        .api_clear
        {
            clear:both;
            display:block;
        }
        .required_field{
            float:right;
            color: red;
        }
    </style>
    <div class="hsbody-2-right"> 
        <h2>Transporteca Partner API Demo</h2>
        <br>
        <div id="courier_service_container_div"> 
            <div class="api_content" id="api_response_container" style="display:none;">
                
            </div>
            <form id="get_api_service_form" name="get_api_service_form" method="post">
                <br><h3>Authentication</h3>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr>
                        <td style="width:15%;">API Key <span class="required_field">*</span></td>
                        <td style="width:85%;"><input type="text" style="width:70%;" name="szApiKey" id="szApiKey" value="<?php echo $_POST['szApiKey']; ?>"></td>
                         
                    </tr>
                </table>
                <br><h3>Request</h3> 
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr> 
                        <td style="width:15%;">Request Type <span class="required_field">*</span></td>
                        <td style="width:35%;">
                            <select  name="getCourierRateAry[szRequest]" id="szRequest" >
                                <option value="price" <?php echo (($_POST['getCourierRateAry']['szRequest']=="price")?'selected':''); ?>>Price</option>
                                <option value="booking" <?php echo (($_POST['getCourierRateAry']['szRequest']=="booking")?'selected':''); ?>>Booking</option>
                            </select>
                        </td>
                        <td style="width:15%;">Mode <span class="required_field">*</span></td>
                        <td style="width:35%;">
                            <select  name="szMode" id="szMode">
                                <option value="TEST" <?php echo (($_POST['szMode']=="TEST")?'selected':''); ?>>Test</option>
                                <option value="LIVE" <?php echo (($_POST['szMode']=="LIVE")?'selected':''); ?>>Live</option> 
                            </select>
                        </td>
                    </tr>
                </table>
                <br><h3>Timing</h3>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr> 
                        <td style="width:15%;">Date <span class="required_field">*</span></td>
                        <td style="width:20%;">
                            <select  name="getCourierRateAry[dtDay]" id="dtDay" >
                                <?php
                                for($dd=1;$dd <=31;$dd++)
                                {
                                    ?>
                                    <option value="<?php echo $dd;?>" <?php echo (((int)date('d')==$dd)?'selected':''); ?>><?php echo $dd;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td style="width:15%;">Month <span class="required_field">*</span></td>
                        <td style="width:15%;">
                            <select name="getCourierRateAry[dtMonth]" id="dtMonth" >
                                <?php
                                for($mm=1;$mm<=12;$mm++)
                                {
                                    ?>
                                    <option value="<?php echo $mm;?>" <?php echo (((int)date('m')==$mm)?'selected':''); ?>><?php echo $mm;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td style="width:20%;">Year <span class="required_field">*</span></td>
                        <td style="width:15%;">
                            <select  name="getCourierRateAry[dtYear]" id="dtYear" >
                                <?php
                                for($yyyy=2015;$yyyy<=2025;$yyyy++)
                                {
                                    ?>
                                    <option value="<?php echo $yyyy;?>" <?php echo ((date('Y')==$yyyy)?'selected':''); ?>><?php echo $yyyy;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <br><h3>Customer</h3> 
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr> 
                        <td style="width:15%;">Customer Token <span class="required_field">*</span></td>
                        <td style="width:85%;">
                            <input type="text" style="width:70%;" name="getCourierRateAry[szCustomerToken]" id="szCustomerToken" value="<?php echo $_POST['szCustomerToken']; ?>">
                        </td>
                    </tr>
                </table>
                <br>
                <h2>CARGO (there are three options for submitting cargo details: A, B and C)</h2> 
                <br>
                <h3>A – Send cargo as packages</h3>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <?php for($cargoCounter=0;$cargoCounter<2;$cargoCounter++){ ?> 
                        <tr>
                            <td align="left" style="width:15%;"><strong>Packages Line <?php echo $cargoCounter+1 ; ?></strong></td>
                            <td align="left" style="text-align: left;width:35%;"><input type="checkbox" name="cargo[<?php echo $cargoCounter; ?>][szShipmentType]" <?php if($_POST['cargo'][$cargoCounter]['szShipmentType']=='PARCEL'){ echo 'checked'; } ?> value="PARCEL"></td>
                            <td style="width:15%;">Colli Length</td>
                            <td style="width:35%;"><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iLength]" id="fCargoLength" value="<?php echo $_POST['cargo'][$cargoCounter]['iLength']; ?>" ></td>
                        </tr>
                        <tr> 
                            <td>Colli Width</td>
                            <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iWidth]" value="<?php echo $_POST['cargo'][$cargoCounter]['iWidth']; ?>" id="fCargoWidth"></td>
                            <td>Colli Height</td>
                            <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iHeight]" id="fCargoHeight" value="<?php echo $_POST['cargo'][$cargoCounter]['iHeight']; ?>"></td>
                        </tr>
                        <tr> 
                            <td>Colli Weight</td>
                            <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iWeight]" id="fCargoWeight" value="<?php echo $_POST['cargo'][$cargoCounter]['iWeight']; ?>"></td>
                            <td>Number of colli</td>
                            <td>
                                <input type="text" name="cargo[<?php echo $cargoCounter; ?>][iQuantity]" id="iQuantity" value="<?php echo $_POST['cargo'][$cargoCounter]['iQuantity']; ?>"> 
                            </td>
                        </tr>
                        <tr> 
                            <td>Dimensions unit</td>
                            <td>
                                <select size="1" name="cargo[<?php echo $cargoCounter; ?>][szDimensionMeasure]" id="szDimensionMeasure"> 
                                    <?php
                                        if(!empty($cargoMeasureAry))
                                        {
                                            foreach($cargoMeasureAry as $cargoMeasureArys)
                                            {
                                                ?>
                                                <option value="<?php echo strtoupper($cargoMeasureArys['szDescription']); ?>"><?php echo $cargoMeasureArys['szDescription']; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                               </select>
                            </td>
                            <td>Weight unit</td>
                            <td>
                                <select size="1" name="cargo[<?php echo $cargoCounter; ?>][szWeightMeasure]" id="szWeightMeasure"> 
                                    <?php
                                        if(!empty($weightMeasureAry))
                                        {
                                            foreach($weightMeasureAry as $weightMeasureArys)
                                            {
                                                ?>
                                                <option value="<?php echo strtoupper($weightMeasureArys['szDescription']); ?>"><?php echo $weightMeasureArys['szDescription']; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                               </select>
                            </td>
                        </tr>
                        <tr><td colspan="4">&nbsp;</td></tr>
                    <?php } ?>  
                </table>
                <br> 
                <?php  $iNewLoopCounter = $cargoCounter+2;$ctr = 1; ?>
                <h3>B – Send cargo as pallets</h3>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <?php for($cargoCounter=$cargoCounter;$cargoCounter<$iNewLoopCounter;$cargoCounter++){ ?> 
                        <tr>
                            <td align="left" style="width:15%;"><strong>Pallet Line <?php echo $ctr++; ?></strong></td>
                            <td align="left" style="text-align: left;width:35%"><input type="checkbox" name="cargo[<?php echo $cargoCounter; ?>][szShipmentType]" <?php if($_POST['cargo'][$cargoCounter]['szShipmentType']=='PALLET'){ echo 'checked'; } ?> id="szShipmentType" value="PALLET"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Pallet Type</td>
                            <td>
                                <select name="cargo[<?php echo $cargoCounter; ?>][szPalletType]" id="szPalletType">  
                                    <option value="Euro" >Euro</option>
                                    <option value="Half" >Half</option>
                                    <option value="Other" >Other</option>
                                </select> 
                            </td>
                            <td>Pallet Length</td>
                            <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iLength]" id="fCargoLength" value="<?php echo $_POST['cargo'][$cargoCounter]['iLength']; ?>" ></td> 
                        </tr>
                        <tr>
                            <td>Pallet Width</td>
                            <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iWidth]" id="fCargoWidth" value="<?php echo $_POST['cargo'][$cargoCounter]['iWidth']; ?>"></td>
                            <td>Pallet Height</td>
                            <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iHeight]" id="fCargoHeight" value="<?php echo $_POST['cargo'][$cargoCounter]['iHeight']; ?>"></td> 
                        </tr>
                        <tr>
                            <td>Pallet Weight</td>
                            <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][iWeight]" id="fCargoWeight" value="<?php echo $_POST['cargo'][$cargoCounter]['iWeight']; ?>"></td>
                            <td>Number of Pallets</td>
                            <td>
                                <input type="text" name="cargo[<?php echo $cargoCounter; ?>][iQuantity]" id="iQuantity" value="<?php echo $_POST['cargo'][$cargoCounter]['iQuantity']; ?>"> 
                            </td> 
                        </tr>
                        <tr>
                            <td>Dimensions unit</td>
                            <td>
                                <select size="1" name="cargo[<?php echo $cargoCounter; ?>][szDimensionMeasure]" id="szDimensionMeasure"> 
                                    <?php
                                        if(!empty($cargoMeasureAry))
                                        {
                                            foreach($cargoMeasureAry as $cargoMeasureArys)
                                            {
                                                ?>
                                                <option value="<?php echo strtoupper($cargoMeasureArys['szDescription']); ?>"><?php echo $cargoMeasureArys['szDescription']; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                               </select>
                            </td>
                            <td>Weight unit</td>
                            <td>
                                <select size="1" name="cargo[<?php echo $cargoCounter; ?>][szWeightMeasure]" id="szWeightMeasure"> 
                                    <?php
                                        if(!empty($weightMeasureAry))
                                        {
                                            foreach($weightMeasureAry as $weightMeasureArys)
                                            {
                                                ?>
                                                <option value="<?php echo strtoupper($weightMeasureArys['szDescription']); ?>"><?php echo $weightMeasureArys['szDescription']; ?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                               </select>
                            </td>
                        </tr>
                        <tr><td colspan="4">&nbsp;</td></tr>
                    <?php } ?> 
                </table>
                <br>  
                <h3>C – Send cargo as break bulk</h3>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr>
                        <td align="left" style="width:15%;"><strong>Break bulk</strong></td>
                        <td align="left" style="text-align: left;width:35%;"><input style="float:left;margin-left: 10px;" type="checkbox" name="cargo[<?php echo $cargoCounter; ?>][szShipmentType]" <?php if($_POST['cargo'][$cargoCounter]['szShipmentType']=='BREAK_BULK'){ echo 'checked'; } ?> id="szShipmentType" value="BREAK_BULK"></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Total Volume</td>
                        <td><input type="text" name="cargo[<?php echo $cargoCounter; ?>][fTotalVolume]" id="fTotalVolume" value="<?php echo $_POST['cargo']['fTotalVolume']; ?>" ></td>
                        <td>Total Weight</td>
                        <td>
                            <input type="text" name="cargo[<?php echo $cargoCounter; ?>][fTotalWeight]" id="fTotalWeight" value="<?php echo $_POST['cargo']['fTotalWeight']; ?>"> 
                        </td>
                    </tr>
                    <tr>
                        <td>Dimensions unit</td>
                        <td>
                            <select size="1" name="cargo[<?php echo $cargoCounter; ?>][szDimensionMeasure]" id="szDimensionMeasure"> 
                                <option value="CBM"><?php echo 'cbm'; ?></option> 
                           </select>
                        </td>
                        <td>Weight unit</td>
                        <td>
                            <select size="1" name="cargo[<?php echo $cargoCounter; ?>][szWeightMeasure]" id="szWeightMeasure"> 
                                <option value="KG"><?php echo 'kg'; ?></option> 
                           </select>
                        </td>
                    </tr>
                </table>
                <h3>D – Send unknown cargo</h3>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr>
                        <td align="left" style="width:15%;"><strong>Unknown</strong></td>
                        <td align="left" style="text-align: left;width:35%;"><input style="float:left;margin-left: 10px;" type="checkbox" name="cargo[<?php echo $cargoCounter; ?>][szShipmentType]" <?php if($_POST['cargo'][$cargoCounter]['szShipmentType']=='UNKNOWN'){ echo 'checked'; } ?> id="szShipmentType" value="UNKNOWN"></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr>
                        <td style="width:15%;">Trading Term </td>
                        <td style="width:35%;">
                            <select name="getCourierRateAry[szServiceTerm]" style="width:70%;"  id="szServiceTerm">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($serviceTermsAry))
                                    {
                                       foreach($serviceTermsAry as $serviceTermsArys)
                                       {
                                        ?>
                                        <option value="<?php echo $serviceTermsArys['szShortTerms']; ?>" <?php echo (($serviceTermsArys['szShortTerms']==$idServiceTerms)?'selected':''); ?>><?php echo $serviceTermsArys['szShortTerms']; ?></option>
                                        <?php
                                       }
                                    }
                                ?> 
                                <option value="DTD" <?php echo (($serviceTermsArys['szShortTerms']=='DTD')?'selected':''); ?>>DTD</option>
                            </select>
                        </td>
                        <td>Cargo Description</td>
                        <td>
                            <input type="text" name="getCourierRateAry[szCargoDescription]" id="szCargoDescription" value="<?php echo $_POST['getCourierRateAry']['szCargoDescription']; ?>" >
                        </td>
                     </tr>
                     <tr>
                        <td>Cargo Value </td>
                        <td><input type="text" name="getCourierRateAry[fCargoValue]" id="fCargoValue" value="<?php echo $_POST['getCourierRateAry']['fCargoValue']; ?>"></td>
                        <td>Cargo Currency </td>
                        <td>
                            <select name="getCourierRateAry[szCargoCurrency]" id="szCargoCurrency">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($currencyAry))
                                    {
                                        foreach($currencyAry as $currencyArys)
                                        {
                                            ?>
                                            <option value="<?php echo $currencyArys['szCurrency']?>" <?php echo (($currencyArys['szCurrency']==$_POST['getCourierRateAry']['szCargoCurrency'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                            <?php
                                        }
                                    }
                                 ?>
                            </select>
                        </td>
                     </tr>
                     <tr> 
                        <td>Cargo Commodity </td>
                        <td>
                            <select name="getCourierRateAry[szCommodity]" id="szCommodity">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($manualFeeCargoTypeArr))
                                    {
                                        foreach($manualFeeCargoTypeArr as $manualFeeCargoTypeArrs)
                                        {
                                            ?>
                                            <option value="<?php echo $manualFeeCargoTypeArrs['id']; ?>" <?php echo (($manualFeeCargoTypeArrs['id']==$_POST['getCourierRateAry']['szCommodity'])?'selected':''); ?>><?=$manualFeeCargoTypeArrs['szName']; ?></option>
                                            <?php
                                        }
                                    }
                                 ?>
                            </select>
                        </td>
                        <td>Customer Type</td>
                        <td align="left">
                            <span class="checkbox-container">
                                <input type="checkbox" name="getCourierRateAry[customerType][0]" <?php if($_POST['getCourierRateAry']['customerType']['0']=='PRIVATE'){ echo 'checked'; } ?> id="customerType_0" value="PRIVATE"> &nbsp; Private<br>
                            </span>
                            <span class="checkbox-container">
                                <input type="checkbox" name="getCourierRateAry[customerType][1]" <?php if($_POST['getCourierRateAry']['customerType']['1']=='BUSINESS'){ echo 'checked'; } ?> id="customerType_1" value="BUSINESS"> &nbsp; Business
                            </span> 
                        </td>
                     </tr>
                </table>
                <br>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr>
                        <td colspan="2"><strong>Shipper</strong></td>
                        <td colspan="2"><strong>Consignee</strong></td> 
                    </tr>
                    <tr>
                        <td style="width:15%;">Company Name</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szShipperCompanyName]" id="szShipperCompanyName" value="<?php echo $_POST['getCourierRateAry']['szShipperCompanyName']; ?>" ></td>
                        <td style="width:15%;">Company Name</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szConsigneeCompanyName]" id="szConsigneeCompanyName" value="<?php echo $_POST['getCourierRateAry']['szConsigneeCompanyName']; ?>" ></td>
                    </tr>
                    <tr>
                        <td style="width:15%;">First Name</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szShipperFirstName]" id="szShipperFirstName" value="<?php echo $_POST['getCourierRateAry']['szShipperFirstName']; ?>" ></td>
                        <td style="width:15%;">First Name</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szConsigneeFirstName]" id="szConsigneeFirstName" value="<?php echo $_POST['getCourierRateAry']['szConsigneeFirstName']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input type="text" name="getCourierRateAry[szShipperLastName]" id="szShipperLastName" value="<?php echo $_POST['getCourierRateAry']['szShipperLastName']; ?>" ></td>
                        <td>Last Name</td>
                        <td><input type="text" name="getCourierRateAry[szConsigneeLastName]" id="szConsigneeLastName" value="<?php echo $_POST['getCourierRateAry']['szConsigneeLastName']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>Email Id</td>
                        <td><input type="text" name="getCourierRateAry[szShipperEmail]" id="szShipperEmail" value="<?php echo $_POST['getCourierRateAry']['szShipperEmail']; ?>" ></td>
                        <td>Email Id</td>
                        <td><input type="text" name="getCourierRateAry[szConsigneeEmail]" id="szConsigneeEmail" value="<?php echo $_POST['getCourierRateAry']['szConsigneeEmail']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>phone</td>
                        <td>
                            <select name="getCourierRateAry[iShipperCountryDialCode]" style="width:25%;" id="iShipperCountryDialCode" >
                                <option value="">Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                    foreach($allCountriesArr as $allCountriesArrs)
                                    {
                                        if(empty($usedDialCode) || !in_array($allCountriesArrs['iInternationDialCode'],$usedDialCode))
                                        {
                                            $usedDialCode[] = $allCountriesArrs['iInternationDialCode'] ;
                                        ?>
                                        <option value="<?php echo $allCountriesArrs['iInternationDialCode']; ?>" <?php echo (($_POST['getCourierRateAry']['iShipperCountryDialCode']==$allCountriesArrs['iInternationDialCode'])?'selected':''); ?>>+<?php echo $allCountriesArrs['iInternationDialCode']?></option>
                                        <?php
                                        }
                                    }
                                }
                             ?>
                            </select>&nbsp;&nbsp;<input type="text" name="getCourierRateAry[iShipperPhoneNumber]" style="width:60%;" id="iShipperPhoneNumber" value="<?php echo $_POST['getCourierRateAry']['iShipperPhoneNumber']; ?>" >
                        </td>
                        <td>Phone</td>
                        <td>
                            <select name="getCourierRateAry[iConsigneeCountryDialCode]" style="width:25%;" id="iConsigneeCountryDialCode">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($allCountriesArr))
                                    {
                                        $usedDialCode = array();
                                        foreach($allCountriesArr as $allCountriesArrs)
                                        {
                                            if(empty($usedDialCode) || !in_array($allCountriesArrs['iInternationDialCode'],$usedDialCode))
                                            {
                                                $usedDialCode[] = $allCountriesArrs['iInternationDialCode'] ;
                                            ?>
                                            <option value="<?php echo $allCountriesArrs['iInternationDialCode']; ?>" <?php echo (($_POST['getCourierRateAry']['iConsigneeCounrtyDialCode']==$allCountriesArrs['iInternationDialCode'])?'selected':''); ?>>+<?php echo $allCountriesArrs['iInternationDialCode']?></option>
                                            <?php
                                            }
                                        }
                                    }
                                 ?>
                            </select>&nbsp;&nbsp;<input type="text" name="getCourierRateAry[iConsigneePhoneNumber]" style="width:60%;" id="iConsigneePhoneNumber" value="<?php echo $_POST['getCourierRateAry']['iConsigneePhoneNumber']; ?>" >
                        </td>
                    </tr> 
                    <tr>
                        <td>Address</td>
                        <td><input type="text" name="getCourierRateAry[szShipperAddress]" id="szShipperAddress" value="<?php echo $_POST['getCourierRateAry']['szShipperAddress']; ?>" ></td>
                        <td>Address</td>
                        <td><input type="text" name="getCourierRateAry[szConsigneeAddress]" id="szConsigneeAddress" value="<?php echo $_POST['getCourierRateAry']['szConsigneeAddress']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>Postcode</td>
                        <td><input type="text" name="getCourierRateAry[szShipperPostcode]" id="szShipperPostcode" value="<?php echo $_POST['getCourierRateAry']['szShipperPostcode']; ?>"  ></td>
                        <td>Postcode</td>
                        <td><input type="text" name="getCourierRateAry[szConsigneePostcode]" id="szConsigneePostcode" value="<?php echo $_POST['getCourierRateAry']['szConsigneePostcode']; ?>"  ></td>
                     </tr>
                     <tr>
                        <td>City <span class="required_field">*</span></td>
                        <td><input type="text" name="getCourierRateAry[szShipperCity]" id="szShipperCity" value="<?php echo $_POST['getCourierRateAry']['szShipperCity']; ?>" ></td>
                        <td>City <span class="required_field">*</span></td>
                        <td><input type="text" name="getCourierRateAry[szConsigneeCity]" id="szConsigneeCity" value="<?php echo $_POST['getCourierRateAry']['szConsigneeCity']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>Country <span class="required_field">*</span></td>
                        <td>
                            <?php 
                                if(!empty($allCountriesArr))
                                {
                                    $allCountriesArr = sortArray($allCountriesArr,'szCountryName');
                                }
                            ?>
                            <select   name="getCourierRateAry[szShipperCountry]" id="szShipperCountry" >
                                <option value="">Select</option>
                                <?php
                                    if(!empty($allCountriesArr))
                                    {
                                        foreach($allCountriesArr as $allCountriesArrs)
                                        {
                                            ?>
                                            <option value="<?php echo $allCountriesArrs['szCountryISO']; ?>" <?php echo (($_POST['getCourierRateAry']['szShipperCountry']==$allCountriesArrs['szCountryISO'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                            <?php
                                        }
                                    }
                                 ?>
                            </select>
                        </td>
                        <td>Country <span class="required_field">*</span></td>
                        <td>
                            <select name="getCourierRateAry[szConsigneeCountry]" id="szConsigneeCountry" >
                                <option value="">Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                    foreach($allCountriesArr as $allCountriesArrs)
                                    {
                                        ?>
                                        <option value="<?php echo $allCountriesArrs['szCountryISO']; ?>" <?php echo (($_POST['getCourierRateAry']['szConsigneeCountry']==$allCountriesArrs['szCountryISO'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                        <?php
                                    }
                                }
                             ?>
                            </select>
                        </td>
                     </tr>   
                </table> 
                <br>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr>
                        <td colspan="3"><strong>Billing Address</strong></td> 
                    </tr>
                    <tr>
                        <td style="width:15%;">Company Name</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szBillingCompanyName]" id="szBillingCompanyName" value="<?php echo $_POST['getCourierRateAry']['szBillingCompanyName']; ?>" ></td> 
                        <td style="width:50%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>First Name</td>
                        <td><input type="text" name="getCourierRateAry[szBillingFirstName]" id="szBillingFirstName" value="<?php echo $_POST['getCourierRateAry']['szBillingFirstName']; ?>" ></td> 
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input type="text" name="getCourierRateAry[szBillingLastName]" id="szBillingLastName" value="<?php echo $_POST['getCourierRateAry']['szBillingLastName']; ?>" ></td> 
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Email Id</td>
                        <td><input type="text" name="getCourierRateAry[szBillingEmail]" id="szBillingEmail" value="<?php echo $_POST['getCourierRateAry']['szBillingEmail']; ?>" ></td> 
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>phone</td>
                        <td> 
                            <select name="getCourierRateAry[iBillingCountryDialCode]" style="width:25%;" id="iBillingCountryDialCode" >
                                <option value="">Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                    $allCountriesArr = sortArray($allCountriesArr,'iInternationDialCode');
                                    $usedDialCode = array();
                                    foreach($allCountriesArr as $allCountriesArrs)
                                    {
                                        if(empty($usedDialCode) || !in_array($allCountriesArrs['iInternationDialCode'],$usedDialCode))
                                        {
                                            $usedDialCode[] = $allCountriesArrs['iInternationDialCode'] ;
                                        ?>
                                        <option value="<?php echo $allCountriesArrs['iInternationDialCode']; ?>" <?php echo (($_POST['getCourierRateAry']['iBillingCountryDialCode']==$allCountriesArrs['iInternationDialCode'])?'selected':''); ?>>+<?php echo $allCountriesArrs['iInternationDialCode']?></option>
                                        <?php
                                        }
                                    }
                                }
                             ?>
                            </select>&nbsp;&nbsp;<input type="text" name="getCourierRateAry[iBillingPhoneNumber]" style="width:60%;" id="iBillingPhoneNumber" value="<?php echo $_POST['getCourierRateAry']['iBillingPhoneNumber']; ?>" >
                        </td> 
                        <td>&nbsp;</td>
                    </tr> 
                    <tr>
                        <td>Address</td>
                        <td><input type="text" name="getCourierRateAry[szBillingAddress]" id="szBillingAddress" value="<?php echo $_POST['getCourierRateAry']['szBillingAddress']; ?>" ></td> 
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Postcode</td>
                        <td><input type="text" name="getCourierRateAry[szBillingPostcode]" id="szBillingPostcode" value="<?php echo $_POST['getCourierRateAry']['szBillingPostcode']; ?>"  ></td> 
                        <td>&nbsp;</td>
                     </tr>
                     <tr>
                        <td>City</td>
                        <td><input type="text" name="getCourierRateAry[szBillingCity]" id="szBillingCity" value="<?php echo $_POST['getCourierRateAry']['szBillingCity']; ?>" ></td> 
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td>
                            <?php 
                                if(!empty($allCountriesArr))
                                {
                                    $allCountriesArr = sortArray($allCountriesArr,'szCountryName');
                                }
                            ?>
                            <select   name="getCourierRateAry[szBillingCountry]" id="szBillingCountry" >
                                <option value="">Select</option>
                                <?php
                                    if(!empty($allCountriesArr))
                                    {
                                        foreach($allCountriesArr as $allCountriesArrs)
                                        {
                                            ?>
                                            <option value="<?php echo $allCountriesArrs['szCountryISO']; ?>" <?php echo (($_POST['getCourierRateAry']['szBillingCountry']==$allCountriesArrs['szCountryISO'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                            <?php
                                        }
                                    }
                                 ?>
                            </select>
                        </td> 
                        <td>&nbsp;</td>
                     </tr>   
                </table> 
                <h3>Service</h3>
                <table class="format-2" style="width:100%;" cellpadding="5">
                    <tr>
                        <td style="width:15%;">Service Reference</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szServiceID]" id="szServiceID" value="<?php echo $_POST['getCourierRateAry']['szServiceID']; ?>" ></td>
                        <td style="width:15%;">Reference Token</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[token]" id="token" value="<?php echo $_POST['getCourierRateAry']['token']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>Insurance required</td>
                        <td>
                            <select name="getCourierRateAry[bInsuranceRequired]"  id="bInsuranceRequired"> 
                                <option value="No">No</option> 
                                <option value="Yes">Yes</option> 
                            </select>
                        </td>
                        <td style="width:15%;">Insurance Price</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szInsurancePrice]" id="szInsurancePrice" value="<?php echo $_POST['getCourierRateAry']['szInsurancePrice']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>Response language</td>
                        <td>
                             <select name="getCourierRateAry[szLanguage]"  id="szLanguage"> 
                                <?php
                                    if(!empty($langArr))
                                    {
                                        foreach($langArr as $langArrs)
                                        {?>
                                            <option value="<?php echo strtoupper($langArrs['szLanguageCode']);?>"><?php echo ucfirst($langArrs['szName']);?></option> 
                                           <?php 
                                        }
                                    }
                                    ?>
                                 
<!--                                <option value="DA">Danish</option> -->
                            </select>
                        </td>
                        <td style="width:15%;">Your Reference</td>
                        <td style="width:35%;"><input type="text" name="getCourierRateAry[szReference]" id="szReference" value="<?php echo $_POST['getCourierRateAry']['szReference']; ?>" ></td>
                     </tr> 
                     <tr>
                        <td>Customer Currency</td>
                        <td>
                            <select name="getCourierRateAry[szCurrency]" id="szCurrency">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($currencyAry))
                                    {
                                        foreach($currencyAry as $currencyArys)
                                        {
                                            ?>
                                            <option value="<?php echo $currencyArys['szCurrency']?>" <?php echo (($currencyArys['id']==$searchResult[0]['idCurrency'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                            <?php
                                        }
                                    }
                                 ?>
                            </select>
                        </td>
                        <td style="width:15%;">&nbsp;</td>
                        <td style="width:15%;">&nbsp;</td>
                    </tr>
                </table>
                <br>
                <div>Note: Fields marked as [ <span style="color:red;">*</span> ] is mandatory field</div>
                <div style="text-align:center;">
                    <input type="button" name="getCourierRateAry[szSubmit]" value="Get Details" onclick="submit_partner_api_demo_form();"> 
                </div>
            </form>
        </div>
    </div>
</div>

<?php
//include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
