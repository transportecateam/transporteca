<!DOCTYPE>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Transporteca Api Documentation</title>
<link href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/api_style.css" rel="stylesheet" type="text/css">
<style>
	html, body { width:100%; height:100%;}
</style>
<script type="text/javascript" language="javascript">
function show(id)
{
    $('#'+id).show('slow');
}
function hide(id)
{//alert(id);
    $('#'+id).hide('slow');
}
</script>
</head>

<body>
	<header class="api_header">
                <div class="headerara">
                    <h1 class="vhc api_logo">Transporteca Api Documentation</h1>
                </div>
	</header>
     
        <div class="api_contentarea">
            <div id="user" align="center" class="api_headr_path">Client Request</div>
		 <!--API 1-->
		 <div id="path_0">
                    <h3 class="api_content">   
                        <span class="api_area_heading">
                                <h3>
                                    <span class="api_hdng1">Path :</span>
                                    <span class="inpt1 api-flo">http://dev.transporteca.com/api/services</span>
                                </h3>
                                <h3>
                                    <span class="api_hdng1">Description :</span>
                                    <span class="api_txtara dscrarea_44 api-flo">Client's unique Api key pass in header to authenticate their account. </span>
                                </h3>
                        </span>
                        <div id="path_0_operation">
                            <div id="path_0_operation_delete_0">
                                <span class="api_area_heading" id="register">

                                </span>
                                <span class="api_subheading_1" id="path_0_operation_show_0">
                                    <h3 class="api_dscrarea" align="left">
                                        <span class="ori_stxt">

                                             <span class="api_ori_hdng">HTTP Method :</span>
                                             <span class="api_dp_menu_1">POST</span>

                                                 <span class="api_clear"></span>
                                             <span class="api_ori_hdng">Summary :</span>
                                             <span class="inpt_api">Account Authentication.</span>
                                            <span class="api_clear"></span>
                                         </span>
                                    </h3>
                                    <div id="path_0_operation_0_parameters_id">
                                        <div id="path_0_operation_0_parameters_delete_">
                                            <span class="api_pad"></span>
                                            <span class="api_subheading" id="path_0_operation_0_parameters_show">                        
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_da_api dscrarea_1">Sample Request :</span>                                                                
                                                    <span class="api_txtara_api_6 dscrarea_4">string</span>
                                                    <span class="api_txtara_api dscrarea_4">
<pre> 
{ 
    "X-API-KEY":"3f31e6d2729d90a98024468a3f3b2bf5" 
} 
</pre>
                                                    </span>
                                                </h3>       
                                            </span>
                                        </div>
                                    </div>
                                    <span id="content"></span>
                                    <div id="path_9_operation_0_Errparameters_id">
                                        <span class="api_pad"></span>
                                            <span class="api_subheading_error" id="path_9_operation_0_Errparameters_show_">
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">200</span>  <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">SUCCESS</span></h3>
                                                    </span>
                                                </h3>
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">401</span>   <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">UNAUTHORIZED ACCESS</span></h3>
                                                    </span>
                                                </h3>
                                            </span>
                                    </div>
                                    <span class="api_txtara1 dscrarea_4 api_padng">
                                        <code style="line-height: 1;font-weight: normal;">
<pre>
    {
        "code":"200"
    } 
    or
    {
        "code":"401"
        "description":"Authentication credentials were missing or incorrect."
    }
</pre>
                                        </code>
                                    </span>
                                </span>
                            </div> 
                        </div>
                  </h3> 
                </div>
            <!--API 1 end-->
        </div>
    <!--CARGO FILED DISCRIPTIONS-->
        <div class="api_contentarea">
            <div id="user" align="center" class="api_headr_path">Cargo Selection</div>
		 <!--CARGO AS BREAK BULK-->
		 <div id="path_1">
                    <h3 class="api_content">   
                        <span class="api_area_heading">
                                <h3>
                                    <span class="api_hdng1">Path :</span>
                                    <span class="inpt1 api-flo">http://dev.transporteca.com/api/services</span>
                                </h3>
                                <h3>
                                    <span class="api_hdng1">Description :</span>
                                    <span class="api_txtara dscrarea_44 api-flo">CARGO (there are three options for submitting cargo details: A, B and C).</span>
                                </h3>
                        </span>
                        <div id="user" style="width:985px;margin-left:3%" class="api_headr_path">Case A : Cargo as Break Bulk.</div>
                        <div id="path_0_operation">
                            <div id="path_0_operation_delete_0">
                                <span class="api_area_heading" id="register">

                                </span>
                                <span class="api_subheading_1" id="path_0_operation_show_0">
                                    <h3 class="api_dscrarea" align="left">
                                        <span class="ori_stxt">

                                             <span class="api_ori_hdng">HTTP Method :</span>
                                             <span class="api_dp_menu_1">POST</span>

                                                 <span class="api_clear"></span>
                                             <span class="api_ori_hdng">Summary :</span>
                                             <span class="inpt_api">Cargo as Break Bulk.</span>
                                            <span class="api_clear"></span>
                                         </span>
                                    </h3>
                                    <div id="path_0_operation_0_parameters_id">
                                        <div id="path_0_operation_0_parameters_delete_">
                                            <span class="api_pad"></span>
                                            <span class="api_subheading" id="path_0_operation_0_parameters_show">                        
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_da_api dscrarea_1">Sample Request :</span>                                                                
                                                    <span class="api_txtara_api_6 dscrarea_4">string</span>
                                                    <span class="api_txtara_api dscrarea_4">
<pre> 
{ 
    "szPacketType":"BREAK_BULK",
    "fTotalVolume":"4.5",
    "fTotalWeight":"350"
} 
</pre>
                                                    </span>
                                                </h3>       
                                            </span>
                                        </div>
                                    </div>
                                    <span id="content"></span>
                                    <div id="path_9_operation_0_Errparameters_id">
                                        <span class="api_pad"></span>
                                            <span class="api_subheading_error" id="path_9_operation_0_Errparameters_show_">
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">200</span>  <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">SUCCESS</span></h3>
                                                    </span>
                                                </h3>
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">404</span>   <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">BAD REQUEST</span></h3>
                                                    </span>
                                                </h3>
                                            </span>
                                    </div>
                                    <span class="api_txtara1 dscrarea_4 api_padng">
                                        <code style="line-height: 1;font-weight: normal;">
<pre>
{
    "code":"200",
    "status":"OK",
    "token":"TTPJG5K5DPFBX2VRGS0C",
    "services":
                [
                    {
                        "szServiceID":"1612TTP92549",
                        "dtDeliveryDay":"23",
                        "dtDeliveryMonth":"12",
                        "dtDeliveryYear":"2015",
                        "szPackingType":"Cartons",
                        "szCurrency":"USD",
                        "fBookingPrice":3073,
                        "fVatAmount":768.25,
                        "bInsuranceAvailable":"N"
                    }
                ]
} 
or
{
    code:"404"
    "status":"Error",
    "token":"TT1AIJDMOVZU0IXX6AOF",
    "errors":
                [
                    {
                        "code":"E1011",
                        "description":"Cargo volume is required"
                    }
                ]
}
</pre>
                                        </code>
                                    </span>
                                </span>
                            </div> 
                        </div>
                        
                        <!--CARGO AS PALLETS-->
                        <br>
                        <div id="user" style="width:985px;margin-left:3%" class="api_headr_path">Case B : Cargo as Pallets.</div>
                        <div id="path_0_operation">
                            <div id="path_0_operation_delete_0">
                                <span class="api_area_heading" id="register">

                                </span>
                                <span class="api_subheading_1" id="path_0_operation_show_0">
                                    <h3 class="api_dscrarea" align="left">
                                        <span class="ori_stxt">

                                             <span class="api_ori_hdng">HTTP Method :</span>
                                             <span class="api_dp_menu_1">POST</span>

                                                 <span class="api_clear"></span>
                                             <span class="api_ori_hdng">Summary :</span>
                                             <span class="inpt_api">Cargo as Pallets.</span>
                                            <span class="api_clear"></span>
                                         </span>
                                    </h3>
                                    <div id="path_0_operation_0_parameters_id">
                                        <div id="path_0_operation_0_parameters_delete_">
                                            <span class="api_pad"></span>
                                            <span class="api_subheading" id="path_0_operation_0_parameters_show">                        
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_da_api dscrarea_1">Sample Request :</span>                                                                
                                                    <span class="api_txtara_api_6 dscrarea_4">string</span>
                                                    <span class="api_txtara_api dscrarea_4">
<pre> 
{ 
    
    "szPacketType":"PALLET",
    "cargo":
                [
                    "szPalletType[0]":"Euro",
                    "iHeight[0]":"1.1",
                    "iWeight[0]":"10",
                    "iQuantity[0]":"5"
                ]
    
} 
</pre>
                                                    </span>
                                                </h3>       
                                            </span>
                                        </div>
                                    </div>
                                    <span id="content"></span>
                                    <div id="path_9_operation_0_Errparameters_id">
                                        <span class="api_pad"></span>
                                            <span class="api_subheading_error" id="path_9_operation_0_Errparameters_show_">
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">200</span>  <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">SUCCESS</span></h3>
                                                    </span>
                                                </h3>
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">404</span>   <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">BAD REQUEST</span></h3>
                                                    </span>
                                                </h3>
                                            </span>
                                    </div>
                                    <span class="api_txtara1 dscrarea_4 api_padng">
                                        <code style="line-height: 1;font-weight: normal;">
<pre>
{
    "code":"200",
    "status":"OK",
    "token":"TTPVBOT1JADIDJYIYH73",
    "services":
                [
                    {
                        "szServiceID":"1612TTP92554",
                        "dtDeliveryDay":"23",
                        "dtDeliveryMonth":"12",
                        "dtDeliveryYear":"2015",
                        "szPackingType":"Cartons",
                        "szCurrency":"USD",
                        "fBookingPrice":213,
                        "fVatAmount":53.25,
                        "bInsuranceAvailable":"N"
                    }
                ]
} 
or
{
    code:"404"
    "status":"Error",
    "token":"TT1AIJDMOVZU0IXX6AOF",
    "errors":
                [
                    {
                        "code":"E1060",
                        "description":"Height must be greater then 0"
                    },
                    {
                        "code":"E1063",
                        "description":"Quantity must be greater then 0"
                    }
                ]
}
</pre>
                                        </code>
                                    </span>
                                </span>
                            </div> 
                        </div>
                        <!--CARGO AS PACKAGE-->
                        <br>
                        <div id="user" style="width:985px;margin-left:3%" class="api_headr_path">Case C : Cargo as Packages.</div>
                        <div id="path_0_operation">
                            <div id="path_0_operation_delete_0">
                                <span class="api_area_heading" id="register">

                                </span>
                                <span class="api_subheading_1" id="path_0_operation_show_0">
                                    <h3 class="api_dscrarea" align="left">
                                        <span class="ori_stxt">

                                             <span class="api_ori_hdng">HTTP Method :</span>
                                             <span class="api_dp_menu_1">POST</span>

                                                 <span class="api_clear"></span>
                                             <span class="api_ori_hdng">Summary :</span>
                                             <span class="inpt_api">Cargo as Packages.</span>
                                            <span class="api_clear"></span>
                                         </span>
                                    </h3>
                                    <div id="path_0_operation_0_parameters_id">
                                        <div id="path_0_operation_0_parameters_delete_">
                                            <span class="api_pad"></span>
                                            <span class="api_subheading" id="path_0_operation_0_parameters_show">                        
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_da_api dscrarea_1">Sample Request :</span>                                                                
                                                    <span class="api_txtara_api_6 dscrarea_4">string</span>
                                                    <span class="api_txtara_api dscrarea_4">
<pre> 
{ 
    
    "szPacketType":"PARCEL",
    "cargo":
                [
                    "iLength[0]":"60",
                    "iHeight[0]":"40",
                    "iWeight[0]":"50",
                    "szDimensionMeasure[0]":"cm"
                ]
    
} 
</pre>
                                                    </span>
                                                </h3>       
                                            </span>
                                        </div>
                                    </div>
                                    <span id="content"></span>
                                    <div id="path_9_operation_0_Errparameters_id">
                                        <span class="api_pad"></span>
                                            <span class="api_subheading_error" id="path_9_operation_0_Errparameters_show_">
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">200</span>  <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">SUCCESS</span></h3>
                                                    </span>
                                                </h3>
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">404</span>   <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">BAD REQUEST</span></h3>
                                                    </span>
                                                </h3>
                                            </span>
                                    </div>
                                    <span class="api_txtara1 dscrarea_4 api_padng">
                                        <code style="line-height: 1;font-weight: normal;">
<pre>
{
    "code":"200",
    "status":"OK",
    "token":"TTPCB9OKFTNTXOLYB8JY",
    "services": 
                [
                    {
                        "szServiceID":"1612TT1122841",
                        "fBookingPrice":3621,
                        "fVatAmount":905.25,
                        "bInsuranceAvailable":"N"
                    }
                ]
} 
or
{
    "code":"404",
    "status":"Error",
    "token":"TT1Y22ZFUELVUSLFMNUY",
    "errors":
                [
                    {
                        "code":"E1057",
                        "description":"Width must be greater then 0"
                    },
                    {
                        "code":"E1060",
                        "description":"Height must be greater then 0"
                    }
                ]
}
</pre>
                                        </code>
                                    </span>
                                </span>
                            </div> 
                        </div>
                  </h3> 
                </div>
            <!--API 1 end-->
        </div>
    
<!--        Shipper Detail-->
        <br>
        <div class="api_contentarea">
            <div id="user" align="center" class="api_headr_path">Shipper & Consignee Detail</div>
		 <!--API 1-->
		 <div id="path_0">
                    <h3 class="api_content">   
                        <span class="api_area_heading">
                                <h3>
                                    <span class="api_hdng1">Path :</span>
                                    <span class="inpt1 api-flo">http://dev.transporteca.com/api/services</span>
                                </h3>
                                <h3>
                                    <span class="api_hdng1">Description :</span>
                                    <span class="api_txtara dscrarea_44 api-flo">Submit Shipper and Consignee's detail for booking. </span>
                                </h3>
                        </span>
                        <div id="path_0_operation">
                            <div id="path_0_operation_delete_0">
                                <span class="api_area_heading" id="register">

                                </span>
                                <span class="api_subheading_1" id="path_0_operation_show_0">
                                    <h3 class="api_dscrarea" align="left">
                                        <span class="ori_stxt">

                                             <span class="api_ori_hdng">HTTP Method :</span>
                                             <span class="api_dp_menu_1">POST</span>

                                                 <span class="api_clear"></span>
                                             <span class="api_ori_hdng">Summary :</span>
                                             <span class="inpt_api">Shipper & Consignee's Detail.</span>
                                            <span class="api_clear"></span>
                                         </span>
                                    </h3>
                                    <div id="path_0_operation_0_parameters_id">
                                        <div id="path_0_operation_0_parameters_delete_">
                                            <span class="api_pad"></span>
                                            <span class="api_subheading" id="path_0_operation_0_parameters_show">                        
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_da_api dscrarea_1">Sample Request :</span>                                                                
                                                    <span class="api_txtara_api_6 dscrarea_4">string</span>
                                                    <span class="api_txtara_api dscrarea_4">
<pre> 
{ 
    
    "szMethod":"price",
    "szShipperFirstName":"XYZ",
    "szShipperCity":"Shanghai",
    "szShipperCountry":"CN"
    "szConsigneeFirstName":"ABC",
    "szConsigneeCity":"Copenhagen",
    "szConsigneeCountry":"DK"
}
</pre>
                                                    </span>
                                                </h3>       
                                            </span>
                                        </div>
                                    </div>
                                    <span id="content"></span>
                                    <div id="path_9_operation_0_Errparameters_id">
                                        <span class="api_pad"></span>
                                            <span class="api_subheading_error" id="path_9_operation_0_Errparameters_show_">
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">200</span>  <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">SUCCESS</span></h3>
                                                    </span>
                                                </h3>
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">404</span>   <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">BAD REQUEST</span></h3>
                                                    </span>
                                                </h3>
                                            </span>
                                    </div>
                                    <span class="api_txtara1 dscrarea_4 api_padng">
                                        <code style="line-height: 1;font-weight: normal;">
<pre>
{
    "code":"200",
    "status":"OK",
    "token":"TT13J2MYTEZ1BSGI42XY",
    "services": 
                [
                    {
                        "szServiceID":"1612TT1122841",
                        "fBookingPrice":3621,
                        "fVatAmount":905.25,
                        "bInsuranceAvailable":"N"
                    }
                ]
} 
or
{
    "code":"404",
    "status":"Error",
    "token":"TT1UE4PYFEPPTDFL1DNI",
    "errors":
                [
                    {
                        "code":"E1035",
                        "description":"Shipper city is required"
                    },
                    {
                        "code":"E1036",
                        "description":"Shipper country is required"
                    }
                ]
}
</pre>
                                        </code>
                                    </span>
                                </span>
                            </div> 
                        </div>
                  </h3> 
                </div>
            
        </div>
        <!-- Other Fields -->
        <br>
        <div class="api_contentarea">
            <div id="user" align="center" class="api_headr_path">Other Details</div>
		 <!--API 1-->
		 <div id="path_0">
                    <h3 class="api_content">   
                        <span class="api_area_heading">
                                <h3>
                                    <span class="api_hdng1">Path :</span>
                                    <span class="inpt1 api-flo">http://dev.transporteca.com/api/services</span>
                                </h3>
                                <h3>
                                    <span class="api_hdng1">Description :</span>
                                    <span class="api_txtara dscrarea_44 api-flo">Submit other details for services.</span>
                                </h3>
                        </span>
                        <div id="path_0_operation">
                            <div id="path_0_operation_delete_0">
                                <span class="api_area_heading" id="register">

                                </span>
                                <span class="api_subheading_1" id="path_0_operation_show_0">
                                    <h3 class="api_dscrarea" align="left">
                                        <span class="ori_stxt">

                                             <span class="api_ori_hdng">HTTP Method :</span>
                                             <span class="api_dp_menu_1">POST</span>

                                                 <span class="api_clear"></span>
                                             <span class="api_ori_hdng">Summary :</span>
                                             <span class="inpt_api">Other details.</span>
                                            <span class="api_clear"></span>
                                         </span>
                                    </h3>
                                    <div id="path_0_operation_0_parameters_id">
                                        <div id="path_0_operation_0_parameters_delete_">
                                            <span class="api_pad"></span>
                                            <span class="api_subheading" id="path_0_operation_0_parameters_show">                        
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_da_api dscrarea_1">Sample Request :</span>                                                                
                                                    <span class="api_txtara_api_6 dscrarea_4">string</span>
                                                    <span class="api_txtara_api dscrarea_4">
<pre> 
{ 
    
    "szServiceRefrence":"xyz_Refrence",
    "szCurrency":"USD",
    "defaultLanguage":"EN"
}
</pre>
                                                    </span>
                                                </h3>       
                                            </span>
                                        </div>
                                    </div>
                                    <span id="content"></span>
                                    <div id="path_9_operation_0_Errparameters_id">
                                        <span class="api_pad"></span>
                                            <span class="api_subheading_error" id="path_9_operation_0_Errparameters_show_">
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">200</span>  <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">SUCCESS</span></h3>
                                                    </span>
                                                </h3>
                                                <h3 class="api_dscrarea" align="center">
                                                    <span class="api_error_msg">
                                                        <h3 align="left" class="er_sp"><span class="ori_hdng_er_api">Response Code:</span><span class="inpt_error_api">404</span>   <span class="ori_hdng_er_reason">Message :</span><span class="inpt_error_api">BAD REQUEST</span></h3>
                                                    </span>
                                                </h3>
                                            </span>
                                    </div>
                                    <span class="api_txtara1 dscrarea_4 api_padng">
                                        <code style="line-height: 1;font-weight: normal;">
<pre>
{
    "code":"200",
    "status":"OK",
    "token":"TT15V1JJJJDY8RJX9ATO",
    "services":
                [
                    {
                        "szServiceID":"1612TT1122897",
                        "szCurrency":"USD",
                        "fBookingPrice":"704"
                    }
                ]
} 
</pre>
                                        </code>
                                    </span>
                                </span>
                            </div> 
                        </div>
                  </h3> 
                </div>
            
        </div>
</body>
</html>