<?php 
session_start();
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL);
$szMetaTitle="Transporteca API Demo";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once(__APP_PATH__ . "/inc/constants.php");  
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once(__APP_PATH_LAYOUT__ ."/header_new.php");
 
if(!empty($_SESSION['partnerApiRequestAry']))
{
    $_POST['partnerApiRequestAry'] = $_SESSION['partnerApiRequestAry'] ;
}

$arrRequestTypeAry = array();
$arrRequestTypeAry['GET_PRICE'] = 'Get price';
$arrRequestTypeAry['VALIDATE_PRICE'] = 'Validate price'; 
$arrRequestTypeAry['CREATE_BOOKING'] = 'Create booking'; 
$arrRequestTypeAry['GET_GOOD_INSURANCE_VALUE'] = 'GET Insurance Buy Rate';
$arrRequestTypeAry['CUSTOMER_EVENT'] = 'Customer Event';
$arrRequestTypeAry['GET_STANDARD_CARGO_LIST'] = 'GET Standard Cargo List';
$arrRequestTypeAry['GET_QUOTE'] = 'Get quote';
$arrRequestTypeAry['GET_LABEL'] = 'Get Labels'; 
$arrRequestTypeAry['CONFIRM_LABEL_DOWNLOAD'] = 'Confirm Label Download';
$arrRequestTypeAry['SUBMIT_NPS'] = 'Submit NPS'; 

/*
$arrRequestTypeAry['CONFIRM_PAYMENT'] = 'Confirm Payment';
$arrRequestTypeAry['CUSTOMER_LOGIN'] = 'Customer login';
$arrRequestTypeAry['CUSTOMER_SIGNUP'] = "Customer signup";
$arrRequestTypeAry['CUSTOMER_PROFILE_DETAILS'] = "Customer profile details";
$arrRequestTypeAry['CUSTOMER_BOOKING_HISTORY'] = "Customer booking history";
$arrRequestTypeAry['CUSTOMER_CHANGE_PASSWORD'] = "Customer change password";
$arrRequestTypeAry['CUSTOMER_FORGOT_PASSWORD'] = "Customer forgot password";
$arrRequestTypeAry['DELETE_CUSTOMER_ACCOUNT'] = "Delete Customer Account";
*/
?>
<div id="hsbody-2"> 
    <style type="text/css">
        .format-2 td {
            background-color: #ffffff;
            font-size: 14px;
            padding: 4px 10px;
            text-align: left;
        }
        .api_content {
            background: #f4f4f4 none repeat scroll 0 0;
            border: 1px solid #dadada;
            border-radius: 6px;
            display: table;
            font-size: 100%;
            letter-spacing: 1px;
            margin: 12px auto;
            padding: 5px 0 5px 5px;
            width: 100%;
        }

        .api_subheading_1 {
            background: #ebf3f9 none repeat scroll 0 0;
            border: 1px solid #c3d9ec;
            border-radius: 6px;
            display: block;
            margin: auto;
            overflow: auto;
            padding: 10px;
            width: 92%;
        } 
        .api_subheading {
            background: #e7f6ec none repeat scroll 0 0;
            border: 1px solid #c3e8d1;
            border-radius: 6px;
            display: block;
            margin: auto;
            padding: 10px;
            width: 95%;
        }
        .api_area_heading {
            color: #666;
            font-weight: normal;
        }
        .api_area_heading h3 span {
            display: block;
            padding: 10px 0;
        }
        .api-flo {
            float: left;
            font-size: 13px;
            font-weight: normal;
            margin-left: 16px;
            width: 85%;
        }  
        .api_txtara {
            width: 25%;
        }
        .api_hdng1 {
            color: #333;
            display: block;
            float: left;
            font: bold 13px/1.5 "Helvetica Neue",Arial,"Liberation Sans",FreeSans,sans-serif;
            padding-left: 20px;
            text-align: right;
            width: 12%;
        } 
        .api_dscrarea span {
            border: 0 solid #333;
            float: left;
            font-size: 13px;
            margin-bottom: 3px;
        }
        .api_ori_hdng {
            color: #333;
            display: block;
            float: left;
            font-size: 13px;
            font-weight: bold;
            padding-right: 10px;
            text-align: right;
            width: 15%;
        }
        .api_dp_menu_1 {
            color: #666;
            display: block;
            font-size: 13px;
            font-weight: normal;
            width: 83%;
        }
        .api_clear
        {
            clear:both;
            display:block;
        }
    </style>
    <div class="hsbody-2-right"> 
        <h2>Transporteca Partner API Demo</h2>
        <br>
        <div id="courier_service_container_div">  
            <form id="get_api_service_form" name="get_api_service_form" method="post"> 
                <div class="api_content">
                    <div class="api_subheading_1"> 
                        <!--<h2>API Request</h2>-->
                        <table cellpadding="5" cellspacing="2">
                            <tr>
                                <td style="width:5%">Mode: </td>
                                <td style="width:45%">
                                    <select  name="partnerApiRequestAry[szMethod]" id="szMode">
                                        <option value="TEST" <?php echo (($_POST['partnerApiRequestAry']['szMode']=="TEST")?'selected':''); ?>>Test</option>
                                        <option value="LIVE" <?php echo (($_POST['partnerApiRequestAry']['szMode']=="LIVE")?'selected':''); ?>>Live</option> 
                                    </select>
                                </td> 
                                <td style="width:15%">Request Type: </td>
                                <td style="width:35%">
                                    <select  name="partnerApiRequestAry[szRequestType]" id="szRequestType" onchange="update_default_params(this.value)" style="width:100%;">
                                        <option val="">Select Request Type</option>
                                        <?php
                                            if(!empty($arrRequestTypeAry))
                                            {
                                                foreach($arrRequestTypeAry as $szRequestType=>$arrRequestTypeArys)
                                                {
                                                    ?>
                                                    <option value="<?php echo $szRequestType; ?>" <?php echo (($_POST['partnerApiRequestAry']['szRequestType']==$szRequestType)?'selected':''); ?>><?php echo $arrRequestTypeArys; ?></option>
                                                    <?php
                                                }
                                            }
                                        ?> 
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="width:100%;">
                                    <span class="api_dscrarea">
                                        <br>
                                        <textarea cols="100" style="min-height:200px;width:100%;" rows="10" name="partnerApiRequestAry[szApiRequestParams]" id="szApiRequestParams"><?php echo $_POST['partnerApiRequestAry']['szApiRequestParams']; ?></textarea> 
                                    </span>
                                </td>
                            </tr> 
                        </table> 
                    </div>
                    
                    <br>
                    <div style="text-align:center;">
                        <input type="button" name="getCourierRateAry[szSubmit]" value="Get Details" onclick="submit_partner_api_demo_form();"> 
                    </div>
                </div>
            </form>
        </div>
        <div class="api_content" id="api_response_container" style="display:none;"> 
        </div>
    </div> 
</div>
<script type="text/javascript">
    function  update_default_params(szRequestType)
    {
        var szDefaultValue = $("#request_type_"+szRequestType).val();
        $("#szApiRequestParams").val(szDefaultValue);
    }
</script> 
<div id="hidden_request_params_container" style="display:none;">
    <textarea name="request_type_GET_PRICE" id="request_type_GET_PRICE" cols="100" style="min-height:200px;" rows="10">{"szRequest":"price","dtDay":"<?php echo date('d'); ?>","dtMonth":"<?php echo date('m'); ?>","dtYear":"<?php echo date('Y'); ?>","szServiceTerm":"","szCargoDescription":"","fCargoValue":"","szCargoCurrency":"","szCommodity":"","customerType":["PRIVATE","BUSINESS"],"szShipperCompanyName":"","szConsigneeCompanyName":"","szShipperFirstName":"","szConsigneeFirstName":"","szShipperLastName":"","szConsigneeLastName":"","szShipperEmail":"","szConsigneeEmail":"","iShipperCountryDialCode":"","iShipperPhoneNumber":"","iConsigneeCountryDialCode":"","iConsigneePhoneNumber":"","szShipperAddress":"","szConsigneeAddress":"","szShipperPostcode":"400050","szConsigneePostcode":"2300","szShipperCity":"Mumbai","szConsigneeCity":"Copenhagen","szShipperCountry":"IN","szConsigneeCountry":"DK","iShipperConsigneeType":["SHIPPER","CONSIGNEE","BILLING"],"szBillingCompanyName":"","szBillingFirstName":"","szBillingLastName":"","iBillingCountryDialCode":"","iBillingPhoneNumber":"","szBillingEmail":"","szBillingAddress":"","szBillingPostcode":"","szBillingCity":"","szBillingCountry":"","szServiceID":"","token":"","bInsuranceRequired":"No","szCurrency":"","szLanguage":"DA","szReference":"","szCustomerToken":"","szAutomatedRfqResponseCode":"szAutomatedRfqResponseCode","iShipperDetailsLocked":"0","iConsigneeDetailsLocked":"0","iCargoDescriptionLocked":"0","iRemovePriceGuarantee":"0","cargo":[{"szShipmentType":"PARCEL","iLength":"25","iWidth":"25","iHeight":"25","iWeight":"25","iQuantity":"2","szWeightMeasure":"KG","szDimensionMeasure":"CM"}]}</textarea>    
    <textarea name="request_type_CONFIRM_PAYMENT" id="request_type_CONFIRM_PAYMENT" cols="100" style="min-height:200px;" rows="10">{"szRequest":"confirmPayment","token":"","szPaymentType":"Stripe","szPaymentToken":"tok_XXXXXXXXXX"}</textarea>     
    <textarea name="request_type_VALIDATE_PRICE" id="request_type_VALIDATE_PRICE" cols="100" style="min-height:200px;" rows="10">{"szRequest":"validatePrice","dtDay":"<?php echo date('d'); ?>","dtMonth":"<?php echo date('m'); ?>","dtYear":"<?php echo date('Y'); ?>","szServiceTerm":"","szCargoDescription":"Private -testing","fCargoValue":"","szCargoCurrency":"","szCommodity":"1","szShipperCompanyName":"Transporteca","szConsigneeCompanyName":"Whiz Solutions","szShipperFirstName":"Ajay","szConsigneeFirstName":"Ajay","szShipperLastName":"Jha","szConsigneeLastName":"Jha","szShipperEmail":"ajay@whiz-solutions.com","szConsigneeEmail":"ajay@whiz-solutions.com","iShipperCountryDialCode":"91","iShipperPhoneNumber":"2342545","iConsigneeCountryDialCode":"45","iConsigneePhoneNumber":"9911466405","szShipperAddress":"Hindi Street","szConsigneeAddress":"Dansk Street","szShipperPostcode":"400050","szConsigneePostcode":"2300","szShipperPostcodeRequired":"No","szConsigneePostcodeRequired":"No","szShipperCity":"Mumbai","szConsigneeCity":"Copenhagen","szShipperCountry":"IN","szConsigneeCountry":"DK","szServiceID":"","token":"","bInsuranceRequired":"No","szCurrency":"","szLanguage":"DA","iShipperDetailsLocked":"0","iConsigneeDetailsLocked":"0","szReference":"","cargo":[{"szShipmentType":"PARCEL","iLength":"25","iWidth":"25","iHeight":"25","iWeight":"25","iQuantity":"2","szWeightMeasure":"KG","szDimensionMeasure":"CM"}]}</textarea>     
    <textarea name="request_type_CREATE_BOOKING" id="request_type_CREATE_BOOKING" cols="100" style="min-height:200px;" rows="10">{"szRequest":"booking","dtDay":"<?php echo date('d'); ?>","dtMonth":"<?php echo date('m'); ?>","dtYear":"<?php echo date('Y'); ?>","szServiceTerm":"","szCargoDescription":"Private -testing","fCargoValue":"","szCargoCurrency":"","szCommodity":"1","szShipperCompanyName":"Transporteca","szConsigneeCompanyName":"Whiz Solutions","szShipperFirstName":"Ajay","szConsigneeFirstName":"Ajay","szShipperLastName":"Jha","szConsigneeLastName":"Jha","szShipperEmail":"ajay@whiz-solutions.com","szConsigneeEmail":"ajay@whiz-solutions.com","iShipperCountryDialCode":"91","iShipperPhoneNumber":"2342545","iConsigneeCountryDialCode":"45","iConsigneePhoneNumber":"9911466405","szShipperAddress":"Hindi Street","szConsigneeAddress":"Dansk Street","szShipperPostcode":"400050","szConsigneePostcode":"2300","szShipperCity":"Mumbai","szConsigneeCity":"Copenhagen","szShipperCountry":"IN","szConsigneeCountry":"DK","iShipperConsigneeType":"SHIPPER","szBillingCompanyName":"WS","szBillingFirstName":"Ajay","szBillingLastName":"Jha","iBillingCountryDialCode":"91","iBillingPhoneNumber":"9711030117","szBillingEmail":"ajay@whiz-solutions.com","szBillingAddress":"A-40","szBillingPostcode":"400001","szBillingCity":"Mumbai","szBillingCountry":"IN","szServiceID":"","token":"","bInsuranceRequired":"No","szCurrency":"","szLanguage":"DA","szReference":"","szPaymentType":"Stripe","szPaymentToken":"","szAmount":"","idTransaction":"TRX76767676","iOfferType":"","cargo":[{"szShipmentType":"PARCEL","iLength":"25","iWidth":"25","iHeight":"25","iWeight":"25","iQuantity":"2","szWeightMeasure":"KG","szDimensionMeasure":"CM"}]}</textarea>     
    <textarea name="request_type_CUSTOMER_LOGIN" id="request_type_CUSTOMER_LOGIN" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerLogin","szUserName":"ajay@whiz-solutions.com","szPassword":"Test1234"}</textarea> 
    <textarea name="request_type_CUSTOMER_SIGNUP" id="request_type_CUSTOMER_SIGNUP" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerSignup","szFirstName":"Ajay","szLastName":"Jha","szEmail":"***@****.com",  "iPhoneNumber":"9911466405", "iCountryDialCode":"91", "szCompanyName":"", "szCompanyRegNo":"", "isPrivate":"Yes","szPassword":"******","szConfirmPassword":"******", "szAddress":"Bandra", "szPostCode":"400050", "szCity":"Mumbai", "szCountry":"IN"}</textarea>  
    <textarea name="request_type_CUSTOMER_BOOKING_HISTORY" id="request_type_CUSTOMER_BOOKING_HISTORY" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerBookings","token":"XXXXXXXXXXX"}</textarea> 
    <textarea name="request_type_CUSTOMER_PROFILE_DETAILS" id="request_type_CUSTOMER_PROFILE_DETAILS" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerProileDetails","token":"XXXXXXXXXXX"}</textarea> 
    <textarea name="request_type_CUSTOMER_CHANGE_PASSWORD" id="request_type_CUSTOMER_CHANGE_PASSWORD" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerChangePassword","token":"XXXXXXXXXXX","szCurrentPassword":"XXXXXXXXXXX","szPassword":"XXXXXXXXXXX","szConfirmPassword":"XXXXXXXXXXX"}</textarea> 
    <textarea name="request_type_CUSTOMER_FORGOT_PASSWORD" id="request_type_CUSTOMER_FORGOT_PASSWORD" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerForgotPassword","szEmail":"ajay@whiz-solutions.com"}</textarea>
    <textarea name="request_type_DELETE_CUSTOMER_ACCOUNT" id="request_type_DELETE_CUSTOMER_ACCOUNT" cols="100" style="min-height:200px;" rows="10">{"szRequest":"deleteCustomerAccount","token":"XXXXXXXXXXX","szEmail":"ajay@whiz-solutions.com","szPassword":"Test1234"}</textarea>
    <textarea name="request_type_GET_GOOD_INSURANCE_VALUE" id="request_type_GET_GOOD_INSURANCE_VALUE" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getInsuranceValue","szOriginCountry":"IN","szDestinationCountry":"DK","szTransportMode":"COURIER","szCargoType":"1","szPriceToken":"","szServiceID":"","szQuoteID":""}</textarea>
    <textarea name="request_type_CUSTOMER_EVENT" id="request_type_CUSTOMER_EVENT" cols="100" style="min-height:200px;" rows="10">{"szRequest":"customerEvents","szPriceToken":"","szCustomerToken":"","szEventType":"PriceRequestContact","szBillingCompanyName":"","iPrivate":"Yes","szBillingFirstName":"Ajay","szBillingLastName":"Jha","szBillingEmail":"***@***.com","iBillingCountryDialCode":"45","iBillingPhoneNumber":"123456789","szCargoType":"1"}</textarea>
    <textarea name="request_type_GET_STANDARD_CARGO_LIST" id="request_type_GET_STANDARD_CARGO_LIST" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getStandardCargo","iDataSet":"","iLanguage":"1"}</textarea>
    <textarea name="request_type_GET_QUOTE" id="request_type_GET_QUOTE" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getQuote","szQuoteID":"","iLanguage":"1"}</textarea> 
    <textarea name="request_type_GET_LABEL" id="request_type_GET_LABEL" cols="100" style="min-height:200px;" rows="10">{"szRequest":"getLabels","szBookingKey":"","iLanguage":"1"}</textarea> 
    <textarea name="request_type_CONFIRM_LABEL_DOWNLOAD" id="request_type_CONFIRM_LABEL_DOWNLOAD" cols="100" style="min-height:200px;" rows="10">{"szRequest":"confirmDownload","szBookingKey":"","szShipperCompanyName":"Transporteca Ltd","szShipperFirstName":"Ajay","szShipperLastName":"Jha","szShipperEmail":"ajay@whiz-solutions.com","iShipperCountryDialCode":"91","iShipperPhoneNumber":"9911466405","iLanguage":"1","dtPickUpDay":"<?php echo date('d'); ?>","dtPickUpMonth":"<?php echo date('m'); ?>","dtPickUpYear":"<?php echo date('Y'); ?>"}</textarea> 
    <textarea name="request_type_SUBMIT_NPS" id="request_type_SUBMIT_NPS" cols="100" style="min-height:200px;" rows="10">{"szRequest":"submitNPS","szEmail":"ajay@whiz-solutions.com","iScore":"5","szComment":""}</textarea>  
    
</div> 
<?php
//include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
