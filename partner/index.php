<?php
session_start();
if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/../"));
}
ini_set (max_execution_time,300000);
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL); 
require_once(__APP_PATH__ . "/inc/constants.php");  
require_once( __APP_PATH__ . "/inc/functions.php" );
  
$priceInputAry = array();  

$priceInputAry = $_REQUEST['transporteca'];  
$szMethod = sanitize_all_html_input($_REQUEST['method']);

$kSeo = new cSEO();
$szLanguage = $kSeo->getLanguageName($requestInputAry);
require_once(__APP_PATH_LAYOUT__."/ajax_partnerHeader.php");  

$kPartner = new cPartner();   
$t_base = "Partner/"; 
static $bAuthentication = false;
 
if(function_exists('getallheaders'))
{ 
    $clientApiHeaderAry = getallheaders(); 
}
else
{
    $clientApiHeaderAry = cPartner::request_headers();     
}   
$kApi = new cApi();
if($szMethod=='validatePrice')
{
    $kApi->callTransportecaPartnerAPI($clientApiHeaderAry,$priceInputAry,'VALIDATE_PRICE');
}
else if($szMethod=='confirmPayment')
{
    $kApi->callTransportecaPartnerAPI($clientApiHeaderAry,$priceInputAry,'CONFIRM_PAYMENT');
}
else
{
    $kApi->callTransportecaPartnerAPI($clientApiHeaderAry,$priceInputAry,'PRICE');
}

 
/*
$idPartnerApiEventApi = $kPartner->addPartnerApiEventLogs($priceInputAry,$clientApiHeaderAry);
   
if($kPartner->validatePartnerKey($clientApiHeaderAry,$idPartnerApiEventApi))
{      
    $kPartner->getPrice($priceInputAry);   
    if(cPartner::$szGlobalErrorType=='VALIDATION')
    { 
        $szResponseSerialized = cResponseError::getSerializedMessages();
        $kPartner->updateApiEventLogResponse($szResponseSerialized,404,$idPartnerApiEventApi);
        $szResponseJson = cResponseError::getMessages();
        echo $szResponseJson;
        $priceCalculationLogs = cPartner::$priceCalculationLogs;
        if(!empty($priceCalculationLogs))
        {
            echo "<br><br> <a href='javascript:void(0)' class='button-green1' onclick=showHideCourierCal('courier_service_calculation')>View Courier Calculation</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
		</a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$priceCalculationLogs." </div></div> </div></div>";
        } 
        die;
    }
    else if(cPartner::$szGlobalErrorType=='NOTIFICATION')
    {
        $szResponseSerialized = cResponseError::getSerializedMessages();
        $kPartner->updateApiEventLogResponse($szResponseSerialized,200,$idPartnerApiEventApi);
        $szResponseJson = cResponseError::getMessages();
        echo $szResponseJson;
        die;
    }
    else if(cPartner::$szGlobalErrorType=='SUCCESS')
    { 
        $szResponseSerialized = cResponseError::getSerializedMessages();
        $kPartner->updateApiEventLogResponse($szResponseSerialized,200,$idPartnerApiEventApi);
        $szResponseJson = cResponseError::getMessages(); 
        echo $szResponseJson;
        $priceCalculationLogs = cPartner::$priceCalculationLogs;
        if(!empty($priceCalculationLogs))
        {
            echo "<br><br> <a href='javascript:void(0)' class='button-green1' onclick=showHideCourierCal('courier_service_calculation')>View Courier Calculation</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
		</a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$priceCalculationLogs." </div></div> </div></div>";
        } 
        die;
    }
}
else
{ 
//    ob_end_clean();
//    cResponseError::display_http_response_header(401);
    $szResponseSerialized = cResponseError::getSerializedMessages();
    $kPartner->updateApiEventLogResponse($szResponseSerialized,404,$idPartnerApiEventApi);
    $szResponseJson = cResponseError::getMessages();
    echo $szResponseJson;
    die;
} 
 * 
 */
?>
