<!DOCTYPE html>
<?php
session_start();
if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/../"));
}
require_once(__APP_PATH__ . "/inc/constants.php");
require_once(__APP_PATH_LAYOUT__."/ajax_header.php"); 

$t_base_partner_api = "PartnerApiDoc/"; 
$t_base_partner = "Partner/"; 

$mode="php";

$yourApi=  t($t_base_partner_api.'requestAttribute/yourApi');
$requestmode =t($t_base_partner_api.'requestAttribute/mode');

if($mode == "php")
{
    $szPriceCallString='';
    $szPriceCallString1 .="\Transporteca\Transporteca::setApiKey(".$yourApi.",".$requestmode.");<br>";
    $szPriceCallString2 .="\Transporteca\Service::getPrice(array(<br>";
    $szBookingString2 .="\Transporteca\Service::createBooking(array(<br>";
}
elseif(trim($mode == "json"))
{
    $szPriceCallString='';
    $szPriceCallString1 .="\Transporteca\Transporteca::setApiKey(".$yourApi.",".$requestmode.");<br>";
    $szPriceCallString2 .="\Transporteca\Service::getPrice([{<br>";
    $szBookingString2 .="\Transporteca\Service::createBooking([{<br>";
}
$requestAry = array();

$responseAry = array();
        
$requestAry['szRequest']='price';
$requestAry['dtDay']=5;
$requestAry['dtMonth']=1;
$requestAry['dtYear']=2016;
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][0]['szPalletType'] = '';
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = '';
$requestAry['cargo'][0]['iHeight'] = '';
$requestAry['cargo'][0]['iWeight'] = '';
$requestAry['cargo'][0]['iQuantity'] = '';
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['cargo'][1]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][1]['szPalletType'] = '';
$requestAry['cargo'][1]['iLength'] = '';
$requestAry['cargo'][1]['iWidth'] = '';
$requestAry['cargo'][1]['iHeight'] = '';
$requestAry['cargo'][1]['iWeight'] = '';
$requestAry['cargo'][1]['iQuantity'] = '';
$requestAry['cargo'][1]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cm');
$requestAry['cargo'][1]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']='';
$requestAry['szCargoDescription']="Sample Description";
$requestAry['fCargoValue']=10000;
$requestAry['szCargoCurrency']="USD";
$requestAry['szShipperCompanyName']="ABC comapny";
$requestAry['szShipperFirstName']="Johan";
$requestAry['szShipperLastName']="Miller";
$requestAry['iShipperCountryDialCode']=91;
$requestAry['iShipperPhoneNumber']=1234567890;
$requestAry['szShipperEmail']="shipper@email.com";
$requestAry['szShipperAddress']="A-49 , Phase - 1, Sector 2";
$requestAry['szShipperPostcode']=440000;
$requestAry['szShipperCity']="Mumbai";
$requestAry['szShipperCountry']="IN";
$requestAry['szConsigneeCompanyName']="XYZ company";
$requestAry['szConsigneeFirstName']="Albert";
$requestAry['szConsigneeLastName']="Methew";
$requestAry['iConsigneeCountryDialCode']=46;
$requestAry['iConsigneePhoneNumber']=1231231234;
$requestAry['szConsigneeEmail']="consignee@email.com";
$requestAry['szConsigneeAddress']="51, Block D, West Street";
$requestAry['szConsigneePostcode']=2100;
$requestAry['szConsigneeCity']="Copenhagen";
$requestAry['szConsigneeCountry']="DK";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['iInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
$requestAry['szLanguage']=t($t_base_partner_api.'requestAttribute/szLanguageKey');
$requestAry['szReference']=t($t_base_partner_api.'requestAttribute/reference');




$responseAry['code'] = 200;
$responseAry['status'] = "OK";
$responseAry['token'] = "XXXXXXXXXXXXXXXXXXXX";
$responseAry['service'][0]['szServiceID'] = "123ABC123ABC";
$responseAry['service'][0]['dtDeliveryDay'] = 13;
$responseAry['service'][0]['dtDeliveryMonth'] = 01;
$responseAry['service'][0]['dtDeliveryYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/onlyCatons');
$responseAry['service'][0]['szCurrency'] = "USD";
$responseAry['service'][0]['fBookingPrice'] = 510.00;
$responseAry['service'][0]['fVatAmount'] = 127.5;
$responseAry['service'][0]['bInsuranceAvailable'] = t($t_base_partner_api.'responseAttribute/yes');;
$responseAry['service'][0]['fInsurancePrice'] = 106.00;


$bookingResponseAry['code']=200;
$bookingResponseAry['status']="OK";
$bookingResponseAry['token']="XXXXXXXXXXXXXXXXXXXX";
$bookingResponseAry['bookings'][0]['szBookingStatus']="Confirmed";
$bookingResponseAry['bookings'][0]['szBookingRef']="123ABC1234";
$bookingResponseAry['bookings'][0]['szServiceDescription']=t($t_base_partner_api.'responseAttribute/szServiceDescription');
$bookingResponseAry['bookings'][0]['szPackingType']="Courier";
$bookingResponseAry['bookings'][0]['szTransportMode']=t($t_base_partner_api.'responseAttribute/onlyCatons');
$bookingResponseAry['bookings'][0]['szCurrency']="USD";
$bookingResponseAry['bookings'][0]['fBookingPrice']=510.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=127.50;
$bookingResponseAry['bookings'][0]['fInsurancePrice']=106.0000;
$bookingResponseAry['bookings'][0]['bInsuranceAvailable']=t($t_base_partner_api.'responseAttribute/yes');;
$bookingResponseAry['bookings'][0]['szCarrierName']="DSV";
$bookingResponseAry['bookings'][0]['szCarrierAddressLine1']="Fruebjergvej 3";
$bookingResponseAry['bookings'][0]['szCarrierAddressLine2']='';
$bookingResponseAry['bookings'][0]['szCarrierAddressLine3']='';
$bookingResponseAry['bookings'][0]['szCarrierPostcode']=2100;
$bookingResponseAry['bookings'][0]['szCarrierCity']="Copenhagen";
$bookingResponseAry['bookings'][0]['szCarrierRegion']='';
$bookingResponseAry['bookings'][0]['szCarrierCountry']="DK";
$bookingResponseAry['bookings'][0]['szBookingContactNumber']="+45 7199 7299";
$bookingResponseAry['bookings'][0]['szBookingContactEmail']="contact@transporteca.com";
$bookingResponseAry['bookings'][0]['szCarrierCompanyRegistration']="23 54 12";
        
function getRequestAry($caseAry,$mode)
{  
    
    if(trim($mode) == "php")
    { 
        $szResponseString = ''; 
        
        if(!empty($caseAry))
        {  
            foreach ($caseAry as $key => $value) 
            {  
                if($key == "cargo")
                {   
                    $cargoAry = $caseAry['cargo'];
                    if(!empty($cargoAry))
                    {
                        $cargoCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($cargoAry as $cargoArys)
                        { 
                            if(!empty($cargoArys))
                            {
                                $szResponseString .= "array([".$cargoCtr."]=><br>"; 
                                $lineResponseString = getRequestAry($cargoArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "),<br>";
                                $cargoCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    }
                    
                    if(trim($key) == "szReference")
                    {
                        $comma = '';
                    }
                    else
                    {
                        $comma=",<br>";
                    }
                    $szResponseString .= "<span class='attribute'>".$key."</span> => <span class='value'><span class=".$class.">".$value." </span>".$comma;
                } 
            }
            
        }
        return $szResponseString1."".$szResponseString;
    }
    elseif(trim($mode) == "json")
    {
        $szResponseString = ''; 
        
        if(!empty($caseAry))
        {  
            foreach ($caseAry as $key => $value) 
            {  
                if($key == "cargo")
                {   
                    $cargoAry = $caseAry['cargo'];
                    if(!empty($cargoAry))
                    {
                        
                        $totalCount = count($cargoAry);
                        
                        $cargoCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> : ["; 
                        foreach($cargoAry as $cargoArys)
                        { 
                            if(!empty($cargoArys))
                            {
                                $cargoCtr++; 
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getRequestAry($cargoArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}";
                                if($totalCount == $cargoCtr )
                                {
                                    $szResponseString .= "],<br>";
                                }
                                else
                                {
                                    $szResponseString .= ",<br>";
                                }
                                
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    }
                    
                    if(trim($key) == "szReference")
                    {
                        $comma = '<br>}]);';
                    }
                    else
                    {
                        $comma=",<br>";
                    }
                    $szResponseString .= "<span class='attribute'>".$key."</span> : <span class='value'><span class=".$class.">".$value." </span>".$comma;
                } 
            }
            
        }
        
        return $szResponseString1."".$szResponseString;
    } 
    
//    $finalString = rtrim($szResponseString,",<br>");
} 

function getResponseAry($responseAry,$mode)
{  
    
    if(trim($mode) == "php")
    { 
        $szResponseString = ''; 
        if(!empty($responseAry))
        {  
            foreach ($responseAry as $key => $value) 
            {  
                if($key == "service")
                {   
                    $serviceAry = $responseAry['service'];
                    if(!empty($serviceAry))
                    {
                        $serviceCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($serviceAry as $serviceArys)
                        { 
                            if(!empty($serviceArys))
                            {
                                $szResponseString .= "array([".$serviceCtr."]=><br>"; 
                                $lineResponseString = getResponseAry($serviceArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "),<br>";
                                $serviceCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                elseif($key == "bookings")
                {
                    $bookingAry = $responseAry['bookings'];
                    if(!empty($bookingAry))
                    {
                        $bookingCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> => "; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $szResponseString .= "array([".$bookingCtr."]=><br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= ")";
                                $bookingCtr++; 
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    }
                    
                    $szResponseString .= "<span class='attribute'>".$key."</span> => <span class='value'><span class=".$class.">".$value." ,<br></span>";
                } 
            }
            
        }
    }
    elseif(trim($mode) == "json")
    { 
        $szResponseString = ''; 
        if(!empty($responseAry))
        {  
            foreach ($responseAry as $key => $value) 
            {  
                if($key == "service")
                {   
                    $serviceAry = $responseAry['service'];
                    if(!empty($serviceAry))
                    {
                        $totalServices= count($serviceAry);
                        $serviceCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> : ["; 
                        foreach($serviceAry as $serviceArys)
                        { 
                            if(!empty($serviceArys))
                            {
                                $serviceCtr++; 
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getResponseAry($serviceArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}";
                                if($totalServices == $serviceCtr)
                                {
                                    $szResponseString .= "]<br>";
                                }
                                else
                                {
                                    $szResponseString .= ",<br>";
                                }
                                
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                elseif($key == "bookings")
                {
                    $bookingAry = $responseAry['bookings'];
                    if(!empty($bookingAry))
                    {
                        $totalBooking= count($bookingAry);
                        $serviceCtr=0;
                        $bookingCtr=0;
                        $szResponseString .= "<span class='attribute'>".$key."</span> : [ "; 
                        foreach($bookingAry as $bookingArys)
                        { 
                            if(!empty($bookingArys))
                            {
                                $bookingCtr++;
                                $szResponseString .= "{<br>"; 
                                $lineResponseString = getResponseAry($bookingArys,$mode);
                                $szResponseString .= $lineResponseString;
                                $szResponseString .= "}]";
                            } 
                        }
                        $szResponseString .= '</span>';
                    }  
                }
                else
                { 
                    if(is_numeric($value))
                    {
                        $class = "number";
                    }
                    else
                    {
                        $class = "string";
                    }
                    
                    $szResponseString .= "<span class='attribute'>".$key."</span> : <span class='value'><span class=".$class.">".$value." ,<br></span>";
                } 
            }
            
        }
    } 
    
//    $finalString = rtrim($szResponseString,",<br>");
    
    return $szResponseString;
    
} 


?>
<html lang='en'>
<head>
<meta charset='utf-8'>
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
<title>
<?php echo t($t_base_partner_api.'heading/titleHeading');?>
</title>
<meta content='Transporteca' name='author'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<meta content='website' property='og:type'>
<link rel="stylesheet" media="all" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/apidocs.css?t=<?php echo time(); ?>" />
</head>
    <body>
        <header class="line" style="height:56px;">
          <hgroup class="unit size3of8 padding-left-0">
              <div class="logo">
                  <a href="https://transporteca.com"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/transporteca-logo.png" alt="Transporteca"></a>
            </div>
          </hgroup>
          
        </header>
        <section class="line wrapper">
          <aside id="sidebar" class="unit side" style="background-color:#404041;margin-top:20px;">
            <div class="ab">
                <nav class="navigation" id="nav">
                  <ul>
                    <li class="active-section section">
                        <a href='#introduction'>
                            <?php echo t($t_base_partner_api.'leftMenu/introduction');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#authentication'>
                        <a href='#authentication'>
                            <?php echo t($t_base_partner_api.'leftMenu/authentication');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#parcels'>
                        <a href='#parcels'>
                            <?php echo t($t_base_partner_api.'leftMenu/parcels');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#pallets'>
                        <a href='#pallets'>
                            <?php echo t($t_base_partner_api.'leftMenu/pallets');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#breakBulk'>
                        <a href='#breakBulk'>
                            <?php echo t($t_base_partner_api.'leftMenu/breakBulk');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#palletsParcels'>
                        <a href='#palletsParcels'>
                            <?php echo t($t_base_partner_api.'leftMenu/palletsParcels');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#parameters'>
                        <a href='#parameters'>
                            <?php echo t($t_base_partner_api.'leftMenu/parameters');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#returnValues'>
                        <a href='#returnValues'>
                            <?php echo t($t_base_partner_api.'leftMenu/returnValues');?>
                        </a>
                    </li>
                    <li class="active-section section" data-hash='#errors'>
                        <a href='#errors'>
                            <?php echo t($t_base_partner_api.'leftMenu/errors');?>
                        </a>
                    </li>
                </ul>
                </nav>
              </div>
            </aside>
            
            <div id="main" data-swiftype-index="true">
              <div class="in ab double">
                  <div class="padtop main">
                    <div class="content">
                    <!--START INTRODUCTION ROW-->
                    <h1  id="introduction">&nbsp;</h1>
                        <div>
                            <h1><?php echo t($t_base_partner_api.'heading/titleHeading');?></h1>
                            <div class="namespace-summary">
                                <p>
                                    <?php echo t($t_base_partner_api.'description/introduction');?>
                                </p> 
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/priceRequest');?></h3>
                                <p>
                                    <?php echo t($t_base_partner_api.'description/priceRequest');?>
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/bookingRequest');?></h3>
                                <p>
                                    <?php echo t($t_base_partner_api.'description/bookingRequest');?>
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                                <p>
                                   <?php echo t($t_base_partner_api.'description/apiEndPoint');?> 
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/requestMode');?></h3>
                                <p>
                                    <?php echo t($t_base_partner_api.'description/requestMode');?> 
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/responseCode');?></h3>
                                    <p>
                                        <?php echo t($t_base_partner_api.'description/responseCode');?> 
                                    </p>
                                <ul>
                                    <li>
                                        <strong>Code - </strong>200
                                        <div>
                                            <strong>status - </strong>OK
                                        </div>
                                    </li>
                                    <li>
                                        <strong>Code - </strong>400
                                        <div>
                                            <strong>status - </strong>Error
                                        </div>
                                    </li>
                                    <li>
                                        <strong>Code - </strong>401
                                        <div>
                                            <strong>status - </strong><?php echo t($t_base_partner_api.'description/responseCode401');?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                <!--END INTRODUCTION ROW-->

                <!--START AUTHENTICATION ROW-->
                <h2  id="authentication">&nbsp;</h2>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/authentication');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/authenticationDescription');?>
                        </p>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <ul>
                            <li>
                               \Transporteca\Transporteca::setApiKey(<?php echo t($t_base_partner_api.'requestAttribute/yourApi');?>,<?php echo t($t_base_partner_api.'requestAttribute/mode');?>); 
                            </li>
                        </ul>
                    </div>
                                    

                <!--END AUTHENTICATION ROW-->
                
                <!--START PARCEL ROW-->
                
                <h2  id="parcels">&nbsp;</h2>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/parcels');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/parcelDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 1 (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/parcelExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                            <button id ="parcelExample1PriceRequest_btn_json" style="float:right;margin-right: 200px;" type="button" class="selected" onclick="showHideApiDocDiv('parcelExample1PriceRequest','json');">JSON</button>
                            <button id ="parcelExample1PriceRequest_btn_php" style="float:right;margin-right: 20px;background-color:darkgray;" type="button" class="" onclick="showHideApiDocDiv('parcelExample1PriceRequest','php');">PHP</button>

<?php
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12.234;
$requestAry['cargo'][1]['iQuantity'] = 2;
unset($requestAry['cargo'][0]['szPalletType']);
unset($requestAry['cargo'][1]['szPalletType']);
?>
<div id="parcelExample1PriceRequest_php" style="display:block;">
<pre>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,"php");
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>  
</div>
<div id="parcelExample1PriceRequest_json"  style="display:none;">
<pre>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,"json");
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>  
</pre>
</div>
        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<?php
$responseAry['service'][1]['szServiceID'] = "123ABC123ABC";
$responseAry['service'][1]['dtDeliveryDay'] = 07;
$responseAry['service'][1]['dtDeliveryMonth'] = 01;
$responseAry['service'][1]['dtDeliveryYear'] = 2016;
$responseAry['service'][1]['szPackingType'] = t($t_base_partner_api.'responseAttribute/palletCartons');
$responseAry['service'][1]['szCurrency'] = "USD";
$responseAry['service'][1]['fBookingPrice'] = 2044.00;
$responseAry['service'][1]['fVatAmount'] = 511.00;
$responseAry['service'][1]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][1]['fInsurancePrice'] = 121.00;
$responseAry['service'][2]['szServiceID'] = "123ABC123ABC";
$responseAry['service'][2]['dtDeliveryDay'] = 29;
$responseAry['service'][2]['dtDeliveryMonth'] = 01;
$responseAry['service'][2]['dtDeliveryYear'] = 2016;
$responseAry['service'][2]['szPackingType'] = t($t_base_partner_api.'responseAttribute/halfPallet');
$responseAry['service'][2]['szCurrency'] = "USD";
$responseAry['service'][2]['fBookingPrice'] = 1909.00;
$responseAry['service'][2]['fVatAmount'] = 477.25;
$responseAry['service'][2]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][2]['fInsurancePrice'] = 143.00;
?>
<code class="clear json">
<?php
$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                <h3>Booking Call</h3>
                <strong>
                    <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                </strong>

<pre>
<?php
$requestAry['szRequest']='booking';
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12.234;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        
                        
                        
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 2 (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/parcelExample2');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<?php
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']='';
$requestAry['szShipperPostcode']='';
$requestAry['szShipperCity']='';
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['cargo'][0]['iLength'] = '';
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12.234;
$requestAry['cargo'][1]['iQuantity'] = '';
?>
<code class="clear json">
<?php
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
"<span class="attribute">errors</span>" => array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1054"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1054');?>"</span></span>
),
array([1]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1063"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1063');?>"</span></span>
),
array([2]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1034"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1034');?>"</span></span>
),
array([3]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1035"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1035');?>"</span></span>
)
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['iLength'] = 80;
$requestAry['cargo'][1]['iWidth'] = 68;
$requestAry['cargo'][1]['iHeight'] = 35;
$requestAry['cargo'][1]['iWeight'] = 12.234;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['szShipperPostcode']=210000;
$requestAry['szShipperCity']="Shanghai";
$requestAry['szShipperCountry']="CN";
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
"<span class="attribute">errors</span>" => array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1097"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1097');?>"</span></span>
)
</code>
</pre>
                    </div>
                <!--END PARCEL ROW-->
                
                
                <!--START PALLETS ROW-->
                <h2  id="pallets">&nbsp;</h2>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/pallets');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/palletsDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 1 (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/palletExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=7;
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['szPalletType'] = "Euro";
$requestAry['cargo'][0]['iLength'] = 85.31;
$requestAry['cargo'][0]['iWidth'] = 73.32;
$requestAry['cargo'][0]['iHeight'] = 130;
$requestAry['cargo'][0]['iWeight'] = 59.69;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = 30.342;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']="DKK";
$requestAry['szShipperPostcode']=440000;
$requestAry['szShipperCity']="Mumbai";
$requestAry['szShipperCountry']="IN";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['iInsuranceRequired']="No";
$requestAry['szCurrency']="DKK";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$responseAry['service'][0]['szServiceID'] = "123ABC123ABC";
$responseAry['service'][0]['dtDeliveryDay'] = 05;
$responseAry['service'][0]['dtDeliveryMonth'] = 02;
$responseAry['service'][0]['dtDeliveryYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/euroPallet');
$responseAry['service'][0]['szCurrency'] = "DKK";
$responseAry['service'][0]['fBookingPrice'] = 43263.00;
$responseAry['service'][0]['fVatAmount'] = 10815.75;
$responseAry['service'][0]['bInsuranceAvailable'] = "No, cargo value required";
$responseAry['service'][0]['fInsurancePrice'] = '';
unset($responseAry['service'][1]);
unset($responseAry['service'][2]);
$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=7;
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['szPalletType'] = "Euro";
$requestAry['cargo'][0]['iLength'] = 85.31;
$requestAry['cargo'][0]['iWidth'] = 73.32;
$requestAry['cargo'][0]['iHeight'] = 130;
$requestAry['cargo'][0]['iWeight'] = 59.69;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = 30.342;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']="DKK";
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
$requestAry['iInsuranceRequired']="No";
$requestAry['szCurrency']="DKK";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$bookingResponseAry['bookings'][0]['szPackingType']=t($t_base_partner_api.'responseAttribute/euroPallet');
$bookingResponseAry['bookings'][0]['szTransportMode']="LTL";
$bookingResponseAry['bookings'][0]['szCurrency']="DKK";
$bookingResponseAry['bookings'][0]['fBookingPrice']=43263.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=10815.75;
$bookingResponseAry['bookings'][0]['fInsurancePrice']='';
$bookingResponseAry['bookings'][0]['bInsuranceAvailable']=t($t_base_partner_api.'responseAttribute/no');
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 2 (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/palletExample2');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=7;
$requestAry['szConsigneeCity']='';
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['szPalletType'] = "Euro";
$requestAry['cargo'][0]['iLength'] = 85.31;
$requestAry['cargo'][0]['iWidth'] = 73.32;
$requestAry['cargo'][0]['iHeight'] = 130;
$requestAry['cargo'][0]['iWeight'] = 59.69;
$requestAry['cargo'][0]['iQuantity'] ='';
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = '';
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['iInsuranceRequired']="No";
$requestAry['szCurrency']='';
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
"<span class="attribute">errors</span>" => array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1063"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1063');?>"</span></span>
),
array([1]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1093"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1093');?>"</span></span>
),
array([2]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1049"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1049');?>"</span></span>
)
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=7;
$requestAry['cargo'][0]['szShipmentType'] = "PALLET";
$requestAry['cargo'][0]['szPalletType'] = "Euro";
$requestAry['cargo'][0]['iLength'] = 85.31;
$requestAry['cargo'][0]['iWidth'] = 73.32;
$requestAry['cargo'][0]['iHeight'] = 130;
$requestAry['cargo'][0]['iWeight'] = 59.69;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = 30.342;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['fCargoValue']=1436.26;
$requestAry['szCargoCurrency']="DKK";
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
$requestAry['iInsuranceRequired']="Yes";
$requestAry['szCurrency']="DKK";
$requestAry['szConsigneeCity']="Copenhagen";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
"<span class="attribute">errors</span>" => array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1097"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1097');?>"</span></span>
)
</code>
</pre>
                    </div>
                
                <!--END PALLETS DIV-->
                
                <!--START BREAK BULK DIV-->
                <h2  id="breakBulk">&nbsp;</h2>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/breakBulk');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/breakBulkDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 1 (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/breakBulkExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=15;
$requestAry['cargo']=array();
$requestAry['cargo'][0]['szShipmentType'] = "BREAK_BULK";
$requestAry['cargo'][0]['fTotalVolume'] = 1.78;
$requestAry['cargo'][0]['fTotalWeight'] = 100;
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cbm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']="CFR";
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']='';
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['iInsuranceRequired']="No";
$requestAry['szCurrency']='';
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$responseAry['service'][0]['dtDeliveryDay'] = 12;
$responseAry['service'][0]['dtDeliveryMonth'] = 02;
$responseAry['service'][0]['dtDeliveryYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/palletCartons');
$responseAry['service'][0]['szCurrency'] = "USD";
$responseAry['service'][0]['fBookingPrice'] = 2659.00;
$responseAry['service'][0]['fVatAmount'] = 664.75;
$responseAry['service'][0]['bInsuranceAvailable'] = "No, cargo value required";
$responseAry['service'][0]['fInsurancePrice'] = '';
$responseAry['service'][1]['dtDeliveryDay'] = 19;
$responseAry['service'][1]['dtDeliveryMonth'] = 02;
$responseAry['service'][1]['dtDeliveryYear'] = 2016;
$responseAry['service'][1]['szPackingType'] = t($t_base_partner_api.'responseAttribute/palletCartons');
$responseAry['service'][1]['szCurrency'] = "USD";
$responseAry['service'][1]['fBookingPrice'] = 2659.00;
$responseAry['service'][1]['fVatAmount'] = 664.75;
$responseAry['service'][1]['bInsuranceAvailable'] = "No, cargo value required";
$responseAry['service'][1]['fInsurancePrice'] = '';
unset($responseAry['service'][2]);
$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=15;
$requestAry['dtMonth']=01;
$requestAry['cargo']=array();
$requestAry['cargo'][0]['szShipmentType'] = "BREAK_BULK";
$requestAry['cargo'][0]['fTotalVolume'] = 1.78;
$requestAry['cargo'][0]['fTotalWeight'] = 100;
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cbm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']="CFR";
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']='';
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
$requestAry['iInsuranceRequired']="No";
$requestAry['szCurrency']='';
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$bookingResponseAry['dtDeliveryDay']=12;
$bookingResponseAry['dtDeliveryMonth']=02;
$bookingResponseAry['dtDeliveryYear']=2016;
$bookingResponseAry['bookings'][0]['szPackingType']=t($t_base_partner_api.'responseAttribute/palletCartons');
$bookingResponseAry['bookings'][0]['szTransportMode']="LCL";
$bookingResponseAry['bookings'][0]['szCurrency']="USD";
$bookingResponseAry['bookings'][0]['fBookingPrice']=2659.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=664.75;
$bookingResponseAry['bookings'][0]['fInsurancePrice']='';
$bookingResponseAry['bookings'][0]['bInsuranceAvailable']=t($t_base_partner_api.'responseAttribute/no');
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        
                        
                        
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 2 (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/breakBulkExample2');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=15;
$requestAry['dtMonth']=01;
$requestAry['cargo']=array();
$requestAry['cargo'][0]['szShipmentType'] = "BREAK_BULK";
$requestAry['cargo'][0]['fTotalVolume'] = 1.78;
$requestAry['cargo'][0]['fTotalWeight'] = '';
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cbm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']='';
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']='';
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['iInsuranceRequired']="No";
$requestAry['szCurrency']='';
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
"<span class="attribute">errors</span>" => array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1014"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1014');?>"</span></span>
),
"<span class="attribute">errors</span>" => array([1]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1017"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1017');?>"</span></span>
)
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=15;
$requestAry['dtMonth']=01;
$requestAry['cargo']=array();
$requestAry['cargo'][0]['szShipmentType'] = "BREAK_BULK";
$requestAry['cargo'][0]['fTotalVolume'] = 1.78;
$requestAry['cargo'][0]['fTotalWeight'] = 100;
$requestAry['cargo'][0]['szDimensionMeasure'] = t($t_base_partner_api.'requestAttribute/cbm');
$requestAry['cargo'][0]['szWeightMeasure'] = t($t_base_partner_api.'requestAttribute/kg');
$requestAry['szServiceTerm']="DTD";
$requestAry['fCargoValue']='';
$requestAry['szCargoCurrency']='';
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
$requestAry['iInsuranceRequired']="No";
$requestAry['szCurrency']='';
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
"<span class="attribute">errors</span>" => array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1097"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1097');?>"</span></span>
)
</code>
</pre>
                    </div>
                
                <!--END BREAK BULK DIV-->
                
                
                <!--START PALLETS OR PARCEL DIV-->
                <h2  id="palletsParcels">&nbsp;</h2>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/palletsParcels');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/parcelPalletsDescription');?>
                        </p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 1 (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/parcelPalletsExample1');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=10;
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][0]['szPalletType'] = '';
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = 30.342;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['fCargoValue']=1558.96;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['iInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
$requestAry['szServiceTerm']='';
unset($requestAry['cargo'][0]['szPalletType']); 
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$responseAry['status']= "OK";
$responseAry['token']= "XXXXXXXXXXXXXXXXXXXX";
$responseAry['service'][0]['szServiceID'] = "123ABC123ABC";
$responseAry['service'][0]['dtDeliveryDay'] = 12;
$responseAry['service'][0]['dtDeliveryMonth'] = 01;
$responseAry['service'][0]['dtDeliveryYear'] = 2016;
$responseAry['service'][0]['szPackingType'] = t($t_base_partner_api.'responseAttribute/onlyCatons');
$responseAry['service'][0]['szCurrency'] = "USD";
$responseAry['service'][0]['fBookingPrice'] = 157.00;
$responseAry['service'][0]['fVatAmount'] = 39.25;
$responseAry['service'][0]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][0]['fInsurancePrice'] = 73.00;
$responseAry['service'][1]['szServiceID'] = "123ABC123ABC";
$responseAry['service'][1]['dtDeliveryDay'] = 05;
$responseAry['service'][1]['dtDeliveryMonth'] = 02;
$responseAry['service'][1]['dtDeliveryYear'] = 2016;
$responseAry['service'][1]['szPackingType'] = t($t_base_partner_api.'responseAttribute/euroPallet');
$responseAry['service'][1]['szCurrency'] = "USD";
$responseAry['service'][1]['fBookingPrice'] = 1362.00;
$responseAry['service'][1]['fVatAmount'] = 340.5;
$responseAry['service'][1]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][1]['fInsurancePrice'] = 24.00;
$responseAry['service'][2]['szServiceID'] = "123ABC123ABC";
$responseAry['service'][2]['dtDeliveryDay'] = 12;
$responseAry['service'][2]['dtDeliveryMonth'] = 01;
$responseAry['service'][2]['dtDeliveryYear'] = 2016;
$responseAry['service'][2]['szPackingType'] = t($t_base_partner_api.'responseAttribute/onlyCatons');
$responseAry['service'][2]['szCurrency'] = "USD";
$responseAry['service'][2]['fBookingPrice'] = 167.00;
$responseAry['service'][2]['fVatAmount'] = 41.75;
$responseAry['service'][2]['bInsuranceAvailable'] = "Yes";
$responseAry['service'][2]['fInsurancePrice'] = 73.00;
$szResponseString = getResponseAry($responseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=10;
$requestAry['dtMonth']=01;
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = 30.342;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['fCargoValue']=1558.96;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
$requestAry['iInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
unset($requestAry['cargo'][0]['szPalletType']);
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
<?php
$bookingResponseAry['bookings'][0]['dtDeliveryDay']=12;
$bookingResponseAry['bookings'][0]['dtDeliveryMonth']=01;
$bookingResponseAry['bookings'][0]['dtDeliveryYear']=2016;
$bookingResponseAry['bookings'][0]['szPackingType']=t($t_base_partner_api.'responseAttribute/onlyCatons');
$bookingResponseAry['bookings'][0]['szTransportMode']="Courier";
$bookingResponseAry['bookings'][0]['szCurrency']="USD";
$bookingResponseAry['bookings'][0]['fBookingPrice']=167.00;
$bookingResponseAry['bookings'][0]['fVatAmount']=41.75;
$bookingResponseAry['bookings'][0]['fInsurancePrice']=73;
$bookingResponseAry['bookings'][0]['bInsuranceAvailable']=t($t_base_partner_api.'responseAttribute/yes');
$szResponseString = getResponseAry($bookingResponseAry,$mode);
echo $szResponseString;
?>
</code>
</pre>
                        
                        
                        
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> 2 (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <p><?php echo t($t_base_partner_api.'description/parcelPalletsExample2');?></p>
                        <h3>Price Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='price';
$requestAry['dtDay']=10;
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
unset($requestAry['cargo'][0]['szPalletType']);
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = '';
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Half";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = 30.342;
$requestAry['cargo'][1]['iQuantity'] = '';
$requestAry['fCargoValue']=1558.96;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceTerm']='';
$requestAry['szServiceID']='';
$requestAry['token']='';
$requestAry['iInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szPriceCallString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1060"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1060');?>"</span></span>
),
array([1]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1063"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1063');?>"</span></span>
)
</code>
</pre>
                        <h3>Booking Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        
<pre>
<code class="clear json">
<?php
$requestAry['szRequest']='booking';
$requestAry['dtDay']=10;
$requestAry['dtMonth']=01;
$requestAry['cargo'][0]['szShipmentType'] = "PARCEL";
unset($requestAry['cargo'][0]['szPalletType']);
$requestAry['cargo'][0]['iLength'] = 60.7;
$requestAry['cargo'][0]['iWidth'] = 55.9;
$requestAry['cargo'][0]['iHeight'] = 40;
$requestAry['cargo'][0]['iWeight'] = 10.050;
$requestAry['cargo'][0]['iQuantity'] = 1;
$requestAry['cargo'][1]['szShipmentType'] = "PALLET";
$requestAry['cargo'][1]['szPalletType'] = "Euro";
$requestAry['cargo'][1]['iLength'] = 45.3;
$requestAry['cargo'][1]['iWidth'] = 36.45;
$requestAry['cargo'][1]['iHeight'] = 50;
$requestAry['cargo'][1]['iWeight'] = 30.342;
$requestAry['cargo'][1]['iQuantity'] = 2;
$requestAry['fCargoValue']=1558.96;
$requestAry['szCargoCurrency']="USD";
$requestAry['szServiceID']="123ABC123ABC";
$requestAry['token']="XXXXXXXXXXXXXXXXXXXX";
$requestAry['iInsuranceRequired']="Yes";
$requestAry['szCurrency']="USD";
$szResponseString = getRequestAry($requestAry,$mode);
echo $szPriceCallString1;
echo $szBookingString2;
echo $szResponseString;
?>
</code>
</pre>

        <strong>
            <?php echo t($t_base_partner_api.'heading/exampleResponse');?>
        </strong>
                        
<pre>
<code class="clear json">
"<span class="attribute">code</span>" => <span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>" => <span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>" => <span class="value"><span class="string">"XXXXXXXXXXXXXXXXXXXX"</span></span>,
"<span class="attribute">errors</span>" => array([0]=>
"<span class="attribute">code</span>" => <span class="value"><span class="string">"E1097"</span></span>,
"<span class="attribute">description</span>" => <span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1097');?>"</span></span>
)
</code>
</pre>
                    </div>
                
                <!--END PALLETS OR PARCEL DIV-->
                
                <!--START PARAMETER DIV-->
                <h2  id="parameters">&nbsp;</h2>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/parameters');?></h1>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Request Parameter</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szRequest
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szRequest');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Timing Parameters</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtDay
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Format: DD
                                    </span>
                                    <span class="param-type">
                                      Example: 03
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/dtDay');?>
                                    <div>
                                        Validation: required, numeric
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtMonth
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Format: MM
                                    </span>
                                    <span class="param-type">
                                      Example: 01
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/dtMonth');?>
                                    <div>
                                        Validation: required, numeric
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtYear
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Format: YYYY
                                    </span>
                                    <span class="param-type">
                                      Example: 2016
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/dtYear');?>
                                    <div>
                                        Validation: required, numeric
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Cargo Parameter</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipmentType
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: PARCEL
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipmentType');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iLength
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 56.34
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iLength');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iWidth
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 36.34
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iWidth');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iHeight
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 50
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iHeight');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iWeight
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 100
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iWeight');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fTotalVolume
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 0.6
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fTotalVolume');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fTotalWeight
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 100
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fTotalWeight');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iQuantity
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 3
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iQuantity');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szDimensionMeasure
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: cm
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szDimensionMeasure');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szWeightMeasure
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: pounds
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szWeightMeasure');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szServiceTerm
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: DTD
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szServiceTerm');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCargoDescription
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Wooden Chairs
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCargoDescription');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fCargoValue
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 1356.34
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/fCargoValue');?>
                                    <div>
                                        Validation: should be numeric and greater than 0
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCargoCurrency
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: DKK
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCargoCurrency');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Shipper Parameter</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperCompanyName
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: ABC Company
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperCompanyName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperFirstName
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: John
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperFirstName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperLastName
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Miller
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperLastName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperEmail
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: shipper@email.com
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperEmail');?>
                                    <div>
                                        Validation: email
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iShipperCountryDialCode
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 91
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iShipperCountryDialCode');?>
                                    <div>
                                        Validation: numeric
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iShipperPhoneNumber
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 1234567890
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iShipperPhoneNumber');?>
                                    <div>
                                        Validation: numeric
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperAddress
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: A-49 , Phase - 1, Sector 2
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperAddress');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperPostcode
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 440000
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperPostcode');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperCity
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Mumbai
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperCity');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szShipperCountry
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: IN
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szShipperCountry');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Consignee Parameters</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneeCompanyName
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: XYZ Company
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeCompanyName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneeFirstName
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Albert
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeFirstName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneeLastName
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Methew
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeLastName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneeEmail
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: consignee@email.com
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeEmail');?>
                                    <div>
                                        Validation: email
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iConsigneeCountryDialCode
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 46
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iConsigneeCountryDialCode');?>
                                    <div>
                                        Validation: numeric
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iConsigneePhoneNumber
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 123123123
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iConsigneePhoneNumber');?>
                                    <div>
                                        Validation: numeric
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneeAddress
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 51, Block D, West Street
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeAddress');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneePostcode
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/int');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 2100
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneePostcode');?>
                                    <div>
                                        Validation: numeric
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneeCity
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Copenhagen
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeCity');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szConsigneeCountry
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/reqtrue');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: DK
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szConsigneeCountry');?>
                                    <div>
                                        Validation: required
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Response Parameters</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szServiceID
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 3112TTP92242
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szServiceID');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        token
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/conditional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        iInsuranceRequired
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Yes
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/iInsuranceRequired');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Other Parameters</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCurrency
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: USD
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szCurrency');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szLanguage
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: EN
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szLanguage');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szReference
                                    </span>
                                    <span class="param-type">
                                        <?php echo t($t_base_partner_api.'attributeRequired/optional');?>
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Mr.Methew
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'attributeDescription/szReference');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--END PARAMETER DIV-->
                
                <!--START RETURN VALUE DIV-->
                
                <h2  id="returnValues">&nbsp;</h2>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/returnValues');?></h1>
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Price Response Parameters</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        token
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szServiceID
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 3112TTP92242
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szServiceID');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtDeliveryDay
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 09
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryDay');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtDeliveryMonth
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 01
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryMonth');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtDeliveryYear
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 2016
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryYear');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szPackingType
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Pallets and cartons
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szPackingType');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCurrency
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: HKD
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCurrency');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fBookingPrice
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 1324.34
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fBookingPrice');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fVatAmount
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 624.45
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fVatAmount');?>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <span class="param-name">
                                        bInsuranceAvailable
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Yes
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/bInsuranceAvailable');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fInsurancePrice
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 190.39
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fInsurancePrice');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table class="params simple top30">
                        <thead><tr><th colspan="3">Booking Response Parameters</th></tr></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        token
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: TTPHH4UFIPA76CZGR6F3
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/token');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtDeliveryDay
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 12
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryDay');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtDeliveryMonth
                                    </span>
                                    <span class="param-type">
                                      Example: 01
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryMonth');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        dtDeliveryYear
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/date');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 2016
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/dtDeliveryYear');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szBookingStatus
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Confirmed
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingStatus');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szBookingRef
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 1512TSM072
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingRef');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szServiceDescription
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szServiceDescription');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szTransportMode
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Courier
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szTransportMode');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szPackingType
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Only cartons
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szPackingType');?>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <span class="param-name">
                                        szCurrency
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: USD
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCurrency');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fBookingPrice
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 167.50
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fBookingPrice');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fVatAmount
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 41.75
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fVatAmount');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        fInsurancePrice
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/float');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 73
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/fInsurancePrice');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        bInsuranceAvailable
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Yes
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/bInsuranceAvailable');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierName
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: DSV
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierName');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierAddressLine1
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Fruebjergvej 3
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierAddressLine1');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierAddressLine2
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierAddressLine2');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierAddressLine3
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierAddressLine3');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierPostcode
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 2100
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierPostcode');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierCity
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Copenhagen
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierCity');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierRegion
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: Copenhagen West
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierRegion');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierCountry
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: DK
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierCountry');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szBookingContactNumber
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: +45 7199 7299
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingContactNumber');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szBookingContactEmail
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: contact@transporteca.com
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szBookingContactEmail');?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="param-name">
                                        szCarrierCompanyRegistration
                                    </span>
                                    <span class="param-type">
                                      <?php echo t($t_base_partner_api.'attributeType/string');?>
                                    </span>
                                    <span class="param-type">
                                      Example: 23 54 12
                                    </span>
                                </td>
                                <td class="param-desc">
                                    <?php echo t($t_base_partner_api.'bookingResponse/szCarrierCompanyRegistration');?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--END RETURN VALUE DIV-->
                
                <!--START ERROR DIV-->
                <h2  id="errors">&nbsp;</h2>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/errors');?></h1>
                        <table class="simple top30 errors">
                            <thead>
                                <tr>
                                    <th><?php echo t($t_base_partner_api.'heading/codeHeading');?></th>
                                    <th><?php echo t($t_base_partner_api.'heading/descriptionHeading');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="param-type">E1001</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1001');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1002</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1002');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1003</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1003');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1004</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1004');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1005</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1005');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1006</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1006');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1007</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1007');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1008</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1008');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1009</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1009');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1010</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1010');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1011</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1011');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1012</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1012');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1013</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1013');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1014</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1014');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1015</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1015');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1016</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1016');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1017</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1017');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1018</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1018');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1019</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1019');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1020</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1020');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1021</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1021');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1022</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1022');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1023</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1023');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1024</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1024');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1025</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1025');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1026</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1026');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1027</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1027');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1028</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1028');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1029</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1029');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1030</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1030');?></td>
                                </tr>
                                <tr>
                                    <td class="param-type">E1031</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1031');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1032</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1032');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1033</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1033');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1034</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1034');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1035</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1035');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1036</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1036');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1037</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1037');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1038</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1038');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1039</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1039');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1040</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1040');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1041</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1041');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1042</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1042');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1043</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1043');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1044</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1044');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1045</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1045');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1046</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1046');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1047</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1047');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1048</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1048');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1049</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1049');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1050</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1050');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1051</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1051');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1052</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1052');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1053</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1053');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1054</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1054');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1055</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1055');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1056</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1056');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1057</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1057');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1058</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1058');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1059</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1059');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1060</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1060');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1061</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1061');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1062</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1062');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1063</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1063');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1064</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1064');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1065</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1065');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1066</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1066');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1067</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1067');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1068</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1068');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1069</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1069');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1070</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1070');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1071</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1071');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1073</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1073');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1074</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1074');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1075</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1075');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1076</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1076');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1078</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1078');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1079</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1079');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1080</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1080');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1081</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1081');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1082</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1082');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1083</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1083');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1087</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1087');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1088</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1088');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1089</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1089');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1090</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1090');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1091</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1091');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1092</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1092');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1093</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1093');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1094</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1094');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1095</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1095');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1096</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1096');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1097</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1097');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-type">E1098</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1098');?></td>
                                </tr>
                            </tbody>
                        </table>
                </div>
                <!--END ERROR DIV-->
                </div>
              </div>
            </div>
        </div>
        </section>
    </body>
</html>