<!DOCTYPE html>
<?php
session_start();
if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/../"));
}
require_once(__APP_PATH__ . "/inc/constants.php");

$iGetLanguageFromRequest = 1;
$_REQUEST['lang'] = 'english';

require_once(__APP_PATH_LAYOUT__."/ajax_header.php"); 

$t_base_partner_api = "Partner/PartnerApiDoc/"; 
$t_base_partner = "Partner/"; 

$mode = $_REQUEST['mode'];
if(empty($mode))
{
    $mode = 'php';
}

$yourApi=  t($t_base_partner_api.'requestAttribute/yourApi');
$requestmode =t($t_base_partner_api.'requestAttribute/mode');

if($mode == "php")
{
    $szPriceCallString='';
    $szPriceCallString1 .="\Transporteca\Transporteca::setApiKey(".$yourApi.",".$requestmode.");<br>";
}
elseif(trim($mode) == "json")
{
    $szPriceCallString='';
    $szPriceCallString1 .="\Transporteca\Transporteca::setApiKey(\"".$yourApi."\",".$requestmode.");<br>";
}

//echo __MAIN_SITE_HOME_PAGE_URL__;
?>
<html lang='en'>
<head>
<meta charset='utf-8'>
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
<title>
<?php echo t($t_base_partner_api.'heading/titleHeading');?>
</title>
<meta content='Transporteca' name='author'>
<meta content='width=device-width, initial-scale=1.0' name='viewport'>
<meta content='website' property='og:type'>
<link rel="stylesheet" media="all" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/apidocs.css?t=<?php echo time(); ?>" />
<script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
</head>
    <body>
        <header class="line" style="height:56px;">
          <hgroup class="unit padding-left-0">
              <div class="logo" style="margin-top:8px;">
                  <a href="#"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/transporteca-logo.png" alt="Transporteca"></a>
            </div>
            <h1 style="margin-left:309px;">API Documentation</h1>
          </hgroup> 
        </header>
        <section class="line wrapper" id="api_documentation">
          <aside id="sidebar" class="unit side" style="background-color:#404041;margin-top:20px;">
            <div class="ab">
                <nav class="navigation" id="nav">
                  <ul>
                    <li class="active-section section introduction" data-hash='#introduction' onclick="heighLightSelectedMenu('introduction');">
                        <a href='#introduction'>
                            <?php echo t($t_base_partner_api.'leftMenu/introduction');?>
                        </a>
                    </li>
                    
<!--                    <li class="section api-lib" data-hash='#api-lib' onclick="heighLightSelectedMenu('api-lib');">
                        <a href='#api-lib'>
                            <?php echo t($t_base_partner_api.'leftMenu/api_library');?>
                        </a>
                    </li>-->
                    <li class="section authentication" data-hash='#authentication' onclick="heighLightSelectedMenu('authentication');">
                        <a href='#authentication'>
                            <?php echo t($t_base_partner_api.'leftMenu/authentication');?>
                        </a>
                    </li>
                    <li class="section customer-signup" data-hash='#customer-signup' onclick="heighLightSelectedMenu('customer-signup');">
                        <a href='#customer-signup'>
                            <?php echo t($t_base_partner_api.'leftMenu/customer_signup');?>
                        </a>
                    </li>
                    <li class="section customer-signin" data-hash='#customer-signin' onclick="heighLightSelectedMenu('customer-signin');">
                        <a href='#customer-signin'>
                            <?php echo t($t_base_partner_api.'leftMenu/customer_signin');?>
                        </a>
                    </li>
                    <li class="section customer-detail" data-hash='#customer-detail' onclick="heighLightSelectedMenu('customer-detail');">
                        <a href='#customer-detail'>
                            <?php echo t($t_base_partner_api.'leftMenu/customer_detail');?>
                        </a>
                    </li>
                    <li class="section update-customer-profile" data-hash='#update-customer-profile' onclick="heighLightSelectedMenu('update-customer-profile');">
                        <a href='#update-customer-profile'>
                            <?php echo t($t_base_partner_api.'leftMenu/update_customer_profile');?>
                        </a>
                    </li>
                    <li class="section customer-change-password" data-hash='#customer-change-password' onclick="heighLightSelectedMenu('customer-change-password');">
                        <a href='#customer-change-password'>
                            <?php echo t($t_base_partner_api.'leftMenu/customer_change_password');?>
                        </a>
                    </li>
                    <li class="section forgot-password" data-hash='#forgot-password' onclick="heighLightSelectedMenu('forgot-password');">
                        <a href='#forgot-password'>
                            <?php echo t($t_base_partner_api.'leftMenu/forgot_password');?>
                        </a>
                    </li>
                    <li class="section delete-customer-account" data-hash='#delete-customer-account' onclick="heighLightSelectedMenu('delete-customer-account');">
                        <a href='#delete-customer-account'>
                            <?php echo t($t_base_partner_api.'leftMenu/delete_customer_account');?>
                        </a>
                    </li>
                    <li class="section customer-booking-history" data-hash='#customer-booking-history' onclick="heighLightSelectedMenu('customer-booking-history');">
                        <a href='#customer-booking-history'>
                            <?php echo t($t_base_partner_api.'leftMenu/customer_booking_history');?>
                        </a>
                    </li>
                    <!--<li class="section parameters" data-hash='#parameters' onclick="heighLightSelectedMenu('parameters');">
                        <a href='#parameters'>
                            <?php echo t($t_base_partner_api.'leftMenu/parameters');?>
                        </a>
                    </li>
                    <li class="section returnValues" data-hash='#returnValues' onclick="heighLightSelectedMenu('returnValues');">
                        <a href='#returnValues'>
                            <?php echo t($t_base_partner_api.'leftMenu/returnValues');?>
                        </a>
                    </li>-->
                    <li class="section errors" data-hash='#errors' onclick="heighLightSelectedMenu('errors');">
                        <a href='#errors'>
                            <?php echo t($t_base_partner_api.'leftMenu/errors');?>
                        </a>
                    </li>
                </ul>
                </nav>
              </div>
            </aside>
            
            <div id="main" data-swiftype-index="true">
<!--                <div id="doc-lang">
                    <div id="lang-current">
                        <p class="below0"><?php if(trim($mode) == "php"){ ?>PHP<?php }else{?>JSON<?php }?></p>
                    </div>
                    <div id="lang-nav">
                      <p class="below0">Choose Language</p>
                      <ul>
                          <li>
                            <a id="phpLanageLink" href="/api/customer/docs" <?php if(trim($mode) == "php"){ ?>class="selected"<?php }?>>PHP</a>
                          </li>
                          <li>
                            <a id="jsonLanageLink" href="/api/customer/docs/json/" <?php if(trim($mode) == "json"){ ?>class="selected"<?php }?> onclick="chooseAPIDocLanguage('JSON');">JSON</a>
                          </li>                 
                      </ul>
                    </div>
                  </div>-->
              <div class="in ab double">
                  <div class="main">
                    <script type="text/javascript">
                        $(window).load(function(){
                            // Cache selectors
                            var lastId,
                                topMenu = $("#nav"),
                                topMenuHeight = topMenu.outerHeight()+20,
                                // All list items
                                menuItems = topMenu.find("a"),
                                // Anchors corresponding to menu items
                                scrollItems = menuItems.map(function(){
                                  var item = $($(this).attr("href"));
                                  if (item.length) { return item; }
                                });

                            // Bind click handler to menu items
                            // so we can get a fancy scroll animation

                            // Bind to scroll
                            $(window).scroll(function(){
                               // Get container scroll position
                               var fromTop = $(this).scrollTop()+topMenuHeight-200;
                               // Get id of current scroll item
                               var cur = scrollItems.map(function(){
                                 if ($(this).offset().top < fromTop)
                                   return this;
                               });
                               // Get the id of the current element
                               cur = cur[cur.length-1];

                               var id = cur && cur.length ? cur[0].id : "";

                               heighLightSelectedMenu(id);
                            });
                        });
                    </script>
                      
                    <div class="content">
                        
                    <!--START INTRODUCTION ROW-->
                    <a id="introduction"><h2>&nbsp;</h2></a>
                        <div class="namespace-summary margintop">
                            <h1><?php echo t($t_base_partner_api.'leftMenu/introduction');?></h1>
                            <div class="namespace-summary">
                                <p>
                                    <?php echo t($t_base_partner_api.'description/customer_api_introduction');?>
                                </p> 
                            </div>
                            
<!--                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                                <p>
                                   <?php echo t($t_base_partner_api.'description/apiEndPoint');?> 
                                </p>
                            </div>
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/requestMode');?></h3>
                                <p>
                                    <?php echo t($t_base_partner_api.'description/requestMode').": ".t($t_base_partner_api.'description/requestMode_1');?> 
                                </p>
                            </div>-->
                            <div class="namespace-summary">
                                <h3><?php echo t($t_base_partner_api.'heading/responseCode');?></h3>
                                    <p>
                                        <?php echo t($t_base_partner_api.'description/responseCode');?> 
                                    </p>
                                <ul>
                                    <li>
                                        <strong>Code - </strong>200
                                        <div>
                                            <strong>status - </strong>OK
                                        </div>
                                    </li>
                                    <li>
                                        <strong>Code - </strong>400
                                        <div>
                                            <strong>status - </strong>Error
                                        </div>
                                    </li>
                                    <li>
                                        <strong>Code - </strong>401
                                        <div>
                                            <strong>status - </strong><?php echo t($t_base_partner_api.'description/responseCode401');?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                <!--END INTRODUCTION ROW-->

<!--                <a id="api-lib"><h2>&nbsp;</h2></a>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/api_library');?></h1>
                    <p>
                        <?php echo t($t_base_partner_api.'description/api_library_description')." ";?><a href="<?php echo __BASE_URL__.'/api/download/'; ?>"><?php echo  t($t_base_partner_api.'description/click_here'); ?></a>
                    </p>
                </div> -->
                <!--START AUTHENTICATION ROW-->
                <a id="authentication"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/authentication');?></h1>
                        <p>
                            <?php echo t($t_base_partner_api.'description/authenticationDescription');?>
                        </p>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <ul>
                            <li>
                                <?php if(trim($mode) == "php"){ ?>
                                    \Transporteca\Transporteca::setApiKey(<?php echo t($t_base_partner_api.'requestAttribute/yourApi');?>,<?php echo t($t_base_partner_api.'requestAttribute/mode');?>); 
                                <?php }else{ ?>
                                    \Transporteca\Transporteca::setApiKey("<?php echo t($t_base_partner_api.'requestAttribute/yourApi');?>",<?php echo t($t_base_partner_api.'requestAttribute/mode');?>); 
                                <?php } ?> 
                            </li>
                        </ul>
                    </div>
                                    

                <!--END AUTHENTICATION ROW-->
                <!--START Customer Signup ROW-->
                <a id="customer-signup"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/customer_signup');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/customer_signup_description');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/signup/";?></p>
                        
                        <h3>Customer Signup Call</h3>
                        <p>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestSignupAry['szFirstName']='john';
                            $requestSignupAry['szLastName']='deo';
                            $requestSignupAry['szEmail']='xxxx@gmail.com';
                            $requestSignupAry['szPhone']='99xxxxxxxx';
                            $requestSignupAry['iInternationDialCode']='45';
                            $requestSignupAry['szCompanyName']='';
                            $requestSignupAry['szCompanyRegNo']='';
                            $requestSignupAry['isPrivate']='Yes';
                            $requestSignupAry['szPassword']='******';
                            $requestSignupAry['szConfirmPassword']='******';
                            $requestSignupAry['szAddress']='A-12';
                            $requestSignupAry['szPostCode']='1050';
                            $requestSignupAry['szCity']='Copenhagen';
                            $requestSignupAry['szCountry']='DK';
                            $requestSignupAry['iAcceptNewsUpdate']='Yes';
                            
                            $responseSignupAry['code']='200';
                            $responseSignupAry['status']='OK';
                            $responseSignupAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $responseSignupAry['notification']='For furthure communication please use this token. Token is valid for 60 minutes';
                            
                            ?>
<code class="clear json"><?php
$szResponseSignupString = getRequestAry($requestSignupAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::customerSignup({<br>";
echo $szResponseSignupString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<?php
$szSignupResponseString = getRequestAry($responseSignupAry,$mode);
echo $szSignupResponseString;
?>
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Customer Signup Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestSignupAry['szFirstName']='john';
                            $requestSignupAry['szLastName']='deo';
                            $requestSignupAry['szEmail']='xxxx@gmailcom';
                            $requestSignupAry['szPhone']='99xxxxxxxx';
                            $requestSignupAry['iInternationDialCode']='45';
                            $requestSignupAry['szCompanyName']='';
                            $requestSignupAry['szCompanyRegNo']='';
                            $requestSignupAry['isPrivate']='Yes';
                            $requestSignupAry['szPassword']='test1234@!';
                            $requestSignupAry['szConfirmPassword']='test1234@';
                            $requestSignupAry['szAddress']='A-12';
                            $requestSignupAry['szPostCode']='1050';
                            $requestSignupAry['szCity']='Copenhagen';
                            $requestSignupAry['szCountry']='DK';
                            $requestSignupAry['iAcceptNewsUpdate']='Yes';
                            
                            
                            ?>
<code class="clear json"><?php
$szResponseSignupString = getRequestAry($requestSignupAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::customerSignup({<br>";
echo $szResponseSignupString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1110"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1110');?>"</span></span>
},
{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1118"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1118');?>"</span></span>
}
</code>
</code>
</pre>
                    </div>    
                <!--End Customer Signup ROW-->
                
                <!--START Customer Signin ROW-->
                <a id="customer-signin"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/customer_signin');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/customer_signin');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/login/";?></p>
                        <h3>Customer Signin Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestSigninAry['szUsername']='xxxx@gmail.com';
                            $requestSigninAry['szPassword']='******';
                            
                            $responseSigninAry['code']='200';
                            $responseSigninAry['status']='OK';
                            $responseSigninAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $responseSigninAry['notification']='For furthure communication please use this token. Token is valid for 60 minutes';
                            
                            ?>
<code class="clear json"><?php
$szResponseSigninString = getRequestAry($requestSigninAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::customerLogin({<br>";
echo $szResponseSigninString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<?php
$szSignupResponseString = getRequestAry($responseSignupAry,$mode);
echo $szSignupResponseString;
?>
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Customer Signin Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestSigninAry['szUsername']='xxxx@gmailcom';
                            $requestSigninAry['szPassword']='test1234@!';
                            
                            ?>
<code class="clear json"><?php
$szResponseSigninString = getRequestAry($requestSigninAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::customerLogin({<br>";
echo $szResponseSigninString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1110"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1110');?>"</span></span>
},
{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1106"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1106');?>"</span></span>
}
</code>
</code>
</pre>
                    </div>    
                <!--End Customer Signin ROW-->
                
                
                <!--START Customer Profile Details ROW-->
                <a id="customer-detail"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/customer_detail');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/customer_detail');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/profileDetails/";?></p>
                        <h3>Customer Profile Details Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            $responseProfileAry['code']='200';
                            $responseProfileAry['status']='OK';
                            $responseProfileAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $responseProfileAry['notification']='For furthure communication please use this token. Token is valid for 60 minutes';
                            $responseProfileAry["customer"]["szFirstName"]="Ashish";
                            $responseProfileAry["customer"]["szLastName"]="Srivastava";
                            $responseProfileAry["customer"]["szEmail"]="ajay@whiz-solutions.com";
                            $responseProfileAry["customer"]["szPhone"]="9711030117";
                            $responseProfileAry["customer"]["iInternationDialCode"]="91";
                            $responseProfileAry["customer"]["szCompanyName"]="Whiz";
                            $responseProfileAry["customer"]["szCompanyRegNo"]="";
                            $responseProfileAry["customer"]["isPrivate"]="Yes";
                            $responseProfileAry["customer"]["szAddress"]="A22";
                            $responseProfileAry["customer"]["szPostCode"]="1050";
                            $responseProfileAry["customer"]["szCity"]="Copenhagen";
                            $responseProfileAry["customer"]["szCountry"]="Denmark";
                            $responseProfileAry["customer"]["szCurrency"]="DKK";
                            $responseProfileAry["customer"]["szLanguage"]="English";
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::customerProfileDetails({<br>";
echo $szResponseProfileString;
                            ?>
    
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<?php
$szProfileResponseString = getRequestAry($responseProfileAry,$mode);
echo $szProfileResponseString;
?>
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Customer Profile Details Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::customerProfileDetails({<br>";
echo $szResponseProfileString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1126"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1126');?>"</span></span>
}
</code>
</code>
</pre>
                    </div>    
                <!--End Customer Profile Details ROW-->
                
                
                <!--START Customer Update Profile ROW-->
                <a id="update-customer-profile"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/update_customer_profile');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/update_customer_profile_description');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/updateProfile/";?></p>
                        
                        <h3>Update Customer Profile Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestUPAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $requestUPAry['szFirstName']='john';
                            $requestUPAry['szLastName']='deo';
                            $requestUPAry['szEmail']='xxxx@gmail.com';
                            $requestUPAry['szPhone']='99xxxxxxxx';
                            $requestUPAry['iInternationDialCode']='45';
                            $requestUPAry['szCompanyName']='';
                            $requestUPAry['szCompanyRegNo']='';
                            $requestUPAry['isPrivate']='Yes';
                            $requestUPAry['szAddress']='A-12';
                            $requestUPAry['szPostCode']='1050';
                            $requestUPAry['szCity']='Copenhagen';
                            $requestUPAry['szCountry']='DK';
                            $requestUPAry['iAcceptNewsUpdate']='No';
                            
                            $responseUPAry['code']='200';
                            $responseUPAry['status']='OK';
                            $responseUPAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $responseUPAry['notification']='Customer account successfully updated';
                            
                            ?>
<code class="clear json"><?php
$szResponseUPString = getRequestAry($requestUPAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::updateProfile({<br>";
echo $szResponseUPString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<?php
$szUPResponseString = getRequestAry($responseUPAry,$mode);
echo $szUPResponseString;
?>
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Update Customer Profile Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestUPAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $requestUPAry['szFirstName']='john';
                            $requestUPAry['szLastName']='deo';
                            $requestUPAry['szEmail']='xxxx@gmail.com';
                            $requestUPAry['szPhone']='99xxxxxxxx';
                            $requestUPAry['iInternationDialCode']='45';
                            $requestUPAry['szCompanyName']='';
                            $requestUPAry['szCompanyRegNo']='';
                            $requestUPAry['isPrivate']='Yes';
                            $requestUPAry['szAddress']='A-12';
                            $requestUPAry['szPostCode']='1050';
                            $requestUPAry['szCity']='Copenhagen';
                            $requestUPAry['szCountry']='DK';
                            $requestUPAry['iAcceptNewsUpdate']='No';
                            
                            
                            ?>
<code class="clear json"><?php
$szResponseUPString = getRequestAry($requestUPAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::updateProfile({<br>";
echo $szResponseUPString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1126"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1126');?>"</span></span>
},{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1110"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1110');?>"</span></span>
},
{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1124"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1124');?>"</span></span>
}
</code>
</code>
</pre>
                    </div>    
                <!--End Customer Update Profile ROW-->
                
                <!--START Customer Change Password ROW-->
                <a id="customer-change-password"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/customer_change_password');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/customer_change_password');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/changePassword/";?></p>
                        <h3>Customer Change Password Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestCPDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $requestCPDetailAry['szCurrentPassword']="*******";
                            $requestCPDetailAry['szPassword']="*******";
                            $requestCPDetailAry['szConfirmPassword']="*******";
                            
                            
                            
                            
                            $responseCPAry['code']='200';
                            $responseCPAry['status']='OK';
                            $responseCPAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $responseCPAry['notification']='Your password has been changed successfully.';
                            ?>
<code class="clear json"><?php
$szResponseCPString = getRequestAry($requestCPDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::changePassword({<br>";
echo $szResponseCPString;
                            ?>
    
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<?php
$szCPResponseString = getRequestAry($responseCPAry,$mode);
echo $szCPResponseString;
?>
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Customer Change Password Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestCPDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $requestCPDetailAry['szCurrentPassword']="*******";
                            $requestCPDetailAry['szPassword']="*******";
                            $requestCPDetailAry['szConfirmPassword']="*******";
                            
                            ?>
<code class="clear json"><?php
$szResponseCPString = getRequestAry($requestCPDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::changePassword({<br>";
echo $szResponseCPString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1126"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1126');?>"</span></span>
},{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1128"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1128');?>"</span></span>
},{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1118"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1118');?>"</span></span>
}
</code>
</code>
</pre>
                    </div>    
                <!--End Customer Change Password ROW-->
                
                
                
                <!--START Forgot Password ROW-->
                <a id="forgot-password"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/forgot_password');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/forgot_password');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/forgotPassword/";?></p>
                        <h3>Forgot Password Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestFPDetailAry['szEmail']='xxxx@xxx.com';
                            
                            
                            
                            
                            $responseFPAry['code']='200';
                            $responseFPAry['status']='OK';
                            $responseFPAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $responseFPAry['notification']="A new password has been sent to your email. The password can be changed in My Account";
                            ?>
<code class="clear json"><?php
$szResponseFPString = getRequestAry($requestFPDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::forgotPassword({<br>";
echo $szResponseFPString;
                            ?>
    
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<?php
$szFPResponseString = getRequestAry($responseFPAry,$mode);
echo $szFPResponseString;
?>
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Forgot Password Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestFPDetailAry['szEmail']='xxxx@xxx.com';
                            
                            ?>
<code class="clear json"><?php
$szResponseFPString = getRequestAry($requestFPDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::forgotPassword({<br>";
echo $szResponseFPString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1109"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1109');?>"</span></span>
},{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1110"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1110');?>"</span></span>
},{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1130"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1130');?>"</span></span>
}
</code>
</code>
</pre>
                    </div>    
                <!--End Customer Forgot Password ROW-->
                
                <!--START Delete Customer Account ROW-->
                <a id="delete-customer-account"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/delete_customer_account');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/delete_customer_account');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint');?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/deleteAccount/";?></p>
                        <h3>Delete Customer Account Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestDCADetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $requestDCADetailAry['szEmail']='xxxx@xxx.com';
                            $requestDCADetailAry['szPassword']='*******';
                            
                            
                            
                            
                            $responseDCAAry['code']='200';
                            $responseDCAAry['status']='OK';
                            $responseDCAAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $responseDCAAry['notification']="Customer account successfully deleted from transpoteca.";
                            ?>
<code class="clear json"><?php
$szResponseFPString = getRequestAry($requestFPDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::deleteCustomerAccount({<br>";
echo $szResponseFPString;
                            ?>
    
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<?php
$szDCAResponseString = getRequestAry($responseDCAAry,$mode);
echo $szDCAResponseString;
?>
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Delete Customer Account Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestDCADetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            $requestDCADetailAry['szEmail']='xxxx@xxx.com';
                            $requestDCADetailAry['szPassword']='*******';
                            
                            ?>
<code class="clear json"><?php
$szResponseDCAString = getRequestAry($requestDCADetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::deleteCustomerAccount({<br>";
echo $szResponseDCAString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1109"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1109');?>"</span></span>
},{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1110"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1110');?>"</span></span>
},{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1136"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1136');?>"</span></span>
}
</code>
</code>
</pre>
                    </div>    
                <!--End Delete Customer Account ROW-->
                
                
                <!--START Customer Booking History ROW-->
                <a id="customer-booking-history"><h2>&nbsp;</h2></a>
                    <div id='' class="namespace-summary margintop">
                        <h1><?php echo t($t_base_partner_api.'leftMenu/customer_booking_history');?></h1>
                        <p><?php echo t($t_base_partner_api.'description/customer_booking_history');?></p>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/successfulResponse');?>)</h2>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint')." for all bookings";?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/bookings/";?></p>
                        <h3>Customer All Booking History Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::bookings({<br>";
echo $szResponseProfileString;
                            ?>
    
                            </code> 
</pre>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint')." for all active bookings";?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/bookings/active/";?></p>
                        <h3>Customer All Active Booking History Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::activebookings({<br>";
echo $szResponseProfileString;
                            ?>
    
                            </code> 
</pre>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint')." for all archived bookings";?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/bookings/archived/";?></p>
                        <h3>Customer All Archived Booking History Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::archivedtbookings({<br>";
echo $szResponseProfileString;
                            ?>
    
                            </code> 
</pre>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint')." for all draft bookings";?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/bookings/draft/";?></p>
                        <h3>Customer All Draft Booking History Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::draftbookings({<br>";
echo $szResponseProfileString;
                            ?>
    
                            </code> 
</pre>
                        <h3><?php echo t($t_base_partner_api.'heading/apiEndPoint')." for all hold bookings";?></h3>
                        <p><?php echo __MAIN_SITE_HOME_PAGE_URL__."/api/customer/bookings/hold/";?></p>
                        <h3>Customer All Hold Booking History Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::holdbookings({<br>";
echo $szResponseProfileString;
                            ?>
    
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
                        
                        
<pre>
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"OK"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">myBookingList</span>":[{
"<span class="attribute">szBookingRandomNum</span>":<span class="value"><span class="string">TB6YCBRFUANG</span></span>,
"<span class="attribute">szOriginCity</span>":<span class="value"><span class="string">Mumbai</span></span>
"<span class="attribute">szDestinationCity</span>":<span class="value"><span class="string">Copenhagen</span></span>,
"<span class="attribute">dtExpectedDelivery</span>":<span class="value"><span class="string">"05-12-2015"</span></span>
"<span class="attribute">dtExpectedPickup</span>":<span class="value"><span class="string">"25-11-2015"</span></span>,
"<span class="attribute">szServiceDescription</span>":<span class="value"><span class="string">Transportation from shippers door in Mumbai to consignees door in Copenhagen and customs clearance in India and Denmark. This is also known as DAP terms.</span></span>
"<span class="attribute">szServiceName</span>":<span class="value"><span class="string">""</span></span>,
"<span class="attribute">dtBookingConfirmed</span>":<span class="value"><span class="string">18-11-2016</span></span>
"<span class="attribute">szBookingRef</span>":<span class="value"><span class="string">1611TQK003</span></span>,
"<span class="attribute">fCustomerAmountPaid</span>":<span class="value"><span class="string">265</span></span>
"<span class="attribute">szBooingCurrency</span>":<span class="value"><span class="string">USD</span></span>,
"<span class="attribute">szLogoFileName</span>":<span class="value"><span class="string">Worldtrans_1405949150_1406713347_1407492174_pdflogo_1407492209.jpg</span></span>
"<span class="attribute">szDisplayName</span>":<span class="value"><span class="string">ShipIt Very Loop23</span></span>,
"<span class="attribute">szBookingStatus</span>":<span class="value"><span class="string">Active</span></span>
"<span class="attribute">szCargoText</span>":<span class="value"><span class="string">0.06 cbm, 8 kg, CD</span></span>
}
</code>
</pre>
                        <h2><?php echo t($t_base_partner_api.'heading/example');?> (<?php echo t($t_base_partner_api.'heading/errorResponse');?>)</h2>
                        <h3>Customer Booking History Call</h3>
                        <strong>
                            <?php echo t($t_base_partner_api.'heading/exampleRequest');?>
                        </strong>
                        <pre>
                            <?php
                            $requestProfileDetailAry['token']='TAJCTNR3TN9BTHHXPPBM';
                            
                            ?>
<code class="clear json"><?php
$szResponseProfileString = getRequestAry($requestProfileDetailAry,$mode);
echo $szPriceCallString1;
echo "\Transporteca\Customer::bookings({<br>";
echo $szResponseProfileString;
                            ?>
                            </code> 
</pre>
<strong>
<?php echo t($t_base_partner_api.'heading/exampleResponse');


?>
</strong>
                        
<pre>
<code class="clear json">
{
"<span class="attribute">code</span>":<span class="value"><span class="number">200</span></span>,
"<span class="attribute">status</span>":<span class="value"><span class="string">"Error"</span></span>,
"<span class="attribute">token</span>":<span class="value"><span class="string">"TTP8DVLLFVP8U60JTULC"</span></span>,
"<span class="attribute">errors</span>":[{
"<span class="attribute">code</span>":<span class="value"><span class="string">"E1126"</span></span>,
"<span class="attribute">description</span>":<span class="value"><span class="string">"<?php echo t($t_base_partner.'error/E1126');?>"</span></span>
}
</code>
</pre>
                    </div>    
                <!--End Customer Booking History ROW-->
                
                <!--START ERROR DIV-->
                <a id="errors"><h2>&nbsp;</h2></a>
                <div id='' class="namespace-summary margintop">
                    <h1><?php echo t($t_base_partner_api.'leftMenu/errors');?></h1>
                    <p><?php echo t($t_base_partner_api.'description/errorCodeDescription');?></p>
                        <table class="simple top30">
                            <thead>
                                <tr>
                                    <th><?php echo t($t_base_partner_api.'heading/codeHeading');?></th>
                                    <th><?php echo t($t_base_partner_api.'heading/descriptionHeading');?></th>
                                </tr>
                            </thead>
                            <tbody>                                
                                <tr>
                                    <td  class="param-desc">E1103</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1103');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1104</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1104');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1105</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1105');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1106</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1106');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1107</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1107');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1108</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1108');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1109</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1109');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1110</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1110');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1111</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1111');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1112</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1112');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1113</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1113');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1114</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1114');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1115</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1115');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1116</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1116');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1117</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1117');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1118</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1118');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1119</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1119');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1120</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1120');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1121</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1121');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1122</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1122');?></td>
                                </tr><tr>
                                    <td  class="param-desc">E1123</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1123');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1124</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1124');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1125</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1125');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1126</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1126');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1127</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1127');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1128</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1128');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1129</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1129');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1130</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1130');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1131</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1131');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1132</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1132');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1133</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1133');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1134</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1134');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1135</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1135');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1136</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1136');?></td>
                                </tr>
                                <tr>
                                    <td  class="param-desc">E1137</td>
                                    <td class="param-desc"><?php echo t($t_base_partner.'error/E1137');?></td>
                                </tr>
                                
                                
                            </tbody>
                        </table>
                </div>
                <!--END ERROR DIV-->
                </div>
              </div>
            </div>
        </div>
        </section>
    </body>
</html>