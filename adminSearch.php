<?php
/**
 * Customer Account Confirmation 
 */
 ob_start();
session_start();
$szMetaTitle= __EMAIL_CONFIRMATION_META_TITLE__;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php" );
  
$t_base = "Users/AccountPage/";

$kBooking = new cBooking();
$kUser = new cUser();
 
$dtCurrentDBTime = $kBooking->getRealNow();
$szBookingRandomNum = trim($_REQUEST['booking_random_num']); 
$idAdmin = $_REQUEST['admin_id'];
 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
        
$postSearchAry = array();
$postSearchAry = $kBooking->getBookingDetails($idBooking);
  
$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
$kConfig = new cConfig();
$kConfig->loadCountry($postSearchAry['idOriginCountry']);
$szCountryStrUrl = $kConfig->szCountryISO ;
$kConfig->loadCountry($postSearchAry['idDestinationCountry']);
$szCountryStrUrl .= $kConfig->szCountryISO ;
   
ob_end_clean();
$_SESSION['anonymous_user_logged_in'] = 1; 
$_SESSION['admin_test_search_'.$szBookingRandomNum] = $szBookingRandomNum; 

$redirect_url = __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ; 
header("location:".$redirect_url);
die; 


/*
if(empty($postSearchAry))
{
    ob_end_clean();
    header("location: ".__BASE_URL__);
    die;
}
$kWHSSearch = new cWHSSearch();
if($_SESSION['user_id']>0)
{
    $kUser = new cUser();
    $kUser->getUserDetails($_SESSION['user_id']);

    $idAdmin = $_SESSION['admin_id'];
    $dtCurrentDBTime = $kBooking->getRealNow();
 
    $res_ary['iBookingInitiatedByAdmin'] = 1;
    $res_ary['iAnonymusBooking'] = 1;
    $res_ary['idAdminInitiatedBy'] = $idAdmin;
    $res_ary['dtBookingInitiatedByAdmin'] = $dtCurrentDBTime ; 
    $res_ary['idUser'] = $kUser->id ;				
    $res_ary['szFirstName'] = $kUser->szFirstName ;
    $res_ary['szLastName'] = $kUser->szLastName ;
    $res_ary['szEmail'] = $kUser->szEmail ;
    $res_ary['szCustomerAddress1'] = $kUser->szAddress1;
    $res_ary['szCustomerAddress2'] = $kUser->szAddress2;
    $res_ary['szCustomerAddress3'] = $kUser->szAddress3;
    $res_ary['szCustomerPostCode'] = $kUser->szPostcode;
    $res_ary['szCustomerCity'] = $kUser->szCity;
    $res_ary['szCustomerCountry'] = $kUser->szCountry;
    $res_ary['szCustomerState'] = $kUser->szState;
    $res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
    $res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
    $res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;
    
    if(!empty($res_ary))
    {
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    } 
    
    $update_query = rtrim($update_query,",");   
    $kBooking->updateDraftBooking($update_query,$idBooking) ; 
}
    
$postSearchAry = array();
$postSearchAry = $kBooking->getBookingDetails($idBooking);

if($postSearchAry['idUser']>0)
{ 
    $idUser = $postSearchAry['idUser']; 
    $_SESSION['user_id'] = $idUser;
    $_SESSION['anonymous_user_logged_in'] = 1; 
    //$_SESSION['admin_test_search_'.$szBookingRandomNum] = $szBookingRandomNum; 
}  
$szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
$kConfig = new cConfig();
$kConfig->loadCountry($postSearchAry['idOriginCountry']);
$szCountryStrUrl = $kConfig->szCountryISO ;
$kConfig->loadCountry($postSearchAry['idDestinationCountry']);
$szCountryStrUrl .= $kConfig->szCountryISO ;
  
$iNumRecordFound = 1;
if($iNumRecordFound>0)
{
    ob_end_clean();
    $redirect_url = __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
    header("location:".$redirect_url);
    die;
}
else
{
    ob_end_clean();
    //$redirect_url = __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
    $redirect_url = __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ; 
    header("location:".$redirect_url);
    die;
}  
 * 
 */
die;    
?>