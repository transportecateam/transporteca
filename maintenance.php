<?php

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title>Booking and Price Comparison for International Shipping and Transportation with Forwarders</title>
 <style type="text/css">
    *{ margin: 0; padding: 0;}
    body{background-color:#FFFFFF;font-family:Arial;font-weight:lighter;}
    div#wrapper
    {
        margin:0 auto;
        width:1200px;
    }
    div#main{
        width:1200px;height:644px;margin:0px auto;
             background-image:url(/images/search-background.jpg);background-repeat:no-repeat;
             float:left;
             margin:0 auto;
    }
    div#main #maint_message{
        margin:200px 0 0 355px;
        float:left;
        width:484px;
        color:#fff;
        font-family:Helvetica,Arial,sans-serif;
        text-align: center;
        font-size:18px;
    }
 </style>
</head>
    <body>
        <div id="wrapper">
            <div id="main">
                <div id="maint_message">
                   <h3> Transporteca is undergoing scheduled maintenance. We'll be back live shortly. </h3>
                </div>
            </div>
        </div>
    </body>
</html>