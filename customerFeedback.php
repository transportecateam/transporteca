<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "BookingReceipt/";
$kBooking = new cBooking();
$kNPS = new cNPS();
$path=$_REQUEST['path'];
$sucessflag=false;
if(!empty($_POST['custfeedbackArr']) && $_POST['flag']=='submitFeedback')
{
	if($kNPS->addCustomerFeedBack($_POST['custfeedbackArr'],$_SESSION['user_id']))
	{
		$sucessflag=true;
	}
}
if(!$sucessflag)
{
?>
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup rating-popup" style="width:480px;">
<form name="customerFeedBack" id="customerFeedBack" method="post">
	<p class="close-icon" align="right">
	<a onclick="close_feedback_popup();" href="javascript:void(0);">
	<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
	</a>
	</p>
	<h5><strong><?=t($t_base.'fields/feedback_popup_heading')?></strong></h5>
	<p><?=t($t_base.'fields/feedback_popup_line_1')?></p><br />
	<p class="clearfix"><span class="left-text" style="width:80px;"><i><?=t($t_base.'fields/not_at_all_likely')?></i></span>
	<? if(__CUSTOMER_FEEDBACK_RATING__>0)
	{
		for($i=0;$i<=__CUSTOMER_FEEDBACK_RATING__;++$i)
		{?>
			<span class="readio-container"><?=$i?><input type="radio" name="custfeedbackArr[iScore]" id="iScore_<?=$i?>" value="<?=$i?>" onclick="enable_feedback_submit_button(this.value);"></span>
		<?	
		}
	}?>
	<span class="right-text"><i><?=t($t_base.'fields/extremely_likely')?></i></span></p><br />
	<p><?=t($t_base.'fields/feedback_popup_line_2')?></p>
	<p><textarea cols="30" rows="5" name="custfeedbackArr[szComment]" id="szComment" style="width:444px"></textarea></p>
	<br />
	<p align="center">
	<input type="hidden" name="path" value="<?=$path?>" id="path">
	<a href="javascript:void(0)" onclick="redirect_page_customer_feedback('<?=$path?>');" class="button2"><span><?=t($t_base.'fields/no_thanks')?></span></a> <a href="javascript:void(0)" style="opacity:0.4" class="button1" id="submit_feedback_button"><span><?=t($t_base.'fields/submit')?></span></a></p>
</form>
</div>
</div>
<?
}
if($sucessflag)
{?>
<script>
redirect_page_customer_feedback('<?=$path?>');
</script>
<? }?>