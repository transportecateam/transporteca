<?php
/*
**
 * Customer Login
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$kCourierServices = new cCourierServices();
if(!empty($_POST['shipperArr']))
{
    if($kCourierServices->updateShipperDetails($_POST['shipperArr']))
    {    
        $kBooking = new cBooking();
        $bookingDataArr=$kBooking->getExtendedBookingDetails($_POST['shipperArr']['idBooking']);
        $viewCourierLabel=__BASE_STORE_URL__."/viewCourierLabel/".$bookingDataArr['szBookingRef']."/";
        echo "SUCCESS||||";
        echo download_courier_label_html($kCourierServices)."||||";
        echo $viewCourierLabel;
        die();
    }
    else
    {
        echo "ERROR||||";
        echo download_courier_label_html($kCourierServices);
        die();
    }
}
?>
