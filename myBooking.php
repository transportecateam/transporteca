<?php
/**
 * Customer My Account  
 */
 ob_start();
session_start();
$szMetaTitle= __MY_BOOKING_META_TITLE__;
$szMetaKeywords = __MY_BOOKING_META_KEYWORDS__;
$szMetaDescription = __MY_BOOKING_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
 
$confirm_key = $_REQUEST['myBookingkey'];
if(!empty($confirm_key))
{
    $keyAry = explode('__',$confirm_key);
    $szConfirmationBookingKey = $keyAry[0];	
} 
$kUser_new = new cUser();
$kBooking = new cBooking();
$kRegisterShipCon = new cRegisterShipCon();
if((int)$_SESSION['user_id']<=0)
{
    $redirect_url='';
    if((int)$szConfirmationBookingKey>0)
    { 
        $bookingDetailsAry = array();
        $bookingDetailsAry = $kBooking->getBookingDetails($szConfirmationBookingKey);
        
        $idCustomer = $bookingDetailsAry['idUser'];
        if($idCustomer>0)
        {
            $kUser_new->manualLogin($idCustomer);
            
            if($idCustomer>0)
            {
                $updateUserDetailsAry = array();  
                $updateUserDetailsAry['iConfirmed'] = 1 ; 

                if(!empty($updateUserDetailsAry))
                {
                    $update_user_query = '';
                    foreach($updateUserDetailsAry as $key=>$value)
                    {
                        $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_user_query = rtrim($update_user_query,",");  
                $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer); 
            }
        } 
    } 
} 
 
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;

require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Booking/MyBooking/";
//checkAuth();
 $iLanguage = getLanguageId();   
if((int)$_SESSION['user_id']<=0)
{ 
    $redirect_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$_SERVER['REQUEST_URI']; 
    ?>
    <script type="text/javascript">
    ajaxLogin('<?=$redirect_url?>');
    </script>
    <?php	
    exit();
}

if($_SESSION['user_id']>0)
{
    unset($_SESSION['valid_confirmation']);	
}

$email_notification_flag = (int)$_REQUEST['unsubscribe_notification'];
$price_changed_flag = (int)$_REQUEST['price_changed']; 
         
if($email_notification_flag == 1)
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));  
    ?>
    <script type="text/javascript">
        display_email_notification_update_popup('<?php echo $szBookingRandomNum;?>');
    </script>	
    <?php 
} 
else if($price_changed_flag==1)
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
    ?>
    <script type="text/javascript">
        display_price_changed_popup('<?=$_SESSION['booking_page_url']?>','<?=$szBookingRandomNum?>');
    </script>	
    <?php
}
else
{
/*
	$_SESSION['booking_id'] = '';
	unset($_SESSION['booking_id']);	
*/
    $_SESSION['booking_register_shipper_id'] = '';
    $_SESSION['booking_register_consignee_id'] = '';
    unset($_SESSION['booking_register_shipper_id']);
    unset($_SESSION['booking_register_consignee_id']); 
}	
$showRatingFlag=false;
if((int)$_SESSION['customer_booking_confirmation_key']>0)
{
    $szConfirmationBookingKey=$_SESSION['customer_booking_confirmation_key'];
    $_SESSION['customer_booking_confirmation_key'] = '';
    unset($_SESSION['customer_booking_confirmation_key']);
}
if(!empty($szConfirmationBookingKey) && (int)$szConfirmationBookingKey>0)
{
    if($kBooking->isBookingBelongsToCustomer($szConfirmationBookingKey,$_SESSION['user_id'],true))
    {
        if($kBooking->getBookingRatingDetails($szConfirmationBookingKey))
        {
            $rate_id=$kBooking->id;
            $showRatingFlag=false;
            $idBooking=$szConfirmationBookingKey; 
        }
        else
        {
                $showRatingFlag=true;
                $idBooking=$szConfirmationBookingKey;
        }
        $_SESSION['customer_booking_confirmation_key'] = '';
        unset($_SESSION['customer_booking_confirmation_key']);
    }
    else
    {
            $showRatingFlag=false;
    ?>
        <div id="invalid_booking">
            <div id="popup-bg"></div>
            <div id="popup-container">
                <div class="popup signin-popup signin-popup-verification">
                    <h5><strong><?=t($t_base.'title/invalid_booking');?></strong></h5>
                    <p><?=t($t_base.'messages/invalid_booking_text');?></p><br/>
                    <p align="center"><a href="javascript:void(0)" class="button1" onclick="hide_invalid_booking_popup();"><span><?=t($t_base.'fields/close');?></span></a></p>
                </div>
            </div>
        </div>	
    <?php 
        $_SESSION['customer_booking_confirmation_key'] = '';
        unset($_SESSION['customer_booking_confirmation_key']);
    }
}
?>
    <!--
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.css" type="text/css">
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.blue.css" type="text/css">
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.plastic.css" type="text/css">
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.plastic.css" type="text/css">
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.round.plastic.css" type="text/css">
 
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jshashtable-2.1_src.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jquery.numberformatter-1.2.3.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/tmpl.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jquery.dependClass-0.1.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/draggable-0.1.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jquery.slider.js"></script>
-->
<div id="my-account-body">
    <div id="booking_expire_div" style="display:none;"></div>
    <div id="change_price_div" style="display:none"></div>
    <div id="signin_help_pop_right" class="help-pop right"></div>
    <div id="email_notification_description_div" style="display:none"></div>
    <div class="hsbody-2-left filter_style">
        <div id="mybooking_left">
        <?php 
            //require_once( __APP_PATH__ ."/bookingLeftNav.php" ); 
            require_once( __APP_PATH_LAYOUT__ ."/bookingLeftNav.php" ); 
        ?>
        </div>
    </div> 
    <?php require_once( __APP_PATH__ ."/ajax_myBooking.php" ); ?>
</div> 

<?php 
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>