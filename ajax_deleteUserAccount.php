<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/DeleteAccount/";
$t_base_error = "Error";
$successPopup=false;
//print_r($_POST['deleteAccountArr']);
if(!empty($_POST['deleteAccountArr']))
{
	if($kUser->checkdeleteUserAccount($_POST['deleteAccountArr']))
	{
		$successPopup=true;
	}
}
if(!empty($_POST['deleteUserId']))
{
	if($kUser->deactivateUserAccount($_POST['deleteUserId']))
	{
		$redirect_url=__BASE_URL__;
		unset($_SESSION['user_id']);
	?>
		<script type="text/javascript">
		$(location).attr('href','<?=$redirect_url?>');
		</script>
	<?php			
		exit();
	}
}

$szLabelClassName = "class='common-fields-container clearfix' ";

if($successPopup)
{
	setCookie('__USER_EMAIL_COOKIE__','');
	$_COOKIE['__USER_EMAIL_COOKIE__'] = '';
	unset($_COOKIE['__USER_EMAIL_COOKIE__']);
?>
<script type="text/javascript">
addPopupScrollClass();
</script>
<div id="delete_account_user_id">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup delete-account-popup">
<p class="close-icon" align="right">
<a onclick="cancel_remove_user_popup('delete_account_user_id','hsbody-2');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<form name="delete_account" id="delete_account" method="post">
	<h5><?=t($t_base.'messages/delete_account');?></h5>
	<p><?=t($t_base.'messages/sure_delete_account');?>?</p>
	<input type="hidden" name="deleteUserId" id="deleteUserId" value="<?=$_POST['deleteAccountArr']['idUser']?>">
	<br />
	<p align="center"><a href="javascript:void(0)" class="button1" onclick="delete_account_user();"><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_remove_user_popup('delete_account_user_id','hsbody-2');"><span><?=t($t_base.'fields/cancel');?></span></a></p>
	</form>
</div>
</div>
</div>
<?php
}
if(!empty($kUser->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kUser->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }?>
<form name="deleteUserAccount" id="deleteUserAccount" method="post">
	<h2 class="create-account-heading"><?=t($t_base.'title/are_yor_sure_delete_account');?></h2>
						
	<p class="f-size-22"><?=t($t_base.'title/permanently_delete_your_account');?></p>
	<br />
	<br />
	<p><?=t($t_base.'title/delete_your_account');?></p><br />
	<br />
        <label <?php echo $szLabelClassName; ?>>
            <span class="field-name" style="width:49%;"><?=t($t_base.'fields/email_address_deleted');?></span>
            <span class="field-container" style="width:49%;"><input type="text" name="deleteAccountArr[szEmail]" id="szDeleteEmail" size="28" value="<?=$_POST['deleteAccountArr']['szEmail']?>"/></span>
        </label> 
        
        <label <?php echo $szLabelClassName; ?>>
            <span class="field-name" style="width:49%;"><?=t($t_base.'fields/password');?></span>
            <span class="field-container" style="width:49%;"><input type="password" name="deleteAccountArr[szPassword]" id="szPassword" size="28" /></span>
        </label> 
	<br />
	<p align="center">
	<input type="hidden" name="deleteAccountArr[szLoginEmail]" id="szLoginEmail" size="48" value="<?=$_POST['deleteAccountArr']['szLoginEmail'] ? $_POST['deleteAccountArr']['szLoginEmail'] :$kUser->szEmail?>"/>
	<input type="hidden" name="deleteAccountArr[idUser]" id="idUser" size="48" value="<?=$_POST['deleteAccountArr']['idUser'] ? $_POST['deleteAccountArr']['idUser'] :$kUser->id?>"/>
	<a href="javascript:void(0)" class="button1" style="margin-left: 109px;" onclick="delete_user_account();"><span><?=t($t_base.'fields/delete_account');?></span></a></p>
</form>