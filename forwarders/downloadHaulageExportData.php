<?php
ob_start();
session_start();
//error_reporting (E_ALL);
ini_set('max_execution_time',1200);
ini_set('MEMORY_LIMIT','512M');
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//echo "hi";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$Excel_export_import=new cExport_Import();
	$kConfig = new cConfig();
if((int)$_SESSION['forwarder_user_id']==0 || empty($_POST['dataExportArr']))
{
	header('Location:'.__BASE_URL__);
	exit();	
}

if(isset($_SESSION['forwarder_admin_id']) && $_SESSION['forwarder_admin_id']>0)
{
	//$kForwarderContact->load($_SESSION['forwarder_admin_id']);
	$kForwarder->load($_SESSION['forwarder_id']);
}
else
{
	$kForwarderContact->load($_SESSION['forwarder_user_id']);
	$kForwarder->load($kForwarderContact->idForwarder);
}

require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
$objPHPExcel=new PHPExcel();
$szDisplayName=str_replace(" ","_",$kForwarder->szDisplayName);
$szDisplayName=str_replace("/","_",$szDisplayName);
$folderName=$szDisplayName."_".$kForwarderContact->idForwarder;
$exportData="exportData";
if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName)) {
    mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName);
    chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName,0777);
    if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData)) {
    	 mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData);
    	 chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData,0777);
    }
}
else
{
 if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData)) {
    	 mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData);
    	 chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData,0777);
    }
}
$fh = fopen(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData."/index.php", "w+");
$file_name= __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData."/Transporteca_Haulage_upload_sheet_".$folderName.".xlsx";
$sheetindex=0;
if(!empty($_POST['dataExportArr']))
{
	$idhaulageCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idhaulageWarehouse']);
	if(!empty($idhaulageCountryWarehouseArr))
	{
		for($i=0;$i<count($idhaulageCountryWarehouseArr);++$i)
		{
			$idhaulageCountryWarehouseArrary=explode("_",$idhaulageCountryWarehouseArr[$i]);
			$idhaulageWarehouseArr[$i]['idWarehouse']=$idhaulageCountryWarehouseArrary[0];
			$idhaulageWarehouseArr[$i]['idPricingModel']=$idhaulageCountryWarehouseArrary[2];
		}
	}
	
	$idCurrency=$_POST['dataExportArr']['idCurrency'];
	$inActiveService=$_POST['dataExportArr']['inActiveService'];
	array_unique($idhaulageWarehouseArr,SORT_NUMERIC);
	if($Excel_export_import->warehouseHaulageDataExport($objPHPExcel,$sheetindex,$file_name,$kConfig,$idhaulageWarehouseArr,$kForwarderContact->idForwarder,$_POST['dataExportArr']['downloadType'],$inActiveService,$idCurrency))
	{	
			ob_clean();
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			//$f = fopen($file_name,'rb');
			//$fsize=filesize($file_name);
			header('Content-Disposition: attachment; filename=Transporteca_Haulage_upload_sheet_'.date('Ymd').'.xlsx');
			ob_clean();
			flush();
			readfile($file_name);
			exit;
		
	}
}
?>