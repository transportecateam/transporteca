<?php
/**
 * Resending Activation Code
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "Forwarders/homepage/";
 
if($_SESSION['forwarder_user_id']>0 && (int)$_SESSION['forwarder_admin_id']==0)
{	
	$idForwarderContacts=$_SESSION['forwarder_user_id'];
	if($kForwarderContact->resendActivationCode($idForwarderContacts))
	{
		$msg_success=t($t_base.'messages/resend_activation_key_success');
	}
	else
	{
	 $msg_error=t($t_base.'messages/resend_activation_key_error');
		
	}
}
else
{
	$msg_error=t($t_base.'messages/invalid_user');
}

if(!empty($msg_success)){
//ob_flush();
 //flush();	
$redirect_url=urlencode($_REQUEST['redirect_uri']);	

if(__ENVIRONMENT__ == "DEV_LIVE")
{
	setcookie(__COOKIE_NAME__ , $redirect_url,time() + __COOKIE_TIME__ , '/',$_SERVER['HTTP_HOST'],true);
}
else
{
	setcookie(__COOKIE_NAME__ , $redirect_url,time() + __COOKIE_TIME__ , '/');
}
?>
<p style="color:#000000;font-style:italic"><?=$msg_success?><br/><br/></p>
<?php }else if(!empty($msg_error)){?>
<p style="color:#000"><?=$msg_error?></p>
<?php }?>
			