<?php
/**
 * Customer Account Confirmation 
 */
ob_start();
session_start();
$szMetaTitle="Transporteca | Email Confirmation";
//$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
//require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "forwarders/AccountPage/";

$kForwardersContact = new cForwarderContact();

$szConfirmationKey=trim($_REQUEST['confirmationKey']);
?>
<div id="hsbody">
<?
if(!empty($_COOKIE[__COOKIE_NAME__]))
{
	$redirect_url=$_COOKIE[__COOKIE_NAME__];
}
else
{
	if((int)$_SESSION['forwarder_id']>0)
	{
		$redirect_url=__FORWARDER_DASHBOARD_URL__;
	}
	else 
	{
		$redirect_url=__FORWARDER_HOME_PAGE_URL__;
	}
}
if(!empty($szConfirmationKey))
{
	if($kForwardersContact->checkConfirmationKey($szConfirmationKey))
	{	
		$_SESSION['valid_confirmation']='1';
		header('Location:'.$redirect_url);
		exit();
	}
	else
	{
		$_SESSION['valid_confirmation']='2';
		
		header('Location:'.$redirect_url);
		exit();
	}
}
else
{
	$_SESSION['valid_confirmation']='2';
	
	header('Location:'.$redirect_url);
	exit();
}
?>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>