<?php
ob_start();
session_start();
//error_reporting (E_ALL);
ini_set('max_execution_time',1200);
ini_set('MEMORY_LIMIT','512M');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
 
$Excel_export_import=new cExport_Import();
$kConfig = new cConfig();
if((int)$_SESSION['forwarder_user_id']==0 || empty($_POST['dataExportArr']))
{
    header('Location:'.__BASE_URL__.'/forwarders/');
    exit();	
}
if(isset($_SESSION['forwarder_admin_id']) && $_SESSION['forwarder_admin_id']>0)
{
    //$kForwarderContact->load($_SESSION['forwarder_admin_id']);
    $kForwarder->load($_SESSION['forwarder_id']);
}
else
{
    $kForwarderContact->load($_SESSION['forwarder_user_id']);
    $kForwarder->load($kForwarderContact->idForwarder);
}
require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
  
$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_in_memory_serialized;
$cacheSettings = array( ' memoryCacheSize ' => '64MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
$objPHPExcel=new PHPExcel();
$szDisplayName=str_replace(" ","_",$kForwarder->szDisplayName);
$szDisplayName=str_replace("/","_",$szDisplayName);
$folderName=$szDisplayName."_".$kForwarderContact->idForwarder;
//$folderName = "Test_".$kForwarderContact->idForwarder;
$exportData="exportData";
if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName)) {
    mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName);
    chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName,0777);
    if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData)) {
        mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData);
        chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData,0777); 
    }
}
else
{
 if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData)) {
        mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData);
        chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData,0777);
    }
}
$fh = fopen(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData."/index.php", "w+");
 
$sheetindex=0;
if(!empty($_POST['dataExportArr']))
{
	$idOriginCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idOriginWarehouse']);
	if(!empty($idOriginCountryWarehouseArr))
	{
            for($i=0;$i<count($idOriginCountryWarehouseArr);++$i)
            {
                $idOriginCountryWarehouseArrary=explode("_",$idOriginCountryWarehouseArr[$i]);
                $idOriginWarehouseArr[]=$idOriginCountryWarehouseArrary[0];
            }
	}
	array_unique($idOriginWarehouseArr,SORT_NUMERIC);
	
	$idDesCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idDesWarehouse']);
	if(!empty($idDesCountryWarehouseArr))
	{
            for($i=0;$i<count($idDesCountryWarehouseArr);++$i)
            {
                $idDesCountryWarehouseArrary=explode("_",$idDesCountryWarehouseArr[$i]);
                $idDesWarehouseArr[]=$idDesCountryWarehouseArrary[0];
            }
	}
	
	//print_r($_POST)
	array_unique($idDesWarehouseArr,SORT_NUMERIC);
	$idCurrency=$_POST['dataExportArr']['idCurrency'];
	$inActiveService=$_POST['dataExportArr']['inActiveService'];
        $iWarehouseType = $_POST['dataExportArr']['iWarehouseType'];
	  
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $file_name= __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData."/Transporteca_Airfreight_Services_upload_sheet_".$folderName.".xlsx";
            $szDownloadFileName = "Transporteca_Airfreight_Services_upload_sheet_".date('Ymd').".xlsx";
        }
        else
        {
            $file_name= __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData."/Transporteca_LCL_Services_upload_sheet_".$folderName.".xlsx";
            $szDownloadFileName = "Transporteca_LCL_Services_upload_sheet_".date('Ymd').".xlsx";
        } 
	if($Excel_export_import->exportDataTest($objPHPExcel,$sheetindex,$file_name,$kConfig,$idDesWarehouseArr,$idOriginWarehouseArr,$kForwarderContact->idForwarder,$_POST['dataExportArr']['downloadType'],$idCurrency,$inActiveService,$iWarehouseType))
	{	
            $strdata=array();
            $strdata[0]=" \n\n *** Got data redirecting to Excel file...\n\n"; 
            file_log(true, $strdata, $filename);
            
            ob_clean();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //$f = fopen($file_name,'rb');
            //$fsize=filesize($file_name);
            header('Content-Disposition: attachment; filename='.$szDownloadFileName);
            ob_clean();
            flush();
            readfile($file_name);
            exit;	
	}
        else
        {
            $strdata=array();
            $strdata[0]=" \n\n *** Don't recieved any data ...\n\n"; 
            file_log(true, $strdata, $filename);
        }
}
?>