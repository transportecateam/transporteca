<?php
/**
 * Edit User Information
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "ForwardersCompany/company_information/";
checkAuthForwarder_ajax();

$forwarderCompanyAry = array();
$kForwardersContact = new cForwarderContact();
$kForwarder = new cForwarder();

if(!empty($_REQUEST['mode']))
{
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
	$idForwarder = sanitize_all_html_input(trim($_REQUEST['id']));
}
if($operation_mode=='EDIT_REGISTERED_COMPANY_DETAILS')
{
	$kForwarder->load($idForwarder);
	echo display_company_information_edit_form($t_base,$kForwarder);
}
else if(!empty($_REQUEST['updateRegistComapnyAry']))
{
	$idForwarder = sanitize_all_html_input(trim($_REQUEST['updateRegistComapnyAry']['idForwarder']));
	if(!empty($_REQUEST['updateRegistComapnyAry']))
	{
		
		if($kForwarder->save($_REQUEST['updateRegistComapnyAry']))
		{
			echo "SUCCESS";
			die;
		}
		if(!empty($_POST['updateRegistComapnyAry']['szPhoneUpdate']))
		{
			$_POST['updateRegistComapnyAry']['szPhone']=urldecode(base64_decode($_POST['updateRegistComapnyAry']['szPhoneUpdate']));
		}
	}
	if(!empty($kForwarder->arErrorMessages))
	{
		?>
		<div id="regError" class="errorBox">
			<div class="header"><?=t($t_base.'feilds/please_correct_the_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kForwarder->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
		</div>
		<?php
	}
	
	$kForwarder->load($idForwarder);
	
	echo display_company_information_edit_form($t_base,$kForwarder);
}
else if($operation_mode=='DISPLAY_REGISTERED_COMPANY_DETAILS')
{
	$kForwarder->load($idForwarder);
	echo display_registered_company_details($kForwarder,$t_base);
}
else if($operation_mode=='DISPLAY_FORWARWARDER_COMPANY_DETAILS')
{
	$kForwarder->load($idForwarder);
	echo display_forwarder_company_details($kForwarder,$t_base);
}
else if($operation_mode=='EDIT_FORWARWARDER_COMPANY_DETAILS')
{
	$kForwarder->load($idForwarder);
	echo edit_company_information($t_base,$kForwarder);
}
else if(!empty($_REQUEST['updateForwarderComapnyAry']))
{	
	if(!empty($_REQUEST['updateForwarderComapnyAry']))
	{
		if($kForwarder->updateForwarder($_REQUEST['updateForwarderComapnyAry'],$_FILES))
		{
			echo "SUCCESS";
			die;
		}
	}
	?>
	<?
	if(!empty($kForwarder->arErrorMessages))
	{
		?>
		 <div id="regError" class="errorBox">
			<div class="header"><?=t($t_base.'feilds/please_correct_the_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
		      foreach($kForwarder->arErrorMessages as $key=>$values)
		      {
			      ?>
			      <li><?=$values?></li>
			      <?php 
		      }
			?>
			</ul>
			</div>
		</div>
		<?
		die;
	}
	$idForwarder = (int)$_REQUEST['updateForwarderComapnyAry']['idForwarder'];
	$kForwarder->load($idForwarder);
	echo edit_company_information($t_base,$kForwarder);
}
else if($operation_mode=='DELETE_FORWARDER_LOGO')
{

	$kForwarder = new cForwarder();
	if($kForwarder->deleteForwarderLogo($idForwarder))
	{
		echo "SUCCESS";
		die;
	}	
	if(!empty($kForwarder->arErrorMessages))
	{
		?>
		<div id="regError" class="errorBox">
			<div class="header"><?=t($t_base.'feilds/please_correct_the_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kForwarder->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
		</div>
		<?
	}
}
?>