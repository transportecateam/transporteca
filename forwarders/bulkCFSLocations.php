<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$szMetaTitle="Transporteca | CFS Locations";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();
$t_base = "BulkUpload/";
$t_base_error="Error";
$idForwarderContact = $_SESSION['forwarder_user_id'];
$iWarehouseType = __WAREHOUSE_TYPE_CFS__;
if(isset($_SESSION['completed']))
{
    $content = $_SESSION['completed'];
    confirmationPOPup($content,$iWarehouseType);
}

if(isset($_POST))
{ 
    require_once( __APP_PATH__ ."/forwarders/ajax_bulkCfsUpload.php" );
     
    /*
    $mode = trim(sanitize_all_html_input($_REQUEST['mode']));
    if($mode=='UPLOAD_CFS_BULK_LISTING')
    {	
        $Excel_export_import=new cExport_Import();
        $staus = $Excel_export_import->uploadCFSLocationTemplate($idForwarder,$idForwarderContact,$_FILES['cfsBulkUpload'],$iWarehouseType);
        if($staus==false)
        {	
            if(!empty($Excel_export_import->arErrorMessages))
            {
                ?>
                <script type="text/javascript">
                    addPopupScrollClass('error-bg');
                </script>
                <div id="error-bg">
                    <div id="popup-bg"></div>
                    <div id="popup-container" style="padding-top: 12%">
                        <div class="popup">
                            <p class="close-icon" align="right">
                                <a onclick="close_popup_content();" href="javascript:void(0);">
                                    <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                                </a>
                            </p>  
                            <h4><b><?=t($t_base_error.'/invalid_file_format')?></b></h4><br />
                            <p><?=t($t_base_error.'/we_did_not_recg_cfs')?></p><br />
                            <P align="center"><a class="button1" onclick="close_popup_content();"><span><?=t($t_base_error.'/close')?></span></a></P>
                        </div>
                    </div>	
                </div>		
                <?php
            }
        }
        else
        {	
            if($staus[0])
            {
                $_SESSION['idBatch'] = $staus[1];
                $_SESSION['option']  = 'cfsUpload';
                header('location:/CFSLocationBulk2/');
            }
        }
    }
     * 
     */
}  
?>

<div id="hsbody-2">
    <div class="hsbody-2-left account-links">
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
    </div>
    <div class="hsbody-2-right"> 
        <p><?=t($t_base.'messages/this_function_cfs_bulk_upload')?> <a href="<?=__FORWARDER_CFS_LOCATIONS_URL__?>" style="font-size: 16px;"><?=t($t_base.'links/here')?></a>.</p><br /> 	
        <ol>
            <li><?=t($t_base.'messages/download_cfs_loc_one_by_one')?></li>
            <p style="padding-top: 10px;padding-bottom: 10px;text-align: center;"><a class="button1" onclick="download_cfs_file_format('DOWNLOAD_CFS_EXCEL')"><span><?=t($t_base.'fields/download')?></span></a></p><br />
            <li><?=t($t_base.'messages/update_file_with_cfs_locations')?>. <!--  <a href="<?='#' ?>"><?=t($t_base.'links/help_me_update_this')?></a>--></li>
            <p style="padding-top: 10px;"><?=t($t_base.'messages/latitude_and_longitude')?> <a href="<?=__FORWARDER_HOME_PAGE_URL__?>/googleMapAssist.php" target="google_map_target_1" onclick="toolLatLongPopup();" style="font-size: 16px;"> <?=t($t_base.'links/this_tool_can_assist')?></a>.</p><br /> <br />
            <li><?=t($t_base.'messages/when_you_have_completed')?></li>
            <div class="file">
                <form id="uploadCfsFile" name ="uploadCfsFile" method="post" enctype="multipart/form-data">
                    <input id="fileUpload" type="file" name="cfsBulkUpload">
                    <input type="hidden" name="mode" value="UPLOAD_CFS_BULK_LISTING">
                    <span class="button">No file selected - click browse to select file for upload</span>
                </form>
            </div>
            <p style="padding-top: 10px;padding-bottom: 10px;text-align: center;"><a class="button1" onclick="upload_cfs_file();"><span><?=t($t_base.'fields/upload')?></span></a></p>
        </ol> 
        <div id="popup_container_google_map" style="display:none;">
            <div id="popup-bg"></div>
            <div id="popup-container">
                <div class="popup" style="width:720px;margin-top:30px;">
                    <p class="close-icon" align="right">
                        <a href="javascript:void(0);" onclick="window.top.window.hide_google_map();">
                            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                        </a>
                    </p>
                    <iframe id="google_map_target_1" class="google_map_select" style="width:720px;"  scrolling="no" name="google_map_target_1" src="#" ></iframe>
                </div>
            </div>
        </div>			
    </div> 	
</div>
<?php include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" ); ?>				