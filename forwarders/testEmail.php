<?php
ob_start();
if(!isset($_SESSION))
{
    session_start();
}
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);
$szMetaTitle="Transporteca | Terms & Conditions for Forwarders";
$display_profile_not_completed_message = false;
if( !defined( "__APP_PATH__" ) )

define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$kSendEmail = new cSendEmail();
/*
* building block for send Transporteca E-mail
*/
$mailBasicDetailsAry = array();
$mailBasicDetailsAry['szEmailTo'] = "ajay@whiz-solutions.com";
$mailBasicDetailsAry['szEmailFrom'] = "contact@transporteca.com";
$mailBasicDetailsAry['szEmailCC'] = "";
$mailBasicDetailsAry['szEmailSubject'] = "Test email from new Live server";
$mailBasicDetailsAry['szEmailMessage'] = "This is test message for Ajay";
$mailBasicDetailsAry['szReplyTo'] = $reply_to;
$mailBasicDetailsAry['idUser'] = 1;
$mailBasicDetailsAry['szBookingRef'] = "";
$mailBasicDetailsAry['idBooking'] = "";
$mailBasicDetailsAry['szUserLevel'] = $flag; 
$mailBasicDetailsAry['bBookingLogs'] = $booking_logs;
$mailBasicDetailsAry['szFromUserName'] = "Transporteca"; 
$mailBasicDetailsAry['iBookingLanguage'] = 1;   
try
{
    $kSendEmail->sendEmail($mailBasicDetailsAry);
} catch (Exception $ex) {
    echo "Exception <br><br>";
    print_r($ex);
}
