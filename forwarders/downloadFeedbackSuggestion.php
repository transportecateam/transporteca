<?php
ob_start();
if(!isset($_SESSION)) 
{
  session_start();
}
$szMetaTitle="Transporteca | Forwarder Company";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__  . "/forwarder.class.php" );
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
$objPHPExcel=new PHPExcel();
$kForwarder= new  cForwarder();
if(isset($_POST['flag']))
{   $mode='';
	$flag=trim(sanitize_all_html_input($_POST['flag']));
	if(isset($_POST['mode']))
	$mode=trim(sanitize_All_html_input($_POST['mode']));
	if($flag=="DOWNLOAD_HISTORY_REVIEW")
	{
		$value=$kForwarder->downloadFeedbackHistory($_SESSION['forwarder_id']);
		{
			
			echo $value;
			exit();
		}
	}
	if($flag=='DOWNLOAD_SUGGESTION_REVIEW' && $mode!='')
	{ 
		$valueNum=$kForwarder->downloadFeedbackSuggestion($mode,$_SESSION['forwarder_id']);
		
		echo  $valueNum;
		exit();
	}
	if($flag=="DELETE" && $mode!='')
	{
	@unlink(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/forwarderDetails/$mode.xlsx");
	 ob_clean();
	echo true;
	}
}	
	