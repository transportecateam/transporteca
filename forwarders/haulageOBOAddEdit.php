<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca | OBO Haulage";
$display_profile_not_completed_message=true;
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$showmsgFlag=false;
$t_base = "BulkUpload/";
$t_base_error="Error";
$Excel_export_import=new cExport_Import();
$kConfig = new cConfig();
$currencyArr=$Excel_export_import->getFreightCurrency('',true);
$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);

$updateFlag=false;
if(!empty($_POST['haulageDataArr']) && $_REQUEST['flag']=='edit')
{
	if($Excel_export_import->saveHaulageData($_POST['haulageDataArr'],$_REQUEST['idHaulageData']))
	{
		$updateFlag=true;
	?>
		<script type="text/javascript">
		getOBOWarehouseHaulageData('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');
		clear_form_after_save_data('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');
		document.getElementById("text_change").innerHTML='<?=t($t_base."fields/get_data");?>';
		//$("#get_haulage_data").attr("onclick","getOBOWarehouseHaulageData(<?=$idForwarder?>,'<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');");		
		$("#get_haulage_data").unbind("click");
		$("#get_haulage_data").click(function(){getOBOWarehouseHaulageData(<?=$idForwarder?>,'<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');});
		</script>
	<?	
	}
}
$expiryDate=date('d/m/Y');
if($_REQUEST['flag']=='edit' && (int)$_REQUEST['idHaulageData']>0 && !$updateFlag)
{
	$flag=$_REQUEST['flag'];
	$idHaulageData=$_REQUEST['idHaulageData'];
	$haulagePricingDataArr=$Excel_export_import->haulagePricingData($idHaulageData);
	//print_r($haulagePricingDataArr);
	if($haulagePricingDataArr[0]['dtExpiry']!='000-00-00' && $haulagePricingDataArr[0]['dtExpiry']!='')
	{
		$expiryDate=date('d/m/Y',strtotime($haulagePricingDataArr[0]['dtExpiry']));
	}
}

if($_REQUEST['flag']=='add' && !empty($_POST['haulageDataArr']))
{
	if($Excel_export_import->addHaulageData($_POST['haulageDataArr']))
	{
		$updateFlag=true;
	?>
		<script type="text/javascript">
		getOBOWarehouseHaulageData('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');
		clear_form_after_save_data('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');
		document.getElementById("text_change").innerHTML='<?=t($t_base."fields/get_data");?>';
		//$("#get_haulage_data").attr("onclick","getOBOWarehouseHaulageData(<?=$idForwarder?>,'<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');");		
		$("#get_haulage_data").unbind("click");
		$("#get_haulage_data").click(function(){getOBOWarehouseHaulageData(<?=$idForwarder?>,'<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');});
		</script>
	<?	
	}
}
if($_REQUEST['flag']!='edit')
{
	$flag='add';
?>
<script type="text/javascript">
//$("#add_edit_button").attr("onclick","add_haulage_data()");
$("#add_edit_button").unbind("click");	
$("#add_edit_button").click(function(){add_haulage_data()});
</script>
<?
}
if(!empty($Excel_export_import->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($Excel_export_import->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>

<script type="text/javascript">
$("#clear_form").unbind("click");	
//$("#clear_form").attr("onclick","clear_form_data('addeditHaulageData');");
$("#clear_form").click(function(){clear_form_data('addeditHaulageData');});
$("#cancel_button").unbind("click");	
//$("#cancel_button").attr("onclick","cancel_form_data('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');");
$("#cancel_button").click(function(){cancel_form_data('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');});
</script>
<?
if($_REQUEST['flag']=='edit')
{?>
<script type="text/javascript">
//$("#add_edit_button").attr("onclick","save_haulage_data()");
$("#add_edit_button").unbind("click");	
$("#add_edit_button").click(function(){save_haulage_data()});
</script>
<?php }}
?>
<script type="text/javascript">
$().ready(function() {	
    $("#datepicker1").datepicker();
});
</script>
<div id="dang-cargo-pop" class="help-pop right">
</div>
<div id="add_edit_haulage_data">	
<h5><strong><?=t($t_base.'title/update_haulage_rate_line');?></strong></h5>
			
		<form name="addeditHaulageData"	id="addeditHaulageData" method="post" >		
			<div class="oh">
				<p class="fl-40 gap-text-field"><?=t($t_base.'fields/distance_up_to')?><a href="javascript:void(0);" class="help" id="customs-clearance-link" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/distance_tool_tip');?>','<?=t($t_base.'messages/distance_tool_tip_1');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-15"><span class="f-size-12"><?=t($t_base.'fields/km')?></span><br /><input type="text" size="8" name="haulageDataArr[upto]" id="upto" value="<?=$_POST['haulageDataArr']['upto']?$_POST['haulageDataArr']['upto'] :$haulagePricingDataArr[0]['iUpToKM'] ?>" /></p>
			</div>
			<div class="oh">
				<p class="fl-40 gap-text-field" style="display:inline"><?=t($t_base.'fields/haulage_rate')?><a href="javascript:void(0)" class="help" id="customs-clearance-link" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_rate_tool_tip');?>','<?=t($t_base.'messages/haulage_rate_tool_tip_1');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-15"><span class="f-size-12" style="display:inline"><?=t($t_base.'fields/per_wm')?></span><br /><input type="text" size="8" name="haulageDataArr[per_wm]" id="per_wm" value="<?=$_POST['haulageDataArr']['per_wm']?$_POST['haulageDataArr']['per_wm'] :$haulagePricingDataArr[0]['fRateWM_Km'] ?>"/></p>
				<p class="fl-15"><span class="f-size-12"><?=t($t_base.'fields/minimum')?></span><br /><input type="text" size="8" name="haulageDataArr[minimum]" id="minimum" value="<?=$_POST['haulageDataArr']['minimum']?$_POST['haulageDataArr']['minimum'] :$haulagePricingDataArr[0]['fMinRateWM_Km'] ?>"/></p>
				<p class="fl-15"><span class="f-size-12"><?=t($t_base.'fields/per_booking')?></span><br /><input type="text" size="8" name="haulageDataArr[per_booking]" id="per_booking" value="<?=$_POST['haulageDataArr']['per_booking']?$_POST['haulageDataArr']['per_booking'] :$haulagePricingDataArr[0]['fRate'] ?>"/></p>
				<p class="fl-15"><span class="f-size-12"><?=t($t_base.'fields/currency')?></span><br />
					<select size="1" name="haulageDataArr[idCurrency]" id="idCurrency">
						<option value=" "><?=t($t_base.'fields/select');?></option>
						<? if(!empty($currencyArr))
							{
								foreach($currencyArr as $currencyArrs)
								{?>
									<option value="<?=$currencyArrs['id']?>" <?=((($_POST['haulageDataArr']['idCurrency'])?$_POST['haulageDataArr']['idCurrency']:$haulagePricingDataArr[0]['szHaulageCurrency']) ==  $currencyArrs['id'] ) ? "selected":""?>><?=$currencyArrs['szCurrency']?></option>
								<?}
									
							}
						?>
					</select>
				</p>
			</div>
			<div class="oh">
				<p class="fl-40 gap-text-field"><?=t($t_base.'fields/expiry_date')?><a href="javascript:void(0)" class="help" id="customs-clearance-link" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/validity_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-15"><span class="f-size-12">&nbsp;</span><br><input type="text" size="8" name="haulageDataArr[datepicker1]" id="datepicker1" value="<?=$_POST['haulageDataArr']['datepicker1']?$_POST['haulageDataArr']['datepicker1']:$expiryDate?>" onblur="show_me(this.id,'dd/mm/yyyy')" onfocus="blank_me(this.id,'dd/mm/yyyy')"/></p>
			</div>
			<div class="oh">
				<p class="fl-40 gap-text-field"><?=t($t_base.'fields/cross_border')?><a href="javascript:void(0)" class="help" id="customs-clearance-link" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/cross_border_tool_tip');?>','<?=t($t_base.'messages/cross_border_tool_tip_1');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-15"><span class="f-size-12">&nbsp;</span><br>
					<?php
						if($haulagePricingDataArr[0]['iCrossBorder']=='0')
						{
							$iCrossBorder='no';
						}
						else if($haulagePricingDataArr[0]['iCrossBorder']=='1')
						{
							$iCrossBorder='yes';
						}
					?>	
					<select size="1" name="haulageDataArr[idCrossBorder]"  id="idCrossBorder" style="width:76px;">
						<option value=""><?=t($t_base.'fields/select');?></option>
						<option value="no" <?=((($_POST['haulageDataArr']['idCrossBorder'])?$_POST['haulageDataArr']['idCrossBorder']:$iCrossBorder) ==  'no' ) ? "selected":""?> ><?=t($t_base.'fields/no')?></option>
						<option value="yes" <?=((($_POST['haulageDataArr']['idCrossBorder'])?$_POST['haulageDataArr']['idCrossBorder']:$iCrossBorder) ==  'yes' ) ? "selected":""?>><?=t($t_base.'fields/yes')?></option>
					</select>
				</p>
				<p class="fl-30"><span class="f-size-12">&nbsp;</span><br><?=t($t_base.'fields/haulage_transit_time')?><a href="javascript:void(0)" class="help" id="customs-clearance-link" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/haulage_transit_time_tool_tip');?>','','dang-cargo-pop',event);" onmouseout="hide_tool_tip('dang-cargo-pop')">&nbsp;</a></p>
				<p class="fl-15">
					<span class="f-size-12">&nbsp;</span><br>
					<select size="1" name="haulageDataArr[idTransitTime]" id="idTransitTime">
						<option value=""><?=t($t_base.'fields/select');?></option>
						<? if(!empty($haulageTransitTimeArr))
							{
								foreach($haulageTransitTimeArr as $haulageTransitTimeArrs)
								{
										$iHours='';
										if($haulageTransitTimeArrs['iHours']>24)
										{
											$iHours="< ".($haulageTransitTimeArrs['iHours']/24)." days";
										}
										else
										{
											$iHours="< ".$haulageTransitTimeArrs['iHours']." hours";
										}
									?>
									<option value="<?=$haulageTransitTimeArrs['id']?>" <?=((($_POST['haulageDataArr']['idTransitTime'])?$_POST['haulageDataArr']['idTransitTime']:$haulagePricingDataArr[0]['idHaulageTransitTime']) ==  $haulageTransitTimeArrs['id'] ) ? "selected":""?>><?=$iHours?></option>
								<?}
									
							}
						?>
					</select>
				</p>
			</div>
			<br />
			<div class="oh">
				<p class="fl-40"><a href="javascript:void(0)" class="button2" id="cancel_button"><span><?=t($t_base.'fields/cancel')?></span></a> <a href="javascript:void(0)" class="button2" id="clear_form"><span><?=t($t_base.'fields/clear_data')?></span></a></p>
				<p class="fl-40" align="right" style="padding-top:5px"><?=t($t_base.'fields/click_to_update_changes')?></p>
				<p class="fl-20" align="right">
				<input type="hidden" name="idHaulageData" id="idHaulageData" value="<?=$idHaulageData?>">
				<input type="hidden" name="haulageDataArr[idCountry]" id="idCountry" value="<?=$_POST['haulageDataArr']['idCountry']?>">
				<input type="hidden" name="haulageDataArr[Direction]" id="Direction" value="<?=$_POST['haulageDataArr']['Direction']?>">
				<input type="hidden" name="haulageDataArr[City]" id="City" value="<?=$_POST['haulageDataArr']['City']?>">
				<input type="hidden" name="haulageDataArr[idWareHouse]" id="idWareHouse" value="<?=$_POST['haulageDataArr']['idWareHouse']?>">
				<input type="hidden" name="haulageDataArr[idForwarder]" id="idForwarder" value="<?=$_POST['haulageDataArr']['idForwarder']?$_POST['haulageDataArr']['idForwarder']:$idForwarder?>">
				<input type="hidden" name="flag" id="flag" value="<?=$flag?>">
				<a href="javascript:void(0)" class="button1" id="add_edit_button"><span><? if($flag=='edit') {echo t($t_base.'fields/save');}else {echo t($t_base.'fields/add');}?></span></a></p>
			</div>
		</form>
		</div>