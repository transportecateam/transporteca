<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
$kHaulagePricing = new cHaulagePricing();
$idHaulageModel = (int)$_REQUEST['idHaulagePricingModel'];

constantApiKey();

$verticesAry = array();
if((int)$idHaulageModel>0)
{
$verticesAry = $kHaulagePricing->getAllVerticesOfZone($idHaulageModel,true);
$count=count($verticesAry);
if(!empty($verticesAry))
{
	$szLongitude=0;
	$szLatitude=0;
	foreach($verticesAry as $verticesArrays)
	{
		$szLongitude=$szLongitude+$verticesArrays['szLongitude'];
		$szLatitude=$szLatitude+$verticesArrays['szLatitude'];
	}
}
$szLongitudeMid=($szLongitude/$count);;
$szLatitudeMid=($szLatitude/$count);
}
$kWHSSearch = new cWHSSearch();
if((int)$idHaulageModel==0)
{
$idWareHouse=$_REQUEST['idWareHouse'];
$kWHSSearch->load($idWareHouse);
$szLongitudeMid=$kWHSSearch->szLongitude;
$szLatitudeMid=$kWHSSearch->szLatitude;
?>
<script type="text/javascript">
window.parent.document.getElementById('szLatLongPipeline1').value ='';
</script>	
<?
}

$maxPoints=$kWHSSearch->getManageMentVariableByDescription('__MAX_NUMBER_POINT_FOR_ZONE__');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>		
<script src="<?php echo __GOOGLE_MAP_JSON_URL__;?>" type="text/javascript"></script>
<script type="text/javascript">
  var poly, map;
  var bounds = new google.maps.LatLngBounds;
  var markers = [];
  var path = new google.maps.MVCArray;
  var polygon_resizing = false;	
  var maxpoints="<?=$maxPoints?>";
  var marker_removed_position = '';
  function initialize() {
    //var uluru = new google.maps.LatLng(-25.344, 131.036);
	
    map = new google.maps.Map(document.getElementById("map_canvas"), {
      zoom: 4,
    	center:  new google.maps.LatLng('<?=$szLatitudeMid?>', '<?=$szLongitudeMid?>'),
		    disableDefaultUI: true,
		    zoomControl: true,
			scaleControl: true,
		    overviewMapControl: true,
		    scrollwheel: true,
		    disableDoubleClickZoom: true,
		    draggableCursor:'crosshair',
		    largemapcontrol3d:true, maptypecontrol:true, scalecontrol:true,
		    mapTypeId: google.maps.MapTypeId.ROADMAP,
		    zoomControlOptions: {
              style: google.maps.ZoomControlStyle.LARGE,
              position: google.maps.ControlPosition.LEFT_CENTER
          }
    });
    
     <?
        	if(!empty($verticesAry))
        	{
        		foreach($verticesAry as $verticesArys)
        		{
        			?>
        			var old_points = new google.maps.LatLng('<?=$verticesArys['szLatitude']?>','<?=$verticesArys['szLongitude']?>');
					addEditPoint(old_points); 
        			<?
        		}
        		?>
        		var bounds = new google.maps.LatLngBounds();
	         	<?
	        	if(!empty($verticesAry))
	        	{
	        		foreach($verticesAry as $verticesArys)
	        		{
	        			?>
	        			var randomPoint = new google.maps.LatLng('<?=$verticesArys['szLatitude']?>','<?=$verticesArys['szLongitude']?>');
	        			bounds.extend(randomPoint);
	        			<?
	        		}
	        	}
	        ?>
	        map.setCenter(bounds.getCenter(), map.getZoom(bounds)) ;
	        <?
        	}
        ?>
	var fillColor = (polygon_resizing) ? '#0101DF' : '#0101DF'; //Set Polygon Fill Color
    poly = new google.maps.Polygon({
	    strokeColor: '#0101DF',
	    strokeOpacity: 1,
	    strokeWeight: 2,
	    fillColor: fillColor,
	    fillOpacity: 0.2
    });
    poly.setMap(map);
    poly.setPaths(new google.maps.MVCArray([path]));

    google.maps.event.addListener(map, 'click', addPoint);
  }

  function addPoint(event) {
    path.insertAt(path.length, event.latLng);
	
    var marker = new google.maps.Marker({
      position: event.latLng,
      map: map,
      draggable: true,
      icon: '<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/red-map-box.jpg'
    });
    markers.push(marker);
    marker.setTitle("#" + path.length);

    google.maps.event.addListener(marker, 'dblclick', function() {
      marker.setMap(null);
      for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
      markers.splice(i, 1);
      path.removeAt(i);
      dragenddeletingMarker();
      }
    );

    google.maps.event.addListener(marker, 'dragend', function() {
    		
      for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
      path.setAt(i, marker.getPosition());
      dragenddeletingMarker();
      }
    );
    
    addLatLngArr(event.latLng)
  }
  
  
  function addEditPoint(point) {
    path.insertAt(path.length, point);
	
    var marker = new google.maps.Marker({
      position: point,
      map: map,
      draggable: true,
      icon: '<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/red-map-box.jpg'
    });
    markers.push(marker);
    marker.setTitle("#" + path.length);

    google.maps.event.addListener(marker, 'dblclick', function() {
      marker.setMap(null);
      for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
      markers.splice(i, 1);
      path.removeAt(i);
      dragenddeletingMarker();
      }
    );

    google.maps.event.addListener(marker, 'dragend', function() {
    		
      for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
      path.setAt(i, marker.getPosition());
      dragenddeletingMarker();
      }
    );
    
    addLatLngArr(point)
  }
  
  function addLatLngArr(point)
  {
  		var poib=point;
  		var input_value = document.getElementById("szLatLongPipeline").value;
	
		var input_ary = input_value.split("||");
		
		var ret_ary = new Array();
		var j=0;
		var i=0;
		for(i=0;j<maxpoints;i++)
		{
			if(j==marker_removed_position)
			{
				ret_ary[j] = poib;
				
				j = j+1;
				ret_ary[j] = input_ary[i];
			}
			else
			{
				ret_ary[j] = input_ary[i];
			}
			j= j+1;
		}
		input_value = ret_ary.join("||");
		
	    document.getElementById("szLatLongPipeline").value = input_value;
	    window.parent.document.getElementById('szLatLongPipeline1').value = input_value;
  }
  
  function dragenddeletingMarker()
  {
  
  	document.getElementById("szLatLongPipeline").value = '';
	window.parent.document.getElementById('szLatLongPipeline1').value = '';
  	if(markers.length>0)
  	{
  		for(var i=0;i<markers.length;++i )
  		{
  			addLatLngArr(markers[i].getPosition());
  		}
  	}
  }
</script>
</head>
<body onload="initialize()">
<div id="map_canvas" style="width:100%; height:360px"></div>
<input type="hidden" id="szLatLongPipeline" name="szLatLongPipeline" value="">

</body> 
</html>