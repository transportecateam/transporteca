<?php
/**
 * Forwarder My Company  Information
 */
 ob_start();
if(!isset($_SESSION))
{
session_start();
}
$szMetaTitle="Transporteca | Update Bank Details for Receiving Funds";
//$display_profile_not_completed_message = false;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );

$t_base = "ForwardersCompany/BankDetails/";
$kForwardersContact = new cForwarderContact();
$kForwarder = new cForwarder();
$kConfig = new cConfig();

$forwarderContactAry = array();
$idForwarder = $_SESSION['forwarder_id'];
$kForwarder->getForwarderBankDetails($idForwarder);

//$szForwarderBankCountryAry = $kConfig->getAllCountryInKeyValuePair($kForwarder->idBankCountry);
//$szForwarderBankCountry = $szForwarderBankCountryAry[$kForwarder->idBankCountry]['szCountryName'] ;
//test(33,2);
?>
<div id="hsbody-2">
<div id="forwarder_company_div" style="display:none;"></div>
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
   <?php require_once(__APP_PATH_LAYOUT__ ."/forwarder_company_nav.php"); ?> 
	 <div class="hsbody-2-right">
		<div id="forwarder_company">
			<h4><strong><?=t($t_base.'titles/registered_bank_details');?></strong>&nbsp;&nbsp;<span id="upper_edit_link"><a href="javascript:void(0);" onclick="edit_forwarder_bank_details('<?=$idForwarder?>')"><?=t($t_base.'feilds/edit');?></a></span></h4>	
			<div id="forwarder_bank_details">
				<?
					echo display_forwarder_bank_details($kForwarder,$t_base);
				?>
			</div>
		</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>