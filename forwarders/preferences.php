<?php
/**
 * Forwarder My Company  
 */
ob_start();
if(!isset($_SESSION))
 {
	session_start();
 }
$szMetaTitle="Transporteca | Setup Company Systems Preferences";
//$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();
 
$t_base = "ForwardersCompany/Preferences/";
$kForwarderContact = new cForwarderContact();
$kConfig = new cConfig();	
$forwarderContactAry = array();
$szBookingEmailAry = array();
$szPaymentEmailAry = array();
$szCustomerServiceEmailAry = array();
$forwarderControlPanelArr = array();
$allCurrencyArr = array();
$idForwarder = $_SESSION['forwarder_id'];
$allCurrencyArr=$kConfig->getBookingCurrency(false,false,true);
$forwarderContactAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder);
  
?>	
<style>
<!--
input {margin-bottom:3px;}
-->
</style>
<div id="hsbody-2">
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader"></div>				
        <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
    </div>
   <?php  require_once( __APP_PATH_LAYOUT__ ."/forwarder_company_nav.php" );  ?> 
    <div class="hsbody-2-right"> 
        <div id="forwarder_preference_container">
            <div class="clearfix">
                <span class="accordian_small"><?=t($t_base.'titles/quote_and_booking');?></span>
            </div> 
            <div id="forwarder_preference_list_container">
                <div id="forwarder_email_addresses">
                    <?php
                        echo display_forwarder_preferenced_emails($kForwarderContact);
                    ?>
                </div> 
            </div>
        </div>
        <div id="forwarder_company_div"> 
            <?php echo display_forwarder_setting_emails($forwarderContactAry); ?> 
<!--            <br><br>  
            <div id="forwarder_system_control">
                <?php
                    echo forwarder_system_control_emails();
                ?>
            </div>-->
        </div>  
    </div>
</div> 
<?php
    include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" ); 
?>