<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
ini_set('max_execution_time',1200);
ini_set('memory_limit', '-1');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$display_profile_not_completed_message=true;
$szMetaTitle='Transporteca | Pending Quotes';
  
if($_REQUEST['flag']=='courierBooking')
{
    $quote_key = $_REQUEST['idBooking'];
    if(!empty($quote_key))
    {
        $keyAry = explode('__',$quote_key);
        $szBookingKey = $keyAry[0];	
    }
}
else
{
    $quote_key = $_REQUEST['idQuotes'];
    if(!empty($quote_key))
    {
        $keyAry = explode('__',$quote_key);
        $szQuotationKey = $keyAry[0];	
    }
} 
unset($_SESSION['forwarder_booking_booking_key']);
if((int)($_SESSION['forwarder_user_id']<=0) && ((int)$szBookingKey>0))
{
    $_SESSION['forwarder_booking_booking_key'] = $szBookingKey;
} 

if((int)($_SESSION['forwarder_user_id']<=0) && ((int)$szQuotationKey>0))
{
    $_SESSION['forwarder_booking_quotation_key'] = $szQuotationKey;
} 
$iPendingTrayPage = 1;
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );

?>

<link href="<?=__BASE_STORE_CSS_URL__?>/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/swfobject.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.uploadify.v2.1.4.min.js"></script>
<style type="text/css">
    li { cursor: pointer; } 
</style>
<?php 
if(empty($_SESSION['szCustomerTimeZoneGlobal']))
{
    $ret_ary = array();
    $ret_ary = getCountryCodeByIPAddress();
    if(empty($ret_ary))
    {
        $ret_ary['szCountryCode'] = 'DK';
    } 
    $szCustomerTimeZone = get_time_zone($ret_ary['szCountryCode'],$ret_ary['szRegion']);
    $_SESSION['szCustomerTimeZoneGlobal'] = $szCustomerTimeZone ;
} 
$BookingFileName = __APP_PATH_ROOT__."/logs/forwarderIPAddress.log";
$strdata=array();
$strdata[0]=" \n\n *** Start Date  ".date("Y-m-d H:i:s")."\n\n"; 
$strdata[1]="szCustomerTimeZoneGlobal: ".$_SESSION['szCustomerTimeZoneGlobal']."\n" ;
file_log(true, $strdata, $BookingFileName);

$kConfig = new cConfig();
$t_base = "BulkUpload/";
$t_base_error="Error"; 
 
if((int)$_SESSION['forwarder_user_id']==0)
{
    header('Location:'.__BASE_URL__.'/');
    exit();	
}
$kBooking = new cBooking();
$dtRemoved=date('Y-m-d',strtotime('- 14 DAY'));
$kBooking->removeNotSubmittedQuoteFromForwarderPendingTray($dtRemoved,$_SESSION['forwarder_id']);

 $_REQUEST['mode']='SHOW_PENDING_QUOTE_DATA';
?>   
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="hsbody">
    <?php require_once( __APP_PATH__ . "/forwarders/ajax_pendingTray.php" );?>
</div>
 <div id="pdfDiv" style="display:none;">                   
<div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup pdf-popup">
            <p class="close-icon" align="right">
                <a href="javascript:void(0);" onclick="closePdfDiv();">
                <img src="<?php echo __BASE_STORE_IMAGE_URL__?>/close1.png" alt="close">
                </a>
            </p>
            <iframe id="showPdf" width="790px" height="500px;">

           </iframe>
        </div> 
    </div>
 </div>
<?php  
    include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>	