<?php
/**
 * View Booking Confirmation
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

checkAuthForwarder();
$kBooking = new cBooking();
$kConfig = new cConfig();
$kUser = new cUser();
$kWHSSearch = new cWHSSearch();
$idBooking = sanitize_all_html_input($_REQUEST['idBooking']);

$idForwarder =$_SESSION['forwarder_id'];
if((int)$idBooking>0)
{
	$idBooking=$_REQUEST['idBooking'];
	$flag=$_REQUEST['flag'];
        $iHideHandlingFeeTotal=false;
        $iBookingPageFlag=$_REQUEST['iBookingPageFlag'];
        if(strtolower(trim($iBookingPageFlag))=='booking')
        {
            $iHideHandlingFeeTotal=true;
        }
	$pdfhtmlflag=true;
        $flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
	if($flag=='pdf')
	{
		$pdfhtmlflag=false;
	}
	$kBooking = new cBooking();
	$kBooking->load($idBooking);
	if($kBooking->idForwarder == $idForwarder)
	{
            $downloadedByAry = $kBooking->getAcknowledgeDownloadedByUserDetails($idBooking,$idForwarder);
            
            if(empty($downloadedByAry))
            {
                $kBooking->sendAcknowlodgementEmail($idBooking,$idForwarder);
            } 
	}
        if($kBooking->iFinancialVersion==2 && $kBooking->iForwarderGPType==1)
        {
            $bookingConfirmationPdf=getForwarderBookingConfirmationPdfFile_v2($idBooking,$pdfhtmlflag,false,$iHideHandlingFeeTotal);
        }
        else
        {
            $bookingConfirmationPdf=getForwarderBookingConfirmationPdfFile($idBooking,$pdfhtmlflag,false,$iHideHandlingFeeTotal);
        }
	if($flag=='pdf')
	{
		download_booking_pdf_file($bookingConfirmationPdf);
        die;
	}
}
else
{
	header('Location:'.__BASE_URL__.'/');
	exit();
}
?>