<?php 
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" ); 

$output_dir =  __UPLOAD_FORWARDER_BULK_SERVICES_TEMP__ . '/';
// Validate the file type
$fileTypes = array('jpg','jpeg','gif','png','pdf','doc','docx','xlsx','pptx'); // File extensions
    

if(isset($_FILES["myfile"]))
{
    $ret = array(); 
    $error =$_FILES["myfile"]["error"];
    if($error=='')
    { 
    	if(!is_array($_FILES["myfile"]['name'])) //single file
    	{
            $RandomNum   = time();
            
            $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
            $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
         
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt       = str_replace('.','',$ImageExt);
            $ImageName      = preg_replace_callback("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt; 
              
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $NewImageName); 
            
            $ret[0]['original_name']= $_FILES['myfile']['name']; 
            $ret[0]['name']= $NewImageName; 
            $ret[0]['size']=$_FILES['myfile']['size']/1000;
            ob_end_clean(); 
            echo json_encode($ret);
    	}
    }
}
?>