<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
} 
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "ForwardersCompany/Preferences/"; 
$t_base_error="management/Error/";

$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$kConfig = new cConfig();
$kAdmin = new cAdmin(); 
$kForwarderContact = new cForwarderContact();

$idForwarder = $_SESSION['forwarder_id'];

if($mode=='DELETE_FORWARDER_PREFERENCES_EMAIL')
{
    $idForwarderPreferences = sanitize_all_html_input(trim($_REQUEST['role_id'])); 
    if($idForwarderPreferences>0)
    {
        $preferencesListAry = array();
        $preferencesListArys = array();
        
        $preferencesListAry = $kForwarderContact->getAllQuotesAndBookingEmails($idForwarder,$idForwarderPreferences);
        $preferencesListArys = $preferencesListAry[0] ;
        
        if(!empty($preferencesListArys))
        {
            echo "SUCCESS_POPUP||||";
            echo display_forwarder_preferences_delete_confirmation($preferencesListArys);
            die;
        }
    }
}
if($mode=='DELETE_FORWARDER_PREFERENCES_EMAIL_CONFIRM')
{
    $idForwarderPreferences = sanitize_all_html_input(trim($_REQUEST['role_id'])); 
    if($kForwarderContact->deletePreferences($idForwarderPreferences))
    {
        echo "SUCCESS||||"; 
        echo display_forwarder_preferenced_emails($kForwarderContact);
        die; 
    }
}
else if($mode=='DISPLAY_EDIT_PREFERENCES_FORM')
{
    $idForwarderPreferences = sanitize_all_html_input(trim($_REQUEST['role_id'])); 
    if($idForwarderPreferences>0)
    {
        $preferencesListAry = array();
        $preferencesListAry = $kForwarderContact->getAllQuotesAndBookingEmails($idForwarder,$idForwarderPreferences);
        
        if(!empty($preferencesListAry[0]))
        {
            echo "SUCCESS||||";
            echo display_preference_emails_form($kForwarderContact,$preferencesListAry[0]);
            die;
        }
    }  
} 
else if(!empty($_POST['privateCustomerAry']))
{ 
    $kForwarder = new cForwarder();
    if($kForwarder->updatePrivateCustomerPrices($_POST['privateCustomerAry']))
    {
        echo "SUCCESS||||";
        echo display_forwarder_private_customer_setting($kForwarder);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_forwarder_private_customer_setting($kForwarder);
        die;
    }
}
else if(!empty($_POST['addPreferencesAry']))
{
    $idRegion = sanitize_all_html_input(trim($_POST['addPreferencesAry']['id'])); 
    if($kForwarderContact->updateForwarderPreferencesByMode($_POST['addPreferencesAry']))
    { 
        $_POST['addPreferencesAry'] = array(); 
        echo "SUCCESS||||"; 
        echo display_forwarder_preferenced_emails($kForwarderContact);
        die; 
    } 
    else
    {
        echo "ERROR||||";
        echo display_preference_emails_form($kForwarderContact);
        die;
    }
}
else if($mode=='AUTO_LOAD_FORWADER_HEADER_NOTIFICATION')
{
    echo "SUCCESS||||"; 
    echo showForwarderDetails(false);
    die;
}
?>