<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
//require_once(__APP_PATH_LAYOUT__."/forwarder_header.php");
$t_base = "BulkUpload/";
constantApiKey();
$kConfig = new cConfig();
$kWhsSearch = new cWHSSearch();
$request_searched_ary=array();
$request_searched_ary = $_REQUEST['oboHaulageHiddenAry'] ;
//print_r($request_searched_ary);
//$_SESSION['oboHaulageHiddenAry'] = $_REQUEST['oboHaulageHiddenAry'];
$idWarehouse = $request_searched_ary['idWarehouse'] ;
if($idWarehouse>0)
{
    $idForwarder = $_SESSION['forwarder_id'];
    $idDirection =  $request_searched_ary['idDirection'] ;
    $iForwarderSectionFlag = (int)$request_searched_ary['iForwarderSectionFlag'];
    
    $kWhsSearch->load($idWarehouse);
    $szLocation = sanitize_all_html_input(trim($request_searched_ary['szLocation']));
    
    if(!empty($szLocation) && $szLocation!='Selected from map')
    {
        $locationGeocodeAry = array();
        $locationGeocodeAry = reverse_geocode($szLocation);
        $szLatitute = $locationGeocodeAry['szLat'];
        $szLongitude = $locationGeocodeAry['szLng'];
    }
    else
    {
        $szCountryName = $kConfig->getCountryName(sanitize_all_html_input(trim($request_searched_ary['szCountry'])));
        $szLatitute = sanitize_all_html_input(trim($request_searched_ary['szLatitude']));
        $szLongitude = sanitize_all_html_input(trim($request_searched_ary['szLongitude']));
    }
    
    $szWhsLatitute = sanitize_all_html_input(trim($request_searched_ary['szWhsLatitude']));
    $szWhsLongitude = sanitize_all_html_input(trim($request_searched_ary['szWhsLongitude']));

    if(empty($szLatitute) && empty($szLongitude))
    {
        $szLatitute = $szWhsLatitute;
        $szLongitude = $szWhsLongitude;
    }
    $szWhsName = $kWhsSearch->szWareHouseName ;

    if($idDirection==1)
    {		
        $header_text = "Select the pick-up location  ";
    }
    else
    {
        $header_text = "Select the delivery location ";
    } 
    
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
            <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
            <script src="<?php echo __GOOGLE_MAP_JSON_URL__;?>" type="text/javascript"></script>
	      <!--  <SCRIPT type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/mapctrl.js"></SCRIPT>-->
    <script type="text/javascript"> 
        
	// Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html 
	var map = null;
	var geocoder = null;
	var latsgn = 1;
	var lgsgn = 1;
	var zm = 0; 
	var marker = null;
	var posset = 0;
	var marker2 = null ;
	var markersArray = [];
	function map_initialize() 
	{ 
            var myLatLng = new google.maps.LatLng('<?=$szWhsLatitute?>', '<?=$szWhsLongitude?>');
            var myOptions = {
                zoom: 2,
                zoomControl: true,
                center: myLatLng,
                scaleControl: true,
                overviewMapControl: true,
                scrollwheel: true,
                disableDoubleClickZoom: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggableCursor:'crosshair',
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.LEFT_CENTER
                }
            };
            map = new google.maps.Map(document.getElementById("map"),myOptions);
			 
            geocoder = new google.maps.Geocoder();
			
            var baseIcon = new google.maps.MarkerImage();
            //baseIcon.iconSize=new GSize(32,32);
            baseIcon.shadowSize=new google.maps.Size(56,32);
            baseIcon.iconAnchor=new google.maps.Point(16,32);
            baseIcon.infoWindowAnchor=new google.maps.Point(16,0);
            var thisicon = new google.maps.MarkerImage(baseIcon, "<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/red-marker.png'?>", null, "http://itouchmap.com/i/msmarker.shadow.png");
            var point_marker = new google.maps.LatLng('<?=$szWhsLatitute?>', '<?=$szWhsLongitude?>'); 

            google.maps.event.addListener(map, 'click', function(event) 
            {
                posset = 1;
                clearOverlays();
                fc(event.latLng) ;
                //marker2.setPoint(event.latLng);
                computepos(event.latLng);
            });
            var properties = {
                position: point_marker,
                map: map,
                icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/red-marker.png'?>',
                visible: true 
            }; 
            var marker = new google.maps.Marker(properties); 
            //markersArray.push(marker); 
             
            google.maps.event.addListener(marker, "click", function() {
                var point = marker.getPosition();
                var oinfowindow = new google.maps.InfoWindow();
                oinfowindow.setContent('<?=$szWhsName?>');
                google.maps.event.addListener(
                      marker, 
                      'click'
                );
                oinfowindow.open(map,marker);
                computepos (point);
            });
						
            <?php if(!empty($szLatitute) && !empty($szLongitude)) { ?>
                var baseIcon = new google.maps.MarkerImage();
                //baseIcon.iconSize=new GSize(32,32);
                baseIcon.shadowSize=new google.maps.Size(56,32);
                baseIcon.iconAnchor=new google.maps.Point(16,32);
                baseIcon.infoWindowAnchor=new google.maps.Point(16,0);
                var thisicon = new google.maps.MarkerImage(baseIcon, "<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>", null, "http://itouchmap.com/i/msmarker.shadow.png");
                var markeroptions={icon:thisicon,draggable: true};
 
                map.setZoom(6);
                var properties1 = {
                    position: new google.maps.LatLng('<?=$szLatitute?>','<?=$szLongitude?>'),
                    map: map,
                    icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>',
                    visible: true,
                    draggable: true
                };
                var marker2 = new google.maps.Marker(properties1);
                 
                //map.addOverlay(marker2);
                markersArray.push(marker2);

                var blueinfowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker2,'dragstart',function(){ //Add drag start event
                    var point = marker2.getPosition();
                    computepos(point);
                    blueinfowindow.close();
                });
                google.maps.event.addListener(marker2,'drag',function()
                {
                    var point = marker2.getPosition();
                    computepos(point);
                }); //Add drag event
                google.maps.event.addListener(marker2,'dragend',function(){   //Add drag end event
                   var point = marker2.getPosition();
                    computepos(point);
                });
                 
                var html = "Latitude, Longitude<br>";
                google.maps.event.addListener(marker2, "click", function() {
                    var point = marker2.getPosition(); 
                    blueinfowindow.setContent(html+point.toUrlValue(6));
                    
                    google.maps.event.addListener(
                        marker2, 
                        'click'
                    );
                    blueinfowindow.open(map,marker2);
                    computepos(point);
                }); 		
                map.setCenter(new google.maps.LatLng('<?=$szLatitute?>','<?=$szLongitude?>'));
	<?php } else { ?>
                map.setCenter(new google.maps.LatLng('<?=$szWhsLatitute?>','<?=$szWhsLongitude?>'));
        <?php } ?>  
	}
	
	function computepos(point)
	{
            var newPoints=point.toUrlValue(6);
            var myarr = newPoints.split(",");
            var latA = Math.abs(Math.round(value=myarr[1] * 1000000.));
            var lonA = Math.abs(Math.round(value=myarr[0] * 1000000.));
		
            if(value=point.y < 0)
            {
                var ls = '-' + Math.floor((latA / 1000000));
            }
            else
            {
                var ls = Math.floor((latA / 1000000));
            }
		
            var lm = Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60);
            var ld = ( Math.floor(((((latA/1000000) - Math.floor(latA/1000000)) * 60) - Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60)) * 100000) *60/100000 );

            if(value=point.x < 0)
            {
                var lgs = '-' + Math.floor((lonA / 1000000));
            }
            else
            {
                var lgs = Math.floor((lonA / 1000000));
            }
		
            var lgm = Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60);
            var lgd = ( Math.floor(((((lonA/1000000) - Math.floor(lonA/1000000)) * 60) - Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60)) * 100000) *60/100000 );
		
            document.getElementById("latbox").value=myarr[0];
            document.getElementById("lonbox").value=myarr[1];
	}
	
	function showAddress() 
        {
            var address = document.getElementById("address").value;
            if (geocoder) 
            {
                geocoder.getLatLng(
                    address,
                    function(point) {
                        if (!point) {
                            alert(address + " not found");
                        } else { 
                            posset = 1; 
                           // map.setMapType(G_HYBRID_MAP);
                            map.setMapType(G_NORMAL_MAP);
                            map.setCenter(point,8);
                            zm = 1;
                            marker.setPoint(point);
                            GEvent.trigger(marker, "click");
                        }
                    }
                );
            }
	}
	
	function showLatLong(latitude, longitude) 
	{
            if (isNaN(latitude)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
            if (isNaN(longitude)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}

            latitude1 = Math.abs( Math.round(latitude * 1000000.));
            if(latitude1 > (90 * 1000000)) 
            { 
                alert(' Latitude must be between -90 to 90. ');  
                document.getElementById("latbox1").value=''; 
                return;
            }
            longitude1 = Math.abs( Math.round(longitude * 1000000.));
            if(longitude1 > (180 * 1000000)) { alert(' Longitude must be between -180 to 180. ');  document.getElementById("lonbox1").value='';  return;}

            var point = new GLatLng(latitude,longitude);

            posset = 1;

            if (zm == 0)
            {
                //map.setMapType(G_HYBRID_MAP);
                map.setMapType(G_NORMAL_MAP);
                map.setCenter(point,16);
                zm = 1;
            }
            else
            {
                map.setCenter(point);
            }

            var html = "";
            html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);

            var baseIcon = new GIcon();
            //baseIcon.iconSize=new GSize(32,32);
            baseIcon.shadowSize=new GSize(56,32);
            baseIcon.iconAnchor=new GPoint(16,32);
            baseIcon.infoWindowAnchor=new GPoint(16,0);
            var thisicon = new GIcon(baseIcon, "<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>", null, "http://itouchmap.com/i/msmarker.shadow.png");

            var marker = new GMarker(point,thisicon);
            GEvent.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
            map.addOverlay(marker);

            GEvent.trigger(marker, "click");
	}
	
	function showLatLong1(latitude, latitudem,latitudes, longitude,  longitudem,  longitudes) 
	{
            if (isNaN(latitude)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
            if (isNaN(latitudem)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
            if (isNaN(latitudes)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
            if (isNaN(longitude)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
            if (isNaN(longitudem)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
            if (isNaN(longitudes)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}

            if(latitude < 0)  { latsgn = -1; }
            alat = Math.abs( Math.round(latitude * 1000000.));
            if(alat > (90 * 1000000)) 
            { 
                    alert(' Degrees Latitude must be between -90 to 90. ');  
                    return; 
             }
            latitudem = Math.abs(Math.round(latitudem * 1000000.)/1000000);
            absmlat = Math.abs(Math.round(latitudem * 1000000.));  //integer
            if(absmlat >= (60 * 1000000)) 
            {  
                    alert(' Minutes Latitude must be between 0 to 59. ');
                    return;
            }
            latitudes = Math.abs(Math.round(latitudes * 1000000.)/1000000);
            absslat = Math.abs(Math.round(latitudes * 1000000.));
            if(absslat > (59.99999999 * 1000000)) 
            {  
                    alert(' Seconds Latitude must be between 0 and 59.99. '); 
                    document.getElementById("latbox1ms").value=''; 
                    return; 
            }

            if(longitude < 0)  
            {
             lgsgn = -1; 
            }
            alon = Math.abs( Math.round(longitude * 1000000.));
            if(alon > (180 * 1000000)) 
            {
              alert(' Degrees Longitude must be between -180 to 180. ');
              return;
            }
            longitudem = Math.abs(Math.round(longitudem * 1000000.)/1000000);
            absmlon = Math.abs(Math.round(longitudem * 1000000));
            if(absmlon >= (60 * 1000000))   
            {  
                    alert(' Minutes Longitude must be between 0 to 59. '); 
                    return;
            }
            longitudes = Math.abs(Math.round(longitudes * 1000000.)/1000000);
            absslon = Math.abs(Math.round(longitudes * 1000000.));
            if(absslon > (59.99999999 * 1000000)) 
            {  
                    alert(' Seconds Longitude must be between 0 and 59.99. '); 
                    return;
            }

            latitude = Math.round(alat + (absmlat/60.) + (absslat/3600.) ) * latsgn/1000000;
            longitude = Math.round(alon + (absmlon/60) + (absslon/3600) ) * lgsgn/1000000;

            var point = new GLatLng(latitude,longitude);
            posset = 1;

            if (zm == 0)
            {
                    //map.setMapType(G_HYBRID_MAP);
                    map.setMapType(G_NORMAL_MAP);
                    map.setCenter(point,16);
                    zm = 1;
            }
            else
            {
                    map.setCenter(point);
            }
             var html = "";
             html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);

             var baseIcon = new GIcon();
            // baseIcon.iconSize=new GSize(32,32);
             baseIcon.shadowSize=new GSize(56,32);
             baseIcon.iconAnchor=new GPoint(16,32);
             baseIcon.infoWindowAnchor=new GPoint(16,0);
             var thisicon = new GIcon(baseIcon, "<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>", null, "http://itouchmap.com/i/msmarker.shadow.png");

             var marker = new GMarker(point,thisicon);
             GEvent.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
             map.addOverlay(marker);

             GEvent.trigger(marker, "click");
	}
	
	function streetview()
	{
            if (posset == 0)
            {
                alert("Position Not Set.  Please click on the spot on the map to set the street view point.");
                return;
            }

            var point = map.getCenter();
            var t1 = String(point);
            t1 = t1.replace(/[() ]+/g,"");
            var str = "http://www.streetviews.co?e=" + t1;
            var popup = window.open(str, "streetview");
            popup.focus();
	}
	
	function googleearth()
	{
            if (posset == 0)
            {
                alert("Position Not Set.  Please click on the spot on the map to set the Google Earth map point.");
                return;
            }
            var point = map.getCenter();
            var gearth_str = "http://gmap3d.com?r=3dmap&mt=Latitude-Longitude Point&ml=" + point.y+ "&mg=" + point.x;
            var popup = window.open(gearth_str, "googleearth");
            popup.focus();
	}
	
	function pictures()
	{
            if (posset == 0)
            {
                alert("Position Not Set.  Please click on the spot on the map to set the photograph point.");
                return;
            }
            var point = map.getCenter();
            var pictures_str = "http://ipicture.mobi?r=pictures&mt=Latitude-Longitude Point&ml=" + point.y+ "&mg=" + point.x;
            var popup = window.open(pictures_str, "pictures");
            popup.focus();
	}
	
	function lotsize()
	{
            if (posset == 0)
            {
                alert("Position Not Set.  Please click on the spot on the map to set the lot size map point.");
                return;
            }
            var point = map.getCenter();
            var t1 = String(point);
            t1 = t1.replace(/[() ]+/g,"");
            var vpike_str = "http://viewofhouse.com?e=" + t1 + "::findlotsize:";
            var popup = window.open(vpike_str, "lotsize");
            popup.focus();
	}
	
	function getaddress()
	{
            if (posset == 0)
            {
                alert("Position Not Set.  Please click on the spot on the map to set the get address map point.");
                return;
            }
            var point = map.getCenter();
            var t1 = String(point);
            t1 = t1.replace(/[() ]+/g,"");
            var getaddr_str = "http://www.getaddress.net?latlng=" + t1;
            var popup = window.open(getaddr_str, "getaddress");
            popup.focus();
	}
	
	function removeMarker() 
	{
            if (markersArray) 
            {
                for (i in markersArray) {
                    map.removeOverlay(markersArray[i]);
                }
            }
	}
	
	function fc( point )
	{
            var html = "";
            html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
            var image_path = '<?=__MAIN_SITE_HOME_PAGE_URL__."/images/blue-marker.png"?>';
		 
		 
            var html = "";
            html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
           //alert(html);
            var baseIcon = new google.maps.MarkerImage();
            baseIcon.iconSize=new google.maps.Size(32,32);
            baseIcon.shadowSize=new google.maps.Size(56,32);
            baseIcon.iconAnchor=new google.maps.Point(16,32);
            baseIcon.infoWindowAnchor=new google.maps.Point(16,0);
            var thisicon = new google.maps.MarkerImage(baseIcon, '<?=__BASE_STORE_IMAGE_URL__.'/blue-marker.png'?>', null, "http://itouchmap.com/i/msmarker.shadow.png");

           // var marker = new GMarker(point,{draggable: true});
            var properties = {
                position: point,
                map: map,
                icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>',
                        visible: true,
                draggable: true
             };
	
            var marker = new google.maps.Marker(properties);
            markersArray.push(marker);
            //map.setCenter(point); 
            //map.setZoom(6);

            google.maps.event.addListener(marker, "click", function() { 
		var oinfowindow = new google.maps.InfoWindow();
	  	oinfowindow.setContent(html);
	  	oinfowindow.open(map,marker);
            });
	 
            google.maps.event.addListener(marker, 'dragstart', function(overlay,point)  { //Add drag start event
                computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'drag', function(overlay,point)  { //Add drag event 
                computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'dragend', function(overlay,point)  { //Add drag end event
                computepos(marker.getPosition());
	    });		 
	}
	
	function createMarker(point, html) 
	{
		 reset();
		 var marker = new GMarker(point);
		 GEvent.addListener(marker, "click", function()
		 {
		 	marker.openInfoWindowHtml(html);
		 });
		 return marker;
	}
	
	function reset() 
	{
		map.clearOverlays();
		
		document.getElementById("latbox").value='';
		document.getElementById("lonbox").value='';
		
		marker = new GMarker(new GLatLng('<?=$szLatitute?>','<?=$szLongitude?>'), {draggable: true});
		map.addOverlay(marker);
		marker.setPoint(map.getCenter());
		
		GEvent.addListener(marker, "dragend", function() {
		var point = marker.getLatLng();
		posset = 0;
		
		if (zm == 0)
		{map.setCenter(point,7); zm = 1;}
		else
		{map.setCenter(point);}
		computepos(point);
		});
		
		GEvent.addListener(marker, "click", function() {
		var point = marker.getLatLng();
		marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
		computepos (point);
		});
	}
	
	function reset1() 
	{
            marker.setPoint(map.getCenter());
	}
	
	function clearOverlays() 
        {
            //console.log(markersArray.length);	
            if (markersArray) 
            {
                for (var i = 0; i < markersArray.length; i++) 
                { 
                    //console.log((markersArray[i]); 
                    markersArray[i].setMap(null); 
                }  
            }
        }
	
    function sendValue()
    {
        var latitute = document.getElementById("latbox").value;
        var logitude = document.getElementById("lonbox").value;
            //window.parent.document.getElementById('szLatitude').value = latitute;
	    //window.parent.document.getElementById('szLongitude').value = logitude;
	    //alert("I m in googlemap.php   latitute "+latitute+" longitute "+logitude);
	  
        window.top.window.insert_value_google_map_haulage(latitute,logitude,'<?php echo $iForwarderSectionFlag; ?>');    
    }
	
	function show_address_on_submit(kEvent)
	{
		var temp=(window.event)?kEvent.keyCode:kEvent.which;
		if(temp == 13)
		{
			showAddress();
		}
	}
	/*
	function hide_google_map()
	{
		window.parent.document.getElementsByTagName('body').class='';	
		window.parent.document.getElementsByTagName('body').style='' ;	
		window.parent.document.getElementById('popup_container_google_map').style.display = 'none';
		//parent.document.getElementById('mainClass').style.overflow = 'none';
		
		$("#mainClass",window.parent.document).removeClass("popupscroll");
		$('#mainClass',window.parent.document).removeAttr('style');
		$('.mainpage',window.parent.document).removeAttr('id');
		
	//	$('#mainClass',parent.document).removeId('mainClass');
		//$("parent.document.body").removeClass("popupscroll");
		//$("parent.document.body").attr('style',"");
	}
	*/
	</script>
	  </head>
	
	  <body onload="map_initialize()">
	  <p align="left"><strong><?=$header_text?></strong></p>
	  <div >	  
	   <div id="map" align="center" style="width: 698px; height: 480px;margin-top:5px;border:solid 1px #bfbfbf;"></div>
		<div class="map_note" align="left">
			<p style="margin-top:-15px;"><?=t($t_base.'title/note')?>: <?=t($t_base.'title/note_msg_google_map')?></p>
		</div>
		<p align="center">
			<input type="hidden" id="latbox" name="szLatitude" value="<?=$szLatitute?>"></p>
			<input type="hidden" id="lonbox" name="szLongitude" value="<?=$szLongitude?>">
			<a href="javascript:void(0);" onclick="window.top.window.hide_google_map()" class="button2"><span><?=t($t_base.'fields/close');?></span></a>
			<a href="javascript:void(0);" onclick="sendValue();" class="button1"><span><?=t($t_base.'fields/select');?></span></a>	    	   	  
		</p> 
		</div>
	  </body>
	</html>
<?
}
else
{
	?>
	<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="utf-8" />
	        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
			<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
			<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
		</head>
		 <body>
		  <div >
		  	 <div class="oh">
		  	 	<h3>No CFS selected. please select a CFS and then click on [select on map] then only you can select location on map.</h3>
		  	 </div>
		 	<p align="center">
				<a href="javascript:void(0);" onclick="window.top.window.hide_google_map()" class="button2"><span><?=t($t_base.'fields/close');?></span></a>
			</p> 
		</div>
		 </body>
	</html>
	<?
}
?>