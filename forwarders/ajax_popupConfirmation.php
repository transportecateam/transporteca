<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$szMetaTitle="Transporteca | CFS Locations";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$kForwarder  = new cForwarder();
$t_base_error = "management/Error/";

if(isset($_POST['updateRegistComapnyAry']))
{
	if(isset($_POST['updateForwarderComapnyAry']['szForwarderLogo']))
	{
		$_REQUEST['updateRegistComapnyAry']['szForwarderLogo'] = $_POST['updateForwarderComapnyAry']['szForwarderLogo'];
	}
	if($kForwarder->updateForwarderCompanyInformation($_REQUEST['updateRegistComapnyAry'],$_FILES))
	{
		echo "SUCCESS|||||";
		require_once(__APP_PATH__ ."/forwarders/popupPreferences.php");
		die;
	}
	else
	{
		echo "ERROR|||||";
		if(!empty($kForwarder->arErrorMessages))
		{
		?>
			<div id="regError" class="errorBox" style="display:block;">
			<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kForwarder->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
		<?php
		}
	}
}

if( ( isset($_POST['updatePrefEmailAry']) || isset($_POST['updatePrefControlArr']) ) && !empty($idForwarder))
{ 		
	$idOldCurrency = (int)sanitize_all_html_input(trim($_POST['updatePrefControlArr']['szCurrency_hidden']));
	$idNewCurrency = (int)sanitize_all_html_input(trim($_POST['updatePrefControlArr']['szCurrency']));
	
		$data = array();
		$data = array_merge($_POST['updatePrefControlArr'],$_POST['updatePrefEmailAry']);
		//$data = $_POST['updatePrefControlArr'];
		//$data = $_POST['updatePrefEmailAry'];
		//echo $idForwarder;
		$updateController=$kForwarderContact->updateForwarderPreferences($data,$idForwarder);
		if($updateController)
		{	echo "SUCCESS|||||";
			require_once(__APP_PATH__ ."/forwarders/popVideoSeeHow.php");
		}
	else
	{
		echo "ERROR|||||";
		if(!empty($kForwarderContact->arErrorMessages))
		{
		?>
			<div id="regError" class="errorBox" style="display:block;">
			<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kForwarderContact->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
		<?php
		}
	}
	//$updateMailer=$kForwarderContact->updateMailingAddress($_POST['updatePrefEmailAry'],$idForwarder);
	
}
if(isset($_POST['mode']))
{
	if($_POST['mode']=='REMOVE_POP_UP')
	{
		$kForwarder->updatePopupdetails($idForwarder);
		unset($_SESSION['popUpForm']);
	}

	if($_POST['mode']=='SHOW_CHECK_MESSAGE')
	{
		$t_base = 'management/dashboard/';
		?>
				<div id="popup-bg"></div>
				<div id="popup-container">
				<div class="popup" style="margin-top:100px;">
				<p class="close-icon" align="right">
				<a id="checkForwarderListing"  href="javascript:void(0);">
				<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
				</a>
				</p>
				<h5><b><?=t($t_base."title/you_can_complete")?></b></h5>
				<p><?=t($t_base."title/p1")?></p>
				<br/>
				<p><?=t($t_base."title/p2")?></p>
				<br/>
				<p style="text-align: center;">
				<a class="button1" style="opacity: 1;" href="javascript:void(0);" onclick="removePopUpIncompleteFormFill();">
					<span><?=t($t_base."title/close")?></span>
				</a></p>
		<?php
	}
	if($_POST['mode']=='UNLINK_FILE')
	{
		$file = $_POST['fileName'];
		@unlink(__APP_PATH_FORWARDER_IMAGES__POP_UP_TEMP__."/".$file);
	}
	if($_POST['mode']=='CHANGE_STATUS')
	{
		$_SESSION['popUpForm'] = (int)$_POST['value']+1;
	}
	if($_POST['mode']=='GO_PREVIOUS_PAGE')
	{
		$_SESSION['popUpForm'] = (int)$_POST['value'];
		$value = (int)$_POST['value'];
		if($value == 1 )
		{
			require_once(__APP_PATH__ ."/forwarders/forwardercompanyPopup.php");
		}
		if($value == 2 )
		{
			require_once(__APP_PATH__ ."/forwarders/popupPreferences.php");
		}
	}

}
