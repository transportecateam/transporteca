<?php 
/**
  *forwarder myProfile 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Privacy Notice for Forwarders";
$display_profile_not_completed_message = false;
	if( !defined("__APP_PATH__") )
		define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
	require_once (__APP_PATH__ ."/inc/constants.php");
	require_once(__APP_PATH_LAYOUT__ ."/forwarder_header.php");
	$KwarehouseSearch=new cWHSSearch();
	checkAuthForwarder();
$t_base="Privacy/";
$privacyPolicy = html_entity_decode($KwarehouseSearch->getManageMentVariableByDescription('__FORWARDER_PRIVACY_POLICY__'));
$privacyPolicy = refineDescriptionData($privacyPolicy);
?>

<div id="hsbody-2">
	<div class="hsbody-2-left">
		<h5><strong><?=t($t_base.'title/privacy_notice');?></strong></h5>
	</div>
	<div class="hsbody-2-right link16">	
		<?=$privacyPolicy;?>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>		