<?php
/**
 * Edit User Information
 */
  ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "OBODataExport/";

checkAuthForwarder_ajax();
$kExport_Import=new cExport_Import();
//print_r($_REQUEST);
if(!empty($_REQUEST['mode']))
{
    $operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
    $idForwarder = sanitize_all_html_input(trim($_REQUEST['idForwarder']));
    $idCountry = sanitize_all_html_input(trim($_REQUEST['idCountry']));
    $szCity = sanitize_all_html_input(trim($_REQUEST['szCity']));
    $iWarehouseType = sanitize_all_html_input(trim($_REQUEST['iWarehouseType']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['szFromPage']));
} 
if($operation_mode=='ORIGIN_WHS_COUNTRY')
{
    $forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,false,false,$iWarehouseType);
    ?>
    <select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse" <?php if($szFromPage=='OBO_CUSTOM_CLEARANCE'){ ?> onclick="show_destination_country_dropdown(this.value,'<?php echo $szFromPage; ?>'); <?php } ?>>
        <option value=""><?=t($t_base.'title/cfs_name');?></option>
        <?php
            if(!empty($forwardWareHouseArr))
            {
                foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                {
                    ?>
                    <option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                    <?php
                }
            }
        ?>
    </select>
<?php
echo "||||";
    $forwardWareHouseCityAry = $kExport_Import->getForwaderWarehousesCity($idForwarder,$idCountry,$iWarehouseType);
?>
    <select id="szOriginCity" name="oboDataExportAry[szOriginCity]" onchange="show_forwarder_warehouse_by_city('<?=$idForwarder?>',this.value,'origin_warehouse_span','ORIGIN_WHS_CITY','<?php echo $iWarehouseType; ?>')">
	<option value=""><?=t($t_base.'title/city');?></option>
	<?php
            if(!empty($forwardWareHouseCityAry))
            {
                foreach($forwardWareHouseCityAry as $forwardWareHouseCityArys)
                {
                    ?>
                    <option value="<?=$forwardWareHouseCityArys['szCity']?>"><?=$forwardWareHouseCityArys['szCity']?></option>
                    <?php
                }
            }
	?>
</select>
<?php
}
else if($operation_mode=='ORIGIN_WHS_CITY')
{
	$forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,$szCity,false,$iWarehouseType);
	?>
	<select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse">
		<option value=""><?=t($t_base.'title/cfs_name');?></option>
		<?php
			if(!empty($forwardWareHouseArr))
			{
				foreach($forwardWareHouseArr as $forwardWareHouseArrs)
				{
					?>
					<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
					<?
				}
			}
		?>
	</select>
<?php	
}
if($operation_mode=='DESTINATION_WHS_COUNTRY')
{
	$forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,false,false,$iWarehouseType);
	?>
	<select name="oboDataExportAry[idDestinationWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');" id="idDestinationWarehouse">
		<option value=""><?=t($t_base.'title/cfs_name');?></option>	
		<?php
			if(!empty($forwardWareHouseArr))
			{
				foreach($forwardWareHouseArr as $forwardWareHouseArrs)
				{
					?>
					<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
					<?php
				}
			}
		?>
	</select>
<?php
echo "||||";
$forwardWareHouseCityAry = $kExport_Import->getForwaderWarehousesCity($idForwarder,$idCountry,$iWarehouseType);
?>
<select id="szDestinationCity" name="oboDataExportAry[szDestinationCity]" onchange="show_forwarder_warehouse_by_city('<?=$idForwarder?>',this.value,'destination_warehouse_span','DESTINATION_WHS_CITY','<?php echo $iWarehouseType; ?>')">
	<option value=""><?=t($t_base.'title/city');?></option>
	<?php
		if(!empty($forwardWareHouseCityAry))
		{
			foreach($forwardWareHouseCityAry as $forwardWareHouseCityArys)
			{
				?>
				<option value="<?=$forwardWareHouseCityArys['szCity']?>"><?=$forwardWareHouseCityArys['szCity']?></option>
				<?php
			}
		}
	?>
</select>
<?php
}
elseif($operation_mode=='DESTINATION_WHS_CITY')
{
	$forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,$szCity,false,$iWarehouseType);
	?>
	<select name="oboDataExportAry[idDestinationWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');" id="idDestinationWarehouse">
		<option value=""><?=t($t_base.'title/cfs_name');?></option>
		<?php
			if(!empty($forwardWareHouseArr))
			{
				foreach($forwardWareHouseArr as $forwardWareHouseArrs)
				{
					?>
					<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
					<?php
				}
			}
		?>
	</select>
<?php	
}
else if($operation_mode=='CANCEL_OBO_LCL')
{
    $idPricingWTW = sanitize_all_html_input(trim($_REQUEST['id']));
    if($idPricingWTW<=0)
    {
        $idOriginWarehouse = sanitize_all_html_input(trim($_REQUEST['idOriginWarehouse']));
        $idDestinationWarehouse = sanitize_all_html_input(trim($_REQUEST['idDestinationWarehouse']));		

        $idPricingWTW = $kExport_Import->getWTWIdByCfs($idOriginWarehouse,$idDestinationWarehouse);
    }
    $searchedDataAry = $kExport_Import->getDataForLCL('',$idPricingWTW);
    $requestAry = array();
    if(empty($searchedDataAry))
    {
        $idOriginWarehouse = sanitize_all_html_input(trim($_REQUEST['idOriginWarehouse']));
        $idDestinationWarehouse = sanitize_all_html_input(trim($_REQUEST['idDestinationWarehouse']));		
        $requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
        $requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
    }
    else
    {
        $idOriginWarehouse = $searchedDataAry[0]['idWarehouseFrom'];
        $idDestinationWarehouse = $searchedDataAry[0]['idWarehouseTo'];
    } 
    $formData['idOriginWarehouse'] = $idOriginWarehouse ;
    $formData['idDestinationWarehouse'] = $idDestinationWarehouse ;

    echo display_obo_lcl_bottom_html($t_base,false,$pricingWtwAry,$formData);
    echo "||||";
    echo display_multiple_lcl_services($searchedDataAry,$t_base,$requestAry);
}
else if($operation_mode=='CHANGE_OBO_LCL')
{ 
    $iWarehouseType = sanitize_all_html_input(trim($_REQUEST['iWarehouseType'])); 
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        $szIllustrationTitle = t($t_base.'title/illustration_of_air_service');
        $szImageName = "CargoFlowAirfreight.png"; 
    }
    else
    {
        $szIllustrationTitle = t($t_base.'title/illustration_of_lcl');
        $szImageName = "CargoFlowLCL.png"; 
    }
    ?>
    <h5><?php echo $szIllustrationTitle; ?></h5>
    <p><img width="100%" src="<?=__BASE_STORE_IMAGE_URL__.'/'.$szImageName; ?>"></p>  
    <?php
    echo "||||";
    //echo display_multiple_lcl_services('',$t_base);
}
else if($operation_mode=='GET_AVAILABLE_DAY')
{
    $kConfig=new cConfig();
    $iTransitDays = sanitize_all_html_input(trim($_REQUEST['iTransitHours']));
    $idCutOffDay = sanitize_all_html_input(trim($_REQUEST['idCutOffDay']));
    $iAvailableDay = $kExport_Import->getAvailableDay($iTransitDays,$idCutOffDay);
    $szAvailableDayAry = $kConfig->getAllWeekDays($iAvailableDay);
    if(!empty($szAvailableDayAry))
    {
        echo $szAvailableDayAry[0]['id']."||||".$szAvailableDayAry[0]['szWeekDay'] ;
        die;
    }
}
elseif($operation_mode=='SELECTED_TABLE_DATA')
{
    $searchedDataAry=array();
    $idPricingWTW = sanitize_all_html_input(trim($_REQUEST['id']));
    $searchedDataAry = $kExport_Import->getDataForLCLById($idPricingWTW);	
    $_REQUEST['oboDataExportAry']['idOriginWarehouse'] = $searchedDataAry[0]['idWarehouseFrom'];
    $_REQUEST['oboDataExportAry']['idDestinationWarehouse'] = $searchedDataAry[0]['idWarehouseTo'];
    echo display_obo_lcl_bottom_html($t_base,false,$searchedDataAry[0],$_REQUEST['oboDataExportAry']);
}
elseif($operation_mode=='DELETE_RECORD')
{
    $idPricingWTW = sanitize_all_html_input(trim($_REQUEST['id']));
    $kExport_Import->loadPricingWtw($idPricingWTW);
    $_REQUEST['oboDataExportAry']['idOriginWarehouse'] = $kExport_Import->idWarehouseFrom ;
    $_REQUEST['oboDataExportAry']['idDestinationWarehouse'] = $kExport_Import->idWarehouseTo ;

    $kExport_Import->inactivatePricingWTW($idPricingWTW);
    echo display_obo_lcl_bottom_html($t_base,false,$searchedDataAry[0],$_REQUEST['oboDataExportAry']);
    echo "||||";
    $searchedDataAry = $kExport_Import->getDataForLCL(false,$idPricingWTW);

    $requestAry = array();
    if(empty($searchedDataAry))
    {
        $idOriginWarehouse = $_REQUEST['oboDataExportAry']['idOriginWarehouse'] ;
        $idDestinationWarehouse = $_REQUEST['oboDataExportAry']['idDestinationWarehouse'];		
        $requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
        $requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
    }
    echo display_multiple_lcl_services($searchedDataAry,$t_base,$requestAry);
	
}
else if(!empty($_REQUEST['oboDataExportAry']))
{ 
    $searchedDataAry=array();
    //for showing message at top for forwarder as admin and change its lclobo status
    // START
    $iWarehouseType = $_REQUEST['oboDataExportAry']['iWarehouseType'];
    
    $idForwarderUser =  (int)$_SESSION['forwarder_user_id'];
    if(($kForwarder->adminEachForwarder($idForwarderUser) == __ADMINISTRATOR_PROFILE_ID__ )|| ($kForwarder->adminEachForwarder($idForwarderUser) == __PRICING_PROFILE_ID__ ) && ((int)$_SESSION['forwarder_admin_id']==0))
    {
        $kConfig = new cConfig;
        if($kConfig->findForwarderOBOHaulageStauts($idForwarderUser))
        {
            $kConfig->updateForwarderOBOHaulageStauts($idForwarderUser);
        }
    }
    // END
    $searchedDataAry = $kExport_Import->getDataForLCL($_REQUEST['oboDataExportAry']);
    if(count($searchedDataAry)>1)
    {
        echo "MULTIPLE||||";
        echo display_multiple_lcl_services($searchedDataAry,$t_base,array(),$iWarehouseType);
        echo "||||";
        echo display_obo_lcl_bottom_html($t_base,false,'',$_REQUEST['oboDataExportAry'],false,$iWarehouseType);
        die;
    }
    else if(count($searchedDataAry)==1)
    {
        echo "SINGLE||||";
        echo display_multiple_lcl_services($searchedDataAry,$t_base,array(),$iWarehouseType);
        echo "||||";
        echo display_obo_lcl_bottom_html($t_base,false,'',$_REQUEST['oboDataExportAry'],false,$iWarehouseType);
        die;
    }
    else 
    {
        $requestAry = array();
        if(empty($searchedDataAry))
        {
            $idOriginWarehouse = $_REQUEST['oboDataExportAry']['idOriginWarehouse'] ;
            $idDestinationWarehouse = $_REQUEST['oboDataExportAry']['idDestinationWarehouse'];		
            $requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
            $requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
        } 
        echo "NODATA||||";
        echo display_multiple_lcl_services($searchedDataAry,$t_base,$requestAry,$iWarehouseType);
        echo "||||";
        echo display_obo_lcl_bottom_html($t_base,false,'',$_REQUEST['oboDataExportAry'],false,$iWarehouseType);
        die;
    }	
}
elseif(!empty($_REQUEST['editOBOLCLData']))
{
    if($kExport_Import->updateOBOLCL($_REQUEST['editOBOLCLData']))
    {
        $searchedDataAry = $kExport_Import->getDataForLCL('',$kExport_Import->id);

        $requestAry = array();
        if(empty($searchedDataAry))
        {
            $idOriginWarehouse = $_REQUEST['editOBOLCLData']['idOriginWarehouse'] ;
            $idDestinationWarehouse = $_REQUEST['editOBOLCLData']['idDestinationWarehouse'];		
            $requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
            $requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
        }
        else
        {
            $idOriginWarehouse = $searchedDataAry[0]['idWarehouseFrom'];
            $idDestinationWarehouse = $searchedDataAry[0]['idWarehouseTo'];

            $requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
            $requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
        } 
        echo "SUCCESS||||";
        echo display_obo_lcl_bottom_html($t_base,false,false,$requestAry);
        echo "||||";
        echo display_multiple_lcl_services($searchedDataAry,$t_base,$requestAry);
        echo "|||| <li>".t($t_base.'messages/update_service_success_message')."</li>";
        die;
    }
    if(!empty($kExport_Import->arErrorMessages))
    {
        if($_REQUEST['editOBOLCLData']['dtValidFrom']!='')
        {
            $todayDateTime=strtotime(date("Y-m-d"));
            if($todayDateTime>strtotime($_REQUEST['editOBOLCLData']['dtValidFrom']))
            {
                $dtValidFrom=date("d/m/Y");
            }
        } 
        echo "ERROR ||||" ;
        ?>
        <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
        <div id="regErrorList">
            <ul>
            <?php
                foreach($kExport_Import->arErrorMessages as $key=>$values)
                {
                    ?><li><?=$values?></li><?php 
                }
            ?>
            </ul>
        </div>
        <?php
        echo "||||".$dtValidFrom;
    }
}
else if(!empty($_REQUEST['addOBOLCLData']))
{
	if($kExport_Import->addOBOLCL($_REQUEST['addOBOLCLData']))
	{
		$searchedDataAry = $kExport_Import->getDataForLCL('',$kExport_Import->id);
		$requestAry = array();
		if(!empty($searchedDataAry))
		{
			$idOriginWarehouse = $searchedDataAry[0]['idWarehouseFrom'];
			$idDestinationWarehouse = $searchedDataAry[0]['idWarehouseTo'];
			
			$requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
			$requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
		}
		else
		{
			$idOriginWarehouse = $_REQUEST['editOBOLCLData']['idOriginWarehouse'] ;
			$idDestinationWarehouse = $_REQUEST['editOBOLCLData']['idDestinationWarehouse'];		
			$requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
			$requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
		}
		echo "SUCCESS||||";	
		echo display_obo_lcl_bottom_html($t_base,false,false,$requestAry);
		echo "||||";
		echo display_multiple_lcl_services($searchedDataAry,$t_base,$requestAry);
		echo "|||| <li>".t($t_base.'messages/add_service_success_message')."</li>";
		die;
	}
	if(!empty($kExport_Import->arErrorMessages))
	{
		echo "ERROR ||||" ;
		?>
		<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
	      foreach($kExport_Import->arErrorMessages as $key=>$values)
	      {
		      ?>
		      <li><?=$values?></li>
		      <?php 
	      }
		?>
		</ul>
		</div>
		<?php
	}
}

if($_REQUEST['flag']=='update_lclservice_video')
{
	$idForwarderContact=$_SESSION['forwarder_user_id'];
	$updateFlag=$_REQUEST['updateFlag'];
	
	$kExport_Import->updateLCLServiceVideoFlag($idForwarderContact,$updateFlag);
}
else if($_REQUEST['flag']=='show_lclservice_video')
{
	//$iHaulageVideo=$_REQUEST['iHaulageVideo'];
	$idForwarderContact=$_SESSION['forwarder_user_id'];
	$kForwarderContact->load($idForwarderContact);
	$iLCLServiceVideo=$kForwarderContact->iLCLServiceVideo;
	lclservice_video_popup($iLCLServiceVideo);
}
else if($_REQUEST['flag']=="show_lclservice_video_obo")
{
?>
<center><object width="425" height="350" data="https://youtu.be/6VOFeKGsdjE" type="application/x-shockwave-flash"><param name="src" value="https://youtu.be/6VOFeKGsdjE" /></object></center>
<?	
$_SESSION['showvideo']=='';
unset($_SESSION['showvideo']);
}
?>