<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
ini_set('max_execution_time',1200);
ini_set('memory_limit', '-1');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$display_profile_not_completed_message=true;

$szMetaTitle='Transporteca | Bulk Upload Services Approval List';
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
$t_base = "BulkUpload/";

if((int)$_SESSION['forwarder_user_id']==0)
{
	header('Location:'.__BASE_URL__);
	exit();	
}
$kUploadBulkService = new cUploadBulkService();
$completedApprovedByManagementArr=$kUploadBulkService->getAllDataNotApprovedByForwarder('topdata');
$bottomTableData=$kUploadBulkService->getAllDataNotApprovedByForwarder('bottomdata');
$forwarderAwaitingCFSDataArr=$kUploadBulkService->getForwarderUploadCFSLocation($idForwarder);
constantApiKey();
?>
<style type="text/css">
    #map_canvas { height: 100% }     
    .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
    .map-content p{font-size:12px !important;}
    .map_note p{font-size:12px;font-style:italic;display:inline-block;margin:1px 0 18px;}
</style>
    
<script type="text/javascript" src = "http://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true"></script>

 <script type="text/javascript">
	  var destinationIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/red-marker.png";
      var originIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/blue-marker.png";
	  var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
    var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(pipe_line_string)
    {
    	var result_ary = pipe_line_string.split("|");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	initialize(result_ary);
    	addPopupScrollClass('map_popup_div');
    }
    	  
	function initialize(result_ary) {
	  var olat = result_ary[0];
	  var olang = result_ary[1];
	  var dlat = result_ary[2];
	  var dlang = result_ary[3];
	  var distance = result_ary[4];
	  var origin_address = result_ary[5];
	  var destination_address = result_ary[6];
	  var iDirection = result_ary[7];
	  var origin = new google.maps.LatLng(olat, olang);      
      var destination = new google.maps.LatLng(dlat, dlang);
	  var myLatLng = new google.maps.LatLng(olat, olang);
	  var myOptions = {
		zoom: 6,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  
	  if(iDirection==1)
	  {
		 $("#distance_div").html('<?=t($t_base.'fields/distance_to_warehouse');?> ' + distance + ' ' +'Km');
	  }
	  else if(iDirection==2)
	  {
	  	 $("#distance_div").html('<?=t($t_base.'fields/distance_from_warehouse');?> ' + distance + ' ' +'Km');
	  }
	  
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(olat, olang),
		new google.maps.LatLng(dlat, dlang)   
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
	  
	  var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"Origin"
	  }); 

	  var oaddress = '<div class="map-content"><p>Your cargo:<br>'+origin_address+'</p></div>';
	  var daddress = '<div class="map-content"><p>Forwarder\'s warehouse:<br>'+destination_address+'</p></div>';

	  var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker, 
		'click', 
		infoCallback(oinfowindow, originmarker)
	  );
      //oinfowindow.open(map,originmarker);        

	  var destinationmarker = new google.maps.Marker({
		  position: destination,
		  map: map,
		  icon: destinationIcon,
		  visible: true,
		  title:"Destination"
	  });    
	  
	  var dinfowindow = new google.maps.InfoWindow();
	  dinfowindow.setContent(daddress);
	  google.maps.event.addListener(
		destinationmarker, 
		'click', 
		infoCallback(dinfowindow, destinationmarker)
	  );
	  //dinfowindow.open(map,destinationmarker);        
}
</script>
<div id="map_popup_div" style="display:none;">		
    <div id="popup-bg"></div>
    <div id="popup-container"  class="popup-scroller">			
        <div class="popup map-popup">
            <p class="close-icon" align="right">
                <a onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" href="javascript:void(0);">
                    <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <div id="distance_div" style="font-weight:bold;margin-bottom:5px;"></div>
            <div id="map_canvas" style="width:500px;height:500px;border: 1px solid #C4BDA1;"></div>
            <div class="map_note">
                <p><?=t($t_base.'fields/notes');?>: <?=t($t_base.'fields/google_map_notes');?></p>
            </div>
            <p align="center"><a href="javascript:void(0);" onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
        </div>
   </div>
</div>
<div id="hsbody-2">
    <div id="customs_clearance_pop" class="help-pop">
</div>
<div id="customs_clearance_pop_right" class="help-pop right">
</div>
	<div class="hsbody-2-left account-links">
		<?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
	</div>
	<div id="loader" class="loader_popup_bg" style="display:none;">
		<div class="popup_loader"></div>
		<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification" style="padding:10px;width:290px;">
		<p><?=t($t_base.'messages/plz_wait_while_we_process');?></p>
		<br/><br/>
		<p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
		</div>	</div>			
					
	</div>
	<div class="hsbody-2-right" id="approve_data_review">
		<div id="open_processing_popup"></div>
		<p align="left"><h5><strong><?=t($t_base.'messages/data_approval_from_transporteca')?></strong></h5></p>		
		<p style="TEXT-ALIGN:LEFT"><?=t($t_base.'messages/processing_data_review_record_by_record')?></p>
	   	<br/>
	    <div id="processing_cost_approved_list">
		<?php
		process_cost_approved_by_management($completedApprovedByManagementArr,$forwarderAwaitingCFSDataArr);
		?>
		
		</div>	
		<br/><br/>
		<p style="TEXT-ALIGN:LEFT"><?=t($t_base.'messages/data_submitted_but_not_yet_been_processed')?></p>
		<br/>
		<div id="waiting_for_approved_list">
			<?php
                            waiting_for_approved_by_management($bottomTableData);
			?>
		</div>
		<iframe style="border:0" name="hssiframe" id="hssiframe" width="0px" height="0px"></iframe>
	</div>
</div>

<form method="post" id="obo_haulage_tryit_out_hidden_form" action="<?=__FORWARDER_HOME_PAGE_URL__?>/oboHaulageHelpMap.php" target="google_map_target_1">
		<input type="hidden" name="oboHaulageHiddenAry[szPostCode]" id="szPostCode_hidden" value="">
		<input type="hidden" name="oboHaulageHiddenAry[szLatitude]" id="szLatitude_hidden" value="">
		<input type="hidden" name="oboHaulageHiddenAry[szLongitude]" id="szLongitude_hidden" value="">
		<input type="hidden" name="oboHaulageHiddenAry[szWhsLatitude]" id="szWhsLatitude_hidden" value="">
		<input type="hidden" name="oboHaulageHiddenAry[szWhsLongitude]" id="szWhsLongitude_hidden" value="">
		<input type="hidden" name="oboHaulageHiddenAry[idWarehouse]" id="idWarehouse_hidden" value="">
		<input type="hidden" name="oboHaulageHiddenAry[idDirection]" id="idDirection_hidden" value="">
 </form>
	
<div id="popup_container_google_map" style="display:none;">
	<div id="popup-bg"></div>
	<div id="popup-container">
		<iframe id="google_map_target_1" class="google_map_select popup"  name="google_map_target_1" src="#" >
		</iframe>
	</div>
</div>
<?php
	require_once( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>