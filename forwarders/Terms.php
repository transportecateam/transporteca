<?php

ob_start();

if(!isset($_SESSION))

{

session_start();

}

$szMetaTitle="Transporteca | Terms & Conditions for Forwarders";

$display_profile_not_completed_message = false;

if( !defined( "__APP_PATH__" ) )

define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );

require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
if(!isset($_SESSION['forwarder_id']) && $_GET['status']=='update')
{
	$_SESSION['update_tnc'] = 1;
}
checkAuthForwarder();

$t_base="Forwarders/TermCondition/";

$kForwarder = new cForwarder();

$kForwarder->load($idForwarder);

$kForwarderContact= new cForwarderContact();

$kForwarderContact->getMainAdmin($idForwarder);

$kForwarderContact->load($_SESSION['forwarder_user_id']);
$forwarderAgreementArr=$kForwarder->getForwarderAgreement($idForwarder);
//$agreementVersion="Version 28 August 2012";
$agreement = $kForwarder->selectVersion();
if($agreement!=array())
{
$agreementVersion = 'Version '.date('d F Y',strtotime($agreement['dtVersionUpdated']));
}
else
{
	$agreementVersion = '';
}
if($kForwarderContact->idForwarderContactRole=='1')
{
	if(!empty($_POST))
	{
		if($_POST['agree']!='')
		{	
			if($kForwarder->updateForwarderAgreement($idForwarder,$_POST['agree'],$_POST['agreementVersion']))
			{
				header('Location:'.__FORWARDER_TERM_CONDITION_URL__);
				die();
			}
		}
	}
}

//$version="Version-28August2012.pdf";
$KwarehouseSearch=new cWHSSearch();
$tnc=$KwarehouseSearch->selectQuery(1);
?>

<div id="hsbody-2">

<div class="hsbody-2-left">

	<h4><strong><?=t($t_base.'title/term_condition')?></strong></h4>

	<ol class="forwarders_tandc">

	<?if($tnc)
	{
		foreach($tnc as $terms)
		{	
			echo '<li><a href="#'.str_replace($replacearray,"_",strtolower($terms['szHeading'])).'"  > '.ucfirst($terms['szHeading']).'</a></li>';
		}
	}
	?>

	<li><a href="#agreement">Agreement</a></li>
	</ol>

	<p style="margin:5px 1 0 -7px"><a href="javascript:void(0)" onclick="download_term_condition_pdf();" style="color:#000000;font-style: normal;text-decoration: none;font-size: 16px;">PDF Version</a></p>
	
	<p style="margin:5px 1 0 -7px">
	<form name="tnc_left" id="tnc_left" method="post">
	<br/>
	<p align="center">
		
	<? if($kForwarder->iAgreeTNC!='1' || $kForwarder->iVersionUpdate!='0'){?>
	<input type="hidden" value="<?=$agreementVersion?>" name="agreementVersion" id="agreementVersion"/>
	<input type="hidden" value="1" name="agree" id="agree"/>

	<a class="button1" <? if($kForwarderContact->idForwarderContactRole=='1') {?>onclick="agree_tncleft();" href="javascript:void(0)" <? }else {?> style="opacity:0.4;" <? }?> href="javascript:void(0)">

		<span>Agree</span>

		</a></p>

	<? }else if($kForwarder->iAgreeTNC=='1' || $kForwarder->iVersionUpdate!='1'){?>

	<input type="hidden" value="0" name="agree" id="agree"/>
	<input type="hidden" value="<?=$agreementVersion?>" name="agreementVersion" id="agreementVersion"/>
	<a class="button2" <? if($kForwarderContact->idForwarderContactRole=='1') {?>onclick="agree_tncleft();" href="javascript:void(0)" <? }else {?> style="opacity:0.4;" <? }?> >

		<span>DISAGREE</span>

		</a></p>	

	<? }?>

		</form>
	</p>
</div>	

	<div class="hsbody-2-right">	

	<? foreach($tnc as $terms)
	{?>
		<a name="<?=str_replace($replacearray,"_",strtolower($terms['szHeading']))?>" href="javascript:void(0);" ></a>
		<h5 class="clearfix terms-heading">
		<span class="fl-80"><strong><?=ucfirst($terms['szHeading'])?></strong></span>
		<a class="fr" href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
		</h5>
		<div class="link16">
		<?=$terms['szDescription']?>
		</div>
		<br />
	<? } ?>

	

	<br/>

	<a href="javascript:void(0);" name="agreement"></a>

	<h5><strong>Agreement</strong> </h5>
	<p align="right" class="terms_condition_top">
		<a href="#top"><?=t($t_base.'title/top')?></a>
	</p>
	<? 
		if(!empty($forwarderAgreementArr)){
			foreach($forwarderAgreementArr as $forwarderAgreementArrs)
			{
				if($forwarderAgreementArrs['dtAgreement']!='' && $forwarderAgreementArrs['dtAgreement']!='0000-00-00 00:00:00')
				{
					$agreementDate=date('d F Y',strtotime($forwarderAgreementArrs['dtAgreement']));
					
				}
				if($forwarderAgreementArrs['iAgree']=='1')
				{
					$agreement="agree";
				}
				else if($forwarderAgreementArrs['iAgree']=='0')
				{
					$agreement="disagree";
				}
				else if($forwarderAgreementArrs['iAgree']=='2')
				{
					$agreement="changed";
				}
				
			?>
				<p><?if($forwarderAgreementArrs['iAgree']!='2'){?><?=$agreementDate?> - <? } ?><?=$forwarderAgreementArrs['iAgreementVersion']?> <?=$agreement?> by <?=$forwarderAgreementArrs['szName']?> <?if($forwarderAgreementArrs['iAgree'] !=2){?>on behalf of <?=$forwarderAgreementArrs['szCompanyName']?><? } ?></p>
	<?		}
		}	
	?>	
	
	
	<form name="tnc" id="tnc" method="post">
	<br/>
	<p align="center">

	<? if($kForwarder->iAgreeTNC!='1' || $kForwarder->iVersionUpdate!='0'){?>
	<input type="hidden" value="<?=$agreementVersion?>" name="agreementVersion" id="agreementVersion"/>
	<input type="hidden" value="1" name="agree" id="agree"/>

	<a class="button1" <? if($kForwarderContact->idForwarderContactRole=='1') {?>onclick="agree_tnc();" href="javascript:void(0)" <? }else {?> style="opacity:0.4;" <? }?> href="javascript:void(0)">

		<span>Agree</span>

		</a></p>

	<? }else if($kForwarder->iAgreeTNC=='1' || $kForwarder->iVersionUpdate!='1'){?>

	<input type="hidden" value="0" name="agree" id="agree"/>
	<input type="hidden" value="<?=$agreementVersion?>" name="agreementVersion" id="agreementVersion"/>
	<a class="button2" <? if($kForwarderContact->idForwarderContactRole=='1') {?>onclick="agree_tnc();" href="javascript:void(0)" <? }else {?> style="opacity:0.4;" <? }?> >

		<span>DISAGREE</span>

		</a></p>	

	<? }?>

		</form>

</div>

</div>







<?php

include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );

?>