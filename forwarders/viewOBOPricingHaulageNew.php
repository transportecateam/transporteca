<?php
/**
 * Forwarder Pricing Haluage  
 */
 ob_start();
session_start();
$szMetaTitle="Transporteca | Update Haulage Services and Pricing - Line by Line";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php");

checkAuthForwarder();
constantApiKey();
$t_base = "BulkUpload/";
$t_base_error="Error";
$t_base_haulage="HaulaugePricing/";
$Excel_export_import=new cExport_Import();
$kConfig = new cConfig();

if((int)$_SESSION['forwarder_user_id']==0)
{
    header('Location:'.__BASE_URL__.'/forwarders/');
    exit();	
}

$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder);
$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$forwardCountriesArr[0]['idCountry']);

$forwardCitiesArr=$Excel_export_import->getForwaderWarehousesCity($idForwarder,$forwardCountriesArr[0]['idCountry']);
$kForwarderContact->load($_SESSION['forwarder_user_id']);


$kWhsSearch = new cWHSSearch();   
$arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      
  
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];
$szLanguage = "en"; 

if($kForwarderContact->iHaulageVideo=='1')
{
    ?>
    <script type="text/javascript"> 
        show_haulage_video_popup('<?=$kForwarderContact->iHaulageVideo?>');
    </script>
    <?php	
}
?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places,drawing,geometry,visualization&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
<style type="text/css">
    #map_canvas { height: 100% }     
    .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
    .map-content p{font-size:12px !important;}
    .map_note p{font-size:12px;font-style:italic;display:inline-block;margin:1px 0 18px;}
</style>
<!--    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
    </script>-->

<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style> 
 <script type="text/javascript">
    var destinationIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/red-marker.png";
    var originIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/blue-marker.png";
    var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
    var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(pipe_line_string)
    {
    	var result_ary = pipe_line_string.split("|");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	addPopupScrollClass('map_popup_div');
    	initialize(result_ary);
    }
    	  
    function initialize(result_ary) 
    {
        var olat = result_ary[0];
        var olang = result_ary[1];
        var dlat = result_ary[2];
        var dlang = result_ary[3];
        var distance = result_ary[4];
        var origin_address = result_ary[5];
        var destination_address = result_ary[6];
        var iDirection = result_ary[7];
        var origin = new google.maps.LatLng(olat, olang);      
        var destination = new google.maps.LatLng(dlat, dlang);
        var myLatLng = new google.maps.LatLng(olat, olang);
        var myOptions = {
		zoom: 6,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  
	  if(iDirection==1)
	  {
		 $("#distance_div").html('<?=t($t_base.'fields/distance_to_warehouse');?> ' + distance + ' ' +'Km');
	  }
	  else if(iDirection==2)
	  {
	  	 $("#distance_div").html('<?=t($t_base.'fields/distance_from_warehouse');?> ' + distance + ' ' +'Km');
	  }
	  
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(olat, olang),
		new google.maps.LatLng(dlat, dlang)   
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
	  
        var originmarker = new google.maps.Marker({
            position: origin,
            map: map,
            icon: originIcon,
            visible: true,
            title:"Origin"
        }); 

        var oaddress = '<div class="map-content"><p>Your cargo:<br>'+origin_address+'</p></div>';
        var daddress = '<div class="map-content"><p>Forwarder\'s warehouse:<br>'+destination_address+'</p></div>';

        var oinfowindow = new google.maps.InfoWindow();
        oinfowindow.setContent(oaddress);
        google.maps.event.addListener(
            originmarker, 
            'click', 
            infoCallback(oinfowindow, originmarker)
        );
      //oinfowindow.open(map,originmarker);        

        var destinationmarker = new google.maps.Marker({
              position: destination,
              map: map,
              icon: destinationIcon,
              visible: true,
              title:"Destination"
        });     
        var dinfowindow = new google.maps.InfoWindow();
        dinfowindow.setContent(daddress);
        google.maps.event.addListener(
              destinationmarker, 
              'click', 
              infoCallback(dinfowindow, destinationmarker)
        );
        //dinfowindow.open(map,destinationmarker);        
}
</script> 
<div id="map_popup_div" style="display:none;">		
    <div id="popup-bg"></div>
    <div id="popup-container"  class="popup-scroller">			
        <div class="popup map-popup">
            <p class="close-icon" align="right">
                <a onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" href="javascript:void(0);">
                    <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <div id="distance_div" style="font-weight:bold;margin-bottom:5px;"></div>
            <div id="map_canvas" style="width:500px;height:500px;border: 1px solid #C4BDA1;"></div>
            <div class="map_note">
                <p><?=t($t_base.'fields/notes');?>: <?=t($t_base.'fields/google_map_notes');?></p>
            </div>
            <p align="center"><a href="javascript:void(0);" onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
        </div>
   </div>
</div>

<div id="hsbody-2">
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="customs_clearance_pop_right" class="help-pop right">
</div>
<div class="hsbody-2-left account-links">
		<?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
	</div>
	<div class="hsbody-2-right">
		<h5><?=t($t_base.'title/update_pricing_haulage_one_by_one')?>. <a href="javascript:void(0)" onclick="show_haulage_video_popup(<?=$kForwarderContact->iHaulageVideo?>);"><?=t($t_base.'title/show_me_a_video')?></a></h5>
			<form name="haulageOBO" id="haulageOBO" method="post">
				<div class="oh haulage-get-data">
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/country')?></span>
						<select name="dataExportArr[haulageCountry]" id="haulageCountry" onchange="showForwarderWarehouseOBONew('<?=$idForwarder?>',this.value,'country','city_warehouse')">
							<option value=""><?=t($t_base.'fields/select')?></option>		
							<?php
										if(!empty($forwardCountriesArr))
										{
											foreach($forwardCountriesArr as $forwardCountriesArrs)
											{
												?>
												<option value="<?=$forwardCountriesArrs['idCountry']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div id='city_warehouse'>
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/city')?></span>
						<select name="dataExportArr[szCity]" disabled id="szCity" onchange="showForwarderWarehouseOBONew('<?=$idForwarder?>',this.value,'city','warehouse')">
									<option value=""><?=t($t_base.'fields/all');?></option>
									<?php
										if(!empty($forwardCitiesArr))
										{
											foreach($forwardCitiesArr as $forwardCitiesArrs)
											{
												?>
												<option value="<?=$forwardCitiesArrs['szCity']?>"><?=$forwardCitiesArrs['szCity']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div class="fl-15 s-field" id="warehouse">
						<span class="f-size-12">Name</span>
						<select name="dataExportArr[haulageWarehouse]" disabled id="haulageWarehouse" onchange="enableHaulageGetDataNew('<?=$idForwarder?>')">
							<option value=""><?=t($t_base.'fields/select');?></option>
							<?php
								if(!empty($forwardWareHouseArr))
								{
									foreach($forwardWareHouseArr as $forwardWareHouseArrs)
									{
										?>
										<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
										<?
									}
								}
							?>
						</select>
					</div>
					</div>
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
						<select name="dataExportArr[idDirection]" disabled id="idDirection">
							<option value='1'><?=t($t_base.'fields/export_data')?></option>
							<option value='2'><?=t($t_base.'fields/import_data')?></option>
							
						</select>
					</div>
					<div class="fr-20" align="right" style="width:18%;padding-top: 12px;">						
						<a href="javascript:void(0)" class="button1" style="opacity:0.4;" id="get_haulage_data"><span id="text_change"><?=t($t_base.'fields/get_data')?></span></a>
					</div>
				</div>
			</form>	
			<div id="model_list">
				
				<?=pricingHaulage();?>
				<hr>
				<?=t($t_base_haulage.'title/illustration_of_what_haulage_services_cover')?>
				<img src="<?=__BASE_STORE_IMAGE_URL__.'/new-haulagePricingBulk-1.png'?>" id="Upload one by one Haulage Services" >
				<!-- <canvas style="margin-left:-10px;" id="myCanvas" width="730" height="180"></canvas> -->
			</div>
			<form method="post" id="obo_haulage_tryit_out_hidden_form" action="<?=__FORWARDER_HOME_PAGE_URL__?>/oboHaulageHelpMap.php" target="google_map_target_1">
                            <input type="hidden" name="oboHaulageHiddenAry[szPostCode]" id="szPostCode_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[szLocation]" id="szLocation_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[szLatitude]" id="szLatitude_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[szLongitude]" id="szLongitude_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[szWhsLatitude]" id="szWhsLatitude_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[szWhsLongitude]" id="szWhsLongitude_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[idWarehouse]" id="idWarehouse_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[idDirection]" id="idDirection_hidden" value="">
                            <input type="hidden" name="oboHaulageHiddenAry[iForwarderSectionFlag]" id="iForwarderSectionFlag" value="1">
                        </form>
			
		<div id="popup_container_google_map" style="display:none;">
			<div id="popup-bg"></div>
			<div id="popup-container">
			<div class="popup" style="width:720px;margin-top:30px;">
			<p class="close-icon" align="right">
			<a href="javascript:void(0);" onclick="window.top.window.hide_google_map();">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
				<iframe id="google_map_target_1" class="google_map_select"  style="width:717px;" scrolling="no" name="google_map_target_1">
				</iframe>
			</div>
			</div>
		</div> 
                <input type="hidden" value="" name="openHaulageModel" id="openHaulageModel">
                <input type="hidden" value="" name="openHaulageModelMapped" id="openHaulageModelMapped">
                <input type="hidden" value="0" name="callvalue" id="callvalue">
</div>
</div> 
<?php
//echo $kForwarderContact->iHaulageVideo."hello";
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>