<?php
 ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "BulkUpload/";
$Excel_export_import=new cExport_Import();
$kWHSSearch = new cWHSSearch();
$kHaulagePricing=new cHaulagePricing();
$maxrowsexcced=$kWHSSearch->getManageMentVariableByDescription('__MAX_ROWS_EXCEED__');
if($_REQUEST['flag']=='showOriginWarehouse')
{
    $idForwarder = (int)$_REQUEST['idForwarder'];
    $iWarehouseType = (int)$_REQUEST['iWarehouseType'];
    $countryArr=explode("_",$_REQUEST['idCountry']);
    $forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$countryArr[0],false,true,$iWarehouseType);
    ?>
    <select name="dataExportArr[originWarehouse]" id="originWarehouse">
        <option value=""><?=t($t_base.'fields/all');?></option>
        <?php
            if(!empty($forwardWareHouseArr))
            {
                foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                {
                    ?>
                    <option value="<?=$forwardWareHouseArrs['id']?>_<?=$forwardWareHouseArrs['szWareHouseName']?>_<?=$forwardWareHouseArrs['szCountryName']?>_<?=$forwardWareHouseArrs['idCountry']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                    <?php
                }
            }
        ?>
    </select>
<?php
}
if($_REQUEST['flag']=='showDesWarehouse')
{
    $idForwarder=$_REQUEST['idForwarder'];
    $iWarehouseType = (int)$_REQUEST['iWarehouseType'];
    $countryArr=explode("_",$_REQUEST['idCountry']);
    $forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$countryArr[0],false,true,$iWarehouseType);
    ?>
    <select name="dataExportArr[desWarehouse]" id="desWarehouse">
        <option value=""><?=t($t_base.'fields/all');?></option>
        <?php
            if(!empty($forwardWareHouseArr))
            {
                foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                {
                    ?>
                    <option value="<?=$forwardWareHouseArrs['id']?>_<?=$forwardWareHouseArrs['szWareHouseName']?>_<?=$forwardWareHouseArrs['szCountryName']?>_<?=$forwardWareHouseArrs['idCountry']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                    <?php
                }
            }
        ?>
    </select>
<?php
}
if($_REQUEST['flag']=='OriginAdd')
{
    $originCountry=$_REQUEST['originCountry'];
    $originWarehouseArr=explode("_",$_REQUEST['originWarehouse']);
    $count=$_REQUEST['count'];
    ?>
    <th width="33%" valign="top">
        <?=$originWarehouseArr[2]?>															
    </th>
    <td width="67%" valign="top">
        <?=$originWarehouseArr[1]?>	
    </td>
<?php	
}
if($_REQUEST['flag']=='DesAdd')
{
    $desCountry=$_REQUEST['desCountry'];
    $desWarehouseArr=explode("_",$_REQUEST['desWarehouse']);
    $count=$_REQUEST['count']; 
    ?> 
    <th valign="top">
        <?=$desWarehouseArr[2]?>															
    </th>
    <td valign="top">
        <?=$desWarehouseArr[1]?>	
    </td>
<?php	
}
if($_REQUEST['flag']=='OriginAddAll')
{
    $idCountry=0;
    if($_REQUEST['originCountry']!="")
    {
        $countryArr=explode("_",$_REQUEST['originCountry']);
        $idCountry=$countryArr[0];
    }
    $iWarehouseType = (int)$_REQUEST['iWarehouseType'];
    //$idForwarder=1;
    //echo $idForwarder;
    $forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry,false,false,$iWarehouseType); 
	
    if(!empty($forwardWareHouseArr))
    {
			?>
			<tbody>
			<tr>
                            <th style="border-top:none;" width="33%"><strong><?=t($t_base.'fields/country');?></strong></th>
                            <td style="border-top:none;" width="67%"><strong><?=t($t_base.'fields/name');?></strong></td>
			</tr>			
			<?php
			$k=1;
			$w_c_value="";
			foreach($forwardWareHouseArr as $forwardWareHouseArrs)
			{
				$div_id="origin_data_".$k;
				$w_id=$forwardWareHouseArrs['id']."_".$forwardWareHouseArrs['idCountry'];
				if($w_c_value=="")
				{
					$w_c_value .=$w_id;
				}
				else
				{
					$w_c_value .=";".$w_id;
				}
				?>
					<tr id="origin_data_<?=$k?>" onclick="delete_ware_houseInfo('<?=$w_id?>','<?=$div_id?>','Origin','<?=$maxrowsexcced;?>')">
						<th width="33%" valign="top">
						<?=$forwardWareHouseArrs['szCountryName']?>
						</th>
						<td width="67%" valign="top">
						<?=$forwardWareHouseArrs['szWareHouseName']?>
						</td>
					</tr>
				<?php
				++$k;
			}?>
				<tr id="origin_data_<?=$k?>" onclick="">
				</tr>
			</tbody>
			<script>
				$("#idOriginWarehouse").attr('value','<?=$w_c_value?>');
				$("#originWarehouseAdded").attr('value','<?=$k?>');
			</script>
			<?php
		}
}
if($_REQUEST['flag']=='DesAddAll')
{
	//$idForwarder=1;
	$idCountry=0;
	
	if($_REQUEST['desCountry']!="")
	{
		$countryArr=explode("_",$_REQUEST['desCountry']);
		$idCountry=$countryArr[0];
	}
	$iWarehouseType = (int)$_REQUEST['iWarehouseType'];
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry,false,false,$iWarehouseType);
	
	
		if(!empty($forwardWareHouseArr))
		{
			?>
			<tbody>
			<tr>
				<th style="border-top:none;" width="33%"><strong><?=t($t_base.'fields/country');?></strong></th>
				<td style="border-top:none;" width="67%"><strong><?=t($t_base.'fields/name');?></strong></td>
			</tr>
			<?php
			$k=1;
			$w_c_value="";
			foreach($forwardWareHouseArr as $forwardWareHouseArrs)
			{
				$div_id="des_data_".$k;
				$w_id=$forwardWareHouseArrs['id']."_".$forwardWareHouseArrs['idCountry'];
				if($w_c_value=="")
				{
					$w_c_value .=$w_id;
				}
				else
				{
					$w_c_value .=";".$w_id;
				}
				?>
					<tr id="des_data_<?=$k?>" onclick="delete_ware_houseInfo('<?=$w_id?>','<?=$div_id?>','Destination','<?=$maxrowsexcced?>')">
						<th width="33%" valign="top">
						<?=$forwardWareHouseArrs['szCountryName']?>
						</th>
						<td width="67%" valign="top">
						<?=$forwardWareHouseArrs['szWareHouseName']?>
						</td>
					</tr>
				<?
				++$k;
			}?>
				<tr id="des_data_<?=$k?>" onclick="">
				</tr>
			</tbody>
			<script>
				$("#idDesWarehouse").attr('value','<?=$w_c_value?>');
				$("#desWarehouseAdded").attr('value','<?=$k?>');
			</script>
			<?php
		}
}
if($_REQUEST['flag']=='HaulageAddAll')
{
	$idCountry=0;
	if($_REQUEST['haulageCountry']!="")
	{
		$countryArr=explode("_",$_REQUEST['haulageCountry']);
		$idCountry=$countryArr[0];
	}
	//$idForwarder=1;
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry);
	
	$pricingModelArr=$kHaulagePricing->getAllModelPricing();
        
		if(!empty($forwardWareHouseArr))
		{
			?>
			<tbody>
			<tr>
				<th style="border-top:none;" width="21%"><strong><?=t($t_base.'fields/country');?></strong></th>
				<td style="border-top:none;" width="32%"><strong><?=t($t_base.'fields/cfs_name');?></strong></td>
				<td style="border-top:none;" width="47%"><strong><?=t($t_base.'fields/pricing_model');?></strong></td>
			</tr>
			<?php
			$k=1;
			$w_c_value="";
			foreach($forwardWareHouseArr as $forwardWareHouseArrs)
			{
			
				foreach($pricingModelArr as $pricingModelArrs)
				{
					$dataArr=array();
					$dataArr[0]['idWarehouse']=$forwardWareHouseArrs['id'];
					$dataArr[0]['idPricingModel']=$pricingModelArrs['id'];
                                        $dataArr[0]['iWarehouseType']=$forwardWareHouseArrs['iWarehouseType'];
					//print_r($dataArr);
					$totalUpdatedService=0;
					$totalUpdatedService=$Excel_export_import->getAllUpdatedHaulageModelExists($dataArr);
					if((int)$totalUpdatedService>0)
					{
						$div_id="haulage_data_".$k;
						$w_id=$forwardWareHouseArrs['id']."_".$forwardWareHouseArrs['idCountry']."_".$pricingModelArrs['id'];
						if($w_c_value=="")
						{
							$w_c_value .=$w_id;
						}
						else
						{
							$w_c_value .=";".$w_id;
						}
                                                
                                                if($pricingModelArrs['id']==4)
                                                {
                                                    if($forwardWareHouseArrs['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                                                    {  
                                                        $szModelLongName = __DISTANCE_MODEL_LONG_NAME_AIR__;
                                                    }
                                                    else
                                                    {
                                                        $szModelLongName = __DISTANCE_MODEL_LONG_NAME_CFS__;
                                                    } 
                                                }
                                                else
                                                {
                                                    $szModelLongName = $pricingModelArrs['szModelLongName'];
                                                }
						?>
							<tr id="haulage_data_<?=$k?>" onclick="delete_ware_haulage('<?=$w_id?>','<?=$div_id?>','haulage')">
								<th valign="top">
								<?=$forwardWareHouseArrs['szCountryName']?>
								</th>
								<td valign="top">
								<?=$forwardWareHouseArrs['szWareHouseName']?>
								</td>
								<td valign="top">
								<?=$szModelLongName?>
								</td>
							</tr>
						<?php
						++$k;
					}
				}
			}?>
				<tr id="haulage_data_<?=$k?>" onclick="">
				</tr>
			</tbody>
			<script>
				$("#idhaulageWarehouse").attr('value','<?=$w_c_value?>');
				$("#haulageWarehouseAdded").attr('value','<?=$k?>');
			</script>
			<?php
		}
}
if($_REQUEST['flag']=='haulageAdd')
{
	$originCountry=$_REQUEST['haulageCountry'];
	$haulagePricingModel=$_REQUEST['haulagePricingModel'];
	if($haulagePricingModel!='')
	{
		$haulagePricingModelArr=explode("_",$haulagePricingModel);
	}
	$haulageWarehouseArr=explode("_",$_REQUEST['haulageWarehouse']);
	$count=$_REQUEST['count'];
	$dataArr[]['idWarehouse']=$haulageWarehouseArr[1];
	$dataArr[]['idPricingModel']=$haulagePricingModelArr[1];
	?>
		<th valign="top">
			<?=$haulageWarehouseArr[2]?>															
		</th>
		<td valign="top">
			<?=$haulageWarehouseArr[1]?>	
		</td>
		<td valign="top">
			<?=$haulagePricingModelArr[1]?>	
		</td>
<?php	
}
if($_REQUEST['flag']=='showHaulageWarehouse')
{
	$idForwarder=$_REQUEST['idForwarder'];
	$countryArr=explode("_",$_REQUEST['idCountry']);
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehousesPricingModel($idForwarder,$countryArr[0]);
	$pricingModelArr=$kHaulagePricing->getAllModelPricing();
	$forwardWareHouseHaulageModelArr=array();
	$forwardWareHouseHaulageModelArr=$Excel_export_import->getForwaderWarehousesPricingModel($idForwarder,$countryArr[0],0,'pricingModel');
	?>
	<div class="fl-30 s-field">
	<span class="f-size-12">Name</span><br/>
	<select name="dataExportArr[haulageWarehouse]" id="haulageWarehouse" onchange="sel_warehouse_for_haulage_model('<?=$idForwarder?>','<?=$countryArr[0]?>',this.value,'haulage_princing_model')">
		<option value=""><?=t($t_base.'fields/all');?></option>
		<?php
			if(!empty($forwardWareHouseArr))
			{
				foreach($forwardWareHouseArr as $forwardWareHouseArrs)
				{
					?>
					<option value="<?=$forwardWareHouseArrs['id']?>_<?=$forwardWareHouseArrs['szWareHouseName']?>_<?=$forwardWareHouseArrs['szCountryName']?>_<?=$forwardWareHouseArrs['idCountry']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
					<?
				}
			}
		?>
	</select>
	</div>
	<div class="fl-30 s-field" id="haulage_princing_model">
	<span class="f-size-12"><?=t($t_base.'fields/pricing_model');?></span><br/>
		<select name="dataExportArr[haulagePricingModel]" id="haulagePricingModel">
			<option value=""><?=t($t_base.'fields/all');?></option>
			<?php
				if(!empty($pricingModelArr))
				{
					foreach($pricingModelArr as $pricingModelArrs)
					{
						if(!empty($forwardWareHouseHaulageModelArr) && in_array($pricingModelArrs['id'],$forwardWareHouseHaulageModelArr))
						{
							?>
							<option value="<?=$pricingModelArrs['id']?>_<?=$pricingModelArrs['szModelLongName']?>"><?=$pricingModelArrs['szModelLongName']?></option>
							<?
						}
					}
				}
			?>
		</select>
	</div>
<?php
}
if($_REQUEST['flag']=='OriginAddCCAll')
{
	$idCountry=0;
	if($_REQUEST['originCountry']!="")
	{
		$countryArr=explode("_",$_REQUEST['originCountry']);
		$idCountry=$countryArr[0];
	}
	//$idForwarder=1;
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry);
	
	
		if(!empty($forwardWareHouseArr))
		{
			?>
			<tbody>
			<tr>
				<th style="border-top:none;" width="33%"><strong><?=t($t_base.'fields/country');?></strong></th>
				<td style="border-top:none;" width="67%"><strong><?=t($t_base.'fields/name');?></strong></td>
			</tr>
			<?php
			$k=1;
			$w_c_value="";
			foreach($forwardWareHouseArr as $forwardWareHouseArrs)
			{
				$div_id="origin_data_".$k;
				$w_id=$forwardWareHouseArrs['id']."_".$forwardWareHouseArrs['idCountry'];
				if($w_c_value=="")
				{
					$w_c_value .=$w_id;
				}
				else
				{
					$w_c_value .=";".$w_id;
				}
				?>
					<tr id="origin_data_<?=$k?>" onclick="delete_ware_houseInfo_customclearance('<?=$w_id?>','<?=$div_id?>','Origin','<?=$maxrowsexcced?>')">
						<th valign="top">
						<?=$forwardWareHouseArrs['szCountryName']?>
						</th>
						<td valign="top">
						<?=$forwardWareHouseArrs['szWareHouseName']?>
						</td>
					</tr>
				<?php
				++$k;
			}?>
				<tr id="origin_data_<?=$k?>" onclick="">
				</tr>
			</tbody>
			<script>
				$("#idOriginWarehouse").attr('value','<?=$w_c_value?>');
				$("#originWarehouseAdded").attr('value','<?=$k?>');
			</script>
			<?php
		}
}
if($_REQUEST['flag']=='DesAddCCAll')
{
	//$idForwarder=1;
	$idCountry=0;
	
	if($_REQUEST['desCountry']!="")
	{
		$countryArr=explode("_",$_REQUEST['desCountry']);
		$idCountry=$countryArr[0];
	}
	
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry);
	
	
		if(!empty($forwardWareHouseArr))
		{
			?>
			<tbody>
			<tr>
				<th style="border-top:none;" width="33%"><strong><?=t($t_base.'fields/country');?></strong></th>
				<td style="border-top:none;" width="67%"><strong><?=t($t_base.'fields/name');?></strong></td>
			</tr>
			<?php
			$k=1;
			$w_c_value="";
			foreach($forwardWareHouseArr as $forwardWareHouseArrs)
			{
				$div_id="des_data_".$k;
				$w_id=$forwardWareHouseArrs['id']."_".$forwardWareHouseArrs['idCountry'];
				if($w_c_value=="")
				{
					$w_c_value .=$w_id;
				}
				else
				{
					$w_c_value .=";".$w_id;
				}
				?>
					<tr id="des_data_<?=$k?>" onclick="delete_ware_houseInfo_customclearance('<?=$w_id?>','<?=$div_id?>','Destination','<?=$maxrowsexcced?>')">
						<th valign="top">
						<?=$forwardWareHouseArrs['szCountryName']?>
						</th>
						<td valign="top">
						<?=$forwardWareHouseArrs['szWareHouseName']?>
						</td>
					</tr>
				<?php
				++$k;
			}?>
				<tr id="des_data_<?=$k?>" onclick="">
				</tr>
			</tbody>
			<script>
				$("#idDesWarehouse").attr('value','<?=$w_c_value?>');
				$("#desWarehouseAdded").attr('value','<?=$k?>');
			</script>
			<?php
		}
}
if($_REQUEST['flag']=='Warehouse_city')
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCoustry=$_REQUEST['idCountry'];
	
	
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCoustry);
	$forwardCitiesArr=$Excel_export_import->getForwaderWarehousesCity($idForwarder,$idCoustry);
	?>
	<div class="fl-15 s-field">
			<span class="f-size-12"><?=t($t_base.'fields/city')?></span>
			<select name="dataExportArr[szCity]" id="szCity" onchange="showForwarderWarehouseOBO('<?=$idForwarder?>',this.value,'city','warehouse')">
						<option value=""><?=t($t_base.'fields/all');?></option>
						<?php
							if(!empty($forwardCitiesArr))
							{
								foreach($forwardCitiesArr as $forwardCitiesArrs)
								{
									?>
									<option value="<?=$forwardCitiesArrs['szCity']?>"><?=$forwardCitiesArrs['szCity']?></option>
									<?
								}
							}
							
						
						?>
					</select>
		</div>
		<div class="fl-15 s-field" id="warehouse">
			<span class="f-size-12"><?=t($t_base.'fields/cfs_name')?></span>
			<select name="dataExportArr[haulageWarehouse]" id="haulageWarehouse" onchange="enableHaulageGetData('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>')">
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($forwardWareHouseArr))
					{
						foreach($forwardWareHouseArr as $forwardWareHouseArrs)
						{
							?>
							<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
							<?
						}
					}
				?>
			</select>
		</div>
<?php
}
if($_REQUEST['flag']=='Warehouse')
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCoustry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCoustry,$szCity);
	
	?>	
		
	<span class="f-size-12"><?=t($t_base.'fields/cfs_name')?></span>
	<select name="dataExportArr[haulageWarehouse]" id="haulageWarehouse" onchange="enableHaulageGetData('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>')">
		<option value=""><?=t($t_base.'fields/select');?></option>
	<?php
		if(!empty($forwardWareHouseArr))
		{
			foreach($forwardWareHouseArr as $forwardWareHouseArrs)
			{
				?>
				<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
				<?php
			}
		}
	?>
	</select>
		
<?php
}
if($_REQUEST['flag']=='haulageAddOBO')
{
//print_r($_REQUEST);
	$idForwarder=$_REQUEST['idForwarder'];
	$originCountry=$_REQUEST['haulageCountry'];
	$haulageWarehouse=$_REQUEST['haulageWarehouse'];
	$szCity=$_REQUEST['szCity'];
	$idDirection=$_REQUEST['idDirection'];
	if($idDirection=='1')
	{
		$message=t($t_base.'messages/export');
		$message1=t($t_base.'messages/to');
	}
	else
	{
		$message=t($t_base.'messages/import');
		$message1=t($t_base.'messages/from');
	}
	$forwardWareHouseHaulageArr=$Excel_export_import->haulageOBOData($idForwarder,$originCountry,$idDirection,$haulageWarehouse,$szCity);
	
	if($originCountry!='' && empty($forwardWareHouseHaulageArr) && $idForwarder!='')
	{?>
		
		<div id="obo_cc_no_record_found" style="display:block;">
			<ul id="message_ul" style="padding:15px 5px 15px 35px;">
				<li><?=t($t_base.'messages/no')?> <?=$message?> <?=t($t_base.'messages/haulage_data_is_empty')?></li>
			</ul>
		</div>	
	<?php
	}
	if((int)$haulageWarehouse>0)
	{
		$kWHSSearch=new cWHSSearch();
		$kWHSSearch->load($haulageWarehouse);
		$warehouseName=$kWHSSearch->szWareHouseName;
	}
		if((int)$haulageWarehouse>0)
		{
	?>
		<h5><strong><?=$message?> <?=t($t_base.'messages/haulage_to_and_from_warehouse_to')?> <?=$message1?> <?=$warehouseName?></strong></h5>
	<?php	}else { ?>
		<h5><strong><?=t($t_base.'title/haulage_to_and_from_warehouse')?></strong></h5>
		<? }?>
		<table cellspacing="0" cellpadding="0" border="0" class="format-2 haulage_obo" width="100%">
		<tr>
			<td rowspan="2" valign="top" class="haulage_col1"><strong><?=t($t_base.'fields/distance_up_to')?></strong></td>
			<td colspan="4" valign="top"><strong><?=t($t_base.'fields/haulage_rate')?></strong></td>
			<td rowspan="2" valign="top" class="haulage_col3"><strong><?=t($t_base.'fields/cross_border')?></strong></td>
			<td rowspan="2" valign="top" class="haulage_col4"><strong><?=t($t_base.'fields/transit_time')?></strong></td>
			<td rowspan="2" valign="top" class="haulage_col5"><strong><?=t($t_base.'fields/expiry_date')?></strong></td>
		</tr>
		<tr>
			<td valign="top" class="haulage_col2_1"><strong><?=t($t_base.'fields/per_wm')?></strong></td>
			<td valign="top" class="haulage_col2_2"><strong><?=t($t_base.'fields/minimum')?></strong></td>
			<td valign="top" class="haulage_col2_3"><strong><?=t($t_base.'fields/per_booking')?></strong></td>
			<td valign="top"  class="haulage_col2_4"><strong><?=t($t_base.'fields/currency')?></strong></td>
		</tr>
	<?php	
	if(!empty($forwardWareHouseHaulageArr))
	{
		$i=1;
		foreach($forwardWareHouseHaulageArr as $forwardWareHouseHaulageArrs)
		{
			
		?>
			<tr id="haulage_data_<?=$i?>" onclick="edit_delete_haulage_data('<?=$forwardWareHouseHaulageArrs['id']?>','haulage_data_<?=$i?>','<?=$idForwarder?>','<?=t($t_base.'fields/save')?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>')">
				<td><?=number_format($forwardWareHouseHaulageArrs['iUpToKM'])?> <?=t($t_base.'fields/km')?></td>
				<td><?=$forwardWareHouseHaulageArrs['fRateWM_Km']?></td>
				<td><?=$forwardWareHouseHaulageArrs['fMinRateWM_Km']?></td>
				<td><?=$forwardWareHouseHaulageArrs['fRate']?></td>
				<td><?=$forwardWareHouseHaulageArrs['szCurrency']?></td>
				<td><? 
				if((int)$forwardWareHouseHaulageArrs['iCrossBorder']=='1')
				{
					echo t($t_base.'fields/yes');
				}
				else
				{
					echo t($t_base.'fields/no');
				}
				?></td>
				<td><? 
				if((int)$forwardWareHouseHaulageArrs['iHours']>24)
				{
					echo "< ".($forwardWareHouseHaulageArrs['iHours']/24)." ".t($t_base.'fields/days');
				}
				else
				{
					echo "< ".$forwardWareHouseHaulageArrs['iHours']." ".t($t_base.'fields/hours');
				}
				?></td>
				<td><?=date('d/m/Y',strtotime($forwardWareHouseHaulageArrs['dtExpiry']))?></td>
			</tr>
		<?php
			++$i;
		}
	}
	else
	{?>
	
		<tr id="haulage_data_1">
			<td>&nbsp;
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			
		</tr>	
		
	<?php }
	?>
	</table>
	<?php
}
if($_REQUEST['flag']=='deleteHaulage')
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idHaulageData=$_REQUEST['idHaulageData'];
	
	if($Excel_export_import->deleteHaulageData($idHaulageData))
	{
		?>
		<script>
			getOBOWarehouseHaulageData('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');
			$("#haulage_remove_button").attr("onclick","");
			$("#haulage_edit_button").attr("onclick","");
			$("#haulage_remove_button").attr("style","opacity:0.4;");
			$("#haulage_edit_button").attr("style","opacity:0.4;");	
			$("#delete_confirm").attr("style","display:none;");
		</script>
		<?php
	}
}
if($_REQUEST['flag']=='haulageDataMsg')
{
	if(!empty($_POST['dataExportArr']))
	{
		$idhaulageWarehouseArr = array();
		$idhaulageWarehouseOldArr=explode(";",$_POST['dataExportArr']['idhaulageWarehouse']);
		if(!empty($idhaulageWarehouseOldArr))
		{
                    for($i=0;$i<count($idhaulageWarehouseOldArr);++$i)
                    {
                        $idhaulageWarehouseOldArrary=explode("_",$idhaulageWarehouseOldArr[$i]);
                        $idhaulageWarehouseArr[$i]['idWarehouse']=$idhaulageWarehouseOldArrary[0];
                        $idhaulageWarehouseArr[$i]['idPricingModel']=$idhaulageWarehouseOldArrary[2];
                    }
		}
		
		$kWHSSearch = new cWHSSearch();
		$maxrowsexcced=$kWHSSearch->getManageMentVariableByDescription('__MAX_ROWS_EXCEED__');
		$idhaulageWarehousecount=count($idhaulageWarehouseArr);
		
		if($idhaulageWarehousecount>$maxrowsexcced)
		{
			if($_POST['dataExportArr']['downloadType']=='2')
			{
				echo "ERROR||||".$idhaulageWarehousecount;
			}
			else if($_POST['dataExportArr']['downloadType']=='1')
			{
				$totalUpdatedService=$Excel_export_import->getAllUpdatedHaulageModelExists($idhaulageWarehouseArr);
				//echo $totalUpdatedService;
				if($totalUpdatedService<=$maxrowsexcced)
				{
					echo "SUCCESS||||".$totalUpdatedService;
				}
				else
				{
					echo "ERROR||||".$totalUpdatedService;
				}
			}
		}
		else
		{
			$totalUpdatedService=$Excel_export_import->getAllUpdatedHaulageModelExists($idhaulageWarehouseArr);
			if($totalUpdatedService<=$maxrowsexcced)
			{
				echo "SUCCESS||||".$totalUpdatedService;
			}
			else
			{
				echo "ERROR||||".$totalUpdatedService;
			}
		}
	}
}
else if($_POST['dataExportArr']['flag']=='CC_DATA_CHECK')
{  
    $forwardWareHouseArr = array(); 
    $Excel_export_import = new cExport_Import();
    $forwardWareHouseArr = $Excel_export_import->getForwaderWarehouses($idForwarder,0,false,false,false,true);  
    
    $idOriginWarehouseArr = array();
    $idOriginCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idOriginWarehouse']);
    
    $originLclCfsAry = array();
    $originAirportAry = array();
    
    $destinationLclCfsAry = array();
    $destinationAirportAry = array();
    
    if(!empty($idOriginCountryWarehouseArr))
    {
        for($i=0;$i<count($idOriginCountryWarehouseArr);++$i)
        {
            $idOriginCountryWarehouseArrary=explode("_",$idOriginCountryWarehouseArr[$i]);
            $idOriginWarehouse = $idOriginCountryWarehouseArrary[0]; 
            $idOriginWarehouseArr[] = $idOriginWarehouse;
            if($forwardWareHouseArr[$idOriginWarehouse]==1) //LCL CFS
            {
                $originLclCfsAry[] = $idOriginWarehouse;
            }
            else if($forwardWareHouseArr[$idOriginWarehouse]==2) //Airport Warehouse
            {
                $originAirportAry[] = $idOriginWarehouse;
            } 
        } 
    }

    $idDesCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idDesWarehouse']);
    if(!empty($idDesCountryWarehouseArr))
    {
        for($i=0;$i<count($idDesCountryWarehouseArr);++$i)
        {
            $idDesCountryWarehouseArrary=explode("_",$idDesCountryWarehouseArr[$i]); 
            $idDestinationWarehouse = $idDesCountryWarehouseArrary[0]; 
            $idDesWarehouseArr[] = $idDestinationWarehouse;
            if($forwardWareHouseArr[$idDestinationWarehouse]==1) //LCL CFS
            {
                $destinationLclCfsAry[] = $idDestinationWarehouse;
            }
            else if($forwardWareHouseArr[$idDestinationWarehouse]==2) //Airport Warehouse
            {
                $destinationAirportAry[] = $idDestinationWarehouse;
            } 
        }
    }

    $kWHSSearch = new cWHSSearch();
    $maxrowsexcced=$kWHSSearch->getManageMentVariableByDescription('__MAX_ROWS_EXCEED__'); 
      
    $iNumOriginLclCfs = count($originLclCfsAry);
    $iNumOriginAirport = count($originAirportAry);
    
    $iNumDestinationLclCfs = count($destinationLclCfsAry);
    $iNumDestinationAirport = count($destinationAirportAry);
         
    $totalRowDownloaded = (($iNumOriginLclCfs * $iNumDestinationLclCfs) + ($iNumOriginAirport * $iNumDestinationAirport));

    if($totalRowDownloaded>$maxrowsexcced)
    {
        if($_POST['dataExportArr']['downloadType']=='2')
        {
            echo "ERROR||||".$totalRowDownloaded;
        }
        else if($_POST['dataExportArr']['downloadType']=='1')
        {
            $totalUpdatedService=$Excel_export_import->getAllUpdatedServicesCC($idOriginWarehouseArr,$idDesWarehouseArr);
            //echo $totalUpdatedService;
            if($totalUpdatedService<=$maxrowsexcced)
            {
                echo "SUCCESS||||".$totalUpdatedService;
            }
            else
            {
                echo "ERROR||||".$totalUpdatedService;
            }
        }
    }
    else
    {
        $totalUpdatedService=$Excel_export_import->getAllUpdatedServicesCC($idOriginWarehouseArr,$idDesWarehouseArr); 
        if($totalUpdatedService<=$maxrowsexcced)
        {
            echo "SUCCESS||||".$totalUpdatedService;
        }
        else
        {
            echo "ERROR||||".$totalUpdatedService;
        }
    } 
}
else
{
    if(!empty($_POST['dataExportArr']))
    {  
        $iWarehouseType = (int)$_POST['dataExportArr']['iWarehouseType']; 
        
        $forwardWareHouseArr = array(); 
        $Excel_export_import = new cExport_Import();
        $forwardWareHouseArr = $Excel_export_import->getForwaderWarehouses($idForwarder,0,false,false,false,true);  
    
        $idOriginWarehouseArr = array();
        $idOriginCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idOriginWarehouse']);
        if(!empty($idOriginCountryWarehouseArr))
        {
            for($i=0;$i<count($idOriginCountryWarehouseArr);++$i)
            {
                $idOriginCountryWarehouseArrary=explode("_",$idOriginCountryWarehouseArr[$i]);
                $idOriginWarehouse = $idOriginCountryWarehouseArrary[0]; 
                $idOriginWarehouseArr[] = $idOriginWarehouse;
                if($forwardWareHouseArr[$idOriginWarehouse]==1) //LCL CFS
                {
                    $originLclCfsAry[] = $idOriginWarehouse;
                }
                else if($forwardWareHouseArr[$idOriginWarehouse]==2) //Airport Warehouse
                {
                    $originAirportAry[] = $idOriginWarehouse;
                }
            } 
        }

        $idDesCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idDesWarehouse']);
        if(!empty($idDesCountryWarehouseArr))
        {
            for($i=0;$i<count($idDesCountryWarehouseArr);++$i)
            {
                $idDesCountryWarehouseArrary=explode("_",$idDesCountryWarehouseArr[$i]);
                $idDestinationWarehouse = $idDesCountryWarehouseArrary[0]; 
                $idDesWarehouseArr[] = $idDestinationWarehouse;
                if($forwardWareHouseArr[$idDestinationWarehouse]==1) //LCL CFS
                {
                    $destinationLclCfsAry[] = $idDestinationWarehouse;
                }
                else if($forwardWareHouseArr[$idDestinationWarehouse]==2) //Airport Warehouse
                {
                    $destinationAirportAry[] = $idDestinationWarehouse;
                } 
            }
        } 
        
        $kWHSSearch = new cWHSSearch();
        $maxrowsexcced = $kWHSSearch->getManageMentVariableByDescription('__MAX_ROWS_EXCEED__'); 
          
        $iNumOriginLclCfs = count($originLclCfsAry);
        $iNumOriginAirport = count($originAirportAry);

        $iNumDestinationLclCfs = count($destinationLclCfsAry);
        $iNumDestinationAirport = count($destinationAirportAry);

        $totalRowDownloaded = (($iNumOriginLclCfs * $iNumDestinationLclCfs) + ($iNumOriginAirport * $iNumDestinationAirport));
        
        if($totalRowDownloaded>$maxrowsexcced)
        {
            if($_POST['dataExportArr']['downloadType']=='2')
            {
                echo "ERROR||||".$totalRowDownloaded;
            }
            else if($_POST['dataExportArr']['downloadType']=='1')
            {
                $totalUpdatedService=$Excel_export_import->getAllUpdatedServices($idOriginWarehouseArr,$idDesWarehouseArr); 
                if($totalUpdatedService<=$maxrowsexcced)
                {
                    echo "SUCCESS||||".$totalUpdatedService;
                }
                else
                {
                    echo "ERROR||||".$totalUpdatedService;
                }
            }
        }
        else
        {
            $totalUpdatedService=$Excel_export_import->getAllUpdatedServices($idOriginWarehouseArr,$idDesWarehouseArr);
            //echo $totalUpdatedService;
            if($totalUpdatedService<=$maxrowsexcced)
            {
                echo "SUCCESS||||".$totalUpdatedService;
            }
            else
            {
                echo "ERROR||||".$totalUpdatedService;
            }
        }
    }
} 
if($_REQUEST['flag']=='checkHaulageService')
{
    $haulagePricingModel=$_REQUEST['haulagePricingModel'];
    if($haulagePricingModel!='')
    {
        $haulagePricingModelArr=explode("_",$haulagePricingModel);
    }
    $haulageWarehouseArr=explode("_",$_REQUEST['haulageWarehouse']);
    $dataArr[0]['idWarehouse']=$haulageWarehouseArr[0];
    $dataArr[0]['idPricingModel']=$haulagePricingModelArr[0];
    $totalUpdatedService=$Excel_export_import->getAllUpdatedHaulageModelExists($dataArr);
    if($totalUpdatedService>0)
    {
        echo "SUCCESS||||".$totalUpdatedService;
    }
    else
    {
        noHaulageServices();
    }
} 
if($_REQUEST['flag']=='checkHaulageServiceModel')
{
    $haulagePricingModel=$_REQUEST['haulagePricingModel'];
    if(!empty($haulagePricingModel))
    {
        $ctr=0;
        for($i=1;$i<count($haulagePricingModel);++$i)
        {
            //print_r($_REQUEST['haulageWarehouse']);
            if($haulagePricingModel[$i]!='')
            {
                $haulagePricingModelArr=explode("_",$haulagePricingModel[$i]);
            }
            $haulageWarehouseArr=explode("_",$_REQUEST['haulageWarehouse']);
            $dataArr=array();
            $dataArr[0]['idWarehouse']=$haulageWarehouseArr[0];
            $dataArr[0]['idPricingModel']=$haulagePricingModelArr[0];
            //print_r($dataArr);
            $totalUpdatedService=0;
            $totalUpdatedService=$Excel_export_import->getAllUpdatedHaulageModelExists($dataArr);
            //echo $totalUpdatedService."total";
            if($totalUpdatedService>0)
            {
                    $arrHaulageModel[]=$haulagePricingModel[$i];
                    ++$ctr;
            }
        }
        //echo $ctr;
        if((int)$ctr>0)
        {
            $arrHaulageModelStr=implode(";",$arrHaulageModel);
            echo "SUCCESS||||".$ctr."||||".$arrHaulageModelStr;
        }
        else
        {
            noHaulageServices();
        }
    }
} 
if($_REQUEST['flag']=='update_haulage_pricing_model')
{
    $idForwarder=$_REQUEST['idForwarder'];
    $idWarehouse=$_REQUEST['idWarehouse'];
    $countryArr=explode("_",$_REQUEST['idCountry']);
    $pricingModelArr=$kHaulagePricing->getAllModelPricing();
    $forwardWareHouseHaulageModelArr=array();
    $forwardWareHouseHaulageModelArr=$Excel_export_import->getForwaderWarehousesPricingModel($idForwarder,$countryArr[0],$idWarehouse,'pricingModel');
    
    $kWhsSearch = new cWHSSearch();
    $kWhsSearch->load($idWarehouse);
    $iWarehouseType = $kWhsSearch->iWarehouseType;
     
    ?>
    <span class="f-size-12"><?=t($t_base.'fields/pricing_model');?></span><br/>
    <select name="dataExportArr[haulagePricingModel]" id="haulagePricingModel">
        <option value=""><?=t($t_base.'fields/all');?></option>
        <?php
            if(!empty($pricingModelArr))
            {
                foreach($pricingModelArr as $pricingModelArrs)
                {
                    if(!empty($forwardWareHouseHaulageModelArr) && in_array($pricingModelArrs['id'],$forwardWareHouseHaulageModelArr))
                    {
                        if($pricingModelArrs['id']==4)
                        {
                            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                            {  
                                $szModelLongName = __DISTANCE_MODEL_LONG_NAME_AIR__;
                            }
                            else
                            {
                                $szModelLongName = __DISTANCE_MODEL_LONG_NAME_CFS__;
                            } 
                        }
                        else
                        {
                            $szModelLongName = $pricingModelArrs['szModelLongName'];
                        } 
                        ?>
                        <option value="<?=$pricingModelArrs['id']?>_<?=$szModelLongName?>"><?=$pricingModelArrs['szModelLongName'];?></option>
                        <?php
                    }
                }
            }
        ?>
    </select>
<?php
}
if($_REQUEST['flag']=='checkHaulageServiceWarehouse')
{
    $haulagePricingModel=$_REQUEST['haulagePricingModel'];
    $haulageWarehouse=$_REQUEST['haulageWarehouse'];
	$ctr=0;
    //print_r($haulageWarehouse);
    if(!empty($haulageWarehouse))
    {
        $ctr=0;
        //echo count($haulageWarehouse)."count";
        for($i=1;$i<count($haulageWarehouse);++$i)
        {
            //echo  $i."value";
            //print_r($haulageWarehouse[$i]);
            $haulageWarehouseArr=array();
            if($haulageWarehouse[$i]!='')
            {
                $haulageWarehouseArr=explode("_",$haulageWarehouse[$i]);
            }
            //print_r($haulageWarehouseArr);
            $haulagePricingModelArr=explode("_",$haulagePricingModel);
            $dataArr=array();
            //echo $haulageWarehouseArr[0]."idWarehouse<br/>";
            $iWarehouseType=0;
            if((int)$haulageWarehouseArr[0]>0)
            {
                $kWhsSearch = new cWHSSearch();
                $kWhsSearch->load($haulageWarehouseArr[0]);
                $iWarehouseType = $kWhsSearch->iWarehouseType;
            }
            $dataArr[0]['idWarehouse']=$haulageWarehouseArr[0];
            $dataArr[0]['idPricingModel']=$haulagePricingModelArr[0];
            //print_r($dataArr);
            $totalUpdatedService=0;
            $totalUpdatedService=$Excel_export_import->getAllUpdatedHaulageModelExists($dataArr);
            //echo $totalUpdatedService."total";
            if($totalUpdatedService>0)
            {
                $arrHaulageModel[$ctr]=$haulageWarehouse[$i];
                if($iWarehouseType>0){
                    $arrHaulageTypeModel[$ctr]=$iWarehouseType;
                }
                ++$ctr;
            }
        }
        
        //echo $ctr;
        if((int)$ctr>0)
        {
            $arrHaulageModelStr=implode(";",$arrHaulageModel);
            $arrHaulageModelTypeStr=implode(";",$arrHaulageTypeModel);
            echo "SUCCESS||||".$ctr."||||".$arrHaulageModelStr."||||".$arrHaulageModelTypeStr;
        }
        else
        {
            noHaulageServices();
        }
    }
}
if($_REQUEST['flag']=='HaulageAddWarehouseAll')
{
    $idCountry=0;
    if($_REQUEST['haulageCountry']!="")
    {
        $countryArr=explode("_",$_REQUEST['haulageCountry']);
        $idCountry=$countryArr[0];
    }
    $haulageWarehouseAdded=$_REQUEST['haulageWarehouseAdded'];
    $idhaulageWarehouse=$_REQUEST['idhaulageWarehouse'];
    //print_r($idhaulageWarehouse);
    $idhaulageWarehouseArr=array();
    if(!empty($idhaulageWarehouse))
    {
        $idhaulageWarehouseArr=explode(";",$idhaulageWarehouse);
    }
    //print_r($idhaulageWarehouse);
    //$idForwarder=1;
    $forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry);
    //print_r($forwardWareHouseArr);
    $pricingModelArr=$kHaulagePricing->getAllModelPricing();
    if(!empty($forwardWareHouseArr))
    { 
        $k=$haulageWarehouseAdded;
        if(!empty($idhaulageWarehouse))
        {
            $w_c_value=$idhaulageWarehouse;
        }
        else
        {
            $w_c_value="";
        }
        foreach($forwardWareHouseArr as $forwardWareHouseArrs)
        {
			
            foreach($pricingModelArr as $pricingModelArrs)
            {
                $dataArr=array();
                $dataArr[0]['idWarehouse']=$forwardWareHouseArrs['id'];
                $dataArr[0]['idPricingModel']=$pricingModelArrs['id'];
                $dataArr[0]['iWarehouseType']=$forwardWareHouseArrs['iWarehouseType'];
                //print_r($dataArr);
                $totalUpdatedService=0;
                $totalUpdatedService=$Excel_export_import->getAllUpdatedHaulageModelExists($dataArr);
                if((int)$totalUpdatedService>0)
                {
                    $div_id="haulage_data_".$k;
                    $w_id=$forwardWareHouseArrs['id']."_".$forwardWareHouseArrs['idCountry']."_".$pricingModelArrs['id'];

                    if(!in_array($w_id,$idhaulageWarehouseArr))
                    {
                        if($w_c_value=="")
                        {
                            $w_c_value .=$w_id;
                        }
                        else
                        {
                            $w_c_value .=";".$w_id;
                        }
                        
                        if($pricingModelArrs['id']==4)
                        {
                            if($forwardWareHouseArrs['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                            {  
                                $szModelLongName = __DISTANCE_MODEL_LONG_NAME_AIR__;
                            }
                            else
                            {
                                $szModelLongName = __DISTANCE_MODEL_LONG_NAME_CFS__;
                            } 
                        }
                        else
                        {
                            $szModelLongName = $pricingModelArrs['szModelLongName'];
                        }
                ?>
                    <tr id="haulage_data_<?=$k?>" onclick="delete_ware_haulage('<?=$w_id?>','<?=$div_id?>','haulage')">
                        <th valign="top">
                        <?=$forwardWareHouseArrs['szCountryName']?>
                        </th>
                        <td valign="top">
                        <?=$forwardWareHouseArrs['szWareHouseName']?>
                        </td>
                        <td valign="top">
                        <?=$szModelLongName?>
                        </td>
                    </tr>
							<?php
							++$k;
						}
					}
				}
			}?>
			<tr id="haulage_data_<?=$k?>" onclick="">
			</tr>
			<script>
                            $("#idhaulageWarehouse").attr('value','<?=$w_c_value?>');
                            $("#haulageWarehouseAdded").attr('value','<?=$k?>');
			</script>
			<?php
		}
}
?>