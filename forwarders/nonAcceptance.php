<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
$display_profile_not_completed_message=true;
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca | Update Non-Acceptance of Cargo or Customers";
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
$nonAcc=$kForwarder->nonAcceptance($idForwarder,2);
$nonAccEmail=$kForwarder->nonAcceptance($idForwarder,1);
$t_base = "BulkUpload/";
$t_base_error="Error";
?>
<div id="hsbody-2">
	<div class="hsbody-2-left account-links">
		<?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); 
		$t_base = "nonAcceptance/";
		?>
	</div>
	<div class="hsbody-2-right">
	
	<h5><strong><?=t($t_base.'title/Cargo_limitations');?></strong></h5>
	<p><?=t($t_base.'messages/message1');?></p>
	<br/>
	<p><?=t($t_base.'messages/message2');?></p>
	<br/>
	<div id="error"></div>
	<form name="addNonAcceptance" id="addNonAcceptance" style="padding: 5px 0 5px 0;">
            <input type="text" name="arrNonAcceptanceData[add]" id="arrNonAcceptanceData" size="38" id="nonAccAddData" style="width:381px;margin-bottom:5px;"/><br>
            <div style="clear: both;"></div>
            <div class="oh">
                <p align="left">	
                    <a href="javascript:void(0)" class="button1" onclick="addNonAccepptance()" id="addNonAccepptanceData"><span style="min-width:74px;"><?=t($t_base.'title/add');?></span></a>		
                </p>    
            </div>
        </form>
	<div style="clear: both;"></div>
	<form name ="removeNonAcceptanceData" id="removeFormNonAcceptance" method="post">	
		<select id="showNonAcceptanceData" name="showNonAcceptanceData[]" multiple="multiple" style="width: 388px;height: 100px;margin-bottom:5px;" onclick="select_non_acceptance('<?=$nonAcceptance['id']; ?>','selectGoods<?=$nonAcceptance['id']; ?>','showNonAcceptanceData')">
	
		<? if($nonAcc!='')
		{ foreach($nonAcc as $nonAcceptance){ ?>
		<option value="<?=$nonAcceptance['id']; ?>" >
		
		<? echo $nonAcceptance['szNonAcceptance']; ?></option>
		<? }} ?>
		</select>
	<div style="clear: both;"></div>	
	<div class="oh">
	<p class="fl-40">	
		<?=t($t_base.'title/remove_limitations');?>
	</p>
	<p align="left" class="fl-25">	
		<a href="javascript:void(0)" class="button1" onclick="" id="removeNonAccepptanceData" style="opacity:0.4;"><span style="min-width:74px;"><?=t($t_base.'title/remove');?></span></a>		
	</p></div>
	</form>
	<h5><strong><?=t($t_base.'title/Customers');?></strong></h5>
	<p><?=t($t_base.'messages/message3');?></p>
	<br/>
	<p><?=t($t_base.'messages/message4');?></p>
	<br/>
	<div id="errorEmail"></div>
	<form name="addNonAcceptanceEmail">
            <input type="text" name="arrNonAcceptanceEmail[add]" size="38" id="nonAccEmail" style="width:381px;margin-bottom:5px;"/><br>
        <div style="clear: both;"></div>
            <div class="oh">
                <p align="left">    
                    <a href="javascript:void(0)" class="button1" onclick="addNonAccepptanceEmail()" id="addNonAccepptanceEmail"><span style="min-width:74px;"><?=t($t_base.'title/add');?></span></a>		
                </p>    
            </div>    
        </form>
	<form name ="removeNonAcceptanceEmail" id="removeFormNonAcceptanceEmail" method="post" style="padding: 5px 0 5px 0;">	
	<select id="showNonAcceptanceEmail" name="showNonAcceptanceEmail[]" multiple="multiple" style="width: 388px;height: 100px;margin-bottom:5px;" onclick="select_non_acceptanceEmail('<?=$nonAcceptance['id']; ?>','selectEmails<?=$nonAcceptance['id']; ?>','showNonAcceptanceEmail')">
	<? if($nonAccEmail!='')
	{ foreach($nonAccEmail as $nonAcceptance){ ?>
	<option value="<?=$nonAcceptance['id']; ?>" >
	<? echo $nonAcceptance['szNonAcceptance']; ?>
	
	<? }} ?>
	</option>
	</select>
	<div style="clear: both;"></div>
	<div class="oh">
	<p class="fl-40">	
	<?=t($t_base.'title/remove_customers');?>
	</p>
	<p align="left" class="fl-25">	
	<a href="javascript:void(0)" class="button1"  id="removeNonAccepptanceEmail" style="opacity:0.4;"><span style="min-width:74px;"><?=t($t_base.'title/remove');?></span></a>		
	</p></div>
	</form>
	</div>
</div>

<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>