<?php
 ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "BulkUpload/";
$Excel_export_import=new cExport_Import();
$kWHSSearch = new cWHSSearch();

if($_REQUEST['flag']=='Warehouse_city')
{
    $idForwarder=$_REQUEST['idForwarder'];
    $idCoustry=$_REQUEST['idCountry'];

    $forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCoustry);
    $forwardCitiesArr=$Excel_export_import->getForwaderWarehousesCity($idForwarder,$idCoustry);

    if(!empty($forwardWareHouseArr))
    {	
        $countForwarder = count($forwardWareHouseArr);
        if($countForwarder == 1)
        {
            ?>
            <script type="text/javascript">
                enableHaulageGetDataNew('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');
            </script>
            <?php
        }
        else
        {
            ?>
            <script type="text/javascript">
                $("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
                $("#get_haulage_data").unbind("click");
            </script>
            <?php
        }
    }
    ?>
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader">
            <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />
        </div>			
    </div>
    <div class="fl-15 s-field">
        <span class="f-size-12"><?=t($t_base.'fields/city')?></span>
        <select name="dataExportArr[szCity]" id="szCity" onchange="showForwarderWarehouseOBONew('<?=$idForwarder?>',this.value,'city','warehouse')">
            <option value=""><?=t($t_base.'fields/all');?></option>
            <?php
                if(!empty($forwardCitiesArr))
                {	
                    $forwardCitiesCount = count($forwardCitiesArr);
                    foreach($forwardCitiesArr as $forwardCitiesArrs)
                    {
                        ?>
                        <option value="<?=$forwardCitiesArrs['szCity']?>" <? if($forwardCitiesCount==1){echo "selected=\"selected\"";}?>><?=$forwardCitiesArrs['szCity']?></option>
                        <?php
                    }
                } 
            ?>
        </select>
    </div>
    <div class="fl-15 s-field" id="warehouse">
        <span class="f-size-12">Name</span>
        <select name="dataExportArr[haulageWarehouse]" id="haulageWarehouse" onchange="enableHaulageGetDataNew('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>')">
            <option value=""><?=t($t_base.'fields/select');?></option>
            <?php
                if(!empty($forwardWareHouseArr))
                {	
                    foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                    {
                        ?>
                        <option value="<?=$forwardWareHouseArrs['id']?>" <? if($countForwarder==1){echo "selected=\"selected\"";}?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                        <?php
                    }
                }
            ?>
        </select>
    </div>
<?php
}
else if($_REQUEST['flag']=='Warehouse')
{
    $idForwarder = $_REQUEST['idForwarder'];
    $idCoustry = $_REQUEST['idCountry'];
    $szCity = $_REQUEST['szCity'];

    $forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCoustry,$szCity);
    if(!empty($forwardWareHouseArr))
    {	
        $countForwarders = count($forwardWareHouseArr);
        if($countForwarders == 1)
        {
            ?>
            <script type="text/javascript">
                enableHaulageGetDataNew('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>');
            </script>
            <?php
        }
        else
        {
            ?>
            <script type="text/javascript">
                $("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
                $("#get_haulage_data").unbind("click");
            </script>
            <?php
        }
    }
?>
    <span class="f-size-12">Name</span>
    <select name="dataExportArr[haulageWarehouse]" id="haulageWarehouse" onchange="enableHaulageGetDataNew('<?=$idForwarder?>','<?=t($t_base."fields/change_data");?>','<?=t($t_base."fields/get_data");?>')">
        <option value=""><?=t($t_base.'fields/select');?></option>
        <?php
            if(!empty($forwardWareHouseArr))
            {	
                foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                {
                    ?>
                    <option value="<?=$forwardWareHouseArrs['id']?>" <? if($countForwarders==1){echo "selected=\"selected\"";}?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                    <?php
                }
            }
        ?>
    </select>
<?php
}