<?php
ob_start();
session_start();
ini_set('max_execution_time',1200);
ini_set('MEMORY_LIMIT','512M');
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$Excel_export_import=new cExport_Import();
$kConfig = new cConfig();
if((int)$_SESSION['forwarder_user_id']==0 || empty($_POST['dataExportArr']))
{
    header('Location:'.__BASE_URL__.'/forwarders/');
    exit();	
}
if($_SESSION['forwarder_admin_id']>0)
{
    //$kForwarderContact->load($_SESSION['forwarder_admin_id']);
    $kForwarder->load($_SESSION['forwarder_id']);
}
else
{
    $kForwarderContact->load($_SESSION['forwarder_user_id']);
    $kForwarder->load($kForwarderContact->idForwarder);
}
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
$objPHPExcel=new PHPExcel();
$szDisplayName=str_replace(" ","_",$kForwarder->szDisplayName);
$szDisplayName=str_replace("/","_",$szDisplayName);
$folderName=$szDisplayName."_".$kForwarder->id;
$exportData="exportData";
if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName)) {
    mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName);
    chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName,0777);
    if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData)) {
    	 mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData);
    	 chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData,0777);
    }
}
else
{
 if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData)) {
    	 mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData);
    	 chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData,0777);
    }
}
$fh = fopen(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData."/index.php", "w+");
$file_name= __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$exportData."/Transporteca_Customs_Clearance_Upload_Sheet_".$folderName.".xlsx";
$sheetindex=0;
if(!empty($_POST['dataExportArr']))
{
    $idOriginCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idOriginWarehouse']);
    if(!empty($idOriginCountryWarehouseArr))
    {
        for($i=0;$i<count($idOriginCountryWarehouseArr);++$i)
        {
            $idOriginCountryWarehouseArrary=explode("_",$idOriginCountryWarehouseArr[$i]);
            $idOriginWarehouseArr[]=$idOriginCountryWarehouseArrary[0];
        }
    }
    sort(array_unique($idOriginWarehouseArr));

    $idDesCountryWarehouseArr=explode(";",$_POST['dataExportArr']['idDesWarehouse']);
    if(!empty($idDesCountryWarehouseArr))
    {
        for($i=0;$i<count($idDesCountryWarehouseArr);++$i)
        {
            $idDesCountryWarehouseArrary=explode("_",$idDesCountryWarehouseArr[$i]);
            $idDesWarehouseArr[]=$idDesCountryWarehouseArrary[0];
        }
    }

    sort(array_unique($idDesWarehouseArr));
    //print_r($idDesWarehouseArr);
    $idCurrency=$_POST['dataExportArr']['idCurrency'];
    $inActiveService=$_POST['dataExportArr']['inActiveService'];
    if($Excel_export_import->exportDataCustomClearance($objPHPExcel,$sheetindex,$file_name,$kConfig,$idDesWarehouseArr,$idOriginWarehouseArr,$kForwarder->id,$idCurrency,$inActiveService,$_POST['dataExportArr']['downloadType']))
    {
        //$filename= __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
        ob_clean();
        //$filename= __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca-LCL-Services-upload-sheet_1.xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //$f = fopen($file_name,'rb');
        //$fsize=filesize($file_name);
        header('Content-Disposition: attachment; filename=Transporteca_Customs_Clearance_upload_sheet_'.date('Ymd').'.xlsx');
        ob_clean();
        flush();
        readfile($file_name);
        exit; 
    }
}
?>