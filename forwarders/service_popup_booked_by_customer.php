<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");

$t_base = "home/homepage/";

?>
<div id="popup-bg"></div>
	<div id="popup-container">
	  <div class="help-select-graph-popup popup">
	  <p class="close-icon" align="right">
		<a onclick="showHide('Transportation_pop');" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<h5><?=t($t_base.'fields/grap_popup_title_booking_service');?></h5>
		<p>
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/HelpMeSelect.png">
		</p>		
		<p align="center"><a href="javascript:void(0);" onclick="showHide('Transportation_pop');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
</div>
</div>
