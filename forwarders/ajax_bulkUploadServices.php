<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$kUploadBulkService = new cUploadBulkService();
$t_base_error="Error";
$t_base="BulkUpload/";
if($_REQUEST['flag']=='show_format')
{
    showUploadFileFormat($_REQUEST['uploadServiceType']);
}
else if($_REQUEST['flag']=='delete_uploaded_file')
{
    $allFilename=$_REQUEST['allFilename'];
    $tempfilename=$_REQUEST['tempfilename'];
    $filename=$_REQUEST['filename'];
    $originFile=__UPLOAD_FORWARDER_BULK_SERVICES_TEMP__."/".$tempfilename;
    @unlink($originFile);
    $newAllFileStr='';
    if(!empty($allFilename))
    {
        $allUploadedFileArr=explode(";",$allFilename);
        if(!empty($allUploadedFileArr))
        {
            for($i=0;$i<count($allUploadedFileArr);$i++)
            {
                $newFileStr='';
                $uploadFileArr=explode("#####",$allUploadedFileArr[$i]);
                if($filename!=$uploadFileArr[1])
                {
                    echo "<span class='uploadfilelist'><span>".$uploadFileArr[0]." (".round($uploadFileArr[2])." kB)</span><a href='javascript:void(0)' onclick='delete_uploaded_image(\"".$uploadFileArr[1]."\");'><img src='".__BASE_STORE_IMAGE_URL__."/delete-icon.png'></a></span>";
					
                    $newFileStr=$allUploadedFileArr[$i];

                    if($newAllFileStr!='')
                    {
                        $newAllFileStr= $newAllFileStr.";".$newFileStr;
                    }
                    else
                    {
                        $newAllFileStr=$newFileStr;
                    }
                }
            }
        }
    }
    ?>
    <script>
        $(".upload").attr('style','position: relative; overflow: hidden; cursor: default;')
        var count=$("#filecount").attr('value');
        var newcount=parseInt(count)-1;
        $("#filecount").attr('value',newcount);
        $("#file_name").attr('value','<?=$newAllFileStr?>');
    </script>
    <?php
}
else if($_REQUEST['flag']=='submit_form')
{
        if(!empty($_POST['bulkUploadServiceArr']))
	{
		if($kUploadBulkService->uploadBulkData($_POST['bulkUploadServiceArr']))
		{
                    echo "SUCCESS||||";
?>
                   
                    <div id="success-msg">
                        <div id="popup-bg"></div>
                        <div id="popup-container">
                            <div class="popup signin-popup signin-popup-verification">
                                <!-- <p class="close-icon" align="right">
                                <a href="<?=__FORWARDER_BULK_UPLOAD_SERVICES_URL__?>" >
                                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                                </a>
                                </p> -->
                                <h4><b><?=t($t_base.'messages/files_successfully_submitted')?></b></h4><br />
                                <p style="text-align:left;"><?=t($t_base.'messages/thank_you_for_submiiting_line_1');?> <?=$kUploadBulkService->FileCount?> <? if($kUploadBulkService->FileCount>1){ echo t($t_base.'fields/Files'); }else { echo t($t_base.'fields/File'); }  echo "&nbsp;".t($t_base.'messages/thank_you_for_submiiting_line_2');?></p><br />
                                <p align="center"><a href="<?=__FORWARDER_BULK_UPLOAD_SERVICES_URL__?>" class="button1"><span><?=t($t_base.'fields/ok')?></span></a> </p>	
                            </div>
                        </div>
                    </div>	
                    <!--  <script type="text/javascript">
                    window.location.href='<?=__FORWARDER_BULK_SERVICES_APPROVAL_URL__?>';
                    </script>-->
		<?php	
                    die();
		}
                else
                {	
                    echo "ERROR||||";
                    if(!empty($kUploadBulkService->arErrorMessages)){
                    ?>
                    <div id="regError" class="errorBox ">
                    <div class="header"><?=t($t_base_error.'/please_following');?></div>
                    <div id="regErrorList">
                    <ul>
                    <?php
                            foreach($kUploadBulkService->arErrorMessages as $key=>$values)
                            {
                            ?><li><?=$values?></li>
                            <?php	
                            }
                    ?>
                    </ul>
                    </div>
                    </div>
                    <?php
                    }
                    
                }
	}
        die();
}
else
{?>

	
        <script type="text/javascript">
            $(document).ready(function() {
                var settings = {
                url: "<?=__FORWARDER_HOME_PAGE_URL__?>/uploadify.php",
                method: "POST",
                allowedTypes:"jpg,jpeg,gif,png,pdf,doc,docx,xlsx,pptx",
                fileName: "myfile",
                multiple: false,
                dragDrop: false,
                uploadButtonClass: "upload file-upload-button",
                maxFileSize: 5000000,
                dragDropStr:'<?=t($t_base.'fields/no_file_selected_services');?>',
                onSuccess:function(files,data,xhr)
                { 
                    var IS_JSON = true;
                    try
                    {
                        var json = $.parseJSON(data);
                    }
                    catch(err)
                    {
                        IS_JSON = false;
                    } 
                    if(IS_JSON)
                    { 
                        var obj = JSON.parse(data); 

                        var iFileCounter = 1;
                        
                        var filecount=$("#filecount").attr('value');
                        var filecountOld=$("#filecount").attr('value');
                        
                        for (var i = 0, len = obj.length; i < len; i++)
                        { 
                            console.log(obj[i]);
                            var newfilecount = parseInt(filecount) + iFileCounter;  
                            var uploadedFileName = obj[i]['name']; 
                            var FileUrl="<?php echo __BASE_STORE_URL__?>/forwarders/forwarderBulkUploadServices/temp/"+uploadedFileName; 
                            var size  = obj[i]['size'];
                            var container_li_id = "id_"+newfilecount;
                            //var size=obj[i]['size'];
                            if(parseInt(filecount)==0)
                            {
                                $('#fileList').html('');
                            }
                            $('#fileList').append( '<span class="uploadfilelist"><span>'+uploadedFileName+' ('+size.toFixed(0)+' kB)</span><a href="javascript:void(0)" onclick="delete_uploaded_image(\''+uploadedFileName+'\');"></a>');  
                            //$('#'+container_li_id).html( '<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Page.png&temp=2&w=50&h=50'?>" border="0" /><span class="page">'+newfilecount+'</span>');

                            iFileCounter++; 

                            var filename_all=$("#file_name").val();  
                            if(filename_all!='')
                            { 
                                var newvalue=filename_all+";"+files+"#####"+uploadedFileName+"#####"+size;
                                $("#fileuploadlink").attr('style','display:block');
                                $("#file_uploade_plus_link").attr('style','display:block');
                            }
                            else
                            { 
                                var newvalue=files+"#####"+uploadedFileName+"#####"+size;
                                $("#fileuploadlink").attr('style','display:block');	
                                $("#file_uploade_plus_link").attr('style','display:block');
                            }
                        }
                        $("#filecount").attr('value',newfilecount);
                        $("#file_name").attr('value',newvalue);
                        $("#upload_process_list").attr('style','display:none');
                        $("#upload_process_list").html('');
                        
                        if(parseInt(newfilecount)==5)
                        {
                            $(".upload").attr('style','position: relative; overflow: hidden; cursor: default;display:none;');
                        }
                        

                         
                    }
                    else
                    {  
                       // $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='showHide(\"upload_process_list\")'>close</a></p>");
                    }

                },onSelect:function(s){
                    $("#upload_process_list").attr('style','display:block;top:93%;width:345px');
                    $("#upload_error_container").attr('style','display:none');
                },
                    afterUploadAll:function()
                    {
                        //alert("all images uploaded!!");
                    },
                    onError: function(files,status,errMsg)
                    {
                        //$("#status").html("<font color='red'>Upload is Failed</font>");
                       // alert('hi');
                    }
                }
                $("#upload_file").uploadFile(settings);
                
                
            });
            
        </script>
	<!--
	<link href="<?=__BASE_STORE_CSS_URL__?>/uploadify.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/swfobject.js"></script>
	<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.uploadify.v2.1.4.min.js"></script>		
	<script type="text/javascript">
	$(document).ready(function() {
  	$('#file_upload').uploadify({ 	
    'uploader'  : '<?=__FORWARDER_HOME_PAGE_URL__?>/guide/uploadify.swf',
    'script'    : '<?=__FORWARDER_HOME_PAGE_URL__?>/uploadify.php',
    'cancelImg' : '<?=__BASE_STORE_IMAGE_URL__?>/cancel.png',
    'folder'    : "<?=__UPLOAD_FORWARDER_BULK_SERVICES_TEMP__?>",
    'buttonClass' : 'uploadifyProgressBar',
    'buttonText' : 'BROWSE',
    'sizeLimit': 5*1024*1024,
    'auto'      : true,
	'height'        : 33,
	'multi': true,
	'queueSizeLimit': 5,
	'removeCompleted' : true,
	'removeTimeout' : 200,
	'successTimeout' : 30,
    'method' : 'post',
	'progressData' : 'all',
	'requeueErrors' : true,
	'fileExt': '*.jpg;*.pdf;*.png;*.doc;*.docx;*.jpeg;*.gif;*.xlsx;*.pptx',
	'formData'        : {},               
	'preventCaching'  : true,
	'onQueueFull': function(event, queueSizeLimit) {
		alert("Please don't put anymore files in me! You can upload " + queueSizeLimit + " files at once");
		return false;
	},
	 'onError': function (event, ID, fileObj, errorObj) {
            alert(errorObj.type + ' Error: ' + errorObj.info);
        },
        'onSelect': function(e, q, f) {
        	//alert('hi');	
            var validExtensions = new Array('doc','docx','pdf','jpg','png','jpeg','gif','xlsx','pptx');				
            var fileName = f.name;
            //alert(fileName);
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            //alert(fileNameExt);
            var res_ex=jQuery.inArray(fileNameExt,validExtensions);
            //alert(res_ex);
            //alert(in_array(fileNameExt,validExtensions));
            if (res_ex == -1){
               alert('.'+fileNameExt+' is not a format accepted for upload. Please use Word, Excel, PDF or image files.');				
                return false;
            }
        },
		'onComplete': function(event, ID, fileObj, response, data) {
			var result = response.split('|||||');
			var filename_all = '';
			var newvalue='';	
			if(result[0]== true)
			{
				var size=fileObj.size;
				var sizeValue=size/1024;
				//alert("Filename: " + fileObj.name + "\nSize: " + fileObj.size + "\nFilepath: " + fileObj.filePath);
				$("#nofile-selected").attr('style','display:none;');	
				$('#fileList').append( '<span class="uploadfilelist"><span>'+fileObj.name+' ('+sizeValue.toFixed(0)+' kB)</span><a href="javascript:void(0)" onclick="delete_uploaded_image(\''+fileObj.name+'\',\''+result[1]+'\');"></a></span>');
				var filename_all=$("#file_name").val();
				var filecount=$("#filecount").attr('value');
				var newfilecount=parseInt(filecount)+1;
				$("#filecount").attr('value',newfilecount);
				
				if(parseInt(newfilecount)=='5')
				{
					$("#fileuploadlink").attr('style','display:none');
				}
				
				
				if(filename_all!='')
				{
					var newvalue=filename_all+";"+fileObj.name+"|||||"+result[1]+"|||||"+sizeValue;
					$("#fileuploadlink").attr('style','display:block');
				}
				else
				{
					var newvalue=fileObj.name+"|||||"+result[1]+"|||||"+sizeValue;
					$("#fileuploadlink").attr('style','display:block');	
				}
				$("#file_name").attr('value',newvalue);
				
			}			
				// you can use here jQuery AJAX method to send info at server-side.
		} 
  });
});
</script>-->
<?php

$bulkTypeAry = array();
$bulkTypeAry['Airfreight Services'] = "Airfreight Services";
$bulkTypeAry['Airport Warehouses'] = "Airport Warehouses";
$bulkTypeAry['CFS locations'] = "CFS locations";
$bulkTypeAry['LCL Services'] = "LCL Services";
$bulkTypeAry['Haulage'] = "Haulage";
$bulkTypeAry['Customs Clearance'] = "Customs Clearance";
ksort($bulkTypeAry);

?>
<div id="error_msg" style="display:none;"></div>       
<form id="formUp" name="formUp" method="POST" enctype="multipart/form-data">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td width="70%" style="TEXT-ALIGN:JUSTIFY"><?=t($t_base.'title/title_bulk_upload_service_1')?></td>
            <td width="30%" align="right">
                <select style="width:200px;" name="bulkUploadServiceArr[bulkFileType]" id="bulkFileType" onchange="show_select_data_format_detial(this.value);">
                    <option value=""><?=t($t_base.'fields/select')?></option>
                    <?php
                        if(!empty($bulkTypeAry))
                        {
                            foreach($bulkTypeAry as $key=>$bulkTypeArys)
                            {
                                ?>
                                <option value="<?php echo $key; ?>" <?php if($_POST['bulkUploadServiceArr']['bulkFileType']==$key){?> selected <?php }?>><?php echo $bulkTypeArys; ?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </td>
        </tr>
    </table>
    <div id="show_file_upload_data_format" style="TEXT-ALIGN:LEFT;height:150px;margin-top:10px;">
        <?php 
           if(!empty($_POST['bulkUploadServiceArr']['bulkFileType']))
           {
                showUploadFileFormat($_POST['bulkUploadServiceArr']['bulkFileType']);
           }
           else
           {
            ?>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>  
        <?php } ?>
    </div> 
    <div id="msguplaud" style="margin-bottom:10px;"></div>
    <table width="100%">
        <tr>
            <td colspan="2" id="fileuploadlink">
                <div class="clearfix file-upload-button-list"> 
                    <div id="upload_process_list" style="display:none;"></div>
                    <div id="fileList" style="float: left; width: 80%; text-align: left; padding: 3px 0 0 3px;">
                        <?php 
                            if(!empty($_POST['bulkUploadServiceArr']['file_name']))
                            {
                                $allUploadedFileArr=explode(";",$_POST['bulkUploadServiceArr']['file_name']);
                                if(!empty($allUploadedFileArr))
                                {
                                    for($i=0;$i<count($allUploadedFileArr);$i++)
                                    {
                                        $uploadFileArr=explode("#####",$allUploadedFileArr[$i]); 
                                        echo "<span class='uploadfilelist'><span>".$uploadFileArr[0]." (".round($uploadFileArr[2])." kB)</span><a href='javascript:void(0)' onclick='delete_uploaded_image(\"".$uploadFileArr[1]."\");'><img src='".__BASE_STORE_IMAGE_URL__."/delete-icon.png'></a></span>";							
                                    }
                                }
                            }
                            else
                            {
                                ?>	
                                <span id="nofile-selected"><?=t($t_base.'fields/no_file_selected_services');?></span> 
                                <?php
                            }
                        ?>
                    </div>
                    
                
                    <div id="upload_file" class="upload_label">
                          <span>BROWSE</span>
                    </div>
                </div>
                <div id="upload_error_container" style="display:none;"></div>
                
            </td>
        </tr>
        <tr>
            <td colspan="2"><br/><?=t($t_base.'title/comment_title')?></td>
        </tr>
        <tr>
            <td colspan="2"><textarea style="width:716px" cols="95" rows="5" name="bulkUploadServiceArr[szComment]" id="szComment"><?=$_POST['bulkUploadServiceArr']['szComment']?></textarea></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="hidden" name="bulkUploadServiceArr[filecount]" id="filecount" value="<?=(($_POST['bulkUploadServiceArr']['filecount'])?$_POST['bulkUploadServiceArr']['filecount']:"0")?>">			
                <input type="hidden" name="bulkUploadServiceArr[file_name]" id="file_name" value="<?=$_POST['bulkUploadServiceArr']['file_name']?>">
                <a href="<?=__FORWARDER_BULK_UPLOAD_SERVICES_URL__?>" class="button2" ><span><?=t($t_base.'fields/clear')?></span></a>
                <a href="javascript:void(0)" class="button1" onclick="uploadBulkService();"><span><?=t($t_base.'fields/submit')?></span></a>
            </td>
        </tr>
    </table>
</form>
<?php }?>