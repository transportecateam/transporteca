<?php
/**
 * Forwarder My Company  
 */
 ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle='Transporteca | Pricing & Services Introduction';
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
$t_base = "BulkUpload/";
checkAuthForwarder();
$KwarehouseSearch=new cWHSSearch();
$content  = html_entity_decode($KwarehouseSearch->getManageMentVariableByDescription('__FORWARDER_PRICING_SERVICING_INTRODUCTION__'));
$content = refineDescriptionData($content);
if(!empty($_REQUEST['updatekey']))
{
	if($kForwarder->checkExpiryConfirmationKey($_REQUEST['updatekey']))
	{
		$date=$kForwarder->dtExpiryMail;
		if($kForwarder->iExpiryMailType=='1')
			$flagUpdate=false;
		else if($kForwarder->iExpiryMailType=='2')
			$flagUpdate=true;
			$counter=$kForwarder->updateForwarderLCLServices($flagUpdate,$date);
			if((int)$counter>0)
			{
				if((int)$counter==1)
				{
					$text_line=t($t_base.'title/lcl_service');
				}
				else
				{
					$text_line=t($t_base.'title/lcl_services');
				}			
			?>
				<div id="service_vification_popup">
					<div id="popup-bg"></div>
						<div id="popup-container">
							<div class="popup signin-popup signin-popup-verification" style="width:300px;">
								<h5><?=t($t_base.'title/expiry_date')?></h5>
								<p><?=t($t_base.'title/update_expiry_date_1');?> <?=$counter?> <?=$text_line?></p><br/>
								<p align="center">
									<a href="javascript:void(0);" onclick="closePopupUpdatedService('service_vification_popup');" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>
								</p>
							</div>
					</div>
				</div>			
				<?			
			}
			else
			{
				if($kForwarder->szServiceUpdatedBy!='')
				{
					$text=t($t_base.'title/update_expiry_date_2')." ".$kForwarder->szServiceUpdatedBy.".";
				}
				else
				{
					
					$text=t($t_base.'title/update_expiry_date_3');
				}
			?>
			<div id="service_vification_popup">
				<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup signin-popup signin-popup-verification" style="width:300px;">
							<h5><?=t($t_base.'title/expiry_date_already')?></h5>
							<p><?=$text?></p><br/>
							<p align="center">
								<a href="javascript:void(0);" onclick="closePopupUpdatedService('service_vification_popup');" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>
							</p>
						</div>
				</div>
			</div>			
			<?					
			}
	}
	else
	{?>
	<div id="service_vification_popup">
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification" style="width:300px;">
					<h5><?=t($t_base.'title/expiry_date_not_extend')?></h5>
					<p><?=t($t_base.'title/update_expiry_date_4')?></p><br/>
					<p align="center">
						<a href="javascript:void(0);" onclick="closePopupUpdatedService('service_vification_popup');" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
					</p>
				</div>
		</div>
	</div>			
	<?	
	}
}
?>
<div id="hsbody-2">
   <div class="hsbody-2-left account-links">
		<?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
	</div>
	<div class="hsbody-2-right link16">
		<?=$content?>	
	</div>
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
if(!empty($_REQUEST['updatekey']))
{
?>
	<script type="text/javascript">
	addPopupScrollClass('service_vification_popup');
	</script>
	<?
}
?>				