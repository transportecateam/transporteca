<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
ini_set('max_execution_time',1200);
ini_set('memory_limit', '-1');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$display_profile_not_completed_message=true;
$szMetaTitle='Transporteca | Pending Quotes';

$quote_key = $_REQUEST['idQuotes'];
if(!empty($quote_key))
{
    $keyAry = explode('__',$quote_key);
    $szQuotationKey = $keyAry[0];	
}
if((int)($_SESSION['forwarder_user_id']<=0) && ((int)$szQuotationKey>0))
{
    $_SESSION['forwarder_booking_quotation_key'] = $szQuotationKey;
}


if(empty($_SESSION['szCustomerTimeZoneGlobal']))
{
    $ret_ary = array();
    $ret_ary = getCountryCodeByIPAddress();
    if(empty($ret_ary))
    {
        $ret_ary['szCountryCode'] = 'DK';
    } 
    $szCustomerTimeZone = get_time_zone($ret_ary['szCountryCode'],$ret_ary['szRegion']);
    $_SESSION['szCustomerTimeZoneGlobal'] = $szCustomerTimeZone ;
}

require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
//UNSETTING SESSION OF COMPLETED DATA IN CFS BULK DATA
unsetContent();
$maxCountRowError=false;
$Excel_export_import=new cExport_Import();
	$kConfig = new cConfig();
$t_base = "BulkUpload/";
$t_base_error="Error";
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

$objPHPExcel=new PHPExcel();
if((int)$_SESSION['forwarder_user_id']==0)
{
    header('Location:'.__BASE_URL__.'/');
    exit();	
}
 

?>   
<div id="hsbody">

</div> 

<?php  
    include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>	