<? 
session_start();
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
//print_r($_POST);
if(isset($_POST['createPDF']))
{
$create=$_POST['createPDF'];
//echo $create['idBilling'];
$idBilling=sanitize_all_html_input(trim($create['idBilling']));
$file=getInvoiceConfirmationPdfFile($idBilling,1);
//print_r($file);
if(!empty($file))
	{			
	header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header("Content-Type: application/force-download");
    header('Content-Disposition: attachment; filename=' . urlencode(basename($file)));
    // header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
    unlink($file);
    exit;
	
	
		
	}

}
?>