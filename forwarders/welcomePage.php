<?php
ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
$szMetaTitle="Transporteca | Welcome Page";
$display_profile_not_completed_message=false;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();

//echo $_SESSION['forwarder_user_id'];
$idForwarder = $_SESSION['forwarder_id'] ;
if($kForwarder->isProfileInfoComplete($_SESSION['forwarder_user_id']))
{
	?>
	<script type="text/javascript">
		open_edit_profile_popup();
	</script>
	<?php
}
else if($kForwarder->checkForwarderComleteCompanyInfo($idForwarder))
{
	$t_base_header = "home/";
	?>
	<div id="leave_page_div_prefernces" style="display:block;">	
	<?	echo incomplete_comapny_details_popup($t_base_header);	?>
	</div>	
    <?
 }
?>
<div id="forwarder_company_div" style="display:none;">
</div>
<div id="hsbody">
<?	function dateView()
	{
		 $now = time(); // or your date as well
	     $your_date = strtotime("2012-10-01");
	     $datediff =$your_date-  $now ;
	     $date = abs(ceil($datediff/(60*60*24)));
	     return $date;
	}
?>
<h3>Transporteca is live!</h3> 	

Customers looking for LCL services online can now place bookings directly with you on www.transporteca.com. We already have more than 17,000 registered companies in Europe alone looking forward to this!
<br/><br/>
In order to start receiving bookings, you need to agree to Transporteca's Standard Terms & Conditions found <a href="http://devforwarder.transporteca.com/T&amp;C/">here</a>, and publish your services through this Transporteca Control Panel in three simple steps:
<br/><br/>

<span class="welcome">1.	Update your standard company information <a href="<?=__BASE_URL__?>/welcomeVideo.php" onclick="open_welcome_video();" target="google_map_target_1">Show me how</a></span><br/>
<span class="welcome">2.	Setup your CFS locations <a  href="<?=__BASE_URL__?>/welcomeVideo.php?type=cfs" onclick="open_welcome_video();" target="google_map_target_1">Show me how</a></span><br/>
<span class="welcome">3.	Submit your services and rates <a href="<?=__BASE_URL__?>/welcomeVideo.php?type=service_rate" onclick="open_welcome_video();" target="google_map_target_1">Show me how</a></span><br/>

<br/>
Once this is done, you can test the rate calculations and see what your services will look like in the eyes of the customer <a href="<?=__BASE_URL__?>/welcomeVideo.php?type=customer" onclick="open_welcome_video();" target="google_map_target_1">Show me how.</a>
<br/><br/>
<h3>Navigation</h3>

Your Transporteca Control Panel is structured in six sections, always found at the top of your screen. Two sections, Company and Pricing & Services, are prerequisites for setting up and controlling your online sales. The remaining four sections are only relevant when you have received your first booking and will be available and populated by then.
<br/><br/>
<h3>Company section</h3>

Firstly, you are requested to setup your company profile. You or your administrator does that by updating the Company section of your control panel. You will notice that this section is located at the end of the menu, because once it is updated, it is not likely to change very often.
<br/><br/>
<img class="image" style="border-left: 1px solid #C4BDA1;border-top: 1px solid #C4BDA1;" src="<?=__BASE_STORE_IMAGE_URL__?>/welcome/Menu_Company.png">
<br/> 

The section is navigated from the menu on the left side of the screen. When you click on a heading, you will notice a black dot indicating your current location. Under the first part, you can establish and maintain access for different roles in the organisation. This is useful if you need to grant a team of people access to for example updating pricing or reviewing your bookings and invoices.
 <br/><br/>
 <img class="image" style="border-left: 1px solid #C4BDA1;border-top: 1px solid #C4BDA1;" src="<?=__BASE_STORE_IMAGE_URL__?>/welcome/Menu_Company_sub.png">
 <br/>
 <br/>
The other three parts in the Company section are for you to control the fundamental behaviour of Transporteca, and are all mandatory to complete in order to start selling your services online.
<br/><br />
<h3>Pricing & Services section</h3>

Once the Company section is completed, you can start uploading and testing LCL services. You do this from the Pricing & Services section.
<br/><br/>
 <img class="image" style="border-left: 1px solid #C4BDA1;border-top: 1px solid #C4BDA1;" src="<?=__BASE_STORE_IMAGE_URL__?>/welcome/Menu_Pricing&Services.png">
<br/> 
<br/>
In this section, you will find nine screens grouped under four convenient headings. Again, a black dot will help you keep track of which screen you are currently on, and the screens listed in the top of this menu are the ones you are likely to use most often, once the initial setup is complete.
<br/> <br/>
<img class="image" style="border-left: 1px solid #C4BDA1;border-top: 1px solid #C4BDA1;" src="<?=__BASE_STORE_IMAGE_URL__?>/welcome/Menu_Pricing_sub.png">
<br/>
As described in detail in the Forwarder guide to Transporteca, your services and pricing comprise three components:
<br/><br/>
<span class="welcome">1. LCL Service, which cover CFS to CFS</span>
<br/>
<span class="welcome">2. Haulage, which is an add-on to LCL Services and can be export or import haulage, and</span>
<br/>
<span class="welcome">3. Customs Clearance, at either origin or destination.</span>
<br/><br/>


For each of these components, you have a choice to upload them either in bulk from an Excel file, or line by line directly in your Control Panel.
<br/><br/>
Before that, you will, however, need to complete the pre-requisites. Initially, you will need to update the list of CFS locations from which you and your agents operate. This is updated in the "CFS locations" screen. In the "Non acceptance" screen you can submit any limitations, such as restricted parties or commodities. It is recommended to wait completing that screen till you are acquainted with the rest of the Pricing & Services section.
<br/><br/>
Once your CFS locations have been updated, you can start updating your services and prices. First time you do that, it is recommended to do it in bulk, as that will enable updating of all trades at once. When navigating to "LCL Services" under "Upload in Bulk", you will find that you can download an Excel spread sheet with all the different service combinations, complete the details and conveniently re-upload the data.
<br/><br/>
If you offer export or import haulage in certain geographies, or customs clearance, you can upload your services in a similar fashion.
<br/><br/>
<h3>Try it out</h3>
When you have completed your upload of services and rates, you can test what it will look like for a customer on Transporteca. You do this from the "Try it out" screen, which is simply a snapshot of the booking screen from www.transporteca.com. An additional feature included for your convenience is that you in this screen can click on any rate Transporteca has calculated on your behalf, and get the complete breakdown of the calculation. This is obviously a feature not available to customers.
<br/><br/>
<h3>Help</h3>
Throughout the sections you will find comments in blue boxes appearing either by themselves, where relevant, or when you put the cursor on one of the question marks. In addition, you will find a number of useful videos in Pricing & Services demonstrating how to upload and maintain services and rates.
<br/><br/>
If you have any questions, please do not hesitate to contact us - we strive to respond to all questions within a few hours.
			
</div>	
<div id="popup_container_google_map" style="display:none;">
		<div id="popup-bg"></div>
		<div id="popup-container">
			<iframe id="google_map_target_1" style="height:620px;width:750px;" align="center" valign="middle" class="google_map_select popup"  name="google_map_target_1" src="#" >
			</iframe>
	   </div>
</div>
<?
	require_once( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>			