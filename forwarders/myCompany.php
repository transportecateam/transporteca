<?php
/**
 * Forwarder My Company  
 */
 ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle='Transporteca | Setup Users and Access rights';
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();
$t_base = "ForwardersCompany/UserAccess/";
$kForwardersContact = new cForwarderContact();

$forwarderContactAry = array();
$idForwarder = $_SESSION['forwarder_id'];
$idRoleAry = array();
$idRoleAry[0]=1 ;
$forwarderContactAry = $kForwardersContact->getAllForwardersContact($idForwarder,$idRoleAry);
?>
		
		<div id="hsbody-2">
		<div id="forwarder_company_div" style="display:none;"></div>
   		<?php require_once( __APP_PATH_LAYOUT__ ."/forwarder_company_nav.php" ); ?> 
			<div class="hsbody-2-right">
				<h4><strong><?=t($t_base.'titles/admin_profile');?></strong></h4>
				<p><?=t($t_base.'titles/admin_pref_text');?></p>
				<br>	
				<div class="oh">
				<?php
					if(!empty($forwarderContactAry))
					{
						$ctr=0;
						foreach($forwarderContactAry as $forwarderContactArys)
						{
							$name = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
							?>
							<div class="profile-box">
								<p class="title"><?=strlen($name)<=17?$name:substr($name,0,17).'..'?></p>
								<p class="email"><span title="E-mail address">&nbsp;</span><?=strlen($forwarderContactArys['szEmail'])<=17?$forwarderContactArys['szEmail']:substr($forwarderContactArys['szEmail'],0,15).".."?></p>
								<p class="date-setting">
								    <span class="padlock" title="Last login">&nbsp;</span>
									<span class="date"  title="Last login"><?
										if($forwarderContactArys['iConfirmed']>0)
										{
                                                                                    if(!empty($forwarderContactArys['dtLastLogin']) && ($forwarderContactArys['dtLastLogin']!='0000-00-00 00:00:00'))
                                                                                    {
                                                                                        echo date('d/m/Y',strtotime($forwarderContactArys['dtLastLogin']));
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        echo 'N/A';
                                                                                    }
										}
										else
										{
											echo 'Pending';
										}
									
									?></span>
									<?
									if($_SESSION['forwarder_user_id'] != $forwarderContactArys['id']){?>
									<a href="javascript:void(0)"  title="Delete profile" alt="Delete profile" onclick="delete_forwarder_contact('<?=$forwarderContactArys['id']?>')" class="delete">&nbsp;</a><a href="javascript:void(0)" alt="Edit profile" title="Edit profile" onclick="update_forwarder_user('<?=$forwarderContactArys['id']?>');" class="settings">&nbsp;</a>
									<? } ?>
								</p>
							</div>
							<?
							$ctr++;
							if($ctr%4==0)
							{
								echo '</div><div class="oh">';
							}
						}
					}
				?>
					<div class="profile-box-link">
						<p><br /><a href="javascript:void(0)" onclick="add_new_user(1)"><?=t($t_base.'titles/add_new_admin_profile');?></a></p>
					</div>
				</div>
				
				<br />
				
				<h4><strong><?=t($t_base.'titles/pricing_services');?></strong></h4>
				<p><?=t($t_base.'titles/pricing_services_msg');?></p>
				<br>
				<div class="oh">
				<?
					$idRoleAry = array();
					$idRoleAry[0]=3 ;
					$forwarderContactAry = $kForwardersContact->getAllForwardersContact($idForwarder,$idRoleAry);
					if(!empty($forwarderContactAry))
					{
						$ctr=0;
						foreach($forwarderContactAry as $forwarderContactArys)
						{
							$name = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
							?>
							<div class="profile-box" id="forwarder_contact_<?=$forwarderContactArys['id']?>">
								<p class="title"><?=strlen($name)<=17?$name:substr($name,0,17).'..'?></p>
								<p class="email" alt="E-mail address"><span title="E-mail address">&nbsp;</span><?=strlen($forwarderContactArys['szEmail'])<=17?$forwarderContactArys['szEmail']:substr($forwarderContactArys['szEmail'],0,15).".."?></p>
								<p class="date-setting">
									<span class="padlock" title="Last login">&nbsp;</span>
									<span class="date" alt="Last login"><?
										if($forwarderContactArys['iConfirmed']>0)
										{
											if(!empty($forwarderContactArys['dtLastLogin']) && ($forwarderContactArys['dtLastLogin']!='0000-00-00 00:00:00'))
											{
												echo date('d/m/Y',strtotime($forwarderContactArys['dtLastLogin']));
											}
											else
											{
												echo 'N/A';
											}
										}
										else
										{
											echo 'Pending';
										}
									
									?></span>
									<a href="javascript:void(0)" title="Delete profile" alt="Delete profile" onclick="delete_forwarder_contact('<?=$forwarderContactArys['id']?>');" class="delete">&nbsp;</a>
									<a href="javascript:void(0)" title="Edit profile" alt="Edit profile" onclick="update_forwarder_user('<?=$forwarderContactArys['id']?>');" class="settings">&nbsp;</a>
								</p>
							</div>
							<?
							$ctr++;
							if($ctr%4==0)
							{
								echo '</div><div class="oh">';
							}
						}
					}
				?>
					<div class="profile-box-link">
						<p><br /><a href="javascript:void(0)" onclick="add_new_user(3)" ><?=t($t_base.'titles/add_new_pricing_services');?></a></p>
					</div>
				</div>
				<br />
				<h4><strong><?=t($t_base.'titles/booking_billing_profile');?></strong></h4>	
				<p><?=t($t_base.'titles/booking_billing_profile_msg');?></p>
				<br>
				<div class="oh">
					<?
					$idRoleAry = array();
					$idRoleAry[0]=5 ;
					$forwarderContactAry = $kForwardersContact->getAllForwardersContact($idForwarder,$idRoleAry);
					if(!empty($forwarderContactAry))
					{
						$ctr=0;
						foreach($forwarderContactAry as $forwarderContactArys)
						{
							$name = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
							?>
							<div class="profile-box" id="forwarder_contact_<?=$forwarderContactArys['id']?>">
								<p class="title"><?=strlen($name)<=17?$name:substr($name,0,17).'..'?></p>
								<p class="email" alt="E-mail address"><span title="E-mail address">&nbsp;</span><?=strlen($forwarderContactArys['szEmail'])<=15?$forwarderContactArys['szEmail']:substr($forwarderContactArys['szEmail'],0,13).".."?></p>
								<p class="date-setting">
									<span class="padlock" title="Last login">&nbsp;</span>
									<span class="date" alt="Last login"><?
										if($forwarderContactArys['iConfirmed']>0)
										{
											if(!empty($forwarderContactArys['dtLastLogin']) && ($forwarderContactArys['dtLastLogin']!='0000-00-00 00:00:00'))
											{
												echo date('d/m/Y',strtotime($forwarderContactArys['dtLastLogin']));
											}
											else
											{
												echo 'N/A';
											}
										}
										else
										{
											echo 'Pending';
										}
									?></span>
									<a href="javascript:void(0)" title="Delete profile" alt="delete profile" onclick="delete_forwarder_contact('<?=$forwarderContactArys['id']?>')" class="delete">&nbsp;</a>
									<a href="javascript:void(0)" title="Edit profile" alt="Edit profile" onclick="update_forwarder_user('<?=$forwarderContactArys['id']?>');"  class="settings">&nbsp;</a>
								</p>
							</div>
							<?
							$ctr++;
							if($ctr%4==0)
							{
								echo '</div><div class="oh">';
							}
						}
					}
				?>
					<div class="profile-box-link">
						<p><br /><a href="javascript:void(0)" onclick="add_new_user(5)"><?=t($t_base.'titles/add_new_booking_billing_profile');?></a></p>
					</div>
				</div>
				
			</div>
		</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>		