<?php
/**
 * dashboard Details
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "Dashboard/";
checkAuthForwarder_ajax();

if(isset($_POST))
{
	$mode = sanitize_all_html_input($_POST['mode']);
	if($mode == 'LIST_LCL_SERVICES')
	{
		$LCLDetails = $kForwarder->findLclServices($idForwarder);
		if($LCLDetails!= array())
		{	
		
			$szWareHouseFrom	= array();
			$szWarehouseTo	= array();
			foreach($LCLDetails as $details)
			{	
				$szWareHouseFromContent = $kForwarder->findEachLclServices($details['idWarehouseFrom']);
				$szWarehouseToContent   = $kForwarder->findEachLclServices($details['idWarehouseTo']);
				$szWareHouseFrom[]		= $szWareHouseFromContent['szCountryName'].", ".$szWareHouseFromContent['szCity'];
				$szWarehouseTo[]		= $szWarehouseToContent['szCountryName'].", ".$szWarehouseToContent['szCity'];
			}
			if(isset($_POST['sortCode']))
			{
				$i = (int)sanitize_all_html_input($_POST['sortCode']);
			}
			else
			{
				$i = 0;
			}
			//$arrWareHouseFrom = $szWareHouseFrom.$sortFrom;
			//$arrWarehouseTo   = $szWarehouseTo.$sortTo;
			SWITCH($i)
			{
				CASE 1:
					array_multisort($szWareHouseFrom,SORT_ASC,$szWarehouseTo);
					$classForm ="sort-arrow-up";
					$classTo   ="sort-arrow-up-grey";
					$sortFrom  =2;
					$sortTo	   =4;
					BREAK;
				CASE 2:
					array_multisort($szWareHouseFrom,SORT_DESC,$szWarehouseTo);
					$classForm ="sort-arrow-down";
					$classTo   ="sort-arrow-up-grey";
					$sortFrom  =1;
					$sortTo	   =4;
					BREAK;
				CASE 3:
					array_multisort($szWarehouseTo,SORT_DESC,$szWareHouseFrom);
					$classForm ="sort-arrow-up-grey";
					$classTo   ="sort-arrow-down";
					$sortFrom  =1;
					$sortTo	   =4;
					BREAK;
				CASE 4:
					array_multisort($szWarehouseTo,SORT_ASC,$szWareHouseFrom);
					$classForm ="sort-arrow-up-grey";
					$classTo   ="sort-arrow-up";
					$sortFrom  =1;
					$sortTo	   =3;
					BREAK;
				DEFAULT:
					array_multisort($szWareHouseFrom,SORT_ASC,$szWarehouseTo);
					$classForm ="sort-arrow-up";
					$classTo   ="sort-arrow-up-grey";
					$sortFrom  =2;
					$sortTo	   =4; 
					BREAK;				
			}
		?><div id="popup-bg"></div>
					<div id="popup-container" style="margin-top: 90px;">
						<div class="multi-payment-transfer-parent-popup popup" style="width: 600px;"> 
						<p class="close-icon" align="right">
						<a onclick="removeInnerContent('ajaxLogin');" href="javascript:void(0);">
						<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
						</a>
						</p>
							<h5><?=t($t_base.'title/heading1')?></h5>
								<div class="scroll-div" style="height:250px;padding: 0px;">
									<table class="format-4" style="width: 100%;border-left: medium none;border-top:none;" cellpadding="0" cellspacing="0" >
									<tr>
										<th width="50%"><?=t($t_base.'fields/origin')?><a id="from" class="<?=$classForm?>" onclick="showLCLServicesSort(<?=$idForwarder?>,<?=$sortFrom?>)">&nbsp;</a></th>
										<th width="50%" style="border-right:none;"><?=t($t_base.'fields/destination')?><a id="to" class="<?=$classTo?>" onclick="showLCLServicesSort(<?=$idForwarder?>,<?=$sortTo?>)">&nbsp;</a></th>
									</tr>
									<?
										$count = 0;				
										foreach($szWareHouseFrom as $key=>$value)
										{
											echo "<tr>
													<td>".$value."</td>
													<td style=\"border-right:none;\">".$szWarehouseTo[$count++]."</td>
											</tr>";
										}				
									?>
									</table>
								</div>
							<div style="text-align:center;">
								<a class="button1" onclick="removeInnerContent('ajaxLogin')"><span><?=t($t_base.'fields/close')?></span></a>
							</div>
						</div>
					</div>
<?					
		}
	}
}
?>
