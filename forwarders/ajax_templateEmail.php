<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_forwarderHeader.php");
checkAuthForwarder_ajax();
$t_base="management/temp_email_mgmt/";
$t_base_error="management/Error/";
$flag=sanitize_all_html_input($_REQUEST['flag']);
$kAdmin = new cAdmin();
if($flag=='SHOW_CONTENT')
{
	$id	=	sanitize_all_html_input($_REQUEST['id']);	
	$iLanguage	=	sanitize_all_html_input($_REQUEST['iLanguage']);
	$emailTemp=$kAdmin->templateEmail($id,$iLanguage);
	$oneEmailArray = call_user_func_array('array_merge', $emailTemp);
	if($emailTemp!=array())
	{ 
		//if($iLanguage==__LANGUAGE_ID_DANISH__)
		//{
			$szDescription = utf8_decode($oneEmailArray['section_description']);
			$szSubjectStr = utf8_decode($oneEmailArray['subject']);
//		}
//		else
//		{
//			$szDescription = $oneEmailArray['section_description'];
//			$szSubjectStr = $oneEmailArray['subject'];
//		}
		?>
                <script type="text/javascript">
                    NiceEditorInstance = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance("szDescription",{hasPanel : true}); 
                </script>
                
		<div style="padding-top: 20px;">
			<form id="saveEmailTemplate">
				<table cellpadding="0"  cellspacing="0" border="0"  width="100%" style="margin-bottom:20px;">
				<tr>
                                    <td style="width: 8%;"><?=t($t_base.'fields/subject')?></td>
                                    <td>
                                        <input type="text" name="templateArr[szSubject]"  id="szSubject" value="<?php echo $szSubjectStr ;?>" />
                                        <input type="hidden" name = "templateArr[id]" value="<?=$oneEmailArray['id']?>" />
                                        <input type="hidden" name = "flag" value="SAVE_TEMPLATE" />
                                        <input type="hidden" name="templateArr[iLanguage]" value="<?php echo $iLanguage; ?>" />
                                    </td>
				</tr>
                                <tr>
                                    <td style="height:8px;" colspan="2"></td>
                                </tr>
				<tr>
                                    <td valign="top"><?=t($t_base.'fields/html')?></td><td align="right"><textarea id="szDescription" name="templateArr[szDescription]" rows="15" cols="94"><?php echo $szDescription; ?></textarea></td>
				</tr>
                                    
				
				</table>
			</form>
			<div>
				<span id="previewText"></span>
			</div>
		</div>
		<?php
	}
}
if($flag=='SAVE_TEMPLATE')
{
	$kAdmin->saveTemplateEmail($_POST['templateArr']);
	if(!empty($kAdmin->arErrorMessages))
	{
		?>
		<div id="regError" class="errorBox" style="display:block;">
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kAdmin->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?php
	}
}

if($flag=='SHOW_CONTENT_MESSAGE')
{
    echo "<div>";
    echo t($t_base.'fields/subject').": ".$_POST['szSubject']; 	
    require_once(__APP_PATH_LAYOUT__.'/email_header.php');
    echo htmlspecialchars_decode(urldecode(base64_decode($_POST['message'])));
    require_once(__APP_PATH_LAYOUT__.'/email_footer.php'); 
    echo "</div>";
}
