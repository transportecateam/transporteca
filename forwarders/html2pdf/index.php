<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH__.'/forwarders/html2pdf/html2fpdf.php');
$pdf=new HTML2FPDF();
$pdf->AddPage();
$version=$kForwarder->selectVersion();
$version=date('dFY',strtotime($version['dtVersionUpdated']));
$order=$kForwarder->getMaxTncOrder();
$forwarderAgreementArr=$kForwarder->getForwarderAgreement($idForwarder);
		  $kForwarder->load($idForwarder);
		  $str='';
			$str .="<br><h4><strong>".$order.". Agreement</strong></a></h4>";
			if(!empty($forwarderAgreementArr)){
			foreach($forwarderAgreementArr as $forwarderAgreementArrs)
			{
				if($forwarderAgreementArrs['dtAgreement']!='' && $forwarderAgreementArrs['dtAgreement']!='0000-00-00 00:00:00')
				{
					$agreementDate=date('d F Y',strtotime($forwarderAgreementArrs['dtAgreement']));
					
				}
				if($forwarderAgreementArrs['iAgree']=='1')
				{
					$agreement="agree";
				}
				else if($forwarderAgreementArrs['iAgree']=='0')
				{
					$agreement="disagree";
				}
				else if($forwarderAgreementArrs['iAgree']=='2')
				{
					$agreement="changed";
				}
				if($forwarderAgreementArrs['iAgree']!='2')
				{
					$str1 = $agreementDate." - "; 
				}
				else
				{
					$str1 = '';	
				} 
				$str1 .= $forwarderAgreementArrs['iAgreementVersion']." ".$agreement." by ".$forwarderAgreementArrs['szName'];
				
				if($forwarderAgreementArrs['iAgree'] !=2)
				{
					$str1 .=" on behalf of ".$forwarderAgreementArrs['szCompanyName'];
				}
				
				$str .="<p>". $str1 ."</p>";
				$str1 = '';
			}
		}	
		$str.="	<br/></body></html>";
$version="Transporteca_TNC_Version_".$version;
$fp = fopen(__APP_PATH__."/forwarders/html2pdf/terms_condition_pdf.html","r");
$strContent = fread($fp, filesize(__APP_PATH__."/forwarders/html2pdf/terms_condition_pdf.html"));
//print_r($strContent);
fclose($fp);
$strContent=$strContent."".$str;
//$pdf->Header('TransportecaMail');
$pdf->WriteHTML($strContent);
$pdf->Output(__APP_PATH__."/forwarders/html2pdf/".$version.".pdf");
//echo "PDF file is generated successfully!";
?>