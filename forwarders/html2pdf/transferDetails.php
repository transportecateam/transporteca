<?php
ob_start();
session_start();
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
if(isset($_GET))
{
    if( !defined( "__APP_PATH__" ) )
    define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
    require_once( __APP_PATH__ . "/inc/constants.php" );
    require_once(__APP_PATH__.'/inc/functions.php');
    require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

    if($_GET['id'] && $_GET['user'])
    {
        $id=$_GET['id'];
        $user=$_GET['user'];
        $flag=$_GET['flag'];
        $flag = 'PDF';
        $filename = getForwarderTransferPDF($user,$id,$flag,true);
        
        if(!empty($filename))
        {
            download_booking_pdf_file($filename);
            die; 
        }
    }
} 

?>