<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
$display_profile_not_completed_message_uploadService = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
///$display_profile_not_completed_message=true;

$szMetaTitle='Transporteca | Submit data for Upload Service';
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
$t_base = "BulkUpload/";

if((int)$_SESSION['forwarder_user_id']==0)
{
    header('Location:'.__BASE_URL__);
    exit();	
}
?>
<div id="hsbody-2">
    <div class="hsbody-2-left account-links">
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
    </div>
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader"></div>
        <div id="popup-container">
            <div class="popup signin-popup signin-popup-verification" style="padding:10px;width:290px;">
                <p><?=t($t_base.'messages/plz_wait_while_we_process');?></p>
                <br/><br/>
                <p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
            </div>	
        </div> 			
    </div>
    <div class="hsbody-2-right">
        <p align="left"><strong><?=t($t_base.'messages/submit_data_for_upload_service');?></strong></p><br/>
        <p style="TEXT-ALIGN:left"><?=t($t_base.'messages/files_submitted_must_relate');?></p><br />
        <div id="uploadBulkService">
            <?php require_once( __APP_PATH__ ."/forwarders/ajax_bulkUploadServices.php" );?>
        </div>
    </div>
</div>	
<?php include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" ); ?>	