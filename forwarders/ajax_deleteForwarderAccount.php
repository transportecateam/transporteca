<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
checkAuthForwarder();
$t_base = "Forwarders/DeleteAccount/";
$t_base_error = "Error";
$successPopup=false;

	if(!empty($_POST['deleteAccountArr']))
	{ 
		if($kForwarderContact->checkdeleteForwarderAccount($_POST['deleteAccountArr']))
		{
			$successPopup=true;
		}
	}
	if(!empty($_POST['deleteForwarderId']))
	{
	
		if($kForwarderContact->deactivateForwarderAccount($_POST['deleteForwarderId']))
		{
			$redirect_url=__FORWARDER_HOME_PAGE_URL__;
			unset($_SESSION['forwarder_user_id']);
			unset($_SESSION['forwarder_id']);
			unset($_SESSION['forwarder_admin_id']);
		?>
			<script type="text/javascript">
			$(location).attr('href','<?=$redirect_url?>');
			</script>
		<?			
			exit();
		}
	}
		if($successPopup)
		{
		?>	<script type="text/javascript">
			addPopupScrollClass('remove_ship_con_id');
			</script>
			<div id="remove_ship_con_id">
			<div id="popup-bg"></div>
			<div id="popup-container">
			<div class="popup delete-account-popup">
			<p class="close-icon" align="right">
			<a onclick="cancel_remove_popup();" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<form name="delete_account" id="delete_account" method="post">
				<h5><?=t($t_base.'messages/delete_account');?></h5>
				<p><?=t($t_base.'messages/sure_delete_forwarder_account');?>?</p>
				<input type="hidden" name="deleteForwarderId" id="deleteForwarderId" value="<?=$kForwarderContact->id;?>">
				<br />
				<p align="center"><a href="javascript:void(0)" class="button1" onclick="delete_account_forwarder();"><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_remove_popup();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
				</form>
			</div>
			</div>
			</div>
		<?php
		}
		if(!empty($kForwarderContact->arErrorMessages)){
		?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base_error.'/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
				foreach($kForwarderContact->arErrorMessages as $key=>$values)
				{
				?><li><?=$values?></li>
				<?php	
				}
			?>
			</ul>
			</div>
			</div>
			<?php }?>
	<form name="deleteForwarderAccount" id="deleteForwarderAccount" method="post">
	<h5><strong><?=t($t_base.'title/are_yor_sure_delete_account');?></strong></h5>
						
	<p><?=t($t_base.'title/permanently_delete_your_account');?></p>
	<br />
	<br />
	<p><?=t($t_base.'title/delete_your_account');?></p><br />
	<br />
	<div class="oh">
		<p class="fl-49"><?=t($t_base.'fields/email_address_deleted');?></p>
		<p class="fl-49"><input type="text" name="deleteAccountArr[szEmail]" id="szDeleteEmail" size="28" value="<?=isset($_POST['deleteAccountArr']['szEmail'])?$_POST['deleteAccountArr']['szEmail']:'';?>"/></p>
	</div>
	<div class="oh">
		<p class="fl-49"><?=t($t_base.'fields/password');?></p>
		<p class="fl-49"><input type="password" name="deleteAccountArr[szPassword]" id="szPassword" size="28" /></p>
	</div>
	<br />
	<p align="center">
	<input type="hidden" name="deleteAccountArr[szLoginEmail]" id="szLoginEmail" size="48" value="<?=$kForwarderContact->szEmail;?>"/>
	<input type="hidden" name="deleteAccountArr[idForwarder]" id="idUser" size="48" value="<?=$kForwarderContact->id;?>"/>
	<a href="javascript:void(0)" class="button1" style="margin-left: 109px;" onclick="delete_forwarder_account();"><span><?=t($t_base.'fields/delete_account');?></span></a></p>
</form>