<?php
/**
 * Edit User Information
 */
ob_start();
if( !isset( $_SESSION ) )
{
    session_start();
} 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
$idForwarder=$_SESSION['forwarder_id'];
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/courier_functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$kCourierServices = new cCourierServices();
$mode=sanitize_all_html_input(trim($_POST['mode']));

if($mode=='ADD_COURIER_PROVIDER_AGREEMENT')
{
    addCourierProviderAgreement($kCourierServices,false,true);

}
else if($mode=='REVALIDATE_ALL_CARRIER_AGREEMENT')
{ 
    if($kCourierServices->checkCourierProviderCredentials(true))
    {
        //TO DO
    } 
}
else if($mode=='ADD_COURIER_AGREEMENT_DATA')
{
    if($kCourierServices->addCourierAgreementData($_POST['courierProviderAgreementArr'],true))
    {
        $idCourierProviderAgree=(int)$kCourierServices->idCourierProviderAgree;
        //$idCourierProviderAgree=0;	
        $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
        $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

        $courierAgreementArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree);	

        //$_POST['pricingArr']=$courierAgreementArr[0];
        //$_POST['pricingArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
        $idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
        echo "SUCCESS||||";
        echo courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree)."||||";
        echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider)."||||";
        echo addDisplayCouierAgreementTrades($kCourierServices)."||||";
        echo addDisplayCouierAgreementPricing($kCourierServices,true);
        die;
    }
    else
    {
        echo "ERROR||||";
        addCourierProviderAgreement($kCourierServices,false,true);
    }
}
else if($mode=='ADD_EDIT_COURIER_SERVICE_COVERED_DATA')
{
    if($kCourierServices->addCourierAgreementServiceData($_POST['agreeServiceCoverArr']))
    {
        $idCourierProvider=$_POST['agreeServiceCoverArr']['idCourierProvider'];
        $idCourierAgreeProvider=$_POST['agreeServiceCoverArr']['idCourierAgreeProvider'];
        unset($_POST['agreeServiceCoverArr']);
        $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
        
        $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);
        
        $data=$courierAgreeArr[0];
        $response=$kCourierServices->isCourierAgreementDataComplete($data);

        
        echo "SUCCESS||||";
        addDisplayCouierAgreementService($kCourierServices,$idCourierProvider);
        echo "||||".$response;
        die();
    }
    else
    {
        echo "ERROR||||";
        addAgreementServiceCoverdFormHtml($kCourierServices,$_POST['agreeServiceCoverArr']['idCourierProvider']);
        die();
    } 
}
else if($mode=='SELECT_AGREE_SERVICE_COVERED')
{
    $idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $id=(int)$_POST['id'];
    
    $serviceCoveredArr=$kCourierServices->getCourierAgreeServiceData($idCourierAgreeProvider,$id);	

    $_POST['pricingArr']=$serviceCoveredArr[0];
    $_POST['pricingArr']['idCourierServiceOffered']=$id;
    $_POST['pricingArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;

    $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
    echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider,$id)."||||";
    echo addDisplayCouierAgreementPricing($kCourierServices);
}
else if($mode=="EDIT_AGREE_SERVICE_COVERED")
{
    $idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $id=(int)$_POST['id'];

    $serviceCoveredArr=$kCourierServices->getCourierAgreeServiceData($idCourierAgreeProvider,$id);
    $_POST['agreeServiceCoverArr']=$serviceCoveredArr[0];
    $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
    $_POST['agreeServiceCoverArr']['idCourierProvider']=$idCourierProvider;
    addAgreementServiceCoverdFormHtml($kCourierServices,$idCourierProvider);
}
else if($mode=='DELETE_AGREE_SERVICE_COVERED')
{
    $idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $id=(int)$_POST['id'];

    $kCourierServices->deleteServiceData($id);

    $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
    $_POST['agreeServiceCoverArr']['idCourierProvider']=$idCourierProvider;
    
    $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);
        
    $data=$courierAgreeArr[0];
    $response=$kCourierServices->isCourierAgreementDataComplete($data);

    echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider);
    echo "||||".$response."||||";
    echo addDisplayCouierAgreementPricing($kCourierServices,true);
    die();
}
else if($mode=='ADD_EDIT_COURIER_TRADES_OFFERED_DATA')
{
    if($kCourierServices->addCourierAgreementTradeOffered($_POST['agreeTradesOfferArr']))
    {
        $idCourierAgreeProvider=$_POST['agreeTradesOfferArr']['idCourierAgreeProvider'];
        unset($_POST['agreeTradesOfferArr']);
        $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
        $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);
        
        $data=$courierAgreeArr[0];
        $response=$kCourierServices->isCourierAgreementDataComplete($data);
        
        echo "SUCCESS||||";
        addDisplayCouierAgreementTrades($kCourierServices);
        echo "||||".$response;
        die();
    }
    else
    {
        echo "ERROR||||";
        addAgreementTradesOfferedFormHtml($kCourierServices);
        die();
    } 
}
else if($mode=='SELECT_AGREE_TRADE_OFFERED')
{ 
    $idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
    $id=(int)$_POST['id'];

    $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
    addDisplayCouierAgreementTrades($kCourierServices,$id);
}
else if($mode=='EDIT_AGREE_TRADE_OFFERED')
{
    $idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
    $id=(int)$_POST['id'];


    $tardeOfferedArr=$kCourierServices->getCourierAgreeTradeData($idCourierAgreeProvider,$id);
    $_POST['agreeTradesOfferArr']=$tardeOfferedArr[0];
    $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
    addAgreementTradesOfferedFormHtml($kCourierServices);
}
else if($mode=='DELETE_TRADE_OFFERED')
{
    $idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
    $id=(int)$_POST['id'];

    $kCourierServices->deleteTradeOfferedData($id);

    $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;

    $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);

    $data=$courierAgreeArr[0];
    $response=$kCourierServices->isCourierAgreementDataComplete($data);

    echo addDisplayCouierAgreementTrades($kCourierServices);
    echo "||||".$response;
    die();
}
else if($mode=='SHOW_COURIER_AGREE_PRICING_DATA')
{ 
    if($kCourierServices->updateCourierAgreementPricingData($_POST['pricingArr']))
    { 
        $idCourierProviderAgree=$_POST['pricingArr']['idCourierAgreeProvider']; 
        $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree);

        $data=$courierAgreeArr[0];
        $response=$kCourierServices->isCourierAgreementDataComplete($data); 
        $idCourierProvider=$courierAgreementArr[0]['idCourierProvider']; 
        $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree); 
        echo "SUCCESS||||";
        echo addDisplayCouierAgreementPricing($kCourierServices)."||||";
        echo $response;
        die;
    }
    else
    {
        echo "ERROR||||";
        addDisplayCouierAgreementPricing($kCourierServices);
        die();
    }
}
else if($mode=='SHOW_AGREEMENT_SERVICE_TRADES_PRICING')
{
    $idCourierProviderAgree=(int)$_POST['id'];
    if((int)$idCourierProviderAgree>0)
    {
        $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
        $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

        $courierAgreementArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree);	

        //$_POST['pricingArr']=$courierAgreementArr[0];
        ////$_POST['pricingArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
        $idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
    }
    echo courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree)."||||";
    echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider)."||||";
    echo addDisplayCouierAgreementTrades($kCourierServices)."||||";
    echo addDisplayCouierAgreementPricing($kCourierServices,true);
    die;
}
else if($mode=='CHECK_COURIER_AGREEMENT_DATA')
{ 
    $courierProviderAgreementArr = $_POST['courierProviderAgreementArr'];
    if((int)$_SESSION['admin_id']>0)
    {
        $idForwarder = $courierProviderAgreementArr['idForwarder'];
    }
    else
    {
        $idForwarder = $_SESSION['forwarder_id'] ;
    }
    if($kCourierServices->isAccountAlreadyExists($courierProviderAgreementArr['szAccountNumber'],$courierProviderAgreementArr['idCourierProvider'],$idForwarder,$courierProviderAgreementArr['idEdit']))
    {
        $kCourierServices->szSpecialErrorMesage = "This account number has already been added" ;
        echo "ERROR||||";
        echo addCourierProviderAgreement($kCourierServices,true,true); 
        die;
    }
    else
    {
        $courierProviderAgreementArr = $_POST['courierProviderAgreementArr'];
        $kWebservices = new cWebServices();
        if($kWebservices->checkCourierApiCredentials($courierProviderAgreementArr,true))
        {
            echo "SUCCESS||||";
            echo addCourierProviderAgreement($kCourierServices,false,true);
            die;
        } 
        else
        {
            echo "ERROR||||";
            echo addCourierProviderAgreement($kCourierServices,true,true);		
            die;
        }  
    } 
}

else if($mode=='DELETE_AGREEMENT_SERVICE_TRADES_PRICING')
{
    $id=(int)$_POST['id'];

    $kCourierServices->deleteCourierProviderAgreementData($id);
    $courierAgreementArr=$kCourierServices->getCourierAgreementData();
    $idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
    $idCourierProviderAgree=$courierAgreementArr[0]['id'];
    $idCourierProviderAgree=0;
    $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
    $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

    //$_POST['pricingArr']=$courierAgreementArr[0];
    //$_POST['pricingArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

    echo courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree)."||||";
    echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider)."||||";
    echo addDisplayCouierAgreementTrades($kCourierServices)."||||";
    echo addDisplayCouierAgreementPricing($kCourierServices,true);
    die;
}
else if($mode=='EDIT_COURIER_PROVIDER_AGREEMENT')
{
    $id=(int)$_POST['id'];

    $courierAgreementArr=$kCourierServices->getCourierAgreementData($id);

    $_POST['courierProviderAgreementArr']['idEdit']=$courierAgreementArr[0]['id'];
    $_POST['courierProviderAgreementArr']['idForwarder']=$courierAgreementArr[0]['idForwarder'];
    $_POST['courierProviderAgreementArr']['idCourierProvider']=$courierAgreementArr[0]['idCourierProvider'];
    $_POST['courierProviderAgreementArr']['szAccountNumber']=decrypt($courierAgreementArr[0]['szAccountNumber'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['szUsername']=decrypt($courierAgreementArr[0]['szUsername'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['szPassword']=decrypt($courierAgreementArr[0]['szPassword'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['szMeterNumber']=decrypt($courierAgreementArr[0]['szMeterNumber'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['iServiceType']= $courierAgreementArr[0]['iServiceType'];  
    addCourierProviderAgreement($kCourierServices,false,true); 
}
else if($mode=='CHECK_COURIER_PROVIDER_CREDENTIALS')
{
    if($kCourierServices->checkCourierProviderCredentials(true))
    {
        
    }
    else
    {
        
    }
}
else if($mode=='CHECKTRADE_ALREADY_EXISTS')
{
    if($kCourierServices->checkTradeAlreadyExists($_POST['agreeTradesOfferArr']))
    {
        echo "SUCCESS||||";
    }
    else
    {
        echo "ERROR||||";
    }
}
else if($mode=='CLEAR_FORWARDER_COURIER_TRY_IT_OUT')
{
    echo display_forwarder_courier_try_it_out();
}
else if($mode=='CLEAR_TRYITOUT_SEARCH_FORM')
{
    echo "SUCCESS||||";
    echo display_tryitout_search_form();
    die;
}
else if($mode=='ADD_MORE_TRYITOUT_QUOTE')
{
    $number = sanitize_all_html_input($_REQUEST['number'])+1; 
    $szShipmentType = sanitize_all_html_input(trim($_REQUEST['shipment_type'])); 
    
    echo display_tryitout_quote_cargo_details_form($szShipmentType,$number);
    die;
}
if($mode == 'SHOW_PRICE_DETAILS')
{
    $szServiceID = sanitize_all_html_input(trim($_REQUEST['service_id']));
    $szReferenceToken = sanitize_all_html_input(trim($_REQUEST['token']));
  
    if(!empty($szServiceID) && !empty($szReferenceToken))
    {  
        cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__;
        cPartner::$szGlobalToken = $szReferenceToken; 

        $kWHSSearch = new cWHSSearch();
        $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true);
        $searchResultAry = unserialize($tempResultAry['szSerializeData']);
           
        if(!empty($searchResultAry))
        {
            $updateBookingAry = array();
            foreach($searchResultAry as $searchResultArys)
            { 
                if(trim($searchResultArys['unique_id']) == $szServiceID )
                { 
                    $updateBookingAry = $searchResultArys ;
                    break;
                }
            }
        } 
        $kPartner = new cPartner();
        $postSearchAry = array(); 
        cPartner::$szApiRequestMode = "QUICK_QUOTE"; 
        $postSearchAry = $kPartner->getQuickQuoteRequestData(); 
         
        $kConfig = new cConfig();
        if(!empty($postSearchAry['szOriginCountryStr']))
        {
            $szOriginCountryArr = reverse_geocode($postSearchAry['szOriginCountryStr'],true);   
            $idOriginCountry = $kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);
            $postSearchAry['idOriginCountry'] = $idOriginCountry;
        } 
        if(!empty($postSearchAry['szDestinationCountryStr']))
        {
            $szDesCountryArr = reverse_geocode($postSearchAry['szDestinationCountryStr'],true);   
            $idDestinationCountry = $kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);
            $postSearchAry['idDestinationCountry'] = $idDestinationCountry;
        }
        
        $bDTDTradesFlag = false;
        $kCourierServices = new cCourierServices(); 
        if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
        { 
            $bDTDTradesFlag = true; 
        } 
                 
        $t_base = "TryItOut/"; 
        echo "SUCCESS||||";
        if($updateBookingAry['idCourierAgreement']>0) 
        {
            echo display_forwarder_try_it_out_courier_price_details($updateBookingAry,$postSearchAry); 
        }
        else
        { 
            echo display_dtd_forwarder_price_details($updateBookingAry,$t_base,$postSearchAry,$bDTDTradesFlag);  
        }
        die;
    }	
}
else if(!empty($_POST['tryitOutAry']))
{ 
    $iQuickQuoteFlag=false;
    $kQuote = new cQuote();
    $searchResultAry = array();
    $searchResultAry = $kQuote->forwarder_try_it_out_search($_POST['tryitOutAry']); 
    if((int)$_POST['tryitOutAry']['iQuickQuotePage']==1)
    {
        $iQuickQuoteFlag=true;
    }
      
    if(!empty($kQuote->arErrorMessages))
    {
        echo "ERROR$$$$";
        echo display_tryitout_search_form(false,$kQuote,$iQuickQuoteFlag); 
        die;
    } 
    else
    { 
        echo "SUCCESS$$$$";
        echo display_forwarder_try_it_out_search_result($searchResultAry,$kQuote,$iQuickQuoteFlag);
        //echo "$$$$".$iScrollTop;
        if($iQuickQuoteFlag==1)
        {
            echo "$$$$";
            echo display_quick_quote_shipper_consignee($bookingDataAry,$mode,false,false,true);
        }
        die; 
    }
}
?>