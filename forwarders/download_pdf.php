<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca";
	

if((int)$_POST['createPDF']['idBooking']>0)
{
	$kBooking = new cBooking();
	$kConfig = new cConfig();
	$kWHSSearch = new cWHSSearch();
        
        $kBooking->load($_POST['createPDF']['idBooking']);
        
        if($kBooking->iFinancialVersion==2 && $kBooking->iForwarderGPType==1)
        {
            $file=getForwarderBookingConfirmationPdfFile_v2($_POST['createPDF']['idBooking']);
        }
        else
        {
            $file=getForwarderBookingConfirmationPdfFile($_POST['createPDF']['idBooking']);
        }
	
	if(!empty($file))
	{			
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header("Content-Type: application/force-download");
	    header('Content-Disposition: attachment; filename=' . urlencode(basename($file)));
	    // header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    ob_clean();
	    flush();
	    readfile($file);
	    unlink($file);
	    exit;		
	}
}
else
{
	//header("");	
}
?>