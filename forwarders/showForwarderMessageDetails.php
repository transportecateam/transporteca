<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
showForwarderDetails(); 
displayForwarderHeaderNotificaion(false);
?>
