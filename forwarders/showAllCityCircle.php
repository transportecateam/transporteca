<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
constantApiKey();

$kHaulagePricing = new cHaulagePricing();
$idWarehouse=$_REQUEST['idWarehouse'];
$idDirection=$_REQUEST['idDirection'];
$idHaulageModel=$_REQUEST['idHaulageModel'];

$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);

$latlongradArr=$kHaulagePricing->getAllCityHaulageData($idWarehouse,$idDirection,$idHaulageModel);

$maxRadius=$kWHSSearch->getManageMentVariableByDescription('__MAX_RADIUS_CIRCLE__');
//echo $kWHSSearch->szLongitude;

//echo $kWHSSearch->szLatitude;
if(count($latlongradArr)==1)
{
	$distance=$kHaulagePricing->getDistanceForShowCityOnMap($latlongradArr,$kWHSSearch->szLongitude,$kWHSSearch->szLatitude);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>		
		<script src="<?php echo __GOOGLE_MAP_JSON_URL__;?>" type="text/javascript"></script>
		<script type="text/javascript">
/* Developed by: Ajay jha */
//Global variables
var map;
var bounds = new google.maps.LatLngBounds; //Circle Bounds

var map_center = new google.maps.LatLng('<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>');

var Circle; //Circle object
var CirclePoints = []; //Circle drawing points
var CircleCenterMarker;
var circle_moving = false; //To track Circle moving
var circle_resizing = false; //To track Circle resizing
//var radius = <?=$radius?>; //1 km
var min_radius = 0; //0.5km
var max_radius = <?=$maxRadius?>; //5km

var redpin_1 = new google.maps.MarkerImage();
redpin_1.image = "<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/red-marker.png'?>";
//redpin_1.iconSize = new GSize(22, 33);
redpin_1.iconAnchor = new google.maps.Point(10, 32);

//Circle Marker/Node icons
var redpin = new google.maps.MarkerImage(); //Red Pushpin Icon

redpin.image = '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/resize.png'?>';
redpin.iconSize = new google.maps.Size(15,16);
redpin.iconAnchor = new google.maps.Point(10, 32);
var bluepin = new google.maps.MarkerImage(); //Blue Pushpin Icon
//bluepin.image = "http://maps.google.com/mapfiles/ms/icons/blue-pushpin.png";
bluepin.image = '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>';
//bluepin.iconSize = new GSize(22, 33);
bluepin.iconAnchor = new google.maps.Point(10, 32);
geocoder = new google.maps.Geocoder();
function initialize() 
{
	 //Initialize Google Map
    //if (GBrowserIsCompatible()) 
    //{
        /*map = new GMap2(document.getElementById("map_canvas"),6); //New GMap object
        map.setCenter(map_center);

        var ui = new GMapUIOptions(); //Map UI options
        ui.maptypes = { normal:true, satellite:true, hybrid:true, physical:false }
        ui.zoom = {scrollwheel:true, doubleclick:true};
        ui.controls = { largemapcontrol3d:true, maptypecontrol:true, scalecontrol:true };
        map.setUI(ui); *///Set Map UI options
        
        var myOptions = {
			zoom: 2,
			zoomControl: true,
			center: map_center,
			scaleControl: true,
		    overviewMapControl: true,
		    scrollwheel: true,
		    disableDoubleClickZoom: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControlOptions: {
		              style: google.maps.ZoomControlStyle.LARGE,
		              position: google.maps.ControlPosition.LEFT_CENTER
		          }
		  };
		map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		
		addWarehouseMarker(map_center,'<?=$kWHSSearch->szWareHouseName?>');
		<? if(!empty($latlongradArr)){
		
	        /*
	        var paddings = {top:30, right:10, left:50};
	        var box = null;
	         if(box) map.removeOverlay(box);
			  box = map.showBounds(bounds, paddings).drawBox();
			  map.addOverlay(box);
			  */
	        //map.fitBounds(bounds);
			$i=1;
			foreach($latlongradArr as $latlongradArrs)
			{
				if($latlongradArrs['szLatitude']!='' && $latlongradArrs['szLongitude']!='')
				{
					?> 
					var mapcenter_<?=$i?> = new google.maps.LatLng(<?=$latlongradArrs['szLatitude']?>,<?=$latlongradArrs['szLongitude']?>);
			        addCircleCenterMarker(mapcenter_<?=$i?>,<?=$latlongradArrs['iRadiousKM']?>,<?=$i?>);
			        addCircleResizeMarker(mapcenter_<?=$i?>,<?=$i?>);
			        drawCircle(mapcenter_<?=$i?>,<?=$latlongradArrs['iRadiousKM']?>,<?=$i?>);
			      
			        <? 
				}
				++$i;
			}
			?>		
			var bounds = new google.maps.LatLngBounds();
	         <?
	        	if(!empty($latlongradArr))
	        	{
	        		foreach($latlongradArr as $latlongradArrs)
	        		{
	        			?>
	        			var randomPoint = new google.maps.LatLng('<?=$latlongradArrs['szLatitude']?>','<?=$latlongradArrs['szLongitude']?>');
	        			bounds.extend(randomPoint);
	        			<?
	        		}
	        	}
	        ?>
	        var zoom_level = map.getZoom(bounds) ;
	        //alert(zoom_level);
	        if(zoom_level>16)
	        {
	        <? if(count($latlongradArr)>1){?>
	        	zoom_level = 12 ;
	        <? }else {?>
	        <? if($distance>500){?>
	        zoom_level = 4 ;
	        <? }else{?>
	        zoom_level = 6 ;
	        <? }}?>
	        // map.fitBounds(bounds); 
		    }
		   // alert(zoom_level);
		   	 <? if(count($latlongradArr)>1){?>
		   	  map.fitBounds(bounds); 
		   	 <? }?>
	       		 map.setZoom(zoom_level) ;
			<?
			
		} else{?>
			zoom_level = 2 ;
		 map.setCenter(bounds.getCenter(),zoom_level) ;
		<? }?>
   // }
}

// Adds Circle Center marker
function addCircleCenterMarker(point,radius,i)
{

	var properties = {
        position: point,
        map: map,
        draggable: false,
        icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>'
    };

	var CircleCenterMarker = new google.maps.Marker(properties);
   
   CircleCenterMarker.setMap(map);
   
    google.maps.event.addListener(CircleCenterMarker, 'dragstart', function() { //Add drag start event
        circle_moving = true;
    });
    google.maps.event.addListener(CircleCenterMarker, 'drag', function(point) { //Add drag event
        drawCircle(point, radius,i);
        fitCircle();
    });
    google.maps.event.addListener(CircleCenterMarker, 'dragend', function(point) { //Add drag end event
        circle_moving = false;
        drawCircle(point, radius,i);
        fitCircle();
    });
}

// Adds Circle Resize marker
function addCircleResizeMarker(point,i) 
{
    var resize_icon = new google.maps.MarkerImage(redpin);
    resize_icon.maxHeight = 0;
    var properties = {
        position: point,
        map: map,
        draggable: false,
        icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/red-marker.png'?>'
    };

	var CircleCenterMarker = new google.maps.Marker(properties);
    //map.addOverlay(CircleResizeMarker); //Add marker on the map
    /*GEvent.addListener(CircleResizeMarker, 'dragstart', function() { //Add drag start event
        circle_resizing = true;
    });*/
    var map_center_new="mapcenter_"+i;
    map_center_new=point;
    //alert(map_center_new);
  /*  GEvent.addListener(CircleResizeMarker, 'drag', function(point) { //Add drag event
        var new_point = new google.maps.LatLng(map_center_new.lat(), point.lng()); //to keep resize marker on horizontal line
        var new_radius = new_point.distanceFrom(map_center_new) / 1000; //calculate new radius
        if (new_radius < min_radius) new_radius = min_radius;
        if (new_radius > max_radius) new_radius = max_radius;
        drawCircle(map_center_new, new_radius);
    });
    GEvent.addListener(CircleResizeMarker, 'dragend', function(point) { //Add drag end event
        circle_resizing = false;
        var new_point = new google.maps.LatLng(map_center_new.lat(), point.lng()); //to keep resize marker on horizontal line
        var new_radius = new_point.distanceFrom(map_center_new) / 1000; //calculate new radius
        if (new_radius < min_radius) new_radius = min_radius;
        if (new_radius > max_radius) new_radius = max_radius;
        drawCircle(map_center_new, new_radius);
    });*/
}

//Draw Circle with given radius and center
function drawCircle(center, new_radius,i)
{
    //Circle Drawing Algorithm from: http://koti.mbnet.fi/ojalesa/googlepages/circle.htm

    //Number of nodes to form the circle
 
   /* var nodes = new_radius * 40;
    if(new_radius < 1) nodes = 40;

    //calculating km/degree
    var latConv = center.distanceFrom(new google.maps.LatLng(center.lat() + 0.1, center.lng())) / 100;
    var lngConv = center.distanceFrom(new google.maps.LatLng(center.lat(), center.lng() + 0.1)) / 100;
	
	
    CirclePoints = [];
    var step = parseInt(360 / nodes) || 10;
    var counter = 0;
    for (var i = 0; i <= 360; i += step) 
    {
        var cLat = center.lat() + (new_radius / latConv * Math.cos(i * Math.PI / 180));
        var cLng = center.lng() + (new_radius / lngConv * Math.sin(i * Math.PI / 180));
        var point = new google.maps.LatLng(cLat, cLng);
        CirclePoints.push(point);
        counter++;
    }
    
    CircleResizeMarker.setLatLng(CirclePoints[Math.floor(counter / 4)]); //place circle resize marker
    CirclePoints.push(CirclePoints[0]); //close the circle polygon
   
   	//if (Circle) { map.removeOverlay(Circle); } //Remove existing Circle from Map
    /* var fillColor = (circle_resizing || circle_moving) ? "#00FF00" : "#00FF00"; //Set Circle Fill Color
    //Circle = new GPolygon(CirclePoints, '', 0, 0, '', 0); //New GPolygon object for Circle
    Circle = new GPolygon(CirclePoints, '#FFFFFF', 1, 1, fillColor, 0.2);
    
    map.addOverlay(Circle); //Add Circle Overlay on the Map
    radius = new_radius; //Set global radius
    
    var map_center_new = "mapcenter_"+i;
    map_center_new=center;
    
    if (!circle_resizing && !circle_moving) { //Fit the circle if it is nor moving or resizing
        //fitCircle();
        //Circle drawing complete trigger function goes here
    }*/
    
     var populationOptions = {
      strokeColor: "#FFFFFF",
      strokeOpacity: 1,
      strokeWeight: 1,
      fillColor: "#00FF00",
      fillOpacity: 0.2,
      map: map,
      center: center,
      radius: new_radius
    };
    cityCircle = new google.maps.Circle(populationOptions);
     
}
function showAddress() 
{
	var address = document.getElementById("address").value;
	var radius_val = document.getElementById("radious").value;

	if(address=='')
	{
		alert("Please enter city name");
		document.getElementById("address").focus();
		return false;
	}
	if(isNaN(radius_val))
	{
		alert("Please enter radius");
		document.getElementById("radious").focus();
		return false;
	}
	var radius = parseFloat(radius_val);
	 if (geocoder) 
	 {
	 	geocoder.getLatLng(
		address,
	 	function(point) {
	 	if (!point) 
	 	{
	 		alert(address + " not found");
	 	} 
	 	else 
	 	{ 
	 		posset = 1;
	 		 map.clearOverlays();
	 		//map.setMapType(G_HYBRID_MAP);
			map.setCenter(point,8);
	 		zm = 1;
	 		addCircleCenterMarker(point);
	 		addCircleResizeMarker(point);
	 		drawCircle(point, radius);
	 		fitCircle();
	 	}
	   });
	 }
}
function computepos (point)
{
var latA = Math.abs(Math.round(value=point.y * 1000000.));
var lonA = Math.abs(Math.round(value=point.x * 1000000.));

if(value=point.y < 0)
{
	var ls = '-' + Math.floor((latA / 1000000));
}
else
{
	var ls = Math.floor((latA / 1000000));
}

var lm = Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60);
var ld = ( Math.floor(((((latA/1000000) - Math.floor(latA/1000000)) * 60) - Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60)) * 100000) *60/100000 );

if(value=point.x < 0)
{
  var lgs = '-' + Math.floor((lonA / 1000000));
}
else
{
	var lgs = Math.floor((lonA / 1000000));
}

var lgm = Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60);
var lgd = ( Math.floor(((((lonA/1000000) - Math.floor(lonA/1000000)) * 60) - Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60)) * 100000) *60/100000 );
alert(point.y.toFixed(6));
alert(point.x.toFixed(6));
document.getElementById("latbox").value=point.y.toFixed(6);
document.getElementById("lonbox").value=point.x.toFixed(6);


}

function show_address_on_submit(kEvent)
{
	var temp=(window.event)?kEvent.keyCode:kEvent.which;
	if(temp == 13)
	{
		showAddress();
	}
}

//Fits the Map to Circle bounds
function fitCircle() {
    bounds = Circle.getBounds();
    map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
}

function addWarehouseMarker(point,warehouse) 
{
		var properties = {
	        position: point,
	        map: map,
	        draggable: false,
	        icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/red-marker.png'?>'
	    };

		var CircleCenterMarker = new google.maps.Marker(properties);
		CircleCenterMarker.setMap(map);
    	CirclePoints.push(point);
    	
    	google.maps.event.addListener(CircleCenterMarker, 'click', function(point) { //Add drag event
       	var daddress = warehouse;
         var oinfowindow = new google.maps.InfoWindow();
			  oinfowindow.setContent(daddress);
			  google.maps.event.addListener(
				CircleCenterMarker, 
				'click'
			  );
		   	oinfowindow.open(map,CircleCenterMarker);
    	});
        //var daddress = warehouse;
        //map.openInfoWindow(point,document.createTextNode(daddress));            
}


</script> 
</head>
<body onload="initialize()">
<div id="map_canvas" style="width:747px; height:384px"></div>
   <div id="map" align="center"></div>     
</body> 
</html>