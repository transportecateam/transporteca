<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$kForwarder = new  	cForwarder();
if(!empty($_SERVER['HTTP_HOST']))
{
	if($kForwarder->isHostNameExist($_SERVER['HTTP_HOST']))
	{
		define ('__BASE_URL__', "http://".$_SERVER['HTTP_HOST']);
   		define ('__BASE_URL_SECURE__', "https:".$_SERVER['HTTP_HOST']);
		define_forwarder_constants();
	}
	else
	{
		header("Location:".__MAIN_SITE_HOME_PAGE_URL__);
		exit;	
	}
}
if(!defined('__BASE_URL__')) 
{
	define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
	define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_URL__);
}

$_SESSION['forwarder_user_id']='';
unset($_SESSION['forwarder_user_id']);

$_SESSION['forwarder_admin_id'] = '';
unset($_SESSION['forwarder_admin_id']);

$_SESSION['forwarder_id']='';
unset($_SESSION['forwarder_id']);

$_SESSION['forwarder_iConfirmed']='';
unset($_SESSION['forwarder_iConfirmed']);


$_SESSION['forwarderHeaderNoticationFlagsAry']='';
unset($_SESSION['forwarderHeaderNoticationFlagsAry']);

header('Location:'.__FORWARDER_HOME_PAGE_URL__);
exit();
?>