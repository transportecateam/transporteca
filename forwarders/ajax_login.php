<?php
/**
 * Customer Login
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base_error = "Error";
$t_base='Users/AccountPage/';
$kForwarderContact	= new 	cForwarderContact();
if((int)$_SESSION['forwarder_user_id']>0)
{
	$kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
}
if((int)$_SESSION['forwarder_user_id']>0 && $kForwarderContact->iConfirmed=='1')
{
	$redirect_url=__FORWARDER_MY_ACCOUNT_URL__;
	header("location:$redirect_url");  
  	die;
}
if(!empty($_POST['loginArr']))
{
	if($kForwarderContact->forwarderLogin($_POST['loginArr']))
	{
		if(!empty($_POST['loginArr']['szRedirectUrl']))
		{
			$redirect_url = $_POST['loginArr']['szRedirectUrl'] ;
		}
		else
		{
			$redirect_url=__FORWARDER_MY_ACCOUNT_URL__;
		}		
		if((int)$_SESSION['verified_email_user']>0 && (int)$_SESSION['forwarder_user_id']>0)
		{	
			$redirect_url=__BASE_URL__;
			if((int)$_SESSION['verified_email_user']==(int)$_SESSION['forwarder_user_id'])
			{
				$_SESSION['valid_confirmation']='1';
			}
			else
			{
				$_SESSION['valid_confirmation']='2';
			}
		}
	?>
		<script type="text/javascript">
		$(location).attr('href','<?=$redirect_url?>');
		</script>
	<?	
		exit();
	}
}
if(!empty($_REQUEST['redirect_url']))
{
	$_POST['loginArr']['szRedirectUrl'] = sanitize_all_html_input(trim($_REQUEST['redirect_url']));		
}
if(!empty($_REQUEST['cancel_url']))
{
	$_POST['loginArr']['szCancelUrl'] = sanitize_all_html_input(trim($_REQUEST['cancel_url']));
}
if((int)$_SESSION['forwarder_user_id']>0)
{
 	$kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
}

$cancel_url="";
// if clicks on cancle redirect to landing page
if($_POST['loginArr']['szCancelUrl']==1)
{
	//$cancel_url = __LANDING_PAGE_URL__ ;
}

?>
<script type="text/javascript">
$("#szEmail").focus();
</script>
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
<div id="activationkey" style="display:none";></div>
<form name="loginForm" id="loginForm" method="post">
	<?php
	
//	if($_SERVER['HTTP_REFERER']==__BOOKING_DETAILS_PAGE_URL__."/")
	//{
		//$cancel_url=__BOOKING_DETAILS_PAGE_URL__;
	//}
	if(!empty($kForwarderContact->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kForwarderContact->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php }
	if((int)$_SESSION['forwarder_user_id']>0)
	{
		echo "<h5>".t($t_base.'messages/verification_email_1')."</h5>";
		echo "<p>".t($t_base.'messages/verification_email_2')." ".date('d/m/Y',strtotime($kForwarderContact->dtConfirmationCodeSent)).".</p><br/><p> ".t($t_base.'messages/verification_email_3')."<p/>"; 
		?>
		<br/>
		<p>
		<a href="javascript:void(0)" onclick="resendActivationCode('<?=$_SERVER['HTTP_REFERER']?>');" class="button1"><span><?=t($t_base.'fields/resend');?></span></a>
		<a href="javascript:void(0)" class="button1" onclick="cancel_signin_login('<?=$_POST['loginArr']['szRedirectUrl']?>');"><span><?=t($t_base.'fields/close')?></span></a></p>
		<?php
	}else{
	?>
	<h5><?=t($t_base.'title/signin')?></h5>
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/signin_email')?></p>
		<p class="fl-65"><input type="email" id="szEmail" name="loginArr[szEmail]" value="<?=$_POST['loginArr']['szEmail']?>" tabindex="1"/></p>
	</div>
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/password')?></p>
		<p class="fl-65"><input type="password" id="szPassword" name="loginArr[szPassword]" tabindex="2" onkeyup="check_login_password_submit_key_press(event);" /></p>
	</div>
		<br style="line-height:7px;" />
	
	<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancel_signin('<?=$cancel_url?>','ajaxLogin');" tabindex="6"><span><?=t($t_base.'fields/cancel')?></span></a> <a href="javascript:void(0)" class="button1" onclick="forwarderLogin('loginForm');" tabindex="7"><span><?=t($t_base.'fields/sign_in')?></span></a></p>
	<?php }?>
	 <input type="hidden" name="loginArr[szControlPanelUrl]" value="<?=$_SERVER['HTTP_HOST'];?>">
	 <!-- <input type="hidden" name="loginArr[szCancelUrl]" value="<? $_POST['loginArr']['szCancelUrl']?>">
	-->
</form>
</div>
		