<?php
ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "BulkUpload/";
$t_base_error="Error/";  
constantApiKey();

$mode = sanitize_all_html_input($_POST['mode']);
if($mode =='CHECK')
{
    if(!empty($_POST['iWarehouseType']))
    {
        $iWarehouseType = (int)$_POST['iWarehouseType'];
    }
    if(!empty($_POST['batch_number']))
    {
        $idBatch = (int)$_POST['batch_number'];
    }
}  
if(isset($iWarehouseType) && (int)$iWarehouseType>0)
{	
    $_SESSION['iWarehouseType'] = (int)$iWarehouseType;
}
if(isset($idBatch) && (int)$idBatch>0)
{	
    $_SESSION['idBatch'] = (int)$idBatch;
}
if(!isset($_SESSION['eachCfs']))
{
    $_SESSION['eachCfs'] = 1;
}   
$kWHSSearch=new cWHSSearch();
$totalCFS = $kWHSSearch->findTotalCFS((int)$idBatch,$iWarehouseType);
  
if($idBatch<=0 || $totalCFS ==0)
{
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        ob_end_clean();
        header("Location:".__BASE_URL__."/airportWarehouseLocationBulk/");
        die;
    }
    else
    {
        ob_end_clean();
        header("Location:".__BASE_URL__."/CFSLocationBulk/");
        die;
    } 
}
else
{	
    if(isset($_POST['mode']))
    {
        $mode = sanitize_all_html_input($_POST['mode']);
        if($mode =='CHECK')
        {	
            echo "SUCCESS|||||";
        }
    }
}
$kConfig = new cConfig();
$allCountriesArr=$kConfig->getAllCountries(true);
$kWHSSearch->selectCfsTempData($idBatch,$iWarehouseType);
$flag=0;
if($kWHSSearch->idCountry!=NULL && ($kWHSSearch->szLatitude==NULL && $kWHSSearch->szLongitude==NULL))
{
    $latLong = $kWHSSearch->getLatitudeLongitudeForCountry($kWHSSearch->idCountry);
    $kWHSSearch->szLatitude = $latLong['szLatitude'];
    $kWHSSearch->szLongitude = $latLong['szLongitude'];
    $flag=true;
}
$szLatitute  = $_POST['cfsAddEditArr']['szLatitude'] ? $_POST['cfsAddEditArr']['szLatitude'] : $kWHSSearch->szLatitude;
$szLongitude = $_POST['cfsAddEditArr']['szLongitude'] ? $_POST['cfsAddEditArr']['szLongitude'] : $kWHSSearch->szLongitude;
 
?>
 <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
     <script src="<?php echo __GOOGLE_MAP_JSON_URL__;?>" type="text/javascript"></script>
      <!-- <SCRIPT type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/mapctrl.js"></SCRIPT>-->
<script type="text/javascript">
// Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html

var map = null;
var geocoder = null;
var latsgn = 1;
var lgsgn = 1;
var zm = 12; 
var marker = null;
var posset = 0;
var lat = <?=($szLatitute?$szLatitute:0)?>;
var long = <?=($szLongitude?$szLongitude:0)?>;
var flag = <?=$flag?>;
var markersArray=[];
function showCountriesByCountry(idCountry)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SHOW_MULTIPLE_COUNTRIES_SELECT',idCountry:idCountry,iWarehouseType:'<?php echo $iWarehouseType; ?>'},function(result){
	$('#bottom-form').html(result);
	$('#CFSListing').attr('value',idCountry);	
    });
    mapLocationOnchange(idCountry);
}
function mapLocationOnchange(id)
{	
    var id = parseInt(id);
    var latLong;
    if(id>0)
    {		
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'mapCountry',id:id},function(result){
            latLong = result.split(',');
            lat =  parseInt(latLong[0]);
            long = parseInt(latLong[1]);
            flag = 1;
            xz();
        });
    }
}

function xz() 
{
    //if (GBrowserIsCompatible()) 
    //{ 
    var myLatLng = new google.maps.LatLng(lat, long);
       var myOptions = {
        zoom: 2,
        zoomControl: true,
        center: myLatLng,
        scaleControl: true,
        overviewMapControl: true,
        scrollwheel: true,
        disableDoubleClickZoom: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_CENTER
        }
    };
    map = new google.maps.Map(document.getElementById("map"),myOptions);
    /*map = new GMap2(document.getElementById("map"));
    map.setCenter(new GLatLng(lat,long), 2);
    map.setMapType(G_NORMAL_MAP);
    map.addControl(new GLargeMapControl());
    map.addControl(new MapTypeControl());
    map.addControl(new GScaleControl());
    map.enableScrollWheelZoom();
    map.disableDoubleClickZoom();
    geocoder = new GClientGeocoder();*/
    geocoder = new google.maps.Geocoder();

    var point_marker = new google.maps.LatLng(lat,long); 

    var properties = {
        position: point_marker,
        map: map,
        draggable: true
    }; 
    var marker = new google.maps.Marker(properties); 
    markersArray.push(marker);
    //marker = new GMarker(point_marker, {draggable: true});
    if( ( lat?lat:0 > 0 || long?long:0 > 0 ) && flag == 0)
    {	
        map.setZoom(5);
        //map.addOverlay(marker);
    }
    else if( flag == 0 )
    {
        map.setZoom(5);
        ///map.addOverlay(marker);
    }
    else
    {
        map.setZoom(5);
    }   
    google.maps.event.addListener(map, 'click', function(event) 
    {
        posset = 1;
        clearOverlays();
        fc( event.latLng) ;
        //marker.setPoint(point);
        computepos(event.latLng);
    });

    google.maps.event.addListener(marker, "click", function() {
        var point = marker.getPosition();
        var oinfowindow = new google.maps.InfoWindow();
        oinfowindow.setContent(marker.getPosition().toUrlValue(6));
        google.maps.event.addListener(
              marker, 
              'click'
        );
        oinfowindow.open(map,marker);
        computepos (point);
    });

    google.maps.event.addListener(marker, 'dragstart', function(overlay,point_marker)  { //Add drag start event
        computepos(marker.getPosition());
    });
    google.maps.event.addListener(marker, 'drag', function(overlay,point_marker)  { //Add drag event 
        computepos(marker.getPosition());
    });
    google.maps.event.addListener(marker, 'dragend', function(overlay,point_marker)  { //Add drag end event
            computepos(marker.getPosition());
    });
    //}
    if(flag)
    {
        document.getElementById("szLatitude").value ='';
        document.getElementById("szLongitude").value = '' ;
    }
}

function computepos (point)
{ 
	var newPoints=point.toUrlValue(6);
	var myarr = newPoints.split(",");
	var latA = Math.abs(Math.round(value=myarr[1] * 1000000.));
	var lonA = Math.abs(Math.round(value=myarr[0] * 1000000.));
	
	if(value=point.y < 0)
	{
		var ls = '-' + Math.floor((latA / 1000000));
	}
	else
	{
		var ls = Math.floor((latA / 1000000));
	}
	
	var lm = Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60);
	var ld = ( Math.floor(((((latA/1000000) - Math.floor(latA/1000000)) * 60) - Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60)) * 100000) *60/100000 );
	
	if(value=point.x < 0)
	{
	  var lgs = '-' + Math.floor((lonA / 1000000));
	}
	else
	{
		var lgs = Math.floor((lonA / 1000000));
	}
	
	var lgm = Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60);
	var lgd = ( Math.floor(((((lonA/1000000) - Math.floor(lonA/1000000)) * 60) - Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60)) * 100000) *60/100000 );
	
	document.getElementById("szLatitude").value=myarr[0];
	document.getElementById("szLongitude").value=myarr[1];
}

function showAddress() 
{
	var address = document.getElementById("address").value;
 	if (geocoder) {
 	geocoder.getLatLng(
	 address,
	 function(point) 
	 {
	 	if (!point) 
	 	{
	 		alert(address + " not found");
 		} 
 		else 
 		{
			 posset = 1;
			
			 map.setMapType(G_HYBRID_MAP);
			 map.setCenter(point,8);
			 zm = 1;
			 marker.setPoint(point);
			 GEvent.trigger(marker, "click");
 		}
 	});
 }
}

function showLatLong(latitude, longitude) 
{
	if (isNaN(latitude)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(longitude)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	
	latitude1 = Math.abs( Math.round(latitude * 1000000.));
	if(latitude1 > (90 * 1000000)) 
	{ 
		alert(' Latitude must be between -90 to 90. ');  
		document.getElementById("latbox1").value=''; 
		return;
	}
	longitude1 = Math.abs( Math.round(longitude * 1000000.));
	if(longitude1 > (180 * 1000000)) { alert(' Longitude must be between -180 to 180. ');  document.getElementById("lonbox1").value='';  return;}
	
	var point = new GLatLng(latitude,longitude);
	
	posset = 1;
	
	if (zm == 0)
	{
		map.setMapType(G_HYBRID_MAP);
		map.setCenter(point,16);
		zm = 1;
	}
	else
	{
		map.setCenter(point);
	}
	
	 var html = "";
	 html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
	
	 var baseIcon = new GIcon();
	 baseIcon.iconSize=new GSize(32,32);
	 baseIcon.shadowSize=new GSize(56,32);
	 baseIcon.iconAnchor=new GPoint(16,32);
	 baseIcon.infoWindowAnchor=new GPoint(16,0);
	 var thisicon = new GIcon(baseIcon, "http://itouchmap.com/i/blue-dot.png", null, "http://itouchmap.com/i/msmarker.shadow.png");
	
	 var marker = new GMarker(point,thisicon);
	 google.maps.event.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
	 map.addOverlay(marker);
	 
	 google.maps.event.trigger(marker, "click");
}

function showLatLong1(latitude, latitudem,latitudes, longitude,  longitudem,  longitudes) 
{
	if (isNaN(latitude)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(latitudem)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(latitudes)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(longitude)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	if (isNaN(longitudem)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	if (isNaN(longitudes)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	
	if(latitude < 0)  { latsgn = -1; }
	alat = Math.abs( Math.round(latitude * 1000000.));
	if(alat > (90 * 1000000)) 
	{ 
            alert(' Degrees Latitude must be between -90 to 90. ');  
            return; 
        }
	latitudem = Math.abs(Math.round(latitudem * 1000000.)/1000000);
	absmlat = Math.abs(Math.round(latitudem * 1000000.));  //integer
	if(absmlat >= (60 * 1000000)) 
	{  
		alert(' Minutes Latitude must be between 0 to 59. ');
		return;
	}
	latitudes = Math.abs(Math.round(latitudes * 1000000.)/1000000);
	absslat = Math.abs(Math.round(latitudes * 1000000.));
	if(absslat > (59.99999999 * 1000000)) 
	{  
		alert(' Seconds Latitude must be between 0 and 59.99. '); 
		document.getElementById("latbox1ms").value=''; 
		return; 
	}
	
	if(longitude < 0)  
	{
	 lgsgn = -1; 
	}
	alon = Math.abs( Math.round(longitude * 1000000.));
	if(alon > (180 * 1000000)) 
	{
	  alert(' Degrees Longitude must be between -180 to 180. ');
	  return;
	}
	longitudem = Math.abs(Math.round(longitudem * 1000000.)/1000000);
	absmlon = Math.abs(Math.round(longitudem * 1000000));
	if(absmlon >= (60 * 1000000))   
	{  
		alert(' Minutes Longitude must be between 0 to 59. '); 
		return;
	}
	longitudes = Math.abs(Math.round(longitudes * 1000000.)/1000000);
	absslon = Math.abs(Math.round(longitudes * 1000000.));
	if(absslon > (59.99999999 * 1000000)) 
	{  
		alert(' Seconds Longitude must be between 0 and 59.99. '); 
		return;
	}
	
	latitude = Math.round(alat + (absmlat/60.) + (absslat/3600.) ) * latsgn/1000000;
	longitude = Math.round(alon + (absmlon/60) + (absslon/3600) ) * lgsgn/1000000;
	
	var point = new GLatLng(latitude,longitude);
	posset = 1;
	
	if (zm == 0)
	{
		map.setMapType(G_HYBRID_MAP);
		map.setCenter(point,12);
		zm = 1;
	}
	else
	{
		map.setCenter(point);
	}
	 var html = "";
	 html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
	
	 var baseIcon = new GIcon();
	 baseIcon.iconSize=new GSize(32,32);
	 baseIcon.shadowSize=new GSize(56,32);
	 baseIcon.iconAnchor=new GPoint(16,32);
	 baseIcon.infoWindowAnchor=new GPoint(16,0);
	 var thisicon = new GIcon(baseIcon, "http://itouchmap.com/i/blue-dot.png", null, "http://itouchmap.com/i/msmarker.shadow.png");
	 var marker = new GMarker(point,thisicon);
	 GEvent.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
	 map.addOverlay(marker);
	
	 google.maps.event.trigger(marker, "click");
}

function streetview()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the street view point.");
		return;
	}
	
	var point = map.getCenter();
	var t1 = String(point);
	t1 = t1.replace(/[() ]+/g,"");
	var str = "http://www.streetviews.co?e=" + t1;
	var popup = window.open(str, "streetview");
	//popup.focus();
}

function googleearth()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the Google Earth map point.");
		return;
	}
	var point = map.getCenter();
	var gearth_str = "http://gmap3d.com?r=3dmap&mt=Latitude-Longitude Point&ml=" + point.y+ "&mg=" + point.x;
	var popup = window.open(gearth_str, "googleearth");
	//popup.focus();
}

function pictures()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the photograph point.");
		return;
	}
	var point = map.getCenter();
	var pictures_str = "http://ipicture.mobi?r=pictures&mt=Latitude-Longitude Point&ml=" + point.y+ "&mg=" + point.x;
	var popup = window.open(pictures_str, "pictures");
	//popup.focus();
}

function lotsize()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the lot size map point.");
		return;
	}
	var point = map.getCenter();
	var t1 = String(point);
	t1 = t1.replace(/[() ]+/g,"");
	var vpike_str = "http://viewofhouse.com?e=" + t1 + "::findlotsize:";
	var popup = window.open(vpike_str, "lotsize");
	//popup.focus();
}

function getaddress()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the get address map point.");
		return;
	}
	var point = map.getCenter();
	var t1 = String(point);
	t1 = t1.replace(/[() ]+/g,"");
	var getaddr_str = "http://www.getaddress.net?latlng=" + t1;
	var popup = window.open(getaddr_str, "getaddress");
	//popup.focus();
}

function fc( point )
{
	 //alert('hi');
	 var html = "";
	 html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
	//alert(html);
	 var baseIcon = new google.maps.MarkerImage();
	 baseIcon.iconSize=new google.maps.Size(32,32);
	 baseIcon.shadowSize=new google.maps.Size(56,32);
	 baseIcon.iconAnchor=new google.maps.Point(16,32);
	 baseIcon.infoWindowAnchor=new google.maps.Point(16,0);
	 var thisicon = new google.maps.MarkerImage(baseIcon, '<?=__BASE_STORE_IMAGE_URL__.'/red-marker.png'?>', null, "http://itouchmap.com/i/msmarker.shadow.png");
	
	// var marker = new GMarker(point,{draggable: true});
	  var properties = {
        position: point,
        map: map,
        draggable: true
	};
	
	var marker = new google.maps.Marker(properties);
	markersArray.push(marker);
	map.setCenter(point);
	
	map.setZoom(8);
	
	 google.maps.event.addListener(marker, "click", function() {
	 //alert('hi');
		var oinfowindow = new google.maps.InfoWindow();
	  	oinfowindow.setContent(html);
	  	oinfowindow.open(map,marker);
	  });
	 
	 google.maps.event.addListener(marker, 'dragstart', function(overlay,point)  { //Add drag start event
	     		computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'drag', function(overlay,point)  { //Add drag event
	        	
				computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'dragend', function(overlay,point)  { //Add drag end event
	    	       computepos(marker.getPosition());
	    });
}



function createMarker(point, html) 
{
	 var marker = new GMarker(point);
	 GEvent.addListener(marker, "click", function()
	 {
	 	marker.openInfoWindowHtml(html);
	 });
	 return marker;
}

function reset() 
{
	map.clearOverlays();
	
	document.getElementById("latbox").value='';
	document.getElementById("lonbox").value='';

	marker = new GMarker(new GLatLng('<?=$szLatitute?>','<?=$szLongitude?>'), {draggable: true});
	map.addOverlay(marker);
	marker.setPoint(map.getCenter());
	
	google.maps.event.addListener(marker, "dragend", function() {
	var point = marker.getLatLng();
	posset = 0;
	
	if (zm == 0)
	{map.setCenter(point,12); zm = 1;}
	else
	{map.setCenter(point);}
	computepos(point);
	});
	
	google.maps.event.addListener(marker, "click", function() {
	var point = marker.getLatLng();
	marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
	computepos (point);
	});
}

function update_google_map(szLatitude,szLongitude)
{
    lat = szLatitude;
    long = szLongitude;
    flag = 0;
    xz();
}
function xzReset() 
{
    lat = $("#szLatitude").val();
    long = $("#szLongitude").val();
    flag = 0;
    xz();
    /*
    if (GBrowserIsCompatible()) 
    {
        map.clearOverlays();
        var lat = 0;
        var lon = 0;
        var lat = $("#szLatitude").val();
        var lon = $("#szLongitude").val();
        lat = parseInt(lat);
        lon = parseInt(lon);
        map = new GMap2(document.getElementById("map"));
        map.setCenter(new GLatLng(lat,lon), 2);
        map.setMapType(G_NORMAL_MAP);
        map.addControl(new GLargeMapControl());
        map.addControl(new MapTypeControl());
        map.addControl(new GScaleControl());
        map.enableScrollWheelZoom();
        map.disableDoubleClickZoom();
        geocoder = new GClientGeocoder();

        var point_marker = new GLatLng(lat,lon); 
        marker = new GMarker(point_marker, {draggable: true});
        map.setZoom(12);
        map.addOverlay(marker);
        google.maps.event.addListener(map, 'click', function(overlay,point) 
        {
            if(!overlay) 
            {
                posset = 1;
                map.clearOverlays();
                fc( point) ;
                map.setZoom(12);
                //marker.setPoint(point);
                if (zm == 0)
                {
                    map.setCenter(point,7); 
                    zm = 1;
                }
                else
                {
                    map.setCenter(point);
                }
                computepos(point);
            }
        });

        google.maps.event.addListener(marker, "click", function() {
            var point = marker.getLatLng();
            marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
            computepos (point);
        });

        google.maps.event.addListener(marker, 'dragstart', function(overlay,point_marker)  { //Add drag start event
            computepos(overlay);
        });
        google.maps.event.addListener(marker, 'drag', function(overlay,point_marker)  { //Add drag event 
            computepos(overlay);
        });
        google.maps.event.addListener(marker, 'dragend', function(overlay,point_marker)  { //Add drag end event
                   computepos(overlay);
        });
    }
    <?php if($flag) { ?>
        document.getElementById("szLatitude").value ='';
        document.getElementById("szLongitude").value = '' ;
    <?php } ?>
        */
}
function clearOverlays() {
  //alert(markersArray.length);	
  if (markersArray) {
  for (var i = 0; i < markersArray.length; i++) {
  
  	//alert(markersArray[i]);
  	//alert(markersArray[i]);
    markersArray[i].setMap(null);
    //alert(i);
   }
   //markersArray=[];
    
  }
}
function repositionGoogleMap() 
{	 
    var lat = $('#szLatitude').val();
    var long = $('#szLongitude').val();
    var focuss = $("input:focus");
    if(lat!= null && long!=null && lat!='' && long!='')
    {
        xzReset();
    } 
}


function sendValue()
{
	var latitute = document.getElementById("latbox").value;
	var logitude = document.getElementById("lonbox").value;
    //window.parent.document.getElementById('szLatitude').value = latitute;
    //window.parent.document.getElementById('szLongitude').value = logitude;
    //alert("I m in googlemap.php   latitute "+latitute+" longitute "+logitude);
    window.top.window.insert_value_google_map(latitute,logitude);    
}

function show_address_on_submit(kEvent)
{
	var temp=(window.event)?kEvent.keyCode:kEvent.which;
	if(temp == 13)
	{
		showAddress();
	}
}

$(document).ready(function() {
 xz();
 confirmExit();
});
</script>
<script language="JavaScript">
	//var check = true;
  	//	window.onbeforeunload = confirmExit;
  		function confirmExit()
  		{
  			var elements = document.getElementsByTagName('a');
			for(var i = 0, len = elements.length; i < len; i++) 
			{
				//alert(elements[28].href);
				if(elements[i].href!='' && elements[i].href!='javascript:void(0)' && elements[i].href!='javascript:void(0);' )
				{
					$(elements[i]).attr('onClick','checkPageRedirection(\''+elements[i].href+'\');');
					$(elements[i]).attr('href','javascript:void(0);');
    			}
			}
    		//if(check == true)
    		//{
    			//return 'Leaving CFS locations bulk upload\n\n You are trying to navigate away from the approval of your CFS locations bulk upload. What would you like us to do with the remaining XX CFS locations from this upload file which have not yet been approved?\nWe can ignore the remaining CFS locations uploaded, or we can transfer the locations to the menu called Upload service - Awaiting approval. You will then be able to review and approve them later.\n';
    			//checkPageRedirection();
    			//return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
 
    		//}
  		}
</script>
	<h5><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/approve_upload_airport_cfs'):t($t_base.'title/approve_upload'); ?> <?=$_SESSION['eachCfs']?> of <?=$_SESSION['CFS']?> </b></h5>
	<?php
		echo "<div id='Error-log'>";
		if(!empty($kWHSSearch->arErrorMessages))
		{
		?>
		<div id="regError" class="errorBox">
		<div class="header"><?=t($t_base_error.'please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kWHSSearch->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?php
		//unset($kWHSSearch->arErrorMessages);
		}
		echo "</div>";
	?>
        <?php
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            { 
                $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text_airport_cfs'); 
                $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text_airport_warehouse');
                $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text_airport_cfs');
                $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text_airport_cfs'); 
            }
            else
            { 
                $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text'); 
                $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text_airport_warehouse');
                $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text'); 
                $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text');
            } 
            $szCfsNameBluText = t($t_base.'messages/cfs_name_blue_box_text'); 
        ?>
		<form name="addEditWarehouse" id="addEditWarehouse" method="post" >
		<label class="profile-fields">
			<span class="field-name"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'fields/airport_warehouse'):t($t_base.'fields/cfs_name');?></span>
			<span class="field-container"><input type="text" onblur="closeTip('cfs_name');" onfocus="openTip('cfs_name');" name="cfsAddEditArr[szWareHouseName]" id="szCFSName" value="<?=$_POST['cfsAddEditArr']['szWareHouseName'] ? $_POST['cfsAddEditArr']['szWareHouseName'] : $kWHSSearch->szWarehouseName ?>"/></span>
			<div class="field-alert"><div id="cfs_name" style="display:none;"><?=$szCfsNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/address_line');?> 1</span>
			<span class="field-container"><input type="text" onblur="closeTip('address_1');" onfocus="openTip('address_1');" name="cfsAddEditArr[szAddress1]" id="szAddressLine1" value="<?=$_POST['cfsAddEditArr']['szAddress1'] ? $_POST['cfsAddEditArr']['szAddress1'] : $kWHSSearch->szAddress1 ?>"/></span>
			<div class="field-alert"><div id="address_1" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/address_line');?> 2&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
			<span class="field-container"><input onblur="closeTip('address_2');" onfocus="openTip('address_2');"  type="text" name="cfsAddEditArr[szAddress2]" id="szAddressLine2" value="<?=$_POST['cfsAddEditArr']['szAddress2'] ? $_POST['cfsAddEditArr']['szAddress2'] : $kWHSSearch->szAddress2 ?>"/></span>
			<div class="field-alert"><div id="address_2" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/address_line');?> 3&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
			<span class="field-container"><input type="text" onblur="closeTip('address_3');" onfocus="openTip('address_3');" name="cfsAddEditArr[szAddress3]" id="szAddressLine3" value="<?=$_POST['cfsAddEditArr']['szAddress3'] ? $_POST['cfsAddEditArr']['szAddress3'] : $kWHSSearch->szAddress3 ?>"/></span>
			<div class="field-alert"><div id="address_3" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/postcode');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
			<span class="field-container"><input type="text" onblur="closeTip('cfs_post_code');" onfocus="openTip('cfs_post_code');" name="cfsAddEditArr[szPostCode]" id="szOriginPostCode" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  value="<?=$_POST['cfsAddEditArr']['szPostCode'] ? $_POST['cfsAddEditArr']['szPostCode'] : $kWHSSearch->szPostcode ?>"/></span>
			<div class="field-alert"><div id="cfs_post_code" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/city');?></span>
			<span class="field-container"><input type="text" onblur="closeTip('cfs_city');" onfocus="openTip('cfs_city');" name="cfsAddEditArr[szCity]" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')" id="szOriginCity" value="<?=$_POST['cfsAddEditArr']['szCity'] ? $_POST['cfsAddEditArr']['szCity'] : $kWHSSearch->szCity ?>"/></span>
			<div class="field-alert"><div id="cfs_city" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/province');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
			<span class="field-container"><input type="text" onblur="closeTip('cfs_state');" onfocus="openTip('cfs_state');" name="cfsAddEditArr[szState]" id="szState" value="<?=$_POST['cfsAddEditArr']['szState'] ? $_POST['cfsAddEditArr']['szState'] : $kWHSSearch->szState ?>"/></span>
			<div class="field-alert"><div id="cfs_state" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/country');?></span>
                    <span class="field-container">
                        <select onchange="showCountriesByCountry(this.value);" size="1" onblur="closeTip('cfs_country');" onfocus="openTip('cfs_country');" name="cfsAddEditArr[szCountry]" id="szCountry" style="max-width:211px;">
                            <option>Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                    foreach($allCountriesArr as $allCountriesArrs)
                                    {
                                        ?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['cfsAddEditArr']['szCountry'])?$_POST['cfsAddEditArr']['szCountry']:$kWHSSearch->idCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option><?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                    <div class="field-alert"><div id="cfs_country" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label> 
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/phone_number');?></span>
                    <span class="field-container"><input type="text" onblur="closeTip('cfs_phone');" onfocus="openTip('cfs_phone');" name="cfsAddEditArr[szPhone]" id="szPhoneNumber" value="<?=$_POST['cfsAddEditArr']['szPhone'] ? $_POST['cfsAddEditArr']['szPhone'] : $kWHSSearch->szPhoneNumber ?>"/></span>
                    <div class="field-alert"><div id="cfs_phone" style="display:none;"><?=$szPhoneNumberBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/contact_person');?></span>
                    <span class="field-container"><input type="text" onkeyup="checkCfsDetails();" onblur="closeTip('cfs_contact_person');" onfocus="openTip('cfs_contact_person');" name="cfsAddEditArr[szContactPerson]" id="szContactPerson" value="<?=$_POST['cfsAddEditArr']['szContactPerson'] ? $_POST['cfsAddEditArr']['szContactPerson'] : $kWHSSearch->szContactPerson ?>"/></span>
                    <div class="field-alert"><div id="cfs_contact_person" style="display:none;"><?=t($t_base.'messages/contact_person_blue_box_text');?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/email');?></span>
                    <span class="field-container"><input type="text" onkeyup="checkCfsDetails();" onblur="closeTip('cfs_email');" onfocus="openTip('cfs_email');" name="cfsAddEditArr[szEmail]" id="szEmail" value="<?=$_POST['cfsAddEditArr']['szEmail'] ? $_POST['cfsAddEditArr']['szEmail'] : $kWHSSearch->szEmail ?>"/></span>
                    <div class="field-alert"><div id="cfs_email" style="display:none;"><?=t($t_base.'messages/email_blue_box_text');?></div></div>
		</label>
		<br /> 
		<h5><strong><?=t($t_base.'fields/exact_location');?></strong> <a href="javascript:void(0);" onclick="open_help_me_select_popup()" class="f-size-14"><?=t($t_base.'fields/help_me_find_this');?></a></h5>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/latitude');?></span>
                    <span class="field-container">
                        <input type="text" size='5' onblur="closeTip('cfs_lat');repositionGoogleMap();" onfocus="openTip('cfs_lat');" name="cfsAddEditArr[szLatitude]" id="szLatitude" value="<?=$_POST['cfsAddEditArr']['szLatitude'] ? $_POST['cfsAddEditArr']['szLatitude'] : $kWHSSearch->szLatitude ?>"/>
                    </span>		
                    <div class="field-alert1"><div id="cfs_lat" style="display:none;"><?php echo $szLatitudeBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/longitude');?></span>
                    <span class="field-container">
                        <input type="text" onblur="closeTip('cfs_long');repositionGoogleMap();" onfocus="openTip('cfs_long');" size='5' name="cfsAddEditArr[szLongitude]" id="szLongitude" value="<?=$_POST['cfsAddEditArr']['szLongitude'] ? $_POST['cfsAddEditArr']['szLongitude'] : $kWHSSearch->szLongitude ?>"/>
                    </span>
                    <div class="field-alert1"><div id="cfs_long" style="display:none;"><?php echo $szLongitudeBluText;?></div></div>
		</label>
		<br/>
                    <input type="hidden" name="cfsAddEditArr[tempDataId]" value="<?=$_POST['cfsAddEditArr']['tempDataId'] ? $_POST['cfsAddEditArr']['tempDataId'] : $kWHSSearch->tempDataId ?>">
                    <input type="hidden" name="cfsAddEditArr[idBatch]" value="<?=$kWHSSearch->idBatch?>">
                    <input type="hidden" name="cfsAddEditArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="">
                    <input type="hidden" id="CFSListing" name="cfsAddEditArr[selectNearCFS]" value="<?=$kWHSSearch->idCountry;?>">
                    <input type="hidden" name="cfsAddEditArr[iWarehouseType]" id="iWarehouseType" value="<?php echo $iWarehouseType; ?>">
		</form>
                <form action="javascript:void();" method="post" >
                    <div id="map" class="displayMap" align="center" style="width: 700px; height: 300px;"></div> 
                </form>
                <div class="map_note" align="left"><p>Note: <?=t($t_base.'messages/note_for_forwarder_gmaps');?> </p></div>
                <br />
    	
    	 <div id="bottom-form">
    	 <?php 
                $countryList = $kWHSSearch->findWarehouseCountryList($kWHSSearch->idCountry,$kWHSSearch->szLatitude,$kWHSSearch->szLongitude);?>
	    	<div id="selectForCountrySelection">
	    	 <?php 
                    //$selectListCFS = array(1,2,3);
                    if($kWHSSearch->idCountry!= NULL)
                    {
                        $selectListCFS = array($kWHSSearch->idCountry);
                    }
	    	 ?>
                <h4><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_this_airport_warehouse'):t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
                <p style="margin-top: 5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/add_local_market_airport_cfs'):t($t_base.'messages/add_local_market');?></p>
	    	<div style="direction: ltr;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
                    <td style="padding-top:5px;" valign="top" width="215"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_airport_warehouse'):t($t_base.'title/countries_served_by_cfs');?></td>
                    <td>  	
                        <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
                            <?php
                            if(!empty($allCountriesArr))
                            {
				foreach($allCountriesArr as $allCountriesArrs)
				{
                                    if(in_array($allCountriesArrs['id'],$selectListCFS))
                                    {
                                        ?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option><?php
                                    }
				}
                            }
                        ?>	
                        </select>
                        <br />
                        <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
                        <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?> </span></a>
                    </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/sorted_on_airport_cfs'):t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>	<select  onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 <?php
	    	 	if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."'>".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	    	 ?>
	    	 </select> <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	  	   		<div class="profile-fields" style="margin-top: 14px;float:left;">
	    	 	<div class="field-alert" id="example" style="display:none;">
	    	 	<div>
	    	 		<p><?=t($t_base.'messages/for_your_rotterdam');?></p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'messages/for_your_hong_kong');?></p>
	    	 	</div>
	    	 	</div>
	    	 	</div>
	    	 </div>
		
	    
		<br /><br />
	
	    	 <?php //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>
	     </div> 
    	 <div style="clear: both;"></div>
    	 <br />
    	 
    	<div class="oh">
            <p class="fl-30" align="left" style="padding-top: 6px;"><?=t($t_base.'messages/click_to_move');?></p>
            <p class="fl-40" align="left">
    	 	<a class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');submit_warehouse_data('<?php echo $iWarehouseType; ?>','<?php echo $idBatch; ?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/approve');?></span></a>
    	 	<a class="button2" onclick="delete_warehouse_data('<?=$kWHSSearch->tempDataId?>','<?=$idBatch?>','<?php echo $iWarehouseType; ?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/delete');?></span></a>
            </p>
    	 </div>
	</div>