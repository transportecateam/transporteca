<?php
/**
 * Edit User Information
 */
ob_start();
if( !isset( $_SESSION ) )
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
$idForwarder=$_SESSION['forwarder_id'];
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "ForwardersCompany/Preferences/";
//checkAuthForwarder_ajax();

//constants for hardcoded data for preferences
	//$bookingReports=array('Daily','Weekly','Monthy','Never');
	//$paymentReports=array('Weekly','Monthly');
	//$pricngUpdates=array('No','Yes');
	$arrBook=BookingReportsArr();
	$bookingReports	= $arrBook[0];
	$paymentReports	= $arrBook[1];
	$pricngUpdates	= $arrBook[2];
	$forwarderControlPanelArr=$kForwarderContact->getSystemPanelDetails($idForwarder);	
	$t_base_error = "Error";
	$forwarderCompanyAry = array();
	$operation_mode = 'EDIT_SYSTEM_CONTROL';
	
if(!empty($_REQUEST['mode']))
{
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
	$idForwarder    = sanitize_all_html_input(trim($_REQUEST['id']));
}

if(!empty($_REQUEST['status'])=='REMOVE_EMAIL_ADDRESS' &&!empty($_REQUEST['idContact']))
{   
	$idContact = sanitize_all_html_input(trim($_REQUEST['idContact']));		
}
if(!empty($_POST['updatePrefEmailAry']) && !empty($idForwarder))
{ 	
    $updateMailer=$kForwarderContact->updateMailingAddress($_POST['updatePrefEmailAry'],$idForwarder);

    if(isset($_POST['removeEmails']) && $updateMailer)
    {
        $id=explode('_',$_POST['removeEmails']);

        if(count($id)>1)
        {
            foreach($id as $key=>$value) 
            $kForwarderContact->removeMailingAddresses($value);
        }	
    }
    if($updateMailer)
    {
        $operation_mode='DISPLAY_EMAIL_ADDRESS'; 
    }
}
if(!empty($_POST['updatePrefControlArr']) && !empty($idForwarder) && $operation_mode == 'EDIT_SYSTEM_CONTROL')
{	 
	$idOldCurrency = (int)sanitize_all_html_input(trim($_POST['updatePrefControlArr']['szCurrency_hidden']));
	$idNewCurrency = (int)sanitize_all_html_input(trim($_POST['updatePrefControlArr']['szCurrency']));
	if(($idOldCurrency==0)|| $idNewCurrency==$idOldCurrency)
	{
		$updateController=$kForwarderContact->updatePreferencesCotrol($_POST['updatePrefControlArr'],$idForwarder);
		if($updateController)
		{	echo "SUCCESS|||||";
			$operation_mode='VIEW_SYSTEM_CONTROL';
		}
	}
	else
	{
	
		$kWhsSearch	   = new cWHSSearch();
		
		if($idOldCurrency =='1')
		{
			$oldCurrency ='USD';
		}
		else
		{
			$oldCurrency = $kWhsSearch->getCurrencyDetails($idOldCurrency);
			$oldCurrency = $oldCurrency['szCurrency'];
		}
		if($idNewCurrency =='1')
		{
			$newCurrency ='USD';
		}
		else
		{
			$newCurrency = $kWhsSearch->getCurrencyDetails($idNewCurrency);
			$newCurrency = $newCurrency['szCurrency'];
		}
		echo "CONFIRM|||||";
		echo forwarder_currency_change_notifivcation($idForwarder,$t_base,$newCurrency,$oldCurrency);
		die;
	}
}
if($operation_mode=== 'SAVE_SYSTEM_CONTROL')
{
    $updatePrefControlArr =array();
    $updatePrefControlArr['szCurrency'] = sanitize_all_html_input($_POST['szCurrency']);
    $updatePrefControlArr['szPayment'] = sanitize_all_html_input($_POST['szPayment']);
    $updatePrefControlArr['szRss'] = sanitize_all_html_input($_POST['szRss']);
    $szOldCurrency = sanitize_all_html_input($_POST['szOldCurrency']);
    $updateController=$kForwarderContact->updatePreferencesCotrol($updatePrefControlArr,$idForwarder);
    $kForwarder->addCurrencyChangeLogs($szOldCurrency,$updatePrefControlArr['szCurrency'],$idForwarder);
    if($updateController)
    {
            $operation_mode='VIEW_SYSTEM_CONTROL';
    }
} 
if($operation_mode=='EDIT_EMAIL_ADDRESS')
{ 	
    $forwarderContactAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder);
    
    $szBookingEmailAry = $forwarderContactAry['szBookingEmail'];
    $szPaymentEmailAry = $forwarderContactAry['szPaymentEmail'];
    $szCustomerServiceEmailAry = $forwarderContactAry['szCustomerServiceEmail'];
    
    $szCatchUpBookingQuoteEmailAry = (!empty($forwarderContactAry['szCatchUpBookingQuoteEmail'])) ? $forwarderContactAry['szCatchUpBookingQuoteEmail']:array();

    $szAirBookingEmailAry = $forwarderContactAry['szAirBookingEmail'] ;
    $szRoadBookingEmailAry = $forwarderContactAry['szRoadBookingEmail'] ;
    $szCourierBookingEmailAry = $forwarderContactAry['szCourierBookingEmail']; 
    
    $j=1; 
    $count=0; 
    if(!empty($_POST['updatePrefEmailAry']['szEmailCatchUpAll']))
    { 
        $iNumcatcathupEmails = count($_POST['updatePrefEmailAry']['szEmailCatchUpAll']);
    }
    else if(!empty($szCatchUpBookingQuoteEmailAry))
    {
        $iNumcatcathupEmails = count($szCatchUpBookingQuoteEmailAry);
    } 
    else
    { 
        $iNumcatcathupEmails = 1;
    } 
?> 
    <div id="ajax_edit_mail_control">
    <?php  if(!empty($kForwarderContact->arErrorMessages)) { ?>
        <div id="regError" class="errorBox">
            <div class="header"><?=t($t_base_error.'/please_following');?></div>
            <div id="regErrorList">
                <ul>
                <?php
                    foreach($kForwarderContact->arErrorMessages as $key=>$values)
                    {
                ?>
                        <li><?=$key." ".$values?></li>
                <?php	
                    }
                ?>
                </ul>
            </div>
        </div>
    <?php } ?>
    <h4 style="padding-top:40px;"><strong><?=t($t_base.'titles/email_address');?></strong></h4> 
    <form id="ajax_preferences_email" method="post">  
        <div class="profile-fields">
            <span class="field-name"><?=t($t_base.'titles/catch_all_quote');?></span>   
            <div class="field-container" style="width:35%;float:right;" id="forwarder_preferences_edit_catchUpAll"> 
                <div id="forwarder_preferences_catchUpAll"> 
                    <?php 
                        for($j=1;$j<=$iNumcatcathupEmails;$j++) 
                        {
                            $count=$j-1; 
                            if(!empty($_POST['updatePrefEmailAry']['szEmailCatchUpAll'][$j]))
                            { 
                                $szQuoteCatchUpEmail = $_POST['updatePrefEmailAry']['szEmailCatchUpAll'][$j] ;
                            }
                            else
                            { 
                                $szQuoteCatchUpEmail = $szCatchUpBookingQuoteEmailAry[$count]['szEmail'] ;
                            } 
                    ?>
                        <div class="profile-fields" id="payments<?=$j;?>">
                            <input type="text" name="updatePrefEmailAry[szEmailCatchUpAll][<?=$j;?>]" id="szEmailCatchUpAll_<?php echo $j; ?>" value="<?php echo $szQuoteCatchUpEmail; ?>" >
                            <input type="hidden" name="updatePrefEmailAry[quoteEmailId][<?php echo $j;?>]" value="<?php echo $szCatchUpBookingQuoteEmailAry[$count]['id']; ?>"> 
                            <!--<div class="field-alert"><div id="pay<?=$j?>" style="display:none;"><?php echo t($t_base.'messages/payment');?></div></div>-->  
                        </div> 
                    <?php } ?>
                    <input type="hidden" name="ajax_preferences_catchUpAll" id="ajax_preferences_catchUpAll" value="<?php echo $j; ?>">
                </div> 
                <span class="oh"> 
                    <a class="fr-70" style="width: auto;" href="javascript:void(0);" onclick="add_forwarder_profile('catchUpAll','CatchUpAll','validatePayment');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>  
            </div>
        </div> 
        <div style="clear: both;"></div>
         
    <?php
	if(!empty($szPaymentEmailAry) || !empty($_POST['updatePrefEmailAry']))
	{
            $j=1;
    ?>
        <div class="profile-fields">
            <span class="field-name"><?=t($t_base.'titles/Payments_billing');?></span>  
            <div class="field-container" style="width:35%;float:right;" id="forwarder_preferences_edit_payment">
                <div id="forwarder_preferences_payment">
	<?php	if(isset($_POST['updatePrefEmailAry']['szEmailPayment']))
		{
                    $ij = $_POST['ajax_preferences_payment']-$_POST['validatePayment']-1; 
		}
		else
		{
                    $ij = count($szPaymentEmailAry);
		} 
		for($count=0;$ij>$count;$count++) 
		{
                    ?>
                    <div class="profile-fields" id="payments<?=$j;?>">
                        <input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailPayment'][$j])?$_POST['updatePrefEmailAry']['szEmailPayment'][$j]:$szPaymentEmailAry[$count]['szEmail']?>" onblur="closeTip('pay<?=$j?>');" onfocus="openTip('pay<?=$j?>');">
                        <input type="hidden" name="updatePrefEmailAry[paymentId][<?=$j;?>]" value="<?=$szPaymentEmailAry[$count]['id']?>">  
                    </div>
                    <?php 
                    $j++;
		}  
		?>
		<?php  $count=0;
		if(isset($_POST['validatePayment']))
		{	
                    $pay=(int)$_POST['validatePayment'];
                    if($pay>0)
                    { 
                        for($count;$pay>$count;$count++)
                        { 
                        ?>
                    <div id="payments<?=$j+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailPayment'][$j+$count]?>" onblur="closeTip('pay<?=$j+$count?>')" onfocus="openTip('pay<?=$j+$count?>')"></div>
			<?php 
                        } 	
                    }
		} $displayPayment=displayStyle($j+$count);
		?>
		<input type="hidden" name="validatePayment" id="validatePayment" value="<?=isset($_POST['validatePayment'])?$_POST['validatePayment']:0 ?>">
		</div>
		<input type="hidden" name="ajax_preferences_payment" id="ajax_preferences_payment" value="<?=isset($_POST['validatePayment'])?($_POST['validatePayment']+$j):$j?>">
		<span class="oh">
		<?php /*<a  class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_payment" style="display:<?=$displayPayment;?>;" onclick="remove_forwarder_profile('ajax_preferences_payment','payments','remove_forwarder_profile_payment','<?=$szPaymentEmailArys['id']?>','<?=$idForwarder?>','validatePayment');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a class="fr-70" style="width: auto;;" href="javascript:void(0);" onclick="add_forwarder_profile('payment','Payment','validatePayment');"><?=t($t_base.'fields/add_one_more');?></a>
		</span> 
            </div>
        </div>
	<?php } ?>
        <?php 
            if(empty($szPaymentEmailAry) && empty($_POST['updatePrefEmailAry']))
            {
                $j=1;
	?>
            <div class="profile-fields">
                <span class="field-name"><?=t($t_base.'titles/Payments_billing');?></span>  
                <div class="field-container" style="width:35%;float:right;" id="forwarder_preferences_edit_payment">
                    <div id="forwarder_preferences_payment"> 
                        <div class="profile-fields" id="payments<?=$j;?>">
                            <input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailPayment'][$j])?$_POST['updatePrefEmailAry']['szEmailPayment'][$j]:''?>" onblur="closeTip('pay<?=$j?>');" onfocus="openTip('pay<?=$j?>');">
                            <input type="hidden" name="updatePrefEmailAry[paymentId][<?=$j;?>]" value="<?=$szPaymentEmailAry[$count]['id']?>"> 
                            <!--  <div class="field-alert"><div id="pay<?=$j?>" style="display:none;"><?=t($t_base.'messages/payment');?></div></div>--> 
                        </div>
				<?php 
			$j++;
	
			
		?>
		<?php  $count=0;
		if(isset($_POST['validatePayment']))
                {	
                    $pay=(int)$_POST['validatePayment'];
                    if($pay>0)
                    { 
                        for($count;$pay>$count;$count++)
                        { 
                        ?>
                        <div id="payments<?=$j+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailPayment'][$j+$count]?>" onblur="closeTip('pay<?=$j+$count?>')" onfocus="openTip('pay<?=$j+$count?>')"></div>
                            
                        <?php 
                        } 
                    }
		}
                $displayPayment=displayStyle($j+$count);
		?>
                    <input type="hidden" name="validatePayment" id="validatePayment" value="<?=isset($_POST['validatePayment'])?$_POST['validatePayment']:0 ?>">
		</div>
		<input type="hidden" name="ajax_preferences_payment" id="ajax_preferences_payment" value="<?=isset($_POST['validatePayment'])?($_POST['validatePayment']+$j):$j?>">
		<span class="oh">
		<?php /*<a  class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_payment" style="display:<?=$displayPayment;?>;" onclick="remove_forwarder_profile('ajax_preferences_payment','payments','remove_forwarder_profile_payment','<?=$szPaymentEmailArys['id']?>','<?=$idForwarder?>','validatePayment');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a class="fr-70" href="javascript:void(0);" onclick="add_forwarder_profile('payment','Payment','validatePayment');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
            </div>
        </div>
		<?php
	}
		?>
<div style="clear: both;"></div>
<?php
	if(!empty($szCustomerServiceEmailAry) || !empty($_POST['updatePrefEmailAry']))
	{ 
		$k=1;
	?>
            <div class="profile-fields">
                <span class="field-name"><?=t($t_base.'titles/Customer_service');?></span> 
		<div class="field-container" style="width:35%;float:right;" id="forwarder_preferences_edit_customer">
		<div id="forwarder_preferences_customer">
            <?php	
		if(isset($_POST['updatePrefEmailAry']['szEmailCustomer']))
		{
                    $ij=$_POST['ajax_preferences_customer']-$_POST['validateCustomer']-1;
		
		}
		else
		{
                    $ij=count($szCustomerServiceEmailAry);
		}
		
		for($count=0;$ij>$count;$count++) 
		{
                    ?>
                    <div class="profile-fields" id="customers<?=$k;?>">
                        <input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailCustomer'][$k])?$_POST['updatePrefEmailAry']['szEmailCustomer'][$k]:$szCustomerServiceEmailAry[$count]['szEmail']?>"onblur="closeTip('cust<?=$k?>');" onfocus="openTip('cust<?=$k?>');">
                        <input type="hidden" name="updatePrefEmailAry[customerId][<?=$k;?>]" value="<?=$szCustomerServiceEmailAry[$count]['id']?>">  
                    </div>
                    <?php 
                    $k++;
		}  
		?>
		<?php $count=0;
		if(isset($_POST['validateCustomer']))
		{	$custo=(int)$_POST['validateCustomer'];
			if($custo>0)
			{ 
				for($count;$custo>$count;$count++)
				{ 
				?>
                            <div id="customers<?=$k+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailCustomer'][$k+$count]?>" onblur="closeTip('cust<?=$k+$count?>')" onfocus="openTip('cust<?=$k+$count?>')"></div>
					
			<?php	
				
				}
				
			}
		}
                $displayCustomer=displayStyle($k+$count);
		?>
		<input type="hidden" name="validateCustomer" id="validateCustomer" value="<?=isset($_POST['validateCustomer'])?$_POST['validateCustomer']:0 ?>">
		</div><input type="hidden" name="ajax_preferences_customer" id="ajax_preferences_customer" value="<?=isset($_POST['validateCustomer'])?($_POST['validateCustomer']+$k):$k;?>">
		<span class="oh">
		<?php /*<a class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_customer" style="display:<?=$displayCustomer;?>;" onclick="remove_forwarder_profile('ajax_preferences_customer','customers','remove_forwarder_profile_customer','<?=$szCustomerServiceEmailArys['id']?>','<?=$idForwarder?>','validateCustomer');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a class="fr-70" style="width: auto;;" href="javascript:void(0);" onclick="add_forwarder_profile('customer','Customer','validateCustomer');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
            </div>
        </div>
<?php		
	}
	?>
 	<?php if(empty($szCustomerServiceEmailAry) && empty($_POST['updatePrefEmailAry']) ){
		$k=1;
		
	?>
            <div class="profile-fields">
                <span class="field-name"><?=t($t_base.'titles/Customer_service');?></span> 
		<div class="field-container" style="width:35%;float:right;" id="forwarder_preferences_edit_customer">
		<div id="forwarder_preferences_customer"> 
                    <div class="profile-fields" id="customers<?=$k;?>">
                        <input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailCustomer'][$k])?$_POST['updatePrefEmailAry']['szEmailCustomer'][$k]:''?>"onblur="closeTip('cust<?=$k?>');" onfocus="openTip('cust<?=$k?>');">
                        <input type="hidden" name="updatePrefEmailAry[customerId][<?=$k;?>]" value="<?=$szCustomerServiceEmailAry[$count]['id']?>">
                    </div>
		<?php  	$k++;$count=0;
		if(isset($_POST['validateCustomer']))
		{	$custo=(int)$_POST['validateCustomer'];
			if($custo>0)
			{ 
				for($count;$custo>$count;$count++)
				{ 
				?>
					<div id="customers<?=$k+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailCustomer'][$k+$count]?>" onblur="closeTip('cust<?=$k+$count?>')" onfocus="openTip('cust<?=$k+$count?>')"></div>
					
			<?php	
				
	 			}
				
			}
		}
                $displayCustomer=displayStyle($k+$count);
	
		?>
		<input type="hidden" name="validateCustomer" id="validateCustomer" value="<?=isset($_POST['validateCustomer'])?$_POST['validateCustomer']:0 ?>">
		<input type="hidden" name="ajax_preferences_customer" id="ajax_preferences_customer" value="<?=isset($_POST['validateCustomer'])?($_POST['validateCustomer']+$k):$k;?>">
            </div>
                <span class="oh">
		<?php /*<a class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_customer" style="display:<?=$displayCustomer;?>;" onclick="remove_forwarder_profile('ajax_preferences_customer','customers','remove_forwarder_profile_customer','<?=$szCustomerServiceEmailArys['id']?>','<?=$idForwarder?>','validateCustomer');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a class="fr-70" style="width: auto;;" href="javascript:void(0);" onclick="add_forwarder_profile('customer','Customer','validateCustomer');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
            </div>
        </div>
<?php		
	}
	?>	
	<div style="clear: both;"></div>
	<div class="oh">
            <p class="fl-20">&nbsp;</p>
            <p style="float:right"><br>
            		<a href="javascript:void(0);" onclick="show_preference_emails('<?=$idForwarder?>')"  class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>	
                    <a href="javascript:void(0);" onclick="submit_preference_emails()"  class="button1"><span><?=t($t_base.'fields/save');?></span></a>
                    			
                    <input type="hidden" name="mode" value="EDIT_EMAIL_ADDRESS">
                    <input type="hidden" name="removeEmails" value="0" id="removeMails">
                    <input type="hidden" name="id" value="<?=$_SESSION['forwarder_id'];?>">
            </p>
	</div>
    </form>
</div>
	<?php
}
else
{ 
    $idForwarder=$_SESSION['forwarder_id'];
    $forwarderContactAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder);
    echo display_forwarder_setting_emails($forwarderContactAry);
						 
} 

?>
<br /><br />
<?php
if($operation_mode=='EDIT_SYSTEM_CONTROL')
{
    $idForwarder=$_SESSION['forwarder_id'];
    $forwarderControlPanelArr=$kForwarderContact->getSystemPanelDetails($idForwarder);	
    $allCurrencyArr = array();
    $kConfig = new cConfig();	
    $allCurrencyArr=$kConfig->getBookingCurrency(false,false,true);
    
    $kForwarderNew = new cForwarder();
    $paymentFrequency = $kForwarderNew->getAllPaymentFrequency();
?>	
<div id="update_preference_control">
	<div id="ajax_edit_mode">
	<?php
	if(!empty($kForwarderContact->arErrorMessages)){
		?>
		<div id="regError" class="errorBox">
		<div class="header"><?=t($t_base_error.'/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kForwarderContact->arErrorMessages as $key=>$values)
			{
			?><li><?=$key." ".$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<?php } ?>
	<form id="ajax_prefrence_control" method="post">
	<h4><strong><?=t($t_base.'titles/system_config');?></strong></h4>					

					<!--  <div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
					
						
						<?
							if(isset($kForwarderContact->controlBooking))
							{
								$book=$kForwarderContact->controlBooking;
							}
							else
							{
								$book=$forwarderControlPanelArr['BookingFrequency'];
							}
						?>
						<select name="updatePrefControlArr[szBooking]" onblur="closeTip('booking_reports');" onfocus="openTip('booking_reports');">
						<?
							foreach($bookingReports as $key=>$value)
							{
							echo '<option value="'.($key+1) .'"';
							if($key+1==$book)
							{
								echo "selected=selected";
							}
							echo '>'.$value.'</option>';
								
							}
						?>
						</select>
						<div class="field-alert"><div id="booking_reports" style="display:none;"><?=t($t_base.'messages/booking_reports');?></div></div>
					</div>					
					
					-->  
					<div class="profile-fields">
                                            <span class="field-name"><?=t($t_base.'fields/payment_currency');?></span> 
                                            <select size="1" style="float:right;width:35%;" name="updatePrefControlArr[szCurrency]" id="szCurrency" onblur="closeTip('exchange');" onfocus="openTip('exchange');">
                                                <?php 
                                                    if(!empty($allCurrencyArr))
                                                    {
                                                        foreach($allCurrencyArr as $allCurrencyArrs)
                                                        {
                                                            ?><option value="<?=$allCurrencyArrs['id']?>" <?=(($forwarderControlPanelArr['idCurrency']==$allCurrencyArrs['id']) ? "selected":"")?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                                                        }
                                                    }
                                                ?>
                                            </select> 
					</div>	 
					<div class="oh">
		<p class="fl-20">&nbsp;</p>
		<p style="float:right"><br>
			
			<a href="javascript:void(0);" onclick="show_preference_controls('<?=$idForwarder?>')"  class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>
			<a href="javascript:void(0);" onclick="submit_preference_controls()"  class="button1"><span><?=t($t_base.'fields/save');?></span></a>			
			<input type="hidden" name="mode" value="<?=$operation_mode?>">
			<input type="hidden" name="id" value="<?=$_SESSION['forwarder_id'];?>">
			
			<input type="hidden" name="updatePrefControlArr[szCurrency_hidden]" id="szCurrency_hidden" value="<?=$forwarderControlPanelArr['idCurrency']?>">
		</p>
	</div>
</form>	
</div>
<?php			

}
else  
{   
    //echo forwarder_system_control_emails(); 
}
?>
