<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$szMetaTitle="Transporteca | Setup Airport Warehouses";

require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );

constantApiKey();

$Excel_export_import=new cExport_Import();
$kConfig = new cConfig();
$t_base = "BulkUpload/";
$t_base_error="Error";

if((int)$_SESSION['forwarder_user_id']==0)
{
    header('Location:'.__BASE_URL__.'/forwarders/');
    exit();	
}
$kWHSSearch = new cWHSSearch(); 
$iWarehouseType = __WAREHOUSE_TYPE_AIR__;
?>
<style type="text/css">
      #map_canvas { height: 100% }     
      .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
      .map-content p{font-size:12px !important;}
 </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=<?=__GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
    </script>
    <script type="text/javascript">
      var originIcon = '<?=__BASE_STORE_IMAGE_URL__?>' + "/blue-marker.png";
	  var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
   // var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(id)
    {	
    	var pipe_line_string = '';
    	/*
    	$.post(__JS_ONLY_SITE_BASE__+"/ajax_googleMap.php",function(result){
			$("#show_on_map_pipe_line").attr("value",result);
		});
		*/
    	pipe_line_string = $("#show_on_map_pipe_line").attr("value");
       	var result_ary = pipe_line_string.split("||||||");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	addPopupScrollClass('map_popup_div');
    	initialize(result_ary);
    }
    	  
	function initialize(result_ary) 
	{	
	  var length_pipeline = result_ary.length;
	  var origin_lat = $("#average_latitude_pipe_line").attr("value");
	  var origin_long = $("#average_longitute_pipe_line").attr("value");
	   var myLatLng = new google.maps.LatLng(origin_lat, origin_long);
	   var myOptions = {
		zoom: 2,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	 $("#distance_div").html('<?=t($t_base.'fields/your_companies_registered_airport_warehouse');?>');
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(origin_lat, origin_long)  
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);
	  
	  for(i=0;i<length_pipeline;i++)
	  {
	  	result_arr = result_ary[i].split("||||");
	  	var szLat = result_arr[0];
		var szLang = result_arr[1];
		var whs_name = result_arr[2];
		var origin_address = result_arr[3];
		 
		var origin = new google.maps.LatLng(szLat, szLang);  
		var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:whs_name
	  	}); 
	  	
	     var oaddress = '<div class="map-content"><p>'+origin_address+'</p></div>';
		 var oinfowindow = new google.maps.InfoWindow();
		 oinfowindow.setContent(oaddress);
		 google.maps.event.addListener(
			originmarker, 
			'click', 
			infoCallback(oinfowindow, originmarker)
		  );
	   	  //oinfowindow.open(map,originmarker);  
   	
	  }

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
}

function help_me_to_select()
{
  var latitude = $("#szLatitude").attr("value");
  var logitude = $("#szLongitude").attr("value");
  if(latitude!='' && logitude!='')
  {
  	  var result_ary=new Array();
	  result_ary[0]= latitude
	  result_ary[1]= logitude
	  result_ary[2]= "address needs to implement ";
	  $("#help_me_to_select_div").attr("style","display:block;");
	  $("#help_me_to_select_div").focus();
	  initialize_hep_me_select(result_ary);
  }
  else
  {
  	alert("latitute and longitude are required fields");
  } 
}
function initialize_hep_me_select()
{
	var olat = result_ary[0];
	var olang = result_ary[1];
	var origin_address = result_ary[2];
	 
	  var origin = new google.maps.LatLng(olat, olang);  
	  var origin1 = new google.maps.LatLng(olat+1, olang+1);  
	  var myLatLng = new google.maps.LatLng(olat, olang);
	  
	  var myOptions = {
		zoom: 1,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	 
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(olat, olang)  
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
	  
	  var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"Origin"
	  }); 

	  var oaddress = '<div class="map-content"><p>'+origin_address+'</p></div>';
	  var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker, 
		'click', 
		infoCallback(oinfowindow, originmarker)
	  );
   	oinfowindow.open(map,originmarker);     
   	
   	
   	var originmarker1 = new google.maps.Marker({
		  position: origin1,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"Origin1"
	  }); 

	  var oaddress = '<div class="map-content"><p>'+origin_address+'</p></div>';
	  var oinfowindow2 = new google.maps.InfoWindow();
	  oinfowindow2.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker1, 
		'click', 
		infoCallback(oinfowindow2, originmarker1)
	  );
   	oinfowindow2.open(map,originmarker1); 
}

function updateValue(latitude, longitude)
{
    document.getElementById("szLatitude").value = latitude ;
	document.getElementById("szLongitude").value = longitude ;
}
</script>

<div id="hsbody-2">
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader"></div>				
        <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
    </div>
    <div id="map_popup_div" style="display:none;">		
        <div id="popup-bg"></div>
        <div id="popup-container">			
            <div class="popup map-popup" style="margin-top:30px;">
                <p class="close-icon" align="right">
                    <a onclick="showHide('map_popup_div');" href="javascript:void(0);">
                        <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                    </a>
                </p>
                <div id="distance_div" style="font-weight:bold"></div><br>
                <div id="map_canvas" style="width:500px;height:500px;border: 1px solid #C4BDA1;"></div>
                <div class="map_note">
                    <p><?=t($t_base.'fields/notes');?>: <?=t($t_base.'fields/google_map_notes');?></p>
                </div>
                <p align="center"><a href="javascript:void(0);" onclick="showHide('map_popup_div');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
            </div>
        </div>
    </div>
  
    <div class="hsbody-2-left account-links">			
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
    </div>
    <div class="hsbody-2-right">
        <p><?=t($t_base.'title/airport_warehouse_location_top_heading');?></p>
        <br />
        <?php require_once( __APP_PATH__ ."/forwarders/ajax_cfsNameList.php" ); ?> 
        <br />
        <?php require_once( __APP_PATH__ ."/forwarders/addUpdateCFSDetails.php" ); ?> 
    </div>
</div>
    <input type="hidden" name="iScrollValue" value="" id="iScrollValue" />
    <input type="hidden" name="iScrollDivValue" value="" id="iScrollDivValue" />
    <form method="post" id="google_map_hidden_form" action="<?=__FORWARDER_HOME_PAGE_URL__?>/googleMap.php" target="google_map_target_1">
        <input type="hidden" name="cfsHiddenAry[szPostCode]" id="szPostCode_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[szAddressLine1]" id="szAddressLine1_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[szAddressLine2]" id="szAddressLine2_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[szAddressLine3]" id="szAddressLine3_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[szCity]" id="szCity_hidden" value=""> 
        <input type="hidden" name="cfsHiddenAry[szState]" id="szState_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[szCountry]" id="szCountry_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[szLatitude]" id="szLatitude_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[szLongitude]" id="szLongitude_hidden" value="">
        <input type="hidden" name="cfsHiddenAry[iWarehouseType]" id="iWarehouseType_hidden" value="<?php echo $iWarehouseType;?>">
    </form>

    <!-- style="width:600px;height:380px;border:0px solid #fff;" -->
    <div id="popup_container_google_map" style="display:none;">
        <div id="popup-bg"></div>
        <div id="popup-container">
            <div class="popup" style="width:720px;margin-top:30px;">
                <p class="close-icon" align="right">
                    <a href="javascript:void(0);" onclick="window.top.window.hide_google_map();">
                        <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                    </a>
                </p>
                <iframe id="google_map_target_1" class="google_map_select"  style="width:720px;"  scrolling="no" name="google_map_target_1" src="#" >
                </iframe>
            </div>
        </div>
    </div>
<?php
    include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>	