<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "welcomePage/";
?> 
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/style.css" />
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
<script type="text/javascript">
/*
function hide_welcome_video()
{
	parent.document.getElementsByTagName('body').class='';	
	parent.document.getElementsByTagName('body').style='' ;	
	parent.document.getElementById('popup_container_welcome_video').style.display = 'none';
	parent.document.getElementById('google_map_target_1').src = '';
	//$("#google_map_target_1",parent.document).src(' ');
	//parent.document.getElementById('mainClass').style.overflow = 'none';
	
	$("#mainClass",parent.document).removeClass("popupscroll");
	$('#mainClass',parent.document).removeAttr('style');
	$('.mainpage',parent.document).removeAttr('id');
}
*/
</script>
<div align="center" valign="middle">
<?php
$type = trim($_REQUEST['type']);
$style_close_button = 'style="position:absolute;top:585px;left:288px;"';
if($type=='cfs')
{
    ?>

    <p align="left" style="font-weight:bold;font-size:16px;"><?=t($t_base.'titles/cfs_location');?></p>
    <br>
    <p>
        <div id="evp-e138857862e8ab41d60803328f234158-wrap" class="evp-video-wrap"></div>
        <script type="text/javascript" src="http://transporteca.evplayer.com/framework.php?div_id=evp-e138857862e8ab41d60803328f234158&id=Y2ZzLWxvY2F0aW9ucy0xLm1wNA%3D%3D&v=1346874466&profile=default"></script>
        <script type="text/javascript">
                _evpInit('Y2ZzLWxvY2F0aW9ucy0xLm1wNA==[evp-e138857862e8ab41d60803328f234158]');//-->
        </script>
    </p> 
    <?php
}
else if($type=='service_rate')
{
    ?>
	<p align="left" style="font-weight:bold;font-size:16px;"><?=t($t_base.'titles/services_and_rates');?></p>
	<br>
	<p>
            <div id="evp-4b13832b23e846de7f128853ca2a29b2-wrap" class="evp-video-wrap"></div>
            <script type="text/javascript" src="http://transporteca.evplayer.com/framework.php?div_id=evp-4b13832b23e846de7f128853ca2a29b2&id=cHJpY2luZy1hbmQtc2VydmljZXMtMS5tcDQ%3D&v=1346929438&profile=default"></script><script type="text/javascript"><!--
                _evpInit('cHJpY2luZy1hbmQtc2VydmljZXMtMS5tcDQ=[evp-4b13832b23e846de7f128853ca2a29b2]');//-->
            </script> 
	</p>
<?php	
}
else if($type=='customer')
{
    ?>
    <p align="left" style="font-weight:bold;font-size:16px;"><?=t($t_base.'titles/try_it_out');?></p>
    <br>
    <p>
        <div id="evp-8382be78aa677439d6bb8bd2d42e9e56-wrap" class="evp-video-wrap"></div>
        <script type="text/javascript" src="http://transporteca.evplayer.com/framework.php?div_id=evp-8382be78aa677439d6bb8bd2d42e9e56&id=dHJ5LWl0LW91dC0xLm1wNA%3D%3D&v=1346977899&profile=default"></script>
        <script type="text/javascript">
         _evpInit('dHJ5LWl0LW91dC0xLm1wNA==[evp-8382be78aa677439d6bb8bd2d42e9e56]');
         </script>
    </p>
    <?php
}
else if($type=='lcl_bulk')
{
    $style_close_button = 'style="position:absolute;top:445px;left:288px;"';
    $kWarehouse = new cWHSSearch();	
    $lclServiceVideo=$kWarehouse->getManageMentVariableByDescription('__LCL_SERVICE_BULK_VIDEO__');	
    $youtubeId=get_youtube_id($lclServiceVideo);
    ?>
    <p align="left" style="font-weight:bold;font-size:16px;"><?=t($t_base.'titles/lcl_service');?></p>
    <br>
    <p>
        <center>
            <iframe id="google_map_target_video_popup" style="height:405px;width:719px;" align="center" valign="middle" class="google_map_select"  name="google_map_target_video_popup" src="https://www.youtube.com/embed/<?php echo $youtubeId;?>?autoplay=1&rel=0" frameborder="0" allowfullscreen ></iframe>
        </center>
    </p>
    <?php
}
else if($type=='cc_bulk')
{
	$style_close_button = 'style="position:absolute;top:445px;left:288px;"';
        $kWarehouse = new cWHSSearch();	
        $lclServiceVideo=$kWarehouse->getManageMentVariableByDescription('__CUSTOM_CLEARANCE_BULK_VIDEO__');	
        $youtubeId=get_youtube_id($lclServiceVideo);
	?>
	<p align="left" style="font-weight:bold;font-size:16px;"><?=t($t_base.'titles/customs_clearance');?></p>
	<br>
	<p>
            <center>
            <iframe id="google_map_target_video_popup" style="height:405px;width:719px;" align="center" valign="middle" class="google_map_select"  name="google_map_target_video_popup" src="https://www.youtube.com/embed/<?php echo $youtubeId;?>?autoplay=1&rel=0" frameborder="0" allowfullscreen ></iframe>
        </center>
	</p>
	<?php
}
else if($type=='haulage_bulk')
{
	$style_close_button = 'style="position:absolute;top:445px;left:288px;"';
        $kWarehouse = new cWHSSearch();	
        $lclServiceVideo=$kWarehouse->getManageMentVariableByDescription('__HAULAGE_BULK_VIDEO__');	
        $youtubeId=get_youtube_id($lclServiceVideo);
	?>
	<p align="left" style="font-weight:bold;font-size:16px;"><?=t($t_base.'titles/haulage');?></p>
	<br>
	<p>
	<center>
            <iframe id="google_map_target_video_popup" style="height:405px;width:719px;" align="center" valign="middle" class="google_map_select"  name="google_map_target_video_popup" src="https://www.youtube.com/embed/<?php echo $youtubeId;?>?autoplay=1&rel=0" frameborder="0" allowfullscreen ></iframe>
        </center>
	</p>
	<?
}
else
{
?>
	<p align="left" style="font-weight:bold;font-size:16px;"><?=t($t_base.'titles/standard_company_information');?></p>
	<br>
	<p>
		<div id="evp-74cb0c9cb6c4508ec8cf25c3f3bd8a59-wrap" class="evp-video-wrap"></div>
		<script type="text/javascript" src="http://transporteca.evplayer.com/framework.php?div_id=evp-74cb0c9cb6c4508ec8cf25c3f3bd8a59&id=Y29tcGFueS1pbmZvcm1hdGlvbi0xLm1wNA%3D%3D&v=1346848035&profile=default"></script>
		<script type="text/javascript">
				_evpInit('Y29tcGFueS1pbmZvcm1hdGlvbi0xLm1wNA==[evp-74cb0c9cb6c4508ec8cf25c3f3bd8a59]');
		</script>
	</p>
<?
}
?>
<br>
<p <?=$style_close_button?> align="center"><a href="javascript:void(0);" class="button1" onclick="window.top.window.hide_google_map();"><span><?=t($t_base.'titles/close');?></span></a></p>
</div>