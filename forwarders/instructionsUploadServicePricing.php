<?php
/**
 * Forwarder upload services - instructions 
 */
ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
$szMetaTitle="Transporteca | Upload service - Instructions";
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();
$t_base = "BulkUpload/";
$t_base_file ="Forwarders/Instruction/";
$t_base_error="Error";
?>
<div id="hsbody-2">
	<div class="hsbody-2-left account-links">
		<?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
	</div>
	<div class="hsbody-2-right link16">
		<h4><b><?=t($t_base_file.'title/transporteca_upload_serv');?></b></h4><br />
		<p><?=t($t_base_file.'messages/first_time_you');?></p>
		<br />
		<p><?=t($t_base_file.'messages/transporteca_team');?></p>
		<br />
		<p><?=t($t_base_file.'title/the_process_for');?>:</p>
		<br />
		<ol>
			<li><?=t($t_base_file.'messages/collect_the_data');?></li>
			<li><?=t($t_base_file.'messages/upload_the_file');?> <a href="<?=__FORWARDER_BULK_UPLOAD_SERVICES_URL__?>"><?=t($t_base_file.'messages/here');?></a></li>
			<li><?=t($t_base_file.'messages/we_have_review');?></li>
			<li><?=t($t_base_file.'messages/you_have_recieve');?></li>
			<li><?=t($t_base_file.'messages/review_and_approve');?> <a href="<?=__FORWARDER_BULK_SERVICES_APPROVAL_URL__?>"><?=t($t_base_file.'messages/here');?></a></li>
		</ol>
		<br />
		<p><?=t($t_base_file.'messages/service_rates');?></p>
		<br />
		<p><?=t($t_base_file.'messages/files_recieved');?></p>
		<br />
		<p><?=t($t_base_file.'messages/service_available');?></p>
		<br />
		<p class="f-size-10" ><i><?=t($t_base_file.'messages/note');?>: <?=t($t_base_file.'messages/transporteca_is_not');?></i> </p>
	</div>
</div>
<?php include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" ); ?>