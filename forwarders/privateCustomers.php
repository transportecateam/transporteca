<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
$display_profile_not_completed_message=true;
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle = "Transporteca | Setup private customer acceptance";
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" ); 
$t_base = "BulkUpload/";
$t_base_error="Error";
$kForwarder = new cForwarder(); 
$privateCustomerFeeAry = array();
$privateCustomerFeeAry = $kForwarder->getAllPrivateCustomerPricing($idForwarder);
//$kForwarder->dummyPrivateCustomerPricing(); 
?>
<div id="hsbody-2">
    <div class="hsbody-2-left account-links">
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); 
            $t_base = "privateCustomers/";
        ?>
    </div>
    <div class="hsbody-2-right"> 
        <h5><strong><?=t($t_base.'title/private_customer');?></strong></h5>
        <p><?=t($t_base.'messages/private_customer_descriptions');?></p> 
        <div style="clear: both;"></div><br> 
        <div id="forwarder_private_customer_setting_container">
            <?php
                echo display_forwarder_private_customer_setting($kForwarder,$privateCustomerFeeAry);
            ?>
        </div>
    </div>

<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>