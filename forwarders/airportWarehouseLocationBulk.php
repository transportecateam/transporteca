<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$szMetaTitle="Transporteca | Airport Warehouse Locations";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();
$iWarehouseType = __WAREHOUSE_TYPE_AIR__;
$t_base = "BulkUpload/";
$t_base_error="Error";
$idForwarderContact = $_SESSION['forwarder_user_id'];
if(isset($_SESSION['completed']))
{
    $content = $_SESSION['completed'];
    confirmationPOPup($content,$iWarehouseType);
} 

if(isset($_POST))
{ 
    require_once( __APP_PATH__ ."/forwarders/ajax_bulkCfsUpload.php" );
}    
?> 
<div id="hsbody-2"> 
    <div class="hsbody-2-left account-links">
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
    </div>
    <div class="hsbody-2-right"> 
        <p><?=t($t_base.'messages/this_function_airport_cfs_bulk_upload')?> <a href="<?=__FORWARDER_AIRPORT_WAREHOUSE_URL__?>" style="font-size: 16px;"><?=t($t_base.'links/here')?></a>.</p><br /> 
        <ol>
            <li><?=t($t_base.'messages/download_airport_warehouse_loc_one_by_one')?></li>
            <p style="padding-top: 10px;padding-bottom: 10px;text-align: center;"><a class="button1" onclick="download_cfs_file_format('DOWNLOAD_CFS_EXCEL','<?php echo $iWarehouseType; ?>')"><span><?=t($t_base.'fields/download')?></span></a></p>
            <br />
            <li><?=t($t_base.'messages/update_file_with_airport_cfs_locations')?>. </li>
            <p style="padding-top: 10px;"><?=t($t_base.'messages/latitude_and_longitude_airport_warehouse')?> <a href="<?=__FORWARDER_HOME_PAGE_URL__?>/googleMapAssist.php?iWarehouseType=2" target="google_map_target_1" onclick="toolLatLongPopup();" style="font-size: 16px;"> <?=t($t_base.'links/this_tool_can_assist')?></a>.</p>
            <br /><br />
            <li><?=t($t_base.'messages/when_you_have_completed_airport_warehouse')?></li>
            <div class="file">
                <form id="uploadCfsFile" name ="uploadCfsFile" method="post" enctype="multipart/form-data">
                    <input id="fileUpload" type="file" name="cfsBulkUpload">
                    <input type="hidden" name="mode" value="UPLOAD_CFS_BULK_LISTING">
                    <span class="button">No file selected - click browse to select file for upload</span>
                </form>
            </div>
            <p style="padding-top: 10px;padding-bottom: 10px;text-align: center;"><a class="button1" onclick="upload_cfs_file();"><span><?=t($t_base.'fields/upload')?></span></a></p>
        </ol>
        <div id="popup_container_google_map" style="display:none;">
            <div id="popup-bg"></div>
            <div id="popup-container">
                <div class="popup" style="width:720px;margin-top:30px;">
                    <p class="close-icon" align="right">
                        <a href="javascript:void(0);" onclick="window.top.window.hide_google_map();">
                            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                        </a>
                    </p>
                    <iframe id="google_map_target_1" class="google_map_select" style="width:720px;"  scrolling="no" name="google_map_target_1" src="#" ></iframe>
                </div>
            </div>
        </div>			
    </div> 	
</div>
<?php include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" ); ?>				