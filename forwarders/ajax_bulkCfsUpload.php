<?php  
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 

$mode = trim(sanitize_all_html_input($_REQUEST['mode']));
if($mode=='UPLOAD_CFS_BULK_LISTING')
{	
    $Excel_export_import=new cExport_Import();
    $staus = $Excel_export_import->uploadCFSLocationTemplate($idForwarder,$idForwarderContact,$_FILES['cfsBulkUpload'],$iWarehouseType);
    if($staus==false)
    {	
        if(!empty($Excel_export_import->arErrorMessages))
        {
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                $szErrorPopupHeading = t($t_base_error.'/invalid_file_format');
                $szErrorPopupText = t($t_base_error.'/we_did_not_recg_air');
            }
            else
            {
                $szErrorPopupHeading = t($t_base_error.'/invalid_file_format');
                $szErrorPopupText = t($t_base_error.'/we_did_not_recg_cfs');
            }
            ?>
            <script type="text/javascript">
                addPopupScrollClass('error-bg');
            </script>
            <div id="error-bg">
                <div id="popup-bg"></div>
                <div id="popup-container" style="padding-top: 12%">
                    <div class="popup">
                        <p class="close-icon" align="right">
                            <a onclick="close_popup_content('<?php echo $iWarehouseType; ?>');" href="javascript:void(0);">
                                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                            </a>
                        </p>
                        <h4><b><?php echo $szErrorPopupHeading; ?></b></h4><br />
                        <p><?php echo $szErrorPopupText; ?></p><br />
                        <P align="center"><a class="button1" onclick="close_popup_content('<?php echo $iWarehouseType; ?>');"><span><?=t($t_base_error.'/close')?></span></a></P>
                    </div>
                </div>	
            </div>		
            <?php
        }
    }
    else
    {	 
        if($staus[0])
        {
            $_SESSION['idBatch'] = $staus[1];
            $_SESSION['option']  = 'cfsUpload';
            $_SESSION['iWarehouseType']  = $iWarehouseType; 
            
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                header('location:/airportLocationBulk/');
                die;
            }
            else
            {
                header('location:/CFSLocationBulk2/');
                die; 
            } 
        }
    }
}



?>