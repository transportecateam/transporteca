<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca | Bookings";

require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
require_once( __APP_PATH__ . "/forwarders/listExcelData.php" );
$kBooking=new cBooking();
$kConfig = new cConfig();
$kUser = new cUser();
$kCourierServices = new cCourierServices();

require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php");
checkAuthForwarder();

$idForwarder =$_SESSION['forwarder_id'];
$t_base = "ForwarderBookings/";
$t_base_error="Error";
$booking_searched_data = array();
$data = array();
$data['iBookingYear']= date('Y');
$data['iBookingMonth']= date('m');

//echo " session ".$_SESSION['forwarder_booking_confirmation_key'] ;

if((int)$_SESSION['forwarder_booking_confirmation_key']>0)
{
    $szConfirmationKey = $_SESSION['forwarder_booking_confirmation_key'];
    $kBooking->load($szConfirmationKey);
    $insertNotAcknowledgeFlag=true;
    if($kBooking->iCourierBooking==1)
    {
        $courierBookingArr=$kCourierServices->getCourierBookingData($Booking_Id);
        if($courierBookingArr[0]['iCourierAgreementIncluded']==0)
        {
            $insertNotAcknowledgeFlag=false;
        }    
    }
    if($kBooking->idForwarder == $idForwarder)
    {
        if($insertNotAcknowledgeFlag)
        {
            $downloadedByAry = $kBooking->getAcknowledgeDownloadedByUserDetails($_SESSION['forwarder_booking_confirmation_key'],$idForwarder);
            if(empty($downloadedByAry))
            {
                $kBooking->sendAcknowlodgementEmail($szConfirmationKey,$idForwarder);
            }
        }
        $szConfirmationKey = $_SESSION['forwarder_booking_confirmation_key'] ;
        /*
         * 20170129 Financial Revamp – step 1 Removed 31-Jan-2017
         */
        /*if($kBooking->iHandlingFeeApplicable==1 || (float)$kBooking->fTotalForwarderManualFee>0.00)
        {
            $redirect_url = __FORWARDER_HOME_PAGE_URL__.'/forwarderBooking/'.$szConfirmationKey.'/booking/';	
        }
        else
        {*/
            $redirect_url = __FORWARDER_HOME_PAGE_URL__.'/forwarderBooking/'.$szConfirmationKey.'/';
        //}
        $_SESSION['forwarder_booking_confirmation_key']='';
        unset($_SESSION['forwarder_booking_confirmation_key']);	
        ob_clean();
        header("Location:".$redirect_url);
        die;
    } 
}
else if((int)$szConfirmationKey>0)
{
    $downloadedByAry = $kBooking->getAcknowledgeDownloadedByUserDetails($szConfirmationKey,$idForwarder);
    $kBooking->load($szConfirmationKey);
    $insertNotAcknowledgeFlag=true;
    if($kBooking->iCourierBooking==1)
    {
        $courierBookingArr=$kCourierServices->getCourierBookingData($Booking_Id);
        if($courierBookingArr[0]['iCourierAgreementIncluded']==0)
        {
            $insertNotAcknowledgeFlag=false;
        }    
    }
    if($kBooking->idForwarder == $idForwarder)
    {
        if(empty($downloadedByAry) && $insertNotAcknowledgeFlag)
        {   
            //send notification email to customer
            $kBooking->sendAcknowlodgementEmail($szConfirmationKey,$idForwarder);
        }		
       
        $_SESSION['forwarder_booking_confirmation_key']='';
        unset($_SESSION['forwarder_booking_confirmation_key']);	
        /*
         * 20170129 Financial Revamp – step 1 Removed 31-Jan-2017
         */
        /*if($kBooking->iHandlingFeeApplicable==1 || (float)$kBooking->fTotalForwarderManualFee>0.00)
        {
            $redirect_url = __FORWARDER_HOME_PAGE_URL__.'/forwarderBooking/'.$szConfirmationKey.'/booking/';	
        }
        else
        {*/
            $redirect_url = __FORWARDER_HOME_PAGE_URL__.'/forwarderBooking/'.$szConfirmationKey.'/';
        //}	
        ob_clean();
        header("Location:".$redirect_url);
        die;
    }  
} 
//echo $_SESSION['forwarder_booking_confirmation_key'] ;
$booking_searched_data = $kBooking->search_forwarder_bookings($data);

if(!empty($_POST['bookingSearchAry']))
{ 
    $file_name = $kBooking->download_booking_table($_POST['bookingSearchAry']);
    if(!empty($file_name))
    {
        header("Location:".__FORWARDER_HOME_PAGE_URL__.'/forwarderBulkExportImport/forwarderDetails/'.$file_name.'.xlsx');
        $file_path = __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/forwarderDetails/$mode.xlsx" ;
        if(file_exists($file_path))
        {
            @unlink($file_path);
        }	
        exit;
    }
}
?>
<script type="text/javascript">
$().ready(function() {

$("#datepicker1").datepicker();
$("#datepicker2").datepicker();

});
</script>
<div id="customs_clearance_pop" class="help-pop"></div>
<div id="customs_clearance_pop_right" class="help-pop right">
</div>
<div id="Transportation_pop" style="display:none;"></div>
<div id="hsbody">


    <form action="<?=__FORWARDER_HOME_PAGE_URL__?>/download_pdf.php" method="post" id="download_booking_pdf_form">
        <input type="hidden" name="createPDF[idBooking]" value="" id="pdf_booking_id">
    </form>
    <div id="forwarder_booking_top_form">
        <?php
            echo forwarder_booking_search_form($t_base);
        ?>
    </div>	
    <div style="clear: both"></div>
    <div id="forwarder_booking_bottom_form" class="oh">
    <?php
            echo forwarder_booking_bottom_html($booking_searched_data,$t_base);
    ?>
    </div>
    <input type='hidden' id='iBookingPage' name='iBookingPage' value='1'/>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>	