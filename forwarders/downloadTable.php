<?php
/**
 * Edit User Information
 */
 
  ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$kBooking = new cBooking();
$kBilling= new cBilling();
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php");
$t_base = "ForwarderBillings/";
checkAuthForwarder_ajax();

if(!empty($_REQUEST['id']))
{ 
	$mode=sanitize_all_html_input($_REQUEST['id']);
	$format = 'Y-m-d'; 
	if($mode=='1')
	{
		$toDate=date($format);
		$fromDate=date($format,strtotime('-1 week'.$toDate));
	}
	if($mode=='2')
	{
		$toDate=date($format);
		$days=date('d')-1;
		$fromDate=date($format,strtotime('-'.$days.' days'.$toDate));
	}
	if($mode=='3')
	{
		$days=date('d')-1;
		$tdays=date('d');
		$toDate=date($format,strtotime('-'.$tdays.' days '.date($format)));
		$Date=date($format,strtotime('-'.$days.' days '.date($format)));
		$fromDate=date($format,strtotime('-1 month'.$Date));
	}
	if($mode=='4')
	{
		$days=date('d')-1;
		$toDate=date($format);
		$fromDate=date($format,strtotime('-3 MONTH'));
	}
	if($mode=='5')
	{
		$currentArr=get_quarter();
		$toDate=date($format,strtotime($currentArr['end']));
		$fromDate=date($format,strtotime($currentArr['start']));
	}
	if($mode=='6')
	{
		$prevquaArr=get_quarter(1);
		$toDate=date($format,strtotime($prevquaArr['end']));
		$fromDate=date($format,strtotime($prevquaArr['start']));
	}
	if($mode=='7')
	{
		$days=date('d')-1;
		$month=date('m')-1;
		$toDate=date($format);
		$fromDate=date($format,strtotime('-'.$days.' days -'.$month.' month'.$toDate));
	}
	if($mode=='8')
	{
		$todays=date('d');	
		$days=date('d')-1;
		$month=date('m')-1;
		$toDate=date($format,strtotime('-'.$todays.' days -'.$month.' month'.date($format)));
		$Dateto=date($format,strtotime('-'.$days.' days -'.$month.' month'.date($format)));
		$fromDate=date($format,strtotime('-1 year'.$Dateto));
	}
	if($mode=='9')
	{
            $fromDate= '2012-01-01'; //Launch date
            $toDate= date('Y-m-d');
	}
	if($fromDate!='')
	{
            $date = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
            $date="  BETWEEN '".$fromDate."' and '".$date."'";
	}
	else
	{
            $date='';
	}
	if($fromDate)
	{
            $dateRange= 'Date range: '.date('d/m/Y',strtotime($fromDate)).' - '.date('d/m/Y',strtotime($toDate));
	}
        if(!empty($date))
        {   
            $dtPaymentConfirmed = " dtPaymentConfirmed";
            $dtInvoiceOn = " dtInvoiceOn ";
            $dtPaymentScheduled = " dtInvoiceOn "; 
            
            /*
             * In following query we have 3 cases 
             * Case - 1: This block will fetch all the 'Automatic Transfer' records which not have been confirmed yet. I mean all the record which on Management /forwarderTransfer/ screen
             * Case - 2: This section will fetch all the record which has been paid. In system we consider those record as paid where iStatus = 2 and dtPaymentConfirmed <> NULL
             * Case - 3: This section will fetch all the bookings where payment has been been received to Transporteca. I mean all the booking which is on Management > /outstandingPayment/ screen. 
             */
            $date = "	 
                (
                    (
                            iDebitCredit = 2
                        AND
                            iStatus = '1'
                        AND
                            iBatchNo<>'0'
                        AND
                            ".$dtPaymentScheduled." >= '". mysql_escape_custom($fromDate) . "'  
                    )
                    OR
                    (
                            iStatus > 0
                        AND
                            ".$dtPaymentConfirmed."
                        $date 
                    )
                    OR
                    (
                            iStatus = 0
                        AND
                            ".$dtInvoiceOn."
                        $date 
                    )
                )
            ";
        }
        $dateAry = array();
        $dateAry['dtFromDate'] = $fromDate;
        $dateAry['dtToDate'] = $toDate;
	//$date ="dtPaymentConfirmed ".$date."";
	
	$path='forwarders';
	$toDate=date_default_timezone_get();
        $kReport = new cReport();
	//$sum = $kBooking->sumInvoicesBilling($idForwarder,6,'',$toDate);
	echo $file_name = $kReport->download_billing_table($date,$dateRange,$idForwarder,$path,$sum,$date,$dateAry);
	if(!empty($file_name))
	{
		//header("Location:".__FORWARDER_HOME_PAGE_URL__.'/forwarderBulkExportImport/forwarderDetails/'.$file_name.'.xlsx');
		//$file_path = __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/forwarderDetails/".$mode.".xlsx" ;
		
		if(file_exists($file_path))
		{
			//@unlink($file_path);
		}	
		exit;
	}
}
?>