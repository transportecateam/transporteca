<?php
 ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "BulkUpload/";

$kWHSSearch=new cWHSSearch();
$kConfig = new cConfig();

if($_REQUEST['flag']=='deleteWarehouse' &&  (int)$_REQUEST['idWarehouseData']>0)
{
    $kWHSSearch->load($_REQUEST['idWarehouseData']); 
    $iWarehouseType = $kWHSSearch->iWarehouseType;
    if($kWHSSearch->id>0)
    {
        if($kWHSSearch->deleteWarehouse($_REQUEST['idWarehouseData'],$_REQUEST['idForwarder']))
        {

        }
    } 
    else
    {
        //@TO DO
        //Warehouse ID is no longer exists.
    }
}
$order_by='';
if(!empty($_REQUEST['sort_by']))
{
    $order_by = trim(sanitize_all_html_input($_REQUEST['sort_by']));
    //We are only passing iWarehouseType in case when we were sorting the table
    if($_REQUEST['iWarehouseType']>0)
    {
        $iWarehouseType = (int)$_REQUEST['iWarehouseType'];
    }
}
$warehouseDetailAArr=$kWHSSearch->getWareHouseData($idForwarder,$order_by,$iWarehouseType); 
$style_a = 'text-decoration:none;color:#000000;font-style:Normal;';
//$wareHouseAry = $kWHSSearch->getAllWarehouses('forwarder_warehouse',$idForwarder);
//in the following pipeline ||||||(6 times) means new line ||||(4 times) means next element 

if(!empty($warehouseDetailAArr))
{
	$warehopuse_pipe_line = '';
	$count = 1;
	foreach($warehouseDetailAArr as $wareHouseArys)
	{
	/*
		if(!empty($wareHouseArys['szLatitude']))
		{
			$warehopuse_pipe_line .= $wareHouseArys['szLatitude']."||||";
		}
		if(!empty($wareHouseArys['szLongitude']))
		{
			$warehopuse_pipe_line .= $wareHouseArys['szLongitude']."||||";
		}
*/
		$warehopuse_pipe_line .=$wareHouseArys['szLatitude']."||||".$wareHouseArys['szLongitude']."||||".$wareHouseArys['szWareHouseName'];
		$warehopuse_pipe_line .= "||||".$wareHouseArys['szWareHouseName']."<br>".$wareHouseArys['szAddress'];
		$warehopuse_pipe_line .= "<br>".$wareHouseArys['szPostCode']." ".$wareHouseArys['szCity']."<br>".$wareHouseArys['szCountryName'];
		$warehopuse_pipe_line .="||||||";
		
		$totalLatitute += $wareHouseArys['szLatitude'] ;
		$totalLongitude += $wareHouseArys['szLongitude'] ;
		$count++;
	}
	$iAverageLatitute = $totalLatitute/$count ;
	$iAverageLongitute = $totalLongitude/$count ;
}

//echo "Warehouse Type: ".$iWarehouseType;  
if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
{
    $szHeading = t($t_base.'title/air_cfs_location_title');
    $szCfsBottomTitle = t($t_base.'title/air_click_to_remove_cfs');
    
    $szDeleteCfsText = t($t_base.'messages/air_are_you_sure_you_wish_to_delete_this_farwarder_warehouse');
    $szDeleteCfsTitle = t($t_base.'title/delete_airport_warehouse'); 
}
else
{
    $szHeading = t($t_base.'title/cfs_location_title');
    $szCfsBottomTitle = t($t_base.'title/click_to_remove_cfs');
    $szDeleteCfsText = t($t_base.'messages/are_you_sure_you_wish_to_delete_this_farwarder_warehouse');
    $szDeleteCfsTitle = t($t_base.'title/delete_warehouse');  
}
?>
<div id="warehouse_list">
<p><?php echo $szHeading; ?> <a href="javascript:void(0);" onclick="open_google_map_popup()" id="show_cfs_on_map"><?=t($t_base.'title/show_on_map');?></a></p>
<div id="delete_confirm" style="display:none;">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
<p><strong><?=$szDeleteCfsTitle; ?></strong></p><br>
<p><?=$szDeleteCfsText?></p><br/>
<p align="center"><a href="javascript:void(0)" class="button1" tabindex="6" onclick="cancel_remove_user_popup('delete_confirm','hsbody-2')"><span><?=t($t_base.'fields/cancel')?></span></a> <a href="javascript:void(0)" class="button1" tabindex="7" id="delete_confirm_button"><span><?=t($t_base.'fields/confirm');?></span></a></p>
	
</div>
</div>
</div>
    <div class="scroll-div" style="width:723px;padding:0;height:305px;border-top:none; margin:5px 0;">
        <table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="warehouseTable"  style="border-right:none;">
            <tr>
                <td width="15%" class="noleft_border"><a href="javascript:void(0);" style='<?=$style_a?>' onclick="sort_cfs_table('c.szCountryName','<?php echo $iWarehouseType; ?>')"><strong><?=t($t_base.'fields/country');?></strong></a></td>
                <td width="15%"><a href="javascript:void(0);" style='<?=$style_a?>' onclick="sort_cfs_table('w.szCity','<?php echo $iWarehouseType; ?>')"><strong><?=t($t_base.'fields/city');?></strong></a></td>
                <td width="30%"><a href="javascript:void(0);" style='<?=$style_a?>' onclick="sort_cfs_table('w.szWareHouseName','<?php echo $iWarehouseType; ?>')"><strong><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'fields/airport_warehouse'):t($t_base.'fields/cfs_name');?></strong></a></td>
                <td width="15%"><strong><?=t($t_base.'fields/modified');?></strong></td>
                <td width="25%"><strong><?=t($t_base.'fields/user');?></strong></td>
            </tr>
            <?php 
                if(!empty($warehouseDetailAArr))
                {
                    $i=1;
                    foreach($warehouseDetailAArr as $warehouseDetailArrs)
                    {
                        $szOriginAddress = $warehouseDetailArrs['szWareHouseName']."<br>".$warehouseDetailArrs['szAddress']." ".$warehouseDetailArrs['szAddress2']." ".$warehouseDetailArrs['szAddress3']."<br>".$warehouseDetailArrs['szPostCode']." - ".$warehouseDetailArrs['szCity']."<br> ".$warehouseDetailArrs['szCountryName'];
                        $origin_lat_lang_pipeline = $warehouseDetailArrs['szLatitude']."|".$warehouseDetailArrs['szLongitude']."|".$szOriginAddress;
            ?>
                    <input type="hidden" name="cfs_location_pipe_line_<?=$warehouseDetailArrs['id']?>" value="<?=$origin_lat_lang_pipeline?>" id="cfs_location_pipe_line_<?=$warehouseDetailArrs['id']?>">
                    <tr id="warehouse_data_<?=$i?>" onDblClick="get_warehouse_detail('<?=$warehouseDetailArrs['id']?>','<?=$idForwarder?>','save');" onclick="select_to_delete_warehouse('warehouse_data_<?=$i?>','<?=$warehouseDetailArrs['id']?>','<?=$idForwarder?>');">
                        <td valign="top" class="noleft_border">
                            <?=$warehouseDetailArrs['szCountryName']?>		
                        </td>
                        <td valign="top">
                            <?=$warehouseDetailArrs['szCity']?>
                        </td>
                        <td valign="top">
                            <?=$warehouseDetailArrs['szWareHouseName']?> 
                        </td>
                        <td valign="top">
                            <?php
                                if($warehouseDetailArrs['dtUpdatedOn']!='' && $warehouseDetailArrs['dtUpdatedOn']!='0000-00-00 00:00:00')
                                    echo date('d/m/Y',strtotime($warehouseDetailArrs['dtUpdatedOn']));
                                else
                                    echo "N/A";
                            ?> 
                        </td>
                        <td valign="top">
                            <?=$warehouseDetailArrs['szUpdatedBy']?>		
                        </td>
                    </tr>
                    <?php
                    ++$i; 
            }}else{?>
            <tr>
                <td colspan="5" align="center" class="noleft_border"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'fields/start_by_adding_your_first_airport_warehouse_below'):t($t_base.'fields/cfs_name');?></td>
            </tr>
            <?php }?>
        </table>
    </div>

    <div class="oh">
        <input type="hidden" name="warehouse_pipline" value="<?=$warehopuse_pipe_line?>" id="show_on_map_pipe_line">
        <input type="hidden" name="average_latitude_pipe_line" value="<?=$iAverageLatitute?>" id="average_latitude_pipe_line">
        <input type="hidden" name="average_longitute_pipe_line" value="<?=$iAverageLongitute?>" id="average_longitute_pipe_line">
        <input type="hidden" name="iWarehouseType" value="<?=$iWarehouseType?>" id="iWarehouseType">
       <p class="fl-65" style="padding-top:12px;"><?=$szCfsBottomTitle;?>.</p>
       <p class="fr-30" align="right" style="padding-top:6px;">
           <a href="javascript:void(0)" class="button1"  onclick="" style="opacity:0.4;" id="warehouse_edit_button"><span style="min-width: 77px;"><?=t($t_base.'fields/edit');?></span></a>
           <a href="javascript:void(0)" class="button2"  onclick="" style="opacity:0.4;" id="warehouse_remove_button"><span style="min-width: 77px;"><?=t($t_base.'fields/delete');?></span></a>
       </p>
    </div>
</div>
<script type="text/javascript"> 
 var iScrollValue=$("#iScrollValue").attr('value');
console.log("topvalue" + iScrollValue);
$(".scroll-div").scrollTop(iScrollValue);   


var iScrollDivValue=$("#iScrollDivValue").attr('value');
if(iScrollDivValue!=''){
console.log("iScrollDivValue" + iScrollDivValue);

$("#"+iScrollDivValue).attr('style','background:#DBDBDB;color:#828282;');
}
$(".scroll-div").scroll(function()
{
   var topzValue=$(this).scrollTop();
   console.log("topzValue"+topzValue);
   $("#iScrollValue").attr('value',topzValue);
});
</script>  