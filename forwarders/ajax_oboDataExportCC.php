<?php
/**
 * Edit User Information
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "OBODataExport/";

checkAuthForwarder_ajax();
$kExport_Import=new cExport_Import();
if(!empty($_REQUEST['mode']))
{
    $operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
    $idForwarder = sanitize_all_html_input(trim($_REQUEST['idForwarder']));
    $idCountry = sanitize_all_html_input(trim($_REQUEST['idCountry']));
    $szCity = sanitize_all_html_input(trim($_REQUEST['szCity']));
    $iWarehouseType = sanitize_all_html_input(trim($_REQUEST['iWarehouseType']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['szFromPage']));
}
if($operation_mode=='ORIGIN_WHS_COUNTRY')
{
    $forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,false,false,$iWarehouseType);
    ?>
    <select name="oboDataExportAry[idOriginWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');enable_destination_country_dropdown(this.value,'<?php echo $idForwarder; ?>','<?php echo $szFromPage; ?>');" id="idOriginWarehouse">
        <option value=""><?=t($t_base.'fields/select');?></option>	
        <?php
            if(!empty($forwardWareHouseArr))
            {
                foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                {
                    ?>
                    <option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                    <?php
                }
            }
        ?>
    </select>
<?php
echo "||||";
$forwardWareHouseCityAry = $kExport_Import->getForwaderWarehousesCity($idForwarder,$idCountry, $iWarehouseType);
?>
<?php
    if(!empty($forwardWareHouseCityAry))
    {
        if(count($forwardWareHouseCityAry)==1)
        {
            $modeSelect = "selected=\"selected\"";
            ?>
            <script type="text/javascript">
                show_forwarder_warehouse_by_city('<?=$idForwarder?>','<?=$forwardWareHouseCityAry[0]['szCity']?>','origin_warehouse_span','ORIGIN_WHS_CITY','<?php echo $iWarehouseType; ?>');
            </script>
            <?php
        }
        else
        {
            $modeSelect = '';
        }
    } 
    ?>
    <select id="szOriginCity" name="oboDataExportAry[szOriginCity]" onchange="show_forwarder_warehouse_by_city('<?=$idForwarder?>',this.value,'origin_warehouse_span','ORIGIN_WHS_CITY','<?php echo $iWarehouseType; ?>')">
	<option value=""><?=t($t_base.'title/all');?></option>
	<?php
            if(!empty($forwardWareHouseCityAry))
            {
                foreach($forwardWareHouseCityAry as $forwardWareHouseCityArys)
                {
                    ?>
                    <option value="<?=$forwardWareHouseCityArys['szCity']?>" <?=$modeSelect?>><?=$forwardWareHouseCityArys['szCity']?></option>
                    <?php
                }
            }
	?>
</select>
<?php 
}
else if($operation_mode=='DISPLAY_DESTINATION_COUNTRY')
{
    $idWarehouse = sanitize_all_html_input(trim($_REQUEST['idWarehouse']));
    $kWHSSearch = new cWHSSearch();
    $kWHSSearch->load($idWarehouse);
    $iWarehouseType = $kWHSSearch->iWarehouseType;
    
    $forwardCountriesArr = array();
    $forwardCountriesArr = $kExport_Import->getForwaderCountries($idForwarder,$iWarehouseType);
    
    echo "SUCCESS||||";
    ?>
    <select name="oboDataExportAry[idDestinationCountry]" id="idDestinationCountry" onchange="show_forwarder_warehouse('<?=$idForwarder?>',this.value,'destination_warehouse_span','DESTINATION_WHS_COUNTRY','<?php echo $iWarehouseType; ?>')">
        <option value=""><?=t($t_base.'fields/select');?></option>
        <?php
            if(!empty($forwardCountriesArr))
            {
                foreach($forwardCountriesArr as $forwardCountriesArrs)
                {
                    ?>
                    <option value="<?=$forwardCountriesArrs['idCountry']?>" <?=$modeSelect?>><?=$forwardCountriesArrs['szCountryName']?></option>
                    <?php
                }
            }
        ?>
    </select>
    <?php
}
else if($operation_mode=='ORIGIN_WHS_CITY')
{
	$forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,$szCity,false,$iWarehouseType);
	?>
	<?php
            if(!empty($forwardWareHouseArr))
            {
                if(count($forwardWareHouseArr)==1)
                {
                    $modeSelect = "selected=\"selected\""; 
                }
                else
                {
                    $modeSelect = '';
                }
            }  
	?>
	<select name="oboDataExportAry[idOriginWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');enable_destination_country_dropdown(this.value,'<?php echo $idForwarder; ?>','<?php echo $szFromPage; ?>');" id="idOriginWarehouse">
            <option value=""><?=t($t_base.'fields/select');?></option>	
            <?php
                if(!empty($forwardWareHouseArr))
                {
                    foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                    {
                        $idWarehouse = $forwardWareHouseArrs['id'];
                        ?>
                        <option value="<?=$forwardWareHouseArrs['id']?>" <?=$modeSelect?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                        <?php
                    }
                }
            ?>
	</select>
<?php	

    if(!empty($forwardWareHouseArr))
    {
        if(count($forwardWareHouseArr)==1)
        {
            ?>
            <script type="text/javascript">
                enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');
                enable_destination_country_dropdown('<?php echo $idWarehouse; ?>','<?php echo $idForwarder; ?>','<?php echo $szFromPage; ?>');
            </script>
            <?php  
        }
    } 
}
if($operation_mode=='DESTINATION_WHS_COUNTRY')
{
	$forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,false,false,$iWarehouseType);
	?>
	<select name="oboDataExportAry[idDestinationWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');" id="idDestinationWarehouse">
		<option value=""><?=t($t_base.'fields/select');?></option>	
		<?php
			if(!empty($forwardWareHouseArr))
			{
				foreach($forwardWareHouseArr as $forwardWareHouseArrs)
				{
					?>
					<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
					<?php
				}
			}
		?>
	</select>
<?php
echo "||||";
$forwardWareHouseCityAry = $kExport_Import->getForwaderWarehousesCity($idForwarder,$idCountry,$iWarehouseType);
?>
	<?php
            if(!empty($forwardWareHouseCityAry))
            {
                if(count($forwardWareHouseCityAry)==1)
                {
                    $modeSelect = "selected=\"selected\"";
                    ?>
                    <script type="text/javascript">
                        show_forwarder_warehouse_by_city('<?=$idForwarder?>','<?=$forwardWareHouseCityAry[0]['szCity']?>','destination_warehouse_span','DESTINATION_WHS_CITY','<?php echo $iWarehouseType; ?>');
                    </script>
                    <?php
                }
                else
                {
                    $modeSelect = '';
                }
            } 
	?>
<select id="szDestinationCity" name="oboDataExportAry[szDestinationCity]"  onchange="show_forwarder_warehouse_by_city('<?=$idForwarder?>',this.value,'destination_warehouse_span','DESTINATION_WHS_CITY','<?php echo $iWarehouseType; ?>')">
	<option value=""><?=t($t_base.'title/all');?></option>
	<?php
		if(!empty($forwardWareHouseCityAry))
		{
			
			foreach($forwardWareHouseCityAry as $forwardWareHouseCityArys)
			{
				?>
				<option value="<?=$forwardWareHouseCityArys['szCity']?>" <?=$modeSelect?>><?=$forwardWareHouseCityArys['szCity']?></option>
				<?php
			}
		}
	?>
</select>
<?php }
else if($operation_mode=='DESTINATION_WHS_CITY')
{
	$forwardWareHouseArr=$kExport_Import->getForwaderWarehouses($idForwarder,$idCountry,$szCity,false,$iWarehouseType); 
        if(!empty($forwardWareHouseArr))
        {
            if(count($forwardWareHouseArr)==1)
            {
                $modeSelect = "selected=\"selected\"";
            }
            else
            {
                $modeSelect = '';
            }
        }
	
	?>
	<select name="oboDataExportAry[idDestinationWarehouse]" onchange="enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');" id="idDestinationWarehouse">
		<option value=""><?=t($t_base.'fields/select');?></option>	
		<?php
                    if(!empty($forwardWareHouseArr))
                    {
                        foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                        {
                            ?>
                            <option value="<?=$forwardWareHouseArrs['id']?>" <?=$modeSelect?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                            <?php
                        }
                    }
		?>
	</select>
	<?php
		if(!empty($forwardWareHouseArr))
		{
			if(count($forwardWareHouseArr)==1)
			{
?>
                            <script type="text/javascript">
                                enable_get_data('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>');
                            </script>
            <?php }  }?>
<?php	
}
else if($operation_mode=='CANCEL_OBO_CC')
{
    echo display_obo_cc_bottom_html($t_base,true);
}
else if(!empty($_REQUEST['oboDataExportAry']))
{
    $searchedDataAry=array();
    $searchedDataAry = $kExport_Import->getDataForCC($_REQUEST['oboDataExportAry']);
    if(!empty($searchedDataAry))
    {
        echo display_obo_cc_bottom_html($t_base,false,$searchedDataAry,$_REQUEST['oboDataExportAry']);
    }
    else
    {
        echo display_obo_cc_bottom_html($t_base,false,$searchedDataAry,$_REQUEST['oboDataExportAry']);
    }
}
else if(!empty($_REQUEST['editOBOCCData']))
{
    $searchedDataAry=array();
    if($kExport_Import->updateOBOCustomClearance($_REQUEST['editOBOCCData']))
    {
        echo "SUCCESS||||";
        echo display_obo_cc_bottom_html($t_base,true);
    }
    if(!empty($kExport_Import->arErrorMessages))
    {
        echo "ERROR ||||" ;
	?>
	
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kExport_Import->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	<?php
	}
}
else if(!empty($_REQUEST['addOBOCCData']))
{
	$searchedDataAry=array();
	if($kExport_Import->addOBOCustomClearance($_REQUEST['addOBOCCData']))
	{
		echo "SUCCESS||||";
		echo display_obo_cc_bottom_html($t_base,true);
	}
	if(!empty($kExport_Import->arErrorMessages))
	{
		echo "ERROR ||||" ;
	?>
	
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kExport_Import->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	<?php
	}
}
?>