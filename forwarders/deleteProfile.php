<?php 
/**
  *forwarder myProfile 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
//session_destroy();
$szMetaTitle="Transporteca | Delete Profile";
$display_profile_not_completed_message = false;
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/forwarder_header.php");
//$t_base="ForwardersCompany/homepage/";
checkAuthForwarder();
?>

<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/forwarder_profile_nav.php" ); ?> 
	<div class="hsbody-2-right">
		<div id="delete_forwarder_account">
					<?
						require_once( __APP_PATH__ . "/forwarders/ajax_deleteForwarderAccount.php" ); 
					?>
				</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>		