<?php
/**
 * Forwarder My Company  
 */
ob_start();
if (!isset( $_SESSION ))
{
  session_start();
}
$szMetaTitle="Transporteca | Update Airfreight Services and Rates - Line by Line";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php");
require_once( __APP_PATH__ . "/forwarders/listExcelData.php" );
checkAuthForwarder();
$t_base = "OBODataExport/";
$t_base_bulkUpload = "BulkUpload/";
$idForwarder =$_SESSION['forwarder_id'];
$kExport_Import=new cExport_Import();
$kConfig = new cConfig();
$allCurrencyArr=$kConfig->getBookingCurrency(); 
$kForwarderContact->load($_SESSION['forwarder_user_id']); 
$iWarehouseType = __WAREHOUSE_TYPE_AIR__;
?>
<script type="text/javascript">
$().ready(function() {

$("#datepicker1").datepicker();
$("#datepicker2").datepicker();

});
</script>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="delete_confirm" style="display:none;">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
<p class="close-icon" align="right">
<a onclick="cancel_remove_user_popup('delete_confirm','hsbody-2')" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<h5><?=t($t_base.'title/delete_obo_air');?></h5>
<p><?=t($t_base.'title/are_you_sure_you_wish_to_delete_this_obo_air')?>?</p><br/>
<p align="center"><a href="javascript:void(0)" class="button1" tabindex="6" onclick="cancel_remove_user_popup('delete_confirm','hsbody-2')"><span><?=t($t_base.'fields/cancel')?></span></a> <a href="javascript:void(0)" class="button1" tabindex="7" id="delete_confirm_button"><span><?=t($t_base.'fields/confirm');?></span></a></p>
	
</div>
</div>
</div>
<div id="hsbody-2">
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader"></div>				
        <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
    </div>
    <div class="hsbody-2-left account-links">
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
    </div>
    <div class="hsbody-2-right">
            <h5><?=t($t_base.'title/air_freight_top_heading');?></h5>
            <form action="" id="obo_lcl_data_export_form" method="post">
                <div>
                    <?php
                        //displaying country , city and warehouses drop downs 
                        echo obo_data_export_top_form($idForwarder,$t_base,$iWarehouseType);
                    ?>
                    <p class="fl-80" align="right" style="padding-top:6px;"><?=t($t_base.'title/click_to_get_data_air');?></p>
                    <p class="fl-20" align="right" id="obo_cc_getdata_button_p"><a href="javascript:void(0);" id="obo_cc_getdata_button" style="opacity:0.4" class="button1"><span id="text_change"><?=t($t_base.'title/get_data');?></span></a></p>
                </div>
            </form>
            <hr />	
            <div id="lcl_table">
            <?php
                    //echo display_multiple_lcl_services($lclServiceAry,$t_base);
            ?>
            </div>
            <span id="obo_lcl_bottom_span">	
            <?php
                    //echo display_obo_lcl_bottom_html($t_base,true);
            ?>
            <h5><?=t($t_base.'title/illustration_of_air_service')?></h5>
             <img src="<?=__BASE_STORE_IMAGE_URL__.'/CargoFlowAirfreight.png'?>" id="Upload one by one Airfreight Services" >
             <!-- 
                    <canvas style="margin-left:-10px;" id="myCanvas" width="730" height="180"></canvas>
                    <script type="text/javascript">
                            draw_canvas_image_obo('<?=$szOriginWhsName?>','<?=$szDestinationWhsName?>','<?=__BASE_STORE_IMAGE_URL__.'/CargoFlowLCL.png'?>','OBO');
                    </script>
             -->
            </span>
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>