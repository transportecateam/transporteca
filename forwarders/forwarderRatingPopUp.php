<?php
/**
 * Customer Login
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$t_base="Forwarders/";


$kForwarder = new cForwarder();
$kWHSSearch = new cWHSSearch();
$kConfig = new cConfig();
$kForwarder->load($_REQUEST['idForwarder']); 
$Forward6MonthArrs['trating']=0;
$rateValue=$kWHSSearch->getManageMentVariableByDescription('__FORWARDER_RATING__');
$Forward6MonthArrs=$kWHSSearch->getForwarderAvgRating($_REQUEST['idForwarder'],$rateValue);
$reviewValue=$kWHSSearch->getManageMentVariableByDescription('__FORWARDER_RATING_REVIEW__');
$Forward3MonthArrs=$kWHSSearch->getForwarderAvgRating($_REQUEST['idForwarder'],$reviewValue);
$width=($Forward6MonthArrs['fAvgRating']*__RATING_STAR_WIDTH__);
$rating3MonthArr=$kForwarder->getForwardRating($_REQUEST['idForwarder'],$reviewValue);
$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
?>
<div id="popup-bg"></div>
<div id="popup-container">
<p class="close-icon" align="right">
<a onclick="cancel_remove_user_popup('rating_popup','hsbody-2');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<div class="popup rating-popup">
<?

$starone=0;
$starfive=0;
$starfour=0;
$starthree=0;
$startwo=0;
$staronecolourbar=0;
$starfivecolourbar=0;
$starfourcolourbar=0;
$starthreecolourbar=0;
$startwocolourbar=0;

if($Forward3MonthArrs['iNumRating']>0)
{

$starfive=count($rating3MonthArr['5']);
$starfivecolourbar=(($starfive*100)/$Forward3MonthArrs['iNumRating']);
$starfivecolourbar=round($starfivecolourbar,2);

$starfour=count($rating3MonthArr['4']);
$starfourcolourbar=(($starfour*100)/$Forward3MonthArrs['iNumRating']);
$starfourcolourbar=round($starfourcolourbar,2);

$starthree=count($rating3MonthArr['3']);
$starthreecolourbar=(($starthree*100)/$Forward3MonthArrs['iNumRating']);
$starthreecolourbar=round($starthreecolourbar,2);

$startwo=count($rating3MonthArr['2']);
$startwocolourbar=(($startwo*100)/$Forward3MonthArrs['iNumRating']);
$startwocolourbar=round($startwocolourbar,2);

$starone=count($rating3MonthArr['1']);
$staronecolourbar=(($starone*100)/$Forward3MonthArrs['iNumRating']);
$staronecolourbar=round($staronecolourbar,2);

$totalRating3Months = ($starfive + $starfour + $starthree +  $startwo + $starone) ;

}
?>
			<div class="oh">
				<p class="fl-33">
				<? if(!empty($kForwarder->szLogo)) {?>
								<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$kForwarder->szLogo."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$kForwarder->szDisplayName?>" border="0" />
								<? } else {?>
								<?=$kForwarder->szDisplayName?>
								<? }?>
				</p>
				<div class="fl-65">
					<p class="fl-49" style="width:45%"><?=t($t_base.'title/feedback_rating')?>:</p>
					<?
						if($Forward6MonthArrs['iNumRating']<$minRatingToShowStars){ 
									echo '<p class="fl-49">N/A</p>'; 
					?>
						<br class="clear-all" />				
					<p class="f-size-12"><em><?=t($t_base.'title/not_received_enough_ratings')?></em></p>
					<?
								}else {	
					?>
					<div id="ratings-star" class="fl-30" style="margin-top:6px;">
            			<span style="width:<?=$width?>px;">&nbsp;</span>
					</div>
					<br class="clear-all" />				
					<p class="f-size-12"><em><? if ($Forward6MonthArrs['fAvgRating']>0){ echo number_format($Forward6MonthArrs['fAvgRating'],1); } else{ echo "0"; }?> <?=t($t_base.'title/stars_over_the_past_6_months')?> <?=$rateValue?> <?=t($t_base.'title/months')?> (<? if($Forward6MonthArrs['iNumRating']>0){ echo $Forward6MonthArrs['iNumRating']; }else { echo "0";}?> <?=t($t_base.'title/rating')?>)</em></p>
					<? } ?>
					
				</div>
			</div>
			<div class="oh" style="margin-top:10px;">
				<div class="fl-60 rating-box">
					<h5><? if($totalRating3Months>0) { echo $totalRating3Months;}else { echo  "0"; }?> <?=t($t_base.'title/reviews_in_recent_3_month')?> <?=$reviewValue?> <?=t($t_base.'title/months')?></h5>
					<div class="oh">
						<p class="fl-25 rating-count"><a href="javascript:void(0)" onclick="showUserComment('<?=$_REQUEST['idForwarder']?>','5')">5 <?=t($t_base.'title/star')?></a></p>
						<p class="fl-33 rating-graph"><span style="width:<?=$starfivecolourbar?>%;">&nbsp;</span></p>
						<p class="fl-33">(<?=$starfive?>)</p>
					</div>
					<div class="oh">
						<p class="fl-25 rating-count"><a href="javascript:void(0)" onclick="showUserComment('<?=$_REQUEST['idForwarder']?>','4')">4 <?=t($t_base.'title/star')?></a></p>
						<p class="fl-33 rating-graph"><span style="width:<?=$starfourcolourbar?>%;">&nbsp;</span></p>
						<p class="fl-33">(<?=$starfour?>)</p>
					</div>
					<div class="oh">
						<p class="fl-25 rating-count"><a href="javascript:void(0)" onclick="showUserComment('<?=$_REQUEST['idForwarder']?>','3')">3 <?=t($t_base.'title/star')?></a></p>
						<p class="fl-33 rating-graph"><span style="width:<?=$starthreecolourbar?>%;">&nbsp;</span></p>
						<p class="fl-33">(<?=$starthree?>)</p>
					</div>
					<div class="oh">
						<p class="fl-25 rating-count"><a href="javascript:void(0)" onclick="showUserComment('<?=$_REQUEST['idForwarder']?>','2')">2 <?=t($t_base.'title/star')?></a></p>
						<p class="fl-33 rating-graph"><span style="width:<?=$startwocolourbar?>%;">&nbsp;</span></p>
						<p class="fl-33">(<?=$startwo?>)</p>
					</div>
					<div class="oh">
						<p class="fl-25 rating-count"><a href="javascript:void(0)" onclick="showUserComment('<?=$_REQUEST['idForwarder']?>','1')">1 <?=t($t_base.'title/star')?></a></p>
						<p class="fl-33 rating-graph"><span style="width:<?=$staronecolourbar?>%;">&nbsp;</span></p>
						<p class="fl-33">(<?=$starone?>)</p>
					</div>
					<p><a href="javascript:void(0)" onclick="showUserComment('<?=$_REQUEST['idForwarder']?>','0')"><?=t($t_base.'link/show_all_reviews')?></a></p>
				</div>
				<div class="fl-40">
					<h5 style="margin-bottom:0px"><?=t($t_base.'title/registered_address')?>:</h5>
					<p style="line-height:100%;margin-top:0px;"><?=$kForwarder->szCompanyName?><br/> <?=$kForwarder->szAddress?> <?=$kForwarder->szAddress2?> <?=$kForwarder->szAddress3?><br/>  <?=$kForwarder->szPostCode?> <?=$kForwarder->szCity?> <br/><?=$kForwarder->szState?>  <?=$kConfig->getCountryName($kForwarder->idCountry);?></p>
				</div>
			</div>
			<div class="scroll-div">
				<div id="user_comment">
				<?
				if(!empty($rating3MonthArr))
				{
					$i=0;
					foreach($rating3MonthArr as $key=>$values)
					{
						foreach($values as $value)
						{
				?>
				<p><em><?=$key?> <?=t($t_base.'title/of')?> 5: <?=$value?></em></p>
				<?
				++$i;
				}}}?>
				</div>
			</div>		
			<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancel_remove_user_popup('rating_popup','hsbody-2');"><span><?=t($t_base.'title/close');?></span></a></p>
		</div>
		</div>