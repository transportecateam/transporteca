<?php
/**
 * Edit Forwarder Information
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
checkAuthForwarder_ajax();
$t_base = "Forwarders/homepage/";
$t_base_error = "Error";
$kConfig = new cConfig();
$successFlag=false;
$successContactUpdatedFlag=false;
$successPasswordUpdatedFlag=false;
$allCountriesArr=$kConfig->getAllCountries(true);
$kForwarderContact->getMainAdmin($_SESSION['forwarder_id']);

$arrBook=BookingReportsArr();
$bookingReports	= $arrBook[0];

$showflag=$_REQUEST['showflag'];

if(!empty($_POST['editUserInfoArr']) && $showflag=="user_info"){
 		if($kForwarderContact->updateForwarderinfo($_POST['editUserInfoArr'],$_SESSION['forwarder_user_id'])){
 			
 		$successFlag=true;	
 		echo "<script>$('#forwarder_name').html('".$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName."')</script>";	
 }
}
if(!empty($_POST['editUserInfoArr']) && $showflag=="contact_info"){
 		if($kForwarderContact->updateForwarderContact_info($_POST['editUserInfoArr'],$_SESSION['forwarder_user_id'])){
 		$successContactUpdatedFlag=true;
 		$kForwarderContact->getMainAdmin($_SESSION['forwarder_id']);
 		}
 		if(!empty($_POST['editUserInfoArr']['szPhone']))
 		{
	 		$_POST['editUserInfoArr']['szPhone']=urldecode(base64_decode($_POST['editUserInfoArr']['szPhoneUpdate']));
 		}
		if(!empty($_POST['editUserInfoArr']['szMobile']))
 		{
	 		$_POST['editUserInfoArr']['szMobile']=urldecode(base64_decode($_POST['editUserInfoArr']['szMobileUpdate']));
 		}
 }
if(!empty($_POST['editUserInfoArr']) && $showflag=="change_pass"){
// echo $KForwarderContact->updateForwarderPassword_info($_POST['editUserInfoArr'],$_SESSION['forwarder_user_id']);
		if($kForwarderContact->updateForwarderPassword_info($_POST['editUserInfoArr'],$_SESSION['forwarder_user_id'])){
		$successPasswordUpdatedFlag=true;
		}


} 

//if(empty($_POST))
//{
	$kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
//}
if($showflag=='user_info' && !$successFlag)
{
if(!empty($kForwarderContact->arErrorMessages)){
?>
<div id="regError" class="errorBox">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kForwarderContact->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php } ?>	
<div id="user_information">
<h4><strong><?=t($t_base.'fields/forwarder_info');?></strong></h4>
<form name="updateUserInfo" id="updateUserInfo" method="post">
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szFirstName]" id="szFirstName" value="<?=!empty($_POST['editUserInfoArr']['szFirstName']) ? $_POST['editUserInfoArr']['szFirstName'] : $kForwarderContact->szFirstName?>" onblur="closeTip('fname');" onfocus="openTip('fname');" style="width:50%;"/></span>
				<div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szLastName]" id="szLastName" value="<?=!empty($_POST['editUserInfoArr']['szLastName']) ? $_POST['editUserInfoArr']['szLastName'] : $kForwarderContact->szLastName ?>" onblur="closeTip('lname');" onfocus="openTip('lname');" style="width:50%;"/></span>
				<div class="field-alert"><div id="lname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
			</label>
			
			<label class="profile-fields">
				<input type="hidden" name="editUserInfoArr[szOldCountry]" id="szOldCountry" value="<?=$_POST['editUserInfoArr']['szOldCountry'] ? $_POST['editUserInfoArr']['szOldCountry'] : $kForwarderContact->szCountryId ?>" />
				<span class="field-name"><?=t($t_base.'fields/country');?></span>
				<span class="field-container">
					<select size="1"  name="editUserInfoArr[szCountry]" id="szCountry"  style="width:50%;">
					<option value=""><?=t($t_base.'fields/select_country');?></option>
						<?php
							if(!empty($allCountriesArr))
							{
								foreach($allCountriesArr as $allCountriesArrs)
								{
									?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['editUserInfoArr']['szCountry'])?$_POST['editUserInfoArr']['szCountry']:$kForwarderContact->szCountryId) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						?>
					</select>
				</span>
			</label>
			
		<input type='hidden' name='showflag' value="<?=$showflag;?>">
		<br/>
		<p align="center" style="width:60%"><a href="javascript:void(0)" class="button1" onclick="javscript:update_forwarder_user_info();"><span><?=t($t_base.'fields/save');?></span></a><a href="javascript:void(0)" class="button2" onclick="cancel_forwarder_info();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
</form>	
</div><br/><br/>
<? }else{?>
		<div id="user_information">
				<h4><strong><?=t($t_base.'fields/forwarder_info');?></strong> <a href="javascript:void(0)" onclick="editForwarderUserInformation();"><?=t($t_base.'fields/edit');?></a></h4>	
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
					<span class="field-container"><?=$kForwarderContact->szFirstName?></span>
				</div>
					
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
					<span class="field-container"><?=$kForwarderContact->szLastName?></span>
				</div>
					
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/country');?></span>
					<span class="field-container"><?=$kForwarderContact->szCountryName?></span>
				</div>
		</div><br/><br/>
			<? } if($showflag=='contact_info' && !$successContactUpdatedFlag){ 
			if(!empty($kForwarderContact->arErrorMessages)){
?>
<div id="regError" class="errorBox">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kForwarderContact->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php } ?>
	<div id="contact_information">
				<h4><strong><?=t($t_base.'fields/contact');?></strong></h4>
			<form name="updateContactInfo" id="updateContactInfo" method="post">
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/email');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szEmail]" id="szEmail" style="width:50%;" value="<?=$_POST['editUserInfoArr']['szEmail'] ? $_POST['editUserInfoArr']['szEmail'] : $kForwarderContact->szEmail?>" onblur="closeTip('fname');" onfocus="openTip('fname');"/></span>
				<div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/email');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
				<span class="field-container">
				<select name="editUserInfoArr[szBooking]" style="width:50%;" onblur="closeTip('booking_reports');" id="szBooking" onfocus="openTip('booking_reports');">
				<?php
					if(!empty($bookingReports))
					{
						$k=1;
						for($i=0;$i<count($bookingReports);++$i)
						{
							
						?>
							<option value="<?=$k?>" <?=((($_POST['editUserInfoArr']['szBooking'])?$_POST['editUserInfoArr']['szBooking']:$kForwarderContact->szBookingFrequency) ==  $k ) ? "selected":""?>><?=$bookingReports[$i]?></option>
						<?							
							++$k;
						}
					
					}
				?>
				</select>
				</span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/phone');?></span>
				<span class="field-container"><input type="text" style="width:50%;" name="editUserInfoArr[szPhone]" id="szPhone" value="<?=$_POST['editUserInfoArr']['szPhone'] ? $_POST['editUserInfoArr']['szPhone'] : $kForwarderContact->szPhone ?>" onblur="closeTip('phone');" onfocus="openTip('phone');"/></span>
				<div class="field-alert"><div id="phone" style="display:none;"><?=t($t_base.'messages/phone');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/mobile');?> <span class="optional">(<?=t($t_base.'fields/optional');?>)</span></span>
				<span class="field-container"><input type="text" style="width:50%;" name="editUserInfoArr[szMobile]" id="szMobile" value="<?=$_POST['editUserInfoArr']['szMobile'] ? $_POST['editUserInfoArr']['szMobile'] : $kForwarderContact->szMobile ?>" onblur="closeTip('mobile');" onfocus="openTip('mobile');"/></span>
				<div class="field-alert"><div id="mobile" style="display:none;"><?=t($t_base.'messages/mobile');?></div></div>
			</label>
			<input type="hidden" name="editUserInfoArr[szOldEmail]" value="<?=$kForwarderContact->szEmail;?>"> 
			<input type='hidden' name='showflag' value="<?=$showflag;?>">
			<input type='hidden' name='editUserInfoArr[szPhoneUpdate]' id="szPhoneUpdate" value="">
			<input type='hidden' name='editUserInfoArr[szMobileUpdate]' id="szMobileUpdate" value="">
		<br/>
		<p align="center" style="width:60%"><a href="javascript:void(0)" class="button1" onclick="encode_string('szPhone','szPhoneUpdate','szMobile','szMobileUpdate');update_forwarder_contact_info_details();"><span><?=t($t_base.'fields/save');?></span></a><a href="javascript:void(0)" class="button2" onclick="cancel_forwarder_info();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
		</form>	
	</div><br/><br/>
			<?}else{ ?>
			<div id="contact_information">
			<h4><strong><?=t($t_base.'fields/contact');?></strong> <a href="javascript:void(0)" onclick="editForwarderContactInformation();"><?=t($t_base.'fields/edit');?></a></h4>	
	
					<div class="ui-fields" style="height:auto;min-height:26px;">
					<span class="field-name"><?=t($t_base.'fields/email');?></span>
					<span class="field-container"><?=$kForwarderContact->szEmail?>
					<?php if($kForwarderContact->iConfirmed!='1')
			 {?><span style='color:red;font-size:13px;'>(<?=t($t_base.'messages/not_verified');?>)</span></span>
					<? if($successContactUpdatedFlag) {?>
					
					<div style="background: #b6dde8;clear: both;margin:0 0 0 216px;width: 486px;padding:5px 10px 8px;">
					<div id="activationkey" style="display:none";></div>
					<p><?=t($t_base.'messages/click_the_link');?> <?=$_POST['editUserInfoArr']['szEmail']?> <?=t($t_base.'messages/complete_your_account');?></p>
					
					<p><a href="javascript:void(0)" onclick="resendActivationCode('<?=$_SERVER['HTTP_REFERER']?>');"><?=t($t_base.'messages/resend_email');?></a>
					<br /></p>
					</div>
		
			<?php }}?>
					</span>
					</div>
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
						<span class="field-container">
						<? 
						if($kForwarderContact->szBookingFrequency=='1')
						{
							echo "Daily";
						}
						else if($kForwarderContact->szBookingFrequency=='2')
						{
							echo "Weekly ";
						}
						else if($kForwarderContact->szBookingFrequency=='3')
						{
							echo "Monthly";
						}
						else
						{
							echo "Never";
						}
						  
						?></span>
					</div>
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/phone');?></span>
					<span class="field-container"><?=$kForwarderContact->szPhone?></span>
					</div>
					
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/mobile');?> <span class="optional">(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kForwarderContact->szMobile?></span>
					</div>
			
			</div><br/><br/>
			
			<? }if($showflag=='change_pass' && !$successPasswordUpdatedFlag){ 
			if(!empty($kForwarderContact->arErrorMessages)){
?>
<div id="regError" class="errorBox">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kForwarderContact->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }
			?>
			<div id="password_information">
		
		<form name="changePassword" id="changePassword" method="post">
		<h4><strong><?=t($t_base.'fields/password');?></strong></h4>		
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/current_password')?></span>
			<span class="field-container"><input type="password" style="width:49%;" name="editUserInfoArr[szOldPassword]" id="szOldPassword"/></span>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/password');?></span>
			<span class="field-container"><input type="password" style="width:49%;" name="editUserInfoArr[szNewPassword]" id="szNewPassword" onblur="closeTip('pass');" onfocus="openTip('pass');"/></span>
			<div class="field-alert"><div id="pass" style="display:none;"><?=t($t_base.'messages/password_msg');?></div></div>
		</label>
		
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/con_new_password')?></span>
			<span class="field-container"><input type="password" style="width:49%;" name="editUserInfoArr[szConPassword]" id="szConPassword"/></span>
		</label>
		<br />
		<input type='hidden' name='showflag' value="<?=$showflag;?>">
		<p align="center" style="width:60%"><a href="javascript:void(0);" class="button1" onclick="changeForwarderPassword();"><span>Save</span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_forwarder_password();"><span><?=t($t_base.'fields/cancel');?></span></a> </p>
	</form>
			</div><br/><br/>
			<? }else{ ?>
			<div id="password_information">
						<h4 style="width:30%;float:left;"><strong><?=t($t_base.'fields/password');?></strong> <a href="javascript:void(0);" onclick="change_forwarder_password();"><?=t($t_base.'fields/edit');?></a></h4>
						<span class="field-container" style="float:left;width:65%;">*************</span>
						<?php
		if($successPasswordUpdatedFlag)
		{
			echo "<div style='clear: both;'></div><p style=color:green>".t($t_base.'messages/password_change_successfully')."</p>";
		}
	?>
			</div><br/><br/>
			<? } ?>
			<div style="clear: both"></div><br/>
			<?if($kForwarderContact->mainAdminEmail){?><div id="mainAdmin"><p>For questions about your access, please contact your administrator on <?=$kForwarderContact->mainAdminEmail;?></p></div><?}?>