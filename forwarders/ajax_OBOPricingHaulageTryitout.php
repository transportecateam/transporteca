<?php
/**
 * view obo pricing haulage
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "HaulaugePricing/";
$idForwarder = $_SESSION['forwarder_id'];
$kForwarder = new cForwarder();
$kWhsSearch = new cWHSSearch();
$kHaulagePricing = new cHaulagePricing();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
if(!empty($_REQUEST['tryoutArr']) && $mode=='REVIEW')
{	
	$haulagePricingData = $kHaulagePricing->getHaulagePricingDetais_review_tryitout($_REQUEST['tryoutArr']);
	
	if(!empty($kHaulagePricing->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		      {
			      ?>
			      <li><?=$values?></li>
			      <?php 
		      }
		?>
		</ul>
		</div>
		<?php
	}
	else
	{
		echo "SUCCESS||||" ;
		echo display_haulage_tryitout_calculation_details($haulagePricingData,$idForwarder);
	}
}
else if(!empty($_REQUEST['tryoutArr']))
{	
	$haulagePricingData = $kHaulagePricing->getHaulagePricingDetais_tryitout($_REQUEST['tryoutArr']);
	 
	if(!empty($kHaulagePricing->arErrorMessages))
	{
            echo "ERROR||||" ;
            ?>
            <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
            <div id="regErrorList">
            <ul>
            <?php
                  foreach($kHaulagePricing->arErrorMessages as $key=>$values)
                  {
                          ?>
                          <li><?=$values?></li>
                          <?php 
                  }
            ?>
            </ul>
            </div>
            <?php
	}
	else
	{
            echo "SUCCESS||||" ;
            echo display_haulage_tryitout_calculation_details($haulagePricingData,$idForwarder);
	}
}
else if($mode=='COUNTRY_NAME')
{
	$szLatitude = sanitize_all_html_input(trim($_REQUEST['szLatitude']));
	$szLongitude = sanitize_all_html_input(trim($_REQUEST['szLongitude']));
	$kWHSSearch = new cWHSSearch();
	$kConfig = new cConfig();
  	$countryDetailAry=getAddressDetails($szLatitude,$szLongitude);
  	//print_r($countryDetailAry);
  	$szCountryCode=$countryDetailAry['szCountryCode'];
  	$idCountry = $kConfig->getCountryName(false,$szCountryCode);
  	
  	if($idCountry>0)
  	{
  		echo "SUCCESS||||".$idCountry;
  	}
  	else
  	{
  		echo "ERROR||||";
  	}
}
?>