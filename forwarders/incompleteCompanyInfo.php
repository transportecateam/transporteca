<?php
/**
 * Customer Account Confirmation 
 */
ob_start();
session_start();
//$szMetaTitle="Transporteca | Incomplete company information";
//$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

if($_SESSION['forwarder_admin_id']>0)
{
	echo "Its seems that you have log on as Admin so you do not need to complete your basic.";
	die;
}
	$t_base = "ForwardersCompany/UserAccess/";
	$kConfig = new cConfig();
	$kForwarderContact = new cForwarderContact();
	$idForwarderContact = $_SESSION['forwarder_user_id'] ;
	
	//print_r($allCountriesArr);
	$forwarderCompanyAry=array();
	$forwarderCompanyAry = $_REQUEST['forwarderCompanyAry'] ;
	
	if(!empty($forwarderCompanyAry))
	{
		if($kForwarderContact->updateForwarderContactFirstLogin($forwarderCompanyAry))
		{
		   $redirect_url = __FORWARDER_DASHBOARD_URL__;
		 ?>
		 <script type="text/javascript">
		 	redirect_url('<?=$redirect_url?>')
		 </script>
		 <?
		 die;
		}
		
	}
	
	if(!empty($forwarderCompanyAry['szPhoneUpdate']))
	{
		$forwarderCompanyAry['szPhone']  = urldecode(base64_decode($forwarderCompanyAry['szPhoneUpdate']));
	}
	if(!empty($forwarderCompanyAry['szMobileUpdate']))
	{
		$forwarderCompanyAry['szMobile'] = urldecode(base64_decode($forwarderCompanyAry['szMobileUpdate']));
	}	
	if(empty($forwarderCompanyAry))
	{
		$forwarderCompanyAry = $kForwarderContact->getForwarderContactDetails($idForwarderContact);
		$idForwarderRole = $forwarderCompanyAry['idForwarderContactRole'] ;
	}
	$allCountriesArr=$kConfig->getAllCountries(true);
		
?>
<script type="text/javascript">
//addPopupScrollClass();
</script>

<div id="popup-bg"></div>
<div id="popup-container">	
<div class="company-edit popup" style="text-align:left;">
	<h5><strong><?=t($t_base.'fields/first_time_login');?></strong></h5>
	<p><?=t($t_base.'messages/first_time_login_title_text');?></p>
	<br>
		<?php
		if(!empty($kForwarderContact->arErrorMessages))
		{
			?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kForwarderContact->arErrorMessages as $key=>$values)
			      {
				      ?><li><?=$values?></li>
				      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
			<?
		}
			?>
		<form name="addForwarderContactInfo" style="text-align:left;" id="addForwarderContactInfo" method="post">
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/f_name');?></p>
				<p class="fl-60"><input type="text" onkeyup="on_enter_key_press(event,'submit_edit_profile_popup')" name="forwarderCompanyAry[szFirstName]" id="szFirstName" value="<?=$forwarderCompanyAry['szFirstName']?>"/></p>
			</div>				
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/l_name');?></p>
				<p class="fl-60"><input type="text" onkeyup="on_enter_key_press(event,'submit_edit_profile_popup')" name="forwarderCompanyAry[szLastName]" id="szLastName" value="<?=$forwarderCompanyAry['szLastName']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/country');?></p>
				<p class="fl-60">
					<select name="forwarderCompanyAry[idCountry]" id="idCountry">
					<option value=""><?=t($t_base.'fields/select_country');?></option>
					<?php
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$forwarderCompanyAry['idCountry']) || ($allCountriesArrs['id']==$postSearchAry['idOriginCountry'])){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
								<?php
							}
						}
					?>
				   </select>
				</p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/office_phone_number');?></p>
				<p class="fl-60"><input type="text" onkeyup="on_enter_key_press(event,'submit_edit_profile_popup')" name="forwarderCompanyAry[szPhone]" id="szPhone" value="<?=$forwarderCompanyAry['szPhone']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/mobile_phone_number');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
				<p class="fl-60"><input type="text" onkeyup="on_enter_key_press(event,'submit_edit_profile_popup')" name="forwarderCompanyAry[szMobile]" id="szMobile" value="<?=$forwarderCompanyAry['szMobile']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/new_password');?> </p>
				<p class="fl-60"><input type="password" onkeyup="on_enter_key_press(event,'submit_edit_profile_popup')" name="forwarderCompanyAry[szPassword]" id="szPassword" value=""/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/confirm_new_assword');?> </p>
				<p class="fl-60"><input type="password" name="forwarderCompanyAry[szConfirmPassword]" onkeyup="on_enter_key_press(event,'submit_edit_profile_popup')" id="szConfirmPassword" value=""/></p>
			</div>
		<br/>
		<p align="center">
			<a href="javascript:void(0)" class="button1" onclick="encode_string('szPhone','szPhoneUpdate','szMobile','szMobileUpdate');submit_edit_profile_popup()"><span><?=t($t_base.'fields/save');?></span></a>&nbsp;
			<a href="<?=__FORWARDER_LOGOUT_URL__?>" class="button2"><span><?=t($t_base.'fields/logout');?></span></a></p>
			<input type="hidden" name="forwarderCompanyAry[idForwarder]" value="<?=$forwarderCompanyAry['idForwarder']?>">
			<input type="hidden" name="forwarderCompanyAry[id]" value="<?=$forwarderCompanyAry['id']?>">
			<input type="hidden" name="forwarderCompanyAry[szPhoneUpdate]" value="" id="szPhoneUpdate">
			<input type="hidden" name="forwarderCompanyAry[szMobileUpdate]" value="" id="szMobileUpdate">
		</form>	
	</div>
</div>