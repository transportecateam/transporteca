<?php
/**
 * Forwarder My Company  Information
 */
 ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
$szMetaTitle="Transporteca | Update Standard Company Information";
//$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );

$t_base = "ForwardersCompany/company_information/";
$kForwardersContact = new cForwarderContact();
$kForwarder = new cForwarder();

$forwarderContactAry = array();
$idForwarder = $_SESSION['forwarder_id'];
$kForwarder->load($idForwarder);

?>
<div id="hsbody-2">
<div id="forwarder_company_div" style="display:none;"></div>
<div id="company_information_tooltip_right" class="help-pop right">
</div>
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
   <?php require_once(__APP_PATH_LAYOUT__ ."/forwarder_company_nav.php"); ?> 
	 <div class="hsbody-2-right">
                <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/swfobject.js"></script>
                <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.uploadify.v2.1.4.min.js"></script>
		<h4 style="margin-bottom:5px;"><strong><?=t($t_base.'titles/display_customers_transporteca');?></strong>&nbsp;<span id="upper_edit_link"><a href="javascript:void(0);" onclick="edit_forwarder_company_cust_details('<?=$idForwarder?>')"><?=t($t_base.'feilds/edit');?></a></span></h4>	
		<div id="forwarder_company_details">
		<?
			echo display_forwarder_company_details($kForwarder,$t_base);
		?>
		
		</div>
		<br><br>
		<h4><strong><?=t($t_base.'titles/registered_company_details');?></strong>&nbsp;<span id="lower_edit_link"><a href="javascript:void(0);" onclick="edit_registered_company_details('<?=$idForwarder?>')"><?=t($t_base.'feilds/edit');?></a></span></h4>	
		<div id="registered_company_details">
		<?
			echo display_registered_company_details($kForwarder,$t_base) ;
		?>
		</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>