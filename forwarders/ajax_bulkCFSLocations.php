<?php
//session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$Excel_export_import=new cExport_Import();
if((int)$_SESSION['forwarder_user_id']==0 && !(isset($_REQUEST)))
{
    header('Location:'.__BASE_URL__);
    exit();	
}
else
{
    $mode = trim(sanitize_all_html_input($_REQUEST['mode']));
    $iWarehouseType = trim(sanitize_all_html_input($_REQUEST['iWarehouseType']));
     
    if($mode == 'DOWNLOAD_CFS_EXCEL')
    {
        if($Excel_export_import->downloadCFSLocationTemplate($iWarehouseType))
        {		
            if($iWarehouseType==2)
            {
                $file_name = __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca_airport_warehouse_locations_upload_sheet.xlsx";
                $szFileName = "Transporteca_airport_warehouse_locations_upload_sheet.xlsx";
            }
            else
            {
                $file_name = __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/Transporteca_CFS_locations_upload_sheet.xlsx";
                $szFileName = "Transporteca_CFS_locations_upload_sheet.xlsx";
            }
            
            ob_clean();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            //$f = fopen($file_name,'rb');
            //$fsize=filesize($file_name);
            header('Content-Disposition: attachment; filename='.$szFileName);
            ob_clean();
            flush();
            readfile($file_name);
            exit; 
        }
    }
}
