<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$szMetaTitle="Transporteca | CFS Locations";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base_error="Error/";
$t_base = "BulkUpload/";
$kWHSSearch=new cWHSSearch();
if(isset($_POST['cfsAddEditArr']))
{
    $iWarehouseType = $_POST['cfsAddEditArr']['iWarehouseType'];
    if($_POST['cfsAddEditArr']['idEditWarehouse']=='')
    { 
        $kWHSSearch->addWarehouse($_POST['cfsAddEditArr']);
    }
    elseif(isset($_POST['cfsAddEditArr']['idEditWarehouse']))
    { 
        $kWHSSearch->saveWarehouse($_POST['cfsAddEditArr']);
    } 
    if(!empty($_POST['cfsAddEditArr']['szPhoneNumberUpdate']))
    {
        $_POST['cfsAddEditArr']['szPhoneNumber'] = urldecode(base64_decode($_POST['cfsAddEditArr']['szPhoneNumberUpdate']));
    }
    if(!empty($kWHSSearch->arErrorMessages))
    {
        ?>
	Error|||||<div id="regError" class="errorBox">
	<div class="header"><?=t($t_base_error.'please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kWHSSearch->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	</div>
	
	<script type="text/javascript">
	$(window).scrollTop($('#regError').offset().top);
	//$('#regError').focus();
	</script>
	<?php
	unset($kWHSSearch->arErrorMessages); 
    }
    else
    { 
        ?>
        <p><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/airport_warehouse_location_top_heading'):t($t_base.'title/cfs_location_top_heading'); ?></p>
        <br />
        <?php require_once( __APP_PATH__ ."/forwarders/ajax_cfsNameList.php" ); ?> 
        <br />
        <?php 
        unset($_POST['cfsAddEditArr']);
        $_POST['cfsAddEditArr']=array();
        unset($_REQUEST);
        require_once( __APP_PATH__ ."/forwarders/addUpdateCFSDetails.php" );
    }
}
if(isset($_POST['mode']))
{
	$mode = trim(sanitize_all_html_input($_POST['mode']));
        $iWarehouseType = trim(sanitize_all_html_input($_POST['iWarehouseType']));
	
	if($mode == 'DELETE_WAREHOUSE')
	{
		$id	=	(int)$_POST['id'];
		$batch = (int)$_POST['batch'];
		if($id>0)
		{
                    $kWHSSearch->dleteWarehouse($id,$batch,$iWarehouseType);
		}
	}
	if($mode == 'LEAVE_PAGE')
	{
		$path = trim(sanitize_all_html_input($_POST['path']));
		$idBatch = (int)$_SESSION['idBatch'];
                $iWarehouseType = (int)$_SESSION['iWarehouseType'];
                if($iWarehouseType==2)
                {
                    $szCfsText = "airport warehouse";
                }
                else
                {
                    $szCfsText = "CFS";
                }
		echo '
			<div id="popup-bg"></div>
					<div id="popup-container" style="padding-top:100px;">
						<div class="popup" style="width:400px;"> 
						<p class="close-icon" align="right">
						<a onclick="showHidePopUp();" href="javascript:void(0);">
						<img alt="close" src="'.__BASE_STORE_IMAGE_URL__.'/close1.png">
						</a>
						</p>
							<h5><b>Leaving '.$szCfsText.' locations bulk upload</b></h5>
							<p>You are trying to navigate away from the approval of your '.$szCfsText.' locations bulk upload. What would you like us to do with the remaining '.(int)($_SESSION['CFS']-$_SESSION['eachCfs']+1).' '.$szCfsText.' locations from this upload file which have not yet been approved?</p>
							<br />
							<p>We can ignore the remaining '.$szCfsText.' locations uploaded, or we can transfer the locations to the menu called Upload service - Awaiting approval. You will then be able to review and approve them later.</p>
							<br />
							<a class="button2" onclick="showHidePopUp();"><span>cancel</span></a>
							<a class="button1" onclick="deleteCFS(\''.$idBatch.'\',\''.$path.'\',\''.$iWarehouseType.'\');"><span>ignore</span></a>
							<a class="button1" onclick="transfer(\''.$idBatch.'\',\''.$path.'\');"><span>transfer</span></a>
						</div>
					</div>
		';
	}
	if($mode == 'DELETE_BATCH')
	{
		$batch = trim(sanitize_all_html_input($_POST['batch']));
                $iWarehouseType = trim(sanitize_all_html_input($_POST['iWarehouseType']));
		$kWHSSearch->batchDelete($batch);
                $_SESSION['show_once'] =0;
	}
	if($mode == 'SELECT_COUNTRIES_FORM_LIST')
	{
		$selectListCFS = array();
		$selectListCFS = sanitize_all_html_input($_POST['content']);
		$selectListCFS = explode(',',$selectListCFS);
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries(true);
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								if(in_array($allCountriesArrs['id'],$selectListCFS))
								{
									?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						}
						
	
	}
	if($mode == 'REMOVE_COUNTRIES_FROM_LIST')
	{
		$selectListCFS = array();
		$selectListCFS = sanitize_all_html_input($_POST['content']);
		$selectListCFS = explode(',',$selectListCFS);
		$country   = (int)$_POST['country'];
		$latitude  = (int)$_POST['latitude'];
		$longitude = (int)$_POST['longitude'];
		$countryList = $kWHSSearch->findWarehouseCountryList($country,$latitude,$longitude);
		if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."'>".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	}
	if($mode == 'SHOW_POP_UP')
	{
		echo '
			<div id="popup-bg"></div>
					<div id="popup-container" style="padding-top:100px;">
						<div class="popup" style="width:400px;"> 
						<p class="close-icon" align="right">
						<a onclick="showHidePopUp();" href="javascript:void(0);">
						<img alt="close" src="'.__BASE_STORE_IMAGE_URL__.'/close1.png">
						</a>
						</p>	
						<h4><b>Countries serviced</b></h4>
							<br />
								<p>You have not updated any countries serviced by this CFS.<br/> <br/>Please confirm that
								you do not accept customers delivering or picking up cargo directly at this
								CFS.</p>
								<br />
								<p align="center">
								<a class="button1" onclick="submitCFSlisting();"><span>confirm</span></a>
								<a class="button2" onclick="showHidePopUp();"><span>back</span></a>
								</p>
						</div>
					</div>
			</div>						
		';
	}
	if($mode == 'RESET_SESSION_DETAILS')
	{
		unsetContent();
		echo  true;
	}
	if($mode == 'SHOW_MULTIPLE_COUNTRIES_SELECT')
	{
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries(true);
		$idCountry = (int)sanitize_all_html_input($_POST['idCountry']);
                $iWarehouseType = (int)sanitize_all_html_input($_POST['iWarehouseType']);
		$kWHSSearch->set_idCountry($idCountry);
		$countryList = $kWHSSearch->findWarehouseCountryList($kWHSSearch->idCountry,'','');?>
	    	<div id="selectForCountrySelection">
	    	 <?php 
                    //$selectListCFS = array(1,2,3);
                    if($kWHSSearch->idCountry!= NULL)
                    {
                        $selectListCFS = array($kWHSSearch->idCountry);
                    }
	    	 ?>
                <h4><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_this_airport_warehouse'):t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
                <p style="margin-top: 5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/add_local_market_airport_cfs'):t($t_base.'messages/add_local_market');?></p>
	    	<div style="direction: ltr;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td valign="top" width="215" style="padding-top:5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_airport_warehouse'):t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			<?php
			if(!empty($allCountriesArr))
			{
				foreach($allCountriesArr as $allCountriesArrs)
				{
					if(in_array($allCountriesArrs['id'],$selectListCFS))
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			}
		?>	
	    	 </select>
	    	  <br />
	    	   <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	  </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/sorted_on_airport_cfs'):t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>	
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 <?php
	    	 	if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."' >".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	    	 ?>
	    	 </select>
	    	 <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	  	   		<div class="profile-fields" style="margin-top: 14px;float:left;">
	    	 	<div class="field-alert" id="example" style="display:none;">
	    	 		<div>
	    	 		<p><?=t($t_base.'messages/for_your_rotterdam');?></p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'messages/for_your_hong_kong');?></p>
	    	 		</div>
	    	 	</div>
	    	 	</div>
	    	 </div>
		
	    
		<br /><br />
	
	    	 <?php //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>
<?php
	}
	if( $_POST['mode'] == 'mapCountry' )
	{	
		$id = sanitize_all_html_input($_POST['id']);
		$kWHSSearch = new cWHSSearch();
		$latLongs = $kWHSSearch->getLatitudeLongitudeForCountry($id);
		echo $latLongs['szLatitude'].",".$latLongs['szLongitude'];
	}
	if($_POST['mode'] == 'TRANSFER_BATCH')
	{
            $id = sanitize_all_html_input($_POST['id']);
            $kWHSSearch = new cWHSSearch();
            $kWHSSearch->transferUploadCFSLocationData($id);	
            $_SESSION['show_once'] =0;
	}
}
?>