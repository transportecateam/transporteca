<?php 
/**
  *  Admin---> System --> Insurance
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

require_once( __APP_PATH_CLASSES__.'/booking.class.php' );
require_once( __APP_PATH_CLASSES__.'/warehouseSearch.class.php' );
require_once( __APP_PATH_CLASSES__.'/tracking.class.php' ); 

$t_base="management/pendingTray/";
$t_base_error="management/Error/";
checkAuthForwarder_ajax();
 
$kBooking = new cBooking(); 
$kConfig = new cConfig();
$mode = sanitize_all_html_input(trim($_REQUEST['mode'])); 
$action = sanitize_all_html_input(trim($_REQUEST['action'])); 

$idForwarderSession = $_SESSION['forwarder_id'] ;

if($mode=='DISPLAY_PENDING_TASK_DETAILS')
{
    $idBookingQuote = sanitize_all_html_input(trim($_REQUEST['quote_id']));  
    $iClosedTaskScreen = sanitize_all_html_input(trim($_REQUEST['closed_task_flag'])); 
    $iCourierBooking = sanitize_all_html_input(trim($_REQUEST['iCourierBooking']));
    
    if($idBookingQuote>0)
    {
        if($iCourierBooking==1)
        {
            $quotesDetailsAry = array();
            $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$idBookingQuote);
            if(empty($quotesDetailsAry))
            {
                $quotesDetailsAry = $kBooking->getAllBookingQuotesByForwarder($idForwarderSession,$idBookingQuote,false,'','','','',true);
            }
        }
        else
        {
            $quotesDetailsAry = array();
            $quotesDetailsAry = $kBooking->getAllBookingQuotesByForwarder($idForwarderSession,$idBookingQuote,false,'','','','',true);
        }
         
        if(!empty($quotesDetailsAry[1]))
        {
            if($quotesDetailsAry[1]['szForwarderBookingTask']=='T140' || $quotesDetailsAry[1]['szForwarderBookingTask']=='T150')
            {?>
                <?php
                echo "SUCCESS||||";
                if($quotesDetailsAry[1]['iClosed']==1 && $quotesDetailsAry[1]['szForwarderBookingTask']=='T150')
                {
                    echo display_pending_quotes_overview_pane_courier($quotesDetailsAry,$kBooking,false,true); 
                    if($iClosedTaskScreen==0)
                    {?>
                        <script type="text/javascript">
                            showChangeCourierLabelPopup('<?php echo $idBookingQuote;?>');
                        </script>
                    <?php    
                    }    
                }
                else
                {
                    echo display_pending_quotes_overview_pane_courier($quotesDetailsAry,$kBooking);
                }
                die; 
            } 
            else
            { 
                if($quotesDetailsAry[1]['iClosed']==1 && $iClosedTaskScreen!=1)
                {
                    echo "SUCCESS_CLOSED_QUOTE||||";
                    echo display_pending_quotes_overview_pane($quotesDetailsAry,$kBooking,false,true); 
                    echo "||||";
                    echo display_closed_quote_notification_popup($quotesDetailsAry[1]);
                    echo "||||";
                    die; 
                }
                else
                {
                    echo "SUCCESS||||";
                    if($iClosedTaskScreen==1)
                    {
                        echo display_pending_quotes_overview_pane($quotesDetailsAry,$kBooking,false,true); 
                    }
                    else
                    {
                        echo display_pending_quotes_overview_pane($quotesDetailsAry,$kBooking);
                    }
                    die; 
                }
            }
        } 
    }
}  
else if($mode=='ADD_MORE_BOOKING_QUOTES')
{  
    $number = sanitize_all_html_input($_REQUEST['number'])+1; 
    $kConfig = new cConfig();
    $kForwarder = new cForwarder();
      
    echo display_forwarder_quote_form($kBooking,$number);
    die;
}
else if(!empty($_POST['addBookingQuotePriceAry']))
{
    $idBookingQuote = sanitize_all_html_input($_POST['addBookingQuotePriceAry']['idBookingQuote']);
    $saveFlag=trim($_POST['saveFlag']);
    $_POST['addBookingQuotePriceAry']['saveFlag']=$saveFlag;
    
    if($kBooking->addQuotePricingByForwarder($_POST['addBookingQuotePriceAry']))
    {
        $iSuccessCount = $kBooking->iSuccessCount;
         
        $kBooking = new cBooking();
	$incompleteTaskAry = array();
	$incompleteTaskAry = $kBooking->getAllBookingQuotesByForwarder($idForwarder,false,true,false,false,array(),1,true);
        $incompleteTaskAry1=$kBooking->getAllBookingCourierLabelByForwarder($idForwarder,0,false);
	
        $mergeArr=array_merge($incompleteTaskAry,$incompleteTaskAry1);
        
        $mergeArr = sortArray($mergeArr, 'dtTaskStatusUpdatedOn','DESC');
 
        $incompleteTaskAry = $mergeArr ;
        $quotesDetailsAry = array();
        $quotesDetailsAry = $kBooking->getAllBookingQuotesByForwarder($idForwarderSession,$idBookingQuote,false); 
        $t_base = "BulkUpload/";
		$t_base_error="Error";
        echo "SUCCESS||||";
        echo '<div style="text-align:left;">
			<h4 class="clearfix">
			<strong>'.t($t_base.'title/select_a_task_to_view_details').'</strong>
			<a href="javascript:void(0);" style="float:right" onclick="showPastTask();"><span>'.t($t_base.'fields/show_completed_tasks').'</span></a>
			</h4>
			
		</div>
		<div id="pending_quote_list">'; 
        
        echo display_all_peding_tasks_forwarder($mergeArr,false,$idForwarderSession,false,$idForwarderSession);
        echo "<div>";
        echo "||||";
        echo display_pending_quotes_overview_pane($quotesDetailsAry,$kBooking,$iSuccessCount,true); 
        echo "||||";
        die;
    }
    else
    {
        $quotesDetailsAry = array();
        $quotesDetailsAry = $kBooking->getAllBookingQuotesByForwarder($idForwarderSession,$idBookingQuote,'','','','','',true); 
    
        echo "ERROR||||"; 
        echo display_pending_quotes_overview_pane($quotesDetailsAry,$kBooking); 
        die; 
    }
    die;
}
else if($mode=='SHOW_PENDING_QUOTE_PAGINATION')
{
    $page=(int)$_POST['page'];

    if($_SESSION['forwarder_admin_id']>0)
    {
        //$kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($_SESSION['forwarder_id']);
        $err_msg_file='';
        $idForwarder=$kForwarder->id;
        $fileUploadFlag=false;
    }
    else
    {
        $kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($kForwarderContact->idForwarder);
        $err_msg_file='';
        $idForwarder=$kForwarderContact->idForwarder;
        $fileUploadFlag=false;
    }  
    
    $kBooking = new cBooking();
    $incompleteTaskAry = array();
    $incompleteTaskAry1=array();
    $incompleteTaskAry = $kBooking->getAllBookingQuotesByForwarder($idForwarder,false,true,false,false,array(),$page,true);
    $incompleteTaskAry1=$kBooking->getAllBookingCourierLabelByForwarder($idForwarder,0,false);
    
    $mergeArr = array_merge($incompleteTaskAry,$incompleteTaskAry1);
    
    $mergeArr=sortArray($mergeArr, 'dtTaskStatusUpdatedOn','DESC');  
    echo display_all_peding_tasks_forwarder($mergeArr,$idSelectedQuote,$_SESSION['forwarder_id'],false,$idForwarder,$page);
    die(); 
}
else if($mode=='SHOW_PAST_QUOTE_PAGINATION')
{
    $page=(int)$_POST['page'];

    if($_SESSION['forwarder_admin_id']>0)
    {
        //$kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($_SESSION['forwarder_id']);
        $err_msg_file='';
        $idForwarder=$kForwarder->id;
        $fileUploadFlag=false;
    }
    else
    {
        $kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($kForwarderContact->idForwarder);
        $err_msg_file='';
        $idForwarder=$kForwarderContact->idForwarder;
        $fileUploadFlag=false;
    }

    $kBooking = new cBooking();
    $incompleteTaskAry = array();
    $incompleteTaskAry = $kBooking->getAllBookingQuotesByForwarder($idForwarder,false,false,false,true,array(),$page,true);
    $incompleteTaskAry1=$kBooking->getAllBookingCourierLabelByForwarder($idForwarder,0,true);

    $mergeArr=array_merge($incompleteTaskAry,$incompleteTaskAry1);

    $mergeArr=sortArray($mergeArr, 'dtStatusUpdatedOn','DESC');
   // print_r($mergeArr);
    echo display_all_peding_tasks_forwarder($mergeArr,$idSelectedQuote,$_SESSION['forwarder_id'],true,$idForwarder,$page);
    die(); 
}
else if($mode=='SHOW_PENDING_QUOTE_DATA')
{
    $kConfig = new cConfig();
    $t_base = "BulkUpload/";
    $t_base_error="Error"; 

    if($_SESSION['forwarder_admin_id']>0)
    {
        //$kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($_SESSION['forwarder_id']);
        $err_msg_file='';
        $idForwarder=$kForwarder->id;
        $fileUploadFlag=false;
    }
    else
    {
        $kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($kForwarderContact->idForwarder);
        $err_msg_file='';
        $idForwarder=$kForwarderContact->idForwarder;
        $fileUploadFlag=false;
    } 
    $iProfitType = $kForwarder->iProfitType ; 

    $kBooking = new cBooking();
    $incompleteTaskAry = array();
    $incompleteTaskAry1=array();
    $incompleteTaskAry = $kBooking->getAllBookingQuotesByForwarder($idForwarder,false,true,false,false,array(),1,true);
    $incompleteTaskAry1=$kBooking->getAllBookingCourierLabelByForwarder($idForwarder,0,false);
    
    $mergeArr=array_merge($incompleteTaskAry,$incompleteTaskAry1);
    
    $mergeArr=sortArray($mergeArr, 'dtTaskStatusUpdatedOn','DESC');
    if((int)$_SESSION['forwarder_booking_quotation_key']>0)
    {
        $idSelectedQuote = $_SESSION['forwarder_booking_quotation_key'] ;

        $_SESSION['forwarder_booking_quotation_key'] = '';
        unset($_SESSION['forwarder_booking_quotation_key']);
    }
    else if($szBookingKey>0)
    {
        $idSelectedQuote = $szBookingKey ;
        $_SESSION['forwarder_booking_booking_key'] = '';
        unset($_SESSION['forwarder_booking_booking_key']);
    } 
    else if($szQuotationKey>0)
    {
        $idSelectedQuote = $szQuotationKey ;
        $_SESSION['forwarder_booking_quotation_key'] = '';
        unset($_SESSION['forwarder_booking_quotation_key']);
    }  
    ?>
    <div id="pending_task_listing_container">
    	<div style="text-align:left;">
            <h4 class="clearfix">
                <strong><?php echo t($t_base.'title/select_a_task_to_view_details');?></strong>
                <a href="javascript:void(0);" style="float:right" onclick="showPastTask();"><span><?=t($t_base.'fields/show_completed_tasks');?></span></a>
            </h4> 
        </div>
        <div id="pending_quote_list">
        <?php
            echo display_all_peding_tasks_forwarder($mergeArr,$idSelectedQuote,$_SESSION['forwarder_id'],false,$idForwarder);
        ?>
        </div>
    </div> 
    <br>
    <div id="pending_task_overview_main_container"  class="pending-tray-scrollable-div-overview" style="display:none;">  
    </div>
    <!-- <br class="clear-all" /> 
    <div class="btn-container" style="float:right;">
        <a href="<?php echo __FORWARDER_PAST_QUOTES_URL__; ?>" class="button1"><span><?=t($t_base.'fields/completed');?></span></a>
    </div>
    <br class="clear-all" />  -->
	<?php 

}
else if($mode=='SHOW_PAST_QUOTE_DATA')
{
    $t_base = "BulkUpload/";
    $t_base_error="Error";

    if($_SESSION['forwarder_admin_id']>0)
    {
        //$kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($_SESSION['forwarder_id']);
        $err_msg_file='';
        $idForwarder=$kForwarder->id;
        $fileUploadFlag=false;
    }
    else
    {
        $kForwarderContact->load($_SESSION['forwarder_user_id']);
        $kForwarder->load($kForwarderContact->idForwarder);
        $err_msg_file='';
        $idForwarder=$kForwarderContact->idForwarder;
        $fileUploadFlag=false;
    }

    $iProfitType = $kForwarder->iProfitType ; 

    $kBooking = new cBooking();
    $incompleteTaskAry = array();
    $incompleteTaskAry = $kBooking->getAllBookingQuotesByForwarder($idForwarder,false,false,false,true,array(),1,true);
    $incompleteTaskAry1=$kBooking->getAllBookingCourierLabelByForwarder($idForwarder,0,true);

    $mergeArr=array_merge($incompleteTaskAry,$incompleteTaskAry1);

    $mergeArr=sortArray($mergeArr, 'dtStatusUpdatedOn','DESC');
    if((int)$_SESSION['forwarder_booking_quotation_key']>0)
    {
        $idSelectedQuote = $_SESSION['forwarder_booking_quotation_key'] ;

        $_SESSION['forwarder_booking_quotation_key'] = '';
        unset($_SESSION['forwarder_booking_quotation_key']);
    }
    else if($szQuotationKey>0)
    {
        $idSelectedQuote = $szQuotationKey ;
        $_SESSION['forwarder_booking_quotation_key'] = '';
        unset($_SESSION['forwarder_booking_quotation_key']);
    } 
    ?>
    <div id="pending_task_listing_container">
    	<div style="text-align:left;">
            <h4 class="clearfix">
                <strong><?php echo t($t_base.'title/select_a_task_to_view_details');?></strong> 
                <a href="javascript:void(0);" style="float:right" onclick="showPendingTask();"><span><?=t($t_base.'fields/show_pending_tasks');?></span></a>
            </h4> 
        </div>
        <div id="past_quote_list">
        <?php
            echo display_all_peding_tasks_forwarder($mergeArr,$idSelectedQuote,$_SESSION['forwarder_id'],true,$idForwarder);
        ?>
        </div>
    </div> 
    <br>
    <div id="pending_task_overview_main_container"  class="pending-tray-scrollable-div-overview" style="display:none;">  </div>
    <!--  <br class="clear-all" /> 
    <div class="btn-container" style="float:right;">
        <a href="<?php echo __FORWARDER_PENDING_QUOTES_URL__; ?>" class="button1"><span><?=t($t_base.'fields/pending');?></span></a>
    </div>
    <br class="clear-all" />  -->
   <?php 
}
else if($_REQUEST['flag']=='delete_uploaded_file')
{
	$allFilename=$_REQUEST['allFilename'];
	$tempfilename=$_REQUEST['tempfilename'];
	$filename=$_REQUEST['filename'];
	$originFile=__UPLOAD_COURIER_LABEL_PDF__."/".$tempfilename;
	@unlink($originFile);
        $newAllFileStr='';
	if(!empty($allFilename))
	{
		$allUploadedFileArr=explode(";",$allFilename);
		if(!empty($allUploadedFileArr))
		{
			for($i=0;$i<count($allUploadedFileArr);$i++)
			{
				$newFileStr='';
				$uploadFileArr=explode("#####",$allUploadedFileArr[$i]);
				if($filename!=$uploadFileArr[0])
				{
					echo "<span class='uploadfilelist'><span>".$uploadFileArr[0]." (".round($uploadFileArr[2])." kB)</span><a href='javascript:void(0)' onclick='delete_uploaded_courier_label(\"".$uploadFileArr[0]."\",\"".$uploadFileArr[1]."\");'><img src='".__BASE_STORE_IMAGE_URL__."/delete-icon.png'></a></span>";
					
					$newFileStr=$allUploadedFileArr[$i];
					
					if($newAllFileStr!='')
					{
						$newAllFileStr= $newAllFileStr.";".$newFileStr;
					}
					else
					{
						$newAllFileStr=$newFileStr;
					}
				}
			}
		}
	}
        ?>
		<script>
			var count=$("#filecount").attr('value');
			var newcount=parseInt(count)-1;
			$("#filecount").attr('value',newcount);
			$("#file_name").attr('value','<?=$newAllFileStr?>');
		</script>
	<?php

}
else if(!empty($_POST['addBookingCourierLabelAry']) && $mode!='DELETE_COURIER_LABEL_PDF')
{
    $kCourierServices = new cCourierServices(); 
    if($_REQUEST['mode']=='SUBMIT_DATA')
    {
        if($kCourierServices->createBookingCourierLabel($_POST['addBookingCourierLabelAry'],true,true))
        {        
            $quotesDetailsAry = array();
            $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$_POST['addBookingCourierLabelAry']['idBooking']);
            echo "SUCCESS||||";
            echo display_pending_quotes_overview_pane_courier($quotesDetailsAry,$kBooking,false,true); 
            die;
        }
        else
        {
            $quotesDetailsAry = array();
                $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$_POST['addBookingCourierLabelAry']['idBooking']);
                echo "ERROR||||"; 
                echo display_pending_quotes_overview_pane_courier($quotesDetailsAry,$kBooking,false,false,$kCourierServices); 
                die;
        }
    }
    else 
    { 
        if($_REQUEST['saveFlag']==1)
        {
            if($kCourierServices->createBookingCourierLabel($_POST['addBookingCourierLabelAry'],false,true))
            {        
                $quotesDetailsAry = array();
                $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$_POST['addBookingCourierLabelAry']['idBooking']);
                echo "ERROR||||"; 
                echo display_pending_quotes_overview_pane_courier($quotesDetailsAry,$kBooking,false,false,$kCourierServices); 
                die;
            }

        }
        else 
        {
            if($kCourierServices->createBookingCourierLabelValidation($_POST['addBookingCourierLabelAry'],true))
            {        
                echo "SUCCESS||||";
                echo sendShipperCourierLabel($_POST['addBookingCourierLabelAry']['idBooking'],$kCourierServices,true);
                die();
            }
            else 
            {
               $quotesDetailsAry = array();
               $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$_POST['addBookingCourierLabelAry']['idBooking']);
               echo "ERROR||||"; 
               echo display_pending_quotes_overview_pane_courier($quotesDetailsAry,$kBooking,false,false,$kCourierServices); 
               die; 
            }
        }
    }
}
else if($mode=='OPEN_CHANGE_COURIER_LABEL_POPUP')
{
        $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
         
        $quotesDetailsAry = array();
        $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$idBooking);
        $idLabelCreatedBy=$quotesDetailsAry[1]['idStatusUpdatedBy'];
        
        $kForwarderContact=new cForwarderContact();
        if($idLabelCreatedBy>0)
        {
            $kForwarderContact->load($idLabelCreatedBy); 
            
            $szName=$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
        }
        else
        {
                $szName="Transporteca";
        }
    ?>
     <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup abandon-popup">
            <p class="close-icon" align="right">
                <a href="javascript:void(0);" onclick="closePopupUpdatedService('contactPopup');">
                <img src="<?php echo __BASE_STORE_IMAGE_URL__?>/close1.png" alt="close">
                </a>
            </p>
            <h5><?=t($t_base.'title/labels_already_uploaded')?></h5><br/>
            <p><?php echo $szName;?> <?=t($t_base.'messages/msg_13');?>?</p>
            <br class="clear-all" /> 
            <div class="btn-container" align="center">
                    <a href="javascript:void(0);"  onclick="changeCourierLabelStatus('<?php echo $idBooking;?>');" id="cancel_button" class="button1" title="Yes"><span><?=t($t_base.'fields/yes');?></span></a>
                   <a href="javascript:void(0);" onclick="closePopupUpdatedService('contactPopup');" id="confirm_button" class="button2" title="No" ><span><?=t($t_base.'fields/no');?></span></a>

            </div>
        </div>
    </div>   
            
<?php    
}
else if($mode=='CHANGE_COURIER_LABEL_POPUP')
{
     $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
         
    $quotesDetailsAry = array();
    $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$idBooking);
    
    $kBooking = new cBooking();
    $dtResponseTime = $kBooking->getRealNow();
    
    $kBooking = new cBooking();
    $fileLogsAry = array();
    $fileLogsAry['szTransportecaStatus'] = "S7";
    $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
    $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
    $fileLogsAry['szForwarderBookingStatus'] = '';                                    
    $fileLogsAry['szForwarderBookingTask'] = 'T140'; 
    $fileLogsAry['iClosed'] = '0'; 

    if(!empty($fileLogsAry))
    {
        foreach($fileLogsAry as $key=>$value)
        {
            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }  
    $file_log_query = rtrim($file_log_query,",");   
    $kBooking->updateBookingFileDetails($file_log_query,$quotesDetailsAry[1]['idFile'],true); 
    
    $kCourierServices= new cCourierServices();
    $kCourierServices->reOpenBookingQuote($idBookingQuote,$quotesDetailsAry[1]['idBooking']);
        
}
else if($mode=='DELETE_COURIER_LABEL_PDF')
{
    $idFile=$_POST['addBookingCourierLabelAry']['idFile'];
    $idBookingFile=$_POST['addBookingCourierLabelAry']['idBookingFile'];
    $idBooking=$_POST['addBookingCourierLabelAry']['idBooking'];
    $file_name=$_POST['file_name'];
    $counter=(int)$_POST['counter'];
    $counter=$counter-1;
    $fileArr=explode(";",$file_name);
    
    if(!empty($fileArr))
    {
        for($i=0;$i<count($fileArr);++$i)
        {
            if($i!=$counter)
            {
                $newArr[]=$fileArr[$i];
            }
        }
        
        $newFileStr=  implode(";", $newArr);
    }
    $kCourierServices= new cCourierServices();
    if((int)$idFile>0)
    {
            
         $kCourierServices->deleteCourierLabelPdfFile($idFile,$idBooking,$idBookingFile);
   
    
        
    }
    $totalCount=count($newArr);
    $_POST['addBookingCourierLabelAry']['file_name']=$newFileStr;
    $_POST['addBookingCourierLabelAry']['filecount']=$totalCount;
    
     $textpages="page";
    if($totalCount>1)
    {
        $textpages="pages";
    }
    
    $pageText=$totalCount." ".$textpages;
    $_POST['addBookingCourierLabelAry']['filecount']=$totalCount;
    $quotesDetailsAry = array();
    $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$_POST['addBookingCourierLabelAry']['idBooking']);
    echo display_pending_quotes_overview_pane_courier($quotesDetailsAry,$kBooking,false,false); 
    die; 
}
else if($mode=='TRACKING_NUMBER_STATUS')
{
    $idBooking=(int)$_POST['idBooking'];
    $szMasterTrackingNumber=trim($_POST['szMasterTrackingNumber']);
    $idServiceProvider=(int)$_POST['idServiceProvider'];
    $kTracking = new cTracking(); 
    $trackingARY = $kTracking->createAfterShipTracking($idBooking,$szMasterTrackingNumber,$idServiceProvider);  
    if($kTracking->tracking_error==true && __ENVIRONMENT__ == "LIVE")
    {
        echo "ERROR||||"; 
    }
    else
    {
        echo "SUCCESS||||";
    }
    echo $kTracking->szAfterShipTrackingNotification;
    die;
    
    /*
    *   if((int)$_POST['iType']=='1')
        {    
            $resArr=fedexTrackingNumberApi($idBooking,$szMasterTrackingNumber);
        }
        else if((int)$_POST['iType']=='2')
        {
            $resArr=upsTrackingNumberApi($idBooking,$szMasterTrackingNumber);
        }
     * echo $resArr;
    */ 
    
}
else if($mode=='SHOW_OLD_QUOTE_OF_FORWARDER')
{
    
    $szSortField = sanitize_all_html_input(trim($_POST['sort_field'])); 
    $szSortType = sanitize_all_html_input(trim($_POST['sort_by']));  
    
    $estimateSearchAry=array();
    $estimateSearchAry['idOriginCountry'] = $_POST['idOrginCountry'];
    $estimateSearchAry['idDestinationCountry'] = $_POST['idDestinationCountry'];
    $estimateSearchAry['iForwarderFlag'] = '1';
    $estimateSearchAry['idForwarder'] = $_SESSION['forwarder_id'];
    $estimateSearchAry['szSortField'] = $szSortField;
    $estimateSearchAry['szSortType'] = $szSortType; 
    //echo $fApplicableVatRate."fApplicableVatRate";
    
    $quoteEstimateAry = array();
    $quoteEstimateAry = $kBooking->getAllQuoteForEstimatePane($estimateSearchAry);
    echo "SUCCESS||||";
    echo forwarder_old_quote_listing($quoteEstimateAry,$estimateSearchAry);
    die();
}
?>