<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
//print_r($_SESSION);
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca | OBO Haulage";
$display_profile_not_completed_message=true;
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
checkAuthForwarder_ajax();
$idForwarder=$_SESSION['forwarder_id'];
$t_base_error="Error";
//echo $idForwarder;
//$nonAcc=$kForwarder->nonAcceptance(1,1);
if(isset($_POST) && $idForwarder>0)
{	$mode = sanitize_all_html_input(trim($_POST['mode']));
	$id	  = sanitize_all_html_input(trim($_POST['id']));
	if($mode=="ADD_GOODS" &&  is_string($id))
	{
		$response=$kForwarder->add_nonacc($id,$idForwarder,2);
		
	}
	if($mode=="ADD_EMAIL" &&  is_string($id))
	{
		$response=$kForwarder->add_nonacc($id,$idForwarder,1);
		
	}
	if(isset($_POST['showNonAcceptanceData']))
	{	
		foreach($_POST['showNonAcceptanceData'] as $value)
		{
			$id=sanitize_all_html_input(trim($value));
			$response=	$kForwarder->remove_nonacc($id,$idForwarder);
			$mode="ADD_GOODS";
		}
	}
	if(isset($_POST['showNonAcceptanceEmail']))
	{	
		foreach($_POST['showNonAcceptanceEmail'] as $value)
		{
			$id=sanitize_all_html_input(trim($value));
			$response=	$kForwarder->remove_nonacc($id,$idForwarder);
			$mode="ADD_EMAIL";
		}
	}
}

?>
	<? 
	if(!empty($kForwarder->arErrorMessages)){
	echo "ERROR||||";
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kForwarder->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?} 
	
	if($response)
	{ echo "SUCCESS||||";
	if($mode=="ADD_GOODS")
	$nonAcc=$kForwarder->nonAcceptance($idForwarder,2);
	if($mode=="ADD_EMAIL")
	$nonAcc=$kForwarder->nonAcceptance($idForwarder,1);
	?>
	<? if($nonAcc!='' && $mode=="ADD_GOODS")
	{ foreach($nonAcc as $nonAcceptance){ ?>
	<option value="<?=$nonAcceptance['id']; ?>"><? echo $nonAcceptance['szNonAcceptance']; ?></option>
	<? }}
	if($nonAcc!='' && $mode=="ADD_EMAIL")
	{ foreach($nonAcc as $nonAcceptance){ ?>
	<option value="<?=$nonAcceptance['id']; ?>" >
	
	<? echo $nonAcceptance['szNonAcceptance']; ?>
	</option>
	<? }}
	} ?>