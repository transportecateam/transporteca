<?php 
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
ini_set('max_execution_time',1200);
ini_set('memory_limit', '-1');
$t_base_error="Error";
$t_base="BulkUpload/";
$kUploadBulkService = new cUploadBulkService();
$Excel_export_import= new cExport_Import();
$kConfig = new cConfig();
$kWHSSearch=new cWHSSearch();
$kHaulagePricing = new cHaulagePricing();
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

$objPHPExcel=new PHPExcel();
if($_REQUEST['flag']=='approve_processing_cost')
{
    $id=$_REQUEST['id'];
    $kUploadBulkService->load($id);
?> 
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup signin-popup signin-popup-verification" style="width:400px;">
            <p class="close-icon" align="right">
                <a onclick="cancel_processing_cost();" href="javascript:void(0);">
                    <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <b><?=t($t_base.'title/approve_processing_cost')?></b><br/><br/>
            <p><?=t($t_base.'title/approve_processing_1')?>  <?=$kUploadBulkService->szCurrency?> <?=number_format((float)$kUploadBulkService->fProcessingCost,2)?> <?=t($t_base.'title/for')?> <?=$kUploadBulkService->iFileCount?> <?php if($kUploadBulkService->iFileCount>1){ echo t($t_base.'title/files'); }else{ echo t($t_base.'title/file'); } ?> <?=t($t_base.'title/submitted_on')?> <?=date('j. F Y',strtotime($kUploadBulkService->dtSubmitted))?>.</p>
            <br/>
            <p> <?=t($t_base.'title/we_expect_the_processing_time_of_this_data_to_be')?> <?=$kUploadBulkService->iProcessingTime?> <?=t($t_base.'title/days_for_these')?>.</p>
            <br/>
            <p align="center"><a href="javascript:void(0)" class="button1" id="confirm_cost_processing" onclick="confirm_processing_cost('<?=$id?>')"><span><?=t($t_base.'fields/confirm')?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_processing_cost()" id="cancel_cost_processing"><span><?=t($t_base.'fields/cancel')?></span></a></p>
        </div>
    </div>	
<?php	
}
else if($_REQUEST['flag']=='approved_processing_cost')
{
    $id=$_REQUEST['id']; 
    $kUploadBulkService->approvedProcessingCost($id); 
    $bottomTableData=$kUploadBulkService->getAllDataNotApprovedByForwarder('bottomdata');
    waiting_for_approved_by_management($bottomTableData,true);	
}
else if($_REQUEST['flag']=='view_files')
{
$id=$_REQUEST['id'];
$kUploadBulkService->load($id);

$filenameArr=$kUploadBulkService->bulkUploadServiceFiles($id);
?>

	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:400px;">
			<p class="close-icon" align="right">
			<a onclick="cancel_processing_cost();" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<p><b><?=t($t_base.'title/view_files_submitted')?> <?=date('j. F Y',strtotime($kUploadBulkService->dtSubmitted))?></b></p>
			<br/>
			<table width="100%" border="0">
			<?php
				if(!empty($filenameArr))
				{
					foreach($filenameArr as $filenameArrs)
					{
					
						$filenameexist=__UPLOAD_FORWARDER_BULK_SERVICES__."/".$filenameArrs['szFileName'];
						if(file_exists($filenameexist))
						{
							$newFileName='';
							$file_nameArr=explode(".",$filenameArrs['szOriginalName']);
							if(strlen($file_nameArr[0])>40)
							{
								$newFileName=substr($file_nameArr[0],0,30)."... .".$file_nameArr[1];
							}
							else
							{
								$newFileName=$filenameArrs['szOriginalName'];
							}
				?>
							<tr>
								<td align="left"><a href="javascript:void(0)" onclick="download_uploaded_file('<?=$filenameArrs['szFileName']?>')"><img src="<?=__BASE_URL_FORWARDER_LOGO__?>/Download.png" width="17px"></a></td>
								<td align="left"><?=$newFileName?> (<?=round($filenameArrs['iSize'])?> kB)</td>
							</tr>
			<?php }}}
			else{ ?>
				<tr>
					<td colspan="2"><?php echo t($t_base.'messages/no_record_found'); ?></td>
				</tr> 
			<?php }?>
			</table>
			<br/>
			<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancel_processing_cost()" id="cancel_cost_processing"><span><?=t($t_base.'fields/close')?></span></a></p>
			
			</div>
		</div>	
<?php	
}
else if($_REQUEST['flag']=='view_files_management')
{

$id=$_REQUEST['id'];
$kUploadBulkService->load($id);

$filenameArr=$kUploadBulkService->bulkUploadServiceFilesByManagement($id);
?>

	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:400px;">
			<p class="close-icon" align="right">
			<a onclick="cancel_processing_cost();" href="javascript:void(0);)">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<p><b><?=t($t_base.'title/view_files_submitted')?> <?=date('d. F Y',strtotime($kUploadBulkService->dtSubmitted))?></b></p>
			<br/><br/>
			<table width="100%" border="0">
			<?php
				if(!empty($filenameArr))
				{
					foreach($filenameArr as $filenameArrs)
					{
					
						$filenameexist=__UPLOAD_FORWARDER_BULK_SERVICES__."/".$filenameArrs['szFileName'];
						//echo $filenameexist;
						if(file_exists($filenameexist))
						{
			?>
						<tr>
							<td align="left"><?=$filenameArrs['szActualFileName']?></td>
							<td align="left"><a href="javascript:void(0)" onclick="download_uploaded_file('<?=$filenameArrs['szFileName']?>')"><img src="<?=__BASE_URL_FORWARDER_LOGO__?>/Download.png" width="25px"></a></td>
						</tr>
			<?php }}}
			else{ ?>
				<tr>
					<td colspan="2"><?php echo t($t_base.'messages/no_record_found'); ?></td>
				</tr> 
			<?php }?>
			</table>
			<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancel_processing_cost()" id="cancel_cost_processing"><span><?=t($t_base.'fields/close')?></span></a></p>
			
			</div>
		</div>	
<?php
	
}
else if($_REQUEST['flag']=='download_file')
{
		$filename=$_REQUEST['filename'];
		$id=$_REQUEST['id'];
		
		if((int)$id>0)
		{
                    $kUploadBulkService->load($id);

                    if($kUploadBulkService->iFileType=='LCL Services')
                    {
                        $newfilename="Transporteca_LCL_Services_approval_sheet_".date('Ymd').".xlsx";
                    }
                    else if($kUploadBulkService->iFileType=='Haulage')
                    {
                        $newfilename="Transporteca_Haulage_approval_sheet_".date('Ymd').".xlsx";
                    }
                    else if($kUploadBulkService->iFileType=='Customs Clearance')
                    {
                        $newfilename="Transporteca_Customs_Clearance_approval_sheet_".date('Ymd').".xlsx";
                    }
                    else if($kUploadBulkService->iFileType=='CFS locations')
                    {
                        $newfilename="Transporteca_CFS_locations_approval_sheet_".date('Ymd').".xlsx";
                    }
                    else if($kUploadBulkService->iFileType=='Airport Warehouses')
                    {
                        $newfilename="Transporteca_airport_warehouse_locations_approval_sheet_".date('Ymd').".xlsx";
                    }
                    else if($kUploadBulkService->iFileType=='Airfreight Services')
                    {
                        $newfilename="Transporteca_airfreight_services_approval_sheet_".date('Ymd').".xlsx";
                    }
		}
		else
		{
			$newfilename=$filename;
		}
		$fileurl=__UPLOAD_FORWARDER_BULK_SERVICES__."/$filename";
		header("Content-Type: application/octet-stream");
		header('Content-Disposition: attachment; filename='.$newfilename);
		ob_clean();
		flush();
		readfile($fileurl);
		exit;
}
else if($_REQUEST['flag']=='open_comment_popup')
{
	$id=$_REQUEST['id'];
	$allCommentArr=$kUploadBulkService->bulkUploadServiceComments($id);
	$kForwarder = new  	cForwarder();
	showUploadSerivceComment($allCommentArr,$id);
}
else if($_REQUEST['flag']=='add_comment')
{
	$id=$_REQUEST['id'];	
	$szComment=$_REQUEST['szComment'];
	$kUploadBulkService->addComment($id,$szComment);
	
	$allCommentArr=$kUploadBulkService->bulkUploadServiceComments($id);
	$kForwarder = new  	cForwarder();
	showUploadSerivceComment($allCommentArr,$id);

}
else if($_REQUEST['flag']=='open_approved_popup')
{
$id=$_REQUEST['id'];
$kUploadBulkService->load($id);
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:400px;">

			<p class="close-icon" align="right">
			<a onclick="cancel_processing_cost();" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
				<p><b><?=t($t_base.'title/approve')?> <?=$kUploadBulkService->iActualRecords?> <?=$kUploadBulkService->szRecordType?></b></p>
				<br/>
				<p><?=t($t_base.'title/open_approved_popup_text_1')?> <?=$kUploadBulkService->iActualRecords?> <?=$kUploadBulkService->szRecordType?> <?=t($t_base.'title/open_approved_popup_text_2')?></p>
				<br/>
				<p align="center"><a href="javascript:void(0)" class="button1" id="confirm_approved" onclick="confirm_approved('<?=$id?>')"><span>Confirm</span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_processing_cost()" id="cancel_cost_processing"><span>Cancel</span></a></p>
			</div>
		</div>
<?php		
}
else if($_REQUEST['flag']=='confirm_approved_data')
{
	$err_msg_file='';
	$fileUploadFlag=false;
	$id=$_REQUEST['id'];
	$kUploadBulkService->load($id);
	$filenameArr=$kUploadBulkService->bulkUploadServiceFilesByManagement($id);
	
	if($kUploadBulkService->iStatus==__DATA_REVIEWED_BY_FORWARDER__)
	{
		if($kUploadBulkService->iFileType=='LCL Services' || $kUploadBulkService->iFileType=='Airfreight Services')
		{
                    $kUploadBulkService->deleteLCLServiceData($id,$kUploadBulkService->idForwarder);
		}
		else if($kUploadBulkService->iFileType=='Haulage')
		{
                    $kUploadBulkService->deleteHaulageData($id,$kUploadBulkService->idForwarder);
		}
		else if($kUploadBulkService->iFileType=='Customs Clearance')
		{
                    $kUploadBulkService->deleteCustomClearanceData($id,$kUploadBulkService->idForwarder);
		}
		else if($kUploadBulkService->iFileType=='CFS locations' || $kUploadBulkService->iFileType=='Airport Warehouses')
		{
                    if($kUploadBulkService->iFileType=='Airport Warehouses')
                    {
                        $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
                    } 
                    else
                    {
                        $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
                    }
                    $kUploadBulkService->deleteCFSLocationData($id,$kUploadBulkService->idForwarder,false,$iWarehouseType);
		}  
	}
	if($kUploadBulkService->iFileType=='LCL Services' || $kUploadBulkService->iFileType=='Airfreight Services')
	{
		$fileName=$filenameArr[0]['szFileName'];
		$file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
		if(file_exists($file_name) && $fileName!='')
		{
			//echo $file_name;
			$sheetindex=0;
			$objReader = new PHPExcel_Reader_Excel2007();
			//print_r($objReader);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($file_name);
			
			
			$objPHPExcel->setActiveSheetIndex($sheetindex);
			$tabname =($objPHPExcel->getActiveSheet()->getTitle());
			
			$val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL
									
			$colcount=PHPExcel_Cell::columnIndexFromString($val1);
			//echo $colcount."<br/>";
			
			$rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());
			
                        if($kUploadBulkService->iFileType=='Airfreight Services')
                        {
                            $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
                        }
                        else
                        {
                            $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
                        }
			if($colcount>=19)
			{
				if($Excel_export_import->importLCLServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$kUploadBulkService->idForwarder,$iWarehouseType))
				{
					$fileUploadFlag=true;
					$_POST=array();
					$_FILES=array();
				}
				else
				{
					$err_msg_file=t($t_base.'messages/invalid_file_uploaded');
				}
			}
			else
			{
				$err_msg_file=t($t_base.'messages/invalid_file_uploaded');
			}
		}
		else
		{
			$err_msg_file=t($t_base.'messages/file_is_required');
		}
	}
	else if($kUploadBulkService->iFileType=='Haulage')
	{
		$fileName=$filenameArr[0]['szFileName'];
		$file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
		if(file_exists($file_name) && $fileName!='')
		{
			$sheetindex=0;
			$objReader = new PHPExcel_Reader_Excel2007();
			//print_r($objReader);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($file_name);
			
			
			$objPHPExcel->setActiveSheetIndex($sheetindex);
			$tabname =($objPHPExcel->getActiveSheet()->getTitle());
			
			$val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL
									
			$colcount=PHPExcel_Cell::columnIndexFromString($val1);
			//echo $colcount."<br/>";
			
			$rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());
			
			//echo $rowcount."<br/>";
			if($colcount>=13)
			{
				if($Excel_export_import->importHaulageServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$kUploadBulkService->idForwarder))
				{
					$fileUploadFlag=true;
					$_POST=array();
					$_FILES=array();
				}
				else
				{
					$err_msg_file=t($t_base.'messages/invalid_file_uploaded');
				}
			}
			else
			{
				$err_msg_file=t($t_base.'messages/invalid_file_uploaded');
			}
		}
		else
		{
			$err_msg_file=t($t_base.'messages/file_is_required');
		}
	}
	else if($kUploadBulkService->iFileType=='Customs Clearance')
	{
		$fileName=$filenameArr[0]['szFileName'];
		$file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
		if(file_exists($file_name) && $fileName!='')
		{
			$sheetindex=0;
			$objReader = new PHPExcel_Reader_Excel2007();
			//print_r($objReader);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($file_name);
			
			
			$objPHPExcel->setActiveSheetIndex($sheetindex);
			$tabname =($objPHPExcel->getActiveSheet()->getTitle());
			
			$val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL
								
			$colcount=PHPExcel_Cell::columnIndexFromString($val1);
			//echo $colcount."<br/>";
			
			$rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());
			
			//echo $rowcount."<br/>";
			if($colcount>=10)
			{
				if($Excel_export_import->importCustomClearanceData($objPHPExcel,$sheetindex,$colcount,$rowcount,$kUploadBulkService->idForwarder))
				{
					$fileUploadFlag=true;
					$_POST='';
					$_FILES=array();
				}
				else
				{
					$err_msg_file=t($t_base.'messages/invalid_file_uploaded');
				}
			}
			else
			{
				$err_msg_file=t($t_base.'messages/invalid_file_uploaded');
			}
		}
		else
		{
			$err_msg_file=t($t_base.'messages/file_is_required');
		}
	}
	else if($kUploadBulkService->iFileType=='CFS locations' || $kUploadBulkService->iFileType=='Airport Warehouses')
	{
            $fileName=$filenameArr[0]['szFileName'];
            $file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
            if(file_exists($file_name) && $fileName!='')
            {
                $sheetindex=0;
                $objReader = new PHPExcel_Reader_Excel2007();
                //print_r($objReader);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($file_name);


                $objPHPExcel->setActiveSheetIndex($sheetindex);
                $tabname =($objPHPExcel->getActiveSheet()->getTitle());

                $val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL

                $colcount=PHPExcel_Cell::columnIndexFromString($val1); 

                $rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow()); 
                
                if($kUploadBulkService->iFileType=='Airport Warehouses')
                {
                    $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
                }
                else
                {
                    $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
                }
                if($kUploadBulkService->bulkUploadCFS($objPHPExcel,$sheetindex,$colcount,$rowcount,$kUploadBulkService->idForwarder,false,$iWarehouseType))
                {
                    $fileUploadFlag=true;
                    $_POST='';
                    $_FILES=array();
                }
                else
                {
                    $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                }
            }
            else
            {
                $err_msg_file=t($t_base.'messages/file_is_required');
            }
	}
	
	$forwarderAwaitingCFSDataArr=$kUploadBulkService->getForwarderUploadCFSLocation($idForwarder);
	$completedApprovedByManagementArr=$kUploadBulkService->getAllDataNotApprovedByForwarder('topdata');
	process_cost_approved_by_management($completedApprovedByManagementArr,$forwarderAwaitingCFSDataArr,true);
	
		if(!empty($err_msg_file)){
		?>
		<script type="text/javascript">
		$("#loader").attr('style','display:none;');
		addPopupScrollClass('error_msg');
		//alert('hi');
		</script>
		<div id="error_msg">
		<div id="popup-bg"></div>
		<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification">
		<p class="close-icon" align="right">
		<a onclick="cancel_processing_cost();" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<div id="regError" class="errorBox ">
		<div class="header"><?=t($t_base_error.'/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<li><?=$err_msg_file?></li>
		</ul>
		</div>
		</div>
		<p align="center"><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/BulkServiceApproval/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
		</div>
		</div>
		</div>
		<?php }
		if($fileUploadFlag)
		{
                    if($kUploadBulkService->iFileType=='LCL Services' || $kUploadBulkService->iFileType=='Airfreight Services')
                    { 
                        if($kUploadBulkService->iFileType=='Airfreight Services')
                        {
                            $szServiceType = "airfreight services";
                        }
                        else
                        {
                            $szServiceType = t($t_base.'messages/success_file_upload_msg_4');
                        } 
		?>
		<script type="text/javascript">
		$("#loader").attr('style','display:none;');
		addPopupScrollClass('success_msg');
		//alert('hi');
		</script>
		<div id="success_msg">
		<div id="popup-bg"></div>
		<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification">
		<p class="close-icon" align="right">
		<a onclick="cancel_processing_cost();" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
		<p><?=t($t_base.'messages/success_file_upload_msg_1');?> <?=$Excel_export_import->successLine?> <?=$szServiceType;?>.</p><br/>
		<?php if($Excel_export_import->deleteLine>0){?>
		<p><?=$Excel_export_import->deleteLine?> <?=t($t_base.'messages/lines_delete_uploaded');?>.</p><br/>
		<?php }?>
		<?php if($Excel_export_import->errorLine>0){?>
		<p><?=t($t_base.'messages/we_found');?> <?=$Excel_export_import->errorLine?> <?=t($t_base.'messages/lines_error_uploaded');?>.</p><br/>
		<?php }else{?>
		<p><?=t($t_base.'messages/success_file_upload_msg_2');?>.</p><br/>
		<?php }?>
		<p><?=t($t_base.'messages/success_file_upload_msg_3');?>.</p><br/>
		<p align="center"><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/BulkServiceApproval/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
		</div>
		</div>
		</div>
		<?php	
			}
			else if($kUploadBulkService->iFileType=='Haulage')
			{?>
				<script type="text/javascript">
				$("#loader").attr('style','display:none;');
				addPopupScrollClass('success_msg');
				//alert('hi');
				</script>
				<div id="success_msg">
				<div id="popup-bg"></div>
				<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
				<p class="close-icon" align="right">
				<a onclick="cancel_processing_cost();" href="javascript:void(0);">
				<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
				</a>
				</p>
				<h5><?=t($t_base.'title/upload_of_haulage');?> </h5>
				<p><?=t($t_base.'messages/success_file_upload_msg_1');?> <?=$Excel_export_import->successLine?> <?=t($t_base.'messages/success_file_upload_msg_haulage_4');?>.</p><br/>
				<?php if($Excel_export_import->deleteLine>0){?>
				<p><?=$Excel_export_import->deleteLine?> <?=t($t_base.'messages/lines_delete_uploaded');?>.</p><br/>
				<?php }?>
				<?php if($Excel_export_import->errorLine>0){?>
				<p><?=t($t_base.'messages/we_found');?> <?=$Excel_export_import->errorLine?> 
				<?php if($Excel_export_import->errorLine>1){?>
				<?=t($t_base.'messages/lines_error_uploaded');?>
				<?php }else{?>
				<?=t($t_base.'messages/line_error_uploaded');?><? }?>
				.</p><br/>
				<?php }else{?>
				<p><?=t($t_base.'messages/success_file_upload_msg_2');?>.</p><br/>
				<?php }?>
				<p><?=t($t_base.'messages/success_file_upload_msg_3');?>.</p><br/>
				<p align="center"><a href="<?__FORWARDER_HOME_PAGE_URL__?>/BulkServiceApproval/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
				</div>
				</div>
				</div>
				<?php					
			}
			else if($kUploadBulkService->iFileType=='Customs Clearance')
			{?>
				<script type="text/javascript">
				$("#loader").attr('style','display:none;');
				addPopupScrollClass('success_msg');
				//alert('hi');
				</script>
				<div id="success_msg">
				<div id="popup-bg"></div>
				<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
				<p class="close-icon" align="right">
				<a onclick="cancel_processing_cost();" href="javascript:void(0);">
				<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
				</a>
				</p>
				<h5><?=t($t_base.'title/upload_of_custom_clearance');?></h5>
				<p><?=t($t_base.'messages/success_file_upload_msg_1');?> <?=$Excel_export_import->successLine?> <?=t($t_base.'messages/success_file_upload_msg_custom_clearance_4');?>.</p><br/>
				<?php if($Excel_export_import->deleteLine>0){?>
				<p><?=$Excel_export_import->deleteLine?> <?=t($t_base.'messages/lines_delete_uploaded');?>.</p><br/>
				<?php }?>
				<?php if($Excel_export_import->errorLine>0){?>
				<p><?=t($t_base.'messages/we_found');?> <?=$Excel_export_import->errorLine?> <?=t($t_base.'messages/lines_error_uploaded');?>.</p><br/>
				<?php }else{?>
				<p><?=t($t_base.'messages/success_file_upload_msg_2');?>.</p><br/>
				<?php }?>
				<p><?=t($t_base.'messages/success_file_upload_msg_3');?>.</p><br/>
				<p align="center"><a href="<?__FORWARDER_HOME_PAGE_URL__?>/BulkServiceApproval/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
				</div>
				</div>
				</div>
			<?php				
			}
			else if($kUploadBulkService->iFileType=='CFS locations' || $kUploadBulkService->iFileType=='Airport Warehouses')
			{
                            if($kUploadBulkService->iFileType=='Airport Warehouses')
                            {
                               $szHeading = t($t_base.'title/upload_of_airport_warehouse');
                               $szServiceType = "airport warehouse location.";
                            }
                            else
                            {
                               $szHeading = t($t_base.'title/upload_of_cfs');
                               $szServiceType = "CFS location.";
                            }
?>
                            <script type="text/javascript">
				$("#loader").attr('style','display:none;');
				addPopupScrollClass('success_msg'); 
                            </script>
                            <div id="success_msg">
				<div id="popup-bg"></div>
				<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
				<p class="close-icon" align="right">
                                    <a onclick="cancel_processing_cost();" href="javascript:void(0);">
                                        <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                                    </a>
				</p>
				<h5><?php echo $szHeading; ?></h5>
				<p><?=t($t_base.'messages/success_file_upload_msg_1');?> <?=$kUploadBulkService->successLine?> <?php echo $szServiceType; ?></p><br/>
				<?php if($kUploadBulkService->deleteLine>0){?>
				<p><?=$kUploadBulkService->deleteLine?> <?=t($t_base.'messages/lines_delete_uploaded');?>.</p><br/>
				<?php }?>
				<?php if($kUploadBulkService->errorLine>0){?>
				<p><?=t($t_base.'messages/we_found');?> <?=$kUploadBulkService->errorLine?> <?=t($t_base.'messages/lines_error_uploaded');?>.</p><br/>
				<?php }else{?>
				<p><?=t($t_base.'messages/success_file_upload_msg_2');?>.</p><br/>
				<?php }?>
				<p><?=t($t_base.'messages/success_file_upload_msg_3');?>.</p><br/>
				<p align="center"><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/BulkServiceApproval/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
				</div>
				</div>
				</div>
			<?php
			}
			$kUploadBulkService->updateApprovedStatus($id);
		}
}
else if($_REQUEST['flag']=='review_and_confrim_data')
{
	$err_msg_file='';
	$fileUploadFlag=false;
	$id=$_REQUEST['id'];
	$kUploadBulkService->load($id);
	$filenameArr=$kUploadBulkService->bulkUploadServiceFilesByManagement($id);
	if($kUploadBulkService->iFileType=='Customs Clearance')
	{
            if($kUploadBulkService->iStatus==__DATA_REVIEWED_BY_FORWARDER__)
            {
                $totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$id);
                ?>
                <p align="left"><b><?=t($t_base.'title/approve_upload_custom_clearance_line')?> <?php if(count($totalPricingServiceData)>0){?><span id="inc_count">1</span> of <?=count($totalPricingServiceData)?><? }?></b></p>
                <br/>
                <p style="TEXT-ALIGN:left"><?=t($t_base.'title/custom_clearance_line_2')?></p><br/>
                <input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalPricingServiceData)?>"> 
                <input type="hidden" name="idService" id="idService" value="<?=$id?>">
                <input type="hidden" name="dataCounter" id="dataCounter" value="1">  
                <div id="show_pricing_approval_obo_delete"></div>
                <div id="show_pricing_approval_obo">
                <?php
                    pricingCCApprovalList($idForwarder,$totalPricingServiceData,$id);
                ?>
                </div>
                <?php
            }
            else
            {
                $fileName=$filenameArr[0]['szFileName'];
                $file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
                if(file_exists($file_name) && $fileName!='')
                {
                    $sheetindex=0;
                    $objReader = new PHPExcel_Reader_Excel2007();
                    //print_r($objReader);
                    $objReader->setReadDataOnly(true);
                    $objPHPExcel = $objReader->load($file_name);


                    $objPHPExcel->setActiveSheetIndex($sheetindex);
                    $tabname =($objPHPExcel->getActiveSheet()->getTitle());

                    $val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL

                    $colcount=PHPExcel_Cell::columnIndexFromString($val1);
                    //echo $colcount."<br/>";

                    $rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());

                    if($kUploadBulkService->addDataForApprovalPricingCCTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$kUploadBulkService->idForwarder,$id))
                    {
                        $totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$id);
                        ?>
                        <p align="left"><b><?=t($t_base.'title/approve_upload_custom_clearance_line')?> <? if(count($totalPricingServiceData)>0){?><span id="inc_count">1</span> of <?=count($totalPricingServiceData)?><? }?></b></p>
                        <br/>
                        <p style="TEXT-ALIGN:left"><?=t($t_base.'title/custom_clearance_line_2')?></p><br/>
                        <input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalPricingServiceData)?>"> 
                        <input type="hidden" name="idService" id="idService" value="<?=$id?>">
                        <input type="hidden" name="dataCounter" id="dataCounter" value="1">  
                        <div id="show_pricing_approval_obo_delete"></div>
                        <div id="show_pricing_approval_obo">
                        <?php
                            pricingCCApprovalList($idForwarder,$totalPricingServiceData,$id);
                        ?>
                        </div>
                        <?php
                    }
                    else
                    {
                        $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                    }
                }
                else
                {
                    $err_msg_file=t($t_base.'messages/file_is_required');
                }
            }
	}
	else if($kUploadBulkService->iFileType=='LCL Services' || $kUploadBulkService->iFileType=='Airfreight Services')
	{ 
            if($kUploadBulkService->iFileType=='Airfreight Services')
            {
                $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
            } 
            else
            {
                $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
            }
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                $szTitle = "Approve upload - Airfreight Service ";
                $szDescription = t($t_base.'title/air_wtw_line_2');
            }
            else
            {
                $szTitle = t($t_base.'title/approve_upload_wtw_line');
                $szDescription = t($t_base.'title/wtw_line_2'); 
            }
            if($kUploadBulkService->iStatus==__DATA_REVIEWED_BY_FORWARDER__)
            { 
                $totalPricingServiceWTWData=$kUploadBulkService->totalWaitingToBeApprovedPricingWTW($idForwarder,$id); 
                ?>
                <p align="left"><b><?=$szTitle?> <?php if(count($totalPricingServiceWTWData)>0){?><span id="inc_count">1</span> of <?=count($totalPricingServiceWTWData)?><?php }?></b></p>
                <br/>
                <p style="TEXT-ALIGN:left"><?php echo $szDescription; ?></p><br/>
                <input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalPricingServiceWTWData)?>"> 
                <input type="hidden" name="idService" id="idService" value="<?=$id?>">
                <input type="hidden" name="dataCounter" id="dataCounter" value="1">  
                <div id="show_pricing_approval_obo_delete"></div>
                <div id="show_pricing_approval_obo">
                <?php
                    pricingWTWApprovalList($idForwarder,$totalPricingServiceWTWData,$id);
                ?>
                </div>
                <?php
            }
            else
            { 
                $fileName=$filenameArr[0]['szFileName'];
                $file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
                if(file_exists($file_name) && $fileName!='')
                {
                    $sheetindex=0;
                    $objReader = new PHPExcel_Reader_Excel2007();
                    //print_r($objReader);
                    $objReader->setReadDataOnly(true);
                    $objPHPExcel = $objReader->load($file_name);


                    $objPHPExcel->setActiveSheetIndex($sheetindex);
                    $tabname =($objPHPExcel->getActiveSheet()->getTitle());

                    $val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL

                    $colcount=PHPExcel_Cell::columnIndexFromString($val1);
                    //echo $colcount."<br/>";

                    $rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());

                    if($kUploadBulkService->addDataForApprovalPricingWTWTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$id,$iWarehouseType))
                    {
                        $totalPricingServiceWTWData=$kUploadBulkService->totalWaitingToBeApprovedPricingWTW($idForwarder,$id);
                        ?>
                        <p align="left"><b><?=$szTitle?> <?php if(count($totalPricingServiceWTWData)>0){?><span id="inc_count">1</span> of <?=count($totalPricingServiceWTWData)?><?php }?></b></p>
                        <br/>
                        <p style="TEXT-ALIGN:left"><?=$szDescription?></p><br/>
                        <input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalPricingServiceWTWData)?>"> 
                        <input type="hidden" name="idService" id="idService" value="<?=$id?>">
                        <input type="hidden" name="dataCounter" id="dataCounter" value="1">  
                        <div id="show_pricing_approval_obo_delete"></div>
                        <div id="show_pricing_approval_obo">
                        <?php
                            pricingWTWApprovalList($idForwarder,$totalPricingServiceWTWData,$id);
                        ?>
                        </div>
                        <?php
                    }
                    else
                    {
                        $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                    }
                }
                else
                {
                    $err_msg_file=t($t_base.'messages/file_is_required');
                }
            }	
	}
	else if($kUploadBulkService->iFileType=='Haulage')
	{
	
		if($kUploadBulkService->iStatus==__DATA_REVIEWED_BY_FORWARDER__)
		{
			$totalPricingServiceHaulageData=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulage($idForwarder,$id);
			$totalPricingServicePricingHaulageData=$kUploadBulkService->totalHaulagePrincingCount($idForwarder,$id);
			?>
			<p align="left"><b><?=t($t_base.'title/approve_upload_haulage_line')?> <? if($totalPricingServicePricingHaulageData>0){?><span id="inc_count">1</span> of <?=$totalPricingServicePricingHaulageData?><? }?></b></p>
			<br/>
			<p style="TEXT-ALIGN:left"><?=t($t_base.'title/haulage_line_2')?></p><br/>
			<input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalPricingServiceHaulageData)?>">
			<input type="hidden" name="idService" id="idService" value="<?=$id?>"> 
			<div id="show_pricing_approval_obo_delete"></div>
			<div id="show_pricing_approval_obo">
			<?php
				haulage_review_data($idForwarder,$id,$totalPricingServiceHaulageData);
			?>
			</div>
			<?php
		}
		else
		{
			$fileName=$filenameArr[0]['szFileName'];
			$file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
			if(file_exists($file_name) && $fileName!='')
			{
				$sheetindex=0;
				$objReader = new PHPExcel_Reader_Excel2007();
				//print_r($objReader);
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load($file_name);
				
				
				$objPHPExcel->setActiveSheetIndex($sheetindex);
				$tabname =($objPHPExcel->getActiveSheet()->getTitle());
				
				$val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL
									
				$colcount=PHPExcel_Cell::columnIndexFromString($val1);
				//echo $colcount."<br/>";
				
				$rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());
				
				if($kUploadBulkService->addDataForApprovalPricingHaulageTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$id))
				{
					$totalPricingServiceHaulageData=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulage($idForwarder,$id);
					$totalPricingServicePricingHaulageData=$kUploadBulkService->totalHaulagePrincingCount($idForwarder,$id);
					?>
					<p align="left"><b><?=t($t_base.'title/approve_upload_haulage_line')?> <? if($totalPricingServicePricingHaulageData>0){?><span id="inc_count">1</span> of <?=$totalPricingServicePricingHaulageData?><? }?></b></p>
					<br/><br/>
					<p style="TEXT-ALIGN:left"><?=t($t_base.'title/haulage_line_2')?></p><br/>
					<input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalPricingServiceHaulageData)?>"> 
					<input type="hidden" name="idService" id="idService" value="<?=$id?>"> 
					<div id="show_pricing_approval_obo_delete"></div>
					<div id="show_pricing_approval_obo">
					
					<?php
                                            haulage_review_data($idForwarder,$id,$totalPricingServiceHaulageData);
					?>
					</div>
					<?php
				}
				else
				{
					$err_msg_file=t($t_base.'messages/invalid_file_uploaded');
				}
			}
			else
			{
				$err_msg_file=t($t_base.'messages/file_is_required');
			}
		}	
	}
	else if($kUploadBulkService->iFileType=='CFS locations' || $kUploadBulkService->iFileType=='Airport Warehouses')
	{
            if($kUploadBulkService->iFileType=='Airport Warehouses')
            {
                $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
            } 
            else
            {
                $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
            }  
            if($kUploadBulkService->iStatus==__DATA_REVIEWED_BY_FORWARDER__)
            { 
                $totalCFSLocationData=$kUploadBulkService->totalWaitingToBeApprovedCFSLocation($idForwarder,$id,false,false,$iWarehouseType); 
                ?>
                <p align="left"><b><?php echo "Approve upload - ".$kUploadBulkService->iFileType; ?> <?php if(count($totalCFSLocationData)>0){?><span id="inc_count">1</span> of <?=count($totalCFSLocationData)?><?php }?></b></p>
                <br/>
                <input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalCFSLocationData)?>">
                <input type="hidden" name="idService" id="idService" value="<?=$id?>"> 
                <input type="hidden" name="idBatch" id="idBatch" value="0"> 
                <div id="show_pricing_approval_obo_delete"></div>
                <div id="show_pricing_approval_obo">
                <?php
                    cfs_location_review_data($idForwarder,$id,$totalCFSLocationData,false,$iWarehouseType);
                ?>
                </div>
                <?php
            }
            else
            {
                $fileName=$filenameArr[0]['szFileName'];
                $file_name= __UPLOAD_FORWARDER_BULK_SERVICES__."/".$fileName;
                if(file_exists($file_name) && $fileName!='')
                {
                    $sheetindex=0;
                    $objReader = new PHPExcel_Reader_Excel2007();
                    //print_r($objReader);
                    $objReader->setReadDataOnly(true);
                    $objPHPExcel = $objReader->load($file_name);


                    $objPHPExcel->setActiveSheetIndex($sheetindex);
                    $tabname =($objPHPExcel->getActiveSheet()->getTitle());

                    $val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL

                    $colcount=PHPExcel_Cell::columnIndexFromString($val1);
                    //echo $colcount."<br/>";

                    $rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());

                    if($kUploadBulkService->addDataForApprovalCFSLocationTemp($objPHPExcel,$sheetindex,$colcount,$rowcount,$idForwarder,$id,$iWarehouseType))
                    {
                        $totalCFSLocationData=$kUploadBulkService->totalWaitingToBeApprovedCFSLocation($idForwarder,$id,false,false,$iWarehouseType);
                        ?>
                        <p align="left"><b><?=t($t_base.'title/approve_upload_cfs_location')?> <?php if(count($totalCFSLocationData)>0){?><span id="inc_count">1</span> of <?=count($totalCFSLocationData)?><? }?></b></p>
                        <br/>
                        <input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalCFSLocationData)?>"> 
                        <input type="hidden" name="idService" id="idService" value="<?=$id?>"> 
                        <input type="hidden" name="idBatch" id="idBatch" value="0"> 
                        <div id="show_pricing_approval_obo_delete"></div>
                        <div id="show_pricing_approval_obo">
                            <?php
                                cfs_location_review_data($idForwarder,$id,$totalCFSLocationData,false,$iWarehouseType);
                            ?>
                        </div>
                        <?php
                    }
                    else
                    {
                        $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                    }
                }
                else
                {
                    $err_msg_file=t($t_base.'messages/file_is_required');
                }
            }
	}
	if(!empty($err_msg_file)){
	?>
	<script type="text/javascript">
	$("#loader").attr('style','display:none;');
	addPopupScrollClass('error_msg');
	//alert('hi');
	</script>
	<div id="error_msg">
	<div id="popup-bg"></div>
	<div id="popup-container">
	<div class="popup signin-popup signin-popup-verification">
	<p class="close-icon" align="right">
	<a href="<?__FORWARDER_HOME_PAGE_URL__?>/BulkServiceApproval/">
	<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
	</a>
	</p>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<li><?=$err_msg_file?></li>
	</ul>
	</div>
	</div>
	<p align="center"><a href="<?__FORWARDER_HOME_PAGE_URL__?>/BulkServiceApproval/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
	</div>
	</div>
	</div>
	<?php }
	else
	{
	?>
	<script language="JavaScript">
            
                  var elements = document.getElementsByTagName('a');
                  //alert(elements);
                  for(var i = 0, len = elements.length; i < len; i++) 
                  {
                        //alert(elements[28].href);
                        if(elements[i].href!='' && elements[i].href!='javascript:void(0)' && elements[i].href!='javascript:void(0);' )
                        {
                              $(elements[i]).attr('onClick','checkPageRedirectionBulkService(\''+elements[i].href+'\');');
                              $(elements[i]).attr('href','javascript:void(0);');
                  		}
                  }
            
		</script>
	
	<?php
	}
}
else if($_REQUEST['flag']=='delete_pricing_cc_approval_data')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	$idService=$_REQUEST['idService'];
	$kUploadBulkService->deletePricingCCUnApprovedData($id,$idForwarder,$idService);
	if($dataCounter>=$dataTotalCount)
	{
	$kUploadBulkService->updateApprovedStatus($idService);
	cfsApprovalCompletePopup();
	}else{
	$dataCounter=$dataCounter+1;
	?>
	<script>
	$("#loader").attr('style','display:none;');
	$("#inc_count").html('<?=$dataCounter?>');	
	$("#dataCounter").attr('value','<?=$dataCounter?>');
	</script>
	<?php 
	}
	$totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$idService);
	pricingCCApprovalList($idForwarder,$totalPricingServiceData,$idService);
}
else if($_REQUEST['flag']=='approve_pricing_cc_approval_data')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	$idService=$_REQUEST['idService'];
	//print_r($_REQUEST);
	if($kUploadBulkService->addOBOCustomClearanceApprove($_POST['pricingCCApprove']))
	{
		$kUploadBulkService->deletePricingCCUnApprovedData($id,$idForwarder,$idService);
		$_POST['pricingCCApprove']='';
		$_POST['pricingCCApprove']=array();
		$_POST['oboDataExportAry']='';
		$_POST['oboDataExportAry']=array();
		if($dataCounter>=$dataTotalCount)
		{
		$kUploadBulkService->updateApprovedStatus($idService);
		cfsApprovalCompletePopup();
		}else{
		$dataCounter=$dataCounter+1;
		?>
		<script>
		//alert('hi');
		$("#inc_count").html('<?=$dataCounter?>');	
		$("#dataCounter").attr('value','<?=$dataCounter?>');
		$("#loader").attr('style','display:none');
		</script>
		<?php 
		
		//$totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$idService);
		//print_r($totalPricingServiceData);
		//pricingCCApprovalList($idForwarder,$totalPricingServiceData,$idService);
		//$idData=$totalPricingServiceData[0];
		//echo $idData."hello";
		//$pricingCCServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$idData);
		//print_r($pricingCCServiceData);
		///approve_pricing_cc_bottom($pricingCCServiceData,$idForwarder);
		//exit();
		}
	}
	if(!empty($kUploadBulkService->arErrorMessages)){
	?>
	<script>
		$("#loader").attr('style','display:none');
	</script>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kUploadBulkService->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php
	}	
	$totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$idService);
	pricingCCApprovalList($idForwarder,$totalPricingServiceData,$idService);
}
else if($_REQUEST['flag']=='delete_pricing_cc_approval_data_open_popup')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	?>
	<script>
	$("#loader").attr('style','display:none;');
	</script>
	<div id="delete_confirm_popup">
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p class="close-icon" align="right">
			<a href="javascript:void(0);" onclick="close_delete_confirm_popup_approval();">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<h4><b><?=t($t_base.'messages/delete_pricing_cc')?></b></h4>
			<br/>
			<p><?=t($t_base.'messages/are_you_want_to_delete_pricing_cc_data')?></p><br/>
			<p align="center"><a href="javascript:void(0)" id="delete_data_confirm" class="button1" onclick="delete_pricing_cc_appoving_data('<?=$id?>','<?=$idForwarder?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_data_confirm_no" class="button2" onclick="close_delete_confirm_popup_approval()"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>
	</div>		
	<?php
}
else if($_REQUEST['flag']=='delete_pricing_wtw_approval_data_open_popup')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	?>
	<script>
	$("#loader").attr('style','display:none');
	</script>
	<div id="delete_confirm_popup">
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p class="close-icon" align="right">
			<a href="javascript:void(0);" onclick="close_delete_confirm_popup_approval();">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<h4><b><?=t($t_base.'messages/delete_pricing_wtw')?></b></h4>
			<br/>
			<p><?=t($t_base.'messages/are_you_want_to_delete_pricing_wtw_data')?></p><br/>
			<p align="center"><a href="javascript:void(0)" id="delete_data_confirm" class="button1" onclick="delete_pricing_wtw_appoving_data('<?=$id?>','<?=$idForwarder?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_data_confirm_no" class="button2" onclick="close_delete_confirm_popup_approval()"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>
	</div>		
	<?php
}
else if($_REQUEST['flag']=='delete_pricing_wtw_approval_data')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	$idService=$_REQUEST['idService'];
	$kUploadBulkService->deletePricingWTWUnApprovedData($id,$idForwarder,$idService);
	if($dataCounter>=$dataTotalCount)
	{
	$kUploadBulkService->updateApprovedStatus($idService);
	cfsApprovalCompletePopup();
	}else{
	$dataCounter=$dataCounter+1;
	?>
	<script>
	$("#loader").attr('style','display:none');
	$("#inc_count").html('<?=$dataCounter?>');	
	$("#dataCounter").attr('value','<?=$dataCounter?>');
	</script>
	<?php 
	}
	$totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApprovedPricingWTW($idForwarder,$idService);
	pricingWTWApprovalList($idForwarder,$totalPricingServiceData,$idService);
}
else if($_REQUEST['flag']=='approve_pricing_wtw_approval_data')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	$idService=$_REQUEST['idService'];
	//print_r($_REQUEST);
	if($kUploadBulkService->addOBOLCLApproveData($_POST['pricingWTW']))
	{
		$kUploadBulkService->deletePricingWTWUnApprovedData($id,$idForwarder,$idService);
		$_POST['pricingWTW']='';
		$_POST['oboDataExportAry']='';
		if($dataCounter>=$dataTotalCount)
		{
		$kUploadBulkService->updateApprovedStatus($idService);
		cfsApprovalCompletePopup();
		}else{
		$dataCounter=$dataCounter+1;
		?>
		<script>
		$("#loader").attr('style','display:none');
		$("#inc_count").html('<?=$dataCounter?>');	
		$("#dataCounter").attr('value','<?=$dataCounter?>');
		</script>
		<?php 
		
		//$totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApprovedPricingWTW($idForwarder,$idService);
		//print_r($totalPricingServiceData);
		//pricingWTWApprovalList($idForwarder,$totalPricingServiceData,$idService);
		//$idData=$totalPricingServiceData[0];
		//echo $idData."hello";
		//$pricingCCServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$idData);
		//print_r($pricingCCServiceData);
		///approve_pricing_cc_bottom($pricingCCServiceData,$idForwarder);
		//exit();
		}
	}
	if(!empty($kUploadBulkService->arErrorMessages)){
	?>
	<script>
		$("#loader").attr('style','display:none');
	</script>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kUploadBulkService->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php
	}
	$totalPricingServiceData=$kUploadBulkService->totalWaitingToBeApprovedPricingWTW($idForwarder,$idService);
	pricingWTWApprovalList($idForwarder,$totalPricingServiceData,$idService);	
}
else if($_REQUEST['flag']=='delete_haulage_temp_line')
{
	$t_base = "HaulaugePricing/";
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$iUpToKg=number_format((float)$_REQUEST['iUpToKg']);
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$idService=$_REQUEST['idService'];
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p class="close-icon" align="right">
			<a href="javascript:void(0);" onclick="close_delete_confirm_popup_temp();">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<p><b><?=t($t_base.'messages/delete_haulage_line');?></b></p>
			<br/>
			<p><?=t($t_base.'messages/are_you_sure');?> <?=$iUpToKg?> <?=t($t_base.'fields/kg');?>?</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_temp_line_confirm" class="button1" onclick="delete_haulage_pricing_line_confirm_temp('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$idHaulagePricingModel?>','<?=$idService?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup_temp()"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=="delete_haulage_temp_line_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$idService=$_REQUEST['idService'];
	
	$kUploadBulkService->deleteHaulageZonePricingDataTemp($id);
	addHaulagePricingDataTemp($idHaulagePricingModel,$idWarehouse,$iDirection,$idHaulageModel,$idService);
}
else if($_REQUEST['flag']=="edit_haulage_temp_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$wmfactor=$_REQUEST['wmfactor'];
	$editDataArr=$kUploadBulkService->haulageLoadPricing($id);
	$idService=$_REQUEST['idService'];
	//print_r($editDataArr);
	//$arrZone[]=$kHaulagePricing->loadZone($idHaulagePricingModel);
	///$fWmFactor=$arrZone[0]['fWmFactor'];
	
	add_pricing_haulage_form($editDataArr,$idWarehouse,$idDirection,$idHaulageModel,$idHaulagePricingModel,$idService);
}
else if($_REQUEST['flag']=='save_pricing_line_temp')
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$idService=$_REQUEST['zonePricingArr']['idServiceUpload'];
	
	//print_r($_REQUEST);
	add_pricing_haulage_form(array(),$idWarehouse,$idDirection,$idHaulageModel,$idHaulagePricingModel,$idService);
}
else if($_REQUEST['flag']=='pricing_temp_detail_list')
{
	$idHaulagePricingModel=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idService=$_REQUEST['idService'];


	addHaulagePricingDataTemp($idHaulagePricingModel,$idWarehouse,$iDirection,$idHaulageModel,$idService);
}
else if($_REQUEST['flag']=='delete_haulage_approval_data_open_popup')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$idServer=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	?>
	<script>
	$("#loader").attr('style','display:none');
	</script>
	<div id="delete_confirm_popup">
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p class="close-icon" align="right">
			<a href="javascript:void(0);" onclick="close_delete_confirm_popup_approval();">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<h4><b><?=t($t_base.'messages/delete_haulage_pricing')?></b></h4>
			<br/>
			<p><?=t($t_base.'messages/are_you_want_to_delete_haulage_data')?></p><br/>
			<p align="center"><a href="javascript:void(0)" id="delete_data_confirm" class="button1" onclick="delete_haulage_appoving_data('<?=$idServer?>','<?=$idForwarder?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_data_confirm_no" class="button2" onclick="close_delete_confirm_popup_approval()"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>
	</div>		
	<?php
}
else if($_REQUEST['flag']=='delete_haulage_approval_data')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	//$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$iDirection=$_REQUEST['iDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idService=$_REQUEST['idService'];
	$kUploadBulkService->deleteHaulageUnApprovedData($idForwarder,$idService,$idWarehouse,$iDirection,$idHaulageModel);
	
	
	if($dataCounter>=$dataTotalCount)
	{
		$kUploadBulkService->updateApprovedStatus($idService);
	cfsApprovalCompletePopup();
	}else{
	$dataCounter=$dataCounter+1;
	?>
	<script>
	$("#loader").attr('style','display:none');
	$("#inc_count").html('<?=$dataCounter?>');	
	$("#dataCounter").attr('value','<?=$dataCounter?>');
	</script>
	<?php 
	}
	
	$totalPricingServiceHaulageData=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulage($idForwarder,$idService);
	haulage_review_data($idForwarder,$idService,$totalPricingServiceHaulageData);
}
else if($_REQUEST['flag']=='approve_haulage_approval_data')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	$idService=$_REQUEST['idService'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$iDirection=$_REQUEST['iDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	//print_r($_REQUEST);
	if($kUploadBulkService->addOBOHaulageApproveData($idForwarder,$idService,$idWarehouse,$iDirection,$idHaulageModel))
	{
		$kUploadBulkService->deleteHaulageUnApprovedData($idForwarder,$idService,$idWarehouse,$iDirection,$idHaulageModel);
		//$_POST['pricingWTW']='';
		if($dataCounter>=$dataTotalCount)
		{
		
		$kUploadBulkService->updateApprovedStatus($idService);
		cfsApprovalCompletePopup();
		}else{
		$dataCounter=$dataCounter+1;
		?>
		<script>
		$("#loader").attr('style','display:none');
		$("#inc_count").html('<?=$dataCounter?>');	
		$("#dataCounter").attr('value','<?=$dataCounter?>');
		</script>
		<?php 		
		//$totalPricingServiceHaulageData=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulage($idForwarder,$idService);
		//haulage_review_data($idForwarder,$idService,$totalPricingServiceHaulageData);
		//exit();
		}
	}
	if(!empty($kUploadBulkService->arErrorMessages)){
	?>
	<script>
		$("#loader").attr('style','display:none');
	</script>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kUploadBulkService->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php
	}	
	$totalPricingServiceHaulageData=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulage($idForwarder,$idService);
	haulage_review_data($idForwarder,$idService,$totalPricingServiceHaulageData);
}
else if($_REQUEST['flag']=='LEAVE_PAGE')
{
	$path = trim(sanitize_all_html_input($_POST['path']));
	?>
	<div id="delete_confirm_popup">
	<div id="popup-bg"></div>
		<div id="popup-container" style="padding-top:100px;">
			<div class="popup" style="width:400px;">
			<p class="close-icon" align="right">
			<a href="javascript:void(0);" onclick="close_delete_confirm_popup_approval()">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<h4><b><?=t($t_base.'messages/heading_leave_approve_popup')?></b></h4><br/>
			<p><?=t($t_base.'messages/leave_approve_popup_1')?></p><br/>
			<p><?=t($t_base.'messages/leave_approve_popup_2')?></p><br/>
			<p><?=t($t_base.'messages/leave_approve_popup_3')?></p><br/>
			<p align="center"><a href="javascript:void(0)" id="delete_data_confirm" class="button1" onclick="redirect_page_bulk_service('<?=$path?>');"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_data_confirm_no" class="button1" onclick="close_delete_confirm_popup_approval()"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>
	</div>	 
	<?php
}
else if($_REQUEST['flag']=='approve_cfs_location_approval_data')
{
    $dataCounter=$_REQUEST['dataCounter'];
    $dataTotalCount=$_REQUEST['dataTotalCount'];
    $id=$_REQUEST['id'];
    $idForwarder=$_REQUEST['idForwarder'];
    $idService=$_REQUEST['idService'];
    $idBatch=$_REQUEST['idBatch'];
    $iWarehouseType = $_REQUEST['cfsAddEditArr']['iWarehouseType']; 
    if($kWHSSearch->addWarehouseApprovedData($_POST['cfsAddEditArr']))
    {
        $_POST['cfsAddEditArr']='';
        $_POST['cfsAddEditArr']=array();
        $kUploadBulkService->deleteCFSLocationUnApprovedData($id,$idForwarder,$idService,$idBatch);
        //$_POST['pricingWTW']='';
        if($dataCounter>=$dataTotalCount)
        {
            if($idService>0)
            {
                $kUploadBulkService->updateApprovedStatus($idService);
            }
            cfsApprovalCompletePopup();
        }
        else
        { 
            $dataCounter=$dataCounter+1;
            ?>
            <script>
            $("#loader").attr('style','display:none');
            $("#inc_count").html('<?=$dataCounter?>');	
            $("#dataCounter").attr('value','<?=$dataCounter?>');
            </script>
            <?php 		
        }
    }
    if(!empty($kWHSSearch->arErrorMessages)){
    ?>
    <script>
            $("#loader").attr('style','display:none');
    </script>
    <div id="regError" class="errorBox ">
    <div class="header"><?=t($t_base_error.'/please_following');?></div>
    <div id="regErrorList">
    <ul>
    <?php  foreach($kWHSSearch->arErrorMessages as $key=>$values) {  ?>
        <li><?=$values?></li>
    <?php } ?>
    </ul>
    </div>
    </div>
    <?php 
    }
    $totalCFSLocationData=$kUploadBulkService->totalWaitingToBeApprovedCFSLocation($idForwarder,$idService,0,$idBatch,$iWarehouseType);
    cfs_location_review_data($idForwarder,$idService,$totalCFSLocationData,$idBatch,$iWarehouseType);
		
}
else if($_REQUEST['flag']=='delete_cfs_location_approval_data_open_popup')
{
	$dataCounter=$_REQUEST['dataCounter'];
	$dataTotalCount=$_REQUEST['dataTotalCount'];
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder'];
	?>
	<script>
	$("#loader").attr('style','display:none');
	</script>
	<div id="delete_confirm_popup">
	<div id="popup-bg"></div>
		<div id="popup-container">
		<p class="close-icon" align="right">
		<a href="javascript:void(0);" onclick="close_delete_confirm_popup_approval()">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>
			<div class="popup signin-popup signin-popup-verification">
			<h4><b><?=t($t_base.'messages/delete_cfs_location')?></b></h4>
			<br/>
			<p><?=t($t_base.'messages/are_you_want_to_delete_cfs_location_data')?></p><br/>
			<p align="center"><a href="javascript:void(0)" id="delete_data_confirm" class="button1" onclick="delete_cfs_location_appoving_data('<?=$id?>','<?=$idForwarder?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_data_confirm_no" class="button2" onclick="close_delete_confirm_popup_approval()"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>
	</div>		
	<?php
}
else if($_REQUEST['flag']=='delete_cfs_location_approval_data')
{
    $dataCounter=$_REQUEST['dataCounter'];
    $dataTotalCount=$_REQUEST['dataTotalCount'];
    $id=$_REQUEST['id'];
    $idForwarder=$_REQUEST['idForwarder'];
    $idService=$_REQUEST['idService'];
    $idBatch=$_REQUEST['idBatch'];
    
    if($idService>0)
    {
        $kUploadBulkService = new cUploadBulkService(); 
        $kUploadBulkService->load($idService);
        if($kUploadBulkService->iFileType=='Airport Warehouses')
        {
            $iWarehouseType = __WAREHOUSE_TYPE_AIR__;
        }
        else
        {
            $iWarehouseType = __WAREHOUSE_TYPE_CFS__;
        }
    } 
    $kUploadBulkService->deleteCFSLocationUnApprovedData($id,$idForwarder,$idService,$idBatch);

    if($dataCounter>=$dataTotalCount)
    {
        if($idService>0)
        {
            $kUploadBulkService->updateApprovedStatus($idService);
        }
            cfsApprovalCompletePopup();
        }else{
            $dataCounter=$dataCounter+1;
            ?>
            <script>
            $("#loader").attr('style','display:none');
            $("#inc_count").html('<?=$dataCounter?>');	
            $("#dataCounter").attr('value','<?=$dataCounter?>');
            </script>
            <?php 
            $totalCFSLocationData=$kUploadBulkService->totalWaitingToBeApprovedCFSLocation($idForwarder,$idService,0,$idBatch,$iWarehouseType);
            cfs_location_review_data($idForwarder,$idService,$totalCFSLocationData,$idBatch,$iWarehouseType);
    }
}
else if($_REQUEST['flag']=='review_and_confrim_data_awaiting_approval')
{
	$id=$_REQUEST['id'];
	$idForwarder=$_REQUEST['idForwarder']; 
	$totalCFSLocationData = $kUploadBulkService->totalWaitingToBeApprovedCFSLocation($idForwarder,0,0,$id);
        
        $idWarehouseTemp = $totalCFSLocationData[0]; 
        if($idWarehouseTemp>0)
        {
            $tempServiceAry = array();
            $tempServiceAry = $kUploadBulkService->totalWaitingToBeApprovedCFSLocation($idForwarder,0,$idWarehouseTemp); 
            $iWarehouseType = $tempServiceAry[0]['iWarehouseType']; 
        }  
         
		?>
		<p align="left"><b><?=t($t_base.'title/approve_upload_cfs_location')?> <? if(count($totalCFSLocationData)>0){?><span id="inc_count">1</span> of <?=count($totalCFSLocationData)?><? }?></b></p>
		<br/><br/>
		<input type="hidden" name="dataTotalCount" id="dataTotalCount" value="<?=count($totalCFSLocationData)?>"> 
		<input type="hidden" name="idService" id="idService" value="0"> 
		<input type="hidden" name="idBatch" id="idBatch" value="<?=$id?>"> 
		<div id="show_pricing_approval_obo_delete"></div>
		<div id="show_pricing_approval_obo">
		<?php
			cfs_location_review_data($idForwarder,0,$totalCFSLocationData,$id,$iWarehouseType);
		?>
		</div>
		<script language="JavaScript">
            
                  var elements = document.getElementsByTagName('a');
                  //alert(elements);
                  for(var i = 0, len = elements.length; i < len; i++) 
                  {
                        //alert(elements[28].href);
                        if(elements[i].href!='' && elements[i].href!='javascript:void(0)' && elements[i].href!='javascript:void(0);' )
                        {
                              $(elements[i]).attr('onClick','checkPageRedirectionBulkService(\''+elements[i].href+'\');');
                              $(elements[i]).attr('href','javascript:void(0);');
                  		}
                  }
            
		</script>
		<?php
}
else if($_REQUEST['flag']=='delete_uploaded_file')
{
	$id=$_REQUEST['id'];
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p class="close-icon" align="right">
			<a href="javascript:void(0);" onclick="cancel_processing_cost();">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
				<h4><b><?=t($t_base.'title/delete_file')?></b></h4><br/>
				<p><?=t($t_base.'title/are_you_sure_delete_file')?></p><br />
				<p align="center"> <a href="javascript:void(0)" class="button1" onclick="confirm_delete_upload_file('<?=$id?>','bottom')" id="yes_button"><span><?=t($t_base.'fields/yes')?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_processing_cost();" id="no_button"><span><?=t($t_base.'fields/no')?></span></a> </p>
			</div>
		</div>	
<?php }
else if($_REQUEST['flag']=='delete_upload_file_by_forwarder')
{
    $id=$_REQUEST['id'];
    $datatablevalue=$_REQUEST['datatablevalue'];
    $kUploadBulkService->load($id);

    if($kUploadBulkService->iStatus==__DATA_REVIEWED_BY_FORWARDER__)
    {
        if($kUploadBulkService->iFileType=='LCL Services' || $kUploadBulkService->iFileType=='Airfreight Services')
        {
            $kUploadBulkService->deleteLCLServiceData($id,$kUploadBulkService->idForwarder);
        }
        else if($kUploadBulkService->iFileType=='Haulage')
        {
            $kUploadBulkService->deleteHaulageData($id,$kUploadBulkService->idForwarder);
        }
        else if($kUploadBulkService->iFileType=='Customs Clearance')
        {
            $kUploadBulkService->deleteCustomClearanceData($id,$kUploadBulkService->idForwarder);
        }
        else if($kUploadBulkService->iFileType=='CFS locations' || $kUploadBulkService->iFileType=='Airport Warehouses')
        {
            if($kUploadBulkService->iFileType=='Airport Warehouses')
            {
                $kUploadBulkService->deleteCFSLocationData($id,$kUploadBulkService->idForwarder,__WAREHOUSE_TYPE_AIR__);
            }
            else
            {
                $kUploadBulkService->deleteCFSLocationData($id,$kUploadBulkService->idForwarder,__WAREHOUSE_TYPE_CFS__);
            }  
        }
    }

    $kUploadBulkService->deleteUploadedFileByForwarder($id);
    $bottomTableData=$kUploadBulkService->getAllDataNotApprovedByForwarder($datatablevalue);
    if($datatablevalue=='bottomdata')
    {
        waiting_for_approved_by_management($bottomTableData);
    }
    else
    {
        $forwarderAwaitingCFSDataArr=$kUploadBulkService->getForwarderUploadCFSLocation($kUploadBulkService->idForwarder);
        process_cost_approved_by_management($bottomTableData,$forwarderAwaitingCFSDataArr);		
    }
	
}
else if($_REQUEST['flag']=='city_warehouse_cc_origin')
{
	$t_base = "OBODataExport/";
	$idOriginCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_origin=$Excel_export_import->getForwaderWarehouses($idForwarder,$idOriginCountry);
	
	$forwardWareHouseCityAry_origin = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idOriginCountry);
	if(!empty($forwardWareHouseCityAry_origin))
	{
		$countForwarderCity_origin = count($forwardWareHouseCityAry_origin);
		if($countForwarderCity_origin==1)
		{?>
			<script>
				$("#idOriginWarehouse").attr('disabled',true);
				$("#szOriginCity").attr('disabled',true);
				$("#idOriginWarehouse_hidden").attr('value','');
				$("#approve_button").unbind("click");
				$("#approve_button").attr('style',"opacity:0.4");
				select_city_approval('<?=$idForwarder?>','origin','<?=$forwardWareHouseCityAry_origin[0]['szCity']?>');
			</script>
		<?php				
		}
		
	}
	
?>
<div class="fl-33 s-field">
			<span class="f-size-12"><?=t($t_base.'title/city');?></span>
			<span id="origin_city_span">
				<select id="szOriginCity" name="oboDataExportAry[szOriginCity]" onchange="select_city_approval('<?=$idForwarder?>','origin',this.value);">
					<option value=""><?=t($t_base.'title/all');?></option>
					<?php
						if(!empty($forwardWareHouseCityAry_origin))
						{
							foreach($forwardWareHouseCityAry_origin as $forwardWareHouseCityArys)
							{
								?>
								<option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($countForwarderCity_origin==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['szCityFrom']==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
								<?php
							}
						}
					?>
				</select>
			</span>
		</div>
		<div class="fl-33" id="warehouse_origin">
			<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
			<span id="origin_warehouse_span">
				<select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse" onchange="sel_warehouse_for_approval('<?=$idForwarder?>','origin',this.value);">
					<option value=""><?=t($t_base.'fields/select');?></option>
					<?php
						if(!empty($forwardWareHouseArr_origin))
						{
							foreach($forwardWareHouseArr_origin as $forwardWareHouseArrs)
							{
								?>
								<option value="<?=$forwardWareHouseArrs['id']?>" <? if($pricingCCServiceData[0]['idWarehouseFrom']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
								<?php
							}
						}
					?>
				</select>
			</span>
</div>
<?php }
else if($_REQUEST['flag']=='city_warehouse_cc_des')
{
	$t_base = "OBODataExport/";
	$idDesCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_dest=$Excel_export_import->getForwaderWarehouses($idForwarder,$idDesCountry);
	
	$forwardWareHouseCityAry_dest = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idDesCountry);
	
	if(!empty($forwardWareHouseCityAry_dest))
	{
		$countForwarderCity_dest = count($forwardWareHouseCityAry_dest);
		if($countForwarderCity_dest==1)
		{?>
			<script>
				$("#idDestinationWarehouse").attr('disabled',true);
				$("#szDestinationCity").attr('disabled',true);
				$("#idDestinationWarehouse_hidden").attr('value','');
				$("#approve_button").unbind("click");
				$("#approve_button").attr('style',"opacity:0.4");
				select_city_approval('<?=$idForwarder?>','des','<?=$forwardWareHouseCityAry_dest[0]['szCity']?>');
			</script>
		<?php				
		}
		
	}
	
?>
<div class="fl-33 s-field">
		<span class="f-size-12"><?=t($t_base.'title/city');?></span>
		<span id="destination_city_span">
			<select id="szDestinationCity" name="oboDataExportAry[szDestinationCity]" onchange="select_city_approval('<?=$idForwarder?>','des',this.value);">
				<option value=""><?=t($t_base.'title/all');?></option>
				<?php
					if(!empty($forwardWareHouseCityAry_dest))
					{
						foreach($forwardWareHouseCityAry_dest as $forwardWareHouseCityArys)
						{
							?>
							<option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($countForwarderCity_dest==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['szCityTo']==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
	</div>
	<div class="fl-33" id="warehouse_des">
		<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
		<span id="destination_warehouse_span">
			<select name="oboDataExportAry[idDestinationWarehouse]" id="idDestinationWarehouse" onchange="sel_warehouse_for_approval('<?=$idForwarder?>','des',this.value);">
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($forwardWareHouseArr_dest))
					{
						foreach($forwardWareHouseArr_dest as $forwardWareHouseArrs)
						{
							?>
							<option value="<?=$forwardWareHouseArrs['id']?>" <? if($pricingCCServiceData[0]['idWarehouseTo']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
	</div>
<?php }
else if($_REQUEST['flag']=='warehouse_cc_origin')
{
	$t_base = "OBODataExport/";
	$idOriginCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	$szCity=$_REQUEST['szCity'];
	$forwardWareHouseArr_origin=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry,$szCity);
	
	if(!empty($forwardWareHouseArr_origin))
	{
            $countforwardWareHouseArr_origin = count($forwardWareHouseArr_origin);
            if($countforwardWareHouseArr_origin==1)
            {?>
                <script>
                    $("#idOriginWarehouse").attr('disabled',true);
                    $("#idOriginWarehouse_hidden").attr('value','');
                    $("#approve_button").unbind("click");
                    $("#approve_button").attr('style',"opacity:0.4");
                    sel_warehouse_for_approval('<?=$idForwarder?>','origin','<?=$forwardWareHouseArr_origin[0]['id']?>');
                </script>
            <?php				
            } 
	}
	?>
		<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
			<span id="origin_warehouse_span">
				<select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse" onchange="sel_warehouse_for_approval('<?=$idForwarder?>','origin',this.value);">
					<option value=""><?=t($t_base.'fields/select');?></option>
					<?php
						if(!empty($forwardWareHouseArr_origin))
						{
							foreach($forwardWareHouseArr_origin as $forwardWareHouseArrs)
							{
								?>
								<option value="<?=$forwardWareHouseArrs['id']?>" <? if($countforwardWareHouseArr_origin==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['idWarehouseFrom']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
								<?php
							}
						}
					?>
				</select>
			</span>
	<?php
}
else if($_REQUEST['flag']=='warehouse_cc_des')
{
	$t_base = "OBODataExport/";
	$idOriginCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	$szCity=$_REQUEST['szCity'];
	$forwardWareHouseArr_dest=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry,$szCity);
	
	if(!empty($forwardWareHouseArr_dest))
	{
		$countforwardWareHouseArr_dest = count($forwardWareHouseArr_dest);
		if($countforwardWareHouseArr_dest==1)
		{?>
			<script>
				$("#idDestinationWarehouse_hidden").attr('value','');
				$("#approve_button").unbind("click");
				$("#approve_button").attr('style',"opacity:0.4");
				sel_warehouse_for_approval('<?=$idForwarder?>','des','<?=$forwardWareHouseArr_dest[0]['id']?>');
			</script>
		<?php				
		}
		
	}
	?>

	<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
		<span id="destination_warehouse_span">
			<select name="oboDataExportAry[idDestinationWarehouse]" onchange="sel_warehouse_for_approval('<?=$idForwarder?>','des',this.value);" id="idDestinationWarehouse">
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($forwardWareHouseArr_dest))
					{
						foreach($forwardWareHouseArr_dest as $forwardWareHouseArrs)
						{
							?>
							<option value="<?=$forwardWareHouseArrs['id']?>" <? if($countforwardWareHouseArr_dest==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['idWarehouseTo']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
<?php
}
else if($_REQUEST['flag']=='city_warehouse_origin')
{
	$t_base = "OBODataExport/";
	$idOriginCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_origin=$Excel_export_import->getForwaderWarehouses($idForwarder,$idOriginCountry);
	
	$forwardWareHouseCityAry_origin = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idOriginCountry);
	
	if(!empty($forwardWareHouseCityAry_origin))
	{
		$countForwarderCity_origin = count($forwardWareHouseCityAry_origin);
		if($countForwarderCity_origin==1)
		{?>
			<script>
				$("#idOriginWarehouse").attr('disabled',true);
				$("#szOriginCity").attr('disabled',true);
				$("#idOriginWarehouse_hidden").attr('value','');
				$("#approve_button").unbind("click");
				$("#approve_button").attr('style',"opacity:0.4");
				select_city_approval('<?=$idForwarder?>','origin','<?=$forwardWareHouseCityAry_origin[0]['szCity']?>');
			</script>
		<?php				
		}
		
	}
	
?>
<div class="fl-33 s-field">
			<span class="f-size-12"><?=t($t_base.'title/city');?></span>
			<span id="origin_city_span">
				<select id="szOriginCity" name="oboDataExportAry[szOriginCity]" onchange="select_city_approval_cc('<?=$idForwarder?>','origin',this.value);">
					<option value=""><?=t($t_base.'title/all');?></option>
					<?php
						if(!empty($forwardWareHouseCityAry_origin))
						{
							foreach($forwardWareHouseCityAry_origin as $forwardWareHouseCityArys)
							{
								?>
								<option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($countForwarderCity_origin==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['szCityFrom']==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
								<?php
							}
						}
					?>
				</select>
			</span>
		</div>
		<div class="fl-33" id="warehouse_origin">
			<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
			<span id="origin_warehouse_span">
				<select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse" onchange="sel_warehouse_for_approval_cc('<?=$idForwarder?>','origin',this.value);">
					<option value=""><?=t($t_base.'fields/select');?></option>
					<?php
						if(!empty($forwardWareHouseArr_origin))
						{
							foreach($forwardWareHouseArr_origin as $forwardWareHouseArrs)
							{
								?>
								<option value="<?=$forwardWareHouseArrs['id']?>" <? if($pricingCCServiceData[0]['idWarehouseFrom']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
								<?php
							}
						}
					?>
				</select>
			</span>
</div>
<?php }
else if($_REQUEST['flag']=='city_warehouse_des')
{
	$t_base = "OBODataExport/";
	$idDesCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_dest=$Excel_export_import->getForwaderWarehouses($idForwarder,$idDesCountry);
	
	$forwardWareHouseCityAry_dest = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idDesCountry);
	
	if(!empty($forwardWareHouseCityAry_dest))
	{
		$countForwarderCity_dest = count($forwardWareHouseCityAry_dest);
		if($countForwarderCity_dest==1)
		{?>
			<script>
				$("#idDestinationWarehouse").attr('disabled',true);
				$("#szDestinationCity").attr('disabled',true);
				$("#idDestinationWarehouse_hidden").attr('value','');
				$("#approve_button").unbind("click");
				$("#approve_button").attr('style',"opacity:0.4");
				select_city_approval('<?=$idForwarder?>','des','<?=$forwardWareHouseCityAry_dest[0]['szCity']?>');
			</script>
		<?php				
		}
		
	}
	
?>
<div class="fl-33 s-field">
		<span class="f-size-12"><?=t($t_base.'title/city');?></span>
		<span id="destination_city_span">
			<select id="szDestinationCity" name="oboDataExportAry[szDestinationCity]" onchange="select_city_approval_cc('<?=$idForwarder?>','des',this.value);">
				<option value=""><?=t($t_base.'title/all');?></option>
				<?php
					if(!empty($forwardWareHouseCityAry_dest))
					{
						foreach($forwardWareHouseCityAry_dest as $forwardWareHouseCityArys)
						{
							?>
							<option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($countForwarderCity_dest==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['szCityTo']==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
	</div>
	<div class="fl-33" id="warehouse_des">
		<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
		<span id="destination_warehouse_span">
			<select name="oboDataExportAry[idDestinationWarehouse]" id="idDestinationWarehouse" onchange="sel_warehouse_for_approval_cc('<?=$idForwarder?>','des',this.value);">
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($forwardWareHouseArr_dest))
					{
						foreach($forwardWareHouseArr_dest as $forwardWareHouseArrs)
						{
							?>
							<option value="<?=$forwardWareHouseArrs['id']?>" <? if($pricingCCServiceData[0]['idWarehouseTo']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
	</div>
<?php }
else if($_REQUEST['flag']=='warehouse_origin')
{
	$t_base = "OBODataExport/";
	$idOriginCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	$szCity=$_REQUEST['szCity'];
	$forwardWareHouseArr_origin=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry,$szCity);
	if(!empty($forwardWareHouseArr_origin))
	{
		$countforwardWareHouseArr_origin = count($forwardWareHouseArr_origin);
		if($countforwardWareHouseArr_origin==1)
		{?>
			<script>
				$("#idOriginWarehouse").attr('disabled',true);
				$("#idOriginWarehouse_hidden").attr('value','');
				$("#approve_button").unbind("click");
				$("#approve_button").attr('style',"opacity:0.4");
				sel_warehouse_for_approval('<?=$idForwarder?>','origin','<?=$forwardWareHouseArr_origin[0]['id']?>');
			</script>
		<?php				
		}
		
	}
	?>
		<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
			<span id="origin_warehouse_span">
				<select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse" onchange="sel_warehouse_for_approval_cc('<?=$idForwarder?>','origin',this.value);">
					<option value=""><?=t($t_base.'fields/select');?></option>
					<?php
						if(!empty($forwardWareHouseArr_origin))
						{
							foreach($forwardWareHouseArr_origin as $forwardWareHouseArrs)
							{
								?>
								<option value="<?=$forwardWareHouseArrs['id']?>" <? if($countforwardWareHouseArr_origin==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['idWarehouseFrom']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
								<?php
							}
						}
					?>
				</select>
			</span>
	<?php
}
else if($_REQUEST['flag']=='warehouse_des')
{
	$t_base = "OBODataExport/";
	$idOriginCountry=$_REQUEST['idCountry'];
	$idForwarder=$_REQUEST['idForwarder'];
	$szCity=$_REQUEST['szCity'];
	$forwardWareHouseArr_dest=$Excel_export_import->getForwaderWarehouses($idForwarder,$idCountry,$szCity);
	
	if(!empty($forwardWareHouseArr_dest))
	{
		$countforwardWareHouseArr_dest = count($forwardWareHouseArr_dest);
		if($countforwardWareHouseArr_dest==1)
		{?>
			<script>
				$("#idDestinationWarehouse_hidden").attr('value','');
				$("#approve_button").unbind("click");
				$("#approve_button").attr('style',"opacity:0.4");
				sel_warehouse_for_approval('<?=$idForwarder?>','des','<?=$forwardWareHouseArr_dest[0]['id']?>');
			</script>
		<?php				
		}
		
	}
?>
	<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
		<span id="destination_warehouse_span">
			<select name="oboDataExportAry[idDestinationWarehouse]" onchange="sel_warehouse_for_approval_cc('<?=$idForwarder?>','des',this.value);" id="idDestinationWarehouse">
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($forwardWareHouseArr_dest))
					{
						foreach($forwardWareHouseArr_dest as $forwardWareHouseArrs)
						{
							?>
							<option value="<?=$forwardWareHouseArrs['id']?>" <? if($countforwardWareHouseArr_dest==1){echo "selected=\"selected\"";} else if($pricingCCServiceData[0]['idWarehouseTo']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
							<?php
						}
					}
				?>
			</select>
		</span>
<?php
} 
else if($_REQUEST['flag']=='delete_approved_data_popup') 
{
		$id=$_REQUEST['id'];
		$kUploadBulkService->load($id);
		$text='';
		if($kUploadBulkService->iFileType=='LCL Services')
		{
                    $text=t($t_base.'title/delete_draft_lcl_service_data');
		}
		else if($kUploadBulkService->iFileType=='Haulage')
		{
                    $text=t($t_base.'title/delete_draft_haulage_line_data');
		}
		else if($kUploadBulkService->iFileType=='Customs Clearance')
		{
                    $text=t($t_base.'title/delete_draft_custom_clearance_data');
		}
		else if($kUploadBulkService->iFileType=='CFS locations')
		{
                    $text=t($t_base.'title/delete_data_cfs_location_data');
		}
                else if($kUploadBulkService->iFileType=='Airport Warehouses')
		{
                    $text=t($t_base.'title/delete_data_airport_warehouse_location_data');
		}
                else if($kUploadBulkService->iFileType=='Airfreight Services')
		{
                    $text=t($t_base.'title/delete_draft_airfreight_service_data');
		}
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p class="close-icon" align="right">
			<a onclick="cancel_processing_cost();" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
				<h4><b><?=$text?></b></h4><br/>
				<? if((float)$kUploadBulkService->fProcessingCost>0)
				{?>
				<p><?=t($t_base.'title/are_you_sure_delete_file_top')?></p><br />
				<p><?=t($t_base.'title/are_you_sure_delete_file_line_1')?></p><br />
				<? }else{?>
				<p><?=t($t_base.'title/are_you_sure_delete_file')?></p><br />
				<? }?>
				<p align="center"> <a href="javascript:void(0)" class="button1" onclick="confirm_delete_upload_file('<?=$id?>','top')" id="yes_button"><span><?=t($t_base.'fields/confirm')?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_processing_cost();" id="no_button"><span><?=t($t_base.'fields/cancel')?></span></a> </p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=='review_haulage_pricing_model_data')
{
	
	$idWarehouse=$_REQUEST['idWarehouse'];
	$iDirection=$_REQUEST['iDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idService=$_REQUEST['idService'];
	$idForwarder=$_REQUEST['idForwarder'];
	
	$haulageApprovalDataArr=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulageTemp($idForwarder,$idService,$idWarehouse,$idHaulageModel,$iDirection);
	
	$haulageApprovalDataCountArr=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulageTempCount($idForwarder,$idService,$idWarehouse,$idHaulageModel,$iDirection);
	//echo $haulageApprovalDataCountArr."hhhh";
	haulageMiddleLayer($idHaulageModel,$iDirection,$idWarehouse,$idService,$idForwarder,$haulageApprovalDataArr);
	?><script type="text/javascript">
		var startvalue=$("#dataCounter").attr('value');
		var countvalue=<?=$haulageApprovalDataCountArr?>;
		//alert(countvalue);
		if(countvalue>1)
		{
			if(startvalue>1)
			{
				var newvalue=parseInt(startvalue)+parseInt(countvalue)-1;
				var newval=parseInt(startvalue)+parseInt(countvalue)-1;
			}
			else
			{
				var newvalue=<?=$haulageApprovalDataCountArr?>;
				var newval=<?=$haulageApprovalDataCountArr?>;
			}
				
			var valuestr=startvalue+'-'+newvalue;	
		}
		else
		{
			//alert(startvalue);
			if(startvalue>1)
			{
				var valuestr=parseInt(startvalue)+parseInt(countvalue)-1;
				var newval=valuestr+parseInt(countvalue)-1;
			}
			else
			{
				var valuestr=parseInt(startvalue);	
				var newval=parseInt(startvalue);
			}		
		}
		
		$("#inc_count").html(valuestr);
		$("#dataCounter").attr('value',newval);
	  </script>
	<?
}
else if($_REQUEST['flag']=='pricing_detail_list')
{
	$idHaulagePricingModel=$_REQUEST['id'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$iDirection=$_REQUEST['idDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idService=$_REQUEST['idService'];
	addHaulagePricingDataTemp($idHaulagePricingModel,$idWarehouse,$iDirection,$idHaulageModel,$idService);
}
else if($_REQUEST['flag']=='show_ploygon_on_google_map')
{
	$idHaulageModel=$_REQUEST['id'];
	?>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		 <td colspan="5"><iframe style="border:0" name="hssiframe" width="276px" height="228px" src="<?=__BASE_URL__?>/map_polygon.php?idHaulagePricingModel=<?=$idHaulageModel?>"></iframe></td>
		</tr>
	</table>
	<?
}
else if($_REQUEST['flag']=='show_postcode_str')
{
	$t_base = "HaulaugePricing/";
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id=$_REQUEST['id'];
	$szPostCodeArr=array();
	$szPostCodeArr=$kHaulagePricing->getSetOfPostCode($id,true);
	if(!empty($szPostCodeArr))
	{
		$szPostCode_Str=implode("<br/>",$szPostCodeArr);
	}
	?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
		 <th class="noborder" align="left"><strong><?=t($t_base.'fields/postcodes_included')?></strong></th>
		</tr>
		<tr>
		 <td class="noborder" align="left"><?=$szPostCode_Str?></td>
		</tr>
	</table>
	<?
}
else if($_REQUEST['flag']=='show_distance_str')
{
	$t_base = "HaulaugePricing/";
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['id'];
	
	$allHaulageDistanceCountriesArr=$kHaulagePricing->getAllCountriesForDistanceHaulageModel($idHaulagePricingModel);
	?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
		 <th class="noborder" align="left"><strong><?=t($t_base.'fields/countries_included')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/countries_include_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
		</tr>
		<tr>
		 <td class="noborder">
		 <? if(!empty($allHaulageDistanceCountriesArr))
		 	{
		 		foreach($allHaulageDistanceCountriesArr as $allHaulageDistanceCountriesArrs)
		 		{
		 		
		 			echo $allHaulageDistanceCountriesArrs['szCountryName']."<br/>";
		 		}
		 	}?>
		 </td>
		</tr>
	</table>
	<?
}
else if($_REQUEST['flag']=='haulage_pricing_model_data_try_it_out')
{
	$idHaulagePricingModel=$_REQUEST['id'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$iDirection=$_REQUEST['iDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idService=$_REQUEST['idService'];
	
	try_it_out_haulage_temp_data($idWarehouse,$iDirection,$idHaulageModel);
}
else if($_REQUEST['flag']=='delete_approved_data_popup_cfs_bulk_upload')
{
		$id=$_REQUEST['id'];
		$idForwarder=$_REQUEST['idForwarder'];
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p class="close-icon" align="right">
			<a onclick="cancel_processing_cost();" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
				<h4><b><?=t($t_base.'title/delete_data_cfs_location_data')?></b></h4><br/>
				<p><?=t($t_base.'title/are_you_sure_delete_file')?></p><br />
				<p align="center"> <a href="javascript:void(0)" class="button1" onclick="confirm_delete_upload_file_cfs_upload('<?=$id?>','top','<?=$idForwarder?>')" id="yes_button"><span><?=t($t_base.'fields/confirm')?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_processing_cost();" id="no_button"><span><?=t($t_base.'fields/cancel')?></span></a> </p>
			</div>
		</div>	
<?
}
else if($_REQUEST['flag']=='delete_cfs_bulk_upload_file_by_forwarder')
{
	$id=$_REQUEST['id'];
	$datatablevalue=$_REQUEST['datatablevalue'];
	//$kUploadBulkService->load($id);
	$idForwarder=$_REQUEST['idForwarder'];
	$kUploadBulkService->deleteCFSLocationData(0,$idForwarder,$id);
		
	
	//$kUploadBulkService->deleteUploadedFileByForwarder($id);
	$bottomTableData=$kUploadBulkService->getAllDataNotApprovedByForwarder($datatablevalue);
	
	$forwarderAwaitingCFSDataArr=$kUploadBulkService->getForwarderUploadCFSLocation($idForwarder);
	process_cost_approved_by_management($bottomTableData,$forwarderAwaitingCFSDataArr);		
}
?>