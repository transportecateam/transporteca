<?php 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Credit Notes";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/admin_functions.php");

include( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

checkAuthForwarder();

$kBooking = new cBooking();
//checkAuth();
$idBooking = $_REQUEST['idBooking'];
if($idBooking>0){
$flag=$_REQUEST['flag'];
$pdfhtmlflag=true;
$flag = 'PDF'; //We have removed to show HTML so this flag will always be pdf now.
if($flag=='PDF')
{
    $pdfhtmlflag='PDF';
}
$kBooking->load($idBooking);
$bookingInvoicePdf = getSelfBilledCreditNoteInvoice($idBooking,$flag);

if($flag=='PDF')
{
     download_booking_pdf_file($bookingInvoicePdf);
        die;
}}
?>