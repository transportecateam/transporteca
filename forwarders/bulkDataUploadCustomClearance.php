<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
ini_set('max_execution_time',1200);
ini_set('memory_limit', '-1');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$display_profile_not_completed_message=true;
$szMetaTitle='Transporteca | Update Customs Clearance - Upload File';
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
$maxCountRowError=false;
$Excel_export_import=new cExport_Import();
$kConfig = new cConfig();
$kWHSSearch = new cWHSSearch();
$t_base = "BulkUpload/";
$t_base_error="Error";
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

$objPHPExcel=new PHPExcel();
if((int)$_SESSION['forwarder_user_id']==0)
{
    header('Location:'.__BASE_URL__.'/forwarders/');
    exit();	
}
if($_SESSION['forwarder_admin_id']>0)
{
    //$kForwarderContact->load($_SESSION['forwarder_admin_id']);
    $kForwarder->load($_SESSION['forwarder_id']);
    $err_msg_file='';
    $idForwarder=$kForwarder->id;
    $fileUploadFlag=false;
}
else
{
    $kForwarderContact->load($_SESSION['forwarder_user_id']);
    $kForwarder->load($kForwarderContact->idForwarder);
    $err_msg_file='';
    $idForwarder=$kForwarderContact->idForwarder;
    $fileUploadFlag=false;
}
$maxrowsexcced=$kWHSSearch->getManageMentVariableByDescription('__MAX_ROWS_EXCEED__');
if($_POST['file_upload']=='1')
{
    if($_FILES['lclservicefile']['name']!="")
    { 
        $fileextension=get_file_extension($_FILES['lclservicefile']['name']);
        if($fileextension=='xlsx')
        {
            $szDisplayName=str_replace(" ","_",$kForwarder->szDisplayName);
            $szDisplayName=str_replace("/","_",$szDisplayName);
            $folderName=$szDisplayName."_".$kForwarder->id;
            $importData="importData";

            if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName)) {
                mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName);
                chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName,0777);
                if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData)) {
                     mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData);
                    chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData,0777); 
                }
            }
            else
            {
                if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData)) {
                    mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData);
                    chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData,0777);
                }
            }
            $fh = fopen(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData."/index.php", "w+");
            //print_r($_FILES);
            $fileName="Transporteca_Customs_Clerance_Upload_Sheet_".$_SESSION['forwarder_user_id']."_".date('dmYHis').".xlsx";

            $fileUploadPath=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData;
            if(move_uploaded_file($_FILES['lclservicefile']['tmp_name'], $fileUploadPath."/".$fileName)) 
            {  
                $file_name= $fileUploadPath."/".$fileName;
                $sheetindex=0;
                $objReader = new PHPExcel_Reader_Excel2007();
                //print_r($objReader);
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($file_name);


                $objPHPExcel->setActiveSheetIndex($sheetindex);
                $tabname =($objPHPExcel->getActiveSheet()->getTitle());
                if($tabname=='Sheet1')
                {
                    $val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL 
                    $colcount=PHPExcel_Cell::columnIndexFromString($val1);
                    //echo $colcount."<br/>"; 
                    $rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow()); 
                    if($rowcount<=__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__)
                    {
                        //echo $rowcount."<br/>";
                        if($colcount>=10)
                        {
                            if($Excel_export_import->importCustomClearanceData($objPHPExcel,$sheetindex,$colcount,$rowcount,$kForwarder->id))
                            {
                                $fileUploadFlag=true;
                                $_POST='';
                                $_FILES=array();
                            }
                            else
                            {
                                $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                            }
                        }
                        else
                        {
                            $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                        }
                    }
                    else
                    {
                        $maxCountRowError=true;
                        $err_msg_file=t($t_base.'error/file_max_row_count_error_1')." ".number_format((int)__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__)." ".t($t_base.'error/file_max_row_count_error_3');
                    }
                }
                else
                {
                    $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                }
            } 
            else
            {
                $err_msg_file=t($t_base.'messages/error_in_uploading_the_file');
            } 
        }
        else
        {
            $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
        }
    }
    else
    {
        $err_msg_file=t($t_base.'messages/file_is_required');
    } 
}

$pricingCCDataArr=array();
if($_REQUEST['checkmode']=='currency')
{
    if(isset($_REQUEST['idCurrency']) && (int)$_REQUEST['idCurrency']>0)
    {
        $idCurrency=$_REQUEST['idCurrency'];
        if($Excel_export_import->checkCurrencyValid($idCurrency))
        {
            $pricingCCDataArr=$Excel_export_import->getInactiveCCData($idForwarder,$idCurrency);
            if(!empty($pricingCCDataArr))
            {
                foreach($pricingCCDataArr as $pricingCCDataArrs)
                {
                    $warehouseFromListArr[]=$pricingCCDataArrs['idWarehouseFrom'];
                    $warehouseToListArr[]=$pricingCCDataArrs['idWarehouseTo'];
                }
            }
        } 
    }
}

if(!empty($err_msg_file)){
?>
<script type="text/javascript">
$("#loader").attr('style','display:none;');
addPopupScrollClass('error_msg');
//alert('hi');
</script>
<div id="error_msg">
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup signin-popup signin-popup-verification">
            <p class="close-icon" align="right">
                <a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/CustomsClearanceBulk/">
                    <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <?php if($maxCountRowError) {  ?>
               <h4><b><?=t($t_base.'error/file_max_row_count_error_2')?></b></h4><br/>
               <p><?=$err_msg_file?></p><br />
            <?php } else { ?>
            <h4><b><?=t($t_base_error.'/invalid_file_format')?></b></h4><br />
            <p><?=t($t_base_error.'/we_did_not_recg_cc')?></p><br />
            <?php } ?>
            <p align="center"><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/CustomsClearanceBulk/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
        </div>
    </div>
</div>
<?php } if($fileUploadFlag) { ?>
    <script type="text/javascript">
    $("#loader").attr('style','display:none;');
        addPopupScrollClass('success_msg'); 
    </script>
    <div id="success_msg">
        <div id="popup-bg"></div>
        <div id="popup-container">
            <div class="popup signin-popup signin-popup-verification">
                <h5><?=t($t_base.'title/upload_of_custom_clearance');?></h5>
                <p><?=t($t_base.'messages/success_file_upload_msg_1');?> <?=$Excel_export_import->successLine?> <?=t($t_base.'messages/success_file_upload_msg_custom_clearance_4');?>.</p><br/>
                <?php if($Excel_export_import->deleteLine>0){?>
                <p><?=$Excel_export_import->deleteLine?> <?=t($t_base.'messages/lines_delete_uploaded');?>.</p><br/>
                <?php }?>
                <?php if($Excel_export_import->errorLine>0){?>
                <p><?=t($t_base.'messages/we_found');?> <?=$Excel_export_import->errorLine?> <?=t($t_base.'messages/lines_error_uploaded');?>.</p><br/>
                <?php }else{?>
                <p><?=t($t_base.'messages/success_file_upload_msg_2');?>.</p><br/>
                <?php }?>
                <p><?=t($t_base.'messages/success_file_upload_msg_3');?>.</p><br/>
                <p align="center"><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/CustomsClearanceBulk/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
            </div>
        </div>
    </div>
<?php	
}

$forwardWareHouseArr = $Excel_export_import->getForwaderWarehouses($idForwarder,0,false,true);
$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder);

$changeDropDownValue=$kWHSSearch->getManageMentVariableByDescription('__CHANGE_DROP_DOWN_OPTION__'); 
?>
<script type="text/javascript">
    var FORWARDER_WAREHOUSE_LIST = new Array();
    <?php
        if(!empty($forwardWareHouseArr))
        {
            foreach($forwardWareHouseArr as $forwardWareHouseArrs)
            {
                ?>
                FORWARDER_WAREHOUSE_LIST['<?php echo $forwardWareHouseArrs['idWarehouse']; ?>'] = '<?php echo $forwardWareHouseArrs['iWarehouseType']; ?>';
                <?php
            }
        }
    ?> 
</script>
<div id="hsbody-2">
    <div class="hsbody-2-left account-links">
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
    </div>
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader"></div>
        <div id="popup-container">
            <div class="popup signin-popup signin-popup-verification">
                <p><?=t($t_base.'messages/plz_wait_while_we_process');?></p>
                <br/><br/>
                <p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
            </div>	
        </div>	 
    </div>
    <div class="hsbody-2-right">
        <ol style="margin:0;padding:0 0 0 10px">
            <form name="bulkExportData" id="bulkExportData" method="POST">
            <li>
                <p><?=t($t_base.'title/add_remove_update_origin_destination_custom_clearance');?></p><br />
                <div class="oh">
                    
                        <div class="fl-49"> 
                            <h4><strong><?=t($t_base.'title/origin_custom_clear');?></strong></h4>
                            <div class="oh">
                                <div class="fl-33 s-field">
                                    <select name="dataExportArr[originCountry]" id="originCountry" onchange="showCCForwarderWarehouse('<?=$idForwarder?>',this.value,'orgin_warehouse','<?=$maxrowsexcced?>')">
                                        <option value=""><?=t($t_base.'fields/all');?></option>
                                        <?php
                                            if(!empty($forwardCountriesArr))
                                            {
                                                foreach($forwardCountriesArr as $forwardCountriesArrs)
                                                {
                                                    ?>
                                                    <option value="<?=$forwardCountriesArrs['idCountry']?>_<?=$forwardCountriesArrs['szCountryName']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="fl-65 s-field" id="orgin_warehouse">
                                    <select name="dataExportArr[originWarehouse]" id="originWarehouse">
                                        <option value=""><?=t($t_base.'fields/all');?></option>
                                        <?php
                                            if(!empty($forwardWareHouseArr))
                                            {
                                                foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                                                {
                                                    ?>
                                                    <option value="<?=$forwardWareHouseArrs['id']?>_<?=$forwardWareHouseArrs['szWareHouseName']?>_<?=$forwardWareHouseArrs['szCountryName']?>_<?=$forwardWareHouseArrs['idCountry']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="oh">
                                <p class="fl-65" style="padding-top:6px;"><?=t($t_base.'title/click_to_add_origin' ); ?></p>
                                <p class="fl-33" align="right">
                                <input type="hidden" name="dataExportArr[originWarehouseAdded]" id="originWarehouseAdded" value="1">
                                <input type="hidden" name="dataExportArr[idOriginWarehouse]" id="idOriginWarehouse" value="">
                                <input type="hidden" name="dataExportArr[idOriginCountry]" id="idOriginCountry" value="">
                                <input type="hidden" name="deleteWareHouseArr" id="deleteWareHouseOriginArr" value="">
                                <input type="hidden" name="deleteWareHouseDivIdArr" id="deleteWareHouseDivIdOriginArr" value="">
                                <a href="javascript:void(0)" class="button1" onclick="addWareHousesCustomClearance('Origin','<?=$maxrowsexcced?>');" id="cc_orign"><span style="min-width:70px;"><?=t($t_base.'fields/add');?></span></a></p>
                            </div> 	
                            <div class="scroll-div" style="width:340px;padding:0;height:190px;margin-bottom:5px;">
                                <table cellspacing="0" width="323" cellpadding="0" border="0" class="format-2" width="97%" id="originTable" style="border-right:none;top: -1px;left:-1px;min-width:100%;">
                                    <tr>
                                        <th style="border-top:none;" width="33%"><strong><?=t($t_base.'fields/country');?></strong></th>
                                        <td style="border-top:none;" width="67%"><strong><?=t($t_base.'fields/name');?></strong></td>
                                    </tr>
                                    <?php 
                                        $j=1;	
                                        $org_w_c='';
                                        if(!empty($warehouseFromListArr))
                                        {  
                                            foreach($warehouseFromListArr as $warehouseFromListArrs)
                                            {
                                                $kWHSSearch->load($warehouseFromListArrs);
                                                $warehouse_idcounttry=$warehouseFromListArrs."_".$kWHSSearch->idCountry;
                                                if($org_w_c=='')
                                                {
                                                    $org_w_c=$warehouse_idcounttry;
                                                }
                                                else
                                                {
                                                    $org_w_c .=";".$warehouse_idcounttry;
                                                }
                                                ?>
                                                <tr id="origin_data_<?=$j?>" onclick="delete_ware_houseInfo_customclearance('<?=$warehouse_idcounttry?>','origin_data_<?=$j?>','Origin','<?=$maxrowsexcced?>')">
                                                    <td><?=$kConfig->getCountryName($kWHSSearch->idCountry)?></td>
                                                    <td><?=$kWHSSearch->szWareHouseName?></td>
                                                </tr>
                                            <?php	
                                                ++$j;
                                            }
                                        }else {?>
                                            <tr id="origin_data_1" onclick=""></tr>
                                        <?php }?>
                                    </table>
                                </div> 	
                                <div class="oh">
                                    <p class="fl-65" style="padding-top:6px;"><?=t($t_base.'title/click_remove_origin_cfs');?></p>
                                    <p class="fl-33" align="right"><a href="javascript:void(0)" class="button1" onclick="" style="opacity:0.4;" id="origin_remove_button"><span style="min-width:70px;"><?=t($t_base.'fields/remove');?></span></a></p>
                                </div>
                            </div> 
                            <div class="fl-49" style="float:right;">
                                <h4><strong><?=t($t_base.'title/destination_custom_clear');?></strong></h4>
                                <div class="oh">
                                    <div class="fl-33 s-field">
                                        <select name="dataExportArr[desCountry]" id="desCountry" onchange="showCCForwarderWarehouse('<?=$idForwarder?>',this.value,'des_warehouse','<?=$maxrowsexcced?>')">
                                            <option value=""><?=t($t_base.'fields/all');?></option>
                                            <?php
                                                if(!empty($forwardCountriesArr))
                                                {
                                                    foreach($forwardCountriesArr as $forwardCountriesArrs)
                                                    {
                                                        ?>
                                                        <option value="<?=$forwardCountriesArrs['idCountry']?>_<?=$forwardCountriesArrs['szCountryName']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="fl-65 s-field" id="des_warehouse">
                                        <select name="dataExportArr[desWarehouse]" id="desWarehouse">
                                            <option value=""><?=t($t_base.'fields/all');?></option>
                                            <?php
                                                if(!empty($forwardWareHouseArr))
                                                {
                                                    foreach($forwardWareHouseArr as $forwardWareHouseArrs)
                                                    {
                                                        ?>
                                                        <option value="<?=$forwardWareHouseArrs['id']?>_<?=$forwardWareHouseArrs['szWareHouseName']?>_<?=$forwardWareHouseArrs['szCountryName']?>_<?=$forwardWareHouseArrs['idCountry']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="oh">
                                    <p class="fl-65" style="padding-top:6px;"><?=t($t_base.'title/click_to_add_destination' ); ?></p>
                                    <p class="fl-33" align="right">
                                    <input type="hidden" name="dataExportArr[desWarehouseAdded]" id="desWarehouseAdded" value="1">
                                    <input type="hidden" name="dataExportArr[idDesWarehouse]" id="idDesWarehouse" value="">
                                    <input type="hidden" name="dataExportArr[idDesCountry]" id="idDesCountry" value="">
                                    <input type="hidden" name="deleteWareHouseArr" id="deleteWareHouseDesArr" value="">
                                    <input type="hidden" name="deleteWareHouseDivIdArr" id="deleteWareHouseDivIdDesArr" value="">
                                    <a href="javascript:void(0)" class="button1" onclick="addWareHousesCustomClearance('Destination','<?=$maxrowsexcced?>');" id="cc_des"><span style="min-width:70px;"><?=t($t_base.'fields/add');?></span></a></p>
                                </div> 
                                <div class="scroll-div" style="width:340px;padding:0;height:190px;margin-bottom:5px;">
                                    <table width="323" cellspacing="0" cellpadding="0" border="0" class="format-2" width="97%" id="desTable" style="border-right:none;top: -1px;left:-1px;min-width:100%;">
                                        <tr>
                                            <th style="border-top:none;" width="33%"><strong><?=t($t_base.'fields/country');?></strong></th>
                                            <td style="border-top:none;" width="67%"><strong><?=t($t_base.'fields/name');?></strong></td>
                                        </tr>
                                        <?php 
                                            $t=1;
                                            $des_w_c='';
                                            if(!empty($warehouseToListArr)){

                                            foreach($warehouseToListArr as $warehouseToListArrs)
                                            {
                                                $kWHSSearch->load($warehouseToListArrs);
                                                $warehouse_idcounttry=$warehouseToListArrs."_".$kWHSSearch->idCountry;

                                                if($des_w_c=='')
                                                {
                                                    $des_w_c=$warehouse_idcounttry;
                                                }
                                                else
                                                {
                                                    $des_w_c .=";".$warehouse_idcounttry;
                                                }
                                                ?>
                                                <tr id="des_data_<?=$t?>" onclick="delete_ware_houseInfo_customclearance('<?=$warehouse_idcounttry?>','des_data_<?=$t?>','Destination','<?=$maxrowsexcced?>')">
                                                    <td><?=$kConfig->getCountryName($kWHSSearch->idCountry)?></td>
                                                    <td><?=$kWHSSearch->szWareHouseName?></td>
                                                </tr>
                                                <?php	
                                                ++$t;
                                            }
                                        }else { ?>
                                            <tr id="des_data_1">

                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>				
                                <div class="oh">
                                    <p class="fl-65" style="padding-top:6px;"><?=t($t_base.'title/click_remove_destination_cfs');?></p>
                                    <p class="fl-33" align="right"><a href="javascript:void(0)" class="button1" onclick="" style="opacity:0.4;" id="des_remove_button"><span style="min-width:70px;"><?=t($t_base.'fields/remove');?></span></a></p>
                                </div>
                            </div> 
                        
                    </div>
                    <br/>
                    </li>
                    <li> 
                        <p><?=t($t_base.'title/download_excel_file_custom_clearance');?></p>  <br/>
                        <p align="center">
                            <select name="dataExportArr[downloadType]" id="downloadType" onchange="changeDownloadTypeCC('<?=$maxrowsexcced?>');">
                                <option value='2'>Download all possible combinations</option>
                                <option value='1'>Download only services currently updated</option>
                            </select>
                        </p> <br />
                        <div id="row_excced_data_1" class="note_info"><p align="left">Note: No CFS or airport warehouse combinations selected.</p></div>
                        <div id="row_excced_data_2" class="note_info" style="display:none;"><p align="left">Note: Approximately <span id="total_row_excced_1"></span> rows selected - expected file download time of <span id="total_row_excced_time"></span>.</p></div>
                        <div id="row_excced_data" class="note_info" style="display:none;"><p align="left">Note: Approximately <span id="total_row_excced"></span> rows selected - please reduce to maximum <?=number_format($maxrowsexcced)?> rows.</p></div> 
                        <p align="center">
                            <input type="hidden" name="dataExportArr[changeDropDown]" id="changeDropDown" value="<?=$changeDropDownValue?>">
                            <input type="hidden" name="dataExportArr[mainurl]" id="mainurl" value="<?=__MAIN_SITE_HOME_PAGE_URL__?>">
                            <input type="hidden" name="dataExportArr[inActiveService]" id="inActiveService" value="0">
                            <input type="hidden" name="dataExportArr[flag]" id="flag" value="CC_DATA_CHECK">
                            <input type="hidden" name="dataExportArr[idCurrency]" id="idCurrency" value="<?=$idCurrency?>">
                            <a href="javascript:void(0)" class="button1" onclick="" style="opacity:0.4;" id="download_button"><span><?=t($t_base.'fields/download');?></span></a>
                        </p> 
                        <br /> <br/>
                    </li>
                    </form>
                    <li>
                        <form name="uploadFile" id="uploadFile" method="post" enctype="multipart/form-data">
                            <p><?=t($t_base.'title/upload_updated_file');?> <br/><em><a href="<?=__BASE_URL__?>/welcomeVideo.php?type=cc_bulk" onclick="open_welcome_video();" target="google_map_target_1"><?=t($t_base.'links/help_me_update_file');?></a></em></p> 
                            <div class="file">
                                <input type="file" id="fileUpload" name="lclservicefile" />
                                <span class="button"><?=t($t_base.'fields/no_file_selected');?></span>
                            </div>
                            <p align="center">
                                <input type="hidden" value="1" name="file_upload" id="file_upload" accept="*.xlsx">
                                <a href="javascript:void(0)" class="button1" onclick="submit_upload_file();"><span><?=t($t_base.'fields/upload');?></span></a>
                            </p><br/>
                            <p><?=t($t_base.'title/avail_on_transporteca');?></p> <br />
                        </form> <br/>
                    </li>
                    <li>	
                        <p><?=t($t_base.'title/confirmation_email_from_transporteca');?></p>
                    </li>
                </ol>				
            </div> 
        </div>
<div id="popup_container_google_map" style="display:none;">
 <div id="popup-bg"></div>
	<div id="popup-container">
	<div class="popup" style="width:720px;margin-top:50px;">
	<p class="close-icon" align="right">
	<a href="javascript:void(0);" onclick="window.top.window.hide_google_map();">
	<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
	</a>
	</p>
            <iframe id="google_map_target_1" style="height:490px;width:720px;" align="center" valign="middle" class="google_map_select"  name="google_map_target_1" src="#" >
            </iframe>
	</div>
	</div>
</div>
	<?php
	if($j>1)
	{?>
            <script>
                $("#originCountry").attr("disabled",true);
                $("#originWarehouse").attr("disabled",true);
                $("#cc_orign").unbind("click");
                $("#cc_orign").attr("style","opacity:0.4;");
                $("#inActiveService").attr('value','1');
                $("#originWarehouseAdded").attr('value','<?=$j?>');
                $("#idOriginWarehouse").attr('value','<?=$org_w_c?>');
            </script>
            <?php
	}
	
	if($t>1)
	{?>
		<script>
			$("#desCountry").attr("disabled",true);
			$("#desWarehouse").attr("disabled",true);
			$("#cc_des").unbind("click");
			$("#cc_des").attr("style","opacity:0.4;");
			$("#inActiveService").attr('value','1');
			$("#desWarehouseAdded").attr('value','<?=$t?>');
			$("#idDesWarehouse").attr('value','<?=$des_w_c?>');
		</script>
		<?php
	}
	if($t>1 && $j>1)
	{
		?>
			<script>
			$("#download_button").unbind("click");
			$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
			$("#download_button").attr("style","opacity:1;");
			</script>
	
		<?php
	}
	
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>	