<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = "Transporteca | Page not found";
require_once(__APP_PATH_LAYOUT__."/forwarder_header.php");
pageNotFound();
require_once(__APP_PATH_LAYOUT__."/forwarder_footer.php");
?>	