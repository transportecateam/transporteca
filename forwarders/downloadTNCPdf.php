<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
require_once( __APP_PATH__ . "/forwarders/html2pdf/index.php" );
		 $version=$kForwarder->selectVersion();
		 /*
		 if(!empty($version['dtVersionUpdated']) && $version['dtVersionUpdated']!='0000-00-00 00:00')
		 {
		 	$version=date('dFY',strtotime($version['dtVersionUpdated']));
		 }
		 else
		 {
		 	$version=date('dFY');
		 }
		 */
		 $version=date('dFY',strtotime($version['dtVersionUpdated']));
		ob_clean();
		$version="Transporteca_TNC_Version_".$version;
		$file=__APP_PATH__."/forwarders/html2pdf/".$version.".pdf";
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header("Content-Type: application/force-download");
	    header('Content-Disposition: attachment; filename=' . urlencode(basename($file)));
	    // header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    ob_clean();
	    flush();
	    readfile($file);
	    unlink($file);
	    exit;
?>
