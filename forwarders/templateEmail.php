<?php 
/**
  *  Forwarder--- E-Mail log management
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Message Settings - E-Mail Templates ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
$iQuickQuotPage=1;
require_once(__APP_PATH_LAYOUT__ ."/forwarder_header.php");
checkAuthForwarder();
$kAdmin = new cAdmin();
/*
* We have killed this page on 21-12-106 and moved all the templates from here to Managem Manage E-mail template section @Ajay
*/
ob_end_clean();
header("Location: ".__FORWARDER_HOME_PAGE_URL__);
die;

$t_base="management/temp_email_mgmt/";
//$count= $kAdmin->selectMaxEmail();
$emailTemp=$kAdmin->templateEmail(false,false,2);
//PRINT_r($emailTemp);DIE;
//require_once(__APP_PATH_CLASSES__."/pagination.class.php" );
$kConfig =new cConfig();
$langArr=$kConfig->getLanguageDetails();
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/forwarder_company_nav.php" ); ?> 
	<div class="hsbody-2-right">
            <div class="clearfix">
                    <select id="selectEditor" name="messageName" onchange="selectEdit(this.value);" style="max-width:370px;">
			<?php
                            if($emailTemp!=array())
                            {
                                echo "<option ''>".t($t_base.'fields/select')."</option>";
                                foreach($emailTemp as $email)
                                {
                                    echo "<option value='".$email['id']."'>".$email['szFriendlyName']."</option>";
                                }
                            }
			?>	
			</select>	
                        <select id="iLanguage" name="iLanguage" style="max-width:90px;" onchange="selectEdit('',1);">
                            <?php
                                if(!empty($langArr))
                                {
                                    foreach ($langArr as $langArrs)
                                    {?>
                            <option value="<?php echo $langArrs['id']; ?>"><?php echo ucwords($langArrs['szName']);?></option>
                                       <?php 
                                    }
                                }
                            ?>
<!--                            <option value="<?php echo __LANGUAGE_ID_ENGLISH__; ?>">English</option>
                            <option value="<?php echo __LANGUAGE_ID_DANISH__; ?>">Danish</option>-->
			</select>
                    <span>
			<a class="button1" style="opacity:0.4;" id="previewTemp"><span><?=t($t_base.'fields/preview');?></span></a>
			<a class="button1" style="opacity:0.4;" id="editTemp"><span><?=t($t_base.'fields/edit');?></span></a>
                    </span>
		</div>
		<div id="Error-Log"></div>
		<div id="showdetails">
			
		</div>	
	</div>	
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>