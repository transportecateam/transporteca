<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
$t_base = "home/homepage/";
$kConfig=new cConfig();

$szPostCode = sanitize_all_html_input(trim($_GET["q"]));
$szCity = sanitize_all_html_input(trim($_GET["szCity"])) ;
$action = sanitize_all_html_input(trim($_GET["action"])) ;
$default_action = sanitize_all_html_input(trim($_GET["default_action"])) ;
$idCountry = strtolower(sanitize_all_html_input(trim($_GET["idCountry"])));
/*
if (empty($szPostCode) || empty($idCountry))
{
	return;
}
if($idCountry==46) // if country is China then we have to check in Hongkong as well. 46 is id of China and 100 is id of hongkong. 
{
	$idCountry =$idCountry." ,100" ;
	$query_and = "  idCountry IN (".$idCountry.") ";
}

if($idCountry==46) // if country is China then we have to check in Hongkong as well. 46 is id of China and 100 is id of hongkong. 
	{
		$idCountry =$idCountry." ,100" ;
		$query_and = "  idCountry IN (".$idCountry.") ";
	}

*/
if($action=='POST_CODE')
{
	if(trim($szCity)==t($t_base.'fields/type_name'))
	{
		$szCity = '';
	}
	
	if(trim($szPostCode)==t($t_base.'fields/optional') || trim($szPostCode)==t($t_base.'fields/type_code'))
	{
		$szPostCode = '';
	}	
	if($idCountry>0)
	{
		$query_and = "  idCountry ='".(int)$idCountry."'";
	}
	else
	{
		return ;
	}
	
	if(!empty($szCity))
	{
		//$query_and .= " AND szCity ='".mysql_escape_custom(trim($szCity))."'";
		$query_and.= "
				AND 
			(
				szCity = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity1 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity2 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity3 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity4 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity5 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity6 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity7 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity8 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity9 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szCity10 = '".mysql_escape_custom(trim($szCity))."'
			OR
				szRegion1 = '".mysql_escape_custom(trim($szCity))."'	
			OR
				szRegion2 = '".mysql_escape_custom(trim($szCity))."'	
			OR
				szRegion3 = '".mysql_escape_custom(trim($szCity))."'	
			OR
				szRegion4 = '".mysql_escape_custom(trim($szCity))."'	
			)		
		";
	}
	
	if($default_action=='All' && $szPostCode=='')
	{
		//if down key is pressed then only control comes in this block ;
	}
	else if(!empty($szPostCode))
	{
		$query_and.= " AND 
			szPostCode LIKE '".mysql_escape_custom(trim($szPostCode))."%'" ;
	}
	else
	{
		return ;
	}
	$query = " 
		SELECT 
			DISTINCT szPostCode
		FROM
			 ".__DBC_SCHEMATA_POSTCODE__." 
		USE INDEX (country_postcode_city__regions)
		WHERE
			$query_and		
		ORDER BY
			 iPriority DESC, szPostCode ASC
		LIMIT
			0,10	
	";
	//echo $query ;
	if($result = $kConfig->exeSQL($query))
	{
		while($row = $kConfig->getAssoc($result))
		{
			echo $row['szPostCode']."\n";
		}
	}
}
if($action=='CITY')
{	
	if(trim($szPostCode)==t($t_base.'fields/type_name'))
	{
		$szPostCode = '';
		return ;
	}	
	if($idCountry>0)
	{
		$query_and = "  idCountry ='".(int)$idCountry."'";
	}
	else
	{
		return ;
	}	
	
	if($default_action=='All' && $szPostCode=='')
	{
		
	}
	else if(!empty($szPostCode))
	{
		$query_and.= "
				AND 
			(
				szCity LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity1 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity2 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity3 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity4 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity5 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity6 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity7 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity8 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity9 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity10 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szRegion1 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			OR
				szRegion2 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			OR
				szRegion3 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			OR
				szRegion4 LIKE '".mysql_escape_custom(trim($szPostCode))."%'	
			)		
		";
	}
	
	$query = " 
		SELECT 
			szCity,
			szCity1,
			szCity2,
			szCity3,
			szCity4,
			szCity5,
			szCity6,
			szCity7,
			szCity8,
			szCity9,
			szCity10,
			szRegion1,
			szRegion2,
			szRegion3,
			szRegion4
		FROM
			 ".__DBC_SCHEMATA_POSTCODE__." 
			 USE INDEX (country_city_regions)
		WHERE
			$query_and	
		ORDER BY
			iPriority DESC, szCity ASC		
		LIMIT
			0,200			
	";
	//echo $query ;
	if($result = $kConfig->exeSQL($query))
	{
		$ctr = 0;
		while($row = $kConfig->getAssoc($result))
		{
			$cityPostCodeAry[$ctr] = $row ;
			$ctr++;
		}
		if(!empty($cityPostCodeAry))
		{
			$str_length = strlen($szPostCode);
			$final_ary = array();
			$countr = 0;
			foreach($cityPostCodeAry as $cityPostCodeArys)
			{
				if(!empty($cityPostCodeArys['szCity']) && (substr(mb_strtolower($cityPostCodeArys['szCity'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity1']) && (substr(mb_strtolower($cityPostCodeArys['szCity1'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity1'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity2']) && (substr(mb_strtolower($cityPostCodeArys['szCity2'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity2'] ;
				}
				else if(!empty($cityPostCodeArys['szCity3']) && (substr(mb_strtolower($cityPostCodeArys['szCity3'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity3'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity4']) && (substr(mb_strtolower($cityPostCodeArys['szCity4'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity4'] ;
				}
				else if(!empty($cityPostCodeArys['szCity5']) && (substr(mb_strtolower($cityPostCodeArys['szCity5'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity5'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity6']) && (substr(mb_strtolower($cityPostCodeArys['szCity6'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity6'] ;
				}
				else if(!empty($cityPostCodeArys['szCity7']) && (substr(mb_strtolower($cityPostCodeArys['szCity7'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity7'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity8']) && (substr(mb_strtolower($cityPostCodeArys['szCity8'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity8'] ;
				}
				else if(!empty($cityPostCodeArys['szCity9']) && (substr(mb_strtolower($cityPostCodeArys['szCity9'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity9'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity10']) && (substr(mb_strtolower($cityPostCodeArys['szCity10'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity10'] ;
				}
				else if(!empty($cityPostCodeArys['szRegion1']) && (substr(mb_strtolower($cityPostCodeArys['szRegion1'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion1'] ;
				}				
				else if(!empty($cityPostCodeArys['szRegion2']) && (substr(mb_strtolower($cityPostCodeArys['szRegion2'],'UTF-8'),0,$str_length)== mb_strtolower($szPostCode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion2'] ;
				}				
				else if(!empty($cityPostCodeArys['szRegion3']) && (substr(mb_strtolower($cityPostCodeArys['szRegion3'],'UTF-8'),0,$str_length)==$szPostCode))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion3'] ;
				}
				if(!empty($cityPostCodeArys['szRegion4']) && (substr(mb_strtolower($cityPostCodeArys['szRegion4'],'UTF-8'),0,$str_length)==$szPostCode))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion4'] ;
				}
				$countr++;
			}
			$resultingAry = array_unique ($final_ary , SORT_STRING) ;
			asort($resultingAry);
		}
		if(!empty($resultingAry))
		{
			foreach($resultingAry as $resultingArys)
			{
				echo $resultingArys."\n";
			}
		}	
	}
}
if($action == 'CITY_POST_CODE')
{
	$szPostCode = $szCityPostcode ;
	if(trim($szPostCode)==t($t_base.'fields/type_name'))
	{
		$szPostCode = '';
		return ;
	}
	if($idCountry>0)
	{
		$query_and = "  idCountry ='".(int)$idCountry."'";
	}
	else
	{
		return ;
	}

	if($default_action=='All' && $szPostCode=='')
	{

	}
	else if(!empty($szPostCode))
	{
		$query_and.= "
				AND
			(
				szPostCode LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity1 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity2 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity3 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity4 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity5 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity6 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity7 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity8 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity9 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szCity10 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szRegion1 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szRegion2 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szRegion3 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			OR
				szRegion4 LIKE '".mysql_escape_custom(trim($szPostCode))."%'
			)
		";
	}

	$query = "
		SELECT
			szCity,
			szCity1,
			szCity2,
			szCity3,
			szCity4,
			szCity5,
			szCity6,
			szCity7,
			szCity8,
			szCity9,
			szCity10,
			szRegion1,
			szRegion2,
			szRegion3,
			szRegion4,
			szPostCode
		FROM
			 ".__DBC_SCHEMATA_POSTCODE__."
			 USE INDEX (country_city_regions)
			 WHERE
			 $query_and
			 ORDER BY
			 iPriority DESC, szCity ASC
			 LIMIT
			 0,200
			 ";
	//echo $query ;
	if($result = $kConfig->exeSQL($query))
	{
		$ctr = 0;
		while($row = $kConfig->getAssoc($result))
		{
			$cityPostCodeAry[$ctr] = $row ;
			$ctr++;
		}
		if(!empty($cityPostCodeAry))
		{
			$str_length = strlen($szPostCode);
			$final_ary = array();
			$countr = 0;
			foreach($cityPostCodeAry as $cityPostCodeArys)
			{
				if((!empty($cityPostCodeArys['szPostCode']) && (substr(mb_strtolower($cityPostCodeArys['szPostCode'],'UTF-8'),0,$str_length)==mb_strtolower($szPostCode,'UTF-8'))))
				{
					$final_ary[$countr] = $cityPostCodeArys['szPostCode'] ;
					$countr++;
				}
				else
				{
					if(!empty($cityPostCodeArys['szCity']) && (substr(mb_strtolower($cityPostCodeArys['szCity'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity'] ;
					}
					else if(!empty($cityPostCodeArys['szCity1']) && (substr(mb_strtolower($cityPostCodeArys['szCity1'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity1'] ;
					}
					else if(!empty($cityPostCodeArys['szCity2']) && (substr(mb_strtolower($cityPostCodeArys['szCity2'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity2'] ;
					}
					else if(!empty($cityPostCodeArys['szCity3']) && (substr(mb_strtolower($cityPostCodeArys['szCity3'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity3'] ;
					}
					else if(!empty($cityPostCodeArys['szCity4']) && (substr(mb_strtolower($cityPostCodeArys['szCity4'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity4'] ;
					}
					else if(!empty($cityPostCodeArys['szCity5']) && (substr(mb_strtolower($cityPostCodeArys['szCity5'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity5'] ;
					}
					else if(!empty($cityPostCodeArys['szCity6']) && (substr(mb_strtolower($cityPostCodeArys['szCity6'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity6'] ;
					}
					else if(!empty($cityPostCodeArys['szCity7']) && (substr(mb_strtolower($cityPostCodeArys['szCity7'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity7'] ;
					}
					else if(!empty($cityPostCodeArys['szCity8']) && (substr(mb_strtolower($cityPostCodeArys['szCity8'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity8'] ;
					}
					else if(!empty($cityPostCodeArys['szCity9']) && (substr(mb_strtolower($cityPostCodeArys['szCity9'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity9'] ;
					}
					else if(!empty($cityPostCodeArys['szCity10']) && (substr(mb_strtolower($cityPostCodeArys['szCity10'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szCity10'] ;
					}
					else if(!empty($cityPostCodeArys['szRegion1']) && (substr(mb_strtolower($cityPostCodeArys['szRegion1'],'UTF-8'),0,$str_length) == mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szRegion1'] ;
					}
					else if(!empty($cityPostCodeArys['szRegion2']) && (substr(mb_strtolower($cityPostCodeArys['szRegion2'],'UTF-8'),0,$str_length)== mb_strtolower($szPostCode,'UTF-8')))
					{
						$final_ary[$countr] = $cityPostCodeArys['szRegion2'] ;
					}
					else if(!empty($cityPostCodeArys['szRegion3']) && (substr(mb_strtolower($cityPostCodeArys['szRegion3'],'UTF-8'),0,$str_length)==$szPostCode))
					{
						$final_ary[$countr] = $cityPostCodeArys['szRegion3'] ;
					}
					if(!empty($cityPostCodeArys['szRegion4']) && (substr(mb_strtolower($cityPostCodeArys['szRegion4'],'UTF-8'),0,$str_length)==$szPostCode))
					{
						$final_ary[$countr] = $cityPostCodeArys['szRegion4'] ;
					}
					/*
					 if(!empty($cityPostCodeArys['szPostCode']) && !empty($final_ary[$countr]))
					 {
					if(strpos($final_ary[$countr],"/"))
					{
					$final_ary[$countr] = str_replace("/","|",$final_ary[$countr]);
					}
					$final_ary[$countr] .= '/'.$cityPostCodeArys['szPostCode'] ;
					}
					*/
					$countr++;
				}
				$resultingAry = array_unique ($final_ary , SORT_STRING) ;
				asort($resultingAry);
			}
			if(!empty($resultingAry))
			{
				foreach($resultingAry as $resultingArys)
				{
					echo $resultingArys."\n";
				}
			}
		}
	}
}