<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
constantApiKey();
$address_line=$_REQUEST['szCity'];
//echo $address_line."test";
if(trim($address_line)=='')
{
	$szLatitude=$_REQUEST['szLatitude'];
	$szLongitude=$_REQUEST['szLongitude'];
	$flag = $_REQUEST['flag'];	
}
$flag = $_REQUEST['flag'];
//print_r($_REQUEST);
if((int)$_REQUEST['radius']==0)
{
	$radius=__MINI_RADIUS__;
}
else
{
	$radius=$_REQUEST['radius'];
}
$kWHSSearch = new cWHSSearch();
$maxRadius=$kWHSSearch->getManageMentVariableByDescription('__MAX_RADIUS_CIRCLE__');
//print_r($_REQUEST);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    	<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>		
		<script src="<?php echo __GOOGLE_MAP_JSON_URL__;?>" type="text/javascript"></script>
		<script type="text/javascript">
/**
 * map
 */
var map;
var bounds = new google.maps.LatLngBounds; //Circle Bounds
geocoder = new google.maps.Geocoder();
var thePoint = new google.maps.LatLng('<?=$szLatitude?>','<?=$szLongitude?>');
var  circle;
var radius = <?=$radius?>; //1 km
var min_radius = 0; //0.5km
var max_radius = <?=$maxRadius?>; //5km

function initialize() 
{

//var thePoint = new google.maps.LatLng(60.17, 24.94);
var mapOpts = {
  zoom: 2,
	zoomControl: true,
	center: thePoint,
	scaleControl: true,
    overviewMapControl: true,
    scrollwheel: true,
    disableDoubleClickZoom: true,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	zoomControlOptions: {
              style: google.maps.ZoomControlStyle.LARGE,
              position: google.maps.ControlPosition.LEFT_CENTER
          }
};
 map = new google.maps.Map(document.getElementById("map_canvas"), mapOpts);
<?
		if($flag =='ADD')
		{
		?>
			map.setCenter(thePoint,4);
		<?
		}
		else
		{
	?>
	        addCircleCenterMarker(thePoint);
	        addCircleResizeMarker(thePoint,radius);
	        drawCircle(thePoint, radius);
        <? } if($address_line!='') {?>
	showAddress();
	<? } ?>
	
}

/**
 * circle
 */



// selector function
function drawCircle(point,radius){ 
	//alert(map.getCenter());
   circle = new google.maps.Circle({	
  	  strokeColor:"#FFFFFF",
      strokeOpacity: 1,
      strokeWeight: 1,
      fillColor: "#00FF00",
      fillOpacity: 0.2});	    	
   circle.setRadius(radius * 1000);
   circle.setCenter(point);
   map.fitBounds(circle.getBounds());
   map.circleRadius = radius;
   circle.setMap(map);
   
   document.getElementById("radious").value = radius;
   window.parent.document.getElementById('iDistance').value = Math.ceil(radius);
	
   document.getElementById("latlonbox").value = point.lat()+','+point.lng();
   window.parent.document.getElementById('szLatLong').value = point.lat()+','+point.lng();
}

function showAddress() 
{
	var address = document.getElementById("address").value;
	var radius_val = document.getElementById("radious").value;
	//alert(radius_val);
	if(address=='')
	{
		alert("Please enter city name");
		document.getElementById("address").focus();
		return false;
	}
	if(isNaN(radius_val))
	{
		alert("Please enter radius");
		document.getElementById("radious").focus();
		return false;
	}
	var radius = parseFloat(radius_val);
	//alert(radius);
	 
	 if (geocoder) {
		 geocoder.geocode({ 'address': address }, function (results, status) {
		    if (status == google.maps.GeocoderStatus.OK) {    
		        
		        //marker.setMap(map);       
		        var point = results[0].geometry.location;
		      	posset = 1;
		      	//alert(point.lng());
		 		//map.clearOverlays();
		 		//map.setMapType(G_HYBRID_MAP);
		 		thePoint = new google.maps.LatLng(point.lat(),point.lng());
				map.setCenter(point,8);
		 		zm = 1;
		 		addCircleCenterMarker(point);
		 		addCircleResizeMarker(point,radius);
		 		drawCircle(point,radius);
		 		
		    }
		    else
		    {
		    	alert(address + " not found");
		    }
		});
		//alert(markersArray);
		 }
}



function addCircleCenterMarker(point) 
{
	//alert(point);
    var markerOptions = {position:point,map:map, icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/blue-marker.png'?>', draggable: false };
    CircleCenterMarker = new google.maps.Marker(markerOptions);
    CircleCenterMarker.setMap(map);
}
function show_address_on_submit(kEvent)
{
	var temp=(window.event)?kEvent.keyCode:kEvent.which;
	if(temp == 13)
	{
		showAddress();
	}
}

// Adds Circle Resize marker
function addCircleResizeMarker(point,radius) 
{
	//alert(radius)
	radiusInKm=radius;
	var pointB = point.destinationPoint(90, radiusInKm);
	//alert(pointB);
    //var resize_icon = new google.maps.MarkerImage(redpin);
    //resize_icon.maxHeight = 0;
    var properties = {
        position: pointB,
        map: map,
        draggable: true,
        icon: '<?=__MAIN_SITE_HOME_PAGE_URL__.'/images/resize.png'?>'
	};
	
	var CircleResizeMarker = new google.maps.Marker(properties);
	CircleResizeMarker.setMap(map);
   // google.maps.addOverlay(CircleResizeMarker); //Add marker on the map
   google.maps.event.addListener(CircleResizeMarker, 'dragstart', function() { //Add drag start event
        circle_resizing = true;
    });
    google.maps.event.addListener(CircleResizeMarker, 'drag', function(point) { //Add drag event
    	var point=CircleResizeMarker.getPosition();
    	//alert(point)
    	//CircleResizeMarker.setMap(null);
    	var new_radius = point.distanceFrom(thePoint);
		new_radius=(new_radius/1000);
        if (new_radius < min_radius) new_radius = min_radius;
        if (new_radius > max_radius) new_radius = max_radius;
        circle.setMap(null);
       // addCircleCenterMarker(thePoint);
       // addCircleResizeMarker(thePoint,new_radius);
        drawCircle(thePoint, new_radius);
        
    });
    google.maps.event.addListener(CircleResizeMarker, 'dragend', function(point) { //Add drag end event
        circle_resizing = false;
        var point=CircleResizeMarker.getPosition();
       // alert(thePoint);
       CircleResizeMarker.setMap(null);
       	var new_radius = point.distanceFrom(thePoint);
       //	alert(new_radius);
		new_radius=(new_radius/1000);
		//alert(new_radius);
        if (new_radius < min_radius) new_radius = min_radius;
        if (new_radius > max_radius) new_radius = max_radius;
        circle.setMap(null);
       // addCircleCenterMarker(thePoint);
        addCircleResizeMarker(thePoint,new_radius);
        drawCircle(thePoint,new_radius);
        
    });
}
Number.prototype.toRad = function() {
   return this * Math.PI / 180;
}

Number.prototype.toDeg = function() {
   return this * 180 / Math.PI;
}

google.maps.LatLng.prototype.destinationPoint = function(brng, dist) {
   dist = dist / 6371;  
   brng = brng.toRad();  

   var lat1 = this.lat().toRad(), lon1 = this.lng().toRad();

   var lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) + 
                        Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));

   var lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) *
                                Math.cos(lat1), 
                                Math.cos(dist) - Math.sin(lat1) *
                                Math.sin(lat2));

   if (isNaN(lat2) || isNaN(lon2)) return null;

   return new google.maps.LatLng(lat2.toDeg(), lon2.toDeg());
}

google.maps.LatLng.prototype.distanceFrom = function(latlng) {
  var lat = [this.lat(), latlng.lat()]
  var lng = [this.lng(), latlng.lng()]
  var R = 6378137;
  var dLat = (lat[1]-lat[0]) * Math.PI / 180;
  var dLng = (lng[1]-lng[0]) * Math.PI / 180;
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
  Math.cos(lat[0] * Math.PI / 180 ) * Math.cos(lat[1] * Math.PI / 180 ) *
  Math.sin(dLng/2) * Math.sin(dLng/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;
  return Math.round(d);
}
</script>
</head>
<body onload="initialize()">
<div id="map_canvas" style="width:100%; height:360px"></div>
 <form action="javascript:void();" methos="post" >    
    <input type="hidden" onkeyup="show_address_on_submit(event)" size="30" style="width:200px;" id="address" name="address" value="<?=$address_line?>" />
    <input type="hidden" onkeyup="show_address_on_submit(event)" size="30" style="width:200px;" id="radious" name="radious" value="<?=$radius?>" />
    <input type="hidden"  size="30" style="width:200px;" id="latlonbox" name="latlonbox" value="" />
 </form> 
</body> 
</html>