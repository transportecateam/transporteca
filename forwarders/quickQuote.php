<?php
/**
 * Forwarder My Company  
 */
 ob_start();
session_start();
$szMetaTitle="Transporteca | Quick Quote";
$display_profile_not_completed_message = true;
$iQuickQuotPage=1;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php");

$forwarderProfileContactRole= $kForwarder->adminEachForwarder($_SESSION['forwarder_user_id']);
if((int)$_SESSION['forwarder_admin_id']==0 && $forwarderProfileContactRole != __ADMINISTRATOR_PROFILE_ID__ && $forwarderProfileContactRole != __PRICING_PROFILE_ID__){
    $redirect_url=__FORWARDER_MY_ACCOUNT_URL__;
    header('Location:'.$redirect_url);
    die();
}
checkAuthForwarder();
$t_base = "BulkUpload/";
$idForwarder =$_SESSION['forwarder_id'];
$kConfig=new cConfig();
$number = 1;
$kBooking = new cBooking();
$_SESSION['try_it_out_searched_data']  = array();
unset($_SESSION['try_it_out_searched_data']); 
 


constantApiKey();
?> 
<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>
 
<?php 
$kWhsSearch = new cWHSSearch();  
$arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];
$szLanguage = "en"; 

$languageAry = array();
$languageAry = $kConfig->getLanguageDetails(false,false,false,false,false,true); 

$kCourierServices = new cCourierServices();
$dtdTradeStr=$kCourierServices->getAllDTDTradesList(true);

$iLanguage = 1;
$allCountriesArr = array();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
<script type="text/javascript"> 
var transportecaCountryListAry = new Array(); 
var CountryLanguageListAry = new Array();
var dtdTradeStr = '<?php echo $dtdTradeStr;?>';
<?php
    if(!empty($allCountriesArr))
    {
        foreach($allCountriesArr as $allCountriesArrs)
        {
            ?>
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>'] = new Array();
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>']['id'] = '<?php echo $allCountriesArrs['id'] ?>';
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>']['idCurrency'] = '<?php echo $allCountriesArrs['iDefaultCurrency'] ?>';
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>']['iInternationDialCode'] = '<?php echo $allCountriesArrs['iInternationDialCode'] ?>';
            <?php
        }
    }
    if(!empty($languageAry))
    {
        foreach($languageAry as $languageArys)
        {
            $iLanguage = $languageArys['id'];
            $allCountriesArr = array();
            $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage); 
            ?>
            CountryLanguageListAry['<?php echo $iLanguage ?>'] = new Array();
            <?php
            if(!empty($allCountriesArr))
            {
                foreach($allCountriesArr as $allCountriesArrs)
                {
                    ?> 
                    CountryLanguageListAry['<?php echo $iLanguage ?>']['<?php echo $allCountriesArrs['id'] ?>'] = new Array();
                    CountryLanguageListAry['<?php echo $iLanguage ?>']['<?php echo $allCountriesArrs['id'] ?>']['szCountryName'] = '<?php echo addslashes($allCountriesArrs['szCountryName']) ?>'; 
                    <?php
                }
            }  
        }
    }
?>    
</script>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="Transportation_pop" style="display:none;"></div>
<div id="dang-cargo-pop" class="help-pop right">&nbsp;</div>
<div id="customs_clearance_pop_right" class="help-pop right">&nbsp;</div> 

<div id="hsbody">  
    <div id="customs_clearance_pop" class="help-pop"></div>  
        <div id="tryitout_search_form_container">
            <?php echo display_tryitout_search_form('',false,true); ?>
        </div>
        <div class="clear-all"></div>
        <div id="forwarder_try_it_out_search_result">
            <?php
                //echo display_forwarder_try_it_out_search_result($searchResultAry,$kQuote);
            ?>
        </div>
        <div id="quick_quote_booking_infomation_main_container" class="font-12">
        <div class="clearfix accordion-container">
            <span class="accordian" style="text-transform:none;">
            <a id="pending_task_overview_open_close_link" onclick="close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1);" class="open-icon" onclick="" href="javascript:void(0);"></a>
            Booking Information
            </span>
        </div>
        <div id="quick_quote_booking_infomation_container" class="forwarder-quick-quote">
                <?php echo display_quick_quote_shipper_consignee($bookingDataAry,$mode,false,false,true); ?>
        </div>
    </div> 
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>