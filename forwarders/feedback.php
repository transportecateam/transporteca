<?php
ob_start();
if (!isset($_SESSION)) 
{
  session_start();
}
$szMetaTitle="Transporteca | Forwarder Company";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php");
checkAuthForwarder();
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
$objPHPExcel=new PHPExcel();

$szFeedbackArr30=$kForwarder->forwarderFeedback(30,$idForwarder);
$szFeedbackArr90=$kForwarder->forwarderFeedback(90,$idForwarder);
$szFeedbackArr365=$kForwarder->forwarderFeedback(365,$idForwarder);
$szFeedbackArrLt=$kForwarder->forwarderFeedback(0,$idForwarder);
$szFeedbackreview=$kForwarder->forwarderReviewSugg('szReview',$idForwarder,"LIMIT 0,10");
$szFeedbackSuggestion=$kForwarder->forwarderReviewSugg('szSuggestion',$idForwarder,"LIMIT 0,10");
$t_base = "Feedback/";
//$data=$kForwarder->downloadFeedbackHistory('1');
if(isset($szFeedbackArr30))
{
    foreach( $szFeedbackArr30 as $key=> $value )
    {
        if($value!=0 && $value!='')
        {
                $downloadLink = 1;
        }
        else
        {
                $downloadLink = 0;
        }
    }

}
?>
<div id="document"></div>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="hsbody">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="format-5" style="border-bottom-color:#FFFFFF;border-top-color:#FFFFFF;">
				<thead>
					<tr>
						<td width="45%" style="border-left-color:#FFFFFF;background-color:#FFFFFF;border-top:#FFFFFF;"><h4><strong><?=t($t_base."title/Feedback_history")?></strong> <?php if(isset($downloadLink) && $downloadLink!=0 ){?> <a href="javascript:void(0);"  onclick="download_history_review()"  class="f-size-12"><?=t($t_base."heading/download_data")?></a><?php }?></strong></th>
						<td width="15%" style="border-left-color:#FFFFFF;background-color:#FFFFFF;border-top:#FFFFFF;">&nbsp;</th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/30")?></th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/90")?></th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/365")?></th>
						<td width="10%" align="center" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/lt")?></th>
					</tr>
				</thead>
				<tr>
					<td rowspan="3" valign="top" style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."heading/shipment_available_time")?></td>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/yes")?></td>
					<td align="center"><?if($szFeedbackArr30['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr30['timelyYesPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr90['timelyYesPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr365['timelyYesPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['timelyYesPct']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/no")?></td>
					<td align="center"><?if($szFeedbackArr30['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr30['timelyNoPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr90['timelyNoPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArr365['timelyNoPct']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalTimely']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['timelyNoPct']."%";}?></td>
				</tr>
				<tr>
					<td style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."fields/count")?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr30['totalTimely'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr90['totalTimely'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr365['totalTimely'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArrLt['totalTimely'];?></td>
				</tr>
				<tr>
					<td rowspan="3" valign="top" style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."heading/customer_service_prompt_courteous")?></td>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/yes")?></td>
					<td align="center"><? if($szFeedbackArr30['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr30['corteousYesPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr90['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr90['corteousYesPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr365['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr365['corteousYesPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArrLt['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['corteousYesPct']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/no")?></td>
					<td align="center"><? if($szFeedbackArr30['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr30['corteousNoPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr90['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr90['corteousNoPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArr365['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArr365['corteousNoPct']."%";}?></td>
					<td align="center"><? if($szFeedbackArrLt['totalCourtous']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['corteousNoPct']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;background-color:#FFFFFF;"><?=t($t_base."fields/count")?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr30['totalCourtous'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr90['totalCourtous'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr365['totalCourtous'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArrLt['totalCourtous'];?></td>
				</tr>
				<tr>
					<td rowspan="4" valign="top" style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."heading/Rating_history")?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/ratings_history');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')"></td>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/positive")?></td>
					<td align="center"><?if($szFeedbackArr30['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr30['positive']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr90['positive']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr365['positive']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['positive']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/neutral")?></td>
					<td align="center"><?if($szFeedbackArr30['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr30['neutral']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr90['neutral']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr365['neutral']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['neutral']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/negative")?></td>
					<td align="center"><?if($szFeedbackArr30['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr30['negative']."%";}?></td>
					<td align="center"><?if($szFeedbackArr90['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr90['negative']."%";}?></td>
					<td align="center"><?if($szFeedbackArr365['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArr365['negative']."%";}?></td>
					<td align="center"><?if($szFeedbackArrLt['totalRating']=='0'){echo "N/A";}else{echo $szFeedbackArrLt['negative']."%";}?></td>
				</tr>
				<tr>
					<td style="border-left-color:#FFFFFF;background-color:#FFFFFF;"><?=t($t_base."fields/count")?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr30['totalRating'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr90['totalRating'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArr365['totalRating'];?></td>
					<td align="center" style="background-color:#FFFFFF;"><?=$szFeedbackArrLt['totalRating'];?></td>
				</tr>
			</table>
			<br />
			
			<div class="oh">
				<div class="fl-40" style="width:43%;">
					<h4><strong><?=t($t_base."title/Recent_reviews")?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/recent_reviews_header');?>','<?=t($t_base.'messages/recent_reviews');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><?php if(!empty($szFeedbackreview)){?> <a href="javascript:void(0);" onclick="download_suggestion_reviews('szReview');"  class="f-size-12"><?=t($t_base."heading/download_data")?></a><?php } ?></h4>
					<div class="feedback-box">
					<? 
					foreach($szFeedbackreview as $key=>$value)
					{	
						echo "<p>".$value['reviewDate']." - ".$value['iRating']." stars: \"".$value['szReview']."\"</p>";
						
					}
					?>
					</div>
				</div>
				
				<div class="fr-55">
					<h4><strong><?=t($t_base."title/Recent_suggestions_for_improvements")?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/suggestions_for_improvements_header');?>','<?=t($t_base.'messages/suggestions_for_improvements');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a> <?php if(!empty($szFeedbackSuggestion)){?><a href="javascript:void(0);" onclick="download_suggestion_reviews('szSuggestion');"  class="f-size-12"><?=t($t_base."heading/download_data")?></a><?php } ?></h4>
					<div class="feedback-box">
					<? 
					foreach($szFeedbackSuggestion as $key=>$value)
					{	
						echo "<p>".$value['reviewDate']."- \"".$value['szSuggestion']."\", ".$value['szFirstName']." ".$value['szLastName']."</p>";
						
					}
					?>
					</div>
				</div>				
			</div>
		</div>
<?php
	include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>