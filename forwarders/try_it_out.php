<?php
/**
 * Forwarder My Company  
 */
 ob_start();
session_start();
$szMetaTitle="Transporteca | Test how Services and Rates look for a Customer";
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php");
checkAuthForwarder();
$t_base = "BulkUpload/";
$idForwarder =$_SESSION['forwarder_id'];
$kConfig=new cConfig();
$number = 1;
$kBooking = new cBooking();
$_SESSION['try_it_out_searched_data']  = array();
unset($_SESSION['try_it_out_searched_data']); 
 
constantApiKey();
?> 
<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>
 
<?php 
$kWhsSearch = new cWHSSearch();  
$arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];
$szLanguage = "en"; 
?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="Transportation_pop" style="display:none;"></div>
<div id="dang-cargo-pop" class="help-pop right">&nbsp;</div>
<div id="customs_clearance_pop_right" class="help-pop right">&nbsp;</div> 

<div id="hsbody-2">  
    <div id="customs_clearance_pop" class="help-pop"></div> 
    <div class="hsbody-2-left account-links">
        <?php 
            require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" );  
        ?>
    </div> 
    <div class="hsbody-2-right">
        <div id="tryitout_search_form_container">
            <?php echo display_tryitout_search_form(); ?>
        </div>
        <div class="clear-all"></div>
        <div id="forwarder_try_it_out_search_result">
            <?php
                //echo display_forwarder_try_it_out_search_result($searchResultAry,$kQuote);
            ?>
        </div>
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>