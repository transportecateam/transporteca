<?php
/**
 * Forwarder My Company  
 */
 ob_start();
session_start();
$szMetaTitle="Transporteca Control Panel - Forwarder's Log-in";
$szMetaKeywords = "lcl, bookings, booking, leading, world, transport, control, panel, pricing";
$szMetaDescription="Freight forwarders upload best services and rates to the Transporteca Control Panel - The World's Leading Site for LCL Bookings. Customer booking and pay online";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$iForwarderLoginPage = 1;
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );

$t_base = "ForwardersCompany/UserAccess/";
$kForwarderContact= new cForwarderContact();

if(!empty($_POST['loginArr']))
{
    $_POST['loginArr']['szControlPanelUrl'] = $_SERVER['HTTP_HOST'];

    if($kForwarderContact->forwarderLogin($_POST['loginArr']))
    {
        $idForwarder =$_SESSION['forwarder_user_id']; 
        if((int)$_SESSION['verified_email_user']>0 && (int)$_SESSION['forwarder_user_id']>0)
        {
            $redirect_url=__BASE_URL__.'/home/';
            if((int)$_SESSION['verified_email_user']==(int)$_SESSION['forwarder_user_id'])
            {
                $_SESSION['valid_confirmation']='1';
            }
            else
            {
                $_SESSION['valid_confirmation']='2';
            }
        }
        if(isset($_SESSION['redirect_uri']) && $_SESSION['redirect_uri']!='')
        {
            $redirect_url = __FORWARDER_HOME_PAGE_URL__."".sanitize_all_html_input($_SESSION['redirect_uri']);
            unset($_SESSION['redirect_uri']);
            header("Location:".$redirect_url);
            exit;
        }		
        
        $kForwarder_new = new cForwarder();
        $kForwarder_new->load($_SESSION['forwarder_id']);

        if($kForwarder_new->iProfitType==__FORWARDER_PROFIT_TYPE_MARK_UP__)
        {
            $redirect_url = __FORWARDER_PENDING_QUOTES_URL__ ; 
            ob_end_clean();
            header("Location:".$redirect_url);
            exit();
        } 
        else if((int)$_SESSION['forwarder_booking_confirmation_key']>0)
        {
            ob_end_clean();
            header("Location:".__FORWARDER_BOOKING_URL__);
            exit;
        }
        else
        { 
            $redirect_url=__FORWARDER_DASHBOARD_URL__; 
            ob_end_clean();
            header("Location:".$redirect_url);
            exit();
        }		
    }
}
else if(!empty($_COOKIE['__FORWARDR_EMAIL_COOKIE__']))
{
    $_POST['loginArr']['szEmail'] = $_COOKIE['__FORWARDR_EMAIL_COOKIE__'] ;
}
if((int)$_SESSION['forwarder_user_id']>0)
{
	 $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']); 
         
         $kForwarder_new = new cForwarder();
         $kForwarder_new->load($_SESSION['forwarder_id']);
         
         if($kForwarder_new->iProfitType==__FORWARDER_PROFIT_TYPE_MARK_UP__)
         {
            $redirect_url = __FORWARDER_PENDING_QUOTES_URL__ ;
         }
         else
         {
            $redirect_url=__FORWARDER_DASHBOARD_URL__;
         }
         ob_end_clean();
	 header("Location:".$redirect_url);
	 exit(); 
}

?>
<script type="text/javascript">
$().ready( function (){
	var cookie_name = getCookie('__FORWARDR_EMAIL_COOKIE__');
	
	if(jQuery.trim(cookie_name)=='' || jQuery.trim(cookie_name)== 'undefined')
	{
		$("#szEmail").focus().selectionEnd;
	}
	else
	{
		//$("#szEmail").focus().selectionEnd;
		$("#szPassword").focus().selectionEnd;
	}
});
</script>
<div id="hsbody" style="min-height:450px;">

<form id="forwarder_login_form"  method="post">
	<div align="center">
		<br/>
		<p style="font-style:normal;font-size:28px;"><?=t($t_base.'messages/login_page_title');?> </p><br/>
		<p style="font-style:normal;font-size:22px;"><?=t($t_base.'messages/login_page_title_2');?></h3><br/><br/>
		<div align="center" class="forwarder_login">		
		<?php
			if(!empty($kForwarderContact->arErrorMessages)){
			?>
			<div align="left" id="regError" class="errorBox ">
                            <div class="header"><?=t($t_base.'fields/please_following');?></div>
                            <div id="regErrorList">
                                <ul>
                                <?php   
                                    foreach($kForwarderContact->arErrorMessages as $key=>$values)
                                    {
                                        ?><li><?=$values?></li><?php	
                                    }
                                ?>
                                </ul>
                            </div>
			</div>
			<?php }	?>
			
			<div class="oh">
				<p class="fl-63" align="left"><?=t($t_base.'messages/please_sign_in')?></p>
				<p class="fl-35">&nbsp;</div>
			<div class="oh">
				<p class="fl-33" align="left">&nbsp;<?=t($t_base.'fields/signin_email')?></p>
				<p class="fl-65" align="right"><input type="text" id="szEmail" name="loginArr[szEmail]" value="<?=$_POST['loginArr']['szEmail']?>" tabindex="1"/></p>
			</div>
			<div class="oh">
				<p class="fl-33" align="left">&nbsp;<?=t($t_base.'fields/password')?></p>
				<p class="fl-65" align="right"><input type="password" id="szPassword" name="loginArr[szPassword]" tabindex="2" onkeyup="on_enter_key_press(event,'forwarder_login_form');"/></p>
			</div>
			<p align="right"><a href="javascript:void(0)" onclick="open_forgot_passward_popup('ajaxLogin')" class="f-size-12"><?=t($t_base.'links/forgot_pass')?></a></p>
			<br style="line-height: 16px;" />
			<p align="center" style="padding-left:25px;">
				<a href="javascript:void(0)" class="button1" onclick="$('#forwarder_login_form').submit()" tabindex="7"><span><?=t($t_base.'fields/sign_in')?></span></a>
			</p><br/>
			<p align="right">
                            <a href="javascript:void(0)" onclick="showHide('what_is_this_description');" class="f-size-12"><?=t($t_base.'links/what_is_this')?></a>
                            <br/><a href="<?=__HOME_PAGE_URL__?>" class="f-size-12" tabindex="3"><?=t($t_base.'links/take_me_to_ransporteca')?></a>
                            <br/>
			</p>			
                    </div>			
		</div>	
		
		<div id="what_is_this_description" style="border: 0px solid #b1aa87;display:none;">
		<br>
		<br>
		<p>
                    <?=t($t_base.'messages/what_is_this_popup_message1');?>:&nbsp;
                    <a href="<?=__MAIN_SITE_HOME_PAGE_URL__?>/explain" style="font-size:16px;"><?=substr(__MAIN_SITE_HOME_PAGE_URL__."/explain",8)?></a>
                    <br>
                    <br>
                    <?=t($t_base.'messages/what_is_this_popup_message2');?> <a href="<?=__MAIN_SITE_HOME_PAGE_URL__?>/forwarder" style="font-size:16px;"><?=substr(__MAIN_SITE_HOME_PAGE_URL__."/forwarder",8)?></a>
                    <br>
                    <?php //t($t_base.'messages/what_is_this_popup_message3');?>
                    <?php //t($t_base.'messages/what_is_this_popup_message4');?>
		</p>
            </div>
</form>	
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>