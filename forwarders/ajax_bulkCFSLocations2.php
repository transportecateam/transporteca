<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$iWarehouseType = (int)$_SESSION['iWarehouseType'];  
if($iWarehouseType==2)
{
    $szMetaTitle = "Transporteca | Airport Warehouse Locations";
}
else
{
    $szMetaTitle = "Transporteca | CFS Locations";
} 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );

$iWarehouseType = (int)$_SESSION['iWarehouseType']; 
if((isset($_SESSION['option']) && $_SESSION['option']== 'cfsUpload') || (isset($_SESSION['mode']) && $_SESSION['mode']== 'cfsUpload'))
{
    $idBatch = (int)sanitize_all_html_input($_SESSION['idBatch']);
    
    if(isset($_SESSION['option']))
    {
        unset($_SESSION['option']);
        $_SESSION['mode']='cfsUpload';	
    }
    checkAuthForwarder();
    $t_base = "BulkUpload/";
    $t_base_error="Error";
    $idForwarderContact = $_SESSION['forwarder_user_id'];
?>
    <div id="hsbody-2">
        <div class="hsbody-2-left account-links">
            <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
        </div>
        <div class="hsbody-2-right input29">
            <?php require_once( __APP_PATH__ ."/forwarders/CFSBulkUploadUpdate.php" ); ?>
        </div>
        <input type="hidden" name="iScrollValue" value="" id="iScrollValue" />
        <input type="hidden" name="iScrollDivValue" value="" id="iScrollDivValue" />
        <form method="post" id="google_map_hidden_form" action="<?=__FORWARDER_HOME_PAGE_URL__?>/googleMap.php" target="google_map_target_1">
            <input type="hidden" name="cfsHiddenAry[szPostCode]" id="szPostCode_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szAddressLine1]" id="szAddressLine1_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szAddressLine2]" id="szAddressLine2_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szAddressLine3]" id="szAddressLine3_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szCity]" id="szCity_hidden" value=""> 
            <input type="hidden" name="cfsHiddenAry[szState]" id="szState_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szCountry]" id="szCountry_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szLatitude]" id="szLatitude_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szLongitude]" id="szLongitude_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[iWarehouseType]" id="iWarehouseType_hidden" value="<?php echo $iWarehouseType; ?>">
            <input type="hidden" name="cfsHiddenAry[iBulkUploadAprroveFlag]" id="iBulkUploadAprroveFlag_hidden" value="1"> 
        </form> 
        <!-- style="width:600px;height:380px;border:0px solid #fff;" -->
        <div id="popup_container_google_map" style="display:none;">
            <div id="popup-bg"></div>
            <div id="popup-container">
                <div class="popup" style="width:720px;margin-top:30px;">
                    <p class="close-icon" align="right">
                        <a href="javascript:void(0);" onclick="window.top.window.hide_google_map();">
                            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                        </a>
                    </p>
                    <iframe id="google_map_target_1" class="google_map_select"  style="width:720px;"  scrolling="no" name="google_map_target_1" src="#" >
                    </iframe>
                </div>
            </div>
        </div>
    </div>
<?php  
    include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );  
}
else
{	
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        ob_end_clean();
        header("Location:".__BASE_URL__.'/airportWarehouseLocationBulk/');
        die;
    }
    else
    {
        ob_end_clean();
        header("Location:".__BASE_URL__.'/CFSLocationBulk/');
        die;
    } 
}
?>		
			