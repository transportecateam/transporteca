<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
checkAuthForwarder();

 header("Content-disposition: filename=Forwarders_Guide_to_Transporteca.pdf"); 	
 header('Content-type: application/pdf');
 readfile(__APP_PATH__."/forwarders/guide/Forwarders_Guide_to_Transporteca.pdf");
 ?>