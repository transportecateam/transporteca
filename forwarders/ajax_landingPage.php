<?php
ob_start();
session_start();
 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$kConfig=new cConfig();
$kBooking=new cBooking();
$kWHSSearch = new cWHSSearch();
$mode = $_REQUEST['mode'];
if($mode == 'SHOW_PRICE_DETAILS')
{
	$idWTW = sanitize_all_html_input(trim($_REQUEST['idWTW'])); 
	if($idWTW>0)
	{
		$tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,$_SESSION['forwarder_temp_search_id']);
		$searchResultAry = unserialize($tempResultAry['szSerializeData']);
		
		if(!empty($searchResultAry))
		{
			$updateBookingAry = array();
			foreach($searchResultAry as $searchResultArys)
			{
				if($searchResultArys['unique_id'] == $idWTW )
				{
					$updateBookingAry = $searchResultArys ;
					break;
				}
			}
		}
		$t_base = "TryItOut/";
		echo '<div>';
		echo display_dtd_forwarder_price_details($updateBookingAry,$t_base);
		echo "</div>";
	}	
}
else if(!empty($_POST['multiregionCityAry']))
{
	$t_base = "SelectService/";
	$res_ary=array();

	if(((int)$_POST['multiregionCityAry']['iRegionFrom']>0) && ((int)$_POST['multiregionCityAry']['szRegionFrom']<=0))
	{
		$error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['multiregionCityAry']['iRegionTo']>0) && ((int)$_POST['multiregionCityAry']['szRegionTo']<=0))
	{
		$error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
		echo "ERROR||||".$error_message;
		die;
	}
	else
	{
		$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
		
		if((int)$_POST['multiregionCityAry']['szRegionFrom']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionFrom'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idOriginPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szOriginPostCode']= $kConfig->szPostCode;
			$postSearchAry['szOriginCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fOriginLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fOriginLongitude'] = $kConfig->szPostcodeLongitude;		
		}
		if((int)$_POST['multiregionCityAry']['szRegionTo']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionTo'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idDestinationPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szDestinationPostCode']= $kConfig->szPostCode;
			$postSearchAry['szDestinationCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;		
		}
	
		$kConfig = $_SESSION['config_object'] ;
		// converting cargo details into cubic meter 			
		$counter_cargo = count($kConfig->iLength);
		for($i=1;$i<=$counter_cargo;$i++)
		{
			$fLength = $kConfig->iLength[$i] ;
			$fWidth = $kConfig->iWidth[$i] ;
			$fHeight = $kConfig->iHeight[$i] ;
			$iQuantity = $kConfig->iQuantity[$i] ;
			if($kConfig->idCargoMeasure[$i]==1)  // cm
			{
				$fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
			}
			else
			{
				$fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
				if($fCmFactor>0)
				{
					$fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
				}	
			}
		}
	
		$weight_cargo = count($kConfig->iWeight);
		for($i=1;$i<=$weight_cargo;$i++)
		{
			// converting cargo details into cubic meter 
			if($kConfig->idWeightMeasure[$i]==1)  // kg
			{
				$fTotalWeight += ($kConfig->iWeight[$i]);
			}
			else
			{
				$fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
				if($fKgFactor>0)
				{
					$fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
				}	
			}
		}
		
		$idForwarder =$_SESSION['forwarder_id'];
		$kForwarder = new cForwarder();
		$kForwarder->load($idForwarder);
		
		$postSearchAry['fCargoVolume'] = $fTotalVolume ;
		$postSearchAry['fCargoWeight'] = $fTotalWeight ;
		$postSearchAry['forwarder_id'] = $idForwarder ;
		$postSearchAry['dtTimingDate'] = $kConfig->dtTiming ;
		$postSearchAry['idOriginCountry'] = $kConfig->szOriginCountry ;
		$postSearchAry['idDestinationCountry'] = $kConfig->szDestinationCountry ;
		$postSearchAry['iOriginCC'] = $kConfig->iOrigingCC ;
		$postSearchAry['iDestinationCC'] = $kConfig->iDestinationCC ;
		
		//$kForwarder->szCurrency = 1 ;
		//echo $kForwarder->szCurrency;
		if($kForwarder->szCurrency==1)
		{
			$postSearchAry['idCurrency'] = 1 ;
			$postSearchAry['szCurrency'] = 'USD' ;
			$postSearchAry['fExchangeRate'] = 1 ;
		}
		else
		{
			$kWhsSearch = new cWHSSearch();
			$currencySearchAry = $kWhsSearch->getCurrencyDetails($kForwarder->szCurrency);
								
			//$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
			//$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
			//$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
				
			$postSearchAry['idCurrency'] = $currencySearchAry['idCurrency'];
			$postSearchAry['szCurrency'] = $currencySearchAry['szCurrency']; ;
			$postSearchAry['fExchangeRate'] = $currencySearchAry['fUsdValue'];
		}
		
		$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'FORWARDER_TRY_IT_OUT');
		$_SESSION['try_it_out_searched_data'] = $postSearchAry;
		echo "SUCCESS ||||";
		?>
		<div style="border:1px solid #D0D0D0;">
		<?php
		//echo count($searchResultAry);
		//echo "service type ".$postSearchAry['idServiceType'] ;
		if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
		{
			display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
		{
			display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
		{
			display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
		{
			display_wtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		echo '</div>';
		die;
	}
}
else if(!empty($_POST['multiEmptyPostcodeCityAry']))
{
	$t_base = "SelectService/";
	$res_ary=array();
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionFrom']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']<=0))
	{
		$error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionTo']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']<=0))
	{
		$error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
		echo "ERROR||||".$error_message;
	}
	else
	{
		$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
	
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionFrom'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idOriginPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szOriginPostCode']= $kConfig->szPostCode;
			$postSearchAry['szOriginCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fOriginLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fOriginLongitude'] = $kConfig->szPostcodeLongitude;
		}
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionTo'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idDestinationPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szDestinationPostCode']= $kConfig->szPostCode;
			$postSearchAry['szDestinationCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
		}

		$kConfig = $_SESSION['config_object'] ;
		// converting cargo details into cubic meter 			
		$counter_cargo = count($kConfig->iLength);
		for($i=1;$i<=$counter_cargo;$i++)
		{
			$fLength = $kConfig->iLength[$i] ;
			$fWidth = $kConfig->iWidth[$i] ;
			$fHeight = $kConfig->iHeight[$i] ;
			$iQuantity = $kConfig->iQuantity[$i] ;
			if($kConfig->idCargoMeasure[$i]==1)  // cm
			{
				$fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
			}
			else
			{
				$fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
				if($fCmFactor>0)
				{
					$fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
				}	
			}
		}
	
		$weight_cargo = count($kConfig->iWeight);
		for($i=1;$i<=$weight_cargo;$i++)
		{
			// converting cargo details into cubic meter 
			if($kConfig->idWeightMeasure[$i]==1)  // kg
			{
				$fTotalWeight += ($kConfig->iWeight[$i]);
			}
			else
			{
				$fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
				if($fKgFactor>0)
				{
					$fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
				}	
			}
		}
		
		$idForwarder =$_SESSION['forwarder_id'];
		$kForwarder = new cForwarder();
		$kForwarder->load($idForwarder);
		
		$postSearchAry['fCargoVolume'] = $fTotalVolume ;
		$postSearchAry['fCargoWeight'] = $fTotalWeight ;
		$postSearchAry['forwarder_id'] = $idForwarder ;
		$postSearchAry['dtTimingDate'] = $kConfig->dtTiming ;
		$postSearchAry['idOriginCountry'] = $kConfig->szOriginCountry ;
		$postSearchAry['idDestinationCountry'] = $kConfig->szDestinationCountry ;
		$postSearchAry['iOriginCC'] = $kConfig->iOrigingCC ;
		$postSearchAry['iDestinationCC'] = $kConfig->iDestinationCC ;
		
		//$kForwarder->szCurrency = 1 ;
		//echo $kForwarder->szCurrency;
		if($kForwarder->szCurrency==1)
		{
			$postSearchAry['idCurrency'] = 1 ;
			$postSearchAry['szCurrency'] = 'USD' ;
			$postSearchAry['fExchangeRate'] = 1 ;
		}
		else
		{
			$kWhsSearch = new cWHSSearch();
			$currencySearchAry = $kWhsSearch->getCurrencyDetails($kForwarder->szCurrency);
								
			//$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
			//$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
			//$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
				
			$postSearchAry['idCurrency'] = $currencySearchAry['idCurrency'];
			$postSearchAry['szCurrency'] = $currencySearchAry['szCurrency']; ;
			$postSearchAry['fExchangeRate'] = $currencySearchAry['fUsdValue'];
		}
		
		//print_r($postSearchAry);
		$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'FORWARDER_TRY_IT_OUT');
		$_SESSION['try_it_out_searched_data'] = $postSearchAry;
		echo "SUCCESS ||||";
		?>
		<div style="border:1px solid #D0D0D0;">
		<?php
		//echo count($searchResultAry);
		//echo "service type ".$postSearchAry['idServiceType'] ;
		if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
		{
			display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
		{
			display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
		{
			display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
		{
			display_wtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		echo '</div>';
		die;
	}
}
else
{	
    $postSearchAry=$_REQUEST['searchAry'];
    $t_base = "home/homepage/";
    //for showing message at top for forwarder as admin and change its TRITOUT status
    // START
    $idForwarderUser =  (int)$_SESSION['forwarder_user_id'];
    if(($kForwarder->adminEachForwarder($idForwarderUser) == __ADMINISTRATOR_PROFILE_ID__) && ((int)$_SESSION['forwarder_admin_id']==0))
    {
        $kConfig = new cConfig;
        if($kConfig->findForwarderTryItOutStauts($idForwarderUser))
        {
            $kConfig->updateForwarderTryItOutStauts($idForwarderUser);
        }
    }  
    if($_GET['mode']=='FORWARDER_COURIER_TRY_IT_OUT')
    {
        $cargodetailAry = $_REQUEST['cargodetailAry']; 
        $postSearchAry['idServiceType'] = __SERVICE_TYPE_DTD__; 
        $postSearchAry['szPageName'] = 'SIMPLEPAGE';
        $postSearchAry['idCC'][0]='1';
        $postSearchAry['idCC'][1]='2';
        $postSearchAry['iFromRequirementPage']=2; 
        $postSearchAry['szBookingRandomNum'] = mt_rand(0, 9999); 
        $postSearchAry['idTimingType'] = 1;
        $postSearchAry['dtTiming'] = date('d/m/Y');
        $postSearchAry['szOriginCountry'] = $postSearchAry['idOriginCountry'];
        $postSearchAry['szDestinationCountry'] = $postSearchAry['idDestinationCountry'];
        $postSearchAry['szSearchMode'] = 'FORWARDER_COURIER_TRY_IT_OUT';
        
        if($postSearchAry['idOriginCountry']>0 && !empty($postSearchAry['szOriginPostCode']))
        {
            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
            $postSearchAry['szOriginCountryStr'] = $postSearchAry['szOriginPostCode'].", ".$kConfig_new->szCountryName ; 
            $szOriginCountryArr = reverse_geocode($postSearchAry['szOriginCountryStr'],true);  
            
            $postSearchAry['szOLat'] = $szOriginCountryArr['szLat'];
            $postSearchAry['szOLng'] = $szOriginCountryArr['szLng'];
            $postSearchAry['szOriginCity'] = $szOriginCountryArr['szCityName'];  
            $postSearchAry['szOriginCountry'] = $postSearchAry['idOriginCountry'];
            $postSearchAry['iOriginPostcodeFoundAtStep'] = 1;
            $postSearchAry['szOriginCountryStr'] = $postSearchAry['szOriginPostCode'].", ".$szOriginCountryArr['szCityName'].", ".$kConfig_new->szCountryName ; 
        }
        if($postSearchAry['idDestinationCountry']>0 && !empty($postSearchAry['szDestinationPostCode']))
        {
            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']);
            $postSearchAry['szDestinationCountryStr'] = $postSearchAry['szDestinationPostCode'].", ".$kConfig_new->szCountryName ; 
            $szDesCountryArr = reverse_geocode($postSearchAry['szDestinationCountryStr'],true);  
            
            $postSearchAry['szDLat']=$szDesCountryArr['szLat'];
            $postSearchAry['szDLng']=$szDesCountryArr['szLng'];
            $postSearchAry['szDestinationCity']=$szDesCountryArr['szCityName'];
            $postSearchAry['szDestinationCountry'] = $postSearchAry['idDestinationCountry'];
            $postSearchAry['iOriginPostcodeFoundAtStep'] = 1;
            $postSearchAry['szDestinationCountryStr'] = $postSearchAry['szDestinationPostCode'].", ".$szDesCountryArr['szCityName'].", ".$kConfig_new->szCountryName ; 
        } 
    } 
    // END
    $kConfig->validateLandingPage($postSearchAry,$cargodetailAry,3); 
      
    if(!empty($kConfig->arErrorMessages))
    {
        if($postSearchAry['szPageName']=='SIMPLEPAGE')
        {
            echo "ERROR_COURIER_TRYOUT||||";	
            echo display_forwarder_courier_try_it_out($kConfig);
            die;  
        }
        else
        {
             echo "ERROR ||||" ;
            ?>
            <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
            <div id="regErrorList">
            <ul>
            <?php
                  foreach($kConfig->arErrorMessages as $key=>$values)
                  {
                    ?><li><?=$values?></li> <?php 
                  }
            ?>
            </ul>
            </div>
            <?php
        } 
    }
    else if(!empty($kConfig->multiEmaptyPostcodeFromCityAry) || !empty($kConfig->multiEmaptyPostcodeToCityAry))
    {
        echo "MULTI_EMPTY_POSTCODE||||" ;

        $kBooking=new cBooking();	

        $_SESSION['config_object'] = $kConfig ;
        $_SESSION['try_it_out_searched_data'] = $postSearchAry ;

        ?>
        <div id="popup-bg"></div>
        <form name="multi_empty_postcode_from_form" id="multi_empty_postcode_from_form" action="" method="post">
        <div id="popup-container">
        <div class="compare-popup popup">
        <p class="close-icon" align="right">
        <a onclick="showHide('Transportation_pop');" href="javascript:void(0);">
        <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
        </a>
        </p>
        <?php
        if(!empty($kConfig->multiEmaptyPostcodeFromCityAry))
        {	
            $counterFrom = count($kConfig->multiEmaptyPostcodeFromCityAry);
            if($counterFrom>5)
            {
                    //$div_class = 'class="compare-popup popup" style="margin-top:-30px;"';
            }
            else
            {
                    //$div_class = 'class="compare-popup popup"';
            }
            ?>
            <div <?=$div_class?>>				
                    <H5><?=t($t_base.'fields/empty_postcode_origin');?></H5>
                    <p><?=t($t_base.'messages/which_part_of');?> <?=$kConfig->szOriginCity?> <?=t($t_base.'messages/need_transportation');?></p>
                    <?php
                            foreach($kConfig->multiEmaptyPostcodeFromCityAry as $key=>$fromRegions)
                            {
                    ?>
                            <p><input type="radio" name="multiEmptyPostcodeCityAry[szRegionFrom]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
                    <?php
                            }
                    ?>
                    <input type="hidden" name="multiEmptyPostcodeCityAry[iRegionFrom]" value="1">
                    </div>
            <?php
    }			

    if(!empty($kConfig->multiEmaptyPostcodeToCityAry))
    {		
			$counterFrom = count($kConfig->multiEmaptyPostcodeToCityAry);
			if($counterFrom>5)
			{
				//$div_class = 'class="compare-popup popup" style="margin-top:-30px;"';
			}
			else
			{
				//$div_class = 'class="compare-popup popup"';
			}
			?>			
			<div <?=$div_class?>>
				<H5><?=t($t_base.'fields/empty_postcode_destination');?></H5>
				<p><?=t($t_base.'messages/which_part_of');?> <?=$kConfig->szDestinationCity?> <?=t($t_base.'messages/need_transportation_to');?></p>
				<?php
					foreach($kConfig->multiEmaptyPostcodeToCityAry as $key=>$fromRegions)
					{
				?>
					<p><input type="radio" name="multiEmptyPostcodeCityAry[szRegionTo]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
				<?php
					}
					?>
					<input type="hidden" name="multiEmptyPostcodeCityAry[iRegionTo]" value="1">
			</div>
			<?php
		  }
		?>
		<br/>
		<p align="center">
			<input type="hidden" name="multiEmptyPostcodeCityAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$kConfig->szBookingRandomNum?>">
				<a href="javascript:void(0);" onclick="updateEmptyPostcodeCity_forwarder('multi_empty_postcode_from_form')" class="button1">
				<span><?=t($t_base.'fields/select');?></span></a>
				<a href="javascript:void(0);" onclick="showHide('Transportation_pop')" class="button2">
				<span><?=t($t_base.'fields/cancel');?></span></a>
		</p>
		</div>
		</div>
	</form>	
		<?php
	}
	else if(!empty($kConfig->multiRegionToCityAry) || !empty($kConfig->multiRegionFromCityAry))
	{
		echo "MULTIREGIO||||";
		$kBooking=new cBooking();	
		$_SESSION['config_object'] = $kConfig ;
		$_SESSION['try_it_out_searched_data'] = $postSearchAry ;
			
		?>
		<div id="popup-bg"></div>
		<form name="multiregion_select_form" id="multiregion_select_form" action="" method="post">
		<div id="popup-container">
		<div class="compare-popup popup" style="margin-top:50px;">
		<p class="close-icon" align="right">
		<a onclick="showHide('Transportation_pop');" href="javascript:void(0);">
		<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
		</a>
		</p>		
		<?php
			if(!empty($kConfig->multiRegionFromCityAry))
			{
				?>			
				<H5><?=t($t_base.'fields/from_regions');?></H5>
				<span id="popupError" class="errorBox" style="display:none;"></span>
				<p><?=t($t_base.'messages/from_regions_popup_header');?></p>
				<?php
					foreach($kConfig->multiRegionFromCityAry as $key=>$fromRegions)
					{
				?>
					<p> <input type="radio" name="multiregionCityAry[szRegionFrom]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
				<?php
					}
				?>
				<input type="hidden" name="multiregionCityAry[iRegionFrom]" value="1">
				<?php
			}
		?>
		<?php
			if(!empty($kConfig->multiRegionToCityAry))
			{
				?>		
				<br>	
				<H5><?=t($t_base.'fields/to_regions');?></H5>
				<?php
					if(empty($kConfig->multiRegionFromCityAry))
					{
						?>
						<span id="popupError" class="errorBox" style="display:none;"></span>
						<?
					}
				?>
				<p><?=t($t_base.'messages/to_regions_popup_header');?></p>
				<?php
					foreach($kConfig->multiRegionToCityAry as $key=>$fromRegions)
					{
				?>
					<p> <input type="radio" name="multiregionCityAry[szRegionTo]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
				<?php
					}
				?>
				<input type="hidden" name="multiregionCityAry[iRegionTo]" value="1">
				<?php
			}
		?>
		<br/>
		<p align="center">
			<input type="hidden" name="multiregionCityAry[szBookingRandomNum]" value="<?=$kConfig->szBookingRandomNum?>">
			<a href="javascript:void(0);" onclick="updatePostcodeRegion_tryitnow();" class="button1">
				<span><?=t($t_base.'fields/continue');?></span></a></p>
		</div>
		</div>
	</form>	
		<?php
	}
	else
	{
		$t_base = "SelectService/";
		
		$customizedCargodetailAry = array();
		// converting cargo details into cubic meter 			
		$counter_cargo = count($kConfig->iLength);
		for($i=1;$i<=$counter_cargo;$i++)
		{
                    $fLength = $kConfig->iLength[$i] ;
                    $fWidth = $kConfig->iWidth[$i] ;
                    $fHeight = $kConfig->iHeight[$i] ;
                    $iQuantity = $kConfig->iQuantity[$i] ;
                    
                    $customizedCargodetailAry[$i]['fLength'] = $fLength;
                    $customizedCargodetailAry[$i]['fWidth'] = $fWidth;
                    $customizedCargodetailAry[$i]['fHeight'] = $fHeight;
                    $customizedCargodetailAry[$i]['fWeight'] = $kConfig->iWeight[$i];
                    $customizedCargodetailAry[$i]['iQuantity'] = $iQuantity;
                    $customizedCargodetailAry[$i]['iColli'] = $iQuantity;
                    $customizedCargodetailAry[$i]['idWeightMeasure'] = $kConfig->idWeightMeasure[$i];
                    $customizedCargodetailAry[$i]['idCargoMeasure'] = $kConfig->idCargoMeasure[$i];
                    
                    if($kConfig->idCargoMeasure[$i]==1)  // cm
                    {
                            $fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
                        if($fCmFactor>0)
                        {
                            $fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
                        }	
                    }
                    
		}
	
		$weight_cargo = count($kConfig->iWeight);
		for($i=1;$i<=$weight_cargo;$i++)
		{
			// converting cargo details into cubic meter 
			if($kConfig->idWeightMeasure[$i]==1)  // kg
			{
				$fTotalWeight += ($kConfig->iWeight[$i]);
			}
			else
			{
				$fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
				if($fKgFactor>0)
				{
					$fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
				}	
			}
		}
		
		$idForwarder =$_SESSION['forwarder_id'];
		$kForwarder = new cForwarder();
		$kForwarder->load($idForwarder);
		
		$postSearchAry['fCargoVolume'] = $fTotalVolume ;
		$postSearchAry['fCargoWeight'] = $fTotalWeight ;
		$postSearchAry['forwarder_id'] = $idForwarder ;
		$postSearchAry['dtTimingDate'] = $kConfig->dtTiming ;
		$postSearchAry['idOriginCountry'] = $kConfig->szOriginCountry ;
		$postSearchAry['idDestinationCountry'] = $kConfig->szDestinationCountry ;
		$postSearchAry['iOriginCC'] = $kConfig->iOrigingCC ;
		$postSearchAry['iDestinationCC'] = $kConfig->iDestinationCC ;
		
		//$kForwarder->szCurrency = 1 ;
		//echo $kForwarder->szCurrency;
		if($kForwarder->szCurrency==1)
		{
                    $postSearchAry['idCurrency'] = 1 ;
                    $postSearchAry['szCurrency'] = 'USD' ;
                    $postSearchAry['fExchangeRate'] = 1 ;
		}
		else
		{
                    $kWhsSearch = new cWHSSearch();
                    $currencySearchAry = $kWhsSearch->getCurrencyDetails($kForwarder->szCurrency);

                    //$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
                    //$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
                    //$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];

                    $postSearchAry['idCurrency'] = $currencySearchAry['idCurrency'] ;
                    $postSearchAry['szCurrency'] = $currencySearchAry['szCurrency']; ;
                    $postSearchAry['fExchangeRate'] = $currencySearchAry['fUsdValue'];
		} 
                if($mode=='FORWARDER_COURIER_TRY_IT_OUT')
                {
                    $postSearchAry['fOriginLatitude'] = round((float)$kConfig->fOriginLatitude,8); 
                    $postSearchAry['fOriginLongitude'] = round((float)$kConfig->fOriginLongitude,8);

                    $postSearchAry['fDestinationLatitude'] = round((float)$kConfig->fDestinationLatitude,8);
                    $postSearchAry['fDestinationLongitude'] = round((float)$kConfig->fDestinationLongitude,8);
                    
                    $postSearchAry['szOriginCountry'] = $postSearchAry['szOriginCountryStr'];
                    $postSearchAry['szDestinationCountry'] = $postSearchAry['szDestinationCountryStr']; 
                    
                    cWHSSearch::$cForwarderTryitoutCargoDetails = $customizedCargodetailAry;
                    
                    $searchResultAry = $kWHSSearch->getCourierProviderData($postSearchAry,$postSearchAry['idCurrency'],$idForwarder,'FORWARDER_COURIER_TRY_IT_OUT');
                    $szCalculationLogString = $kWHSSearch->szCalculationLogString;  
                    
                    $postSearchAry['iPageNumber'] = 100;
                    $searchResultAry = fetch_selected_services($searchResultAry,$kWHSSearch,$postSearchAry);
                    $szCalculationLogString .= " ".$kWHSSearch->szLogString; 
                }
                else
                {
                    $originPostCodeAry=array();
                    $originPostCodeAry = $kConfig->getPostCodeDetails($kConfig->szOriginCountry,$kConfig->szOriginPostCode,$kConfig->szOriginCity);

                    $destinationPostCodeAry=array();
                    $destinationPostCodeAry = $kConfig->getPostCodeDetails($kConfig->szDestinationCountry,$kConfig->szDestinationPostCode,$kConfig->szDestinationCity);

                    $postSearchAry['fOriginLatitude'] = round((float)$originPostCodeAry['szLatitude'],8); 
                    $postSearchAry['fOriginLongitude'] = round((float)$originPostCodeAry['szLongitude'],8);

                    $postSearchAry['fDestinationLatitude'] = round((float)$destinationPostCodeAry['szLatitude'],8);
                    $postSearchAry['fDestinationLongitude'] = round((float)$destinationPostCodeAry['szLongitude'],8);
                    $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'FORWARDER_TRY_IT_OUT');
                     
                    $szCourierCalculationLogString = $kWHSSearch->szLCLCalculationLogs ;
                }  
		$_SESSION['try_it_out_searched_data'] = $postSearchAry;
		echo "SUCCESS ||||"; 
                if($mode=='FORWARDER_COURIER_TRY_IT_OUT')
                {
                    echo display_courier_services_list($postSearchAry,$searchResultAry,$szCalculationLogString);
                }
                else
                {
                    ?>
                    <div style="border:1px solid #D0D0D0;">
                    <?php 
                    //echo count($searchResultAry);
                    if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
                    {
                        display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                    }
                    else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
                    {
                        display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                    }
                    else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
                    {
                        display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                    }
                    else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
                    {
                        display_wtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                    }   
                    echo '</div>';
                    echo "<a href='javascript:void(0)' style='color:#eff0f2;' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                        </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$szCourierCalculationLogString." ".$kCourierService_new->szLogString." </div></div> </div></div>";
                }  
	}
}	
?>