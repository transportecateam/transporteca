<?php
 ob_start();
session_start();
$page_title="My Account";
//ini_set (max_execution_time,60000);
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
// If you want to ignore the uploaded files, 
// set $demo_mode to true;
$output_dir = __UPLOAD_COURIER_LABEL_PDF_FORWARDER__."/";
$allowed_ext = array('pdf');
$iSplitData = true;
$kCourierService = new cCourierServices();
if(isset($_FILES["myfile"]))
{
    $ret = array(); 
   $error =$_FILES["myfile"]["error"];
   { 
    	if(!is_array($_FILES["myfile"]['name'])) //single file
    	{
            $RandomNum   = time();  
            $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
            $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
            $size = $_FILES['myfile']['size'];
            if($size>(int)__MAX_FILE_SIZE_To_UPLOAD__)
            {
                //echo "Maximum file size allowed is 3MB";
                //die;
            }  
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt       = preg_replace_callback('.','',$ImageExt);
            $ImageName      = preg_replace_callback("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt; 
             
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $NewImageName); 
            
            if($iSplitData)
            {
                /*
                 * We do not split pd from forwarder section
                 */
                $szNewImagePath = trim($output_dir); 
                $NewImageName = trim($NewImageName); 
              
                $ret = $kCourierService->splitUploadedLabels($NewImageName,$szNewImagePath,true);
                
                if(!empty($ret) && is_array($ret))
                { 
                    echo json_encode($ret); 
                }
                else
                { 
                    $output_dir = __UPLOAD_COURIER_LABEL_PDF__."/";
                    $old_file_dir = __UPLOAD_COURIER_LABEL_PDF_FORWARDER__."/";
        
                    $szOldPdfFilePath = $old_file_dir."".$NewImageName ;
                    $szNewPdfFilePath = $output_dir."".$NewImageName ;
                    
                    @rename($szOldPdfFilePath, $szNewPdfFilePath);
                    $ret[0]['name']= $NewImageName;  
                    ob_end_clean(); 
                    echo json_encode($ret);
                }
            }
            else
            { 
                $ret[0]['name']= $NewImageName;  
            } 
    	}
    	else
    	{
            $fileCount = count($_FILES["myfile"]['name']);
            for($i=0; $i < $fileCount; $i++)
            {
                $RandomNum   = time();
            
                $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name'][$i]));
                $ImageType      = $_FILES['myfile']['type'][$i]; //"image/png", image/jpeg etc.
             
                $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
                $ImageExt       = str_replace('.','',$ImageExt);
                $ImageName      = preg_replace_callback("/\.[^.\s]{3,4}$/", "", $ImageName);
                $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt;
                
                $ret[$NewImageName]= $NewImageName;
                move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$NewImageName );
            }
    	}
    }  
}
?>
