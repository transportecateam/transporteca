<?php
ob_start(); 	
session_start();

$display_profile_not_completed_message=true;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle="Transporteca | Billing "; 
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
require_once( __APP_PATH__ . "/forwarders/listExcelData.php" );
 
$kBooking = new cBooking();
$kForwarder = new cForwarder();
$kBilling= new cBilling();
$kConfig= new cConfig();
        
checkAuthForwarder();
$detailsBankAcc=$kBooking->detailsForwarderBankAcc($idForwarder);
//print_r($detailsBankAcc);
$t_base = "ForwarderBillings/";
$t_base_error="Error";
$idForwarder =$_SESSION['forwarder_id'];

//dates are for searching in a range
$format = 'Y-m-d';
$date = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
$days=date('d')-1;
$toDate=date($format);
$fromDate=date($format,strtotime('- 3 MONTH'));
//passing an date array for searching
$dateArr=array($fromDate,$date);
//$date="  BETWEEN '".$fromDate."' AND '".$date."'";

$dateView = 'Date range: '.date('d/m/Y',strtotime($fromDate)).' - '.date('d/m/Y',strtotime($toDate)); 
?>

<div id="customs_clearance_pop" class="help-pop"></div>
<div id="customs_clearance_pop_right" class="help-pop right"></div>
<div id="hsbody" style="padding-top: 5px"> 
    <div style="float: right;font-size: 10pt;margin-right: 7.4%" id="date_change"><?=$dateView;?></div>
    <div style="clear: both;"></div>
    <div style="float: left;"><h5><strong><?=t($t_base.'title/transaction_history');?> <?=$detailsBankAcc['szDisplayName'];?></strong></h5></div>
    <div id="forwarder_billing_top_form" style="float: right;">
    <?php
        echo forwarder_billing_search_form_new($t_base);
    ?>
    </div>	
    <div style="clear: both"></div> 
    <div id="content">
        <?php echo showBillingDetails_new($t_base,$idForwarder,$dateArr);?>
    </div>	
</div> 

<?php require_once( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" ); ?>	