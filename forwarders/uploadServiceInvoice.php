<?
session_start();
if($_GET['id'] && $_GET['user'])
{
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
require_once(__APP_PATH__.'/inc/functions.php');
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );


	$kForwarder= new cForwarder();
	$kBooking = new cBooking();
	$idForwarder=sanitize_all_html_input($_GET['user']);
	$invoiceNumber=sanitize_all_html_input($_GET['id']);
	$flag=sanitize_all_html_input($_GET['flag']);
	$filename=getForwarderInvoiceUploadService($idForwarder,$invoiceNumber,$flag);
	if($flag=='PDF')
	{	
	   download_booking_pdf_file($filename);
        die;
	}
}?> 