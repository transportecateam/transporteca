<?php
/**
 * Forwarder My Company  
 */
 ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle='Transporteca | Company Introduction';
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();
$t_base = "ForwardersCompany/Introduction/";
$idForwarder 		=	$_SESSION['forwarder_id'];
$kForwarder 		=	new cForwarder;
$forwarderDetails	=	$kForwarder->findSzLinkForwarder($idForwarder);
$forwarderUrl 		= 	$forwarderDetails['szLink'];
$url = parse_url($forwarderUrl);
if(isset($url['scheme']))
{
	$forwarderUrl = $forwarderUrl;
}
else
{
	$forwarderUrl = 'http://'.$forwarderUrl;
}
$link = ' <span class="tc_link"><a href="'.($forwarderDetails['szLink']?$forwarderUrl:'#').'" target="_blank">'.($forwarderDetails['szLink']?$forwarderDetails['szLink']:'')."</a></span> ";

$KwarehouseSearch=new cWHSSearch();
$content  = html_entity_decode($KwarehouseSearch->getManageMentVariableByDescription('__FORWARDER_COMPANY_INTRODUCTION__'));
$content = refineDescriptionData($content);
$content= str_replace('szDisplayName','<b>'.$forwarderDetails['szDisplayName'].'</b>',$content);
$content= str_replace('szLink',$link,$content);
?>
<div id="hsbody-2">
   	<?php require_once( __APP_PATH_LAYOUT__ ."/forwarder_company_nav.php" ); ?> 
	<div class="hsbody-2-right link16">
		<?=$content?>	
	</div>
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>				