<?php
/**
 * Forwarder My Company  
 */
ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
$szMetaTitle="Dashboard";
//$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder(); 

$idForwarder = $_SESSION['forwarder_id'] ;
if($kForwarder->isProfileInfoComplete($_SESSION['forwarder_user_id']) && ((int)$_SESSION['forwarder_admin_id']==0))
{
	?>
	<script type="text/javascript">
		open_edit_profile_popup();
	</script>
	<?php
}
else if(isset($idForwarder) && (int)$idForwarder>0)
{
    if(!$kForwarder->iPopupDetails && isset($_SESSION['forwarder_user_id']) && (int)$_SESSION['forwarder_user_id']>0)
    {
        fillPopupDetailsDetails(); 
    }
    else if((int)$kForwarderContact->iPasswordUpdated==1)
    {	 
        checkForgotPasswordUpdated();
    } 
} 

/*else if($kForwarder->checkForwarderComleteCompanyInfo($idForwarder))
{
	$t_base_header = "home/";
	?>
	<div id="leave_page_div_prefernces" style="display:block;">	
	<?	echo incomplete_comapny_details_popup($t_base_header);	?>
	</div>	
    <?
}
*/
$kForwarder->isForwarderOnline($idForwarder,$mode,false);

	if($kForwarder->isCurrencyLogExist($idForwarder))
	{
		$redirect_url = $_SERVER['HTTP_REFERER'];
		$base_url = __BASE_URL__.'/';
		$base_secure_url = __BASE_URL_SECURE__.'/';
		if((int)$_SESSION['forwarder_admin_id']>0)
		{
			$redirect_url = __FORWARDER_COMPANY_PAGE_INTRODUCTION_URL__ ;
		}
		else if(($redirect_url==$base_url) || ($redirect_url==$base_secure_url))
		{
			$redirect_url = __FORWARDER_MY_ACCOUNT_URL__ ;
		}
		else if(empty($redirect_url))
		{
			$redirect_url = __FORWARDER_MY_ACCOUNT_URL__ ;
		}
		$_SESSION['display_currency_change_popup'] = 1 ;
		header("Location:".$redirect_url);
		die;
	/*
		$t_base = "Dashboard/";
		?>
		<div id="leave_page_div_prefernces" style="display:block;">	
		<?
		$div_id = 'leave_page_div_prefernces';
		echo forwarder_currency_change_notifivcation_dashboard($idForwarder,$t_base,$div_id);
		//die;
		?>
		</div>
		<?
	*/
	}

$lclServices 	= $kForwarder->dashboardDetails('LCL_SERVICES',$idForwarder);
//$lclServices = $kForwarder->getTotalLCLCount($idForwarder);
$lclRating	 	= $kForwarder->dashboardDetails('RATING',$idForwarder);
$graphBookings	= $kForwarder->displayBookingDetails('BOOKING_PER_MONTH',$idForwarder);
$graphSales		= $kForwarder->displaySalesDetails('SALES_PER_MONTH',$idForwarder);
$mostSellingLcl = $kForwarder->mostSellingLcl('MOST_SELLING_LCL',$idForwarder);
$t_base = "Dashboard/";

$kForwarder->load($idForwarder);
$kWhsSearch = new cWHSSearch();
$forwarderCurrencyAry = array();

$idCurrency = $kForwarder->szCurrency;
if($idCurrency==1)
{
	$forwarderCurrencyAry['szCurrency'] = 'USD';
}
else
{
	$forwarderNewCurrencyAry = $kWhsSearch->getCurrencyDetails($idCurrency);
				
	if(!empty($forwarderNewCurrencyAry))
	{					
		$forwarderCurrencyAry['szCurrency'] = $forwarderNewCurrencyAry['szCurrency'];
		$fNewExchangeRate = $forwarderNewCurrencyAry['fUsdValue'];
	}
}
//$forwarderCurrencyAry = $kWhsSearch->getCurrencyDetails();

?>
<div id="forwarder_company_div" style="display:none;">
</div>
<div id="hsbody">
<div>
<div style="float:left;height:50px;margin: 0 0 0px 0px;margin-top:-15px;font-style:italic;font-size:14px;color:#8A9454;"><a style="text-decoration: none;cursor: pointer;"><span <?if((int)$lclServices['lclServices']>0){?>onclick='showLCLServices(<?=$idForwarder;?>)'<?} ?>><?=t($t_base."title/You_currently_have")." ".number_format($lclServices['lclServices'])." ".t($t_base."title/LCL_services"); ?></span></a></div>

<div style="float:right;height:50px;margin-top:-15px;font-style:italic;font-size:14px;color:#8A9454;"><div style="float:left;"><a style="text-decoration: none;" href="<?=$feedback_page_url?>"><?=t($t_base."title/Your_average_rating")." ".$lclRating['avgRating']; if($lclRating['avgRating']>1){echo ' stars ';}else if($lclRating['avgRating']<1.1){echo ' star ';} echo t($t_base."title/out_of_5_possible")?></a></div> 

<?php 
if(1.0<=$lclRating['avgRating'] && $lclRating['avgRating']<=1.5  )
{
	$star="1star.png";
}
if(1.6<=$lclRating['avgRating'] && $lclRating['avgRating']<=2.5  )
{
	$star="2star.png";
}
if(2.6<=$lclRating['avgRating'] && $lclRating['avgRating']<=3.5  )
{
	$star="3star.png";
}
if(3.6<=$lclRating['avgRating'] && $lclRating['avgRating']<=4.5  )
{
	$star="4star.png";
}
if(4.6<=$lclRating['avgRating'] && $lclRating['avgRating']<=5.0  )
{
	$star="5star.png";
}
//print_r($_SERVER);
?>
<?php if($star){?>
	<div style="float:right; margin-top: -5px"><img src="<?=__BASE_STORE_IMAGE_URL__."/".$star?>" ></div>
<?php } ?>
 </div>
</div>
<div class="clear-all"></div>
<div>
	<div style="float:left;margin-top: -18px;">
		<p style="padding-left: 0px;font-weight:bolder;font-size:16px;"><?=t($t_base."title/Number_of_bookings")?></p>
		<img style="margin-left:-19px;" src="<?=__FORWARDER_HOME_PAGE_URL__."/".$graphBookings."?n=".time();?>" alt="<?=t($t_base."alt/Booking_Details");?>">
	</div>
	<div style="float:right;padding-right: 30px;margin-top: -18px;">
		<p style="padding-left: 17px;font-weight:bolder;font-size:16px;"><?=t($t_base."title/Total_sales")?> (<?=t($t_base."title/thousand")?><?=($forwarderCurrencyAry['szCurrency']?' '.$forwarderCurrencyAry['szCurrency']:'');?>)</p>
		<img src="<?=__FORWARDER_HOME_PAGE_URL__."/".$graphSales."?n=".time();?>" alt="<?=t($t_base."alt/Sales_Details");?>">
	</div>
</div>
<div class="clear-all"></div> 
<br/>
<br/>
<div>
	<?php 
	//$mostSellingLcl=NULL;
	 ?>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="format-5" style="border-bottom-color: #FFFFFF;border-top-color: #FFFFFF;" >
						<thead>
							<tr>
								<td width="50%" class="tabcolor" style="border-left-color: white;background-color:#FFFFFF;border-top:#FFFFFF;"><h4><strong><?=t($t_base."title/Most_selling")?></strong> </td>
								<td width="20%" class="tabcolor" style="border-left-color: white;background-color:#FFFFFF;border-top:#FFFFFF;"></td>
								<td width="10%" align="center" class="tabcolor" style="background-color:#FFFFFF;border-top:#FFFFFF;" ><?=t($t_base."days/30")?></td>
								<td width="10%" align="center" class="tabcolor" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/90")?></td>
								<td width="10%" align="center" class="tabcolor" style="background-color:#FFFFFF;border-top:#FFFFFF;"><?=t($t_base."days/365")?></td>
							</tr>
						</thead>
		<?php  if(!empty($mostSellingLcl))
			{
				$i=1;
				foreach ($mostSellingLcl as $key=>$value )
				{
					$mostSellingBooking30=$kForwarder->dashboardDetails('BOOKING_STATUS',$idForwarder,$value['idWarehouseFrom'],$value['idWarehouseTo'],30);
					$mostSellingBooking90=$kForwarder->dashboardDetails('BOOKING_STATUS',$idForwarder,$value['idWarehouseFrom'],$value['idWarehouseTo'],90);
					$mostSellingBooking365=$kForwarder->dashboardDetails('BOOKING_STATUS',$idForwarder,$value['idWarehouseFrom'],$value['idWarehouseTo'],365);
					//print_r($mostSellingBooking30);
					?>
					<tr>
						<td rowspan="3" valign="top" style="border-left-color:#FFFFFF;background-color:#FFFFFF;" ><span class="fl-3"><?=$i.". "?></span><span class="fl-95"><?=$value['warehouseFromName'] ." (".$value['warehouseFormCity'].", ".$value['countryFrom']  .") to " 
					.$value['warehouseToName'] ." (".$value['warehouseToCity'].", ".$value['countryTo']  .")";?></span></td>
						<td style="border-left-color:#FFFFFF;" ><?=t($t_base."fields/Bookings")?></td>
						<td align="center" ><?if($mostSellingBooking30['booking']=='0'){echo "N/A";}else{echo number_format($mostSellingBooking30['booking']);}?></td>
						<td align="center"><?if($mostSellingBooking90['booking']=='0'){echo "N/A";}else{echo number_format(($mostSellingBooking90['booking']/3));}?></td>
						<td align="center"><?if($mostSellingBooking365['booking']=='0'){echo "N/A";}else{echo number_format((($mostSellingBooking365['booking']*30)/365));}?></td>
					</tr>
					<tr>
						<td style="border-left-color:#FFFFFF;"><?=t($t_base."fields/Total_sales")?> <span style="font-size:12px;">(t<?=$forwarderCurrencyAry['szCurrency']?>)</span></td>
						<td align="center"><?if($mostSellingBooking30['price']=='0'){echo "N/A";}else{echo number_format((float)$mostSellingBooking30['price'],1);}?></td>
						<td align="center"><?if($mostSellingBooking90['price']=='0'){echo "N/A";}else{echo number_format((float)($mostSellingBooking90['price']/3),1);}?></td>
						<td align="center"><?if($mostSellingBooking365['price']=='0'){echo "N/A";}else{echo number_format((float)(($mostSellingBooking365['price']*30)/365),1);}?></td>
					</tr>
					<tr>
						<td style="background-color:#FFFFFF;border-left-color:#FFFFFF;"><?=t($t_base."fields/Sales")?> <span style="font-size:12px;">(<?=$forwarderCurrencyAry['szCurrency']?>)</span></td>
						<td align="center" style="background-color:#FFFFFF;"><?if($mostSellingBooking30['average']=='0'){echo "0";}else{echo number_format((float)$mostSellingBooking30['average']);}?></td>
						<td align="center" style="background-color:#FFFFFF;"><?if($mostSellingBooking90['average']=='0'){echo "0";}else{echo number_format((float)$mostSellingBooking90['average']);}?></td>
						<td align="center" style="background-color:#FFFFFF;"><?if($mostSellingBooking365['average']=='0'){echo "0";}else{echo number_format((float)$mostSellingBooking365['average']);}?></td>
					</tr>				
					<?php
					$i++;
				}
			}
		if(empty($mostSellingLcl))
		{
		?>
			<tr>
				<th colspan="5" style="border-bottom:1px solid #BFBFBF;text-align: center;"><b style="font-size: 20px;font-stretch: wider;"> No Record Found</b></th>
			</tr>
	<?php } ?>	
		</table>
	<br/>
	<br/>
</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>