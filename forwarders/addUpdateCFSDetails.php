<?php
 ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "BulkUpload/";
$t_base_error="Error";
//echo __APP_PATH__ ;
$kWHSSearch=new cWHSSearch();
$kConfig = new cConfig();
$updateFlag=false;
$allCountriesArr=$kConfig->getAllCountries(true);
if($_REQUEST['flag']=='update' && (int)$_REQUEST['idWarehouse']>0 && !$updateFlag)
{
    $flag=$_REQUEST['flag'];
    $kWHSSearch->load($_REQUEST['idWarehouse']);

    $iWarehouseType = $kWHSSearch->iWarehouseType;
    $szPhoneNumber = urlencode($kWHSSearch->szPhone);
    $str = substr($szPhoneNumber, 0, 1);
    $substr=substr($szPhoneNumber,1);
    $phone=str_replace("+"," ",$substr);
    if(!empty($phone))
    {
        //$kWHSSearch->szPhone = $str."".$phone;
    }
	
}
else
{
    $flag='add';
    if($_REQUEST['iWarehouseType']>0)
    {
        $iWarehouseType = $_REQUEST['iWarehouseType'];
    }
}  
if(!empty($_POST['cfsAddEditArr']['szPhoneNumber']))
{
    $szPhoneNumber=urlencode($_POST['cfsAddEditArr']['szPhoneNumber']);
    $str = substr($szPhoneNumber, 0, 1); 
    //echo $str;
    $substr=substr($szPhoneNumber,1);
    $phone=str_replace("+"," ",$substr);
    //echo $phone;
    if(!empty($phone))
    {
        $_POST['cfsAddEditArr']['szPhoneNumber']=$str."".$phone;
    }
} 
if($flag=='update')
{
?>
<script type="text/javascript">
document.getElementById("add_edit_warehouse_button").innerHTML='<span style="min-width: 79px;">save</span>';
</script>
<?php }
if(!empty($kWHSSearch->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kWHSSearch->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php
die;
}
?>
<script type="text/javascript">
$().ready(function() {	
	$("#szPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
	checkCfsDetails();
});
	</script>
        
        <?php
            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            { 
                $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text_airport_cfs'); 
                $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text_airport_warehouse');
                $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text_airport_cfs');
                $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text_airport_cfs'); 
            }
            else
            { 
                $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text'); 
                $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text');
                $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text'); 
                $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text');
            } 
            $szCfsNameBluText = t($t_base.'messages/cfs_name_blue_box_text'); 
        ?>
	<div id ="Error-log"></div>
	<div id='addedit_warehouse'>
            <form name="addEditWarehouse" id="addEditWarehouse" method="post" >
		<label class="profile-fields">
                    <span class="field-name"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'fields/airport_warehouse'):t($t_base.'fields/cfs_name');?></span>
                    <span class="field-container"><input type="text" onblur="closeTip('cfs_name');" onkeyup="checkCfsDetails();" onfocus="openTip('cfs_name');" name="cfsAddEditArr[szWareHouseName]" id="szCFSName" value="<?=$_POST['cfsAddEditArr']['szCFSName'] ? $_POST['cfsAddEditArr']['szCFSName'] : $kWHSSearch->szWareHouseName ?>"/></span>
                    <div class="field-alert"><div id="cfs_name" style="display:none;"><?php echo $szCfsNameBluText; ?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/address_line');?> 1</span>
                    <span class="field-container"><input type="text" onblur="closeTip('address_1');" onkeyup="checkCfsDetails();" onfocus="openTip('address_1');" name="cfsAddEditArr[szAddress1]" id="szAddressLine1" value="<?=$_POST['cfsAddEditArr']['szAddressLine1'] ? $_POST['cfsAddEditArr']['szAddressLine1'] : $kWHSSearch->szAddress ?>"/></span>
                    <div class="field-alert"><div id="address_1" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/address_line');?> 2&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                    <span class="field-container"><input onblur="closeTip('address_2');" onfocus="openTip('address_2');"  type="text" name="cfsAddEditArr[szAddress2]" id="szAddressLine2" value="<?=$_POST['cfsAddEditArr']['szAddressLine2'] ? $_POST['cfsAddEditArr']['szAddressLine2'] : $kWHSSearch->szAddress2 ?>"/></span>
                    <div class="field-alert"><div id="address_2" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/address_line');?> 3&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                    <span class="field-container"><input type="text" onblur="closeTip('address_3');" onfocus="openTip('address_3');" name="cfsAddEditArr[szAddress3]" id="szAddressLine3" value="<?=$_POST['cfsAddEditArr']['szAddressLine3'] ? $_POST['cfsAddEditArr']['szAddressLine3'] : $kWHSSearch->szAddress3 ?>"/></span>
                    <div class="field-alert"><div id="address_3" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/postcode');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                    <span class="field-container"><input type="text" onblur="closeTip('cfs_post_code');" onfocus="openTip('cfs_post_code');" name="cfsAddEditArr[szPostCode]" id="szOriginPostCode" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  value="<?=$_POST['cfsAddEditArr']['szPostCode'] ? $_POST['cfsAddEditArr']['szPostCode'] : $kWHSSearch->szPostCode ?>"/></span>
                    <div class="field-alert"><div id="cfs_post_code" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/city');?></span>
                    <span class="field-container"><input type="text" onblur="closeTip('cfs_city');" onkeyup="checkCfsDetails();" onfocus="openTip('cfs_city');" name="cfsAddEditArr[szCity]" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')" id="szOriginCity" value="<?=$_POST['cfsAddEditArr']['szCity'] ? $_POST['cfsAddEditArr']['szCity'] : $kWHSSearch->szCity ?>"/></span>
                    <div class="field-alert"><div id="cfs_city" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/province');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                    <span class="field-container"><input type="text" onblur="closeTip('cfs_state');" onfocus="openTip('cfs_state');" name="cfsAddEditArr[szState]" id="szState" value="<?=$_POST['cfsAddEditArr']['szState'] ? $_POST['cfsAddEditArr']['szState'] : $kWHSSearch->szState ?>"/></span>
                    <div class="field-alert"><div id="cfs_state" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/country');?></span>
                    <span class="field-container">
                        <select size="1" onchange="showCountriesByCountry(this.value);checkCfsDetails();" onblur="closeTip('cfs_country');" onfocus="openTip('cfs_country');" name="cfsAddEditArr[szCountry]" id="szCountry" style="max-width:211px;">
                            <option value="">Select Country</option>
                                <?php
                                    if(!empty($allCountriesArr))
                                    {
                                        foreach($allCountriesArr as $allCountriesArrs)
                                        {
                                            ?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['cfsAddEditArr']['szCountry'])?$_POST['cfsAddEditArr']['szCountry']:$kWHSSearch->idCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option><?php
                                        }
                                    }
                                ?>
                        </select>
                    </span>
                    <div class="field-alert"><div id="cfs_country" style="display:none;"><?=$szAddressNameBluText;?></div></div>
		</label> 
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/phone_number');?></span>
                    <span class="field-container"><input type="text" onkeyup="checkCfsDetails();" onblur="closeTip('cfs_phone');" onfocus="openTip('cfs_phone');" name="cfsAddEditArr[szPhone]" id="szPhoneNumber" value="<?=$_POST['cfsAddEditArr']['szPhoneNumber'] ? $_POST['cfsAddEditArr']['szPhoneNumber'] : $kWHSSearch->szPhone ?>"/></span>
                    <div class="field-alert"><div id="cfs_phone" style="display:none;"><?php echo $szPhoneNumberBluText; ?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/contact_person');?></span>
                    <span class="field-container"><input type="text" onkeyup="checkCfsDetails();" onblur="closeTip('cfs_contact_person');" onfocus="openTip('cfs_contact_person');" name="cfsAddEditArr[szContactPerson]" id="szContactPerson" value="<?=$_POST['cfsAddEditArr']['szContactPerson'] ? $_POST['cfsAddEditArr']['szContactPerson'] : $kWHSSearch->szContactPerson ?>"/></span>
                    <div class="field-alert"><div id="cfs_contact_person" style="display:none;"><?=t($t_base.'messages/contact_person_blue_box_text');?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/email');?></span>
                    <span class="field-container"><input type="text" onkeyup="checkCfsDetails();" onblur="closeTip('cfs_email');" onfocus="openTip('cfs_email');" name="cfsAddEditArr[szEmail]" id="szEmail" value="<?=$_POST['cfsAddEditArr']['szEmail'] ? $_POST['cfsAddEditArr']['szEmail'] : $kWHSSearch->szEmail ?>"/></span>
                    <div class="field-alert"><div id="cfs_email" style="display:none;"><?=t($t_base.'messages/email_blue_box_text');?></div></div>
		</label>
		<br />
		<!-- onclick="open_help_me_select_popup()" 
		 __FORWARDER_HOME_PAGE_URL__/googleMap.php
		 -->
		<h5><strong><?=t($t_base.'fields/exact_location');?></strong> <a href="javascript:void(0);" onclick="open_help_me_select_popup()" class="f-size-14"><?=t($t_base.'fields/help_me_find_this');?></a></h5>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/latitude');?></span>
                    <span class="field-container"><input type="text" onkeyup="checkCfsDetails();" onblur="closeTip('cfs_lat');" onfocus="openTip('cfs_lat');" name="cfsAddEditArr[szLatitude]" id="szLatitude" value="<?=$_POST['cfsAddEditArr']['szLatitude'] ? $_POST['cfsAddEditArr']['szLatitude'] : $kWHSSearch->szLatitude ?>"/></span>
                    <div class="field-alert1"><div id="cfs_lat" style="display:none;"><?php echo $szLatitudeBluText; ?></div></div>
		</label>
		<label class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/longitude');?></span>
                    <span class="field-container"><input type="text" onkeyup="checkCfsDetails();" onblur="closeTip('cfs_long');" onfocus="openTip('cfs_long');" name="cfsAddEditArr[szLongitude]" id="szLongitude" value="<?=$_POST['cfsAddEditArr']['szLongitude'] ? $_POST['cfsAddEditArr']['szLongitude'] : $kWHSSearch->szLongitude ?>"/></span>
                    <div class="field-alert1"><div id="cfs_long" style="display:none;"><?php echo $szLongitudeBluText; ?></div></div>
		</label>
		<br /> 
		<div id='bottom-form'>
	
	<?php
	if($kWHSSearch->szWareHouseName==''){
		
	?> 
                <h4><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_this_airport_warehouse'):t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
                <p style="margin-top: 5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/add_local_market_airport_cfs'):t($t_base.'messages/add_local_market');?></p>
	    	 <div style="direction: ltr;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td valign="top" width="215" style="padding-top:5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_airport_warehouse'):t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			
	    	 </select>
	    	 <br />
	    	  <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?></span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	 </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;" ><?=t($t_base.'messages/add_from_list');?><br /><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/sorted_on_airport_cfs'):t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>	    	 
	    	 <select id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 </select>
	    	 <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	    	 	
	    	 </div>
		
	    
		<br /><br />
	<?php }else{
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries(true);
		//$idCountry = ;
		//$kWHSSearch->set_idCountry($idCountry);
		$countryList = $kWHSSearch->findWarehouseCountryList($kWHSSearch->idCountry,$kWHSSearch->szLatitude,$kWHSSearch->szLongitude);?>
	    	<div id="selectForCountrySelection">
	    	 <?php //$selectListCFS = array(1,2,3);
				if($kWHSSearch->id)
				{
					$selectListCFS  = $kWHSSearch->selectWarehouseCountries($kWHSSearch->id);
					 
				}
	    	 	elseif($kWHSSearch->idCountry!= NULL)
	    	 	{
	    	 		$selectListCFS = array($kWHSSearch->idCountry);
	    	 	}
	    	 ?> 
                <h4><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_this_airport_warehouse'):t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
                <p style="margin-top: 5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/add_local_market_airport_cfs'):t($t_base.'messages/add_local_market');?></p>
	    	<div style="direction: ltr;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td valign="top" width="215" style="padding-top:5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_airport_warehouse'):t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			<?php
			if(!empty($allCountriesArr))
			{
				foreach($allCountriesArr as $allCountriesArrs)
				{
					if(in_array($allCountriesArrs['id'],$selectListCFS))
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			}
		?>	
	    	 </select>
	    	  <br />
	    	   <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	  </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/sorted_on_airport_cfs'):t($t_base.'messages/sorted_on');?></p>
	    	 </td>
	    	<td>		    	
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 <?php
	    	 	if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."' >".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	    	 ?>
	    	 </select>
	    	  <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	  	   <div class="profile-fields" style="margin-top: 14px;float:left;">
	    	 	<div class="field-alert" id="example" style="display:none;">
	    	 		<div>
	    	 		<p><?=t($t_base.'messages/for_your_rotterdam');?></p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'messages/for_your_hong_kong');?></p>
	    	 		</div>
	    	 	</div>
	    	 </div>
		   </div>
	    
		<br /><br />
	
	    	 <?php //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>	
		<?php } ?>
		</div>	
		<div style="clear: both;"></div>
		<div class="oh">
			<p class="fl-30" align="right" style="padding-top: 6px;">&nbsp;</p>
			<p class="fl-40" align="left">
			<input type="hidden" name="szTransportation" id="szTransportation" value="4">
			<input type="hidden" name="flag" id="flag" value="<?=$flag?>">
			<input type="hidden" name="idWarehouse" id="idWarehouse" value="<?=$_REQUEST['idWarehouse']?>">
			<input type="hidden" name="cfsAddEditArr[idForwarder]" id="idForwarder" value="<?=$idForwarder?>">
			<input type="hidden" name="cfsAddEditArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="">
                        <input type="hidden" name="cfsAddEditArr[iWarehouseType]" id="iWarehouseType" value="<?php echo $iWarehouseType; ?>">
			<input type="hidden" name="cfsAddEditArr[idEditWarehouse]" id="idEditWarehouse" value="<?=$_POST['cfsAddEditArr']['idEditWarehouse'] ? $_POST['cfsAddEditArr']['idEditWarehouse'] : $_REQUEST['idWarehouse']?>">
			<br />
			<a href="javascript:void(0)" class="button1" id="add_edit_warehouse_button" style="opacity:0.4;" <?php /*onclick="<?if($kWHSSearch->id ==''){?>submit_warehouse_data_obo()<?}elseif($kWHSSearch->id!=''){?>update_warehouse_data_obo()<?}?>"  */?>><span style="min-width: 79px;"><?=t($t_base.'fields/save');?></span></a>
			<a href="javascript:void(0);" id="clear_cfs_location_button" onclick="refresh_bottom_div('<?php echo $iWarehouseType; ?>')" class="button2"><span style="min-width: 79px;"><?=t($t_base.'fields/clear');?></span></a></p>
		</div>
		<input type="hidden" id="CFSListing" name="cfsAddEditArr[selectNearCFS]" value="<?php if($kWHSSearch->id !=NULL){echo implode(',',$selectListCFS);}?>">
		</form>
	</div>
	