<?php
/**
 * Edit User Information
 */ 
ob_start();
session_start();
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );

$t_base = "ForwarderBookings/";
$kBooking = new cBooking();
checkAuthForwarder_ajax();

if(!empty($_REQUEST['mode']))
{
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
}
if($operation_mode=='CLEAR_FORM')
{	
	$booking_searched_data = array();
	$data = array();
	$data['iBookingYear']=date('Y');
	$data['iBookingMonth']=date('m');
	$booking_searched_data = $kBooking->search_forwarder_bookings($data);
	
	echo "SUCCESS||||";
	echo forwarder_booking_search_form($t_base);
	echo "||||";
	echo forwarder_booking_bottom_html($booking_searched_data,$t_base);
}
elseif($operation_mode=='UPDATE_YOUR_REFERENCE')
{
	$idBooking = sanitize_all_html_input(trim($_REQUEST['id']));
	$szBookingRef = sanitize_all_html_input(trim($_REQUEST['ref']));
        $idBillingFlag=sanitize_all_html_input(trim($_REQUEST['idBillingFlag']));
	$kBooking=new cBooking();
	$kBooking->load($idBooking);
	?>
<div id="popup-bg"></div>
<div id="popup-container">	
	<div class="compare-popup popup" >
	<p class="close-icon" align="right">
	<a onclick="showHide('invoice_comment_div');" href="javascript:void(0);">
	<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
	</a>
	</p>
		<h5><strong><?=t($t_base.'title/booking_reference');?> <?=$szBookingRef?></strong></h5>
		<p><?=t($t_base.'title/type_reference');?>:</p>
		<p>
			<input type="text"  name="updateInvoiceAry[szForwarderReference]" onkeypress="return submitForm(event)"  id="fwdReference"  value="<?=$kBooking->szInvoiceFwdRef?>" size="43" AUTOCOMPLETE ="off">
		</p>
		<br>
		<p align="center">
			<input type="hidden" id="idBooking" name="updateInvoiceAry[idBooking]" value="<?=$idBooking?>">
			<input type="hidden" id="szRef" name="updateInvoiceAry[szBookingRef]" value="<?=$szBookingRef?>">
                        <input type="hidden" id="idBillingFlag" name="updateInvoiceAry[idBillingFlag]" value="<?=$idBillingFlag?>">
			<a href="javascript:void(0)" class="button1" onclick="update_invoice_comment_confirm()"><span><?=t($t_base.'fields/save');?></span></a>
			<a href="javascript:void(0)" class="button2" onclick="showHide('invoice_comment_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
		</p>
	</div>
</div>
	<?php
}
else if(!empty($_REQUEST['bookingSearchAry']))
{
	$sortby='';
	$sort='';
	if($_REQUEST['sortby']!='')
	{
		$sortby=$_REQUEST['sortby'];
		$sort=$_REQUEST['sort'];
	}	
	$booking_searched_data = $kBooking->search_forwarder_bookings($_REQUEST['bookingSearchAry'],$sortby,$sort);
	
}
else if(!empty($_REQUEST['updateInvoiceAry']))
{	
    $kBooking->set_id(sanitize_all_html_input(trim($_REQUEST['idBooking'])));
    $kBooking->set_szInvoiceForwarderReference(sanitize_all_html_input(trim($_REQUEST['szForwarderReference'])),false);
    
    if($kBooking->id>0)
    {
        $update_query = " szForwarderReference = '".mysql_escape_custom(trim($_REQUEST['szForwarderReference']))."'" ;
        $idBooking = $kBooking->id ;
        if($kBooking->updateDraftBooking($update_query,$kBooking->id));
        {	
            $kBooking->updateBookingReferences($_REQUEST['szForwarderReference'],$idBooking);
            echo "SUCCESS||||".$kBooking->id."||||".createSubString($_REQUEST['szForwarderReference'],0,12);
            die;
        }
    }
    else
    {
        echo "ERROR||||";
    }
?> 
<div id="popup-bg"></div>
<div id="popup-container">	
    <p class="close-icon" align="right">
        <a onclick="showHide('invoice_comment_div');" href="javascript:void(0);">
            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
        </a>
    </p>
    <form name="update_forwarder_invoice_comment_form" id="update_forwarder_invoice_comment_form">
        <div class="compare-popup popup">
        <?php if(!empty($kBooking->arErrorMessages)){ 	?>
            <div id="regError" class="errorBox ">
                <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
                <div id="regErrorList">
                    <ul>
                    <?php
                        foreach($kBooking->arErrorMessages as $key=>$values)
                        {
                            ?><li><?=$values?></li>
                    <?php } ?>
                    </ul>
                </div>
            </div>
	 <?php } ?>
            <h5><strong><?=t($t_base.'title/booking_reference');?> <?=$_REQUEST['updateInvoiceAry']?></strong></h5>
            <p><?=t($t_base.'title/type_reference');?>:</p>
            <p>
                <textarea rows="1" name="updateInvoiceAry[szInvoiceComment]" cols="40" maxlength="80" id="fwdReference"><?=$_REQUEST['szInvoiceComment']?></textarea>
            </p>
            <br>
            <p align="center">
                <input type="hidden" id="idBillingFlag" name="updateInvoiceAry[idBillingFlag]" value="<?=$_REQUEST['idBillingFlag']?>">
                <input type="hidden" name="updateInvoiceAry[idBooking]" id="idBooking" value="<?=$_REQUEST['idBooking']?>">
                <input type="hidden" name="updateInvoiceAry[szBookingRef]" value="<?=$_REQUEST['updateInvoiceAry']?>" id="szRef">
                <a href="javascript:void(0)" class="button1" onclick="update_invoice_comment_confirm()" ><span><?=t($t_base.'fields/save');?></span></a>
                <a href="javascript:void(0)" class="button2" onclick="showHide('invoice_comment_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
            </p>
	</div>
    </form>
</div>
	<?php
}
else if($operation_mode=='UPDATE_YOUR_REFERENCE_BILLING')
{
    $idBilling=(int)$_POST['idBilling'];
     $kBilling = new cBilling();
    $kBilling->load($idBilling);
    ?> 
<div id="popup-bg"></div>
<div id="popup-container">	
    <p class="close-icon" align="right">
        <a onclick="showHide('invoice_comment_div');" href="javascript:void(0);">
            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
        </a>
    </p>
    <form name="update_forwarder_invoice_comment_form" id="update_forwarder_invoice_comment_form">
        <div class="compare-popup popup">
       
            <h5><strong><?=t($t_base.'fields/your');?> <?=t($t_base.'fields/reference');?></strong></h5>
            <p>
                <textarea rows="1" name="updateInvoiceAry[szBookingNotes]" cols="40" maxlength="80" id="szBookingNotes"><?=$kBilling->szBookingNotes?></textarea>
            </p>
            <br>
            <p align="center">
                <input type="hidden" name="updateInvoiceAry[idBilling]" id="idBilling" value="<?=$idBilling?>">
                <a href="javascript:void(0)" class="button1" onclick="update_invoice_billing_comment_confirm('<?=$idBilling?>')" ><span><?=t($t_base.'fields/save');?></span></a>
                <a href="javascript:void(0)" class="button2" onclick="showHide('invoice_comment_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
            </p>
	</div>
    </form>
</div>
	<?php
}
else if($operation_mode=='ADD_UPDATE_YOUR_REFERENCE_BILLING')
{	
    $kBilling = new cBilling();
    $idBilling=(int)$_POST['idBilling'];
    if($idBilling>0)
    {
        $update_query = " szBookingNotes = '".mysql_escape_custom(trim($_REQUEST['szBookingNotes']))."'" ;
        
        if($kBilling->updateBillingBookingNotes($update_query,$idBilling));
        {	
            echo "SUCCESS||||".createSubString($_REQUEST['szBookingNotes'],0,12);
            die;
        }
    }
    else
    {
        echo "ERROR||||";
    }
}
if(!empty($_REQUEST['bookingSearchAry']))
{
    if(!empty($kBooking->arErrorMessages))
    { 
	?>
	<div id="regError" class="errorBox ">
            <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
            <div id="regErrorList">
                <ul>
                <?php
                      foreach($kBooking->arErrorMessages as $key=>$values)
                      {
                        ?><li><?=$values?></li><?php 
                      }
                ?>
                </ul>
            </div>
	</div>
	<?php
    }
    echo forwarder_booking_bottom_html($booking_searched_data,$t_base);
}


?>
