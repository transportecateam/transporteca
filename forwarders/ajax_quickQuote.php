<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset($_SESSION )) 
{
  session_start();
} 
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
checkAuthForwarder_ajax();
$kQuote = new cQuote();
$kBooking = new cBooking();
$kVatApplication = new cVatApplication();
$mode = sanitize_all_html_input(trim($_REQUEST['mode'])); 
  
if($mode=='CLEAR_QUICK_QUOTE_FORM')
{
    echo "SUCCESS||||";
    echo display_quick_quote_contents();
    die;
}
else if($mode=='ADD_MORE_QUICK_QUOTE')
{
    $number = sanitize_all_html_input($_REQUEST['number'])+1; 
    $szShipmentType = sanitize_all_html_input(trim($_REQUEST['shipment_type'])); 
    
    echo display_quick_quote_cargo_details_form($szShipmentType,$number);
    die;
}
else if($mode=='DELETE_MANUAL_FEE')
{ 
    $idManualFee = sanitize_all_html_input(trim($_REQUEST['manual_fee_id'])); 
    if($kVatApplication->deleteManualFee($idManualFee))
    {
        $_POST['manualFeeArr'] = array();
        echo "SUCCESS||||"; 
        echo manualFeePricingHtml();
        die;
    } 
} 
else if($mode=='EDIT_MANUAL_FEE')
{ 
    $idManualFee = sanitize_all_html_input(trim($_REQUEST['manual_fee_id'])); 
    if($idManualFee>0)
    {
        $manualFeeArr = array();
        $manualFeeArr = $kVatApplication->getManualFeeListing(false,$idManualFee); 
        
        $_POST['manualFeeArr'] = array();
        echo "SUCCESS||||"; 
        echo display_add_manual_fee_form($kVatApplication,$manualFeeArr[0]);
        die; 
    }  
}
else if($mode=='RELOAD_QUICK_QUOTE_EMAIL_TEMPLATE')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['booking_id']));
    $szBookingType = sanitize_all_html_input(trim($_REQUEST['booking_type']));
    $idBookingLanguage = sanitize_all_html_input(trim($_REQUEST['booking_language']));
    $szPaymentType = sanitize_all_html_input(trim($_REQUEST['payment_type'])); 
    
    $bookingDataAry = array();
    $bookingDataAry = $kBooking->getExtendedBookingDetails($idBooking);
    
    echo "SUCCESS||||";
    echo display_quick_quote_email_templates($szBookingType,$bookingDataAry,$idBookingLanguage,$szPaymentType,false,false,true);
    die;
}
else if(!empty($_POST['quickQuoteShipperConsigneeAry']))
{  
    if($mode=='CONVERT_RFQ')
    {  
        if($kQuote->quickQuoteBuildConvertToRFQData($_POST['quickQuoteShipperConsigneeAry'],$mode))
        {
            echo "SUCCESS||||";
            echo display_convert_rfq_confirmation_popup($convertToRFQAry,$kQuote);
            die;
        } 
    }
    else if($mode=='CONVERT_RFQ_CONFIRM')
    { 
        $_POST['quickQuoteShipperConsigneeAry']['iSendQuickQuote']='1';
        if($kQuote->quickQuoteConvertToRFQ($_POST['quickQuoteShipperConsigneeAry'],$mode))
        {
            $szBookingRandomNum = $kQuote->szBookingRandomNum; 
            echo "SUCCESS||||";
            echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__."".$szBookingRandomNum."/";
            die;
        } 
    }
    else if($mode=='SEND_QUOTE' || $mode=='CREATE_BOOKING')
    { 
        if($kQuote->quickQuoteConvertToRFQ($_POST['quickQuoteShipperConsigneeAry'],$mode,true))
        { 
            $idBooking = $kQuote->idQuickQuoteBooking; 
            $bookingDataAry = array();
            $bookingDataAry = $kBooking->getExtendedBookingDetails($idBooking);
            $_SESSION['bookingdetails'] = $bookingDataAry;
            $_POST['quickQuoteShipperConsigneeAry'] = array();
            unset($_POST['quickQuoteShipperConsigneeAry']);
            echo "SUCCESS||||";
            //echo display_quick_quote_shipper_consignee($bookingDataAry,$mode);
            echo display_quick_quote_email_templates($mode,$bookingDataAry,false,false,false,false,true,true);
            die;
        }  
    }
    else if($mode=='SEND_QUOTE_CONFIRM' || $mode=='CREATE_BOOKING_CONFIRM')
    { 
        if($mode=='SEND_QUOTE_CONFIRM')
        {
            $szBookingType = "SEND_QUOTE";
        }
        else
        {
            $szBookingType = "CREATE_BOOKING";
        } 
        $idBookingLanguage = $_POST['quickQuoteShipperConsigneeAry'][$szBookingType]['idBookingLanguage'];
        $szPaymentType = $_POST['quickQuoteShipperConsigneeAry'][$szBookingType]['szPaymentType'];
        
        $_POST['quickQuoteShipperConsigneeAry'][$szBookingType]['szEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['quickQuoteShipperConsigneeAry'][$szBookingType]['szEmailBody'])));
         
        if($kQuote->sendQuickQuote($_POST['quickQuoteShipperConsigneeAry'],$mode,true))
        {
            if($mode=='SEND_QUOTE_CONFIRM')
            {
                echo "SUCCESS||||";
                echo display_quick_quote_email_templates($szBookingType,$bookingDataAry,$idBookingLanguage,$szPaymentType,1,false,true); 
                echo "||||";
                echo success_message_quick_quote_popup(1);
                die;
            }
            else
            {
                $idBooking = $kQuote->idQuickQuoteBooking; 
                $bookingDataAry = array();
                $bookingDataAry = $kBooking->getBookingDetails($idBooking); 
                $szBookingRandomNum = $bookingDataAry['szBookingRandomNum']; 
                echo "SUCCESS||||";
                echo success_message_quick_quote_popup(2);
                //echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__."".$szBookingRandomNum."/";
                die;
            } 
        }
        else
        { 
            echo "ERROR||||";
            echo display_quick_quote_shipper_consignee($bookingDataAry,$szBookingType,$kQuote,$iInsuranceNotAvailable);
            die;
        }
    }
    die;
}
else if(!empty($_POST['quickQuoteAry']))
{
    $searchResultAry = array();
    $searchResultAry = $kQuote->searchQuickQuoteRequest($_POST['quickQuoteAry']); 
    if(!empty($kQuote->arErrorMessages))
    {
        echo "ERROR$$$$";
        echo display_quick_quote_search_form($kQuote);
        die;
    } 
    else
    {
        $iScrollTop = 0;
        if(count($searchResultAry)>=25)
        {
            $iScrollTop = 1;
        }
        echo "SUCCESS$$$$";
        echo display_quick_quote_search_result($searchResultAry,$kQuote);
        echo "$$$$".$iScrollTop;
        die; 
    }
}
else if(!empty($_POST['manualFeeArr']))
{ 
    $iDefaultManualPricing = $_POST['manualFeeArr']['iDefaultManualPricing'];
    
    if($kVatApplication->addUpdateManualFee($_POST['manualFeeArr']))
    {
        $_POST['manualFeeArr'] = array();
        if($iDefaultManualPricing==1)
        {
            $defaultManualFeeAry = array();
            $defaultManualFeeAry = $kVatApplication->getDefaultManualFee();
            echo "SUCCESS||||"; 
            echo display_default_manual_fee_form($kVatApplication,$defaultManualFeeAry);
            die;
        }
        else
        {
            echo "SUCCESS||||"; 
            echo manualFeePricingHtml();
            die;
        } 
    }
    else
    { 
        if($iDefaultManualPricing==1)
        {
            echo "ERROR||||";
            echo display_default_manual_fee_form($kVatApplication);
            die;
        }
        else
        {
            echo "ERROR||||";
            echo display_add_manual_fee_form($kVatApplication);
            die;
        } 
    }
}