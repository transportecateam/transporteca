<?php 
/**
  *forwarder myProfile 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
//session_destroy();
//print_r($_SESSION);

$szMetaTitle="Transporteca | Profile Settings";
//$display_profile_not_completed_message = true;
	if( !defined("__APP_PATH__") )
		define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
	require_once (__APP_PATH__ ."/inc/constants.php");
	require_once(__APP_PATH_LAYOUT__ ."/forwarder_header.php");
	checkAuthForwarder();
$t_base="Forwarders/homepage/";

$kForwarderCont = new cForwarderContact();
$kForwarderCont->getMainAdmin($_SESSION['forwarder_id']);

//print_r($kForwarderContact);
//echo !$kForwarderContact->iConfirmed;

/**
* $_REQUEST['err']=1 means no user profile not completed 
*
*
**/

if($_REQUEST['err']==1)
{
?>
<div id="incomplete_profile_error_div">	
	
	<?
	echo incomplete_comapny_details_popup($t_base,true);
	?>
</div>
	<?
}
if($_REQUEST['err']==2)
{
?>
<div id="incomplete_profile_error_div">	
	
	<?
	echo non_verified_email_popup($t_base,$kForwarderContact);
	?>
</div>
	<?
}
?>

<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/forwarder_profile_nav.php" ); ?> 
	<div class="hsbody-2-right">
		<div id="myprofile_info">
			<div id="user_information">
				<h4><strong><?=t($t_base.'fields/forwarder_info');?></strong> <a href="javascript:void(0)" onclick="editForwarderUserInformation();"><?=t($t_base.'fields/edit');?></a></h4>	
	
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
					<span class="field-container"><?=$kForwarderContact->szFirstName?></span>
					</div>
					
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
					<span class="field-container"><?=$kForwarderContact->szLastName?></span>
					</div>
					
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/country');?></span>
					<span class="field-container"><?=$kForwarderContact->szCountryName?></span>
					</div>
			</div>
			</br>
			</br>
		  <div id="contact_information">
				<h4><strong><?=t($t_base.'fields/contact');?></strong> <a href="javascript:void(0)" onclick="editForwarderContactInformation();"><?=t($t_base.'fields/edit');?></a></h4>	
		
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/email');?></span>
					<span class="field-container"><?=$kForwarderContact->szEmail?><?php if($kForwarderContact->iConfirmed!='1')
{?><span style='color:red;font-size:13px;'> (<?=t($t_base.'messages/not_verified');?>)</span> <? }?></span>		</span>
					</div>
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
						<span class="field-container">
						<? 
						if($kForwarderContact->szBookingFrequency=='1')
						{
							echo "Daily";
						}
						else if($kForwarderContact->szBookingFrequency=='2')
						{
							echo "Weekly ";
						}
						else if($kForwarderContact->szBookingFrequency=='3')
						{
							echo "Monthly";
						}
						else
						{
							echo "Never";
						}
						  
						?></span>
					</div>
					
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/phone');?></span>
					<span class="field-container"><?=$kForwarderContact->szPhone?></span>
					</div>
					
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/mobile');?> <span class="optional">(<?=t($t_base.'fields/optional');?>)</span></span>
					<span class="field-container"><?=$kForwarderContact->szMobile?></span>
					</div>
			</div>
			</br>
			</br>
			<div id="password_information">
				<h4 style="width:30%;float:left;"><strong><?=t($t_base.'fields/password');?></strong> <a href="javascript:void(0);" onclick="change_forwarder_password();"><?=t($t_base.'fields/edit');?></a></h4>
						<span class="field-container" >*************</span>
			</div>
			<div style="clear: both"></div>
			<br/>
			<?if($kForwarderCont->mainAdminEmail){?><div id="mainAdmin"><p>For questions about your access, please contact your administrator on <?=$kForwarderCont->mainAdminEmail;?></p></div><?}?>
		</div>
		
		
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>