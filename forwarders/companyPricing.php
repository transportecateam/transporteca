<?php
/**
 * Forwarder Comapany Pricing 
 */
 ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle='Transporteca | Setup Users and Access rights';
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
checkAuthForwarder();
$t_base = "companyPricing/";
$kForwarderContact = new cForwarderContact;
$CompanyPricingInformation	=	$kForwarderContact->getCompanyPricingDtails($idForwarder);
?>
	<div id="hsbody-2">
		<div id="forwarder_company_div" style="display:none;"></div>
   		<?php require_once( __APP_PATH_LAYOUT__ ."/forwarder_company_nav.php" ); ?> 
			<div class="hsbody-2-right">
			<h5 style="margin-bottom:5px;"><strong>Transporteca Pricing</strong></h5>
<!--		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/referal_fee');?></span>
			<span class="field-container"><i><?=($CompanyPricingInformation['fReferalFee']?$CompanyPricingInformation['fReferalFee']." %":'N/A')?></i></span>
		</div>-->
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/monthly_sub');?></span>
			<span class="field-container"><i>N/A</i></span>
		</div>
                <div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/payment_cycle');?></span>
                        <span class="field-container"><i><?=t($t_base.'fields/end_of_month_plus');?> <?=$CompanyPricingInformation['iCreditDays'];?> <?php if((int)$CompanyPricingInformation['iCreditDays']>1) { echo t($t_base.'fields/days'); }else { echo t($t_base.'fields/day');} ?></i></span>
		</div>
		<div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/validto');?></span>
                    <span class="field-container"><i><?=(($CompanyPricingInformation['dtReferealFeeValid']!='' && $CompanyPricingInformation['dtReferealFeeValid']!='0000-00-00 00:00:00' )?date('d. F Y',strtotime($CompanyPricingInformation['dtReferealFeeValid'])):'N/A')?></i></span>
		</div> 
                <div class="profile-fields">
                    <i><?=($CompanyPricingInformation['szReferalFeeComment']?$CompanyPricingInformation['szReferalFeeComment']:'N/A')?></i>
                </div> 
            </div>
	</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>			