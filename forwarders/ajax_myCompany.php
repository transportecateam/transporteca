<?php
/**
 * Edit User Information
 */
  ob_start();
 if(!isset($_SESSION))
 {
	session_start();
 }
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
$t_base = "ForwardersCompany/UserAccess/";

checkAuthForwarder_ajax();

$forwarderCompanyAry = array();
if(!empty($_REQUEST['mode']))
{
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
	$idForwarderRole =  sanitize_all_html_input(trim($_REQUEST['role']));
	$idForwarder = sanitize_all_html_input(trim($_REQUEST['id']));
	$idForwarderContact = sanitize_all_html_input(trim($_REQUEST['idForwarderContact']));
}
else
{
	$forwarderCompanyAry = $_REQUEST['forwarderCompanyAry'] ;
	$operation_mode = sanitize_all_html_input(trim($forwarderCompanyAry['szMode']));
	$idForwarderRole = sanitize_all_html_input(trim($forwarderCompanyAry['idForwarderRole']));
	$idForwarder = sanitize_all_html_input(trim($forwarderCompanyAry['idForwarder']));
	$idForwarderContact = sanitize_all_html_input(trim($forwarderCompanyAry['idForwarderContact']));
}
if($operation_mode =='EDIT_USER')
{
	$kConfig = new cConfig();
	$kForwarderContact = new cForwarderContact();
	$allCountriesArr=$kConfig->getAllCountries(true);
	if(!empty($forwarderCompanyAry))
	{
		if($kForwarderContact->updateForwarderContact($forwarderCompanyAry))
		{
		   $redirect_url = __FORWARDER_COMPANY_PAGE_URL__
			?>
			<script type="text/javascript">
				redirect_url('<?=$redirect_url?>');
			</script>
			<?
			die;
		}
	}
	
	if(!empty($forwarderCompanyAry['szPhoneUpdate']))
	{
		$forwarderCompanyAry['szPhone']=urldecode(base64_decode($forwarderCompanyAry['szPhoneUpdate']));
	}	
	
	if(!empty($forwarderCompanyAry['szMobileUpdate']))
	{
		$forwarderCompanyAry['szMobile']=urldecode(base64_decode($forwarderCompanyAry['szMobileUpdate']));
	}
	if(empty($forwarderCompanyAry))
	{
		$forwarderCompanyAry = $kForwarderContact->getForwarderContactDetails($idForwarderContact);
		$idForwarderRole = $forwarderCompanyAry['idForwarderContactRole'] ;
	}
	$forwarderRoleAry = $kForwarderContact->getAllForwarderRole();
	//print_r($forwarderRoleAry);
	?>
<div id="popup-bg"></div>
<div id="popup-container">	
<div class="company-edit popup" style="text-align:left;">
<p class="close-icon" align="right">
<a onclick="showHide('forwarder_company_div');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
	<h5><strong><?=t($t_base.'fields/edit_profile');?></strong></h5>
		<?php
		if(!empty($kForwarderContact->arErrorMessages))
		{
			?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kForwarderContact->arErrorMessages as $key=>$values)
			      {
				      ?><li><?=$values?></li>
				      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
			<?
		}
			?>
		<form name="addForwarderContactInfo" style="text-align:left;" id="addForwarderContactInfo" method="post">
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/f_name');?></p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szFirstName]" id="szFirstName" value="<?=$forwarderCompanyAry['szFirstName']?>"/></p>
			</div>				
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/l_name');?></p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szLastName]" id="szLastName" value="<?=$forwarderCompanyAry['szLastName']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/country');?></p>
				<p class="fl-60">
					<select name="forwarderCompanyAry[idCountry]" id="idCountry">
					<option value=""><?=t($t_base.'fields/select_country');?></option>
					<?php
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$forwarderCompanyAry['idCountry']) || ($allCountriesArrs['id']==$postSearchAry['idOriginCountry'])){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
								<?php
							}
						}
					?>
				   </select>
				</p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/email');?> </p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szEmail]" id="szEmail" value="<?=$forwarderCompanyAry['szEmail']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/office_phone_number');?></p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szPhone]" id="szPhone" value="<?=$forwarderCompanyAry['szPhone']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/mobile_phone_number');?> <span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szMobile]" id="szMobile" value="<?=$forwarderCompanyAry['szMobile']?>"/></p>
			</div>
			<br><br>
			<div class="oh">
				<p class="fl-40"><strong><?=t($t_base.'fields/access_rights');?></strong></p>
				<p class="fl-60">
					<select name="forwarderCompanyAry[idForwarderRole]" id="idCountry">
					<?php
						if(!empty($forwarderRoleAry))
						{
							foreach($forwarderRoleAry as $forwarderRoleArys)
							{
								?><option value="<?=$forwarderRoleArys['id']?>" <?php if(($forwarderRoleArys['id']==$idForwarderRole)){?> selected <?php }?>><?=$forwarderRoleArys['szRoleName']?></option>
								<?php
							}
						}
					?>
				   </select>
				</p>
			</div>
		<br/>
		<p align="center">
			<a href="javascript:void(0)" class="button1" onclick="encode_string('szPhone','szPhoneUpdate','szMobile','szMobileUpdate');add_forwarder_contact_details();"><span><?=t($t_base.'fields/save');?></span></a>&nbsp;
			<a href="javascript:void(0)" class="button2" onclick="showHide('forwarder_company_div')"><span><?=t($t_base.'fields/cancel');?></span></a></p>
			<input type="hidden" name="forwarderCompanyAry[szMode]" value="<?=$operation_mode?>">
			<input type="hidden" name="forwarderCompanyAry[idForwarder]" value="<?=$forwarderCompanyAry['idForwarder']?>">
			<input type="hidden" name="forwarderCompanyAry[id]" value="<?=$forwarderCompanyAry['id']?>">
			<input type='hidden' name='forwarderCompanyAry[szPhoneUpdate]' id="szPhoneUpdate" value="">
			<input type='hidden' name='forwarderCompanyAry[szMobileUpdate]' id="szMobileUpdate" value="">
		</form>	
	</div>
</div>
<?php
}
elseif($operation_mode =='ADD_USER')
{

/*
	$kConfig = new cConfig();
	$kForwarderContact = new cForwarderContact();
	$allCountriesArr=$kConfig->getAllCountries(true);
	if(!empty($forwarderCompanyAry))
	{
		if($kForwarderContact->addForwarderContact($forwarderCompanyAry))
		{
			$redirect_url = __FORWARDER_COMPANY_PAGE_URL__
			?>
			<script type="text/javascript">
				redirect_url('<?=$redirect_url?>');
			</script>
			<?
		}
	}	
	?>
<div id="popup-bg"></div>
<div id="popup-container">	
<div class="compare-popup popup">
	<h5><strong>Add Profile</strong></h5>
		<?php
		if(!empty($kForwarderContact->arErrorMessages))
		{
		?>
		<div id="regError" class="errorBox ">
		<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kForwarderContact->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?
		}
		?>
		<form name="addForwarderContactInfo" id="addForwarderContactInfo" method="post">
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/f_name');?></p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szFirstName]" id="szFirstName" value="<?=$forwarderCompanyAry['szFirstName']?>"/></p>
			</div>				
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/l_name');?></p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szLastName]" id="szLastName" value="<?=$forwarderCompanyAry['szLastName']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/country');?></p>
				<p class="fl-60">
					<select name="forwarderCompanyAry[idCountry]" id="idCountry">
					<?php
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$forwarderCompanyAry['idCountry']) || ($allCountriesArrs['id']==$postSearchAry['idOriginCountry'])){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
								<?php
							}
						}
					?>
				   </select>
				</p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/email');?> </p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szEmail]" id="szEmail" value="<?=$forwarderCompanyAry['szEmail']?>"/></p>
			</div>
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/office_phone_number');?></p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szPhone]" id="szPhone" value="<?=$forwarderCompanyAry['szPhone']?>"/></p>
			</div>
			
			<div class="oh">
				<p class="fl-40"><?=t($t_base.'fields/mobile_phone_number');?><span class="optional">(<?=t($t_base.'fields/optional');?>)</span>&nbsp;</p>
				<p class="fl-60"><input type="text" name="forwarderCompanyAry[szMobile]" id="szMobile" value="<?=$forwarderCompanyAry['szMobile']?>"/></p>
			</div>
		<br/>
		<p align="center">
			<a href="javascript:void(0)" class="button1" onclick="javscript:add_forwarder_contact_details();"><span><?=t($t_base.'fields/save');?></span></a>&nbsp;
			<a href="javascript:void(0)" class="button2" onclick="showHide('forwarder_company_div')"><span><?=t($t_base.'fields/cancel');?></span></a></p>
			<input type="hidden" name="forwarderCompanyAry[szMode]" value="<?=$operation_mode?>">
			<input type="hidden" name="forwarderCompanyAry[idForwarderRole]" value="<?=$idForwarderRole?>">
		</form>	
	</div>
</div>
	
	<?
*/
$kForwarderContact->loadContactRole($idForwarderRole);
/*
if($idForwarderRole==1)
{
	$szForwardrRole = 'Administrator';
}
elseif($idForwarderRole==3)
{
	$szForwardrRole = 'Pricing & Service';
}
elseif($idForwarderRole==5)
{
	$szForwardrRole = 'Booking & Billing';
}
*/
$szForwardrRole = $kForwarderContact->szRoleName ;
$success_sent = false;
if(!empty($forwarderCompanyAry))
{
	if($kForwarderContact->addForwarderEmail($forwarderCompanyAry))
	{
		$success_sent = true;
	}
}	
?>
<div id="popup-bg"></div>
<div id="popup-container">	
<div class="popup compare-popup">
<p class="close-icon" align="right">
<a onclick="showHide('forwarder_company_div');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
	<h5><strong><?=t($t_base.'fields/add_new');?> <?=$szForwardrRole?></strong></h5>
	<?
		if($success_sent)
		{
			?>
			<p><?=t($t_base.'titles/add_new_profile_success_message');?></p>
			<br>
			<p align="center">
				<a href="javascript:void(0)" class="button2" onclick="redirect_url('<?=__FORWARDER_COMPANY_PAGE_URL__?>')"><span><?=t($t_base.'fields/close');?></span></a>				
			</p>
			<?
			die;
		}
	?>
		<?php
		if(!empty($kForwarderContact->arErrorMessages))
		{
		?>
		<div id="regError" class="errorBox ">
		<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kForwarderContact->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?
		}
		?>
		<form action="javascript:void(0);" name="addForwarderContactInfo" id="addForwarderContactInfo" method="post">
			<p><?=t($t_base.'titles/add_new_profile_popup_message');?></p>
			<br>
			<div class="oh">
				<p class="fl-30"><?=t($t_base.'fields/email');?> </p>
				<p class="fl-70"><input type="text" size="30" style="width:238px;" name="forwarderCompanyAry[szEmail]" onkeyup="on_enter_key_press(event,'add_forwarder_contact_details')" id="szEmail" value="<?=(isset($forwarderCompanyAry['szEmail']))?$forwarderCompanyAry['szEmail']:'';?>"/></p>
			</div>
			<p align="center">
				<a href="javascript:void(0)" class="button1" onclick="javscript:add_forwarder_contact_details();"><span><?=t($t_base.'fields/add');?></span></a>&nbsp;
				<a href="javascript:void(0)" class="button2" onclick="showHide('forwarder_company_div')"><span><?=t($t_base.'fields/cancel');?></span></a>
				<input type="hidden" name="forwarderCompanyAry[szMode]" value="<?=$operation_mode?>">
				<input type="hidden" name="forwarderCompanyAry[idForwarderRole]" value="<?=$idForwarderRole?>">
			</p>
		</form>
	</div>
</div>
<?			
}
else if($operation_mode =='DELETE_USER')
{
	$kForwarderContact = new cForwarderContact();
	if(!empty($forwarderCompanyAry))
	{
		if($kForwarderContact->deleteForwarderContact($forwarderCompanyAry['idForwarderContact']))
		{
		   $redirect_url = __FORWARDER_COMPANY_PAGE_URL__;
		   if(($forwarderCompanyAry['idForwarderContact']==$_SESSION['forwarder_user_id']) && ((int)$_SESSION['forwarder_admin_id']==0))
		   {
		   		$_SESSION['forwarder_user_id']='';
		   		$_SESSION['forwarder_id'] = '';
		   		unset($_SESSION['forwarder_user_id']);
		   		unset($_SESSION['forwarder_id']);
		   }
			?>
			<script type="text/javascript">
				redirect_url('<?=$redirect_url?>');
			</script>
			<?
			die;
		}
	}
	//echo " id ".$idForwarderContact ;
	$kForwarderContact->load($idForwarderContact);	
	
	$szFullName = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName ;
	if(empty($kForwarderContact->szFirstName))
	{
		$szFullName = $kForwarderContact->szEmail ;
	}
?>	
<div id="popup-bg"></div>
<div id="popup-container">	
<form name="delete_forwarder_contact_form" id="delete_forwarder_contact_form">
	<div class="compare-popup popup">
	<p class="close-icon" align="right">
	<a onclick="showHide('forwarder_company_div');" href="javascript:void(0);">
	<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
	</a>
	</p>
		<h5><strong><?=t($t_base.'fields/delete_profile');?></strong></h5>
		<p><?=t($t_base.'titles/delete_profile_confirm_message');?> <?=$szFullName?>?</p>
		<br>
		<p align="center">
			<a href="javascript:void(0)" class="button2" onclick="showHide('forwarder_company_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
			<a href="javascript:void(0)" class="button1" onclick="confirm_delete()"><span><?=t($t_base.'fields/delete');?></span></a>
		</p>
	</div>
	<input type="hidden" name="forwarderCompanyAry[szMode]" value="<?=$operation_mode?>">
	<input type="hidden" name="forwarderCompanyAry[idForwarderContact]" value="<?=$idForwarderContact?>">
	</form>
</div>
	<?
}
?>