<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca | Update Haulage - Line by Line";

require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );

$t_base = "BulkUpload/";
$t_base_error="Error";
$Excel_export_import=new cExport_Import();
	$kConfig = new cConfig();
if((int)$_SESSION['forwarder_user_id']==0)
{
	header('Location:'.__BASE_URL__.'/forwarders/');
	exit();	
}
$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder);
$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$forwardCountriesArr[0]['idCountry']);

$forwardCitiesArr=$Excel_export_import->getForwaderWarehousesCity($idForwarder,$forwardCountriesArr[0]['idCountry']);
//print_r($forwardCountriesArr[0]);
?>
<div id="delete_confirm" style="display:none;">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
<p class="close-icon" align="right">
<a onclick="cancel_remove_user_popup('delete_confirm','hsbody-2')" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<h5><?=t($t_base.'title/delete_haulage_rate');?></h5>
<p><?=t($t_base.'messages/are_you_sure_you_wish_to_delete_this_haulage_rate')?>?</p><br/>
<p align="center"><a href="javascript:void(0)" class="button1" tabindex="6" onclick="cancel_remove_user_popup('delete_confirm','hsbody-2')"><span><?=t($t_base.'fields/close')?></span></a> <a href="javascript:void(0)" class="button1" tabindex="7" id="delete_confirm_button"><span><?=t($t_base.'fields/confirm');?></span></a></p>
	
</div>
</div>
</div>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="hsbody-2">
	<div class="hsbody-2-left account-links">
		<?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
	</div>
	<div class="hsbody-2-right">
		<h5><?=t($t_base.'title/update_pricing_haulage_one_by_one')?>.</h5>
			<form name="haulageOBO" id="haulageOBO" method="post">
				<div class="oh haulage-get-data">
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/country')?></span>
						<select name="dataExportArr[haulageCountry]" id="haulageCountry" onchange="showForwarderWarehouseOBO('<?=$idForwarder?>',this.value,'country','city_warehouse')">
							<option value=""><?=t($t_base.'fields/select')?></option>		
							<?php
										if(!empty($forwardCountriesArr))
										{
											foreach($forwardCountriesArr as $forwardCountriesArrs)
											{
												?>
												<option value="<?=$forwardCountriesArrs['idCountry']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div id='city_warehouse'>
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/city')?></span>
						<select name="dataExportArr[szCity]" disabled id="szCity" onchange="showForwarderWarehouseOBO('<?=$idForwarder?>',this.value,'city','warehouse')">
									<option value=""><?=t($t_base.'fields/all');?></option>
									<?php
										if(!empty($forwardCitiesArr))
										{
											foreach($forwardCitiesArr as $forwardCitiesArrs)
											{
												?>
												<option value="<?=$forwardCitiesArrs['szCity']?>"><?=$forwardCitiesArrs['szCity']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div class="fl-15 s-field" id="warehouse">
						<span class="f-size-12"><?=t($t_base.'fields/cfs_name')?></span>
						<select name="dataExportArr[haulageWarehouse]" disabled id="haulageWarehouse" onchange="enableHaulageGetData('<?=$idForwarder?>')">
							<option value=""><?=t($t_base.'fields/select');?></option>
							<?php
								if(!empty($forwardWareHouseArr))
								{
									foreach($forwardWareHouseArr as $forwardWareHouseArrs)
									{
										?>
										<option value="<?=$forwardWareHouseArrs['id']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
										<?
									}
								}
							?>
						</select>
					</div>
					</div>
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
						<select name="dataExportArr[idDirection]" disabled id="idDirection">
							<option value='1'><?=t($t_base.'fields/export_data')?></option>
							<option value='2'><?=t($t_base.'fields/import_data')?></option>
							
						</select>
					</div>
					<div class="fl-15" align="center" style="padding-top:16px;width:125px;"><?=t($t_base.'fields/click_to_see_rates')?></div>
					<div class="fl-20" align="right" style="width:18%;padding-top:9px;">						
						<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="get_haulage_data"><span id="text_change"><?=t($t_base.'fields/get_data')?></span></a>
					</div>
				</div>
				<hr />
				
				
				<div  id="haulageTable">
					<h5><strong><?=t($t_base.'title/haulage_to_and_from_warehouse')?></strong></h5>
					<table cellspacing="0" cellpadding="0" border="0" class="format-2 haulage_obo" width="100%">
						<tr>
							<td rowspan="2" valign="top" class="haulage_col1"><strong><?=t($t_base.'fields/distance_up_to')?></strong></td>
							<td colspan="4" valign="top"><strong><?=t($t_base.'fields/haulage_rate')?></strong></td>
							<td rowspan="2" valign="top" class="haulage_col3"><strong><?=t($t_base.'fields/cross_border')?></strong></td>
							<td rowspan="2" valign="top" class="haulage_col4"><strong><?=t($t_base.'fields/transit_time')?></strong></td>
							<td rowspan="2" valign="top" class="haulage_col5"><strong><?=t($t_base.'fields/expiry_date')?></strong></td>
						</tr>
						<tr>
							<td valign="top" class="haulage_col2_1"><strong><?=t($t_base.'fields/per_wm')?></strong></td>
							<td valign="top" class="haulage_col2_2"><strong><?=t($t_base.'fields/minimum')?></strong></td>
							<td valign="top" class="haulage_col2_3"><strong><?=t($t_base.'fields/per_booking')?></strong></td>
							<td valign="top" class="haulage_col2_4"><strong><?=t($t_base.'fields/currency')?></strong></td>
						</tr>
						<tr id="haulage_data_1">
							<td>&nbsp;
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
								<br />
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
						</tr>
					</table>
				</div>
				<br />
				<div align="right">
					<a href="javascript:void(0)" class="button2" style="opacity:0.4;" onclick="" id="haulage_remove_button"><span><?=t($t_base.'fields/delete_line')?></span></a> <a href="javascript:void(0)" class="button1" onclick="" style="opacity:0.4;" id="haulage_edit_button"><span><?=t($t_base.'fields/edit')?></span></a>
				</div>
				<br />
				</form>
				<? require_once('haulageOBOAddEdit.php');?>
				
	</div>
</div>
<script type="text/javascript">
$("#addeditHaulageData :input").attr("disabled", "disabled");	
$("#cancel_button").attr("style","opacity:0.4;");
	$("#add_edit_button").attr("style","opacity:0.4;");
	$("#clear_form").attr("style","opacity:0.4;");
</script>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>