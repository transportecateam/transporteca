<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$szMetaTitle="Transporteca | CFS Locations";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_forwarderHeader.php" );
checkAuthForwarder();
$kConfig = new cConfig();
$case = 'FORWARDER';
$id = (int)$_SESSION['forwarder_user_id'];
$data = $_POST['update'];
if($kConfig->updateForgotPassword($id,$data,$case) == true)
{
	echo "SUCCESS";
}
$t_base_error = "management/Error/";
if(!empty($kConfig->arErrorMessages))
{
?>
<div id="regError" class="errorBox" style="display:block;">
<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
      foreach($kConfig->arErrorMessages as $key=>$values)
      {
      ?><li><?=$values?></li>
      <?php 
      }
?>
</ul>
</div>
</div>
<?
	}
?>