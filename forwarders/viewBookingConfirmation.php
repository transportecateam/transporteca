<?php
/**
 * View Booking Confirmation
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );


$kBooking = new cBooking();
checkAuth();
$idBooking=$_REQUEST['idBooking'];
if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
{
$idBooking=$_REQUEST['idBooking'];
$flag=$_REQUEST['flag'];
$pdfhtmlflag=true;
$flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
if($flag=='pdf')
{
	$pdfhtmlflag=false;
}
$bookingConfirmationPdf=getBookingConfirmationPdfFileHTML($idBooking,$pdfhtmlflag);
	if($flag=='pdf')
	{
		 download_booking_pdf_file($bookingConfirmationPdf);
        die;
	}
}
else
{
	header('Location:'.__BASE_URL__.'/home/');
	exit();

}
/*
$t_base = "BookingDetails/";
$kBooking = new cBooking();
$kConfig = new cConfig();

$kForwarder= new cForwarder();
checkAuth();
$idBooking=$_REQUEST['idBooking'];
if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
{
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
	
	

$shipperCountry=$kConfig->getCountryName($bookingDataArr['idShipperCountry']);

$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);

$consigneesCountry=$kConfig->getCountryName($bookingDataArr['idConsigneeCountry']);  

$shipperCountry=$kConfig->getCountryName($bookingDataArr['idShipperCountry']);

$kForwarder->load($bookingDataArr['idForwarder']); 
?>
<html>
	<head>
		<title><?=t($t_base.'fields/booking_confirmation');?></title>
	</head>
	<script>
	function PrintDiv()
	{    
		      var divToPrint = document.getElementById('viewBookingConfimation');
		      var popupWin = window.open('', '_blank', 'width=900,height=700');
		      popupWin.document.open();
		      popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
		      popupWin.document.close();
	}
	</script>
	<body>
		<p style="text-align:right;margin-right:300px;"><a href="javascript:void(0)" onclick="PrintDiv();"><?=t($t_base.'fields/print')?></a></p>
		<div id="viewBookingConfimation">
			<div style="margin:0px;padding:0px;font-family:calibri,arial;font-size:14px;color:#000;text-align:center;">
			<div style="margin:0 auto;width:600px;text-align:left;">
				<h1 style="font-size:20px;text-align:center;"><?=t($t_base.'fields/booking_confirmation');?></h1>
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'fields/booking_date')?>:</p>
					<p style="margin:0;float:right;width:295px;"><?=date('d F Y H:i',strtotime($bookingDataArr['dtBookingConfirmed']))?></p>
				</div>
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'fields/booked_by');?>:</p>
					<p style="margin:0;float:right;width:295px;"><?=$bookingDataArr['szFirstName']." ".$bookingDataArr['szLastName']."(".$bookingDataArr['szEmail'].")"?></p>
				</div>
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'fields/booing_reference')?>:</p>
					<p style="margin:0;float:right;width:295px;"><?=$bookingDataArr['szBookingRef']?></p>
				</div>
				<br />
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'fields/booking_width');?>:</p>
					<p style="margin:0;float:right;width:295px;"><?=$bookingDataArr['szForwarderDispName']?><br />
						<?=$bookingDataArr['szForwarderAddress']." ".$bookingDataArr['szForwarderAddress2']?><br />
						<?=$bookingDataArr['szForwarderPostCode']." ".$bookingDataArr['szForwarderCity']?><br />
						<?=$forwardCountry?></p>
				</div>
				<p><strong><?=t($t_base.'fields/service');?>:</strong> <?=$bookingDataArr['szServiceName']?></p>
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;float:left;width:295px;"><strong><?=t($t_base.'fields/shipper');?>:</strong><br />
						<?=$bookingDataArr['szShipperCompanyName']?><br />
						<?=$bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']?><br />
						<?=$bookingDataArr['szShipperPhone']?><br />
						<?=$bookingDataArr['szShipperAddress']." ".$bookingDataArr['szShipperAddress2']?><br />
						<?=$bookingDataArr['szShipperPostCode']." ".$bookingDataArr['szShipperCity']?><br />
						<?=$shipperCountry?>
					</p>
					<p style="margin:0;float:right;width:295px;"><strong><?=t($t_base.'fields/consignee');?>:</strong><br />
						<?=$bookingDataArr['szConsigneeCompanyName']?><br />
						<?=$bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szShipperLastName']?><br />
						<?=$bookingDataArr['szConsigneePhone']?><br />
						<?=$bookingDataArr['szConsigneeAddress']." ".$bookingDataArr['szConsigneeAddress2']?><br />
						<?=$bookingDataArr['szConsigneePostCode']." ".$bookingDataArr['szConsigneeCity']?><br />
						<?=$consigneesCountry?>
					</p>
				</div>
				<br />
				<p><strong><?=t($t_base.'fields/cargo');?>:</strong><br />
					<?
					$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
					if(!empty($cargoDetailArr))
					{
						$ctr=0;
						foreach($cargoDetailArr as $cargoDetailArrs)
						{
							$str='';
							$str=++$ctr.". ".$cargoDetailArrs['fWeight']."".strtoupper($cargoDetailArrs['wmdes']).",  ".$cargoDetailArrs['fLength']."x".$cargoDetailArrs['fWidth']."x".$cargoDetailArrs['fHeight']."".$cargoDetailArrs['cmdes'].",".$cargoDetailArrs['szCommodity'];	
							echo $str."<br/>";
							
						}
					}
					?>
					</p>
				
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'fields/cut_off');?>:</p>
					<p style="margin:0;font-weight:bold;float:right;width:295px;"><?=t($t_base.'fields/avial');?>:</p>
				</div>
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;float:left;width:295px;"><?=date('d F Y H:i',strtotime($bookingDataArr['dtCutOff']))?><br/>
						<?=$bookingDataArr['szWarehouseFromName']?><br />
						<?=$bookingDataArr['szWarehouseFromAddress']." ".$bookingDataArr['szWarehouseFromAddress2']?><br />
						<?=$bookingDataArr['szWarehouseFromAddress3']?><br />
						<?=$bookingDataArr['szWarehouseFromPostCode']." ".$bookingDataArr['szWarehouseFromCity']?><br/>
						<?=$fromWareHouseCountry?>
						
					</p>
					<p style="margin:0;float:right;width:295px;">
						<?=date('d F Y H:i',strtotime($bookingDataArr['dtAvailable']))?><br />
						<?=$bookingDataArr['szWarehouseToName']?><br />
						<?=$bookingDataArr['szWarehouseToAddress']." ".$bookingDataArr['szWarehouseToAddress2']?><br />
						<?=$bookingDataArr['szWarehouseToAddress3']?><br />
						<?=$bookingDataArr['szWarehouseToPostCode']." ".$bookingDataArr['szWarehouseToCity']?><br/>
						<?=$toWareHouseCountry?>
					</p>
				</div>
				<br />
				
				<div style="overflow:hidden;margin:0;">
					<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'messages/next_step_after_booking')?>:</p>
					<p style="margin:0;float:right;width:295px;">
						<?=t($t_base.'messages/plz_ensure_that_cargo_is_delivered')?><br />
						<?=$bookingDataArr['szWarehouseFromName']?><br />
						<?=$bookingDataArr['szWarehouseFromAddress']." ".$bookingDataArr['szWarehouseFromAddress2']?><br />
						<?=$bookingDataArr['szWarehouseFromAddress3']?><br />
						<?=$bookingDataArr['szWarehouseFromPostCode']." ".$bookingDataArr['szWarehouseFromCity']?><br/>
						<?=$fromWareHouseCountry?>
					</p>
				</div>
				<br />
				<p><?=t($t_base.'messages/the_booking_was_placed')?> <?=$bookingDataArr['szForwarderDispName']?> <?=t($t_base.'messages/on')?> Transporteca.com.<br />
					<?=t($t_base.'messages/for_booking_related_ques')?>  <?=$bookingDataArr['szForwarderDispName']?> <?=t($t_base.'messages/customer_service_on')?> customerservice@dsv.com.<br />
					<?=t($t_base.'messages/standard_trading_terms_condition')?> <?=$bookingDataArr['szForwarderDispName']?> <?=t($t_base.'messages/apply_to_this_booking')?> <?=$kForwarder->szLink?>
				</p>
	
				</div>
			</div>
		</div>
	</body>
</html>
<?
}
else
{
	header('Location:'.__BASE_URL__.'/home/');
	exit();
}*/
?>