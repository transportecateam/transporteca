<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
ini_set('max_execution_time',1200);
ini_set('memory_limit', '-1');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$display_profile_not_completed_message=true;
$szMetaTitle='Transporteca | Update Haulage - Upload File';

require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php" );
$maxCountRowError=false;
$Excel_export_import=new cExport_Import();
	$kConfig = new cConfig();
	$kWHSSearch = new cWHSSearch();
	$kHaulagePricing = new cHaulagePricing();
$t_base = "BulkUpload/";
$t_base_error="Error";
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );

$objPHPExcel=new PHPExcel();
if((int)$_SESSION['forwarder_user_id']==0)
{
	header('Location:'.__BASE_URL__.'/forwarders/');
	exit();	
}
if(isset($_SESSION['forwarder_admin_id']) && $_SESSION['forwarder_admin_id']>0)
{
	//$kForwarderContact->load($_SESSION['forwarder_user_id']);
	$kForwarder->load($_SESSION['forwarder_id']);
	$idForwarder=$kForwarder->id;
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,0,false,true);
	$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder);
}
else
{
	$kForwarderContact->load($_SESSION['forwarder_user_id']);
	$kForwarder->load($kForwarderContact->idForwarder);
	$idForwarder=$kForwarderContact->idForwarder;
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,0,false,true);
	$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder);
}
$w_id='';
$k=1;
$haulageModelCurrencyArr=array();
//print_r($_REQUEST);
if($_REQUEST['checkmode']=='currency')
{
	if(isset($_REQUEST['idCurrency']) && (int)$_REQUEST['idCurrency']>0)
	{
		$idCurrency=$_REQUEST['idCurrency'];
		if($Excel_export_import->checkCurrencyValid($idCurrency))
		{
			$tcount=0;
			$haulageModelCurrencyArr=$Excel_export_import->getWarehouseHaulagModelDetail($idForwarder,$idCurrency);
			if(!empty($haulageModelCurrencyArr))
			{
				foreach($haulageModelCurrencyArr as $haulageModelCurrencyArrs)
				{
					$w_id_value=$haulageModelCurrencyArrs['idWarehouse']."_".$haulageModelCurrencyArrs['idCountry']."_".$haulageModelCurrencyArrs['idHaulageModel'];
						
					if($w_id=="")
					{
						$w_id=$w_id_value;
					}
					else
					{
						$w_id .=";".$w_id_value;
					}
					
					$dataArr=array();
					$dataArr[0]['idWarehouse']=$haulageModelCurrencyArrs['idWarehouse'];
					$dataArr[0]['idPricingModel']=$haulageModelCurrencyArrs['idHaulageModel'];
					$totalcount=$Excel_export_import->getAllUpdatedHaulageModelExistsInActiveService($dataArr,$idCurrency);
					$tcount=$tcount+$totalcount;
					++$k;
				}
				
				if($tcount>0)
				{
					$countime=ceil($tcount*__TIME_PER_ROW_CREATE__);
					if($countime>1)
					{
						$tcountime=$countime." seconds";
					}
					else
					{
						$tcountime=$countime." second";
					}
				}
			}
		}
	}
}
else
{
	if(isset($_REQUEST['idWarehouse']) && (int)$_REQUEST['idWarehouse']>0 && (int)$_REQUEST['idHaulageModel']>0)
	{
		$haulageModelArr=$Excel_export_import->getPricingModelName($_REQUEST['idHaulageModel']);
		
		$kWHSSearch->load($_REQUEST['idWarehouse']);
		//$div_id="haulage_data_".$k;
		$dataArr=array();
		$dataArr[0]['idWarehouse']=$_REQUEST['idWarehouse'];
		$dataArr[0]['idPricingModel']=$_REQUEST['idHaulageModel'];
		$tcount=$Excel_export_import->getAllUpdatedHaulageModelExists($dataArr);
		if($tcount>0)
		{
			$countime=ceil($tcount*__TIME_PER_ROW_CREATE__);
			if($countime>1)
			{
				$tcountime=$countime." seconds";
			}
			else
			{
				$tcountime=$countime." second";
			}
			++$k;
			$w_id=$_REQUEST['idWarehouse']."_".$kWHSSearch->idCountry."_".$_REQUEST['idHaulageModel'];
		}
		else
		{
			noHaulageServices();
		}
							
	}
}

$pricingModelArr=$kHaulagePricing->getAllModelPricing();


	$maxrowsexcced=$kWHSSearch->getManageMentVariableByDescription('__MAX_ROWS_EXCEED__');
	
	$changeDropDownValue=$kWHSSearch->getManageMentVariableByDescription('__CHANGE_DROP_DOWN_OPTION_FOR_HAULAGE__');
?>
<script type="text/javascript">
$().ready( function () {
	//draw_canvas_image_obo('','','__BASE_STORE_IMAGE_URL__.'/haulagePricingBulk.png'','BULK');
});
</script>
<?php

if($_POST['haulage_file_upload']=='1')
{
	if($_FILES['haulageservicefile']['name']!="")
	{
		
			$fileextension=get_file_extension($_FILES['haulageservicefile']['name']);
			if($fileextension=='xlsx')
			{
				$szDisplayName=str_replace(" ","_",$kForwarder->szDisplayName);
                                $szDisplayName=str_replace("/","_",$szDisplayName);
				$folderName=$szDisplayName."_".$kForwarder->id;
				$importData="importData";
			
				if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName)) {
				    mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName);
				    chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName,0777);
				    if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData)) {
				    	 mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData);
				    	chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData,0777); 
				    }
				}
				else
				{
				 if (!is_dir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData)) {
				    	 mkdir(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData);
				    	 chmod(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData,0777);
				    }
				}
				
				$fh = fopen(__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData."/index.php", "w+");
				//print_r($_FILES);
				$fileName="Transporteca_haulage_Services_upload_sheet_".$_SESSION['forwarder_user_id']."_".date('dmYHis').".xlsx";
				
				$fileUploadPath=__APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/".$folderName."/".$importData;
				if(move_uploaded_file($_FILES['haulageservicefile']['tmp_name'], $fileUploadPath."/".$fileName)) 
                                { 
                                    $file_name= $fileUploadPath."/".$fileName;
                                    $sheetindex=0;
                                    $objReader = new PHPExcel_Reader_Excel2007();
                                    //print_r($objReader);
                                    $objReader->setReadDataOnly(true);
                                    $objPHPExcel = $objReader->load($file_name);


                                    $objPHPExcel->setActiveSheetIndex($sheetindex);
                                    $tabname =($objPHPExcel->getActiveSheet()->getTitle());
                                    if($tabname=='Sheet1')
                                    {
                                        $val1 = ($objPHPExcel->getActiveSheet()->getHighestColumn()); //CELL

                                        $colcount=PHPExcel_Cell::columnIndexFromString($val1);
                                        //echo $colcount."<br/>";

                                        $rowcount = ($objPHPExcel->getActiveSheet()->getHighestRow());

                                        if($rowcount<=__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__)
                                        {
                                        //echo $rowcount."<br/>";
                                            if($colcount>=13)
                                            {
                                                if($Excel_export_import->importHaulageServicesData($objPHPExcel,$sheetindex,$colcount,$rowcount,$kForwarder->id))
                                                {
                                                    $fileUploadFlag=true;
                                                    $_POST=array();
                                                    $_FILES=array();
                                                }
                                            }
                                            else
                                            {
                                                $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                                            }
                                        }
                                        else
                                        {
                                            $maxCountRowError=true;
                                            $err_msg_file=t($t_base.'error/file_max_row_count_error_1')." ".number_format((int)__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__)." ".t($t_base.'error/file_max_row_count_error_3');
                                        }
                                    }
                                    else
                                    {
                                        $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
                                    }
			    } 
			    else
			    {
			    	$err_msg_file=t($t_base.'messages/error_in_uploading_the_file');
			    } 
			}
			else
			{
                            $err_msg_file=t($t_base.'messages/invalid_file_uploaded');
			}
	}
	else
	{
		$err_msg_file=t($t_base.'messages/file_is_required');
	}
	
}
if(!empty($err_msg_file)){

?>
<script type="text/javascript">
$("#loader").attr('style','display:none;');
addPopupScrollClass('error_msg');
//alert('hi');
</script>
<div id="error_msg">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
<p class="close-icon" align="right">
<a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<?php /* ?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<li><?=$err_msg_file?></li>
</ul>
</div>
</div>
<?php 

*/

if($maxCountRowError)
{?>
	<h4><b><?=t($t_base.'error/file_max_row_count_error_2')?></b></h4><br/>
	<p><?=$err_msg_file?></p><br />
<?php
}
else
{
?>
<h4><b><?=t($t_base_error.'/invalid_file_format')?></b></h4><br />
<p><?=t($t_base_error.'/we_did_not_recg_haulage')?></p><br />
<?php }?>
<p align="center"><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
</div>
</div>
</div>
<?php }
if($fileUploadFlag){ ?>
<script type="text/javascript">
$("#loader").attr('style','display:none;');
addPopupScrollClass('success_msg');
//alert('hi');
</script>
<div id="success_msg">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
<p class="close-icon" align="right">
<a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<h5><?=t($t_base.'title/upload_of_haulage');?> </h5>
<p><?=t($t_base.'messages/success_file_upload_msg_1');?> <?=$Excel_export_import->successLine?> <? if((int)$Excel_export_import->successLine>1) { echo t($t_base.'messages/success_file_upload_msg_haulage_4');}else { echo t($t_base.'messages/success_file_upload_msg_haulage_4_line');} ?>.</p><br/>
<?php if($Excel_export_import->deleteLine>0){?>
<p><?=$Excel_export_import->deleteLine?> <? if($Excel_export_import->deleteLine>1){ echo t($t_base.'messages/lines_delete_uploaded'); }else { echo t($t_base.'messages/lines_delete_uploaded_line'); }?>.</p><br/>
<?php }?>
<?php if($Excel_export_import->errorLine>0){?>
<p><?=t($t_base.'messages/we_found');?> <?=$Excel_export_import->errorLine?> 
<?php if($Excel_export_import->errorLine>1){?>
<?=t($t_base.'messages/lines_error_uploaded');?>
<?php }else{?>
<?=t($t_base.'messages/line_error_uploaded');?><?php }?>
.</p><br/>
<?php }else{ ?>
<p><?=t($t_base.'messages/success_file_upload_msg_2');?>.</p><br/>
<?php }?>
<p><?=t($t_base.'messages/success_file_upload_msg_3');?>.</p><br/>
<p align="center"><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
</div>
</div>
</div>
<?php	
}
?>
<div id="hsbody-2">
			<div class="hsbody-2-left account-links">
				<?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" );

				?>
			</div>
			<div id="loader" class="loader_popup_bg" style="display:none;">
				<div class="popup_loader"></div>
				<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification" style="padding:10px;width:290px;">
				<p><?=t($t_base.'messages/plz_wait_while_we_process');?></p>
				<br/><br/>
				<p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
				</div>	</div>			
							
			</div>
			<div class="hsbody-2-right">
				<!-- <h4><strong><?=t($t_base.'title/haulage_bulk_update_function')?></strong></h4><br/><br/>-->
				<p align="text-align:justify"><?=t($t_base.'messages/top_line_msg_bulk_haulage');?> <a style="font-size: 16px;" href="<?__FORWARDER_HOME_PAGE_URL__?>/Haulage/"><?=t($t_base.'links/here')?></a>.</p><br/>
				<ol>
					<li>
						<p><?=t($t_base.'title/add_remove_update_origin_destination_haulage');?></p>
						<br/>
						
						<form name="bulkExportData" id="bulkExportData" method="POST">
							<div class="oh">
								<div class="fl-20 s-field">
								<span class="f-size-12"><?=t($t_base.'fields/country');?></span><br/>
									<select name="dataExportArr[haulageCountry]" id="haulageCountry" onchange="showForwarderWarehouse('<?=$idForwarder?>',this.value,'haulage_warehouse')">
										<option value=""><?=t($t_base.'fields/all');?></option>
										<?php
											if(!empty($forwardCountriesArr))
											{
												foreach($forwardCountriesArr as $forwardCountriesArrs)
												{
													?>
													<option value="<?=$forwardCountriesArrs['idCountry']?>_<?=$forwardCountriesArrs['szCountryName']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
													<?php
												}
											}
										?>
									</select>
								</div>
								<div id="haulage_warehouse">
								<div class="fl-30 s-field">
								<span class="f-size-12">Name</span><br/>
									<select name="dataExportArr[haulageWarehouse]" id="haulageWarehouse">
										<option value=""><?=t($t_base.'fields/all');?></option>
										<?php
											if(!empty($forwardWareHouseArr))
											{
												foreach($forwardWareHouseArr as $forwardWareHouseArrs)
												{
													?>
													<option value="<?=$forwardWareHouseArrs['id']?>_<?=$forwardWareHouseArrs['szWareHouseName']?>_<?=$forwardWareHouseArrs['szCountryName']?>_<?=$forwardWareHouseArrs['idCountry']?>"><?=$forwardWareHouseArrs['szWareHouseName']?></option>
													<?php
												}
											}
										?>
									</select>
								</div>
								<div class="fl-30 s-field" id="haulage_princing_model">
								<span class="f-size-12"><?=t($t_base.'fields/pricing_model');?></span><br/>
									<select name="dataExportArr[haulagePricingModel]" id="haulagePricingModel">
										<option value=""><?=t($t_base.'fields/all');?></option>
										<?php
											if(!empty($pricingModelArr))
											{
												foreach($pricingModelArr as $pricingModelArrs)
												{
													?>
													<option value="<?=$pricingModelArrs['id']?>_<?=$pricingModelArrs['szModelLongName']?>"><?=$pricingModelArrs['szModelLongName']?></option>
													<?php
												}
											}
										?>
									</select>
								</div>
								</div>
								<div class="fr-15" align="right">
									<!-- <p class="fl-65" style="padding-top:6px;"><?=t($t_base.'title/click_to_add_destination' ); ?></p>-->
									
									<input type="hidden" name="dataExportArr[haulageWarehouseAdded]" id="haulageWarehouseAdded" value="<?=$k?>">
									<input type="hidden" name="dataExportArr[idhaulageWarehouse]" id="idhaulageWarehouse" value="<?=$w_id?>">
									<input type="hidden" name="dataExportArr[idhaulageCountry]" id="idhaulageCountry" value="">
									<input type="hidden" name="deleteWareHouseArr" id="deleteWareHouseArr" value="">
									<input type="hidden" name="deleteWareHouseDivIdArr" id="deleteWareHouseDivIdArr" value="">
									<input type="hidden" name="dataExportArr[mainurl]" id="mainurl" value="<?=__MAIN_SITE_HOME_PAGE_URL__?>">
									<input type="hidden" name="dataExportArr[changeDropDown]" id="changeDropDown" value="<?=$changeDropDownValue?>">
									<input type="hidden" name="dataExportArr[changeDropDownCounter]" id="changeDropDownCounter" value="0">
									<input type="hidden" name="dataExportArr[maxrowsexcced]" id="maxrowsexcced" value="<?=$maxrowsexcced?>">
									<input type="hidden" name="dataExportArr[inActiveService]" id="inActiveService" value="0">
									<input type="hidden" name="dataExportArr[idCurrency]" id="idCurrency" value="<?=$idCurrency?>">
									<a href="javascript:void(0)" class="button1" onclick="addWareHousesHaulageNew('<?=$maxrowsexcced?>');" id="haulage_add"><span style="min-width:70px;"><?=t($t_base.'fields/add');?></span></a>
								</div>
							</div>
							
								<div class="scroll-div" style="padding:0;height:190px;margin-bottom:5px;">
									<table cellspacing="0" cellpadding="0" border="0" class="format-2" id="haulageTable" style="border-right:none;top: -1px;left:-1px;min-width:100%;float:left;">
										<tr>
											<th style="border-top:none;" width="21%"><strong><?=t($t_base.'fields/country');?></strong></td>
											<td style="border-top:none;" width="32%"><strong>Name</strong></td>
											<td style="border-top:none;" width="47%"><strong><?=t($t_base.'fields/pricing_model');?></strong></td>
										</tr>
										<?php if(!empty($haulageModelCurrencyArr))
											{
												$w_id_value='';
												$j=1;
												foreach($haulageModelCurrencyArr as $haulageModelCurrencyArrs)
												{
													$w_id_value=$haulageModelCurrencyArrs['idWarehouse']."_".$haulageModelCurrencyArrs['idCountry']."_".$haulageModelCurrencyArrs['idHaulageModel'];
														$haulageModelArr=$Excel_export_import->getPricingModelName($haulageModelCurrencyArrs['idHaulageModel']);
													?>
													<tr id="haulage_data_<?=$j?>" onclick="delete_ware_haulage('<?=$w_id_value?>','haulage_data_<?=$j?>','haulage')">
                                                                                                            <td class="noborder"><?=$kConfig->getCountryName($haulageModelCurrencyArrs['idCountry'])?></td>
                                                                                                            <td><?=$haulageModelCurrencyArrs['szWareHouseName']?></td>
                                                                                                            <td><?=$haulageModelArr[0]['szModelLongName']?></td>
													</tr>
												<?php	
												++$j;
												}
											}
										else if($k>1){?>
										<tr id="haulage_data_1" onclick="delete_ware_haulage('<?=$w_id?>','haulage_data_1','haulage')">
											<td class="noborder"><?=$kConfig->getCountryName($kWHSSearch->idCountry)?></td>
											<td><?=$kWHSSearch->szWareHouseName?></td>
											<td><?=$haulageModelArr[0]['szModelLongName']?></td>
										</tr>
										<tr id="haulage_data_2">
											
										</tr>
										<?php }else{?>
										<tr id="haulage_data_1">
											
										</tr>
										<?php }?>
									</table>
								</div>
								<div class="oh">
    							<p align="right" class="fl-85" style="margin: 5px 2% 0 0; width: 83%;"><?=t($t_base.'title/click_to_remove_pricing_model');?></p>
								<p align="right"  class="fl-15"><a href="javascript:void(0)" class="button1" onclick="" style="opacity:0.4;" id="haulage_remove_button"><span style="min-width:70px;"><?=t($t_base.'fields/remove');?></span></a></p>
								</div>
							<p><?=t($t_base.'title/click_remove_destination_cfs_haulage');?> <a href="<?=__FORWARDER_HOME_PAGE_URL__?>/Haulage/"><?=t($t_base.'links/here');?></a>.</p>
							<br/>
						</li>
						<li>
							<p><?=t($t_base.'title/download_excel_file_haulage');?></p>
							<!--  <p align="center">
								<select name="dataExportArr[downloadType]" id="downloadType" onchange="changeDownloadType('<?=$maxrowsexcced?>');">
									<option value='2'>Download all possible combinations</option>
									<option value='1'>Download only services currently updated</option>
								</select>
							</p>-->
							<br />
							<div id="row_excced_data_popup"></div>
							<div id="row_excced_data_1" class="note_info" <?php if($k>1){?> style="display:none" <?}?> ><p align="left">Note: No pricing models selected.</p></div>
							<div id="row_excced_data_2" class="note_info" <?php if($k>1){?> style="display:block" <?}else{?> style="display:none;"<? }?>><p align="left">Note: Approximately <span id="total_row_excced_1"><?=$tcount?></span> rows selected - expected file download time of <span id="total_row_excced_time"><?=$tcountime?></span>.</p></div>
							<div id="row_excced_data" class="note_info" style="display:none;"><p align="left">Note: Approximately <span id="total_row_excced"></span> rows selected - please reduce to maximum <?=number_format($maxrowsexcced)?> rows.</p></div>
							<p align="center">
							<input type="hidden" name="dataExportArr[downloadType]" id="downloadType" value='1'>
							<a href="javascript:void(0)" class="button1" onclick="" style="opacity:0.4;" id="download_button"><span><?=t($t_base.'fields/download');?></span></a></p>
							</form>
							<br/>												
							<p>
                                                            <!--<img width="100%" src="<?=__BASE_STORE_IMAGE_URL__.'/haulagePricingBulk-1.png'?>">-->
                                                            <img width="100%" src="<?=__BASE_STORE_IMAGE_URL__.'/CargoFlowHaulage.png'?>"> 
                                                        </p>
							<br/>
						</li>
						<li>
							<form name="uploadHaulageFile" id="uploadHaulageFile" method="post" enctype="multipart/form-data">
							<p><?=t($t_base.'title/upload_updated_file');?> <br/><em><a href="<?=__BASE_URL__?>/welcomeVideo.php?type=haulage_bulk" onclick="open_welcome_video();" target="google_map_target_1"><?=t($t_base.'links/help_me_update_file');?></a></em></p>
							
							<div class="file">
								<input type="file" id="fileUpload" name="haulageservicefile" />
								<span class="button"><?=t($t_base.'fields/no_file_selected');?></span>
							</div>
							<p align="center">
							<input type="hidden" value="1" name="haulage_file_upload" id="haulage_file_upload">
							<a href="javascript:void(0)" class="button1" onclick="submit_upload_haulage_file();"><span><?=t($t_base.'fields/upload');?></span></a></p><br/>
							
							</form>
							<p><?=t($t_base.'title/avail_on_transporteca');?></p>
							<br />				
							<br/>
						</li>
						<li>
							<p><?=t($t_base.'title/confirmation_email_from_transporteca');?></p>
						</li>
					</ol>
				</div>
			</div>	
			<div id="popup_container_google_map" style="display:none;">
				<div id="popup-bg"></div>
					<div id="popup-container">
					<div class="popup" style="width:720px;margin-top:50px;">
					<p class="close-icon" align="right">
					<a onclick="window.top.window.hide_google_map();" href="javascript:void(0);">
					<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
					</a>
					</p>
						<iframe id="google_map_target_1" style="height:490px;width:720px;" align="center" valign="middle" class="google_map_select"  name="google_map_target_1" src="#" ></iframe>
					</div>
					</div>
				</div>
				<?php
					if($k>1 && empty($haulageModelCurrencyArr))
					{
				?>
					<script>
					$("#download_button").unbind("click");
					$("#download_button").click(function(){downloadHaulageFormSubmit()});
					$("#download_button").attr("style","opacity:1;");
					</script>
		
				<?php
				}
				if(!empty($haulageModelCurrencyArr))
				{?>
					<script>
						$("#haulageCountry").attr("disabled",true);
						$("#haulageWarehouse").attr("disabled",true);
						$("#haulagePricingModel").attr("disabled",true);
						$("#haulage_add").unbind("click");
						$("#haulage_add").attr("style","opacity:0.4;");
						$("#inActiveService").attr('value','1');
						$("#download_button").unbind("click");
						$("#download_button").click(function(){downloadHaulageFormSubmit()});
						$("#download_button").attr("style","opacity:1;");
					</script>
				<?php					
				}
				include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
				?>	