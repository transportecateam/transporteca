<?php
/**
 * Forwarder My Company  
 */
 ob_start();
session_start();
$szMetaTitle="Transporteca | Update Customs Clearance  - Line by Line";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/forwarder_header.php");
checkAuthForwarder();
$t_base = "OBODataExport/";
$idForwarder =$_SESSION['forwarder_id'];

?>
<div id="hsbody-2">
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader"></div>				
        <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
    </div>
    <div id="customs_clearance_pop" class="help-pop"></div> 
    <div class="hsbody-2-left account-links">
        <?php require_once( __APP_PATH__ ."/layout/bulkUploadNav.php" ); ?>
    </div>
    <div class="hsbody-2-right">
        <h5><?=t($t_base.'title/custom_clearance_top_heading');?></h5>
        <form action="" id="obo_cc_data_export_form" method="post">
            <?php
                echo obo_data_export_top_form($idForwarder,$t_base,false,'OBO_CUSTOM_CLEARANCE');
            ?>
            <span is="obo_cc_getdata_button" style="display:none;"></span>
            <p class="fl-80" align="right" style="padding-top:6px;"><?=t($t_base.'title/click_to_get_custom_clearance_data');?></p>
            <p class="fl-20" align="right"><a href="javascript:void(0);" id="obo_cc_cc_getdata_button" onclick="" class="button1" style="opacity:0.4;"><span id="text_change"><?=t($t_base.'title/get_data');?></span></a></p>
        </form>
    </div> 
    <hr />
    <span id="obo_cc_bottom_span">
    <?php
            echo display_obo_cc_bottom_html($t_base,true);
    ?>
    </span>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
?>