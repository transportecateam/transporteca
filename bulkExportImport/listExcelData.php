<?php
function gColumnData()
{
	$gColumnData=array('3 hours','6 hours','12 hours','24 hours','36 hours','48 hours',
	'3 days','4 days','5 days','6 days','7 days','8 days','9 days','10 days','11 days',
	'12 days','13 days','14 days','15 days','16 days','17 days','18 days','19 days','20 days',
	'21 days');
	
	return $gColumnData;
}

function fColumnData()
{
	$fColumnData=array('00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30',
	'04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00',
	'09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30',
	'15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00',
	'20:30','21:00','21:30','22:00','22:30','23:00','23:30');
	
	return $fColumnData;
}

function eColumnData()
{
	$eColumnData=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
	
	return $eColumnData;
}


function cColumnData()
{
	$cColumnData=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
	
	return $cColumnData;
}


function dColumnData()
{
	$dColumnData=array('1','2','3','4','5','6','7');
	
	return $dColumnData;
}


function bColumnData()
{
	$bColumnData=array('Weekly','Biweekly');
	
	return $bColumnData;
}
?>