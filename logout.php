<?php

ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

//$f = fopen(__APP_PATH_LOGS__."/logout.log", "a");
//fwrite($f, "\n\n################### Logout Requet recieved:".date("d-m-Y h:i:s")."#######################\n");

$szLogString =  " \n \n Before logout Session<br> ";
$szLogString .= print_R($_SESSION,true);
$szLogString .= "\n\n Cookie \n\n  ";
$szLogString .= print_R($_COOKIE,true);

$_SESSION['user_id']='';
unset($_SESSION['user_id']);
$_SESSION['booking_id']='';
unset($_SESSION['booking_id']);

$_SESSION['came_from_landing_page']='';
unset($_SESSION['came_from_landing_page']);

$_COOKI['__USER_PASSWORD_COOKIE__'] = '' ;
unset($_COOKI['__USER_PASSWORD_COOKIE__']);

$_COOKI['__USER_LOGIN_COOKIE__'] = '' ;
unset($_COOKI['__USER_LOGIN_COOKIE__']);

if(__ENVIRONMENT__=='LIVE')
{
    setcookie("__USER_PASSWORD_COOKIE__", '', time()-(60*60*24*30),'/',$_SERVER['HTTP_HOST'],true);  
    setcookie("__USER_LOGIN_COOKIE__", '', time()-(60*60*24*30),'/',$_SERVER['HTTP_HOST'],true); 
}
else
{
    setcookie("__USER_PASSWORD_COOKIE__", '', time()-(60*60*24*30),'/');  
    setcookie("__USER_LOGIN_COOKIE__", '', time()-(60*60*24*30),'/'); 
}


$szLogString .= "\n\n Aftr logout \n Session\n\n ";
$szLogString .= print_R($_SESSION,true);
$szLogString .= "\n\n Cookie \n\n ";
$szLogString .= print_R($_COOKIE,true);
 
//fwrite($f, $szLogString);
//fclose($f);

ob_end_clean();
header('Location:'.__BASE_URL__);
exit();
?>