<?php
/**
 * Edit User Information
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/AccountPage/";
$t_base_error = "Error";
$kConfig = new cConfig();
$kUser = new cUser();
$successFlag=false;

if(!empty($_POST['editUserInfoArr']))
{
	//print_r($_POST['editUserInfoArr']);
	if($kUser->updateUserInfo($_POST['editUserInfoArr'],$_SESSION['user_id']))
	{
		$successFlag=true;
		
	}
}

$kUser->getUserDetails($_SESSION['user_id']);
$iLanguage = getLanguageId();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
$allCurrencyArr=$kConfig->getBookingCurrency(false,true);


?>
<?php
if(!empty($kUser->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kUser->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }if($successFlag){
	
//echo t($t_base.'messages/user_info_success_updated');	
?>
<script>
cancel_user_info();
</script>
<?php
}else {?>
<h5><strong>User Information</strong></h5>
<form name="updateUserInfo" id="updateUserInfo" method="post">
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szFirstName]" id="szFirstName" value="<?=$_POST['editUserInfoArr']['szFirstName'] ? $_POST['editUserInfoArr']['szFirstName'] : $kUser->szFirstName ?>" onblur="closeTip('fname');" onfocus="openTip('fname');"/></span>
				<div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szLastName]" id="szLastName" value="<?=$_POST['editUserInfoArr']['szLastName'] ? $_POST['editUserInfoArr']['szLastName'] : $kUser->szLastName ?>" onblur="closeTip('lname');" onfocus="openTip('lname');"/></span>
				<div class="field-alert"><div id="lname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/c_name');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szCompanyName]" id="szCompanyName" value="<?=$_POST['editUserInfoArr']['szCompanyName'] ? $_POST['editUserInfoArr']['szCompanyName'] : $kUser->szCompanyName ?>" onblur="closeTip('cname');" onfocus="openTip('cname');"/></span>
				<div class="field-alert"><div id="cname" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szAddress1]" id="szAddress1" value="<?=$_POST['editUserInfoArr']['szAddress1'] ? $_POST['editUserInfoArr']['szAddress1'] : $kUser->szAddress1 ?>" onblur="closeTip('address1');" onfocus="openTip('address1');"/></span>
				<div class="field-alert"><div id="address1" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szAddress2]" id="szAddress2" value="<?=$_POST['editUserInfoArr']['szAddress2'] ? $_POST['editUserInfoArr']['szAddress2'] : $kUser->szAddress2 ?>" onblur="closeTip('address2');" onfocus="openTip('address2');"/></span>
				<div class="field-alert"><div id="address2" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szAddress3]" id="szAddress3" value="<?=$_POST['editUserInfoArr']['szAddress3'] ? $_POST['editUserInfoArr']['szAddress3'] : $kUser->szAddress3 ?>" onblur="closeTip('address3');" onfocus="openTip('address3');"/></span>
				<div class="field-alert"><div id="address3" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szPostCode]" id="szPostCode" value="<?=$_POST['editUserInfoArr']['szPostCode'] ? $_POST['editUserInfoArr']['szPostCode'] : $kUser->szPostcode ?>" onblur="closeTip('postcode');" onfocus="openTip('postcode');"/></span>
				<div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/city');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szCity]" id="szCity" value="<?=$_POST['editUserInfoArr']['szCity'] ? $_POST['editUserInfoArr']['szCity'] : $kUser->szCity ?>" onblur="closeTip('city');" onfocus="openTip('city');"/></span>
				<div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szState]" id="szState" value="<?=$_POST['editUserInfoArr']['szState'] ? $_POST['editUserInfoArr']['szState'] : $kUser->szState ?>" onblur="closeTip('state');" onfocus="openTip('state');"/></span>
				<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
				<input type="hidden" name="editUserInfoArr[szOldCountry]" id="szOldCountry" value="<?=$_POST['editUserInfoArr']['szOldCountry'] ? $_POST['editUserInfoArr']['szOldCountry'] : $kUser->szCountry ?>" />
				<input type="hidden" name="editUserInfoArr[szPhoneNo]" id="szPhoneNo" value="<?=$_POST['editUserInfoArr']['szPhoneNo'] ? $_POST['editUserInfoArr']['szPhoneNo'] : $kUser->szPhoneNumber ?>" />
				<span class="field-name"><?=t($t_base.'fields/country');?></span>
				<span class="field-container">
					<select size="1" name="editUserInfoArr[szCountry]" id="szCountry">
						<?php
							if(!empty($allCountriesArr))
							{
								foreach($allCountriesArr as $allCountriesArrs)
								{
									?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['editUserInfoArr']['szCountry'])?$_POST['editUserInfoArr']['szCountry']:$kUser->szCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						?>
					</select>
				</span>
			</label>
			<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
			<span class="field-container">
				<select size="1" name="editUserInfoArr[szCurrency]" id="szCurrency" onblur="closeTip('currency');" onfocus="openTip('currency');">
					<?php
						if(!empty($allCurrencyArr))
						{
							foreach($allCurrencyArr as $allCurrencyArrs)
							{
								?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['editUserInfoArr']['szCurrency'])?$_POST['editUserInfoArr']['szCurrency']:$kUser->szCurrency) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
								<?php
							}
						}
					?>
				</select>
			</span>
			<div class="field-alert"><div id="currency" style="display:none;"><?=t($t_base.'messages/currency');?></div></div>
		</label>	
		<br/>
		<p align="center" style="width:60%"><a href="javascript:void(0)" class="button1" onclick="javscript:update_userinfo_details();"><span><?=t($t_base.'fields/save');?></span></a><a href="javascript:void(0)" class="button2" onclick="cancel_user_info();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
</form>	
<?php }?>