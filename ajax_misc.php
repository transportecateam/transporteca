<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once(__APP_PATH_INC__."/lib/Mautic/mautic.config.php");
if(!empty($_REQUEST['bookingConfirmAry']) || (sanitize_all_html_input(trim($_REQUEST['mode']))=='SHOW_HIDE_DROP_DWON_IN_SEARCH_FROM'))
{ 
    $iGetLanguageFromRequest = 1;
} 
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$kConfig=new cConfig();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$iLanguage = getLanguageId();
if($mode=='RESUME_BOOKING')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
    if($idBooking>0)
    {
        //$_SESSION['booking_id']='';
        //unset($_SESSION['booking_id']);		
        //$_SESSION['booking_id'] = $idBooking ;
        $redir_url = '';
        $postSearchAry = array();
        if($idBooking>0)
        {
            $kBooking = new cBooking();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            
        }
        
        
		/*
		if((($postSearchAry['idShipperConsignee']>0)) && ($postSearchAry['idForwarder']>0) && ($postSearchAry['idWarehouseTo']>0) && ($postSearchAry['idWarehouseFrom']>0))
		{
			$redir_url = __BOOKING_CONFIRMATION_PAGE_URL__;		
		}
		else 
		*/
		if(($postSearchAry['idForwarder']>0) && ($postSearchAry['idWarehouseTo']>0) && ($postSearchAry['idWarehouseFrom']>0))
		{
			$kConfig_new = new cConfig();
			$kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
			$szCountryStrUrl = $kConfig_new->szCountryISO ;
			$kConfig_new->loadCountry($postSearchAry['idDestinationCountry']);
			$szCountryStrUrl .= $kConfig_new->szCountryISO ; 
			$szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
			$redir_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
				
		}
		else if($postSearchAry['idServiceType']>0)
		{
			$kConfig_new = new cConfig();
			$kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
			$szCountryStrUrl = $kConfig_new->szCountryISO ;
			$kConfig_new->loadCountry($postSearchAry['idDestinationCountry']);
			$szCountryStrUrl .= $kConfig_new->szCountryISO ; 
			$szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
			$redir_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
			//$redir_url = __SELECT_SERVICES_URL__.'/'.$postSearchAry['szBookingRandomNum'].'/' ;
		}
		else
		{
			$redir_url = __LANDING_PAGE_URL__.'/'.$postSearchAry['szBookingRandomNum'].'/';
		}
		echo "SUCCESS||||".$redir_url ;
		die;
	}
}
else if($mode=='DISPLAY_SEARCH_MINI_VERSIONS')
{
    $iSearchPageVersion = sanitize_all_html_input(trim($_REQUEST['page_version']));
    $idSeoPage = sanitize_all_html_input(trim($_REQUEST['seo_page_id']));
    
    $kBooking = new cBooking();
    echo "SUCCESS||||";    
    /*
    * Never change this version class, bcz all designing is maintained on the basis of this.
    */
    if($_SESSION['HOLDININGPAGE']=='') {
        $szVersionClass = "version-".$iSearchPageVersion;
        if($iSearchPageVersion==2 || $iSearchPageVersion==4 || $iSearchPageVersion==6)
        {
            echo '<div id="transporteca_search_form_container" class="search-mini-2-container '.$szVersionClass.'">';
            echo display_new_search_form($kBooking,$postSearchAry,true,false,false,$szDefaultFromField,$idSeoPage,true,$idDefaultLandingPage,false,$iSearchPageVersion);
            echo '</div></section>';
            die;
        }
        else
        { 
            echo '<div id="transporteca_search_form_container" class="search-mini-container '.$szVersionClass.'">';
            echo display_new_search_form($kBooking,$postSearchAry,true,false,false,$szDefaultFromField,$idSeoPage,true,$idDefaultLandingPage,false,$iSearchPageVersion);
            echo '</div>';
            die;
        } 
    }
    else
    {
         $kExplain = new cExplain();
         $landingPageDataArys = $kExplain->getAllNewLandingPageData(false,false,false,$iLanguage,true,false,false,true);
         $landingPageDataAry = $landingPageDataArys[0];
          $idLandingPage = $landingPageDataArys[0]['id'];
         echo '<div id="transporteca_search_form_container">';
         echo holdingpageHtml($landingPageDataAry,$idLandingPage,false,$iSearchPageVersion);
         echo '</div>';
         die;
    }
}
else if($mode=='CREATE_VOGA_AUTOMATED_BOOKINGS')
{
    ignore_user_abort(true);
    $kConfig = new cConfig();
    $kWHSSearch = new cWHSSearch();
    $kBooking = new cBooking();
    
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
     
    if(!empty($postSearchAry))
    {
        $res_ary=array(); 
        $res_ary['iShipperDetailsLocked'] = 1;
            
        if(!empty($res_ary))
        {
            $update_query = "";
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            } 
            $update_query = rtrim($update_query,",");

            $kBooking->updateDraftBooking($update_query,$idBooking);
        } 
        $idDefaultLandingPage = $postSearchAry['idLandingPage']; 
        $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
        $postSearchAry['iNumRecordFound'] = count($searchResultAry);
        $iNumRecordFound = count($searchResultAry);
 
        $postSearchAry['iNumRecordFound'] = $iNumRecordFound; 
        $kBooking->addSelectServiceData($postSearchAry);

        $bRecalculateFlag=false;
        if($iNumRecordFound>0)
        {
            /*
            *  Creating Automatic quotes for Voga pages and added this to Mgt >  Pending Tray > Quote
            */  
            $kConfig->createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage,false,1);   
            if($kConfig->iAutomaticQuoteAdded==1)
            {
                echo "SUCCESS||||";
            }
            else
            {
                $bRecalculateFlag = true;
            }
        }
        else
        {
            $bRecalculateFlag = true;
        }  
    } 
    die;
}
else if($mode=='DISPLAY_PALLETS_POPUP')
{
    $iSearchPageVersion = sanitize_all_html_input(trim($_REQUEST['page_version']));
    $iNumPallets = sanitize_all_html_input(trim($_REQUEST['num_pallets']));
    $palletDimensionStr = sanitize_all_html_input(trim($_REQUEST['pallet_dimensions']));
    $kBooking = new cBooking();
    
    if(!empty($palletDimensionStr))
    {
        $palletDimensionAry = unserialize($palletDimensionStr);
    }
    echo "SUCCESS||||";
    echo display_pallet_details_popup($kBooking,$iNumPallets,$iSearchPageVersion,$palletDimensionAry);
    die;
}
else if($mode=='CUSTOMER_FEEDBACK')
{ 
	$kNPS = new cNPS();
	if($kNPS->addCustomerFeedBack($_POST['custfeedbackArr'],$_SESSION['user_id']))
	{
		$t_base = "BookingReceipt/";
		echo "SUCCESS||||";
		echo "<h2>".t($t_base.'fields/feedback_thank_you_message')."</h2>";
		die;
	} 
	else
	{
		echo "ERROR||||";
		echo customer_feedback_form();
		die;
	}
}
else if($mode=='DISPLAY_INSURANCE_TERMS_CONDITION')
{
	$kWHSSearch = new cWHSSearch(); 
//	if($iLanguage==__LANGUAGE_ID_DANISH__)
//	{
//		$szTermsText = $kWHSSearch->getManageMentVariableByDescription('__INSURANCE_TERMS_DANISH__');
//	}
//	else
//	{
		 $szTermsText = $kWHSSearch->getManageMentVariableByDescription('__INSURANCE_TERMS__',$iLanguage);
	//}
	
	echo "SUCCESS||||"; 
	$szTermsText = html_entity_decode($szTermsText);
	echo display_insurance_terms_condition($szTermsText);
	die;
}
else if($mode=='UPDATE_QUOTE_INSURANCE')
{ 
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key'])); 
    $szFlag = sanitize_all_html_input(trim($_REQUEST['flag'])); 
    
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $t_base = "BookingConfirmation/";

    if($idBooking>0)
    {
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking); 

        if((($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4')) && ($postSearchAry['iQuickQuote']!=__QUICK_QUOTE_MAKE_BOOKING__))
        {
            echo "POPUP||||";
            echo display_booking_already_paid($t_base,$idBooking);
            die;
        }
        else
        {
            $res_ary = array(); 
            if($szFlag=='ADD_INSURANCE_DETAILS_QUOTES')
            {
                $res_ary['iInsuranceIncluded'] = 1 ;	
            }
            else
            {
                $res_ary['iInsuranceIncluded'] = 0;	
            }  
            
            if(!empty($res_ary))
            {
                $update_query = "";
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,",");
                
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {
                    echo "SUCCESS||||";
                }
                else
                {
                    echo "ERROR||||";
                }
            } 
        }
    }
    die;
}
else if($mode=='ADD_INSURANCE_DETAILS')
{ 
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
    $iIncludeInsuranceWithZeroCargo = (int)sanitize_all_html_input($_REQUEST['bookingInsuranceAry']['iIncludeInsuranceWithZeroCargo']);
    $iBookingQuoteFlag = (int)sanitize_all_html_input($_REQUEST['bookingInsuranceAry']['iBookingQuoteFlag']);
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $t_base = "BookingConfirmation/";

    if($idBooking>0)
    {
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking); 

        if((($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4')) && ($postSearchAry['iQuickQuote']!=__QUICK_QUOTE_MAKE_BOOKING__)) 
        {
            echo "POPUP||||";
            echo display_booking_already_paid($t_base,$idBooking);
            die;
        }
        else
        {
            if($kBooking->updateInsuranceDetails($_POST['bookingInsuranceAry'],$postSearchAry))
            {
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking);

                echo "SUCCESS||||";
                if($postSearchAry['iInsuranceIncluded']==1)
                { 
                    echo $postSearchAry['szCurrency']." ".getPriceByLang(($postSearchAry['fTotalPriceCustomerCurrency'] + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'])),$iLanguage)." (".t($t_base.'fields/including_insurance')." ".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).")"; 
                }
                else
                {
                    echo $postSearchAry['szCurrency']." ".getPriceByLang($postSearchAry['fTotalPriceCustomerCurrency'],$iLanguage);
                }
                $fTotalInvoiceValue = $postSearchAry['fTotalPriceCustomerCurrency'] + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']) + $postSearchAry['fTotalVat']; 
                echo "||||"; 
                echo display_booking_insurance_form($kBooking,$postSearchAry,$iBookingQuoteFlag,$iIncludeInsuranceWithZeroCargo);
                echo "||||"; 
                echo $postSearchAry['szCurrency']." ".getPriceByLang($fTotalInvoiceValue,$iLanguage,2);
                die; 
            }
            else
            {
                echo "ERROR||||";
                echo display_booking_insurance_form($kBooking,$postSearchAry,$iBookingQuoteFlag,$iIncludeInsuranceWithZeroCargo);
                die;
            }
        }
    }
    die;
}
else if($mode=='REMOVE_INSURANCE_DETAILS')
{ 
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
    $iIncludeInsuranceWithZeroCargo = (int)sanitize_all_html_input($_REQUEST['bookingInsuranceAry']['iIncludeInsuranceWithZeroCargo']);
    $iBookingQuoteFlag = (int)sanitize_all_html_input($_REQUEST['bookingInsuranceAry']['iBookingQuoteFlag']);
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $t_base = "BookingConfirmation/";

    if($idBooking>0)
    {
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking);

        if((($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4')) && ($postSearchAry['iQuickQuote']!=__QUICK_QUOTE_MAKE_BOOKING__)) 
        {
            echo "POPUP||||";
            echo display_booking_already_paid($t_base,$idBooking);
            die;
        }
        else
        {
            if($kBooking->updateInsuranceDetails($_POST['bookingInsuranceAry'],$postSearchAry,'REMOVE'))
            {
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                
                $fTotalInvoiceValue = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat']; 

                echo "SUCCESS||||"; 
                echo $postSearchAry['szCurrency']." ".getPriceByLang($postSearchAry['fTotalPriceCustomerCurrency'],$iLanguage); 
                echo "||||"; 
                echo display_booking_insurance_form($kBooking,$postSearchAry,$iBookingQuoteFlag,$iIncludeInsuranceWithZeroCargo);
                echo "||||";
                echo $postSearchAry['szCurrency']." ".getPriceByLang($fTotalInvoiceValue,$iLanguage,2);
                die; 
            }
            else
            {
                echo "ERROR||||";
                echo display_booking_insurance_form($kBooking,$postSearchAry,$iBookingQuoteFlag,$iIncludeInsuranceWithZeroCargo);
                die;
            }
        }
    }
    die;
}
else if($mode=='BACK_TO_BOOKING')
{
	$kBooking = new cBooking();
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
                    echo "POPUP||||";
                    echo display_booking_already_paid($t_base,$idBooking);
                    die;
		}
		else if(($postSearchAry['idForwarder']>0) && ($postSearchAry['idWarehouseTo']>0) && ($postSearchAry['idWarehouseFrom']>0) && ($postSearchAry['idShipperConsignee']>0))
		{
			$redirect_url = __BOOKING_CONFIRMATION_PAGE_URL__.'/'.$szBookingRandomNum.'/';
		}
		else if(($postSearchAry['idForwarder']>0) || ($postSearchAry['idWarehouseTo']>0) || ($postSearchAry['idWarehouseFrom']>0))
		{
			$redirect_url =__BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/';
		}
		else
		{
			if($postSearchAry['iFromRequirementPage']==1)
			{
				$redirect_url = __REQUIREMENT_PAGE_URL__."/".$szBookingRandomNum.'/' ;
			}
			else
			{
				$redirect_url = __LANDING_PAGE_URL__."/".$szBookingRandomNum.'/' ;
			}
		}
		echo "SUCCESS||||".$redirect_url;
		die;
	}
}
else if($mode=='REPEAT_BOOKING')
{
		
}
else if($mode=='REPEAT_ACTIVE_BOOKING')
{ 
    $kRegisterShipCon = new cRegisterShipCon();
    $kBooking = new cBooking();
    if((int)$_SESSION['user_id']<=0)
    {
        ?>
        <script type="text/javascript">
        ajaxLogin();
        </script>
        <?php	
        die;
    }
    else
    {
        $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
        $kRegisterShipCon->copyBookingQuote($idBooking,true,false,false,false,$_SESSION['user_id']);
        if($kRegisterShipCon->iNewBookingID>0)
        { 
            $idBooking = $kRegisterShipCon->iNewBookingID ;
            echo "SUCCESS||||"; 
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $szBookingRandomNumber = $postSearchAry['szBookingRandomNum'];
            $kConfig = new cConfig();
            $kConfig->loadCountry($postSearchAry['idOriginCountry']);
            $szCountryStrUrl = $kConfig->szCountryISO ;
            $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
            $szCountryStrUrl .= $kConfig->szCountryISO ; 
            
            
            $landing_page_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNumber."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            echo $landing_page_url ;
            die;
        }
    }
}
else if($mode=='BOOKING_EXPIRE')
{
	$kBooking = new cBooking();
	$idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	//$_SESSION['booking_id'] = $idBooking ;
	$t_base = "Booking/MyBooking/";
	
	if(!empty($postSearchAry))
	{
            $kExportImport = new cExport_Import();		
            $idPricingWtw = $postSearchAry['idPricingWtw'];
            $kExportImport->loadPricingWtw($idPricingWtw);		
            /*
            * updating booking from hold to draft booking
            * 
            * */
            
            $res_ary=array();
            $res_ary['idBookingStatus'] = 1 ;	 
            if(!empty($res_ary))
            {
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,",");
                $kBooking->updateDraftBooking($update_query,$idBooking);
            }  
            $dtValidFromDate =strtotime($kExportImport->dtValidFrom);
            $dtExpiryFromDate = strtotime($kExportImport->dtExpiry);
            //!$kExportImport->isPricingAlreadyExists($idOriginWarehouse,$idDestinationWarehouse)
            $dtCreatedOn = date('Y-m-d h:i:s',strtotime($postSearchAry['dtCreatedOn']));
            $dt30MinBefore = date('Y-m-d h:i:s',strtotime("-30 minutes"));

            $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
            
            if($postSearchAry['iBookingType'] == __BOOKING_TYPE_COURIER__)
            {
                if($dtCreatedOn<$dt30MinBefore)
                {
                    echo "ERROR||||";
                    echo display_booking_expire_popup($t_base,$idBooking,'hold');
                    die;
                }
                else
                {
                    if($kBooking->recalculateCourierPricing($idBooking))
                    {
                        echo "SUCCESS||||".$postSearchAry['szBookingRandomNum'];
                    }
                    else
                    {
                        echo "ERROR||||";
                        echo display_booking_expire_popup($t_base,$idBooking,'hold');
                        die;
                    }
                }
            }
            else if($postSearchAry['iBookingType'] == __BOOKING_TYPE_AUTOMATIC__)
            {
                if((int)$kExportImport->idPricingWtw<=0)
                {
                        echo "ERROR||||";
                        echo display_rate_expire_popup($t_base,$idBooking,'hold');
                        die;
                }
                // cutoff date must be in between dtValidFrom and dtExpiryDate of LCL Service and must be greater then current time
                else if((strtotime($postSearchAry['dtCutOff']) <= $dtValidFromDate) || (strtotime($postSearchAry['dtCutOff']) > $dtExpiryFromDate) || (strtotime($postSearchAry['dtCutOff']) < time()))
                {		
                        echo "ERROR||||";
                        echo display_booking_expire_popup($t_base,$idBooking,'hold');
                        die;
                }
                else
                {
                        echo "SUCCESS||||".$postSearchAry['szBookingRandomNum'];
                }
            }
            else if($postSearchAry['iBookingType'] == __BOOKING_TYPE_RFQ__)
            {
                $iTodayMinusOne = strtotime("-1 DAY"); 
                $iTodayMinusOneDateTime = strtotime(date('Y-m-d 23:59:59',$iTodayMinusOne)); 
                
                if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
                {
                    echo "ERROR||||";
                    echo display_booking_already_paid($t_base,$idBooking);
                    die;
                } 
                else if((strtotime($postSearchAry['dtQuoteValidTo']) < $iTodayMinusOneDateTime ) || ($postSearchAry['iQuotesStatus']==__BOOKING_QUOTES_STATUS_EXPIRED__))
                { 
                    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                    $szCountryStrUrl = $kConfig->szCountryISO ;
                    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                    $szCountryStrUrl .= $kConfig->szCountryISO ; 

                    $redirect_url =  __BOOKING_QUOTE_EXPIRED_PAGE_URL__."/".$szBookingRandomNum."/".__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__."-".$szCountryStrUrl."/" ;

                    echo "REDIRECT_URL||||";
                    echo $redirect_url;
                    die;
                }
                else 
                {
                    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                    $szCountryStrUrl = $kConfig->szCountryISO ;
                    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                    $szCountryStrUrl .= $kConfig->szCountryISO ; 
                    
                    $redirect_url = __BOOKING_QUOTE_PAY_PAGE_URL__."/".$szBookingRandomNum."/".__OVERVIEW_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    echo "REDIRECT_URL||||";
                    echo $redirect_url;
                    die;
                }
            } 
	}
	else
	{
            echo "ERROR||||";
            echo display_rate_expire_popup($t_base,$idBooking,'hold');
            die;
	}
        die;
}
else if($mode == 'RECALCULATE_BOOKING_PRICE')
{
	$kBooking = new cBooking();
	$kWHSSearch = new cWHSSearch();
	
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	
	$page_url = sanitize_all_html_input(trim($_REQUEST['page_url']));
	$t_base = "Booking/MyBooking/";
	if((int)$idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if(empty($postSearchAry))
		{
			echo "ERROR||||";
			echo display_rate_expire_popup($t_base,$idBooking,'hold');
			die;
		}
		$kExportImport = new cExport_Import();
		
		$idPricingWtw = $postSearchAry['idPricingWtw'];
		$kExportImport->loadPricingWtw($idPricingWtw);
		
		$dtValidFromDate = 	strtotime($kExportImport->dtValidFrom);
		$dtExpiryFromDate = strtotime($kExportImport->dtExpiry);
		//$idBooking = $_SESSION['booking_id'] ;
		//!$kExportImport->isPricingAlreadyExists($idOriginWarehouse,$idDestinationWarehouse)
		if((int)$kExportImport->idPricingWtw<=0)
		{
			echo "ERROR||||";
			echo display_rate_expire_popup($t_base,$idBooking,'draft');
		}
		else if((strtotime($postSearchAry['dtCutOff']) <= $dtValidFromDate) || (strtotime($postSearchAry['dtCutOff']) > $dtExpiryFromDate) || (strtotime($postSearchAry['dtCutOff']) < time()))
		{
			echo "ERROR||||";
			echo display_booking_expire_popup($t_base,$idBooking,'draft');
		}
		else
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);		
			$resultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,'RECALCULATE_PRICING');
			//print_r($resultAry);
			$updateBookingAry = array();
			$res_ary=array();
			$updateBookingAry = $resultAry[0];
			if(!empty($updateBookingAry))
			{
				$res_ary = updatePricingChanges($updateBookingAry);
			}
			else
			{
				echo "ERROR||||";
				echo display_no_pricing_available_popup($szBookingRandomNum);
				die;
			}
			
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			//print_r($updateBookingAry);
			$update_query = rtrim($update_query,",");
			$kBooking->updateDraftBooking($update_query,$idBooking) ;
		
		echo "SUCCESS||||";
		$szPriceString = $postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalPriceCustomerCurrency'],2)." to ".$resultAry[0]['szCurrency']." ".number_format((float)$resultAry[0]['fDisplayPrice'],2);
		echo recalculate_price_changed_popup($szPriceString,$page_url);
		die;
		
	}
	}
	else
	{
		echo "BOOKING_NOTIFICATION||||" ;
		$redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
		echo display_booking_not_found_popup($redirect_url);
		die;
	}
}
else if($mode == 'RECALCULATE_BOOKING_PRICE_CARGO_CHANGED')
{
	$kBooking = new cBooking();
	$kWHSSearch = new cWHSSearch();
	
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	
	$page_url = sanitize_all_html_input(trim($_REQUEST['page_url']));
	$t_base = "Booking/MyBooking/";
	if((int)$idBooking>0)
	{
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if(empty($postSearchAry))
		{
			echo "ERROR||||";
			echo display_rate_expire_popup($t_base,$idBooking,'hold');
			die;
		}
		$kExportImport = new cExport_Import();
		
		$idPricingWtw = $postSearchAry['idPricingWtw'];
		$kExportImport->loadPricingWtw($idPricingWtw);
		
		$dtValidFromDate = 	strtotime($kExportImport->dtValidFrom);
		$dtExpiryFromDate = strtotime($kExportImport->dtExpiry);
		//$idBooking = $_SESSION['booking_id'] ;
		//!$kExportImport->isPricingAlreadyExists($idOriginWarehouse,$idDestinationWarehouse)
		if((int)$kExportImport->idPricingWtw<=0)
		{
			echo "ERROR||||";
			echo display_rate_expire_popup($t_base,$idBooking,'draft');
		}
		else
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);		
			$resultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,'RECALCULATE_PRICING');
			if(round((float)$resultAry[0]['fDisplayPrice'],2) != round((float)$postSearchAry['fTotalPriceCustomerCurrency'],2))
			{
				$type = sanitize_all_html_input(trim($_REQUEST['type']));
				if($type == 'SHOW_POPUP_ONLY')
				{
					echo "SUCCESS||||";
					$szPriceString = $resultAry[0]['szCurrency']." ".number_format((float)$resultAry[0]['fDisplayPrice'],2);
					echo price_changed_popup($szPriceString,$szBookingRandomNum,$type);
					die;
				}								
				if($type == 'UPDATE_PRICING_ONLY')
				{
					//$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);
				}
				//else
				//{
				$updateBookingAry = array();
				$res_ary=array();
				$updateBookingAry = $resultAry[0];
				
				if(!empty($updateBookingAry))
				{
					$res_ary = updatePricingChanges($updateBookingAry);
				}
				else
				{
					echo "ERROR||||";
					
					$res_ary=array();
					$res_ary['iDoNotKnowCargo'] = 0;
					if(!empty($res_ary))
					{
						$update_query = '';
						foreach($res_ary as $key=>$value)
						{
							$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
						}
					}
					$update_query = rtrim($update_query,",");
					$kBooking->updateDraftBooking($update_query,$idBooking) ;
					
					echo display_no_pricing_available_popup($szBookingRandomNum);
					die;
				}
				
				if(!empty($res_ary))
				{
					foreach($res_ary as $key=>$value)
					{
						$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
					}
				}
				$update_query = rtrim($update_query,",");
				$kBooking->updateDraftBooking($update_query,$idBooking) ;
				
				$postSearchAry = $kBooking->getBookingDetails($idBooking);
				$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);	
				$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);
				echo "SUCCESS||||";
				$szPriceString = $resultAry[0]['szCurrency']." ".number_format((float)$resultAry[0]['fDisplayPrice'],2);
				echo price_changed_popup($szPriceString,$szBookingRandomNum,$type);
				die;	
				//}
			}
			else
			{
				$res_ary=array();
				$res_ary['iDoNotKnowCargo'] = 0;
				
				if(!empty($res_ary))
				{
					foreach($res_ary as $key=>$value)
					{
						$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
					}
				}
				
				$update_query = rtrim($update_query,",");
				$kBooking->updateDraftBooking($update_query,$idBooking) ;
				
				$redirect_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/';
				echo "REDIRECT||||".$redirect_url;
				die;
			}
		}
	}
	else
	{
		echo "BOOKING_NOTIFICATION||||" ;
		$redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
		echo display_booking_not_found_popup($redirect_url);
		die;
	}
}
else if($mode == 'RECALCULATE_PRICING')
{
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);		
	$resultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,'RECALCULATE_PRICING');
	if(round((float)$resultAry[0]['fDisplayPrice'],2) != round((float)$postSearchAry['fTotalPriceCustomerCurrency'],2))
	{
		$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);
	}	
	if($type == 'CANCEL')
	{
		
	}
	else if($type == 'CONTINUE')
	{
		
	}
}
else if(!empty($_REQUEST['palletDetailAry']))
{
    $kConfig = new cConfig();  
    $idSearchMiniPage = sanitize_all_html_input(trim($_REQUEST['palletDetailAry']['idSearchMiniPage']));
    if($kConfig->validatePalletFields($_REQUEST['palletDetailAry']))
    {
        echo "SUCCESS||||";
        echo rtrim($kConfig->szPalletDescriptionStr,',')."||||".$kConfig->iNumPallet."||||".serialize($kConfig->tempCargoARy);
        die;
    }
    else
    {
        echo "ERROR||||";
        //print_R($kConfig);
        echo display_pallet_details_popup($kConfig,false,$idSearchMiniPage);
        die;
    }
}
else if(!empty($_REQUEST['bookingConfirmAry']))
{
    $userSessionData = array();
    $userSessionData = get_user_session_data(true);
    $idUserSession = $userSessionData['idUser'];

    if((int)$idUserSession<=0)
    {?>
        <script type="text/javascript">
        ajaxLogin();
        </script>
    <?php	
    exit();
    }
    else
    {		 
		$kBooking = new cBooking();	
		$kUser = new cUser();
		$kWHSSearch = new cWHSSearch();
		
		$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['bookingConfirmAry']['szBookingRandomNum'])); 
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
		if((int)$idBooking>0)
		{
                    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
                    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);

                    if($postSearchAry['iFromRequirementPage']==2)
                    {
                        $szBookingDetailsPageUrl = __NEW_BOOKING_DETAILS_PAGE_URL__ ;
                        $szBookingConfirmationPageUrl = __NEW_BOOKING_OVERVIEW_PAGE_URL__ ;
                    }
                    else
                    {
                        $szBookingDetailsPageUrl = __BOOKING_DETAILS_PAGE_URL__;
                        $szBookingConfirmationPageUrl = __BOOKING_CONFIRMATION_PAGE_URL__ ;
                    } 
                    if(($postSearchAry['idForwarder']<=0))
                    { 
                        echo "BOOKING_NOTIFICATION||||" ;
                        $redirect_url = $szBookingDetailsPageUrl.'/'.$szBookingRandomNum ;				
                        echo display_booking_notification($redirect_url);
                        die;
                    }
                    else if((($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4')) && ($postSearchAry['iQuickQuote']!=__QUICK_QUOTE_MAKE_BOOKING__))
                    {
                        echo "BOOKING_NOTIFICATION||||" ;
                        echo display_booking_already_paid($t_base,$idBooking);
                        die;
                    }
		}
		else
		{
                    echo "BOOKING_NOTIFICATION||||" ;
                    $redirect_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/' ;				
                    echo display_booking_not_found_popup($redirect_url);
                    die;
		}  
		$t_base = "BookingConfirmation/";
                if($postSearchAry['iUpdateInsuranceWithBlankValue']==1)
                {
                    if($postSearchAry['fValueOfGoods']<=0 && $postSearchAry['iInsuranceIncluded']==1)
                    {
                        echo "ERROR_NEW_PAGE||||";
                        ?>
                        <ul><li><?=t($t_base.'fields/enter_value_goods');?></li></ul>
                        <?php
                        echo "||||PURCHASE_GOODS_ERROR"; 
                        die;
                    } 
                }  
                
                if($_REQUEST['bookingConfirmAry']['iBookingStatus']!='2')
                {
                    if($_REQUEST['bookingConfirmAry']['iInsuranceSelectedFlag']!=1)
                    {
                        echo "ERROR_NEW_PAGE||||";
                        ?>
                        <ul><li><?=t($t_base.'fields/insurance_not_selected_message');?></li></ul>
                        <?php
                        die;
                    }
                    
                    if($postSearchAry['iInsuranceIncluded']==1 && (float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']<=0)
                    {
                        echo "ERROR_NEW_PAGE||||";
                        ?>
                        <ul><li><?=t($t_base.'fields/insurance_price_cant_be_zero_message');?></li></ul>
                        <?php
                        die;
                    }
                }
//                echo "Included: ".$postSearchAry['iInsuranceIncluded']." Price: ".$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'];
//                die;
                
                
		$idUser = $idUserSession;
		$kUser->getUserDetails($idUser);
		$kBooking->set_szInvoiceComment(sanitize_all_html_input($_REQUEST['bookingConfirmAry']['szInvoiceComment']));
		
		if((float)$postSearchAry['fTotalPriceCustomerCurrency']<=1)
		{
                    $kBooking->addError('fBookingAmount',t($t_base.'title/internal_server_error'));
		}
		else 
		{
                    $kBooking->set_fBookingAmount((float)$postSearchAry['fTotalPriceCustomerCurrency']);
		}
		
		if($_REQUEST['bookingConfirmAry']['iBookingStatus']==3)
		{
                    $kBooking->set_iTermsCondition(sanitize_all_html_input($_REQUEST['bookingConfirmAry']['iTerms']));
		}
		$kBooking->iRemindToRate = (int)$_REQUEST['bookingConfirmAry']['iRemindToRate'] ;
		
		if(!empty($kBooking->arErrorMessages))
		{
                    echo "ERROR_NEW_PAGE||||";
                    ?>
                    <ul>
                        <?php
                            foreach($kBooking->arErrorMessages as $key=>$values)
                            {
                              ?><li><?=$values?></li><?php 
                            }
                        ?>
                    </ul>
                    <?php 
                    die; /*
                            
                            echo "ERROR||||" ;
                            ?>
                            <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
                            <div id="regErrorList">
                            <ul>
                            <?php
                                  foreach($kBooking->arErrorMessages as $key=>$values)
                                  {
                                          ?><li><?=$values?></li>
                                          <?php 
                                  }
                            ?>
                            </ul>
                            </div>
		<?php
                    */  
                    die;
		}
		else
		{		
                    $idCustomer = (int)$idUserSession;
                    $kUser->getUserDetails($idCustomer);
                            /*
                    // don't allow to confirm or hold any booking for incomplete user.
                    if((int)$kUser->iIncompleteProfile==1)
                    {
                            //For now we are discontinuing this check. 
                            $updateAry = array();
                            $updateAry['iAcceptedTerms'] = $kBooking->iTermsCondition;
                            if((int)$postSearchAry['iBookingStep']<12)
                            {
                                    $updateAry['iBookingStep'] = 12 ;
                            }				
                            if(!empty($updateAry))
                            {
                                    foreach($updateAry as $key=>$value)
                                    {
                                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                    }
                            }
                            $update_query = rtrim($update_query,",");

                            $kBooking->updateDraftBooking($update_query,$idBooking);

                            $t_base_book_confim = "BookingConfirmation/";
                            if($_REQUEST['bookingConfirmAry']['iBookingStatus']==2)
                            {
                                    $szTextMessage =t($t_base_book_confim.'messages/put_this_booking_in_hold'); 
                            }
                            else if($_REQUEST['bookingConfirmAry']['iBookingStatus']==3)
                            {
                                    $szTextMessage =t($t_base_book_confim.'messages/place_this_booking'); 
                            }
                            echo "BOOKING_NOTIFICATION||||" ;
                            echo display_incomplete_user_popup($szBookingRandomNum,$szTextMessage);
                            die;
                    }
                    */

                    if((int)$_REQUEST['bookingConfirmAry']['iBookingStatus']==3)
                    {
                        /*
                        if($kUser->iConfirmed!=1)
                        {
                                echo "ERROR||||" ;
                                if(empty($kUser->dtConfirmationCodeSent) || ($kUser->dtConfirmationCodeSent=='0000-00-00 00:00:00' ))
                                {
                                        $dtConfirmationCodeSent = $kUser->dtCreateOn ;
                                }
                                else
                                {
                                        $dtConfirmationCodeSent = $kUser->dtConfirmationCodeSent ;
                                }
                                //echo "<br>".t($t_base.'messages/on_message')."<br>";
                                $szErrorMessages =t($t_base.'messages/unconfirm_account_error_message1')." ".$kUser->szEmail." on ".date('d/m/Y',strtotime($dtConfirmationCodeSent)).", or " ; 
                                $szErrorMessages .= '<a href="javascript:void(0)" style="text-decoration:none;color:#000000;font-style:normal;font-size:16px;" onclick="resendActivationCode_bc(\'\',\'booking_confirm_error\',\''.$szBookingRandomNum.'\');" ><span style="font-style:italic;text-decoration:underline;">'.t($t_base.'fields/click_here').'</span></a>';
                                $szErrorMessages .= " ".t($t_base.'messages/unconfirm_account_error_message2');
                                echo $szErrorMessages ;
                                ?>
                                <br>
                                <span style="color:green" id="activationkey"></span>
                                <?
                                die;
                        } */
                    }
			
                    if($_REQUEST['bookingConfirmAry']['iBookingStatus']==3)
                    {
                        $kForwarder=new cForwarder();
                        $kForwarder->load($postSearchAry['idForwarder']);

                        //Generating Booking Reference Number
                        if(!empty($postSearchAry['szBookingRef']) && !($kBooking->alreadyUsed("BOOKING_REF", $postSearchAry['szBookingRef'],$idBooking)))
                        {
                            $szBookingRef = $postSearchAry['szBookingRef'];
                        }
                        else
                        {				
                            $max_number_used_for_forwarder = $kForwarder->getMaximumUsedNumberByForwarder($postSearchAry['idForwarder']);
                            $szBookingRef = $kBooking->generateBookingRef($max_number_used_for_forwarder,$kForwarder->szForwarderAlies);

                            $bookingRefLogAry = array();
                            $bookingRefLogAry['idForwarder'] = $postSearchAry['idForwarder'];
                            $bookingRefLogAry['szBookingRef'] = $szBookingRef;	
                            $bookingRefLogAry['szReferenceType'] = 'BOOKING';	
                            $kForwarder->updateMaxNumBookingRef($bookingRefLogAry);
                        }

                        //do update 
                        $updateAry = array();
                        $updateAry['szBookingRef'] = $szBookingRef ;
                        $updateAry['szInvoiceComment'] = $kBooking->szInvoiceComment ;
                        $updateAry['iRemindToRate'] = $kBooking->iRemindToRate ;
                        if((int)$postSearchAry['iBookingStep']<13)
                        {
                            $updateAry['iBookingStep'] = 13 ;
                        }	
                        $updateAry['iPaymentType'] = sanitize_all_html_input(trim($_REQUEST['bookingConfirmAry']['iBookingPayment']));
                        //$updateAry['dtBookingConfirmed'] = date('Y-m-d H:i:s');
                        //$updateAry['idBookingStatus'] = 3 ;

                        //$redirect_url = __BOOKING_PAYMENT_PAGE_URL__ ;
                    }
                    else
                    {
                        $updateAry = array();
                        $updateAry['szInvoiceComment'] = $kBooking->szInvoiceComment ;
                        $updateAry['iRemindToRate'] = $kBooking->iRemindToRate ;
                        $updateAry['idBookingStatus'] = 2;
                        $redirect_url =__BASE_URL__.'/myBooking/hold/' ;
                    } 
                    $updateAry['iAcceptedTerms'] = $kBooking->iTermsCondition;
                    $updateAry['iBookingLanguage'] = $iLanguage ;
                    $updateAry['iQuotesStatus'] = sanitize_all_html_input(trim($_REQUEST['bookingConfirmAry']['iQuotesStatus'])) ;

                    if(!empty($updateAry))
                    {
                        foreach($updateAry as $key=>$value)
                        {
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    }
                    $update_query = rtrim($update_query,","); 
                    if($kBooking->updateDraftBooking($update_query,$idBooking))
                    {
                        /*
                        * Updating account language of user
                        */
                        $updateUserDetailsAry = array();
                        $updateUserDetailsAry['iLanguage'] = $iLanguage ;   
                        if(!empty($updateUserDetailsAry))
                        {
                            $update_user_query = '';
                            foreach($updateUserDetailsAry as $key=>$value)
                            {
                                $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        }

                        $kRegisterShipCon = new cRegisterShipCon();
                        $update_user_query = rtrim($update_user_query,",");  
                        if($kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer))
                        {
                            $kRegisterShipCon->updateCapsuleCrmDetails($idCustomer);
                        }
                        if($_REQUEST['bookingConfirmAry']['iBookingStatus']==3)
                        {
                            $szPaymentGateWayStatus = $kWHSSearch->getManageMentVariableByDescription('__PAYMENT_GATEWAY_STATUS__');

                            if($szPaymentGateWayStatus!='ENABLED')
                            {
                                echo "BOOKING_NOTIFICATION||||";			
                                echo display_payment_gateway_disabled_popup();
                                die;
                            }
                            //if any session is already exist then user must have to complete previous session then only he can proceed for new session.
                            if(((int)$_SESSION['booking_id']<=0) || (($_SESSION['booking_id'] == $idBooking) && ($_SESSION['booking_id']>0)) || ($_REQUEST['bookingConfirmAry']['iBookingPayment']==__TRANSPORTECA_PAYMENT_TYPE_3__))
                            {
                                $_SESSION['booking_id'] = $idBooking ;

                                /*
                                * updating booking iPaymentProcess='1' means this booking will no longer updatable
                                *
                                *  */
                                $kBooking->updatePaymentFlag($idBooking,1);
                                
                                $langArr=$kConfig->getLanguageDetails('',$iLanguage);
                                $szDimensionUnit=$langArr[0]['szDimensionUnit'];
                                $fCargoWeight=$postSearchAry['fCargoWeight']/1000; 
//                                if($iLanguage==__LANGUAGE_ID_DANISH__)
//                                {
//                                    $szDimensionUnit = "m3";
//                                }
//                                else 
//                                {
//                                    $szDimensionUnit = "cbm";
//                                }
                                if($postSearchAry['fCargoWeight']<1000)
                                {
                                    $fCargoWeight=round(get_formated_cargo_measure($postSearchAry['fCargoWeight']))." kg";
                                }
                                else
                                {    
                                    $fCargoWeight=get_formated_cargo_measure($postSearchAry['fCargoWeight']/1000)." mt"; 
                                }
                                /*
                                if($postSearchAry['iCourierBooking']==1)
                                { 
                                    $szConsigneeCity = html_entities_flag($postSearchAry['szConsigneeCity'],true);
                                    $szShipperCity = html_entities_flag($postSearchAry['szShipperCity'],true);
                                    $szForwarderDispName = html_entities_flag($postSearchAry['szForwarderDispName'],true);
                                    $szWarehouseFromCity = html_entities_flag($postSearchAry['szWarehouseFromCity'],true);
                                     
                                    //$item_ref_text = "Courier services from door-to-door, all inclusive, from ".$szShipperCity." to ".$szConsigneeCity." with ".$szForwarderDispName." (".get_formated_cargo_measure($postSearchAry['fCargoVolume'])." cbm / ".$fCargoWeight." / ".$postSearchAry['iNumColli']." pieces)";
                                    $item_ref_text = t($t_base.'fields/courier_service_door_to_door')." ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".get_formated_cargo_measure($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight." / ".$postSearchAry['iNumColli']." ".t($t_base.'fields/pieces').")";
                                    
                                }
                                else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)  // 3- PTD
                                {	
                                    $szConsigneeCity = html_entities_flag($postSearchAry['szConsigneeCity'],true);
                                    $szForwarderDispName = html_entities_flag($postSearchAry['szForwarderDispName'],true);
                                    $szWarehouseFromCity = html_entities_flag($postSearchAry['szWarehouseFromCity'],true);
                                     
                                    //$item_ref_text = "FOB ".$szWarehouseFromCity." to ".$szConsigneeCity." with ".$szForwarderDispName." (".get_formated_cargo_measure($postSearchAry['fCargoVolume'])." cbm / ".get_formated_cargo_measure($fCargoWeight)." mt)";
                                    $item_ref_text = "FOB ".$szWarehouseFromCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".get_formated_cargo_measure($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
                                }
                                else
                                {
                                    $szConsigneeCity = html_entities_flag($postSearchAry['szConsigneeCity'],true);
                                    $szForwarderDispName = html_entities_flag($postSearchAry['szForwarderDispName'],true);
                                    $szShipperCity = html_entities_flag($postSearchAry['szShipperCity'],true);
                                     
                                    $item_ref_text = "EXW ".$szShipperCity." ".t($t_base.'fields/to')." ".$szConsigneeCity." ".t($t_base.'fields/with')." ".$szForwarderDispName." (".get_formated_cargo_measure($postSearchAry['fCargoVolume'])." ".$szDimensionUnit." / ".$fCargoWeight.")";
                                }
                                 * 
                                 */
                                $item_ref_text = getServiceDescription($postSearchAry);

                                if($postSearchAry['iInsuranceIncluded']==1)
                                {
                                    $fTotalCostForBooking =  $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] ; 
                                }
                                else
                                {
                                    $fTotalCostForBooking = $postSearchAry['fTotalPriceCustomerCurrency'];
                                } 
                                
                                $fTotalCostForBooking = $fTotalCostForBooking + $postSearchAry['fTotalVat']; 
                                if($_REQUEST['bookingConfirmAry']['iBookingPayment']==__TRANSPORTECA_PAYMENT_TYPE_2__)
                                {
                                    if(__PAYPAL_LIVE__==true)
                                    {
                                        $paypalurl=__PAYPAL_PAYMENT_URL_LIVE__;
                                    }
                                    else
                                    {
                                        $paypalurl=__PAYPAL_PAYMENT_URL_SANDBOX__;
                                    } 
                                    //$item_ref_text = utf8_encode($item_ref_text); 
                                    $redirect_url=$paypalurl."?cmd=_xclick&charset=utf8&LOCALECODE=da_DK&business=".__BUSINESS_PAYPAL_ACCOUNT__."&item_name=".$item_ref_text."&amount=". number_format((float)$fTotalCostForBooking,2)."&item_number=".$updateAry['szBookingRef']."&no_shipping=1&no_note=1&currency_code=".$postSearchAry['szCurrency']."&no_tax=1&lc=US&return=".__MAIN_SITE_HOME_PAGE_SECURE_URL__."/paypal/paypal_confirm.php?utm_nooverride=1&cancel_return=".__MAIN_SITE_HOME_PAGE_SECURE_URL__."/paypal/paypal_confirm.php?utm_nooverride=1&notify_url=".__MAIN_SITE_HOME_PAGE_SECURE_URL__."/paypal/paypal_ipn.php&bn=PP-BuyNowBF&utm_nooverride=1";
                                    echo "SUCCESS||||".$redirect_url;
                                }
                                else if($_REQUEST['bookingConfirmAry']['iBookingPayment']==__TRANSPORTECA_PAYMENT_TYPE_1__)
                                { 
                                    //$var_value="cmd=openTrx&amount=".(float)$fTotalCostForBooking."&currencyCode=".$postSearchAry['szCurrency']."&email=".$kUser->szEmail."&firstName=".$kUser->szFirstName."&lastName=".$kUser->szLastName."&additionalDetails=".$item_ref_text."&number=".$updateAry['szBookingRef']."";
                                    $iStripeFlag = 1; 
                                    $var_value = substr(trim($item_ref_text),0,-1)." - ".$szBookingRef;
                                    if($iStripeFlag==1)
                                    {
                                        /*
                                        * Creating input data for stripe gateway
                                        */  
                                        echo "SUCCESSZO0Z||||".__STRIPE_API_KEY__."||||".$fTotalCostForBooking."||||".$postSearchAry['szCurrency']."||||".$var_value."||||".$postSearchAry['szEmail']; 
                                        die; 
                                    }
                                    else
                                    { 
                                        echo "SUCCESSZO0Z||||".$var_value."||||".__API_UNIQUE_KEY__."||||".__URL_FLAG__;
                                    }
                                    die; 
                                }
                                else if($_REQUEST['bookingConfirmAry']['iBookingPayment']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                                { 
                                    $kConfig = new cConfig();
                                    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                                    $szCountryStrUrl = $kConfig->szCountryISO ;
                                    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                                    $szCountryStrUrl .= $kConfig->szCountryISO ;

                                    $thank_you_page_url =  __THANK_YOU_PAGE_URL__."/".__THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                                    echo "REDIRECT||||";
                                    echo $thank_you_page_url ;
                                    die; 
                                }
                            ?>
                            <?php
                            }
                            else
                            {
                                echo "BOOKING_NOTIFICATION||||";
                                $redirect_url = $szBookingDetailsPageUrl ;				
                                echo display_booking_not_found_popup($redirect_url);
                                die;
                            }
                        }
                        else
                        { 
                            if($kUser->iConfirmed==1)
                            {
                                echo "SUCCESS||||".$redirect_url;
                                die;
                            }
                            else
                            {
                                $_SESSION['display_booking_saved_popup'] =1; 	
                                echo "SUCCESS||||".__LANDING_PAGE_URL__;
                                die;
                            }
                        }
                    }
		}
	}
}
else if($mode=='CHECK_COUNTRY_VALIDITION_OF_SIMPLE_SEARCH')
{
    $t_base = "home/homepage/";
    $szValue=sanitize_all_html_input(trim($_REQUEST['szValue']));
    $flag=sanitize_all_html_input(trim($_REQUEST['flag']));
    $iBookingQuote=sanitize_all_html_input(trim($_REQUEST['iBookingQuote']));

    if($flag=='VOLUME' || $flag=='WEIGHT')
    {
        $kWHSSearch=new cWHSSearch();
        $maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
        $maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
        $maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');
        $maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');
        $maxCargoQty = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_QUANTITY__');

        if($flag=='VOLUME')
        {
            $maxVolume=($maxCargoLength*$maxCargoWidth*$maxCargoHeight)/1000000;
            $kConfig->set_iVolume($szValue,$maxVolume);

            if($kConfig->arErrorMessages!='')
            {
                foreach($kConfig->arErrorMessages as $key=>$value)
                {
                    echo "ERROR||||".$value;
                }
            }
        }
        else if($flag=='WEIGHT')
        {
            $kConfig->set_iNewWeight($szValue,$maxCargoWeight);
            if($kConfig->arErrorMessages!='')
            {
                foreach($kConfig->arErrorMessages as $key=>$value)
                {
                    echo "ERROR||||".$value;
                }
            }
        } 
    }
    else
    { 
        if(!empty($szValue))
        {			
            $szCountryArr=reverse_geocode($szValue,true);     
        }
        else
        {
            echo "ERROR||||".t($t_base.'errors/check_location');
            die;
        }      
        $szCountry=$szCountryArr['szCountryName'];
        $idCountry = $kConfig->getCountryIdByCountryName($szCountry,$iLanguage); 

        if($idCountry<=0 && !empty($szCountryArr['szCountryCode']))
        {
            $kConfig->loadCountry(false,$szCountryArr['szCountryCode']);
            $idCountry = $kConfig->idCountry ; 
        }
        if((int)$idCountry>0)
        {
            if($flag=='FROM_COUNTRY')
            {
                $kConfig->loadCountry($idCountry);
                if($kConfig->iSmallCountry==1 && $szCountryArr['szCityName']=='')
                {
                    $szCountryArr['szCityName'] = $szCountry ;
                } 
                if($szCountryArr['szCityName']=='')
                {
                    echo "ERROR||||".t($t_base.'errors/city_name_is_required');
                }
                else
                {
                    echo "SUCCESS||||";
                }
            }
            else if($flag=='TO_COUNTRY')
            {
                $kConfig->loadCountry($idCountry);
                if($kConfig->iSmallCountry==1 && $szCountryArr['szCityName']=='')
                {
                    $szCountryArr['szCityName'] = $szCountry ;
                } 
                if($szCountryArr['szCityName']=='')
                {
                    echo "ERROR||||".t($t_base.'errors/city_name_is_required');
                }
                else
                {
                    echo "SUCCESS||||";
                }
            }
        }
        else
        {
            if($flag=='FROM_COUNTRY')
            {
                echo "ERROR||||".t($t_base.'errors/do_not_service_from_export_country');
            }
            else if($flag=='TO_COUNTRY')
            {
                echo "ERROR||||".t($t_base.'errors/do_not_service_from_import_country');
            }
        }
    }
}
else if($mode=='open_new_search_form_pop')
{?>
		<div id="popup-container">
		<div class="popup search-popup">
			<p class="close-icon" align="right">
			<a onclick="cancel_signin('<?=$cancel_url?>','ajaxLogin');" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
	<?php
	$kBooking = new cBooking();
	display_new_search_form($kBooking,$postSearchAry,true);
	?>
	</div>
	</div>
	<?php
}
else if($mode=='GET_ADDRESS_FORM_IPADDRESS')
{
    $addressArr=getCountryCodeByIPAddress($_SERVER['REMOTE_ADDR']);
    
    $szAddress=$addressArr['szCityName'].", ".$addressArr['szCountryName'];
    echo $szAddress;
    die();
}
else if($mode=='__PRIVATE_CUSTOMER_CONTINUE_SEARCHING__')
{
    $idBooking = (int)$_POST['idBooking'];
    $szCountryStrUrl = trim($_POST['szCountryStrUrl']);
    $iNumRecordFound = (int)$_POST['iNumRecordFound'];
    $iSearchMiniPageVersion = (int)$_POST['iSearchMiniPageVersion']; 
    
    $szFormId = sanitize_all_html_input($_REQUEST['form_id']);
    $szPageUrl = sanitize_all_html_input($_REQUEST['page_url']);
    $iHiddenFlag = sanitize_all_html_input($_REQUEST['hidden_flag']); 
    $szFromPage = sanitize_all_html_input($_REQUEST['from_page']);
    
    $additionalAry = array();
    $additionalAry['szFormId'] = $szFormId;
    $additionalAry['szPageUrl'] = $szPageUrl;
    $additionalAry['iHiddenFlag'] = $iHiddenFlag;
    $additionalAry['iSearchMiniVersion'] = $iSearchMiniPageVersion;
    $additionalAry['szFromPage'] = $szFromPage;
     
    $kBooking = new cBooking();
    $kWHSSearch = new cWHSSearch();
    $kConfig = new cConfig();
    $postSearchAry = $kBooking->getBookingDetails($idBooking); 
    
    $kExplain = new cExplain();
    $searchNotificationArr = array();
    $searchNotificationArr = $kExplain->isSearchNotificationExists($postSearchAry,true);
 
    if(!empty($searchNotificationArr))
    { 
        echo "SUCCESS||||";
        echo display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,'','',$iSearchMiniPageVersion,$additionalAry);
        die;
    }
    else
    {
        echo "ERROR||||";
    }
}
else if($mode=='__CONTINUE_SEARCHING__')
{
    $idBooking = (int)$_POST['idBooking'];
    $szCountryStrUrl = trim($_POST['szCountryStrUrl']);
    $iNumRecordFound = (int)$_POST['iNumRecordFound'];
    $kBooking = new cBooking();
    $kWHSSearch = new cWHSSearch();
    $kConfig = new cConfig();
    $postSearchAry = $kBooking->getBookingDetails($idBooking); 
    
    $szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
    
    if($postSearchAry['iSearchMiniVersion']==7 || $postSearchAry['iSearchMiniVersion']==8 || $postSearchAry['iSearchMiniVersion']==9)
    {
        $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
        $postSearchAry['iNumRecordFound'] = count($searchResultAry);
        $iNumRecordFound = count($searchResultAry);
        
        $postSearchAry['iNumRecordFound'] = $iNumRecordFound; 
        $kBooking->addSelectServiceData($postSearchAry);

        if($iNumRecordFound>0)
        {
            $idDefaultLandingPage = $postSearchAry['idLandingPage'];
            /*
            *  Creating Automatic quotes for Voga pages and added this to Mgt >  Pending Tray > Quote
            */  
            $kConfig->createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage,false,1); 
            $iNumRecordFound = 0;
        }
    }
    else
    {
        $iNumRecordFound = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);  
        if($iNumRecordFound<=0)
        {
            $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
            $postSearchAry['iNumRecordFound'] = count($searchResultAry);
            $iNumRecordFound = count($searchResultAry);

            $postSearchAry['iNumRecordFound'] = $iNumRecordFound; 
            $kBooking->addSelectServiceData($postSearchAry); 
        }
    }
    
    //$szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;

    if((int)$iNumRecordFound>0 && $_SESSION['user_id']<=0)
    {
        $res_ary=array(); 
        $res_ary['iBookingStep'] = 3 ; 
        $res_ary['iBookingQuotes'] = 0 ;

        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {
            //booking moves on to second step means Trade Available.
        } 
    }
    else
    { 
        $res_ary=array();
        if((int)$postSearchAry['iBookingStep']<7)
        {
            $res_ary['iBookingStep'] = 7 ;
        } 
        $res_ary['iBookingQuotes'] = 1 ;
        if((int)$iNumRecordFound<=0)
        {
            $res_ary['iGetQuotePageFlag'] = 1 ;
        }
        if(!empty($res_ary))
        {
            $update_query = '';
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        $update_query = rtrim($update_query,",");
        if($kBooking->updateDraftBooking($update_query,$idBooking))
        {
            //booking moves on to second step means Trade Available.
        } 
    } 
    //echo "REDIRECT||||";   
    if($iNumRecordFound>0)
    { 
        if($_SESSION['user_id']>0 || $postSearchAry['iSmallShipment']==1)
        {
            //$postSearchAry = $kBooking->getBookingDetails($idBooking);  
            $postSearchAry['iNumRecordFound'] = $iNumRecordFound;
            $kBooking->insertBookingSearchLogs($postSearchAry); 

           echo __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
           die;
        }
        else
        {
            echo __BOOKING_GET_RATES_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            die;

        } 
    }
    else
    {
        if($postSearchAry['iSearchMiniVersion']==7 || $postSearchAry['iSearchMiniVersion']==8 || $postSearchAry['iSearchMiniVersion']==9)
        {  
            echo __BOOKING_QUOTE_THANK_YOU_PAGE_URL__."/".___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            die;
        }
        else
        {
            $kWHSSearch=new cWHSSearch();
            $szUserDidNotGetAnyOnlineService = $kWHSSearch->getManageMentVariableByDescription('__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__');
    
            $res_ary=array(); 
            $res_ary['szInternalComment'] = $szUserDidNotGetAnyOnlineService ;
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {

            }

            echo __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            die;

        } 
    } 
    die;
}
else if(!empty($_POST['partnerApiRequestAry']))
{
    $_SESSION['mauticApiRequestAry'] = $_POST['partnerApiRequestAry'];   
    $kMautic = new cMautic();
    if(!empty($_POST['partnerApiRequestAry']['szApiRequestParams']))
    { 
        $encodedString = utf8_encode($_POST['partnerApiRequestAry']['szApiRequestParams']);
        $apiRequestParams = json_decode($encodedString,true);    
    }    
    $kMautic->CreateMauticLead($apiRequestParams,false,3); 
    echo "SUCCESS||||"; 
    ?>
    <span class="api_area_heading">
        <h3>
            <span class="api_hdng1">API End Point:</span>
            <span class="inpt1 api-flo"><?php echo Mautic\Auth\OAuth::$szEndPoint;?></span>
        </h3> 
    </span>  
    <div class="api_subheading_1"> 
        <h2>API Request</h2>
        <span class="api_dscrarea"> 
            <span class="api_ori_hdng">HTTP Method :</span>
            <span class="api_dp_menu_1"><?php echo Mautic\Auth\OAuth::$szMethod?></span> 
            <span class="api_clear"></span>
            <span class="api_ori_hdng">Request Params: </span>
            <span class="api_dp_menu_1"><?php echo Mautic\Auth\OAuth::$szRequestParameters; ?></span> 
        </span>
    </div>
    <br>   
    <div class="api_subheading_1"> 
        <h2>API Response</h2>
        <span class="api_dscrarea">
            <span class="api_ori_hdng">Response Code:</span>
            <span class="api_dp_menu_1"><?php echo Mautic\Auth\OAuth::$szResponseCode; ?></span>
            <span class="api_clear"></span> 
            <span class="api_ori_hdng">Response Headers :</span>
            <span class="api_dp_menu_1"><?php echo Mautic\Auth\OAuth::$szResponseHeader; ?></span>
            <span class="api_clear"></span>
            <span class="api_ori_hdng">API Response :</span>
            <span class="api_dp_menu_1"><?php echo Mautic\Auth\OAuth::$szResponseMessage; ?></span>
            <span class="api_clear"></span> 
        </span>
    </div> 
    <?php
}
else if($mode=='__SHOW_SEARCH_NOTIFICATION_POPUP__')
{
    $szBookingRandomNum = trim($_POST['szBookingRandomNum']);
    $szFlag = trim($_POST['szFlag']);
    $iHiddenValue = trim($_POST['iHiddenValue']);

    $kBooking = new cBooking();
    $kWHSSearch = new cWHSSearch();
    $kConfig = new cConfig();

    $idBooking=$kBooking->getBookingIdByRandomNum($szBookingRandomNum);

    $postSearchAry = $kBooking->getBookingDetails($idBooking);

    $idBooking = (int)$_POST['idBooking'];
    $searchNotificationArr = array();
    if($iSearchMiniPageVersion!=7 && $iSearchMiniPageVersion!=8 && $iSearchMiniPageVersion!=9) 
    {
        if($_POST['searchAry']['szOriginCountryStr']!='')
        {
            $szOriginCountryArr=reverse_geocode($_POST['searchAry']['szOriginCountryStr'],true);
            $origionGeoCountryAry = $szOriginCountryArr ;
            $postSearchAry['szOriginCountry']=$szOriginCountryArr['szCountryName'];
            $idOriginCountry=$kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);

            if($idOriginCountry<=0 && !empty($szOriginCountryArr['szCountryCode']))
            {
                $kConfig->loadCountry(false,$szOriginCountryArr['szCountryCode']);
                $idOriginCountry = $kConfig->idCountry ; 
            }

             $szOriginPostCode = $szOriginCountryArr['szPostCode']; 

            if(empty($szOriginPostCode))
            {
                $postcodeCheckAry=array();
                $postcodeResultAry = array();

                $postcodeCheckAry['idCountry'] = $idOriginCountry ;
                $postcodeCheckAry['szLatitute'] = $szOriginCountryArr['szLat'];
                $postcodeCheckAry['szLongitute'] = $szOriginCountryArr['szLng'];

                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 

                if(!empty($postcodeResultAry))
                {
                    $szOriginPostCode = $postcodeResultAry['szPostCode'];
                }
            } 
        }

        if($_POST['searchAry']['szDestinationCountryStr']!='')
        {
            $szDesCountryArr = reverse_geocode($_POST['searchAry']['szDestinationCountryStr'],true);
            $destinationGeoCountryAry = $szDesCountryArr ;
            $postSearchAry['szDestinationCountry']=$szDesCountryArr['szCountryName'];
            $idDesCountry=$kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);

            if($idDesCountry<=0 && !empty($szDesCountryArr['szCountryCode']))
            {
                $kConfig->loadCountry(false,$szDesCountryArr['szCountryCode']);
                $idDesCountry = $kConfig->idCountry ; 
            }


            $szDestinationPostCode = $szDesCountryArr['szPostCode']; 

            if(empty($szDestinationPostCode))
            {
                $postcodeCheckAry = array(); 
                $postcodeResultAry = array(); 
                $postcodeCheckAry['idCountry'] = $idDesCountry ;
                $postcodeCheckAry['szLatitute'] = $szDesCountryArr['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $szDesCountryArr['szLng'] ;
                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                if(!empty($postcodeResultAry))
                {
                    $szDestinationPostCode = $postcodeResultAry['szPostCode'];
                }
            } 
        } 
        
        $iBookingLanguage=  getLanguageId();
        $searchNotificationRequestAry = array();
        $searchNotificationRequestAry['idOriginCountry'] = $idOriginCountry;
        $searchNotificationRequestAry['idDestinationCountry'] = $idDesCountry; 
        $searchNotificationRequestAry['szOriginCountry']=$_POST['searchAry']['szOriginCountryStr'];
        $searchNotificationRequestAry['szDestinationCountry']=$_POST['searchAry']['szDestinationCountryStr']; 
        $searchNotificationRequestAry['szOriginCity']=$szOriginCountryArr['szCityName'];
        $searchNotificationRequestAry['szDestinationCity']=$szDesCountryArr['szCityName']; 
        $searchNotificationRequestAry['szOriginPostCode']=$szOriginPostCode;
        $searchNotificationRequestAry['szDestinationPostCode']=$szDestinationPostCode; 
        $searchNotificationRequestAry['iLanguage']=$iBookingLanguage;

        $kExplain = new cExplain(); 
        $searchNotificationArr = array();
        $searchNotificationArr = $kExplain->isSearchNotificationExists($searchNotificationRequestAry);
    } 
    if(!empty($searchNotificationArr))
    {
        $kConfig = new cConfig();
        $kConfig->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig->szCountryISO ;
        $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
        $szCountryStrUrl .= $kConfig->szCountryISO ;

        $searchNotificationArr=sortArray($searchNotificationArr,'iType',true);
        echo "SUCCESS_LOCAL_SEARCH||||";
        echo display_local_search_popup_new($searchNotificationArr[0],$idBooking,$szCountryStrUrl,$iNumRecordFound,$szFlag,$iHiddenValue);
        die;
    }
    else
    {
        echo "SUCCESS||||";
        die;
    }
}
else if($mode=='SHOW_DUTY_CALCULATOR')
{
    echo dutyCalculatorHtml();
}
else if($mode=='CALCULATE_DUTY_VALUE')
{
    $kWHSSearch = new cWHSSearch();
    $idGoodResultCurrency=(int)$_POST['idGoodResultCurrency'];
    
    $fGoodPrice=(float)$_POST['fGoodPrice'];
    $fLoadPrice=(float)$_POST['fLoadPrice'];
    
    $fProductRate=(float)$_POST['fProductRate'];
    
    $idGoodCurrency=(int)$_POST['idGoodCurrency'];
    $idLoadCurrency=(int)$_POST['idLoadCurrency'];
    //print_r($_POST);
    if($idLoadCurrency=='1')
    {
        $fLoadPriceUSDPrice=$fLoadPrice;
    }
    else
    {
        $ffLoadPriceUsdExchangeRate=$kWHSSearch->getCurrencyFactor($idLoadCurrency);
        $fLoadPriceUSDPrice=$fLoadPrice*$ffLoadPriceUsdExchangeRate;
        $fLoadPriceUSDPrice=round($fLoadPriceUSDPrice,2);
    }
    
    
    if($idGoodCurrency=='1')
    {
        $fGoodPriceUSDPrice=$fGoodPrice;
    }
    else
    {
        $fGoodPriceUsdExchangeRate=$kWHSSearch->getCurrencyFactor($idGoodCurrency);
        $fGoodPriceUSDPrice=$fGoodPrice*$fGoodPriceUsdExchangeRate;
        $fGoodPriceUSDPrice=round($fGoodPriceUSDPrice,2);
    }
    
    $fResultPriceDKKExchangeRate=$kWHSSearch->getCurrencyFactor(__DKK_CURRENCY_ID__);
    $fGoodPriceDKKPrice=round(($fGoodPriceUSDPrice/$fResultPriceDKKExchangeRate),2);
    if($fGoodPriceDKKPrice<__MINIMUM_GOOD_VALUE_IN_DKK__)
    {
         echo ($fProductRate*100)."%||||";
        echo "0.00";
        die();
    }
    
    $totalPriceUSD=($fGoodPriceUSDPrice+$fLoadPriceUSDPrice)*$fProductRate;
    
    if($idGoodResultCurrency==1)
    {
        $totalResultPrice=$totalPriceUSD;
    }
    else
    {
        $fResultPriceUsdExchangeRate=$kWHSSearch->getCurrencyFactor($idGoodResultCurrency);
        $totalResultPrice=($totalPriceUSD/$fResultPriceUsdExchangeRate);
    }
    
    echo ($fProductRate*100)."%||||";
    echo round($totalResultPrice,2);
    die();
}
else if($mode=='SHOW_HIDE_DROP_DWON_IN_SEARCH_FROM')
{
    $t_base = "home/homepage/";
    $szOriginCountryCode=trim($_POST['szOriginCountryCode']);
    $szDestinationCountryCode=trim($_POST['szDestinationCountryCode']);
    $idCustomerCountry=(int)$_POST['idCustomerCountry'];
    $id_suffix=trim($_POST['id_suffix']);
    $idServiceType = (int)$_POST['idServiceType'];
    $id_old_suffix=trim($_POST['id_suffix']);
    
    $kConfig_new = new cConfig(); 
    $kConfig_new->loadCountry('',$szOriginCountryCode);
    $idOriginCountry = $kConfig_new->idCountry ;  
    $kConfig_new->loadCountry('',$szDestinationCountryCode); 
    $idDesCountry = $kConfig_new->idCountry ;
    $szWithoutPickUpCLass='';
    $kCourierServices = new cCourierServices();
    $iHiddenValue=0;
    
    if($idCustomerCountry==$idOriginCountry)
    {
        $iShipperConsigneeFlag = 1; //Customer is consignee 
    }
    else if($idCustomerCountry==$idDesCountry)
    {
        $iShipperConsigneeFlag = 2; //Customer is consignee
    }
    else
    {
        $iShipperConsigneeFlag = 3; //Customer is billing
    }


    if($iShipperConsigneeFlag==1)
    {
        $iPickupDefaultValue_option1 = t($t_base.'fields/pick_no')." - ".t($t_base.'fields/cfr');
        $iPickupDefaultValue_option2 = t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/dap');
        $iPickupDefaultValue_option3 = "";

        $iPickupDropdownValue_option1 = t($t_base.'fields/pick_no');
        $iPickupDropdownValue_option2 = t($t_base.'fields/pick_yes');
        $iPickupDropdownValue_option3 = "";
    } 
    else if($iShipperConsigneeFlag==3)
    {
        $iPickupDefaultValue_option1 = t($t_base.'fields/fob');
        $iPickupDefaultValue_option2 = t($t_base.'fields/cfr');
        $iPickupDefaultValue_option3 = t($t_base.'fields/dtd');

        $iPickupDropdownValue_option1 = t($t_base.'fields/fob');
        $iPickupDropdownValue_option2 = t($t_base.'fields/cfr');
        $iPickupDropdownValue_option3 = t($t_base.'fields/dtd');
    }
    else
    {
        $iPickupDefaultValue_option1 = t($t_base.'fields/pick_no')." - ".t($t_base.'fields/fob');
        $iPickupDefaultValue_option2 = t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/exw');
        $iPickupDefaultValue_option3 = "";

        $iPickupDropdownValue_option1 = t($t_base.'fields/pick_no');
        $iPickupDropdownValue_option2 = t($t_base.'fields/pick_yes');
        $iPickupDropdownValue_option3 = "";
    }
    if($kCourierServices->checkFromCountryToCountryExists($idOriginCountry,$idDesCountry,true))
    {
        $postSearchAry['idServiceType'] = __SERVICE_TYPE_DTD__ ;
        $bDisplayPickupField = false;
        $szWithoutPickUpCLass = " dtd-services-list";
        echo "HIDEDROPDOWN||||";
        echo $iPickupDefaultValue_option1."||||";
        echo $iPickupDefaultValue_option2."||||";
        echo $iPickupDefaultValue_option3."||||";

        echo $iPickupDropdownValue_option1."||||";
        echo $iPickupDropdownValue_option2."||||";
        echo $iPickupDropdownValue_option3;
        if(strtolower($id_suffix)=='_v_5')
        {
            echo "||||".t($t_base.'fields/pallet');
        }
        
    }
    else
    {
        echo "SHOWDROPDOWN||||";
        
       
        echo $iPickupDefaultValue_option1."||||";
        echo $iPickupDefaultValue_option2."||||";
        echo $iPickupDefaultValue_option3."||||";
        
        echo $iPickupDropdownValue_option1."||||";
        echo $iPickupDropdownValue_option2."||||";
        echo $iPickupDropdownValue_option3."||||";
        ?>
        <div class="pickup">
                <?php  
                    if($iShipperConsigneeFlag==1)
                    { 
                        /*
                        * This means customer is shipper
                        */ 
                ?>
                    <label><?=t($t_base.'fields/delivery');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/DELIVERY_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                    <div style="text-align:center;">				
                        <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                            <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_DTP__)?'selected':'');?>><?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/cfr');?></option>
                            <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/dap');?></option>
                        </select>  
                    </div>
                <?php 

                    } 
                    else if($iShipperConsigneeFlag==3)
                    { 
                        /*
                        * This means customer is billing
                        */ 
                        ?>
                        <label><?=t($t_base.'fields/terms');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/TERMS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                        <div style="text-align:center;">				
                            <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                                <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_PTD__)?'selected':'');?>><?php echo t($t_base.'fields/fob');?></option>
                                <option value="2" <?php echo (($idServiceType==__SERVICE_TYPE_DTP__)?'selected':'');?>><?php echo t($t_base.'fields/cfr');?></option>
                                <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/dtd');?></option>
                            </select>  
                        </div> 
                <?php } else { ?>
                    <label><?=t($t_base.'fields/pick_up_new');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/PICKUP_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                    <div style="text-align:center;">				
                        <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                            <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_PTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/fob');?></option>
                            <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/exw');?></option>
                        </select>  
                    </div>
                <?php } ?>  
            </div>
            <script type="text/javascript">
            Custom.init();
            </script>
        <?php
        $id_suffix="_hiden";
        echo "||||";
        ?>
        <div class="pickup">
                <?php  
                    if($iShipperConsigneeFlag==1)
                    { 
                        /*
                        * This means customer is shipper
                        */ 
                ?>
                    <label><?=t($t_base.'fields/delivery');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/DELIVERY_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                    <div style="text-align:center;">				
                        <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                            <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_DTP__)?'selected':'');?>><?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/cfr');?></option>
                            <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/dap');?></option>
                        </select>  
                    </div>
                <?php 

                    } 
                    else if($iShipperConsigneeFlag==3)
                    { 
                        /*
                        * This means customer is billing
                        */ 
                        ?>
                        <label><?=t($t_base.'fields/terms');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/TERMS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                        <div style="text-align:center;">				
                            <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                                <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_PTD__)?'selected':'');?>><?php echo t($t_base.'fields/fob');?></option>
                                <option value="2" <?php echo (($idServiceType==__SERVICE_TYPE_DTP__)?'selected':'');?>><?php echo t($t_base.'fields/cfr');?></option>
                                <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/dtd');?></option>
                            </select>  
                        </div> 
                <?php } else { ?>
                    <label><?=t($t_base.'fields/pick_up_new');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/PICKUP_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                    <div style="text-align:center;">				
                        <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                            <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_PTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/fob');?></option>
                            <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/exw');?></option>
                        </select>  
                    </div>
                <?php } ?>  
            </div>
            <script type="text/javascript">
            Custom.init();
            </script>
        <?php        
        if(strtolower($id_old_suffix)=='_v_5')
        {
            if($iLanguage==__LANGUAGE_ID_DANISH__){
                echo '||||m<span class="sup">3</span>';
            }else{
            echo "||||".t($t_base.'fields/cm');
            }
        }
            
    }
    die();
    
}
else if($mode=='USER_NEW_SIGNUP_FROM_HOLDING_PAGE')
{
    if($kUser->userSignupFormHoldingPage($_POST['signupArr']))
    {
        $iSearchMini=true;
        $kExplain = new cExplain();
        $landingPageDataArys = $kExplain->getAllNewLandingPageData($_POST['signupArr']['idLandingPage']);
        $landingPageDataAry = $landingPageDataArys[0];
        $iSearchPageVersion=(int)$_POST['iSearchPageVersion'];
        if($iSearchPageVersion>0)
        {
            $iSearchMini=false;
        }
        $szHoldingThanksButtonText=  sanitize_all_html_input(trim($_POST['szHoldingThanksButtonText']));
        echo "SUCCESS||||";
        echo $szHoldingThanksButtonText."||||";
        echo holdingpageHtml($landingPageDataAry,$_POST['signupArr']['idLandingPage'],$iSearchMini,$iSearchPageVersion);
        
    }
    else
    {
        if(!empty($kUser->arErrorMessages))
        {
             $szValidationErrorKey = '';
            foreach($kRegisterShipCon->arErrorMessages as $errorKey=>$errorValue)
            {
                if(empty($szValidationErrorKey))
                {
                    $szValidationErrorKey = $errorKey;
                }
                ?>
                <script type="text/javascript">
                        $("#"+'<?php echo $errorKey; ?>').attr('class','red_border');
                </script>
                <?php
            }
        }
    }
}
else if($mode=='SHOW_NOTIFICATION_POPUP_FOR_VOGA_PAGE')
{
    $szOriginData=  sanitize_all_html_input(trim($_POST['szOriginData']));
    $szDesData=  sanitize_all_html_input(trim($_POST['szDesData']));
    $kConfig= new cConfig();
    if($szOriginData!='')
    {
        $szOriginCountryArr = reverse_geocode($szOriginData,true);  
        $replaceAry = array(" ",",");
        $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szOriginCountryStr']); 
        $szCountryStrSerialized = serialize($szOriginCountryArr);   

        $postSearchAry['szOriginCountry']=$szOriginCountryArr['szCountryName'];
        $idOriginCountry=$kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);

        if($idOriginCountry<=0 && !empty($szOriginCountryArr['szCountryCode']))
        {
            $kConfig->loadCountry(false,$szOriginCountryArr['szCountryCode']);
            $idOriginCountry = $kConfig->idCountry ; 
        }

        $postSearchAry['szOriginCountry']=$szOriginData;
        $postSearchAry['szOLat']=$szOriginCountryArr['szLat'];
        $postSearchAry['szOLng']=$szOriginCountryArr['szLng'];
        $postSearchAry['szOriginCity']=$szOriginCountryArr['szCityName']; 
        $postSearchAry['szOriginPostCode'] = $szOriginCountryArr['szPostCode']; 
        $postSearchAry['szOriginPostCodeTemp'] = $szOriginCountryArr['szPostcodeTemp'];
        $postSearchAry['idOriginCountry'] = $idOriginCountry;

        $postSearchAry['iOriginPostcodeFoundAtStep'] = 1;

        if(empty($postSearchAry['szOriginPostCode']))
        {
            $postcodeCheckAry=array();
            $postcodeResultAry = array();

            $postcodeCheckAry['idCountry'] = $idOriginCountry ;
            $postcodeCheckAry['szLatitute'] = $szOriginCountryArr['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $szOriginCountryArr['szLng'] ;

            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
            if(!empty($postcodeResultAry))
            {
                $postSearchAry['szOriginPostCode'] = $postcodeResultAry['szPostCode'];
                $postSearchAry['iOriginPostcodeFoundAtStep'] = 2;
                $postSearchAry['szOriginPostCodeTemp'] = $postcodeResultAry['szPostCode']; 
            }
        } 
    } 
    
    if($szDesData!='')
    {
        $szDesCountryArr=reverse_geocode($szDesData,true); 

        $replaceAry = array(" ",",");
        $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szDestinationCountryStr']); 
        $szCountryStrSerialized = serialize($szDesCountryArr);   

        if(__ENVIRONMENT__ == "LIVE")
        {
            setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/',$_SERVER['HTTP_HOST'],true); 
            setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/',$_SERVER['HTTP_HOST'],true);
        }
        else
        {
            setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/');  
            setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/');
        }
        $postSearchAry['szDestinationCountry']=$szDesCountryArr['szCountryName'];
        $idDesCountry=$kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);

        if($idDesCountry<=0 && !empty($szDesCountryArr['szCountryCode']))
        {
            $kConfig->loadCountry(false,$szDesCountryArr['szCountryCode']);
            $idDesCountry = $kConfig->idCountry ; 
        }

        $postSearchAry['szDestinationCountry']=$szDesData;
        $postSearchAry['szDLat']=$szDesCountryArr['szLat'];
        $postSearchAry['szDLng']=$szDesCountryArr['szLng'];
        $postSearchAry['szDestinationCity']=$szDesCountryArr['szCityName'];
        $postSearchAry['szDestinationPostCode'] = $szDesCountryArr['szPostCode']; 
        $postSearchAry['iDestinationPostcodeFoundAtStep'] = 1;
        $postSearchAry['szDestinationPostCodeTemp'] = $szDesCountryArr['szPostcodeTemp'];
        $postSearchAry['idDestinationCountry'] = $idDesCountry;

        if(empty($postSearchAry['szDestinationPostCode']))
        {
            $postcodeCheckAry = array(); 
            $postcodeResultAry = array(); 
            $postcodeCheckAry['idCountry'] = $idDesCountry ;
            $postcodeCheckAry['szLatitute'] = $szDesCountryArr['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $szDesCountryArr['szLng'] ;
            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

            if(!empty($postcodeResultAry))
            {
                $postSearchAry['szDestinationPostCode'] = $postcodeResultAry['szPostCode'];
                $postSearchAry['szDestinationPostCodeTemp'] = $postcodeResultAry['szPostCode'];
                $postSearchAry['iDestinationPostcodeFoundAtStep'] = 2;
            }
        } 
    }
    $postSearchAry['iDoNotShowPrivatePopForVogaPage']=true;

     
    $kExplain = new cExplain();
    $searchNotificationArr = array();
    $searchNotificationArr = $kExplain->isSearchNotificationExists($postSearchAry);
    
    
    
    $szFormId = sanitize_all_html_input($_REQUEST['formid']);
    $szPageUrl = sanitize_all_html_input($_REQUEST['page_url']);
    $iHiddenFlag = sanitize_all_html_input($_REQUEST['iHiddenValue']);
    $iSearchMiniVersion = sanitize_all_html_input($_REQUEST['iSearchMiniPage']);
    $szFromPage = sanitize_all_html_input($_REQUEST['from_page']);
    
    $additionalAry = array();
    $additionalAry['szFormId'] = $szFormId;
    $additionalAry['szPageUrl'] = $szPageUrl;
    $additionalAry['iHiddenFlag'] = $iHiddenFlag;
    $additionalAry['iSearchMiniVersion'] = $iSearchMiniVersion;
    $additionalAry['szFromPage'] = $szFromPage;
    

    if(!empty($searchNotificationArr))
    { 
        echo "SUCCESS_LOCAL_SEARCH||||";
        echo display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,'','',$iSearchMiniPageVersion,$additionalAry);
        die;
    }
}
?>