<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$display_abandon_message = true;

$szMetaTitle = __BOOKING_CONFIRMATION_META_TITLE__;
$szMetaKeywords = __BOOKING_CONFIRMATION_META_KEYWORDS__;
$szMetaDescription = __BOOKING_CONFIRMATION_META_DESCRIPTION__;

$iBookingTermsPage = 1 ;
require_once(__APP_PATH_LAYOUT__."/preview_header_new.php");

$t_base = "BookingConfirmation/";
$iLanguage = getLanguageId();

$userSessionData = array();
$userSessionData = get_user_session_data(true);
$idUserSession = $userSessionData['idUser'];

$kBooking = new cBooking();
$kConfig = new cConfig();
$kRegisterShipCon = new cRegisterShipCon();
$kWHSSearch = new cWHSSearch(); 

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

$postSearchAry = $kBooking->getBookingDetails($idBooking);

$dtTodayDate = date('j'); 
$iMonth = date('m');
$szMonthName = getMonthName($iLanguage,$iMonth);
$dtTodayDate .=" ".$szMonthName.", ".date('Y');
 
?>
<script> 
function PrintDiv()
{    /*
      var divToPrint = document.getElementById("terms_condtion_container");
      var popupWin = window.open("", "_blank", "width=900,height=700");
      popupWin.document.open();
      popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
      popupWin.document.close();
      */
      window.print();
}
//PrintDiv();

</script>
<div id="hsbody-2">
<div class="clearfix" id="terms_condtion_container">
<h2><?=t($t_base.'fields/booking_terms');?> <?php echo $dtTodayDate;  ?></h2>
<div class="terms-text">
	<?php
	echo terms_condition_popup_html($postSearchAry,$t_base,$iLanguage,true);
	?>
	<br>
</div>
</div>
</div>

<?php
echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
//require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>