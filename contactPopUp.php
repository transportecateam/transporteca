<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$successFlag=false;
$subject="Contact Form submitted on Transporteca.com the";
if(!empty($_POST['contactArr']))
{
	if($kUser->contactEmail($_POST['contactArr'],$subject))
	{
		$successFlag=true;
		$_POST['contactArr']=array();
	}
}

$t_base="Contact/";
$t_base_error="Error/";
?>
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup contact-popup">
<p class="close-icon" align="right">
<a onclick="cancel_remove_user_popup('contactPopup');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
	<?php
		if(!empty($kUser->arErrorMessages)){
		?>
		<div id="regError" class="errorBox ">
		<div class="header"><?=t($t_base_error.'please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kUser->arErrorMessages as $key=>$values)
			{
			?><li><?=$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<script>
		$("#contactButton").attr("onclick","sumit_contact_form()");
		</script>
		<?php }
			if($successFlag)
			{
			?>
			<h5><?=t($t_base.'title/contact');?></h5>
			<p><?=t($t_base.'messages/success_send');?></p>
			<p align="center"><a href="javascript:void(0)" class="button2" onclick="cancel_remove_user_popup('contactPopup');"><span><?=t($t_base.'fields/close');?> </span></a></p>
			<?
			}else{
		?>
		
	<form name="contactForm" id="contactForm" method="psot">
	<h5><?=t($t_base.'title/contact');?></h5>
	<p><?=t($t_base.'fields/we_look_forward_reading');?></p>
	<p><textarea name="contactArr[szMessage]" id="szMessage"><?=$_POST['contactArr']['szMessage']?></textarea></p>
	<br>
	<p><input type="checkbox" name="contactArr[contactEditEmail]" id="contactEditEmail" value="1" <? if($_POST['contactArr']['contactEditEmail']=='1') {?> checked <? }?> onclick="enable_email_field();"> <?=t($t_base.'fields/i_would_like_receive');?>:</p>
	<p><input type="text" name="contactArr[szEmail]" id="szEmail" value="<?=$_POST['contactArr']['szEmail'] ? $_POST['contactArr']['szEmail'] : $kUser->szEmail ?>" <? if($_POST['contactArr']['contactEditEmail']!='1') {?> readonly style="background:#DBDBDB;color:#828282;"<? }?> /></p>
	<br />
	<p align="center"><a href="javascript:void(0)" class="button2" onclick="cancel_remove_user_popup('contactPopup');"><span><?=t($t_base.'fields/close');?> </span></a> <a href="javascript:void(0)" class="button1" onclick="sumit_contact_form();" id="contactButton"><span><?=t($t_base.'fields/submit');?> </span></a></p>
	</form>
	<? }?>
</div>
</div>