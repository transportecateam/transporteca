<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$iNewLayoutDesign = 1;

$szURL = $_SERVER['REQUEST_URI'] ;

$kExplain= new cExplain();
$kWHSSearch=new cWHSSearch();
$szLanguage = sanitize_all_html_input($_REQUEST['lang']); 
$iLanguage = getLanguageId($szLanguage);
  
$landingPageDataArys = array();
$landingPageDataAry = array();

if(!empty($szURL))
{
    $szURLAry = explode("/",urldecode($szURL));  
    $szURL = $szURLAry[1]; 
    if($szURL=='da')
    {
         $szURL = "/".$szURLAry[1]."/".$szURLAry[1]."/"; 
    }
    if(!empty($szURL))
    {
        $landingPageDataArys = $kExplain->getAllNewLandingPageData(false,$szURL);
        $landingPageDataAry = $landingPageDataArys[0];
    }
}

if(empty($landingPageDataAry))
{
    //fetching default Landing page.
    $landingPageDataArys = $kExplain->getAllNewLandingPageData(false,false,false,$iLanguage,false,true);
    $landingPageDataAry = $landingPageDataArys[0];
} 
$idLandingPage = $landingPageDataAry['id'] ; 
$_SESSION['Landing_Page_ID']=$idLandingPage;
$customerTestimonialAry = array();
$customerTestimonialAry = $kExplain->getAllTestimonials($idLandingPage);

$partnerLogoAry = array();
$partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);

if(!empty($landingPageDataAry['szMetaTitle']))
{
    $szMetaTitle = $landingPageDataAry['szMetaTitle'] ;
}
else 
{
    $szMetaTitle = __LANDING_PAGE_META_TITLE__ ;
}

if(!empty($landingPageDataAry['szMetaKeywords']))
{
    $szMetaKeywords = $landingPageDataAry['szMetaKeywords'] ;
}
else
{
    $szMetaKeywords = __LANDING_PAGE_META_KEYWORDS__;
}

if(!empty($landingPageDataAry['szMetaDescription']))
{
    $szMetaDescription = $landingPageDataAry['szMetaDescription'];
}
else
{
    $szMetaDescription = __LANDING_PAGE_META_DESCRIPTION__;
}

$szDefaultFromField = $landingPageDataAry['szTextB2'];
$idDefaultLandingPage = $landingPageDataAry['id'];
$iFromLandingPage = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
 
if($_SESSION['valid_confirmation']>0 || $_SESSION['display_booking_saved_popup']>0)
{
    echo display_confirmation_popup();
}  
$t_base = "home/homepage/"; 
$kConfig=new cConfig();
$number = 1;
$kBooking = new cBooking();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingNumber'])); 
$idBooking = $kBooking->getBookingIdByBookngRef($szBookingRandomNum);

$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking); 
$kCourierServices= new cCourierServices();
$newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($idBooking);

$checkFlagLabelUploaded=false;
if((int)$bookingDataArr['idFile']>0)
{
    $kBookingFile = new cBooking();   
    $kBookingFile->loadFile($bookingDataArr['idFile']);
 
    if($kBookingFile->iLabelStatus!=__LABEL_SEND_FLAG__ && $kBookingFile->iLabelStatus!=__LABEL_DOWNLOAD_FLAG__)
    {
        $checkFlagLabelUploaded=true;
    }
}
else
{
    $checkFlagLabelUploaded=true;
}
$courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);
  
$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
  
$szShipperDetail=$bookingDataArr['szConsigneeCompanyName'].", ".$bookingDataArr['szConsigneeCity'].", ".$bookingDataArr['szConsigneeCountry'];

$textDescription=str_replace("szProvider",$newCourierBookingAry[0]['szProviderName'],$landingPageDataAry['szTextB3']);
$textDescription=str_replace("szBookingReference",$bookingDataArr['szBookingRef'],$textDescription);
$textDescription=str_replace("szDetial",$szShipperDetail,$textDescription);
$_POST['shipperArr']=$bookingDataArr;
if($courierBookingArr[0]['szCollection']=='Scheduled')
{
  $_POST['shipperArr']['dtShipperPickUp']=date('d/m/Y',strtotime($courierBookingArr[0]['dtCollection']));
}
else
{
    $dtCutoffDateTime = strtotime(date('Y-m-d',strtotime($bookingDataArr['dtCutOff'])));
    $dtTodayDateTime = strtotime(date('Y-m-d'));
    
    if($dtCutoffDateTime>$dtTodayDateTime)
    {
        $_POST['shipperArr']['dtShipperPickUp']=date('d/m/Y',strtotime($bookingDataArr['dtCutOff']));
    }
    else
    {
        $_POST['shipperArr']['dtShipperPickUp']=date('d/m/Y');
    }
    
    //$error_text=t($kCourierServices->t_base_courier_label.'messages/msg_3') ;
    //$error_text=str_replace("szProvider",$allCourierBookingArr[0]['szProviderName'],$error_text);
    //$kCourierServices->addError( "dtShipperPickUp1" , $error_text );
   // print_r($bookingDataArr['dtCutOff']);
}
$_POST['shipperArr']['idBooking']=$idBooking;
//pr
$_SESSION['secondClickFlag']=0;
unset($_SESSION['valid_confirmation']);
$_SESSION['__BOOKING_TIMESTAMP__'] = '';
unset($_SESSION['__BOOKING_TIMESTAMP__']);
  
/**
* $_REQUEST['err']=1 means no record found on service page
* $_REQUEST['err']=2 means force user to login for confirming multiuser access ;
* $_REQUEST['err']=3 means no record found for this user either lcl pricing is deleted or user email is in selected forwarders non-acceptance list
 * * $_REQUEST['err']=4 means quotation is expured
**/
// geting all service type 
$kConfig =new cConfig();
$serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');

$postSearchAry = array();
$cargoAry = array();
$compareButtonDataAry = array();
$compareButtonDataAry = $kBooking->getCompareButtonDetails();
$iAvailableLclService = $kBooking->getAvailableLclService(); 
 
//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{ 
//    $iTotalUniqueIP = number_format((float)$compareButtonDataAry['iTotalUniqueIP'],0,'.','.');
//    $iTotalNumRows = number_format((float)$compareButtonDataAry['iTotalNumRows'],0,'.','.'); 
//    $iAvailableLclService = number_format((float)$iAvailableLclService,0,'.','.'); 
//	
//}
//else
//{
//    $iTotalUniqueIP = number_format((float)$compareButtonDataAry['iTotalUniqueIP'],0,'.',',');
//    $iTotalNumRows = number_format((float)$compareButtonDataAry['iTotalNumRows'],0,'.',','); 
//    $iAvailableLclService = number_format((float)$iAvailableLclService,0,'.',','); 
//}
$iTotalUniqueIP = number_format_custom($compareButtonDataAry['iTotalUniqueIP'],$iLanguage,0);
$iTotalNumRows = number_format_custom($compareButtonDataAry['iTotalNumRows'],$iLanguage,0); 
$iAvailableLclService = number_format_custom($iAvailableLclService,$iLanguage,0);
?> 
<script type="text/javascript">
jQuery().ready(function(){ 
      
    $('#video').click(function() {
        jQuery.fancybox(
        { 
                'href' : "#video_container_div"
        });
        console.log("clicked");
    });	
    $('.sl-slider').bxSlider();
    setTimeout(function(){ $(".sl-slider-div").css('visibility','visible'); },'500'); 
}); 
</script> 

<div id="Transportation_pop" style="display:none;"></div>
<section id="search-freight" class="shipper-labels" data-type="background" data-speed="10" class="pages" style="background-image:url('<?php echo $landingPageDataAry['szTextA1']; ?>');">    
    <article>
        <h1><?php echo $landingPageDataAry['szTextB']; ?></h1>
        <?php if(!empty($landingPageDataAry['szTextB3'])){ ?>
            <div class="search-description-box">
                <?php echo $textDescription; ?>
            </div>
        <?php } ?>
        <div id="transporteca_search_form_container">
            <?php 
                echo download_courier_label_html($kCourierServices,$checkFlagLabelUploaded);
            ?>
        </div>
        
    </article>
</section>   
<section class="what-is-in pages">
    <h2><?php echo $landingPageDataAry['szTextC']; ?></h2>
    <ul class="clearfix">
        <li class="quick">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD1']; ?>');">&nbsp;</div>
            <h3><?php echo $landingPageDataAry['szTextE1']; ?></h3>
            <p><?php echo $landingPageDataAry['szTextE2']; ?></p>
        </li>
        <li class="all-price">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD4']; ?>');">&nbsp;</div>
            <h3><?php echo $landingPageDataAry['szTextE3']; ?></h3>
            <p><?php echo $landingPageDataAry['szTextE4']; ?></p>
        </li>
        <li class="on-time">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD7']; ?>');">&nbsp;</div>
            <h3><?php echo $landingPageDataAry['szTextE5']; ?></h3>
            <p><?php echo $landingPageDataAry['szTextE6']; ?></p>
        </li>
    </ul>
</section>
<section class="how-does-work pages">
    <h2><?php echo $landingPageDataAry['szTextF1']; ?></h2>
    <ul class="clearfix">
        <li class="quick">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG1']; ?>')no-repeat 0 0;">&nbsp;</div>
            <p><?php echo $landingPageDataAry['szTextH1']; ?></p>
        </li>
        <li class="all-price">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG4']; ?>')no-repeat 0 0;">&nbsp;</div>
            <p><?php echo $landingPageDataAry['szTextH2']; ?></p>
        </li>
        <li class="on-time">
            <div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG7']; ?>')no-repeat 0 0;">&nbsp;</div>
            <p><?php echo $landingPageDataAry['szTextH3']; ?></p>
        </li>
    </ul>
</section>
<section id="partners" data-type="background" data-speed="15" class="pages" style='background-image:url("<?php echo $landingPageDataAry['szTextI1']; ?>");'>
    <article>
        <h2><?php echo $landingPageDataAry['szTextI4']; ?></h2>
        <div class="partners-number clearfix">
            <div class="professionals"><span><?php echo $iTotalUniqueIP; ?></span><br><?php echo $landingPageDataAry['szTextI5']; ?></div>
            <div class="companies"><span><?php echo $iTotalNumRows;?></span><br><?php echo $landingPageDataAry['szTextI6']; ?></div>
            <div class="routes"><span><?php echo $iAvailableLclService ; ?></span><br><?php echo $landingPageDataAry['szTextI7']; ?></div>
        </div>
            <ul class="clearfix">
            <?php
                if(!empty($partnerLogoAry))
                { 
                    $i=1;
                    foreach($partnerLogoAry as $partnerLogoArys)
                    {
                        $idImage="img_".$i;
                        ?>
                        <li id="<?=$idImage?>">
                            <img class="normal" src="<?php echo $partnerLogoArys['szLogoImageUrl']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
                            <img class="hover" src="<?php echo $partnerLogoArys['szWhiteLogoImageURL']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
                            <div><div><?php echo $partnerLogoArys['szToolTipText']; ?></div></div>
                        </li>
                        <?php
                        ++$i;
                    }
                }
            ?>
            </ul>
    </article>
</section> 
 
<section id="testimonials" class="pages">
	<article>
		<h2><?php echo $landingPageDataAry['szTextJ1']; ?></h2>
		<div class="testimonials-container" id="testimonials-container"> 
			<ul class="sl-slider">	
				<?php 
					if(!empty($customerTestimonialAry)) 
					{
						foreach($customerTestimonialAry as $customerTestimonialArys)
						{
							?>
							<li>
								<div class="sl-slider-div" style="visibility:hidden;">
									<div class="deco"><img src="<?php echo $customerTestimonialArys['szImageURL']; ?>" alt="<?php echo $customerTestimonialArys['szImageTitle']?>" title="<?php echo $customerTestimonialArys['szImageDesc']; ?>"></div>
									<h3><?php echo $customerTestimonialArys['szHeading']; ?></h3>
									<blockquote><p><?php echo nl2br($customerTestimonialArys['szTextDesc']); ?></p><cite><?php echo $customerTestimonialArys['szTitleName']; ?></cite></blockquote>
								</div>
							</li>
							<?php
						}
					}
				?>
			</ul>  
		</div>
	</article>
</section>  

<section id="video" class="pages" style="cursor:pointer;background-image:url('<?php echo $landingPageDataAry['szTextL1']; ?>');">
	<article>
		
	</article>
</section>   
<div style="display:none;">
	<div id="video_container_div" style="width:1020px;height:580px;overflow:hidden;">	
		<?php echo $landingPageDataAry['szTextL4']?>
	</div>
</div>
<?php if($landingPageDataAry['iDisplaySeoText']==1){ 
    
    $bgColor='';
    $bgColor="background-color:".$landingPageDataAry['szBackgroundColor'].""; 
    ?> 
    <div class="body-color <?php if($landingPageDataAry['szBackgroundColor']=='#fff' || $landingPageDataAry['szBackgroundColor']=='' || $landingPageDataAry['szBackgroundColor']=='#ffffff'){?> white<?php }?>" style="<?php echo $bgColor;?>">
        <div id="hsbody"> 
            <h2 align="center" class="heading"><?php echo $landingPageDataAry['szHeading']?></h2> 
            <?php echo str_replace("&#39;","'",$landingPageDataAry['szSeoDescription']);?>
        </div>
    </div>
    <?php	
}?>
<div id="all_available_service" style="display:none;"></div>
<?php
if($checkFlagLabelUploaded)
{
    $iLanguage = getLanguageId();
    
    $kConfig =new cConfig();
    $langArr=$kConfig->getLanguageDetails('',$iLanguage);
                    
    if(!empty($langArr) && $iLanguage==__LANGUAGE_ID_ENGLISH__)
    {
        $szLanguageCode=$langArr[0]['szLanguageCode'];
        $redirectUrl=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode."/";
    }
    else
    {
        $redirectUrl=__MAIN_SITE_HOME_PAGE_SECURE_URL__;
    }
?>
<div id="label_not_uploaded_error">
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:20%;">
            <h3><?php echo t($t_base.'messages/label_not_uploaded_heading'); ?></h3> <br/>
            <p><?php echo t($t_base.'messages/label_not_uploaded_message'); ?></p><br />
            <p align="center">
                   <a class="button1" onclick="redirect_url('<?php echo $redirectUrl;?>')" href="javascript:void(0);">
                        <span><?php echo t($t_base.'fields/ok'); ?></span>
                </a>
                </p>
        </div>
    </div>    
</div>
 <?php   
}
?>
<?php
//ssecho html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>