<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __BOOKING_GET_QUOTE_META_TITLE__;
$szMetaKeywords = __BOOKING_GET_QUOTE_META_KEYWORDS__;
$szMetaDescription = __BOOKING_GET_QUOTE_META_DESCRIPTION__;

$iDonotIncludeHiddenSearchHeader = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingDetails/";
$t_base_user = "Users/AccountPage/";
$t_base_error = "Error";
$kBooking = new cBooking();
$kConfig = new cConfig();
$kWHSSearch=new cWHSSearch();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
  
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
$postSearchAry = $kBooking->getBookingDetails($idBooking);
$idLandingPage = $postSearchAry['idLandingPage'] ;
 
if($postSearchAry['iQuotesStatus']>0)
{ 
    //It means this RFQ has already been sent so no need to send it again, we simply redirected user to landing page.
    ob_end_clean();
    header("Location:".__BASE_URL__);
    die;
}
if($idLandingPage<=0)
{
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
        $idLandingPage = 2; 
    }
    else
    {
        $idLandingPage = 1; 
    }  
} 
$userSessionData = array();
$userSessionData = get_user_session_data();
$idUserSession = $userSessionData['idUser'];

$iLanguage = getLanguageId();
 
if((int)$_REQUEST['idBooking']>0)
{
    $idBooking = (int)$_REQUEST['idBooking'] ;
    $idBooking = $idBooking ;

    $kBooking = new cBooking();
    if($idUserSession>0)
    {
        if($kBooking->isBookingBelongsToCustomer($idBooking,$idUserSession))
        {
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
            $booking_mode = 2;
        }
        else
        {
            header("Location:".__NEW_SEARCH_PAGE_URL__);
            die;
        }
    }
}
else if((int)$idBooking>0)
{
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);	

    if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
        echo display_booking_already_paid($t_base,$idBooking);
        die;
    }
}
else
{
    $redirect_url = __NEW_SEARCH_PAGE_URL__ ;
    ?>
    <div id="change_price_div">
    <?php
    display_booking_notification($redirect_url);
    echo "</div>";
} 
if(((int)$idUserSession>0) && ($_REQUEST['err']<=0))
{
    checkUserPrefferedCurrencyActive();
}
 
$kUser = new cUser();  
$kRegisterShipCon = new cRegisterShipCon(); 

//if($iLanguage==__LANGUAGE_ID_DANISH__) 
//{ 
//    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__'); 
//} 
//else 
//{
    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage); 
//}  
$kConfig = new cConfig();
$kForwarder = new cForwarder();

$kConfig->loadCountry($postSearchAry['idOriginCountry']);
$szOriginCountryName = $kConfig->szCountryName ;

$kConfig->loadCountry($postSearchAry['idDestinationCountry']);
$szDestinationCountryName = $kConfig->szCountryName ;

$iForwarderCount = 0;
$iForwarderCount = $kForwarder->getAllForwarderDetails(true);
 
$iWeekDay = date('w');
if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
{
    $szPageHeading = " ".t($t_base.'fields/latest_by')."." ;
}
else
{
    $szPageHeading = " ".t($t_base.'fields/with_in_24_details')."." ; 
}
$mode = 'GET_QUOTES';  
?>
<div id="hsbody-2" class="quote-page get-rates"> 
    <div id="all_available_service" style="display:none;"></div>
    <div id="customs_clearance_pop_right" class="help-pop right"></div> 
    <div id="customs_clearance_pop" class="help-pop"></div>
    <div id="change_price_div" style="display:none"></div>  
    <div class="hsbody-2-right">
        <div class="forwarder-ready clearfix">
            <div class="number" id='forwarder_count'><?php echo $iForwarderCount; ?>
                <!--<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />-->
            </div>
            <div class="text">
                <h3><?php echo t($t_base.'fields/booking_quotes_heading'); ?></h3>
                <p class="get-quote-sub-heading"><?php echo t($t_base.'fields/with_criteria_cannot_display_service').$szPageHeading; ?></p>
            </div>
        </div>
        <div class="get-price get-quote-pg-container" id="booking_quotes_details_container">
            <div class="clearfix">
                <div class="price-guarantee-points">
                    <h3><?php echo t($t_base.'fields/price_guarantee'); ?></h3>
                    <p>1. <?php echo t($t_base.'fields/price_guarantee_text_1'); ?></p>
                    <p>2. <?php echo t($t_base.'fields/price_guarantee_text_2'); ?></p>
                    <p class="read-more"><a href="javascript:void(0);" onclick="focus_price_guarantee('price-guarantee-container');" name='price-guarantee'><?php echo t($t_base.'fields/read_more'); ?></a></p>
                </div>
                <div class="price-form" id="booking-form-main-container">
                    <?php
                        echo display_booking_quotes_details_form($postSearchAry,$kRegisterShipCon,$mode);
                    ?>
                </div>
            </div>
        </div> 
        <div class="image-names clearfix">
            <div><?php echo t($t_base.'fields/morten_laekholm'); ?><span><?php echo t($t_base.'fields/partner_transporteca'); ?></span></div>
            <div><?php echo t($t_base.'fields/thorsten_boeck'); ?><span><?php echo t($t_base.'fields/partner_transporteca'); ?></span></div>
        </div>
    </div>
</div>
<?php echo display_get_quote_get_rate_bottom_section($mode); ?>
        
<script type='text/javascript'>
    $().ready(function(){ 
        setTimeout(function(){
           $("#forwarder_count").html('<?php echo $iForwarderCount; ?>');
       },'3000'); 
    });
</script>
<?php
    echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
?> 
<?php
    $iGetRateGetQuotePageAdword='1';
  require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>  
  