<?php
// Copyright 2009, FedEx Corporation. All rights reserved.
// Version 6.0.0
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
//require_once(__APP_PATH_CLASSES__.'/fedex-common.php');
$kBooking = new cBooking();
$kCourierServices = new cCourierServices();
$idBooking = (int)$_POST['idBooking'];
if((int)$idBooking>0)
{
    $bookingArr=$kBooking->getExtendedBookingDetails($idBooking); 
    $idServiceProvider = $bookingArr['idServiceProvider']; 
     
    $courierForwarderAgreementArr=$kCourierServices->getCourierAgreementData($idServiceProvider);
    
    $szAccountNumber=decrypt($courierForwarderAgreementArr[0]['szAccountNumber'],ENCRYPT_KEY);
    $szUsername=decrypt($courierForwarderAgreementArr[0]['szUsername'],ENCRYPT_KEY);
    $szMeterNumber=decrypt($courierForwarderAgreementArr[0]['szMeterNumber'],ENCRYPT_KEY);
    $szPassword=decrypt($courierForwarderAgreementArr[0]['szPassword'],ENCRYPT_KEY);

    // print_r($courierForwarderAgreementArr);
    //The WSDL is not included with the sample code.
    //Please include and reference in $path_to_wsdl variable.
    $path_to_wsdl = __APP_PATH__."/wsdl/TrackService_v9.wsdl";

    ini_set("soap.wsdl_cache_enabled", "0");

    $client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

    $request['WebAuthenticationDetail'] = array(
            'UserCredential' =>array(
                    'Key' => $szUsername, 
                    'Password' => $szPassword
            )
    );
    $request['ClientDetail'] = array(
            'AccountNumber' => $szAccountNumber, 
            'MeterNumber' => $szMeterNumber
    );

    $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Track Request using PHP ***');

    $request['Version'] = array(
            'ServiceId' => 'trck', 
            'Major' => '9', 
            'Intermediate' => '1', 
            'Minor' => '0'
    );
    $request['SelectionDetails'] = array(
            'PackageIdentifier' => array(
                    'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
                    'Value' => $bookingArr['szTrackingNumber'] // Replace 'XXX' with a valid tracking identifier
            )
    );
 
    try {
            if(setEndpoint('changeEndpoint')){
                    $newLocation = $client->__setLocation(setEndpoint('endpoint'));
            }

            $response = $client ->track($request); 
            $successFlag=false;
        if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
                    if($response->HighestSeverity != 'SUCCESS'){
                            $resArr=toArray($response->CompletedTrackDetails);
                    }else{
                    if ($response->CompletedTrackDetails->HighestSeverity != 'SUCCESS'){
                                $resArr=toArray($response->CompletedTrackDetails);

                            }else{
                                $resArr=toArray($response->CompletedTrackDetails); 
                                $trackingStatusArr=$resArr['TrackDetails']['StatusDetail'];
                                if($resArr['TrackDetails']['StatusDetail']['Code']!='')
                                {
                                    $kCourierServices->updateBookingCourierStatus($trackingStatusArr,$idBooking);
                                    echo $resArr['TrackDetails']['StatusDetail']['Description'];
                                } 
                            }
                    } 
        }else{
            //printError($client, $response);
        }  
    } catch (SoapFault $exception) {
        //printFault($exception, $client);
    }
}
?>