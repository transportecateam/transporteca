<?php
ob_start();
session_start();
 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
 
$szLanguage = sanitize_all_html_input($_REQUEST['lang']);
$iLanguage = getLanguageId($szLanguage);

$f = fopen(__APP_PATH_LOGS__."/test12.log", "a");
fwrite($f, "\n\n###################Page Started :".print_R($_REQUEST,true)."#######################\n");
$strReplace=$_REQUEST['szLink']; 
//$strReplace = $_SERVER['REQUEST_URI'] ;
$szLinkLastChar = substr($strReplace, -1);
     
if($szLinkLastChar == '/')
{
    $strReplace = $_SERVER['REQUEST_URI'];
    $szLinkLastChar = substr($strReplace, -1);   
    $stringLen = strlen($strReplace); 

    $szReuestUrl = substr($strReplace,0,($stringLen-1));  

    $SnapABugRef=__MAIN_SITE_HOME_PAGE_SECURE_URL__.$szReuestUrl;
    header("HTTP/1.1 301 Moved Permanently"); 
    header('Location:'.$SnapABugRef);
    die;
}
else
{
    $strReplace = explode("/",$strReplace);

    $_REQUEST['szLink']=$strReplace[0];
    $_REQUEST['szUrl']=$strReplace[1];

    if($_REQUEST['szLink']!='' && $_REQUEST['szUrl']!='')
    {
        $_REQUEST['menuflag']='ind';
        $_REQUEST['flag'] = 'ep'; 
    }
    else if($_REQUEST['szLink']!='')
    {
        $_REQUEST['menuflag']='agg';
    }  
}

$kExplain= new cExplain();
$t_base="ExplainPage/"; 
$levelExplainDataArr_page = array();


$mainTab=trim($_REQUEST['tabFlag']);
if($mainTab==1)
{
	if($iLanguage==2)
	{
		$subUrl=__HOW_IT_WORKS_URL_DA__;
	}
	else
	{
		$subUrl=__HOW_IT_WORKS_URL__;
	}
}
else if($mainTab==2)
{
	if($iLanguage==2)
	{
		$subUrl=__COMPANY_URL_DA__;
	}
	else
	{
		$subUrl=__COMPANY_URL__;
	}
}
else if($mainTab==3)
{
	if($iLanguage==2)
	{
		$subUrl=__TRANSTPORTPEDIA_URL_DA__;
	}
	else
	{
		$subUrl=__TRANSTPORTPEDIA_URL__;
	}
}  
if($_REQUEST['menuflag']=='agg')
{ 
    $szLink=sanitize_all_html_input(trim($_REQUEST['szLink'])); 
    $levelExplainDataArr_page = $kExplain->selectExplainId($szLink,$mainTab,$iLanguage); 
 
    $szLinkedPageUrl = $levelExplainDataArr_page['szLinkedPageUrl'];
    
    if(!empty($levelExplainDataArr_page))
    {
        if(!empty($levelExplainDataArr_page['szMetaTitle']))
        {
            $szMetaTitle= $levelExplainDataArr_page['szMetaTitle'];
        }
        else
        {
            $szMetaTitle= __EXPLAIN_META_TITLE__;
        }
        if(!empty($levelExplainDataArr_page['szMetaKeyWord']))
        {
                $szMetaKeywords= $levelExplainDataArr_page['szMetaKeyWord'];
        }
        else
        {
                $szMetaKeywords= __EXPLAIN_META_KEYWORDS__;
        } 

        if(!empty($levelExplainDataArr_page['szMetaDescription']))
        {
                $szMetaDescription = $levelExplainDataArr_page['szMetaDescription'];
        }
        else
        {
                $szMetaDescription = __EXPLAIN_META_DESCRIPTION__;
        }  

        $szAggregatePageUrl=__MAIN_SITE_HOME_PAGE_SECURE_URL__.$_SERVER['REQUEST_URI']; 
        $szOpenGraphAuthor = $levelExplainDataArr_page['szOpenGraphAuthor'];
        $szOpenGraphImagePath = __MAIN_SITE_HOME_PAGE_URL__."/image.php?img=".$levelExplainDataArr_page['szOpenGraphImage']."&w=300&h=300&temp=explainLevel2";
    }
}
else if($_REQUEST['menuflag']=='ind')
{ 
	$szUrl=sanitize_all_html_input(trim($_REQUEST['szUrl']));

	$szLink=sanitize_all_html_input(trim($_REQUEST['szLink']));
	
	$levelExplainDataArr_page = $kExplain->selectExplainId($szLink,$mainTab,$iLanguage); 
	
	$level3ExplainDataArr=array();
	if((int)$levelExplainDataArr_page['id']>0)
	{
		$level3ExplainDataArr=$kExplain->getExplainContentByUrl($szUrl,$levelExplainDataArr_page['id']);
		
		if(!empty($level3ExplainDataArr) && empty($level3ExplainDataArr[0]['szHeading']))
		{
			$level3ExplainDataArr[0]['szHeading'] = $levelExplainDataArr_page['szPageHeading'] ;
		} 
	}   
	if(!empty($level3ExplainDataArr))
	{
		$server_uri = $_SERVER['REQUEST_URI'];
		if(!empty($server_uri))
		{
			$szRedirectUrl = ''; 
			$serverAry = explode("/",$server_uri);
			$count_1 = count($serverAry);
			 
			$ctr=1;
			foreach($serverAry as $serverArys)
			{  
				if($count_1==$ctr)
				{
					break;
				}
				else if($ctr!=($count_1-1))
				{	
					$szRedirectUrl .= $serverArys."/" ; 
				}
				else
				{
					$szRedirectUrl .= $serverArys  ;
				}
				$ctr++;
			} 
		} 
		
		
		$SnapABugRef=__MAIN_SITE_HOME_PAGE_SECURE_URL__."".$szRedirectUrl."#".$level3ExplainDataArr[0]['szHeading'];
		  
		header("HTTP/1.1 301 Moved Permanently"); 
		header('Location:'.$SnapABugRef);
		die;
		
		if(!empty($level3ExplainDataArr[0]['szMetaTitle']))
		{
			$szMetaTitle= $level3ExplainDataArr[0]['szMetaTitle'];
		}
		else
		{
			$szMetaTitle= __EXPLAIN_META_TITLE__;
		}
		if(!empty($level3ExplainDataArr[0]['szMetaKeyWord']))
		{
			$szMetaKeywords= $level3ExplainDataArr[0]['szMetaKeyWord'];
		}
		else
		{
			$szMetaKeywords= __EXPLAIN_META_KEYWORDS__;
		} 
		
		if(!empty($level3ExplainDataArr[0]['szMetaDescription']))
		{
			$szMetaDescription = $level3ExplainDataArr[0]['szMetaDescription'];
		}
		else
		{
			$szMetaDescription = __EXPLAIN_META_DESCRIPTION__;
		}  
	}
}

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" ); 
$t_base="ExplainPage/";
 
$showFlag=true;
 
$mainTab=trim($_REQUEST['tabFlag']);
if($mainTab==1)
{
	if($iLanguage==2)
	{
		$subUrl=__HOW_IT_WORKS_URL_DA__;
	}
	else
	{
		$subUrl=__HOW_IT_WORKS_URL__;
	}
}
else if($mainTab==2)
{
	if($iLanguage==2)
	{
		$subUrl=__COMPANY_URL_DA__;
	}
	else
	{
		$subUrl=__COMPANY_URL__;
	}
}
else if($mainTab==3)
{
	if($iLanguage==2)
	{
		$subUrl=__TRANSTPORTPEDIA_URL_DA__;
	}
	else
	{
		$subUrl=__TRANSTPORTPEDIA_URL__;
	}
}  
if($_REQUEST['menuflag']=='agg')
{  
	$szLink=sanitize_all_html_input(trim($_REQUEST['szLink']));
	
	$levelExplainDataArr = array();
	$levelExplainDataArr = $levelExplainDataArr_page ;
	
	$iLanguage = getLanguageId();
	$toatlArr=$kExplain->selectPageDetailsUserEnd($iLanguage,$mainTab);
	$totalCount=count($toatlArr);
	
	
	if(!empty($toatlArr))
	{
		$i=0;
		foreach($toatlArr as $toatlArrs)
		{
			++$i;
			if($levelExplainDataArr['id']==$toatlArrs['id'])
			{
				break;
			}
			
		}
	}
	$toatlArr1=$kExplain->selectPageDetailsUserEnd($iLanguage,$mainTab);
	 
	if($i==1)
	{
		$totalCount1=$totalCount-1;
		$prevUrlText=$toatlArr1[$totalCount1]['szPageHeading'];
		$nextUrlText=$toatlArr1[1]['szPageHeading'];
		$prevUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$totalCount1]['szLink'];		
		$nextUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[1]['szLink'];
		
		$szPreviousLinkTitle = $toatlArr1[$totalCount1]['szLinkTitle'] ;
		$szNextLinkTitle = $toatlArr1[1]['szLinkTitle'] ;
		
	}
	else if($i==$totalCount)
	{
		$totalCount1=$totalCount-2;
		$prevUrlText=$toatlArr1[$totalCount1]['szPageHeading'];
		$nextUrlText=$toatlArr1[0]['szPageHeading'];
		$prevUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$totalCount1]['szLink'];
		$nextUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[0]['szLink'];
		
		$szPreviousLinkTitle = $toatlArr1[$totalCount1]['szLinkTitle'] ;
		$szNextLinkTitle = $toatlArr1[0]['szLinkTitle'] ;
	}
	else
	{
		$t=$i-2;
		$prevUrlText=$toatlArr1[$t]['szPageHeading'];
		$nextUrlText=$toatlArr1[$i]['szPageHeading'];
		$prevUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$t]['szLink'];
		$nextUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$i]['szLink'];	
		
		$szPreviousLinkTitle = $toatlArr1[$t]['szLinkTitle'] ;
		$szNextLinkTitle = $toatlArr1[$t]['szLinkTitle'] ;
	}
	
	if(empty($levelExplainDataArr))
	{
		$showFlag=false;
	}
}
else if($_REQUEST['menuflag']=='ind')
{  
	$szUrl=sanitize_all_html_input(trim($_REQUEST['szUrl']));
	$szLink=sanitize_all_html_input(trim($_REQUEST['szLink']));
	
	$levelExplainDataArr = array();
	$levelExplainDataArr = $levelExplainDataArr_page ;
	$level3ExplainDataArr=array();
	
	if((int)$levelExplainDataArr['id']>0)
	{
		$level3ExplainDataArr=$kExplain->getExplainContentByUrl($szUrl,$levelExplainDataArr['id']);
		
		if(!empty($level3ExplainDataArr) && empty($level3ExplainDataArr[0]['szHeading']))
		{
			$level3ExplainDataArr[0]['szHeading'] = $levelExplainDataArr['szPageHeading'] ;
		} 
	} 
	if(empty($level3ExplainDataArr))
	{
		$showFlag=false;
	}
} 
if($showFlag)
{
		if($_REQUEST['menuflag']=='agg')
		{			
			$currentOrder=$i;
			$prev=$currentOrder;
			if($currentOrder==1)
			{
				$prev=$totalCount;
			}
			else
			{
				$prev=$currentOrder-1;
			}
			$explainContentArr=array();
			$explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($levelExplainDataArr['id']);
			
			$next=$currentOrder+1;
			if($totalCount==$currentOrder)
			{
				$next=1;
			}
			?>
			<?php 
			if($totalCount>1)
			{?>
			 <div class="pagination-a">
				<ul>
					<li><a href="<?php echo $prevUrl;?>" title="<?php echo $szPreviousLinkTitle; ?>" onmouseover="shownav('leftDiv');" onmouseout="hidenav('leftDiv');"><?php echo "<< ".$prevUrlText." ".$prev."/".$totalCount;?></a>
					<div id="leftDiv" style="display:none;" class="text" onmouseover="shownav('leftDiv');" onmouseout="hidenav('leftDiv');"><a href="<?php echo $prevUrl;?>" title="<?php echo $szPreviousLinkTitle; ?>" ><?php echo $prevUrlText." ".$prev."/".$totalCount;?></a></div>
					</li>
					<li class="right-msg"><a href="<?php echo $nextUrl;?>" title="<?php echo $szNextLinkTitle; ?>" onmouseover="shownav('rightDiv');" onmouseout="hidenav('rightDiv');"><?php echo ">> ".$nextUrlText." ".$next."/".$totalCount; ?></a>
					<div id="rightDiv" class="text" style="display:none;" onmouseover="shownav('rightDiv');" onmouseout="hidenav('rightDiv');"><a href="<?php echo $nextUrl;?>" title="<?php echo $szNextLinkTitle; ?>"><?php echo $nextUrlText." ".$next."/".$totalCount;?></a></div>
					</li>
				</ul>
			  </div>
			  <?php }?>
			  <div class="social-section-head clearfix">
					<div class="text">
							<h1><?php echo $levelExplainDataArr['szPageHeading']?></h1>
							<h4><?php echo $levelExplainDataArr['szSubTitle']?></h4>
							<div class="link-container">
							<?php 
								if(!empty($explainContentArr))
								{
									$strReplace = $_SERVER['REQUEST_URI'];
									$szLinkLastChar = substr($strReplace, -1);  
									
									$szTagUrl='';
									if($szLinkLastChar=='/')
									{
										$stringLen = strlen($strReplace);
										$szTagUrl = substr($strReplace,0,($stringLen-1)); 
									} 
									
									//$szTagUrl='';
									$count_1 = count($explainContentArr);
									foreach($explainContentArr as $explainContentArrs)
									{ 
										$ctr++;  
										if(!empty($explainContentArrs['szHeading']))
										{
											?> 
											<a href="<?php echo $szTagUrl."#".trim($explainContentArrs['szHeading']) ?>" name="<?php echo trim($explainContentArrs['szHeading']); ?>" title="<?php echo $explainContentArrs['szLinkTitle']?>"><?php echo $explainContentArrs['szHeading']?></a>
											<?php
											if($count_1!=$ctr)
											{
												echo " | ";
											}
										}
									 }
								 }		
							?>
							</div>
						</div>	
						<div class="image"><img src="<?php echo __BASE_EXPLAIN_LEVEL2_IMAGES_URL__."/".$levelExplainDataArr['szHeaderImage']; ?>" alt="<?php echo $levelExplainDataArr['szPictureDescription']?>" title="<?php echo $levelExplainDataArr['szPictureTitle']?>" ></div>
			</div>
		</div>
		<?php	
			
			if(!empty($explainContentArr))
			{
				foreach($explainContentArr as $explainContentArrs)
				{
					$bgColor='';
					$bgColor="background-color:".$explainContentArrs['szBackgroundColor'].""; 
                                        $szIndividualPageContent = str_replace("&#39;","'",$explainContentArrs['szDescription']);
                                        $szIndividualPageContent = replaceSearchMiniConstants($szIndividualPageContent);
					?> 
                                        <div class="body-color <?php if($explainContentArrs['szBackgroundColor']=='#fff' || $explainContentArrs['szBackgroundColor']=='' || $explainContentArrs['szBackgroundColor']=='#ffffff'){?> white<?php }?>" style="<?php echo $bgColor;?>" id="<?php echo trim($explainContentArrs['szHeading']); ?>" name="<?php echo trim($explainContentArrs['szHeading']); ?>">
                                            <div id="hsbody">
                                                <?php if(!empty($explainContentArrs['szHeading'])){ ?>
                                                    <h2 align="center" class="heading"><?php echo $explainContentArrs['szHeading']?></h2>
                                                <?php }?>
                                                <?php echo $szIndividualPageContent; ?>
                                            </div>
                                        </div>
					<?php					
				}
			}	
			?> 
			<?php				
		}
		else if($_REQUEST['menuflag']=='ind')
		{
			?>
				<style>
					#hsbody a {
						color: <?=$level3ExplainDataArr[0]['szLinkColor'];?>
					}
				</style>
			<?php
			$bgColor="background-color:".$level3ExplainDataArr[0]['szBackgroundColor']."";
			$szLinKColor="color:".$level3ExplainDataArr[0]['szLinkColor']."";
                        
                        $szIndividualPageContent = str_replace("&#39;","'",$level3ExplainDataArr[0]['szDescription']);
                        $szIndividualPageContent = replaceSearchMiniConstants($szIndividualPageContent);
		?>
			<div class="body-color <?php if($level3ExplainDataArr[0]['szBackgroundColor']=='#fff' || $level3ExplainDataArr[0]['szBackgroundColor']=='' || $level3ExplainDataArr[0]['szBackgroundColor']=='#ffffff'){?> white<?php }?>" style="<?php echo $bgColor;?>;height:100%;">
				<div id="hsbody">
				<h1 align="center" class="heading"><strong><?php echo $level3ExplainDataArr[0]['szHeading']?></strong></h1>
				<?php echo $szIndividualPageContent;?><br/>
				<?php
					$explainContentArr=array();
					$explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($level3ExplainDataArr[0]['idExplainData']);
					
					if(count($explainContentArr)>1)
					{?>
						<p class="para" style="<?=$szLinKColor?>"><strong><?php echo t($t_base.'fields/related_topics');?></strong></p>
					<?php	
					}
					if(!empty($explainContentArr))
					{?>
					<ul class="ulclass" style="<?=$szLinKColor?>">
					<?php
						foreach($explainContentArr as $explainContentArrs)
						{
							if($explainContentArrs['id']!=$level3ExplainDataArr[0]['id'])
							{?>
								<li><a  style="<?=$szLinKColor?>" href="<?php echo __BASE_URL__."/".$subUrl."/".$szLink."/".$explainContentArrs['szUrl'].'/'?>"><?php echo $explainContentArrs['szPageName']?></a></li>
							<?php
							}
						}
						?>
						</ul>	
						<?php	
					}		
				?>
				</div>
			</div> 
		<?php		
		}
}
else
{
?>

<div id="hsbody">
<?php
pageNotFound();
?></div>
<?php
}
?>
<div id="all_available_service" style="display:none;"></div>
<?php 
require_once( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>