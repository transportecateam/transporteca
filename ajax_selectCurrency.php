<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH__ . "/inc/functions.php" );
include( __APP_PATH__ . "/inc/I18n.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");

$t_base = "home/homepage/";
$kBooking= new cWHSSearch();
if(!empty($_REQUEST['currencyAry']))
{
	if($kBooking->updateBookingWithCurrency($_REQUEST['currencyAry']['idCurrency'],$_REQUEST['currencyAry']['szBookingRandomNum']))
	{
		if($_REQUEST['szPageNameValue']=='SIMPLEPAGE')
		{
			$redirect_url = __SELECT_SIMPLE_SERVICES_URL__.'/'.trim($_REQUEST['currencyAry']['szBookingRandomNum']).'/';
		}
		else if($_REQUEST['szPageNameValue']=='NEW_LANDING_PAGE')
		{
			$redirect_url = __NEW_SERVICES_PAGE_URL__.'/'.trim($_REQUEST['currencyAry']['szBookingRandomNum']).'/';
		}
		else
		{
			$redirect_url = __SELECT_SERVICES_URL__.'/'.trim($_REQUEST['currencyAry']['szBookingRandomNum']).'/';
		}
		
		?>
		<script type="text/javascript">
		redirect_url('<?=$redirect_url?>');
		</script>
		<?
	}
} 
$kConfig=new cConfig();
$currencyAry = array();
$currencyAry = $kConfig->getBookingCurrency(false,true);
$requirement_page_flag = false;
if(!empty($_POST['searchAry']['szBookingRandomNum']))
{
	$szBookingRandomNum = sanitize_all_html_input(trim($_POST['searchAry']['szBookingRandomNum']));
}
elseif(!empty($_POST['booking_key']))
{
	// if control reaches to this block means request came from Requirement page
	$szBookingRandomNum = sanitize_all_html_input(trim($_POST['booking_key']));
	$requirement_page_flag = true;
	
}
?>
<script type="text/javascript">
	$().ready(function(){
		Custom.init();
	});
</script>
<?

if($requirement_page_flag)
{
	?>

<div id="popup-bg"  class="white-bg"></div>
<form action="" method="post" id="currency_popup_form" name="currency_popup_form" >
			<div id="popup-container">			
			<div class="standard-popup" style="margin-top:120px;">
			<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('Transportation_pop');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
			<h5><?=t($t_base.'fields/select_currency_requirement');?></h5>
			<p><?=t($t_base.'fields/select_currency_popup_title_requirement');?></p>			
			<p style="margin: 17px 129px 10px;min-height: 30px;" class="currency-option">
				<select class="styled" name="currencyAry[idCurrency]" id="idCurrency" onkeyup="on_enter_key_press(event,'submit_currency_popup','');" >
				<?
				 	if(!empty($currencyAry))
				 	{
				 		foreach($currencyAry as $currencyArys)
				 		{
				 			?>
				 				<option value="<?=$currencyArys['id']?>"><?=$currencyArys['szCurrency']?></option>
				 			<?
				 		}
				 	}
				 ?>
				</select>
			</p>			
			<div class="oh">
				<p class="fl-40" align="center">
					<a href="javascript:void(0);" onclick="submit_currency_popup()" class="orange-button1"><span><?=t($t_base.'fields/show_option');?></span></a>
				</p>
				<p class="fl-20" align="center"><?=t($t_base.'fields/or');?></p>
				<p class="fl-40" >
					<input type="hidden" name="currencyAry[szBookingRandomNum]" id="szBookingRandomNum_hidden" value="<?=$szBookingRandomNum?>" />
					<input type="hidden" name="booking_key" id="booking_key" value="<?=$szBookingRandomNum?>" />
					
					<a href="javascript:void(0)" style="font-size: 16px; margin-bottom: 8px;display: inline-block;" onclick="show_signin_popup('<?=__SELECT_SERVICES_URL__.'/'.$szBookingRandomNum.'/'?>');" class="f-size-16"><?=t($t_base.'fields/sign_in');?></a>
					<a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/SIGN_IN_TOOLTIP_HEADER_TEXT');?>','<?=t($t_base.'messages/SIGN_IN_TOOLTIP_TEXT');?>','currency_popup_tip',event);" onmouseout="hide_tool_tip('currency_popup_tip')">&nbsp;</a><br />
					
					<a href="javascript:void(0)" onclick="open_benefit_popup('Transportation_pop','transportation');"><?=t($t_base.'fields/benefit_sign_in');?></a>
					<a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/BENEFIT_SIGN_IN_TOOLTIP_HEADER_TEXT');?>','<?=t($t_base.'messages/BENEFIT_SIGN_IN_TOOLTIP_TEXT');?>','currency_popup_tip',event);" onmouseout="hide_tool_tip('currency_popup_tip')">&nbsp;</a><br />
					
					<a href="<?=__CREATE_ACCOUNT_PAGE_URL__.$szBookingRandomNum.'/'?>"><?=t($t_base.'fields/create_account');?></a>
					<a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CREATE_ACCOUNT_TOOLTIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CREATE_ACCOUNT_TOOLTIP_TEXT');?>','currency_popup_tip',event);" onmouseout="hide_tool_tip('currency_popup_tip')">&nbsp;</a>
				</p>
			</div>
		</div>
		</div>
		<div id="currency_popup_tip" class="help-pop"></div>
	</form>
<?
}
else
{
	?>
	<div id="popup-bg" class="white-bg"></div>
	<form action="" method="post" id="currency_popup_form" name="currency_popup_form" >
			<div id="popup-container">			
			<div class="standard-popup" style="margin-top:120px;">
			<p class="close-icon" align="right"><a heref="javascript:void(0);" onclick="closePopup('Transportation_pop');"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
			<h5><?=t($t_base.'fields/select_currency_requirement');?></h5>
			<p><?=t($t_base.'fields/select_currency_popup_title_requirement');?></p>			
			<p style="margin: 17px 129px 10px;min-height: 30px;" class="currency-option">
				<select class="styled" name="currencyAry[idCurrency]" id="idCurrency" onkeyup="on_enter_key_press(event,'submit_currency_popup','');" >
				<?
				 	if(!empty($currencyAry))
				 	{
				 		foreach($currencyAry as $currencyArys)
				 		{
				 			?>
				 				<option value="<?=$currencyArys['id']?>"><?=$currencyArys['szCurrency']?></option>
				 			<?
				 		}
				 	}
				 ?>
				</select>
			</p>			
			<div class="oh"><p class="fl-40" align="center"><a href="javascript:void(0);" onclick="submit_currency_popup()" class="orange-button1"><span><?=t($t_base.'fields/show_option');?></span></a></p><p class="fl-20" align="center"><?=t($t_base.'fields/or');?></p>
				<p class="fl-40">
					<input type="hidden" name="currencyAry[szBookingRandomNum]" id="szBookingRandomNum_hidden" value="<?=$szBookingRandomNum?>" />
					<input type="hidden" name="booking_key" id="booking_key" value="<?=$szBookingRandomNum?>" />
					
					<a href="javascript:void(0)" style="font-size: 16px; margin-bottom: 8px;display: inline-block;" onclick="show_signin_popup('<?=__SELECT_SERVICES_URL__.'/'.$szBookingRandomNum.'/'?>');" class="f-size-16"><?=t($t_base.'fields/sign_in');?></a> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/SIGN_IN_TOOLTIP_HEADER_TEXT');?>','<?=t($t_base.'messages/SIGN_IN_TOOLTIP_TEXT');?>','currency_popup_tip',event);" onmouseout="hide_tool_tip('currency_popup_tip')">&nbsp;</a><br />
					<a href="javascript:void(0)" onclick="open_benefit_popup('Transportation_pop','transportation');"><?=t($t_base.'fields/benefit_sign_in');?></a> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/BENEFIT_SIGN_IN_TOOLTIP_HEADER_TEXT');?>','<?=t($t_base.'messages/BENEFIT_SIGN_IN_TOOLTIP_TEXT');?>','currency_popup_tip',event);" onmouseout="hide_tool_tip('currency_popup_tip')">&nbsp;</a><br />
					<a href="<?=__CREATE_ACCOUNT_PAGE_URL__.$szBookingRandomNum.'/'?>"><?=t($t_base.'fields/create_account');?></a> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CREATE_ACCOUNT_TOOLTIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CREATE_ACCOUNT_TOOLTIP_TEXT');?>','currency_popup_tip',event);" onmouseout="hide_tool_tip('currency_popup_tip')">&nbsp;</a>
				</p>
			</div>
		</div>
		</div>
		<div id="currency_popup_tip" class="help-pop"></div>
	</form>
	<?
}
	?>