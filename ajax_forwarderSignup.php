<?php
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );


$t_base = "Forwarders/";
$t_base_error="Error";
$successFlag=false;
$kConfig = new cConfig();
$kForwarder = new cForwarder();

$subject="Forwarder Contact Form submitted on"; 
if(!empty($_POST['forwarderSignupArr']))
{
 	if($kForwarder->forwarderSignUp($_POST['forwarderSignupArr'],$subject))
 	{
 		$successFlag=true;
 	}
}
if(!empty($_POST['forwarderSignupArr']['szPhoneNumberUpdate']))
{
	$_POST['forwarderSignupArr']['szPhoneNumber']=urldecode(base64_decode($_POST['forwarderSignupArr']['szPhoneNumberUpdate']));
}
$iLanguage = getLanguageId();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
?>
<?php
if(!empty($kForwarder->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kForwarder->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }
if($successFlag){
?>
<table border="0" cellpadding="0" cellspacing="0" class="profile-done-alert-forwarder" >
	<tr>
	<td valign="middle">
		<div class="profile-alert-content">
		   <div id="activationkey"></div>
			<p><?=t($t_base.'messages/success_msg1');?></p>			
			<p><?=t($t_base.'messages/success_msg2');?></p>
		</div>
	</div>
	</td>
	</tr>
</table>
<script type="text/javascript">
 $('input, select').attr('disabled', 'disabled');
 $("#submit_forwarder_button").removeAttr('href');
 $("#submit_forwarder_button").attr("onclick","");
 $("#submit_forwarder_button").attr("style","opacity:0.4;");
  $("#clear_forwarder_button").removeAttr('href');
 $("#clear_forwarder_button").attr("onclick","");
 $("#clear_forwarder_button").attr("style","opacity:0.4;");
 </script>
<? }?>
<form name="forwarderSignup" id="forwarderSignup" method="post" action="">
		<p ><h1><?=t($t_base.'title/forwarder_signup')?></h1>&nbsp;<a href="<?=__BASE_URL__?>/forwarder/" ><?=t($t_base.'title/haven_read')?>?</a></p>
		<br />
		<p><?=t($t_base.'title/plz_fill_contact_details')?></p>
		<br />
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/c_name');?></span>
			<span class="field-container"><span><input type="text" name="forwarderSignupArr[szCompanyName]" id="szCompanyName" value="<?=$_POST['forwarderSignupArr']['szCompanyName']?>"/></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
			<span class="field-container"><span><input type="text" name="forwarderSignupArr[szFirstName]" id="szFirstName" value="<?=$_POST['forwarderSignupArr']['szFirstName']?>" /></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
			<span class="field-container"><span><input type="text" name="forwarderSignupArr[szLastName]" id="szLastName" value="<?=$_POST['forwarderSignupArr']['szLastName']?>" /></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/email');?></span>
			<span class="field-container"><span><input type="text" name="forwarderSignupArr[szEmail]" id="szEmail" value="<?=$_POST['forwarderSignupArr']['szEmail']?>" /></span></span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/country');?></span>
			<span class="field-container"><span>
				<select size="1" name="forwarderSignupArr[szCountry]" id="szCountry">
					<option value=""><?=t($t_base.'fields/select_country');?></option>
					<?php
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								?><option value="<?=$allCountriesArrs['szCountryName']?>" <?php if($allCountriesArrs['szCountryName']==$_POST['forwarderSignupArr']['szCountry']){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
								<?php
							}
						}
					?>
				</select></span>
			</span>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/p_number');?></span>
			<span class="field-container"><span><input type="text"  name="forwarderSignupArr[szPhoneNumber]" id="szPhoneNumber" value="<?=$_POST['forwarderSignupArr']['szPhoneNumber']?>" onfocus="openTip('pno');" onblur="closeTip('pno');"/></span></span>
			<div class="field-alert">
				<div id="pno" style="display:none"><?=t($t_base.'messages/p_number_message');?></div>
			</div>
		</label>
		<label class="profile-fields  clearfix">
			<span class="field-name"><?=t($t_base.'fields/website');?></span>
			<span class="field-container"><span><input type="text" name="forwarderSignupArr[szWebsite]" id="szWebsite" value="<?=$_POST['forwarderSignupArr']['szWebsite']?>" /></span></span>
		</label>
		
		<br />
		<br />
		<p align="center">
		<input type="hidden" name="forwarderSignupArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="" />
		<a href="javascript:void(0)" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');signup_forwarder();" id="submit_forwarder_button"><span><?=t($t_base.'fields/submit');?></span></a> <a href="javascript:void(0)" class="button2" id="clear_forwarder_button" onclick="clear_forwarder_form();"><span><?=t($t_base.'fields/clear');?></span></a></p>
		</form>