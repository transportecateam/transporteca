<?php
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
require_once(__APP_PATH__."/zooz/zooz.php");
     
if(!empty($_REQUEST['sessionToken']))
{
    $kZoozLib = new cZoozLib();
    $kZooz = new cZooz();
          
    $result = array();
    $result = $_REQUEST; 
    
    $addZoozLogsAry = array();
    $addZoozLogsAry['idTransaction'] = $result['transactionID']; 
    $addZoozLogsAry['idTransactionDisplay'] = $result['transactionDisplayID']; 
    $addZoozLogsAry['szSessionToken'] = $_REQUEST['sessionToken']; 
     
    $check = $kZoozLib->verifyTransaction($result['transactionID']);
  
    $trxId = $result['transactionID'] ;
    
    if($check['result']) 
    {
        $status = 'Completed';
        $addZoozLogsAry['szStatus'] = "PAYMENT_COMPLETED";
        $addZoozLogsAry['szDeveloperNotes'] = "Credit Card payment successfully completed.";
    } 
    else 
    {
        $status = 'Failed';
        $addZoozLogsAry['szStatus'] = "PAYMENT_FAILED";
        $addZoozLogsAry['szDeveloperNotes'] = "Credit Card payment failed. check zoozPayment.log for more details";
    }  
    
    
    $kZooz->updateTransactionDetails($addZoozLogsAry); 
    $kZooz->updateBookingZoozPaymentDetail($trxId,$status);
     
    if((int)$_SESSION['booking_id']>0)
    {
        $idBooking = (int)$_SESSION['booking_id'] ;
        $kBooking = new cBooking();
        $kConfig = new cConfig();
        $kForwarderContact = new cForwarderContact();
        $kUser = new cUser();
        $kWHSSearch = new cWHSSearch();
        $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);

        $kConfig = new cConfig();
        $kConfig->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig->szCountryISO ;
        $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
        $szCountryStrUrl .= $kConfig->szCountryISO ;

        $thank_you_page_url =  __THANK_YOU_PAGE_URL__."/".__THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;

        if($postSearchAry['szPaymentStatus']=='Completed')
        { 
            confirmBookingAfterPayment($postSearchAry);
            if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
            {
                ob_end_clean();
                header("Location:".$thank_you_page_url);
                die();
            }
            else
            {
                ob_end_clean();
                header("Location:".__BOOKING_RECEIPT_PAGE_URL__);
                die();
            }
        }
        else if($postSearchAry['szPaymentStatus']=='Pending')
        {
            if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
            {
                ob_end_clean();
                header("Location:".$thank_you_page_url);
                die();
            }
            else
            {			
                ob_end_clean();
                header("Location:".__BOOKING_RECEIPT_PAGE_URL__);
                die();
            }
        }
        else
        {
            $_SESSION['statusFailed']='Failed';
            if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
            {
                ob_end_clean();
                header("Location:".__NEW_BOOKING_OVERVIEW_PAGE_URL__);
                die();
            }
            else
            {
                ob_end_clean();
                header("Location:".__BOOKING_CONFIRMATION_PAGE_URL__);
                die();
            } 
        }
    }
    else
    { 
        $_SESSION['sessionBookingId']='Not Found';
        header("Location:".__HOME_PAGE_URL__);
        die();
    }
}
else
{ 
    $_SESSION['sessionBookingId']='Not Found';
    header("Location:".__HOME_PAGE_URL__);
    die();
}
?>