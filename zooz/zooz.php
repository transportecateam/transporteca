<?php
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
require_once( __APP_PATH_CLASSES__ . "/booking.class.php");


/*
 * This File is no longer in use. We have kept this only for backup purpose.
 * 
 */
if((int)$_SESSION['booking_id']<=0)
{ 
    $_SESSION['sessionBookingId']='Not Found';
    ?>
    <script type="text/javascript">
    window.location.href='<?=__HOME_PAGE_URL__?>';
    </script>
    <?php
    die();
}

$kBooking = new cBooking();
$bookingSearchedAry = array();
$bookingSearchedAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']);

// Flag to indicate whether sandbox environment should be used
$isSandbox = true;
$f = fopen(__APP_PATH__."/logs/zoozApiLog.log", "a");
fwrite($f, "\n\n###################Started-:".date("d-m-Y h:i:s")."#######################\n");
$url;

if ($isSandbox == __URL_FLAG__) {
	$url = 'https://sandbox.zooz.co/mobile/SecuredWebServlet';
} else {
	$url = 'https://app.zooz.com/mobile/SecuredWebServlet';
}
fwrite($f, "Url-:".$url."\n");
// is cURL installed yet?
if (!function_exists('curl_init')){
	die('Sorry cURL is not installed!');
}

// OK cool - then let's create a new cURL resource handle
$ch = curl_init();
fwrite($f, "post-:".print_r($_REQUEST,true)."\n");
// Now set some options

// Set URL
curl_setopt($ch, CURLOPT_URL, $url);



//Header fields: ZooZUniqueID, ZooZAppKey, ZooZResponseType
curl_setopt($ch, CURLOPT_HTTPHEADER, array('ZooZUniqueID:'.__API_UNIQUE_KEY__,'ZooZAppKey:'.__API_KEY__,'ZooZResponseType: NVP'));
fwrite($f, "ZooZUniqueID-: com.zooz.mobileweb.transporteca1\n");
fwrite($f, "ZooZAppKey-: ea3e4c9a-53ae-4ba4-85e1-db50a8edd4d4\n");
fwrite($f, "ZooZResponseType-: NVP\n");
//If it is a post request
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
// Timeout in seconds
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
fwrite($f, "cmd-: ".$_REQUEST["cmd"]."\n");
if (isset($_REQUEST["cmd"]) && $_REQUEST["cmd"] == "openTrx") 
{ 
    //Mandatory POST fields: cmd, amount, currencyCode
    $postFields = "cmd=".$_REQUEST['cmd']."&amount=".$_REQUEST['amount']."&currencyCode=".$_REQUEST['currencyCode'];

    //Optional POST fields:
    $postFields .= "&email=".$_REQUEST['email'];
    $postFields .= "&firstName=".$_REQUEST['firstName'];
    $postFields .= "&lastName=".$_REQUEST['lastName'];
    $postFields .= "&invoice.additionalDetails=".$_REQUEST['additionalDetails'];
    $postFields .= "&invoice.number=".$_REQUEST['number'];
    $postFields .= "&user.addresses.billing.zipCode = ".$bookingSearchedAry['szShipperPostCode'];
    $postFields .= "&user.addresses.billing.countryCode = ".$bookingSearchedAry['szShipperCountryCode'];
    $postFields .= "&user.addresses.billing.city = ".$bookingSearchedAry['szShipperCity']; 
    $postFields .= "&user.addresses.shipping.zipCode = ".$bookingSearchedAry['szConsigneePostCode'];
    $postFields .= "&user.addresses.shipping.countryCode = ".$bookingSearchedAry['szConsigneeCountryCode'];
    $postFields .= "&user.addresses.shipping.city = ".$bookingSearchedAry['szConsigneeCity'];
  
    fwrite($f, "postFields-: ".print_r($postFields,true)."\n");
    curl_setopt ($ch, CURLOPT_POSTFIELDS, $postFields);

    ob_start();

    curl_exec($ch);

    $result = ob_get_contents();

    ob_end_clean();
    fwrite($f, "result-: ".print_r($result,true)."\n");
    parse_str($result);
    fwrite($f, "statusCode-: ".print_r($statusCode,true)."\n");	
    if ($statusCode == 0) { 
            // Get token from ZooZ server
            $trimmedSessionToken = rtrim($sessionToken, "\n");
            fwrite($f, "trimmedSessionToken-: ".$trimmedSessionToken."\n");		
            // Send token back to page
            echo "var data = {'token' : '" . $trimmedSessionToken . "'}";

    } else if (isset($errorMessage)) { 
            echo "ERROR||||";
            echo display_zooz_api_error_popup($errorMessage);
            die;

            //echo "Error to open transaction to ZooZ server. " . $errorMessage;

            //echo "var data = {'errorMessage': '".$errorMessage."'}"; 

    } 
} else {

	fwrite($f, "cmd-: verifyTrx\n");
	fwrite($f, "trxId-: ".$_REQUEST["trxId"]."\n");
	//Post fields: cmd, trxId
	$trxId=$_REQUEST["trxId"];
	$transactionDisplayID=$_REQUEST['transactionDisplayID'];
	$token=$_REQUEST['token'];
	$sessionToken=$_REQUEST['sessionToken'];
	$kBooking = new cBooking();
	$postSearchAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']);
	$kZooz = new cZooz();
	$kZooz->add($_REQUEST,$postSearchAry['szBookingRef']);
	curl_setopt ($ch, CURLOPT_POSTFIELDS, "cmd=verifyTrx&trxId=".$trxId."&transactionDisplayID=".$_REQUEST["transactionDisplayID"]);
	//print_r($ch);
	ob_start();

	curl_exec($ch);

	$result = ob_get_contents();
	fwrite($f, "result_verifyTrx-: ".print_r($result,true)."\n");
	ob_end_clean();

	parse_str($result);
	
	if ($statusCode == 0) {
	
	fwrite($f, "statusCode_verifyTrx-: ".$statusCode."\n");
		//You can use the value of sessionToken to check if it equals the sessionToken you recieved in openTrx
		$trimmedSessionToken = rtrim($sessionToken, "\n");
		fwrite($f, "trimmedSessionToken_verifyTrx-: ".$trimmedSessionToken."\n");
		//trxToken is meant to be used with the ZooZ Extended Server API (See www.zooz.com for more details)
		$trimmedTrxToken = rtrim($transactionID, "\n");
		fwrite($f, "transactionID_verifyTrx-: ".$transactionID."\n");
		//echo "<script>window.location = '/zooz/thankyou.php'</script>";
		$status="Completed";
			
	} else {
			
		//echo "<script>window.location = '/zooz/failed.html'</script>";
			$status="Failed";
	}
	
	$kZooz->updateBookingZoozPaymentDetail($trxId,$status);
}


// Close the cURL resource, and free system resources
curl_close($ch);
fwrite($f, "\n\n###################End-:".date("d-m-Y h:i:s")."#######################\n");
?>