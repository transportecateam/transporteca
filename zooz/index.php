<?php
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
require_once( __APP_PATH_CLASSES__ . "/booking.class.php");

$t_base = "Users/AccountPage/";

if((int)$_SESSION['booking_id']<=0)
{ 
    $_SESSION['sessionBookingId']='Not Found';
    ?>
    <script type="text/javascript">
    window.location.href='<?=__HOME_PAGE_URL__?>';
    </script>
    <?php
    die();
}

// Flag to indicate whether sandbox environment should be used
$isSandbox = true;
$f = fopen(__APP_PATH__."/logs/zoozApiLog.log", "a");
fwrite($f, "\n\n################### Started-: ".date("d-m-Y h:i:s")."#######################\n"); 

$szAdditionalValue = sanitize_all_html_input(trim($_REQUEST['var_val']));
$szOperationMode = sanitize_all_html_input(trim($_REQUEST['opr_mode']));

$kBooking = new cBooking();
$postSearchAry = array();
$postSearchAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']);
        
$kZoozLib = new cZoozLib();
if($postSearchAry['iInsuranceIncluded']==1)
{
    $fTotalCostForBooking =  $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] ; 
}
else
{
    $fTotalCostForBooking = $postSearchAry['fTotalPriceCustomerCurrency'];
} 
$fTotalCostForBooking = $fTotalCostForBooking + $postSearchAry['fTotalVat'] ;

//$fTotalCostForBooking = 1; 
$var_value="cmd=openTrx&amount=".(float)$fTotalCostForBooking."&currencyCode=".$postSearchAry['szCurrency']."&email=".$kUser->szEmail."&firstName=".$kUser->szFirstName."&lastName=".$kUser->szLastName."&additionalDetails=".$item_ref_text."&number=".$postSearchAry['szBookingRef']."";

if($szOperationMode=='OPEN_TRANSACTION')
{ 
    $transactionAry = array();
    $transactionAry = $postSearchAry ;
    $transactionAry['szFirstName'] = $postSearchAry['szFirstName'];
    $transactionAry['szLastName'] = $postSearchAry['szLastName'];
    $transactionAry['szEmail'] = $postSearchAry['szEmail'];
    $transactionAry['szCurrency'] = $postSearchAry['szCurrency']; 
    $transactionAry['szBookingRef'] = $postSearchAry['szBookingRef'];  
    $transactionAry['fTotalAmount'] = $fTotalCostForBooking;
    $transactionAry['szAdditionalDetails'] = $szAdditionalValue;
     
    $resultAry = array();
    $resultAry = $kZoozLib->newTransaction($transactionAry);
    
    $addZoozLogsAry = array();
    $addZoozLogsAry['idBooking'] = $postSearchAry['id'];
    $addZoozLogsAry['szBookingRef'] = $postSearchAry['szBookingRef'];
     
    if(!empty($resultAry['token']))
    { 
        $trimmedSessionToken = trim($resultAry['token']);
        
        $addZoozLogsAry['szToken'] = $trimmedSessionToken;
        $addZoozLogsAry['szSessionToken'] = $resultAry['sessionToken']; 
        $addZoozLogsAry['szStatus'] = 'PAYMENT_INITIATED';  
        $addZoozLogsAry['szDeveloperNotes'] = "Initialize zooz payment token";
                
        $kZooz = new cZooz();
        $kZooz->add($addZoozLogsAry);
        
        echo "var data = {'token' : '" . $trimmedSessionToken . "'}";
    }
    else 
    { 
        $addZoozLogsAry['szToken'] = '';
        $addZoozLogsAry['szSessionToken'] = $resultAry['sessionToken']; 
        $addZoozLogsAry['szStatus'] = 'PAYMENT_FAILED';  
        $addZoozLogsAry['szDeveloperNotes'] = "Initialization of zooz payment token has failed. Returning back to /overview/ page with sorry popup.";
                
        $kZooz = new cZooz();
        $kZooz->add($addZoozLogsAry);
        
        echo "ERROR||||";
        echo display_payment_gateway_disabled_popup();
        die; 
    }
} 
else
{
    $vars = explode( '&', $args[1] );  
    $result = Array();
    foreach( $vars as $var ) 
    {
        $actualVar = explode( '=', $var );
        $result[( $actualVar[0] )] = $actualVar[1];
    }
    $check = ZooZ::verifyTransaction($result['transactionID']);
    if( $check[ 'result' ] ) {
        print 'Paid!';
    } else {
        print 'Failed!';
    }
}

?>
