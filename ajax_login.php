<?php
/**
 * Customer Login
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base_error = "Error";
$t_base='Users/AccountPage/'; 
if((int)$_SESSION['user_id']>0 && $kUser->iConfirmed=='1')
{ 
    $redirect_url=__USER_MY_ACCOUNT_URL__;
 ?>
    <script type="text/javascript">
	$(location).attr('href','<?=$redirect_url?>');
    </script>
  <?php	
  die;
}
$kUser = new cUser();
$kBooking = new cBooking();
$iDoNotDisplayErrorMessage = sanitize_all_html_input(trim($_POST['loginArr']['iDoNotDisplayErrorMessage']));
if(!empty($_POST['loginArr']))
{
    if(trim($_POST['loginArr']['szEmail']) == t($t_base.'fields/email'))
    {
        $_POST['loginArr']['szEmail'] = '';
    }
    if(trim($_POST['loginArr']['szPassword']) == t($t_base.'fields/password'))
    {
        $_POST['loginArr']['szPassword'] = '';
    }
    $szFromPage = sanitize_all_html_input(trim($_POST['loginArr']['szFromPage']));
    $szBookingRandomKey = sanitize_all_html_input(trim($_POST['loginArr']['szBookingRandomKey']));
   
    if($kUser->userLogin($_POST['loginArr']))
    {
        $bRedirect = true;
        $_POST['bookingQuotesAry'] = array();
        
        if(!empty($szFromPage) && !empty($szBookingRandomKey))
        {
            $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomKey);
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            if(!empty($postSearchAry))
            {
                $bRedirect = false;
                echo "SUCCESS_RATE||||";
                echo display_booking_quotes_details_form($postSearchAry,$kRegisterShipCon,$szFromPage,true);
                die;
            }
        } 
        if($bRedirect)
        { 
            $_SESSION['logged_in_by_booking_process']='';
            unset($_SESSION['logged_in_by_booking_process']);

            if(!empty($_POST['loginArr']['szRedirectUrl']))
            {
                $redirect_url = $_POST['loginArr']['szRedirectUrl'] ;
            }
            else
            {
                $redirect_url=__BASE_URL__.'/myAccount/';
            } 
            if((int)$_SESSION['verified_email_user']>0 && (int)$_SESSION['user_id']>0)
            {
                $redirect_url=__HOME_PAGE_URL__;
                if((int)$_SESSION['verified_email_user']==(int)$_SESSION['user_id'])
                {
                    $_SESSION['valid_confirmation']='1';
                }
                else
                {
                    $_SESSION['valid_confirmation']='2';
                }
            }
            if((int)$_SESSION['user_id']>0)
            {
                $kUser->getUserDetails($_SESSION['user_id']);
                $kUserNew = new cUser();
                $kUserNew->loadCurrency($kUser->szCurrency);

                if(($kUserNew->iBooking<=0)) // User is in booking steps and currency is not active.
                {
                    echo "CURRENCY_CHANGED||||";
                    $_SESSION['user_temp_id'] = $_SESSION['user_id'];

                    $_SESSION['user_id'] = 0;
                    $_SESSION['idUser'] = 0;
                    unset($_SESSION['user_id']);
                    unset($_SESSION['idUser']);

                    echo displayCurrencyChangePopup($kUser->szCurrency,false,$redirect_url);
                    die;
                }
            }
            
            if((int)$_SESSION['verified_email_user']>0 && (int)$_SESSION['user_id']>0)
            {
                $redirect_url=__HOME_PAGE_URL__;
                if((int)$_SESSION['verified_email_user']==(int)$_SESSION['user_id'])
                {
                    $kUser->updateIConfirmedStatus($_SESSION['user_id']);
                    $_SESSION['valid_confirmation']='1';
                }
                else
                {
                    $_SESSION['valid_confirmation']='2';
                }         
            }

        ?>
            <script type="text/javascript">
                $(location).attr('href','<?=$redirect_url?>');
            </script>
        <?php	
        exit();
        }
    } 
    if((int)$_SESSION['verified_email_user']>0 && (int)$_SESSION['user_id']>0)
    {
        $redirect_url=__HOME_PAGE_URL__;
        if((int)$_SESSION['verified_email_user']==(int)$_SESSION['user_id'])
        {
            $kUser->updateIConfirmedStatus($_SESSION['user_id']);
            $_SESSION['valid_confirmation']='1';
            ?>
            <script type="text/javascript">
            $(location).attr('href','<?=$redirect_url?>');
            </script>
            <?php
            unset($_SESSION['verified_email_user']);
            exit();
        }
        else
        {
            $_SESSION['valid_confirmation']='2';
            ?>
            <script type="text/javascript">
            $(location).attr('href','<?=$redirect_url?>');
            </script>
            <?php
            exit();
        }
    }	
}
if(!empty($_REQUEST['redirect_url']))
{
    $_POST['loginArr']['szRedirectUrl'] = sanitize_all_html_input(trim($_REQUEST['redirect_url']));		
}
if(!empty($_REQUEST['cancel_url']))
{
    $_POST['loginArr']['szCancelUrl'] = sanitize_all_html_input(trim($_REQUEST['cancel_url']));
}
if(!empty($_REQUEST['booking_key']))
{
    $_POST['loginArr']['szBookingRandomKey'] = sanitize_all_html_input(trim($_REQUEST['booking_key']));
}
if(!empty($_REQUEST['from_page']))
{
    $_POST['loginArr']['szFromPage'] = sanitize_all_html_input(trim($_REQUEST['from_page']));
}

if((int)$_SESSION['user_id']>0)
{
    $kUser->getUserDetails($_SESSION['user_id']);
} 
$cancel_url="";
// if clicks on cancle redirect to landing page
if($_POST['loginArr']['szCancelUrl']==1)
{
    $cancel_url = __HOME_PAGE_URL__ ;
}

?>
<script type="text/javascript">
    var email = $("#szEmail").val();
    if(email!='')
    { 
        $("#szPassword").focus();			
        var $thisVal = $("#szPassword").val();
        $("#szPassword").val('').val($thisVal);
    }
    else
    {
        $("#szEmail").focus();			
        var $thisVal = $("#szEmail").val(); 
        $("#szEmail").val('').val($thisVal); 
    }
</script>
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
    <p class="close-icon" align="right">
        <a onclick="cancel_signin('<?=$cancel_url?>','ajaxLogin');" href="javascript:void(0);">
            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
        </a>
    </p>
    <div id="activationkey" style="display:none";></div>
    <form name="loginForm" id="loginForm" method="post">
        <?php

        if($_SERVER['HTTP_REFERER']==__BOOKING_DETAILS_PAGE_URL__."/")
        {
            $cancel_url=__BOOKING_DETAILS_PAGE_URL__;
        }
        if((isset($_POST['link']) && $_POST['link'] == 'showNoError') && ( $_POST['loginArr']['szEmail']!='' || $_POST['loginArr']['szPassword']!='' ) || !isset($_POST['link']) )
        {
            if(!empty($kUser->arErrorMessages) && $iDoNotDisplayErrorMessage!=1){
            ?>
            <div id="regError" class="errorBox ">
                <div class="header"><?=t($t_base_error.'/please_following');?></div>
                <div id="regErrorList">
                    <ul>
                        <?php foreach($kUser->arErrorMessages as $key=>$values){?>
                            <li><?=$values?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php }
        }
        if((int)$_SESSION['user_id']>0)
        {
            echo "<h5>".t($t_base.'messages/verification_email_1')."</h5>";
            echo "<p>".t($t_base.'messages/verification_email_2')." ".date('d/m/Y',strtotime($kUser->dtConfirmationCodeSent)).".</p><br/><p> ".t($t_base.'messages/verification_email_3')."<p/>"; 
            ?>
            <br/>
            <p>
            <a href="javascript:void(0)" onclick="resendActivationCode('<?=$_SERVER['HTTP_REFERER']?>');" class="button1"><span><?=t($t_base.'fields/resend');?></span></a>
            <a href="javascript:void(0)" class="button1" onclick="cancel_signin_login('<?=$_POST['loginArr']['szRedirectUrl']?>');"><span><?=t($t_base.'fields/close')?></span></a></p>
            <script type="text/javascript">
                update_header_text();
            </script>
        <?php } else { ?>
        <h5><?=t($t_base.'title/signin')?></h5>
        <div class="oh">
                <p class="fl-33"><?=t($t_base.'fields/signin_email')?></p>
                <p class="fl-65 pull-right" align="right"><input type="email" tabindex="101" id="szEmail" name="loginArr[szEmail]" value="<?=!empty($_COOKIE['__USER_EMAIL_COOKIE__']) ? $_COOKIE['__USER_EMAIL_COOKIE__']:$_POST['loginArr']['szEmail'];?>" style="width:95%;"/></p>
        </div> 
        <div class="oh">
                <p class="fl-33"><?=t($t_base.'fields/password')?></p>
                <p class="fl-65 pull-right" align="right"><input type="password" tabindex="102" style="width:95%;" id="szPassword" name="loginArr[szPassword]" tabindex="2" onkeyup="check_login_password_submit_key_press(event);"/></p>
        </div>
        <p align="right"><a href="javascript:void(0)" tabindex="103" class="f-size-12" tabindex="3" onclick="open_forgot_passward_popup('ajaxLogin','<?=$_POST['loginArr']['szRedirectUrl']?>');"><?=t($t_base.'links/forgot_pass')?></a></p>
        <br style="line-height:7px;" />
        <p align="right"><a href="javascript:void(0)" tabindex="104" class="f-size-14" tabindex="4" onclick="open_benefit_popup('ajaxLogin','<?=$_POST['loginArr']['szRedirectUrl']?>');"><?=t($t_base.'links/benefits_of_sign')?></a> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/benefits_of_signing_in_1');?>','<?=t($t_base.'messages/benefits_of_signing_in_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop')">&nbsp;</a></p>
        <p align="right"><a href="<?=__CREATE_ACCOUNT_PAGE_URL__?>" tabindex="105" class="f-size-14" tabindex="5"><?=t($t_base.'links/create_free_profile')?></a> <a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/create_profile_free_1');?>','<?=t($t_base.'messages/create_profile_free_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop')">&nbsp;</a></p>
        <br style="line-height: 16px;" />
        <p align="center"><a href="javascript:void(0)" tabindex="106" class="button1" onclick="cancel_signin('<?=$cancel_url?>','ajaxLogin');" ><span><?=t($t_base.'fields/cancel')?></span></a> <a href="javascript:void(0)" class="button1" onclick="userLogin();" tabindex="107"><span><?=t($t_base.'fields/sign_in')?></span></a></p>
        <?php } ?>
        <input type="hidden" name="loginArr[szRedirectUrl]"  value="<?=$_POST['loginArr']['szRedirectUrl']?>">
        <input type="hidden" name="loginArr[szCancelUrl]" value="<?=$_POST['loginArr']['szCancelUrl']?>">
        <input type="hidden" name="loginArr[szFromPage]" value="<?=$_POST['loginArr']['szFromPage']?>">
        <input type="hidden" name="loginArr[szBookingRandomKey]" value="<?=$_POST['loginArr']['szBookingRandomKey']?>">
    </form>
</div>
		