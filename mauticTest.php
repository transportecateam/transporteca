<?php 
/**
 * View Invoice
 */ 
ob_start();
session_start();
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/header_new.php");  
ini_set (max_execution_time,360000);

$kMautic = new cMautic();
//$_SESSION['oauth'] = array();
//unset($_SESSION['oauth']); 
//$kMautic->validateMauticApiAuthentication();
//print_R($_SESSION['oauth']); 

$mauticLeadApiAry = $kMautic->getMauticApiLogsData();



?> 

<h1 style="float:left;">Transporteca Mautic API Logs</h1><br><br>
<table class="format-1" width="100%" cellpadding="2" cellspacing="2">
    <tr>
        <th style="width: 10%;">Name</th>
        <th style="width: 10%;">Email</th>
        <th style="width: 35%;">Request Data</th>
        <th style="width: 35%;">Response Data</th> 
        <th style="width: 10%;">Added On</th> 
    </tr>
    <?php
        if(!empty($mauticLeadApiAry))
        {
            $ipAddressCount=0;
            $ctr = 0;
            foreach($mauticLeadApiAry as $mauticLeadApiArys)
            {
                $style = ""; 
                if(empty($mauticLeadApiArys['szResponseData']) || trim($mauticLeadApiArys['szResponseData'])=='Array ( )')
                {
                    $style = "style='background: #FF0000 none repeat scroll 0 0;color: #FFFFFF' "; 
                    $ipAddressCount++;
                }
                else
                {
                    $style = "style='background: #e5e5e7 none repeat scroll 0 0;color: #424242' ";
                }
                $ctr++;
                $mauticLeadApiArys['szResponseData'] = sanitize_all_html_input($mauticLeadApiArys['szResponseData']);
                ?>
                <tr <?php echo $style; ?>>
                    <td><?php echo $mauticLeadApiArys['szFirstName']." ".$mauticLeadApiArys['szLastName']; ?></td>
                    <td><?php echo $mauticLeadApiArys['szEmail']; ?></td>
                    <td>
                        <?php echo $mauticLeadApiArys['szRequestData']; ?>
                    </td>
                    <td>
                        <?php 
                            $div_id = "logs_div_".$mauticLeadApiArys['idMauticLogs'];
                            if(strlen($mauticLeadApiArys['szResponseData'])>200)
                            {
                                echo substr($mauticLeadApiArys['szResponseData'],0,197)."...";
                                ?>
                                <a href='javascript:void(0);' onclick="showHide('<?php echo $div_id; ?>')" style="color: blue">See more</a> 
                                <div id="<?php echo $div_id; ?>" style="display:none;"><?php echo showLogsPopup("Mautic Response Data",$mauticLeadApiArys['szResponseData'],$div_id); ?></div>
                                <?php
                            }
                            else
                            {
                                echo $mauticLeadApiArys['szResponseData']; 
                            } 
                        ?>
                    </td> 
                    <td><?php echo $mauticLeadApiArys['dtCreatedOn']; ?></td> 
                </tr>
                <?php
            }
        } 
    ?>
</table>
<?php  

function showLogsPopup($szTitle,$szContent,$div_id)
{
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:50%;"> 
            <h3><?php echo $szTitle;?></h3> <br>
            <div class="scroll-div" style="padding: 0px;height: 350px;" > 
                <p><?php echo $szContent; ?></p>
                <br/>  
            </div>
            <br>
            <p align="center"> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('<?php echo $div_id; ?>')"><span>Close</span></a>
            </p>
        </div> 
    </div>
    <?php
}
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
