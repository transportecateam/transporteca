<?php
ob_start();
session_start(); 
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) ); 
//$szGlobalLanguage = get_language_name();  
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes();
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );  

if(__ENVIRONMENT__ == "LIVE")
{
    /*
    * From now if we receive request on https://api.transporteca.com then we will redirect user to https://www.transporteca.com
    */
   ob_end_clean();
   header("location: https://transporteca.com");
   die;
} 

$display_abandon_message=false; 
if(!empty($_COOKIE['__USER_LOGIN_COOKIE__']) && (int)$_SESSION['user_id']<=0)
{  
    $kUserNew1 = new cUser();
    $kUserNew1->cookieUserLogin($_COOKIE['__USER_LOGIN_COOKIE__']);
} 
 
$szLanguage = sanitize_all_html_input($_REQUEST['lang']); 
if(empty($szLanguage))
{
    //$szLanguage = $_SESSION['transporteca_language_en'];
} 
if($page_404==1)
{
    $szLanguage = $_SESSION['transporteca_language_en']; 
}
 
/*
*  If any user searches from USA then we just redirect them to 404 page

$defaultCountryAry = array();
$defaultCountryAry = getCountryCodeByIPAddress();
if($defaultCountryAry['szCountryCode']=='US')
{ 
    header("HTTP/1.1 301 Moved Permanently");
    ob_end_clean();
    header("Location:".$redir_da_url);
    die;
}
 * 
 */
if(empty($szLanguage))
{
    $szLanguageCode=  str_replace("/","", substr($_SERVER['REQUEST_URI'], 0, 4));
    if($szLanguageCode!='')
    {
        $szLanguage=setSiteUrlsByLanguageCode($szLanguageCode);
    }
    else
    {
        $szLanguage = strtolower(__LANGUAGE_TEXT_ENGLISH__);
        if($szLanguage!=$_SESSION['transporteca_language_en'])
        {
            $_SESSION['HOLDININGPAGE']='';
            unset($_SESSION['HOLDININGPAGE']);
        }
        define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
        define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
        define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
        define_constants($szLanguage);
        $_SESSION['transporteca_language_en'] = $szLanguage; 
    }
}
else
{
    setSiteUrlsByLanguageName($szLanguage);
}
//echo $szLanguage."szLanguage";
$kConfig = new cConfig();
$currentLangArr=$kConfig->getLanguageDetails('','',false,$szLanguage,false,true,'',false);
if(!empty($currentLangArr))
{
    $szLang=$currentLangArr[0]['szLanguageHtmlCode'];
    $szLangFlag=$currentLangArr[0]['szName'];
    $szLangCodeFlag=$currentLangArr[0]['szLanguageCode'];
    $idCurrentLanguage=$currentLangArr[0]['id'];
}
else
{
    $szLang="en";
    $szLangFlag="english";
    $szLangCodeFlag=__LANGUAGE_ID_ENGLISH_CODE__;
    $idCurrentLanguage=__LANGUAGE_ID_ENGLISH__;
}
$showCookiePopForHoldingPage=true;
if($iHoldingPage==1)
{
    $showCookiePopForHoldingPage=false;
    $cookieValue="HoldingPage!@#";
    $_SESSION['HOLDININGPAGE']=$cookieValue;
    
    if($_SESSION['transporteca_language_en']=='danish')
    {
        $showCookiePopForHoldingPage=true;
    }
}
else
{
    $kExplain = new cExplain();
    $iHoldingPageValue=$kExplain->getLandingPageType($idCurrentLanguage);
    if((int)$iHoldingPageValue==1)
    {
        $showCookiePopForHoldingPage=false;
        $cookieValue="HoldingPage!@#";
        $_SESSION['HOLDININGPAGE']=$cookieValue;

        if($_SESSION['transporteca_language_en']=='danish')
        {
            $showCookiePopForHoldingPage=true;
        }
    }
}  
if($_SESSION['google_plus_data']['szEmail']!='' || $_SESSION['fb_data']['szEmail']!='')
{
    if($_SESSION['fb_data']['szEmail']!='')
    {
        $data['bookingFBGPlusAry']=$_SESSION['fb_data'];
        $data['bookingFBGPlusAry']['iFaceBookLogin']='1';
        unset($_SESSION['fb_data']);
        $_SESSION['fb_data']=array();        
        unset($_SESSION['google_plus_data']);
        $_SESSION['google_plus_data']=array();
    }
    else
    {
        $data['bookingFBGPlusAry']=$_SESSION['google_plus_data'];
        $data['bookingFBGPlusAry']['iGoogleLogin']='1';
        unset($_SESSION['google_plus_data']);
        $_SESSION['google_plus_data']=array();        
         unset($_SESSION['fb_data']);
        $_SESSION['fb_data']=array();
    }
    $szBookingRandomKey=trim($_REQUEST['booking_random_num']);
    $kBooking = new cBooking();
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomKey);
    //echo $idBooking."idBooking";
    if((int)$idBooking>0){
        
        if(!empty($_COOKIE['__USER_COUNTRY_NAME___']))
        {
            $szUserCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ;
            
            $kConfig = new cConfig();
            $kConfig->loadCountry(false,$szUserCountryName); 
            $idCountry = $kConfig->idCountry ;
            $data['bookingFBGPlusAry']['idShipperDialCode']=$idCountry;
        }

       $data['bookingFBGPlusAry']['szShipperEmail']=$data['bookingFBGPlusAry']['szEmail'];
       $data['bookingFBGPlusAry']['szShipperFirstName']=$data['bookingFBGPlusAry']['szFirstName'];
       $data['bookingFBGPlusAry']['szShipperLastName']=$data['bookingFBGPlusAry']['szLastName'];
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $szBookingRandomNum = $_REQUEST['bookingQuotesAry']['szBookingRandomNum'] ;
    $kRegisterShipCon = new cRegisterShipCon();
    $kConfig = new cConfig();
     
        if($kRegisterShipCon->addBookingQuotesContactDetails($data['bookingFBGPlusAry'],$idBooking,$mode,true))
        { 
            $kConfig->loadCountry($postSearchAry['idOriginCountry']);
            $szCountryStrUrl = $kConfig->szCountryISO ;
            $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
            $szCountryStrUrl .= $kConfig->szCountryISO ; 

            if($mode=='GET_RATES')
            { 
                $redirect_url = __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomKey."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            }
            else
            {
                $redirect_url = __BOOKING_QUOTE_THANK_YOU_PAGE_URL__."/".___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            } 
            ob_end_clean();
            header("Location:".$redirect_url);
            die; 
        }
    }
}  
$lang_code = __LANGUAGE__;
I18n::add_language(strtolower($lang_code));
 
$iLanguage = getLanguageId(); 
   
$szServerURI = trim($_SERVER['REQUEST_URI']);
$possibleHomePageAry = array('/','/da/','/da');
  
if(!empty($_COOKIE['__USER_COUNTRY_NAME___']))
{
    $szUserCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ;
}
else
{  
    $crawler = crawlerDetect($_SERVER['HTTP_USER_AGENT']);
    if(empty($crawler))
    {
        $userIpDetailsAry = array();
        $userIpDetailsAry = getCountryCodeByIPAddress();
        $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;
        $userIpDetailsStr = serialize($userIpDetailsAry);
        
        $visitorsRedirectLogs = array(); 
        if(!empty($userIpDetailsAry))
        { 
            $visitorsRedirectLogs['szReferer'] = $_SERVER['HTTP_REFERER'];
            $visitorsRedirectLogs['szIPAddress'] = $userIpDetailsAry['szIPAddress'];
            $visitorsRedirectLogs['idUser'] = $_SESSION['user_id'];
            $visitorsRedirectLogs['iFoudBYAPI'] = $userIpDetailsAry['iFoudBYAPI'];
            $visitorsRedirectLogs['szApiResponseData'] = print_R($userIpDetailsAry,true);
            $visitorsRedirectLogs['szCountryCode'] = $szUserCountryName;
        }  

        setCookie('__USER_COUNTRY_NAME___', $szUserCountryName ,time()+3600, '', $_SERVER['HTTP_HOST']);
        setCookie('__USER_IP_DETAILS___', $userIpDetailsStr ,time()+3600, '', $_SERVER['HTTP_HOST']);
    }
}  
if((int)$_SESSION['LANGUAGE_CHANGED_BY_USER']==0)
{   
    $bRedirctSiteFlag = false;
    $redirectCountryAry = array('GB','DK','DE','SE','NL','ES','PL','PT','FR','IT');
    if(in_array($szUserCountryName, $redirectCountryAry))
    {
        $bRedirctSiteFlag = true; 
        $redir_da_url = getUrlForLanguage($szUserCountryName); 
        $_SESSION['LANGUAGE_CHANGED_BY_USER'] = 1;
    } 
    if($bRedirctSiteFlag && !empty($redir_da_url) && __ENVIRONMENT__ == "LIVE")
    {
        if(!empty($visitorsRedirectLogs))
        {  
            $visitorsRedirectLogs['isRedirected'] = 1;
            $kUserNew1 = new cUser();
            $kUserNew1->addVisitorsRedirectLogs($visitorsRedirectLogs);
        }
        
        header("HTTP/1.1 301 Moved Permanently");
        ob_end_clean();
        header("Location:".$redir_da_url);
        die;
    } 
    else
    {
        if(!empty($visitorsRedirectLogs))
        {
            $visitorsRedirectLogs['isRedirected'] = 0;
            $kUserNew1 = new cUser();
            $kUserNew1->addVisitorsRedirectLogs($visitorsRedirectLogs);
        }
    }
}    
if(empty($szMetaTitle))
{
    $szMetaTitle = "Landing page ";
}  
$t_base = "home/";
$kUser= new  cUser();
if((int)$_SESSION['user_id']>0)
{
    $kUser->getUserDetails($_SESSION['user_id']);
    
    if($kUser->iActive!=1)
    { 
        ob_end_clean();
        header("Location:".__USER_LOGOUT_URL__);
        die;
    }
    
    $kUserNew = new cUser();
    $kUserNew->loadCurrency($kUser->szCurrency);

    if(($kUserNew->iBooking<=0) && ($_REQUEST['flag']=='cb')) // User is in booking steps and currency is not active.
    {
        //echo "<div id='Transporteca_popup'>";
        //echo displayCurrencyChangePopup($kUser->szCurrency);
        //echo "</div>";
    }
}  

$landing_page_url = __LANDING_PAGE_URL__ ;

$kBooking = new cBooking();
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

/**
  *	THIS BELOW SECTON IS FOR CONTENT -- Explain pages
  * 
  */ 
if( isset($_REQUEST['flag']) && ($_REQUEST['flag'] == 'ep') )
{
    if(isset($_REQUEST['menuflag']) && $_REQUEST['menuflag']=='ind')
    {
        //print_r($_REQUEST);
        //$_REQUEST['menuflag']  = str_replace('/','',$_REQUEST['menuflag']);
        $kExplain = new cExplain;

        $szLink=sanitize_all_html_input(trim($_REQUEST['szLink']));

        $levelExplainDataArr=$kExplain->selectExplainId($szLink,$mainTab);

        $szUrl = sanitize_all_html_input($_REQUEST['szUrl']);
        $content = $kExplain->getExplainContentByUrl($szUrl,$levelExplainDataArr['id']);
        if($content != array())
        {
            $idExplain = $content['id'];
            $szMetaTitle = $content[0]['szMetaTitle'];
            $szMetaKeywords = $content[0]['szMetaKeyWord'];
            $szMetaDescription = $content[0]['szMetaDescription'];
        }
    } 
}

/**
* SECTION ENDS HERE
*/
  
  /**
  * THIS BELOW SECTON IS FOR  -- Blog pages
  * 
  */
 
if( isset($_REQUEST['menuflag']) && ($_REQUEST['menuflag'] == 'blg') )
{
    if(isset($_REQUEST['flag']) && $_REQUEST['flag']!='')
    {
        $_REQUEST['flag']  = str_replace('/','',$_REQUEST['flag']);
        $kExplain = new cExplain;
        $link = $kExplain->selectBlogLink($_REQUEST['flag'],$iLanguage);
        if($link != array())
        {
            $szMetaTitle = ($link['szMetaTitle']?$link['szMetaTitle']:'');
            $szMetaKeywords = ($link['szMetaKeyWord']?$link['szMetaKeyWord']:'');
            $szMetaDescription = ($link['szMetaDescription']?$link['szMetaDescription']:'');	
        }
    }
    else
    {
        $kExplain = new cExplain;
        $link = $kExplain->selectBlogLink(false,$iLanguage);
        if($link != array())
        {
            //$szMetaTitle = ($link['szMetaTitle']?$link['szMetaTitle']:'');
            //$szMetaKeywords = ($link['szMetaKeyWord']?$link['szMetaKeyWord']:'');
            //$szMetaDescription = ($link['szMetaDescription']?$link['szMetaDescription']:'');
            $_REQUEST['flag'] = $link['szLink'];
            header('Location:'.__BLOG_PAGE_URL__.$_REQUEST['flag'].'/');
            die;
        }
    } 
}  
$confirm_key = $_REQUEST['myBookingkey'];
if(!empty($confirm_key))
{
    $keyAry = explode('__',$confirm_key);
    $szConfirmationBookingKey = $keyAry[0];	
}

if((int)($_SESSION['user_id']<=0) && ((int)$szConfirmationBookingKey>0))
{
    $_SESSION['customer_booking_confirmation_key'] = $szConfirmationBookingKey;
}

/*
 * Getting management var for social media link
 */  
$kWhsSearch = new cWHSSearch();
$kExplain = new cExplain;
$kSEO = new cSEO();
$iLanguage = getLanguageId();
//echo "lang ".$iLanguage ;

$leftmenuLinksAry = array();
$howDoesworksLinks = array();
$companyLinksAry = array();
$tranportpediaLinksAry = array(); 
$leftmenuLinksAry = $kExplain->selectPageDetailsUserEndByType($iLanguage); 

$howDoesworksLinks = $leftmenuLinksAry[__HOW_IT_WORKS__];
$companyLinksAry = $leftmenuLinksAry[__COMPANY__];
$tranportpediaLinksAry = $leftmenuLinksAry[__TRANSTPORTPEDIA__]; 

$seoPageHeaderAry = array();
$seoPageHeaderAry = $kSEO->getAllPublishedSeoPages($iLanguage,true);

$blogPageHeaderAry = array();
$blogPageHeaderAry = $kExplain->selectBlogDetailsActive($iLanguage);
   
if(__LANGUAGE__==__LANGUAGE_TEXT_SWEDISH__)
{ 
    //$szLang = 'SE';
    $szOgLang = 'en_SE';
}
else if(__LANGUAGE__==__LANGUAGE_TEXT_NORWEGIAN__)
{ 
    //$szLang = 'NO';
    $szOgLang = 'en_NO';
}
else if($iLanguage==__LANGUAGE_ID_DANISH__)
{
    //$szLang = 'da';
    $szOgLang = 'da_DA';
}
else
{
    //$szLang = 'en';
    $szOgLang = 'en_GB';
}  
$kWhsSearch = new cWHSSearch();  
$arrDescriptions = array("'__FACEBOOK_URL__'","'__TWITTER_URL__'","'__LINKEDIN_URL__'","'__YOUTUBE_URL__'","'__GOOGLE_PLUS_URL__'","'__GOOGLE_MAP_V3_API_KEY__'","'__TRANSPORTECA_CUSTOMER_CARE_DANISH__'","'__TRANSPORTECA_CUSTOMER_CARE__'","'__TRANSPORTECA_CUSTOMER_CARE__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      
 
$szFacebookUrl = $bulkManagemenrVarAry['__FACEBOOK_URL__'];
$szTwitterUrl = $bulkManagemenrVarAry['__TWITTER_URL__'];
$szLinkedinUrl = $bulkManagemenrVarAry['__LINKEDIN_URL__'];
$szYoutubeUrl = $bulkManagemenrVarAry['__YOUTUBE_URL__'];
$szGooglePlusAuthor = $bulkManagemenrVarAry['__GOOGLE_PLUS_URL__'];
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];
   
$iLanguage = getLanguageId();
$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage);

$szFacebookUrl= addhttp($szFacebookUrl);
$szTwitterUrl= addhttp($szTwitterUrl);
$szLinkedinUrl= addhttp($szLinkedinUrl);
$szYoutubeUrl= addhttp($szYoutubeUrl); 

$kConfig = new cConfig();
$smallCountryAry = array();
$smallCountryAry = $kConfig->getAllCountryForMaxmindScript(true);
$szPageURI = __HOME_PAGE_BASE_URL__."".$_SERVER['REQUEST_URI'];

$currentLangArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);

if($sitemapFlag==1)
{
    $szMetaTitle = $szMetaTitle." - ".$currentLangArr[0]['szName'];
} 
?> 
<!doctype html>
<html lang="<?php echo $szLang; ?>">
    <head>
        <script type="text/javascript"> 
            if (document.location.protocol == 'https:')
            {
                var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
            }
            else
            {
                var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
            }
            var __JS_ONLY_VOGA_CATEGORY_LISTING__ = {};
            var __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__ = {};
            var __BOOKING_INSURANCE_SELL_RATE_ARY__ = new Array();
            var __BOOKING_INSURANCE_BUY_RATE_ARY__ = new Array();
            var __BOOKING_INSURANCE_COMMON_ARY__ = new Array();
            
            var __GLOBAL_SMALL_COUNTRY_LIST__ = new Array(); 
            var googleApiCall = [];
            <?php 
                if(!empty($smallCountryAry))
                {
                    foreach($smallCountryAry as $countryIso=>$smallCountryArys)
                    {
                        ?>
                            __GLOBAL_SMALL_COUNTRY_LIST__.push('<?php echo $countryIso; ?>');
                        <?php
                    }
                }
            ?> 
        </script>	 
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title id="metatitle"><?php if(!empty($szMetaTitle)){ echo $szMetaTitle; }?></title>

        <?php if(!empty($szMetaDescription)){?>
             <meta name="description" content="<?=$szMetaDescription;?>" />
        <?php } ?>    
             
        <?php  if(!empty($_GET['search_params']) && $iFromLandingPage==1){ ?>		
            <link rel="canonical" href="<?php echo __BASE_URL__; ?>" />
        <?php  } ?> 

        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta property="og:locale" content="<?php echo $szOgLang; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Transporteca" />

        <meta property="og:title" content="<?php echo $szMetaTitle; ?>" />
        <meta property="og:description" content="<?php echo $szMetaDescription; ?>" /> 
         
	   <?php  if($_REQUEST['menuflag']=='agg'){ ?>		
                    <link rel="publisher" href="<?php echo $szOpenGraphAuthor; ?>"/> 
                    <meta property="og:url" content="<?php echo $szAggregatePageUrl; ?>" /> 
                    <meta property="og:image" content="<?php echo $szOpenGraphImagePath; ?>" />
		<?php  } else { ?> 
	   		<link rel="publisher" href="<?php echo $szGooglePlusAuthor; ?>"/>
	   	<?php } ?>
	   	
	   	<?php if(!empty($szMetaKeywords) && $_REQUEST['menuflag']!='agg'){ ?>
	   		<meta name="keywords" content="<?=$szMetaKeywords;?>" />
	   	<?php } ?> 
	    	<?php
	    	if(__ENVIRONMENT__ == "LIVE")
	    	{
                    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
                    $protocol = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
                    if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.' || strtolower($protocol)=='http://') 
                    {
                        if(substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.')
                        {
                            $www_str = "www.";
                        }

//                        header("HTTP/1.1 301 Moved Permanently");
//                        header('Location: https://'.$www_str.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']);
//                        exit;
                    }
	    	}
                else if(__ENVIRONMENT__ == "DEV_LIVE")
                {
                    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
                    $allowed_ip_ary = array('122.160.151.237','90.184.235.58','127.0.0.1');
                    $server_ip_addre = trim($_SERVER['REMOTE_ADDR']);
                    if(!empty($allowed_ip_ary) && !in_array($server_ip_addre,$allowed_ip_ary))
                    {
                        //header("Location:".__BASE_URL__.'/holding');
                        //die;
                    }
                }
	    	else
	    	{
                    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
	    	}  
	    	if(__ENVIRONMENT__ == "LIVE")
	    	{
	    ?>  
                    <!-- CSS FILES  --> 
                    <!--<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>-->
                    <!--<link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/font-awesome.min.css' rel='stylesheet' type='text/css'>-->
                    <link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/transporteca.min.css?v=<?php echo time(); ?>" rel="stylesheet" />  
                    <link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/common-header.css?v=<?php echo time(); ?>" rel="stylesheet" />
                      
                    <!-- Loading CSS FILES --> 
                    <script>var cb_1=function(){var e=document.createElement("link");e.rel="stylesheet",e.href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300";var t=document.getElementsByTagName("head")[0];t.parentNode.insertBefore(e,t)},raf=requestAnimationFrame||mozRequestAnimationFrame||webkitRequestAnimationFrame||msRequestAnimationFrame;raf?raf(cb_1):window.addEventListener("load",cb_1);</script>
                     
                    <!-- JS FILES  	
                    <script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.number.js'></script>
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js'></script> 
                    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
                    <script src="<?php echo __BASE_STORE_SECURE_JS_URL__?>/jquery.bxslider.min.js"></script> 
                    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
                    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
                    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>  
					<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
                    <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
                    <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/modernizr.custom.79639.js"></script> 
                    <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js"></script> 
                    -->
                    <!-- Loading CSS FILES 
                    <script>var cb_1=function(){var e=document.createElement("link");e.rel="stylesheet",e.href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300";var t=document.getElementsByTagName("head")[0];t.parentNode.insertBefore(e,t)},raf=requestAnimationFrame||mozRequestAnimationFrame||webkitRequestAnimationFrame||msRequestAnimationFrame;raf?raf(cb_1):window.addEventListener("load",cb_1);var cb=function(){var e=document.createElement("link");e.rel="stylesheet",e.href='<?php echo __BASE_STORE_SECURE_CSS_URL__."/transporteca.min.css"; ?>';var t=document.getElementsByTagName("head")[0];t.parentNode.insertBefore(e,t)},raf=requestAnimationFrame||mozRequestAnimationFrame||webkitRequestAnimationFrame||msRequestAnimationFrame;raf?raf(cb):window.addEventListener("load",cb);</script>
                     --> 
                    <!-- Loading JS FILES  --> 
                    <script src="<?php echo __BASE_STORE_JS_URL__;?>/transporteca.min.js?t=<?php echo time(); ?>" type="text/javascript"></script>  
                    <?php if($iFromLandingPage==1){ ?>
                        <script type="text/javascript" defer="1">
                            !function(){var e=document.createElement("script");e.async=!0,e.type="text/javascript",e.src='<?php echo __BASE_STORE_JS_URL__."/transporteca-internal.min.js";?>';var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}();
                            !function(){var e=document.createElement("script");e.async=!0,e.type="text/javascript",e.src='<?php echo __BASE_STORE_JS_URL__."/fancybox/fancy-box-min.js";?>';var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}();
                        </script>
                    <?php } else {?>
                        <script src="<?php echo __BASE_STORE_JS_URL__;?>/transporteca-internal.min.js" type="text/javascript"></script>  
                        <script src='<?php echo __BASE_STORE_JS_URL__."/fancybox/fancy-box-min.js"; ?>' type="text/javascript"></script>  
                    <?php } ?> 
                    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js?t=<?php echo time(); ?>"></script>
                    <!--<script src="//cdn.optimizely.com/js/3407690608.js"></script>-->
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js'></script>
                    
		<?php } else { ?>
                    
                    <!-- CSS FILES  --> 
                    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
                    <link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/jquery.bxslider.css" rel="stylesheet" />
                    <!--<link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/bootstrap.min.css' rel='stylesheet' type='text/css'>-->
                    <link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/font-awesome.min.css' rel='stylesheet' type='text/css'>

                    <?php if($iMyAccountPage==1){ ?>
                        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/style.css" />
                    <?php } ?>
                    <link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/custom-style.css?v=<?php echo time(); ?>" /> 
                    <link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/flags.css?v=<?php echo time(); ?>" /> 
                    <link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/new-layout.css?v=<?php echo time(); ?>" />   
                    
                    <link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/common-header.css?v=<?php echo time(); ?>" rel="stylesheet" />
                    <link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
                    <link type="text/css" rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" /> 

                    <!-- JS FILES  -->	
                    <script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.number.js'></script>
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js?v=<?php echo time(); ?>'></script> 
                    <!-- JS FILES  -->	 
                    <!-- 
                    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/transporteca.min.js"></script>
                    COMBINED ALL .JS INTO ONE FILE.
                    -->
                   
                    <script src="<?php echo __BASE_STORE_SECURE_JS_URL__?>/jquery.bxslider.min.js"></script>
                    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js?v=<?php echo time(); ?>"></script> 
                    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
                    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
                   
                    <!--<script src="//cdn.optimizely.com/js/3415020380.js"></script>-->
                   <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>  			
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/modernizr.custom.79639.js"></script> 
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js"></script>   
                   <script src="<?=__BASE_STORE_SECURE_JS_URL__?>/geoCode.js"></script>
<!--                   <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/custom.js'></script> -->
                   <!--<script src="//cdn.optimizely.com/js/3415020380.js"></script>-->
                   
		<?php } ?>	
                   <!--<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.flagstrap.js'></script>-->
                   
		   <link rel="icon" id="favicon_icon_1" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />  
		   <link rel="shortcut icon" id="favicon_icon_2" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />
		   <?php
                    if(__ENVIRONMENT__ == "LIVE")
                    {
                        
                        if(strtolower($_SERVER['REQUEST_URI'])=='/flytning')
                        {
                        ?> 
                        
                            <style>.async-hide { opacity: 0 !important} </style>
                            <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
                            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
                            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
                            })(window,document.documentElement,'async-hide','dataLayer',4000,
                            {'GTM-M6JG3BG':true});</script>

                            <script>
                              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                              ga('create', 'UA-33742200-1', 'auto');
                              ga('require', 'GTM-M6JG3BG');
                              ga('send', 'pageview');
                            </script>
                        <?php }?>
                        <!--<style>.async-hide 
                        { opacity: 0 !important} 
                        </style>
                        <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.end=i=function()
                        { s.className=s.className.replace(RegExp(' ?'+y),'')} 
                        ;(a[n]=a[n]||[]).hide=h;
                        setTimeout(function()
                        {i();h.end=null} 
                        ,c);})(window,document.documentElement,
                        'async-hide','dataLayer',2000,
                        {'GTM-T4BXRGW':true,} 
                        );</script>-->

                        <!-- Facebook Pixel Code -->
                        <script>
                       !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                        document,'script','https://connect.facebook.net/en_US/fbevents.js');



                        fbq('init', '265908583791085');
                        fbq('track', "PageView");
                        
                        // ViewContent
                        // Track key page views (ex: product page, landing page or article)
                        fbq('track', 'ViewContent');

                        <?php
                        if($_SERVER['PHP_SELF']=='/newServices.php' || $_SERVER['PHP_SELF']=='/get-rates3.php' || $_SERVER['PHP_SELF']=='/get-quotes.php')
                        {
                        ?>
                            // Search
                            // Track searches on your website (ex. product searches)
                            fbq('track', 'Search');
                        <?php }
                        else if($_SERVER['PHP_SELF']=='/newServices.php' || $_SERVER['PHP_SELF']=='/serviceQuotes.php')
                        {
                        ?>
                            // Other
                            // Track when users see a price for the requested service
                            fbq('track', 'Other');

                         <?php
                        }
                        else if($_SERVER['PHP_SELF']=='/booking-confirmation-new.php' || $_SERVER['PHP_SELF']=='/booking_quotes_confirmation.php')
                        {?>
                            // InitiateCheckout
                            // Track when people enter the checkout flow (ex. click/landing page on checkout button)
                            fbq('track', 'InitiateCheckout');

                         <?php   
                        }else if($_SERVER['PHP_SELF']=='/booking-reciept-new.php')
                        {?>
                            // Purchase
                            // Track purchases or checkout flow completions (ex. landing on "Thank You" or confirmation page)
                            fbq('track', 'Purchase', {value: '<?php echo round((float)$fTotalPriceUSDForFBPixel,2);?>', currency: 'USD'});


                         <?php   
                        }
                        ?>
                        </script>
                        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=265908583791085&ev=PageView&noscript=1"/></noscript>

                    <?php }?> 
                    <?php if(__ENVIRONMENT__ =='DEV_LIVE' || __ENVIRONMENT__ =='LIVE') { 	?>
		   <script type="text/javascript"> 
                        var _gaq = _gaq || [];
                        <?php 
                        if(__ENVIRONMENT__ =='LIVE'){?>
                        _gaq.push(['_setAccount', 'UA-33742200-1']);
                        <?php
                        }else
                        {?>
                        _gaq.push(['_setAccount', 'UA-92073326-1']);
                           <?php 
                        }
                        if($_SERVER['PHP_SELF']=='/services.php')
                        {
                        ?>
                            _gaq.push(['_trackPageview', '/services/']);
                        <?php
                        }
                        else if ($_SERVER['PHP_SELF']=='/booking-confirmation-new.php')
                        {
                            ?>
                            _gaq.push(['_trackPageview', '/overview/']);
                            <?php
                        }
                        else if ($_SERVER['PHP_SELF']=='/booking-reciept-new.php')
                        {
                            ?>
                            _gaq.push(['_trackPageview', '/thank-you/']);
                            <?php
                        }
                        else if ($_SERVER['PHP_SELF']=='/searchServices.php')
                        {
                            ?>
                            _gaq.push(['_trackPageview', '/search_services/']);
                            <?php
                        }
                        else
                        {
                            ?>
                            _gaq.push(['_trackPageview']);	
                            /*
                            ga('send', 'pageview'); 
                            ga('set', {
                                page: '<?php echo $szPageURI; ?>',
                                title: '<?php echo $szMetaTitle; ?>'
                            }); */
                            <?php
                        }
                    ?> 	 
                    (function() {
                      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                      //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                      ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                    })();
	</script>
	
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WV4RB6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WV4RB6');</script>
	<!-- End Google Tag Manager -->
		
	<?php }	
        $isMobileValue= checkMobileDevice();
        
        $langArr=$kConfig->getLanguageNameMapped(__LANGUAGE__); 
	?>
        <script>
        (function(){
            document.addEventListener("DOMContentLoaded", function(event) {
            try {
            var hash = document.location.hash;

            if (hash.length) {
            var params = hash.split('#').splice(1);
            var fieldIds =
            { from: 'szOriginCountryStr', to: 'szDestinationCountryStr', date: 'datepicker1_search_services', volume: 'iVolume', weight: 'iWeight' }

            for (var i in params) {
            var param = params[i];
            var [id, value] = param.split('|');

            if (id in fieldIds)
            { var fieldId = fieldIds[id]; document.getElementById(fieldId).value = value; }

            }

            history.pushState("", document.title, window.location.pathname);
            }
            } catch (e)
            { console.log('Could not fill in form!'); }

            });
            })();
            
            console.log('Country: '+'<?php echo $szUserCountryName; ?>');
</script>
</head> 
<body <?php if((int)$iDonotIncludeHiddenSearchHeader==1 && $isMobileValue==1){ echo "class='new-searvice-page-main-container get-rate-header-mobile'"; } else { if($iNewServicePage==1 || $iEmailVerificationPage==1){ echo "class='new-searvice-page-main-container'"; }} ?><?php if($isMobileValue!=1){?> onclick="showHideCookieDiv();" <?php }?>> 
<div id="Transporteca_popup"></div>
<div id="signin_help_pop" class="help-pop"></div>
<div id="leave_page_div" style="display:none;">	</div>
<div id="ajaxLogin"></div>
<div id="contactPopup"></div> 
    <header class="header clearfix">
        <?php echo display_transporteca_menu();?>
		<div class="logo"><a <?php if((int)$idBooking>0 && ($display_abandon_message)) {?>href="javascript:void(0);" onclick="display_abandon_popup();" <?php }else {?>href="<?=__BASE_URL_SECURE__?>"<? }?>><?=t($t_base.'header/transporteca');?></a></div>
		
		<?php  
                    $szMyAccountUrl_onclick = '';
                    if((int)$_SESSION['user_id']>0)
                    { 
                        if($kUser->iConfirmed==1)
                        {
                            if($kUser->iFirstTimePassword==1)
                            { 
                                $szMyAccountUrl = "javascript:void(0);" ;
                                $szMyAccountUrl_onclick="onclick='display_first_time_password_popup(2)'";
                            }
                            else
                            { 
                                //check if there is cookie for password is set or not
                                if(empty($_COOKIE['__USER_PASSWORD_COOKIE__']) || ($_COOKIE['__USER_PASSWORD_COOKIE__']!=$kUser->szUserPasswordKey))
                                {
                                    $szMyAccountUrl = "javascript:void(0);" ;
                                    $szMyAccountUrl_onclick="onclick='display_check_for_password_popup(2)'";
                                }
                                else
                                { 
                                    $szMyAccountUrl = __BASE_URL__."/myAccount" ;
                                }
                            } 
                        }
                        else
                        {
                            $szMyAccountUrl = __BASE_URL__."/verify-email" ;
                        }  
		?>
		<div class="login" onmouseover='$("#show_main_link").attr("style","display:block;");' onmouseout='$("#show_main_link").attr("style","display:none;");'>
			<a href="<?php echo $szMyAccountUrl; ?>" <?php echo $szMyAccountUrl_onclick; ?>>
				<?php if($kUser->iIncompleteProfile == 1){
                                        if(!empty($kUser->szFirstName)){?>
                                           <p id="userInfo"><?=$kUser->szFirstName?></p>
                                        <?php } else {?>
						<p id="userInfo"><?=$kUser->szEmail?></p>
				<?php } } else {?>					
					<p id="userInfo"><?=$kUser->szFirstName?></p>
				<?php } ?>
			</a>
			<div id="show_main_link" style="display:none;">
				<?php 
				/*
				 * If user profile is incomplete and is in booking process then before sending myaccount we give confirmation then redirect. 
				 * 
				 * */
				$booking_key = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
				?>
				<p class="logout_account"><a href="<?=__USER_LOGOUT_URL__?>">Logout</a></p>
				<p class="user_account"><a href="<?=$szMyAccountUrl?>" <?php echo $szMyAccountUrl_onclick; ?>><?=t($t_base.'header/myAccount');?></a></p> 
			</div>
		</div>
		<?php } else { ?>
		<div class="login">
			<a href="javascript:void(0)" onclick="userLoginPopUp('loginForm_header');" id="login"><?=t($t_base.'header/log_in');?></a>
			<form id="loginForm_header" name="loginForm" method="post" action="">						
				<input type="hidden" name="loginArr[szRedirectUrl]" id="szHeaderRedirectUrl" value="<?=__MAIN_SITE_HOME_PAGE_URL__.$_SERVER['REQUEST_URI']?>" >
				<input type="hidden" name="loginArr[iDoNotDisplayErrorMessage]" id="1" value="1" >
			</form>	
		</div>
		<?php }
                $currentLangArr=$kConfig->getLanguageDetails('',$iLanguage,'','','',true,'',false);
                ?>
		<div class="country_logo" >
                    <a href="javascript:void(0)" onclick="showHideCountryDropDown();"  class="<?php echo strtolower($szLangFlag);?>-flag default-lang" alt="<?php echo ucwords($szLangFlag);?>" title="<?php echo ucwords($szLangFlag);?>">&nbsp;</a>
                    <div class="flag-dropdown" onmouseout="$('.flag-dropdown').attr('style','display:none;');" onmouseover="$('.flag-dropdown').attr('style','display:block;');">
                    <ul>
                    <?php
                        
                     
                $langArr=$kConfig->getLanguageNameMapped($iLanguage); 
                
                    $langAllArr=$kConfig->getLanguageDetails('','','','','',true,'',false);
                         if(!empty($langAllArr))
                        {
                            foreach($langAllArr as $langAllArrs)
                            {
                                if($langAllArrs['iDoNotShow']==1)
                                {    
                                    if($langAllArrs['szLanguageCode']!=$szLangCodeFlag)
                                    {
                                        if($iFromLandingPage==1)
                                        {
                                            if($langAllArrs['szLanguageCode']==__LANGUAGE_ID_ENGLISH_CODE__)
                                            {
                                                
                                                if($iPageSearchType=='4')
                                                {
                                                    $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$replaceVogaPagesAry[$langAllArrs['szLanguageCode']];
                                                }else{
                                                $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__;
                                                }
                                            }
                                            else
                                            {
                                                
                                                if($iPageSearchType=='4')
                                                {
                                                    $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$replaceVogaPagesAry[$langAllArrs['szLanguageCode']];
                                                }else{
                                                    $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$langAllArrs['szLanguageCode']; 
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $szLanguageRedirectUrl = getFooterLanguageUrlNew($_SERVER['REQUEST_URI'],$langAllArrs['szLanguageCode'],$currentLangArr[0]['szLanguageCode']);
                                        }?>
                                            <li><a href="<?php echo $szLanguageRedirectUrl; ?>"  class="<?php echo strtolower($langAllArrs['szName']);?>-flag" alt="<?php echo ucwords($langAllArrs['szName']);?>" title="<?php echo ucwords($langAllArrs['szName']);?>">&nbsp;</a> </li>
                                      <?php  
                                    }
                                }
                            }
                        }
//                        if(__LANGUAGE__==__LANGUAGE_TEXT_ENGLISH__)
//                        {   
//                            if($iFromLandingPage==1)
//                            {
//                                $szLanguageRedirDanishUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da"; 
//                            }
//                            else
//                            {
//                                $szLanguageRedirDanishUrl = getFooterLanguageUrlNew($_SERVER['REQUEST_URI'],__LANGUAGE_ID_DANISH__,$szLinkedPageUrl); 
//                            }
//                            ?>
                            <!--<a href="<?php echo $szLanguageRedirDanishUrl; ?>" alt="Dansk" title="Dansk" class="danish-flag">&nbsp;</a>--> 
                            <?php
//                        }
//                        else
//                        { 
//                            if($iFromLandingPage==1)
//                            {
//                                $szLanguageRedirEnglishUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__; 
//                            }
//                            else
//                            {
//                                $szLanguageRedirEnglishUrl = getFooterLanguageUrl($_SERVER['REQUEST_URI'],__LANGUAGE_ID_ENGLISH__,$szLinkedPageUrl); 
//                            }
//                            ?> 
                            <!--<a href="<?php echo $szLanguageRedirEnglishUrl; ?>"  class="english-flag" alt="Engelsk" title="Engelsk">&nbsp;</a>--> 
                            <?php
//                        }
                    ?> 
                       </ul>  
                        </div>
                </div>
                <?php if($_SESSION['HOLDININGPAGE']==''  && $iHoldingPage==0) {?>
		<div class="telephone">
		<?php 
                    if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
                    { 
                        echo $szCustomerCareNumer ;  
                    } 
                    else 
                    { 
                        echo $szCustomerCareNumer ; 
                    }
		?>
		</div> 
                <div class="customer-services <?php if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__){?>cookie-icon-da<?php } ?>" id="customer-services-link"><a href="javascript:$zopim.livechat.window.toggle();" onclick="close_chat_bubble();"><?=t($t_base.'header/customer_services');?></a> 
                
                <?php
                if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
                {?>
                    <a href="<?php echo __COOKIE_DANISH_URL__;?>" class="cookie-icon-da-link"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/cookieicon.png";?>"></a>
                 <?php   
                }?>
                </div>
                <?php }?>
</header>
<?php  
if((int)$iDonotIncludeHiddenSearchHeader==0){  
    if(trim($style)=='')
    {
        $style="style='display:none;'";
    }
?>
<header id="header-search" class="header sub clearfix" <?php echo $style;?>>
    <nav><a href="javascript:void(0);" class="menu"></a></nav>
    <div class="logo"><a <?php if((int)$idBooking>0 && ($display_abandon_message)) {?>href="javascript:void(0);" onclick="display_abandon_popup();" <?php }else {?>href="<?=__BASE_URL_SECURE__?>"<?php }?>><?=t($t_base.'header/transporteca');?></a></div>	
    <?php if($_SESSION['HOLDININGPAGE']=='' && $iHoldingPage==0){?>
    <div id="transporteca_hidden_search_form_container"> 
        <?php 
            if($iNewServicePageFlag==1)
            {
                if(!empty($_REQUEST['booking_random_num']))
                {
                    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
                }
                $kBooking = new cBooking();
                $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum); 
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking); 
                
                echo display_new_search_form($kBooking,$postSearchAry,true,true);
            }
            else
            {  
                $idSeoPage = false;
                $_SESSION['seo_page_id'] = 0;
                if($_REQUEST['seo_page_flag']==1)
                { 
                    $idSeoPage = $seoLinkAry['id'] ;
                    $_SESSION['seo_page_id'] = $idSeoPage ;
                }				
                echo display_new_search_form($kBooking,$postSearchAry,true,false,false,$szDefaultFromField,$idSeoPage,true,$idDefaultLandingPage);
            } 
        ?>
    </div>	
    <?php }?>
    <?php if((int)$_SESSION['user_id']>0){?>
    <div class="login" onmouseover='$("#show_link").attr("style","display:block;");' onmouseout='$("#show_link").attr("style","display:none;");'>
        <a href="<?php echo $szMyAccountUrl; ?>" <?php echo $szMyAccountUrl_onclick; ?>>
            <?php if($kUser->iIncompleteProfile == 1){
                if(!empty($kUser->szFirstName)){?>
                    <p id="userInfo"><?=$kUser->szFirstName?></p>
                <?php } else {?>
                    <p id="userInfo"><?=$kUser->szEmail?></p>
            <?php } } else {?>					
                    <p id="userInfo"><?=$kUser->szFirstName?></p>
            <?php } ?>
        </a>
        <div id="show_link" style="display:none;">
            <?php 
            /*
             * If user profile is incomplete and is in booking process then before sending myaccount we give confirmation then redirect. 
             * 
             * */
            $booking_key = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
            if(($kUser->iIncompleteProfile == 1) && (!empty($booking_key))){ ?>
                    <p class="logout_account"><a href="javascript:void(0);" onclick="display_abandon_popup('<?=__USER_LOGOUT_URL__?>')"><img src="<?=__BASE_STORE_IMAGE_URL__?>/logout-icon.png" alt="Logout" title="Logout" border="0" /></a></p>
                    <p class="user_account"><a href="javascript:void(0);" onclick="display_abandon_popup('<?=__USER_MY_ACCOUNT_URL__?>')"><?=t($t_base.'header/myAccount');?></a></p>
            <?php } else {?>
                    <p class="logout_account"><a href="<?=__USER_LOGOUT_URL__?>">Logout</a></p>
                    <p class="user_account"><a href="<?php echo $szMyAccountUrl; ?>" <?php echo $szMyAccountUrl_onclick; ?>><?=t($t_base.'header/myAccount');?></a></p>
            <?php }?>
        </div>
    </div>
    <?php }else{?>
        <div class="login">
            <a href="javascript:void(0)" onclick="userLoginPopUp('loginForm_header');" id="login"><?=t($t_base.'header/log_in');?></a>
            <form id="loginForm_header" name="loginForm" method="post" action="">						
                <input type="hidden" name="loginArr[szRedirectUrl]" id="szHeaderRedirectUrl" value="<?=__MAIN_SITE_HOME_PAGE_URL__.$_SERVER['REQUEST_URI']?>" >
                <input type="hidden" name="loginArr[iDoNotDisplayErrorMessage]" id="1" value="1" >
            </form>	
        </div>
    <?php }?>
    <div class="country_logo" >
                    <a href="javascript:void(0)" onclick="showHideCountryDropDown();"  class="<?php echo strtolower($szLangFlag);?>-flag default-lang" alt="<?php echo ucwords($szLangFlag);?>" title="<?php echo ucwords($szLangFlag);?>">&nbsp;</a>
                    <div class="flag-dropdown" onmouseout="$('.flag-dropdown').attr('style','display:none;');" onmouseover="$('.flag-dropdown').attr('style','display:block;');">
                    <ul>
                    <?php
                        
                     
                $langArr=$kConfig->getLanguageNameMapped($iLanguage); 
                
                    $langAllArr=$kConfig->getLanguageDetails('','','','','',true,'',false);
                         if(!empty($langAllArr))
                        {
                            foreach($langAllArr as $langAllArrs)
                            {
                                if($langAllArrs['iDoNotShow']==1)
                                {    
                                    if($langAllArrs['szLanguageCode']!=$szLangCodeFlag)
                                    {
                                        if($iFromLandingPage==1)
                                        {
                                            if($langAllArrs['szLanguageCode']==__LANGUAGE_ID_ENGLISH_CODE__)
                                            {
                                                
                                                if($iPageSearchType=='4')
                                                {
                                                    $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$replaceVogaPagesAry[$langAllArrs['szLanguageCode']];
                                                }else{
                                                $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__;
                                                }
                                            }
                                            else
                                            {
                                                
                                                if($iPageSearchType=='4')
                                                {
                                                    $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$replaceVogaPagesAry[$langAllArrs['szLanguageCode']];
                                                }else{
                                                    $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$langAllArrs['szLanguageCode']; 
                                                }
                                            }
                                        }
                                        else
                                        {
                                            $szLanguageRedirectUrl = getFooterLanguageUrlNew($_SERVER['REQUEST_URI'],$langAllArrs['szLanguageCode'],$currentLangArr[0]['szLanguageCode']);
                                        }?>
                                            <li><a href="<?php echo $szLanguageRedirectUrl; ?>"  class="<?php echo strtolower($langAllArrs['szName']);?>-flag" alt="<?php echo ucwords($langAllArrs['szName']);?>" title="<?php echo ucwords($langAllArrs['szName']);?>">&nbsp;</a> </li>
                                      <?php  
                                    }
                                }
                            }
                        }
                    ?> 
                       </ul>  
                        </div>
                </div>
    <?php if($_SESSION['HOLDININGPAGE']==''  && $iHoldingPage==0) {?>
    <div class="telephone">
        <?php echo $szCustomerCareNumer; ?>
    </div>
    
    <a href="javascript:void(0);" class="search-icon" onclick="openSearchPopup();">&nbsp;</a>
    <input type="hidden" id="openSearchPopUp" value="">
    <div id="searchFormPop"></div>
    <?php }?>
</header> 
<?php }  
$szLastSeoPageURL = str_replace("/","",$_REQUEST['flag']);  
//this is for seopage aquision
if(!empty($_SESSION['seo_aquisition']['szReferalUrl']) && !empty($_SESSION['seo_aquisition']['szUrl']) && ($_SESSION['seo_aquisition']['szUrl']!=$szLastSeoPageURL))
{
    $kSEO = new cSEO();
    $szURL = $_SESSION['seo_aquisition']['szUrl'] ;

    $seoAquisitionLinkAry = array();
    $seoAquisitionLinkAry = $kSEO->getSeoPageDataByUrl($szURL);

    $seoTrackerAry = array();
    $seoTrackerAry['szReferalUrl'] = $_SESSION['seo_aquisition']['szReferalUrl'] ;
    $seoTrackerAry['szSeoUrl'] = $szURL;
    $seoTrackerAry['idSeoPage'] = $seoAquisitionLinkAry['id'];

    $kSEO->addSEOTracker($seoTrackerAry);

    unset($_SESSION['seo_aquisition']['szReferalUrl']);
    unset($_SESSION['seo_aquisition']['szUrl']);
} 

?>
<script type="text/javascript">
$().ready(function(){ 
    <?php if((int)$headerSearchFlag==0){ ?>
        $.fn.scrollBottom=function(){return $(document).height()-this.scrollTop()-this.height()};var $el=$("#header-search"),$window=$(window);$window.bind("scroll resize",function(){var o=($window.height()-$el.height()-10,870-$window.scrollBottom(),$window.scrollTop());437>o?($el.css({top:870-o+"px",bottom:"auto",display:"none"}),$("#iHiddenChanged").attr("value","2")):($el.css({top:0,bottom:"auto",display:"block"}),fill_data_to_hidden_form())}); 
    <?php } ?>
    $(".menu").click(function(){$(this).hasClass("active")?($(this).removeClass("active"),$("#sub-nav").animate({left:"-260px"}),$(".sub-nav-overlay").hide(),$("body").css({overflow:"auto"}),$("#sub-nav li").removeClass("active")):($(this).addClass("active"),$("#sub-nav").animate({left:"0"}),$(".sub-nav-overlay").show(),$("body").css({overflow:"hidden"}))}),$(".sub-nav-overlay").click(function(){$(".menu").hasClass("active")&&($(".menu").removeClass("active"),$("#sub-nav").animate({left:"-260px"}),$(this).hide(),$("body").css({overflow:"auto"}),$("#sub-nav li").removeClass("active"))}),$(".main-menu").click(function(){if($(this).hasClass("active")){$(".main-menu").removeClass("active")}else{$(".main-menu").removeClass("active"),$(this).addClass("active")}});
}); 
function showHideCookieDiv(){var o=$("#cookie-blue-box-container").css("display");"block"==o&&($("#cookie-blue-box-container").slideUp("slow"),set_transporteca_cookies())} 
</script> 
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="customs_clearance_pop" class="tooltip-pop"></div>
<div id="user_account_login_popup" style="display:none;"></div>   
<?php  
    if((int)$iDonotIncludeHiddenSearchHeader==0 && !empty($style) && $showCookiePopForHoldingPage)
    { 
       if(empty($_COOKIE['__DISPLAY_COOKIE_NOTIFICATION__']))
       {
           if($_SESSION['displayed_cookie_message']==1 && $_SESSION['displayed_cookie_on_page']!=$_SERVER['REQUEST_URI'])
           {
                $cookieValue = "CookieValue";
                if(__ENVIRONMENT__ == "LIVE")
                {
                    setcookie("__DISPLAY_COOKIE_NOTIFICATION__", $cookieValue, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
                }
                else
                {
                    setcookie("__DISPLAY_COOKIE_NOTIFICATION__", $cookieValue, time()+(3600*24*90),'/');
                }
                unset($_SESSION['displayed_cookie_on_page']);

                $_SESSION['displayed_cookie_message']=0;
                unset($_SESSION['displayed_cookie_message']);
           } 
           else
           {
                echo display_cookie_text_details($iFromLandingPage);   
                $_SESSION['displayed_cookie_message'] = 1;
                $_SESSION['displayed_cookie_on_page'] = $_SERVER['REQUEST_URI'] ;
           }
       }
    } 
?>