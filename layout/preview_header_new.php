<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

//$szGlobalLanguage = get_language_name();
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
require_once( __APP_PATH__ . "/inc/admin_functions.php" );
require_once( __APP_PATH__ . "/inc/courier_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once(__APP_PATH__."/ipcheck/userip/ip.codehelper.io.php");
require_once(__APP_PATH__. "/ipcheck/userip/php_fast_cache.php");

if(!empty($_COOKIE['__USER_LOGIN_COOKIE__']) && (int)$_SESSION['user_id']<=0)
{  
	$kUserNew1 = new cUser();
	$kUserNew1->cookieUserLogin($_COOKIE['__USER_LOGIN_COOKIE__']);
} 
$szLanguage = sanitize_all_html_input($_REQUEST['lang']);

if(empty($szLanguage))
{
	//$szLanguage = $_SESSION['transporteca_language_en'];
}

if($page_404==1)
{
	$szLanguage = $_SESSION['transporteca_language_en'];
}

if(!empty($szLanguage))
{
	define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__."/".__TRANSPORTECA_DANISH_URL__);
	define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__);
   	define_constants($szLanguage); 
   	
	$_SESSION['transporteca_language_en'] = $szLanguage ;
}
else
{
	$szLanguage = 'english';
	define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
	define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__);
	define_constants($szLanguage);
	
	$_SESSION['transporteca_language_en'] = '';
	unset($_SESSION['transporteca_language_en']);
	
}

$lang_code = __LANGUAGE__;
I18n::add_language(strtolower($lang_code));

$iLanguage = getLanguageId();

if(empty($szMetaTitle))
{
	$szMetaTitle = "Landing page ";
}

$t_base = "home/";

$landing_page_url = __LANDING_PAGE_URL__ ;

$kWhsSearch = new cWHSSearch();
$kExplain = new cExplain;

?>
<!doctype html>
<html lang="en">
	<head>
	<script type="text/javascript">
                  if (document.location.protocol == 'https:')
                  {
                        var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                         var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
                  }
                  else
                  {
                        var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                         var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
                  }                 
      </script>	  
 	   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
	   <? if(!empty($szMetaKeywords)){?>
	   <meta name="keywords" content="<?=$szMetaKeywords;?>" />
	   <? } 
	   	  if(!empty($szMetaDescription)){?>
	   <meta name="description" content="<?=$szMetaDescription;?>" />
	   <? } ?>
	    <title id="metatitle"><? if(!empty($szMetaTitle)){ echo $szMetaTitle; }?></title>
	    	<?php
	    	if(__ENVIRONMENT__ == "LIVE")
	    	{
	    		define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
	    	}
			else if(__ENVIRONMENT__ == "DEV_LIVE")
			{
				define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
			
			}
	    	else
	    	{
	    		define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
	    	}
	    		?>
	   		 
    		<!-- CSS FILES  --> 
    		<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/new-layout.css" />
    		<!-- <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/style.css" /> -->
    		<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/custom-style.css" />
    		
    		<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" />
    		<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
    		<link href="<?php echo __BASE_STORE_JS_URL__?>/icheck/skins/all.css?v=1.0.2" rel="stylesheet">
    		
    		<!-- JS FILES  -->
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>		
		<script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery-1.7.2.js" type="text/javascript"></script>		
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
		<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/custom.js"></script>
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.nicescroll.js"></script>
		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery-slider-testimonials.js"></script>
			
		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/modernizr.custom.79639.js"></script>
		<link rel="icon" id="favicon_icon_1" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />  
	   <link rel="shortcut icon" id="favicon_icon_2" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />
		 
</head>
<body> 
<?php


$kWHSSearch = new cWHSSearch();
$szGoogleMapV3Key = $kWHSSearch->getManageMentVariableByDescription('__GOOGLE_MAP_V3_API_KEY__');

if($iBookingTermsPage==1)
{ 
	//Nothing need to show here 
}
else
{
?>
<div id="Transporteca_popup"></div>
<div id="signin_help_pop" class="help-pop"></div>
<div id="leave_page_div" style="display:none;">	</div>
<div id="ajaxLogin"></div>
<div id="contactPopup"></div>

	<header class="header clearfix">
		<nav><a href="javascript:void(0);" class="menu"><?=t($t_base.'header/menu');?></a>
			<div class="sub-nav-overlay">&nbsp;</div>
			<div class="sub-nav" id="sub-nav">
				<div class="login-btn"><a href="javascript:void(0)" id="login"><?=t($t_base.'header/log_in');?></a></div> 
				<div class="social-icons"><a href="<?php echo $szFacebookUrl;?>" target="_blank" class="facebook"><?=t($t_base.'header/facebook');?></a><a href="<?php echo $szTwitterUrl;?>" target="_blank" class="twitter"><?=t($t_base.'header/twitter');?></a><a href="<?php echo $szLinkedinUrl;?>" target="_blank" class="linkedin"><?=t($t_base.'header/linkedin');?></a></div>
			</div>
		</nav>
		<h1 class="logo"><a href="javascript:void(0);" onclick="display_abandon_popup();"><?=t($t_base.'header/transporteca');?></a></h1>
		<div class="login">
			<a href="javascript:void(0)" id="login"><?=t($t_base.'header/log_in');?></a>
		</div>		
		<div class="telephone">
		<?php 
//			if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
//			{ 
//			 	$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//			 	echo $szCustomerCareNumer ;  
//			} else {
				$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage);
			    echo $szCustomerCareNumer ; 
			 //}
		?>
		</div>
		<div class="customer-services"><a href="javascript:$zopim.livechat.window.show()"><?=t($t_base.'header/customer_services');?></a></div>
</header>
<header id="header-search" class="header sub clearfix" style="display:none;">
	<nav><a href="javascript:void(0);" class="menu"><?=t($t_base.'header/menu');?></a></nav>
	<h1 class="logo"><a <?php if((int)$idBooking>0 && ($display_abandon_message)) {?>href="javascript:void(0);" onclick="display_abandon_popup();" <?php }else {?>href="<?=__BASE_URL__?>"<? }?>><?=t($t_base.'header/transporteca');?></a></h1>	
	<div id="transporteca_hidden_search_form_container">
		<?php 
			echo display_new_search_form($kBooking,$postSearchAry,true);
		?>
	</div>	
	<div class="login">
		<a href="javascript:void(0)" onclick="userLoginPopUp('loginForm_header');" id="login"><?=t($t_base.'header/log_in');?></a>
		<form id="loginForm_header" name="loginForm" method="post" action="">						
			<input type="hidden" name="loginArr[szRedirectUrl]" id="szHeaderRedirectUrl" value="<?=__MAIN_SITE_HOME_PAGE_URL__.$_SERVER['REQUEST_URI']?>" >
			<input type="hidden" name="loginArr[iDoNotDisplayErrorMessage]" id="1" value="1" >
		</form>	
	</div>
	<?php 
//		if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
//		{ 
//		 	$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//		} else {
			$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage);
		    //echo $szCustomerCareNumer ; 
		 //}
	?>
	<div class="telephone" title="<?php echo $szCustomerCareNumer; ?>">
		<?php //echo $szCustomerCareNumer; ?>
	</div>
</header>	
	
	
<header id="header-search" class="header sub clearfix" <?php echo $headerSearchStyle;?>>
	<nav><a href="javascript:void(0);" class="menu"><?=t($t_base.'header/menu');?></a></nav>
	<h1 class="logo"><a <?php if((int)$idBooking>0 && ($display_abandon_message)) {?>href="javascript:void(0);" onclick="display_abandon_popup();" <?php }else {?>href="<?=__BASE_URL__?>"<? }?>><?=t($t_base.'header/transporteca');?></a></h1>	
	<div id="transporteca_hidden_search_form_container" <?php echo $headerSearchStyle;?>>
		<?php 
			echo display_new_search_form($kBooking,$postSearchAry,true);
		?>
	</div>	
	<div class="login">
		<a href="javascript:void(0)"  id="login"><?=t($t_base.'header/log_in');?></a>
	</div>
	<?php 
//		if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
//		{ 
//		 	$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//		} else {
			$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage);
		//}
	?>
	<div class="telephone" title="<?php echo $szCustomerCareNumer; ?>"> 
	</div>
</header>	
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=en" type="text/javascript"></script>
<script type="text/javascript">

$().ready(function(){
<?php if((int)$headerSearchFlag==0){?>

	$.fn.scrollBottom = function() {
		return $(document).height() - this.scrollTop() - this.height();
	}; 
	var $el = $('#header-search');
	var $window = $(window);

	$window.bind("scroll resize", function() {
		var gap = $window.height() - $el.height() - 10;
		var visibleFoot = 870 - $window.scrollBottom();
		var scrollTop = $window.scrollTop()
		
		//previously it was 870 + 10
		if(scrollTop < 870 - 433){
			$el.css({
				top: (870 - scrollTop) + "px",
				bottom: "auto",				
				display: "none"
			});			
			$("#iHiddenChanged").attr('value','2');
		} else {
			$el.css({
				top: 0,
				bottom: "auto",
				display: "block"
			});
			fill_data_to_hidden_form();
		}
	});
	<?php }?>
});
</script>

<?php }?>