<?php $t_left_nav='management/leftNav'; ?>
	<p><?=t($t_left_nav.'/title/upload_Serv');?></p>
	<ul>
		<li <?php if($_REQUEST['flag']=='sub'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_SERVICES_SUBMITTED_URL__;?>"><?=t($t_left_nav.'/fields/submitted');?></a></li>
		<li <?php if($_REQUEST['flag']=='workProgress'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_WORK_IN_PROGRESS_URL__?>"><?=t($t_left_nav.'/fields/work_in_progress');?></a></li>
	</ul> 
	<br>
	<p><?=t($t_left_nav.'/title/insurance');?></p>
	<ul>
		<li <?php if($_REQUEST['flag']=='nb'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_SERVICES_NEW_BOOKING__;?>"><?=t($t_left_nav.'/fields/new_bookings');?></a></li>
		<li <?php if($_REQUEST['flag']=='sent'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_BOOKING_SENT__?>"><?=t($t_left_nav.'/fields/sent');?></a></li>
		<li <?php if($_REQUEST['flag']=='conf'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_BOOKING_CONFIRMED__?>"><?=t($t_left_nav.'/fields/confirmed');?></a></li>
		<li <?php if($_REQUEST['flag']=='canc'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_BOOKING_CANCELLED__?>"><?=t($t_left_nav.'/fields/cancelled');?></a></li>
		<br>
		<li <?php if($_REQUEST['flag']=='rates'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_INSURANCE_RATES__?>"><?=t($t_left_nav.'/fields/rates');?></a></li>
	</ul>