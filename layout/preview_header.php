<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/admin_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );

if(!defined('__BASE_URL__')) 
{
    $kConfig = new cConfig();

    $currentLangArr=$kConfig->getLanguageDetails('',$iLanguage);
    if(!empty($currentLangArr) && $iLanguage!=__LANGUAGE_ID_ENGLISH__)
    {
        $szLanguageCode=strtolower($currentLangArr[0]['szLanguageCode']);
        define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__.'/'.$szLanguageCode);
        define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__.'/'.$szLanguageCode);

        define("__LANGUAGE__",strtolower($currentLangArr[0]['szName']));
    }
    else
    {
        define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
        define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__);
        define("__LANGUAGE__",'english');
    }	
}
$t_base = "home/";
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code));


if(empty($szMetaTitle))
{
    $szMetaTitle = "Landing page ";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
	<head>
	   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
	   <?php if(!empty($szMetaKeywords)){?>
	   <meta name="keywords" content="<?=$szMetaKeywords;?>" />
	   <? } 
	   	  if(!empty($szMetaDescription)){?>
	   <meta name="description" content="<?=$szMetaDescription;?>" />
	   <?php } ?>
	    <title id="metatitle"><? if(!empty($szMetaTitle)){ echo $szMetaTitle; }?></title>
	    		
	    <?php
	    	if(__ENVIRONMENT__ == "LIVE")
	    	{
	    		define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
	    		?>
	    		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery-latest.js"></script>
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/style.css" />
				<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/functions.js"></script>
				<script type='text/javascript' src='<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.autocomplete.js'></script>
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/jquery.autocomplete.css" />
				<link rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
				<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/ui.datepicker.js"></script>
	    		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/custom.js"></script>
	    		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js"></script>
	    		
	    		<link rel="alternate" type="application/rss+xml" title="Transporteca" title="RSS 2.0" href="<?=__RSS_PAGE_URL__.'/'?>" />
				<link rel="alternate" type="text/xml" title="RSS .92" href="<?=__RSS_PAGE_URL__.'/'?>" />
				<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?=__RSS_PAGE_URL__.'/'?>" />
				
	    		<?php
	    	}
	    	else 
	    	{
	    		define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL__."/images");
	    ?>
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
				
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/style.css" />
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
				<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" />
				<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/custom.js"></script>
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.nicescroll.js"></script>
				
				<link rel="alternate" type="application/rss+xml" title="Transporteca" title="RSS 2.0" href="<?=__RSS_PAGE_URL__.'/'?>" />
				<link rel="alternate" type="text/xml" title="RSS .92" href="<?=__BASE_URL__.'/rss'?>" />
				<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?=__BASE_URL__.'/rss'?>" />
		<?php
			}
		?>
			<link rel="icon" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />  
		   <link rel="shortcut icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />
	</head>
	<body class="mainpage">
		<div id="header">
			<a href="javascript:void(0);" onclick="showHide('leave_page_div');" href="<?=__BASE_URL__?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/transporteca-logo.png" alt="Transporteca" title="Transporteca" border="0" class="logo" /></a>
			<div class="header-links">
			<span id="header_user_name_container">
				
				<p class="user" >
				<div class="header_login">
                                    <form id="loginForm_header" name="loginForm" method="post" action="">
                                        <input type="email" name="loginArr[szEmail]" id="szHeaderEmail" value="<?=!empty($_COOKIE['__USER_EMAIL_COOKIE__']) ? $_COOKIE['__USER_EMAIL_COOKIE__'] : t($t_base.'header/email');?>" onfocus="blank_me(this.id,'<?=t($t_base.'header/email');?>')" onblur="show_me_header(this.id,'<?=t($t_base.'header/email');?>')"  >
                                        <input type="text" name="loginArr[szPassword]" id="szHeaderPassword" value="<?=t($t_base.'header/password');?>" onfocus="blank_me_password(this.id,'<?=t($t_base.'header/password');?>')" onblur="show_me_password(this.id,'<?=t($t_base.'header/password');?>')" onkeyup="on_enter_key_press(event,'header_login');">
                                        <input type="hidden" name="loginArr[szRedirectUrl]" id="szHeaderRedirectUrl" value="<?=__MAIN_SITE_HOME_PAGE_URL__.$_SERVER['REQUEST_URI']?>" >
                                    </form>
				</div>
				<a href="javascript:void(0)" onclick="userLoginPopUp('loginForm_header');" id="login"><?=t($t_base.'header/sign_in');?></a>
				</p>	
				</span>
				<a href="javascript:void(0)" onclick="open_contact_popup();"><?=t($t_base.'header/contact');?></a>
				<div class="social-icons">
				<a href="<?=__RSS_PAGE_URL__.'/'?>" class="rss-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/rss-icon.png" alt="RSS" title="RSS" border="0" /></a>			
				<a href="http://www.linkedin.com/company/transporteca" target="_blank" class="linkedin-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/linkedin-icon.png" alt="Linkedin" title="Linkedin" border="0" /></a>
				<a href="http://twitter.com/transporteca" target="_blank" class="twitter-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/twitter-icon.png" alt="Twitter" title="Twitter" border="0" /></a>
				<a href="http://www.facebook.com/MyTransporteca" target="_blank" class="facebook-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/facebook-icon.png" alt="facebook" title="facebook" border="0" /></a>
				</div>
			</div>
		</div>
		<div id="navigation">
			<a href="<?=__BASE_URL_SECURE__?>" class="active" ><span><?=t($t_base.'header/compare');?> &amp; <?=t($t_base.'header/book');?></span><br /><?=t($t_base.'header/rates');?> &amp; <?=t($t_base.'header/services');?></a>
			<a href="<?=__BASE_URL_SECURE__?>/myBooking/" ><span><?=t($t_base.'header/my_bookings');?></span><br /><?=t($t_base.'header/review');?> &amp; <?=t($t_base.'header/repeat');?></a>
			<a href="<?=__BASE_URL_SECURE__?>/explain/"><span><?=t($t_base.'header/explain');?></span><br /><?=t($t_base.'header/how_does_it_work');?></a>
			<table class="norten_secured" width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications."> <tr> <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.transporteca.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script></td> </tr> </table>
		</div>			
			