<?php
/**
 * Ajax Header file
 */
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes();
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
  
$szLanguage = $_SESSION['transporteca_language_en'] ;

if($iGetLanguageFromRequest==1)
{ 
    $szLanguage = $_REQUEST['lang'];
    if(!empty($szLanguage))
    {
        $_SESSION['transporteca_language_en'] = $szLanguage ;
    }
}  
if(!empty($szLanguage))
{
    setSiteUrlsByLanguageName($szLanguage);
}
else
{
    $szLanguage = strtolower(__LANGUAGE_TEXT_ENGLISH__);
    define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
    define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define_constants($szLanguage);
    $_SESSION['transporteca_language_en'] = $szLanguage;
}  
if(__ENVIRONMENT__ == "LIVE")
{
    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
}
else 
{
    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL__."/images");
} 
$lang_code=__LANGUAGE__;
//echo "<br> language ".$lang_code ;
I18n::add_language(strtolower($lang_code));

$kUser= new  cUser();
if((int)$_SESSION['user_id']>0)
{
    $kUser->getUserDetails($_SESSION['user_id']);
} 

?>