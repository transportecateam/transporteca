<?php 
$t_base = "home/"; 
$szFacebookUrl= addhttp($szFacebookUrl);
$szTwitterUrl= addhttp($szTwitterUrl);
$szLinkedinUrl= addhttp($szLinkedinUrl);

$iLanguage = getLanguageId();  
 
$szLanguageRedirEnglishUrl = getFooterLanguageUrl($_SERVER['REQUEST_URI'],__LANGUAGE_ID_ENGLISH__,$szLinkedPageUrl);
$szLanguageRedirDanishUrl = getFooterLanguageUrl($_SERVER['REQUEST_URI'],__LANGUAGE_ID_DANISH__,$szLinkedPageUrl);

if($iLanguage==__LANGUAGE_ID_DANISH__)
{
    $szLanguage = "da";
}
else
{
    $szLanguage = "en";
}
  
//no need to change language so updated by default to english
$szLanguage = "en";  
$iDonotShowLanguageOption = 2;
?> 
    <footer class="footer">
        <div class="footer-container clearfix">
            <ul>
                <li class="head"><?=t($t_base.'footer/about_us');?></li>
            <?php if($iLanguage==2){?>	
                <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL_DA__?>/vision/" ><?=t($t_base.'footer/vision');?></a></li>
                <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL_DA__?>/team/" ><?=t($t_base.'footer/team');?></a></li>
            <?php }else{?>
                <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL__?>/our-vision/" ><?=t($t_base.'footer/vision');?></a></li>
                <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL__?>/team/" ><?=t($t_base.'footer/team');?></a></li>
            <?php }?>
            </ul>
            <ul>
                <li class="head"><?=t($t_base.'footer/for_forwarders');?></li>
                <li><a href="<?php echo __BASE_URL__?>/forwarder/" ><?=t($t_base.'footer/how_does_it_works_footer');?></a></li>
                <li><a href="<?php echo __BASE_URL__?>/forwarderSignUp/"><?=t($t_base.'footer/signing_up');?></a></li>
            </ul>
            <ul>
                <li class="head"><?=t($t_base.'footer/for_affiliate');?></li>
                <li><a href="<?php echo __BASE_URL__?>/partnerProgramme/" ><?=t($t_base.'footer/affiliate_programme');?></a></li>
                <li><a href="javascript:void(0)" onclick="ajaxAffliateLogin();"><?=t($t_base.'header/sign_in');?></a></li>
            </ul>
            <ul>
                <li class="head"><?=t($t_base.'footer/resources');?></li>
                <li><a href="javascript:void(0);" onclick="toggleLeftMenu();"><?=t($t_base.'footer/information');?></a></li>
                <li><a href="<?php echo __RSS_FEED_PAGE_URL__?>"><?=t($t_base.'footer/news');?></a></li>
            </ul>
            <?php if($iDonotShowLanguageOption==1){?>
                <ul></ul>
            <?php } else {?>
            <ul>
                <li class="head"><?=t($t_base.'footer/quick_links');?></li>
                <?php
                    if(__LANGUAGE__==__LANGUAGE_TEXT_ENGLISH__)
                    {  
                         ?>
                        <li><a><?=t($t_base.'footer/english');?></a></li>
                        <li><a href="<?php echo $szLanguageRedirDanishUrl; ?>"  ><?=t($t_base.'footer/danish');?></a></li>
                        <?php
                    }
                    else
                    {
                        ?> 
                        <li><a href="<?php echo $szLanguageRedirEnglishUrl; ?>"><?=t($t_base.'footer/english');?></a></li>
                        <li><a><?=t($t_base.'footer/danish');?></a></li>
                        <?php
                    }
                ?>
            </ul>
            <?php } ?>
        </div>
        <address>
            <div class="copyright">&copy; <?=DATE('Y');?> <?=t($t_base.'footer/copy_right');?></div>
            <div class="social-icons"><a href="<?php echo $szFacebookUrl;?>" target="_blank" class="facebook">facebook</a><a href="<?php echo $szTwitterUrl;?>" target="_blank" class="twitter">twitter</a><a href="<?php echo $szLinkedinUrl;?>" target="_blank" class="linkedin">linkedin</a><a href="<?php echo $szYoutubeUrl;?>" target="_blank" class="youtube">youtube</a></div>
            <div class="links"><a href="<?php echo __BASE_URL__?>/terms/" ><?=t($t_base.'footer/terms');?> &amp; <?=t($t_base.'footer/conditions');?></a><a href="<?php echo __BASE_URL__?>/privacyNotice/" ><?=t($t_base.'footer/privacy_notice');?></a><a href="<?php echo __BASE_URL__?>/cookies/"><?=t($t_base.'footer/cookies');?></a> <a href="<?php echo __SITE_MAP_PAGE_URL__?>" target="__blank"><?=t($t_base.'footer/site_map');?></a></div>
        </address>
    </footer>  
    </body>
</html>  
    <?php  if(__LANGUAGE__==__LANGUAGE_TEXT_ENGLISH__){ ?>
        <!--<script src="<?php echo __BASE_STORE_JS_URL__?>/api/google-map-en.min.js" type="text/javascript"></script>--> 
    <?php } else { ?>
        <!--<script src="<?=__BASE_STORE_JS_URL__?>/api/google-map-da.min.js" type="text/javascript"></script>-->
    <?php } ?> 
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
    <?php if(__ENVIRONMENT__ =='LIVE'){ 
        if($iServicePageAdword==1){ ?>
                    <!-- Google Code for S&oslash;gning Conversion Page -->
                    <script type="text/javascript" defer="1">var google_conversion_id=968221154,google_conversion_language="en",google_conversion_format="3",google_conversion_color="ffffff",google_conversion_label="nBj_CK70yggQ4sPXzQM",google_remarketing_only=!1;</script>
                    <script type="text/javascript" defer="1" src="//www.googleadservices.com/pagead/conversion.js"></script>
                    <noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968221154/?label=nBj_CK70yggQ4sPXzQM&amp;guid=ON&amp;script=0"/></div></noscript>  
		  <?php } else if($iThankYouPageAdword==1){?>			
                        <!-- Google Code for Bestil Conversion Page -->
                        <script type="text/javascript" defer="defer">
                            var google_conversion_id=968221154,google_conversion_language="da",google_conversion_format="3",google_conversion_color="ffffff",google_conversion_label="tv8iCLbzyggQ4sPXzQM",google_remarketing_only=!1;
                        </script>
                        <script type="text/javascript" defer="defer" src="//www.googleadservices.com/pagead/conversion.js"></script>
                        <noscript>
                            <div style="display:inline;">
                                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968221154/?label=tv8iCLbzyggQ4sPXzQM&amp;guid=ON&amp;script=0"/>
                            </div>
                        </noscript>  
                <?php } else { ?>
                        <script type="text/javascript" defer="1">var google_conversion_id=968221154,google_custom_params=window.google_tag_params,google_remarketing_only=!0;</script>
                        <script type="text/javascript" defer="1" src="//www.googleadservices.com/pagead/conversion.js"></script>
                        <noscript>
                            <div style="display:inline;">
                                <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/968221154/?value=0&amp;guid=ON&amp;script=0"/>
                            </div>
                        </noscript> 
                <?php } if($iServicePageAdword==1){ /* ?>	
		    <script type="text/javascript">
		    	var mouseflowHtmlDelay = 7000;
		        var _mfq = _mfq || [];
		        (function () {
		        var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
		        mf.src =
		"//cdn.mouseflow.com/projects/59ae78af-d3da-4cdc-b179-43b60579476d.js";
		        document.getElementsByTagName("head")[0].appendChild(mf);
		      })();
		    </script>
		<?php } else { ?>
			<script type="text/javascript">
		        var _mfq = _mfq || [];
		        (function () {
		        var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
		        mf.src =
		"//cdn.mouseflow.com/projects/59ae78af-d3da-4cdc-b179-43b60579476d.js";
		        document.getElementsByTagName("head")[0].appendChild(mf);
		      })();
		    </script>	
                    
                    <!-- Start Visual Website Optimizer Asynchronous Code -->
                    <script type='text/javascript'>
                    var _vwo_code=(function(){
                    var account_id=97776,
                    settings_tolerance=2000,
                    library_tolerance=2500,
                    use_existing_jquery=false,
                    // DO NOT EDIT BELOW THIS LINE
                    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
                    </script>
                    <!-- End Visual Website Optimizer Asynchronous Code -->

                    
		<?php */ } } ?>
            <?php
		if(__ENVIRONMENT__ =='DEV_LIVE')
		{
                    ?>
                    <script type="text/javascript" defer="defer">var _gaq=_gaq||[];_gaq.push(["_setAccount","UA-37029182-1"]),_gaq.push(["_trackPageview"]),function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.cache=!0,t.src="<?=__BASE_STORE_JS_URL__?>/api/ga.min.js";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}(); </script> 
                    <!--Start of Zopim Live Chat Script-->
                    <script type="text/javascript" defer="defer">$().ready(function(){setTimeout(function(){console.log("Zopim called"),window.$zopim||function(t,e){var n=$zopim=function(t){n._.push(t)},o=n.s=t.createElement(e),c=t.getElementsByTagName(e)[0];n.set=function(t){n.set._.push(t)},n._=[],n.set._=[],o.async=!0,o.setAttribute("charset","utf-8"),o.src="<?=__BASE_STORE_JS_URL__?>/api/zopim.min.js",n.t=+new Date,o.type="text/javascript",c.parentNode.insertBefore(o,c)}(document,"script")},"20000")});  </script>
                    <!--End of Zopim Live Chat Script-->  
                <?php
		}
		else if(__ENVIRONMENT__ == 'LIVE')
		{
	?>
                <script type="text/javascript">
                    !function(){var t=document.createElement("script");t.async=!0,t.type="text/javascript",t.src="//98229.hittail.com/mlt.js";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}();
                </script> 
                <!--Start of Zopim Live Chat Script-->
                <script type="text/javascript">
                    window.$zopim||function(t,e){var s=$zopim=function(t){s._.push(t)},n=s.s=t.createElement(e),c=t.getElementsByTagName(e)[0];s.set=function(t){s.set._.push(t)},s._=[],s.set._=[],n.async=!0,n.setAttribute("charset","utf-8"),n.src="//v2.zopim.com/?28qgkS9x5hAjidkz0ubaskHS1pQcLcMU",s.t=+new Date,n.type="text/javascript",c.parentNode.insertBefore(n,c)}(document,"script");
                </script>
                <!--End of Zopim Live Chat Script-->
	<?php } ?> 
            