<?php
    $t_left_nav='management/leftNav';
    
    $counFlag=checkManagementPermission($kAdmin,"__SYSTEM_COUNTRY__",2);
    $regFlag=checkManagementPermission($kAdmin,"__SYSTEM_REGION__",2);
    $postFlag=checkManagementPermission($kAdmin,"__SYSTEM_POSTCODES__",2);
    $locFlag=checkManagementPermission($kAdmin,"__SYSTEM_LOCATION_DESCRIPTION__",2);
    $currFlag=checkManagementPermission($kAdmin,"__SYSTEM_CURRENCY__",2);
    $vatFlag=checkManagementPermission($kAdmin,"__SYSTEM_VAT_APPLICATION__",2);
    $manVarFlag=checkManagementPermission($kAdmin,"__SYSTEM_SETTINGS__",2);
    $crwnFlag=checkManagementPermission($kAdmin,"__SYSTEM_CRONJOBS__",2);
    $dnsFlag=checkManagementPermission($kAdmin,"__SYSTEM_DNS_AND_APACHE__",2);
    $apiFlag=checkManagementPermission($kAdmin,"__SYSTEM_API__",2);
    $vsFlag=checkManagementPermission($kAdmin,"__SYSTEM_VIDEO_SCRIPT__",2);
    $dsFlag=checkManagementPermission($kAdmin,"__SYSTEM_DOWNLOAD__",2);
    $langVarFlag=checkManagementPermission($kAdmin,"__SYSTEM_LANGUAGE_SETTING__",2);
    $langVersionsFlag=checkManagementPermission($kAdmin,"__SYSTEM_LANGUAGE_VERSIONS__",2);
    $langMetaFlag=checkManagementPermission($kAdmin,"__SYSTEM_LANGUAGE_META_TAGS__",2);
    $standardCargoFlag=checkManagementPermission($kAdmin,"__SYSTEM_STANDARD_CARGO__",2);
    
    $dtd_tradeFlag=checkManagementPermission($kAdmin,"__DOOR_TO_DOOR_TRADES__",2);
    $railFlag=checkManagementPermission($kAdmin,"__RAIL_TRANSPORT__",2);
    $mode_transportFlag=checkManagementPermission($kAdmin,"__MODE_OF_TRANSPORT__",2);
    $CCFlag=checkManagementPermission($kAdmin,"__SYSTEM_CUSTOMER_CLEARANCE__",2);
    
    $AELFlag=checkManagementPermission($kAdmin,"__API_ERROR_LOG__",2);
    ?>
            <script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce.js"></script>
            <script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce_edit.js"></script>
<div class="hsbody-2-left account-links">
<?php /*
	<p><?=t($t_left_nav.'/title/site_text');?></p>
	<ul>
		<li <?php if($_REQUEST['flag']=='gen'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_TEXT_EDITOR_URL__;?>"><?=t($t_left_nav.'/fields/customer_site');?></a></li>
		<li <?php if($_REQUEST['flag']=='tnccust'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_TERMS_AND_CONDITION_CUSTOMER_URL__?>"><?=t($t_left_nav.'/fields/Terms_n_condition_customer');?></a></li>
		<li <?php if($_REQUEST['flag']=='faq_cust'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_FAQ_CUSTOMER_URL__;?>"><?=t($t_left_nav.'/fields/Faq_customer');?></a></li>
		<li <?php if($_REQUEST['flag']=='genfwd'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_TEXT_EDITOR_FORWARDER_URL__;?>"><?=t($t_left_nav.'/fields/forwarder_site');?></a></li>
		<li <?php if($_REQUEST['flag']=='tncfwd'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_TERMS_AND_CONDITION_URL__?>"><?=t($t_left_nav.'/fields/Terms_n_condition_forwarder');?></a></li>
		<li <?php if($_REQUEST['flag']=='faq_fwd'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_FAQ_URL__;?>"><?=t($t_left_nav.'/fields/Faq_forwarder');?></a></li>
		<li <?php if($_REQUEST['flag']=='guide'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_GUIDE_URL__;?>"><?=t($t_left_nav.'/fields/forwarder_guide');?></a></li>
		<li <?php if($_REQUEST['flag']=='hist'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_HISTORY_URL__;?>"><?=t($t_left_nav.'/fields/history');?></a></li>
	</ul>
	<br/>
 */ 
        if($counFlag || $regFlag || $postFlag || $locFlag || $currFlag || $vatFlag || $manVarFlag ) {
        ?>
	<p><?=t($t_left_nav.'/title/system_variable');?></p>	
	<ul>
                <?php if($counFlag){ ?>
		<li <?php if($_REQUEST['flag']=='coun'){?>class="active" <?php }?> ><a href="<?php echo __MANAGEMENT_COUNTRY_URL__?>"><?=t($t_left_nav.'/fields/countries');?></a></li>
                 <?php } if($regFlag){?>
                <li <?php if($_REQUEST['flag']=='reg'){?>class="active" <?php }?> ><a href="<?php echo __MANAGEMENT_REGION_URL__?>"><?=t($t_left_nav.'/fields/region');?></a></li>
		 <?php } if($postFlag){?>
                <li <?php if($_REQUEST['flag']=='post'){?>class="active" <?php }?> ><a href="<?php echo __MANAGEMENT_POSTCODE_URL__?>"><?=t($t_left_nav.'/fields/postcodes');?></a></li>
		 <?php } if($locFlag){?>
                    <!--<li <?php if($_REQUEST['flag']=='loc'){?>class="active" <?php }?> ><a href="<?php echo __MANAGEMENT_LOCATION_DESCRIPTION_URL__?>"><?=t($t_left_nav.'/fields/location_description');?></a></li>-->
		 <?php } if($currFlag){?>
                <li <?php if($_REQUEST['flag']=='curr'){?>class="active" <?php }?>><a href="<?php echo __MANAGEMENT_CURRENCY_URL__?>"><?=t($t_left_nav.'/fields/currencies');?></a></li>
		 <?php } if($vatFlag){?>
                <li <?php if($_REQUEST['flag']=='vat'){?>class="active" <?php }?>><a href="<?php echo __MANAGEMENT_OPRERATION_VAT_APPLICATION__?>"><?=t($t_left_nav.'/fields/vat_application');?></a></li>
		<!-- <li <?php if($_REQUEST['flag']=='elogs'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_EMAIL_LOGS_URL__?>?flag=elogs"><?=t($t_left_nav.'/fields/e_mail_log');?></a></li>
		 -->
                  <?php } if($manVarFlag){?>
                 <li <?php if($_REQUEST['flag']=='manVar'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_VARIABLE_URL__?>"><?=t($t_left_nav.'/fields/manage_variables');?></a></li>
                 <?php } ?>
	</ul>
        <?php }
               
        if($dtd_tradeFlag || $mode_transportFlag || $railFlag || $CCFlag)
        {?>
            <br/>
            <p><?=t($t_left_nav.'/title/trades');?></p>
            <ul>
            <?php  if($dtd_tradeFlag){?>
                <li <?php if($_REQUEST['flag']=='dtd_trade'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_DTD_TRADES__;?>"><?=t($t_left_nav.'/fields/dtd_trades');?></a></li>
             <?php } ?>
                <?php  if($mode_transportFlag){?>
                <li <?php if($_REQUEST['flag']=='mode_transport'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPRERATION_COUIER_MODE_TRANSPORT__;?>"><?=t($t_left_nav.'/fields/courier_mode');?></a></li>
             <?php } ?>
                <?php  if($langMetaFlag){?>
                <li <?php if($_REQUEST['flag']=='rail_transport'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_RAIL_TRANSPORT_URL__;?>"><?=t($t_left_nav.'/fields/lcl_rail_transport');?></a></li> 
             <?php } ?>
                 <?php  if($CCFlag){?>
                <li <?php if($_REQUEST['flag']=='customer_clearance'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_TRADE_CUSTOMER_CLEARANCE_URL__?>"><?=t($t_left_nav.'/fields/customer_clearance');?></a></li>
             <?php } ?>
             </ul>
        <?php }
        if($langVarFlag || $langVersionsFlag || $langMetaFlag || $standardCargoFlag)
        {?>
            <br/>
            <p><?=t($t_left_nav.'/title/language');?></p>
            <ul>
            <?php  if($langVarFlag){?>
                <li <?php if($_REQUEST['flag']=='langVar'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_LANGUAGE_CONFIGURATION_URL__?>"><?=t($t_left_nav.'/fields/configuration');?></a></li>
             <?php } ?>
                <?php  if($langVersionsFlag){?>
                <li <?php if($_REQUEST['flag']=='langVersions'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_LANGUAGE_VERSIONS_URL__?>"><?=t($t_left_nav.'/fields/versions');?></a></li>
             <?php } ?>
                <?php  if($langMetaFlag){?>
                <li <?php if($_REQUEST['flag']=='langMetaFlag'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_LANGUAGE_PAGE_META_TAGS_URL__?>"><?=t($t_left_nav.'/fields/page_meta_tags');?></a></li>
             <?php } ?>
                 <!--<?php  if($standardCargoFlag){?>
                <li <?php if($_REQUEST['flag']=='standardCargoFlag'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_LANGUAGE_STANDARD_CARGO_URL__?>"><?=t($t_left_nav.'/fields/standard_cargo_manage');?></a></li>
             <?php } ?>-->
             </ul>
        <?php }
        if($crwnFlag || $dnsFlag || $AELFlag){?>
	<br/>
	<p><?=t($t_left_nav.'/title/log');?></p>
	<ul>
            <?php if($crwnFlag){ ?>
            <li <?php if($_REQUEST['flag']=='crwn'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_CRON_JOB_URL__?>"><?=t($t_left_nav.'/fields/crown_job');?></a></li>
             <?php } if($dnsFlag){?>
            <li <?php if($_REQUEST['flag']=='dns'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_DNS_APACHE_URL__?>"><?=t($t_left_nav.'/fields/dns_apache');?></a></li>
             <?php } /*if($apiFlag){?>
            <li <?php if($_REQUEST['flag']=='api'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_API_LOGS_URL__?>"><?=t($t_left_nav.'/fields/api_logs');?></a></li>
            <?php }*/
            if($AELFlag){
            ?>            
            <li <?php if($_REQUEST['flag']=='apierr'){?>class="active" <?php }?> ><a href="<?php echo __MANAGEMENT_API_ERROR_URL__?>"><?=t($t_left_nav.'/fields/error_message');?></a></li>
            <?php }?>
        </ul>
        <?php } if($vsFlag || $dsFlag){?>
	<br>
	<p><?=t($t_left_nav.'/title/sitemap');?></p>
	<ul>
            <?php if($vsFlag){ ?>
            <li <?php if($_REQUEST['flag']=='vs'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SITE_MAP_VIDEO_URL__?>"><?=t($t_left_nav.'/fields/video_script');?></a></li>
            <?php } if($dsFlag){?>
            <li <?php if($_REQUEST['flag']=='ds'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SITE_MAP_VIDEO_DOWLOAD_URL__?>"><?=t($t_left_nav.'/fields/download');?></a></li>
            <?php } ?>
        </ul>	
        <?php }?>
</div>