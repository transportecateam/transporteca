<?php
	require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
	if(!isset($kExplain))
	{
		$kExplain = new cExplain;
	}
	
	$iLanguage = getLanguageId();
	$links = array();
	$links = $kExplain->selectPageDetailsUserEnd($iLanguage);
	
	$t_base = "home/";
	
	$howDoesworksLinks = array();
	$howDoesworksLinks = $kExplain->selectPageDetailsUserEnd($iLanguage,1);
	
	
	$companyLinksAry = array();
	$companyLinksAry = $kExplain->selectPageDetailsUserEnd($iLanguage,2);

	$tranportpediaLinksAry = array();
	$tranportpediaLinksAry = $kExplain->selectPageDetailsUserEnd($iLanguage,3);
	?>
<div class="hsbody-2-left account-links">
	<ul>
	<div id="showExplainLeftNav">
	<? 
	if($howDoesworksLinks != array())
	{
		foreach($howDoesworksLinks as $linking)
		{
		
			if($_REQUEST['menuflag'] == $linking['szLink'])
			{
				$szClass = 'class="active-triangle"';
				if($iLanguage==2)
				{
					$szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL_DA__."/".$linking['szLink']."/";
				}
				else
				{
					$szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL__."/".$linking['szLink']."/";
				}
			}
			else 
			{
				$szClass = 'class="triangle" ' ;
				if($iLanguage==2)
				{
					$szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL_DA__."/".$linking['szLink']."/";
				}
				else
				{
					$szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL__."/".$linking['szLink']."/";
				}
			}
	?>	
		<li <?=$szClass?> >
		<a href="<?=$szUrl?>"> <?=$linking['szPageHeading']?></a></li>
		<? 
	  }
	}
	if($companyLinksAry != array())
	{
		foreach($companyLinksAry as $linking)
		{
			$szUrl='';
			if($_REQUEST['menuflag'] == $linking['szLink'])
			{
				$szClass = 'class="active-triangle"';
				if($iLanguage==2)
				{
					$szUrl = __BASE_URL__."/".__COMPANY_URL_DA__."/".$linking['szLink']."/";
				}
				else
				{
					$szUrl = __BASE_URL__."/".__COMPANY_URL__."/".$linking['szLink']."/";
				}
			}
			else 
			{
				$szClass = 'class="triangle" ' ;
				if($iLanguage==2)
				{
					$szUrl = __BASE_URL__."/".__COMPANY_URL_DA__."/".$linking['szLink']."/";
				}
				else
				{
					$szUrl = __BASE_URL__."/".__COMPANY_URL__."/".$linking['szLink']."/";
				}
			}
	?>	
		<li <?=$szClass?> >
		<a href="<?=$szUrl?>"> <?=$linking['szPageHeading']?></a></li>
		<? 
		}
	}
	
	if($tranportpediaLinksAry != array())
	{
		foreach($tranportpediaLinksAry as $linking)
		{
			$szUrl='';
			if($_REQUEST['menuflag'] == $linking['szLink'])
			{
				$szClass = 'class="active-triangle"';
				if($iLanguage==2)
				{
					$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL_DA__."/".$linking['szLink']."/";
				}
				else
				{
					$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL__."/".$linking['szLink']."/";
				}
			}
			else 
			{
				$szClass = 'class="triangle" ' ;
				if($iLanguage==2)
				{
					$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL_DA__."/".$linking['szLink']."/";
				}
				else
				{
					$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL__."/".$linking['szLink']."/";
				}
			}
	?>	
		<li <?=$szClass?> >
		<a href="<?=$szUrl?>"> <?=$linking['szPageHeading']?></a></li>
		<? 
		}
	}
	$szSearchDefaultText = t($t_base.'footer/type_word_or_phrase');
?>
	</div>
	<li <?php if($_REQUEST['menuflag'] == 'blg'){?>class="active-triangle" <?php }else{?> class="triangle" <?}?> ><a href="<?=__BLOG_PAGE_URL__?>"><?=t($t_base.'footer/knowledge_centre');?></a></li>
	<li class="triangle" ><a href="<?=__FAQ_PAGE_URL__?>"> <?=t($t_base.'footer/FAQ');?></a></li>
	</ul>
	<div style="padding-top:10px;text-align:center;margin-left:-4px;"><div style="margin-bottom:15px;"><input style="font-style:italic;color: #7F7F7F;" type="text" id="searchContent" name="searchExplain" onblur="show_me(this.id,'<?php echo $szSearchDefaultText; ?>');" onkeyup="submitDetailsSearch(event,'<?php echo $szSearchDefaultText; ?>');" onfocus="blank_me(this.id,'<?php echo $szSearchDefaultText; ?>');" value="<?php echo $szSearchDefaultText; ?>"></div><a class="button1" onclick="searchContentSubmit('<?php echo $szSearchDefaultText; ?>')"><span><?php echo t($t_base.'footer/search')?></span></a></div>
</div>