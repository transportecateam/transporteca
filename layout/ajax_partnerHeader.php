<?php
/**
 * Ajax Header file
 */
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
include_classes(); 
require_once( __APP_PATH__ . "/inc/I18n.php" ); 

if(__ENVIRONMENT__ == "LIVE")
{
    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
}
else 
{
    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL__."/images");
} 
define("__LANGUAGE__",$szLanguage); 
$lang_code = $szLanguage; 
I18n::add_language(strtolower($lang_code));
 
?>