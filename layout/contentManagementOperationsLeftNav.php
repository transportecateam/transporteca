<?php
    $t_left_nav='management/leftNav';
    $statFlag=checkManagementPermission($kAdmin,"__OPERATION_STATISTICS__",2);
    $operFlag=checkManagementPermission($kAdmin,"__BOOKING_CONFIRMED__",2);
    $non_ackFlag=checkManagementPermission($kAdmin,"__BOOKING_NOT_ACKNOWLEDGED__",2);
    $ncbFlag=checkManagementPermission($kAdmin,"__BOOKING_NOT_CONFIRMED__",2);
    $cbFlag=checkManagementPermission($kAdmin,"__BOOKING_CANCELLED__",2);        
?>
<div class="hsbody-2-left account-links">
    <!--<p><?=t($t_left_nav.'/title/tasks');?></p>
    <ul>
        <li <?php if($_REQUEST['flag']=='tpt'){?>class="active" <?php }?>><a href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?>"><?=t($t_left_nav.'/title/pending_tray');?></a></li>
    </ul> -->
    <?php
    if($statFlag || $operFlag || $non_ackFlag || $ncbFlag || $cbFlag){?>
    <p><?=t($t_left_nav.'/title/bookings');?></p>    
    <ul>
        <?php
        if($statFlag){?>
        <li <?php if($_REQUEST['flag']=='stat'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__STATISTICS_URL__;?>"><?=t($t_left_nav.'/fields/statistics');?></a></li>
        <?php } if($operFlag){?>
        <li <?php if($_REQUEST['flag']=='oper'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__CONFIRMED_URL__;?>"><?=t($t_left_nav.'/fields/confirmed');?></a></li>
        <?php } if($non_ackFlag){?>
        <li <?php if($_REQUEST['flag']=='non_ack'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__NON_ACKNOWLEDGE_URL__;?>"><?=t($t_left_nav.'/fields/non_acknowledge');?></a></li>
        <?php } if($ncbFlag){?>
        <li <?php if($_REQUEST['flag']=='ncb'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS_NOT_CONFIRMED_URL__;?>"><?=t($t_left_nav.'/fields/not_confirm');?></a></li>
        <?php } if($cbFlag){?>
        <li <?php if($_REQUEST['flag']=='cb'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__CANCELLED_BOOKING_URL__;?>"><?=t($t_left_nav.'/fields/cancelled');?></a></li>
        <?php }?>
    </ul> 
    <?php }
    
    $wtwFlag=checkManagementPermission($kAdmin,"__CURRENT_WTW__",2);
    $haulageFlag=checkManagementPermission($kAdmin,"__CURRENT_HAULAGE__",2);
    $ccFlag=checkManagementPermission($kAdmin,"__CUSTOMS_CLEARANCE__",2);
    $expFlag=checkManagementPermission($kAdmin,"__LCL_SERVICES_EXPIRING__",2);
    $tryFlag=checkManagementPermission($kAdmin,"__LCL_SERVICES_TRY_IT_OUT__",2);
    $lbFlag=checkManagementPermission($kAdmin,"__LCL_SERVICES_LINK_BUILDER__",2);
    $curr_agrrFlag=checkManagementPermission($kAdmin,"__CURRENT_AGREEMENT__",2);
    $voga_pricingFlag=checkManagementPermission($kAdmin,"__STANDARD_CARGO_PRICING__",2);
    
    ?>
    <!--<p style="margin: 15px 0 5px;" ><?=t($t_left_nav.'/title/manual_quote');?></p>
    <ul>
        <li <?php if($_REQUEST['flag']=='new'){?>class="manual-quotes-li active" <?php }else {?> class="manual-quotes-li"<?php }?> id="manual-quotes-li-new"><a href="<?=__MANAGEMENT_OPRERATION_NEW_BOOKING_QUOTES__;?>"><?=t($t_left_nav.'/fields/new');?></a></li>
        <li <?php if($_REQUEST['flag']=='sen'){?>class="manual-quotes-li active" <?php }else {?> class="manual-quotes-li"<?php }?> id="manual-quotes-li-sent"><a href="<?=__MANAGEMENT_OPRERATION_SENT_BOOKING_QUOTES__;?>"><?=t($t_left_nav.'/fields/sent');?></a></li>
        <li <?php if($_REQUEST['flag']=='won'){?>class="manual-quotes-li active" <?php }else {?> class="manual-quotes-li"<?php }?> id="manual-quotes-li-won"><a href="<?=__MANAGEMENT_OPRERATION_WON_BOOKING_QUOTES__;?>"><?=t($t_left_nav.'/fields/won');?></a></li>
        <li <?php if($_REQUEST['flag']=='exp_quote'){?>class="manual-quotes-li active" <?php }else {?> class="manual-quotes-li"<?php }?> id="manual-quotes-li-expired"><a href="<?=__MANAGEMENT_OPRERATION_EXPIRED_BOOKING_QUOTES__;?>"><?=t($t_left_nav.'/fields/expired');?></a></li>
    </ul>-->
    <?php
    if($wtwFlag || $haulageFlag || $ccFlag || $expFlag || $tryFlag || $curr_agrrFlag){?>
    <p style="margin: 15px 0 5px;" ><?=t($t_left_nav.'/title/lcl_services');?></p>
    <ul>
        <?php if($curr_agrrFlag) {?>
            <li <?php if($_REQUEST['flag']=='curr_agrr'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPRERATION_COUIER_AGREEMENT__;?>"><?=t($t_left_nav.'/fields/current_agreements');?></a></li> 
        <?php }if($wtwFlag){?>
            <li <?php if($_REQUEST['flag']=='wtw'){?>class="active" <?php }?>>
                <a href="<?=__MANAGEMENT_OPERATIONS__WTW_URL__;?>"><?=t($t_left_nav.'/fields/current_wtw');?></a>
            </li>
            <li <?php if($_REQUEST['flag']=='air'){?>class="active" <?php }?>>
                <a href="<?=__MANAGEMENT_OPERATIONS_AIR_SERVICES_URL__;?>">Airfreight Services</a>
            </li>
        <?php } if($haulageFlag){?>
            <li <?php if($_REQUEST['flag']=='haulage'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__HAULAGE_URL__;?>"><?=t($t_left_nav.'/fields/current_haulage');?></a></li>
        <?php } if($ccFlag){?>
            <li <?php if($_REQUEST['flag']=='cc'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_CC_URL__;?>"><?=t($t_left_nav.'/fields/customer_clearence');?></a></li>
        <?php } if($expFlag){?>
            <li <?php if($_REQUEST['flag']=='exp'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_EXPIRING_WTW_URL__;?>"><?=t($t_left_nav.'/fields/expirint_wtw');?></a></li>
        <?php } if($voga_pricingFlag){?>
            <li <?php if($_REQUEST['flag']=='voga_pricing'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_STANDARD_CARGO_PRICING__;?>"><?=t($t_left_nav.'/fields/pricing');?></a></li>
        <?php } if($tryFlag){?>
            <li <?php if($_REQUEST['flag']=='try'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_TRY_IT_OUT_URL__;?>"><?=t($t_left_nav.'/fields/try_it_out');?></a></li>
        <?php } /*if($railFlag){?>
            <li <?php if($_REQUEST['flag']=='rail_transport'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_RAIL_TRANSPORT_URL__;?>"><?=t($t_left_nav.'/fields/rail_transport');?></a></li> 
        <?php } /*if($lbFlag){?>
            <li <?php if($_REQUEST['flag']=='lb'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_LINK_BUILDER_URL__;?>"><?=t($t_left_nav.'/fields/link_builder');?></a></li>
        <?php }*/ ?> 
    </ul>
    <?php } 
    
    $subFlag=checkManagementPermission($kAdmin,"__UPLOAD_SERVICE_SUBMITTED__",2);
    $workProgressFlag=checkManagementPermission($kAdmin,"__UPLOAD_SERVICE_WORK_IN_PROGRESS__",2); 
    ?>
    <!-- Placing menus from service to here -->
    <?php
    if($subFlag || $workProgressFlag){?>
    <p style="margin: 15px 0 5px;"><?=t($t_left_nav.'/title/upload_Serv');?></p>    
    <ul>
        <?php
        if($subFlag){?>
        <li <?php if($_REQUEST['flag']=='sub'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_SERVICES_SUBMITTED_URL__;?>"><?=t($t_left_nav.'/fields/submitted');?></a></li>
        <?php } if($workProgressFlag){?>
        <li <?php if($_REQUEST['flag']=='workProgress'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_WORK_IN_PROGRESS_URL__?>"><?=t($t_left_nav.'/fields/work_in_progress');?></a></li>
        <?php  }?>
    </ul>  
    <?php }
    
    $label_dwnFlag=checkManagementPermission($kAdmin,"__LABEL_DOWNLOAD__",2);
    $label_sendFlag=checkManagementPermission($kAdmin,"__LABEL_SENT__",2);
    $label_n_sendFlag=checkManagementPermission($kAdmin,"__LABEL_NOT_SENT__",2);
    
    $provider_productFlag=checkManagementPermission($kAdmin,"__PROVIDER_PRODUCTS__",2);
    ?>
    <?php
    if($label_dwnFlag || $label_sendFlag || $label_n_sendFlag || $provider_productFlag){?>
    <p style="margin: 15px 0 5px;" ><?=t($t_left_nav.'/title/couier_services');?></p>
    <ul>
        <?php
        if($label_dwnFlag){?>
    	<li <?php if($_REQUEST['flag']=='label_dwn'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPRERATION_COUIER_LABEL_DOWNLOAD__;?>"><?=t($t_left_nav.'/fields/labels_downloaded');?></a></li>
    	<?php } if($label_sendFlag){?>
        <li <?php if($_REQUEST['flag']=='label_send'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPRERATION_COUIER_LABEL_SEND__;?>"><?=t($t_left_nav.'/fields/labels_sent');?></a></li>
    	<?php } if($label_n_sendFlag){?>
        <li <?php if($_REQUEST['flag']=='label_n_send'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPRERATION_COUIER_LABEL_NOT_SEND__;?>"><?=t($t_left_nav.'/fields/labels_not_sent');?></a></li>
    	 <?php } if($provider_productFlag){?>
        <li <?php if($_REQUEST['flag']=='provider_product'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPRERATION_COUIER_PROVIDER_PRODUCT__;?>"><?=t($t_left_nav.'/fields/providers_products');?></a></li>
        <?php } /*if($mode_transportFlag){?>
        <li <?php if($_REQUEST['flag']=='mode_transport'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPRERATION_COUIER_MODE_TRANSPORT__;?>"><?=t($t_left_nav.'/fields/mode_transport');?></a></li>
        <?php }*/?>
    </ul>  
    <?php }
    
    if($voga_pricingFlag)
    { /*
    ?>
    <p style="margin: 15px 0 5px;"><?=t($t_left_nav.'/title/standard_cargo');?></p>
    <ul>
        <li <?php if($_REQUEST['flag']=='voga_pricing'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_STANDARD_CARGO_PRICING__;?>"><?=t($t_left_nav.'/fields/pricing');?></a></li>
    </ul>
    <?php  
     */ 
    }
    $nbFlag=checkManagementPermission($kAdmin,"__INSURANCE_NEW_BOOKING__",2);
    $sentFlag=checkManagementPermission($kAdmin,"__INSURANCE_SENT_BOOKING__",2);
    $confFlag=checkManagementPermission($kAdmin,"__INSURANCE_CONFIRMED_BOOKING__",2);
    $cancFlag=checkManagementPermission($kAdmin,"__INSURANCE_CANCELLED_BOOKING__",2);
    $ratesFlag=checkManagementPermission($kAdmin,"__INSURANCE_RATES_BOOKING__",2);
     
    if($nbFlag || $sentFlag || $confFlag || $cancFlag || $ratesFlag){
    ?>
    <p style="margin: 15px 0 5px;"><?=t($t_left_nav.'/title/insurance');?></p>
    <ul>
        <?php
        if($nbFlag){?>
        <li <?php if($_REQUEST['flag']=='nb'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_SERVICES_NEW_BOOKING__;?>"><?=t($t_left_nav.'/fields/new_bookings');?></a></li>
        <?php } if($sentFlag){?>
        <li <?php if($_REQUEST['flag']=='sent'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_BOOKING_SENT__?>"><?=t($t_left_nav.'/fields/sent');?></a></li>
        <?php } if($confFlag){?>
        <li <?php if($_REQUEST['flag']=='conf'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_BOOKING_CONFIRMED__?>"><?=t($t_left_nav.'/fields/confirmed');?></a></li>
        <?php } if($cancFlag){?>
        <li <?php if($_REQUEST['flag']=='canc'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_BOOKING_CANCELLED__?>"><?=t($t_left_nav.'/fields/cancelled');?></a></li>
        <?php } if($ratesFlag){?>
        <li <?php if($_REQUEST['flag']=='rates'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SERVICES_INSURANCE_RATES__?>"><?=t($t_left_nav.'/fields/rates');?></a></li>
        <?php }?>
    </ul> 
    <?php }
    
    
    $search_notiFlag=checkManagementPermission($kAdmin,"__SEARCH_NOTIFICATION__",2);
    $manual_feeFlag=checkManagementPermission($kAdmin,"__MANUAL_FEE__",2);
    $dummy_feeFlag=checkManagementPermission($kAdmin,"__DUMMY_SERVICES__",2);
    $search_user_feeFlag=checkManagementPermission($kAdmin,"__SEARCH_USER_LIST__",2);
    if($search_notiFlag || $manual_feeFlag || $dummy_feeFlag){
    ?>
    <p style="margin: 15px 0 5px;"><?=t($t_left_nav.'/title/general');?></p>
    <ul>
        <?php
         if($search_notiFlag){?>
        <li <?php if($_REQUEST['flag']=='search_noti'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_SEARCH_NOTIFICATION__;?>"><?=t($t_left_nav.'/fields/search_notification');?></a></li> 
        <?php }if($manual_feeFlag){?>
        <li <?php if($_REQUEST['flag']=='manual_fee'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MANUAL_FEE__;?>"><?=t($t_left_nav.'/fields/pricing_manual_fee');?></a></li> 
        <?php }if($dummy_feeFlag){?>
        <li <?php if($_REQUEST['flag']=='dummy_services'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_DUMMY_SERVICES__;?>"><?=t($t_left_nav.'/fields/dummy_services');?></a></li> 
        <?php }/*if($search_user_feeFlag){?>
        <li <?php if($_REQUEST['flag']=='search_user_list'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_SEARCH_USER_LIST__;?>"><?=t($t_left_nav.'/fields/search_user_list');?></a></li> 
        <?php }*/?>
    </ul> 
    <?php }
    
    $visualFlag=checkManagementPermission($kAdmin,"__SALES_FUNNEL_VISUALIZATION__",2);
    $monthlyFlag=checkManagementPermission($kAdmin,"__SALES_MONTHLY__",2);
    $selling_tradeFlag=checkManagementPermission($kAdmin,"__SALES_SELLING_TRADES__",2);
    $searched_tradeFlag=checkManagementPermission($kAdmin,"__SALES_SEARCHED_TRADES__",2);
    
    if($visualFlag || $monthlyFlag || $selling_tradeFlag || $searched_tradeFlag){
    ?>    
    <!-- Operation menu started again -->
    <p style="margin: 15px 0 5px;" ><?=t($t_left_nav.'/title/sales_funnel');?></p>
    <ul>
        <?php
        if($visualFlag){?>
        <li <?php if($_REQUEST['flag']=='visual'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS_VISUALIZATION_URL__;?>"><?=t($t_left_nav.'/fields/visualization');?></a></li>
        <?php } if($monthlyFlag){?>
        <li <?php if($_REQUEST['flag']=='monthly'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS_MOTNTHLY_URL__;?>"><?=t($t_left_nav.'/fields/monthly');?></a></li>
        <?php } if($selling_tradeFlag){?>
        <li <?php if($_REQUEST['flag']=='selling_trade'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS_SELLING_TRADE_URL__;?>"><?=t($t_left_nav.'/fields/selling_trade');?></a></li>
        <?php } if($searched_tradeFlag){?>
        <li <?php if($_REQUEST['flag']=='searched_trade'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS_SEARCHED_TRADE_URL__;?>"><?=t($t_left_nav.'/fields/searched_trades');?></a></li>
        <?php }?>
    </ul>
    <?php }
    
     $listFlag=checkManagementPermission($kAdmin,"__CFS_LOCATION_LIST__",2);
     $mapFlag=checkManagementPermission($kAdmin,"__CFS_LOCATION_MAP__",2);
    if($listFlag || $mapFlag){
    ?>
    <p style="margin: 15px 0 5px;" ><?=t($t_left_nav.'/title/CFSLocation');?></p>
    <ul>
        <?php
        if($listFlag){?>
        <li <?php if($_REQUEST['flag']=='list'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_CFS_LIST_URL__;?>"><?=t($t_left_nav.'/fields/list');?></a></li>
        <?php } if($mapFlag){?>
        <li <?php if($_REQUEST['flag']=='map'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_FORWARDER_ON_MAP_URL__;?>"><?=t($t_left_nav.'/fields/map');?></a></li>
        <?php }?>
    </ul>
    <?php }
    $custfeedbackFlag=checkManagementPermission($kAdmin,"__NPS_SUGGESTIONS__",2);
    $perfFlag=checkManagementPermission($kAdmin,"__PERFORMANCE__",2);
    $detaFlag=checkManagementPermission($kAdmin,"__FEEDBACK_DETAILS__",2);
    if($custfeedbackFlag || $perfFlag || $detaFlag){
    ?>
    <p style="margin: 15px 0 5px;" ><?=t($t_left_nav.'/title/feedback');?></p>
    <ul>
         <?php
        if($custfeedbackFlag){?>
        <li <?php if($_REQUEST['flag']=='custfeedback'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__CUSTOMER_FEEDBACK_URL__;?>"><?=t($t_left_nav.'/fields/cust_feedback');?></a></li>
         <?php } if($perfFlag){?>
        <li <?php if($_REQUEST['flag']=='perf'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__PERFORMANCE_URL__;?>"><?=t($t_left_nav.'/fields/performance');?></a></li>
         <?php } if($detaFlag){?>
        <li <?php if($_REQUEST['flag']=='deta'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_OPERATIONS__DETAILS_URL__;?>"><?=t($t_left_nav.'/fields/details');?></a></li>
         <?php }?>
    </ul>
    <?php }?>
</div>