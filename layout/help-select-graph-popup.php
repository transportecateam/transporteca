<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code));
$t_base = "home/homepage/";

?>

<div id="popup-bg"></div>
    <div id="popup-container">
		<div class="help-select-graph-popup popup">
			<h5><?=t($t_base.'fields/grap_popup_title');?></h5>
			<div class="links">
				<span class="first"><?=t($t_base.'fields/Shipper_door');?></span>
				<span class="third"><?=t($t_base.'fields/origin_warehouse');?> </span>
				<span class="seventh"><?=t($t_base.'fields/destination_warehouse');?></span>
				<span class="ninth"><?=t($t_base.'fields/consigner_door');?></span>
			</div>
			<p align="center" class="shipment"><?=t($t_base.'fields/shipment_flow');?></p>
			<p align="center"><?=t($t_base.'fields/DTD');?></p>
			<p align="center"><?=t($t_base.'fields/DTW');?></p>
			<p align="center"><?=t($t_base.'fields/WTD');?></p>
			<p align="center"><?=t($t_base.'fields/WTW');?></p>
			<br />
			<br />
			<p align="center"><a href="javascript:void(0);" onclick="showHide('Transportation-pop');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
		</div>
	</div>
