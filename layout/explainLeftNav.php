<?php
	$t_left_nav='management/leftNav';
	require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
	validateManagement();
	if(!isset($kExplain))
	{
		$kExplain  =new cExplain();
	}
	if($_SESSION['session_admin_language']>0)
	{
		$iSessionLanguage = $_SESSION['session_admin_language'] ;
	}
	else
	{
		$iSessionLanguage = 1;
		$_SESSION['session_admin_language'] = $iSessionLanguage ;
	}
	
	$howDoesworksLinks = array();
	$howDoesworksLinks = $kExplain->selectPageDetails(false,1,$iSessionLanguage);
	
	$companyLinksAry = array();
	$companyLinksAry = $kExplain->selectPageDetails(false,2,$iSessionLanguage);
	
	$tranportpediaLinksAry = array();
	$tranportpediaLinksAry = $kExplain->selectPageDetails(false,3,$iSessionLanguage);
        
        $explainFlag=checkManagementPermission($kAdmin,"__MANAGE_EXPLAIN_PAGES__",1);
        
        $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails($szLanguageCode);
	
?>
	
<script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce_edit.js"></script>
<!-- <script type="text/javascript" src="https://www.transporteca.com/evp/framework.php?div_id=evp-b3976ec94250a19efd7b977f0811c27b&amp;id=ZXhwbGFpbi1ob3ctdHJhbnNwb3J0ZWNhLXdvcmtzLTEubXA0&amp;v=1360625840&amp;profile=default"></script> -->

<div class="hsbody-2-left account-links">
	<p>
		<span><?=t($t_left_nav.'/title/language');?>:</span>
		<span>
                    <select nam="iLeftNaveLanguage" style="width:75%;" id="iLeftNaveLanguage" onchange="choose_language(this.value,'<?php echo __BASE_URL__."".$_SERVER['REQUEST_URI'] ;?>');">
                        <?php
                        if(!empty($languageArr))
                        {
                            foreach($languageArr as $languageArrs)
                            {?>
                        <option value="<?php echo $languageArrs['id'];?>" <?php echo (($iSessionLanguage==$languageArrs['id'])?'selected':''); ?> ><?=  ucfirst($languageArrs['szName']);?></option>
                            <?php 
                            }
                        }?>
                        
                        
                    </select>
		</span>
	</p>
        <?php if($explainFlag){?>
	<p <?php if($_REQUEST['flag']=='manexp'){?>class="active_list" <?php }?>><a href="<?=__MANANAGEMENT_MANAGE_EXPLAIN_PAGE_URL__;?>" style="text-decoration:none;"><?php if($iSessionLanguage==2){?><?php echo t($t_left_nav.'/title/how_does_it_work_da');?><?php }else{?><?=t($t_left_nav.'/title/how_does_it_work');?><?php }?></a></p>	
	<ul>
		<div id="showLeftNavContent_how_it_work">	
			<?php 
			if(!empty($howDoesworksLinks))
			{
				foreach($howDoesworksLinks as $howDoesworksLinkss)
				{
			?>	
				<li <?php if(isset($_REQUEST['flag']) && ($_REQUEST['flag'] == $howDoesworksLinkss['szLink'])){?>class="active" <?php }?> ><?=($howDoesworksLinkss['iActive']==0?'*':'') ?><a href="<?=__MANAGEMENT_EXPLAIN_URL__.$howDoesworksLinkss['szLink']?>"><?=$howDoesworksLinkss['szPageHeading']?></a></li>
			<?  }
			}
			?>
		</div>	
	</ul>  
	<p <?php if($_REQUEST['flag']=='expcomp'){?>class="active_list" <?php }?>><a href="<?php echo __MANAGEMENT_MANAGE_COMPANY_EXPLIAN_URL__;?>" style="text-decoration:none;"><?php if($iSessionLanguage==2){?><?=utf8_encode(t($t_left_nav.'/title/company_da'));?><?php }else{?><?=t($t_left_nav.'/title/company');?><?php }?></a></p>
	<ul>		
		<div id="showLeftNavContent_company">	
		<?php 
			if(!empty($companyLinksAry))
			{
				foreach($companyLinksAry as $companyLinksArys)
				{
			?>	
				<li <?php if(isset($_REQUEST['flag']) && ($_REQUEST['flag'] == $companyLinksArys['szLink'])){?>class="active" <?php }?> ><?=($companyLinksArys['iActive']==0?'*':'') ?><a href="<?=__MANAGEMENT_COMPANY_EXPLIAN_URL__.$companyLinksArys['szLink']?>"><?=$companyLinksArys['szPageHeading']?></a></li>
			<?  }
			}
		?>
		</div>	
	</ul>  
	<p <?php if($_REQUEST['flag']=='exptrped'){?>class="active_list" <?php }?>><a href="<?php echo __MANAGEMENT_MANAGE_TRANSPORTPEDIA_EXPLIAN_URL__;?>" style="text-decoration:none;"><?php if($iSessionLanguage==2){?><?=utf8_encode(t($t_left_nav.'/title/transportpedia_da'));?><?php }else{?><?=t($t_left_nav.'/title/transportpedia');?><?php }?></a></p>
	<ul>		
		<div id="showLeftNavContent_pedia">	
			<?php 
			if(!empty($tranportpediaLinksAry))
			{
				foreach($tranportpediaLinksAry as $tranportpediaLinksArys)
				{
			?>	
				<li <?php if(isset($_REQUEST['flag']) && ($_REQUEST['flag'] == $tranportpediaLinksArys['szLink'])){?>class="active" <?php }?> ><?=($tranportpediaLinksArys['iActive']==0?'*':'') ?><a href="<?=__MANAGEMENT_TRANSPORTPEDIA_EXPLIAN_URL__.$tranportpediaLinksArys['szLink']?>"><?=$tranportpediaLinksArys['szPageHeading']?></a></li>
			<?  }
			}
			?>
		</div>	
	</ul> 
        <?php }
        
        $searFlag=checkManagementPermission($kAdmin,"__SEARCH_PHRASE__",2);
        $manblgFlag=checkManagementPermission($kAdmin,"__KNOWLEDGE_CENTRE__",2);
        if($searFlag){
        ?>
	<p <?php if($_REQUEST['flag']=='sear'){?>class="active_list" <?php }?>><a href="<?=__MANANAGEMENT_SEARCH_EXPLAIN_PAGE_URL__;?>" style="text-decoration:none;"><?=t($t_left_nav.'/fields/search_phrase');?></a></p>
        <?php } if($manblgFlag){?>
        <p <?php if($_REQUEST['flag']=='manblg'){?> class="active_list" <?php }?>><a href="<?=__MANANAGEMENT_MANAGE_BLOG_PAGE_URL__;?>" style="text-decoration:none;"><?=t($t_left_nav.'/fields/knowledge_centre');?></a></p>
        <?php }
        
        $new_land_pageFlag=checkManagementPermission($kAdmin,"__LANDING_PAGES__",2);
        $seo_pageFlag=checkManagementPermission($kAdmin,"__Articles__",2);
        $histFlag=checkManagementPermission($kAdmin,"__HISTORY__",2);
        $genFlag=checkManagementPermission($kAdmin,"__CUSTOMER_SITE__",2);
        $tnccustFlag=checkManagementPermission($kAdmin,"__CUSTOMER_TC__",2);
        $faq_custFlag=checkManagementPermission($kAdmin,"__CUSTOMER_FAQ__",2);
        $genfwdFlag=checkManagementPermission($kAdmin,"__FORWARDER_SITE__",2);
        $tncfwdFlag=checkManagementPermission($kAdmin,"__FORWARDER_TC__",2);
        $faq_fwdFlag=checkManagementPermission($kAdmin,"__FORWARDER_FAQ__",2);
        $guideFlag=checkManagementPermission($kAdmin,"__FORWARDER_GUIDE__",2);
        
        
        if($guideFlag || $new_land_pageFlag || $seo_pageFlag || $histFlag || $genFlag || $tnccustFlag || $faq_custFlag || $genfwdFlag || $tncfwdFlag || $faq_fwdFlag){?>
        <p><?=t($t_left_nav.'/title/site_text');?></p>
	<ul> 
		<!-- <li <?php if($_REQUEST['flag']=='land_page'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_LANDING_PAGE_EDITOR_URL__;?>"><?=t($t_left_nav.'/fields/landing_page');?></a></li> -->
		<?php
                if($new_land_pageFlag){
                ?>
                <li <?php if($_REQUEST['flag']=='new_land_page'){?>class="active" <?php }?>><a href="<?php echo __MANAGEMENT_NEW_LANDING_PAGE_EDITOR_URL__;?>"><?=t($t_left_nav.'/fields/landing_page');?></a></li>
		<?php } if($seo_pageFlag){?>
                <li <?php if($_REQUEST['flag']=='seo_page'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_SEO_PAGE_EDITOR_URL__;?>"><?=t($t_left_nav.'/fields/seo_page');?></a></li>
		<?php } if($genFlag){?>
                <li <?php if($_REQUEST['flag']=='gen'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_TEXT_EDITOR_URL__;?>"><?=t($t_left_nav.'/fields/customer_site');?></a></li>
		<?php } if($tnccustFlag){?>
                <li <?php if($_REQUEST['flag']=='tnccust'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_TERMS_AND_CONDITION_CUSTOMER_URL__?>"><?=t($t_left_nav.'/fields/Terms_n_condition_customer');?></a></li>
		<?php } if($faq_custFlag){?>
                <li <?php if($_REQUEST['flag']=='faq_cust'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_FAQ_CUSTOMER_URL__;?>"><?=t($t_left_nav.'/fields/Faq_customer');?></a></li>
		<?php } if($genfwdFlag){?>
                <li <?php if($_REQUEST['flag']=='genfwd'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_TEXT_EDITOR_FORWARDER_URL__;?>"><?=t($t_left_nav.'/fields/forwarder_site');?></a></li>
		<?php } if($tncfwdFlag){?>
                <li <?php if($_REQUEST['flag']=='tncfwd'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_TERMS_AND_CONDITION_URL__?>"><?=t($t_left_nav.'/fields/Terms_n_condition_forwarder');?></a></li>
		<?php } if($faq_fwdFlag){?>
                <li <?php if($_REQUEST['flag']=='faq_fwd'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_FAQ_URL__;?>"><?=t($t_left_nav.'/fields/Faq_forwarder');?></a></li>
		<?php } if($guideFlag){?>
                <li <?php if($_REQUEST['flag']=='guide'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_GUIDE_URL__;?>"><?=t($t_left_nav.'/fields/forwarder_guide');?></a></li>
		<?php } if($histFlag){?>
                <li <?php if($_REQUEST['flag']=='hist'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_HISTORY_URL__;?>"><?=t($t_left_nav.'/fields/history');?></a></li>
                <?php }?>
	</ul>
        <?php }?>

</div>