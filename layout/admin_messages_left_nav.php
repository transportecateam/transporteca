<?php
$custmsgFlag=checkManagementPermission($kAdmin,"__MESSAGE_TO_CUSTOMERS__",2);
$formsgFlag=checkManagementPermission($kAdmin,"__MESSAGE_TO_FORWARDERS__",2);
$reviewmsgFlag=checkManagementPermission($kAdmin,"__MESSAGE_SEND_MESSAGES__",2);
$reviewquemsgFlag=checkManagementPermission($kAdmin,"__MESSAGE_QUEUE_MESSAGES__",2);
$reviewemaillogFlag=checkManagementPermission($kAdmin,"__MESSAGE_SYSTEM_MESSAGES__",2);
$emailTempFlag=checkManagementPermission($kAdmin,"__MESSAGE_EMAIL_TEMPLATES__",2);
$rssTempFlag=checkManagementPermission($kAdmin,"__MESSAGE_RSS_TEMPLATES__",2);
$timeFlag=checkManagementPermission($kAdmin,"__MESSAGE_TIMING__",2);
$rssFlag=checkManagementPermission($kAdmin,"__MESSAGE_RSS_FEED__",2);
$editNewsFlag=checkManagementPermission($kAdmin,"__MESSAGE_EDIT_NEWS__",2); 
$searchedTradeFlag = checkManagementPermission($kAdmin,"__USER_TRADE_SEARCH_LIST__",2);

?>
<div class="hsbody-2-left account-links">
<?php $t_base_nav="management/leftNav/";
    
    if($custmsgFlag || $formsgFlag){?>

	<p><?=t($t_base_nav.'title/send_message');?></p>
	<ul>
             <?php if($custmsgFlag){ ?>    
            <li <?php if($_REQUEST['leftmenuflag']=='custmsg'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_CUSTOMER_URL__?>"><?=t($t_base_nav.'title/to_customer');?></a></li>
            <?php } if($formsgFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='formsg'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_MESSAGE_FORWARDER_URL__?>"><?=t($t_base_nav.'title/to_forwarder');?></a></li>
            <?php } ?>
        </ul>
    <?php } if($reviewmsgFlag || $reviewquemsgFlag || $reviewemaillogFlag){?>
	<br/>
	<p><?=t($t_base_nav.'title/review');?></p>
	<ul>
             <?php if($reviewmsgFlag){ ?>
            <li <?php if($_REQUEST['leftmenuflag']=='reviewmsg'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_REVIEW_URL__?>"><?=t($t_base_nav.'title/review_sent_message');?></a></li>
            <?php } if($reviewquemsgFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='reviewquemsg'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_REVIEW_QUEUE_URL__?>"><?=t($t_base_nav.'title/review_queue_message');?></a></li>
            <?php } if($reviewemaillogFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='reviewemaillog'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_REVIEW_EMAIL_LOG_URL__?>"><?=t($t_base_nav.'title/system_messages');?></a></li>
            <?php } ?>
        </ul>
         <?php } if($rssTempFlag || $rssTempFlag || $timeFlag){?>
	<br/>
	<p><?=t($t_base_nav.'title/message_setting');?></p>
	<ul>
             <?php if($emailTempFlag){ ?>
            <li <?php if($_REQUEST['leftmenuflag']=='emailTemp'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_EMAIL_TEMPLATES_URL__?>"><?=t($t_base_nav.'title/emial_template');?></a></li>
            <?php } if($rssTempFlag){?>
            <!--<li <?php if($_REQUEST['leftmenuflag']=='rssTemp'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_RSS_TEMPLATES_URL__?>"><?=t($t_base_nav.'title/rss_template');?></a></li>-->
            <?php } if($timeFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='time'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_MESSAGE_EMAIL_TIMING_URL__?>"><?=t($t_base_nav.'title/timing');?></a></li>
            <?php } ?>
        </ul>
         <?php } if($rssFlag || $editNewsFlag){?>
	<br/>
	<p><?=t($t_base_nav.'title/news');?></p>
	<ul>
             <?php if($rssFlag){ ?>
            <!--<li <?php if($_REQUEST['leftmenuflag']=='rss'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_RSS_URL__?>"><?=t($t_base_nav.'title/rss_feed');?></a></li>-->
            <?php } if($editNewsFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='editNews'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_EDIT_NEWS_URL__?>"><?=t($t_base_nav.'title/edit_news');?></a></li>
            <?php } ?>
	</ul>
        <?php } if($searchedTradeFlag){?>
            <br/>
            <p><?=t($t_base_nav.'title/lists');?></p>
            <ul> 
                <li <?php if($_REQUEST['leftmenuflag']=='search_user_list'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SEARCH_USER_LIST__?>"><?=t($t_base_nav.'title/user_trade_search');?></a></li> 
            </ul>
        <?php }?>
</div>