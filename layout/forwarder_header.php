<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes();
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
if(empty($szMetaTitle))
{
    $szMetaTitle = "Landing page ";
}
$t_base = "Forwarders/";
//echo $_SERVER['HTTP_HOST'];
//echo "helooo";
//die;
$kForwarder = new cForwarder(); 

if(!empty($_SERVER['HTTP_HOST']))
{
    if($kForwarder->isHostNameExist($_SERVER['HTTP_HOST']))
    {
        define ('__BASE_URL__', "http://".$_SERVER['HTTP_HOST']);
        define ('__BASE_URL_SECURE__', "https://".$_SERVER['HTTP_HOST']);
        define_forwarder_constants();
    }
    else
    {
        header("Location:".__MAIN_SITE_HOME_PAGE_URL__);
        exit;	
    }
} 
if(!defined('__BASE_URL__')) 
{
    define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__);
}

$kForwarderContact	= new 	cForwarderContact();

$confirm_key = $_REQUEST['key'];
if(!empty($confirm_key))
{
    $keyAry = explode('__',$confirm_key);
    $szConfirmationKey = $keyAry[0];	
}

if((int)($_SESSION['forwarder_user_id']<=0) && ((int)$szConfirmationKey>0))
{
    $_SESSION['forwarder_booking_confirmation_key'] = $szConfirmationKey;
}

/*if((int)($_SESSION['forwarder_user_id']<=0) && ($_REQUEST['szUpdateKey']!=''))
{
	$_SESSION['update_service_key'] = $_REQUEST['szUpdateKey'];
}
*/
if((int)($_SESSION['forwarder_user_id']<=0) && trim($_SERVER['REQUEST_URI']!='') && $_SERVER['REQUEST_URI']!='/' )
{
	$_SESSION['redirect_uri'] = $_SERVER['REQUEST_URI'];
}
//$_SESSION['forwarder_user_id']=1;
//$_SESSION['forwarder_id']=1;

if((int)$_SESSION['forwarder_user_id']>0)
{	
    $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
    $idForwarder=$kForwarderContact->idForwarder;
    if($kForwarderContact->id <=0)
    {
        header("Location:".__FORWARDER_LOGOUT_URL__);
        exit;
    }

    $kForwarder->load($idForwarder);
    if($kForwarder->szControlPanelUrl != $_SERVER['HTTP_HOST'])
    {
        header("Location:".__FORWARDER_LOGOUT_URL__);
        exit;
    } 
}

$show_abandon_popup = false;
/*
* for removing notification 
* @$display_profile_not_completed_message not defined
* setting hardcoded value
* */
if(!isset ( $display_profile_not_completed_message ) )
{
	$display_profile_not_completed_message=0;
}
//echo $display_profile_not_completed_message ;
//remove lines in between when required for checking forwarder filled proper data or not
if($display_profile_not_completed_message && ((int)$_SESSION['forwarder_admin_id']==0))
{
	if($kForwarder->isProfileInfoComplete($_SESSION['forwarder_user_id']))
	{
		header("Location:".__FORWARDER_INCOMPLETE_PROFILE_URL__);
		exit;
	}
	if(!empty($kForwarderContact->szActivationKey) && !empty($kForwarderContact->dtActivationCodeSent))
	{
		header("Location:".__FORWARDER_NON_VERIFIED_PROFILE_URL__);
		exit;
	}
		
	$idForwarder = $kForwarderContact->idForwarder ;
	/*if($kForwarder->checkForwarderComleteCompanyInfo($idForwarder))
	{
		$show_abandon_popup = true;
		header("Location:".__FORWARDER_DASHBOARD_URL__);
		exit;
	}*/
}
$show_incomplete_company_popup=false;
if($display_profile_not_completed_message_uploadService && ((int)$_SESSION['forwarder_admin_id']==0))
{
    if($kForwarder->checkForwarderComleteCompanyInfoUploadService($idForwarder))
    {
        //print_r($_SERVER['HTTP_REFERER']);
        //$show_incomplete_company_popup = true;
        if($_SERVER['HTTP_REFERER']!='')
        {
                $redirect_url=$_SERVER['HTTP_REFERER'];
        }
        else
        {
                $redirect_url=__FORWARDER_DASHBOARD_URL__;
        }

        $_SESSION['show_incomplete_company_popup']='1';
        header("Location:".$redirect_url);
        exit;
    }
}
///$_SESSION['show_incomplete_company_popup']=0;

/*
 * Getting management var for social media link
*/
$kWhsSearch = new cWHSSearch();
$szFacebookUrl = $kWhsSearch->getManageMentVariableByDescription('__FACEBOOK_URL__');
$szTwitterUrl = $kWhsSearch->getManageMentVariableByDescription('__TWITTER_URL__');
$szLinkedinUrl = $kWhsSearch->getManageMentVariableByDescription('__LINKEDIN_URL__');

$szFacebookUrl= addhttp($szFacebookUrl);
$szTwitterUrl= addhttp($szTwitterUrl);
$szLinkedinUrl= addhttp($szLinkedinUrl);
$bDoNotDisplayHeaderFlag = false;
if($kForwarder->iProfitType==__FORWARDER_PROFIT_TYPE_MARK_UP__) 
{
    $bDoNotDisplayHeaderFlag = true;
} 
$iDisplayQuickQuote = $kForwarder->iDisplayQuickQuote;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <script type="text/javascript">
           if(document.location.protocol == 'https:')
           {
               var __JS_ONLY_SITE_BASE__ = "<?=__BASE_URL_SECURE__?>";
               var __TIME_PER_ROW_CREATE__="0.06";
               var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
           }
           else
           {
               var __JS_ONLY_SITE_BASE__ = "<?=__BASE_URL__?>";
               var __TIME_PER_ROW_CREATE__="0.06";
               var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
           }      
           var __GLOBAL_SMALL_COUNTRY_LIST__ = new Array(); 
            <?php 
                if(!empty($smallCountryAry))
                {
                    foreach($smallCountryAry as $countryIso=>$smallCountryArys)
                    {
                        ?>
                            __GLOBAL_SMALL_COUNTRY_LIST__.push('<?php echo $countryIso; ?>');
                        <?php
                    }
                }
                if((int)$iQuickQuotPage==1)
                {
            ?> 
            var NiceEditorInstance;
            <?php
                }?>
        </script>
       
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <link rel="icon" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon-L.png" />
        <link rel="shortcut icon" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon-L.png" />
        <?php if(!empty($szMetaKeywords)){?><meta name="keywords" content="<?=$szMetaKeywords; unset($szMetaKeywords);?>" /><?}?>
        <?php if(!empty($szMetaDescription)){?><meta name="description" content="<?=$szMetaDescription; unset($szMetaDescription);?>" /><?}?>
        <title id="metatitle"><? if(!empty($szMetaTitle)){ echo $szMetaTitle; unset($szMetaTitle);}?></title>
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.1.8.2.js"></script>
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-ui.min.js"></script> 
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.fileuploadmulti.min.js?v=<?php echo time(); ?>"></script>     
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css?v=<?php echo time(); ?>" />
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_MANAGEMENT_URL__?>/uploadfilemulti.css?v=<?php echo time(); ?>" />
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js?v=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/forwarder.js?v=<?php echo time(); ?>"></script>
        <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" />
        <link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>
        <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
        <?php
        if((int)$iQuickQuotPage==1)
        {?>
        
        <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__."/jQuery.nice.editor.min.js"; ?>"></script>
        <?php }?>
        </head>
	<body class="mainpage">
            <div id="loader_popup_1" class="loader_popup_bg" style="display:none;">
                <div class="popup_loader"></div>				
                <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
            </div>
            <div id="contactPopup" style="display:none;"></div>		
		<div id="ajaxLogin"></div>
            <?php if($bDoNotDisplayHeaderFlag){ ?>
                <div id="header">
                    <a href="javascript:void(0);"> 
                        <img src="<?=__BASE_STORE_IMAGE_URL__?>/transporteca-logo-new.png" alt="Transporteca" title="Transporteca"  class="logo" />
                    </a>
                    <div class="header-links">
                        <a href="javascript:void(0)" onclick="open_contact_popup();" style="float:right;"><?=t($t_base.'forwarder_header/contact');?></a>
                        <?php if((int)$_SESSION['forwarder_user_id']>0){?>
                        <div class="user" onmouseover='$("#show_link").attr("style","display:block;");' onmouseout='$("#show_link").attr("style","display:none;");'><a href="javascript:void(0);" ><div id="forwarder_name"><?=$kForwarderContact->szFirstName?>&nbsp;<?=$kForwarderContact->szLastName?></div></a>
                            <div id="show_link" style="display:none;">
                                <p class="logout_account"><a href="<?=__FORWARDER_LOGOUT_URL__?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/logout-icon.png" alt="Logout" title="Logout" border="0" /></a></p>
                            </div>
                        </div>
                        <?php }else{?>
                        <p class="user">&nbsp;</p>				
                        <?php }?> 
<!--                        <div class="social-icons">
                            <a href="<?php echo $szFacebookUrl;?>" target="_blank" class="facebook-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/facebook-icon.png" alt="facebook" title="facebook" border="0" /></a>
                            <a href="<?php echo $szTwitterUrl;?>" target="_blank" class="twitter-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/twitter-icon.png" alt="Twitter" title="Twitter" border="0" /></a>
                            <a href="<?php echo $szLinkedinUrl;?>" target="_blank" class="linkedin-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/linkedin-icon.png" alt="Linkedin" title="Linkedin" border="0" /></a>
                            <a href="<?=__BASE_URL__.'/rss.php'?>" class="rss-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/rss-icon.png" alt="RSS" title="RSS" border="0" /></a>
                        </div>-->
                    </div>
                </div>
                <div id="navigation" style="height: 0px;">&nbsp;</div>
            <?php } else { ?>
		<div id="header">
                    <a href="<?=__FORWARDER_HOME_PAGE_URL__?>"> 
                        <img src="<?=__BASE_STORE_IMAGE_URL__?>/transporteca-logo-new.png" alt="Transporteca" title="Transporteca"  class="logo" />
                    </a>
                    <div class="header-links">
                        <?php 
                        if((int)$_SESSION['forwarder_user_id']>0)
                        { 
                            $forwarderProfileContactRole= $kForwarder->adminEachForwarder($_SESSION['forwarder_user_id']);
                            if($iDisplayQuickQuote==1 && ((int)$_SESSION['forwarder_admin_id']>0 || ($forwarderProfileContactRole == __ADMINISTRATOR_PROFILE_ID__ || $forwarderProfileContactRole == __PRICING_PROFILE_ID__)))
                            {
                                ?>
                                <a class="quick-quote" href="<?php echo __FORWARDER_QUICK_QUOTE_URL__;?>" style="float:right;"><?=t($t_base.'forwarder_header/quick_quote');?></a>
                                <?php
                            }
                            ?> 
                            <div class="user" onmouseover='$("#show_link").attr("style","display:block;");' onmouseout='$("#show_link").attr("style","display:none;");'><a href="<?=__FORWARDER_MY_ACCOUNT_URL__?>" ><div id="forwarder_name"><?=$kForwarderContact->szFirstName?>&nbsp;<?=$kForwarderContact->szLastName?></div></a>
                                <div id="show_link" style="display:none;">
                                    <p class="logout_account"><a href="<?=__FORWARDER_LOGOUT_URL__?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/logout-icon.png" alt="Logout" title="Logout" border="0" /></a></p>
                                    <p class="user_account"><a href="<?=__FORWARDER_MY_ACCOUNT_URL__?>"><?=t($t_base.'forwarder_header/myAccount');?></a></p>
                                </div>
                            </div> 
                        <?php   
                        }else { ?>
                            <a href="javascript:void(0)" onclick="open_contact_popup();" style="float:right;"><?=t($t_base.'forwarder_header/contact');?></a>
                            <p class="signin-user"><a href="javascript:void(0)" onclick="loginPopUpForwarder();" id="login"><?=t($t_base.'forwarder_header/sign_in');?></a></p>				
                        
                            
<!--                        <div class="social-icons">
                            <a href="<?php echo $szFacebookUrl;?>" target="_blank" class="facebook-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/facebook-icon.png" alt="facebook" title="facebook" border="0" /></a>
                            <a href="<?php echo $szTwitterUrl;?>" target="_blank" class="twitter-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/twitter-icon.png" alt="Twitter" title="Twitter" border="0" /></a>
                            <a href="<?php echo $szLinkedinUrl;?>" target="_blank" class="linkedin-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/linkedin-icon.png" alt="Linkedin" title="Linkedin" border="0" /></a>
                            <a href="<?=__BASE_URL__.'/rss.php'?>" class="rss-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/rss-icon.png" alt="RSS" title="RSS" border="0" /></a>
                        </div>-->
                        <?php } ?>
                    </div>
		</div> 
		<?php
			if($show_abandon_popup)
			{
                            $style_abandon_popup = 'block';
                            header("Location:".__FORWARDER_INCOMPLETE_PROFILE_URL__);
                            exit;
                            ?>
                                    <div id="leave_page_div" style="display:<?=$style_abandon_popup?>">	
                                    <?php	echo incomplete_comapny_details_popup($t_base);	?>
                            </div>	
                            <?php
                            die;
			}
			else
			{
                            $style_abandon_popup = 'none';
			}
			//print_r($_SESSION['show_incomplete_company_popup']);
			if($_SESSION['show_incomplete_company_popup']=='1')
			{
                            //echo "hello";
                            //die;
                            $style_abandon_upload_service_popup = 'block';
			}
			else
			{
                            $_SESSION['show_incomplete_company_popup']=0;
                            $style_abandon_upload_service_popup = 'none';
			}
			
			if(isset($_SESSION['forwarder_user_id']))
			{
                            $_SESSION['show_incomplete_company_popup']=0;
                            ?>
                            <div id="leave_page_div_upload_service" style="display:<?=$style_abandon_upload_service_popup?>">	
                                    <?php	echo incomplete_comapny_details_popup_upload_service($t_base);	?>
                            </div>		
                            <?php
			}
			
			if(isset($_SESSION['forwarder_user_id']))
			{
				?>
				<div id="leave_page_div" style="display:<?=$style_abandon_popup?>">	
					<?php	echo incomplete_comapny_details_popup($t_base);	?>
				</div>		
				<?php
			}
			if(((int)$_SESSION['display_currency_change_popup']>0) && ((int)$_SESSION['forwarder_user_id']>0))
			{
				$t_base_popup = "Dashboard/";
				?>
				<div id="leave_page_div_prefernces" style="display:block;">	
				<?php
				$div_id = 'leave_page_div_prefernces';
				echo forwarder_currency_change_notifivcation_dashboard($idForwarder,$t_base_popup,$div_id);
				//die;
				?>
				</div>
				<?php
				$_SESSION['display_currency_change_popup'] = '';
				unset($_SESSION['display_currency_change_popup']);
			}
			
			//this code block is used for selecting headings 
			$dash_board_header_ary = array('dashboard');
                        $pending_quote_header_ary = array('quote_pend','quote_past');
                        
			$pricing_services_header_ary = array('obo_lcl','obo_cc','obo_haul','cfs_loc','haulage','customClearance','lclservice','try_it','non_acceptance','cfs_loc_bulk','inst','pric','submit','awat_app','quote_pend','quote_past','ser_ofr','bulkairservice','obo_air','bulk_air_cfs','air_cfs');
			$my_company_header_ary = array('ua','pref','ci','bd','cmi','trapc','et');
			$feedback_header_ary = array('feedback');
			$booking_header_ary = array('book');
			$billing_header_ary = array('bill');
			$welcome_header_ary = array('well');
			$my_account_header_ary = array('set','priv','del');
			$header_flag = (!empty($_REQUEST['flag']))?sanitize_all_html_input(trim($_REQUEST['flag'])):'';
			
			/*
			//for designing the big active change to active if want change in future to class="active" to class="big active"
			
			* DEFINING VARIABLES
			* */
			 $dash_board_header_select       = '';
			 $pricing_services_header_select = 'class="big"';
			 $my_company_header_select		 = '';
			 $feedback_header_select		 = '';
			 $booking_header_select			 = '';
			 
			if(!empty($dash_board_header_ary) && in_array($header_flag,$dash_board_header_ary))
			{
                            $login_flag = true;
                            $dash_board_header_select = 'class="active"';
			}
                        else if(!empty($pending_quote_header_ary) && in_array($header_flag,$pending_quote_header_ary))
			{
                            $login_flag = true;
                            $task_pending_tray_select = 'class="active"';
			} 
			else if(!empty($pricing_services_header_ary) && in_array($header_flag,$pricing_services_header_ary))
			{
                            $login_flag = true;
                            $pricing_services_header_select = 'class="big active"';
			}
			else if(!empty($my_company_header_ary) && in_array($header_flag,$my_company_header_ary))
			{
                            $login_flag = true;
                            $my_company_header_select = 'class="active"';
			}
			else if(!empty($feedback_header_ary) && in_array($header_flag,$feedback_header_ary))
			{
                            $login_flag = true;
                            $feedback_header_select = 'class="active"';
			}	
			else if(!empty($booking_header_ary) && in_array($header_flag,$booking_header_ary))
			{
                            $login_flag = true;
                            $booking_header_select = 'class="active"';
			}
			else if(!empty($billing_header_ary) && in_array($header_flag,$billing_header_ary))
			{
                            $login_flag = true;
                            $billing_header_select = 'class="active"';
			}
			else if(!empty($welcome_header_ary) && in_array($header_flag,$welcome_header_ary))
			{
                            $login_flag = true;
			}
			else if(!empty($welcome_header_ary) && in_array($header_flag,$welcome_header_ary))
			{
                            $login_flag = true;
			}
			else if(!empty($my_account_header_ary) && in_array($header_flag,$my_account_header_ary))
			{
                            $login_flag = true;
			}
			
			//default values for style attribute
			$style_company='style="margin-right:0;"';

			//default values for href of menu tabs 
			if(isset($kForwarderContact->idForwarderContactRole) && $kForwarderContact->idForwarderContactRole == __ADMINISTRATOR_PROFILE_ID__)
			{
				$pricing_services_page_url = __FORWARDER_PRICING_SERVICING_INTRODUCTION_URL__ ;
			}
			else
			{
				$pricing_services_page_url ='/LCLServicesBulk/';
			}		
				
			$company_page_url = __FORWARDER_COMPANY_PAGE_INTRODUCTION_URL__ ;			
			//$pricing_services_page_url =__FORWARDER_SERVICE_OFFERING_URL__ ;
			//$pricing_services_page_url = '/LCLServicesBulk/' ;
			$feedback_page_url = __FORWARDER_FEEDBACK_URL__ ;
			$booking_page_url = __FORWARDER_BOOKING_URL__ ;
			$billing_page_url = __FORWARDER_BILLING_URL__ ;
                        $task_pending_tray_url = __FORWARDER_PENDING_QUOTES_URL__ ;
			
			if($login_flag)
			{
				checkAuthForwarder();
				
				//checking page permission 
				if($kForwarderContact->idForwarderContactRole == __PRICING_PROFILE_ID__)
				{
					// for the pricing and srvices profiles we are showing dashboard and Pricing & Service Tabs
					$not_allowed_page_array = array_merge($my_company_header_ary);
					
					if((!empty($not_allowed_page_array) && in_array($header_flag,$not_allowed_page_array)))
					{
						header("Location:".__FORWARDER_OBO_LCL_SERVICES_URL__);
						die;
					}
					
					//if a user with pricing & services profiles logs in we make company,booking,billing and feedback tabs disable for him.
					$style_company = 'style="color:grey;margin-right:0;"';
					$company_page_url = 'javascript:void(0);';
				}
				else if($kForwarderContact->idForwarderContactRole == __BILLING_PROFILE_ID__)
				{
					// for the Booking & Billing profiles we are showing dashboard and Booking and Billing Tabs
					$not_allowed_page_array = array_merge($my_company_header_ary,$pricing_services_header_ary);
					
					if((!empty($not_allowed_page_array) && in_array($header_flag,$not_allowed_page_array)))
					{
						header("Location:".$billing_page_url);
						die;
					}					
					$style_company = 'style="color:grey;margin-right:0;"';
					$style_pricing_services = 'style="color:grey;"';					
					
					$pricing_services_page_url = 'javascript:void(0);';
					$company_page_url = 'javascript:void(0);';
				}
			}
		
		?>
		<div id="navigation">
		<ul>
		<?php
			if(!$kForwarder->isCurrencyLogExist($idForwarder))
			{
		?>
			<li <?=$dash_board_header_select?>>
			<a href="<?=__FORWARDER_DASHBOARD_URL__?>" ><span><?=t($t_base.'forwarder_header/dashboard');?></span><br/><?=t($t_base.'forwarder_header/quick_overview');?></a>
			</li>
		<?php
			}else{
		?>
			<li  <?=$dash_board_header_select?>>
			<a href="javascript:void(0);" onclick="open_currency_change_popup('<?=$idForwarder?>')" ><span><?=t($t_base.'forwarder_header/dashboard');?></span><br/><?=t($t_base.'forwarder_header/quick_overview');?></a>
			</li>
		<?php }?>
                        <li <?php echo $task_pending_tray_select ?>>
                            <a href="<?php echo $task_pending_tray_url ?>" <?=$style_task_pending_tray; ?>><span><?=t($t_base.'forwarder_header/pending_tray');?></span><br/><?=t($t_base.'forwarder_header/quote_and_booking');?></a>
                        </li>
			<li <?=$pricing_services_header_select?>>
                            <a href="<?=$pricing_services_page_url?>" <?=$style_pricing_services?>><span><?=t($t_base.'forwarder_header/automatic_pricing');?></span><br/><?=t($t_base.'forwarder_header/lcl_courier_service');?></a>
			</li>
			<li <?=$booking_header_select?>>
			<a href="<?=$booking_page_url?>"  <?=$style_booking_billing?>><span><?=t($t_base.'forwarder_header/bookings');?></span><br /><?=t($t_base.'forwarder_header/searc_review');?></a>
			</li>
			<li <?=$feedback_header_select?>>
			<a href="<?=$feedback_page_url?>" <?=$style_feed_back?> ><span><?=t($t_base.'forwarder_header/feedback');?></span><br/><?=t($t_base.'forwarder_header/customer_voice');?></a>
			</li>
			<li <?=$billing_header_select?>>
			<a href="<?=$billing_page_url?>" <?=$style_booking_billing?>><span><?=t($t_base.'forwarder_header/billing');?></span><br/><?=t($t_base.'forwarder_header/documentation');?></a>
			</li>
			<li <?=$style_company?> <?=$my_company_header_select?>>
			<a href="<?=$company_page_url?>" ><span><?=t($t_base.'forwarder_header/comapny');?></span><br/><?=t($t_base.'forwarder_header/preferences_users');?></a>
			</li>
		</ul>
		</div>
		<div id="top_messages"> 	
	<?php 		
			if(isset($_SESSION['forwarder_user_id']) && (int)$_SESSION['forwarder_user_id']>0 && !empty($_SESSION['forwarderHeaderNoticationFlagsAry']))
			{  
                            //require_once(__APP_PATH_LAYOUT__."/showForwarderMessageDetails.php");
                            showForwarderDetails(true);
                            //displayForwarderHeaderNotificaion(false);
			}?>
                </div> 	
            <?php } ?>
			<?php
			//unsetting values
			$dash_board_header_ary=array();
			$pricing_services_header_ary=array();
			$my_company_header_ary=array();
			$feedback_header_ary=array();
			$booking_header_ary=array();
			$header_flag=array();
			
		$t_base_user = "Users/AccountPage/";
		
		if($_SESSION['valid_confirmation']==1)
		{
		    if((int)$_SESSION['forwarder_user_id']<=0) 
		    {
		    	$_SESSION['verified_email_user']=$_SESSION['forwarder_user_id'];
		    	?>		
		    	<script type="text/javascript">loginPopUpForwarder();</script>
		    <?php
			}
			else
			{
				echo $t_base_user;
		?>
			<div id="popup-bg"></div>
			<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<h5><?=t($t_base_user.'messages/forwarder_verified_popup_header')?></h5>
			<p><?=t($t_base_user.'messages/forwarder_verified_popup_message');?></p><br/>
			<p align="center"><a href="<?=$redirect_url?>" class="button1"><span><?=t($t_base_user.'fields/continue');?></span></a></p>
			</div>
			</div>
			<?php  
				$_SESSION['valid_confirmation'] = '';
				unset($_SESSION['valid_confirmation']);	
			}
			
			}else if($_SESSION['valid_confirmation']==2){?>
			<div id="popup-bg"></div>
			<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<h5><?=t($t_base_user.'messages/verification')?></h5>
			<p><?=t($t_base_user.'messages/invalid_con_key');?></p>
			<br/>
			<p align="center"><a href="<?=$redirect_url?>" class="button1"><span><?=t($t_base_user.'fields/close');?></span></a></p>
			</div>
			</div>
			<br  />
			<?php 
			$_SESSION['valid_confirmation'] = '';
			unset($_SESSION['valid_confirmation']);
}
if(($_SERVER['PHP_SELF']=='/myProfile.php' || $_SERVER['PHP_SELF']=='/deleteProfile.php') && ((int)$_SESSION['forwarder_admin_id']>0))
{
    echo "<br /><br /><h3>It seems that you have logged in as Administrator, so you can't access this page.</h3><br/> <br />";
    require_once( __APP_PATH_LAYOUT__ . "/forwarder_footer.php" );
    die;
} 

if($bDoNotDisplayHeaderFlag)
{
    if((int)$kForwarderContact->iPasswordUpdated==1)
    {	 
        checkForgotPasswordUpdated();
    }
}  

?>	