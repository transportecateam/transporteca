<?php 
//if(!ob_start('ob_gzhandler')) ob_start('sanitize_output');
ob_start();
//ob_start('sanitize_output'); 
session_start();   
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );  
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" );
//include_classes();
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );  
 
$display_abandon_message=false; 
if(!empty($_COOKIE['__USER_LOGIN_COOKIE__']) && (int)$_SESSION['user_id']<=0)
{  
    $kUserNew1 = new cUser();
    $kUserNew1->cookieUserLogin($_COOKIE['__USER_LOGIN_COOKIE__']);
} 
$szLanguage = sanitize_all_html_input($_REQUEST['lang']);
  
if($page_404==1)
{
    $szLanguage = $_SESSION['transporteca_language_en']; 
}
if(empty($szLanguage))
{
    if(substr($_SERVER['REQUEST_URI'], 0, 4) === '/da/' || substr($_SERVER['REQUEST_URI'], 0, 4) === '/da')
    {
        $szLanguage = 'danish';
    }
    else
    { 
        $szLanguage = 'english';
    }
} 
    
if(!empty($szLanguage) && $szLanguage=='danish')
{
    define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__."/".__TRANSPORTECA_DANISH_URL__);
    define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__);
    
    define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define_constants($szLanguage); 

    $_SESSION['transporteca_language_en'] = $szLanguage ;
}
else if(empty($szLanguage) || $szLanguage=='english')
{
    $szLanguage = 'english';
    //define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__."/".__TRANSPORTECA_ENGLISH_URL__);
    //define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_ENGLISH_URL__); 
   // define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__."/".__TRANSPORTECA_ENGLISH_URL__);
    
    define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
    define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define_constants($szLanguage);

    $_SESSION['transporteca_language_en'] = $szLanguage;  
}

$lang_code = __LANGUAGE__;
I18n::add_language(strtolower($lang_code));
 
$iLanguage = getLanguageId(); 
   
$szServerURI = trim($_SERVER['REQUEST_URI']);
$possibleHomePageAry = array('/','/da/','/da');
 
if(!empty($_COOKIE['__USER_COUNTRY_NAME___']))
{
    $szUserCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ;
}
else
{  
    $crawler = crawlerDetect($_SERVER['HTTP_USER_AGENT']);
    if(empty($crawler))
    {
        $userIpDetailsAry = array();
        $userIpDetailsAry = getCountryCodeByIPAddress();
        $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;
        $userIpDetailsStr = serialize($userIpDetailsAry);
        
        $visitorsRedirectLogs = array(); 
        if(!empty($userIpDetailsAry))
        { 
            $visitorsRedirectLogs['szReferer'] = $_SERVER['HTTP_REFERER'];
            $visitorsRedirectLogs['szIPAddress'] = $userIpDetailsAry['szIPAddress'];
            $visitorsRedirectLogs['idUser'] = $_SESSION['user_id'];
            $visitorsRedirectLogs['iFoudBYAPI'] = $userIpDetailsAry['iFoudBYAPI'];
            $visitorsRedirectLogs['szApiResponseData'] = print_R($userIpDetailsAry,true);
            $visitorsRedirectLogs['szCountryCode'] = $szUserCountryName;
        }  
        //setting cookien variable
        if($_COOKIE['__USER_COUNTRY_NAME___']!=$szUserCountryName)
        {
            setCookie('__USER_COUNTRY_NAME___', $szUserCountryName ,time()+3600, '', $_SERVER['HTTP_HOST']);
        }  
        if($_COOKIE['__USER_IP_DETAILS___']!=$userIpDetailsStr)
        {
            setCookie('__USER_IP_DETAILS___', $userIpDetailsStr ,time()+3600, '', $_SERVER['HTTP_HOST']);  
        }  
    }
} 
if((int)$_SESSION['LANGUAGE_CHANGED_BY_USER']==0 && $iLanguage!=__LANGUAGE_ID_DANISH__ && in_array($szServerURI,$possibleHomePageAry))
{ 
    if(trim($szUserCountryName)=='DK')
    {
        if(!empty($visitorsRedirectLogs))
        {  
            $visitorsRedirectLogs['isRedirected'] = 1;
            $kUserNew1 = new cUser();
            $kUserNew1->addVisitorsRedirectLogs($visitorsRedirectLogs);
        }
        
        if(substr($_SERVER['REQUEST_URI'], 0, 4) === '/da/')
        {
            $redir_da_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$_SERVER['REQUEST_URI'];
        }
        else
        {
            $redir_da_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__."".$_SERVER['REQUEST_URI'];
        } 
        $_SESSION['LANGUAGE_CHANGED_BY_USER'] = 1;
        header("HTTP/1.1 301 Moved Permanently");
        ob_end_clean();
        header("Location:".$redir_da_url);
        die;
    }
    else
    {
        if(!empty($visitorsRedirectLogs))
        {
            $visitorsRedirectLogs['isRedirected'] = 0;
            $kUserNew1 = new cUser();
            $kUserNew1->addVisitorsRedirectLogs($visitorsRedirectLogs);
        }
    }
} 

if(empty($szMetaTitle))
{
    $szMetaTitle = "Landing page ";
} 
$t_base = "home/";
$kUser= new  cUser();
if((int)$_SESSION['user_id']>0)
{
    $kUser->getUserDetails($_SESSION['user_id']); 
    if($kUser->iActive!=1)
    { 
        ob_end_clean();
        header("Location:".__USER_LOGOUT_URL__);
        die;
    }
    
    $kUserNew = new cUser();
    $kUserNew->loadCurrency($kUser->szCurrency);

    if(($kUserNew->iBooking<=0) && ($_REQUEST['flag']=='cb')) // User is in booking steps and currency is not active.
    {
        //echo "<div id='Transporteca_popup'>";
        //echo displayCurrencyChangePopup($kUser->szCurrency);
        //echo "</div>";
    }
} 
$landing_page_url = __LANDING_PAGE_URL__ ;

$kBooking = new cBooking();
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

/**
* THIS BELOW SECTON IS FOR CONTENT -- Explain pages
* 
*/ 
	
if( isset($_REQUEST['flag']) && ($_REQUEST['flag'] == 'ep') )
{
    if(isset($_REQUEST['menuflag']) && $_REQUEST['menuflag']=='ind')
    { 
        $kExplain = new cExplain;

        $szLink=sanitize_all_html_input(trim($_REQUEST['szLink']));

        $levelExplainDataArr=$kExplain->selectExplainId($szLink,$mainTab);

        $szUrl = sanitize_all_html_input($_REQUEST['szUrl']);
        $content = $kExplain->getExplainContentByUrl($szUrl,$levelExplainDataArr['id']);
        if($content != array())
        {
            $idExplain = $content['id'];
            $szMetaTitle = $content[0]['szMetaTitle'];
            $szMetaKeywords = $content[0]['szMetaKeyWord'];
            $szMetaDescription = $content[0]['szMetaDescription'];
        }
    } 
}

/**
*  SECTION ENDS HERE
*/
  
  /**
  * THIS BELOW SECTON IS FOR  -- Blog pages
  * 
  */


if( isset($_REQUEST['menuflag']) && ($_REQUEST['menuflag'] == 'blg') )
{
    if(isset($_REQUEST['flag']) && $_REQUEST['flag']!='')
    {
        $_REQUEST['flag']  = str_replace('/','',$_REQUEST['flag']);
        $kExplain = new cExplain;
        $link = $kExplain->selectBlogLink($_REQUEST['flag'],$iLanguage);
        if($link != array())
        {
            $szMetaTitle = ($link['szMetaTitle']?$link['szMetaTitle']:'');
            $szMetaKeywords = ($link['szMetaKeyWord']?$link['szMetaKeyWord']:'');
            $szMetaDescription = ($link['szMetaDescription']?$link['szMetaDescription']:'');	
        }
    }
    else
    {
        $kExplain = new cExplain;
        $link = $kExplain->selectBlogLink(false,$iLanguage);
        if($link != array())
        {
            //$szMetaTitle = ($link['szMetaTitle']?$link['szMetaTitle']:'');
            //$szMetaKeywords = ($link['szMetaKeyWord']?$link['szMetaKeyWord']:'');
            //$szMetaDescription = ($link['szMetaDescription']?$link['szMetaDescription']:'');
            $_REQUEST['flag'] = $link['szLink'];
            header('Location:'.__BLOG_PAGE_URL__.$_REQUEST['flag'].'/');
            die;
        }
    } 
} 

$confirm_key = $_REQUEST['myBookingkey'];
if(!empty($confirm_key))
{
    $keyAry = explode('__',$confirm_key);
    $szConfirmationBookingKey = $keyAry[0];	
}

if((int)($_SESSION['user_id']<=0) && ((int)$szConfirmationBookingKey>0))
{
    $_SESSION['customer_booking_confirmation_key'] = $szConfirmationBookingKey;
}

/*
 * Getting management var for social media link
 */

$kWhsSearch = new cWHSSearch();
$kExplain = new cExplain;
$kSEO = new cSEO();
$iLanguage = getLanguageId(); 
 
$leftmenuLinksAry = array();
$howDoesworksLinks = array();
$companyLinksAry = array();
$tranportpediaLinksAry = array(); 
$leftmenuLinksAry = $kExplain->selectPageDetailsUserEndByType($iLanguage); 

$howDoesworksLinks = $leftmenuLinksAry[__HOW_IT_WORKS__];
$companyLinksAry = $leftmenuLinksAry[__COMPANY__];
$tranportpediaLinksAry = $leftmenuLinksAry[__TRANSTPORTPEDIA__]; 

$seoPageHeaderAry = array();
$seoPageHeaderAry = $kSEO->getAllPublishedSeoPages($iLanguage,true);

$blogPageHeaderAry = array();
$blogPageHeaderAry = $kExplain->selectBlogDetailsActive($iLanguage);
  
if($iLanguage==__LANGUAGE_ID_DANISH__)
{
    $szLang = 'da';
    $szOgLang = 'da_DA';
}
else
{
    $szLang = 'en';
    $szOgLang = 'en_GB';
} 

$kWhsSearch = new cWHSSearch(); 
$kConfig = new cConfig();

//$smallCountryAry = array();
//$smallCountryAry = $kConfig->getAllCountryForMaxmindScript(true);
$kWhsSearch = new cWHSSearch();  
$arrDescriptions = array("'__FACEBOOK_URL__'","'__TWITTER_URL__'","'__LINKEDIN_URL__'","'__YOUTUBE_URL__'","'__GOOGLE_PLUS_URL__'","'__GOOGLE_MAP_V3_API_KEY__'","'__TRANSPORTECA_CUSTOMER_CARE_DANISH__'","'__TRANSPORTECA_CUSTOMER_CARE__'","'__TRANSPORTECA_CUSTOMER_CARE__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWHSSearch->getBulkManageMentVariableByDescription($arrDescriptions);      
 
$szFacebookUrl = $bulkManagemenrVarAry['__FACEBOOK_URL__'];
$szTwitterUrl = $bulkManagemenrVarAry['__TWITTER_URL__'];
$szLinkedinUrl = $bulkManagemenrVarAry['__LINKEDIN_URL__'];
$szYoutubeUrl = $bulkManagemenrVarAry['__YOUTUBE_URL__'];
$szGooglePlusAuthor = $bulkManagemenrVarAry['__GOOGLE_PLUS_URL__'];
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];

if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
{ 
    $szCustomerCareNumer = $bulkManagemenrVarAry['__TRANSPORTECA_CUSTOMER_CARE_DANISH__']; 
} 
else 
{
    $szCustomerCareNumer = $bulkManagemenrVarAry['__TRANSPORTECA_CUSTOMER_CARE__']; 
}

$iPageNumber = '';
if($_SERVER['PHP_SELF']=='/newService.php')
{
    $iPageNumber = 1;
} 
else if ($_SERVER['PHP_SELF']=='/booking-confirmation-new.php')
{  
    $iPageNumber = 2;
} else if ($_SERVER['PHP_SELF']=='/booking-reciept-new.php'){  
    $iPageNumber = 3;
} else { 
    $iPageNumber = 4;
}  
?>   
<!doctype html>
<html lang="<?php echo $szLang; ?>">
	<head>
	<script type="text/javascript" defer="1">if("https:"==document.location.protocol)var __JS_ONLY_SITE_BASE__="http://dev.transporteca.com",__JS_ONLY_SITE_BASE_IMAGE__="http://dev.transporteca.com";else var __JS_ONLY_SITE_BASE__="http://dev.transporteca.com",__JS_ONLY_SITE_BASE_IMAGE__="http://dev.transporteca.com";var __GLOBAL_SMALL_COUNTRY_LIST__=new Array;__GLOBAL_SMALL_COUNTRY_LIST__.push("VA"),__GLOBAL_SMALL_COUNTRY_LIST__.push("HK"),__GLOBAL_SMALL_COUNTRY_LIST__.push("SG"),__GLOBAL_SMALL_COUNTRY_LIST__.push("ZW");</script>  
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title id="metatitle"><?php if(!empty($szMetaTitle)){ echo $szMetaTitle; }?></title> 
        <?php if(!empty($szMetaDescription)){?>
             <meta name="description" content="<?=$szMetaDescription;?>" />
        <?php } ?> 
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta property="og:locale" content="<?php echo $szOgLang; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Transporteca" />

        <meta property="og:title" content="<?php echo $szMetaTitle; ?>" />
        <meta property="og:description" content="<?php echo $szMetaDescription; ?>" />
	    
        <?php  if($_REQUEST['menuflag']=='agg'){ ?>		
            <link rel="publisher" href="<?php echo $szOpenGraphAuthor; ?>"/> 
            <meta property="og:url" content="<?php echo $szAggregatePageUrl; ?>" /> 
            <meta property="og:image" content="<?php echo $szOpenGraphImagePath; ?>" />
        <?php  } else { ?> 
            <link rel="publisher" href="<?php echo $szGooglePlusAuthor; ?>"/>
        <?php } ?> 
        <?php if(!empty($szMetaKeywords) && $_REQUEST['menuflag']!='agg'){ ?>
                <meta name="keywords" content="<?=$szMetaKeywords;?>" />
        <?php }?> 
        <?php
            if(__ENVIRONMENT__ == "LIVE")
            {
                define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
                $protocol = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
                if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.' || strtolower($protocol)=='http://') 
                {
                    if(substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.')
                    {
                            $www_str = "www.";
                    } 
                    header("HTTP/1.1 301 Moved Permanently");
                    header('Location: https://'.$www_str.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI']);
                    exit;
                }
            }
            else if(__ENVIRONMENT__ == "DEV_LIVE")
            {
                define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
                $allowed_ip_ary = array('122.160.151.237','90.184.235.58','127.0.0.1');
                $server_ip_addre = trim($_SERVER['REMOTE_ADDR']);
                if(!empty($allowed_ip_ary) && !in_array($server_ip_addre,$allowed_ip_ary))
                {
                    //header("Location:".__BASE_URL__.'/holding');
                    //die;
                }
            }
            else
            {
                define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
            } 
	    	
            if(__ENVIRONMENT__ == "LIVE")
            {
	    ?>  
                <!-- CSS FILES  --> 
                <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'> 
                <link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/transporteca.min.css?v=3" rel="stylesheet" />  

                <!-- JS FILES  -->	
                <script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>
                <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.number.js'></script>
                <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js'></script> 
                <!-- JS FILES  -->	 	
                <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
                <script src="<?php echo __BASE_STORE_SECURE_JS_URL__?>/jquery.bxslider.min.js"></script>
 
                <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
                <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
                <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
                <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>  			
                <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
                <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
                <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/modernizr.custom.79639.js"></script> 
                <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js"></script>   
		<?php } else { ?>
                <!-- CSS FILES  --> 
    		<!--<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>-->
    		<!--<link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/transporteca.min.css" rel="stylesheet" />-->  
                <script>var cb_1=function(){var e=document.createElement("link");e.rel="stylesheet",e.href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300";var t=document.getElementsByTagName("head")[0];t.parentNode.insertBefore(e,t)},raf=requestAnimationFrame||mozRequestAnimationFrame||webkitRequestAnimationFrame||msRequestAnimationFrame;raf?raf(cb_1):window.addEventListener("load",cb_1);var cb=function(){var e=document.createElement("link");e.rel="stylesheet",e.href='<?php echo __BASE_STORE_SECURE_CSS_URL__."/transporteca.min.css"; ?>';var t=document.getElementsByTagName("head")[0];t.parentNode.insertBefore(e,t)},raf=requestAnimationFrame||mozRequestAnimationFrame||webkitRequestAnimationFrame||msRequestAnimationFrame;raf?raf(cb):window.addEventListener("load",cb);</script>

                <?php
                    if($iFromLandingPage==1)
                    {
                        //Include css file here which on for landing page 
                        ?>
                
                        <?php
                    }
                    else
                    {
                        ?>
                        <link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/bootstrap.min.css' rel='stylesheet' type='text/css'>
                        <link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/font-awesome.min.css' rel='stylesheet' type='text/css'>
                        <link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" />
                        <?php
                    }
                ?>
                <?php if($iMyAccountPage==1){?>
                    <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/style.css" />
                <?php }?> 
    		    		
    		<!-- JS FILES  -->	
                <!--<script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>-->
                <!--<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.number.js'></script>-->
                <!--<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js'></script>--> 
                <!--<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>-->   
                <!--<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>-->  	 
                <!--<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js"></script>-->   
                <!--<script src="<?php echo __BASE_STORE_SECURE_JS_URL__?>/jquery.bxslider.min.js"></script>-->
                
                <!-- JS FILES  -->
                <script src="<?php echo __BASE_STORE_JS_URL__;?>/transporteca.min.js" type="text/javascript"></script>  
                <script type="text/javascript" defer="1">
                    !function(){var e=document.createElement("script");e.async=!0,e.type="text/javascript",e.src='<?php echo __BASE_STORE_JS_URL__."/transporteca-internal.min.js";?>';var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}();
                </script>
		<?php } ?>	 	
            <link rel="icon" id="favicon_icon_1" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />  
            <link rel="shortcut icon" id="favicon_icon_2" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" /> 
            <?php if(__ENVIRONMENT__ =='DEV_LIVE' || __ENVIRONMENT__ =='LIVE') { ?> 
<!--                <script type="text/javascript" defer="1">
                    var iPageNumber = '<?php echo $iPageNumber; ?>';
                    var _gaq=_gaq||[];_gaq.push(["_setAccount","UA-33742200-1"]),_gaq.push(1==iPageNumber?["_trackPageview","/services/"]:2==iPageNumber?["_trackPageview","/overview/"]:3==iPageNumber?["_trackPageview","/thank-you/"]:["_trackPageview"]),function(){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src=("https:"==document.location.protocol?"https://":"http://")+"stats.g.doubleclick.net/dc.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}(),function(e,t,a,r,c){e[r]=e[r]||[],e[r].push({"gtm.start":(new Date).getTime(),event:"gtm.js"});var n=t.getElementsByTagName(a)[0],s=t.createElement(a),g="dataLayer"!=r?"&l="+r:"";s.async=!0,s.src="//www.googletagmanager.com/gtm.js?id="+c+g,n.parentNode.insertBefore(s,n)}(window,document,"script","dataLayer","GTM-WV4RB6"); 
                </script> -->
            <?php } ?> 
</head>     
<body <?php if($iNewServicePage==1 || $iEmailVerificationPage==1){ echo "class='new-searvice-page-main-container'"; } ?> onclick="showHideCookieDiv();"> 
<div id="Transporteca_popup"></div>
<div id="signin_help_pop" class="help-pop"></div>
<div id="leave_page_div" style="display:none;">	</div>
<div id="ajaxLogin"></div>
<div id="contactPopup"></div> 
    <header class="header clearfix">
        <nav><a href="javascript:void(0);" class="menu"><?=t($t_base.'header/menu');?></a>
            <div class="sub-nav-overlay">&nbsp;</div>
            <div class="sub-nav" id="sub-nav">
                <ul>
                    <li id="first-nav" <?php if($_REQUEST['tabFlag']==1 && $_REQUEST['menuflag']=='agg'){?> class="active"<?php }?>><a href="javascript:void(0);"><?=t($t_base.'footer/how_does_it_works');?></a>
        <?php 
            if(!empty($howDoesworksLinks))
            {
                echo '<ul>';
                foreach($howDoesworksLinks as $howDoesworksLinkss)
                {
                    if($_REQUEST['menuflag'] == $howDoesworksLinkss['szLink'])
                    {
                        $szClass = 'class="active-triangle"';
                        if($iLanguage==2)
                        {
                            $szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL_DA__."/".$howDoesworksLinkss['szLink'];
                        }
                        else
                        {
                            $szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL__."/".$howDoesworksLinkss['szLink'];
                        }										
                    }
                    else 
                    {
                        $szClass = 'class="triangle" ' ;
                        if($iLanguage==2)
                        {
                            $szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL_DA__."/".$howDoesworksLinkss['szLink'];
                        }
                        else
                        {
                            $szUrl = __BASE_URL__."/".__HOW_IT_WORKS_URL__."/".$howDoesworksLinkss['szLink'];
                        }
                    }
        ?>
                    <li><a href="<?php echo $szUrl; ?>" title="<?php echo $howDoesworksLinkss['szLinkTitle']; ?>"> <?=$howDoesworksLinkss['szPageHeading']?></a></li>
        <?php }	?>
                </ul>
        <?php } ?>
</li>
<li id="second-nav" <?php if($_REQUEST['tabFlag']==2 && $_REQUEST['menuflag']=='agg'){?> class="active"<?php }?>><a href="javascript:void(0);"><?=t($t_base.'footer/company');?></a>
        <?php
        if(!empty($companyLinksAry))
        {
                echo '<ul>';
                foreach($companyLinksAry as $companyLinksArys)
                {
                    if($_REQUEST['menuflag'] == $companyLinksArys['szLink'])
                    {
                        $szClass = 'class="active-triangle"';
                        if($iLanguage==2)
                        {
                            $szUrl = __BASE_URL__."/".__COMPANY_URL_DA__."/".$companyLinksArys['szLink'];
                        }
                        else
                        {
                            $szUrl = __BASE_URL__."/".__COMPANY_URL__."/".$companyLinksArys['szLink'];
                        }
                    }
                    else 
                    {
                        $szClass = 'class="triangle" ' ;
                        if($iLanguage==2)
                        {
                            $szUrl = __BASE_URL__."/".__COMPANY_URL_DA__."/".$companyLinksArys['szLink'];
                        }
                        else
                        {
                            $szUrl = __BASE_URL__."/".__COMPANY_URL__."/".$companyLinksArys['szLink'];
                        }
                    }
?>
            <li><a href="<?php echo $szUrl; ?>"  title="<?php echo $companyLinksArys['szLinkTitle']; ?>" > <?=$companyLinksArys['szPageHeading']?></a></li>
    <?php }	?>
        </ul>
					<?php } ?>
					</li>
					<li id="third-nav" <?php if($_REQUEST['tabFlag']==3 && $_REQUEST['menuflag']=='agg'){?> class="active"<?php }?>><a href="javascript:void(0);"><?=t($t_base.'footer/transportpedia');?></a>
					<?php
						if(!empty($tranportpediaLinksAry))
						{
							echo '<ul>';
							foreach($tranportpediaLinksAry as $tranportpediaLinksArys)
							{
								if($_REQUEST['menuflag'] == $tranportpediaLinksArys['szLink'])
								{
									$szClass = 'class="active-triangle"';
									if($iLanguage==2)
									{
										$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL_DA__."/".$tranportpediaLinksArys['szLink'];
									}
									else
									{
										$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL__."/".$tranportpediaLinksArys['szLink'];
									}
								}
								else 
								{
									$szClass = 'class="triangle" ' ;
									if($iLanguage==2)
									{
										$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL_DA__."/".$tranportpediaLinksArys['szLink'];
									}
									else
									{
										$szUrl = __BASE_URL__."/".__TRANSTPORTPEDIA_URL__."/".$tranportpediaLinksArys['szLink'];
									}
								}
					?>
							<li><a href="<?php echo $szUrl; ?>"  title="<?php echo $companyLinksArys['szLinkTitle']; ?>"> <?=$tranportpediaLinksArys['szPageHeading']?></a></li>
					<?php }	?>
						</ul>
					<?php } ?>
					</li> 
					<li id="fourth-nav" <?php if($_REQUEST['menuflag']=='info'){?> class="active"<?php }?>><a href="javascript:void(0);"><?=t($t_base.'footer/information');?></a>
					<ul>
					<?php
                                            if(!empty($seoPageHeaderAry))
                                            { 
                                                foreach($seoPageHeaderAry as $seoPageHeaderArys)
                                                { 
                                            ?>
                                                    <li><a href="<?php echo __INFORMATION_PAGE_URL__.'/'.$seoPageHeaderArys['szUrl'] ?>" title="<?php echo $seoPageHeaderArys['szLinkTitle']; ?>"> <?=$seoPageHeaderArys['szPageHeading']?></a></li>
                                            <?php   }
                                            }	 
						if(!empty($blogPageHeaderAry))
						{ 
							foreach($blogPageHeaderAry as $blogPageHeaderArys)
							{ 
					?>
							<li><a href="<?php echo __INFORMATION_PAGE_URL__.'/'.$blogPageHeaderArys['szLink'] ?>" title="<?php echo $blogPageHeaderArys['szLinkTitle']; ?>"> <?=$blogPageHeaderArys['szHeading']?></a></li>
					<?php }	 } ?>
						</ul> 
					</li> 
					<?php
					 	$szMyBookingUrl_onclick='';
						if((int)$_SESSION['user_id']>0)
						{
							if($kUser->iConfirmed==1)
							{
								if($kUser->iFirstTimePassword==1)
								{
									$redirect_booking_url = __BASE_URL__."/myBooking" ;
									$szMyBookingUrl = "javascript:void(0);" ;
									$szMyBookingUrl_onclick="onclick='display_first_time_password_popup(1)'";
								}
								else
								{
									//check if there is cookie for password is set or not
									if(empty($_COOKIE['__USER_PASSWORD_COOKIE__']) || ($_COOKIE['__USER_PASSWORD_COOKIE__']!=$kUser->szUserPasswordKey))
									{
										$szMyBookingUrl = "javascript:void(0);" ;
										$szMyBookingUrl_onclick="onclick='display_check_for_password_popup(2)'";
									}
									else
									{
									 	$szMyBookingUrl = __BASE_URL__."/myBooking" ;
									} 
								}
							}
							else
							{
								$szMyBookingUrl = __BASE_URL__."/verify-email" ;
							}  
					?> 
						<li id="fifth-nav"><a href="<?php echo $szMyBookingUrl; ?>" <?php echo $szMyBookingUrl_onclick; ?>><?=t($t_base.'header/my_bookings');?></a></li>
					<?php } 
						$t_base_explain="ExplainPage/";
						if(__LANGUAGE__==__LANGUAGE_TEXT_DANISH__) 
						{
							$szSearchUrl=__BASE_URL__."/searchword/";
						}
						else
						{
							$szSearchUrl=__BASE_URL__."/searchword/";
						} 
					?>
				</ul>	
                                <form name="wordSearchLeftForm" id="wordSearchLeftForm" method="post" action="<?php echo $szSearchUrl;?>">
                                    <div class="clearfix">
                                        <input type="text" size="20" name="szKeyWordLeftValue" autocomplete="off" onkeyup="submitenterKeyValue(event,'wordSearchLeftForm');" id="szKeyWordLeftValue" value="" placeholder="<?php echo t($t_base_explain.'fields/search');?>">	
                                        <input type="text" style="display: none;" />
                                        <img align="absmiddle" style="opacity:0.4" id="searchButtonIdLeft" src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/images/magnifying-icon.jpg">
                                    </div>
				</form>			
				<div class="social-icons"><a href="https://www.facebook.com/MyTransporteca" target="_blank" class="facebook"><?=t($t_base.'header/facebook');?></a><a href="https://twitter.com/transporteca" target="_blank" class="twitter"><?=t($t_base.'header/twitter');?></a><a href="http://www.linkedin.com/company/transporteca" target="_blank" class="linkedin"><?=t($t_base.'header/linkedin');?></a><a href="<?php echo $szYoutubeUrl;?>" target="_blank" class="youtube">youtube</a></div>
			</div>
		</nav>
		<div class="logo"><a <?php if((int)$idBooking>0 && ($display_abandon_message)) {?>href="javascript:void(0);" onclick="display_abandon_popup();" <?php }else {?>href="<?=__BASE_URL_SECURE__?>"<? }?>><?=t($t_base.'header/transporteca');?></a></div>
		
		<?php  
                    $szMyAccountUrl_onclick = '';
                    if((int)$_SESSION['user_id']>0)
                    { 
                        if($kUser->iConfirmed==1)
                        {
                            if($kUser->iFirstTimePassword==1)
                            { 
                                $szMyAccountUrl = "javascript:void(0);" ;
                                $szMyAccountUrl_onclick="onclick='display_first_time_password_popup(2)'";
                            }
                            else
                            { 
                                //check if there is cookie for password is set or not
                                if(empty($_COOKIE['__USER_PASSWORD_COOKIE__']) || ($_COOKIE['__USER_PASSWORD_COOKIE__']!=$kUser->szUserPasswordKey))
                                {
                                    $szMyAccountUrl = "javascript:void(0);" ;
                                    $szMyAccountUrl_onclick="onclick='display_check_for_password_popup(2)'";
                                }
                                else
                                { 
                                    $szMyAccountUrl = __BASE_URL__."/myAccount" ;
                                }
                            } 
                        }
                        else
                        {
                            $szMyAccountUrl = __BASE_URL__."/verify-email" ;
                        }  
		?>
		<div class="login" onmouseover='$("#show_main_link").attr("style","display:block;");' onmouseout='$("#show_main_link").attr("style","display:none;");'>
			<a href="<?php echo $szMyAccountUrl; ?>" <?php echo $szMyAccountUrl_onclick; ?>>
				<?php if($kUser->iIncompleteProfile == 1){
                                        if(!empty($kUser->szFirstName)){?>
                                           <p id="userInfo"><?=$kUser->szFirstName?></p>
                                        <?php } else {?>
						<p id="userInfo"><?=$kUser->szEmail?></p>
				<?php } } else {?>					
					<p id="userInfo"><?=$kUser->szFirstName?></p>
				<?php } ?>
			</a>
			<div id="show_main_link" style="display:none;">
				<?php 
				/*
				 * If user profile is incomplete and is in booking process then before sending myaccount we give confirmation then redirect. 
				 * 
				 * */
				$booking_key = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
				?>
				<p class="logout_account"><a href="<?=__USER_LOGOUT_URL__?>">Logout</a></p>
				<p class="user_account"><a href="<?=$szMyAccountUrl?>" <?php echo $szMyAccountUrl_onclick; ?>><?=t($t_base.'forwarder_header/myAccount');?></a></p> 
			</div>
		</div>
		<?php } else { ?>
		<div class="login">
			<a href="javascript:void(0)" onclick="userLoginPopUp('loginForm_header');" id="login"><?=t($t_base.'header/log_in');?></a>
			<form id="loginForm_header" name="loginForm" method="post" action="">						
				<input type="hidden" name="loginArr[szRedirectUrl]" id="szHeaderRedirectUrl" value="<?=__MAIN_SITE_HOME_PAGE_URL__.$_SERVER['REQUEST_URI']?>" >
				<input type="hidden" name="loginArr[iDoNotDisplayErrorMessage]" id="1" value="1" >
			</form>	
		</div>
		<?php }?>
		<div class="country_logo">
                    <?php
                        if(__LANGUAGE__==__LANGUAGE_TEXT_ENGLISH__)
                        {   
                            $szLanguageRedirDanishUrl = getFooterLanguageUrl($_SERVER['REQUEST_URI'],__LANGUAGE_ID_DANISH__,$szLinkedPageUrl); 
                             ?>
                            <a href="<?php echo $szLanguageRedirDanishUrl; ?>" alt="Dansk" title="Dansk" class="danish-flag">&nbsp;</a> 
                            <?php
                        }
                        else
                        {
                            $szLanguageRedirEnglishUrl = getFooterLanguageUrl($_SERVER['REQUEST_URI'],__LANGUAGE_ID_ENGLISH__,$szLinkedPageUrl); 
                            ?> 
                            <a href="<?php echo $szLanguageRedirEnglishUrl; ?>"  class="english-flag" alt="Engelsk" title="Engelsk">&nbsp;</a> 
                            <?php
                        }
                    ?> 
                </div>
		<div class="telephone">
		<?php  echo $szCustomerCareNumer; ?>
		</div> 
		<div class="customer-services" id="customer-services-link"><a href="javascript:$zopim.livechat.window.toggle();" onclick="close_chat_bubble();"><?=t($t_base.'header/customer_services');?></a></div>
</header>
<?php  
if((int)$iDonotIncludeHiddenSearchHeader==0){ 

if(trim($style)=='')
{
    $style="style='display:none;'";
}
?>
<header id="header-search" class="header sub clearfix" <?php echo $style;?>>
	<nav><a href="javascript:void(0);" class="menu"></a></nav>
	<div class="logo"><a <?php if((int)$idBooking>0 && ($display_abandon_message)) {?>href="javascript:void(0);" onclick="display_abandon_popup();" <?php }else {?>href="<?=__BASE_URL_SECURE__?>"<? }?>><?=t($t_base.'header/transporteca');?></a></div>	
	<div id="transporteca_hidden_search_form_container">
		
		<?php 
                    if($iNewServicePageFlag==1)
                    {
                        //echo display_new_search_form($kBooking,$postSearchAry,true,true);
                    }
                    else
                    {  
                        $idSeoPage = false;
                        if($_REQUEST['seo_page_flag']==1)
                        { 
                            $idSeoPage = $seoLinkAry['id'] ;
                        }				
                        //echo display_new_search_form($kBooking,$postSearchAry,true,false,false,$szDefaultFromField,$idSeoPage,true,$idDefaultLandingPage);
                    } 
		?>
	</div>	
	<?php if((int)$_SESSION['user_id']>0){?>
	<div class="login" onmouseover='$("#show_link").attr("style","display:block;");' onmouseout='$("#show_link").attr("style","display:none;");'>
            <a href="<?php echo $szMyAccountUrl; ?>" <?php echo $szMyAccountUrl_onclick; ?>>
                <?php if($kUser->iIncompleteProfile == 1){
                    if(!empty($kUser->szFirstName)){?>
                        <p id="userInfo"><?=$kUser->szFirstName?></p>
                    <?php } else { ?>
                       <p id="userInfo"><?=$kUser->szEmail?></p>
                <?php } } else {?>					
                    <p id="userInfo"><?=$kUser->szFirstName?></p>
                <?php } ?>
            </a>
            <div id="show_link" style="display:none;">
                <?php 
                /*
                 * If user profile is incomplete and is in booking process then before sending myaccount we give confirmation then redirect. 
                 * 
                 * */
                $booking_key = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
                if(($kUser->iIncompleteProfile == 1) && (!empty($booking_key))){ ?>
                    <p class="logout_account"><a href="javascript:void(0);" onclick="display_abandon_popup('<?=__USER_LOGOUT_URL__?>')"><img width="100%" height="100%" src="<?=__BASE_STORE_IMAGE_URL__?>/logout-icon.png" alt="Logout" title="Logout" border="0" /></a></p>
                    <p class="user_account"><a href="javascript:void(0);" onclick="display_abandon_popup('<?=__USER_MY_ACCOUNT_URL__?>')"><?=t($t_base.'forwarder_header/myAccount');?></a></p>
                <?php } else { ?>
                    <p class="logout_account"><a href="<?=__USER_LOGOUT_URL__?>">Logout</a></p>
                    <p class="user_account"><a href="<?php echo $szMyAccountUrl; ?>" <?php echo $szMyAccountUrl_onclick; ?>><?=t($t_base.'forwarder_header/myAccount');?></a></p>
                <?php } ?>
            </div>
        </div>
        <?php }else{?>
            <div class="login">
                <a href="javascript:void(0)" onclick="userLoginPopUp('loginForm_header');" id="login"><?=t($t_base.'header/log_in');?></a>
                <form id="loginForm_header" name="loginForm" method="post" action="">						
                    <input type="hidden" name="loginArr[szRedirectUrl]" id="szHeaderRedirectUrl" value="<?=__MAIN_SITE_HOME_PAGE_URL__.$_SERVER['REQUEST_URI']?>" >
                    <input type="hidden" name="loginArr[iDoNotDisplayErrorMessage]" id="1" value="1" >
                </form>	
            </div>
        <?php }?> 
	<div class="telephone">
            <?php echo $szCustomerCareNumer; ?>
	</div>
	<a href="javascript:void(0);" class="search-icon" onclick="openSearchPopup();">&nbsp;</a>
            <input type="hidden" id="openSearchPopUp" value="">
	<div id="searchFormPop"></div>
</header>	 
<?php }
 
$szLastSeoPageURL = str_replace("/","",$_REQUEST['flag']); 

//this is for seopage aquision
if(!empty($_SESSION['seo_aquisition']['szReferalUrl']) && !empty($_SESSION['seo_aquisition']['szUrl']) && ($_SESSION['seo_aquisition']['szUrl']!=$szLastSeoPageURL))
{
    $kSEO = new cSEO();
    $szURL = $_SESSION['seo_aquisition']['szUrl'] ;

    $seoAquisitionLinkAry = array();
    $seoAquisitionLinkAry = $kSEO->getSeoPageDataByUrl($szURL);

    $seoTrackerAry = array();
    $seoTrackerAry['szReferalUrl'] = $_SESSION['seo_aquisition']['szReferalUrl'] ;
    $seoTrackerAry['szSeoUrl'] = $szURL;
    $seoTrackerAry['idSeoPage'] = $seoAquisitionLinkAry['id'];

    $kSEO->addSEOTracker($seoTrackerAry);

    unset($_SESSION['seo_aquisition']['szReferalUrl']);
    unset($_SESSION['seo_aquisition']['szUrl']);
} 
?>
<script type="text/javascript" defer="1">
$().ready(function(){
    <?php if((int)$headerSearchFlag==0){ ?> 
            $.fn.scrollBottom=function(){return $(document).height()-this.scrollTop()-this.height()};var $el=$("#header-search"),$window=$(window);$window.bind("scroll resize",function(){var o=($window.height()-$el.height()-10,870-$window.scrollBottom(),$window.scrollTop());437>o?($el.css({top:870-o+"px",bottom:"auto",display:"none"}),$("#iHiddenChanged").attr("value","2")):($el.css({top:0,bottom:"auto",display:"block"}),fill_data_to_hidden_form())}); 
    <?php }?>
    $(".menu").click(function(){$(this).hasClass("active")?($(this).removeClass("active"),$("#sub-nav").animate({left:"-260px"}),$(".sub-nav-overlay").hide(),$("body").css({overflow:"auto"}),$("#sub-nav li").removeClass("active")):($(this).addClass("active"),$("#sub-nav").animate({left:"0"}),$(".sub-nav-overlay").show(),$("body").css({overflow:"hidden"}))}),$(".sub-nav-overlay").click(function(){$(".menu").hasClass("active")&&($(".menu").removeClass("active"),$("#sub-nav").animate({left:"-260px"}),$(this).hide(),$("body").css({overflow:"auto"}),$("#sub-nav li").removeClass("active"))}),$("#first-nav").click(function(){$(this).hasClass("active")?$(this).removeClass("active"):$(this).addClass("active"),$("#second-nav, #third-nav, #fourth-nav, #fifth-nav").removeClass("active")}),$("#second-nav").click(function(){$(this).hasClass("active")?$(this).removeClass("active"):$(this).addClass("active"),$("#first-nav, #third-nav, #fourth-nav, #fifth-nav").removeClass("active")}),$("#third-nav").click(function(){$(this).hasClass("active")?$(this).removeClass("active"):$(this).addClass("active"),$("#first-nav, #second-nav, #fourth-nav, #fifth-nav").removeClass("active")}),$("#fourth-nav").click(function(){$(this).hasClass("active")?$(this).removeClass("active"):$(this).addClass("active"),$("#first-nav, #second-nav, #third-nav, #fifth-nav").removeClass("active")}),$("#fifth-nav").click(function(){$(this).hasClass("active")?$(this).removeClass("active"):$(this).addClass("active"),$("#first-nav, #second-nav, #third-nav, #fourth-nav").removeClass("active")}); 
}); 
function showHideCookieDiv(){var o=$("#cookie-blue-box-container").css("display");"block"==o&&($("#cookie-blue-box-container").slideUp("slow"),set_transporteca_cookies())}
</script>

<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="Loading..." width="100%" height="100%" />				
</div>
<div id="customs_clearance_pop" class="tooltip-pop"></div>
<div id="user_account_login_popup" style="display:none;"></div> 
<?php 

if((int)$iDonotIncludeHiddenSearchHeader==0 && !empty($style))
{ 
   if(empty($_COOKIE['__DISPLAY_COOKIE_NOTIFICATION__']))
   {
       if($_SESSION['displayed_cookie_message']==1 && $_SESSION['displayed_cookie_on_page']!=$_SERVER['REQUEST_URI'])
       {
            $cookieValue = "CookieValue";
            if(__ENVIRONMENT__ == "LIVE")
            {
                setcookie("__DISPLAY_COOKIE_NOTIFICATION__", $cookieValue, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
            }
            else
            {
                setcookie("__DISPLAY_COOKIE_NOTIFICATION__", $cookieValue, time()+(3600*24*90),'/');
            }
            unset($_SESSION['displayed_cookie_on_page']);

            $_SESSION['displayed_cookie_message']=0;
            unset($_SESSION['displayed_cookie_message']);
       } 
       else
       {
            echo display_cookie_text_details($iFromLandingPage);   
            $_SESSION['displayed_cookie_message'] = 1;
            $_SESSION['displayed_cookie_on_page'] = $_SERVER['REQUEST_URI'] ;
       }
   }
}
?>