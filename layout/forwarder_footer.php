<?php
$t_base = "Forwarders/";

if((int)$_SESSION['forwarder_user_id']>0)
{	
    $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']); 
    $szCustomerName = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
    $szCustomerEmail = $kForwarderContact->szEmail;
    $szPhoneNumber = $kForwarderContact->szPhone;
    //$szCustomerNotes = "From Forwarder section";
}

?>
<span id="haulage_zone_countries_calculation_logs" style="display:none;"></span>
        <div id="footer">
            <div class="copyright">
                <span>&copy; <?=DATE('Y');?> <?=t($t_base.'footer/copy_right');?></span> 
                <?php if(isset($_SESSION['forwarder_user_id'])>0 && !$bDoNotDisplayHeaderFlag) {?>
                    <span class="forwarder_footer_links">	
                        <a href="<?=__BASE_URL__?>/ForwardersGuide/" target="blank" ><?=t($t_base.'footer/guide');?></a>
                        <a href="<?=__BASE_URL__?>/T&C/" ><?=t($t_base.'footer/terms');?> &amp; <?=t($t_base.'footer/conditions');?></a>
                        <a href="<?=__BASE_URL__?>/PrivacyNotice/" ><?=t($t_base.'footer/privacy_notice');?></a>
                        <a href="<?=__BASE_URL__?>/FAQ/" ><?=t($t_base.'footer/FAQ');?></a>
                        <a href="javascript:void(0)" onclick="open_contact_popup();"><?=t($t_base.'footer/contact');?></a>
                    </span>
                <?php } ?> 
            </div>
        </div>	
        <?php
        if((int)$_SESSION['forwarder_admin_id']==0 && (int)$_SESSION['forwarder_user_id']>0)
        {?>
        <script type='text/javascript'>
        $().ready(function(){   
            
                auto_forwarder_header_notification();
           
        });
    </script>
        <?php }?>
        <script type="text/javascript">  
            <?php  if(!empty($szCustomerEmail)){?>
                /*Start of Zopim Live Chat Script*/
                window.$zopim||function(t,e){var s=$zopim=function(t){s._.push(t)},n=s.s=t.createElement(e),c=t.getElementsByTagName(e)[0];s.set=function(t){s.set._.push(t)},s._=[],s.set._=[],n.async=!0,n.setAttribute("charset","utf-8"),n.src="//v2.zopim.com/?28qgkS9x5hAjidkz0ubaskHS1pQcLcMU",s.t=+new Date,n.type="text/javascript",c.parentNode.insertBefore(n,c)}(document,"script");
            
                $zopim(function() {
                    $zopim.livechat.setName('<?php echo $szCustomerName; ?>');
                    $zopim.livechat.setEmail('<?php echo $szCustomerEmail; ?>');
                    $zopim.livechat.setPhone('<?php echo $szPhoneNumber; ?>');
                    $zopim.livechat.addTags('<?php echo __FORWARDER_CHAT_TAG_KEYWORD__; ?>');  
                });
            <?php } ?> 
            /* End of Zopim Live Chat Script */ 
        </script>  
    </body>
</html>