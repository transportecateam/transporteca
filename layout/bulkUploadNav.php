<!--    <p><?=t($t_base.'title/quotes');?></p> 
    <ul>
        <li <?php if($_REQUEST['flag']=='quote_pend') {?>class="active"<?php }?>><a href="<?php echo __FORWARDER_PENDING_QUOTES_URL__?>"><?=t($t_base.'links/pending');?></a></li>
        <li <?php if($_REQUEST['flag']=='quote_past') {?>class="active"<?php }?>><a href="<?php echo __FORWARDER_PAST_QUOTES_URL__?>"><?=t($t_base.'links/past');?></a></li>
    </ul>-->    
    <p><?=t($t_base.'title/update_one_by_one');?></p> 
    <ul>
        <li <?php if($_REQUEST['flag']=='ser_ofr') {?>class="active"<?php }?>><a href="<?php echo __FORWARDER_SERVICE_OFFERING_URL__?>">Courier Services</a></li> 
        <li <?php if($_REQUEST['flag']=='obo_air') {?> class="active" <?php } ?>><a href="<?=__FORWARDER_AIR_FREIGHT_SERVICE_URL__?>">Airfreight Services</a></li>
        <li <?php if($_REQUEST['flag']=='obo_lcl') {?>class="active" <?php }?>><a href="<?=__FORWARDER_OBO_LCL_SERVICES_URL__?>"><?=t($t_base.'links/lcl_services');?></a></li>
        <li <?php if($_REQUEST['flag']=='obo_haul') {?>class="active" <?php }?>><a href="<?=__FORWARDER_OBO_HAULAGE_URL__?>"><?=t($t_base.'links/haulage');?></a></li>
        <li <?php if($_REQUEST['flag']=='obo_cc') {?>class="active" <?php }?>><a href="<?=__FORWARDER_OBO_CC_SERVICES_URL__?>"><?=t($t_base.'links/customs_clearance');?></a></li>
        <li <?php if($_REQUEST['flag']=='air_cfs') {?> class="active" <?php } ?>><a href="<?=__FORWARDER_AIRPORT_WAREHOUSE_URL__?>">Airport Warehouses</a></li>
        <li <?php if($_REQUEST['flag']=='cfs_loc') {?> class="active" <?php } ?>><a href="<?=__FORWARDER_CFS_LOCATIONS_URL__?>"><?=t($t_base.'links/cfs_locations');?></a></li>  
    </ul>
    <br />
    <p><?=t($t_base.'title/upload_in_bulk');?></p> 
    <ul>
        <li <?php if($_REQUEST['flag']=='bulkairservice') {?> class="active" <?php } ?>><a href="<?php echo __FORWARDER_AIR_FREIGHT_BULK_SERVICE_URL__; ?>">Airfreight Services</a></li>
        <li <?php if($_REQUEST['flag']=='lclservice') {?>class="active" <?php }?>><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/LCLServicesBulk/"><?=t($t_base.'links/lcl_services');?></a></li>
        <li <?php if($_REQUEST['flag']=='haulage') {?>class="active" <?php }?>><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/"><?=t($t_base.'links/haulage');?></a></li>
        <li<?php if($_REQUEST['flag']=='customClearance') {?> class="active" <?php }?>><a href="<?php echo __FORWARDER_HOME_PAGE_URL__?>/CustomsClearanceBulk/"><?=t($t_base.'links/customs_clearance');?></a></li>
        <li <?php if($_REQUEST['flag']=='bulk_air_cfs') {?> class="active" <?php } ?>><a href="<?=__FORWARDER_BULK_AIRPORT_WAREHOUSE_URL__?>">Airport Warehouses</a></li> 
        <li <?php if($_REQUEST['flag']=='cfs_loc_bulk') {?> class="active" <?php } ?>><a href="<?php echo __FORWARDER_CFS_BULK_LOCATIONS_URL__?>"><?=t($t_base.'links/cfs_locations');?></a></li>  
    </ul>  
    <br /> 
    <p><?=t($t_base.'title/upload_services');?></p> 
    <ul>
        <li <?php if($_REQUEST['flag']=='inst') {?>class="active" <?php }?>><a href="<?=__FORWARDER_CFS_UPLOAD_INSTRUCTION_URL__?>"><?=t($t_base.'links/instructions');?></a></li>
        <li <?php if($_REQUEST['flag']=='submit') {?>class="active" <?php }?>><a href="<?=__FORWARDER_BULK_UPLOAD_SERVICES_URL__?>"><?=t($t_base.'links/submit');?></a></li>
        <li <?php if($_REQUEST['flag']=='awat_app') {?>class="active" <?php }?>><a href="<?=__FORWARDER_BULK_SERVICES_APPROVAL_URL__?>"><?=t($t_base.'links/awaiting_approval');?></a></li>
    </ul> 
    <br />	
	
    <p><?=t($t_base.'title/settings');?></p> 
    <ul>
        <li <?php if($_REQUEST['flag']=='non_acceptance') {?> class="active" <?php }?>><a href="<?=__FORWARDER_NON_ACCEPTANCE_URL__?>"><?=t($t_base.'links/non_acceptance');?></a></li>
        <li <?php if($_REQUEST['flag']=='private_customer') {?> class="active" <?php }?>><a href="<?php echo __FORWARDER_PRIVATE_CUSTOMER_URL__?>"><?=t($t_base.'links/private_customer');?></a></li>
    </ul> 
    <br> 
    
    <p><?=t($t_base.'title/cutomer_view');?></p> 
    <ul>
        <li <?php if($_REQUEST['flag']=='try_it') {?> class="active" <?php } ?>><a href="<?=__FORWARDER_TRY_IT_OUT_URL__?>"><?=t($t_base.'links/try_it_out');?></a></li>
    </ul>