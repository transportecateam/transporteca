<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 
include_classes();
require_once( __APP_PATH__ . "/inc/admin_functions.php" );
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
if(!empty($_SERVER['HTTP_HOST']))
{
    define ('__BASE_URL__', __BASE_METRICS_URL__);
    define ('__BASE_URL_SECURE__', __BASE_SECURE_METRICS_URL__);
    define_metrics_constants(); 
}

define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
if(empty($szMetaTitle))
{
    $szMetaTitle = "Metrics Landing page ";
}
$t_base = "management/header/";
//validateManagement();
$kAdmin=new cAdmin();
 
if((int)$_SESSION['admin_id']>0)
{
    $idAdmin = $_SESSION['admin_id'];
}
$kAdmin->getAdminDetails($idAdmin);
 
$kBooking = new cBooking();

 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
       <script type="text/javascript">
           if(document.location.protocol == 'https:')
           {
               var __JS_ONLY_SITE_BASE__ = "<?=__BASE_URL_SECURE__?>";
               var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
           }
           else
           {
               var __JS_ONLY_SITE_BASE__ = "<?=__BASE_URL__?>";
               var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
           }      
       </script>	
	    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
            <?php if(!empty($szMetaKeywords)){?><meta name="keywords" content="<?=$szMetaKeywords; unset($szMetaKeywords);?>" /><?}?>
            <?php if(!empty($szMetaDescription)){?><meta name="description" content="<?=$szMetaDescription; unset($szMetaDescription);?>" /><?}?>
            <title id="metatitle"><? if(!empty($szMetaTitle)){ echo $szMetaTitle; unset($szMetaTitle);}?></title>
            <link href='https://fonts.googleapis.com/css?family=Roboto:700italic,700,400' rel='stylesheet' type='text/css'>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
            <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_MANAGEMENT_URL__?>/style.css" />
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/forwarder.js"></script>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/admin.js"></script>
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
            <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" />
            <link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>
            <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
            <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js?v=2'></script>
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jQuery.zclip.min.js?v=2'></script>
	</head>
	<body class="mainpage">
		<div id="header">
                    
                        <img src="<?=__BASE_STORE_IMAGE_URL__?>/transporteca-logo-new.png" alt="Transporteca" title="Transporteca"  class="logo" />
                  
                    <div class="header-links"> 
                        <div style="float: right;">
                            <?php if((int)$_SESSION['metrics_user_id']>0){ ?>
                                <p class="logout_account"><a href="<?=__METRICS_LOGOUT_URL__?>"><?=t($t_base.'title/metrics_logout');?></a></p>
                            <?php }?>
                        </div>
                    </div>
		</div>
		<div id="contactPopup"></div>
		<div id="ajaxLogin"></div>
		<?php
			$header_flag = (!empty($_REQUEST['menuflag']))?sanitize_all_html_input(trim($_REQUEST['menuflag'])):'';	
		?>	
		 <div id="navigation">
		 </div>
		<!--  <div id="navigation">
		<?php if((int)$_SESSION['metrics_user_id']==0){ ?>
		<ul>
                    <li <?php if($header_flag=='dash') {?> class="active" <? }?>>
                    <a href="<?=__METRICS_DASHBOARD_URL__?>"><span><?=t($t_base.'title/dashboard');?></span><br/><?=t($t_base.'title/quick_over_view');?></a>	
                    </li> 
		</ul>
		 <?php } ?>
		</div>-->
		
		<div style="clear:both; "></div>
             
                
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>