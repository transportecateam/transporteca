<?php
/**
 * Ajax Header file
 */
 ob_start();
if(!isset($_SESSION))
{
session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) ); 
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 
include_classes();
require_once( __APP_PATH__ . "/inc/admin_functions.php" );
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/vat_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
if(!empty($_SERVER['HTTP_HOST']))
{
    define ('__BASE_URL__', "http://".$_SERVER['HTTP_HOST']);
    define ('__BASE_URL_SECURE__', "https://".$_SERVER['HTTP_HOST']);
    define_management_constants(); 
}
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
$kAdmin= new  cAdmin();
if((int)$_SESSION['admin_id']>0)
{	
    $idAdmin = $_SESSION['admin_id'];
    $kAdmin->getAdminDetails($idAdmin);
}

//print_r(get_declared_classes()); 
?>