<?php
/**
 * Ajax Header file
 */
 ob_start();
if(!isset($_SESSION))
 {
	session_start();
 }
$idForwarder=$_SESSION['forwarder_user_id'];
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes();
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);

$kForwarder = new cForwarder();
if(!empty($_SERVER['HTTP_HOST']))
{
	if($kForwarder->isHostNameExist($_SERVER['HTTP_HOST']))
	{
		define ('__BASE_URL__', "http://".$_SERVER['HTTP_HOST']);
   		define ('__BASE_URL_SECURE__', "https://".$_SERVER['HTTP_HOST']);
		define_forwarder_constants();
	}
	else
	{
		header("Location:".__MAIN_SITE_HOME_PAGE_URL__);
		exit;	
	}
}
if(!defined('__BASE_URL__')) 
{
	define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
	define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__);
}

//$_SESSION['forwarder_user_id']='12';
$kForwarder= new  cForwarder();
$kForwarderContact= new cForwarderContact();

if((int)$_SESSION['forwarder_user_id']>0)
{
    $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
    $idForwarder=$kForwarderContact->idForwarder;
}
?>