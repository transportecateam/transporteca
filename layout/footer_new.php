<?php
require(__WORDPRESS_FOLDER__.'/wp-config.php');
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();
$t_base = "home/";


$kWhsSearch = new cWHSSearch(); 
$szFacebookUrl= addhttp($szFacebookUrl);
$szTwitterUrl= addhttp($szTwitterUrl);
$szLinkedinUrl= addhttp($szLinkedinUrl); 
$iLanguage = getLanguageId(); 


$kConfig = new cConfig();

$currentLangArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);

$langArr=$kConfig->getLanguageNameMapped($iLanguage); 
//if($iFromLandingPage==1)
//{
//    $szLanguageRedirEnglishUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__; 
//}
//else
//{
//    $szLanguageRedirEnglishUrl = getFooterLanguageUrl($_SERVER['REQUEST_URI'],__LANGUAGE_ID_ENGLISH__,$szLinkedPageUrl);
//}
//
//if($iFromLandingPage==1)
//{
//    $szLanguageRedirDanishUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/da"; 
//}
//else
//{
//    $szLanguageRedirDanishUrl = getFooterLanguageUrl($_SERVER['REQUEST_URI'],__LANGUAGE_ID_DANISH__,$szLinkedPageUrl);
//}

//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{
//   // $szLanguage = "da";
//    $menuName=__DANISH_FOOTER_MENU__;
//    $menuRightFooter=__DANISH_RIGHT_FOOTER_MENU__;
//}
//else
//{
    //$szLanguage = "en";
    $menuName=__ENLISH_FOOTER_MENU__."".strtoupper($currentLangArr[0]['szLanguageCode']);
    $menuRightFooter=__ENGLISH_RIGHT_FOOTER_MENU__."".strtoupper($currentLangArr[0]['szLanguageCode']);
//}
//no need to change language so updated by default to english
//$szLanguage = "en";
//$szYoutubeUrl = 'https://www.youtube.com/user/TransportecaVideo';
 
$iDonotShowLanguageOption = 2;
$REQUEST_URI='';
if($_SERVER['REQUEST_URI']!='/' && $_SERVER['REQUEST_URI']!='')
{
    $REQUEST_URI="".$_SERVER['REQUEST_URI'];
}
$szEmail='';
if((int)$_SESSION['user_id']>0)
{
    $kUser = new cUser();
    $kUser_new = new cUser();
    $userArr = $kUser->getUserDetails($_SESSION['user_id']);
    $szEmail = $kUser->szEmail;
    $szCustomerEmail = $kUser->szEmail;
    $szCustomerName = $kUser->szFirstName." ".$kUser->szLastName;
    $idInternationalDialCode = $kUser->idInternationalDialCode;
    
    if($idInternationalDialCode>0)
    {
        $kConfig = new cConfig();
        $kConfig->loadCountry($idInternationalDialCode);
        $iCustomerInternationDialCode = "+".$kConfig->iInternationDialCode;

        $szPhoneNumber = $iCustomerInternationDialCode." ".$kUser->szPhoneNumber;
    }
    else
    {
        $szPhoneNumber = $kUser->szPhoneNumber;
    } 
    $lastBookingAry = array();
    $lastBookingAry = $kUser_new->getLastBookingByUserID($_SESSION['user_id']); 
    if(!empty($lastBookingAry))
    {
        //1607TCL012, 2.3cbm/100kg from Frankfurt to Milan (XXXX)
        $szPendingTrayUrl = __BASE_MANAGEMENT_URL__."/pendingTray/".$lastBookingAry['szBookingRandomNum']."/";
        $szVolume =  format_volume($lastBookingAry['fCargoVolume']);
        $szWeight =  get_formated_cargo_measure($lastBookingAry['fCargoWeight']);
        
        //$szPendingTrayUrl = '<a href="'.$szPendingTrayUrl.'" target="_blank">here</a>';
        $szCustomerNotes = "Last booking: ".$lastBookingAry['szBookingRef'].", ".$szVolume."cbm/".$szWeight."kg from ".$lastBookingAry['szOriginCity']." to ".$lastBookingAry['szDestinationCity']." (".$szPendingTrayUrl.") ";
    }
    else
    {
        $szCustomerNotes = 'No booking yet';
    }
}  
if($_REQUEST['service_str']!='')
{
    $service_strArr=  explode("-", $_REQUEST['service_str']);
    $service_str=$service_strArr[1];
}

$szMauticPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$REQUEST_URI; 
$szMauticPageUrl = urlencode($szMauticPageUrl); 
$szPageTitle = urlencode($szMetaTitle); 
$szEmail = urlencode($szEmail);
$szTags = urlencode($service_str);
$szTagUrl="";
if(!empty($szTags))
{
    $szTagUrl = "&tags=".$szTags;
}
//$szImageStr = '<img src="'. __MAUTIC_API_BASE_URL__.'/mtracking.gif?page_url='.$szMauticPageUrl.'&page_title='.$szPageTitle.'&email='.$szEmail.$szTagUrl.'" style="display: none;"/>';
 
/*
 * This code was used for Mautic PHP implementation
$szMauticTrackingcode = urlencode(base64_encode(serialize(array(
    'page_url'   => __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$REQUEST_URI,
    'page_title' => $szMetaTitle,    // Use your website's means of retrieving the title or manually insert it
    'email' => $szEmail, // Use your website's means of user management to retrieve the email
    'tags' => $service_str
    )))); 
 * <img src="<?php echo __MAUTIC_API_BASE_URL__; ?>/mtracking.gif?d=<?php echo $szMauticTrackingcode; ?>" style="display: none;"/> 
 * 
 */

?>  
<footer class="footer">
    <div class="footer-container clearfix">
        <?php
        $menuarr=wp_get_nav_menu_items($menuName);
        $menuarr=(toArray($menuarr));
        if(empty($menuarr))
        {
            $menuarr=wp_get_nav_menu_items(__ENLISH_FOOTER_MENU__);
            $menuarr=(toArray($menuarr));
        }
        //print_r($menuarr);
        
        if(!empty($menuarr))
        {
            $i=1;
            foreach($menuarr as $menuarrs)
            {
                if($i==1)
                {?>
                    <ul>
                        <li class="head"><?php echo ($menuarrs['post_title']!=''? $menuarrs['post_title']:$menuarrs['title'])?></li>    
                   <?php 
                }
                else if(count($menuarr)==$i)
                {?>
                        <li><a href="<?php echo $menuarrs['url'];?>" ><?php echo $menuarrs['title'];?></a></li>
                    </ul>
                  <?php      
                }
                else 
                {   
                    if($menuarrs['menu_item_parent']>0)
                    {
                    ?>
                    <li><a href="<?php echo $menuarrs['url'];?>" ><?php echo $menuarrs['title'];?></a></li>
                    <?php }else{?>
                    </ul><ul>
                        <li class="head"><?php echo ($menuarrs['post_title']!=''? $menuarrs['post_title']:$menuarrs['title']);?></li> 
                <?php
                    }
                }
               ++$i;
            }
        }?>
<!--        <ul>
            <li class="head"><?=t($t_base.'footer/about_us');?></li>
        <?php if($iLanguage==2){?>	
            <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL_DA__?>/vision/" ><?=t($t_base.'footer/vision');?></a></li>
            <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL_DA__?>/team/" ><?=t($t_base.'footer/team');?></a></li>
        <?php }else{?>
            <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL__?>/our-vision/" ><?=t($t_base.'footer/vision');?></a></li>
            <li><a href="<?php echo __BASE_URL__?>/<?=__COMPANY_URL__?>/team/" ><?=t($t_base.'footer/team');?></a></li>
        <?php }?>
        </ul>
        <ul>
            <li class="head"><?=t($t_base.'footer/for_forwarders');?></li>
            <li><a href="<?php echo __BASE_URL__?>/forwarder/" ><?=t($t_base.'footer/how_does_it_works_footer');?></a></li>
            <li><a href="<?php echo __BASE_URL__?>/forwarderSignUp/"><?=t($t_base.'footer/signing_up');?></a></li>
        </ul>
        <ul>
            <li class="head"><?=t($t_base.'footer/for_affiliate');?></li>
            <li><a href="<?php echo __BASE_URL__?>/partnerProgramme/" ><?=t($t_base.'footer/affiliate_programme');?></a></li>
            <li><a href="javascript:void(0)" onclick="ajaxAffliateLogin();"><?=t($t_base.'header/sign_in');?></a></li>
        </ul>
        <ul>
            <li class="head"><?=t($t_base.'footer/resources');?></li>
            <li><a href="javascript:void(0);" onclick="toggleLeftMenu();"><?=t($t_base.'footer/information');?></a></li>
            <li><a href="<?php echo __RSS_FEED_PAGE_URL__?>"><?=t($t_base.'footer/news');?></a></li>
        </ul>-->
        <?php if($iDonotShowLanguageOption==1){?>
            <ul></ul>
        <?php } else {?>
        <!--<ul>
            <li class="head"><?=t($t_base.'footer/quick_links');?></li>
            <?php
                if(!empty($langArr))
                {
                    foreach($langArr as $langArrs)
                    {
                        if($langArrs['szLanguageCode']!=__LANGUAGE_ID_SWEDISH_CODE__)
                        {
                            if($langArrs['szLanguageCode']==$currentLangArr[0]['szLanguageCode'])
                            {
                                ?>
                                <li><a><?=ucwords($langArrs['szName']);?></a></li>
                                <?php
                            }
                            else
                            {
                                $szLanguageRedirectUrl='';
                                if($iFromLandingPage==1)
                                {
                                    if($langArrs['szLanguageCode']==__LANGUAGE_ID_ENGLISH_CODE__)
                                    {
                                        $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__; 
                                    }
                                    else
                                    {
                                        $szLanguageRedirectUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$langArrs['szLanguageCode']."/"; 
                                    }
                                }
                                else
                                {
                                    $szLanguageRedirectUrl = getFooterLanguageUrlNew($_SERVER['REQUEST_URI'],$langArrs['szLanguageCode'],$currentLangArr[0]['szLanguageCode']);
                                }
                                ?>
                                    <li><a href="<?php echo $szLanguageRedirectUrl; ?>"  ><?=ucwords($langArrs['szName']);?></a></li>
                                <?php
                            }
                        }
                    }
                }
               /* if(__LANGUAGE__==__LANGUAGE_TEXT_ENGLISH__)
                {  
                     ?>
                    <li><a><?=t($t_base.'footer/english');?></a></li>
                    <li><a href="<?php echo $szLanguageRedirDanishUrl; ?>"  ><?=t($t_base.'footer/danish');?></a></li>
                    <li><a><?=t($t_base.'footer/english');?></a></li>
                    <?php
                }
                else
                {
                    ?> 
                    <li><a href="<?php echo $szLanguageRedirEnglishUrl; ?>"><?=t($t_base.'footer/english');?></a></li>
                    <li><a><?=t($t_base.'footer/danish');?></a></li>
                    <li><a><?=t($t_base.'footer/english');?></a></li>
                    <?php
                }*/
            ?>
        </ul>-->
        <?php }
        
        $menurightarr=wp_get_nav_menu_items($menuRightFooter);
        //print_r($menuarr);
        $menurightarr=(toArray($menurightarr));
        if(empty($menurightarr))
        {
            $menurightarr=wp_get_nav_menu_items(__ENGLISH_RIGHT_FOOTER_MENU__);
            $menurightarr=(toArray($menurightarr));
        }
        ?>
    </div>
    <address>
        <div class="copyright">&copy; <?=DATE('Y');?> <?=t($t_base.'footer/copy_right');?></div>
        <div class="social-icons"><a href="<?php echo $szFacebookUrl;?>" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a><a href="<?php echo $szTwitterUrl;?>" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a><a href="<?php echo $szLinkedinUrl;?>" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a><a href="<?php echo $szYoutubeUrl;?>" target="_blank" class="youtube"><i class="fa fa-youtube-play"></i></a></div>
        <div class="links">
            <?php
            if(!empty($menurightarr))
            {
                foreach($menurightarr as $menurightarrs)
                {?>
                    <a href="<?php echo $menurightarrs['url'];?>" ><?=$menurightarrs['title'];?></a>
                   <?php 
                }
            }?>
        </div>
    </address>
</footer> 
<?php 
    if(__ENVIRONMENT__ =='LIVE')
    {
        echo $szImageStr; 
    } 
?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
<?php if(__ENVIRONMENT__ =='DEV_LIVE' || __ENVIRONMENT__ =='LIVE') { /* ?>
    <script async type="text/javascript"> document.write(unescape("%3Cscript src='//www.snapengage.com/snapabug.js' type='text/javascript'%3E%3C/script%3E"));</script> 
    <script type="text/javascript">
        SnapABug.init("c634a6f5-4705-4882-84e6-55de61deba29");
    </script>
<?php  
 */ } if(__ENVIRONMENT__ =='LIVE') {  		 
        if($iServicePageAdword==1) { 
?>
    <!-- Google Code for Se Priser - AdWords Sporingskode Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 968221154;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "oPo6CLGK3WMQ4sPXzQM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968221154/?label=oPo6CLGK3WMQ4sPXzQM&amp;guid=ON&amp;script=0"/>
       </div>
   </noscript>  
<?php } else if($iThankYouPageAdword==1) { 
    
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
    ?>			
    <!-- Google Code for Bestil Conversion Page -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 968221154;
        var google_conversion_language = "da";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "tv8iCLbzyggQ4sPXzQM";
        var google_remarketing_only = false;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968221154/?label=tv8iCLbzyggQ4sPXzQM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>  
<?php 
    }else if($iLanguage==__LANGUAGE_ID_ENGLISH__)
    {?>
        <!-- Google Code for Bestil - AdWords Sporingskode Conversion Page -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 880839276;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "9pKdCPm_lmcQ7JSCpAM";
        var google_remarketing_only = false;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/880839276/?label=9pKdCPm_lmcQ7JSCpAM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>

      <?php  
    }
    } else if($iGetRateGetQuotePageAdword==1){ ?>
    <!-- Google Code for S&oslash;gning Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 968221154;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "nBj_CK70yggQ4sPXzQM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968221154/?label=nBj_CK70yggQ4sPXzQM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>  
<?php }?> 
   
  <script type="text/javascript">
        !function(){var t=document.createElement("script");t.async=!0,t.type="text/javascript",t.src="//98229.hittail.com/mlt.js";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}(); 
    <?php if($_SESSION['HOLDININGPAGE']==''){?>    
    /*Start of Zopim Live Chat Script*/
        window.$zopim||function(t,e){var s=$zopim=function(t){s._.push(t)},n=s.s=t.createElement(e),c=t.getElementsByTagName(e)[0];s.set=function(t){s.set._.push(t)},s._=[],s.set._=[],n.async=!0,n.setAttribute("charset","utf-8"),n.src="//v2.zopim.com/?28qgkS9x5hAjidkz0ubaskHS1pQcLcMU",s.t=+new Date,n.type="text/javascript",c.parentNode.insertBefore(n,c)}(document,"script");
        <?php  if(!empty($szCustomerEmail)){ ?>
            $zopim(function() {
                $zopim.livechat.setName('<?php echo $szCustomerName; ?>');
                $zopim.livechat.setEmail('<?php echo $szCustomerEmail; ?>');
                $zopim.livechat.setPhone('<?php echo $szPhoneNumber; ?>');
                $zopim.livechat.setNotes('<?php echo $szCustomerNotes; ?>'); 
            });
        <?php } ?>
    </script> 
 <?php }}else if(__ENVIRONMENT__ =='DEV_LIVE' || __ENVIRONMENT__ =='ANILDEV'){ ?>
    <script type="text/javascript"> 
        /*Start of Goggle Analytical Script*/
        var _gaq=_gaq||[];_gaq.push(["_setAccount","UA-37029182-1"]),_gaq.push(["_trackPageview"]),function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"==document.location.protocol?"https://ssl":"http://www")+".google-analytics.com/ga.js";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}();
         <?php if($_SESSION['HOLDININGPAGE']==''){?> 
        /*Start of Zopim Live Chat Script*/ 
       
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="//v2.zopim.com/?4FNbPdhoctxbRDspJFPqNJhQUrjPD8TJ";z.t=+new Date;$.
        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");  

        <?php  if(!empty($szCustomerEmail)){ ?>
            $zopim(function() {
                $zopim.livechat.setName('<?php echo $szCustomerName; ?>');
                $zopim.livechat.setEmail('<?php echo $szCustomerEmail; ?>');
                $zopim.livechat.setPhone('<?php echo $szPhoneNumber; ?>');
                $zopim.livechat.setNotes('<?php echo $szCustomerNotes; ?>'); 
            });
 <?php } } ?> 
        /* End of Zopim Live Chat Script */
        $().ready(function(){var e=document.getElementsByName("google_conversion_frame");e.style="display:none"});
    </script>  
<?php }
?>
    <script type="text/javascript">
        $().ready(function() {	
       setTimeout(function(){     
        console.log(googleApiCall+"googleApiCall");
        if(googleApiCall.length>0)
        {
            var i=0;
            $(".pac-container").each(function(pac)
            {
                var pac_container_id=googleApiCall[i];
               // alert(pac_container_id+"pac_container_id");
                $(this).attr('id',pac_container_id);
                ++i;
            });
        }
    },1000);
    });
    </script>
  
    </body>
</html> 