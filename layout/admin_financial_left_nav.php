<?php
$billFlag=checkManagementPermission($kAdmin,"__OUTSTANDING__",2);
$settFlag=checkManagementPermission($kAdmin,"__SETTLED__",2);
$forpayFlag=checkManagementPermission($kAdmin,"__PAY_FORWARDER__",2);
$fortransFlag=checkManagementPermission($kAdmin,"__TRANSFER_DUE__",2);
$fortranshisFlag=checkManagementPermission($kAdmin,"__PAYMENT_HISTORY__",2);
$fortransinvFlag=checkManagementPermission($kAdmin,"__TRANSPORTECA_INVOICES__",2);
$pay_fwd_invFlag=checkManagementPermission($kAdmin,"__PAY_FORWARDER_INVOICE__",2);
$paid_fwd_invFlag=checkManagementPermission($kAdmin,"__FORWARDER_INVOICES__",2);
$transhisFlag=checkManagementPermission($kAdmin,"__TRANSACTION_HISTORY__",2);
$fwdturnFlag=checkManagementPermission($kAdmin,"__FORWARDER_TRUNOVER__",2);
$transturnFlag=checkManagementPermission($kAdmin,"__TRANSPORTECA_TRUNOVER__",2);
?>
<div class="hsbody-2-left account-links">
        <?php if($billFlag || $settFlag){?>
	<p><?=t('management/leftNav/fields/accounts_receivable');?></p>
	<ul>
            <?php if($billFlag){ ?>
            <li <?php if($_REQUEST['leftmenuflag']=='bill'){?>class="active" <?php }?>><a href="<?=__MANAGEMENT_BILLING_URL__?>"><?=t('management/leftNav/fields/outstanding');?></a></li>
            <?php } if($settFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='sett'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_SETTLED_URL__?>"><?=t('management/leftNav/fields/settled');?></a></li>
            <?php } ?>
        </ul>
        <?php }?>
        <?php if($forpayFlag || $fortransFlag || $fortranshisFlag || $fortransinvFlag || $pay_fwd_invFlag || $paid_fwd_invFlag){?>
	<p style="margin: 15px 0 5px;"><?=t('management/leftNav/fields/accounts_payable');?></p>
	<ul>
            <?php if($forpayFlag){ ?>
            <li <?php if($_REQUEST['leftmenuflag']=='forpay'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_FORWARDER_PAYMENT_URL__?>"><?=t('management/leftNav/fields/pay_forwarder');?></a></li>
            <?php } if($fortransFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='fortrans'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_FORWARDER_TRANSFER_URL__?>"><?=t('management/leftNav/fields/transfer_due');?></a></li>
            <?php } if($fortranshisFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='fortranshis'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_FORWARDER_PAYMENT_HISTORY_URL__?>"><?=t('management/leftNav/fields/pay_forwarder_history');?></a></li>
            <?php } if($fortransinvFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='fortransinv'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_TRANSPORTECA_INVOICE_URL__?>"><?=t('management/leftNav/fields/forwarder_transporteca_invoices');?></a></li>
            <?php } if($pay_fwd_invFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='pay_fwd_inv'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_PAY_FORWARDER_INVOICE_URL__?>"><?=t('management/leftNav/fields/pay_forwarder_invoices');?></a></li>
            <?php } if($paid_fwd_invFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='paid_fwd_inv'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_PAID_FORWARDER_INVOICE_URL__?>"><?=t('management/leftNav/fields/forwarder_invoices');?></a></li> 
            <?php } ?>
        </ul>
        <?php }?>
        <?php if($transhisFlag){ /* ?>
            <p style="margin: 15px 0 5px;"><?=t('management/leftNav/fields/forwarder_view');?></p>
            <ul>
                <li <?php if($_REQUEST['leftmenuflag']=='transhis'){?>class="active" <?php }?> ><a href="<?=__MANAGEMENT_TRANSACTION_HISTORY_URL__?>"><?=t('management/leftNav/fields/transaction_history');?></a></li>
            </ul>
        <?php */ } ?>
        <?php if($fwdturnFlag || $transturnFlag){ ?>
        <p style="margin: 15px 0 5px;"><?=t('management/leftNav/fields/accounting');?></p>
	<ul>
            <?php if($fwdturnFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='fwdturn'){?>class="active" <?php }?> ><a href="<?php echo __MANAGEMENT_FORWARDER_TURNOVER_URL__?>"><?=t('management/leftNav/fields/forwarder_turnover');?></a></li>
            <?php } if($transturnFlag){?>
            <li <?php if($_REQUEST['leftmenuflag']=='transturn'){?>class="active" <?php }?> ><a href="<?php echo __MANAGEMENT_TRANSPORTECA_TURNOVER_URL__?>"><?=t('management/leftNav/fields/transporteca_turnover');?></a></li>
            <?php }?>
        </ul>
        <?php }?>
</div>