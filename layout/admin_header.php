<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" ); 
include_classes();
require_once( __APP_PATH__ . "/inc/admin_functions.php" );
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
if(!empty($_SERVER['HTTP_HOST']))
{
    define ('__BASE_URL__', __BASE_MANAGEMENT_URL__);
    define ('__BASE_URL_SECURE__', __BASE_SECURE_MANAGEMENT_URL__);
    define_management_constants(); 
}

define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
if(empty($szMetaTitle))
{
    $szMetaTitle = "Management Landing page ";
}
$t_base = "management/header/";
//validateManagement();
$kAdmin=new cAdmin();
 
if((int)$_SESSION['admin_id']>0)
{
    $idAdmin = $_SESSION['admin_id'];
}
if((int)$idAdmin>0)
{
    $kAdmin->getAdminDetails($idAdmin);
    $checkPerissionDashboardFlag=checkManagementPermission($kAdmin,"__DASHBOARD__",1);
    $checkPerissionFilesFlag=checkManagementPermission($kAdmin,"__FILIES__",1);
    $checkPerissionContentFlag=checkManagementPermission($kAdmin,"__CONTENT__",1);
    $checkPerissionOperationsFlag=checkManagementPermission($kAdmin,"__OPERATIONS__",1);
    $checkPerissionFinancialFlag=checkManagementPermission($kAdmin,"__FINANCIAL__",1);
    $checkPerissionSystemFlag=checkManagementPermission($kAdmin,"__SYSTEM__",1);
    $checkPerissionMessageFlag=checkManagementPermission($kAdmin,"__MESSAGE__",1);
    $checkPerissionUserFlag=checkManagementPermission($kAdmin,"__USER__",1);
    $checkPerissionQuickQuoteFlag = checkManagementPermission($kAdmin,"__QUICK_QUOTE__",1);
    //echo $_REQUEST['menuflag']."menuflag";
    if($_REQUEST['menuflag']!='')
    {
        if(!$checkPerissionUserFlag && !$checkPerissionMessageFlag && !$checkPerissionSystemFlag && !$checkPerissionFinancialFlag && !$checkPerissionOperationsFlag && !$checkPerissionFilesFlag && !$checkPerissionContentFlag && !$checkPerissionDashboardFlag)
        {
            $redirect_url=__MANAGEMENT_BLANK_PAGE_FEE__;
            header("Location:".$redirect_url);
            exit();
        }
        else
        {
            if(!$checkPerissionDashboardFlag && $_REQUEST['menuflag']=='dash')
            {
                $redirect_url=__MANAGEMENT_BLANK_PAGE_FEE__;
                header("Location:".$redirect_url);
                exit();
            }
            else
            {
                redirectPagePermission($_REQUEST['menuflag'],$checkPerissionUserFlag,$checkPerissionMessageFlag,$checkPerissionSystemFlag,$checkPerissionFinancialFlag,$checkPerissionOperationsFlag,$checkPerissionFilesFlag,$checkPerissionContentFlag,$checkPerissionDashboardFlag,PAGE_PERMISSION,$kAdmin);
            }
        }
     }
}
/**
  *	THIS BELOW SECTON IS FOR CONTENT -- Explain pages
  * 
  */	
if( isset($_REQUEST['menuflag']) && ($_REQUEST['menuflag'] == 'content') )
{
    $kExplain = new cExplain();
    $iType = sanitize_all_html_input($_REQUEST['type']);
    if(isset($_REQUEST['flag']) && $_REQUEST['flag']!='')
    {
        $explianFlag = sanitize_all_html_input($_REQUEST['flag']);
        $content = $kExplain->selectExplainId($explianFlag,$iType,$_SESSION['session_admin_language']);
        if($content != array())
        {
            $szMetaTitle = $content['szMetaTitle'];
        }
    }
    else if(isset($_REQUEST['flag']))
    {
        $content = $kExplain->selectExplainId(false,$iType);
        header('location:'.__MANAGEMENT_EXPLAIN_URL__.$content['szLink']);
        die;
    }
}
$kBooking = new cBooking(); 

/**
* SECTION ENDS HERE
*/ 
  
/*
* This function was used to put cursor in szFreeFieldHeader on click of '/' shortcut
* onkeyup="auto_select_search_box(event)"
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <script type="text/javascript">
            if(document.location.protocol == 'https:')
            {
               var __JS_ONLY_SITE_BASE__ = "<?=__BASE_URL_SECURE__?>";
               var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
            }
            else
            {
               var __JS_ONLY_SITE_BASE__ = "<?=__BASE_URL__?>";
               var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
            }      
            var NiceEditorInstance;
            var NiceEditorInstance_popup;
            var NiceEditorInstanceCRM=[];
            var __JS_ONLY_VOGA_CATEGORY_LISTING__ = {};
            var __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__ = {};
            var countriesArr = new Array(); 
            
       </script>	
        <link rel="icon" id="favicon_icon_1" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/management_fav_icon.png" />  
        <link rel="shortcut icon" id="favicon_icon_2" href="<?=__BASE_STORE_IMAGE_URL__?>/management_fav_icon.png" />
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if(!empty($szMetaKeywords)){?><meta name="keywords" content="<?=$szMetaKeywords; unset($szMetaKeywords);?>" /><?}?>
        <?php if(!empty($szMetaDescription)){?><meta name="description" content="<?=$szMetaDescription; unset($szMetaDescription);?>" /><?}?>
        <title id="metatitle"><? if(!empty($szMetaTitle)){ echo $szMetaTitle; unset($szMetaTitle);}?></title>
        <link href='http://fonts.googleapis.com/css?family=Roboto:700italic,700,400italic,400' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
        <?php if($addFile==1){ ?>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.1.8.2.js"></script>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-ui.min.js"></script> 
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.fileuploadmulti.min.js?v=<?php echo time(); ?>"></script> 
        <?php } ?>
            <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_MANAGEMENT_URL__?>/style.css?v=<?php echo time(); ?>" />
            <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_MANAGEMENT_URL__?>/uploadfilemulti.css?v=<?php echo time(); ?>" />
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js?v=<?php echo time(); ?>"></script>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/forwarder.js?v=<?php echo time(); ?>"></script>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/admin.js?v=<?php echo time(); ?>"></script>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/pendingTray.js?v=<?php echo time(); ?>"></script>
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/admin-internal.js?v=<?php echo time(); ?>"></script> 
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js?v=<?php echo time(); ?>'></script>
            <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css?v=<?php echo time(); ?>" />
            <link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
            <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>
            <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
            <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js?v=2'></script>
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jQuery.zclip.min.js?v=2'></script>
            <!--<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jQuery.popupWindow.js'></script>-->
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/highcharts.js?v=2'></script>
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/highchart-theme.js?v=2'></script>
            <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/exporting.js?v=2'></script> 
            <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__."/jQuery.nice.editor.min.js"; ?>"></script>
        </head>
        <body class="mainpage">
		<div id="header">
			<a href="<?=__MANAGEMENT_URL__?>">
                            <img src="<?=__BASE_STORE_IMAGE_URL__?>/transporteca-logo-new.png" alt="Transporteca" title="Transporteca"  class="logo" />
			</a>
                        <?php if((int)$_SESSION['admin_id']>0){?>
                        <form name="headerSearchForm" id="headerSearchForm" method="post" action="javascript:void(0);" onkeypress="submitEnterHeaderSearch(event)">
                            <input type="text" name="szFreeFieldHeader" autocomplete="off" id="szFreeFieldHeader" value="<?php echo $_POST['szFreeFieldHeader'];?>" placeholder="Search.."/>
                        </form>    
                    <?php }?>
			<div class="header-links">
				<?php if((int)$_SESSION['admin_id']>0){?> 
                                <div style="font-style: normal;" class="user" onmouseover='$("#show_link").attr("style","display:block;margin-top:-10px;");' onmouseout='$("#show_link").attr("style","display:none;margin-top:-10px;");'><a href="javascript:void(0);" ><div id="admin_name"><?=$kAdmin->szFirstName?>&nbsp;</div></a>
                                    <div id="show_link" style="display:none;">
                                        <p class="logout_account"><a href="<?=__MANAGEMENT_LOGOUT_URL__?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/logout-icon.png" alt="Logout" title="Logout" border="0" /></a></p>
                                        <p class="user_account"><a href="javascript:void(0);" onclick="openChangePasswordPopup();"><?=t($t_base.'message/change_password');?></a></p>
                                    </div>
				</div> 
				<?php }else{?>
				<!-- <p class="user"><a href="javascript:void(0)" id="login"><?=t($t_base.'message/sign_in');?></a></p> -->				
				<?php }?>
                            
                                <div class="header-quote-container">
                                    <!--
					<a href="http://www.facebook.com/MyTransporteca" target="_blank" class="facebook-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/facebook-icon.png" alt="facebook" title="facebook" border="0" /></a>
					<a href="http://twitter.com/transporteca" target="_blank" class="twitter-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/twitter-icon.png" alt="Twitter" title="Twitter" border="0" /></a>
					<a href="http://www.linkedin.com/company/transporteca" target="_blank" class="linkedin-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/linkedin-icon.png" alt="Linkedin" title="Linkedin" border="0" /></a>
					<a href="<?=__MAIN_SITE_HOME_PAGE_URL__.'/rss.php'?>" class="rss-icon"><img src="<?=__BASE_STORE_IMAGE_URL__?>/rss-icon.png" alt="RSS" title="RSS" border="0" /></a>
                                     -->
                                     <?php if((int)$_SESSION['admin_id']>0){?> 
                                     <a href="javascript:void(0);" class="contacts" onclick="display_remind_pane_add_popup('','','','','FROM_HEADER');">Contacts</a>
                                     
                                     <?php } if($checkPerissionQuickQuoteFlag){?>
                                        <a href="<?php echo __BASE_URL__."/quickQuote/"; ?>" class="quick_quote"><?=t($t_base.'message/quick_quote');?></a>
                                     <?php } ?>
                                     <?php 
                                    if(__ENVIRONMENT__ != "ANILDEV")
                                    {
                                        if((int)$_SESSION['admin_id']>0)
                                        {
                                            $iTotalNumBookingReceived = 0;
                                            $adminHeaderDataAry = array();
                                            $adminHeaderDataAry = $kBooking->getTotalBookingRecievedCount();
                                            $iNumUsersBooking = $kBooking->getUsersCount();
                                            
                                            if($adminHeaderDataAry['iTotalNumBooking']>0)
                                            {
                                                $fAvgSaleRevenue = number_format((float)($adminHeaderDataAry['fSaleRevenue']/$adminHeaderDataAry['iTotalNumBooking'])) ;
                                            }
                                            $szHeadingTextStr = number_format((int)$adminHeaderDataAry['iTotalNumBooking'])." / ".number_format((int)$adminHeaderDataAry['iTotalNumUniqueUser'])." / ".number_format((int)$iNumUsersBooking)." / ".$fAvgSaleRevenue;
                                        
                                        ?>
                                        <strong><?php echo $szHeadingTextStr ?></strong>
                                    <?php } } ?>
				</div>
				</div>
		</div>
                <?php
                if($_POST['szFreeFieldHeader']!='')
                {
                    $searchTaskAry['szFreeText']=$_POST['szFreeFieldHeader'];
                    $iPageNumber='1';
                    $searchTaskAry['iFromPopup'] = 1 ;
                    $incompleteTaskArySearch = $kBooking->getAllBookingWithOpenedTaskSearchPopup($searchTaskAry,false,false,$iPageNumber);
                    if(count($incompleteTaskArySearch)==1)
                    {
                        $kBookingNew = new cBooking();
                        $idBookingFile=$incompleteTaskArySearch[0]['idBookingFile'];
                        $kBookingNew->loadFile($idBookingFile);
                    }
                }?>
		<div id="contactPopup" <?php if(count($incompleteTaskArySearch)>1){?> style="display:block;" <?php }?>>
                    <?php
                    if(count($incompleteTaskArySearch)>1)
                    { 
                        $_SESSION['searchPendingTaskAry']['szFreeText'] = $_POST['szFreeFieldHeader']; 
                        $_POST['searchPendingTaskAry']['szFreeText'] = $_POST['szFreeFieldHeader'];
                        display_pending_task_popup($searchTaskAry,'SEARCH_TASK',$kBooking,$incompleteTaskArySearch);
                        ?>
                        <script>
                        $('body').addClass('popup-body-scroll');
                        $("#contactPopup").focus();
                        var openDiv = 'task-popup-id-recent'; 
                        $(document).click(function(e) {
                             if (!$(e.target).closest('#'+openDiv).length) {

                                 var disp = $("#ui-datepicker-div").css('display');
                                 if(disp=='block')
                                 {
                                     console.log("Date picker is active");
                                 }
                                 else
                                 {
                                     $("#contactPopup").attr('style','display:none;'); 
                                     $(document).unbind('click');
                                 } 
                                 $('body').removeClass('popup-body-scroll'); 
                             }
                         });
                        </script>
                    <?php
                    }?>
                </div>
                
		<div id="remindEmailPopup"></div>
		<div id="ajaxLogin"></div>
		<?php
			$header_flag = (!empty($_REQUEST['menuflag']))?sanitize_all_html_input(trim($_REQUEST['menuflag'])):'';	
		?>	
		<div id="navigation">
		<ul>
                    <?php if($checkPerissionDashboardFlag){?>
                    <li <?php if($header_flag=='dash') {?> class="active" <? }?>>
                    <a href="<?=__MANANAGEMENT_DASHBOARD_URL__?>"><span><?=t($t_base.'title/dashboard');?></span><br/><?=t($t_base.'title/quick_over_view');?></a>	
                    </li>
                    <?php } if($checkPerissionFilesFlag){?>
                    <li <?php if($header_flag=='file') {?> class="active" <? }?>>
                    <a href="<?=__MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?>" ><span><?=t($t_base.'title/files');?></span><br/><?=t($t_base.'title/pending_tray');?></a>	
                    </li>
                    <?php } if($checkPerissionContentFlag){?>
                    <li <?php if($header_flag=='content') {?> class="active" <? }?>>
                    <a href="<?=__MANANAGEMENT_MANAGE_EXPLAIN_PAGE_URL__?>" ><span><?=t($t_base.'title/content');?></span><br/><?=t($t_base.'title/update_publish');?></a>	
                    </li>
                    <?php } if($checkPerissionOperationsFlag){?>
                    <li style="width:13%" <?php if($header_flag=='opr') {?> class="active" <? }?>>
                    <a href="<?=__MANAGEMENT_OPERATIONS__STATISTICS_URL__?>"><span><?=t($t_base.'title/operations');?></span><br/><?=t($t_base.'title/maintainance_detail');?></a>	
                    </li>
                    <?php } if($checkPerissionFinancialFlag){?>
                    <li style="width:13%" <?php if($header_flag=='fin') {?> class="active" <? }?>>
                    <a href="<?=__MANAGEMENT_BILLING_URL__;?>" ><span><?=t($t_base.'title/financial');?></span><br/><?=t($t_base.'title/payments_billings');?></a>	
                    </li>
                    <?php } ?>
                    <?php if($checkPerissionSystemFlag){?>
                    <li  <?php if($header_flag=='tncfwd') {?> class="active" <? }?>>
                    <a href="<?=__MANAGEMENT_COUNTRY_URL__;?>" ><span><?=t($t_base.'title/system');?></span><br/><?=t($t_base.'title/text_and_variable');?></a>	
                    </li>
                    <?php } if($checkPerissionMessageFlag){?>
                    <li <?php if($header_flag=='msg') {?> class="active" <? }?>>
                    <a href="<?=__MANAGEMENT_MESSAGE_CUSTOMER_URL__;?>" ><span><?=t($t_base.'title/message');?></span><br/><?=t($t_base.'title/send_review');?></a>	
                    </li>
                    <?php } if($checkPerissionUserFlag){?>
                    <li style="margin-right:0;width:12%;" <?php if($header_flag=='graph') {?> class="active" <? }?>>
                    <a href="<?=__MANAGEMENT_GRAPH_URL__;?>"><span><?=t($t_base.'title/user');?></span><br/><?=t($t_base.'title/details_and_access');?></a>	
                    </li>
                    <?php }?>
		</ul>
		</div>
		<div style="clear:both; "></div>
            <?php 
                //unsetting values
                $dash_board_header_ary=array();
                $header_flag=array();
                if(isset($idAdmin) && (int)$idAdmin>0)
                {
                    if((int)$kAdmin->iPasswordUpdated==1)
                    {
                        checkForgotPasswordUpdated();
                    }
                    ?>
                    <div id="admin_header_notification_container_div" style="text-align: center;">
                        <?php echo showAdminChecks(true);?>
                    </div>
                    <?php
                }
	?>	
                
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div> 
<script type="text/javascript">
//    $("#szFreeFieldHeader" ).on("keyup keypress keydown",function(e) {  
//       var val_2 = this.value; 
//       if(val_2==='/')
//       { 
//          // this.value='';
//       }
//    });
</script>