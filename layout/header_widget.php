<?php
ob_start();
session_start(); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );

//$szGlobalLanguage = get_language_name();
require_once( __APP_PATH__ . "/inc/constants.php" );

require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes();
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once(__APP_PATH__."/ipcheck/userip/ip.codehelper.io.php");
require_once(__APP_PATH__. "/ipcheck/userip/php_fast_cache.php");

$display_abandon_message=false; 
$szLanguage = sanitize_all_html_input($_REQUEST['lang']);
 
if($page_404==1)
{
	$szLanguage = $_SESSION['transporteca_language_en']; 
}

if(!empty($szLanguage))
{
	define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__."/".__TRANSPORTECA_DANISH_URL__);
	define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__);
   	define_constants($szLanguage); 
   	
	$_SESSION['transporteca_language_en'] = $szLanguage ;
}
else
{
	$szLanguage = 'english';
	define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
	define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__);
	define_constants($szLanguage);
	
	$_SESSION['transporteca_language_en'] = '';  
}

$lang_code = __LANGUAGE__;
I18n::add_language(strtolower($lang_code));

$iLanguage = getLanguageId();

if((int)$_SESSION['LANGUAGE_CHANGED_BY_USER']==0 && $iLanguage!=__LANGUAGE_ID_DANISH__)
{
	if(!empty($_COOKIE['__USER_COUNTRY_NAME___']))
	{
		$szUserCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ;
	}
	else
	{
		$_ip = new ip_codehelper();
		$real_client_ip_address = $_ip->getRealIP();
		$visitor_location = $_ip->getLocation($real_client_ip_address);
		$szUserCountryName = $visitor_location['CountryCode2'];
		
		setCookie('__USER_COUNTRY_NAME___', $szUserCountryName ,time()+3600, '', $_SERVER['HTTP_HOST']);
	}
	
	if($szUserCountryName=='DK')
	{
		if(substr($_SERVER['REQUEST_URI'], 0, 4) === '/da/')
		{
			$redir_da_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$_SERVER['REQUEST_URI'];
		}
		else
		{
			$redir_da_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__."".$_SERVER['REQUEST_URI'];
		}
		
		$_SESSION['LANGUAGE_CHANGED_BY_USER'] = 1;
		ob_end_clean();
		header("Location:".$redir_da_url);
		die;
	}
}

if(empty($szMetaTitle))
{
	$szMetaTitle = "Landing page ";
}

$t_base = "home/";
$kUser= new  cUser();
if((int)$_SESSION['user_id']>0)
{
	$kUser->getUserDetails($_SESSION['user_id']);
	$kUserNew = new cUser();
	$kUserNew->loadCurrency($kUser->szCurrency);
	
	if(($kUserNew->iBooking<=0) && ($_REQUEST['flag']=='cb')) // User is in booking steps and currency is not active.
	{
		//echo "<div id='Transporteca_popup'>";
		//echo displayCurrencyChangePopup($kUser->szCurrency);
		//echo "</div>";
	}
} 
$landing_page_url = __LANDING_PAGE_URL__ ;

$kBooking = new cBooking();
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

/**
  *	THIS BELOW SECTON IS FOR CONTENT -- Explain pages
  * 
  */

	
if( isset($_REQUEST['flag']) && ($_REQUEST['flag'] == 'ep') )
{
	if(isset($_REQUEST['menuflag']) && $_REQUEST['menuflag']=='ind')
	{
		//print_r($_REQUEST);
		//$_REQUEST['menuflag']  = str_replace('/','',$_REQUEST['menuflag']);
		$kExplain = new cExplain;
		
		$szLink=sanitize_all_html_input(trim($_REQUEST['szLink']));
	
		$levelExplainDataArr=$kExplain->selectExplainId($szLink,$mainTab);
	
		$szUrl = sanitize_all_html_input($_REQUEST['szUrl']);
		$content = $kExplain->getExplainContentByUrl($szUrl,$levelExplainDataArr['id']);
		if($content != array())
		{
			$idExplain = $content['id'];
			$szMetaTitle = $content[0]['szMetaTitle'];
			$szMetaKeywords = $content[0]['szMetaKeyWord'];
			$szMetaDescription = $content[0]['szMetaDescription'];
		}
	}

}

/**
  *	 	SECTION ENDS HERE
  */
  
  /**
  *	THIS BELOW SECTON IS FOR  -- Blog pages
  * 
  */


if( isset($_REQUEST['menuflag']) && ($_REQUEST['menuflag'] == 'blg') )
{
	if(isset($_REQUEST['flag']) && $_REQUEST['flag']!='')
	{
		$_REQUEST['flag']  = str_replace('/','',$_REQUEST['flag']);
		$kExplain = new cExplain;
		$link = $kExplain->selectBlogLink($_REQUEST['flag'],$iLanguage);
		if($link != array())
		{
			$szMetaTitle = ($link['szMetaTitle']?$link['szMetaTitle']:'');
			$szMetaKeywords = ($link['szMetaKeyWord']?$link['szMetaKeyWord']:'');
			$szMetaDescription = ($link['szMetaDescription']?$link['szMetaDescription']:'');	
		}
	}
	else
	{
		$kExplain = new cExplain;
		$link = $kExplain->selectBlogLink(false,$iLanguage);
		if($link != array())
		{
			//$szMetaTitle = ($link['szMetaTitle']?$link['szMetaTitle']:'');
			//$szMetaKeywords = ($link['szMetaKeyWord']?$link['szMetaKeyWord']:'');
			//$szMetaDescription = ($link['szMetaDescription']?$link['szMetaDescription']:'');
			$_REQUEST['flag'] = $link['szLink'];
			header('Location:'.__BLOG_PAGE_URL__.$_REQUEST['flag'].'/');
			die;
		}
	} 
} 

$confirm_key = $_REQUEST['myBookingkey'];
if(!empty($confirm_key))
{
	$keyAry = explode('__',$confirm_key);
	$szConfirmationBookingKey = $keyAry[0];	
}

if((int)($_SESSION['user_id']<=0) && ((int)$szConfirmationBookingKey>0))
{
	$_SESSION['customer_booking_confirmation_key'] = $szConfirmationBookingKey;
}

/*
 * Getting management var for social media link
 */

$kWhsSearch = new cWHSSearch();
$kExplain = new cExplain;
$kSEO = new cSEO();
$iLanguage = getLanguageId();

$howDoesworksLinks = array();
$howDoesworksLinks = $kExplain->selectPageDetailsUserEnd($iLanguage,1);
	
$companyLinksAry = array();
$companyLinksAry = $kExplain->selectPageDetailsUserEnd($iLanguage,2);

$tranportpediaLinksAry = array();
$tranportpediaLinksAry = $kExplain->selectPageDetailsUserEnd($iLanguage,3);

$seoPageHeaderAry = array();
$seoPageHeaderAry = $kSEO->getAllPublishedSeoPages($iLanguage);

$blogPageHeaderAry = array();
$blogPageHeaderAry = $kExplain->selectBlogDetailsActive($iLanguage);

if($iLanguage==__LANGUAGE_ID_DANISH__)
{
	$szLang = 'da';
}
else
{
	$szLang = 'en';
}

?>
<!doctype html>
<html lang="<?php echo $szLang; ?>">
	<head>
	<script type="text/javascript">
                  if (document.location.protocol == 'https:')
                  {
                        var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                         var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
                  }
                  else
                  {
                        var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                         var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
                  }                 
      </script>	  
       
	   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
	   <title id="metatitle"><? if(!empty($szMetaTitle)){ echo $szMetaTitle; }?></title>
	   <?php if(!empty($szMetaKeywords)){?>
	   		<meta name="keywords" content="<?=$szMetaKeywords;?>" />
	   <?php } if(!empty($szMetaDescription)){?>
	   		<meta name="description" content="<?=$szMetaDescription;?>" />
	   <? } ?>
	    
	   <meta name="viewport" content="width=device-width, user-scalable=no">
	   <meta property="og:locale" content="da_DA" />
	   <meta property="og:type" content="website" />
	   <meta property="og:site_name" content="Transporteca" />
	   		
	   <meta property="og:title" content="<?php echo $szMetaTitle; ?>" />
	   <meta property="og:description" content="<?php echo $szMetaDescription; ?>" />
	    
	   <?php  if($_REQUEST['menuflag']=='agg'){ ?>		
			<link rel="publisher" href="<?php echo $szOpenGraphAuthor; ?>"/> 
			<meta property="og:url" content="<?php echo $szAggregatePageUrl; ?>" /> 
			<meta property="og:image" content="<?php echo $szOpenGraphImagePath; ?>" />
		<?php  } ?>
	   
	    	<?php
	    	 
	    		define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images"); 
	    	
	    	if(__ENVIRONMENT__ == "LIVE")
	    	{
	    ?> 
	    
    		<!-- CSS FILES  --> 
    		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    		<link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/transporteca.min.css?v=3" rel="stylesheet" />  
    		
    		<!-- JS FILES  -->	
    		<script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js?v=2" type="text/javascript"></script>
    				 
			<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.min.js?v=2"></script>
			<script async src="<?php echo __BASE_STORE_SECURE_JS_URL__?>/jquery.bxslider.min.js?v=2"></script> 
			<script async type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js?v=2"></script>
			<script async type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js?v=2"></script>
			<script async type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js?v=2'></script>
			<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js?v=2"></script> 
			 			
			<script async type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js?v=2"></script>
			<script async type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js?v=2"></script> 
			<script async type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/modernizr.custom.79639.js?v=2"></script>
			<script async type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js?v=2"></script> 
			<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js?v=2'></script>
			
		<?php } else { ?>
			<!-- CSS FILES  --> 
    		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    		<link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/jquery.bxslider.css" rel="stylesheet" />
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/custom-style.css" /> 
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/new-layout.css" />   
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
    		<link type="text/css" rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" /> 
    		
    		<!-- JS FILES  -->	
			<script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>		
			<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
			<script src="<?php echo __BASE_STORE_SECURE_JS_URL__?>/jquery.bxslider.min.js"></script>
			 
			<!--  
			COMBINED ALL .JS INTO ONE FILE.
			<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/combined.min.js"></script>
			 -->
			<script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
			<script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
			<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
			<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>  			
			<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
			<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
			<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/modernizr.custom.79639.js"></script> 
			<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js"></script> 
			<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js'></script>
		<?php } ?>  
</head>
<body <?php if($iNewServicePage==1 || $iEmailVerificationPage==1){ echo "class='new-searvice-page-main-container'"; } ?>> 
<div id="Transporteca_popup"></div>
<div id="signin_help_pop" class="help-pop"></div>
<div id="leave_page_div" style="display:none;">	</div>
<div id="ajaxLogin"></div>
<div id="contactPopup"></div>
 
<?php 

$kWHSSearch = new cWHSSearch();
$szGoogleMapV3Key = $kWHSSearch->getManageMentVariableByDescription('__GOOGLE_MAP_V3_API_KEY__');
  
?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=en" type="text/javascript"></script>

<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="customs_clearance_pop" class="tooltip-pop"></div>
<div id="user_account_login_popup" style="display:none;"></div>