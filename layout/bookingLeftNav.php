<div class="hsbody-2-left account-links"> 
    <p class="my-booking-heading f-size-16"><?=t($t_base.'links/my_booking');?></p>
    <ul>
        <li <?php if($_REQUEST['flag']=='mb'){?>class="active booking_left_nav" <?php } else {?>class="booking_left_nav"<?php }?> id="booking_left_nav_active"><a href="<?=__BASE_URL__?>/myBooking"><?=t($t_base.'links/active_booking');?></a></li>
        <li <?php if($_REQUEST['flag']=='hldb'){?>class="active booking_left_nav" <?php } else {?>class="booking_left_nav"<?php }?> id="booking_left_nav_hold"><a href="javascript:void(0)" onclick="mybooking_change_tab('hold');"><?=t($t_base.'links/hold_booking');?></a></li>
        <li <?php if($_REQUEST['flag']=='drfb'){?>class="active booking_left_nav" <?php } else {?>class="booking_left_nav"<?php }?> id="booking_left_nav_draft"><a href="javascript:void(0)" onclick="mybooking_change_tab('draft');"><?=t($t_base.'links/draft_booking');?></a></li>
        <li <?php if($_REQUEST['flag']=='arcb'){?>class="active booking_left_nav" <?php } else {?>class="booking_left_nav"<?php }?> id="booking_left_nav_archive"><a href="javascript:void(0)" onclick="mybooking_change_tab('archive');"><?=t($t_base.'links/archive_booking');?></a></li> 
    </ul>
</div>