<?php
/**
 * Ajax Header file
 */
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes(); 
  
$szLanguage = $_SESSION['transporteca_language_en']; 
 
if($iGetLanguageFromRequest==1)
{ 
    $szLanguage = $_REQUEST['lang'] ;
    if(!empty($szLanguage))
    {
        $_SESSION['transporteca_language_en'] = $szLanguage ;
    }
} 
if(!empty($szLanguage))
{ 
    setSiteUrlsByLanguageName($szLanguage);
}
else
{ 
    $szLanguage = strtolower(__LANGUAGE_TEXT_ENGLISH__);
    define ('__BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define ('__BASE_URL_SECURE__', __MAIN_SITE_HOME_PAGE_SECURE_URL__); 
    define ('__HOME_PAGE_BASE_URL__', __MAIN_SITE_HOME_PAGE_URL__);
    define_constants($szLanguage);
    $_SESSION['transporteca_language_en'] = $szLanguage;
}

if(__ENVIRONMENT__ == "LIVE")
{
    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_SECURE_URL__."/images");
}
else 
{
    define("__BASE_STORE_IMAGE_URL__",__MAIN_SITE_HOME_PAGE_URL__."/images");
} 
$lang_code=__LANGUAGE__;
//echo "<br> language ".$lang_code ;
I18n::add_language(strtolower($lang_code));

$kUser= new  cUser();
if((int)$_SESSION['user_id']>0)
{
    $kUser->getUserDetails($_SESSION['user_id']);
} 

?>  
<html lang="<?php echo $szLang; ?>">
    <head>
        <script type="text/javascript">
            if (document.location.protocol == 'https:')
            {
                var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
            }
            else
            {
                var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_URL__?>";
                var __JS_ONLY_SITE_BASE_IMAGE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
            }          
            var __GLOBAL_SMALL_COUNTRY_LIST__ = new Array();
            <?php 
                if(!empty($smallCountryAry))
                {
                    foreach($smallCountryAry as $countryIso=>$smallCountryArys)
                    {
                        ?>
                            __GLOBAL_SMALL_COUNTRY_LIST__.push('<?php echo $countryIso; ?>');
                        <?php
                    }
                }
            ?> 
        </script>	
            <?php if(__ENVIRONMENT__ == "LIVE") {  ?>   
                    <!--<link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/font-awesome.min.css' rel='stylesheet' type='text/css'>-->
<!--                    <link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/transporteca.min.css?v=4" rel="stylesheet" />  
                     Loading CSS FILES  
                    <script>var cb_1=function(){var e=document.createElement("link");e.rel="stylesheet",e.href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300";var t=document.getElementsByTagName("head")[0];t.parentNode.insertBefore(e,t)},raf=requestAnimationFrame||mozRequestAnimationFrame||webkitRequestAnimationFrame||msRequestAnimationFrame;raf?raf(cb_1):window.addEventListener("load",cb_1);</script>-->
                      
        
                <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    		<link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/jquery.bxslider.css" rel="stylesheet" />
                <!--<link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/bootstrap.min.css' rel='stylesheet' type='text/css'>-->
                <link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/font-awesome.min.css' rel='stylesheet' type='text/css'>
                
                <?php if($iMyAccountPage==1){?>
                    <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/style.css" />
                <?php }?>
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/custom-style.css" /> 
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/new-layout.css" />   
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
    		<link type="text/css" rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" /> 
                
                    <!-- Loading JS FILES  --> 
                    <script src="<?php echo __BASE_STORE_JS_URL__;?>/transporteca.min.js" type="text/javascript"></script>   
                    <script type="text/javascript" defer="1">
                        !function(){var e=document.createElement("script");e.async=!0,e.type="text/javascript",e.src='<?php echo __BASE_STORE_JS_URL__."/transporteca-internal.min.js";?>';var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}();
                        !function(){var e=document.createElement("script");e.async=!0,e.type="text/javascript",e.src='<?php echo __BASE_STORE_JS_URL__."/fancybox/fancy-box-min.js";?>';var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}();
                    </script>
                    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js?t=<?php echo time(); ?>"></script>
                    <script src="//cdn.optimizely.com/js/3407690608.js"></script> 
		<?php } else { ?>
			<!-- CSS FILES  --> 
    		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    		<link type="text/css" href="<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/jquery.bxslider.css" rel="stylesheet" />
                <!--<link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/bootstrap.min.css' rel='stylesheet' type='text/css'>-->
                <link href='<?php echo __BASE_STORE_SECURE_CSS_URL__; ?>/font-awesome.min.css' rel='stylesheet' type='text/css'>
                
                <?php if($iMyAccountPage==1){?>
                    <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/style.css" />
                <?php }?>
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/custom-style.css" /> 
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/new-layout.css" />   
    		<link type="text/css" rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
    		<link type="text/css" rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" /> 
    		    		
    		<!-- JS FILES  -->	
                <script src="<?php echo __BASE_STORE_JS_URL__;?>/jquery.1.8.2.js" type="text/javascript"></script>
                <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.number.js'></script>
                <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/JQuery.custom.min.js'></script> 
                 <!-- JS FILES  -->	 
                    <!-- 
                   <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/transporteca.min.js"></script>
                    COMBINED ALL .JS INTO ONE FILE.
                    -->
                   
                    <script src="<?php echo __BASE_STORE_SECURE_JS_URL__?>/jquery.bxslider.min.js"></script>
                    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script> 
                    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine.js"></script>
                    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.validationEngine-en.js"></script>
                    <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
                   
                    
                   <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>  			
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/modernizr.custom.79639.js"></script> 
                   <script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.nicescroll.js"></script>   
                   <script src="<?=__BASE_STORE_SECURE_JS_URL__?>/geoCode.js"></script>
<!--                   <script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/custom.js'></script> -->
                   
		<?php } ?>	
    </head> 
<body> 
   
  