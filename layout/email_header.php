<?php
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once(__APP_PATH__.'/inc/constants.php');
 
$kConfig = new cConfig();
$languageArr=$kConfig->getLanguageDetails('',$iLanguage);
    
if(!empty($languageArr) && $iLanguage!=__LANGUAGE_ID_ENGLISH__)
{
    $szLanguageCode=  strtolower($languageArr[0]['szLanguageCode']);
    $szLandingPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLanguageCode ;
}
else
{
    $szLandingPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
} 
?>
  <p align="right">
        <a href="<?php echo $szLandingPageUrl; ?>">
            <img src="http://<?=__BASE_STORE_TRANPORTECA_IMAGE_URL__?>/transporteca-logo-new.png" alt="Transporteca" title="Transporteca" border="0" />
         </a> 
  </p>