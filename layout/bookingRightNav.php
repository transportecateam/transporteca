<div class="breadcrumb  booking-tabs">
	<p <?php  if($showBookingFlag=='active') {?> class="active"  <?php } ?> >
	<?php  if($showBookingFlag=='active') {?>
	<span><?=t($t_base.'links/active_booking');?>	
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/active_booking_msg_1');?>','<?=t($t_base.'messages/active_booking_msg_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop');">&nbsp;</a></span>
	<span class="active-here"><?=t($t_base.'links/you_are_here');?></span>
	<?php }else {?>
	<a href="javascript:void(0)" onclick="mybooking_change_tab('active');"><?=t($t_base.'links/active_booking');?></a>
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/active_booking_msg_1');?>','<?=t($t_base.'messages/active_booking_msg_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop');">&nbsp;</a>
	<?php }?>
	
	</p>
	
	<p <?php  if($showBookingFlag=='hold') {?> class="active" <?php } ?> >
	<?php  if($showBookingFlag=='hold') {?>
	<span><?=t($t_base.'links/hold_booking');?>
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/hold_booking_msg_1');?>','<?=t($t_base.'messages/hold_booking_msg_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop');">&nbsp;</a>
	</span>
	<span class="active-here"><?=t($t_base.'links/you_are_here');?></span>
	<?php }else{?>
	<a href="javascript:void(0)" onclick="mybooking_change_tab('hold');"><?=t($t_base.'links/hold_booking');?></a>
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/hold_booking_msg_1');?>','<?=t($t_base.'messages/hold_booking_msg_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop');">&nbsp;</a>
	<? }?>
	
	</p>
	
	<p <?php  if($showBookingFlag=='draft') {?> class="active" <?php } ?>>
	<?php  if($showBookingFlag=='draft') {?> 
	<span><?=t($t_base.'links/draft_booking');?>
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/draft_booking_msg_1');?>','<?=t($t_base.'messages/draft_booking_msg_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop');">&nbsp;</a>
	</span>
	<span class="active-here"><?=t($t_base.'links/you_are_here');?></span>
	<? }else {?>
	<a href="javascript:void(0)" onclick="mybooking_change_tab('draft');"><?=t($t_base.'links/draft_booking');?></a>
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/draft_booking_msg_1');?>','<?=t($t_base.'messages/draft_booking_msg_2');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop');">&nbsp;</a>
	<? }?>
	
	</p>
	
	<p <?php  if($showBookingFlag=='archive') {?> class="active last" <?php }else{ ?> class="last" <?php }?>>
	<?php  if($showBookingFlag=='archive') {?> 
	<span><?=t($t_base.'links/archive_booking');?>
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/archive_booking_msg_1');?>','<?=t($t_base.'messages/archive_booking_msg_2');?>','signin_help_pop_right',event);" onmouseout="hide_tool_tip('signin_help_pop_right')">&nbsp;</a>
	</span>
	<span class="active-here"><?=t($t_base.'links/you_are_here');?></span>
	<?php }else {?>
	<a href="javascript:void(0)" onclick="mybooking_change_tab('archive');"><?=t($t_base.'links/archive_booking');?></a>
	<a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/archive_booking_msg_1');?>','<?=t($t_base.'messages/archive_booking_msg_2');?>','signin_help_pop_right',event);" onmouseout="hide_tool_tip('signin_help_pop_right')">&nbsp;</a>
	<? }?>
	
	</p>	
    
</div>
<br/>