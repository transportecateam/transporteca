<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );

ini_set ('max_execution_time',360000); 
ini_set('memory_limit','3048M');
ini_set('error_reporting',E_ALL & ~E_NOTICE );
ini_set('display_error','1');

$kDatabase = new cDatabase();
$allpostmappingAry=array();
$szType='post_translations';
$query="
            SELECT
                term_taxonomy_id,
                description
            FROM
                ".__DBC_SCHEMATA_WP_TERM_TAXONOMY__."
            WHERE
                taxonomy='".  mysql_escape_custom($szType)."'
            ORDER BY
                term_taxonomy_id ASC 
            
            ";
if ($result = $kDatabase->exeSQL($query))
{
    if ($kDatabase->iNumRows > 0)
    {
       while($row=$kDatabase->getAssoc($result))
       {
            $allpostmappingAry[]=$row;
       }
    }
}
if(!empty($allpostmappingAry))
{
    $j=1;
    
    foreach($allpostmappingAry as $postmapping)
    {
        $sourcepost_id=0;
        $translatepost_idAry= array();
        $term_taxonomy_id = $postmapping['term_taxonomy_id'];
        $mappingresult= unserialize($postmapping['description']);
        if(!empty($mappingresult))
        {
            $post_idAry=array();
            foreach($mappingresult as $langcode=>$postid)
            {
                $post_idAry[]=$postid;
            }
        }
        if(!empty($post_idAry))
        {
            sort($post_idAry,SORT_NUMERIC);
            $i=1;
            foreach ($post_idAry as $key => $val) 
            {
             if($i==1)
             {
                $sourcepost_id = $val;
             }
             else 
             {
                 $translatepost_idAry[] = $val;
             }
             $i++;
            }
        }
 
    if($sourcepost_id >0 && !empty($translatepost_idAry))
    {
        foreach($translatepost_idAry as $key=>$translatepost_id)
        {
            // adding translation batch
            $batch_id = addtranslationbatch();
            // getting source post
            $sourceposttranslateAry = getSourcePostTranslation($sourcepost_id);
            $sourcepostdata = getPageData($sourcepost_id);
            $translatepostdata = getPageData($translatepost_id);

            // updating translated post and get translation_id
            $translation_id = updateTranslatedPost($translatepost_id,$sourceposttranslateAry);

            // Making the translate package 
           $translateAry=array();
           $translateAry['url']=__MAIN_SITE_HOME_PAGE_URL__.'?page_id='.$sourcepost_id;
           $translateAry['contents'] =array('original_id'=>array('translate'=>'0','data'=>$sourcepost_id),
                                     'title' =>array('translate'=>'1','data'=>base64_encode($sourcepostdata['post_title']),'format'=>'base64'),
                                     'body' =>array('translate'=>'1','data'=>base64_encode($sourcepostdata['post_content']),'format'=>'base64'),
                                     'excerpt' =>array('translate'=>'1','data'=>base64_encode($sourcepostdata['post_excerpt']),'format'=>'base64')
                                    );
           $translateAry['type']='post';
           $translation_package = serialize($translateAry);

            // Inserting data into table 'wp_icl_translation_status'
            $querystatus="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATION_STATUS__."
                    (
                        translation_id,	
                        status,
                        translator_id,
                        needs_update,
                        md5,
                        translation_service,
                        batch_id,
                        translation_package,
                        timestamp,
                        links_fixed
                    )
                    VALUES
                    (
                       '".(int)$translation_id."',
                       '10',	
                       '1',
                       '0',
                       '".mysql_escape_custom(md5(rand()))."',
                       'local',
                       '".(int)$batch_id."',
                       '".mysql_escape_custom($translation_package)."',
                       NOW(),
                       '0'
                    )
            ";
            if( ( $result = $kDatabase->exeSQL( $querystatus ) ) )
            {
                $rid =$kDatabase->iLastInsertID;
            }

            // Inserting data into table 'wp_icl_translate_job'
            $queryjob="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATE_JOB__."
                    (
                        rid,	
                        translator_id,
                        translated,
                        manager_id
                    )
                    VALUES
                    (
                       '".(int)$rid."',
                       '1',	
                       '1',
                       '1'
                    )
            ";
            if( ( $result = $kDatabase->exeSQL( $queryjob ) ) )
            {
                $job_id =$kDatabase->iLastInsertID;
                //inserting original id
                addOriginalIdData($job_id,$sourcepost_id);
                //inserting title
                addTitleData($job_id,$sourcepostdata['post_title'],$translatepostdata['post_title']);
                //inserting content
                addContentData($job_id,$sourcepostdata['post_content'],$translatepostdata['post_content']);
                //inserting excerpt
                addExcerptData($job_id,$sourcepostdata['post_excerpt'],$translatepostdata['post_excerpt']);

                echo $j.'Record  Translated Successfully<br>';
                
            }
        }
        $j++;
    }
  }
}

function addtranslationbatch()
{
  $kDatabase = new cDatabase();
  $batchname='Manual Translations from '.date('F').' the '.date('dS').', '.date('Y').'.';
  $querybatch="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATION_BATCHES__."
                    (
                        batch_name,
                        last_update
                    )
                    VALUES
                    (
                        '".mysql_escape_custom($batchname)."',
                        NOW()
                    )
            ";
        if( ( $result = $kDatabase->exeSQL( $querybatch ) ) )
        {
            $batch_id =$kDatabase->iLastInsertID;
            return $batch_id;
        }
}

function getSourcePostTranslation($sourcepost_id)
{
    $kDatabase = new cDatabase();
    $query="
            SELECT
                trid,
                language_code
            FROM
                ".__DBC_SCHEMATA_WP_ICL_TRANSLATIONS__."
            WHERE
                element_id='".(int)($sourcepost_id)."'
            ";
    if($result = $kDatabase->exeSQL($query))
    {
        $row=$kDatabase->getAssoc($result);
        return $row;
    }
}

function updateTranslatedPost($translatepost_id,$sourceposttranslateAry)
{
        $kDatabase = new cDatabase();
        $query="
                    UPDATE
                        ".__DBC_SCHEMATA_WP_ICL_TRANSLATIONS__."
                    SET
                        source_language_code='".mysql_escape_custom($sourceposttranslateAry['language_code'])."',
                        trid = '".(int)$sourceposttranslateAry['trid']."'
                    WHERE
                        element_id='".(int)$translatepost_id."'     
                ";
      
      if($result = $kDatabase->exeSQL( $query ))
      {
            $query="
                    SELECT
                        translation_id
                    FROM
                        ".__DBC_SCHEMATA_WP_ICL_TRANSLATIONS__."
                    WHERE
                        element_id='".(int)($translatepost_id)."'
            ";
            if($result = $kDatabase->exeSQL($query))
            {
                $row=$kDatabase->getAssoc($result);
                return $row['translation_id'];
            }
      }
}
function getPageData($post_id)
{
    $kDatabase = new cDatabase();
    $query="
            SELECT
                post_title,
                post_content,
                post_excerpt
            FROM
                ".__DBC_SCHEMATA_WP_POSTS__."
            WHERE
                ID='".(int)($post_id)."'
        ";
        if($result = $kDatabase->exeSQL($query))
        {
            $row=$kDatabase->getAssoc($result);
            return $row;
        }
}

function addOriginalIdData($job_id,$sourcepost_id)
{
    $kDatabase = new cDatabase();
    $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATE__."
                    (
                        job_id,	
                        content_id,
                        timestamp,
                        field_type,
                        field_translate,
                        field_data,
                        field_finished
                    )
                    VALUES
                    (
                        '".(int)($job_id)."',	
                        '0',
                        NOW(),
                        'original_id',
                        '0',
                        '".(int)($sourcepost_id)."',
                        '1'
                    )
            ";
        $result = $kDatabase->exeSQL($query);
}

function addTitleData($job_id,$sourcetitle,$translatetitle)
{
    $kDatabase = new cDatabase();
    $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATE__."
                    (
                        job_id,	
                        content_id,
                        timestamp,
                        field_type,
                        field_format,
                        field_translate,
                        field_data,
                        field_data_translated,
                        field_finished
                    )
                    VALUES
                    (
                        '".(int)($job_id)."',	
                        '0',
                        NOW(),
                        'title',
                        'base64',
                        '1',
                        '".mysql_escape_custom(base64_encode($sourcetitle))."',
                        '".mysql_escape_custom(base64_encode($translatetitle))."',  
                        '1'
                    )
            ";
        $result = $kDatabase->exeSQL($query);
}

function addContentData($job_id,$sourcecontent,$translatecontent)
{
    $kDatabase = new cDatabase();
    $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATE__."
                    (
                        job_id,	
                        content_id,
                        timestamp,
                        field_type,
                        field_format,
                        field_translate,
                        field_data,
                        field_data_translated,
                        field_finished
                    )
                    VALUES
                    (
                        '".(int)($job_id)."',	
                        '0',
                        NOW(),
                        'body',
                        'base64',
                        '1',
                        '".mysql_escape_custom(base64_encode($sourcecontent))."',
                        '".mysql_escape_custom(base64_encode($translatecontent))."',  
                        '1'
                    )
            ";
            $result = $kDatabase->exeSQL($query);
}
function addExcerptData($job_id,$sourceexcerpt,$translateexcerpt)
{
    $kDatabase = new cDatabase();
    $query="
                INSERT INTO
                    ".__DBC_SCHEMATA_WP_ICL_TRANSLATE__."
                    (
                        job_id,	
                        content_id,
                        timestamp,
                        field_type,
                        field_format,
                        field_translate,
                        field_data,
                        field_data_translated,
                        field_finished
                    )
                    VALUES
                    (
                        '".(int)($job_id)."',	
                        '0',
                        NOW(),
                        'excerpt',
                        'base64',
                        '1',
                        '".mysql_escape_custom(base64_encode($sourceexcerpt))."',
                        '".mysql_escape_custom(base64_encode($translateexcerpt))."',  
                        '1'
                    )
            ";
        $result = $kDatabase->exeSQL($query);
}

?>				