<?php
/**
 * Creating Customer Account
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$successContactUpdatedFlag=false;
$t_base = "Users/AccountPage/";
$t_base_error = "Error";
$kConfig = new cConfig();
$kUser = new cUser();
if(!empty($_POST['editUserInfoArr']))
{
    
    
	if($kUser->updateContactInfo($_POST['editUserInfoArr'],$_SESSION['user_id']))
	{
		$successContactUpdatedFlag=true;
	}
}
$kUser->getUserDetails($_SESSION['user_id']);
?>
<?php
if(!empty($kUser->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kUser->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }
	if($successContactUpdatedFlag){

	echo "<p style=color:green>".t($t_base.'messages/update_contact_info_successfully')."</p>";	
?>	
<h5><strong><?=t($t_base.'messages/contact_info');?></strong> <a href="javascript:void(0)" onclick="cancel_user_info(2);"><?=t($t_base.'fields/edit');?></a></h5>
					
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/email');?></span>
		<span class="field-container"><?=$kUser->szEmail?> <?php if($kUser->iConfirmed=='1')
			{?><span style='color:#8A9454'>(<?=t($t_base.'messages/verified');?>)</span><?php }else {?><span style='color:red'>(<?=t($t_base.'messages/not_verified');?>)</span><?php }?> </span>
		<?php if($kUser->iConfirmed!='1')
		{?>
		<label class="profile-fields">
			<div class="field-alert"><div>
			<div id="activationkey" style="display:none";></div>
			<p><?=t($t_base.'messages/check_email');?></p>
			<p><?=t($t_base.'messages/click_the_link');?> <?=$_POST['editUserInfoArr']['szEmail']?> <?=t($t_base.'messages/complete_your_account');?></p>
			
			<p><a href="javascript:void(0)" onclick="resendActivationCode();"><?=t($t_base.'messages/resend_email');?></a>
			<br /><a href="javascript:void(0)" onclick="edit_contact_information();"><?=t($t_base.'messages/change_email');?></a></p>
			</div></div>
	</label>
	<?php }?>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/contact_by_email');?></span>
		<span class="field-container checkbox-ab"><input type="checkbox" <?php if((int)$kUser->iAcceptNewsUpdate=='1'){?> checked <?php }?> disabled="disabled"/><?=t($t_base.'messages/please_news_updates');?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szPhoneNumber?></span>
	</div>	
<?php }else{?>
<h5><strong><?=t($t_base.'messages/contact_info');?></strong></h5>
<form id="edit_contact_information_data" name="edit_contact_information_data" method="post"> 
<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'fields/email');?></span>
			<span class="field-container"><input type="text" name="editUserInfoArr[szEmail]" id="szEmail" value="<?=$_POST['editUserInfoArr']['szEmail'] ? $_POST['editUserInfoArr']['szEmail'] : $kUser->szEmail ?>" onblur="closeTip('email');" onfocus="openTip('email');" value="<?=$_POST['createAccountArr']['szEmail']?>" /></span>
			<div class="field-alert"><div id="email" style="display:none;"><?=t($t_base.'messages/email');?></div></div>
	</label>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/contact_by_email');?></span>
		<span class="field-container checkbox-ab"><input type="checkbox" name="editUserInfoArr[iSendUpdate]" id="iSendUpdate" <?=((($_POST['editUserInfoArr']['iSendUpdate'])?$_POST['editUserInfoArr']['iSendUpdate']:$kUser->iAcceptNewsUpdate) ==  1 ) ? "checked":""?> value='1'/><?=t($t_base.'messages/please_news_updates');?></span>
	</div>
	
	<label class="profile-fields">
		<span class="field-name"><?=t($t_base.'fields/p_number');?></span>
		<span class="field-container"><input type="text" name="editUserInfoArr[szPhoneNumber]" id="szPhoneNumber" value="<?=$_POST['editUserInfoArr']['szPhoneNumber'] ? $_POST['editUserInfoArr']['szPhoneNumber'] : $kUser->szPhoneNumber ?>" /></span>
	</label>
	<br />
	<br />
	<p align="center" style="width:60%">
	<input type="hidden" name="editUserInfoArr[szOldEmail]" id="szOldEmail" value="<?=$_POST['editUserInfoArr']['szOldEmail'] ? $_POST['editUserInfoArr']['szOldEmail'] : $kUser->szEmail ?>"/>
	<input type="hidden" name="editUserInfoArr[szCountry]" id="szCountry" value="<?=$_POST['editUserInfoArr']['szCountry'] ? $_POST['editUserInfoArr']['szCountry'] : $kUser->szCountry ?>"/>
	<a href="javascript:void(0)" class="button1" onclick="edit_user_contact_info();"><span><?=t($t_base.'fields/save');?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_user_info();"><span><?=t($t_base.'fields/cancel');?></span></a> </p>
</form>
<?php }?>