<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __PAYMENT_META_TITLE__;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingDetails/";

$kBooking = new cBooking();
$kConfig = new cConfig();
$kRegisterShipCon = new cRegisterShipCon();
$kWHSSearch = new cWHSSearch();



if((int)$_SESSION['user_id']<=0)
{
	header("Location:".__BOOKING_DETAILS_PAGE_URL__);
	die;
}
if((int)$_REQUEST['idBooking']>0)
{
	$idBooking = (int)$_REQUEST['idBooking'] ;
	$_SESSION['booking_id'] = $idBooking ;
}
else
{
	$idBooking = $_SESSION['booking_id'];
}
if((int)$idBooking>0)
{
	$serviceTypeAry= array();
	$postSearchAry = array();
	$cargoAry = array();
	$shipperConsigneeAry = array();
	
	$postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
	//print_r($postSearchAry);

	$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
	// geting  service type 
	$serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
	$serviceTypeAry = $serviceTypeAry[$postSearchAry['idServiceType']];
	//getting shipper consignee details
	//$shipperConsigneeAry = $kRegisterShipCon->getShipperConsigneeDetails($idBooking);
	
	
	$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br> ".$postSearchAry['szOriginCountry'];
    //$szDestinationAddress = $postSearchAry['szWarehouseFromPostCode']." - ".$postSearchAry['szWarehouseFromCity']."<br>".$postSearchAry['szWarehouseFromCountry'];
    
	$origin_lat_lang_pipeline = $postSearchAry['fOriginLatitude']."|".$postSearchAry['fOriginLongitude']."|".$postSearchAry['fWarehouseFromLatitude']."|".$postSearchAry['fWarehouseFromLongitude']."|".$postSearchAry['fOriginHaulageDistance']."|".$szOriginAddress;
				
	$szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br> ".$postSearchAry['szDestinationCountry'];
    //$szDestinationAddress2 = $postSearchAry['szWarehouseToPostCode']." - ".$postSearchAry['szWarehouseToCity']."<br>".$postSearchAry['szWarehouseToCountry'];
	
    $destination_lat_lang_pipeline = $postSearchAry['fDestinationLatitude']."|".$postSearchAry['fDestinationLongitude']."|".$postSearchAry['fWarehouseToLatitude']."|".$postSearchAry['fWarehouseToLongitude']."|".$postSearchAry['fDestinationHaulageDistance']."|".$szOriginAddress2;
    
	if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__) // 1-DTD 
	{	
		$item_ref_text="DTD ".$postSearchAry['szOriginCity']." to ".$postSearchAry['szDestinationCity']."(".$postSearchAry['szBookingRef'].")";
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__) // 2-DTW
	{	
		$item_ref_text="DTW ".$postSearchAry['szOriginCity']." to ".$postSearchAry['szWarehouseToCity']."(".$postSearchAry['szBookingRef'].")";							
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__)  // 3- WTD
	{	
		$item_ref_text="WTD ".$postSearchAry['szWarehouseFromCity']." to ".$postSearchAry['szDestinationCity']."(".$postSearchAry['szBookingRef'].")";								
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTW__) // WTW
	{								
		$item_ref_text="WTW ".$postSearchAry['szWarehouseFromCity']." to ".$postSearchAry['szWarehouseToCity']."(".$postSearchAry['szBookingRef'].")";
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__) // 2-DTW
	{	
		$item_ref_text="DTP ".$postSearchAry['szOriginCity']." to ".$postSearchAry['szWarehouseToCity']."(".$postSearchAry['szBookingRef'].")";							
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)  // 3- WTD
	{	
		$item_ref_text="PTD ".$postSearchAry['szWarehouseFromCity']." to ".$postSearchAry['szDestinationCity']."(".$postSearchAry['szBookingRef'].")";								
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTP__) // WTW
	{								
		$item_ref_text="WTP ".$postSearchAry['szWarehouseFromCity']." to ".$postSearchAry['szWarehouseToCity']."(".$postSearchAry['szBookingRef'].")";
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTW__) // WTW
	{								
		$item_ref_text="PTW ".$postSearchAry['szWarehouseFromCity']." to ".$postSearchAry['szWarehouseToCity']."(".$postSearchAry['szBookingRef'].")";
	}
	else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTP__) // WTW
	{								
		$item_ref_text="PTP ".$postSearchAry['szWarehouseFromCity']." to ".$postSearchAry['szWarehouseToCity']."(".$postSearchAry['szBookingRef'].")";
	}
}
else
{
	header("Location:".__HOME_PAGE_URL__);
	die;
}
if(($postSearchAry['idShipperConsignee']<=0) || ($postSearchAry['idForwarder']<=0) || ($postSearchAry['idWarehouseTo']<=0) || ($postSearchAry['idWarehouseFrom']<=0))
{
	header("Location:".__BOOKING_DETAILS_PAGE_URL__);
	die;
}
$var_value="cmd=openTrx&amount=".(float)$postSearchAry['fTotalPriceCustomerCurrency']."&currencyCode=".$postSearchAry['szCurrency']."&email=".$kUser->szEmail."&firstName=".$kUser->szFirstName."&lastName=".$kUser->szLastName."";
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script src="<?=__BASE_STORE_JS_URL__?>/jquery.mobile-1.1.0.min.js"></script>
<!-- Include the ZooZ script that contains the zoozStartCheckout() function -->
<script src="https://app.zooz.com/mobile/js/zooz-ext-web.js"></script>

<script type="text/javascript">
function callzoozapi(var_value) {
			$.ajax({
				url: __JS_ONLY_SITE_BASE__+'/zooz/zooz.php?'+var_value,  // A call to server side to initiate the payment process
				dataType: 'html',
				cache: false,
				success: function(response) {
					eval(response);
					var path = __JS_ONLY_SITE_BASE__ + "/zooz";
					
					zoozStartCheckout({
						token : data.token,							// Session token recieved from server
						statusCode: data.statusCode,
						uniqueId : "com.zooz.mobileweb.transporteca1",					// unique ID as registered in the developer portal
						isSandbox : true,							// true = Sandbox environment
						returnUrl : path + "/callback.php",					// return page URL
						cancelUrl : path + "/failed.jsp"					// cancel page URL
						
					});
				}
			});
			
		}
	
	</script>
<div id="hsbody-2">
	<div class="hsbody-2-left filter_style">
		<img src="<?=__BASE_URL_FORWARDER_LOGO__.'/'.$postSearchAry['szForwarderLogo']?>" alt="<?=$postSearchAry['szForwarderDispName']?>" border="0" /><br><br>
		<?
			//display html for service requirements and cargo details  .
			echo display_services_requirements_left($postSearchAry,$cargoAry,$t_base,true);			
		?>
		<p><?=t($t_base.'fields/price');?>: <?=$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalPriceCustomerCurrency'],2)?></p>
	</div>
	<div class="hsbody-2-right">
		<?
			/**
			* display_booikng_bread_crum is used to display bread crum, it accepts two param first one is step and second is $t_base
			*/ 
			echo display_booikng_bread_crum(4,$t_base);		
	?><br/><br/>
	<?
	if($_SESSION['statusFailed']=='Failed')
	{
	unset($_SESSION['statusFailed']);
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<li><?=t($t_base.'messages/payment_transaction_is_failed');?></li>
	</ul>
	</div>
	</div>
	<?
	}?>
	<h5 style="color:#22518A"><?=t($t_base.'title/pay_with_credit_card_or_login');?></h5>
	<br/><br/>
	
		<form action="<?php echo __PAYPAL_PAYMENT_URL__; ?>" method="get" name="paypal_form" id="paypal_form">
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="<?=__BUSINESS_PAYPAL_ACCOUNT__?>">
			<input type="hidden" name="item_name" value="<?php echo $item_ref_text;?>">
			<input type="hidden" name="amount" value="<?php echo number_format((float)$postSearchAry['fTotalPriceCustomerCurrency'],2);?>">	
			<input type='hidden' name='item_number' value='<?php echo $postSearchAry['szBookingRef'];?>'>							
			<input type="hidden" name="no_shipping" value="1">
			<input type="hidden" name="no_note" value="1">
			<input type="hidden" name="currency_code" value="<?=$postSearchAry['szCurrency']?>">
			<input type="hidden" name="no_tax" value="1">
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="return" value="<?=__BASE_URL__?>/paypal/paypal_confirm.php">
			<input type="hidden" name="cancel_return" value="<?=__BASE_URL__?>/paypal/paypal_confirm.php">
			<input type="hidden" name="notify_url" value="<?=__BASE_URL__?>/paypal/paypal_ipn.php">
			<input type="hidden" name="bn" value="PP-BuyNowBF">
			<input type="image" src="https://www.paypalobjects.com/en_GB/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal � The safer, easier way to pay online.">
		</form>
		
		<!--  <input type="button" name="zoozapi" id="zoozapi" onclick="callzoozapi('<?=$var_value?>');" value="Zooz Payment"/>-->
	</div>
</div>
<?
  require_once(__APP_PATH_LAYOUT__."/footer.php");
?>		