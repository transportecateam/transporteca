<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __BOOKING_DETAILS_META_TITLE__;
$szMetaKeywords = __BOOKING_DETAILS_META_KEYWORDS__;
$szMetaDescription = __BOOKING_DETAILS_META_DESCRIPTION__;

require_once(__APP_PATH_LAYOUT__."/header.php");
$t_base = "BookingDetails/";
$t_base_user = "Users/AccountPage/";
$t_base_error = "Error";
$kBooking = new cBooking();
$kConfig = new cConfig();
$kWHSSearch=new cWHSSearch();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
$postSearchAry = $kBooking->getBookingDetails($idBooking);

if((int)$_SESSION['user_id']>0)
{
	$kUser->getUserDetails($_SESSION['user_id']);
	$res_ary=array();
	$res_ary['idUser'] = $kUser->id ;				
	$res_ary['szFirstName'] = $kUser->szFirstName ;
	$res_ary['szLastName'] = $kUser->szLastName ;
	$res_ary['szEmail'] = $kUser->szEmail ;	
	$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
	$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
	$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
	$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
	$res_ary['szCustomerCity'] = $kUser->szCity;
	$res_ary['szCustomerCountry'] = $kUser->szCountry;
	$res_ary['szCustomerState'] = $kUser->szState;
	$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
	$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
	$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;	
	if((int)$postSearchAry['iBookingStep']<10)
	{
		$res_ary['iBookingStep'] = 10 ;
	}
	
	if(!empty($res_ary))
	{
		foreach($res_ary as $key=>$value)
		{
			$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
		}
	}
	
	$update_query = rtrim($update_query,",");
	$kBooking->updateDraftBooking($update_query,$idBooking);
}
$shipperConsigneeAry = array();

//echo "<br> shiper ".$_SESSION['booking_register_shipper_id'] ;
//echo "<br>consignee ".$_SESSION['booking_register_consignee_id'] ;

if($_SESSION['booking_id']==$idBooking)
{
	$kBooking->updatePaymentFlag($idBooking,0);
	//$_SESSION['booking_id'] = '';
	//unset($_SESSION['booking_id']);
}

if((int)$_REQUEST['idBooking']>0)
{
	$idBooking = (int)$_REQUEST['idBooking'] ;
	$idBooking = $idBooking ;

	$kBooking = new cBooking();
	if($_SESSION['user_id']>0)
	{
		if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
			$booking_mode = 2;
		}
		else
		{
			header("Location:".__HOME_PAGE_URL__);
			die;
		}
	}
	else
	{
		header("Location:".__HOME_PAGE_URL__);
		die;
	}
}
else if((int)$idBooking>0)
{
	$idBooking = $idBooking ; 
	
	if(!$kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
	{
		echo display_booking_invalid_user_popup($t_base);
		die;
	}
	
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);	
	
	if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
	{
		echo display_booking_already_paid($t_base,$idBooking);
		die;
	}
}
else
{
	$redirect_url = __LANDING_PAGE_URL__ ;
	?>
	<div id="change_price_div">
	<?php
	display_booking_notification($redirect_url);
	echo "</div>";
}
if(($postSearchAry['idForwarder']<=0) || ($postSearchAry['idWarehouseTo']<=0) || ($postSearchAry['idWarehouseFrom']<=0) || ($postSearchAry['iPaymentProcess']==1))
{
	$redirect_url = __SELECT_SERVICES_URL__.'/'.$szBookingRandomNum.'/';
	header("Location:".$redirect_url);
	exit;
	//echo '<div id="change_price_div">';
	//display_booking_notification($redirect_url);
	//echo "</div>";
}

if(((int)$_SESSION['user_id']>0) && ($_REQUEST['err']<=0))
{
	checkUserPrefferedCurrencyActive();
}

/**
* This function recalculate pricing after a perticular interval set in constants.
* 
**/
//print_r($postSearchAry);
//die;
$ret_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/';
//echo "<br> booking id ".$idBooking ;
echo check_price_30_minute($postSearchAry,$ret_url,false,$idBooking);

$kUser = new cUser();
$kRegisterShipCon = new cRegisterShipCon();

if(!empty($_POST['loginArr']))
{
	if($kUser->userLogin($_POST['loginArr']))
	{
		$redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;
		header("Location:".$redirect_url);
		exit();
	}
}
if(!empty($_POST['shipperConsigneeAry']))
{
	$shipperConsigneeAry = $_POST['shipperConsigneeAry'] ;
	$shipperConsigneeAry['szShipperPhone'] = urldecode($shipperConsigneeAry['szShipperPhone']);
}
else
{
	$shipperConsigneeAry = $kRegisterShipCon->getShipperConsigneeDetails($idBooking);
}

if(empty($shipperConsigneeAry))
{
	$shipperConsigneeAry['iPickupAddress'] = 1;
	$shipperConsigneeAry['iPickupAddress_hidden'] = 1;
	$shipperConsigneeAry['iDeliveryAddress'] = 1;
	$shipperConsigneeAry['iDeliveryAddress_hidden'] = 1;
	$show_from_landing_page = true ;
}
else
{
	$shipperConsigneeAry['iPickupAddress_hidden'] = $shipperConsigneeAry['iPickupAddress'];
	$shipperConsigneeAry['iDeliveryAddress_hidden'] = $shipperConsigneeAry['iDeliveryAddress'];
	$show_from_landing_page = false;
}
$display_pickup_address = 'none' ;
$display_delivery_address = 'none' ;
if($shipperConsigneeAry['iPickupAddress_hidden'] == 0 && ($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__)) //DTD or DTW
{
	$display_pickup_address = 'block';
}
if($shipperConsigneeAry['iDeliveryAddress_hidden'] == 0 && ($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)) //DTD or WTD
{
	$display_delivery_address = 'block';
}

if($display_pickup_address=='none')
{
	if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__)  //DTD
	{		
		$disable_shipper_fields = true ;
	}	
	else
	{
		$disable_shipper_fields = false ;
	}
}	

if($display_delivery_address=='none')
{
	if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)  //DTD
	{		
		$disable_consignee_fields = true ;
	}
	else 
	{
		$disable_consignee_fields = false ;
	}
}

$save_shipper_address = false;
$save_consignee_address = false ;
if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__)  //DTD
{
	$display_pickup_checkbox = true;
	$save_shipper_address = true;
}
if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)  //DTD
{
	$display_delivery_checkbox = true;
	$save_consignee_address = true;
}

if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__)  //DTD
{
	$shipper_tootl_tip_header = t($t_base.'messages/SHIPPER_DTD_DTW_TOOL_TIP_HEADER_TEXT');
	$shipper_tootl_tip_text = t($t_base.'messages/SHIPPER_DTD_DTW_TOOL_TIP_TEXT');
	$consignee_tootl_tip_header = t($t_base.'messages/CONSIGNEE_DTD_DTW_TOOL_TIP_HEADER_TEXT');
	$consignee_tootl_tip_text = t($t_base.'messages/CONSIGNEE_DTD_DTW_TOOL_TIP_TEXT');
}
else
{
	$shipper_tootl_tip_header = t($t_base.'messages/SHIPPER_WTD_WTW_TOOL_TIP_HEADER_TEXT'); 
	$shipper_tootl_tip_text = t($t_base.'messages/SHIPPER_WTD_WTW_TOOL_TIP_TEXT');
	
	$consignee_tootl_tip_header = t($t_base.'messages/CONSIGNEE_WTD_WTW_TOOL_TIP_HEADER_TEXT');
	$consignee_tootl_tip_text = t($t_base.'messages/CONSIGNEE_WTD_WTW_TOOL_TIP_TEXT');
}
?>
<script type="text/javascript">
$().ready(function() {
$("#szShipperPostcode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szShipperCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
	
	$("#szConsigneePostcode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szConsigneeCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	change_postcode_city_default_text_booking_details('<?=$postSearchAry['idServiceType']?>','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>');
});
</script>
<div id="hsbody-2">
<div id="customs_clearance_pop_right" class="help-pop right">
</div>
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="change_price_div" style="display:none"></div>

	<div class="hsbody-2-left filter_style">
	<?php
		if(!empty($postSearchAry['szForwarderLogo']))
		{
	?>
		<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$postSearchAry['szForwarderLogo']."&w=150&h=60"?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
	<?php
		}
		else
		{
			echo $postSearchAry['szForwarderDispName']."<br />" ;
		}
	?>	
		<?php
			//display html for service requirements and cargo details  .
			echo display_services_requirements_left($postSearchAry,$cargoAry,$t_base,true);			
		?>
		<p><?=t($t_base.'fields/price');?>: <?=$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalPriceCustomerCurrency'],2)?></p>
	</div>
	<div class="hsbody-2-right">
		<?php
			/**
			* display_booikng_bread_crum is used to display bread crum, it accepts two param first one is step and second is $t_base
			*/ 
			echo display_booikng_bread_crum(2,$t_base);		
			//
		if((int)$_SESSION['user_id']>0)
		{
					
			$idCustomer = (int)$_SESSION['user_id'];
			$kUser->getUserDetails($idCustomer);
			
			// if user's profile is incomplete then do not show shipper consignee dropdowns 
			if((int)$kUser->iIncompleteProfile==0)
			{
				$registConsigneeAry = array();
				$registShippersAry = array();	
				
				if($display_pickup_checkbox)
				{
					// getting all registered shippers of the user 
					$registShippersAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('ship',$idCustomer,$postSearchAry['szOriginPostCode'],$postSearchAry['idOriginCountry'],$postSearchAry['szOriginCity']);
				}
				else
				{
					// getting all registered shippers of the user 
					$registShippersAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('ship',$idCustomer);
				}
				//echo $display_delivery_address ;
				if($display_delivery_checkbox)
				{
					// getting all registered consignee of the user 
					$registConsigneeAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('con',$idCustomer,$postSearchAry['szDestinationPostCode'],$postSearchAry['idDestinationCountry'],$postSearchAry['szDestinationCity']);
				}
				else
				{
					// getting all registered consignee of the user 
					$registConsigneeAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('con',$idCustomer);
				}
			}
			$iLanguage = getLanguageId();
			$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
	?>
		
		<br />
		<br />		
		<form action="" id="booking_detail_form" name="booking_detail_form" method="post">
		<div class="oh">
		 <div id="booking_details_error_div" class="errorBox " style="display:none;">
		</div>
			<div class="fl-60 bd-details-field" style="width:64%">
				<h5 style="margin-left:44%"><?=t($t_base.'fields/shipper');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$shipper_tootl_tip_header?>','<?=$shipper_tootl_tip_text?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></h5>
				<span class="checkbox-ab f-size-12" style="margin-left:44%;min-height:18px;max-height: 18px;">
				<?php
					if($display_pickup_checkbox)
					{
				?>
					<input type="checkbox" <?php if($shipperConsigneeAry['iPickupAddress']==1){?> checked="true" <?php }?> onclick="toggle_pick_address('<?=$postSearchAry['szOriginPostCode']?>','<?=$postSearchAry['szOriginCity']?>','<?=$postSearchAry['idOriginCountry']?>','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" id="iPickupAddress" name="shipperConsigneeAry[iPickupAddress]" value="1" />
					<em><?=t($t_base.'fields/this_is_pickup_address');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/PICK_UP_ADDRESS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/PICK_UP_ADDRESS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></em>
				<?php	
					}
					else
					{
						echo "&nbsp;";
					}
				?>	
					<input type="hidden" name="shipperConsigneeAry[iPickupAddress_hidden]" value="<?=$shipperConsigneeAry['iPickupAddress_hidden']?>" id="iPickupAddress_hidden">
					</span>
				<div class="oh">				
					<p class="fl-40" style="width:44%;"><?=t($t_base.'fields/registered');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/REGISTERED_LEFT_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/REGISTERED_LEFT_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
					<p class="fl-60" style="width:56%;">
						<span id="registered_shipper_drop_down">
						<select size="1" name="shipperConsigneeAry[idRegistShipper]" id="idRegistShipper" onchange="auto_fill_shipper_consignee(this.value,'SHIPPER','shipper_info_div')">
							<option value="New">New</option>
							<?php
								if(!empty($registShippersAry))
								{
									foreach($registShippersAry as $registShippersArys)
									{
										?>										
										 <option value="<?=$registShippersArys['id']?>" <?php if($_SESSION['booking_register_shipper_id']==$registShippersArys['id']) {?>selected<?php }?>><?=$registShippersArys['szCompanyName']?></option>
										<?php
									}
								}
							?>
						</select>
						</span>
						<span class="checkbox-ab f-size-12"><input type="checkbox" <?php if($_SESSION['booking_register_shipper_id']>0){ ?>disabled<?php } ?> id="iAddRegistShipper" name="shipperConsigneeAry[iAddRegistShipper]" value="1" /><em><?=t($t_base.'fields/add_to_my_registered_shippers');?></em></span>
					</p>
				</div>
				<div id="shipper_info_div">
					<?php
						
						// displaying input fields for entering the details for shippers 
						display_shipper_information_html($shipperConsigneeAry,$postSearchAry,$t_base,$allCountriesArr,$display_pickup_address,$disable_shipper_fields,$show_from_landing_page);
					?>
				</div>
				<div class="oh">
				<div id="shipper_consignee_label" class="fl-44" <?php if($display_pickup_address=='block' || $display_delivery_address=='block'){?>style="display:block;"<?php } else {?>style="display:none"<?php }?>>
					<br/>
					<br style="line-height:18px;" />
					<div class="oh">
					<?=t($t_base.'fields/country');?>					
					</div>
					<div class="oh">
					<?=t($t_base.'fields/city');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>						
					</div>
					<div class="oh">
						<?=t($t_base.'fields/postcode');?>						
					</div>		
					<div class="oh">
						<?=t($t_base.'fields/address');?>						
					</div>
					<div class="oh">
						<?=t($t_base.'fields/address2');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>
					</div>
					<div class="oh">
						<?=t($t_base.'fields/address3');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>						
					</div>
					<div class="oh">
						<?=t($t_base.'fields/province');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>						
					</div>
				</div>
				
				<div id="pickup_address_div" class="fl-56" style="display:<?=$display_pickup_address?>;">
					<br/>
					<h5><?=t($t_base.'fields/pickup_address');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/PICK_UP_ADDRESS_TITLE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/PICK_UP_ADDRESS_TITLE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>
					</h5>
					<div class="oh">					
					<!-- onfocus="this.defaultIndex=this.selectedIndex;" onchange="this.selectedIndex=this.defaultIndex;"  -->
						<select size="1" name="shipperConsigneeAry[idShipperCountry_pickup1]" id="idShipperCountry_pickup" disabled>
						<?php
							if(!empty($allCountriesArr))
							{
								foreach($allCountriesArr as $allCountriesArrs)
								{
									?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$postSearchAry['idOriginCountry'])){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						?>
					   </select>					
					</div>
				
					<div class="oh">
						<input type="text" <?php if($display_pickup_checkbox){?> disabled <?php }?> name="shipperConsigneeAry[szShipperCity_pickup1]" id="szShipperCity_pickup" value="<?=$postSearchAry['szOriginCity']?>"/>
					</div>
					<div class="oh">						
						<input type="text" <?php if($display_pickup_checkbox){?> disabled <?php }?> name="shipperConsigneeAry[szShipperPostcode_pickup1]" id="szShipperPostcode_pickup" value="<?=$postSearchAry['szOriginPostCode']?>"/>
					</div>		
					<div class="oh">						
						<input type="text" name="shipperConsigneeAry[szShipperAddress_pickup]" id="szShipperAddress_pickup" value="<?=$shipperConsigneeAry['szShipperAddress_pickup']?>"/>
					</div>
					<div class="oh">						
						<input type="text" name="shipperConsigneeAry[szShipperAddress2_pickup]" id="szShipperAddress2_pickup" value="<?=$shipperConsigneeAry['szShipperAddress2_pickup']?>"/>
					</div>
					<div class="oh">						
						<input type="text" name="shipperConsigneeAry[szShipperAddress3_pickup]" id="szShipperAddress3_pickup" value="<?=$shipperConsigneeAry['szShipperAddress3_pickup']?>"/>
					</div>
					<div class="oh">						
						<input type="text" name="shipperConsigneeAry[szShipperState_pickup]" id="szShipperState_pickup" value="<?=$shipperConsigneeAry['szShipperState_pickup']?>"/>
					</div>
				</div>
		</div>
		</div>
			
			<div class="fl-40 bd-details-field"  style="float: right; width: 35%;">
				<h5><?=t($t_base.'fields/consignee');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=$consignee_tootl_tip_header?>','<?=$consignee_tootl_tip_text?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></h5>
				<span class="checkbox-ab f-size-12" style="min-height:18px;max-height: 18px;">
				<?php
					if($display_delivery_checkbox)
					{
				?>
					<input type="checkbox" <?php if($shipperConsigneeAry['iDeliveryAddress']==1){?> checked="true" <?php }?> onclick="toggle_delivery_address('<?=$postSearchAry['szDestinationPostCode']?>','<?=$postSearchAry['szDestinationCity']?>','<?=$postSearchAry['idDestinationCountry']?>','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" id="iDeliveryAddress" name="shipperConsigneeAry[iDeliveryAddress]" value="1" />
					<em><?=t($t_base.'fields/this_is_delivery_address');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/DELIVERY_ADDRESS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DELIVERY_ADDRESS_TOOL_TIP_TEXT');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a> </em>
				<?php
					}
					else
					{
						echo "&nbsp;";
					}
				?>	
					<input type="hidden" name="shipperConsigneeAry[iDeliveryAddress_hidden]" value="<?=$shipperConsigneeAry['iDeliveryAddress_hidden']?>" id="iDeliveryAddress_hidden">
					</span>
				<div class="oh">
					<p>
						<span id="registered_consignee_drop_down">
							<select size="1" name="shipperConsigneeAry[idRegistConsignee]" id="idRegistConsignee" onchange="auto_fill_shipper_consignee(this.value,'CONSIGNEE','consignee_info_div')">
								<option value="New">New</option>
								<?php
									if(!empty($registConsigneeAry))
									{
										foreach($registConsigneeAry as $registConsigneeArys)
										{
											?>										
											 <option value="<?=$registConsigneeArys['id']?>" <?php if($_SESSION['booking_register_consignee_id']==$registConsigneeArys['id']){?>selected<?php }?>><?=$registConsigneeArys['szCompanyName']?></option>
											<?php
										}
									}
								?>
							</select>
						</span>
						<span class="checkbox-ab f-size-12"><input type="checkbox" <?php if($_SESSION['booking_register_consignee_id']>0){?>disabled<?php }?> id="iAddRegistConsignee" name="shipperConsigneeAry[iAddRegistConsignee]" value="1" /><em><?=t($t_base.'fields/add_to_my_registered_consignees');?></em></span>
					</p>
				</div>
				<div id="consignee_info_div">
					<?php
						// displaying input fields for entering the details for consignee 
						display_consignee_information_html($shipperConsigneeAry,$postSearchAry,$t_base,$allCountriesArr,$display_delivery_address,$disable_consignee_fields,$show_from_landing_page);
					?>
			</div>
			<div id="delivery_address_div" style="display:<?=$display_delivery_address?>;">
				<br>
				<h5><?=t($t_base.'fields/deliver_address');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DELIVERY_ADDRESS_TITLE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DELIVERY_ADDRESS_TITLE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>
				</h5>
				<div class="oh">
				<p class="fl-65"> <!-- onfocus="this.defaultIndex=this.selectedIndex;" onchange="this.selectedIndex=this.defaultIndex;"  -->
					<select size="1" name="shipperConsigneeAry[idConsigneeCountry_pickup2]" id="idConsigneeCountry_pickup2" onfocus="this.blur();" disabled >
					<?php
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==$postSearchAry['idDestinationCountry'])){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
								<?php
							}
						}
					?>
				   </select>
				</p>
				</div>
				<div class="oh">
					<p><input type="text" disabled name="shipperConsigneeAry[szConsigneeCity_pickup2]" id="szConsigneeCity_pickup2" value="<?=$postSearchAry['szDestinationCity']?>"/></p>
				</div>
				<div class="oh">
					<p><input type="text" disabled name="shipperConsigneeAry[szConsigneePostcode_pickup2]" id="szConsigneePostcode_pickup2" value="<?=$postSearchAry['szDestinationPostCode']?>"/></p>
				</div>		
				<div class="oh">
					<p><input type="text" name="shipperConsigneeAry[szConsigneeAddress_pickup]" id="szConsigneeAddress_pickup" value="<?=$shipperConsigneeAry['szConsigneeAddress_pickup']?>"/></p>
				</div>
				<div class="oh">
					<p><input type="text" name="shipperConsigneeAry[szConsigneeAddress2_pickup]" id="szConsigneeAddress2_pickup" value="<?=$shipperConsigneeAry['szConsigneeAddress2_pickup']?>"/></p>
				</div>
				<div class="oh">
					<p><input type="text" name="shipperConsigneeAry[szConsigneeAddress3_pickup]" id="szConsigneeAddress3_pickup" value="<?=$shipperConsigneeAry['szConsigneeAddress3_pickup']?>"/></p>
				</div>
				<div class="oh">
					<p><input type="text" name="shipperConsigneeAry[szConsigneeState_pickup2]" id="szConsigneeState_pickup2" value="<?=$shipperConsigneeAry['szConsigneeState_pickup']?>"/></p>
				</div>
			</div>
		</div>
	</div>	
		<h5 style="margin-bottom:5px;"><?=t($t_base.'fields/commodity');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CARGO_DESCRIPTION_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CARGO_DESCRIPTION_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		</h5>
		<?php
			$count_cargo = count($cargoAry);
			$counter_1=0;
			//print_r($cargoAry);
			for($i=1;$i<=$count_cargo;$i++)
			{
				if(!empty($cargoAry))
				{
					$fLength = ceil($cargoAry[$i]['fLength']);
					$fWidth = ceil($cargoAry[$i]['fWidth']);
					$fHeight = ceil($cargoAry[$i]['fHeight']);
					$fWeight = ceil($cargoAry[$i]['fWeight']);
					$iQuantity = ceil($cargoAry[$i]['iQuantity']);
					$szCommodity = $cargoAry[$i]['szCommodity'];
					$szCargoMeasure = $cargoAry[$i]['cmdes'];
					$szWeightMeasure =  $cargoAry[$i]['wmdes'];
				}
										
				// geting all weight measure 
				//$weightMeasureAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_WEIGHT_MEASURE__,$cargoAry[$i]['idWeightMeasure']);
				
				// geting all cargo measure 
			//	$cargoMeasureAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_CARGO_MEASURE__,$cargoAry[$i]['idCargoMeasure']);
				
				echo '<div class="oh"><p class="fl-40">'.$i.": ".$iQuantity."x".$fLength."x".$fWidth."x".$fHeight."".$szCargoMeasure.", ".number_format((float)$fWeight)." ".$szWeightMeasure ;
				?>				
				<p class="fl-60">
					<input type="text" name="shipperConsigneeAry[szCargoCommodity][<?=$counter_1?>]" id="szCargoCommodity_<?=$counter_1?>" onkeyup="limit_chars('<?=$counter_1?>')" value="<?=!empty($shipperConsigneeAry['szCargoCommodity'][$counter_1]) ? $shipperConsigneeAry['szCargoCommodity'][$counter_1]:$szCommodity?>" style="width:426px;"/>
					<input type="hidden" name="shipperConsigneeAry[idCargo][<?=$counter_1?>]" value="<?=$cargoAry[$i]['id']?>" />
				</p>
				</div>
				<?php
				$counter_1++;
			}
		?>
		<br/>
		<div class="oh">
			<p class="fl-33" align="center">
				<?php
				//print_r($postSearchAry);
				if($postSearchAry['iFromRequirementPage']==2)
				{?>
					<a href="<?=__SELECT_SIMPLE_SERVICES_URL__.'/'.$_REQUEST['booking_random_num'].'/'?>" class="button2"><span><?=t($t_base.'fields/back');?></span></a>
				<?php
				}
				else
				{?>
				<a href="<?=__SELECT_SERVICES_URL__.'/'.$_REQUEST['booking_random_num'].'/'?>" class="button2"><span><?=t($t_base.'fields/back');?></span></a>
				<?php }?>
				<input type="hidden" id="szTransportation" name="shipperConsigneeAry[idServiceType]" value="<?=$postSearchAry['idServiceType']?>">
				<input type="hidden" id="idForwarder" name="shipperConsigneeAry[idForwarder]" value="<?=$postSearchAry['idForwarder']?>">
				<input type="hidden" id="szForwarderDispName" name="shipperConsigneeAry[szForwarderDispName]" value="<?=$postSearchAry['szForwarderDispName']?>">
			</p>
			<p class="fl-33" align="center" style="margin-top:6px"><span><?=t($t_base.'fields/click_to_continue_to_review_booking');?></span></p>
			<p class="fl-33" align="center"><a href="javascript:void(0);" onclick="encode_string('szShipperPhone','szShipperPhoneNumberUpdate','szConsigneePhone','szConsigneePhoneNumberUpdate');submit_bookig_details();"  class="button1"><span><?=t($t_base.'fields/continue');?></span></a></p>
		</div>
				
		<input type="hidden" name="shipperConsigneeAry[szShipperCity_hidden]" id="szShipperCity_hidden" value="<?=$save_shipper_address ? $postSearchAry['szOriginCity']:$shipperConsigneeAry['szShipperCity']?>">		
		<input type="hidden" name="shipperConsigneeAry[szShipperPostcode_hidden]" id="szShipperPostcode_hidden" value="<?=$save_shipper_address ? $postSearchAry['szOriginPostCode']:$shipperConsigneeAry['szShipperPostCode']?>">
		<input type="hidden" name="shipperConsigneeAry[idShipperCountry_hidden]" id="idShipperCountry_hidden" value="<?=$save_shipper_address ? $postSearchAry['idOriginCountry']:$shipperConsigneeAry['idShipperCountry']?>">
		
		<input type="hidden" name="shipperConsigneeAry[szShipperPhoneNumberUpdate]" id="szShipperPhoneNumberUpdate" value="">
		<input type="hidden" name="shipperConsigneeAry[szShipperCity_pickup]" id="szShipperCity_pickup" value="<?=$postSearchAry['szOriginCity']?>">		
		<input type="hidden" name="shipperConsigneeAry[szShipperPostcode_pickup]" id="szShipperPostcode_pickup" value="<?=$postSearchAry['szOriginPostCode']?>">
		<input type="hidden" name="shipperConsigneeAry[idShipperCountry_pickup]" id="idShipperCountry_pickup" value="<?=$postSearchAry['idOriginCountry']?>">
		
		<input type="hidden" name="shipperConsigneeAry[szConsigneeCity_hidden]" id="szConsigneeCity_hidden" value="<?=$save_consignee_address ? $postSearchAry['szDestinationCity']:$shipperConsigneeAry['szConsigneeCity']?>">		
		<input type="hidden" name="shipperConsigneeAry[szConsigneePostcode_hidden]" id="szConsigneePostcode_hidden" value="<?=$save_consignee_address ? $postSearchAry['szDestinationPostCode']:$shipperConsigneeAry['szConsigneePostCode']?>">
		<input type="hidden" name="shipperConsigneeAry[idConsigneeCountry_hidden]" id="idConsigneeCountry_hidden" value="<?=$save_consignee_address ? $postSearchAry['idDestinationCountry']:$shipperConsigneeAry['idConsigneeCountry']?>">
		
		<input type="hidden" name="shipperConsigneeAry[szConsigneePhoneNumberUpdate]" id="szConsigneePhoneNumberUpdate" value="">
		<input type="hidden" name="shipperConsigneeAry[szConsigneeCity_pickup]" id="szConsigneeCity_pickup" value="<?=$postSearchAry['szDestinationCity']?>">		
		<input type="hidden" name="shipperConsigneeAry[szConsigneePostcode_pickup]" id="szConsigneePostcode_pickup" value="<?=$postSearchAry['szDestinationPostCode']?>">
		<input type="hidden" name="shipperConsigneeAry[idConsigneeCountry_pickup]" id="idConsigneeCountry_pickup" value="<?=$postSearchAry['idDestinationCountry']?>">
		
		<input type="hidden" name="shipperConsigneeAry[szBookingRandomNum]" id="szBookingRandomNum_booking_details" value=""/>
</form>

<?php

echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);

}
else
{
	$reload_page = true;
?>
	<br /><br />		
	<div id="ajaxLogin">
	<?php
		require_once( __APP_PATH__ . "/login.php" ); 
	?>
	</div>
	<?php
}
?>
</div>	
</div>
<?php
  require_once(__APP_PATH_LAYOUT__."/footer.php");
?>				