<?php
ob_start();
session_start();

$szMetaTitle= __EXPLAIN_META_TITLE__;
$szMetaKeywords = __EXPLAIN_META_KEYWORDS__;
$szMetaDescription = __EXPLAIN_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$style="style='top: 0px; bottom: auto; display: block;'";
$iEmailVerificationPage = 1; 
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "home/homepage/"; 
checkAuth();
$kUser = new cUser();
$userSessionData = array();
$userSessionData = get_user_session_data();
$idUserSession = $userSessionData['idUser'];

if((int)$idUserSession>0)
{
	$kUser->getUserDetails($idUserSession);
}
else
{
	//redirect to home page
}

?>
<div id="hsbody-2">  
		<div class="social-section-head email-verification clearfix">
			<div class="text">
				<h1><?php echo t($t_base.'fields/verify_your_email');?></h1>  
			</div>	
			<div class="image"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/EmailVerify.png"; ?>" alt="<?php echo t($t_base.'fields/verify_your_email');?>" title="<?php echo t($t_base.'fields/verify_your_email');?>" ></div>
		</div> 
		<div> 
			<div id="email-verification-div" class="clearfix">
				<?php
					echo display_email_verification_form($kUser,true); 
				?>
			</div>
		</div>
	</div>   
<div id="all_available_service" style="display:none;"></div>
<?php 
require_once( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>