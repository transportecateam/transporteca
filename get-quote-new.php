<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __BOOKING_GET_QUOTE_META_TITLE__;
$szMetaKeywords = __BOOKING_GET_QUOTE_META_KEYWORDS__;
$szMetaDescription = __BOOKING_GET_QUOTE_META_DESCRIPTION__;

$iDonotIncludeHiddenSearchHeader = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingDetails/";
$t_base_user = "Users/AccountPage/";
$t_base_error = "Error";
$kBooking = new cBooking();
$kConfig = new cConfig();
$kWHSSearch=new cWHSSearch();

//$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
  
$szBookingRandomNum = 'TBHNMU5S5B9O';
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
$postSearchAry = $kBooking->getBookingDetails($idBooking);
$idLandingPage = $postSearchAry['idLandingPage'];
 
if($postSearchAry['iQuotesStatus']>0)
{ 
    //It means this RFQ has already been sent so no need to send it again, we simply redirected user to landing page.
//    ob_end_clean();
//    header("Location:".__BASE_URL__);
//    die;
}
if($idLandingPage<=0)
{
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
        $idLandingPage = 2; 
    }
    else
    {
        $idLandingPage = 1; 
    }  
} 
$userSessionData = array();
$userSessionData = get_user_session_data();
$idUserSession = $userSessionData['idUser'];

$iLanguage = getLanguageId();
 
if((int)$_REQUEST['idBooking']>0)
{
    $idBooking = (int)$_REQUEST['idBooking'] ;
    $idBooking = $idBooking ;

    $kBooking = new cBooking();
    if($idUserSession>0)
    {
        if($kBooking->isBookingBelongsToCustomer($idBooking,$idUserSession))
        {
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
            $booking_mode = 2;
        }
        else
        {
//            header("Location:".__NEW_SEARCH_PAGE_URL__);
//            die;
        }
    }
}
else if((int)$idBooking>0)
{
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);	

    if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
//        echo display_booking_already_paid($t_base,$idBooking);
//        die;
    }
}
else
{
    $redirect_url = __NEW_SEARCH_PAGE_URL__ ;
    ?>
    <div id="change_price_div">
    <?php
    display_booking_notification($redirect_url);
    echo "</div>";
} 
if(((int)$idUserSession>0) && ($_REQUEST['err']<=0))
{
    checkUserPrefferedCurrencyActive();
}
 
$kUser = new cUser();  
$kRegisterShipCon = new cRegisterShipCon(); 

//if($iLanguage==__LANGUAGE_ID_DANISH__) 
//{ 
//    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__'); 
//} 
//else 
//{
    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage); 
//} 

$customerTestimonialAry = array();
$customerTestimonialAry = $kExplain->getAllTestimonials($idLandingPage);

$partnerLogoAry = array();
$partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);

$kConfig = new cConfig();
$kConfig->loadCountry($postSearchAry['idOriginCountry']);
$szOriginCountryName = $kConfig->szCountryName ;

$kConfig->loadCountry($postSearchAry['idDestinationCountry']);
$szDestinationCountryName = $kConfig->szCountryName ;
 
$iWeekDay = date('w');
if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
{
    $szPageHeading = t($t_base.'fields/booking_quotes_heading')." ".t($t_base.'fields/latest_by') ;
    $szPageSubHeadingDetails = t($t_base.'fields/booking_quotes_sub_heading_2')." ".t($t_base.'fields/latest_by_details')." ".t($t_base.'fields/booking_quotes_sub_heading_3') ;
}
else
{
    $szPageHeading = t($t_base.'fields/booking_quotes_heading')." ".t($t_base.'fields/with_in_24') ; 
    $szPageSubHeadingDetails = t($t_base.'fields/booking_quotes_sub_heading_2')." ".t($t_base.'fields/with_in_24_details')." ".t($t_base.'fields/booking_quotes_sub_heading_3') ;
}
?>
<div id="hsbody-2" class="quote-page get-rates"> 
<div id="all_available_service" style="display:none;"></div>
	<div id="customs_clearance_pop_right" class="help-pop right"></div> 
	<div id="customs_clearance_pop" class="help-pop"></div>
	<div id="change_price_div" style="display:none"></div>  
	<div class="hsbody-2-right">
		<div class="forwarder-ready clearfix">
			<div class="number">22</div>
			<div class="text">
				<h3>FORWARDERS ARE READY!</h3>
				<p>With the selected criteria, we cannot display prices immediately, but we will send you the best quote within 24 hours.</p>
			</div>
		</div>
		<div class="get-price">
			<div class="clearfix">
				<div class="price-guarantee-points">
					<h3>Price guarantee</h3>
					<p>1. You won’t find a better price else where</p>
					<p>2. There will bo no additional expenses</p>
					<p class="read-more"><a href="#">Read more</a></p>
				</div>
				<div class="price-form">
					<div class="form-line1 clearfix"><input type="text" placeholder="First Name"><input type="text" placeholder="Last Name"></div>
					<div class="form-line2 clearfix"><input type="text" placeholder="Company"><span class="checkbox-container"><input type="checkbox">Private</span></div>
					<div class="form-line3 clearfix"><input type="text" placeholder="E-mail"><select><option>+45</option></select><input type="phone" placeholder="Phone"></div>
					<p class="read-more"><a href="#">Or log in</a></p>
					
					<div class="price-btn-text">
						You will receive a price as soon as possible, latest witin 1 working day.
						<button class="button-green1">GET PRICE</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="image-names clearfix">
			<div>MORTEN LÆRKHOLM<span>PARTNER, TRANSPORTECA</span></div>
			<div>THORSTEN BOECK<span>PARTNER, TRANSPORTECA</span></div>
		</div>
	</div>
</div>

		<div class="get-rate-section2">
			<ul class="clearfix search-compare-select">
				<li class="search-get-rate"><span>&nbsp;</span>Search</li>
				<li class="compare-get-rate"><span>&nbsp;</span>COMPARE</li>
				<li class="select-get-rate"><span>&nbsp;</span>SELECT</li>
			</ul>
			
			<div class="time-price-communication">
				<p>We carefully select our forwarders based on following criteria</p>
				<ul class="clearfix">
					<li class="time-get-rate"><span>&nbsp;</span>Delivery on time</li>
					<li class="price-get-rate"><span>&nbsp;</span>Sharp prices</li>
					<li class="communication-get-rate"><span>&nbsp;</span>Good communication</li>
				</ul>
			</div>
			<p><button class="button-green1">GET PRICE</button></p>
		</div>
		
		<div class="get-rate-section3">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="price-guarantee">
							<h3>PRICE GUARANTEE</h3>
							<ol>
								<li>If you find a better price for a similar transportation service, we will match the price - no questions asked</li>
								<li>When you book you transportation online, we guarante there will be no additional unexpected costs</li>
							</ol>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="insurance">
							<h3>INSURANCE</h3>
							<p>Did you know that international conventions limit the liability of forwarders?</p>
							<p>With a transportation insurance, you are fully covered in case your goods are damaged.</p>
							<p>Buy your insurance here on the portal, when you book your transportation.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
<?php
    echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
?> 
<?php
  require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>  
  