<?php
/**
 * Creating Customer Account
 */
 ob_start();
session_start();
$szMetaTitle = __CREATE_ACCOUNT_META_TITLE__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) ); 
include( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle = __CREATE_ACCOUNT_PAGE_META_TITLE__;
$szMetaKeywords = __CREATE_ACCOUNT_PAGE_META_KEYWORDS__;
$szMetaDescription = __CREATE_ACCOUNT_PAGE_META_DESCRIPTION__;
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag = 1;
include( __APP_PATH_LAYOUT__ . "/header_new.php" );

unset($_SESSION['idUser']);
$kBooking = new cBooking();
$kRegisterShipCon = new cRegisterShipCon();

if(!empty($_REQUEST['booking_random_num']))
{
    $szBookingRandomNumber = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNumber);
}

if((int)$_SESSION['user_id']>0)
{
    $idCustomer = (int)$_SESSION['user_id'];
    $kUser->getUserDetails($idCustomer);
    if((int)$kUser->iIncompleteProfile == 0)
    {
        header('Location:'.__BASE_URL__.'/myAccount/');
        exit();
    }
    else
    {
        $iIncompleteProfile = $kUser->iIncompleteProfile ;
        $idUserCurrency = $kUser->szCurrency ;
        $szUserEmail = $kUser->szEmail ;

        if($idBooking>0)
        {
            $postSearchAry = array();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);

            $incomplete_profile_flag = true;
            $shipperConsigneeAry = $kRegisterShipCon->getShipperConsigneeDetails($idBooking);
            if($postSearchAry['iShipperConsignee']==1)
            {
                $kUser->szFirstName = $shipperConsigneeAry['szShipperFirstName'];
                $kUser->szLastName = $shipperConsigneeAry['szShipperLastName'];
                $kUser->szCompanyName = $shipperConsigneeAry['szShipperCompanyName'];
                $kUser->szAddress1 = $shipperConsigneeAry['szShipperAddress'];
                $kUser->szAddress2 = $shipperConsigneeAry['szShipperAddress2'];		
                $kUser->szAddress3 = $shipperConsigneeAry['szShipperAddress3'];				
                $kUser->szPostCode = $shipperConsigneeAry['szShipperPostCode'];
                $kUser->szCity = $shipperConsigneeAry['szShipperCity'];
                $kUser->szState = $shipperConsigneeAry['szShipperState'];
                $kUser->szCountry = $shipperConsigneeAry['idShipperCountry'];
                $kUser->szPhoneNumber = $shipperConsigneeAry['szShipperPhone'];
            }
            else if($postSearchAry['iShipperConsignee']==2)
            {
                $kUser->szFirstName = $shipperConsigneeAry['szConsigneeFirstName'];
                $kUser->szLastName = $shipperConsigneeAry['szConsigneeLastName'];
                $kUser->szCompanyName = $shipperConsigneeAry['szConsigneeCompanyName'];
                $kUser->szAddress1 = $shipperConsigneeAry['szConsigneeAddress'];
                $kUser->szAddress2 = $shipperConsigneeAry['szConsigneeAddress2'];		
                $kUser->szAddress3 = $shipperConsigneeAry['szConsigneeAddress3'];				
                $kUser->szPostCode = $shipperConsigneeAry['szConsigneePostCode'];
                $kUser->szCity = $shipperConsigneeAry['szConsigneeCity'];
                $kUser->szState = $shipperConsigneeAry['szConsigneeState'];
                $kUser->szCountry = $shipperConsigneeAry['idConsigneeCountry'];
                $kUser->szPhoneNumber = $shipperConsigneeAry['szConsigneePhone'];
            }
        }
    }
}

$t_base = "Users/AccountPage/";

$kConfig = new cConfig();
$iLanguage = getLanguageId();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
$allCurrencyArr=$kConfig->getBookingCurrency(false,true);

if(!empty($_REQUEST['szEmail']))
{
    $kUser->szEmail = $_REQUEST['szEmail'];
}

$szLabelClassName = "class='common-fields-container clearfix' ";

if(empty($kUser->szCountry) && !empty($_COOKIE['__USER_COUNTRY_NAME___']))
{
    $szCountryName = $_COOKIE['__USER_COUNTRY_NAME___'] ; 
    $idCountry  = $kConfig->getCountryName(false,$szCountryName);
    if($idCountry>0)
    {
        $kUser->szCountry = $idCountry ;
    }  
}  
if($idUserCurrency<=0 && ($_COOKIE['__USER_COUNTRY_NAME___']=='DK'))
{
    $idUserCurrency = 20 ;
}

$dialUpCodeAry = array();
$dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);

if(empty($kUser->idInternationalDialCode))
{
    $kUser->idInternationalDialCode = $kUser->szCountry ;
} 
$idInternationalDialCode = $kUser->idInternationalDialCode ;

?>

<div id="notification_popup" style="display:none;"></div>
<div id="hsbody"> 
<h2 class="create-account-heading"><?=t($t_base.'fields/create_profile');?> <a href="javascript:void(0);" onclick="show_why_pop_up();"><?=t($t_base.'fields/why');?></a></h2>
<!--<p><?=t($t_base.'fields/already_account_created');?> <a href="javascript:void(0)" onclick="ajaxLogin();" class="f-size-14"><?=t($t_base.'fields/sign_in');?></a></p>-->

    <div id="userAccountCreate">
        <div class="font-22">
        <form name="createAccount" id="createAccount" method="post">
            <label <?php echo $szLabelClassName; ?>>
                <span class="field-name"><?=t($t_base.'fields/f_name');?></span>
                <span class="field-container"><input type="text" name="createAccountArr[szFirstName]" id="szFirstName" value="<?php echo $kUser->szFirstName; ?>" onblur="closeTip('fname');" onfocus="openTip('fname');"/></span>
                <div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?>>
                    <span class="field-name"><?=t($t_base.'fields/l_name');?></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szLastName]" id="szLastName" value="<?php echo $kUser->szLastName; ?>" onblur="closeTip('lname');" onfocus="openTip('lname');"/></span>
                    <div class="field-alert"><div id="lname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?>>
                    <span class="field-name"><?=t($t_base.'fields/email');?></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szEmail]" value="<?php echo $kUser->szEmail; ?>" id="szEmail" onblur="closeTip('email');" onfocus="openTip('email');" /></span>
                    <div class="field-alert"><div id="email" style="display:none;"><?=t($t_base.'messages/email');?></div></div>
            </label>	
            <label <?php echo $szLabelClassName; ?> >
                    <p class="checkbox-container" style="margin-left: 30%;"><input type="checkbox" name="createAccountArr[iSendUpdate]" id="iSendUpdate" onblur="closeTip('news');" onfocus="openTip('news');" checked value="1"/><?=t($t_base.'fields/news_update_send');?></p>
                    <div class="field-alert"><div id="news" style="display:none;"><?=t($t_base.'messages/news_update_send');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/password');?></span>
                    <span class="field-container"><input type="password" name="createAccountArr[szPassword]" id="szPassword" onblur="closeTip('pass');" onfocus="openTip('pass');"/></span>
                    <div class="field-alert"><div id="pass" style="display:none;"><?=t($t_base.'messages/password_msg');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/re_password');?></span>
                    <span class="field-container"><input type="password" name="createAccountArr[szConfirmPassword]" id="szConfirmPassword"/></span>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/c_name');?></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szCompanyName]" value="<?php echo $kUser->szCompanyName; ?>" id="szCompanyName" onblur="closeTip('cname');" onfocus="openTip('cname');"/></span>
                    <div class="field-alert"><div id="cname" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/c_reg_n');?></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szCompanyRegNo]" value="<?php echo $kUser->szCompanyRegNo; ?>" id="szCompanyRegNo" onblur="closeTip('c_reg_no');" onfocus="openTip('c_reg_no');"/></span>
                    <div class="field-alert"><div id="c_reg_no" style="display:none;"><?=t($t_base.'messages/comapany_reg_no');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szAddress1]" value="<?php echo $kUser->szAddress1; ?>" id="szAddress1" onblur="closeTip('address1');" onfocus="openTip('address1');"/></span>
                    <div class="field-alert"><div id="address1" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szAddress2]" value="<?php echo $kUser->szAddress2; ?>" id="szAddress2" onblur="closeTip('address2');" onfocus="openTip('address2');"/></span>
                    <div class="field-alert"><div id="address2" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szAddress3]" value="<?php echo $kUser->szAddress3; ?>" id="szAddress3" onblur="closeTip('address3');" onfocus="openTip('address3');"/></span>
                    <div class="field-alert"><div id="address3" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/postcode');?></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szPostCode]" value="<?php echo $kUser->szPostCode; ?>" id="szPostCode" onblur="closeTip('postcode');" onfocus="openTip('postcode');"/></span>
                    <div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/city');?></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szCity]" value="<?php echo $kUser->szCity; ?>" id="szCity" onblur="closeTip('city');" onfocus="openTip('city');"/></span>
                    <div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
                    <span class="field-container"><input type="text" name="createAccountArr[szState]" value="<?php echo $kUser->szState; ?>" id="szState" onblur="closeTip('state');" onfocus="openTip('state');"/></span>
                    <div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/country');?></span>
                    <span class="field-container">
                        <select size="1" name="createAccountArr[szCountry]" id="szCountry">
                            <option value=""><?=t($t_base.'fields/select_country');?></option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                    foreach($allCountriesArr as $allCountriesArrs)
                                    {
                                        ?><option value="<?=$allCountriesArrs['id']?>" <?php if($kUser->szCountry==$allCountriesArrs['id']){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                    <span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
                    <span class="phone-container">
                        <select size="1"  name="createAccountArr[idInternationalDialCode]" id="idInternationalDialCode" onblur="closeTip('pNumber');" onfocus="openTip('pNumber');">
                            <?php
                               if(!empty($dialUpCodeAry))
                               {
                                   $usedDialCode = array();
                                   foreach($dialUpCodeAry as $dialUpCodeArys)
                                   {
                                        if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                        {
                                            $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                            ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idInternationalDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                        }
                                   }
                               }
                           ?>
                        </select>
                        <input type="text" value="<?php echo $kUser->szPhoneNumber; ?>" name="createAccountArr[szPhoneNumber]" id="szPhoneNumber" onblur="closeTip('pNumber');" onfocus="openTip('pNumber');"/>
                    </span>
                    <div class="field-alert1"><div id="pNumber" style="display:none;"><?=t($t_base.'messages/phone_number');?></div></div>
            </label>
            <label <?php echo $szLabelClassName; ?> >
                <span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
                <span class="field-container">
                    <select size="1" name="createAccountArr[szCurrency]" id="szCurrency" onblur="closeTip('currency');" onfocus="openTip('currency');">
                        <?php
                        if(!empty($allCurrencyArr))
                        {
                            foreach($allCurrencyArr as $allCurrencyArrs)
                            {
                                    ?><option value="<?=$allCurrencyArrs['id']?>" <?php if(($idUserCurrency>0) && ($allCurrencyArrs['id'] == $idUserCurrency)){?>selected<?}else if(((int)$idUserCurrency==0) && ($allCurrencyArrs['szCurrency']=='USD')){?> selected <?php }?>><?=$allCurrencyArrs['szCurrency']?></option>
                                    <?php
                            }
                        }
                        ?>
                    </select>
                </span>
                <div class="field-alert1"><div id="currency" style="display:none;"><?=t($t_base.'messages/currency');?></div></div>
            </label> 
            <input type="hidden" name="createAccountArr[szBookingRandomNumber]" value="<?=$szBookingRandomNumber?>" id="szBookingRandomNumber"/>
            <input type="hidden" name="createAccountArr[iIncompleteProfile]" value="<?=$iIncompleteProfile?>" id="iIncompleteProfile"/>
            <input type="hidden" name="createAccountArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value=""/>
            <br>  
        </form>
        </div> 
    <?php if((int)$kUser->iIncompleteProfile == 1){?>
            <p align="center"><a href="javascript:void(0)" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');create_account();"><span><?=t($t_base.'fields/complete_profile');?></span></a></p>
    <?php } else {?>
    <p align="center"><a href="javascript:void(0)" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');create_account();"><span><?=t($t_base.'fields/button');?></span></a></p>
    <?php } ?>
	</div>		
</div> 
	
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>