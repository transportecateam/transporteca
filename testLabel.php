<?php 
/**
 * View Invoice
 */  
ob_start();
session_start();
ini_set ('max_execution_time',360000);
ini_set('pcre.backtrack_limit', 10000000);
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL && ~E_NOTICE); 
 ini_set (max_execution_time,360000); 
// Increase this for old PHP versions (like 5.3.3). If a large pdf fails, increase it even more.
ini_set('pcre.backtrack_limit', 10000000);
$_REQUEST['lang']='danish';
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
  
$arr = array('TL_87766_203007710_20170619141926_20120.pdf');
//$szLabelPdfFileName = serialize($arr);
$szLabelPdfFileName = "TL_87766_203007710_20170619141926_20120.pdf";
$szTrackingNumber = "202973340";
try
{
    echo "File: ".$szLabelPdfFileName;
    echo "<br> Tracking: ".$szTrackingNumber;  
    
    $szOutputDir = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/";
    $kCourierService = new cCourierServices();
    $pdfFilesAry = array();
    $pdfFilesAry = $kCourierService->splitUploadedLabels($szLabelPdfFileName,$szOutputDir);  
        
    print_R($pdfFilesAry);
    die;
    $pdfDocumentsAry = unserialize($szLabelPdfFileName);
    print_R($pdfDocumentsAry); 
    
    $kLabel = new cLabels(); 
    $splitedPdfLabelAry = array();
    $splitedPdfLabelAry = $kLabel->downloadSplitedPdfLabelFrom($szLabelPdfFileName,$szTrackingNumber);
    
} catch (Exception $ex) 
{
    print_R($ex);
}

die;

$outputFileName = __APP_PATH__."/logs/tnt_label_test_reslt_".date('Ymd').".log";
        
$kLabel= new cLabels();
$iRoutingLabel = (int)$_GET['label'];
if((int)$iRoutingLabel<=0)
{
    $iRoutingLabel = true;
} 

$kBooking = new cBooking();

$szHtmlPath = __APP_PATH_TNT_HTML__.'/label_7755_201612260351.html';
$szXmlPath = 
$szHtmlUrl = __BASE_URL_TNT_HTML__."/label_7755_201612260351.html";
   
$szRequestType = $_GET['type'];
$bTestFlag = $_GET['test'];
$szRequestType = "ROUTING_LABEL"; 
if($szRequestType=='TNT_INVOICE')
{
    $szXmlFilePath = __APP_PATH_TNT_XMLS__."/invoice.xml";
}
else if($szRequestType=='MANIFEST')
{
    $szXmlFilePath = __APP_PATH_TNT_XMLS__."/MANIFEST_API_RESPONSE_78540_201703200909.xml";
}
else if($szRequestType=='CONSIGNMENT_NOTE')
{
    $szXmlFilePath = __APP_PATH_TNT_XMLS__."/consignment_notes.xml";
}
else if($szRequestType=='ROUTING_LABEL')
{
    $szXmlFilePath = __APP_PATH_TNT_XMLS__."/ROUTING_LABEL_API_RESPONSE_79385_201703271625.xml"; 
}   
/*
* ROUTING_LABEL
* 2. MANIFEST
* 3. CONSIGNMENT_NOTE
* 4. TNT_INVOICE
*/

try
{ 
    $idBooking = 79385;
    $data['idBooking'] = $idBooking;
    $szAccessCode = "38319981";
    $szPdfFileName = $kLabel->convertXml2Pdf($szXmlFilePath,$idBooking,false,$szRequestType);
    //$szNotesPdf = $kLabel->fetchInvoiceFromTNTAPI($data,$szAccessCode);

    $szPdfUrl = __MAIN_SITE_HOME_PAGE_URL__."/TNT/labels/".$szPdfFileName;
    echo "<br><br>PDF URL: <a href='".$szPdfUrl."' target='_blank'>".$szPdfUrl."</a>";  
    echo "<br>PAGE HTML<br>";
    echo $kLabel->szHtmlString; 
} catch (Exception $ex) {
    print_R($ex);
}

die;
 
//$szTrackingCode = "GE334903073DK";
//$kLabel->downloadBarcodeImage($szTrackingCode);

$str_url = "http://iconnection.tnt.com:81/Shipper/NewStyleSheets/invoice.xsl"; 
$szDataString = getPage($str_url);

$szXslFilePath = __APP_PATH_TNT_XSL__."/Invoice.xsl";
echo "Path: ".$szXslFilePath;
$fw = fopen($szXslFilePath , 'a'); 
fwrite($fw , $szDataString);
fclose($fw); 
die;


if(!empty($kLabel->szPdfString))
{
    ob_start();
    ob_end_clean();
    //echo $kLabel->szPdfString; 
    $szPdfString = ob_get_contents();
    //$szPdfString = file_get_contents($szHtmlPath);
    $szPdfString = getPage($szHtmlUrl);
    echo $szPdfString;
    $kLabel->logDebugger($outputFileName,$szPdfString);
    
    $kLabel->createLabelPdfFile($szPdfString); 
    echo "PDF URL: ".__MAIN_SITE_HOME_PAGE_URL__."/TNT/labels/".$kLabel->szPdfFileNameOriginal; 
} 

function getPage($str_url)
{
    $curl = curl_init();
    //curl_setopt ($curl, CURLOPT_REFERER, strFOARD);
    curl_setopt ($curl, CURLOPT_URL, $str_url);
    curl_setopt ($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt ($curl, CURLOPT_USERAGENT, sprintf("Mozilla/%d.0",rand(4,5)));
    curl_setopt ($curl, CURLOPT_HEADER, 0);
    curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
    $html = curl_exec ($curl);
    curl_close ($curl);
    return $html;
}
die;


/*

$idBooking = 7755;
$postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
$kLabel->createLabelFromTNTApi($postSearchAry);

print_r($kLabel);
if(!empty($kLabel->szLabelPdfFileName))
{
    echo "<br><br>";
    echo __MAIN_SITE_HOME_PAGE_SECURE_URL__."/TNT/labels/".$kLabel->szLabelPdfFileName;
}

*/