<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle= __PRIVACY_NOTICE_PAGE_META_TITLE__ ;
$szMetaKeywords = __PRIVACY_NOTICE_PAGE_META_KEYWORDS__;
$szMetaDescription = __PRIVACY_NOTICE_PAGE_META_DESCRIPTION__;

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$t_base = "Privacy/";
$KwarehouseSearch=new cWHSSearch();
$iLanguage = getLanguageId();
//if($iLanguage ==__LANGUAGE_ID_DANISH__)
//{
//    $privacyPolicy = html_entity_decode($KwarehouseSearch->getManageMentVariableByDescription('__PRIVACY_POLICY_DANISH__'));
//}
//else
//{
    $privacyPolicy = html_entity_decode($KwarehouseSearch->getManageMentVariableByDescription('__PRIVACY_POLICY__',$iLanguage));
//}

?>
<div id="hsbody" class="link16">
		<h1><?=t($t_base.'title/privacy_notice');?></h1><br/>
		<?=refineDescriptionData($privacyPolicy)?>
				
</div>
<?php
//include( __APP_PATH__ . "/footer_holding_page.php" );
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>