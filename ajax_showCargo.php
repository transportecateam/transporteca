<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );

$iGetLanguageFromRequest = 1; 
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");

$kConfig=new cConfig();
$kBooking = new cBooking();

$number1 = sanitize_all_html_input($_REQUEST['number']);
$number = sanitize_all_html_input($_REQUEST['number'])+1;
$mode = sanitize_all_html_input($_REQUEST['mode']);

// geting all weight measure 
$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

// geting all cargo measure 
$cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
$function_params=$_SESSION['user_id'] ;

if($mode == 'REQUIREMENT_PAGE')
{
    $t_base = "LandingPage/"; 
    $kWhsSearch = new cWHSSearch();
    $iLanguage = getLanguageId();
    $szBookingRandomNum = sanitize_all_html_input(trim($_POST['booking_key']));
    $iSearchMinPage = sanitize_all_html_input(trim($_POST['search_mini']));
    
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
    echo display_cargo_details_form($number,$cargoAry,$postSearchAry,$iSearchMinPage); 
    die;
}
else if($mode == 'FURNITURE_ORDER_PAGE')
{ 
    $t_base = "LandingPage/"; 
    $kWhsSearch = new cWHSSearch();
    $iLanguage = getLanguageId();
    $szBookingRandomNum = sanitize_all_html_input(trim($_POST['booking_key']));
    $iSearchMinPage = sanitize_all_html_input(trim($_POST['search_mini']));
    $idSearchMiniStandardCargo = sanitize_all_html_input(trim($_POST['idSearchMiniStandardCargo']));
    
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
    echo display_furniture_order_form($number,$iSearchMinPage,$cargoAry,$idSearchMiniStandardCargo); 
    die;
}
else if($mode=='DISPLAY_STANDARD_CARGO_TYPE')
{
    $idStandardCargoCategory= sanitize_all_html_input(trim($_REQUEST['category_id']));
    $iFieldCounter = sanitize_all_html_input(trim($_REQUEST['counter']));
    $idSearchMiniVersion = sanitize_all_html_input(trim($_REQUEST['search_mini']));
    
    $iLanguage = getLanguageId();
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__);
    $standardCargoAry = array();
    if($idStandardCargoCategory>0)
    { 
        $standardCargoAry = $kConfig->getStandardCargoList($idStandardCargoCategory,$iSelectedLanguage); 
    }
    $idSuffix = "_V_".$idSearchMiniVersion ;
    echo "SUCCESS||||";
    //echo '<span id="selectidStandardCargoType'.$id_suffix.'" class="select">Type</span>';
    echo display_cargo_type_dropdown($standardCargoAry,$iFieldCounter,$idSuffix);
    die;
    
} 
else if($mode == 'PALLET_DETAILS_PAGE')
{
    $t_base = "LandingPage/"; 
    $kWhsSearch = new cWHSSearch();
    $iLanguage = getLanguageId(); 
    $iSearchMinPage = sanitize_all_html_input(trim($_POST['search_mini']));
      
    echo display_pallet_details_form($number,$iSearchMinPage); 
    die;
}
else
{
    $t_base = "home/homepage/";
    $danger_cargo_help_link='';
    if($mode == 'POP_UP')
    {
        $kWhsSearch = new cWHSSearch();
        $iLanguage = getLanguageId();
//        if($iLanguage==__LANGUAGE_ID_DANISH__)
//        {
//            $szVariableName = '__IS_DANGEROUS_CARGO_DANISH__' ;
//        }
//        else
//        {
            $szVariableName = '__IS_DANGEROUS_CARGO__' ;
        //}
        $szExplainUrl = $kWhsSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
        if(empty($szExplainUrl))
        {
            $szExplainUrl = __EXPLAIN_PAGE_URL__ ;
        } 
        $style_danger_cargo = 'class="last f-size-12"';
        $danger_cargo_help_link = '<a href="'.$szExplainUrl.'" target="__blank">'.t($t_base.'fields/help').'!</a>' ;
    }
    else
    {
        $style_danger_cargo = 'class="last"';
    }

    $iLanguage = getLanguageId();
//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    {
        $szDangor_cargo_yes = t($t_base.'fields/yes');
        $szDangor_cargo_no = t($t_base.'fields/no');
//    }
//    else
//    {
//        $szDangor_cargo_yes = 'Yes';
//        $szDangor_cargo_no = 'No';
//    }
?>
    <div class="oh">
        <hr>
        <p class="fl-33 multi_popup"><br /><?=t($t_base.'fields/dimensions');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
        <div class="fl-67 dimensions-field">				
            <span class="f-size-12">
                <?=t($t_base.'fields/length');?><br/>
                <input type="text" pattern="[0-9]*" name="searchAry[iLength][<?=$number?>]" value="<?=$postSearchAry['iLength'][$number]?>" id= "iLength<?=$number?>" /><br>
                <span id="iLength<?=$number?>_div" style="display:none;"></span>
            </span>
            <span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
            <span class="f-size-12">
                <?=t($t_base.'fields/width');?><br/>
                <input type="text" pattern="[0-9]*"  name="searchAry[iWidth][<?=$number?>]" value="<?=$postSearchAry['iWidth'][$number]?>" id= "iWidth<?=$number?>" /><br>
                <span id="iWidth<?=$number?>_div" style="display:none;"></span>
            </span>
            <span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
            <span class="f-size-12">
                <?=t($t_base.'fields/height');?><br/>
                <input type="text" pattern="[0-9]*" name="searchAry[iHeight][<?=$number?>]" value="<?=$postSearchAry['iHeight'][$number]?>" id= "iHeight<?=$number?>" />
                <span id="iHeight<?=$number?>_div" style="display:none;"></span>
            </span>
            <span class="f-size-12">
                &nbsp;<br/>
                <select name="searchAry[idCargoMeasure][<?=$number?>]">
                    <?php
                        if(!empty($cargoMeasureAry))
                        {
                            foreach($cargoMeasureAry as $cargoMeasureArys)
                            {
                                ?>
                                <option value="<?=$cargoMeasureArys['id']?>" <? if($postSearchAry['idCargoMeasure'][$number]==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </span>
            <span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
            <span class="last f-size-12">
                <?=t($t_base.'fields/quantity');?><br/>
                <input type="text" pattern="[0-9]*" name="searchAry[iQuantity][<?=$number?>]" value="<?=$postSearchAry['iQuantity'][$number]?>" id= "iQuantity<?=$number?>" /><br>
                <span id="iQuantity<?=$number?>_div" style="display:none;"></span>
            </span>
        </div>				
    </div>			
    <div class="oh">
        <p class="fl-33 multi_popup"><?=t($t_base.'fields/weight');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WEIGHT_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WEIGHT_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>&nbsp;</a></p>
        <div class="fl-67 dimensions-field">					
            <span>
                <input type="text" pattern="[0-9]*" onkeyup="on_enter_key_press(event,'validateLandingPageForm','<?=$function_params?>','<?=__SELECT_SERVICES_URL__?>');" name="searchAry[iWeight][<?=$number?>]" value="<?=$postSearchAry['iWeight'][$number]?>" id= "iWeight<?=$number?>" />
                <span id="iWeight<?=$number?>_div" style="display:none;"></span>
            </span>
            <span style="width:26%" class="weight">
                <select size="1" name="searchAry[idWeightMeasure][<?=$number?>]" style="margin-left: 14px;min-width: 66px;">
                    <?php
                        if(!empty($weightMeasureAry))
                        {
                            foreach($weightMeasureAry as $weightMeasureArys)
                            {
                                ?>
                                    <option value="<?=$weightMeasureArys['id']?>" <? if($postSearchAry['idWeightMeasure'][$number]==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </span>
            <span style="padding:2px 0 0;" class="dangerous_cargo"><?=t($t_base.'fields/dangerous_cargo');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_TEXT');?>','dang-cargo-pop',event);" onmouseout="hide_tool_tip('dang-cargo-pop')">&nbsp;</a><?=$danger_cargo_help_link?></span>
            <span <?=$style_danger_cargo?>>
                <select size="1" name="searchAry[iDangerCargo][<?=$number?>]" style="min-width:65px">
                    <option value="No" selected=""><?=$szDangor_cargo_no?></option>
                    <option value="Yes"><?=$szDangor_cargo_yes?></option>
                </select>
            </span>
        </div>				
    </div>	
<?php
} 
?>