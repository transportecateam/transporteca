<?php
/**
 * Blog  
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );

$kSEO = new cSEO();
$seoLinkAry = array();
$szLanguage = sanitize_all_html_input($_REQUEST['lang']);
$iLanguage = getLanguageId($szLanguage);
 
if(isset($_REQUEST['menuflag']) && $_REQUEST['menuflag'] == 'info')
{	
    if(!empty($_REQUEST['flag']))
    {	
        $szURL = str_replace("/","",$_REQUEST['flag']);
        $seoLinkAry = $kSEO->getSeoPageDataByUrl($szURL,false,false,false,$iLanguage);
    }
    else
    { 
        if($iLanguage==__LANGUAGE_ID_DANISH__)
        {
             $redirect_url = __MAIN_SITE_HOME_PAGE_URL__."/da";
        }
        else
        {
            $redirect_url = __MAIN_SITE_HOME_PAGE_URL__;
        }
        header("HTTP/1.1 301 Moved Permanently"); 
        header('Location:'.$redirect_url);
        die;
        
        //if clicked on www.transporteca.com/information then we redirect user to first
        //$seoLinkAry = $kSEO->getSeoPageDataByUrl(false,true,false,false,$iLanguage);
        //$_REQUEST['flag'] = $seoLinkAry['szUrl'];
        //$szURL = $seoLinkAry['szUrl'];
    }
}  
if(!empty($seoLinkAry['szMetaTitle']))
{
    $szMetaTitle = $seoLinkAry['szMetaTitle'] ;
}

if(!empty($seoLinkAry['szMetaKeywords']))
{
    $szMetaKeywords = $seoLinkAry['szMetaKeywords'] ;
}
if(!empty($seoLinkAry['szMetaDescription']))
{
    $szMetaDescription = $seoLinkAry['szMetaDescription'];
} 
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$t_base="TermCondition/";
 
$_SESSION['seo_aquisition']['szUrl'] = $szURL ;
$_SESSION['seo_aquisition']['szReferalUrl'] = $_SERVER[HTTP_REFERER] ;

if(empty($seoLinkAry))
{
    pageNotFound(); 
}
else{

$iLanguage = getLanguageId(); 
//getting all active and published seopages
$seoPageAry = array();
$seoPageAry = $kSEO->getAllPublishedSeoPages($iLanguage); 

$t_base_explain="ExplainPage/";  
  
$szSeoPageContent = replaceSearchMiniConstants($seoLinkAry['szContent'],$idSeoPage);

/*
 * Have created individual replace just to make sure if above replace won't work then we use this one.
$szSeoPageContent = str_replace('__INCLUDE_SEARCH_MINI_1__', $szPageVersion_1, $seoLinkAry['szContent']);
$szSeoPageContent = str_replace('__INCLUDE_SEARCH_MINI_2__', $szPageVersion_2, $szSeoPageContent);
$szSeoPageContent = str_replace('__INCLUDE_SEARCH_MINI_3__', $szPageVersion_3, $szSeoPageContent);
$szSeoPageContent = str_replace('__INCLUDE_SEARCH_MINI_4__', $szPageVersion_4, $szSeoPageContent);
$szSeoPageContent = str_replace('__INCLUDE_SEARCH_MINI_5__', $szPageVersion_5, $szSeoPageContent);
$szSeoPageContent = str_replace('__INCLUDE_SEARCH_MINI_6__', $szPageVersion_6, $szSeoPageContent);
 * 
 */

?>
<div> 
    <style type="text/css">
        .evp-video-inner{top:-35px !important;}
    </style>
    <script type="text/javascript"> 
//        (function(window, location){   
//            history.replaceState(null, document.title, location.pathname);
//            history.pushState(null, document.title, location.pathname);
//        }(window, location));
    </script>
    <div id="hsbody" class="body-color-white clearfix"> 
        <?php  
            //echo $seoLinkAry['szContent']; 
            echo $szSeoPageContent;
        ?>			  
        <?php  
            /* 
            if(count($seoPageAry)>1){?>
            <br/>
                <p class="para"><strong><?php echo t($t_base_explain.'fields/related_topics');?></strong></p>			 
                <ul class="ulclass">
                    <?php
                        foreach($seoPageAry as $seoPageArys)
                        {
                            if($seoPageArys['id']!=$seoLinkAry['id'])
                            {?>
                                    <li><a href="<?php echo __INFORMATION_PAGE_URL__.'/'.$seoPageArys['szUrl'].'/'?>"><?php echo $seoPageArys['szPageHeading']?></a></li>
                            <?php
                            }
                        }
                    ?>
                </ul>	
            <?php } */ 
        ?>
    </div>
</div>
<?php
} 
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>