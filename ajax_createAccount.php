<?php
/**
 * Creating Customer Account
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/AccountPage/";
$t_base_error = "Error";
$kConfig = new cConfig();
$kUser = new cUser();
$kBooking = new cBooking();
$iLanguage = getLanguageId();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
$allCurrencyArr=$kConfig->getBookingCurrency(false,true);
		
	if((int)$_SESSION['idUser']==0)
	{
                $_POST['createAccountArr']['iLanguage']=$iLanguage;
		if(!empty($_POST['createAccountArr']))
		{
			if($kUser->createAccount($_POST['createAccountArr']))
			{
				$szBookingRandomNum = sanitize_all_html_input(trim($_POST['createAccountArr']['szBookingRandomNumber'])); 
				if(!empty($szBookingRandomNum))
				{
					$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
				}
				$successflag=true;
			}
		}
	}
	else
	{
		if(!empty($_POST['createAccountArr']))
		{
			if($kUser->updateEmailAddress($_POST['createAccountArr'],$_SESSION['idUser']))
			{
				$szBookingRandomNum = sanitize_all_html_input(trim($_POST['createAccountArr']['szBookingRandomNumber'])); 
				if(!empty($szBookingRandomNum))
				{
					$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
				}
				$successflag=true;
			}
		}
	}
	
	if(!empty($_POST['createAccountArr']['szPhoneNumberUpdate']))
	{
		//$szPhoneNumber=urlencode(base64_decode($_POST['createAccountArr']['szPhoneNumber']));
		$_POST['createAccountArr']['szPhoneNumber'] = urldecode(base64_decode($_POST['createAccountArr']['szPhoneNumberUpdate']));
		
	}
	$szBookingRandomNum = sanitize_all_html_input(trim($_POST['createAccountArr']['szBookingRandomNumber'])); 

?>
<?php
if(!empty($kUser->arErrorMessages)){
?>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kUser->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }
    $redirect_url1=$_SERVER['HTTP_REFERER'];
    $style='';
    $valpos1=strpos($redirect_url1,'createAccount');
	if($valpos1<=0)
	{
		
		$style="style=left:30%;";
	}
	
    if($successflag)
    {
	//echo $_SESSION['idUser'];
	$redirect_url=$_SERVER['HTTP_REFERER'];
	if(__ENVIRONMENT__ == "DEV_LIVE")
	{
		setcookie(__COOKIE_NAME__ , $redirect_url,time() + __COOKIE_TIME__ , '/',$_SERVER['HTTP_HOST'],true);
	}
	else
	{
		setcookie(__COOKIE_NAME__ , $redirect_url,time() + __COOKIE_TIME__ , '/');
	}
	$valpos=strpos($redirect_url,'bookings_details');
	
	if($valpos>0)
	{
		$class="class='profile-done-alert-booking'";
		//$style="style=left:30px;";
	}
	else
	{
		$class="class='profile-done-alert'";
	}
	/*
	
document.getElementById("login").innerHTML='<?=$kUser->szFirstName?> ' + '<?=$kUser->szLastName?>';
$("#login").removeAttr('href');

$("#header_login_form_container").attr("style","display:none;");
$("#login").attr("onclick","");
$("#login").attr('href','<?=$redirect_url?>');
* */
         
?>
<script type="text/javascript">
	update_header_text();
</script>
<div id="complete_signup">	
<table border="0" cellpadding="0" cellspacing="0" <?=$class?> >
    <tr>
	<td valign="middle">
            <div class="profile-alert-content">
                <div id="activationkey"></div>
                <p><?=t($t_base.'messages/almost_done');?></p>			
                <p><?=t($t_base.'messages/check_your_email');?></p>
                <p><?=t($t_base.'messages/click_link');?> <?=$_POST['createAccountArr']['szEmail']?> <?=t($t_base.'messages/complete_profile');?></p>

                <p style="margin-bottom:5px;"><a href="javascript:void(0)" onclick="resendActivationCode('<?=$_SERVER['HTTP_REFERER']?>','<?=$szBookingRandomNum?>');"><?=t($t_base.'messages/resend_email');?></a></p>
                <p><a href="javascript:void(0)" onclick="changeEmailAddress();"><?=t($t_base.'messages/change_email');?></a></p>
                <?php
                if($idBooking>0)
                {
                    $postSearchAry = array();
                    $postSearchAry = $kBooking->getBookingDetails($idBooking);
                    if(($postSearchAry['idForwarder']>0) && ($postSearchAry['idWarehouseTo']>0) && ($postSearchAry['idWarehouseFrom']>0) && ($postSearchAry['idShipperConsignee']>0))
                    {
                            $redirect_url = __BOOKING_CONFIRMATION_PAGE_URL__.'/'.$szBookingRandomNum.'/';
                    }
                    else if(($postSearchAry['idForwarder']>0) && ($postSearchAry['idWarehouseTo']>0) && ($postSearchAry['idWarehouseFrom']>0))
                    {
                            $redirect_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/';
                    }
                    else
                    {
                            $kConfig_new = new cConfig();
                            $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
                            $szCountryStrUrl = $kConfig_new->szCountryISO ;
                            $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']);
                            $szCountryStrUrl .= $kConfig_new->szCountryISO ; 

                            $redirect_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    }
                    ?>
                    <p> <b> >></b> <a href="javascript:void(0);" onclick="back_to_booking('<?=$szBookingRandomNum?>','<?=$redirect_url?>')"><?=t($t_base.'links/back_to_booking');?></a> <b> << </b> </p>
                <?php
                    }
                ?>
            </div> 
        </td>
    </tr>
</table>
</div>
<script type="text/javascript">
 $('input, select').attr('disabled', 'disabled');
 $("#form_submit_button").removeAttr('href');
 $("#form_submit_button").attr("onclick","");
 $("#form_submit_button").attr("style","opacity:0.4;");
 _gaq.push(['_trackPageview', '/createAccountSuccess/']);
 </script>
<?php } 

$dialUpCodeAry = array();
$dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true); 
$szLabelClassName = "class='common-fields-container clearfix' "; ?>	
	
 <div class="font-22">
<form name="createAccount" id="createAccount" method="post">
	<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
			<span class="field-container"><input type="text" name="createAccountArr[szFirstName]" id="szFirstName" value="<?=$_POST['createAccountArr']['szFirstName']?>" onblur="closeTip('fname');" onfocus="openTip('fname');"/></span>
			<div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
			<span class="field-container"><input type="text" name="createAccountArr[szLastName]" id="szLastName" value="<?=$_POST['createAccountArr']['szLastName']?>" onblur="closeTip('lname');" onfocus="openTip('lname');"/></span>
			<div class="field-alert"><div id="lname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/email');?></span>
			<span class="field-container"><input type="text" name="createAccountArr[szEmail]" id="szEmail" onblur="closeTip('email');" onfocus="openTip('email');" value="<?=$_POST['createAccountArr']['szEmail']?>" /></span>
			<div class="field-alert"><div id="email" style="display:none;"><?=t($t_base.'messages/email');?></div></div>
		</label>	
			<label <?php echo $szLabelClassName; ?>>	
			<p class="checkbox-container" style="margin-left: 30%;"><input type="checkbox" name="createAccountArr[iSendUpdate]" id="iSendUpdate" onblur="closeTip('news');" onfocus="openTip('news');" value="1" <?php  if($_POST['createAccountArr']['iSendUpdate']==1){ ?> checked <?php }?>/><?=t($t_base.'fields/news_update_send');?></p>
			<div class="field-alert"><div id="news" style="display:none;"><?=t($t_base.'messages/news_update_send');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/password');?></span>
			<span class="field-container"><input type="password" name="createAccountArr[szPassword]" id="szPassword" onblur="closeTip('pass');" onfocus="openTip('pass');"/></span>
			<div class="field-alert"><div id="pass" style="display:none;"><?=t($t_base.'messages/password_msg');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/re_password');?></span>
			<span class="field-container"><input type="password" name="createAccountArr[szConfirmPassword]" id="szConfirmPassword"/></span>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/c_name');?> </span>
			<span class="field-container"><input type="text" name="createAccountArr[szCompanyName]" id="szCompanyName" onblur="closeTip('cname');" onfocus="openTip('cname');" value="<?=$_POST['createAccountArr']['szCompanyName']?>"/></span>
			<div class="field-alert"><div id="cname" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/c_reg_n');?></span>
			<span class="field-container"><input type="text" name="createAccountArr[szCompanyRegNo]" id="szCompanyRegNo" onblur="closeTip('c_reg_no');" onfocus="openTip('c_reg_no');" value="<?=$_POST['createAccountArr']['szCompanyRegNo']?>"/></span>
			<div class="field-alert"><div id="c_reg_no" style="display:none;"><?=t($t_base.'messages/comapany_reg_no');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
			<span class="field-container"><input type="text" name="createAccountArr[szAddress1]" id="szAddress1" onblur="closeTip('address1');" onfocus="openTip('address1');" value="<?=$_POST['createAccountArr']['szAddress1']?>"/></span>
			<div class="field-alert"><div id="address1" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="createAccountArr[szAddress2]" id="szAddress2" onblur="closeTip('address2');" onfocus="openTip('address2');" value="<?=$_POST['createAccountArr']['szAddress2']?>"/></span>
			<div class="field-alert"><div id="address2" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="createAccountArr[szAddress3]" id="szAddress3" onblur="closeTip('address3');" onfocus="openTip('address3');" value="<?=$_POST['createAccountArr']['szAddress3']?>"/></span>
			<div class="field-alert"><div id="address3" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
			<span class="field-container"><input type="text" name="createAccountArr[szPostCode]" id="szPostCode" onblur="closeTip('postcode');" onfocus="openTip('postcode');" value="<?=$_POST['createAccountArr']['szPostCode']?>"/></span>
			<div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/city');?></span>
			<span class="field-container"><input type="text" name="createAccountArr[szCity]" id="szCity" onblur="closeTip('city');" onfocus="openTip('city');" value="<?=$_POST['createAccountArr']['szCity']?>"/></span>
			<div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><input type="text" name="createAccountArr[szState]" id="szState" onblur="closeTip('state');" onfocus="openTip('state');" value="<?=$_POST['createAccountArr']['szState']?>"/></span>
			<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
                    <span class="field-name"><?=t($t_base.'fields/country');?></span>
                    <span class="field-container">
                        <select size="1" name="createAccountArr[szCountry]" id="szCountry">
                            <option value=""><?=t($t_base.'fields/select_country');?></option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                    foreach($allCountriesArr as $allCountriesArrs)
                                    {
                                        ?><option value="<?=$allCountriesArrs['id']?>" <?php if($allCountriesArrs['id']==$_POST['createAccountArr']['szCountry']){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option> <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="phone-container">
                             <select size="1"  name="createAccountArr[idInternationalDialCode]" id="idInternationalDialCode" onblur="closeTip('pNumber');" onfocus="openTip('pNumber');">
                                <?php
                                   if(!empty($dialUpCodeAry))
                                   {
                                        $usedDialCode = array();
                                       foreach($dialUpCodeAry as $dialUpCodeArys)
                                       {
                                            if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                            {
                                                $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                            ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$_POST['createAccountArr']['idInternationalDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                            <?php
                                            }
                                       }
                                   }
                               ?>
                            </select>
                            <input type="text" name="createAccountArr[szPhoneNumber]" id="szPhoneNumber" value="<?=$_POST['createAccountArr']['szPhoneNumber']?>"/>
                        </span>
			<div class="field-alert1"><div id="pNumber" style="display:none;"><?=t($t_base.'messages/phone_number');?></div></div>
		</label>
		<label <?php echo $szLabelClassName; ?>>
			<span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
			<span class="field-container">
                            <select size="1" name="createAccountArr[szCurrency]" id="szCurrency" onblur="closeTip('currency');" onfocus="openTip('currency');">
                                <?php 
                                    if(!empty($allCurrencyArr))
                                    {
                                        foreach($allCurrencyArr as $allCurrencyArrs)
                                        {
                                            ?><option value="<?=$allCurrencyArrs['id']?>" <?php if($allCurrencyArrs['id']==$_POST['createAccountArr']['szCurrency']){?> selected <?php }?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                                        }
                                    }
                                ?>
                            </select>
			</span>
			<div class="field-alert1"><div id="currency" style="display:none;"><?=t($t_base.'messages/currency');?></div></div>
		</label>	

<input type="hidden" name="createAccountArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value=""/>
<input type="hidden" name="createAccountArr[szBookingRandomNumber]" value="<?=$_POST['createAccountArr']['szBookingRandomNumber']?>" id="szBookingRandomNumber"/>
<input type="hidden" name="createAccountArr[iIncompleteProfile]" value="<?=$_POST['createAccountArr']['iIncompleteProfile']?>" id="iIncompleteProfile"/>
 <br>
</form>		
 </div>

 <?php
if($booking_create_page=="true"){
?>
<div class="oh">
    <p class="fl-80" align="right">Click to create your profile and continue your booking</p>        
       <p align="right"  class="fl-20">
     <a href="javascript:void(0)" class="button1" onclick="javscript:create_account();"><span><?=t($t_base.'fields/button');?></span></a></p>
</div>
<p align="right">
    <a href="javascript:void(0)" class="button2" href="<?=__SELECT_SERVICES_URL__?>"><span>Back</span>
</p>
<?php }else if($_POST['createAccountArr']['iIncompleteProfile']==1){ ?>		
    <p align="center"><a href="javascript:void(0)" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');create_account();" id="form_submit_button"><span><?=t($t_base.'fields/complete_profile');?></span></a></p>
<?php }else{  ?>
    <p align="center"><a href="javascript:void(0)" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');create_account();" id="form_submit_button"><span><?=t($t_base.'fields/button');?></span></a></p>	
<?php } ?>
