<?php

/* 
 * To calculate Read Message Task Response 
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$kDashBoardAdmin = new cDashboardAdmin();
$readMessageArr=$kDashBoardAdmin->getReadMessageResponseTime('',true);
        
$szTurnoverFileName = $kDashBoardAdmin->downloadReadMessageTaskSheet($readMessageArr); 


ob_clean();
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename=Read_Message_Task_'.date('Ymd').'.xlsx');
ob_clean();
flush();
readfile($szTurnoverFileName);
exit;
?>