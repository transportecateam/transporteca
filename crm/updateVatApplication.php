<?php
//session_start();
/**
 * Currency conversion cronjob
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
 
$kConfig = new cConfig();  
$kVatApplication = new cVatApplication();

$countryFromListAry = array();
$countryFromListAry = $kConfig->getAllCountryInPair_demo(true);

$countryToListAry = array();
$countryToListAry = $kConfig->getAllCountryInPair_demo(true);

if(!empty($countryFromListAry) && !empty($countryToListAry))
{
    foreach($countryFromListAry as $countryFromListArys)
    {
        foreach($countryToListAry as $countryToListArys)
        {
            if($countryFromListArys['id']!=$countryToListArys['id'])
            {
                $addVatLogsAry = array();
                $addVatLogsAry['idCountryFrom'] = $countryFromListArys['id'];
                $addVatLogsAry['idCountryTo'] = $countryToListArys['id'];
                $kVatApplication->addVatApplicationData($addVatLogsAry);
            } 
        }
    }
}