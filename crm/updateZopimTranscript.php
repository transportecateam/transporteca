<?php
ob_start();
session_start();
ini_set ('max_execution_time',60000); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
/*
 * Fetching chat Transcript from Zopim chat server
 */
$kChat = new cChat();
$resultAry = $kChat->fetchChatTrascript();

?>