<?php
ob_start();
session_start();
ini_set('max_execution_time',1200);
ini_set('MEMORY_LIMIT','512M');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$kDBImport=new cDBImport();
$filename = __APP_PATH__ . '/crm/tblcourierdeliverytime.csv';
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $courierDeliveryDatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
        
        if(!empty($courierDeliveryDatas))
        {
            for($i=2;$i<=count($courierDeliveryDatas);$i++)
            {
                $courierDeliveryArr['iCourierID']=$courierDeliveryDatas[$i][0];
                $courierDeliveryArr['iCountry']=$courierDeliveryDatas[$i][1];
                $courierDeliveryArr['szPostcode']=$courierDeliveryDatas[$i][2];
                $courierDeliveryArr['iFromHour']=$courierDeliveryDatas[$i][3];
                $courierDeliveryArr['iFromMinute']=$courierDeliveryDatas[$i][4];
                $courierDeliveryArr['iToHour']=$courierDeliveryDatas[$i][5];
                $courierDeliveryArr['iToMinute']=$courierDeliveryDatas[$i][6];
                $kDBImport->importCourierDeliveryTime($courierDeliveryArr);
            }
        }
?>