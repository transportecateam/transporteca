<?php

/* 
 * To calculate Call User Task Response 
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$kDashBoardAdmin = new cDashboardAdmin();
$callUserArr=$kDashBoardAdmin->getCallUserResponseTime('',true);
        
$szTurnoverFileName = $kDashBoardAdmin->downloadCallUserTaskSheet($callUserArr); 


ob_clean();
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename=Call_User_Task_'.date('Ymd').'.xlsx');
ob_clean();
flush();
readfile($szTurnoverFileName);
exit;
?>