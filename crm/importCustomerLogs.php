<?php
//session_start();
/**
 * Currency conversion cronjob
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 
 
$kCustomerLogs = new cCustomerLogs();

$dtSent = date('Y-m-d',strtotime("-6 MONTH"));

$emailLogsAry = array();
$emailLogsAry = $kCustomerLogs->getAllFileEmailLogs($dtSent);

if(!empty($emailLogsAry))
{
    foreach($emailLogsAry as $emailLogsArys)
    {
        //$kCustomerLogs->addEmailDataToCustomerLogSnippet($emailLogsArys);
    }
}

$reminderLogsAry = array();
$reminderLogsAry = $kCustomerLogs->getAllFileReminderLogs($dtSent);
 
if(!empty($reminderLogsAry))
{
    foreach($reminderLogsAry as $reminderLogsArys)
    { 
        //$kCustomerLogs->addReminderNotesDataToCustomerLogs($reminderLogsArys);
    }
} 

$bookingSearchLogsAry = array();
$bookingSearchLogsAry = $kCustomerLogs->getAllFileSearchLogs($dtSent);

if(!empty($bookingSearchLogsAry))
{
    foreach($bookingSearchLogsAry as $bookingSearchLogsArys)
    {
        //$kCustomerLogs->addBookingSearchDataToCustomerLogSnippet($bookingSearchLogsArys);
    }
}

$zopimChatLogsAry = array();
$zopimChatLogsAry = $kCustomerLogs->getAllZopimChatLogs($dtSent);

if(!empty($zopimChatLogsAry))
{
    foreach($zopimChatLogsAry as $zopimChatLogsArys)
    {
       // $kCustomerLogs->addZopimChatDataToCustomerLogSnippet($zopimChatLogsArys);
    }
} 
