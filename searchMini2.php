<?php
ob_start();
session_start(); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/search_header.php" );
$iLanguage = getLanguageId(); 
$t_base = "LandingPage/";

$kWhsSearch = new cWHSSearch();  
$arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      
  
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__']; 
$idSeoPage = $_SESSION['seo_page_id']; 

?> 
    <div id="all_available_service" style="display:none;"></div> 
    <section onmouseout="hideDatePicker();">    
         
        <div id="transporteca_search_form_container" class="search-mini-2-container">
           <?php 
               echo display_new_search_form($kBooking,$postSearchAry,true,false,false,$szDefaultFromField,$idSeoPage,true,$idDefaultLandingPage,false,2);
           ?>
       </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function(){ 
            $(".hasDatepicker").on("blur", function(e) { $(this).datepicker("hide"); });
        }); 
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
    </body>
</html>