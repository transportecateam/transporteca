<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$iLanguage = getLanguageId();

$iLanguage = __LANGUAGE_ID_DANISH__;
$kConfig = new cConfig();
$countryAry=$kConfig->getAllCountries(false,$iLanguage);
$szMetaTitle="Transporteca | Search LCL Shipping Online";

$t_base = "LandingPage/";

?>
	<html xmlns="https://www.w3.org/1999/xhtml">
	<head>
	<script type="text/javascript">
                  if (document.location.protocol == 'https:')
                  {
                        var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
                  }
                  else
                  {
                        var __JS_ONLY_SITE_BASE__ = "<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>";
                  }                 
      </script>	
       <link rel="icon" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />
       <link rel="shortcut icon" type="image/x-icon" href="<?=__BASE_STORE_IMAGE_URL__?>/favicon.ico" />
	   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
	   <? if(!empty($szMetaKeywords)){?>
	   <meta name="keywords" content="<?=$szMetaKeywords;?>" />
	   <? } 
	   	  if(!empty($szMetaDescription)){?>
	   <meta name="description" content="<?=$szMetaDescription;?>" />
	   <? } ?>
	    <title id="metatitle"><? if(!empty($szMetaTitle)){ echo $szMetaTitle; }?></title>
	    		
	    <?
	    	if(__ENVIRONMENT__ == "LIVE")
	    	{
	    		define("__BASE_STORE_IMAGE_URL__",__BASE_URL_SECURE__."/images");
	    		?>
	    		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery-latest.js"></script>
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/style.css" />
				<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/functions.js"></script>
				<script type='text/javascript' src='<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.autocomplete.js'></script>
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/jquery.autocomplete.css" />
				<link rel="stylesheet" href="<?=__BASE_STORE_SECURE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
				<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/ui.datepicker.js"></script>
	    		<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/custom.js"></script>
	    		<?
	    	}
	    	else 
	    	{
	    		define("__BASE_STORE_IMAGE_URL__",__BASE_URL__."/images");
	    ?>
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
				
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/style.css" />
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
				<script type='text/javascript' src='<?=__BASE_STORE_JS_URL__?>/jquery.autocomplete.js'></script>
				<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/jquery.autocomplete.css" />
				<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/ui.datepicker.css" type="text/css" media="screen" />
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/ui.datepicker.js"></script>
				<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/custom.js"></script>
				
				<link rel="alternate" type="application/rss+xml" title="Transporteca" title="RSS 2.0" href="<?=__BASE_URL__.'/rss'?>" />
				<link rel="alternate" type="text/xml" title="RSS .92" href="<?=__BASE_URL__.'/rss'?>" />
				<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?=__BASE_URL__.'/rss'?>" />
		<?
			}
		?>
		<style type="text/css">

          .search-new{width:100%;margin-bottom:0px;}
          .fields p{margin:0 auto;width:228px;text-align:left;}
          .search-new .fields{height:189px;margin-bottom:0;}
		  form{margin:0;display:inline;}
		  .search-option{padding:0px;}
		</style>
		</head>
		<body>
		<div id="all_available_service" style="display:none;"></div>
			<div class="search-option">
			<div class="search-new mini2">
			<h6 align="center">Prøv det?</h6>
				<form action="<?=__LANDING_PAGE_URL__.'/'?>" id="start_search_form" name="start_search_form" method="post" target="_top">
					<div class="oh fields">
						<p class="fl-40">
							<select name="landingPageAry[szOriginCountry]" tabindex="1" class="styled" id="szOriginCountry">
								<option value="">Forsendelse Fra</option>
								<?php
								 	if(!empty($countryAry))
								 	{
								 		foreach($countryAry as $countryArys)
								 		{
								 			?>
								 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idOriginCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,15)?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</p>
						<p class="fl-40">
							<select name="landingPageAry[szDestinationCountry]" tabindex="2" class="styled" onkeyup="on_enter_key_press(event,'compare_validate_home_page');" id="szDestinationCountry">
								<option value="">Orsendelse til</option>
								 <?php
								 	if(!empty($countryAry))
								 	{
								 		foreach($countryAry as $countryArys)
								 		{
								 			?>
								 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idDestinationCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,15)?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</p>
						<input name="landingPageAry[iCheck]" type="hidden" value="3" id="iCheck">
						<input name="landingPageAry[szBookingRandomNum]" type="hidden" value="<?=$szBookingRandomNum?>" id="szBookingRandomNum">
						
						<p class="fl-33">
							<a class="orange-button2"  onclick="compare_validate_home_page()" tabindex="3" href="javascript:void(0);"><span>FIND PRISER OG SERVICES</span></a>
						</p>
						<br/><br/>
					</div>
				</form>
				</div>
				</div>
				</body>
				</html>