<?php
/**
 * Blog  
 */
 ob_start();
session_start();

$szMetaTitle= __CARGO_READYNESS_META_TITLE__;
$szMetaKeywords = __CARGO_READYNESS_META_KEYWORDS__;
$szMetaDescription = __CARGO_READYNESS_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/header.php" );
$t_base="TermCondition/";
$kExplain = new cExplain();
$blogContent = $kExplain->serviceBlog(3);
?>

<div id="hsbody-2">
	<div class="hsbody-2-left account-links">
			<?php require_once( __APP_PATH__ ."/layout/blog_left_nav.php" ); ?>
	</div>	

	<div class="hsbody-2-right">	
			<?=$blogContent['szContent']?>
	<br />
	
	<br />
	
	</div>

</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer.php" );
?>