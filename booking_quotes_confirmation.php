<?php
ob_start();
session_start();
 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$display_abandon_message = true;
$iDonotIncludeHiddenSearchHeader = 1; 
$szMetaTitle = __BOOKING_CONFIRMATION_META_TITLE__;
$szMetaKeywords = __BOOKING_CONFIRMATION_META_KEYWORDS__;
$szMetaDescription = __BOOKING_CONFIRMATION_META_DESCRIPTION__;
 
if((int)$_SESSION['user_id']<=0)
{
    /*
    *  We have allowed customer to reach this page even if they have not logged in only for Quick Quote bookings
    */
    $kBooking = new cBooking();
    $kUser = new cUser();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $kUser = new cUser();
    
    $postSearchAry = array();
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__ && $postSearchAry['idUser']>0)
    {
        /*
        *  If user is not logged in and booking type is Quick Quote then we have made user automatically logged in to the system
        */ 
        $kUser->manualLogin($postSearchAry['idUser']);
    }
}

require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingConfirmation/";
$t_base_details = "BookingDetails/";

$iLanguage = getLanguageId();

$userSessionData = array();
$userSessionData = get_user_session_data(true);
$idUserSession = $userSessionData['idUser'];

constantApiKey();
$kBooking = new cBooking();
$kConfig = new cConfig();
$kRegisterShipCon = new cRegisterShipCon();
$kWHSSearch = new cWHSSearch(); 

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
$_SESSION['load-booking-details-page-for-booking-id'] = $szBookingRandomNum ;
  
$postSearchAry = array();
$postSearchAry = $kBooking->getBookingDetails($idBooking);

if((int)$_SESSION['user_id']<=0)
{ 
    $postSearchAry = $kBooking->getBookingDetails($idBooking); 
    $kConfig_new = new cConfig(); 
    $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig_new->szCountryISO ;  
    $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
    $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
    
    $redirect_url=  __BOOKING_QUOTE_SERVICE_PAGE_URL__."/".$szBookingRandomNum."/".__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__."-".$szCountryStrUrl."/" ; 
    header("Location: ".$redirect_url);
    exit(); 
}  
 
$iTodayMinusOne = strtotime("-1 DAY"); 
$iTodayMinusOneDateTime = strtotime(date('Y-m-d 23:59:59',$iTodayMinusOne));

$bQuickQuotePaymentAwaiting = false; 
$bManualQuickQuoteBooking = false;
$iQuotesStatus = 5;

if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
{
    /*
    * This flag represts that this booking was created from quick quote screen of Management
    */
    $bQuickQuotePaymentAwaiting = true;
    /*
     * This flag represts that booking was created by using 'Send Quote' button
     */
    $bManualQuickQuoteBooking = true;
    
    /*
    * Quick quote bookings will be treated as Automatic bookings
    */
    $iQuotesStatus = 0;
}
else if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
{
    /*
    * This flag represts that this booking was created from quick quote screen of Management and this blocks represents that booking was created using button 'Make Booking'
    */
     $bQuickQuotePaymentAwaiting = true;
     $szQuickQuotePaymentType = $postSearchAry['szQuickQuotePaymentType']; 
    /*
    * Quick quote bookings will be treated as Automatic bookings
    */
    $iQuotesStatus = 0;
}
else if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
{
    echo display_booking_already_paid($t_base,$idBooking);
    die;
}
else if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
{
     $bQuickQuoteBooking = true;
} 
else if((strtotime($postSearchAry['dtQuoteValidTo']) < $iTodayMinusOneDateTime) || ($postSearchAry['iQuotesStatus']==__BOOKING_QUOTES_STATUS_EXPIRED__))
{ 
    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig->szCountryISO ;
    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
    $szCountryStrUrl .= $kConfig->szCountryISO ; 

    $redirect_url =  __BOOKING_QUOTE_EXPIRED_PAGE_URL__."/".$szBookingRandomNum."/".__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__."-".$szCountryStrUrl."/" ;
   
    ob_end_clean();
    header("location:".$redirect_url);
    exit;
}
	 
if($_SESSION['booking_id']==$idBooking)
{
    $kBooking->updatePaymentFlag($idBooking,0);
    $_SESSION['booking_id'] = '';
    unset($_SESSION['booking_id']);
}
if((int)$_REQUEST['idBooking']>0)
{
    $idBooking = (int)$_REQUEST['idBooking'] ;
    //$_SESSION['booking_id'] = $idBooking ;
}
if((int)$idBooking>0)
{
    $serviceTypeAry= array();
    $postSearchAry = array();
    $cargoAry = array();
    $shipperConsigneeAry = array();

    if((int)$idBooking>0 && (int)$idUserSession>0)
    {
        /*
        * Update VAT depending on customer Type Selection
        */
        $kVatApplication = new cVatApplication();
        $kVatApplication->updateVatonBooking($idBooking);
        
        /*
         * Updating User details to Bookings
         */
        $kUser->getUserDetails($idUserSession);
        $res_ary=array();
        $res_ary['idUser'] = $kUser->id ;				
        $res_ary['szFirstName'] = $kUser->szFirstName ;
        $res_ary['szLastName'] = $kUser->szLastName ;
        $res_ary['szEmail'] = $kUser->szEmail ;	
        $res_ary['szCustomerAddress1'] = $kUser->szAddress1;
        $res_ary['szCustomerAddress2'] = $kUser->szAddress2;
        $res_ary['szCustomerAddress3'] = $kUser->szAddress3;
        $res_ary['szCustomerPostCode'] = $kUser->szPostcode;
        $res_ary['szCustomerCity'] = $kUser->szCity;
        $res_ary['szCustomerCountry'] = $kUser->szCountry;
        $res_ary['szCustomerState'] = $kUser->szState;
        $res_ary['iPrivateShipping'] = $kUser->iPrivate;
        $res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
        $res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
        $res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;	
        if((int)$postSearchAry['iBookingStep']<11)
        {
            $res_ary['iBookingStep'] = 11 ;
        }

        if(!empty($res_ary))
        {
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }		
        $update_query = rtrim($update_query,",");
        $kBooking->updateDraftBooking($update_query,$idBooking);
    }
    $iLanguage = getLanguageId();
    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking,$iLanguage);

    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig->szCountryISO ;
    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
    $szCountryStrUrl .= $kConfig->szCountryISO ; 

    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
    {
        $redirect_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__."-".$szCountryStrUrl."/" ;
    }
    else
    {
        $redirect_url =  __BOOKING_QUOTE_SERVICE_PAGE_URL__."/".$szBookingRandomNum."/".__BOOKING_QUOTE_SERVICE_PAGE_SUFFIX__."-".$szCountryStrUrl."/" ;
    } 
    //print_r($postSearchAry);

    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
    // geting  service type 
    $serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
    $serviceTypeAry=$serviceTypeAry[$postSearchAry['idServiceType']];

    //getting shipper consignee details
    //$shipperConsigneeAry = $kRegisterShipCon->getShipperConsigneeDetails($idBooking); 
    $szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br> ".$postSearchAry['szOriginCountry']; 
    $szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br> ".$postSearchAry['szDestinationCountry']; 
}
else
{
    //header("Location:".__HOME_PAGE_URL__);
    //die; 
    $redirect_url = __HOME_PAGE_URL__ ;
    ?>
    <div id="change_price_div">
    <?php
    display_booking_notification($redirect_url);
    echo "</div>";
} 
if(((int)$idUserSession>0) && ($_REQUEST['err']<=0))
{ 
    checkUserPrefferedCurrencyActive();
} 

/*
* Checking Insurance is available for this or not. By default we assume we will have Insurance untill shipper/consignee selected
*/ 
if(!empty($postSearchAry))
{
    $insuranceInputAry = array();
    $insuranceInputAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
    $insuranceInputAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
    $insuranceInputAry['idTransportMode'] = $postSearchAry['idTransportMode'];
    $insuranceInputAry['isMoving'] = $postSearchAry['isMoving'];
    $insuranceInputAry['fTotalBookingPriceUsd'] = 1;
    $insuranceInputAry['szCargoType'] = $postSearchAry['szCargoType']; 

    $kInsurance = new cInsurance();
    if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry))
    {
        /*
        * This means we do have Transportation Insurance for this Trade and should show all available option on this screen
        */ 
        $iTransportationInsuranceAvailable = 1;
    }
    else
    {
        /*
        * This means we do not have Transportation Insurance for this Trade and should show only 'Not available' option to select
        */  
        $iTransportationInsuranceAvailable = 2;
        $insuranceAry = array();
        $insuranceAry['iUpdateInsuranceChoice'] = 1;
        $kBooking->updateInsuranceDetails($insuranceAry,$postSearchAry,'REMOVE');
        $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking,$iLanguage); 
    }
} 

/**
* This function recalculate pricing after a perticular interval set in constants.
* 
**/
$red_url = __BOOKING_CONFIRMATION_PAGE_URL__.'/'.$szBookingRandomNum.'/';
//echo check_price_30_minute($postSearchAry,$red_url,false,$idBooking);
 


//$cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($idBooking);
$szCargoCommodity = $postSearchAry['szCargoDescription'];

$cargo_volume = format_volume($postSearchAry['fCargoVolume']);
$cargo_weight = get_formated_cargo_measure((float)($postSearchAry['fCargoWeight'])) ;

if($iLanguage==__LANGUAGE_ID_DANISH__)
{
    $szSecureImageName	= "secure-payment-da.png";
   // $cargo_weight = number_format((float)$postSearchAry['fCargoWeight'],0,'.','.');
}
else
{
    $szSecureImageName = "secure-payment.png";
   //$cargo_weight = number_format((float)$postSearchAry['fCargoWeight'],0,'.',',');
}

$cargo_weight = number_format_custom($postSearchAry['fCargoWeight'],$iLanguage,0);

if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
{
    $szBankTransferDueDateTime = strtotime($postSearchAry['dtCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60); 

    $iMonth = date('m',$szBankTransferDueDateTime);

    $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime); 
    $szMonthName = getMonthName($iLanguage,$iMonth);
    $szBankTransferDueDateTime .=" ".$szMonthName ;
}
else
{
    $szBankTransferDueDateTime = strtotime($postSearchAry['dtWhsCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);

    $iMonth = date('m',$szBankTransferDueDateTime); 
    $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);  

    $szMonthName = getMonthName($iLanguage,$iMonth);
    $szBankTransferDueDateTime .=" ".$szMonthName ;
}     
$szClassName_1 = "fl-40";
$szClassName_2 = "fl-60";

$idCurrency = $postSearchAry['idCustomerCurrency'];
$currencyDetailsAry = array();
$currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);  

$szTermsAndConditionText = terms_condition_popup_html($postSearchAry,$t_base,$iLanguage,true);

if($postSearchAry['iInsuranceIncluded']==1)
{
    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] ;
}
else
{
    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] ;
}

$fTotalPriceCustomerCurrency_no = $postSearchAry['fTotalPriceCustomerCurrency'] ;
$fTotalPriceCustomerCurrency_no = getPriceByLang($fTotalPriceCustomerCurrency_no,$iLanguage);

$fTotalPriceCustomerCurrency_yes = $postSearchAry['fTotalPriceCustomerCurrency'] + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']) ;
$fTotalPriceCustomerCurrency_yes = getPriceByLang($fTotalPriceCustomerCurrency_yes,$iLanguage);

//echo "Cust: ".$postSearchAry['fTotalPriceCustomerCurrency']. "br> VAT: ".$postSearchAry['fTotalVat']." <br> Ins: ".$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'];
$fTotalBookingInvoice_yes = round($postSearchAry['fTotalPriceCustomerCurrency']) + round($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']) + $postSearchAry['fTotalVat'] ;
$fTotalBookingInvoice_yes = getPriceByLang($fTotalBookingInvoice_yes,$iLanguage,2); 

$fTotalBookingInvoice_No = round($postSearchAry['fTotalPriceCustomerCurrency']) + $postSearchAry['fTotalVat'] ;
$fTotalBookingInvoice_No = getPriceByLang($fTotalBookingInvoice_No,$iLanguage,2);

$fTotalVAT = getPriceByLang($postSearchAry['fTotalVat'],$iLanguage,2); 
   
$iDisplayCCOption = true; 
$bDisplayInsurancePrices = true; 
if($postSearchAry['iUpdateInsuranceWithBlankValue']==1)
{
    $bDisplayInsurancePrices = false;
} 

//echo " V: ".$postSearchAry['isManualCourierBooking'];

$kWhsSearch = new cWHSSearch();
$ShowPaypalOption=false;
$iPaypalStatus = $kWhsSearch->getManageMentVariableByDescription('__PAYPAL_STATUS__');
if($iPaypalStatus=='ENABLED')
{
    $kAdmin = new cAdmin();
    if(!$kAdmin->getListPaypalCoutries($postSearchAry['szCustomerCountry']))
    {
        $ShowPaypalOption=true;
    } 
}

/*
 * Building service string
 */
$szServiceDescriptionString = display_service_type_description($postSearchAry,$iLanguage); 

/*
 * Building Shipper address block
 */
$shipperAddress = '';
if(!empty($postSearchAry['szShipperCompanyName']))
{
    $shipperAddress.=$postSearchAry['szShipperCompanyName'];
}  
if(!empty($postSearchAry['szShipperAddress_pickup']))
{
    $shipperAddress.= ", ".$postSearchAry['szShipperAddress_pickup'];
}
if(!empty($postSearchAry['szShipperAddress2']))
{
    $shipperAddress .=", ".$postSearchAry['szShipperAddress2'] ;
}
if(!empty($postSearchAry['szShipperAddress3']))
{
    $shipperAddress .=", ".$postSearchAry['szShipperAddress3'];
}
if(!empty($postSearchAry['szShipperPostCode']) || !empty($postSearchAry['szShipperCity']))
{
    if(!empty($postSearchAry['szShipperPostCode']) && !empty($postSearchAry['szShipperCity']))
    {
        $shipperAddress.=", ".$postSearchAry['szShipperPostCode']." ".$postSearchAry['szShipperCity']."";
    }
    else if(!empty($postSearchAry['szShipperPostCode']))
    {
        $shipperAddress.=", ".$postSearchAry['szShipperPostCode'];
    }
    else if(!empty($postSearchAry['szShipperCity']))
    {
        $shipperAddress.=", ".$postSearchAry['szShipperCity'];
    }
} 
                    
/*
*  Building Consignee Address block
*/
$ConsigneeAddress = '';
if(!empty($postSearchAry['szConsigneeCompanyName']))
{
    $ConsigneeAddress.=$postSearchAry['szConsigneeCompanyName'];
}  
if(!empty($postSearchAry['szConsigneeAddress']))
{
    $ConsigneeAddress.=", ".$postSearchAry['szConsigneeAddress'];
}
if(!empty($postSearchAry['szConsigneeAddress2']))
{
    $ConsigneeAddress .=", ".$postSearchAry['szConsigneeAddress2'];
}
if(!empty($postSearchAry['szConsigneeAddress3']))
{
    $ConsigneeAddress .=", ".$postSearchAry['szConsigneeAddress3'];
}
if(!empty($postSearchAry['szConsigneePostCode']) || !empty($postSearchAry['szConsigneeCity']))
{
    if(!empty($postSearchAry['szConsigneePostCode']) && !empty($postSearchAry['szConsigneeCity']))
    {
        $ConsigneeAddress.=", ".$postSearchAry['szConsigneePostCode']." ".$postSearchAry['szConsigneeCity']."";
    }
    else if(!empty($postSearchAry['szConsigneePostCode']))
    {
        $ConsigneeAddress.=", ".$postSearchAry['szConsigneePostCode'];
    }
    else if(!empty($postSearchAry['szConsigneeCity']))
    {
        $ConsigneeAddress.=", ".$postSearchAry['szConsigneeCity'];
    } 
}

/*
* Building cargo string
*/
$szCargoText = $cargo_volume." ".t($t_base.'fields/cbm').", ".$cargo_weight." ".t($t_base.'fields/kg').", ".utf8_decode($szCargoCommodity); 

/*
 * Total Insurance Price to be displayed
 */
$szInvoiceTotal = $postSearchAry['szCurrency']." ".$fTotalBookingInvoice_yes;
$szInvoiceTotalWithouthInsurance = $postSearchAry['szCurrency']." ".$fTotalBookingInvoice_No; 
if($postSearchAry['iInsuranceIncluded']==1 && $bDisplayInsurancePrices)
{
    $szTotalInvoiceToBeDisplayed = $szInvoiceTotal; 
}
else
{
    $szTotalInvoiceToBeDisplayed = $szInvoiceTotalWithouthInsurance; 
} 
?> 
<script src="<?php echo __BASE_STORE_SECURE_JS_URL__;?>/nanoslider.js?v=2" type="text/javascript"></script>
<script type="text/javascript">
    $().ready(function() {	
        $(".nano").nanoScroller({scroll: 'top'});
    });
</script>
<!-- These are used to contain data from diffrent popups -->
<div id="customs_clearance_pop" class="help-pop"></div>
<div id="customs_clearance_pop_right" class="help-pop right"></div>
<div id="rating_popup"></div>
<div id="change_price_div" style="display:none">
<?php
if(!empty($_SESSION['booking_id']) && ($_SESSION['booking_id'] != $idBooking))
{
	echo display_booking_not_found_popup(__MAIN_SITE_HOME_PAGE_URL__."".$_SERVER['REQUEST_URI']);
}
?>
</div>	
<div id="booking_confirm_error" class="errorBox " style="display:none;"></div>
<div id="account_confirm_resend_email" style="display:none;"></div>	
	
<div id="hsbody-2">	  
    <div class="hsbody-2-right"> 
        <?php 
            if($_SESSION['statusFailed']=='Failed')
            {
                unset($_SESSION['statusFailed']);
                $_SESSION['statusFailed']='';
                ?>
                <div id="regError" class="errorBox ">
                <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
                <div id="regErrorList">
                <?php
                    if($_SESSION['szErrorMsg']!='')
                    {
                       ?>
                        <ul>
                            <li><?=t($t_base_details.'messages/payment_declined').": ".$_SESSION['szErrorMsg'];?></li>
                        </ul>
                    <?php
                        unset($_SESSION['szErrorMsg']);
                        $_SESSION['szErrorMsg']='';
                    }else{?>
                <ul>
                <li><?=t($t_base_details.'messages/payment_transaction_is_failed');?></li>
                </ul>
                <?php }?>
                </div>
                </div>
        <?php } ?>
        <div id="booking_details_container" class="clearfix">  
            <div class="oh even">
                <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/forwarder');?>:</p>
                <p class="<?php echo $szClassName_2; ?>">
                    <a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$postSearchAry['idForwarder']?>')">
                    <?php if($postSearchAry['szForwarderLogo']!='') { 	?>
                            <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$postSearchAry['szForwarderLogo']."&w=180&h=70"?>" alt="<?=$postSearchAry['szForwarderDispName']?>" border="0" />
                    <?php } else{  echo $postSearchAry['szForwarderDispName']; }?>
                    </a>
                </p>
            </div>
            <div class="oh odd">
                <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/service');?>:</p>
                <p class="<?php echo $szClassName_2; ?>"><?php  echo $szServiceDescriptionString; ?></p>
            </div>
            <?php 
                /*
                * We don't show transit_time and frequency for quick quote bookings
                */
                if($bQuickQuotePaymentAwaiting){   ?>
                    <div class="oh even">
                        <p class="<?php echo $szClassName_1; ?>">
                            <?php  
                                    echo t($t_base.'fields/expacted_delivery').":"; 
                            ?>
                        </p>
                        <p class="<?php echo $szClassName_2; ?>">
                        <?php 
                            $dtExpactedDelivery = date('j.',strtotime($postSearchAry['dtAvailable'])); 
                            $iMonth = date('m',strtotime($postSearchAry['dtAvailable']));
                            $szMonthName = getMonthName($iLanguage,$iMonth);
                            $dtExpactedDelivery .=" ".$szMonthName ;
                            echo $dtExpactedDelivery; 
                        ?>
                        </p>
                    </div>  
                    <div class="oh odd">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/cargo_details');?>:</p>
                        <p class="<?php echo $szClassName_2; ?>"><?php echo $szCargoText; ?></p>
                    </div>  
                    <div class="oh even">
                        <p class="<?php echo $szClassName_1; ?>"><?php echo (!empty($postSearchAry['szShipperHeading'])?$postSearchAry['szShipperHeading']:t($t_base.'fields/shipper')); ?>:</p>
                        <p class="<?php echo $szClassName_2; ?>"><?php echo $shipperAddress; ?></p>
                    </div>
                    <div class="oh odd">
                        <p class="<?php echo $szClassName_1; ?>"><?php echo (!empty($postSearchAry['szConsigneeHeading'])?$postSearchAry['szConsigneeHeading']:t($t_base.'fields/consignee')); ?>:</p>
                        <p class="<?php echo $szClassName_2; ?>"> <?php echo $ConsigneeAddress; ?></p>
                    </div>  
            <?php 
                    $szPriceClass='even';
                    $szInsuranceClass="odd"; 
                } else { ?>
                    <div class="oh even">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/transit_time');?>:</p>
                        <p class="<?php echo $szClassName_2; ?>">
                        <?php 
                            echo $postSearchAry['iTransitHours'] ." ".t($t_base.'fields/days'); 
                        ?>
                        </p>
                    </div> 
                    <div class="oh odd">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/frequency');?>:</p>
                        <p class="<?php echo $szClassName_2; ?>">
                        <?php 
                            echo $postSearchAry['szFrequency'] ; 
                        ?>
                        </p>
                    </div>
                    <div class="oh even">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/cargo_details');?>:</p>
                        <p class="<?php echo $szClassName_2; ?>"><?php echo $szCargoText; ?></p>
                    </div>  
                    <div class="oh odd">
                        <p class="<?php echo $szClassName_1; ?>"><?php echo (!empty($postSearchAry['szShipperHeading'])?$postSearchAry['szShipperHeading']:t($t_base.'fields/shipper')); ?>:</p>
                        <p class="<?php echo $szClassName_2; ?>"><?php echo $shipperAddress; ?></p>
                    </div>
                    <div class="oh even">
                        <p class="<?php echo $szClassName_1; ?>"><?php echo (!empty($postSearchAry['szConsigneeHeading'])?$postSearchAry['szConsigneeHeading']:t($t_base.'fields/consignee')); ?>:</p>
                        <p class="<?php echo $szClassName_2; ?>"> <?php echo $ConsigneeAddress; ?></p>
                    </div>  
            <?php 
                $szPriceClass='odd';
                $szInsuranceClass="even";
            
            } ?>  
            <div class="oh <?php echo $szPriceClass; ?>">
                <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/price');?>:</p> 
                <p class="<?php echo $szClassName_2; ?>" id="booking_price_span"> 
                    <?php  
                        if($postSearchAry['iInsuranceChoice']>0 && $bDisplayInsurancePrices)
                        { 
                            if($postSearchAry['iInsuranceIncluded']==1)
                            {
                                $display_no_flag = 'style="display:none;"';
                                $display_yes_flag = 'style="display:block;"';
                            }
                            else
                            {
                                $display_yes_flag = 'style="display:none;"';
                                $display_no_flag = 'style="display:block;"';
                            } 
                            echo '<span id="insurance_price_yes" '.$display_yes_flag.'>'.$postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrency_yes." (".t($t_base.'fields/including_insurance')." ".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).")"."</span>"; 
                            echo '<span id="insurance_price_no" '.$display_no_flag.'>'.$postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrency_no."</span>";
                        }
                        else
                        {
                            $fTotalPriceCustomerCurrencyStr = getPriceByLang($fTotalPriceCustomerCurrency,$iLanguage);
                            echo $postSearchAry['szCurrency']." ".$fTotalPriceCustomerCurrencyStr;
                        }
                    ?>
                </p>
            </div>	
            <?php if($postSearchAry['fTotalVat']>0) {  
                /*
                * We don't show transit_time and frequency for quick quote bookings
                */
                if($bQuickQuotePaymentAwaiting){  $szInsuranceClass="odd"; ?> 
                    <div class="oh odd">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/vat');?>:</p> 
                        <p class="<?php echo $szClassName_2; ?>"><?php  echo $postSearchAry['szCurrency']." ".$fTotalVAT; ?>
                        </p>
                    </div>
                    <div class="oh even">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/total_invoice');?>:</p> 
                        <p class="<?php echo $szClassName_2; ?>" id="total_invoice_conatiner"> <?php echo $szTotalInvoiceToBeDisplayed; ?></p>
                    </div>
                <?php } else { ?>
                    <div class="oh even">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/vat');?>:</p> 
                        <p class="<?php echo $szClassName_2; ?>"><?php  echo $postSearchAry['szCurrency']." ".$fTotalVAT; ?>
                        </p>
                    </div>
                    <div class="oh odd">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/total_invoice');?>:</p> 
                        <p class="<?php echo $szClassName_2; ?>" id="total_invoice_conatiner"> <?php echo $szTotalInvoiceToBeDisplayed; ?></p>
                    </div>
                <?php } ?>
            <?php } ?> 

            <input type="hidden" name="szInvoiceTotal_withinsurance" id="szInvoiceTotal_withinsurance" value="<?php echo $szInvoiceTotal; ?>">
            <input type="hidden" name="szInvoiceTotal_withoutinsurance" id="szInvoiceTotal_withoutinsurance" value="<?php echo $szInvoiceTotalWithouthInsurance; ?>">
            <?php
                if($postSearchAry['iInsuranceChoice']==3) //Insurance: Optional
                { 
                    if($postSearchAry['fValueOfGoods']>0 && $postSearchAry['iOptionalInsuranceWithZeroCargoValue']!=1)
                    {
                         $szOptionYesText = t($t_base.'fields/yes_please_add_insurance_quote'); 
                        $szBookingPriceText = " (".t($t_base.'fields/including_insurance');
                        if($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']>0)
                        {
                                $fTotalInsuranceCostForBookingCustomerCurrency = getPriceByLang($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'],$iLanguage);
                                $szInsuranceCostStr = " (".$postSearchAry['szCurrency']." ".$fTotalInsuranceCostForBookingCustomerCurrency.") ";
                        }
                        $fValueOfGoods = getPriceByLang($postSearchAry['fValueOfGoods'],$iLanguage);
                        $fGoogsPriceStr = " ".$postSearchAry['szGoodsInsuranceCurrency']." ".$fValueOfGoods ;

                        $fTotalValueOfGoods = $postSearchAry['fValueOfGoods'] ;
                        $idInsuranceCurrency = $postSearchAry['idGoodsInsuranceCurrency'] ;    
                        ?> 
                        <div class="<?php echo $szInsuranceClass; ?>">
                            <div class="oh insurance-desc-container"> 
                                <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/insurance'); ?>:</p>
                                <p class="<?php echo $szClassName_2; ?>"> 
                                    <form name="addInsuranceForm" id="addInsuranceForm" method="post" action="javascript:void(0);"> 
                                        <p class="<?php echo $szClassName_2; ?> insurance-checkbox"> 
                                            <span class="checkbox-container">
                                                <input type="radio" onclick="addInsuranceDetails_quotes('<?php echo $szBookingRandomNum; ?>');" name="bookingInsuranceAry[iInsurance]" <?php echo (($postSearchAry['iInsuranceIncluded']==1)?'checked':''); ?> id="iInsurance_yes" value="1"><span id="option_container_span"><?php echo $szOptionYesText." ".$fGoogsPriceStr."".$szInsuranceCostStr;?></span>
                                            </span>  
                                            <span class="checkbox-container">
                                                <input type="radio" onclick="addInsuranceDetails_quotes('<?php echo $szBookingRandomNum; ?>','REMOVE_INSURANCE_DETAILS_QUOTES');" <?php echo (($postSearchAry['iInsuranceIncluded']!=1)?'checked':''); ?> name="bookingInsuranceAry[iInsurance]" id="iInsurance_no" value="2"><span><?=t($t_base.'fields/no_i_dont_need_this_quote');?></span>
                                            </span>
                                            <input type="hidden" name="bookingInsuranceAry[fValueOfGoods]" id="fValueOfGoods" value="<?php echo $fTotalValueOfGoods; ?>">
                                            <input type="hidden" name="bookingInsuranceAry[idInsuranceCurrency]" id="idInsuranceCurrency" value="<?php echo $idInsuranceCurrency; ?>" > 
                                            <input type="hidden" name="booking_key" value="<?php echo $szBookingRandomNum; ?>">   
                                        </p> 
                                    </form>
                                </p> 
                            </div>
                        </div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="<?php echo $szInsuranceClass; ?>">
                            <div class="oh insurance-desc-container">
                                <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/insurance_main_heading');?>?</p>
                                <p class="<?php echo $szClassName_2; ?>"> 
                                    <?php echo t($t_base.'fields/insurance_heading');?> 
                                    <span class="pg-more"><a href="javascript:void(0);" onclick="display_insurance_terms(1);"><?php echo t($t_base.'fields/read_more_insurance');?></a></span>
                                </p> 
                            </div> 
                            <div id="booking_confirmation_insurance_container"> 
                                <?php 
                                    echo display_booking_insurance_form($kBooking,$postSearchAry,1);
                                ?> 
                            </div>
                        </div>
                        <?php
                    } 
                    if($postSearchAry['iInsuranceUpdatedFlag']==1)
                    {
                        $iInsuranceSelectedFlag = 1;
                    }
                }
                else if($postSearchAry['iInsuranceChoice']==1) //Insurance: Yes
                {
                    if($postSearchAry['fValueOfGoods']>0 && $postSearchAry['iIncludeInsuranceWithZeroCargoValue']!=1)
                    {
                        if($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']>0)
                        {
                            $fTotalInsuranceCostForBookingCustomerCurrency = getPriceByLang($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'],$iLanguage);
                            $szInsuranceCostStr = " (".$postSearchAry['szCurrency']." ".$fTotalInsuranceCostForBookingCustomerCurrency.") ";
                        }
                        $fValueOfGoods = getPriceByLang($postSearchAry['fValueOfGoods'],$iLanguage);
                        $fGoogsPriceStr = " ".$postSearchAry['szGoodsInsuranceCurrency']." ".$fValueOfGoods ;

                        $fTotalValueOfGoods = $postSearchAry['fValueOfGoods'] ;
                        $idInsuranceCurrency = $postSearchAry['idGoodsInsuranceCurrency'] ; 
                        $iInsuranceSelectedFlag = 1;

                        ?>
                        <div class="<?php echo $szInsuranceClass; ?>">
                            <div class="oh insurance-desc-container">
                                <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/insurance'); ?>:</p>
                                <p class="<?php echo $szClassName_2; ?>"><?php echo t($t_base.'fields/include_cargo_value')." ".$fGoogsPriceStr." ".$szInsuranceCostStr; ?></p>
                            </div>
                        </div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="<?php echo $szInsuranceClass; ?>">
                            <div class="oh insurance-desc-container">
                                <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/transportation_insurance'); ?>:</p>
                                <p class="<?php echo $szClassName_2; ?>"> 
                                    <?php echo t($t_base.'fields/transportation_insurance_for_this_shipment');?> 
                                    <span class="pg-more"><a href="javascript:void(0);" onclick="display_insurance_terms(1);"><?php echo t($t_base.'fields/read_more_insurance'); ?></a></span>
                                </p>
                            </div>
                            <div id="booking_confirmation_insurance_container">
                                <?php 
                                    echo display_booking_insurance_form($kBooking,$postSearchAry,1,1);
                                ?> 
                            </div>
                        </div>
                        <?php
                    } 
                }
                else 
                {  
                    $iInsuranceSelectedFlag = 1;
                    ?>
                    <div class="<?php echo $szInsuranceClass; ?>">
                        <div class="oh insurance-desc-container">
                            <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/insurance'); ?>:</p>
                            <p class="<?php echo $szClassName_2; ?>"><?php echo t($t_base.'fields/not_included'); ?></p>
                        </div>
                    </div> 
                <?php } if(!$bQuickQuotePaymentAwaiting){ ?> 
                <div class="oh odd">
                    <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/comments');?>:</p> 
                    <p class="<?php echo $szClassName_2; ?>"> 
                        <?php    
                            if(!empty($postSearchAry['szOtherComments']))
                            {
                                echo nl2br($postSearchAry['szOtherComments']); 
                            }
                            else
                            {
                                echo t($t_base.'fields/none') ;
                            }
                        ?>
                    </p>
                </div> 
                <?php } 
                
                $oddClass="odd";
                $evenClass="even";
                if($postSearchAry['iSearchMiniVersion']!=7 && $postSearchAry['iSearchMiniVersion']!=8 && $postSearchAry['iSearchMiniVersion']!=9)
                {
                    $oddClass="even";
                    $evenClass="odd";
                ?>
                <div class="oh even">
                    <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/price_guarntee_included');?>:</p>
                    <div class="<?php echo $szClassName_2; ?>"> 
                        <p class="price-guarantee-container">
                            <img src="<?php echo __BASE_STORE_IMAGE_URL__."/CONFIRMATION_Award.png"; ?>">
                            <?php echo t($t_base.'fields/find_the_service');?>  
                            <span class="pg-more"><a href="javascript:void(0);" id="price_gurantee_more_span" onclick="toggle_price_gurantee('SHOW')"><?php echo t($t_base.'fields/read_more'); ?></a></span>
                        </p>
                        <div id="price_gurantee_details_text" style="display:none;"> 
                            <p>
                                <?php echo t($t_base.'fields/price_guarantee_details_text_1');?> 
                            </p> 
                            <p class="mrgtop8">
                                <?php echo t($t_base.'fields/price_guarantee_details_text_2');?>
                                <span class="pg-more"><a href="javascript:void(0);" id="price_gurantee_less_span" onclick="toggle_price_gurantee('HIDE')" style="display:none;"><?php echo t($t_base.'fields/read_less'); ?></a></span>
                            </p>
                        </div> 
                    </div>
                </div>	
                <?php }?>
                <form action="" name="booking_confirmation_form" id="booking_confirmation_form" method="post">
                <div class="oh <?=$evenClass?>">
                    <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/payment');?>:</p>
                    <p class="<?php echo $szClassName_2; ?> insurance-checkbox">
                        <?php 
                            if($bQuickQuotePaymentAwaiting && !$bManualQuickQuoteBooking)
                            {
                                if($szQuickQuotePaymentType=='CREDIT_CARD')
                                { 
                                    echo t($t_base.'fields/credit_card'); ?> 
                                    <input type="hidden" name="bookingConfirmAry[iBookingPayment]" id="ZoozPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_1__; ?>" checked="checked"/> 
                                    <?php
                                }
                                else if($szQuickQuotePaymentType=='PAYPAL')
                                {
                                    echo t($t_base.'fields/paypal');?> 
                                    <input type="hidden" name="bookingConfirmAry[iBookingPayment]" id="PaypalPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_2__; ?>">
                                    <?php
                                }
                                else if($szQuickQuotePaymentType=='UNKNOWN')
                                {
                                    if($ShowPaypalOption){?>
                                    <span class="checkbox-container">
                                        <input type="radio" onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" name="bookingConfirmAry[iBookingPayment]" id="ZoozPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_1__; ?>" checked="checked"/><?=t($t_base.'fields/credit_card');?> 
                                    </span>  
                                    <span class="checkbox-container">
                                        <input type="radio"  name="bookingConfirmAry[iBookingPayment]" onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" id="PaypalPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_2__; ?>"><?=t($t_base.'fields/paypal');?>
                                    </span>	
                                    <?php } else {  
                                      echo t($t_base.'fields/credit_card'); ?> 
                                        <input type="hidden" name="bookingConfirmAry[iBookingPayment]" id="ZoozPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_1__; ?>" checked="checked"/>   
                                    <?php
                                    }
                                } 
                            }
                            else
                            {
                                ?>
                                <?php if($postSearchAry['szCurrency']!='HKD' && $iDisplayCCOption){ ?>
                                    <span class="checkbox-container">
                                        <input type="radio" onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" name="bookingConfirmAry[iBookingPayment]" id="ZoozPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_1__; ?>" checked="checked"/><?=t($t_base.'fields/credit_card');?> 
                                    </span> 
                                <?php } ?> 
                                <span class="checkbox-container">
                                    <input type="radio"  onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" name="bookingConfirmAry[iBookingPayment]" id="iBankTransferPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_3__; ?>"><?=t($t_base.'fields/bank_transfer');?>
                                    <span id="bank_transfer_message_span" style="display:none;"><?php echo t($t_base.'title/bank_transfer_confirmation_text'); ?></span>
                                </span>  
                                <?php if($ShowPaypalOption){?>
                                <span class="checkbox-container">
                                    <input type="radio"  name="bookingConfirmAry[iBookingPayment]" onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" id="PaypalPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_2__; ?>"><?=t($t_base.'fields/paypal');?>
                                </span>	
                                <?php }  
                            }
                        ?>
                        <input type="hidden" name="bookingConfirmAry[szPaybuttonName]" value="<?php echo t($t_base.'fields/pay'); ?>" id="szPaybuttonName">
                        <input type="hidden" name="bookingConfirmAry[szConfirmbuttonName]" value="<?php echo t($t_base.'fields/confirm'); ?>" id="szConfirmbuttonName">
                    </p>
                </div>  
                <div class="oh <?=$oddClass?>"> 
                    <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/terms');?>:</p>
                    <div class="<?php echo $szClassName_2; ?>">
                        <div class="terms-text"> 
                            <div class="nano has-scrollbar">
                                <div class="content">
                                    <?php echo $szTermsAndConditionText; ?>
                                </div>
                            </div> 
                        </div>
                        <p class="payment-options-checkbox clearfix">
                            <span class="checkbox-container"><input type="checkbox" onclick="hide_tool_tip('error_message_container_div','1')" name="bookingConfirmAry[iTerms]"  id='iTerms' <?php if((int)$postSearchAry['iAcceptedTerms']==1){ ?>checked<? } ?>  value="1" /><?=t($t_base.'fields/read_terms_condition');?></span>
                            <a href="javascript:void(0);" class="print-icon" onclick="open_terms_condition_popup('<?php echo __NEW_BOOKING_TERMS_PAGE_URL__."/".$_REQUEST['booking_random_num']."/"?>');">print</a>
                        </p> 
                        <input type="hidden" name="bookingConfirmAry[iRemindToRate]" value="1"  />
                        <input type="hidden" name="bookingConfirmAry[iInsuranceSelectedFlag]" id="iInsuranceSelectedFlag" value="<?php echo $iInsuranceSelectedFlag; ?>"  />
                        <input type="hidden" name="bookingConfirmAry[szBookingRandomNum]" id="szBookingRandomNum_booking_conf" value="">
                        <input type="hidden" name="bookingConfirmAry[iBookingStatus]" id="iBookingStatus" value="3"> 
                        <input type="hidden" name="bookingConfirmAry[iQuotesStatus]" id="iQuotesStatus" value="<?php echo $iQuotesStatus; ?>">  
                    </div>
                </div>	
                </form>
                <div class="error_message_container" id="error_message_container_div" style="visibility:hidden;">
                    <ul>
                        <li>Please accept our terms to complete the booking</li>
                    </ul>
                </div> 
                <br> 
                <div class="booking_details_buttons clearfix">
                    <?php if(!$bQuickQuotePaymentAwaiting || $bManualQuickQuoteBooking){?>
                        <p class="back-button"><a href="<?php echo $redirect_url; ?>"><span><< <?=t($t_base.'fields/back');?></span></a></p>
                    <?php }?>
                    <p class="continue-button">						
                        <a  href="javascript:void(0)" onclick="booking_on_hold('CONFIRMED');" class="button1" id="pay_button"><span><?=t($t_base.'fields/pay');?></span></a>
                        <?php if(!$bQuickQuotePaymentAwaiting){ ?>
                            <span class="pay-later-button">
                                <a href="javascript:void(0)" onclick="booking_on_hold('ON_HOLD');"><span><?=t($t_base.'fields/or_save_for_later');?></span></a>
                            </span>
                        <?php } ?>
                    </p> 
                    <?php  
                        $item_ref_text = createStripeDescription($postSearchAry);  
                        if($postSearchAry['iInsuranceIncluded']==1)
                        {
                            $fTotalCostForBooking =  $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] ; 
                        }
                        else
                        {
                            $fTotalCostForBooking = $postSearchAry['fTotalPriceCustomerCurrency'];
                        }  
                        $fTotalCostForBooking = ($fTotalCostForBooking + $postSearchAry['fTotalVat']); 
                        $fTotalCostForBookingCent = $fTotalCostForBooking * 100; 
                    ?>
                    <input type="hidden" name="stripe_refersh" value="1" id="stripe_refersh"> 
                </div> 
            </div>		
            </div>

            <!-- 
            <div id="booking_confirmation_insurance_container">
                    <?php
                            //echo display_booking_insurance_form($kBooking,$postSearchAry);
                    ?>
            </div>   
            <div class="booking_details_buttons clearfix fixed-body-container">
                    <p class="back-button"><a href="<?php echo __NEW_SERVICES_PAGE_URL__.'/'.$_REQUEST['booking_random_num'].'/'?>"><span><< <?=t($t_base.'fields/back');?></span></a></p>
                    <p class="continue-button">						
                            <a  href="javascript:void(0)" onclick="booking_on_hold('CONFIRMED');" class="button1" id="pay_button"><span><?=t($t_base.'fields/pay');?></span></a>
                            <span class="pay-later-button">
                                    <a href="javascript:void(0)" onclick="booking_on_hold('ON_HOLD');"><span><?=t($t_base.'fields/or_save_for_later');?></span></a>
                            </span>
                    </p> 
            </div>	
             <br>
            -->
    </div> 

    <div id="insurance_terms_container" style="display:none;"> 
    </div>
<?php
$iDonotShowLanguageOption = 1;
echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>		

<div id="pay_stripe_button" style="display:none;">
    <form action="<?=__MAIN_SITE_HOME_PAGE_URL__?>/stripe/callback.php" method="POST" id="payForm"><script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="<?php echo __STRIPE_API_KEY__; ?>" data-amount="<?=$fTotalCostForBooking_small?>" data-name="Transporteca" data-description="<?=$item_ref_text?>" data-image="http://staging.transporteca.com/images/favicon.png" data-currency="<?=$postSearchAry['szCurrency']?>" data-email="<?=$kUser->szEmail?>" data-locale="auto"></script></form>
</div>