<?php
ob_start();
session_start();
$szMetaTitle= __PRIVACY_META_TITLE__;
$szMetaKeywords = __PRIVACY_META_KEYWORDS__;
$szMetaDescription = __PRIVACY_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$t_base = "Users/Privacy/"; 

$KwarehouseSearch=new cWHSSearch();
$iLanguage = getLanguageId();
//if($iLanguage ==__LANGUAGE_ID_DANISH__)
//{
//    $privacyPolicy = html_entity_decode($KwarehouseSearch->getManageMentVariableByDescription('__PRIVACY_POLICY_DANISH__'));
//}
//else
//{
    $privacyPolicy = html_entity_decode($KwarehouseSearch->getManageMentVariableByDescription('__PRIVACY_POLICY__',$iLanguage));
//}
?>
<div id="my-account-body">
    <?php require_once( __APP_PATH_LAYOUT__ . "/nav.php" ); ?> 
    <div class="hsbody-2-right">
        <div id="delete_user_account" class="f-size-22">
            <h2 class="create-account-heading"><?=t($t_base.'title/privacy_notice');?></h2>
            <?=refineDescriptionData($privacyPolicy);?>
        </div>
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>