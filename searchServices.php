<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle = __SEARCH_PAGE_META_TITLE__;
$szMetaKeywords = __SEARCH_PAGE_META_KEYWORDS__;
$szMetaDescription = __SEARCH_PAGE_META_DESCRIPTION__;

require_once(__APP_PATH_LAYOUT__."/header.php");

$t_base = "home/homepage/";
//$cargo_value = 3.175;
//echo get_formated_cargo_measure($cargo_value);
$kConfig=new cConfig();
$number = 1;
$kBooking = new cBooking();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

if($_REQUEST['use_session']!=1)
{
	if($idBooking>0)
	{
		$redirect_url = __SELECT_SERVICES_URL__ ;
		?>
		<div id="change_price_div">
		<?
		display_booking_notification($redirect_url);
		echo "</div>";
		/*
		if($kBooking->deleteTempSearchedDataByBookingId($_SESSION['booking_id']))
		{
			$_SESSION['booking_id'] = '';
			unset($_SESSION['booking_id']);	
			$_SESSION['booking_register_shipper_id'] = '';
			$_SESSION['booking_register_consignee_id'] = '';
			unset($_SESSION['booking_register_shipper_id']);
			unset($_SESSION['booking_register_consignee_id']);
		}
		*/
	}
}

unset($_SESSION['valid_confirmation']);
$_SESSION['__BOOKING_TIMESTAMP__'] = '';
unset($_SESSION['__BOOKING_TIMESTAMP__']);

/**
* $_REQUEST['err']=1 means no record found on service page
* $_REQUEST['err']=2 means force user to login for confirming multiuser access ;
* $_REQUEST['err']=1 means no record found for this user either lcl pricing is deleted or user email is in selected forwarders non-acceptance list
**/
if($_REQUEST['err']==1)
{
	$kBooking->addExpactedServiceData($idBooking);
	//$t_base = "SelectService/";
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$idExpactedService = $kBooking->idExpactedService ;
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	?>
	<script type="text/javascript">
		addPopupScrollClass('service_not_found');
	</script>
	<div id="service_not_found" style="display:block;">	
	<?
	//echo display_service_not_found_html($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking);
	echo display_service_not_found_seach_service_popup($postSearchAry,$idExpactedService,$kBooking);
	
	echo "</div>";
	?>
	<script type="text/javascript">
		addPopupScrollClass('all_available_service');
	</script>
	<?php
}
else if(($_REQUEST['err']==2) && !empty($_SESSION['multi_user_key']))
{
	$redirect_url =__BASE_URL__."/inviteConfirmation.php?confirmationKey=".trim($_SESSION['multi_user_key']);
	//echo $_SESSION['multi_user_key'] ;
	//die;
	?>
	<script type="text/javascript">
		ajaxLogin('<?=$redirect_url?>')
	</script>
	<?php
}
else if(($_REQUEST['err']==3))
{
	$t_base_mybooking = "Booking/MyBooking/";
	?>
	<div id="change_price_div">
	<?php
	echo display_rate_expire_popup($t_base_mybooking,$idBooking);
	echo '</div>';
}
else if(($_REQUEST['err']==4))
{
	$t_base_mybooking = "Booking/MyBooking/";
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
	
	?>
	<script type="text/javascript">
		display_price_changed_popup_cargo_changed('<?=$_SESSION['booking_page_url']?>','<?=$szBookingRandomNum?>');
	</script>	
	<?php
	/**
	* we are updating idForwarder,idWarehouseFrom and idWarehouseTo to prevent Multiple tab problem.
	*/
	$res_ary=array();
	$res_ary['iDoNotKnowCargo']= '0';
			
	if(!empty($res_ary))
	{
		foreach($res_ary as $key=>$value)
		{
			$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
		}
	}
	$update_query = rtrim($update_query,",");
	if($kBooking->updateDraftBooking($update_query,$idBooking))
	{
		//echo "SUCCESS";
	}
}

// initialising arrays
$countryAry=array();
$serviceTypeAry=array();
$weightMeasureAry=array();
$cargoMeasureAry = array();
$timingTypeAry=array();
$iLanguage = getLanguageId();
$countryAry=$kConfig->getAllCountries(false,$iLanguage);

// geting all service type 
$serviceTypeAry=$kConfig->getConfigurationData('__TABLE_SERVICE_TYPE__');

// geting all Timing type 
$timingTypeAry=$kConfig->getConfigurationData('__TABLE_TIMING_TYPE__');

// geting all weight measure 
$weightMeasureAry=getConfigurationData('__TABLE_WEIGHT_MEASURE__');

// geting all cargo measure 
$cargoMeasureAry=getConfigurationData('__TABLE_CARGO_MEASURE__');

// geting all custom clearance 
$customClearAry=getConfigurationData('__TABLE_ORIGIN_DESTINATION__');
$postSearchAry = array();
$cargoAry = array();
//print_r($_REQUEST);

if((int)$_REQUEST['idBooking']>0)
{
	//This block of code is called in repeat booking 
	
	$idBooking = (int)$_REQUEST['idBooking'] ;
	//echo $idBooking ;
	
	if($_SESSION['user_id']>0)
	{
		if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
		{
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
			$booking_mode = 2;
		}
		else
		{
			header("Location:".__LANDING_PAGE_URL__);
			die;
		}
	}
	else
	{
		header("Location:".__LANDING_PAGE_URL__);
		die;
	}
}
else if($idBooking>0)
{
	$kBooking = new cBooking();
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
	
	if(($postSearchAry['iPaymentProcess']==1) && ($booking_mode==2))
	{
		header("Location:".__LANDING_PAGE_URL__);
		exit;
	}
	else if(($_SESSION['user_id']>0) && (!$kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id'])))
	{
		echo display_booking_invalid_user_popup($t_base_details);
		die;
	}
	else if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
	{
		$t_base = "home/homepage/";
		echo display_booking_already_paid($t_base,$idBooking);
		die;
	}
	else if($_REQUEST['err'] !=4) // That means user donot come from cargo changed price popup
	{
		/**
		* we are updating idForwarder,idWarehouseFrom and idWarehouseTo to prevent Multiple tab problem.
		**/
		
		$res_ary=array();
		$res_ary['idForwarder']= '';
		$res_ary['idWarehouseFrom']= '';
		$res_ary['idWarehouseTo']= '';
		
		if(!empty($res_ary))
		{
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}
		$update_query = rtrim($update_query,",");
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			//echo "SUCCESS";
		}
	}	
}
else
{
	$_SESSION['booking_register_shipper_id']='';
	$_SESSION['booking_register_shipper_id'] = '';
	unset($_SESSION['booking_register_shipper_id']);
	unset($_SESSION['booking_register_shipper_id']);
}

if(((int)$_SESSION['user_id']>0) && ($_REQUEST['err']<=0))
{
	checkUserPrefferedCurrencyActive();
}

if(count($cargoAry)>1)
{
	$cargo_counter = count($cargoAry);
}
else
{
	$cargo_counter = 1 ;
}
if(!empty($_POST['landingPageAry']))
{
	$postSearchAry['idOriginCountry'] = $_POST['landingPageAry']['szOriginCountry'];
	$postSearchAry['idDestinationCountry'] = $_POST['landingPageAry']['szDestinationCountry'];
}

$idTimingType = $postSearchAry['idTimingType'];
$date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;

if(!empty($serviceTypeAry))
{
	$preload_image_str = '';
	foreach($serviceTypeAry as $serviceTypeArys)
	{
		$idServiceType = $serviceTypeArys['id'] ;
		$alt_text = $serviceTypeArys['szDescription'];
		
		if(!empty($preload_image_str))
		{
			$preload_image_str .=", '".display_service_type_image_search_service($postSearchAry,$idServiceType,$alt_text,false,true)."'";
		}
		else
		{
			$preload_image_str .="'".display_service_type_image_search_service($postSearchAry,$idServiceType,$alt_text,false,true)."'";
		}
	}
}

$requirement_page_url = __REQUIREMENT_PAGE_URL__.'/' ;
if(!empty($szBookingRandomNum))
{
	$requirement_page_url .= $szBookingRandomNum.'/';
}

$iLoadDanishImage = 0;
if($iLanguage==__LANGUAGE_ID_DANISH__)
{
	$iLoadDanishImage = 1;
}


//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{
	$szDangor_cargo_yes = t($t_base.'fields/yes');
	$szDangor_cargo_no = t($t_base.'fields/no');
//}
//else
//{
//	$szDangor_cargo_yes = 'Yes';
//	$szDangor_cargo_no = 'No';
//}
?>
<script type="text/javascript">
$().ready(function() {	
	$("#szOriginPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szDestinationPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
	
	$("#szOriginCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szDestinationCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
	<?
		if($idTimingType==2)
		{
			?>
			$("#datepicker1_search_services").datepicker();
			<?
		}
		else
		{
	?>
    	$("#datepicker1_search_services").datepicker({<?=$date_picker_argument?>});
    <?
		}
    ?>
    
    <?
    	if((int)$_POST['landingPageAry']['szOriginCountry']>0)
		{
			?>
			activatePostCodeField('<?=$_POST['landingPageAry']['szOriginCountry']?>','szOriginCity','szOriginPostCode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')	
			<?
		}
    ?>
    <?
    	if((int)$_POST['landingPageAry']['szDestinationCountry']>0)
		{
			?>
			activatePostCodeField('<?=$_POST['landingPageAry']['szDestinationCountry']?>','szDestinationCity','szDestinationPostCode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')	
			<?
		}
    ?>
    <?
    	if((int)$postSearchAry['iDoNotKnowCargo']==1)
		{
			?>
			$("#iDoNotKnow").attr('checked','checked');			
			autofill_cargo_dimentions('',false,'<?php echo $weightMeasureAry[0]['szDescription']?>','<?php echo $cargoMeasureAry[0]['szDescription']?>','<?php echo $szDangor_cargo_no; ?>');
			<?
		}
	if(!empty($preload_image_str))
	{
		?>
		preload([
		    <?=trim($preload_image_str)?>
		]);
		<?
	}
?>
});
</script>
<div id="change_price_div" style="display:none"></div>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="dang-cargo-pop" class="help-pop right">
</div>
<div id="Transportation_pop" style="display:none;"></div>

<div id="hsbody" class="mainpage">
<div id="regError" class="errorBox" style="display:none;width:850px;">
</div>
<?

	/*if(!empty($_SESSION['multi_user_success_message']))
	{
		?>
		<div id="obo_cc_no_record_found">
			<ul id="message_ul" style="padding:5px 5px 5px 35px;">
				<li><?=$_SESSION['multi_user_success_message']?></li>
			</ul>
		</div>
		<?
		$_SESSION['multi_user_success_message'] = '';
		$_SESSION['multi_user_key'] = '';
		unset($_SESSION['multi_user_key']);
		unset($_SESSION['multi_user_success_message']);
	}*/
?>
<form action="<?=__SELECT_SERVICES_URL__?>" method="post" id="landing_page_form">
	<h5><?=t($t_base.'fields/pagetitle');?> </h5>
	<hr>
	<div class="oh" style="padding:0;">
		<p class="fl-33">1. <?=t($t_base.'fields/your_requirements');?><br />
			<span style="margin-left:15px;"><a href="<?=$requirement_page_url?>"><?=t($t_base.'fields/take_me_step_by_step');?></a></span>
		</p>
		<div class="fl-67 pos-rel">
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/transportation');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/TRANSPORTATION_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/TRANSPORTATION_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<div class="fl-67 pos-rel">
					<div id="service_type_image_div">
						<?
							$idServiceType = $postSearchAry['idServiceType'];
							if((int)$idServiceType>0)
							{							
								$auxServiceTypeAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$idServiceType);
								$alt_text = $auxServiceTypeAry[0]['szDescription'];
							}
							else
							{
								$idServiceType = $serviceTypeAry[0]['id'];
								$alt_text = $serviceTypeAry[0]['szDescription'];
							}
							echo display_service_type_image_search_service($postSearchAry,$idServiceType,$alt_text);
						?>
					</div>	
					<select style="width:424px;margin: 0 0 8px;" size="1" name="searchAry[idServiceType]" id="szTransportation" onchange="change_service_type_image(this.value,'<?php echo $iLoadDanishImage; ?>');change_postcode_city_default_text(this.value,'<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>');" class="transportation-select">
						 <?
						 	if(!empty($serviceTypeAry))
						 	{
						 		foreach($serviceTypeAry as $serviceTypeArys)
						 		{
						 			?>
						 				<option value="<?=$serviceTypeArys['id']?>" <? if($postSearchAry['idServiceType']==$serviceTypeArys['id']){?> selected <? }?>><?=$serviceTypeArys['szDescription']?></option>
						 			<?
						 		}
						 	}
						 ?>
					</select>				
				</div>
			</div>
				<div id="szTransportation_div" style="display:none;"> </div>
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/customs_clearance');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CUSTOM_CLEARANCE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CUSTOM_CLEARANCE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<?php
					if(!empty($customClearAry))
					{
						$ctr =0;
						//foreach($customClearAry as $customClearArys)
						//{
							?>
							<p class="fl-20 pos-rel checkbox-ab">
								<input type="checkbox" <? if($postSearchAry['iOriginCC'] == $customClearAry[0]['id']) {?>checked<? }?> id="iOriginCC" name="searchAry[idCC][0]" value="<?=$customClearAry[0]['id']?>"  /><?=$customClearAry[0]['szDescription']?>
							</p>
							<p class="fr-20 pos-rel checkbox-ab" style="width:105px;">
								<input type="checkbox" <? if($postSearchAry['iDestinationCC'] == $customClearAry[1]['id']) {?>checked<? }?> id="iDestinationCC" name="searchAry[idCC][1]" value="<?=$customClearAry[1]['id']?>"  /><?=$customClearAry[1]['szDescription']?>
							</p>
							<?
							//$ctr++;							
						//}
					}
				?>				
					<div id="iCustomClearance_div" style="display:none;"></div>
			</div>
		</div>
	</div>
	<hr>
	<div class="oh" style="padding:0;">
		<p class="fl-33"><br />2. <?=t($t_base.'fields/when_where');?></p>
		<div class="fl-67">
			<div class="oh">				
				<p class="fl-33"><br /><?=t($t_base.'fields/from');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/COUNTRY_FROM_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/COUNTRY_FROM_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-25 f-size-12">	
					<?=t($t_base.'fields/country');?><br />
					<select name="searchAry[szOriginCountry]" id="szOriginCountry" size="1" onchange="activatePostCodeField(this.value,'szOriginCity','szOriginPostCode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')">
						<option value=""><?=t($t_base.'fields/select_country');?></option>
					 <?php
					 	if(!empty($countryAry))
					 	{
					 		foreach($countryAry as $countryArys)
					 		{
					 			?>
					 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idOriginCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,25)?></option>
					 			<?php
					 		}
					 	}
					 ?>
					</select>
					<br />
					<span id="szOriginCountry_div" style="display:none;"></span>
				</p>
				<p class="fl-20 f-size-12">
				<?=t($t_base.'fields/city');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CITY_POST_CODE_1_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CITY_POST_CODE_1_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br />
				<input name="searchAry[szOriginCity]" id="szOriginCity" value="<?=$postSearchAry['szOriginCity']?>" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')"  size="18" <? if(empty($postSearchAry['idOriginCountry'])){?>disabled="disabled"<? }?>/>
				
				
				<p class="postcode f-size-12">
				<?=t($t_base.'fields/postcode');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/CITY_POST_CODE_2_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CITY_POST_CODE_2_TOOL_TIP_TEXT');?>','dang-cargo-pop',event);" onmouseout="hide_tool_tip('dang-cargo-pop')">&nbsp;</a><br />
				<input <? if(empty($postSearchAry['idOriginCountry'])){?>disabled="disabled"<? }?> name="searchAry[szOriginPostCode]" id="szOriginPostCode" value="<?=$postSearchAry['szOriginPostCode']?>" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  size="18" /></p>
			</div>
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/COUNTRY_TO_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/COUNTRY_TO_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-25">				
					<select name="searchAry[szDestinationCountry]" id="szDestinationCountry" size="1" onchange="activatePostCodeField(this.value,'szDestinationCity','szDestinationPostCode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" >
						 <option value=""><?=t($t_base.'fields/select_country');?></option>
						 <?
						 	if(!empty($countryAry))
						 	{
						 		foreach($countryAry as $countryArys)
						 		{
						 			?>
						 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idDestinationCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,25)?></option>
						 			<?
						 		}
						 	}
						 ?>
						</select>
				</p>
				
				<p class="fl-20">				
				<input <? if(empty($postSearchAry['idDestinationCountry'])){?> disabled="disabled" <? }?> value="<?=$postSearchAry['szDestinationCity']?>" name="searchAry[szDestinationCity]" id="szDestinationCity" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')"  size="18" /></p>
				
				<p class="postcode">				
				<input <? if(empty($postSearchAry['idDestinationCountry'])){?> disabled="disabled" <? }?> value="<?=$postSearchAry['szDestinationPostCode']?>" name="searchAry[szDestinationPostCode]" id="szDestinationPostCode" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  size="18" /></p>
			</div>
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/timing');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/TIMING_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/TIMING_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-33">
					<select name="searchAry[idTimingType]" id="szTiming" onchange="change_date('<?=date('d/m/Y')?>','<?=t($t_base.'fields/date_format');?>')">
						 <?
						 	if(!empty($timingTypeAry))
						 	{
						 		foreach($timingTypeAry as $timingTypeArys)
						 		{
						 			?>
						 				<option value="<?=$timingTypeArys['id']?>" <? if($postSearchAry['idTimingType']==$timingTypeArys['id']){?> selected <? }?>><?=$timingTypeArys['szDescription']?></option>
						 			<?
						 		}
						 	}
						?>
					</select>
				</p>
				<p class="fl-20"><input id="datepicker1_search_services" name="searchAry[dtTiming]" onfocus="blank_me(this.id,'<?=t($t_base.'fields/date_format');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/date_format');?>')" type="text" value="<? if($postSearchAry['dtTimingDate']!='0000-00-00 00:00:00' && !empty($postSearchAry['dtTimingDate'])) {?><?=date('d/m/Y',strtotime($postSearchAry['dtTimingDate']))?><? }else {?><?=date('d/m/Y')?><? }?>" />
				</p>
			</div>
		</div>
	</div>
	<hr>
	<?
		if(!empty($cargoAry))
		{
			$fLength = number_format($cargoAry[$number]['fLength'],0,'.','');
			$fWidth = number_format($cargoAry[$number]['fWidth'],0,'.','');
			$fHeight = number_format($cargoAry[$number]['fHeight'],0,'.','');
			$fWeight = number_format($cargoAry[$number]['fWeight'],0,'.','');
			$idCargo = $cargoAry[$number]['id'];
		}
	  	$function_params=(int)$_SESSION['user_id'];
	?>
	<div class="oh" style="padding:0;">
		<p class="fl-33"><br />3. <?=t($t_base.'fields/cargo_details');?><br />		
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="searchAry[iDoNotKnowCargo]" onclick="autofill_cargo_dimentions('',false,'<?php echo $weightMeasureAry[0]['szDescription']?>','<?php echo $cargoMeasureAry[0]['szDescription']?>','<?php echo $szDangor_cargo_no; ?>');" value="1" id="iDoNotKnow"> <?=t($t_base.'fields/dont_know');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DONOT_KNOW_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DONOT_KNOW_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>			
		</p>
		<div class="fl-67">
			  <div class="oh">
				<p class="fl-33"><br /><?=t($t_base.'fields/dimensions');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<div class="fl-67 dimensions-field">				
						<span class="f-size-12">
							<?=t($t_base.'fields/length');?><br />
							<input type="hidden" name="searchAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
							<input type="text" pattern="[0-9]{*}" name="searchAry[iLength][<?=$number?>]" value="<?=$fLength?>" id= "iLength<?=$number?>" /><br />
							<span id="iLength<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-14 cross_seperator">&nbsp;<br />X</span>
						<span class="f-size-12">
							<?=t($t_base.'fields/width');?><br/>
							<input type="text" pattern="[0-9]*" name="searchAry[iWidth][<?=$number?>]" value="<?=$fWidth?>" id= "iWidth<?=$number?>" /><br />
							<span id="iWidth<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-14  cross_seperator">&nbsp;<br/>X</span>
						<span class="f-size-12">
							<?=t($t_base.'fields/height');?><br/>
							<input type="text" pattern="[0-9]*" name="searchAry[iHeight][<?=$number?>]" value="<?=$fHeight?>" id= "iHeight<?=$number?>" />
							<span id="iHeight<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-12">
						&nbsp;<br/>
							<select name="searchAry[idCargoMeasure][<?=$number?>]" id="idCargoMeasure<?=$number?>">
								 <?
								 	if(!empty($cargoMeasureAry))
								 	{
								 		foreach($cargoMeasureAry as $cargoMeasureArys)
								 		{
								 			?>
								 				<option value="<?=$cargoMeasureArys['id']?>" <? if($cargoAry[$number]['idCargoMeasure']==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
							<input type="hidden" name="idCargoMeasure_hidden" id="idCargoMeasure_hidden" value="1">
							<input type="hidden" name="idWeightMeasure_hidden" id="idWeightMeasure_hidden" value="1">
							<input type="hidden" name="iDangerCargo_hidden" id="iDangerCargo_hidden" value="No">
						</span>
						<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
						<span class="last f-size-12">
							<?=t($t_base.'fields/quantity');?><br/>
							<input type="text" pattern="[0-9]*" name="searchAry[iQuantity][<?=$number?>]" value="<?=$cargoAry[$number]['iQuantity']?>" id= "iQuantity<?=$number?>" /><br />
							<span id="iQuantity<?=$number?>_div" style="display:none;"></span>
						</span>
					</div>
			</div>			
			<div class="oh">
				<p class="fl-33" style="padding:2px 0 0;"><?=t($t_base.'fields/weight');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WEIGHT_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WEIGHT_TOOL_TIP_TEXT');?>','customs_clearance_pop',event)" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>&nbsp;</p>
				<div class="fl-67 dimensions-field">					
						<span>
							<input type="text" pattern="[0-9]*" name="searchAry[iWeight][<?=$number?>]" onkeyup="on_enter_key_press(event,'validateLandingPageForm','<?=$function_params?>','<?=__SELECT_SERVICES_URL__?>');" value="<?=$fWeight?>" id= "iWeight<?=$number?>" />
							<span id="iWeight<?=$number?>_div" style="display:none;"></span>
						</span>
						<span style="width:26%">
							<select size="1" name="searchAry[idWeightMeasure][<?=$number?>]" id="idWeightMeasure<?=$number?>" style="margin-left: 14px;min-width: 66px;">
								 <?
								 	if(!empty($weightMeasureAry))
								 	{
								 		foreach($weightMeasureAry as $weightMeasureArys)
								 		{
								 			?>
								 				<option value="<?=$weightMeasureArys['id']?>" <? if($cargoAry[$number]['idWeightMeasure']==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</span>
						<span style="padding:2px 0 0;" class="dangerous_cargo"><?=t($t_base.'fields/dangerous_cargo');?><a href="javascript:void(0);" id="dang-cargo-link" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_TEXT');?>','dang-cargo-pop',event);" onmouseout="hide_tool_tip('dang-cargo-pop')">&nbsp;</a></span>
						<span class="last">
							<select size="1" name="searchAry[iDangerCargo][<?=$number?>]" id="iDangerCargo<?=$number?>" style="min-width:65px">
								<option value="No" selected="selected"><?=$szDangor_cargo_no?></option>
								<option value="Yes"><?=$szDangor_cargo_yes?></option>
							</select>
						</span>
					</div>			
			</div>	
			<?
			//echo $cargo_counter ;
			if($cargo_counter>1)
			{
				echo "<div id='cargo'>";
					$ctr=0;
					for($number=2;$number<=$cargo_counter;$number++)
					{
						if(!empty($cargoAry))
						{
							$fLength = ceil($cargoAry[$number]['fLength']);
							$fWidth = ceil($cargoAry[$number]['fWidth']);
							$fHeight = ceil($cargoAry[$number]['fHeight']);
							$fWeight = ceil($cargoAry[$number]['fWeight']);
							$idCargo = $cargoAry[$number]['id'];
						}	
				?>
				<div id="cargo<?=$number-1?>">
				<hr>
		        <div class="oh" style="padding:0;">
					<p class="fl-33"><br /><?=t($t_base.'fields/dimensions');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
					<div class="fl-67 dimensions-field">	
					
							<span class="f-size-12">
								<?=t($t_base.'fields/length');?><br/>
								<input type="hidden" name="searchAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
								<input type="text" pattern="[0-9]*" name="searchAry[iLength][<?=$number?>]" value="<?=$fLength?>" id= "iLength<?=$number?>" /><br />
								<span id="iLength<?=$number?>_div" style="display:none;"></span>
							</span>
							<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
							<span class="f-size-12">
								<?=t($t_base.'fields/width');?><br/>
								<input type="text" pattern="[0-9]*"  name="searchAry[iWidth][<?=$number?>]" value="<?=$fWidth?>" id= "iWidth<?=$number?>" /><br />
								<span id="iWidth<?=$number?>_div" style="display:none;"></span>
							</span>
							<span class="f-size-14  cross_seperator">&nbsp;<br/>X</span>
							<span class="f-size-12">
								<?=t($t_base.'fields/height');?><br/>
								<input type="text" pattern="[0-9]*" name="searchAry[iHeight][<?=$number?>]" value="<?=$fHeight?>" id= "iHeight<?=$number?>" />
								<span id="iHeight<?=$number?>_div" style="display:none;"></span>
							</span>
							<span class="f-size-12">
							&nbsp;<br/>
								<select name="searchAry[idCargoMeasure][<?=$number?>]">
									 <?
									 	if(!empty($cargoMeasureAry))
									 	{
									 		foreach($cargoMeasureAry as $cargoMeasureArys)
									 		{
									 			?>
									 				<option value="<?=$cargoMeasureArys['id']?>" <? if($cargoAry[$number]['idCargoMeasure']==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
									 			<?
									 		}
									 	}
									 ?>
								</select>
							</span>
							<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
							<span class="last f-size-12">
								<?=t($t_base.'fields/quantity');?><br/>
								<input type="text" pattern="[0-9]*" name="searchAry[iQuantity][<?=$number?>]" value="<?=$cargoAry[$number]['iQuantity']?>" id= "iQuantity<?=$number?>" /><br />
								<span id="iQuantity<?=$number?>_div" style="display:none;"></span>
							</span>
						</div>
					
				</div>			
				<div class="oh">
					<p class="fl-33" style="padding:2px 0 0;"><?=t($t_base.'fields/weight');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WEIGHT_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WEIGHT_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
					<div class="fl-67 dimensions-field">					
							<span>
								<input type="text" pattern="[0-9]*" name="searchAry[iWeight][<?=$number?>]" onkeyup="on_enter_key_press(event,'validateLandingPageForm','<?=$function_params?>','<?=__SELECT_SERVICES_URL__?>');" value="<?=$fWeight?>" id= "iWeight<?=$number?>" />
								<span id="iWeight<?=$number?>_div" style="display:none;"></span>
							</span>
							<span style="width:26%">
								<select size="1" name="searchAry[idWeightMeasure][<?=$number?>]" style="margin-left: 14px;min-width: 66px;">
									 <?
									 	if(!empty($weightMeasureAry))
									 	{
									 		foreach($weightMeasureAry as $weightMeasureArys)
									 		{
									 			?>
									 				<option value="<?=$weightMeasureArys['id']?>" <? if($cargoAry[$number]['idWeightMeasure']==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
									 			<?
									 		}
									 	}
									 ?>
								</select>
							</span>
							<span style="padding:2px 0 0;" class="dangerous_cargo"><?=t($t_base.'fields/dangerous_cargo');?><a href="javascript:void(0);" id="dang-cargo-link" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_TEXT');?>','dang-cargo-pop',event);" onmouseout="hide_tool_tip('dang-cargo-pop')">&nbsp;</a></span>
							<span class="last">
								<select size="1" name="searchAry[iDangerCargo][<?=$number?>]" style="min-width:65px">
									<option value="No" selected=""><?=$szDangor_cargo_no?></option>
									<option value="Yes"><?=$szDangor_cargo_yes?></option>
								</select>
							</span>
						</div>				
				</div>	
				</div>		
				<?
				$ctr++;				
			}
			echo "<div id='cargo".$cargo_counter."'></div></div>";
			$remove_cargo_link_style = " style='display:block;'";
		}
		else
		{
			$remove_cargo_link_style = " style='display:none;'";
			?>
			<div id="cargo">
				<div id="cargo<?=$cargo_counter?>"></div>
			</div>
			<?
		}		
		//following sesssion will used only for multitabing issue 
		if(!empty($_REQUEST['booking_random_num']) && $booking_mode!=2)
		{
			$szBookingRandomNum = $_REQUEST['booking_random_num'] ;
		}
		else
		{
			$szBookingRandomNum = md5(time());
			$szBookingRandomNum = $kBooking->isBookingRandomNumExist($szBookingRandomNum);
		}
		$_SESSION['came_from_landing_page'.'_'.$szBookingRandomNum] = 1;
		//echo $szBookingRandomNum ;
		?>	
			<input type="hidden" name="searchAry[szBookingMode]" id="szBookingMode" value="<?=$booking_mode?>" />
			<input type="hidden" name="searchAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$szBookingRandomNum?>" />
			<input type="hidden" name="searchAry[hiddenPosition]" id="hiddenPosition" value="<?=$cargo_counter?>" />
			<input type="hidden" name="searchAry[hiddenPosition1]" id="hiddenPosition1" value="<?=$cargo_counter?>" />			
			<div class="oh">
			<p class="fl-49"><a href="javascript:void(0);" onclick="add_more_cargo()"><?=t($t_base.'fields/add_more');?> </a></p>
			<div id="remove" align="right" <?=$remove_cargo_link_style?> class="fr-35"><a href="javascript:void(0);" onclick="remove_cargo();"><?=t($t_base.'fields/remove_cargo');?></a></div>
			</div>
		</div>
	</div>
	<hr>
	<div class="oh" style="padding:0;">
		<p class="fl-85" align="right" style="padding-top: 5px;"><?=t($t_base.'fields/available_schedules_prices');?></p>
		<p class="fl-15" align="right">
		<a href="javascipt:void(0);" onclick="return validateLandingPageForm('landing_page_form','<?=$_SESSION['user_id']?>','<?=__SELECT_SERVICES_URL__?>');" class="button1"><span><?=t($t_base.'fields/show_option');?> </span></a>
		</p>
	</div>	
	</form>		
</div>
<div id="all_available_service" style="display:none;"></div>
<?
	echo html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);
	require_once(__APP_PATH_LAYOUT__."/footer.php");
?>		
		