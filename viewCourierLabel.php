<?php
/**
 * View Courier Label
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
//include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

require_once( __APP_PATH__ . "/pdfLib/PDFMerger.php" );
require_once(__APP_PATH__.'/pdfLib/fpdi/pdf_parser.php');

//require_once(__APP_PATH__.'/invoice/fpdf.php');
$kBooking = new cBooking();
$kCourierServices = new cCourierServices();

if(!empty($_REQUEST['szBookingKey']))
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingKey'])); 
    $idBooking = $kBooking->getBookingIdByBookngRef($szBookingRandomNum); 
}
else
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingNumber'])); 
    $idBooking = $kBooking->getBookingIdByBookingRef($szBookingRandomNum);
}


if((int)$idBooking==0)
{
    header('Location: '.__BASE_URL__);
    die(); 
}
$pdfFileArr=$kCourierServices->getCourierLabelUploadedPdf($idBooking,true);

$postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);

$szBookingRef = $postSearchAry['szBookingRef'];
$szDeliveryInstructions = $postSearchAry['szDeliveryInstructions'];
$szCustomerName = $postSearchAry['szFirstName']." ".$postSearchAry['szLastName'];

if(count($pdfFileArr)>0)
{
    $strPdf='';
    $encryptflag=false;
    //$courierLabelPath="courierLabel.pdf";
    $courierLabelPath = $szBookingRef."-".$szCustomerName."-".$szDeliveryInstructions.".pdf";
    foreach($pdfFileArr as $pdfFileArrs)
    {
        $filePath=__UPLOAD_COURIER_LABEL_PDF__."/".$pdfFileArrs;
        $pdf_parser = new pdf_parser($filePath);
        if($pdf_parser->xref['trailer'][1]['/Encrypt'])
        {
           $encryptflag=true;
        }
    }
    if($encryptflag)
    {
        if(count($pdfFileArr)==1)
        {
           ob_clean();
           header('Content-Type: application/octet-stream');
           header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
           header('Expires: 0');
           header('Cache-Control: must-revalidate');
           header('Pragma: public');
           header('Content-Length: ' . filesize($filePath));
           readfile($filePath);
        }
        else
        {    
            $zip = new ZipArchive();
            
            $zip_file_name = "courierLabel".time().".zip";
            $zip_file_path= __UPLOAD_COURIER_LABEL_PDF__.'/'.$zip_file_name;
             
            $zip->open($zip_file_path, ZIPARCHIVE::CREATE);
            foreach($pdfFileArr as $pdfFileArrs)
            {
                $filePath=__UPLOAD_COURIER_LABEL_PDF__."/".$pdfFileArrs;
                $zip->addFile($filePath,$pdfFileArrs);
            }
            $zip->close();
            
            ob_clean();
            header('Content-type: application/zip'); 
            header('Content-Disposition: attachment; filename="'.basename($zip_file_path).'"');
            header('Content-length: ' . filesize($zip_file_path));
            header('Pragma: no-cache'); 
            header('Expires: 0'); 
            readfile($zip_file_path);
        }
    }
    else
    {
        $pdf = new PDFMerger;
        foreach($pdfFileArr as $pdfFileArrs)
        {
            $filePath=__UPLOAD_COURIER_LABEL_PDF__."/".$pdfFileArrs;
            if($strPdf=='')
            {
                $strPdf =$pdf->addPDF($filePath,"all");
            }
            else
            {
                $strPdf =$pdf->addPDF($filePath,"all");
            }
        }
        $strPdf =$pdf->merge('download', $courierLabelPath);  
    }
    exit;
}
?>
