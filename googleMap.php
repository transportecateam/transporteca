<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
$t_base = "SelectService/";
constantApiKey();
$idWTW = base64_decode(sanitize_all_html_input(trim($_REQUEST['whs_code']))) ;
$postSearchAry=array();
$tempResultAry = array();
$SearchedAry = array();
if($_SESSION['booking_id']>0)
{
	$kBooking = new cBooking();	
	$kWHSSearch = new cWHSSearch();
	
	$postSearchAry = $kBooking->getBookingDetails($_SESSION['booking_id']);
	
	$tempResultAry = $kWHSSearch->getSearchedDataFromTempData($_SESSION['booking_id']);
	$searchResultAry = unserialize($tempResultAry['szSerializeData']);
	
	if(!empty($searchResultAry))
	{
	
		foreach($searchResultAry as $searchResultArys)
		{
			if($searchResultArys['idWTW'] == $idWTW )
			{
				$SearchedAry = $searchResultArys ;
				break;
			}
		}
	}
		
	if(!empty($_REQUEST['action']) && trim($_REQUEST['action'])=='ORIGIN')
	{
		$lat1 = $postSearchAry['fOriginLatitude'];
		$lang1 = $postSearchAry['fOriginLongitude'];		
		$lat2 = $SearchedAry['szFromWHSLat'];
		$lang2 = $SearchedAry['szFromWHSLong'];		
		$distance = $SearchedAry['iDistanceFromWHS'];
		
		$szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br> ".$postSearchAry['szOriginCountry'];
	    $szDestinationAddress = $SearchedAry['szFromWHSPostCode']." - ".$SearchedAry['szFromWHSCity']."<br>".$SearchedAry['szFromWHSCountry'];
		if($SearchedAry['szFromWHSCountry'] ==$postSearchAry['szOriginCountry'] )
		{
			$zoom = 7;
		}
		else
		{
			$zoom = 4;
		}
	}
    if(!empty($_REQUEST['action']) && trim($_REQUEST['action'])=='DESTINATION')
	{
		$lat1 = $postSearchAry['fDestinationLatitude'];
		$lang1 = $postSearchAry['fDestinationLongitude'];
				
		$lat2 = $SearchedAry['szToWHSLat'];
		$lang2 = $SearchedAry['szToWHSLong'];		
		$distance = $SearchedAry['iDistanceToWHS'];
		
		$szOriginAddress =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br> ".$postSearchAry['szDestinationCountry'];
	    $szDestinationAddress = $SearchedAry['szToWHSPostCode']." - ".$SearchedAry['szToWHSCity']."<br>".$SearchedAry['szToWHSCountry'];
	    
	    if($SearchedAry['szDestinationCountry'] ==$postSearchAry['szToWHSCountry'] )
		{
			$zoom = 7;
		}
		else
		{
			$zoom = 4;
		}
	}			
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
    </script>
    <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_URL__?>/style.css" />
    <script type="text/javascript">
	var destinationIcon = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=B|FF0000|000000";
      var originIcon = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=A|FFFF00|000000";
	  var map;
	  var origin = new google.maps.LatLng(<?=$lat1?>, <?=$lang1?>);      
      var destination = new google.maps.LatLng(<?=$lat2?>, <?=$lang2?>);
 
	function initialize() {
	  var myLatLng = new google.maps.LatLng(<?=($lat1+$lat2)/2?>, <?=($lang1+$lang2)/2?>);
	  var myOptions = {
		zoom: <?=$zoom?>,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };

	  map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(<?=$lat1?>, <?=$lang1?>),
		new google.maps.LatLng(<?=$lat2?>, <?=$lang2?>)   
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#FF0000",
		strokeOpacity: 1.0,
		strokeWeight: 2
	  });

	  flightPath.setMap(map);
		
	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
      }
              
	  var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  title:"Origin"
	  }); 
	  
	  var oaddress = '<div class="map-content"><h5><?=$szOriginAddress?></h5></div>';
	  var daddress = '<div class="map-content"><h5><?=$szDestinationAddress?></h5></div>';

	  var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker, 
		'click', 
		infoCallback(oinfowindow, originmarker)
	  );

	  var destinationmarker = new google.maps.Marker({
		  position: destination,
		  map: map,
		  icon: destinationIcon,
		  title:"Destination"
	  });   
	  
	  var dinfowindow = new google.maps.InfoWindow();
	  dinfowindow.setContent(daddress);
	  google.maps.event.addListener(
		destinationmarker, 
		'click', 
		infoCallback(dinfowindow, destinationmarker)
	  ); 
}

</script>
</head>
<body onload="initialize()">
    <div><h5><?=t($t_base.'fields/distance_to_warehouse');?> : <?=number_format((float)$distance,2)?> km</h5></div>
    <div id="map_canvas" style="width:100%; height:100%"></div><br>
    <p align="center">
    	 <a href="javascript:void(0);" onclick="window.close();" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
    </p>
  </body>
</html>