<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
$t_base = "SelectService/";

$kWHSSearch=new cWHSSearch();
$kForwarder = new cForwarder();
$kBooking = new cBooking();
//echo $days_ago = date('Y-m-d H:i:s', strtotime('-5 days', time()));
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

if((int)$idBooking>0)
{
	$idWTW = sanitize_all_html_input($_REQUEST['idWTW']); 
	if($idWTW>0)
	{
            $tempResultAry = $kWHSSearch->getSearchedDataFromTempData($idBooking);
            $searchResultAry = unserialize($tempResultAry['szSerializeData']);
            if(!empty($searchResultAry))
            {
                $updateBookingAry = array();
                foreach($searchResultAry as $searchResultArys)
                {
                    if($searchResultArys['unique_id'] == $idWTW )
                    {
                        $updateBookingAry = $searchResultArys ;
                        break;
                    }
                }
            }
	}
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
}

	if($postSearchAry['iFromRequirementPage']==2)
	{
		$szOriginCountryArr=reverse_geocode($postSearchAry['szOriginCountry']);
		
		if(!empty($szOriginCountryArr))
		{
			$strOrigin="";
			if($szOriginCountryArr['szStreetNumber']!='')
			{
				$strOrigin .=$szOriginCountryArr['szStreetNumber'];
			}
			if($szOriginCountryArr['szRoute']!='')
			{
				if(trim($strOrigin)!='')
				{
					$strOrigin .=", ";
				}
				$strOrigin .=$szOriginCountryArr['szRoute'];
			}
			if($szOriginCountryArr['szCityName']!='')
			{
				if(trim($strOrigin)!='')
				{
					$strOrigin .=", ";
				}
				$strOrigin .=$szOriginCountryArr['szCityName'];
			}
			if($szOriginCountryArr['szState']!='')
			{
				if(trim($strOrigin)!='')
				{
					$strOrigin .=", ";
				}
				$strOrigin .=$szOriginCountryArr['szState'];
			}
			if($szOriginCountryArr['szCountryName']!='')
			{
				if(trim($strOrigin)!='')
				{
					$strOrigin .=", ";
				}
				$strOrigin .=$szOriginCountryArr['szCountryName'];
			}
			if($szOriginCountryArr['szPostCode']!='')
			{
				if(trim($strOrigin)!='')
				{
					$strOrigin .=", ";
				}
				$strOrigin .=$szOriginCountryArr['szPostCode'];
			}
		}
		$szDestinationCountryArr=reverse_geocode($postSearchAry['szDestinationCountry']);
		if(!empty($szDestinationCountryArr))
		{
			$strDes="";
			if($szDestinationCountryArr['szStreetNumber']!='')
			{
				$strDes .=$szDestinationCountryArr['szStreetNumber'];
			}
			if($szDestinationCountryArr['szRoute']!='')
			{
				if(trim($strDes)!='')
				{
					$strDes .=", ";
				}
				$strDes .=$szDestinationCountryArr['szRoute'];
			}
			if($szDestinationCountryArr['szCityName']!='')
			{
				if(trim($strDes)!='')
				{
					$strDes .=", ";
				}
				$strDes .=$szDestinationCountryArr['szCityName'];
			}
			if($szDestinationCountryArr['szState']!='')
			{
				if(trim($strDes)!='')
				{
					$strDes .=", ";
				}
				$strDes .=$szDestinationCountryArr['szState'];
			}
			if($szDestinationCountryArr['szCountryName']!='')
			{
				if(trim($strDes)!='')
				{
					$strDes .=", ";
				}
				$strDes .=$szDestinationCountryArr['szCountryName'];
			}
			if($szDestinationCountryArr['szPostCode']!='')
			{
				if(trim($strDes)!='')
				{
					$strDes .=", ";
				}
				$strDes .=$szDestinationCountryArr['szPostCode'];
			}
		}
	}
        
?>
<style type="text/css">
    .yellow-bg{background-color: yellow;color:#333333;}
</style>
	<div id="popup-bg"></div>
	<div id="popup-container">			
		<div class="compare-popup popup">
		<h4>Price Calculation</h4>
		<p>From Country: <?=$postSearchAry['szOriginCountry']?></p>
		<p>From City: <?=$postSearchAry['szOriginCity']?></p>
		<p>From Postcode: <?=$postSearchAry['szOriginPostCode']?></p>
		<p>From Latitude: <?=$postSearchAry['fOriginLatitude']?></p>
		<p>From Longitude: <?=$postSearchAry['fOriginLongitude']?></p>
		<?php if($postSearchAry['iFromRequirementPage']==2)
		{?>
			<p>From Google Api Detail: <?=$strOrigin?></p>
		<?php		
		}?>
		<hr>
		<p>To Country: <?=$postSearchAry['szDestinationCountry']?></p>
		<p>To City: <?=$postSearchAry['szDestinationCity']?></p>
		<p>To Postcode: <?=$postSearchAry['szDestinationPostCode']?></p>
		<p>To Latitude: <?=$postSearchAry['fDestinationLatitude']?></p>
		<p>To Longitude: <?=$postSearchAry['fDestinationLongitude']?></p>
		<?php if($postSearchAry['iFromRequirementPage']==2)
		{?>
			<p>To Google Api Detail: <?=$strDes?></p>
		<?php		
		}?>
		<hr>
		<p>Transportation: <?php
			if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
			{
				$dtPickupDate = $updateBookingAry['dtCutOffDate'];
				$dtDeliveryDate = $updateBookingAry['dtAvailableDate'] ;
				$dtCutoffDate = $updateBookingAry['dtWhsCutOff'] ;
				$dtAvailableDate = $updateBookingAry['dtWhsAvailable'] ;
				//Total price = Origin port price + PTP Price + Destination port price + custom clearance amount ( if applicable ) + rate per booking + standard trucking rate ( if applicable ) + Haulage price ( if applicable ) + XE Mark-up <br>
				$price_break_up_description = "
					Total price = Origin port price + PTP Price + Destination port price + custom clearance amount ( if applicable ) + rate per booking  + Origin Haulage price + Destination Haulage price + XE Mark-up( if applicable ) <br>
				" ;
				echo "DTD";
			}
			elseif($postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__)
			{
				$dtPickupDate = $updateBookingAry['dtCutOffDate'] ;
				$dtCutoffDate = $updateBookingAry['dtWhsCutOff'] ;
				$dtAvailableDate = $updateBookingAry['dtAvailableDate'] ;
				$price_break_up_description = "
					Total price = Origin port price + PTP Price + Destination port price + custom clearance amount ( if applicable ) + rate per booking  + Origin Haulage price + XE Mark-up( if applicable ) <br>
				" ;
				echo "DTW";
			}
			else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__)
			{
				$dtCutoffDate = $updateBookingAry['dtCutOffDate'] ;
				$dtDeliveryDate = $updateBookingAry['dtAvailableDate'] ;
				$dtAvailableDate = $updateBookingAry['dtWhsAvailable'] ;
				$price_break_up_description = "
					Total price = Origin port price + PTP Price + Destination port price + custom clearance amount ( if applicable ) + rate per booking  + Destination Haulage price + XE Mark-up( if applicable ) <br>
				" ;
				echo "WTD";
			}
			elseif($postSearchAry['idServiceType']==__SERVICE_TYPE_WTW__)
			{
				$dtCutoffDate = $updateBookingAry['dtCutOffDate'] ;
				$dtAvailableDate = $updateBookingAry['dtAvailableDate'] ;
				$price_break_up_description = "
					Total price = Origin port price + PTP Price + Destination port price + custom clearance amount ( if applicable ) + rate per booking + XE Mark-up( if applicable ) <br>
				" ;
				echo "WTW";
			}
			elseif($postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__)
			{
				$dtPickupDate = $updateBookingAry['dtCutOffDate'] ;
				$dtCutoffDate = $updateBookingAry['dtWhsCutOff'] ;
				$dtAvailableDate = $updateBookingAry['dtAvailableDate'] ;
				$price_break_up_description = "
					Total price = Origin port price + PTP Price  + custom clearance amount ( if applicable ) + rate per booking + Origin Haulage price + XE Mark-up( if applicable ) <br>
				" ;
				echo "DTP";
			}
			else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__)
			{
				$dtCutoffDate = $updateBookingAry['dtCutOffDate'] ;
				$dtDeliveryDate = $updateBookingAry['dtAvailableDate'] ;
				$dtAvailableDate = $updateBookingAry['dtWhsAvailable'] ;
				$price_break_up_description = "
					Total price = Destination port price + PTP Price  + custom clearance amount ( if applicable ) + rate per booking + Destination Haulage price + XE Mark-up( if applicable ) <br>
				" ;
				echo "PTD";
			}
			else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTP__) 
			{
				$dtCutoffDate = $updateBookingAry['dtCutOffDate'] ;
				$dtAvailableDate = $updateBookingAry['dtAvailableDate'] ;
				$price_break_up_description = "
					Total price = PTP Price  + custom clearance amount ( if applicable ) + rate per booking + XE Mark-up( if applicable ) <br>
				" ;
				echo "PTP";
			}
			else if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTW__) 
			{
				$dtCutoffDate = $updateBookingAry['dtCutOffDate'] ;
				$dtAvailableDate = $updateBookingAry['dtAvailableDate'] ;
				$price_break_up_description = "
					Total price = Destination port price + PTP Price  + custom clearance amount ( if applicable ) + rate per booking + XE Mark-up( if applicable ) <br>
				" ;
				echo "PTW";
			}
			else if($postSearchAry['idServiceType']==__SERVICE_TYPE_WTP__) 
			{
				$dtCutoffDate = $updateBookingAry['dtCutOffDate'] ;
				$dtAvailableDate = $updateBookingAry['dtAvailableDate'] ;
				
				$price_break_up_description = "
					Total price = Origin port price + PTP Price  + custom clearance amount ( if applicable ) + rate per booking + XE Mark-up( if applicable ) <br>
				" ;
				echo "WTP";
			}
		?></p>
		<p>Origin Customs Clearance: <? if($postSearchAry['iOriginCC']==1){?>Yes<? }else {?>No<? }?></p>
		<p>Destination Customs Clearance: <? if($postSearchAry['iDestinationCC']==2){?>Yes<? }else {?>No<? }?></p>
		
		<p>Cargo volume (cubic meter): <?=$postSearchAry['fCargoVolume']?></p>
		<p>Cargo volume (rounded): <?=ceil($postSearchAry['fCargoVolume'])?></p>
		
		<p>Cargo weight (metric tons): <?=$postSearchAry['fCargoWeight']/1000?></p>
		<p>Cargo weight (rounded): <?=ceil($postSearchAry['fCargoWeight']/1000)?></p>
		<p>Origin Warehouse: <?=$updateBookingAry['idWarehouseFrom']?><br>
		  - Latitude: <?=$updateBookingAry['szFromWHSLat']?><br>
		  - Longitude: <?=$updateBookingAry['szFromWHSLong']?> </p>
		 <p>Destination Warehouse: <?=$updateBookingAry['idWarehouseTo']?><br>
		  - Latitude: <?=$updateBookingAry['szToWHSLat']?><br>
		  - Longitude: <?=$updateBookingAry['szToWHSLong']?> </p>
		 <hr> 
		 <p>PricingPTP - RateWM: <?=$updateBookingAry['fRateWM']?> </p>
		 <p>PricingPTP - Min Rate: <?=$updateBookingAry['fMinRateWM']?> </p>
		 <p>PricingPTP - Rate per booking : <?=$updateBookingAry['fRate']?> </p>
		 <p>Currency Id: <?=$updateBookingAry['szFreightCurrency']?></p>
		 <p class="yellow-bg">Total PTP Price : <?=$postSearchAry['szCurrency']." ".round((float)($updateBookingAry['fPTPPrice']),2)?></p>
		 <p> Transit Hours: <?=$updateBookingAry['iTransitHours']?></p>
		 <p> Booking Cut-off: <?=$updateBookingAry['iBookingCutOffHours']?></p>
		 <p> CutOff Day: <?=$updateBookingAry['szWeekDay']?></p>
		 <p> Available Day: <?=$updateBookingAry['szAvailableDay']?></p>
		 
		 <hr>
		 <p>Origin port - RateWM: <?=$updateBookingAry['fOriginRateWM']?> </p>
		 <p>Origin port - Min Rate: <?=$updateBookingAry['fOriginMinRateWM']?> </p>
		 <p>Origin port - Rate per booking : <?=$updateBookingAry['fOriginRate']?> </p>
		 <p>Origin port Currency Id: <?=$updateBookingAry['szOriginRateCurrency']?></p>
		 <p class="yellow-bg">Origin port Total price : <?=$postSearchAry['szCurrency']." ".$updateBookingAry['fTotalOriginPortPrice']?></p>
		 <p>Origin port Exchange rate : <?=$updateBookingAry['fOriginFreightExchangeRate']?></p>
		 <hr>
		 <p>Destination port - RateWM: <?=$updateBookingAry['fDestinationRateWM']?> </p>
		 <p>Destination port - Min Rate: <?=$updateBookingAry['fDestinationMinRateWM']?> </p>
		 <p>Destination port - Rate per booking : <?=$updateBookingAry['fDestinationRate']?> </p>
		 <p>Destination port Currency Id: <?=$updateBookingAry['szDestinationRateCurrency']?></p>
		 <p class="yellow-bg">Destination port Total price : <?=$postSearchAry['szCurrency']." ".$updateBookingAry['fTotalDestinationPortPrice']?></p>
		 <p>Destination port Exchange rate : <?=$updateBookingAry['fDestinationFreightExchangeRate']?></p>
		 <hr>		 
		 <p>Origin Haulage - Price per 100 Kg : <?=$updateBookingAry['fOriginHaulageRateWM_KM']?></p>
		 <p>Origin Haulage - Min Price : <?=$updateBookingAry['fOriginHaulageMinRateWM_KM']?></p>
		 <p>Origin Haulage - Price per booking : <?=$updateBookingAry['fOriginHaulageBookingRateWM_KM']?></p>
		 <p>Origin Haulage - Currency  : <?=$updateBookingAry['szOriginHaulageCurrency']?></p>
		 <p>Origin Haulage - Exchange rate : <?=$updateBookingAry['fOriginHaulageExchangeRate']>0 ? (1/$updateBookingAry['fOriginHaulageExchangeRate']):0?></p>
		 
		 <p>Origin Haulage - Fuel Surcharge : <?=$updateBookingAry['fOriginHaulageFuelSurcharge']?></p>
		 <p>Origin Haulage - Distance Up to Km : <?=$updateBookingAry['iOriginHaulageDistanceUptoKm']?></p>
		 <p>Origin Haulage - Pricing model name : <?=$updateBookingAry['szOriginHaulageModelName']?></p>
		 
		 <p class="yellow-bg">Origin Haulage Price : <?=$postSearchAry['szCurrency']." ".$updateBookingAry['fOriginHaulagePrice']?></p>
		 <p>Origin Haulage Transit (hours) : <?=$updateBookingAry['fOriginHaulageTransitTime']?></p>
		 <p>Origin Haulage Distance(km) : <?=$updateBookingAry['fOriginHaulageDistance']?></p>		
		 
		 <hr>
		 <p>Destination Haulage - Price per 100 Kg : <?=$updateBookingAry['fDestinationHaulageRateWM_KM']?></p>
		 <p>Destination Haulage -  Min Price : <?=$updateBookingAry['fDestinationHaulageMinRateWM_KM']?></p>
		 <p>Destination Haulage - Price per booking : <?=$updateBookingAry['fDestinationHaulageBookingRateWM_KM']?></p>		 
		 <p>Destination Haulage - Currency : <?=$updateBookingAry['szDestinationHaulageCurrency']?></p>
		 <p>Destination Haulage - Exchange rate : <?=$updateBookingAry['fDestinationHaulageExchangeRate']>0 ? (1/$updateBookingAry['fDestinationHaulageExchangeRate']):0?></p>
		 
		 <p>Destination Haulage - Fuel Surcharge : <?=$updateBookingAry['fDestinationHaulageFuelSurcharge']?></p>
		 <p>Destination Haulage - Distance Up to Km : <?=$updateBookingAry['iDestinationHaulageDistanceUptoKm']?></p>
		 <p>Destination Haulage - Pricing model name : <?=$updateBookingAry['szDestinationHaulageModelName']?></p>
		 
		 <hr>		 
		 <p class="yellow-bg">Destination Haulage Price : <?=$postSearchAry['szCurrency']." ".$updateBookingAry['fDestinationHaulagePrice']?></p>
		 <p>Destination Haulage Transit (hours) : <?=$updateBookingAry['fDestinationHaulageTransitTime']?></p>
		 <p>Destination Haulage Distance(km) : <?=$updateBookingAry['fDestinationHaulageDistance']?></p>
		 <hr>		 
		 <p class="yellow-bg">Origin Customs Clearance: <?=$postSearchAry['szCurrency']." ".$updateBookingAry['fOriginCCPrice']?> </p>
		 <p class="yellow-bg">Destination Customs Clearance: <?=$postSearchAry['szCurrency']." ".$updateBookingAry['fDestinationCCPrice']?></p>
		 <p>Origin Warehouse Country: <?=$updateBookingAry['szFromWHSCountry']?></p>
		 <p>Origin Standard Trucking: <?=$updateBookingAry['fOriginTruckingRate']?></p>
		 <p>Destination Warehouse Country: <?=$updateBookingAry['szToWHSCountry']?></p>
		 <p>Destination Standard Trucking: <?=$updateBookingAry['fDestinationTruckingRate']?></p>
		 <hr>
		 <p>Haulage hours origin: <?=$updateBookingAry['fOriginHaulageTransitTime']?></p>
		 <p>Haulage hours destination: <?=$updateBookingAry['fDestinationHaulageTransitTime']?></p>
		 <p>cut-off hours: <?=$updateBookingAry['iBookingCutOffHours']?></p>
		 <p>Transit hours : <?=$updateBookingAry['iTransitHours']?></p>
		 <p>cut-off Day: <?=$updateBookingAry['szWeekDay']?></p>
		 <p>Available Day : <?=$updateBookingAry['szAvailableDay']?></p>
		 
		 <p>Pick-up date/time: <?=$dtPickupDate?></p>
		 <p>Cut off date/time : <?=$dtCutoffDate?></p>
		 <p>Available date/time: <?=$dtAvailableDate?></p>
		 <p>Delivery date/time : <?=$dtDeliveryDate?></p>		 
		 <p>difference from origin to destination in hours: <?=$updateBookingAry['dtTransportTime_ms']/(1000*60*60)?></p>
                 
                <hr>
		 <p><h1>Pricing details </h1></p>
		 <p>Origin port Price : <?=$updateBookingAry['fOriginFreightExchangeRate']>0 ? round((float)($updateBookingAry['fTotalOriginPortPrice']/$updateBookingAry['fOriginFreightExchangeRate']),2):0.00?></p>
		 <p>PTP Price : <?=$updateBookingAry['fOriginFreightExchangeRate']>0 ? round((float)($updateBookingAry['fPTPPrice']/$updateBookingAry['fOriginFreightExchangeRate']),2):0.00?></p>
		 <p>Destination port Price : <?=$updateBookingAry['fTotalDestinationPortPrice']?></p>
		 <p>Custom Clearance Price : <?=$updateBookingAry['fOriginCCPrice'] + $updateBookingAry['fDestinationCCPrice']?></p>
		 <p>Haulage Price : <?=$updateBookingAry['fOriginHaulagePrice'] + $updateBookingAry['fDestinationHaulagePrice']?></p> 
		 <p>XE Mark-up: <?=$updateBookingAry['fTotalMarkupPrice']?></p>
		 <hr>
		 <p>/**
			 * calculating done by using following fomula<br>
			 * <br>
			 * <?=$price_break_up_description?>
			 *<br>
			*/<br>
		</p>		 
		 <p> price for sorting: <?=$updateBookingAry['fTotalPriceCustomerCurrency']?></p>
		 <p> price for Display: <?=$updateBookingAry['fDisplayPrice']?></p>
		</div>
	</div>