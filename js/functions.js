var adminForwarder = null;
var myData = {};
//var pHeight = $(document).height();

function showHide(id)
{
    var disp = $("#"+id).css('display');
    if(disp=='block')
    {
        $("#"+id).css('display','none');
        removePopupScrollClass(id);
    }
    else
    {
        $("#"+id).css('display','block');
        addPopupScrollClass(id);
    }
}
function hide_google_map()
{	
    $("#popup_container_google_map").attr('style','display:none;');
    $("#google_map_target_1").attr('style','display:none;');
    removePopupScrollClass('popup_container_google_map');
    checkCfsDetails();
}

function emptyDiv(id)
{
    var disp = $("#"+id).css('display');
    if(disp=='block')
    {
        $("#"+id).css('display','none'); 
        var szEmptyStr = '';
        $("#"+id).html(szEmptyStr);
    }
    else
    {
        $("#"+id).css('display','block'); 
    }
}
function show_help_popup(id)
{
    $.get(__JS_ONLY_SITE_BASE__+"/help_select_graph_popup.php",function(result){
        $("#"+id).attr('style','display:block;');
        $("#"+id).html(result);
        addPopupScrollClass(id);
    });
}

function add_more_cargo(mode)
{
    if($("#iDoNotKnow").attr('checked'))
    {
    	$("#iDoNotKnow").removeAttr('checked','');
            autofill_cargo_dimentions(mode,false);
	}
		
	var number = document.getElementById('hiddenPosition').value;
	var divid = document.getElementById('cargo');;
	number=parseInt(number);
	var hidden1 = document.getElementById('hiddenPosition1').value;
	document.getElementById('hiddenPosition1').value=parseInt(hidden1)+1;
	if(mode=='POP_UP')
	{
            $("#remove").attr('style','display:block;width:304px;');
	}
	else
	{
            //document.getElementById('remove').style.display='block';
	}	
	
	document.getElementById('hiddenPosition').value =  parseInt(number)+1;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_showCargo.php",{number:number,mode:mode},function(result){
	         $("#cargo"+number).html(result);
	});
	var newDiv=document.createElement('div'); 
	newDiv.setAttribute('id', 'cargo'+(number+1));
	divid.appendChild(newDiv);
	
	if(mode=='REQUIREMENT_PAGE')
	{
		enable_cargo_show_option_button();
	}
}
function remove_cargo(mode)
{
    var number = document.getElementById('hiddenPosition').value;
    var hidden1=document.getElementById('hiddenPosition1').value;
    var id=parseInt(hidden1)-1;
    div_id="cargo"+id;
    document.getElementById('hiddenPosition').value=parseInt(number)-1;
    document.getElementById('hiddenPosition1').value=parseInt(hidden1)-1;
    document.getElementById(div_id).innerHTML='';
    if(id==1)
    {
        document.getElementById('remove').style.display='none';
    }

    if(mode=='REQUIREMENT_PAGE')
    {
        enable_cargo_show_option_button();
    }
}

function add_more_cargo_details(mode,booking_key,search_mini,idSearchMiniStandardCargo)
{  
    var iSearchMini = parseInt(search_mini); 
    if(iSearchMini>0 || mode=='PALLET_DETAILS_PAGE')
    {
        if(iSearchMini==3 || iSearchMini==7)
        {
            var iLandingPageFlag = $("#iLandingPageFlag").val();
            var active_counter=$('input.first-cargo-fields_V_'+iSearchMini).length ; 
            if(active_counter>=20 && iLandingPageFlag==1)
            {
                //On Landing page only allowed to add 6 cargo line
                return false;
            } 
        } 
        
        if(mode=='PALLET_DETAILS_PAGE')
        { 
            iSearchMini = "POP_V_"+iSearchMini ;
        }  
        
        var number = document.getElementById('hiddenPosition_'+iSearchMini).value;  
        var table_id = document.getElementById('horizontal-scrolling-div-id_'+iSearchMini);   
        number=parseInt(number); 
         
        var hidden1 = document.getElementById('hiddenPosition1_'+iSearchMini).value; 
        document.getElementById('hiddenPosition1_'+iSearchMini).value=parseInt(hidden1)+1; 
        document.getElementById('hiddenPosition_'+iSearchMini).value =  parseInt(number)+1;  
        
        if(mode=='PALLET_DETAILS_PAGE')
        {
            $("#iPalletCounter_"+iSearchMini).val(number+1);
        }
        var div_container_id = "add_booking_quote_container_"+number+"_"+iSearchMini;
        var newTdId = 'add_booking_quote_container_'+(number+1)+"_"+iSearchMini ;
    }
    else
    {
        var number = document.getElementById('hiddenPosition').value;  
        var table_id = document.getElementById('horizontal-scrolling-div-id'); 

        number=parseInt(number); 
        var hidden1 = document.getElementById('hiddenPosition1').value; 
        document.getElementById('hiddenPosition1').value=parseInt(hidden1)+1; 
        document.getElementById('hiddenPosition').value =  parseInt(number)+1; 
        
        var div_container_id = "add_booking_quote_container_"+number;
        var newTdId = 'add_booking_quote_container_'+(number+1) ;
    } 
    var site_language = $("#iSiteLanguageFooter_hidden").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_showCargo.php?lang="+site_language,{number:number,mode:mode,booking_key:booking_key,search_mini:search_mini,idSearchMiniStandardCargo:idSearchMiniStandardCargo},function(result){
        $("#"+div_container_id).html(result); 
        if(mode=='FURNITURE_ORDER_PAGE')
        {
            $("#"+div_container_id).addClass("request-quote-fields clearfix");  
        }
        else
        {
            $("#"+div_container_id).attr("style",'display:  block; '); 
        }
        if(search_mini==3)
        {
            var active_counter=$('input.first-cargo-fields_V_'+iSearchMini).length ;  
            if(active_counter>1)
            {
                $("#cargo-line-remove-1-v"+iSearchMini).attr('style','opacity:0.4');
                $("#cargo-line-remove-1-v"+iSearchMini).removeAttr('onclick');
                $("#cargo-line-remove-1-v"+iSearchMini).unbind("click");
            }  
            if(active_counter>=20 && iLandingPageFlag==1)
            {
                $(".cargo-add-button").attr('style','opacity:0.4'); 
            } 
        }
        if(search_mini == 7)
        {
            $("#hiddenPosition_7").val(number+1); 
        }
    }); 
    
    if($("#"+newTdId).length)
    {
        console.log("td already exists");
    }
    else
    {
        var newTd=document.createElement('div'); 
        newTd.setAttribute('id', newTdId); 
        //newTd.setAttribute('style', 'display:none;');
        table_id.appendChild(newTd); 
    }
} 

function remove_cargo_details_block(div_id,field_count,search_mini)
{   
    var iSearchMiniPage = parseInt(search_mini); 
    console.log("iSearchMiniPage"+iSearchMiniPage);
    var szDivKey = "";
    if(iSearchMiniPage>0)
    { 
        if(search_mini==5 || search_mini==6)
        {
            szDivKey = "_POP_V_"+iSearchMiniPage;
        }
        else
        {
            szDivKey = "_V_"+iSearchMiniPage;
        } 
    }  
    var active_counter=$('input.first-cargo-fields'+szDivKey).length ; 
    console.log(active_counter+"active_counter");
    if(active_counter>1)
    {
        var number = $('#hiddenPosition'+szDivKey).attr('value'); 
        var hidden1= $('#hiddenPosition1'+szDivKey).attr('value');
        var hidden_value = parseInt(hidden1)-1;

        $('#hiddenPosition'+szDivKey).attr('value',hidden_value);
        $('#hiddenPosition1'+szDivKey).attr('value',hidden_value); 

        $("#"+div_id).attr("style",'display:none;');
        $("#"+div_id).html(" "); 
        if(search_mini==3 || search_mini==7)
        {
            var active_counter = $('input.first-cargo-fields'+szDivKey).length ;  
            if(active_counter==1)
            {
                $("#cargo-line-remove-1-v"+search_mini).attr('style','opacity:1');
                $("#cargo-line-remove-1-v"+search_mini).removeAttr('onclick');
                $("#cargo-line-remove-1-v"+search_mini).unbind("click");
                $("#cargo-line-remove-1-v"+search_mini).click(function(){ remove_cargo_details_block('add_booking_quote_container_0_'+search_mini,'1',search_mini); });
            } 
            var iLandingPageFlag = $("#iLandingPageFlag").val(); 
            if(active_counter<20 && iLandingPageFlag==1)
            { 
                $(".cargo-add-button").attr('style','opacity:1'); 
            }  
        } 
    }
    else
    {
        if(search_mini==5 || search_mini==6)
        {
            $("#iLength"+field_count+szDivKey).val("");
            $("#iWidth"+field_count+szDivKey).val(""); 
        }
        else if(search_mini==7)
        {   
            $('#idCargoCategory'+field_count+szDivKey+' :selected').removeAttr('selected');
            $('#idCargoCategory'+field_count+szDivKey +"option[value='']").attr('selected', 'selected'); 
            $('#idStandardCargoType'+field_count+szDivKey+' :selected').removeAttr('selected');
            $('#idStandardCargoType'+field_count+szDivKey +"option[value='']").attr('selected', 'selected');
            $("#selectidCargoCategory"+field_count+szDivKey).html('Kategori');
            $("#selectidStandardCargoType"+field_count+szDivKey).html('Type');
            $("#iQuantity"+field_count+szDivKey).val("");
        }        
        else
        {
            $("#iLength"+field_count+szDivKey).val("");
            $("#iWidth"+field_count+szDivKey).val("");
            $("#iHeight"+field_count+szDivKey).val("");
            $("#iWeight"+field_count+szDivKey).val("");
            $("#iQuantity"+field_count+szDivKey).val("");
        } 
    } 
}

function validateLandingPageForm(formId,idUser,page_url)
{
    if(isFormInputElementHasDefaultValue('szOriginCity','Type name')) 
	{
       $("#szOriginCity").attr('value',"");
    }
    if(isFormInputElementHasDefaultValue('szOriginPostCode','Optional') || isFormInputElementHasDefaultValue('szOriginPostCode','Type code')) 
	{
        $("#szOriginPostCode").attr('value',"");
    }    
    if(isFormInputElementHasDefaultValue('szDestinationCity','Type name')) 
	{
       $("#szDestinationCity").attr('value',"");
    }
    if(isFormInputElementHasDefaultValue('szDestinationPostCode','Optional') || isFormInputElementHasDefaultValue('szDestinationPostCode','Type code')) 
	{
       $("#szDestinationPostCode").attr('value',"");
    }    
    if(isFormInputElementHasDefaultValue('datepicker1','dd/mm/yyyy'))
	{
       $("#datepicker1").attr('value',"");
    }
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#"+formId).serialize(),function(result){		     
         result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {
         	$("#regError").css('display','block'); 
         	$("#regError").html(result_ary[1]);
         	window.scrollTo(50,50);
         }
         else if(jQuery.trim(result_ary[0])=='CARGO_EXCEED_POPUP')
         {
        	$("#all_available_service").attr('style','display:block');
          	$("#regError").attr('style','display:none;');
          	$("#all_available_service").html(result_ary[1]);
         }
         else if(jQuery.trim(result_ary[0])=='MULTI_EMPTY_POSTCODE')
         {
         	$("#Transportation_pop").attr('style','display:block');
         	$("#regError").attr('style','display:none;');
         	$("#Transportation_pop").html(result_ary[1]);
         	addPopupScrollClass('Transportation_pop');
         }
         else if(jQuery.trim(result_ary[0])=='MULTIREGIO')
         {
         	$("#Transportation_pop").attr('style','display:block');
         	$("#regError").attr('style','display:none;');
         	$("#Transportation_pop").html(result_ary[1]);
         	addPopupScrollClass('Transportation_pop');
         }    
         else if(jQuery.trim(result_ary[0])=='SUCCESS_LOCAL_SEARCH')
         {
            $("#regError").attr('style','display:none;');
            $("#ajaxLogin").html(result_ary[1]);
            $("#ajaxLogin").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='NO_SERVICE_POPUP')
         {
         	$("#all_available_service").attr('style','display:block');
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         }         
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
         	var szPageLoaction=$("#szPageLoaction").attr('value');
         	$("#regError").attr('style','display:none;');
         	var user_id = parseInt(idUser);
         	if(szPageLoaction==1)
         	{
         		var szBookingRandomNum = $("#szBookingRandomNum").attr('value');
	    		var page_url_1 = page_url + "/" + szBookingRandomNum + "/";
		        redirect_url(page_url_1);
         	}
         	else
         	{
	         	if(user_id==0 || isNaN(user_id) || user_id=='NaN')
		    	{
		    		show_currency_popup(formId);
		    	}
		    	else
		    	{  
		    		var szBookingRandomNum = $("#szBookingRandomNum").attr('value');
		    		var page_url_1 = page_url + "/" + szBookingRandomNum + "/";
		        	redirect_url(page_url_1);
		        }
		    }
         }
        // $("#overlay").remove();
    });      
    return false;      
}


function isCheckboxChecked(formId, inputElementId)
{
    if($("#"+inputElementId).attr('checked'))
    {
        return true;
    }
    else 
    {
        return false;
    }
}

function isFormInputElementEmpty(formId, inputElementId,default_text) 
{	
    var field_value=$("#"+inputElementId).attr('value');
    if((jQuery.trim(field_value) == "") || (field_value==default_text))
    {
        return true;
    }
    else 
    {
        return false;
    }
}
function isFormInputElementHasDefaultValue(inputElementId,default_text) 
{	
	var field_value=$("#"+inputElementId).attr('value');
	if(field_value==default_text)
    {
        return true;
    }
    else 
    {
        return false;
    }
}

 
function activatePostCodeField(field_value,inputElementId,inputElementId2,text_str1,text_str2,text_str3)
{
  if((jQuery.trim(field_value) != "")) 
  {
    $("#"+inputElementId).removeAttr("disabled");   
    $("#"+inputElementId2).removeAttr("disabled");   
    
    $("#"+inputElementId).attr("value",text_str1);   
    var idServiceType = $("#szTransportation").attr("value");
    if(idServiceType==1 || idServiceType==2 || idServiceType==5)
    {
    	 $("#"+inputElementId2).attr("value",text_str3);
    }
    else
    {
    	 $("#"+inputElementId2).attr("value",text_str2);
    }
      
    $("#"+inputElementId).attr("style","color:grey;font-style:italic;");   
    $("#"+inputElementId2).attr("style","color:grey;font-style:italic;");
  }
  else
  {
    $("#"+inputElementId).attr("disabled","disabled");
    $("#"+inputElementId2).attr("disabled","disabled");
    
    $("#"+inputElementId).attr("value","");   
    $("#"+inputElementId2).attr("value","");  
  }
}
function change_postcode_city_default_text(idServiceType,val1,val2,val3)
{
	var szOriginPostCode_disable = $("#szOriginPostCode").attr('disabled');
	if(szOriginPostCode_disable!='disabled')
	{
		if(idServiceType==1 || idServiceType==2 || idServiceType==5)
	    {
	    	var szOriginPostCode = $("#szOriginPostCode").attr("value");
	    	if(szOriginPostCode=='' || szOriginPostCode==val2)
	    	{
	    		 $("#szOriginPostCode").attr("value",val3);
	    	}	
	    	var szOriginCity = $("#szOriginCity").attr("value");
	    	if(szOriginCity=='')
	    	{
	    		 $("#szOriginCity").attr("value",val1);
	    	}
	    }
	    else
	    {
	    	var szOriginPostCode = $("#szOriginPostCode").attr("value");
	    	if(szOriginPostCode=='' || szOriginPostCode==val3)
	    	{
	    		 $("#szOriginPostCode").attr("value",val2);
	    	}	
	    	var szOriginCity = $("#szOriginCity").attr("value");
	    	if(szOriginCity=='')
	    	{
	    		 $("#szOriginCity").attr("value",val1);
	    	}
	    }
	}
	
	var szDestinationPostCode_disable = $("#szDestinationPostCode").attr('disabled');
	if(szDestinationPostCode_disable!='disabled')
	{
		if(idServiceType==1 || idServiceType==3 || idServiceType==8)
	    {
	    	var szDestinationPostCode = $("#szDestinationPostCode").attr("value");
	    	if(szDestinationPostCode=='' || szDestinationPostCode==val2)
	    	{
	    		 $("#szDestinationPostCode").attr("value",val3);
	    	}	
	    	
	    	var szDestinationCity = $("#szDestinationCity").attr("value");
	    	if(szDestinationCity=='')
	    	{
	    		 $("#szDestinationCity").attr("value",val1);
	    	}    	
	    }
	    else
	    {
	    	var szDestinationPostCode = $("#szDestinationPostCode").attr("value");
	    	if(szDestinationPostCode=='' || szDestinationPostCode==val3)
	    	{
	    		 $("#szDestinationPostCode").attr("value",val2);
	    	}
	    	
	    	var szDestinationCity = $("#szDestinationCity").attr("value");
	    	if(szDestinationCity=='')
	    	{
	    		 $("#szDestinationCity").attr("value",val1);
	    	} 
	    }
	}
}

function show_currency_popup(form_id,szBookingRandomNum,mode)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_selectCurrency.php",$("#"+form_id).serialize(),function(result){
		 $("#Transportation_pop").attr('style','display:block');		
         $("#Transportation_pop").html(result);
         $("#idCurrency").focus();
         addPopupScrollClass('Transportation_pop');         
      });
}
function show_mainpage_tooltip(tool_tip_header,tool_tip_text,div_id,evt,booking_details_page)
{	
		evt = $.event.fix(evt || window.event);		
		$("#"+div_id).css({
			top: evt.pageY + 1,
			left: evt.pageX + -40
		}).show();	
		
		tool_tip_text = tool_tip_text.charAt(0).toUpperCase() + tool_tip_text.slice(1);            // capitalising first char to uppercase 
            var header_text = "<span style='font-weight:bold;'>"+tool_tip_header+"</span>" ;
            var text_content = "&nbsp;<span style='font-style:italic;'>"+tool_tip_text+"</span>" ;
            tool_tip_text= header_text + text_content ;  
                 
            if(booking_details_page==1)
            {
            	$("#"+div_id).html("<div>"+tool_tip_text+"</div>");
            }
            else
            {
            	$("#"+div_id).html("<div>"+tool_tip_text+"</div><img src='"+__JS_ONLY_SITE_BASE_IMAGE__+"/images/help-blue-pop-arrow.png' />");
            }    
              

}

function show_mainpage_tooltip_right(tool_tip_header,tool_tip_text,div_id,evt)
{	
	  evt = $.event.fix(evt || window.event);
	  $("#"+div_id).css({
	   top: evt.pageY + 4,
	   left: evt.pageX + -260
	  }).show();

	  tool_tip_text = tool_tip_text.charAt(0).toUpperCase() + tool_tip_text.slice(1);            // capitalising first char to uppercase 
		var header_text = "<span style='font-weight:bold;'>"+tool_tip_header+"</span>" ;
		var text_content = "&nbsp;<span style='font-style:italic;'>"+tool_tip_text+"</span>" ;
		tool_tip_text= header_text + text_content ;           
	  $("#"+div_id).html("<div>"+tool_tip_text+"</div><img src='"+__JS_ONLY_SITE_BASE_IMAGE__+"/images/help-blue-pop-arrow.png' />");

}

function hide_tool_tip(div_id,flag)
{
	if(flag==1)
	{
		$("#"+div_id).attr('style','visibility:hidden;');
	}
	else
	{
		$("#"+div_id).hide();
	}
}

function blank_me(id,val,val2)
{	
	var inputFeildValue = $("#"+id).attr("value");
	if(inputFeildValue == val || (inputFeildValue == val2 && val2!='') )
	{
		$("#"+id).attr("value","");
		$("#"+id).removeAttr("style");   
	}
}

function show_me(id,val,val2)
{
	var inputFeildValue = $("#"+id).attr("value");
	if(id=='szPostCode')
	{
		szPostCode1 = $("#szPostCode").attr('value');
		if(szPostCode1=='')
		{
			$("#"+id).attr("style","color:grey;font-style:italic;width:135px;"); 
		}
		else
		{
			$("#"+id).attr("style","width:135px;"); 
		}
	}
	if(id=='szComment')
	{
		szComment1 = $("#szComment").attr('value');
		if(szComment1=='')
		{
			$("#"+id).attr("style","width:396px;color:grey;font-style:italic;"); 
		}
		else
		{
			$("#"+id).attr("style","width:396px;"); 
		}
	}
	
	if(inputFeildValue == '')
	{
		if((id=='szOriginPostCode' || id=='szDestinationPostCode') && val2!='')
		{
			idServiceType = $("#szTransportation").attr('value');
			if((idServiceType==1 || idServiceType==2 || idServiceType==5) && (id=='szOriginPostCode'))
			{
				$("#"+id).attr("value",val2);
				$("#"+id).attr("style","color:grey;font-style:italic;"); 
			}
			else if((idServiceType==1 || idServiceType==3 || idServiceType==8) && (id=='szDestinationPostCode'))
			{
				$("#"+id).attr("value",val2);
				$("#"+id).attr("style","color:grey;font-style:italic;"); 
			}
			else
			{
				$("#"+id).attr("value",val);
				$("#"+id).attr("style","color:grey;font-style:italic;"); 
			}	
		}
		else if((id=='szShipperPostcode' || id=='szConsigneePostcode') && val2!='')
		{
			idServiceType = $("#szTransportation").attr('value');
			if((idServiceType==1 || idServiceType==2 || idServiceType==5) && (id=='szShipperPostcode'))
			{
				$("#"+id).attr("value",val2);
			}
			else if((idServiceType==1 || idServiceType==3 || idServiceType==8) && (id=='szConsigneePostcode'))
			{
				$("#"+id).attr("value",val2);
			}
			else
			{
				$("#"+id).attr("value",val);
			}	
		}
		else if(id=='szComment')
		{
			$("#"+id).attr("value",val);
			$("#"+id).attr("style","width:396px;color:grey;font-style:italic;");
		}
		else
		{
			$("#"+id).attr("value",val);
			$("#"+id).attr("style","color:grey;font-style:italic;"); 
		}	
	}
}

function show_me_header(id,val)
{
	var inputFeildValue = $("#"+id).attr("value");
	if(inputFeildValue=='')
	{
		$("#"+id).attr("value",val);
	}
}

function submit_multi_region_city(formId,idUser,szBookingRandomNum,page_url)
{
	if(idUser==0)
   	{
   		show_currency_popup(formId,szBookingRandomNum);
   	}
   	else
   	{    	
   		var page_url_1 = page_url + "/" + szBookingRandomNum + "/";
       	redirect_url(page_url_1);
    }
}

function openTip(divid)
{
    $("#"+divid).attr("style","display:block");
}

function closeTip(divid)
{
    $("#"+divid).attr("style","display:none");
}

function create_account()
{
    var value=decodeURIComponent($("#createAccount").serialize());
    //var new_value=value+'&mobile='+mobile_new;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_createAccount.php",value,function(result){
    $("#userAccountCreate").html(result);});
}
function resendActivationCode(url,szBookingRandomNum)
{
	if(szBookingRandomNum == '')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	
	if(szBookingRandomNum=='undefined')
	{
		szBookingRandomNum = '';
	}	
	$.get(__JS_ONLY_SITE_BASE__+"/resendActivationCode.php",{redirect_uri:url,szBookingRandomNum:szBookingRandomNum},function(result){
		
		res_ary = result.split("||||");
		if(res_ary[0]=='SUCCESS')
		{
			$("#activationkey").attr("style","display:none;");
			$("#ajaxLogin").attr("style","display:none;");
			
			$("#leave_page_div").html(res_ary[1]);		
			$("#leave_page_div").attr("style","display:block;");
		}
		else
		{
			$("#activationkey").html(result);	
			$("#activationkey").attr("style","display:block");
		}
		
	});
}

function resendActivationCode_bc(url,div_id,szBookingRandomNum)
{
	//var szBookingRandomNum
	if(szBookingRandomNum == '' || szBookingRandomNum=='undefined')
	{
		szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	
	if(szBookingRandomNum=='undefined')
	{
		szBookingRandomNum = '';
	}	
	$.get(__JS_ONLY_SITE_BASE__+"/resendActivationCode.php",{redirect_uri:url,szBookingRandomNum:szBookingRandomNum},function(result){
		$("#"+div_id).html(result);
		$("#"+div_id).attr("style","display:block");
	});
}

function changeEmailAddress()
{
	 $('input, select').attr('disabled',false);
     $("#form_submit_button").attr('href','javascript:void(0)');
     $("#form_submit_button").attr("onclick","encode_string('szPhoneNumber','szPhoneNumberUpdate');create_account();");
     $("#form_submit_button").attr("style","opacity:1;"); 
	 $("#complete_signup").attr("style","display:none;");
}

function editUserInformation()
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",{showflag:'user_info'},function(result){
	$("#myaccount_info").html(result);});
}

function update_userinfo_details()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",$("#updateUserInfo").serialize(),function(result){
	$("#myaccount_info").html(result);});
}

function cancel_user_info(value)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",function(result){
	$("#myaccount_info").html(result);});
}

function edit_contact_information()
{	

	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",{showflag:'contact_info'},function(result){
	$("#myaccount_info").html(result);});
}

function edit_user_contact_info()
{
	var value=decodeURIComponent($("#edit_contact_information_data").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",value,function(result){
	$("#myaccount_info").html(result);});
}

function change_password()
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",{showflag:'change_pass'},function(result){
	$("#myaccount_info").html(result);});
}

function changeUserPassword()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",$("#changePassword").serialize(),function(result){
	$("#myaccount_info").html(result);});
}
function change_date(dtDate,default_text)
{
    idTiming = $("#szTiming").attr('value');
    szdate = $("#datepicker1").attr('value');
    if(idTiming==1)
    {		
        if(szdate=="" || szdate==default_text)
        {
            $("#datepicker1").attr("style","");   
            $("#datepicker1").attr('value',dtDate);
        }
    }
    else if(idTiming==2 && szdate==dtDate)
    {
        $("#datepicker1").attr("style","color:grey;font-style:italic;");   
        $("#datepicker1").attr('value',default_text); 
    }
} 
function ajaxLogin(redirect_url,cancel_url,booking_key,from_page)
{	
    disableForm(); 
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_login.php",{redirect_url:redirect_url,cancel_url:cancel_url,booking_key:booking_key,from_page:from_page},function(result){
        $("#ajaxLogin").html(result);
        $("#ajaxLogin").attr("style","display:block;");
        addPopupScrollClass('ajaxLogin');
    }); 
}

function cancel_signin(szUrl,div_id)
{
	enableForm();
	if(div_id =='')
	{
		div_id = "ajaxLogin";
	}
	else if(div_id=='searchFormPop')
	{
		$("#openSearchPopUp").attr('value','');
	}
	if(szUrl!="")
	{
		$(location).attr('href',szUrl);
	}
	else
	{
		$("#"+div_id).attr("style","display:none;");
	}
	removePopupScrollClass(div_id);
	$('#landing_page_form :input').prop('disabled',false);
}

function userLoginPopUp(formId)
{
	if(formId!='loginForm_header')
	{
		formId = 'loginForm';
	}
	var data = $("#"+formId).serialize()+'&link=showNoError';
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_login.php",data,function(result){
		var result_ary = result.split("||||");
		if(result_ary[0]=='CURRENCY_CHANGED')
		{
			$("#Transporteca_popup").html(result_ary[1]);
			$("#ajaxLogin").attr("style","display:none;");
			$("#Transporteca_popup").attr("style","display:block;");
		}
		else
		{
			$('#landing_page_form :input').prop('disabled',true);
			$("#ajaxLogin").html(result);
			$("#ajaxLogin").attr("style","display:block;");
			$("#szEmail").focus();
		}
	});
}
function userLogin(formId)
{
	if(formId!='loginForm_header')
	{
            formId = 'loginForm';
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_login.php",$("#"+formId).serialize(),function(result){
            
            var result_ary = result.split("||||");
            if(result_ary[0]=='SUCCESS_RATE')
            {
                $("#booking-form-main-container").html(result_ary[1]);
                $("#ajaxLogin").attr("style","display:none;"); 
            }
            else if(result_ary[0]=='CURRENCY_CHANGED')
            {
                $("#Transporteca_popup").html(result_ary[1]);
                $("#ajaxLogin").attr("style","display:none;");
                $("#Transporteca_popup").attr("style","display:block;");
            }
            else
            {
                $("#ajaxLogin").html(result);
                $("#ajaxLogin").attr("style","display:block;");
            }
	});
}
function add_edit_register_shiper_consign()
{
	var value=decodeURIComponent($("#registerShipperConsigness").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/registerShipConsign.php",value,function(result){
	$("#registerShipConsign").html(result);});
}

function clear_shipper_consigness()
{
	$('INPUT:text,SELECT', '#registerShipperConsigness').val('');  
}
function get_shipper_consigness_detail(id)
{
	$("#remove").attr("style","opacity:0.4;");
	$("#remove").attr("onclick","");
	setTimeout('shipper_consigness_detail('+id+')','1500');
}

function shipper_consigness_detail(id)
{
	$.get(__JS_ONLY_SITE_BASE__+"/registerShipConsign.php",{idShipperConsigness:id,flag:'update'},function(result){
	        $("#registerShipConsign").html(result);});
}

function remove_register_shipper_consigness()
{

	$.post(__JS_ONLY_SITE_BASE__+"/registeredShipperConsigneesList.php",$("#removeShipperConsigness").serialize(),function(result){
	$("#registerShipConsignList").html(result);});
		disableForm('hsbody-2');
}

function register_shipper_consigness(flag,id)
{
	$.get(__JS_ONLY_SITE_BASE__+"/registeredShipperConsigneesList.php",{successflag:flag,idRegShipperConsigness:id},function(result){
	$("#registerShipConsignList").html(result);});
	$.get(__JS_ONLY_SITE_BASE__+"/registerShipConsign.php",function(result){
	$("#registerShipConsign").html(result);});
}

function remove_value_select_box(value)
{
	if(value=='shipper')
	{
		$("#regCon").val(''); 
	}
	else if(value=='consignes')
	{
		$("#regShipper").val(''); 
	}
	$("#remove").attr("style","opacity:1;");
	$("#remove").attr("onclick","remove_register_shipper_consigness()");
	$.get(__JS_ONLY_SITE_BASE__+"/registerShipConsign.php",function(result){
	$("#registerShipConsign").html(result);});
}

function cancel_remove_popup()
{	
	enableForm('hsbody-2');
	$("#remove_ship_con_id").attr("style","display:none;");
	removePopupScrollClass('remove_ship_con_id');
}

function remove_shipper_con_data()
{
	
	$.post(__JS_ONLY_SITE_BASE__+"/registeredShipperConsigneesList.php",$("#remove_confirm").serialize(),function(result){
	$("#registerShipConsignList").html(result);});
	enableForm('hsbody-2');
}

function get_remove_user_id()
{
	$("#remove").attr("style","opacity:1;margin-top: 121px;");
	$("#remove").attr("onclick","remove_user()");
}

function remove_user()
{
	$.post(__JS_ONLY_SITE_BASE__+"/linkedUserList.php",$("#removeUser").serialize(),function(result){
	$("#linked_user_account").html(result);});
}

function cancel_remove_user_popup(divid,id){
	
	if(id===undefined)
	{
		id="hsbody";
	}
	else
	{
		id=id;
	}
	if(divid=='delete_account_user_id')
	{
		$("#szDeleteEmail").attr('value','');
	}
	else if(divid=='mapped_user_id')
	{
		$("#szEmail").attr('value','');
	}
	enableForm(id);
	$("#"+divid).attr("style","display:none;");
	removePopupScrollClass(divid);
}

function remove_user_data()
{

	$.post(__JS_ONLY_SITE_BASE__+"/linkedUserList.php",$("#remove_confirm").serialize(),function(result){
	$("#linked_user_account").html(result);});
}

function check_email_address()
{
	$.post(__JS_ONLY_SITE_BASE__+"/mappedUsers.php",$("#mappedUser").serialize(),function(result){
	$("#map_user_account").html(result);});
}

function confirm_mapped_user()
{
    $.post(__JS_ONLY_SITE_BASE__+"/mappedUsers.php",$("#linkedUser").serialize(),function(result){

        var result_ary = result.split("||||");
        if($.trim(result_ary[0])=='SUCCESS')
        {
            $("#linked_user_account").html(result_ary[1]);
            $("#map_user_account").html(result_ary[2]);
        }
        else
        {
            $("#map_user_account").html(result);
        }	
    });
} 

function succcess_user_mapped(flag)
{
    $.get(__JS_ONLY_SITE_BASE__+"/linkedUserList.php",{successflag:flag},function(result){
        $("#linked_user_account").html(result);
    });

    $.get(__JS_ONLY_SITE_BASE__+"/mappedUsers.php",function(result){
        $("#map_user_account").html(result);
    });
}

function cancel_signin_login(szUrl)
{
    $(location).attr('href',szUrl);
}

function signup_forwarder()
{
    var value=decodeURIComponent($("#forwarderSignup").serialize());
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderSignup.php",value,function(result){
	$("#forwardersignup").html(result);
    });
} 
function delete_user_account()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_deleteUserAccount.php",$("#deleteUserAccount").serialize(),function(result){
        $("#delete_user_account").html(result);
    });
}

function delete_account_user()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_deleteUserAccount.php",$("#delete_account").serialize(),function(result){
        $("#delete_user_account").html(result);
    });
}

function redirect_url(page_url)
{
    if(page_url!='')
    {
        $(location).attr('href',page_url);
    }
    else
    {
        return false ;
    }
}

function clear_search_result(page_url,user_id,login_flag)
{
    var szPageUrl = $("#redirect_abandon_popup_url").attr('value');
    var login_flag = $("#login_flag").attr('value',login_flag);

    if(szPageUrl!='')
    {
        page_url = szPageUrl ;
    }

    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",{operation:'CLEAR_SEARCH'},function(result){

        var user_id=parseInt(user_id);
        if(user_id>0 || login_flag!='booking')
        {
            $(location).attr('href',page_url);
        }
        else
        {
            $("#leave_page_div").attr('style','display:none;'); 
            // calling login popup if user click cancel on this popup we redirect him to the landing page
            ajaxLogin(page_url,1)
        }	
    });
} 

function check_login_password_submit_key_press(kEvent,login_flag)
{
    if(kEvent.keyCode==13)
    {
        if(login_flag==1)
        {
            userBookingLogin();
        }
        else
        {
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_login.php",$("#loginForm").serialize(),function(result){
                var result_ary = result.split("||||");
                if(result_ary[0]=='SUCCESS_RATE')
                {
                    $("#booking-form-main-container").html(result_ary[1]);
                    $("#ajaxLogin").attr("style","display:none;"); 
                }
                else if(result_ary[0]=='CURRENCY_CHANGED')
                {
                    $("#Transporteca_popup").html(result_ary[1]);
                    $("#ajaxLogin").attr("style","display:none;");
                    $("#Transporteca_popup").attr("style","display:block;");
                }
                else
                {
                    $("#ajaxLogin").html(result);
                    $("#ajaxLogin").attr("style","display:block;");
                }
            });
        }	
    }
}

function search_select_service(searched_by)
{
	$("#loader").attr('style','display:block;');
	if(searched_by!='')
	{
		$("#searched_by").attr('value',searched_by);
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_searchServices.php",$("#select_service_search").serialize(),function(result){
		result_ary = result.split("||||");
		$("#loader").attr('style','display:none;');
		if(result_ary[0]=='SUCCESS')
		{
			$("#sevice_container").html(result_ary[1]);
			/*
			if(result_ary[2]!='')
			{
				$("#left_slider_div").html(result_ary[2]);
			}
			*/	
		}
		else
		{
			$("#customs_clearance_pop1").html(result_ary[1]);
			$("#customs_clearance_pop1").attr('style','display:block;');
		}
		$("#loader").attr('style','display:none;');
	});
}

function open_help_me_select_popup()
{	
	//addPopupScrollClass();
	var szPostCode = $("#szOriginPostCode").attr('value');
	var szAddressLine1 = $("#szAddressLine1").attr('value');	
	var szAddressLine2 = $("#szAddressLine2").attr('value');	
	var szAddressLine3 = $("#szAddressLine3").attr('value');	
	var szCity = $("#szOriginCity").attr('value');	
	var szState = $("#szState").attr('value');	
	var szCountry = $("#szCountry").attr('value');	
	var szPostCode = $("#szOriginPostCode").attr('value');	
	
	var szLatitude = $("#szLatitude").attr('value');	
	var szLongitude = $("#szLongitude").attr('value');	
	
	$("#szPostCode_hidden").attr('value',szPostCode);
	$("#szAddressLine1_hidden").attr('value',szAddressLine1);
	$("#szAddressLine2_hidden").attr('value',szAddressLine2);
	$("#szAddressLine3_hidden").attr('value',szAddressLine3);
	$("#szCity_hidden").attr('value',szCity);
	$("#szState_hidden").attr('value',szState);
	$("#szCountry_hidden").attr('value',szCountry);	
	
	$("#szLatitude_hidden").attr('value',szLatitude);
	$("#szLongitude_hidden").attr('value',szLongitude);	
	
	$('#google_map_hidden_form').submit();
	
	$("#popup_container_google_map").attr('style','display:block;');
	$("body").attr("id","mainClass");	
	addPopupScrollClass('popup_container_google_map'); 
}
function submit_booking_form(idWTW,idUser)
{
    $('#booking_ford_'+idWTW).submit();
}
function show_signin_popup(redirect_url)
{
	$("#Transportation_pop").attr('style','display:none;');
	addPopupScrollClass();
	ajaxLogin(redirect_url);
}

function search_mybooking()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myBooking.php",$("#searchMyBooking").serialize(),function(result){
	$("#mybooking_list").html(result);});
}

function mybooking_change_tab(flag)
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_myBooking.php",{showBookingFlag:flag},function(result){
	$("#mybooking_list").html(result); 
        
        $(".booking_left_nav").removeClass('active');
        $("#booking_left_nav_"+flag).addClass('active');
    });
    
	/*
	//$.get(__JS_ONLY_SITE_BASE__+"/bookingLeftNav.php",{showBookingFlag:flag},function(result){
	//$("#mybooking_left").html(result);});
	*/
	/*
	var title;
	if(flag=='active')
	{
		title='Transporteca | Confirmed Bookings';
	}
	else if(flag=='hold')
	{
		title='Transporteca | Bookings on Hold';
	}
	else if(flag=='draft')
	{
		title='Transporteca | Draft Bookings';
	}
	else if(flag=='archive')
	{
		title='Transporteca | Archived Bookings';
	}
	
	document.getElementById("metatitle").innerHTML=title;
	*/
	
}

function change_booking_status(idBooking,flag,showBookingFlag)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myBooking.php",{idBooking:idBooking,flag:flag,showBookingFlag:showBookingFlag},function(result){
	$("#mybooking_list").html(result);
	$("#booking_expire_div").attr('style','display:none;');	
	$("#change_price_div").attr('style','display:none;');	
	});
}

function my_booking_left_menu(flag)
{
	$.get(__JS_ONLY_SITE_BASE__+"/bookingLeftNav.php",{showBookingFlag:flag},function(result){
	$("#mybooking_left").html(result);});
}

function search_mybooking()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myBooking.php",$("#searchMyBooking").serialize(),function(result){
	$("#mybooking_list").html(result);});
}

function tranport_eca_overlay(callBackObject)
{
	var docHeight = $(document).height();
    $("body").append("<div id='overlay'></div>");
    $("#overlay")
      .height(docHeight)
      .css({
         'opacity' : 0.4,
         'position': 'absolute',
         'top': 0,
         'left': 0,
         'background-color': 'black',
         'width': '100%',
         'z-index': '5000'
    });
    $("#overlay").click( function()
    {
        if( typeof( callBackObject ) != "undefined" )
        {
            if( typeof( callBackObject.closeDivID ) != "undefined"  )
            {
                if( $('#'+callBackObject.closeDivID).length > 0 )
                {
                    toggle_visibility_hide(callBackObject.closeDivID);
                }
            }
            else if ( typeof( callBackObject.callback ) != "undefined" )
            {
                var args;
                if( ( typeof( callBackObject.arguments ) == "object" ) && ( typeof( callBackObject.arguments ) != undefined ) )
                {
                    args = Array.prototype.slice.call(callBackObject.arguments, 0);
                }

                var namespaces = callBackObject.callback.split(".");
                var func = namespaces.pop();
                var context;
                if( typeof( callBackObject.context ) != undefined )
                {
                    context = callBackObject.context;
                }
                else
                {
                    context = window;
                }

                for (var i = 0; i < namespaces.length; i++) {
                    context = context[namespaces[i]];
                }
                return window[func].apply(window, args);
            }
        }
    });
}

function submit_currency_popup()
{
	var szPageName=$("#szPageName").val();
	var value=$("#currency_popup_form").serialize();
	var newvalue=value+"&szPageNameValue="+szPageName;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_selectCurrency.php",newvalue,function(result){
		$("#popup-container").html(result);	
		addPopupScrollClass('popup-container');
	});
}

function booking_rating(idBooking,lang)
{
    divid="rating_"+idBooking;
    var disp = $("#"+divid).css('display');
    if(disp=='block')
    {
        $("#"+divid).attr('style','display:none;');
    }
    else
    {
        $("#"+divid).attr('style','display:block;');
        $.get(__JS_ONLY_SITE_BASE__+"/bookingRating.php",{idBooking:idBooking,lang:lang},function(result){
            $("#"+divid).html(result);
        });
    }
}

function submit_rating_form(idBooking)
{
    $("#submit_rating_button").attr("onclick","");
    $("#submit_rating_button").attr("style","opacity:0.4");
    
	divid="rating_"+idBooking;
	formid="ratingForm_"+idBooking;
	$.post(__JS_ONLY_SITE_BASE__+"/bookingRating.php",$("#"+formid).serialize(),function(result){
            $("#"+divid).html(result);
	});
}
function clear_form(idBooking)
{
	divid="rating_"+idBooking;
	$.get(__JS_ONLY_SITE_BASE__+"/bookingRating.php",{idBooking:idBooking},function(result){
		$("#"+divid).html(result);});
}

function show_booking_list(flag)
{
	var flag=$("#showBookingFlag").attr('value');
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myBooking.php",{showBookingFlag:flag},function(result){
	$("#mybooking_list").html(result);});
}


function booking_rating(idBooking,lang)
{
	divid="rating_"+idBooking;
	var disp = $("#"+divid).css('display');
	if(disp=='block')
	{
            $("#"+divid).attr('style','display:none;');
	}
	else
	{
            var div_id_new="iTimely_"+idBooking;
            $("#"+divid).attr('style','display:block;');
            $.get(__JS_ONLY_SITE_BASE__+"/bookingRating.php",{idBooking:idBooking,lang:lang},function(result){
            $("#"+divid).html(result);
            //$("#iTimely").focus();
                setTimeout("$('#"+div_id_new+"').focus()", 20); 
            });
	}
}

function show_booking_rating_details(idBooking,id)
{
	divid="rating_"+idBooking;
	var disp = $("#"+divid).css('display');
	if(disp=='block')
	{
		$("#"+divid).attr('style','display:none;');
	}
	else
	{
		var div_id_new="booking_rating_detail_id_"+idBooking;
		$.get(__JS_ONLY_SITE_BASE__+"/bookingRating.php",{idBooking:idBooking,idRating:id,showFlag:'details'},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style','display:block;');
		//setTimeout("$('#"+div_id_new+"').focus()", 50); 
		});
	}
}

function collapse(idBooking)
{
	divid="rating_"+idBooking;
	$("#"+divid).attr('style','display:none;');
	$.get(__JS_ONLY_SITE_BASE__+"/bookingRating.php",{idBooking:idBooking},function(result){
	$("#"+divid).html(result);});
}

function show_verifed_open_popup()
{
	$("#non_verifed").attr('style','display:block;');
}

function show_verifed_close_popup()
{
	$("#non_verifed").attr('style','display:none;');
}

function open_benefit_popup(div_id,redirect_url,mode)
{
	$("#"+div_id).attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/benefitsSigningPpopup.php",{redirect_url:redirect_url,mode:mode},function(result){
		$("#"+div_id).html(result);
	});
}

function close_benefit_popup(redirect_url,mode)
{	if(redirect_url=='transportation')
	{	
		$('#Transportation_pop').css('display','none');
		removePopupScrollClass('Transportation_pop');
	}
	else if(mode=='CLICKED_ON_BOOK')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
		displayLoginForm(szBookingRandomNum)
	}
	else
	{
		$("#customs_clearance_pop1").attr('style','display:none;');
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_login.php",{redirect_url:redirect_url},function(result){
			$("#ajaxLogin").html(result);
		});
	}
}

function open_forgot_passward_popup(div_id,redirect_url,mode,from_page)
{	 
        var szEmail='';
       if($("#szEmail").length>0)
       {
          szEmail = $("#szEmail").attr('value');
       }
	$.get(__JS_ONLY_SITE_BASE__+"/forgotPassword.php",{redirect_url:redirect_url,mode:mode,from_page:from_page,szEmail:szEmail},function(result){
		$("#"+div_id).html(result);
		$("#"+div_id).attr('style','display:block;');
		$("#szEmail").focus();
		addPopupScrollClass(div_id);
	});
}

function forgot_passward_popup(div_id)
{ 
	$.post(__JS_ONLY_SITE_BASE__+"/forgotPassword.php",$("#forgotPassword").serialize(),function(result){
		if(div_id!='' && div_id!='undefined')
		{
			$("#"+div_id).html(result);
		}
		else
		{
			$("#ajaxLogin").html(result);
		}	
		$("#szEmail").focus();
	});
}

function customer_forwarder_rating(value,flag,msg_rate,idBooking)
{
	/*$.get(__JS_ONLY_SITE_BASE__+"/ajax_rating.php",{rating:value,flag:flag},function(result){
	$("#forwader_ratings").html(result);});*/
	//var i=1;
	var removevalue=5-value;
	if(flag=='select')
	{		
		if(value!='')
		{
			for(var i=1;i<=value;++i)
			{
				divid="booking_star_"+i+"_"+idBooking;
				$("#"+divid).attr('class','RatingBoxAct1');
				$("#"+divid).attr('onmouseout','');
			}
		}
		if(removevalue>0)
		{
			for(var j=5;j>value;--j)
			{
				divid="booking_star_"+j+"_"+idBooking;
				$("#"+divid).attr('class','RatingBox1');
			}
		}
		
		$("#iRating_"+idBooking).attr('value',value);	
		
		
		var msg=value+" ("+msg_rate+")";
		var divid="show_rating_msg_"+idBooking;
		$("#"+divid).html(msg); 
		$("#iRating_msg_"+idBooking).attr('value',msg);	
	}
	else if(flag=='mouse_over')
	{	
		if(value!='')
		{
			
			for(var i=1;i<=value;++i)
			{
				divid="booking_star_"+i+"_"+idBooking;
				$("#"+divid).attr('class','RatingBoxAct1');
				$("#"+divid).attr('onmouseout','javascript:customer_forwarder_rating("0","","","'+idBooking+'");');
			}
		}
		if(removevalue>0)
		{
			for(var j=5;j>value;--j)
			{
				divid="booking_star_"+j+"_"+idBooking;
				$("#"+divid).attr('class','RatingBox1');
			}
			
		}
		
		var msg=value+" ("+msg_rate+")";
		var divid="show_rating_msg_"+idBooking;
		$("#"+divid).html(msg); 
	}
	else
	{
		for(var j=5;j>value;--j)
		{
			divid="booking_star_"+j+"_"+idBooking;
			$("#"+divid).attr('class','RatingBox1');
		}
		var divid="show_rating_msg_"+idBooking;
		$("#"+divid).html(''); 
		
		var divvalue="iRating_"+idBooking;
		var rat_value=$("#"+divvalue).attr('value');
		if(rat_value>0)
		{
			for(var i=1;i<=rat_value;++i)
			{
				divid="booking_star_"+i+"_"+idBooking;
				$("#"+divid).attr('class','RatingBoxAct1');
				$("#"+divid).attr('onmouseout','');
			}
			value=rat_value;
			var divid="show_rating_msg_"+idBooking;
			var rat_value_msg=$("#iRating_msg_"+idBooking).attr('value');
			$("#"+divid).html(rat_value_msg); 
		} 
	} 
} 
function submit_service_booking(idWTW,booking_url,idUser,new_booking_page)
{
	$("#loader").attr('style','display:block;');
	$("#customs_clearance_pop1").attr('style','display:none;');
	var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
		  
 	if(new_booking_page==1)
 	{
            toggleServiceList(idWTW,booking_url,idUser,new_booking_page,1);
 	} 
 	var site_language = $("#iSiteLanguageFooter_hidden").val();
        
 	$("#loader").attr('style','display:none;');
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php?lang="+site_language,{idWTW:idWTW,szBookingRandomNum:szBookingRandomNum,new_booking_page:new_booking_page},function(result){
		
		result_ary = result.split("||||");
	 
		$("#loader").attr('style','display:none;');
		if(result_ary[0]=='SUCCESS')
		{
                    $("#loader").attr('style','display:none;');
                    if(idUser>0 || new_booking_page==1)
                    {
                            var page_url = booking_url +"/"+szBookingRandomNum+"/";
                            redirect_url(page_url);
                    }
                    else
                    {
                            addPopupScrollClass();
                            displayLoginForm(szBookingRandomNum);
                    }	
		}
		else if(result_ary[0]=='REDIRECT')
		{
                    var page_url = booking_url +"/"+szBookingRandomNum+"/"; 
                    redirect_url(page_url);
		}
		else if(result_ary[0]=='UPDATE_CARGO')
		{
                    $("#loader").attr('style','display:none;');
                    if(idUser>0)
                    {
                        update_booking_cargo_details(szBookingRandomNum);
                    }
                    else
                    {
                        addPopupScrollClass();
                        displayLoginForm(szBookingRandomNum);
                    }	 
		}	
		else if(result_ary[0]=='DISPLAY_BOOKING_FIELDS')
		{  /*
                    $("#service-listing-container").attr("style",'display:none;');
                    $("#selected-service-listing-container").html(result_ary[1]);  

                    $("#selected-service-listing-container").attr("style",'display:block');
                    $("#booking_details_container").attr("style",'display:block;'); 
                    $('html, body').animate({ scrollTop: ($("#selected-service-listing-container").offset().top - 10) }, "100000");

                    $("#szShipperCompanyName").focus();			
                    var $thisVal = $("#szShipperCompanyName").val();
                    $("#szShipperCompanyName").val('').val($thisVal);  */
		}
	});
}

function toggleCourierServiceList(idWTW,booking_url,idUser,new_booking_page,display_flag)
{
    if(display_flag==1)
    { 
        $(".search-result-show-hide-div").attr("style",'display:none;'); 	

        $("#hidden-search-service-book-now-button_"+idWTW).attr('style','display:inline-block;');
        $("#search-service-book-now-button_"+idWTW).attr('style','display:none;'); 
        $("#select_services_tr_"+idWTW).attr("style",'display:block;');

        $("#showBookingFieldsFlag").attr('value','0');

        $("#booking_details_container").attr("style",'display:none;'); 
        $("#update_cargo_details_container_div").attr("style",'display:block;'); 
        
        $("#iLength1").focus();			
        var $thisVal = $("#iLength1").val();
        $("#iLength1").val('').val($thisVal);

        $("#szServiceUniqueKey").val(idWTW);
        
        if($("#see-more-button-link").length)
        {
            $("#see-more-button-link").attr('style','display:none;'); 
        }
        
       // $('html, body').animate({ scrollTop: ($("#select_services_tr_"+idWTW).offset().top - 30) }, "100000");  
    }
    else
    {
        $("#selected-service-listing-container").attr("style",'display:none;');
        $("#booking_details_container").attr("style",'display:none;'); 
        $("#update_cargo_details_container_div").attr("style",'display:none;'); 
         
        $("#service-listing-container").attr("style",'display:block;'); 
        $("#showBookingFieldsFlag").attr('value','0');
        $(".search-result-show-hide-div").attr("style",'display:block;'); 	 
        $("#hidden-search-service-book-now-button_"+idWTW).attr('style','display:none;');
        $("#search-service-book-now-button_"+idWTW).attr('style','display:inline-block;');
        //$('html, body').animate({ scrollTop: (0) }, "100000");

         $("#szServiceUniqueKey").val('');
         
         if($("#see-more-button-link").length)
        {
            $("#see-more-button-link").attr('style','display:block;text-align:center;'); 
        }
    }  
}

function toggleServiceList(idWTW,booking_url,idUser,new_booking_page,display_flag,courier_flag)
{
    if(courier_flag==1)
    {
        toggleCourierServiceList(idWTW,booking_url,idUser,new_booking_page,display_flag);
    }
    else if(display_flag==1)
    { 
        $(".search-result-show-hide-div").attr("style",'display:none;'); 	

        $("#hidden-search-service-book-now-button_"+idWTW).attr('style','display:inline-block;');
        $("#search-service-book-now-button_"+idWTW).attr('style','display:none;'); 
        $("#select_services_tr_"+idWTW).attr("style",'display:block;'); 
        $("#showBookingFieldsFlag").attr('value','1');

        $("#booking_details_container").attr("style",'display:block;'); 
        var iTop = 0;
        if($("#select_services_tr_"+idWTW).length)
        {
            iTop = $("#select_services_tr_"+idWTW).offset().top
        }
        $('html, body').animate({ scrollTop: (iTop - 30) }, "100000");

        $("#szShipperCompanyName").focus();			
        var $thisVal = $("#szShipperCompanyName").val();
        $("#szShipperCompanyName").val('').val($thisVal); 
        $("#szServiceUniqueKey").val(idWTW); 
        
        if($("#see-more-button-link").length)
        {
            $("#see-more-button-link").attr('style','display:none;'); 
        }
    }
    else
    {
        $("#selected-service-listing-container").attr("style",'display:none;');
        $("#booking_details_container").attr("style",'display:none;'); 
        $("#service-listing-container").attr("style",'display:block;'); 
        $("#showBookingFieldsFlag").attr('value','0');
        $(".search-result-show-hide-div").attr("style",'display:block;'); 	 
        $("#hidden-search-service-book-now-button_"+idWTW).attr('style','display:none;');
        $("#search-service-book-now-button_"+idWTW).attr('style','display:inline-block;');
        $('html, body').animate({ scrollTop: (0) }, "100000");

        $("#szServiceUniqueKey").val('');
        if($("#see-more-button-link").length)
        {
            $("#see-more-button-link").attr('style','display:block;text-align:center;'); 
        }
    }  
}
function update_booking_cargo_details(szBookingRandomNum,mode,packing_type)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_updateCargoDetails.php",{booking_random_num:szBookingRandomNum,mode:mode,packing_type:packing_type},function(result){
		
            $("#update_cargo_details").html(result);
            $("#all_available_service").attr('style','display:none');
            $("#change_price_div").attr('style','display:none');
            $("#update_cargo_details").attr('style','display:block;');
            $("#iLength1").focus().selectionEnd;
	});
}
function convert_time_to_date(iDateTime)
{
	// converting iDateTime into milliseconds 
	var miliseconds = parseInt(iDateTime)*1000;
	var d = new Date(miliseconds); 
	
	/*
	var date=d.getDate() ;
	var month=d.getMonth();
	var year=d.getFullYear() ;
	*/
	
	var date_string = d.toUTCString('');
	var res_date = date_string.split(" ");
	var resdate;
	
	if(res_date[2]=='Jan')
	{
		resdate='01';
	}
	else if(res_date[2]=='Feb')
	{
		resdate='02';
	}
	else if(res_date[2]=='Mar')
	{
		resdate='03';
	}
	else if(res_date[2]=='Apr')
	{
		resdate='04';
	}
	else if(res_date[2]=='May')
	{
		resdate='05';
	}
	else if(res_date[2]=='Jun')
	{
		resdate='06';
	}
	else if(res_date[2]=='Jul')
	{
		resdate='07';
	}
	else if(res_date[2]=='Aug')
	{
		resdate='08';
	}
	else if(res_date[2]=='Sep')
	{
		resdate='09';
	}
	else if(res_date[2]=='Oct')
	{
		resdate='10';
	}
	else if(res_date[2]=='Nov')
	{
		resdate='11';
	}
	else if(res_date[2]=='Dec')
	{
		resdate='12';
	}
	return res_date[1]+"/"+resdate+"/"+res_date[3] ;
}

function convert_time_to_date_2(iDateTime1,iDateTime2)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{iDateTime1:iDateTime1,iDateTime2:iDateTime2,type:'CONVERT_DATE'},function(result){
		return result;
	});
}
function disableForm(divid)
{
	if(divid!='')
	{
		divid=divid;
	}
	else
	{
		divid="hsbody";
	}
	$('#'+divid+' :input').attr('disabled', true);	
	//$('#'+divid+' a').click(function(e) { e.preventDefault(); });
}

function enableForm(divid)
{
	if(divid!='')
	{
		divid=divid;
	}
	else
	{
		divid="hsbody";
	}
	$('#'+divid+' :input').attr('disabled', false);
	//$('#'+divid+' a').click(function(e) { return true;});
}
function displayLoginForm(szBookingRandomNum)
{
	$.post(__JS_ONLY_SITE_BASE__+"/login.php",{szBookingRandomNum:szBookingRandomNum},function(result){
		$("#customs_clearance_pop1").html(result);
		$("#customs_clearance_pop1").attr('style','display:block;');
		var cookie_name = getCookie('__USER_EMAIL_COOKIE__');
		if(jQuery.trim(cookie_name)=='' || jQuery.trim(cookie_name)== 'undefined')
		{
			$("#iExistingUser").removeAttr('checked');
			$("#iNewUser").attr('checked','checked');
			$("#szNewEmail").focus().selectionEnd;
		}
		else
		{
			$("#iNewUser").removeAttr('checked');
			$("#iExistingUser").attr('checked','checked');
			$("#szExistingUserPassword").focus().selectionEnd;
		}
	});
}

function userBookingLogin()
{
	$.post(__JS_ONLY_SITE_BASE__+"/login.php",$("#bookingLoginForm").serialize(),function(result){		
		result_ary = result.split("||||");
		
		if(result_ary[0]=='SUCCESS')
		{
			$("#customs_clearance_pop1").attr('style','display:none;');
			redirect_url(result_ary[1]);
		}
		else if(result_ary[0]=='CURRENCY_CHANGED')
		{
			$("#customs_clearance_pop1").attr('style','display:none;');
			$("#Transporteca_popup").html(result_ary[1]);
			$("#Transporteca_popup").attr("style","display:block;");
		}
		else if(result_ary[0]=='UPDATE_CARGO')
		{
			var szBookingRandomNum = result_ary[1] ; 
			$("#customs_clearance_pop1").attr('style','display:none;');
			update_header_text();
			search_select_service();
			update_booking_cargo_details(szBookingRandomNum);
		}
		else
		{
			$("#customs_clearance_pop1").html(result);
			$("#customs_clearance_pop1").attr('style','display:block;');
		}		
	});
}

function checkNum(data) {       
var valid = "0123456789.";     
var ok = 1; var checktemp;
for (var i=0; i<data.length; i++) {
checktemp = "" + data.substring(i, i+1);
if (valid.indexOf(checktemp) == "-1") return 0; }
return 1;
}
function dollarAmount(value) { 
Num = value;
dec = Num.indexOf(".");
end = ((dec > -1) ? "" + Num.substring(dec,Num.length) : "");
Num = "" + parseInt(Num);
var temp1 = "";
var temp2 = "";
if (checkNum(Num) == 0) {
temp2='0';
}
else { 
if (end.length == 2) end += "0";
if (end.length == 1) end += "00";
if (end == "") end += "";
var count = 0;
for (var k = Num.length-1; k >= 0; k--) {
var oneChar = Num.charAt(k);
if (count == 3) {
temp1 += ",";
temp1 += oneChar;
count = 1;
continue;
}
else {
temp1 += oneChar;
count ++;
}
}
for (var k = temp1.length-1; k >= 0; k--) {
var oneChar = temp1.charAt(k);
temp2 += oneChar;
}
temp2 = temp2 + end;
return temp2;
}
}

function toggle_pick_address(szPostCode,szCity,idCountry,default_city,default_postcode,default_postcode2)
{
	$("#loader").attr('style','display:block;');
	var cb_check=$("#iPickupAddress_hidden").attr('value');	
	if(cb_check==1)
	{
		 $.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{szPostCode:szPostCode,szCity:szCity,idCountry:idCountry,iShowByPostCode:'1',type:'SHIPPER_DROPDOWN'},function(result){			
			
			$("#registered_shipper_drop_down").html(result);	
			$("#iPickupAddress_hidden").attr('value','0');			
			$("#idShipperCountry").removeAttr('onfocus');
			$("#idShipperCountry").removeAttr('disabled');			
			$("#szShipperCity").removeAttr('disabled');
			$("#szShipperPostcode").removeAttr('disabled');	
			
			$("#szShipperCity").attr('value','');
			$("#szShipperPostcode").attr('value','');	
			
			$("#szShipperCity_hidden").attr('value','');
			$("#szShipperPostcode_hidden").attr('value','');
					
			$("#pickup_address_div").attr('style','display:block;');
			$("#shipper_consignee_label").attr('style','display:block;');			
			$("#loader").attr('style','display:none;');
			
			$("#iAddRegistShipper").removeAttr('checked');
			$("#iAddRegistShipper").removeAttr('disabled');
			var idServiceType = $("#szTransportation").attr('value');
			change_postcode_city_default_text_booking_details(idServiceType,default_city,default_postcode,default_postcode2);
			
		});
	}
	else
	{
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{szPostCode:szPostCode,szCity:szCity,idCountry:idCountry,iShowByPostCode:'2',type:'SHIPPER_DROPDOWN'},function(result){			
			
			$("#registered_shipper_drop_down").html(result);
			
			var szShipperCity = $("#szShipperCity_pickup").attr('value');
			var idShipperCountry = $("#idShipperCountry_pickup").attr('value');
			var szShipperPostcode = $("#szShipperPostcode_pickup").attr('value');
			
			$("#idShipperCountry").attr('value',idShipperCountry);					
			$("#szShipperCity").attr('value',szShipperCity);
			$("#szShipperPostcode").attr('value',szShipperPostcode);	
			
			$("#szShipperCity_hidden").attr('value',szShipperCity);
			$("#szShipperPostcode_hidden").attr('value',szShipperPostcode);
			
			$("#szShipperCity").attr('style','');
			$("#szShipperPostcode").attr('style','');	
			
			$("#iPickupAddress_hidden").attr('value','1') ;			
			$("#idShipperCountry").attr('onfocus',"this.blur();");
			$("#idShipperCountry").attr('disabled',"disabled");					
			$("#szShipperCity").attr('disabled','disabled');
			$("#szShipperPostcode").attr('disabled','disabled');			
			$("#pickup_address_div").attr('style','display:none;');
			var pickup_style = $("#iDeliveryAddress_hidden").attr('value') ;
			if(pickup_style==1)
			{
				$("#shipper_consignee_label").attr('style','display:none;');
			}
			//$("#shipper_consignee_label").attr('style','display:none;');
			$("#loader").attr('style','display:none;');
			
			$("#iAddRegistShipper").removeAttr('checked');
			$("#iAddRegistShipper").removeAttr('disabled');
			
			var idServiceType = $("#szTransportation").attr('value');
			change_postcode_city_default_text_booking_details(idServiceType,default_city,default_postcode,default_postcode2);
		});
	}
}

function toggle_delivery_address(szPostCode,szCity,idCountry,default_city,default_postcode,default_postcode2)
{
	$("#loader").attr('style','display:block;');
	var cb_check=$("#iDeliveryAddress_hidden").attr('value');	
	if(cb_check==1)
	{
		 $.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{szPostCode:szPostCode,szCity:szCity,idCountry:idCountry,iShowByPostCode:'1',type:'CONSIGNEE_DROPDOWN'},function(result){			
			
			$("#registered_consignee_drop_down").html(result);			
			$("#iDeliveryAddress_hidden").attr('value','0') ;
		
			$("#idConsigneeCountry").removeAttr('onfocus');	
			$("#idConsigneeCountry").removeAttr('disabled');		
			$("#szConsigneeCity").removeAttr('disabled');
			$("#szConsigneePostcode").removeAttr('disabled');
			
			$("#szConsigneeCity").attr('value','');
			$("#szConsigneePostcode").attr('value','');
			
			$("#szConsigneeCity_hidden").attr('value','');
			$("#szConsigneePostcode_hidden").attr('value','');
			
			$("#delivery_address_div").attr('style','display:block;');
			$("#shipper_consignee_label").attr('style','display:block;');
			$("#loader").attr('style','display:none;');
			
			$("#iAddRegistConsignee").removeAttr('checked');
			$("#iAddRegistConsignee").removeAttr('disabled');
			
			var idServiceType = $("#szTransportation").attr('value');
			change_postcode_city_default_text_booking_details(idServiceType,default_city,default_postcode,default_postcode2);
			
		});
	}
	else
	{
		 $.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{szPostCode:szPostCode,szCity:szCity,idCountry:idCountry,iShowByPostCode:'2',type:'CONSIGNEE_DROPDOWN'},function(result){			
			
			$("#registered_consignee_drop_down").html(result);	
			
			var szConsigneeCity = $("#szConsigneeCity_pickup").attr('value');
			var idConsigneeCountry = $("#idConsigneeCountry_pickup").attr('value');
			var szConsigneePostcode = $("#szConsigneePostcode_pickup").attr('value');
			
			$("#idConsigneeCountry").attr('value',idConsigneeCountry);					
			$("#szConsigneeCity").attr('value',szConsigneeCity);
			$("#szConsigneePostcode").attr('value',szConsigneePostcode);	
			
			$("#szConsigneeCity").attr('style','');
			$("#szConsigneePostcode").attr('style','');	
			
			$("#szConsigneeCity_hidden").attr('value',szConsigneeCity);
			$("#szConsigneePostcode_hidden").attr('value',szConsigneePostcode);
			
			$("#iDeliveryAddress_hidden").attr('value','1') ;			
			$("#idConsigneeCountry").attr('onfocus',"this.blur();");
			$("#idConsigneeCountry").attr('disabled',"disabled");		
			$("#szConsigneeCity").attr('disabled','disabled');
			$("#szConsigneePostcode").attr('disabled','disabled');
			
			$("#delivery_address_div").attr('style','display:none;');
			var pickup_style = $("#iPickupAddress_hidden").attr('value') ;
			if(pickup_style==1)
			{
				$("#shipper_consignee_label").attr('style','display:none;');
			}
			$("#loader").attr('style','display:none;');
			
			$("#iAddRegistConsignee").removeAttr('checked');
			$("#iAddRegistConsignee").removeAttr('disabled');
			
			var idServiceType = $("#szTransportation").attr('value');
			change_postcode_city_default_text_booking_details(idServiceType,default_city,default_postcode,default_postcode2);
			
		});
	}
}
function updatePostcodeRegion()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#multiregion_select_form").serialize(),function(result){
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			submit_multi_region_city('landing_page_form',result_ary[1],result_ary[2],result_ary[3]);
		}	
	});
}
function updatePostcodeRegion_tryitnow()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#multiregion_select_form").serialize(),function(result){
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#regError").attr('style','display:none;');
	      	$("#Transportation_pop").attr('style','display:none;');
	       	$("#bottom_search_result").html(result_ary[1]);
	       	$("#bottom_search_result").attr('style','display:block;');
		}
		else if(jQuery.trim(result_ary[0])=='ERROR')
		{
			$("#popupError").html(result_ary[1]);
			$("#popupError").attr('style','display:block;');			
		}	
	});
}
function display_calc_details(idWTW)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_displayCalculationDetails.php",{idWTW:idWTW},function(result){
		
		$("#customs_clearance_pop1").html(result);
		$("#customs_clearance_pop1").attr('style','display:block;');
	});
}
function auto_fill_shipper_consignee(idShipperConsignee,type,div_id)
{
	$("#loader").attr('style','display:block;');
	var idServiceType = $("#szTransportation").attr('value');
	var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	if(idShipperConsignee=='New')
	{
		 $.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{idShipperConsignee:idShipperConsignee,type:type,idServiceType:idServiceType,szBookingRandomNum:szBookingRandomNum},function(result){			
			$("#"+div_id).html(result);
			if(type=='SHIPPER')
			{
				$("#iAddRegistShipper").removeAttr("disabled");
			}
			else
			{
				$("#iAddRegistConsignee").removeAttr("disabled");
			}	
			$("#loader").attr('style','display:none;');
		});
	}
	else
	{
		
		if(type=='SHIPPER')
		{
			$("#iAddRegistShipper").attr("disabled", true);
			$("#iAddRegistShipper").removeAttr("checked");
			var hidden_value = $("#iPickupAddress_hidden").attr("value");
		}
		else
		{
			$("#iAddRegistConsignee").attr("disabled", true);
			$("#iAddRegistConsignee").removeAttr("checked");
			var hidden_value = $("#iDeliveryAddress_hidden").attr("value");
		}	
		
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{idShipperConsignee:idShipperConsignee,type:type,idServiceType:idServiceType,visibility:hidden_value,szBookingRandomNum:szBookingRandomNum},function(result){			
			$("#"+div_id).html(result);
			$("#loader").attr('style','display:none;');
		});
	}	
}
function resume_booking(idBooking)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'RESUME_BOOKING',idBooking:idBooking},function(result){
		res_ary=result.split("||||") ;
			if(res_ary[0]=='SUCCESS')
			{
				redirect_url(res_ary[1]);
			}
			else
			{
				
			}	
		});
}
function repeat_booking(idBooking)
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'REPEAT_BOOKING',idBooking:idBooking},function(result){
        res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        {
            redirect_url(res_ary[1]);
        }
    });
}
function repeat_active_booking(idBooking)
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'REPEAT_ACTIVE_BOOKING',idBooking:idBooking},function(result){
        res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        {
            redirect_url(res_ary[1]);
        }
    });
}
function openForwarderRatingPopUp(id)
{
	$.get(__JS_ONLY_SITE_BASE__+"/forwarderRatingPopUp.php",{idForwarder:id},function(result){
		
		$("#rating_popup").html(result);
		$("#rating_popup").attr('style','display:block;');
		addPopupScrollClass('rating_popup');
	});
}

function showUserComment(id,ratevalue)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_ForwarderComments.php",{idForwarder:id,rateValue:ratevalue},function(result){
		
		$("#user_comment").html(result);
	});
}

function open_contact_popup()
{	
	//
	//disableForm('hsbody');
	
	$.get(__JS_ONLY_SITE_BASE__+"/contactPopUp.php",function(result){
		
		$("#contactPopup").html(result);
		$("#contactPopup").attr('style','display:block;');
		addPopupScrollClass('contactPopup');
	});
	
}

function show_remain_character(str,divid,idBooking)
{
	var total='400';
	var len = str.length;
	var remain=total-len;
	div_id=divid+'_'+idBooking;
	document.getElementById(div_id).innerHTML=remain;
}
function booking_on_hold(szBookingStatus)
{ 
    var stripe = $('#ZoozPayment').prop('checked'); 
    var isCalledFromMobile = isMobileDevice();
    var iAlreadyCalled = 2; 
    $(".continue-button" ).unbind("click");
    if(stripe && isCalledFromMobile)
    {
        var iInsuranceSelectedFlag = $("#iInsuranceSelectedFlag").val();
        iInsuranceSelectedFlag = parseInt(iInsuranceSelectedFlag);

        var iTerms = $('#iTerms').prop('checked'); 
        if(iInsuranceSelectedFlag>0 && iTerms)
        {
            $(".stripe-button-el").trigger("click");
            iAlreadyCalled = 1;
        }
    }
    if(szBookingStatus=='ON_HOLD')
    {
        $("#iBookingStatus").attr("value",'2');
    }
    else
    {
        $("#iBookingStatus").attr("value",'3');
        $("#iBookingPayment").attr("value",'Paypal');
    }
    $("#loader").attr('style','display:block;');

    var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
    $("#szBookingRandomNum_booking_conf").attr('value',szBookingRandomNum);
    var siteLanguage = $("#iSiteLanguageFooter_hidden").val();

    var iCallStripeGateway=0;
    var szApiKey = '';
    var fChargeableAmount = '';
    var szCurrency = '';
    var szDescription = '';
    var szEmail = '';

    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php?lang="+siteLanguage,$("#booking_confirmation_form").serialize(),function(result){
        var res_ary = result.split("||||");
        $("#loader").attr('style','display:none;');
        if(res_ary[0]=='ERROR')
        {
            $("#loader").attr('style','display:none;');
            $("#booking_confirm_error").attr('style','display:block'); 
            $("#booking_confirm_error").html(res_ary[1]);         	
            $("#booking_confirm_error").focus(); 
            window.scrollTo(50,50);
        }
        else if(res_ary[0]=='ERROR_NEW_PAGE')
        {
            $("#loader").attr('style','display:none;');
            $("#error_message_container_div").attr('style','visibility:visible;'); 
            $("#error_message_container_div").html(res_ary[1]);  
            if(res_ary[2]=='PURCHASE_GOODS_ERROR')
            {
                $("#fValueOfGoods").addClass('red_border');
            }
        }
        else if(res_ary[0]=='BOOKING_NOTIFICATION')
        {
            $("#change_price_div").html(res_ary[1]); 
            $("#change_price_div").attr('style','display:block');
        }
        else if(res_ary[0]=='CONFIRM')
        {
            $("#loader").attr('style','display:none;');
            $("#account_confirm_resend_email").attr('style','display:block'); 
            $("#account_confirm_resend_email").html(res_ary[1]);   
            window.scrollTo(50,50);
        }
        else if(res_ary[0]=='SUCCESS')
        {
            redirect_url(res_ary[1]);
        }
        else if(res_ary[0]=='SUCCESSZO0Z')
        {
            $("#booking_confirm_error").attr('style','display:none;');
            var iStripeApi = 1;
            if(iStripeApi==1)
            {
                /*
                *  This Line of code is used only for stripe payment gateway 
                *  */
                szApiKey = res_ary[1];
                fChargeableAmount = res_ary[2];
                szCurrency = res_ary[3];
                szDescription = res_ary[4]; 
                szEmail = res_ary[5]; 
                iCallStripeGateway = 1;
                if(iAlreadyCalled==2)
                {
                    callStripeApi(szApiKey,fChargeableAmount,szCurrency,szDescription,szEmail);
                }
                $(".continue-button" ).unbind("click");
            } 
            else
            {
                /* 
                * To enable zooz payment gateway we need reopen following line of code
                *  
               */ 
                callzoozapi(res_ary[1],res_ary[2],res_ary[3]);  
            } 
        }		
        else if(res_ary[0]=='PRICE_CHANGED')
        {
            $("#loader").attr('style','display:none;');
            display_price_changed_popup(res_ary[1]);
        }	 
        else if(res_ary[0]=='REDIRECT')
        {
            $("#loader").attr('style','display:none;');
            redirect_url(res_ary[1]);
        }
    });  
}
function display_price_changed_popup(page_url,szBookingRandomNum)
{
	if(szBookingRandomNum =='')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{page_url:page_url,mode:'RECALCULATE_BOOKING_PRICE',szBookingRandomNum:szBookingRandomNum},function(result){
		
		result_ary = result.split('||||');
		$("#change_price_div").html(result_ary[1]);
		$("#change_price_div").attr('style','display:block');
		addPopupScrollClass('change_price_div');
	});
}

function display_price_changed_popup_cargo_changed(page_url,szBookingRandomNum,type)
{	
	if(szBookingRandomNum =='')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{type:type,page_url:page_url,mode:'RECALCULATE_BOOKING_PRICE_CARGO_CHANGED',szBookingRandomNum:szBookingRandomNum},function(result){
		
		result_ary = result.split('||||');
		if(result_ary[0]=='REDIRECT')
		{
			redirect_url(result_ary[1]);
		}
		else
		{
			$("#change_price_div").html(result_ary[1]);
			$("#change_price_div").attr('style','display:block');
			addPopupScrollClass('change_price_div');
		}
	});
}

function enable_email_field()
{
	disableForm('hsbody');
	if($("#contactEditEmail").attr('checked'))
    {
		$("#szEmail").attr('readonly', false);
		$("#szEmail").attr('style','background:#fff;color:#000000;');
	}
	else
	{
		$("#szEmail").attr('readonly', true);
		$("#szEmail").attr('style','background:#DBDBDB;color:#828282;');
	}
}

function sumit_contact_form()
{
	$("#popup-bg").css({"height": "0px"});	
	$("#contactButton").attr("onclick","");
	$.post(__JS_ONLY_SITE_BASE__+"/contactPopUp.php",$("#contactForm").serialize(),function(result){
	$("#contactPopup").html(result);});
	removePopupScrollClass('contactPopup');
}

function submit_bookig_details()
{
    $("#loader").attr('style','display:block;');
    var iShipperConsigneeSubmit = $("#iShipperConsigneeSubmit").val(); 
    iShipperConsigneeSubmit = parseInt(iShipperConsigneeSubmit);
    if(iShipperConsigneeSubmit!=1)
    {
        return false;
    }
    
    var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
    $("#szBookingRandomNum_booking_details").attr('value',szBookingRandomNum); 
    
    //var value=decodeURIComponent($("#booking_detail_form").serialize());
    var value= $("#booking_detail_form").serialize();
    var lang = $("#iSiteLanguageFooter_hidden").val();
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php?lang="+lang,value,function(result){

        result_ary = result.split("||||");  
        if(jQuery.trim(result_ary[0])=='ERROR')
        {
            $("#booking_details_error_div").attr('style','display:block'); 
            $("#booking_details_error_div").html(result_ary[1]);
            window.scrollTo(50,50);
        }  
        else if(jQuery.trim(result_ary[0])=='DISPLAY_FORM_ERROR')
        {   
            $("#booking_details_container").html(result_ary[1]); 
        } 
        else if(jQuery.trim(result_ary[0])=='BOOKING_NOTIFICATION')
        {  
            $("#change_price_div").html(result_ary[1]);
            $("#change_price_div").attr('style','display:block'); 
            window.scrollTo(50,50);
        }
        else if(jQuery.trim(result_ary[0])=='NO_RECORD_FOUND')
        {  
            $("#all_available_service").html(result_ary[1]);
            $("#all_available_service").attr('style','display:block');  
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#booking_details_error_div").attr('style','display:none;');
            var page_url =result_ary[1] ;
            redirect_url(page_url);
        }
       $("#loader").attr('style','display:none;');
    });
}
function cancel_booking(page_url)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",{operation:'CLEAR_SEARCH'},function(result){
        $(location).attr('href',page_url);
    });
}

function validate_booking_quote_form(formId,mode)
{
    var error_count = 1;
    var cb_flag = $('#iPrivateShipping').prop('checked');   
    if(!cb_flag)
    {
        if(isFormInputElementEmpty(formId,'szShipperCompanyName')) 
        {		
            $("#szShipperCompanyName").addClass('red_border');
            error_count = error_count+1 ; 
        }
        else
        {
            $("#szShipperCompanyName").removeClass('red_border');
        }
    }  
    
    if(isFormInputElementEmpty(formId,'szShipperFirstName')) 
    {		
        $("#szShipperFirstName").addClass('red_border');
        error_count = error_count+1 ; 
    }
    else
    {
        $("#szShipperFirstName").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'szShipperLastName')) 
    {		
        $("#szShipperLastName").addClass('red_border');
        error_count = error_count+1 ; 
    }
    else
    {
        $("#szShipperLastName").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'szShipperEmail')) 
    {		
        $("#szShipperEmail").addClass('red_border');
        error_count = error_count+1 ; 
    }
    else if(!isValidEmail($('form#' + formId + ' #szShipperEmail' ).val())) 
    { 
        $("#szShipperEmail").addClass('red_border');
        error_count = error_count+1 ; 
    }
    else
    {
        $("#szShipperEmail").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'idShipperDialCode')) 
    {		
        $("#idShipperDialCode").addClass('red_border');
        error_count = error_count+1 ; 
    }
    else
    {
        $("#idShipperDialCode").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'szShipperPhone')) 
    {		
        $("#szShipperPhone").addClass('red_border');
        error_count = error_count+1 ; 
    } 
    else
    {
        $("#szShipperPhone").removeClass('red_border');
    }
    
    if(error_count==1)
    {
        if(mode=='GET_QUOTES')
        {
            submit_bookig_quote_contact_details(mode);
        }
        else
        {
			var calculate_flag = $("#calculate_service_list_flag").val(); 
			calculate_flag = parseInt(calculate_flag); 
			if(calculate_flag==0)
			{
				submit_bookig_quote_contact_details(mode);
			}
			else
			{
				$("#get-quote-price-button").addClass('get-price-button-clicked');
				$("#calculate_service_list_flag").val('2');
			}  
        } 
    }
    else
    { 
        var iTopMargin = 0;
        if($("#booking_quotes_details_container").length)
        {
            iTopMargin = $("#booking_quotes_details_container").offset().top
        }
        $('html, body').animate({ scrollTop: (iTopMargin - 10) }, "1000");
        return false;
    }
}

function submit_bookig_quote_contact_details(mode)
{
    //$("#loader").attr('style','display:block;');
    $("#get-quote-price-button").addClass('get-price-button-clicked');
    var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
    $("#szBookingRandomNum_booking_details").attr('value',szBookingRandomNum); 

    var lang = $("#iSiteLanguageFooter_hidden").val();

    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php?mode="+mode+"&lang="+lang,$("#booking_quote_contact_detail_form").serialize(),function(result){ 
        
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {
            $("#booking-quotes-form-container").attr('style','display:block'); 
            $("#booking-quotes-form-container").html(result_ary[1]); 
            $("#get-quote-price-button").removeClass('get-price-button-clicked');
        }   
        else if(jQuery.trim(result_ary[0])=='BOOKING_NOTIFICATION')
        {
            $("#change_price_div").attr('style','display:block'); 
            $("#change_price_div").html(result_ary[1]);
            window.scrollTo(50,50);
            $("#get-quote-price-button").removeClass('get-price-button-clicked');
        } 
        else if(result_ary[0]=='SUCCESS_LOCAL_SEARCH')
        { 
            $("#ajaxLogin").html(result_ary[1]);
            $("#ajaxLogin").attr('style','display:block'); 
            $("#get-quote-price-button").removeClass('get-price-button-clicked');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            var page_url =result_ary[1];
            redirect_url(page_url);
        }
        $("#loader").attr('style','display:none;');
    });
} 

function clear_forwarder_form()
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_forwarderSignup.php",function(result){
	$("#forwardersignup").html(result);});
}

function open_sorted_popup(servicetype)
{
	addPopupScrollClass('forwarder_sorted_popup');
	$("#forwarder_sorted_popup").attr('style','display:block;');
	$.get(__JS_ONLY_SITE_BASE__+"/forwarderSortedPopUp.php",{servicetype:servicetype},function(result){
		
		$("#forwarder_sorted_popup").html(result);
		
	});
}

function OpenOutlookDoc()
{
try
{

var outlookApp = new ActiveXObject("Outlook.Application");
var nameSpace = outlookApp.getNameSpace("MAPI");
mailFolder = nameSpace.getDefaultFolder(6);
mailItem = mailFolder.Items.add('IPM.Note.FormA');
mailItem.Subject="a subject test";
mailItem.To = "an@email.here";
mailItem.HTMLBody = "<b>bold</b>";
mailItem.display (0);
}
catch(e)
{
alert(e);
// act on any error that you get
}
}

function send_shipper_consignee_mail(email,subject,flag)
{
/*
	 var res_ary = email_body.split("||||");
	 var email = res_ary[0];
	 var subject = res_ary[1];
	 var message = res_ary[2];
	 */
	 if(flag=='Shipper')
	 {
	 	var message = $("#szShipperMailMsg").attr('value');
	 	
	 }
	 else if(flag=='Consigee')
	 {
	 	var message=$("#szConsigneeMailMsg").attr('value');
	 }
	 
	  message2 = message.split("<br>").join("\n");
	  message2=$.trim(message2);
	  if(email!='')
	  {
		  window.location.href="mailto:"+email+"?subject=" + subject + "&body=" + escape(message2);
	  }
	  else
	  {
		  window.location.href="mailto:?subject=" + subject + "&body=" + escape(message2);
	  }
	  
}

function add_new_user(idForwarderRole)
{	
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",{role:idForwarderRole,mode:'ADD_USER'},function(result){		
		$("#forwarder_company_div").html(result);
		$("#forwarder_company_div").attr('style','display:block;');		
		$("#szEmail").focus();
		addPopupScrollClass('forwarder_company_div');
	});
}
function add_forwarder_contact_details()
{
	var form_value=decodeURIComponent($("#addForwarderContactInfo").serialize());
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",form_value,function(result){		
		$("#forwarder_company_div").html(result);
		$("#forwarder_company_div").attr('style','display:block;');	
		addPopupScrollClass('forwarder_company_div');	
	});
}


function update_forwarder_user(idForwarderContact)
{	
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",{idForwarderContact:idForwarderContact,mode:'EDIT_USER'},function(result){		
		$("#forwarder_company_div").html(result);
		$("#forwarder_company_div").attr('style','display:block;');	
		addPopupScrollClass('forwarder_company_div');	
	});
}
function update_forwarder_contact_details()
{
	var form_value=decodeURIComponent($("#addForwarderContactInfo").serialize());
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",form_value,function(result){		
		$("#forwarder_company_div").html(result);
		$("#forwarder_company_div").attr('style','display:block;');		
	});
}

function delete_forwarder_contact(idForwarderContact)
{	
	//var conf_flag = confirm(szConfirmMessage);
	//if(conf_flag)
	//{
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",{idForwarderContact:idForwarderContact,mode:'DELETE_USER'},function(result){		
			$("#forwarder_company_div").html(result);
			$("#forwarder_company_div").attr('style','display:block;');
			addPopupScrollClass('forwarder_company_div');		
		});
	//}	
}
function confirm_delete()
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",$("#delete_forwarder_contact_form").serialize(),function(result){		
		$("#forwarder_company_div").html(result);
		$("#forwarder_company_div").attr('style','display:block;');		
	});
}
/*
function validateLandingPageForm_key_press(kEvent,form_id,user_id)
{
	if(kEvent.keyCode==13)
	{
		validateLandingPageForm(form_id,user_id);
	}
}
*/
function on_enter_key_press(kEvent,functionName,params,page_url)
{
	if(kEvent.keyCode==13)
	{
		if(functionName=='validateLandingPageForm')
		{
			validateLandingPageForm('landing_page_form',params,page_url);
		}	
		if(functionName=='submit_currency_popup')
		{
			submit_currency_popup(params);
		}
		if(functionName=='forgot_passward_popup')
		{
			forgot_passward_popup('');
		}
		if(functionName=='forwarder_login_form')
		{
			$("#"+functionName).submit();
		}
		if(functionName=='add_forwarder_contact_details')
		{
			add_forwarder_contact_details();
		}	
		if(functionName=='validateLandingPageForm_forwarder')
		{
			validateLandingPageForm_forwarder('landing_page_form',params);
		}
		if(functionName=='sumit_contact_form')
		{
			sumit_contact_form();
		}
		if(functionName=='submit_edit_profile_popup')
		{
			submit_edit_profile_popup();
		}
		if(functionName=='header_login')
		{
			userLogin('loginForm_header');
		}
		if(functionName=='update_booking_cargo')
		{
			update_booking_cargo(params);
		}
		if(functionName=='compare_validate_home_page_requirement')
		{
			var szOriginCountry = $("#szOriginCountry").attr('value');
			var szDestinationCountry = $("#szDestinationCountry").attr('value');
			
			if(szOriginCountry>0 && szDestinationCountry>0)
			{
				compare_validate_home_page();
			}
		}
		if(functionName=='compare_validate_home_page')
		{
			compare_validate_home_page();
		}
		if(functionName=='userBookingLogin')
		{
			userBookingLogin();
		}
		if(functionName=='obo_haulage_tryit_out')
		{
			obo_haulage_tryit_out();
		}
		if(functionName=='update_city_postcode_requirement')
		{
			update_city_postcode_requirement('city_postcode_box_form');
		}
		if(functionName=='submitPasswordUpdate')
		{
			submitPasswordUpdate();
		}
		
	}
}

function limit_chars(counter)
{
	var str = $("#szCargoCommodity_"+counter).attr('value');
	var str_len = str.length;
	if(str_len>=250)
	{
		var first_250_chars = str.substr(0,250);
		$("#szCargoCommodity_"+counter).attr('value',first_250_chars);
		//$("#char_count_"+counter).html(str_len);
	}
	else
	{
		//$("#char_count_"+counter).html(str_len);
	}
}
function delete_forwarder_account()
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_deleteForwarderAccount.php",$("#deleteForwarderAccount").serialize(),function(result){
	$("#delete_forwarder_account").html(result);
	});	
}
function delete_account_forwarder()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_deleteForwarderAccount.php",$("#delete_account").serialize(),function(result){
	$("#delete_forwarder_account").html(result);});
}
function editForwarderUserInformation()
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",{showflag:'user_info'},function(result){
	$("#myprofile_info").html(result);});
}
function cancel_forwarder_info(value)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",function(result){
	$("#myprofile_info").html(result);});
}
function editForwarderContactInformation()
{ 
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",{showflag:'contact_info'},function(result){
        $("#myprofile_info").html(result);
    });
}
function change_forwarder_password()
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",{showflag:'change_pass'},function(result){
        $("#myprofile_info").html(result);
    });
}
function cancel_forwarder_password()
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",function(result){
        $("#myprofile_info").html(result);
    });
}
function update_forwarder_user_info()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",$('#updateUserInfo').serialize(),function(result){
        $("#myprofile_info").html(result);
    });
}
function update_forwarder_contact_info_details()
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",$('#updateContactInfo').serialize(),function(result){
        $("#myprofile_info").html(result);
    });
}
function changeForwarderPassword()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",$("#changePassword").serialize(),function(result){
        $("#myprofile_info").html(result);
    });
}

function edit_preferences_email(idForwarder)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:idForwarder,mode:'EDIT_EMAIL_ADDRESS'},function(result){
        $("#forwarder_company_div").html(result);	
    });
}
function show_preference_emails(idForwarder)
{	
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:idForwarder,mode:'DISPLAY_EMAIL_ADDRESS'},function(result){
        $("#forwarder_company_div").html(result);	
    });
}
function add_forwarder_profile(mode,id,valid)
{    
    var message;
    if(mode=='booking')
    {
        message="This is where you will receive sea booking details immediately when a customer books with your company on Transporteca";
    }
    if(mode=='air_booking')
    {
        message="This is where you will receive air booking details immediately when a customer books with your company on Transporteca";
    }
    if(mode=='road_booking')
    {
        message="This is where you will receive road booking details immediately when a customer books with your company on Transporteca";
    }
    if(mode=='courier_booking')
    {
        message="This is where you will receive courier booking details immediately when a customer books with your company on Transporteca";
    }
    else if(mode=='payment')
    {
        message="This is where you will receive transfer notices and invoices from Transporteca";
    }
    else if(mode=='customer')
    {
        message="This is shared with the customers, and where you should expect to receive customer service enquiries";
    }
    if(mode=='catchUpAll')
    {
        message="This is where you will receive all booking details immediately when a customer books with your company on Transporteca";
    }
    var value=$("#ajax_preferences_"+mode+"").val();
    
    if(parseInt(value)>1){
        $("#remove_forwarder_profile_"+mode+"").attr('style','display:block');}else{$("#remove_forwarder_profile"+mode+"").attr('style','display:none');
    } 
    //onblur="closeTip(\''+mode+value+'\');" onfocus="openTip(\''+mode+value+'\');"
    $("#forwarder_preferences_"+mode+"").append('<div class="profile-fields" id="'+mode+'s'+value+'"><input type="text" name="updatePrefEmailAry[szEmail'+id+']['+value+']" ><div class="field-alert"><div id="'+mode+value+'" style="display:none;">'+message+'</div></div></div>');
    value=parseInt(value)+1
    $("#ajax_preferences_"+mode+"").attr('value',value);
    var number=$('#'+valid+'').val();
    number = parseInt(number);
    $('#'+valid+'').attr('value',number+1);

}
function remove_forwarder_profile(preferences,ids,remove,id_contact,id,valid)
{  	var book=$('#validateBooking').val();
	var pay=$('#validatePayment').val();
	var custo=$('#validateCustomer').val();
	var number=$('#'+valid+'').val();
	number = parseInt(number);
	var value=$("#"+preferences+"").val();
	
	if(value>2){
	
		if(number <= 0){//alert(number);	
		$('#'+remove+'').attr('style','display:none');
		//$("#removeMails").attr('value','_'+id_contact);
		//$('input').attr('[name="'+name+'"]',"newValue");
		//$("#"+ids).remove();
		//$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{idContact:id_contact,status:'REMOVE_EMAIL_ADDRESS',id:id,mode:'EDIT_EMAIL_ADDRESS',validateBooking:book,validatePayment:pay,validateCustomer:custo},function(result){
		//$("#forwarder_preferences_edit_"+preferences+"").html(result);	
		//$("#ajax_edit_mail_control").html(result);		
	//}); 
	}
	
	value=parseInt(value)-1;
		
		var name=$('#'+ids+''+value +'> input:hidden').val();
		var valueMail=$('#removeMails').val();
		var newvalueMail=valueMail+"_"+name;
		$("#removeMails").attr("value",newvalueMail);
		$('#'+ids+''+value).remove();
		$("#"+preferences+"").attr('value',value);
		
			if(parseInt(value)>=3){$("#"+remove+"").attr('style','display:block');}else{$("#"+remove+"").attr('style','display:none');}
	 
	if(number>=1){
	$('#'+valid+'').attr('value',number-1);
	 }
	 } 
}
function submit_preference_emails()
{	
    var check=$("#removeMails").val();	
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",$("#ajax_preferences_email").serialize(),function(result)
    { 
        $("#forwarder_company_div").html(result);
        $("#removeMails").attr('value',check);
        show_forwarder_top_messges();
    });
}
/*function add_forwarder_profile_payment()
{
	var value=$("#ajax_preferences_payment").val();
	if(parseInt(value)>1){$("#remove_forwarder_profile_payment").attr('style','display:block');}else{$("#remove_forwarder_profile_payment").attr('style','display:none');}
	$("#forwarder_preferences_payment").append('<div class="oh" id="payments'+value+'"><p class="fl-40">&nbsp;</p><p class="fl-60"><input type="text" name="updatePrefEmailAry[szEmailPayment]['+value+']"></p></div>');
	value=parseInt(value)+1
	$("#ajax_preferences_payment").attr('value',value);

}*/
function edit_preferences_system(id)
{
	
$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:id,mode:'EDIT_SYSTEM_CONTROL'},function(result){
	$("#forwarder_company_div").html(result);
});
}
function submit_preference_controls()
{
$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",$("#ajax_prefrence_control").serialize(),function(result){
//$("#ajaxLogin").html(result);
var n = result.split('|||||');
if(n['0']=='SUCCESS')
{
$("#forwarder_company_div").html(n['1']);
show_forwarder_top_messges();
}
else if(n['0']=='CONFIRM')
{
$("#ajaxLogin").html(n['1']);
}
});
}
function show_preference_controls(idForwarder)
{
$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:idForwarder,mode:'VIEW_SYSTEM_CONTROL'},function(result){
$("#forwarder_company_div").html(result);
});
}

function addWareHouses(type,maxrowexcced)
{
	var type;
	if(type=='Origin')
	{
		var originCountry = $("#originCountry").attr('value');
		var originWarehouse = $("#originWarehouse").attr('value');
		var idOriginWarehouse = $("#idOriginWarehouse").attr('value');
                var iWarehouseType = $("#iWarehouseType").attr('value');
		var count = $("#originWarehouseAdded").attr('value'); 
		 var res_ary = originWarehouse.split("_");
	
		if(originWarehouse!="")
		{
			if(idOriginWarehouse=='')
			{
				var newvalue=res_ary[0]+'_'+res_ary[3];
				$("#idOriginWarehouse").attr('value',newvalue);
			}
			else
			{
				var newvalue=res_ary[0]+'_'+res_ary[3];
				
				 var res_ary_new = idOriginWarehouse.split(";");
				 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			
				if(add_ware!='-1')
				{
					return false;
				}
				
				var value1=idOriginWarehouse+';'+newvalue;
				$("#idOriginWarehouse").attr('value',value1);
			}
			var newcount=parseInt(count)+1;
			
			$("#originWarehouseAdded").attr('value',newcount);
			var div_id="origin_data_"+count;
			$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{originCountry:originCountry,originWarehouse:originWarehouse,flag:'OriginAdd',count:newcount,iWarehouseType:iWarehouseType},function(result){
			
			$("#"+div_id).html(result);
			
			// $("#"+div_id).attr("onclick","delete_ware_houseInfo('"+newvalue+"','"+div_id+"','Origin');");
			$("#"+div_id).click(function(){delete_ware_houseInfo(newvalue,div_id,'Origin',maxrowexcced)});
			var divid="origin_data_"+newcount;
			$('#originTable').append('<tr id='+divid+' onclick=""></tr>');
				var desWarehouseValue = $("#idDesWarehouse").attr('value');
				var originWarehouseValue = $("#idOriginWarehouse").attr('value');
				if(desWarehouseValue!='' && originWarehouseValue!="")
				{
					$("#download_button").attr("onclick","downloadFormSubmit()");
					$("#download_button").attr("style","opacity:1;");
					$("#row_excced_data_1").attr("style","display:none;");
				}
				else
				{
					$("#download_button").unbind("click");
					//$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
				
				var downloadType = $("#downloadType").val();
				if(downloadType=='2')
				{
					checkRowCount(maxrowexcced);
				}
				else if(downloadType=='1')
				{
					updatedNote(maxrowexcced);
				}
				
			});
		}
		else
		{
			if(originCountry!='')
			{
				var items = $("#originWarehouse option").length;
				var  items =items-1;
				
				if(items!=0 && items!='')
				{
					var values = [];
					$('#originWarehouse option').each(function() { 
					    values.push( $(this).attr('value') );
					});
					var arrlen=values.length;
					for(i=1;i<arrlen;++i)
					{
						var newcount=$("#originWarehouseAdded").attr('value');
						var idOriginWarehouse = $("#idOriginWarehouse").attr('value');
						var res_ary = values[i].split("_");
						
						if(idOriginWarehouse=='')
						{
							var newValueWare=values[i];
							var new_value_ware=res_ary[0]+'_'+res_ary[3];
							var value1=new_value_ware;
							var newvalue=res_ary[0]+'_'+res_ary[3];
							//$("#idOriginWarehouse").attr('value',value1);
				 		}
				 		else
				 		{
				 			var newvalue=res_ary[0]+'_'+res_ary[3];
				 			var res_ary_new = idOriginWarehouse.split(";");
				 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
				 			if(add_ware!='-1')
							{
								var newValueWare='';
							}
							else
							{
								var newValueWare=values[i];
								var new_value_ware=res_ary[0]+'_'+res_ary[3];
								var value1=idOriginWarehouse+';'+newvalue;
								//$("#idOriginWarehouse").attr('value',value1);
							}					
				 		}
						if(newValueWare!='undefined' && newValueWare!='')
						{
								div_id="origin_data_"+newcount;
								var Origin ="Origin";
								$("#"+div_id).html('<th width="33%" onclick="delete_ware_houseInfo(\''+newvalue+'\',\''+div_id+'\',\''+Origin+'\',\''+maxrowexcced+'\')">'+res_ary[2]+'</th><td width="67%" onclick="delete_ware_houseInfo(\''+newvalue+'\',\''+div_id+'\',\''+Origin+'\',\''+maxrowexcced+'\')">'+res_ary[1]+'</td>');
								//document.getElementById(div_id).innerHTML='<td width="33%">'+res_ary[2]+'</td><td width="67%">'+res_ary[1]+'</td>';
								
								//new_div_id="#"+div_id;
								/*if(i==1)
								{
									$("#"+div_id).attr("onclick","delete_ware_houseInfo('"+newvalue+"','"+div_id+"','Origin');");
								}
								else
								{
									$("#"+div_id).click(function(){delete_ware_houseInfo(newvalue,div_id,'Origin')});
								}*/
								newcount=parseInt(newcount)+1;
								var divid="origin_data_"+newcount;
								$('#originTable').append('<tr id='+divid+' onclick=""></tr>');
								if(value1!='undefined' && value1!='')
									$("#idOriginWarehouse").attr('value',value1);
								
								$("#originWarehouseAdded").attr('value',newcount);
								
							}
						}
					}
					var desWarehouseValue = $("#idDesWarehouse").attr('value');
					var originWarehouseValue = $("#idOriginWarehouse").attr('value');
					if(desWarehouseValue!='' && originWarehouseValue!="")
					{
						$("#download_button").attr("onclick","downloadFormSubmit()");
						$("#download_button").attr("style","opacity:1;");
						$("#row_excced_data_1").attr("style","display:none;");
					}
					else
					{
						$("#download_button").unbind("click");
						//$("#download_button").attr("onclick","");
						$("#download_button").attr("style","opacity:0.4;");
					}
					
					var downloadType = $("#downloadType").val();
					if(downloadType=='2')
					{
						checkRowCount(maxrowexcced);
					}
					else if(downloadType=='1')
					{
						updatedNote(maxrowexcced);
					}
				}
			else
			{
				$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{flag:'OriginAddAll',iWarehouseType:iWarehouseType},function(result){
					
					$("#originTable").html(result);
					
					var desWarehouseValue = $("#idDesWarehouse").attr('value');
					var originWarehouseValue = $("#idOriginWarehouse").attr('value');
					if(desWarehouseValue!='' && originWarehouseValue!="")
					{
						//$("#download_button").attr("onclick","downloadFormSubmit()");
						$("#download_button").click(function(){downloadFormSubmit()});
						$("#download_button").attr("style","opacity:1;");
						$("#row_excced_data_1").attr("style","display:none;");
					}
					else
					{
						$("#download_button").unbind("click");
						//$("#download_button").attr("onclick","");
						$("#download_button").attr("style","opacity:0.4;");
					}
					
					var downloadType = $("#downloadType").val();
					if(downloadType=='2')
					{
						checkRowCount(maxrowexcced);
					}
					else if(downloadType=='1')
					{
						updatedNote(maxrowexcced);
					}
				});
			}
	 }
		
	}
	else if(type == 'Destination')
	{
		var desCountry = $("#desCountry").attr('value');
		var desWarehouse = $("#desWarehouse").attr('value');
		var idDesWarehouse = $("#idDesWarehouse").attr('value');
		var count = $("#desWarehouseAdded").attr('value');
                var iWarehouseType = $("#iWarehouseType").attr('value');
                var res_ary = desWarehouse.split("_");
		
		if(desWarehouse!="")
		{
			if(idDesWarehouse=='')
			{
				var value1=res_ary[0]+'_'+res_ary[3];
				$("#idDesWarehouse").attr('value',value1);
			}
			else
			{
				var newvalue=res_ary[0]+'_'+res_ary[3];
				
				 var res_ary_new = idDesWarehouse.split(";");
				 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			
				if(add_ware!='-1')
				{
                                    return false; 
				}
				
				var value1=idDesWarehouse+';'+newvalue;
				$("#idDesWarehouse").attr('value',value1);
			}
			var newcount=parseInt(count)+1;	
			$("#desWarehouseAdded").attr('value',newcount);
			var div_id="des_data_"+count;
			$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{desCountry:desCountry,desWarehouse:desWarehouse,flag:'DesAdd',count:newcount,iWarehouseType:iWarehouseType},function(result){
			
			$("#"+div_id).html(result);
			// $("#"+div_id).attr("onclick","delete_ware_houseInfo('"+newvalue+"','"+div_id+"','Destination');");
			$("#"+div_id).click(function(){delete_ware_houseInfo(newvalue,div_id,'Destination',maxrowexcced)});
			var divid="des_data_"+newcount;
			$('#desTable').append('<tr id='+divid+' onclick=""></tr>');
				var desWarehouseValue = $("#idDesWarehouse").attr('value');
				var originWarehouseValue = $("#idOriginWarehouse").attr('value');
				if(desWarehouseValue!='' && originWarehouseValue!="")
				{
					//$("#download_button").attr("onclick","downloadFormSubmit()");
					$("#download_button").click(function(){downloadFormSubmit()});
					$("#download_button").attr("style","opacity:1;");
					$("#row_excced_data_1").attr("style","display:none;");
				}
				else
				{
					$("#download_button").unbind("click");
					//$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
				var downloadType = $("#downloadType").val();
				if(downloadType=='2')
				{
					checkRowCount(maxrowexcced);
				}
				else if(downloadType=='1')
				{
					updatedNote(maxrowexcced);
				}
			});
		
		}
		else
		{
			if(desCountry!='')
			{
				var items = $("#desWarehouse option").length;
				var  items =items-1;
				
				if(items!=0 && items!='')
				{
					var values = [];
					$('#desWarehouse option').each(function() { 
					    values.push( $(this).attr('value') );
					});
					var arrlen=values.length;
					for(i=1;i<arrlen;++i)
					{
						var newcount=$("#desWarehouseAdded").attr('value');
						var idDesWarehouse = $("#idDesWarehouse").attr('value');
						var res_ary = values[i].split("_");
						
						if(idDesWarehouse=='')
						{
							var newValueWare=values[i];
							var new_value_ware=res_ary[0]+'_'+res_ary[3];
							var value1=new_value_ware;
							var newvalue=res_ary[0]+'_'+res_ary[3];
				 		}
				 		else
				 		{
				 			var newvalue=res_ary[0]+'_'+res_ary[3];
				 			var res_ary_new = idDesWarehouse.split(";");
				 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
				 			if(add_ware!='-1')
							{
								var newValueWare='';
							}
							else
							{
								var newValueWare=values[i];
								var new_value_ware=res_ary[0]+'_'+res_ary[3];
								var value1=idDesWarehouse+';'+newvalue;
							}					
				 		}
				 		
						if(newValueWare!='undefined' && newValueWare!='')
						{
								div_id="des_data_"+newcount;
								var Destination ="Destination";
								$("#"+div_id).html('<th onclick="delete_ware_houseInfo(\''+newvalue+'\',\''+div_id+'\',\''+Destination+'\',\''+maxrowexcced+'\')">'+res_ary[2]+'</th><td onclick="delete_ware_houseInfo(\''+newvalue+'\',\''+div_id+'\',\''+Destination+'\',\''+maxrowexcced+'\')">'+res_ary[1]+'</td>');
								//document.getElementById(div_id).innerHTML='<td>'+res_ary[2]+'</td><td>'+res_ary[1]+'</td>';
								
								
								/*if(i==1)
								{
									$("#"+div_id).attr("onclick","delete_ware_houseInfo('"+newvalue+"','"+div_id+"','Destination');");
								}
								else
								{	
															
									$("#"+div_id).click(function(){delete_ware_houseInfo(newvalue,div_id,'Destination')});
								}*/
								newcount=parseInt(newcount)+1;
								var divid="des_data_"+newcount;
								$('#desTable').append('<tr id='+divid+' onclick=""></tr>');
								if(value1!='undefined' && value1!='')
								$("#idDesWarehouse").attr('value',value1);
								
								$("#desWarehouseAdded").attr('value',newcount);
								
							}
						}
					}
					var desWarehouseValue = $("#idDesWarehouse").attr('value');
					var originWarehouseValue = $("#idOriginWarehouse").attr('value');
					if(desWarehouseValue!='' && originWarehouseValue!="")
					{
						//$("#download_button").attr("onclick","downloadFormSubmit()");
						$("#download_button").click(function(){downloadFormSubmit()});
						$("#download_button").attr("style","opacity:1;");
						$("#row_excced_data_1").attr("style","display:none;");
					}
					else
					{
						$("#download_button").unbind("click");
						//$("#download_button").attr("onclick","");
						$("#download_button").attr("style","opacity:0.4;");
					}
					
					var downloadType = $("#downloadType").val();
					if(downloadType=='2')
					{
						checkRowCount(maxrowexcced);
					}
					else if(downloadType=='1')
					{
						updatedNote(maxrowexcced);
					}
				}
				else
				{
					$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{flag:'DesAddAll',desCountry:desCountry,iWarehouseType:iWarehouseType},function(result){
						
						$("#desTable").html(result);
						
						var desWarehouseValue = $("#idDesWarehouse").attr('value');
						var originWarehouseValue = $("#idOriginWarehouse").attr('value');
						if(desWarehouseValue!='' && originWarehouseValue!="")
						{
							//$("#download_button").attr("onclick","downloadFormSubmit()");
							$("#download_button").click(function(){downloadFormSubmit()});
							$("#download_button").attr("style","opacity:1;");
							$("#row_excced_data_1").attr("style","display:none;");
						}
						else
						{
							$("#download_button").unbind("click");
							//$("#download_button").attr("onclick","");
							$("#download_button").attr("style","opacity:0.4;");
						}
						
						var downloadType = $("#downloadType").val();
						if(downloadType=='2')
						{
							checkRowCount(maxrowexcced);
						}
						else if(downloadType=='1')
						{
							updatedNote(maxrowexcced);
						}
					});
				}
		}
	}
	
		
}


function showForwarderWarehouse(idForwarder,idCountry,divid,maxrowexcced,iWarehouseType)
{
	if(divid=='orgin_warehouse')
	{
		$("#lcl_orign").attr("style","opacity:0.4;");
		$("#lcl_orign").attr("onclick","");
		$("#originWarehouse").attr("disabled","disabled");
		
		var showflag='showOriginWarehouse';
	}
	else if(divid=='des_warehouse')
	{
		$("#lcl_des").attr("style","opacity:0.4;");
		$("#lcl_des").attr("onclick","");
		$("#desWarehouse").attr("disabled","disabled");
		var showflag='showDesWarehouse';
	}
	else if(divid=='haulage_warehouse')
	{
		$("#haulage_add").attr("style","opacity:0.4;");
		$("#haulage_add").attr("onclick","");
		var showflag='showHaulageWarehouse';
		$("#haulageWarehouse").attr("disabled",true);
		$("#haulagePricingModel").attr("disabled",true);
	}	
	
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{idForwarder:idForwarder,idCountry:idCountry,flag:showflag,iWarehouseType:iWarehouseType},function(result){
	$("#"+divid).html(result);
	
		if(divid=='orgin_warehouse')
		{
			$("#lcl_orign").attr("style","opacity:1;");
			$("#lcl_orign").unbind("click");
			//$("#lcl_orign").attr("onclick","addWareHouses('Origin');");
			$("#lcl_orign").click(function(){addWareHouses('Origin',maxrowexcced)});
			$("#originWarehouse").removeAttr("disabled");
		}
		else if(divid=='des_warehouse')
		{
			$("#lcl_des").attr("style","opacity:1;");
			$("#lcl_des").unbind("click");
			$("#lcl_des").click(function(){addWareHouses('Destination',maxrowexcced)});
			//$("#lcl_des").attr("onclick","addWareHouses('Destination');");
			$("#desWarehouse").removeAttr("disabled");
		}
		else if(divid=='haulage_warehouse')
		{
			$("#haulageWarehouse").attr("disabled",false);
			$("#haulagePricingModel").attr("disabled",false);
			
			var items = $("#haulageWarehouse option").length;
			var  items =items-1;
			var len=0;
			if(items!=0 && items!='')
			{
				var values = [];
				$('#haulageWarehouse option').each(function() { 
				    values.push( $(this).attr('value') );
				});
				
				var len=values.length;
				if(parseInt(len)>1)
				{
					$("#haulage_add").attr("style","opacity:1;");
					$("#haulage_add").unbind("click");
					//$("#haulage_add").attr("onclick","addWareHousesHaulage();");
					$("#haulage_add").click(function(){addWareHousesHaulageNew()});
				}
				else
				{
					$("#haulage_add").attr("style","opacity:0.4;");
					$("#haulage_add").unbind("click");
					//$("#haulageWarehouse").attr("disabled",true);
					//$("#haulagePricingModel").attr("disabled",true);
				}
			}
			else
			{
				$("#haulage_add").attr("style","opacity:0.4;");
				$("#haulage_add").unbind("click");
				//$("#haulageWarehouse").attr("disabled",true);
				//$("#haulagePricingModel").attr("disabled",true);
			}			
		}	
	
	});
}

function delete_ware_houseInfo(newvalue,div_id,divFlag,maxrowexcced)
{
	if(divFlag=='Origin')
	{
            var rowCount = $('#originTable tr').length;

            var deleteWareHouseOriginArr=$("#deleteWareHouseOriginArr").attr('value');
            var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');

            //var res_ary = deleteWareHouseArr.split("_");

            if(deleteWareHouseOriginArr=='')
            {
                //var newvalue=res_ary[0]+'_'+res_ary[3];
                $("#deleteWareHouseOriginArr").attr('value',newvalue);
                $("#deleteWareHouseDivIdOriginArr").attr('value',div_id);
            }
            else
            {
                var res_ary_new = deleteWareHouseOriginArr.split(";");
                var add_ware=jQuery.inArray(newvalue, res_ary_new);
                if(add_ware!='-1')
                {			
                    res_ary_new = $.grep(res_ary_new, function(value) {
                        return newvalue != value;
                    });

                    var value1=res_ary_new.join(';');
                    $("#deleteWareHouseOriginArr").attr('value',value1);
                }
                else
                {
                    var value1=deleteWareHouseOriginArr+';'+newvalue;
                    $("#deleteWareHouseOriginArr").attr('value',value1);
                } 

                var res_ary_new_div = deleteWareHouseDivIdOriginArr.split(";");
                var add_ware_divid=jQuery.inArray(div_id, res_ary_new_div);
                if(add_ware_divid!='-1')
                {			
                    res_ary_new_div = $.grep(res_ary_new_div, function(value) {
                        return div_id != value;
                    });

                    var valuediv1=res_ary_new_div.join(';');
                    $("#deleteWareHouseDivIdOriginArr").attr('value',valuediv1);
                }
                else
                {
                    var valuediv1=deleteWareHouseDivIdOriginArr+';'+div_id;
                    $("#deleteWareHouseDivIdOriginArr").attr('value',valuediv1);
                }
            }
            if(rowCount>0)
            {
                for(t=1;t<rowCount;++t)
                {
                    newdivid="origin_data_"+t;
                    $("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
                }
            }

            var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');
            if(deleteWareHouseDivIdOriginArr!='')
            {
                var res_ary_new_div = deleteWareHouseDivIdOriginArr.split(";");
                var res_ary_new_div_len = res_ary_new_div.length;
                if(res_ary_new_div_len>0)
                { 
                    for(t=0;t<res_ary_new_div_len;++t)
                    {
                        var div_id=res_ary_new_div[t];
                        $("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
                    }
                }
            }
            var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');
            if(deleteWareHouseDivIdOriginArr!='')
            {
                //$("#origin_remove_button").attr("onclick","delete_ware_house_data('"+divFlag+"')");
                $("#origin_remove_button").click(function(){delete_ware_house_data(divFlag,maxrowexcced)});
                $("#origin_remove_button").attr("style","opacity:1;");
            }
            else
            {
                $("#origin_remove_button").unbind("click");
                //$("#origin_remove_button").attr("onclick","");
                $("#origin_remove_button").attr("style","opacity:0.4;");
            }
	}
	else if(divFlag=='Destination')
	{
		var rowCount = $('#desTable tr').length;
		
		var deleteWareHouseDesArr=$("#deleteWareHouseDesArr").attr('value');
		var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
		
		//var res_ary = deleteWareHouseArr.split("_");
		
		if(deleteWareHouseDesArr=='')
		{
			//var newvalue=res_ary[0]+'_'+res_ary[3];
			$("#deleteWareHouseDesArr").attr('value',newvalue);
			$("#deleteWareHouseDivIdDesArr").attr('value',div_id);
		}
		else
		{
			 var res_ary_new = deleteWareHouseDesArr.split(";");
			 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			if(add_ware!='-1')
			{			
				res_ary_new = $.grep(res_ary_new, function(value) {
				    return newvalue != value;
				});
								
				var value1=res_ary_new.join(';');
				$("#deleteWareHouseDesArr").attr('value',value1);
			}
			else
			{
				var value1=deleteWareHouseDesArr+';'+newvalue;
				$("#deleteWareHouseDesArr").attr('value',value1);
			}
			
			
			var res_ary_new_div = deleteWareHouseDivIdDesArr.split(";");
			 var add_ware_divid=jQuery.inArray(div_id, res_ary_new_div);
			if(add_ware_divid!='-1')
			{			
				res_ary_new_div = $.grep(res_ary_new_div, function(value) {
				    return div_id != value;
				});
								
				var valuediv1=res_ary_new_div.join(';');
				$("#deleteWareHouseDivIdDesArr").attr('value',valuediv1);
			}
			else
			{
				var valuediv1=deleteWareHouseDivIdDesArr+';'+div_id;
				$("#deleteWareHouseDivIdDesArr").attr('value',valuediv1);
			}
		}
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="des_data_"+t;
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				
			}
		}
		
		var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
	
		if(deleteWareHouseDivIdDesArr!='')
		{
			var res_ary_new_div = deleteWareHouseDivIdDesArr.split(";");
			var res_ary_new_div_len = res_ary_new_div.length;
			if(res_ary_new_div_len>0)
			{
				
				for(t=0;t<res_ary_new_div_len;++t)
				{
					var div_id=res_ary_new_div[t];
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
		if(deleteWareHouseDivIdDesArr!='')
		{
			//$("#des_remove_button").attr("onclick","delete_ware_house_data('"+divFlag+"')");
			$("#des_remove_button").click(function(){delete_ware_house_data(divFlag,maxrowexcced)});
			$("#des_remove_button").attr("style","opacity:1;");
		}
		else
		{
			$("#des_remove_button").unbind("click");
			//$("#des_remove_button").attr("onclick","");
			$("#des_remove_button").attr("style","opacity:0.4;");
		}
	}
}
function delete_ware_house_data(divFlag,maxrowexcced)
{ 
    if(divFlag=='Origin')
    {
        var idOriginWarehouse = $("#idOriginWarehouse").attr('value');
        var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');
        var deleteWareHouseOriginArr=$("#deleteWareHouseOriginArr").attr('value');
        var res_ary_new = idOriginWarehouse.split(";");
        var res_ary_new_delete = deleteWareHouseOriginArr.split(";");
        var res_ary_new_div_len = res_ary_new_delete.length;

        if(res_ary_new_div_len>0)
        {
            for(t=0;t<res_ary_new_div_len;++t)
            {
                haulage_value=res_ary_new_delete[t];
                res_ary_new = $.grep(res_ary_new, function(value) {
                    return haulage_value != value;
                });
            }
        }
        var new_arr=res_ary_new.join(';');

        var delete_ware_house_divId_arr = deleteWareHouseDivIdOriginArr.split(";");
        if(delete_ware_house_divId_arr.length>0)
        {
            for(var i=0; i<delete_ware_house_divId_arr.length; i++) 
            {
                var div_id=delete_ware_house_divId_arr[i];
                $("#"+div_id).html("");
                $("#"+div_id).attr('style','display:none;');		
            }
        }

        $("#deleteWareHouseDivIdOriginArr").attr('value','');
        $("#deleteWareHouseOriginArr").attr('value',''); 
        $("#origin_remove_button").attr("onclick","");
        $("#origin_remove_button").attr("style","opacity:0.4;"); 
        $("#idOriginWarehouse").attr('value',new_arr); 
    }
    else if(divFlag=='Destination')
    {
        var idDesWarehouse = $("#idDesWarehouse").attr('value');
        var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
        var deleteWareHouseDesArr=$("#deleteWareHouseDesArr").attr('value');
        var res_ary_new = idDesWarehouse.split(";");
        var res_ary_new_delete = deleteWareHouseDesArr.split(";");
        var res_ary_new_div_len = res_ary_new_delete.length;

        if(res_ary_new_div_len>0)
        {
            for(t=0;t<res_ary_new_div_len;++t)
            {
                haulage_value=res_ary_new_delete[t];
                res_ary_new = $.grep(res_ary_new, function(value) {
                    return haulage_value != value;
                });
            }
        }
        var new_arr_des=res_ary_new.join(';');

        var delete_ware_house_divId_arr = deleteWareHouseDivIdDesArr.split(";");
        if(delete_ware_house_divId_arr.length>0)
        {		
            for(var i=0; i<delete_ware_house_divId_arr.length; i++) 
            {
                var div_id=delete_ware_house_divId_arr[i];		
                $("#"+div_id).html("");
                $("#"+div_id).attr('style','display:none;');		
            }
        }
        $("#deleteWareHouseDivIdDesArr").attr('value','');
        $("#deleteWareHouseDesArr").attr('value','');
        $("#idDesWarehouse").attr('value',new_arr_des); 
        $("#des_remove_button").attr("onclick","");
        $("#des_remove_button").attr("style","opacity:0.4;");
    }

    var desWarehouseValue = $("#idDesWarehouse").attr('value');
    var originWarehouseValue = $("#idOriginWarehouse").attr('value');

    var downloadType = $("#downloadType").val();

    var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
    var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
    var res_ary_originwarhousevalue = originWarehouseValueTotal.split(";");
    var res_ary_deswarhousevalue = desWarehouseValueTotal.split(";");
    var deswarehouselen=res_ary_deswarhousevalue.length;
    var originwarehouselen=res_ary_originwarhousevalue.length;	
    if(originwarehouselen>0 && deswarehouselen>0)
    {
       // var totalCount=parseInt(originwarehouselen)*parseInt(deswarehouselen);
        var totalCount = getDownloadableWarehouseCount(res_ary_originwarhousevalue,res_ary_deswarhousevalue);
        if(totalCount<=maxrowexcced)
        {
            if(downloadType=='1')
            {
                $('#downloadType').removeAttr('disabled');
                updatedNote(maxrowexcced);
            }
            else if(downloadType=='2')
            { 
                var totaltime=(__TIME_PER_ROW_CREATE__*totalCount);
                if(totaltime>60)
                {
                    var minutes = Math.floor(totaltime/60);
                    var secondsleft = totaltime%60;
                    secondsleft = Math.ceil(secondsleft);
                    if(minutes=='1')
                    {
                        var minStr=minutes+" minute"; 
                    }
                    else
                    {
                        var minStr=minutes+" minutes"; 
                    }
                    if(secondsleft=='1')
                    {
                        var secstr=secondsleft+" second";
                    }
                    else
                    {
                        var secstr=Math.ceil(secondsleft)+" seconds";
                    } 
                    var timestr = minStr+" "+secstr;
                }
                else
                {
                    var timetaken=Math.ceil(totaltime)
                    if(timetaken=='1')
                    {
                        var timestr=timetaken+" second";
                    }
                    else
                    {
                        var timestr=timetaken+" seconds";
                    }
                }
                $("#total_row_excced_time").html(timestr);
                $("#row_excced_data").attr("style","display:none");
                $("#row_excced_data_1").attr("style","display:none");
                $("#row_excced_data_2").attr("style","display:block");
                $("#total_row_excced_1").html(number_format_js(totalCount));
                $("#download_button").click(function(){downloadFormSubmit()});
                $("#download_button").attr("style","opacity:1;");
            }
        }
        else
        {
            if(downloadType=='1')
            {
                $('#downloadType').removeAttr('disabled');
                updatedNote(maxrowexcced);
            }
            else if(downloadType=='2')
            {
                $("#row_excced_data_1").attr("style","display:none");
                $("#row_excced_data").attr("style","display:block");
                $("#row_excced_data_2").attr("style","display:none");
                $("#total_row_excced").html(number_format_js(totalCount));
            }
        } 
    }
    if(desWarehouseValue=='' || originWarehouseValue=='')
    {
        $("#download_button").unbind("click");
        //$("#download_button").attr("onclick","");
        $("#download_button").attr("style","opacity:0.4;");

        $("#row_excced_data").attr("style","display:none");
        $("#row_excced_data_1").attr("style","display:block");
        $("#row_excced_data_2").attr("style","display:none");
    }
}

function downloadFormSubmit()
{
    $("#download_button").unbind("click");
    $("#download_button").attr("style","opacity:0.4;");
    
    //$("#loader").attr("style","display:block");
            
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",$("#bulkExportData").serialize(),function(result){
        var result_ary = result.split("||||");
        if(result_ary[0]=='SUCCESS')
        {
            document.bulkExportData.action = __JS_ONLY_SITE_BASE__+"/downloadExportData.php";
            document.bulkExportData.method = "post"
            document.bulkExportData.submit();
           // $("#loader").attr("style","display:none;");
        }
        else
        {
            $("#download_button").unbind("click");
            $("#download_button").attr("style","opacity:0.4;");
            $("#row_excced_data").attr("style","display:block");
            $("#total_row_excced").html(result_ary[1]);
        }
    });
}

function submit_upload_file()
{
	//document.uploadFile.action = __JS_ONLY_SITE_BASE__+"/bulkDataExportLCL.php";
	//document.uploadFile.method = "post"
	$("#loader").attr('style','display:block;');
	document.uploadFile.submit();
	addPopupScrollClass('success_msg');
	
}

function edit_registered_company_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	show_forwarder_company_cust_details(idForwarder);
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",{id:idForwarder,mode:'EDIT_REGISTERED_COMPANY_DETAILS'},function(result){
		$("#registered_company_details").html(result);
		$("#lower_edit_link").attr('style','display:none;');
		$("#loader").attr('style','display:none;');
	});
}
function show_registered_company_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",{id:idForwarder,mode:'DISPLAY_REGISTERED_COMPANY_DETAILS'},function(result){
		
		$("#registered_company_details").html(result);
		$("#lower_edit_link").attr('style','display:');
		$("#loader").attr('style','display:none;');
	});
}

function submit_registered_company_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	var value=decodeURIComponent($("#updateRegistComapnyForm").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",value,function(result){
		
		if(result=='SUCCESS')
		{
			show_registered_company_details(idForwarder);
			show_forwarder_top_messges();
		}
		else
		{
			$("#registered_company_details").html(result);
		}
		$("#loader").attr('style','display:none;');
	});
}

function edit_forwarder_company_cust_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	show_registered_company_details(idForwarder);
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",{id:idForwarder,mode:'EDIT_FORWARWARDER_COMPANY_DETAILS'},function(result){
		$("#forwarder_company_details").html(result);
		$("#upper_edit_link").attr('style','display:none;');
		$("#loader").attr('style','display:none;');
	});
}

function show_forwarder_company_cust_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",{id:idForwarder,mode:'DISPLAY_FORWARWARDER_COMPANY_DETAILS'},function(result){
		
		$("#forwarder_company_details").html(result);
		$("#upper_edit_link").attr('style','display:');
		$("#loader").attr('style','display:none;');
	});
}

function submit_forwarder_company_cust_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",$("#updateRegistComapnyForm").serialize(),function(result){
		
		if(result=='SUCCESS')
		{
			show_forwarder_company_cust_details(idForwarder);
			show_forwarder_top_messges();
		}
		else
		{
			$("#Error").html(result);
		}
		$("#loader").attr('style','display:none;');
	});
}


function edit_forwarder_bank_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bankDetails.php",{id:idForwarder,mode:'EDIT_FORWARWARDER_BANK_DETAILS'},function(result){
		$("#forwarder_bank_details").html(result);
		$("#upper_edit_link").attr('style','display:none;');
		$("#loader").attr('style','display:none;');
	});
}

function show_forwarder_bank_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bankDetails.php",{id:idForwarder,mode:'DISPLAY_FORWARWARDER_BANK_DETAILS'},function(result){
		
		$("#forwarder_bank_details").html(result);
		$("#upper_edit_link").attr('style','display:;');
		$("#loader").attr('style','display:none;');
	});
}
function submit_forwarder_bank_details_pre_check(idForwarder)
{
	var old_currency = $("#szCurrency_hidden").attr('value');
	var new_currency = $("#szCurrency").attr('value');
	
	old_currency = parseInt(old_currency);
	new_currency = parseInt(new_currency);
	
	if((old_currency == new_currency))
	{
		submit_forwarder_bank_details(idForwarder);
	}
	else if(old_currency>0)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bankDetails.php",{id:idForwarder,mode:'SHOW_CURRENCY_CHANGE_NOTICE',old_currency:old_currency,new_currency:new_currency},function(result){
			
			$("#forwarder_company_div").attr('style','display:block;');
			$("#forwarder_company_div").html(result);		
	    });		
	}
	else
	{
		submit_forwarder_bank_details(idForwarder);
		//show_forwarder_top_messges();
	}
}
function open_currency_change_popup(idForwarder)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bankDetails.php",{id:idForwarder,mode:'DISPLAY_CURRENCY_CHANGE_POPUP'},function(result){
		
			$("#contactPopup").attr('style','display:block;');
			$("#contactPopup").html(result);		
	    });	
}
function submit_forwarder_bank_details(idForwarder)
{
	$("#loader").attr('style','display:block;');
	$("#forwarder_company_div").attr('style','display:none;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bankDetails.php",$("#updateRegistBankForm").serialize(),function(result){
		
		if($.trim(result)=='SUCCESS')
		{
			show_forwarder_bank_details(idForwarder);
			show_forwarder_top_messges();
		}
		else
		{
			$("#forwarder_bank_details").html(result);
		}
		$("#loader").attr('style','display:none;');
	});
}
function delete_forwarder_logo(idForwarder)
{
	
	$("#upload_file_success").attr('style','display:none;');
	$("#forwarder_logo_delete_link").attr('style','display:none;');
	
	$("#szOldLogo").attr('value','');
	$("#szForwarderLogo_temp").attr('value','');
	$("#delete_logo_hidden").attr('value',1);
	
/*
	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",{id:idForwarder,mode:'DELETE_FORWARDER_LOGO'},function(result){		
		if(result=='SUCCESS')
		{
			edit_forwarder_company_cust_details(idForwarder);
		}
		else
		{
			$("#forwarder_company_details").html(result);
		}
		$("#loader").attr('style','display:none;');
	});
	*/
}

function addWareHousesHaulage()
{
	var haulageCountry = $("#haulageCountry").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
	var count = $("#haulageWarehouseAdded").attr('value');
	var res_ary = haulageWarehouse.split("_");
		
	if(haulageWarehouse!="")
	{
		if(idhaulageWarehouse=='')
		{
			var newvalue=res_ary[0]+'_'+res_ary[3];
			$("#idhaulageWarehouse").attr('value',newvalue);
		}
		else
		{
			var newvalue=res_ary[0]+'_'+res_ary[3];
			
			 var res_ary_new = idhaulageWarehouse.split(";");
			 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			if(add_ware!='-1')
			{
				return false;
			}
			
			var value1=idhaulageWarehouse+';'+newvalue;
			$("#idhaulageWarehouse").attr('value',value1);
		}
		var newcount=parseInt(count)+1;
		
		$("#haulageWarehouseAdded").attr('value',newcount);
		var div_id="haulage_data_"+count;
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{haulageCountry:haulageCountry,haulageWarehouse:haulageWarehouse,flag:'haulageAdd',count:newcount},function(result){
		
		$("#"+div_id).html(result);
		
		 $("#"+div_id).attr("onclick","delete_ware_haulage('"+newvalue+"','"+div_id+"','Haulage');");
		var divid="haulage_data_"+newcount;
		$('#haulageTable').append('<tr id='+divid+' onclick=""></tr>');
			var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
			
			if(haulageWarehouseValue!='')
			{
				$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
				$("#download_button").attr("style","opacity:1;");
			}
			else
			{
				$("#download_button").attr("onclick","");
				$("#download_button").attr("style","opacity:0.4;");
			}
			
		});
	}
	else
	{
		if(haulageCountry!='')
		{
			var items = $("#haulageWarehouse option").length;
			var  items =items-1;
			if(items!=0 && items!='')
			{
				var values = [];
				$('#haulageWarehouse option').each(function() { 
				    values.push( $(this).attr('value') );
				});
				var arrlen=values.length;
				for(i=1;i<arrlen;++i)
				{
					var newcount=$("#haulageWarehouseAdded").attr('value');
					var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
					var res_ary = values[i].split("_");
				
					if(idhaulageWarehouse=='')
					{
						var newValueWare=values[i];
						var new_value_ware=res_ary[0]+'_'+res_ary[3];
						var value1=new_value_ware;
						var newvalue=res_ary[0]+'_'+res_ary[3];
						//$("#idOriginWarehouse").attr('value',value1);
			 		}
			 		else
			 		{
			 			var newvalue=res_ary[0]+'_'+res_ary[3];
			 			var res_ary_new = idhaulageWarehouse.split(";");
			 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
			 			if(add_ware!='-1')
						{
							var newValueWare='';
						}
						else
						{
							var newValueWare=values[i];
							var new_value_ware=res_ary[0]+'_'+res_ary[3];
							var value1=idhaulageWarehouse+';'+newvalue;
							//$("#idOriginWarehouse").attr('value',value1);
						}					
			 		}
					if(newValueWare!='undefined' && newValueWare!='')
					{
							div_id="haulage_data_"+newcount;
							var haulage="haulage";
							document.getElementById(div_id).innerHTML='<th onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[2]+'</th><td onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[1]+'</td>';
							
							
							//$("#"+div_id).attr("onclick","delete_ware_haulage('"+newvalue+"','"+div_id+"','haulage');");
							newcount=parseInt(newcount)+1;
							var divid="haulage_data_"+newcount;
							$('#haulageTable').append('<tr id='+divid+' onclick=""></tr>');
							if(value1!='undefined' && value1!='')
								$("#idhaulageWarehouse").attr('value',value1);
							
							$("#haulageWarehouseAdded").attr('value',newcount);
							
						}
					}
				}
				var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
				
				if(haulageWarehouseValue!='')
				{
					$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
					$("#download_button").attr("style","opacity:1;");
				}
				else
				{
					$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
			}
		else
		{
			$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{flag:'HaulageAddAll'},function(result){
				
				$("#haulageTable").html(result);
				
				var haulageWarehouseValue = $("#idhaulagesWarehouse").attr('value');
				
				if(haulageWarehouseValue!='')
				{
					$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
					$("#download_button").attr("style","opacity:1;");
				}
				else
				{
					$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
			});
		}
 	}
			
}


/*function delete_ware_haulage(newvalue,div_id,divFlag)
{
	$("#haulage_remove_button").attr("onclick","delete_ware_house_data_haulage('"+newvalue+"','"+div_id+"','"+divFlag+"')");
	$("#haulage_remove_button").attr("style","opacity:1;");	
}
function delete_ware_house_data_haulage(newvalue,div_id,divFlag)
{
		var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
		 var res_ary_new = idhaulageWarehouse.split(";");
		 var new_arr='';
		for(var i=0; i<res_ary_new.length; i++) 
		{
	        if(res_ary_new[i] != newvalue) 
	        {
	        	//alert(new_arr);
	        	if(new_arr=='')
	        	{
	        		new_arr = res_ary_new[i];
	        	}
	        	else
	        	{
	        		new_arr =new_arr+';'+res_ary_new[i];
	        	}
	        	//alert(new_arr);
	        }
  		
  		//alert(new_arr);
		$("#idhaulageWarehouse").attr('value',new_arr);
		document.getElementById(div_id).innerHTML='';
		$("#"+div_id).attr('style','display:none;');	
		
		
		$("#haulage_remove_button").attr("onclick","");
		$("#haulage_remove_button").attr("style","opacity:0.4;");	
	}
	var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
	
	if(haulageWarehouseValue=='')
	{
		$("#download_button").attr("onclick","");
		$("#download_button").attr("style","opacity:0.4;");
	}
}*/

function downloadHaulageFormSubmit()
{
        //$("#loader").attr("style","display:block");	
        $("#download_button").unbind("click");		
        $("#download_button").attr("style","opacity:0.4;");	
	document.bulkExportData.action = __JS_ONLY_SITE_BASE__+"/downloadHaulageExportData.php";
	document.bulkExportData.method = "post"
	document.bulkExportData.submit();
        //$("#loader").attr("style","display:none");
}

function stopUpload(result,idForwarder)
{	
	$("#f1_upload_form").attr('style','display:none;');
	 result_ary = result.split("||||");
	 if(result_ary[0]=='SUCCESS')
     {
     	//show_forwarder_company_cust_details(idForwarder) ;
     	$("#f1_upload_process").attr('style','display:none;');
     	var inner_html = '<input type="hidden" id="szForwarderLogo_temp" name="updateForwarderComapnyAry[szForwarderLogo]" value="'+result_ary[2]+'">' ;
		inner_html +='<img src="'+result_ary[1]+'">';
     	$("#upload_file_success").html(inner_html);
     	$("#f1_upload_form").attr('style','display:none;');
     	$("#upload_file_success").css('display','block');
     	$("#forwarder_logo_delete_link").attr('style','display:block;');
     }
     else
     {
     	var upload_new_file = result_ary[2];
     	var file_upload_text = result_ary[4];
     	var logo_tooltip = result_ary[3];
     	
     	$("#f1_upload_process").attr('style','display:none;');
     	var error_str = '<div id="regError" class="errorBox "><div class="header"></div>' ;
     		error_str +='<div id="regErrorList"><ul>'+result_ary[1]+'</ul></div></div>' ;
     	var form_str = '<div class="file"><input type="file" id="fileUpload" name="updateRegistComapnyAry[szForwarderLogo]" onblur="closeTip(\'company_logo\');" onfocus="openTip(\'company_logo\');" />' ;
		form_str = form_str +'<span class="button">'+upload_new_file+'</span></div><div class="field-alert" style="width:180px;"><div id="company_logo" style="display:none;">'+logo_tooltip+'</div></div>' ;
		form_str = form_str + '<a href="javascript:void(0);" onclick="$(\'#updateLogoForm\').submit();" class="button1"><span>'+file_upload_text+'</span></a>';
     	$("#f1_upload_form").html(error_str+form_str);
     	$('#fileUpload').bind('change focus click', function() {$(this).fileName()});
     	$("#f1_upload_form").attr('style','display:block;');
     }
     return true;   
}
function startUpload()
{
	$("#f1_upload_form").attr('style','display:none;');
	$("#f1_upload_process").attr('style','display:block;');
	return true;
}
function  show_upload_file_form()
{
	$("#f1_upload_form").attr('style','display:block;');
	$("#upload_file").css('display','block');
}
function addWareHousesHaulage()
{
	var haulageCountry = $("#haulageCountry").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
	var count = $("#haulageWarehouseAdded").attr('value');
	var res_ary = haulageWarehouse.split("_");
	
	
	if(haulageWarehouse!="")
	{
		if(idhaulageWarehouse=='')
		{
			var newvalue=res_ary[0]+'_'+res_ary[3];
			$("#idhaulageWarehouse").attr('value',newvalue);
		}
		else
		{
			var newvalue=res_ary[0]+'_'+res_ary[3];
			
			 var res_ary_new = idhaulageWarehouse.split(";");
			 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			if(add_ware!='-1')
			{
				return false;
			}
			
			var value1=idhaulageWarehouse+';'+newvalue;
			$("#idhaulageWarehouse").attr('value',value1);
		}
		var newcount=parseInt(count)+1;
		
		$("#haulageWarehouseAdded").attr('value',newcount);
		var div_id="haulage_data_"+count;
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{haulageCountry:haulageCountry,haulageWarehouse:haulageWarehouse,flag:'haulageAdd',count:newcount},function(result){
		
		$("#"+div_id).html(result);
		
		// $("#"+div_id).attr("onclick","delete_ware_haulage('"+newvalue+"','"+div_id+"','Haulage');");
		 $("#"+div_id).click(function(){delete_ware_haulage(newvalue,div_id,'Destination')});
		var divid="haulage_data_"+newcount;
		$('#haulageTable').append('<tr id='+divid+' onclick=""></tr>');
			var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
			
			if(haulageWarehouseValue!='')
			{
				//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
				$("#download_button").click(function(){downloadHaulageFormSubmit()});
				$("#download_button").attr("style","opacity:1;");
			}
			else
			{
				$("#download_button").unbind("click");
				//$("#download_button").attr("onclick","");
				$("#download_button").attr("style","opacity:0.4;");
			}
			
		});
	}
	else
	{
		if(haulageCountry!='')
		{
			var items = $("#haulageWarehouse option").length;
			var  items =items-1;
			if(items!=0 && items!='')
			{
				var values = [];
				$('#haulageWarehouse option').each(function() { 
				    values.push( $(this).attr('value') );
				});
				var arrlen=values.length;
				for(i=1;i<arrlen;++i)
				{
					var newcount=$("#haulageWarehouseAdded").attr('value');
					var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
					var res_ary = values[i].split("_");
					if(idhaulageWarehouse=='')
					{
						var newValueWare=values[i];
						var new_value_ware=res_ary[0]+'_'+res_ary[3];
						var value1=new_value_ware;
						var newvalue=res_ary[0]+'_'+res_ary[3];
						//$("#idOriginWarehouse").attr('value',value1);
			 		}
			 		else
			 		{
			 			var newvalue=res_ary[0]+'_'+res_ary[3];
			 			var res_ary_new = idhaulageWarehouse.split(";");
			 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
			 			if(add_ware!='-1')
						{
							var newValueWare='';
						}
						else
						{
							var newValueWare=values[i];
							var new_value_ware=res_ary[0]+'_'+res_ary[3];
							var value1=idhaulageWarehouse+';'+newvalue;
							//$("#idOriginWarehouse").attr('value',value1);
						}					
			 		}
					if(newValueWare!='undefined' && newValueWare!='')
					{
							div_id="haulage_data_"+newcount;
							var haulage="haulage";
							$("#"+div_id).html('<th onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[2]+'</th><td onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[1]+'</td>');

							
							//document.getElementById(div_id).innerHTML='<td>'+res_ary[2]+'</td><td>'+res_ary[1]+'</td>';
							
							
							//$("#"+div_id).attr("onclick","delete_ware_haulage('"+newvalue+"','"+div_id+"','haulage');");
							newcount=parseInt(newcount)+1;
							var divid="haulage_data_"+newcount;
							$('#haulageTable').append('<tr id='+divid+' onclick=""></tr>');
							if(value1!='undefined' && value1!='')
								$("#idhaulageWarehouse").attr('value',value1);
							
							$("#haulageWarehouseAdded").attr('value',newcount);
							
						}
					}
				}
				var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
				
				if(haulageWarehouseValue!='')
				{
					//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
					$("#download_button").click(function(){downloadHaulageFormSubmit()});
					$("#download_button").attr("style","opacity:1;");
				}
				else
				{
					$("#download_button").unbind("click");
					//$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
			}
		else
		{
			$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{flag:'HaulageAddAll'},function(result){
				
				$("#haulageTable").html(result);
				
				var haulageWarehouseValue = $("#idhaulagesWarehouse").attr('value');
				
				if(haulageWarehouseValue!='')
				{
					//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
					$("#download_button").click(function(){downloadHaulageFormSubmit()});
					$("#download_button").attr("style","opacity:1;");
				}
				else
				{
					$("#download_button").unbind("click");
					//$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
			});
		}
 	}
			
}


function delete_ware_haulage(newvalue,div_id,divFlag)
{
		var rowCount = $('#haulageTable tr').length;
		
		var deleteWareHouseArr=$("#deleteWareHouseArr").attr('value');
		var deleteWareHouseDivIdArr=$("#deleteWareHouseDivIdArr").attr('value');
		
		//var res_ary = deleteWareHouseArr.split("_");
		
		if(deleteWareHouseArr=='')
		{
			//var newvalue=res_ary[0]+'_'+res_ary[3];
			$("#deleteWareHouseArr").attr('value',newvalue);
			$("#deleteWareHouseDivIdArr").attr('value',div_id);
		}
		else
		{
			 var res_ary_new = deleteWareHouseArr.split(";");
			 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			if(add_ware!='-1')
			{			
				res_ary_new = $.grep(res_ary_new, function(value) {
				    return newvalue != value;
				});
								
				var value1=res_ary_new.join(';');
				$("#deleteWareHouseArr").attr('value',value1);
			}
			else
			{
				var value1=deleteWareHouseArr+';'+newvalue;
				$("#deleteWareHouseArr").attr('value',value1);
			}
			
			
			var res_ary_new_div = deleteWareHouseDivIdArr.split(";");
			 var add_ware_divid=jQuery.inArray(div_id, res_ary_new_div);
			if(add_ware_divid!='-1')
			{			
				res_ary_new_div = $.grep(res_ary_new_div, function(value) {
				    return div_id != value;
				});
								
				var valuediv1=res_ary_new_div.join(';');
				$("#deleteWareHouseDivIdArr").attr('value',valuediv1);
			}
			else
			{
				var valuediv1=deleteWareHouseDivIdArr+';'+div_id;
				$("#deleteWareHouseDivIdArr").attr('value',valuediv1);
			}
		}
		
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="haulage_data_"+t;
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
		}
		
		var deleteWareHouseDivIdArr=$("#deleteWareHouseDivIdArr").attr('value');
		if(deleteWareHouseDivIdArr!='')
		{
			var res_ary_new_div = deleteWareHouseDivIdArr.split(";");
			var res_ary_new_div_len = res_ary_new_div.length;
			if(res_ary_new_div_len>0)
			{
				
				for(t=0;t<res_ary_new_div_len;++t)
				{
					var div_id=res_ary_new_div[t];
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		
		var deleteWareHouseDivIdArr=$("#deleteWareHouseDivIdArr").attr('value');
	if(deleteWareHouseDivIdArr!='')
	{		
	//	$("#haulage_remove_button").attr("onclick","delete_ware_house_data_haulage()");
		$("#haulage_remove_button").click(function(){delete_ware_house_data_haulage()});
		$("#haulage_remove_button").attr("style","opacity:1;");	
	}
	else
	{
		$("#download_button").unbind("click");
		//$("#haulage_remove_button").attr("onclick","");
		$("#haulage_remove_button").attr("style","opacity:0.4;");	
	}
}
function delete_ware_house_data_haulage()
{
		var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
		var deleteWareHouseDivIdArr=$("#deleteWareHouseDivIdArr").attr('value');
		var deleteWareHouseArr=$("#deleteWareHouseArr").attr('value');
		var res_ary_new = idhaulageWarehouse.split(";");
		var res_ary_new_delete = deleteWareHouseArr.split(";");
		var res_ary_new_div_len = res_ary_new_delete.length;
		
		if(res_ary_new_div_len>0)
		{
			for(t=0;t<res_ary_new_div_len;++t)
			{
				haulage_value=res_ary_new_delete[t];
				res_ary_new = $.grep(res_ary_new, function(value) {
				    return haulage_value != value;
					});
			}
		}
		var new_arr=res_ary_new.join(';');
		
		var delete_ware_house_divId_arr = deleteWareHouseDivIdArr.split(";");
		if(delete_ware_house_divId_arr.length>0)
		{
			for(var i=0; i<delete_ware_house_divId_arr.length; i++) 
			{
				var div_id=delete_ware_house_divId_arr[i];		
				$("#"+div_id).html("");
				$("#"+div_id).attr('style','display:none;');		
			}
		}
		$("#deleteWareHouseDivIdArr").attr('value','');
		$("#deleteWareHouseArr").attr('value','');
		
		$("#haulage_remove_button").attr("onclick","");
		$("#haulage_remove_button").attr("style","opacity:0.4;");	
		
		$("#idhaulageWarehouse").attr('value',new_arr);
		var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
		
		if(haulageWarehouseValue=='')
		{
			$("#download_button").unbind("click");
			//$("#download_button").attr("onclick","");
			$("#download_button").attr("style","opacity:0.4;");
		}
		
		var downloadType = $("#downloadType").val();
		var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
		var idhaulageWarehouse_arr = idhaulageWarehouse.split(";");
		var idhaulageWarehouselen=idhaulageWarehouse_arr.length;
		var maxrowexcced  = $("#maxrowsexcced").attr('value');
		if(idhaulageWarehouselen>0)
		{
			var totalCount=parseInt(idhaulageWarehouselen);
			if(totalCount<=maxrowexcced)
			{
				if(downloadType=='1')
				{
					$('#downloadType').removeAttr('disabled');
					updatedNoteHaulage(maxrowexcced);
				}
			}
			else
			{
				if(downloadType=='1')
				{
					$('#downloadType').removeAttr('disabled');
					updatedNoteHaulage(maxrowexcced);
				}
			}
		}
}

function downloadHaulageFormSubmit()
{
    $("#loader").attr("style","display:block");	
    $("#download_button").unbind("click");		
    $("#download_button").attr("style","opacity:0.4;");
    document.bulkExportData.action = __JS_ONLY_SITE_BASE__+"/downloadHaulageExportData.php";
    document.bulkExportData.method = "post"
    document.bulkExportData.submit();
    $("#loader").attr("style","display:none");	
}

function submit_upload_haulage_file()
{
	$("#loader").attr('style','display:block;');
	document.uploadHaulageFile.submit();
	
}
function show_forwarder_warehouse(idForwarder,idCountry,divid,mode,iWarehouseType)
{
	$("#obo_cc_getdata_button").unbind("click");
		
	//$("#obo_cc_getdata_button").attr('onclick','');
	$("#obo_cc_getdata_button").attr("style","opacity:0.4;"); 
	
	$("#obo_cc_cc_getdata_button").unbind("click");
        
        var szFromPage = "";
        if($("#szFromPage").length)
        {
            szFromPage = $("#szFromPage").val();
        }
		
	//$("#obo_cc_cc_getdata_button").attr('onclick','');
	$("#obo_cc_cc_getdata_button").attr("style","opacity:0.4;"); 
	if(mode=='ORIGIN_WHS_COUNTRY')
	{
            $("#idOriginWarehouse").attr('disabled','disabled');
            $("#szOriginCity").attr('disabled','disabled');
	}
	else
	{
            $("#idDestinationWarehouse").attr('disabled','disabled');
            $("#szDestinationCity").attr('disabled','disabled');
	}
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",{idForwarder:idForwarder,idCountry:idCountry,mode:mode,iWarehouseType:iWarehouseType,szFromPage:szFromPage},function(result){
		
		result_ary = result.split("||||");
		if(mode=='ORIGIN_WHS_COUNTRY')
		{
			$("#origin_warehouse_span").html(result_ary[0]);
			$("#origin_city_span").html(result_ary[1]);
			var cont_id = parseInt(idCountry);
			if(cont_id>0)
			{
				$("#idOriginWarehouse").removeAttr('disabled');
				$("#szOriginCity").removeAttr('disabled');
			}
			else
			{
				$("#idOriginWarehouse").attr('disabled','disabled');
				$("#szOriginCity").attr('disabled','disabled');
			}
			//enable_get_data();	
		}
		else
		{
			$("#destination_warehouse_span").html(result_ary[0]);
			$("#destination_city_span").html(result_ary[1]);
			var cont_id = parseInt(idCountry);
			if(cont_id>0)
			{
				$("#idDestinationWarehouse").removeAttr('disabled');
				$("#szDestinationCity").removeAttr('disabled');
			}
			else
			{
				$("#idDestinationWarehouse").attr('disabled','disabled');
				$("#szDestinationCity").attr('disabled','disabled');
			}	
			//enable_get_data();
		}
	});
}
function show_forwarder_warehouse_by_city(idForwarder,city,divid,mode,iWarehouseType)
{
    $("#obo_cc_getdata_button").unbind("click");
    $("#obo_cc_getdata_button").attr("style","opacity:0.4;"); 

    $("#obo_cc_cc_getdata_button").unbind("click");
    $("#obo_cc_cc_getdata_button").attr("style","opacity:0.4;"); 

    var szFromPage = "";
    if($("#szFromPage").length)
    {
        szFromPage = $("#szFromPage").val();
    }
    if(mode=='ORIGIN_WHS_CITY')
    {
        var idCountry=$("#idOriginCountry").attr("value");
        $("#idOriginWarehouse").attr('disabled','disabled');
    }
    else
    {
        var idCountry=$("#idDestinationCountry").attr("value");
        $("#idDestinationWarehouse").attr('disabled','disabled');
    }
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",{idForwarder:idForwarder,idCountry:idCountry,mode:mode,szCity:city,iWarehouseType:iWarehouseType,szFromPage:szFromPage},function(result){
        $("#"+divid).html(result);
        //enable_get_data();
    }); 
} 

function addWareHousesCustomClearance(type,maxrowexcced)
{
	var type;
	if(type=='Origin')
	{
		var originCountry = $("#originCountry").attr('value');
		var originWarehouse = $("#originWarehouse").attr('value');
		var idOriginWarehouse = $("#idOriginWarehouse").attr('value');
		var count = $("#originWarehouseAdded").attr('value');
                var res_ary = originWarehouse.split("_"); 
		
		if(originWarehouse!="")
		{
                    if(idOriginWarehouse=='')
                    {
                        var newvalue=res_ary[0]+'_'+res_ary[3];
                        $("#idOriginWarehouse").attr('value',newvalue);
                    }
                    else
                    {
                        var newvalue=res_ary[0]+'_'+res_ary[3];

                        var res_ary_new = idOriginWarehouse.split(";");
                        var add_ware=jQuery.inArray(newvalue, res_ary_new);
                        if(add_ware!='-1')
                        {
                            return false;
                        }

                        var value1=idOriginWarehouse+';'+newvalue;
                        $("#idOriginWarehouse").attr('value',value1);
                    }
                    var newcount=parseInt(count)+1;
			
                    $("#originWarehouseAdded").attr('value',newcount);
                    var div_id="origin_data_"+count;
                    $.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{originCountry:originCountry,originWarehouse:originWarehouse,flag:'OriginAdd',count:newcount},function(result){
			
			$("#"+div_id).html(result); 
			 //$("#"+div_id).attr("onclick","delete_ware_houseInfo_customclearance('"+newvalue+"','"+div_id+"','Origin');");
                        $("#"+div_id).click(function(){delete_ware_houseInfo_customclearance(newvalue,div_id,'Origin',maxrowexcced)});
                        var divid="origin_data_"+newcount;
                        $('#originTable').append('<tr id='+divid+' onclick=""></tr>');
                        var desWarehouseValue = $("#idDesWarehouse").attr('value');
                        var originWarehouseValue = $("#idOriginWarehouse").attr('value');
                        if(desWarehouseValue!='' && originWarehouseValue!="")
                        {
                            //$("#download_button").attr("onclick","downloadFormSubmitCustomClearance()");
                            $("#download_button").click(function(){downloadFormSubmitCustomClearance()});
                            $("#download_button").attr("style","opacity:1;");
                        }
                        else
                        {
                            $("#download_button").unbind("click");
                            //$("#download_button").attr("onclick","");
                            $("#download_button").attr("style","opacity:0.4;");
                        } 
                        var downloadType = $("#downloadType").val();
                        if(downloadType=='2')
                        {
                            checkRowCountCC(maxrowexcced);
                        }
                        else if(downloadType=='1')
                        {
                            updatedNoteCC(maxrowexcced);
                        } 
                    });
		}
		else
		{
			if(originCountry!='')
			{
				var items = $("#originWarehouse option").length;
				var  items =items-1;
				
				if(items!=0 && items!='')
				{
					var values = [];
					$('#originWarehouse option').each(function() { 
					    values.push( $(this).attr('value') );
					});
					var arrlen=values.length;
					for(i=1;i<arrlen;++i)
					{
						var newcount=$("#originWarehouseAdded").attr('value');
						var idOriginWarehouse = $("#idOriginWarehouse").attr('value');
						var res_ary = values[i].split("_");
					
						if(idOriginWarehouse=='')
						{
							var newValueWare=values[i];
							var new_value_ware=res_ary[0]+'_'+res_ary[3];
							var value1=new_value_ware;
							var newvalue=res_ary[0]+'_'+res_ary[3];
							//$("#idOriginWarehouse").attr('value',value1);
				 		}
				 		else
				 		{
				 			var newvalue=res_ary[0]+'_'+res_ary[3];
				 			var res_ary_new = idOriginWarehouse.split(";");
				 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
				 			if(add_ware!='-1')
							{
								var newValueWare='';
							}
							else
							{
								var newValueWare=values[i];
								var new_value_ware=res_ary[0]+'_'+res_ary[3];
								var value1=idOriginWarehouse+';'+newvalue;
								//$("#idOriginWarehouse").attr('value',value1);
							}					
				 		}
						if(newValueWare!='undefined' && newValueWare!='')
						{
								div_id="origin_data_"+newcount;
								var Origin ="Origin";
								$("#"+div_id).html('<th onclick="delete_ware_houseInfo_customclearance(\''+newvalue+'\',\''+div_id+'\',\''+Origin+'\',\''+maxrowexcced+'\')">'+res_ary[2]+'</th><td onclick="delete_ware_houseInfo_customclearance(\''+newvalue+'\',\''+div_id+'\',\''+Origin+'\',\''+maxrowexcced+'\')">'+res_ary[1]+'</td>');
								//document.getElementById(div_id).innerHTML='<td>'+res_ary[2]+'</td><td>'+res_ary[1]+'</td>';
								
								
								//$("#"+div_id).attr("onclick","delete_ware_houseInfo_customclearance('"+newvalue+"','"+div_id+"','Origin');");
								newcount=parseInt(newcount)+1;
								var divid="origin_data_"+newcount;
								$('#originTable').append('<tr id='+divid+' onclick=""></tr>');
								if(value1!='undefined' && value1!='')
									$("#idOriginWarehouse").attr('value',value1);
								
								$("#originWarehouseAdded").attr('value',newcount);
								
							}
						}
					}
					var desWarehouseValue = $("#idDesWarehouse").attr('value');
					var originWarehouseValue = $("#idOriginWarehouse").attr('value');
					if(desWarehouseValue!='' && originWarehouseValue!="")
					{
						//$("#download_button").attr("onclick","downloadFormSubmitCustomClearance()");
						$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
						$("#download_button").attr("style","opacity:1;");
					}
					else
					{
						$("#download_button").unbind("click");
						//$("#download_button").attr("onclick","");
						$("#download_button").attr("style","opacity:0.4;");
					}
                                        
                                        
                                        var downloadType = $("#downloadType").val();
					if(downloadType=='2')
					{
						checkRowCountCC(maxrowexcced);
					}
					else if(downloadType=='1')
					{
						updatedNoteCC(maxrowexcced);
					}
				}
			else
			{
				$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{flag:'OriginAddCCAll'},function(result){
					
					$("#originTable").html(result);
					
					var desWarehouseValue = $("#idDesWarehouse").attr('value');
					var originWarehouseValue = $("#idOriginWarehouse").attr('value');
					if(desWarehouseValue!='' && originWarehouseValue!="")
					{
						//$("#download_button").attr("onclick","downloadFormSubmitCustomClearance()");
						$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
						$("#download_button").attr("style","opacity:1;");
					}
					else
					{
						$("#download_button").unbind("click");
						//$("#download_button").attr("onclick","");
						$("#download_button").attr("style","opacity:0.4;");
					}
                                        
                                        var downloadType = $("#downloadType").val();
                                        //alert(downloadType);
                                        if(downloadType=='2')
                                        {
                                                checkRowCountCC(maxrowexcced);
                                        }
                                        else if(downloadType=='1')
                                        {
                                                updatedNoteCC(maxrowexcced);
                                        }
				});
			}
	 }
		
	}
	else if(type=='Destination')
	{
		var desCountry = $("#desCountry").attr('value');
		var desWarehouse = $("#desWarehouse").attr('value');
		var idDesWarehouse = $("#idDesWarehouse").attr('value');
		var count = $("#desWarehouseAdded").attr('value');
		 var res_ary = desWarehouse.split("_");
		
		if(desWarehouse!="")
		{
			if(idDesWarehouse=='')
			{
				var value1=res_ary[0]+'_'+res_ary[3];
				$("#idDesWarehouse").attr('value',value1);
			}
			else
			{
				var newvalue=res_ary[0]+'_'+res_ary[3];
				
				 var res_ary_new = idDesWarehouse.split(";");
				 var add_ware=jQuery.inArray(newvalue, res_ary_new);
				if(add_ware!='-1')
				{
					return false;
				}
				
				var value1=idDesWarehouse+';'+newvalue;
				$("#idDesWarehouse").attr('value',value1);
			}
			var newcount=parseInt(count)+1;	
			$("#desWarehouseAdded").attr('value',newcount);
			var div_id="des_data_"+count;
			$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{desCountry:desCountry,desWarehouse:desWarehouse,flag:'DesAdd',count:newcount},function(result){
			
			$("#"+div_id).html(result);
			// $("#"+div_id).attr("onclick","delete_ware_houseInfo_customclearance('"+newvalue+"','"+div_id+"','Destination');");
			 $("#"+div_id).click(function(){delete_ware_houseInfo_customclearance(newvalue,div_id,'Destination',maxrowexcced)});
			var divid="des_data_"+newcount;
			$('#desTable').append('<tr id='+divid+' onclick=""></tr>');
				var desWarehouseValue = $("#idDesWarehouse").attr('value');
				var originWarehouseValue = $("#idOriginWarehouse").attr('value');
				if(desWarehouseValue!='' && originWarehouseValue!="")
				{
					//$("#download_button").attr("onclick","downloadFormSubmitCustomClearance()");
					$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
					$("#download_button").attr("style","opacity:1;");
				}
				else
				{
					$("#download_button").unbind("click");
					//$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
                                
                                var downloadType = $("#downloadType").val();
                                if(downloadType=='2')
                                {
                                        checkRowCountCC(maxrowexcced);
                                }
                                else if(downloadType=='1')
                                {
                                        updatedNoteCC(maxrowexcced);
                                }
			});
		
		}
		else
		{
			if(desCountry!='')
			{
				var items = $("#desWarehouse option").length;
				var  items =items-1;
				
				if(items!=0 && items!='')
				{
					var values = [];
					$('#desWarehouse option').each(function() { 
					    values.push( $(this).attr('value') );
					});
					var arrlen=values.length;
					for(i=1;i<arrlen;++i)
					{
						var newcount=$("#desWarehouseAdded").attr('value');
						var idDesWarehouse = $("#idDesWarehouse").attr('value');
						var res_ary = values[i].split("_");
						
						if(idDesWarehouse=='')
						{
							var newValueWare=values[i];
							var new_value_ware=res_ary[0]+'_'+res_ary[3];
							var value1=new_value_ware;
							var newvalue=res_ary[0]+'_'+res_ary[3];
				 		}
				 		else
				 		{
				 			var newvalue=res_ary[0]+'_'+res_ary[3];
				 			var res_ary_new = idDesWarehouse.split(";");
				 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
				 			if(add_ware!='-1')
							{
								var newValueWare='';
							}
							else
							{
								var newValueWare=values[i];
								var new_value_ware=res_ary[0]+'_'+res_ary[3];
								var value1=idDesWarehouse+';'+newvalue;
							}					
				 		}
				 		
						if(newValueWare!='undefined' && newValueWare!='')
						{
								div_id="des_data_"+newcount;
								var Destination = "Destination";
								$("#"+div_id).html('<th onclick="delete_ware_houseInfo_customclearance(\''+newvalue+'\',\''+div_id+'\',\''+Destination+'\',\''+maxrowexcced+'\')">'+res_ary[2]+'</th><td onclick="delete_ware_houseInfo_customclearance(\''+newvalue+'\',\''+div_id+'\',\''+Destination+'\',\''+maxrowexcced+'\')">'+res_ary[1]+'</td>');
								//document.getElementById(div_id).innerHTML='<td>'+res_ary[2]+'</td><td>'+res_ary[1]+'</td>';
								
								
								//$("#"+div_id).attr("onclick","delete_ware_houseInfo_customclearance('"+newvalue+"','"+div_id+"','Destination');");
								newcount=parseInt(newcount)+1;
								var divid="des_data_"+newcount;
								$('#desTable').append('<tr id='+divid+' onclick=""></tr>');
								if(value1!='undefined' && value1!='')
								$("#idDesWarehouse").attr('value',value1);
								
								$("#desWarehouseAdded").attr('value',newcount);
								
							}
						}
					}
					var desWarehouseValue = $("#idDesWarehouse").attr('value');
					var originWarehouseValue = $("#idOriginWarehouse").attr('value');
					if(desWarehouseValue!='' && originWarehouseValue!="")
					{
						//$("#download_button").attr("onclick","downloadFormSubmitCustomClearance()");
						$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
						$("#download_button").attr("style","opacity:1;");
					}
					else
					{
						$("#download_button").unbind("click");
						//$("#download_button").attr("onclick","");
						$("#download_button").attr("style","opacity:0.4;");
					}
                                        
                                        
                                        var downloadType = $("#downloadType").val();
					if(downloadType=='2')
					{
						checkRowCountCC(maxrowexcced);
					}
					else if(downloadType=='1')
					{
						updatedNoteCC(maxrowexcced);
					}
				}
				else
				{
					$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{flag:'DesAddCCAll',desCountry:desCountry},function(result){
						
						$("#desTable").html(result);
						
						var desWarehouseValue = $("#idDesWarehouse").attr('value');
						var originWarehouseValue = $("#idOriginWarehouse").attr('value');
						if(desWarehouseValue!='' && originWarehouseValue!="")
						{
							//$("#download_button").attr("onclick","downloadFormSubmitCustomClearance()");
							$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
							$("#download_button").attr("style","opacity:1;");
						}
						else
						{
							$("#download_button").unbind("click");
							//$("#download_button").attr("onclick","");
							$("#download_button").attr("style","opacity:0.4;");
						}
                                                
                                                var downloadType = $("#downloadType").val();
                                                //alert(downloadType);
                                                if(downloadType=='2')
                                                {
                                                        checkRowCountCC(maxrowexcced);
                                                }
                                                else if(downloadType=='1')
                                                {
                                                        updatedNoteCC(maxrowexcced);
                                                }
					});
				}
		}
	}
	
		
}

function delete_ware_houseInfo_customclearance(newvalue,div_id,divFlag,maxrowexcced)
{
	if(divFlag=='Origin')
	{
		var rowCount = $('#originTable tr').length;
		var deleteWareHouseOriginArr=$("#deleteWareHouseOriginArr").attr('value');
		var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');
		
		//var res_ary = deleteWareHouseArr.split("_");
		
		if(deleteWareHouseOriginArr=='')
		{
			//var newvalue=res_ary[0]+'_'+res_ary[3];
			$("#deleteWareHouseOriginArr").attr('value',newvalue);
			$("#deleteWareHouseDivIdOriginArr").attr('value',div_id);
		}
		else
		{
			 var res_ary_new = deleteWareHouseOriginArr.split(";");
			 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			if(add_ware!='-1')
			{			
				res_ary_new = $.grep(res_ary_new, function(value) {
				    return newvalue != value;
				});
								
				var value1=res_ary_new.join(';');
				$("#deleteWareHouseOriginArr").attr('value',value1);
			}
			else
			{
				var value1=deleteWareHouseOriginArr+';'+newvalue;
				$("#deleteWareHouseOriginArr").attr('value',value1);
			}
			
			
			var res_ary_new_div = deleteWareHouseDivIdOriginArr.split(";");
			 var add_ware_divid=jQuery.inArray(div_id, res_ary_new_div);
			if(add_ware_divid!='-1')
			{			
				res_ary_new_div = $.grep(res_ary_new_div, function(value) {
				    return div_id != value;
				});
								
				var valuediv1=res_ary_new_div.join(';');
				$("#deleteWareHouseDivIdOriginArr").attr('value',valuediv1);
			}
			else
			{
				var valuediv1=deleteWareHouseDivIdOriginArr+';'+div_id;
				$("#deleteWareHouseDivIdOriginArr").attr('value',valuediv1);
			}
		}
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="origin_data_"+t;
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
		}
		
		var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');
		if(deleteWareHouseDivIdOriginArr!='')
		{
			var res_ary_new_div = deleteWareHouseDivIdOriginArr.split(";");
			var res_ary_new_div_len = res_ary_new_div.length;
			if(res_ary_new_div_len>0)
			{
				
				for(t=0;t<res_ary_new_div_len;++t)
				{
					var div_id=res_ary_new_div[t];
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');
		if(deleteWareHouseDivIdOriginArr!='')
		{
			//$("#origin_remove_button").attr("onclick","delete_ware_house_data_customclearance('"+divFlag+"')");
			 $("#origin_remove_button").click(function(){delete_ware_house_data_customclearance(divFlag,maxrowexcced)});
			$("#origin_remove_button").attr("style","opacity:1;");
		}
		else
		{
			$("#origin_remove_button").unbind("click");
			//$("#origin_remove_button").attr("onclick","");
			$("#origin_remove_button").attr("style","opacity:0.4;");
		}
	}
	else if(divFlag=='Destination')
	{
		var rowCount = $('#desTable tr').length;
		var deleteWareHouseDesArr=$("#deleteWareHouseDesArr").attr('value');
		var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
		
		//var res_ary = deleteWareHouseArr.split("_");
		
		if(deleteWareHouseDesArr=='')
		{
			//var newvalue=res_ary[0]+'_'+res_ary[3];
			$("#deleteWareHouseDesArr").attr('value',newvalue);
			$("#deleteWareHouseDivIdDesArr").attr('value',div_id);
		}
		else
		{
			 var res_ary_new = deleteWareHouseDesArr.split(";");
			 var add_ware=jQuery.inArray(newvalue, res_ary_new);
			if(add_ware!='-1')
			{			
				res_ary_new = $.grep(res_ary_new, function(value) {
				    return newvalue != value;
				});
								
				var value1=res_ary_new.join(';');
				$("#deleteWareHouseDesArr").attr('value',value1);
			}
			else
			{
				var value1=deleteWareHouseDesArr+';'+newvalue;
				$("#deleteWareHouseDesArr").attr('value',value1);
			}
			
			
			var res_ary_new_div = deleteWareHouseDivIdDesArr.split(";");
			 var add_ware_divid=jQuery.inArray(div_id, res_ary_new_div);
			if(add_ware_divid!='-1')
			{			
				res_ary_new_div = $.grep(res_ary_new_div, function(value) {
				    return div_id != value;
				});
								
				var valuediv1=res_ary_new_div.join(';');
				$("#deleteWareHouseDivIdDesArr").attr('value',valuediv1);
			}
			else
			{
				var valuediv1=deleteWareHouseDivIdDesArr+';'+div_id;
				$("#deleteWareHouseDivIdDesArr").attr('value',valuediv1);
			}
		}
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="des_data_"+t;
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				
			}
		}
		
		var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
		if(deleteWareHouseDivIdDesArr!='')
		{
			var res_ary_new_div = deleteWareHouseDivIdDesArr.split(";");
			var res_ary_new_div_len = res_ary_new_div.length;
			if(res_ary_new_div_len>0)
			{
				
				for(t=0;t<res_ary_new_div_len;++t)
				{
					var div_id=res_ary_new_div[t];
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
		if(deleteWareHouseDivIdDesArr!='')
		{
			//$("#des_remove_button").attr("onclick","delete_ware_house_data_customclearance('"+divFlag+"')");
			$("#des_remove_button").click(function(){delete_ware_house_data_customclearance(divFlag,maxrowexcced)});
			$("#des_remove_button").attr("style","opacity:1;");
		}
		else
		{
			$("#des_remove_button").unbind("click");
			//$("#des_remove_button").attr("onclick","");
			$("#des_remove_button").attr("style","opacity:0.4;");
		}
	}
}
function delete_ware_house_data_customclearance(divFlag,maxrowexcced)
{ 
	if(divFlag=='Origin')
	{
		var idOriginWarehouse = $("#idOriginWarehouse").attr('value');
		var deleteWareHouseDivIdOriginArr=$("#deleteWareHouseDivIdOriginArr").attr('value');
		var deleteWareHouseOriginArr=$("#deleteWareHouseOriginArr").attr('value');
		var res_ary_new = idOriginWarehouse.split(";");
		var res_ary_new_delete = deleteWareHouseOriginArr.split(";");
		var res_ary_new_div_len = res_ary_new_delete.length;
		
		if(res_ary_new_div_len>0)
		{
			for(t=0;t<res_ary_new_div_len;++t)
			{
				haulage_value=res_ary_new_delete[t];
				res_ary_new = $.grep(res_ary_new, function(value) {
				    return haulage_value != value;
					});
			}
		}
		var new_arr=res_ary_new.join(';');
		
		var delete_ware_house_divId_arr = deleteWareHouseDivIdOriginArr.split(";");
		if(delete_ware_house_divId_arr.length>0)
		{
			for(var i=0; i<delete_ware_house_divId_arr.length; i++) 
			{
				var div_id=delete_ware_house_divId_arr[i];		
				$("#"+div_id).html("");
				$("#"+div_id).attr('style','display:none;');		
			}
		}
		
		$("#deleteWareHouseDivIdOriginArr").attr('value','');
		$("#deleteWareHouseOriginArr").attr('value','');
		
		$("#origin_remove_button").attr("onclick","");
		$("#origin_remove_button").attr("style","opacity:0.4;");
		$("#idOriginWarehouse").attr('value',new_arr);	
	}
	else if(divFlag=='Destination')
	{
		var idDesWarehouse = $("#idDesWarehouse").attr('value');
		var deleteWareHouseDivIdDesArr=$("#deleteWareHouseDivIdDesArr").attr('value');
		var deleteWareHouseDesArr=$("#deleteWareHouseDesArr").attr('value');
		var res_ary_new = idDesWarehouse.split(";");
		var res_ary_new_delete = deleteWareHouseDesArr.split(";");
		var res_ary_new_div_len = res_ary_new_delete.length;
		
		if(res_ary_new_div_len>0)
		{
			for(t=0;t<res_ary_new_div_len;++t)
			{
				haulage_value=res_ary_new_delete[t];
				res_ary_new = $.grep(res_ary_new, function(value) {
				    return haulage_value != value;
					});
			}
		}
		var new_arr_des=res_ary_new.join(';');
		
		var delete_ware_house_divId_arr = deleteWareHouseDivIdDesArr.split(";");
		if(delete_ware_house_divId_arr.length>0)
		{
			for(var i=0; i<delete_ware_house_divId_arr.length; i++) 
			{
				var div_id=delete_ware_house_divId_arr[i];		
				$("#"+div_id).html("");
				$("#"+div_id).attr('style','display:none;');		
			}
		}
		$("#deleteWareHouseDivIdDesArr").attr('value','');
		$("#deleteWareHouseDesArr").attr('value','');
		$("#idDesWarehouse").attr('value',new_arr_des);
				
		$("#des_remove_button").attr("onclick","");
		$("#des_remove_button").attr("style","opacity:0.4;");
	}
	
	var desWarehouseValue = $("#idDesWarehouse").attr('value');
	var originWarehouseValue = $("#idOriginWarehouse").attr('value');
        
        var downloadType = $("#downloadType").val();
	
		var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
		var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
		var res_ary_originwarhousevalue = originWarehouseValueTotal.split(";");
		var res_ary_deswarhousevalue = desWarehouseValueTotal.split(";");
		var deswarehouselen=res_ary_deswarhousevalue.length;
		var originwarehouselen=res_ary_originwarhousevalue.length;	
		if(originwarehouselen>0 && deswarehouselen>0)
		{
			//var totalCount=parseInt(originwarehouselen)*parseInt(deswarehouselen);
                        var totalCount = getDownloadableWarehouseCount(res_ary_originwarhousevalue,res_ary_deswarhousevalue);
			if(totalCount<=maxrowexcced)
			{
				if(downloadType=='1')
				{
					$('#downloadType').removeAttr('disabled');
					updatedNoteCC(maxrowexcced);
				}
				else if(downloadType=='2')
				{
				
					var totaltime=(__TIME_PER_ROW_CREATE__*totalCount);
				 	if(totaltime>60)
				 	{
				 		var minutes = Math.floor(totaltime/60);
				 		var secondsleft = totaltime%60;
                                                secondsleft = Math.ceil(secondsleft);
				 		if(minutes=='1')
				 		{
				 			var minStr=minutes+" minute"; 
				 		}
				 		else
				 		{
				 			var minStr=minutes+" minutes"; 
				 		}
				 		if(secondsleft=='1')
				 		{
				 			var secstr=secondsleft+" second";
				 		}
				 		else
				 		{
				 			var secstr=Math.ceil(secondsleft)+" seconds";
				 		}
				 		
				 		var timestr=minStr+" "+secstr;
				 	}
				 	else
				 	{
				 		var timetaken=Math.ceil(totaltime)
				 		if(timetaken=='1')
				 		{
				 			var timestr=timetaken+" second";
				 		}
				 		else
				 		{
				 			var timestr=timetaken+" seconds";
				 		}
				 	}
				 	$("#total_row_excced_time").html(timestr);
					$("#row_excced_data").attr("style","display:none");
					$("#row_excced_data_1").attr("style","display:none");
					$("#row_excced_data_2").attr("style","display:block");
					$("#total_row_excced_1").html(number_format_js(totalCount));
					$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
					$("#download_button").attr("style","opacity:1;");
				}
			}
			else
			{
                            if(downloadType=='1')
                            {
                                    $('#downloadType').removeAttr('disabled');
                                    updatedNoteCC(maxrowexcced);
                            }
                            else if(downloadType=='2')
                            {
                                    $("#row_excced_data_1").attr("style","display:none");
                                    $("#row_excced_data").attr("style","display:block");
                                    $("#row_excced_data_2").attr("style","display:none");
                                    $("#total_row_excced").html(number_format_js(totalCount));
                            }
			}
		
	}
	
	if(desWarehouseValue=='' || originWarehouseValue=='')
	{
		$("#download_button").unbind("click");
		//$("#download_button").attr("onclick","");
		$("#download_button").attr("style","opacity:0.4;");
	}
}

function downloadFormSubmitCustomClearance()
{
        //$("#loader").attr("style","display:block");	
        $("#download_button").unbind("click");		
        $("#download_button").attr("style","opacity:0.4;");
	document.bulkExportData.action = __JS_ONLY_SITE_BASE__+"/downloadCustomClearanceData.php";
	document.bulkExportData.method = "post"
	document.bulkExportData.submit();
        //$("#loader").attr("style","display:none;");	
}

function submit_upload_file_customclearance()
{
	//document.uploadFile.action = __JS_ONLY_SITE_BASE__+"/bulkDataExportLCL.php";
	//document.uploadFile.method = "post"
	$("#loader").attr('style','display:block;');
	document.uploadFile.submit();
	
}
function submit_obo_cc_form(text,text2)
{
	$("#loader").attr('style','display:block;');
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",$("#obo_cc_data_export_form").serialize(),function(result){
			
			//disabling to form 
			disableOBOTopForm(true,'',text,text2);			
			$("#obo_cc_bottom_span").html(result);
			$("#loader").attr('style','display:none;');
			document.getElementById("text_change").innerHTML=text;	
			
			$("#obo_cc_cc_getdata_button").unbind("click");
			//$("#obo_cc_cc_getdata_button").attr('onclick','cancel_obo_cc("'+text+'","'+text2+'")');
			$("#obo_cc_cc_getdata_button").click(function(){cancel_obo_cc(text,text2)});
	});
}
function update_obo_cc_data(text,text2)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",$("#update_obo_cc_form").serialize(),function(result){
		result_ary=result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#obo_cc_error").attr('style','display:none;');
			disableOBOTopForm(false,'',text,text2);
			$("#obo_cc_bottom_span").html(result_ary[1]);
			$("#obo_cc_getdata_button").focus();
			document.getElementById("text_change").innerHTML=text;
			$("#obo_cc_cc_getdata_button").unbind("click");
			//$("#obo_cc_cc_getdata_button").attr('onclick','submit_obo_cc_form("'+text2+'","'+text+'")');
			$("#obo_cc_cc_getdata_button").click(function(){submit_obo_cc_form(text2,text)});	
		}
		else
		{
			$("#obo_cc_error").html(result_ary[1]);
			$("#obo_cc_error").attr('style','display:block;');
			$("#obo_cc_error").focus();
		}	
	});
}

function disableOBOTopForm(flag,check,text,text2)
{
	if(flag)
	{
		$("#idOriginCountry").attr('disabled','disabled');
		$("#szOriginCity").attr('disabled','disabled');
		$("#idOriginWarehouse").attr('disabled','disabled');
		$("#idDestinationCountry").attr('disabled','disabled');
		$("#szDestinationCity").attr('disabled','disabled');
		$("#idDestinationWarehouse").attr('disabled','disabled');
		
		$("#obo_cc_getdata_button").attr('onclick','');
		$("#obo_cc_getdata_button").attr('style','opacity:0.4');
	}
	else
	{
		$("#idOriginCountry").removeAttr('disabled');
		$("#szOriginCity").removeAttr('disabled');
		$("#idOriginWarehouse").removeAttr('disabled');
		$("#idDestinationCountry").removeAttr('disabled');
		$("#szDestinationCity").removeAttr('disabled');
		$("#idDestinationWarehouse").removeAttr('disabled');
		
		if(check==2) // called from LCL
		{
			$("#obo_cc_getdata_button").unbind("click");
			$("#obo_cc_getdata_button").click(function(){submit_obo_lcl_form(text,text2)});
			//$("#obo_cc_getdata_button").attr('onclick','submit_obo_lcl_form()');
		}
		else
		{
			$("#obo_cc_getdata_button").unbind("click");
			$("#obo_cc_getdata_button").click(function(){submit_obo_cc_form(text,text2)});
			//$("#obo_cc_getdata_button").attr('onclick','submit_obo_cc_form()');
		}	
		$("#obo_cc_getdata_button").attr('style','');
	}
}

function cancel_obo_cc(text,text2)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",{mode:'CANCEL_OBO_CC'},function(result){
		disableOBOTopForm(false);
		$("#obo_cc_bottom_span").html(result);
		document.getElementById("text_change").innerHTML=text2;	
		//$("#obo_cc_cc_getdata_button").attr('onclick','submit_obo_cc_form("'+text+'","'+text2+'")');
		$("#obo_cc_cc_getdata_button").unbind("click");
		$("#obo_cc_cc_getdata_button").click(function(){submit_obo_cc_form(text,text2)});
	});
}

function clear_all_data_obo_cc()
{
	$("#fOriginPrice").attr('value','');
	$("#fDestinationPrice").attr('value','');
	
	reset_dropdown('idOriginCurrency');
	reset_dropdown('idDestinationCurrency');
}
function showAbandonPopup(div_id,page_url,login_flag)
{
	$("#redirect_abandon_popup_url").attr('value',page_url);
	$("#login_flag").attr('value',login_flag);
	$("#"+div_id).attr('style','display:block;');
}

function showForwarderWarehouseOBO(idForwarder,value,type,divid)
{
	var idCountry;
	var szCity;
	var showflag;
	if(type=='country')
	{
		var idCountry=value;
		var showflag="Warehouse_city";
	}
	else if(type=='city')
	{ 
		szCity=value;
		idCountry=$('#haulageCountry').val();
		showflag="Warehouse";
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{idForwarder:idForwarder,idCountry:idCountry,szCity:szCity,flag:showflag},function(result){
		$("#"+divid).html(result);
		
		if(type=='country')
		{
			var country_id=parseInt(value);
			if(country_id>0)
			{
				 $("#szCity").removeAttr("disabled");   
    			 $("#haulageWarehouse").removeAttr("disabled");
    			 $("#idDirection").removeAttr("disabled");
    			 
    			 var haulageCountry = $("#haulageCountry").attr('value');

				  var haulageWarehouse = $("#haulageWarehouse").attr('value');
				  if(haulageCountry!='' && haulageWarehouse!='')
				  {
    			   		$("#get_haulage_data").attr('style',"opacity:1.0;min-width:70px;");
    			  		$("#get_haulage_data").attr("onclick","('"+idForwarder+"');");
    			  }
    			  else
    			  {
    			  		$("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
    			  		$("#get_haulage_data").attr("onclick","");
    			  }
    			 
			}
			else
			{
				 $("#szCity").attr("disabled",'disabled');   
    			 $("#haulageWarehouse").attr("disabled",'disabled');
    			 $("#idDirection").attr("disabled",'disabled');
    			 $("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
    			 $("#get_haulage_data").attr("onclick",'');
			}
		}		
	});
}

function getOBOWarehouseHaulageData(idForwarder,text,text2)
{
	var haulageCountry = $("#haulageCountry").attr('value');
	var szCity = $("#szCity").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idDirection = $("#idDirection").attr('value');
		
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{idForwarder:idForwarder,haulageCountry:haulageCountry,haulageWarehouse:haulageWarehouse,szCity:szCity,idDirection:idDirection,flag:'haulageAddOBO'},function(result){
	$("#haulageTable").html(result);	
	
		$("#addeditHaulageData :input").attr("disabled", false);
		
		$("#cancel_button").attr("style","opacity:1;");
		$("#add_edit_button").attr("style","opacity:1;");
		$("#clear_form").attr("style","opacity:1;");
			
		$("#cancel_button").unbind("click");
		//$("#cancel_button").attr("onclick","cancel_form_data('"+idForwarder+"','"+text+"','"+text2+"');");
		$("#cancel_button").click(function(){cancel_form_data(idForwarder,text,text2)});
		
		$("#clear_form").unbind("click");
		//$("#clear_form").attr("onclick","clear_form_data('addeditHaulageData');");
		$("#clear_form").click(function(){clear_form_data('addeditHaulageData');});
		
		$("#add_edit_button").unbind("click");
		//$("#add_edit_button").attr("onclick","add_haulage_data()");	
		$("#add_edit_button").click(function(){add_haulage_data();});
		
		var haulageCountry = $("#haulageCountry").attr('value');
		var szCity = $("#szCity").attr('value');
		var haulageWarehouse = $("#haulageWarehouse").attr('value');
		var idDirection = $("#idDirection").attr('value');
		
		$("#Direction").attr('value',idDirection);	
		$("#idCountry").attr('value',haulageCountry);
		$("#City").attr('value',szCity);
		$("#idWareHouse").attr('value',haulageWarehouse);
		$("#flag").attr('value','add');
		document.getElementById("text_change").innerHTML=text;		
		$("#get_haulage_data").unbind("click");
		//$("#get_haulage_data").attr('onclick','cancel_form_data("'+idForwarder+'","'+text+'","'+text2+'");');
		$("#get_haulage_data").click(function(){cancel_form_data(idForwarder,text,text2)});
		$("#szCity").attr("disabled",'disabled');   
		$("#haulageWarehouse").attr("disabled",'disabled');
		$("#idDirection").attr("disabled",'disabled');
		$("#haulageCountry").attr("disabled",'disabled');
	});
}

function edit_delete_haulage_data(new_value,div_id,idForwarder,trans,text,text2)
{
	
	var rowCount = $('#haulageTable tr').length;
	
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_data_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	$("#haulage_remove_button").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#haulage_remove_button").click(function(){delete_haulage_data(new_value,div_id,idForwarder)});
	$("#haulage_edit_button").unbind("click");
	//$("#haulage_edit_button").attr("onclick","edit_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"','"+trans+"','"+text+"','"+text2+"')");
	$("#haulage_edit_button").click(function(){edit_haulage_data(new_value,div_id,idForwarder,trans,text,text2)});
	
	$("#haulage_remove_button").attr("style","opacity:1;");
	$("#haulage_edit_button").attr("style","opacity:1;");
	$("#szCity").attr("disabled",'disabled');   
	$("#haulageWarehouse").attr("disabled",'disabled');
	$("#idDirection").attr("disabled",'disabled');
	$("#haulageCountry").attr("disabled",'disabled');
	//$("#get_haulage_data").attr('style',"opacity:0.4");
}

function edit_haulage_data(id,divid,idForwarder,trans,text,text2)
{
	
	$.post(__JS_ONLY_SITE_BASE__+"/haulageOBOAddEdit.php",{idForwarder:idForwarder,idHaulageData:id,flag:'edit'},function(result){
	$("#add_edit_haulage_data").html(result);
	
	$("#add_edit_button").unbind("click");
	//$("#add_edit_button").attr("onclick","save_haulage_data()");
	$("#add_edit_button").click(function(){save_haulage_data()});
		
	$("#haulage_remove_button").attr("onclick","");
	$("#haulage_edit_button").attr("onclick","");
	$("#haulage_remove_button").attr("style","opacity:0.4;");
	$("#haulage_edit_button").attr("style","opacity:0.4;");	
	$("#cancel_button").unbind("click");
	//$("#cancel_button").attr("onclick","cancel_form_data('"+idForwarder+"','"+text+"','"+text2+"');");
	$("#cancel_button").click(function(){cancel_form_data(idForwarder,text,text2)});
	$("#clear_form").unbind("click");
	//$("#clear_form").attr("onclick","clear_form_data('addeditHaulageData');");
	$("#clear_form").click(function(){clear_form_data('addeditHaulageData');});
	
	document.getElementById("add_edit_button").innerHTML="<span>"+trans+"</span>";			
	});
}
function delete_haulage_data(id,divid,idForwarder)
{	
	
	$("#delete_confirm").attr("style","display:block;");
	$("#delete_confirm_button").attr("onclick","delete_haulage_data_confirm('"+id+"','"+divid+"','"+idForwarder+"')");
	addPopupScrollClass('delete_confirm');
}

function delete_haulage_data_confirm(id,divid,idForwarder)
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{idForwarder:idForwarder,idHaulageData:id,flag:'deleteHaulage'},function(result){
	$("#haulageTable").html(result);				
	});
	removePopupScrollClass('haulageTable');
}

function save_haulage_data()
{
	$.post(__JS_ONLY_SITE_BASE__+"/haulageOBOAddEdit.php",$("#addeditHaulageData").serialize(),function(result){
	$("#add_edit_haulage_data").html(result);});
}


function clear_form_after_save_data(idForwarder,text,text2)
{
	$.post(__JS_ONLY_SITE_BASE__+"/haulageOBOAddEdit.php",function(result){
	$("#add_edit_haulage_data").html(result);
	$("#cancel_button").unbind("click");
	$("#cancel_button").attr("onclick","cancel_form_data('"+idForwarder+"','"+text+"','"+text2+"');");
	});
}

function add_haulage_data()
{
		var haulageCountry = $("#haulageCountry").attr('value');
		var szCity = $("#szCity").attr('value');
		var haulageWarehouse = $("#haulageWarehouse").attr('value');
		var idDirection = $("#idDirection").attr('value');
		
		$("#Direction").attr('value',idDirection);	
		$("#idCountry").attr('value',haulageCountry);
		$("#City").attr('value',szCity);
		$("#idWareHouse").attr('value',haulageWarehouse);
		$("#flag").attr('value','add');		
	$.post(__JS_ONLY_SITE_BASE__+"/haulageOBOAddEdit.php",$("#addeditHaulageData").serialize(),function(result){
	$("#add_edit_haulage_data").html(result);});
}

function clear_form_data(form_id)
{
	if(form_id=='')
	{
		form_id ='addeditHaulageData';
	}
	$('INPUT:text,SELECT', '#'+form_id).val(''); 
}


function cancel_form_data(idForwarder,text,text2)
{
	//$('INPUT:text,SELECT', '#haulageOBO').val(''); 
	getOBOWarehouseHaulageData('',text,text2);
	
	$("#tryitout_result_div").html("");
	$("#tryitout_result_div_error").html("");
	$("#tryitout_result_div").css('display','none');
	$("#tryitout_result_div_error").css('display','none');
	
	setTimeout("cancelForm('"+idForwarder+"','"+text+"','"+text2+"')",'700');
}

function showCCForwarderWarehouse(idForwarder,idCountry,divid,maxrowexcced)
{
	if(divid=='orgin_warehouse')
	{
		$("#cc_orign").attr("style","opacity:0.4;");
		$("#cc_orign").attr("onclick","");
		var showflag='showOriginWarehouse';
	}
	else if(divid=='des_warehouse')
	{
		$("#cc_des").attr("style","opacity:0.4;");
		$("#cc_des").attr("onclick","");
		var showflag='showDesWarehouse';
	}
		
	
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{idForwarder:idForwarder,idCountry:idCountry,flag:showflag},function(result){
	$("#"+divid).html(result);
	
		if(divid=='orgin_warehouse')
		{
			$("#cc_orign").attr("style","opacity:1;");
			//$("#cc_orign").attr("onclick","addWareHousesCustomClearance('Origin');");
			$("#cc_orign").click(function(){addWareHousesCustomClearance('Origin',maxrowexcced)});
		}
		else if(divid=='des_warehouse')
		{
			$("#cc_des").attr("style","opacity:1;");
			//$("#cc_des").attr("onclick","addWareHousesCustomClearance('Destination');");
			$("#cc_des").click(function(){addWareHousesCustomClearance('Destination',maxrowexcced)});
		}
			
	
	});
}

function cancelForm(idForwarder,text,text2)
{
	$('INPUT:text,SELECT', '#addeditHaulageData').val(''); 
	
	$("#addeditHaulageData :input").attr("disabled", true);
	
	
	$("#cancel_button").attr("style","opacity:0.4;");
	$("#add_edit_button").attr("style","opacity:0.4;");
	$("#clear_form").attr("style","opacity:0.4;");	
	$("#cancel_button").attr("onclick","");
	$("#clear_form").attr("onclick","");
	$("#add_edit_button").attr("onclick","");
	
	$("#haulage_remove_button").attr("style","opacity:0.4;");
	$("#haulage_edit_button").attr("style","opacity:0.4;");
	
	$("#haulage_remove_button").attr("onclick","");
	$("#haulage_edit_button").attr("onclick","");
	
	$("#szCity").attr("disabled",false);   
	$("#haulageWarehouse").attr("disabled",false);
	$("#idDirection").attr("disabled",false);
	$("#haulageCountry").attr("disabled",false);
	//$("#get_haulage_data").attr('style',"opacity:0.4");
	document.getElementById("text_change").innerHTML=text2;	
	$("#get_haulage_data").unbind("click");
	
	//$("#get_haulage_data").attr("onclick","getOBOWarehouseHaulageData('"+idForwarder+"','"+text+"','"+text2+"');");
	$("#get_haulage_data").click(function(){getOBOWarehouseHaulageData(idForwarder,text,text2)});
	var disp = $("#regError").css('display');
	if(disp=='block')
	{
		$("#regError").css('display','none');
	}
	//clear_form_after_save_data_cancel();
}

function download_suggestion_reviews(type)
{ 
	$.post(__JS_ONLY_SITE_BASE__+"/downloadFeedbackSuggestion.php",{flag:"DOWNLOAD_SUGGESTION_REVIEW",mode:type},function(result){var result=result;
	window.location.href = __JS_ONLY_SITE_BASE__+"/forwarderBulkExportImport/forwarderDetails/"+result+".xlsx";});
	//.success(function(result) { $.post(__JS_ONLY_SITE_BASE__+"/downloadFeedbackSuggestion.php",{flag:"DELETE",mode:result});});
	
}
function download_history_review()
{
	$.post(__JS_ONLY_SITE_BASE__+"/downloadFeedbackSuggestion.php",{flag:"DOWNLOAD_HISTORY_REVIEW"},function(result){
	window.location.href = __JS_ONLY_SITE_BASE__+"/forwarderBulkExportImport/forwarderDetails/"+result+".xlsx";});
	//.success(function(result) { $.post(__JS_ONLY_SITE_BASE__+"/downloadFeedbackSuggestion.php",{flag:"DELETE",mode:result});});
}


function submit_obo_lcl_form(text,text2)
{
    var iWarehouseType = $("#iWarehouseType").val();
	$("#loader").attr('style','display:block;');	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",$("#obo_lcl_data_export_form").serialize(),function(result){ 
            show_forwarder_top_messges();
            var result_ary=result.split("||||"); 
            if(result_ary[0]=='SINGLE')
            {	
                //disabling to form 
                disableOBOTopForm(true,2);	
                $("#lcl_table").html(result_ary[1]);			
                $("#obo_lcl_bottom_span").html(result_ary[2]);
                $("#lcl_table").attr('style','display:block;');
                $("#obo_cc_getdata_button_p").focus();
            }
            else if(result_ary[0]=='MULTIPLE')
            {	
                disableOBOTopForm(true,2);
                //disableOBOTopForm(true,2);	
                $("#lcl_table").html(result_ary[1]);
                $("#obo_lcl_bottom_span").html(result_ary[2]);
                $("#lcl_table").attr('style','display:block;');
                $("#obo_cc_getdata_button_p").focus();
            }	
            else if(result_ary[0]=='NODATA')
            {	
                disableOBOTopForm(true,2);	
                $("#lcl_table").html(result_ary[1]);
                $("#obo_lcl_bottom_span").html(result_ary[2]);
                $("#lcl_table").attr('style','display:block;');
                //$("#obo_cc_no_record_found").attr('style','display:block;');
                $("#obo_cc_no_record_found").focus();
            }	
            $("#loader").attr('style','display:none;');
            $("#obo_cc_getdata_button").attr('style','');
            document.getElementById("text_change").innerHTML=text;	
            //$("#obo_cc_getdata_button").attr('onclick','cancel_obo_lcl("'+text+'","'+text2+'")');
            $("#obo_cc_getdata_button").unbind("click");
            $("#obo_cc_getdata_button").click(function(){change_obo_lcl(text,text2,iWarehouseType)});		
	});
}

function cancel_obo_lcl(id)
{
	var idDestinationWarehouse = parseInt($("#idDestinationWarehouse").attr('value'));
	var idOriginWarehouse = parseInt($("#idOriginWarehouse").attr('value'));
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",{mode:'CANCEL_OBO_LCL',id:id,idDestinationWarehouse:idDestinationWarehouse,idOriginWarehouse:idOriginWarehouse},function(result){
		result_ary = result.split('||||');	
		$("#obo_lcl_bottom_span").html(result_ary[0]);
		$("#lcl_table").html(result_ary[1]);
	});
}

function change_obo_lcl(text,text2,iWarehouseType)
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",{mode:'CHANGE_OBO_LCL',iWarehouseType:iWarehouseType},function(result){
		disableOBOTopForm(false,2,text,text2);
		$("#obo_lcl_edit_button").attr('style','opacity:1.0;');
		$("#obo_lcl_delete_button").attr('style','opacity:1.0;');	
		result_ary = result.split('||||');	
		$("#obo_lcl_bottom_span").html(result_ary[0]);
		$("#lcl_table").html(result_ary[1]);
		document.getElementById("text_change").innerHTML=text2;	
		$("#obo_cc_getdata_button").unbind("click");
		//$("#obo_cc_getdata_button").attr('onclick','submit_obo_lcl_form("'+text+'","'+text2+'")');
		$("#obo_cc_getdata_button").click(function(){submit_obo_lcl_form(text,text2)});		
	});
}
function update_obo_lcl_data(text,text2)
{
	var disp = $("#obo_cc_no_record_found").css('display');
	if(disp=='block')
	{
		$("#obo_cc_no_record_found").attr('style','display:none');
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",$("#obo_lcl_bottom_form").serialize(),function(result){
		
		result_ary=result.split("||||");
		result_ary[0]=$.trim(result_ary[0]);
		if(result_ary[0]=='SUCCESS')
		{
			$("#obo_lcl_error").attr('style','display:none;');
			//disableOBOTopForm(false,2,text,text2);
			$("#obo_lcl_bottom_span").html(result_ary[1]);
			$("#lcl_table").html(result_ary[2]);
			$("#message_ul").html(result_ary[3]);
			$("#obo_cc_no_record_found").attr('style','display:block;');
			window.scrollTo(100,50);
			//document.getElementById("text_change").innerHTML=text;	
			//$("#obo_cc_getdata_button").unbind("click");
			//$("#obo_cc_getdata_button").attr('onclick','submit_obo_lcl_form("'+text2+'","'+text+'")');
			//$("#obo_cc_getdata_button").click(function(){submit_obo_lcl_form(text2,text)});
		}
		else
		{
			$("#obo_lcl_error").html(result_ary[1]);
			$("#obo_lcl_error").attr('style','display:block;');
                        $("#datepicker1").attr('value',result_ary[2]);
		}	
	});
}
function clear_all_data_obo_lcl()
{
	$("#fRateWM").attr('value','');	
	$("#fMinRateWM").attr('value','');
	$("#fRate").attr('value','');
	
	$("#fOriginRateWM").attr('value','');	
	$("#fOriginMinRateWM").attr('value','');
	$("#fOriginRate").attr('value','');
	
	$("#fDestinationRateWM").attr('value','');	
	$("#fDestinationMinRateWM").attr('value','');
	$("#fDestinationRate").attr('value','');
	
	$("#datepicker1").attr('value','');
	$("#datepicker2").attr('value','');		
	$("#iTransitHours").attr('value','');
	$("#idAvailableDay").attr('value','');
	
	reset_dropdown('szFreightCurrency');
	reset_dropdown('szOriginFreightCurrency');
	reset_dropdown('szDestinationFreightCurrency');
	
	reset_dropdown('iBookingCutOffHours');
	reset_dropdown('idFrequency');
	reset_dropdown('idCutOffDay');
	
	reset_dropdown('szCutOffLocalTime');
	reset_dropdown('szAvailableLocalTime');
}
function reset_dropdown(id)
{
	$("#"+id).val($("#"+id+" option:first").val());
}

function select_to_delete_warehouse(div_id,id,idForwarder,pipe_line)
{
	var rowCount = $('#warehouseTable tr').length;
	
	if(rowCount>0)
	{
            for(t=1;t<rowCount;++t)
            {
                newdivid="warehouse_data_"+t;
                if(newdivid!=div_id)
                {
                        $("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
                }
                else
                {
                        $("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
                }
            }
	}
        
        if($("#iScrollDivValue").length>0)
        {
            $("#iScrollDivValue").attr('value',div_id);
        }
	$("#warehouse_remove_button").unbind("click");
	$("#warehouse_remove_button").click(function(){delete_warehouse_datass(id,div_id,idForwarder)});
	
	//$("#warehouse_remove_button").attr("onclick","delete_warehouse_datass('"+id+"','"+div_id+"','"+idForwarder+"')");
	$("#warehouse_remove_button").attr("style","opacity:1;");
	
	$("#warehouse_edit_button").attr("style","opacity:1;");
	$("#warehouse_edit_button").unbind("click");
	$("#warehouse_edit_button").click(function(){warehouse_detail(id,idForwarder,'save')});
	//$("#warehouse_edit_button").attr("onclick","warehouse_detail('"+id+"','"+idForwarder+"','save')");
	//$("#show_cfs_on_map").attr("onclick","open_google_map_popup("+id+")");
}

function delete_warehouse_datass(id,divid,idForwarder)
{
	$("#delete_confirm").attr("style","display:block;");
	//$("#delete_confirm_button").attr("onclick","delete_warehouse_data_confirm('"+id+"','"+divid+"','"+idForwarder+"')");
	$("#delete_confirm_button").unbind("click");
	$("#delete_confirm_button").click(function(){delete_warehouse_data_confirm(id,divid,idForwarder)});
	addPopupScrollClass('delete_confirm');
}

function delete_warehouse_data_confirm(id,divid,idForwarder)
{
	//this is the id of warehouse which is being edited
	var pop_whs_id = $("#idWarehouse").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_cfsNameList.php",{idForwarder:idForwarder,idWarehouseData:id,flag:'deleteWarehouse'},function(result){
		$("#warehouse_list").html(result);		
		if(pop_whs_id == id)
		{
			$.get(__JS_ONLY_SITE_BASE__+"/addUpdateCFSDetails.php",function(result){
	        	$("#addedit_warehouse").html(result);
	        });
		}		
	});
	removePopupScrollClass();
}

function get_warehouse_detail(id,idForwarder,buttonvalue)
{	
	var buttonvalue;
	$("#warehouse_remove_button").attr("style","opacity:0.4;");
	$("#warehouse_remove_button").attr("onclick","");
	setTimeout('warehouse_detail('+id+','+idForwarder+',"'+buttonvalue+'")','1500');
}

function warehouse_detail(id,idForwarder,buttonvalue)
{	
    var iWarehouseType = $("#iWarehouseType_hidden").val();
    $('#Error-log').html('');
    if(buttonvalue=='save')
    {
        var buttonvalue_clear = 'Cancel';
        var on_clk = "refresh_bottom_div('"+iWarehouseType+"')";
    }
    else
    {
        var buttonvalue_clear = 'Clear';
        var on_clk = "clear_form_data('addEditWarehouse')";
    }

    $.get(__JS_ONLY_SITE_BASE__+"/addUpdateCFSDetails.php",{idWarehouse:id,idForwarder:idForwarder,flag:'update'},function(result){
        $("#addedit_warehouse").html(result);
        document.getElementById("add_edit_warehouse_button").innerHTML='<span style="min-width: 79px;">'+buttonvalue+'</span>'; 
        $("#clear_cfs_location_button").html('<span style="min-width: 79px;">'+buttonvalue_clear+'</span>');
        $("#clear_cfs_location_button").attr('onclick',on_clk);
    });
}

function add_warehouse_data()
{
	var CfsListing = $('#CFSListing').val();
	if(CfsListing == '')
	{ 
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SHOW_POP_UP'},function(result){
		$('#ajaxLogin').html(result);
		$('#ajaxLogin').css('display','block');
		addPopupScrollClass('ajaxLogin');
		
		//return false;
		});
	}
	else
	{
		var value=decodeURIComponent($("#addEditWarehouse").serialize());
		$.post(__JS_ONLY_SITE_BASE__+"/addUpdateCFSDetails.php",value,function(result){
		$("#Error-log").html(result);
			show_forwarder_top_messges();
		});
	}
}

function refresh_after_success()
{
	$("#loader").attr('style','display:block;');
	$.get(__JS_ONLY_SITE_BASE__+"/addUpdateCFSDetails.php",function(result){
	        $("#addedit_warehouse").html(result);});
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_cfsNameList.php",function(result){
	        $("#warehouse_list").html(result);
	  	$("#loader").attr('style','display:none;');
	  });
}
function refresh_bottom_div(iWarehouseType)
{
	$('#Error-log').html('');
	$.get(__JS_ONLY_SITE_BASE__+"/addUpdateCFSDetails.php",{iWarehouseType:iWarehouseType},function(result){
       $("#addedit_warehouse").html(result);});
}
function getAvailableDay_backup()
{
	var iTransitHours = $("#iTransitHours").attr('value');
	var idCutOffDay = $("#idCutOffDay").attr('value');
	if(iTransitHours>0 && idCutOffDay>0)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",{mode:'GET_AVAILABLE_DAY',idCutOffDay:idCutOffDay,iTransitHours:iTransitHours},function(result){
			
			result_ary = result.split("||||");
			$("#idAvailableDay_hidden").attr('value',result_ary[0]);
			$("#idAvailableDay").attr('value',result_ary[1]);
		});
	}	
}
function serch_forwarder_booking()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bookings.php",$("#forwarder_booking_seach_form").serialize(),function(result){
	result_ary = result.split("||||");
		$("#forwarder_booking_bottom_form").html(result_ary[0]);
	});
}
function select_forwarder_booking_tr(tr_id,id,iHidePriceFlag)
{
	var rowCount = $('#booking_table tr').length;
	if(rowCount>0)
	{
		for(x=1;x<rowCount;x++)
		{
			new_tr_id="booking_data_"+x;
			if(new_tr_id!=tr_id)
			{
				$("#"+new_tr_id).attr('style','');
			}
			else
			{
				$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#download_booking").attr("onclick","download_booking_pdf('"+id+"','download_booking_pdf_form','"+tr_id+"','"+iHidePriceFlag+"')");
	$("#pdf_booking_id").attr("value",id);
	
	$("#download_booking").attr("style","opacity:1;");
}
function download_booking_pdf(booking_id,form_id,divid,iHidePriceFlag)
{
        $("#"+divid).removeClass('bold-text');
        /*
         * 20170129 Financial Revamp – step 1 Removed 31-Jan-2017
         */
        /*if($("#iBookingPage").length>0 && iHidePriceFlag==1)
        {
            window.open(__JS_ONLY_SITE_BASE__+'/forwarderBooking/'+booking_id+'/booking/',"_blank");
        }
        else
        {*/
            window.open(__JS_ONLY_SITE_BASE__+'/forwarderBooking/'+booking_id+'/',"_blank");
        //}
	//$("#pdf_booking_id").attr("value",booking_id);
	//$("#"+form_id).submit();
}

function update_invoice_comment(idBooking,szBookingRef,idBillingFlag)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookings.php",{mode:'UPDATE_YOUR_REFERENCE',id:idBooking,ref:szBookingRef,idBillingFlag:idBillingFlag},function(result){
        $("#invoice_comment_div").html(result);
        $("#invoice_comment_div").attr('style','display:block;');
        var value=$("#fwdReference").val();
        $("#fwdReference").focus().selectionEnd;
        //document.forms['updateInvoiceAry[szForwarderReference]'].elements['input'].focus();
    });
}
function update_invoice_comment_confirm()
{	
	var fwdref  = $("#fwdReference").val();
	var booking = $("#idBooking").val();
        var idBillingFlag = $("#idBillingFlag").val();
	var szRef   = $("#szRef").val();
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bookings.php",{szForwarderReference:fwdref,idBooking:booking,updateInvoiceAry:szRef,idBillingFlag:idBillingFlag},function(result){
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
                    if(idBillingFlag=='booking' || idBillingFlag=='cancel')
                    {
                        $("#invoice_comment_"+result_ary[1]).html(result_ary[2]);
                        $("#invoice_cancel_comment_"+result_ary[1]).html(result_ary[2]);
                    }
                    else
                    {
                        $("#invoice_comment_"+result_ary[1]).html(result_ary[2]);
                    }
			
			$("#invoice_comment_div").attr('style','display:none;');
		}
		else
		{
			$("#invoice_comment_div").html(result_ary[1]);
			$("#invoice_comment_div").attr('style','display:block;');
		}	
		
	});
}

function clear_booking_form()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bookings.php",{mode:'CLEAR_FORM'},function(result){
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#forwarder_booking_top_form").html(result_ary[1]);
			$("#forwarder_booking_bottom_form").html(result_ary[2]);
		}
	});
}
function select_obo_lcl_tr(tr_id,idPricingWTW)
{
	var rowCount = $('#obo_lcl_table tr').length;
	if(rowCount>0)
	{
		for(x=1;x<rowCount;x++)
		{
			new_tr_id="obo_lcl_"+x;
			if(new_tr_id!=tr_id)
			{
				$("#"+new_tr_id).attr('style','');
			}
			else
			{
				var style = $("#"+tr_id).attr('style');
				if(style!='display:none;')
				{
					$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
				}	
			}
		}
	}
	$("#obo_lcl_edit_button").unbind("click");
	$("#obo_lcl_edit_button").click(function(){edit_obo_lcl_services(idPricingWTW)});
	$("#obo_lcl_delete_button").unbind("click");
	$("#obo_lcl_delete_button").click(function(){delete_obo_lcl_services(idPricingWTW,tr_id)});
	//$("#obo_lcl_edit_button").attr('onclick','edit_obo_lcl_services('+idPricingWTW+')');
	//$("#obo_lcl_delete_button").attr('onclick','delete_obo_lcl_services('+idPricingWTW+',"'+tr_id+'")');
	
	$("#obo_lcl_edit_button").attr('style','opacity:1.0;');
	$("#obo_lcl_delete_button").attr('style','opacity:1.0;');
}

function edit_obo_lcl_services(idPricingWTW)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",{mode:'SELECTED_TABLE_DATA',id:idPricingWTW},function(result){		
		//disabling to form 
		//disableOBOTopForm(true,2);	
		
		var rowCount = $('#obo_lcl_table tr').length;
		if(rowCount>0)
		{
			for(x=1;x<rowCount;x++)
			{
				new_tr_id="obo_lcl_"+x;
				$("#"+new_tr_id).attr('onclick','');
			}
		}
		$("#obo_lcl_edit_button").unbind("click");
		$("#obo_lcl_delete_button").unbind("click");
		
		//$("#obo_lcl_edit_button").attr('onclick','');
		//$("#obo_lcl_delete_button").attr('onclick','');
		$("#obo_lcl_edit_button").attr('style','opacity:0.4;');
		$("#obo_lcl_delete_button").attr('style','opacity:0.4;');		
		
		$("#obo_lcl_bottom_span").html(result);
	});
}
function delete_obo_lcl_services(id,divid)
{
	$("#delete_confirm").attr("style","display:block;");
        $("#delete_confirm_button").attr('style','opacity:1;');
	//$("#delete_confirm_button").attr("onclick","delete_obo_lcl_services_confirm('"+id+"','"+divid+"')");
	$("#delete_confirm_button").click(function(){delete_obo_lcl_services_confirm(id,divid)});
}
function delete_obo_lcl_services_confirm(idPricingWTW,tr_id)
{
    $("#delete_confirm_button").unbind("click");    
    $("#delete_confirm_button").attr('onclick','');		
    $("#delete_confirm_button").attr('style','opacity:0.4;');
    
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",{mode:'DELETE_RECORD',id:idPricingWTW},function(result){		
		result_ary = result.split("||||");
		$("#obo_lcl_bottom_span").html(result_ary[0]);
		$("#lcl_table").html(result_ary[1]);
		$("#"+tr_id).attr("style","display:none;");
		document.getElementById("delete_confirm").style.display='none';
		//$("#delete_confirm").attr("style","display:none;");
	});
}

function addPopupScrollClass(id_div)
{
	var pHeight = $(document).height();
	$("#"+id_div+" "+"#popup-bg").css({"height": pHeight+"px", "position":"absolute"});	
}
function removePopupScrollClass(id_div)
{	
	$("#"+id_div+" "+"#popup-bg").css({"height": "0px"});	
/*
	$("body").removeClass("popupscroll");
	$("body").attr('style',"");
	$("body").css('overflow-y','scroll');
*/
}
function enable_get_data(text,text2)
{
	var idDestinationWarehouse = parseInt($("#idDestinationWarehouse").attr('value'));
	var idOriginWarehouse = parseInt($("#idOriginWarehouse").attr('value'));

	if(idOriginWarehouse>0 && idDestinationWarehouse>0 && idOriginWarehouse!=idDestinationWarehouse )
	{	
		$("#obo_cc_getdata_button").unbind("click");
		//$("#obo_cc_getdata_button").attr('onclick','submit_obo_lcl_form("'+text+'","'+text2+'")');
		$("#obo_cc_getdata_button").click(function(){submit_obo_lcl_form(text,text2)});		
		$("#obo_cc_getdata_button").attr("style","opacity:1;"); 
		
		$("#obo_cc_cc_getdata_button").unbind("click");
		//$("#obo_cc_cc_getdata_button").attr('onclick','submit_obo_cc_form("'+text+'","'+text2+'")');
		$("#obo_cc_cc_getdata_button").click(function(){submit_obo_cc_form(text,text2)});		
		$("#obo_cc_cc_getdata_button").attr("style","opacity:1;"); 
	}
	else
	{
		$("#obo_cc_getdata_button").unbind("click");
		
		//$("#obo_cc_getdata_button").attr('onclick','');
		$("#obo_cc_getdata_button").attr("style","opacity:0.4;"); 
		
		$("#obo_cc_cc_getdata_button").unbind("click");
			
		//$("#obo_cc_cc_getdata_button").attr('onclick','');
		$("#obo_cc_cc_getdata_button").attr("style","opacity:0.4;"); 
	}
}


function serch_forwarder_billings()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_billing.php",$("#forwarder_billing_seach_form").serialize(),function(result){
		$("#forwarder_billing_bottom_form").html(result);
	});
}
function select_forwarder_billing_tr(tr_id,id)
{
	var rowCount = $('#billing_table tr').length;
	if(rowCount>0)
	{
		for(x=1;x<rowCount;x++)
		{
			new_tr_id="billing_data_"+x;
			if(new_tr_id!=tr_id)
			{
				$("#"+new_tr_id).attr('style','');
			}
			else
			{
				$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#download_billing").attr("onclick","download_billing_pdf('"+id+"','download_billing_pdf_form')");
	$("#pdf_billing_id").attr("value",id);
	
	$("#download_billing").attr("style","opacity:1;");
}
function download_billing_pdf(billing_id,form_id)
{
	$("#pdf_billing_id").attr("value",billing_id);
	$("#"+form_id).submit();
	//$.post(__JS_ONLY_SITE_BASE__+"/download_pdf.php",{createPDF:'createPDF',billing_id:billing_id},function(result){
	//$("#forwarder_billing_bottom_form").html(result);
	//});
}
function select_forwarder_billing_invoices(id,invoices)
{	
	$("#billing_table_invoice tr").attr('style','background:fff;color:ccc');
	$("#"+invoices).attr('style','background:#DBDBDB;color:#828282;');
	$("#download_billing_invoices").attr({style:'opacity:1;',value:id,onclick:"download_billing_pdf("+id+",'download_billing_pdf_form')"});
}
function clear_billing_form()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_billing.php",{mode:'CLEAR_FORM'},function(result){
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#forwarder_billing_top_form").html(result_ary[1]);
			$("#forwarder_billing_bottom_form").html(result_ary[2]);
		}
	});
}

function validateLandingPageForm_forwarder(formId,idUser,bBypasChecks)
{
    $("#loader").attr('style','display:block;');
    
     
    if(bBypasChecks==1)
    {
        var mode = 'FORWARDER_COURIER_TRY_IT_OUT';
    }
    else
    {
        $("#bottom_search_result").html('');
        $("#calculations_details_div").attr('style','display:none;');
    
        if(isFormInputElementHasDefaultValue('szOriginCity','Type name')) 
        {
           $("#szOriginCity").attr('value',"");
        }
        if(isFormInputElementHasDefaultValue('szOriginPostCode','Optional') || isFormInputElementHasDefaultValue('szOriginPostCode','Type code')) 
        {
            $("#szOriginPostCode").attr('value',"");
        }    
        if(isFormInputElementHasDefaultValue('szDestinationCity','Type name')) 
        {
           $("#szDestinationCity").attr('value',"");
        }
        if(isFormInputElementHasDefaultValue('szDestinationPostCode','Optional') || isFormInputElementHasDefaultValue('szDestinationPostCode','Type code')) 
        {
           $("#szDestinationPostCode").attr('value',"");
        }    
        if(isFormInputElementHasDefaultValue('datepicker1','dd/mm/yyyy'))
        {
           $("#datepicker1").attr('value',"");
        }
        var mode = 'FORWARDER_LCL_TRY_IT_OUT';
    } 
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php?mode="+mode,$("#"+formId).serialize(),function(result){
    
    	 //show_forwarder_top_messges();		     
         var result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {
            $("#regError").attr('style','display:block'); 
            $("#regError").html(result_ary[1]);
            window.scrollTo(50,50);
         }
         if(jQuery.trim(result_ary[0])=='ERROR_COURIER_TRYOUT')
         {  
            $("#try_it_out_form_container").html(result_ary[1]); 
            $("#try_it_out_form_container").attr('style','display:block');
         }
         if(jQuery.trim(result_ary[0])=='MULTI_EMPTY_POSTCODE')
         {
            $("#Transportation_pop").attr('style','display:block');
            $("#regError").attr('style','display:none;');
            $("#Transportation_pop").html(result_ary[1]);
            addPopupScrollClass('Transportation_pop');
         }
         if(jQuery.trim(result_ary[0])=='MULTIREGIO')
         {         	
            $("#regError").attr('style','display:none;');
            $("#Transportation_pop").html(result_ary[1]);
            $("#Transportation_pop").attr('style','display:block;');
         }         
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
            $("#regError").attr('style','display:none;');
            $("#bottom_search_result").html(result_ary[1]);
            $("#bottom_search_result").attr('style','display:block;');
         }
         $("#loader").attr('style','display:none;');
        
    });      
    return false;      
}

function display_fowarder_price_details(idWTW,tr_id,booking_key)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",{mode:'SHOW_PRICE_DETAILS',idWTW:idWTW,booking_key:booking_key},function(result){		     
         $("#calculations_details_div").html(result);
         select_services_tr(tr_id);
         $("#calculations_details_div").attr('style','display:block;');
    });      
}

function display_forwarder_non_accepted_popup(idWTW,idForwarder,idUser,page_url,page_type,courier_service,packing_type,lang,LCDTDQuantityFlag)
{  
    var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');

    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php?lang="+lang,{type:'SHOW_NON_ACCEPTANCE_POPUP',id:idForwarder,idWTW:idWTW,szBookingRandomNum:szBookingRandomNum,page_type:page_type,courier_service:courier_service,packing_type:packing_type,LCDTDQuantityFlag:LCDTDQuantityFlag},function(result){		     
        var result_ary = result.split("||||");
        
        if(result_ary[0]=='NO_RECORD')
        {
            var page_url = result_ary[1]; 
            if(courier_service==1)
            { 
                $("#update_cargo_details_container_div").html(result_ary[2]);
                submit_courier_service_booking(idWTW,page_url,idUser,1); 
            }
            else if(page_type=='NEW_BOOKING_PROCESS')
            {         	
                submit_service_booking(idWTW,page_url,idUser,1);
            }
            else
            {
                submit_service_booking(idWTW,page_url,idUser);
            }
         }	
         else if(result_ary[0]=='SUCCESS')
         {
            if(courier_service==1)
            { 
                $("#update_cargo_details_container_div").html(result_ary[2]);
            }
            $("#customs_clearance_pop1").html(result_ary[1]);
            $("#customs_clearance_pop1").attr('style','display:block;');
            addPopupScrollClass('customs_clearance_pop1');
         }
    });
}

function submit_courier_service_booking(idWTW,booking_url,idUser,new_booking_page)
{
	$("#loader").attr('style','display:block;');
	$("#customs_clearance_pop1").attr('style','display:none;');
	var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
		  
 	if(new_booking_page==1)
 	{
            toggleCourierServiceList(idWTW,booking_url,idUser,1,1); 
 	} 
 	var site_language = $("#iSiteLanguageFooter_hidden").val();
 	$("#loader").attr('style','display:none;');
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php?lang="+site_language,{idWTW:idWTW,szBookingRandomNum:szBookingRandomNum,type:'UPDATE_COURIER_BOOKING',new_booking_page:new_booking_page},function(result){
		
		result_ary = result.split("||||");
	 
		$("#loader").attr('style','display:none;');
		if(result_ary[0]=='SUCCESS')
		{
                    $("#loader").attr('style','display:none;');
                    if(idUser>0 || new_booking_page==1)
                    {
                        var page_url = booking_url +"/"+szBookingRandomNum+"/";
                        redirect_url(page_url);
                    }
                    else
                    {
                        addPopupScrollClass();
                        displayLoginForm(szBookingRandomNum);
                    }	
		}
		else if(result_ary[0]=='REDIRECT')
		{
                    var page_url = booking_url +"/"+szBookingRandomNum+"/"; 
                    redirect_url(page_url);
		}
		else if(result_ary[0]=='UPDATE_CARGO')
		{
                    $("#loader").attr('style','display:none;');
                    if(idUser>0)
                    {
                        update_booking_cargo_details(szBookingRandomNum);
                    }
                    else
                    {
                        addPopupScrollClass();
                        displayLoginForm(szBookingRandomNum);
                    }	 
		} 
	});
}
function sort_cfs_table(sort_by,iWarehouseType)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_cfsNameList.php",{sort_by:sort_by,iWarehouseType:iWarehouseType},function(result){
	        $("#warehouse_list").html(result);});
}
function select_non_acceptance(data,id,tableId)
{	$("#removeNonAccepptanceEmail").attr("style","opacity:.4");
	$("#removeNonAccepptanceEmail").attr("onclick","");
	$("#error").empty();
	$("tr").attr("style","background:#fff;color:#000;");
	$("#"+id).attr("style","background:#DBDBDB;color:#828282;");
	$("#removeNonAccepptanceData").attr("style","opacity:1");
	//$("#removeNonAccepptanceData").attr("onclick","removeNonAccepptance('"+data+"')");
	$("#removeNonAccepptanceData").unbind("click");
	$("#removeNonAccepptanceData").click(function(){removeNonAccepptance(data)});
	
}
function removeNonAccepptance()
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_nonacceptance.php",$('#showNonAcceptanceData').serialize(),function(result){
	
	var str_arr=result.split('||||');
	if($.trim(str_arr[0])=='ERROR')
    {		$("#regError").html(str_arr[1]);
	}	
	if($.trim(str_arr[0])=='SUCCESS')
    {	
		$("#regError").empty();
    	
		$("#showNonAcceptanceData").html(str_arr[1]);
	}
	//$("#selectGoods"+Data).remove();
	$("#removeNonAccepptanceData").attr("style","opacity:.4");
	$("#removeNonAccepptanceData").attr("onclick","");
	});
	
}
function addNonAccepptance()
{	var value=$("#arrNonAcceptanceData").val();
	$("#removeNonAccepptanceEmail").attr("style","opacity:.4");
	$("#removeNonAccepptanceEmail").attr("onclick","");
	$("#removeNonAccepptanceData").attr("style","opacity:.4");
	$("#removeNonAccepptanceData").attr("onclick","");
	//$("tr").attr("style","background:#fff;color:#000;");
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_nonacceptance.php",{mode:'ADD_GOODS',id:value},function(result){
	var str_arr=result.split('||||');
	if($.trim(str_arr[0])=='ERROR')
    {
			$("#error").html(str_arr[1]);
			$("#errorEmail").empty();
	}	
	if($.trim(str_arr[0])=='SUCCESS')
    {	$("#error").empty();
		$("#errorEmail").empty();
    	$("#arrNonAcceptanceData").attr("value","");
		$("#showNonAcceptanceData").html(str_arr[1]);
	}
	});
}
function select_non_acceptanceEmail(data,id,tableId)
{	$("#removeNonAccepptanceData").attr("style","opacity:.4");
	$("#removeNonAccepptanceData").attr("onclick","");
	$("#errorEmail").empty();
	$("tr").attr("style","background:#fff;color:#000;");
	$("#"+id).attr("style","background:#DBDBDB;color:#828282;");
	$("#removeNonAccepptanceEmail").attr("style","opacity:1");
	//$("#removeNonAccepptanceEmail").attr("onclick","removeNonAccepptanceEmail('"+data+"')");
	$("#removeNonAccepptanceEmail").unbind("click");
	$("#removeNonAccepptanceEmail").click(function(){removeNonAccepptanceEmail(data)});
	
}
function removeNonAccepptanceEmail()
{		

	$.post(__JS_ONLY_SITE_BASE__+"/ajax_nonacceptance.php",$('#showNonAcceptanceEmail').serialize(),function(result){
	
	var str_arr=result.split('||||');
	if($.trim(str_arr[0])=='ERROR')
    {		$("#regError").html(str_arr[1]);
	}	
	if($.trim(str_arr[0])=='SUCCESS')
    {	
		$("#regError").empty();
    	
		$("#showNonAcceptanceEmail").html(str_arr[1]);
	}
	//$("#selectGoods"+Data).remove();
	$("#removeNonAccepptanceEmail").attr("style","opacity:.4");
	$("#removeNonAccepptanceEmail").attr("onclick","");
	
	});
	
}
function addNonAccepptanceEmail()
{	var value=$("#nonAccEmail").val();
	$("#removeNonAccepptanceEmail").attr("style","opacity:.4");
	$("#removeNonAccepptanceEmail").attr("onclick","");
	$("#removeNonAccepptanceData").attr("style","opacity:.4");
	$("#removeNonAccepptanceData").attr("onclick","");
	$("tr").attr("style","background:#fff;color:#000;");
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_nonacceptance.php",{mode:'ADD_EMAIL',id:value},function(result){
	var str_arr=result.split('||||');
	if($.trim(str_arr[0])=='ERROR')
    {		$("#error").empty();
			$("#errorEmail").html(str_arr[1]);
	}	
	if($.trim(str_arr[0])=='SUCCESS')
    {	$("#error").empty();
    	$("#errorEmail").empty();
	    $("#nonAccEmail").attr("value","");
		$("#showNonAcceptanceEmail").html(str_arr[1]);
	}
	});
}

/* function to display file name when selected */
$.fn.fileName = function() {
	var $this = $(this),
	$val = $this.val(),
	valArray = $val.split('\\'),
	newVal = valArray[valArray.length-1],
	$button = $this.siblings('.button');
	if(newVal !== '') {
		$button.text(newVal);
	}
};

$().ready(function() {
	/* on change, focus or click call function fileName */
	$('input[type=file]').bind('change focus click', function() {$(this).fileName()});
});


function agree_tnc()
{
	//document.tnc.action = __JS_ONLY_SITE_BASE__+"/terms";
	document.tnc.method = "post"
	document.tnc.submit();
}

function download_term_condition_pdf()
{
	window.location.href = __JS_ONLY_SITE_BASE__+"/downloadTNCPdf.php";
}

function enableHaulageGetData(idForwarder,text,text2)
{
	var haulageCountry = $("#haulageCountry").attr('value');

	  var haulageWarehouse = $("#haulageWarehouse").attr('value');
	  if(haulageCountry!='' && haulageWarehouse!='')
	  {
	  		$("#get_haulage_data").unbind("click");
	   		$("#get_haulage_data").attr('style',"opacity:1.0;min-width:70px;");
	  		//$("#get_haulage_data").attr("onclick","getOBOWarehouseHaulageData('"+idForwarder+"','"+text+"','"+text2+"');");
	  		$("#get_haulage_data").click(function(){getOBOWarehouseHaulageData(idForwarder,text,text2)});
	  }
	  else
	  {
	  		$("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
	  		$("#get_haulage_data").unbind("click");
	  		//$("#get_haulage_data").attr("onclick","");
	  }
}
function clear_try__it_out_form(form_id)
{
	$('INPUT:text,SELECT', '#'+form_id).val(''); 
	$("#bottom_search_result").html('');
	$("#calculations_details_div").attr('style','display:none;');
}
function select_services_tr(tr_id)
{
	var rowCount = $('#service_table tr').length;
	if(rowCount>0)
	{
		for(x=1;x<rowCount;x++)
		{
			new_tr_id="select_services_tr_"+x;
			if(new_tr_id!=tr_id)
			{
				$("#"+new_tr_id).attr('style','');
			}
			else
			{
				var style = $("#"+tr_id).attr('style');
				if(style!='display:none;')
				{
					$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
				}	
			}
		}
	}
}
function resendVerificationCode(url,szBookingRandomNum)
{
	if(szBookingRandomNum == '')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	
	if(szBookingRandomNum=='undefined')
	{
		szBookingRandomNum = '';
	}
	
	$.get(__JS_ONLY_SITE_BASE__+"/resendActivationCode.php",{redirect_uri:url,szBookingRandomNum:szBookingRandomNum},function(result){
		result = "<br>"+result ;
		$("#activationkey").html(result);
		$("#activationkey").attr("style","display:block");
	});
}
function open_edit_profile_popup()
{
	$.get(__JS_ONLY_SITE_BASE__+"/incompleteCompanyInfo.php",function(result){
		$("#forwarder_company_div").html(result);
		$("#forwarder_company_div").attr("style","display:block");
		addPopupScrollClass('forwarder_company_div');
	});
}
function submit_edit_profile_popup()
{
	var value=decodeURIComponent($("#addForwarderContactInfo").serialize());
	$.get(__JS_ONLY_SITE_BASE__+"/incompleteCompanyInfo.php",value,function(result){
		$("#forwarder_company_div").html(result);
		$("#forwarder_company_div").attr("style","display:block");
	});
}

function open_welcome_video()
{
	$("body").attr("id","mainClass");
	$('#popup_container_google_map').attr('style','display:block;');
	addPopupScrollClass('popup_container_google_map');
}

function sort_booking_table(sort_by,sort,divid)
{
	var value=decodeURIComponent($("#forwarder_booking_seach_form").serialize());
	new_value=value+"&sortby="+sort_by+"&sort="+sort;
	if(sort=='ASC')
	{
            var new_sort='DESC';
            var innr_htm = '<span class="sort-arrow-up">&nbsp;</span>';
	}
	else
	{
            var new_sort='ASC';
            var innr_htm = '<span class="sort-arrow-down">&nbsp;</span>';
	}
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bookings.php",new_value,function(result){
	        $("#forwarder_booking_bottom_form").html(result);
	$("#"+divid).attr("onclick","sort_booking_table('"+sort_by+"','"+new_sort+"','"+divid+"');");      
	$("#"+divid).html(innr_htm);  
	if(divid!='booking_date')
	{
		$("#booking_date").html("<span class='sort-arrow-down-grey'></span>");	
	}    
	});
}

function sort_billing_table(sort_feild,sort_by,divid)
{
    var value=decodeURIComponent($("#forwarder_billing_seach_form").serialize());
    new_value=value+"&sort_feild="+sort_feild+"&sort_by="+sort_by;

    if(sort_by=='ASC')
    {
        var new_sort='DESC';
        var innr_htm = '<span class="sort-arrow-down">&nbsp;</span>';
    }
    else
    {
        var new_sort='ASC';
        var innr_htm = '<span class="sort-arrow-up">&nbsp;</span>';
    }
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_billing.php",new_value,function(result){
        $("#forwarder_billing_bottom_form").html(result);
        $("#"+divid).attr("onclick","sort_billing_table('"+sort_feild+"','"+new_sort+"','"+divid+"');");  
        $("#"+divid).html(innr_htm);            
    });
}
function hide_google_map()
{	
    $("#popup_container_google_map").attr("style","display:none;");
    var iframe = window.parent.document.getElementById('google_map_target_1');
    iframe.src='';
    removePopupScrollClass('popup_container_google_map');
}
function insert_value_google_map(latitute,logitude,bulk_upload_flag)
{
    window.top.window.document.getElementById('szLatitude').value = latitute;
    window.top.window.document.getElementById('szLongitude').value = logitude;
    hide_google_map();
    checkCfsDetails(); 
    if(bulk_upload_flag==1)
    {
        update_google_map(latitute,logitude);
    }
}
function chage_old_bookings(page_url,idBooking)
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'BOOKING_EXPIRE',idBooking:idBooking,page_url:page_url},function(result){
        var res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        {
            page_url_1 = page_url +"/"+ res_ary[1]+"/";
            redirect_url(page_url_1);
        }
        else if(res_ary[0]=='REDIRECT_URL')
        {
            var page_url_1 =  res_ary[1];
            redirect_url(page_url_1);
        }
        else
        {	
            $("#booking_expire_div").html(res_ary[1]);  
            $("#booking_expire_div").attr('style','display:block;');  
            addPopupScrollClass('booking_expire_div');
        }	
    });
}

function show_why_pop_up()
{	
	$.post(__JS_ONLY_SITE_BASE__+"/benefitsSigningPpopup.php",{redirect_url:redirect_url,flag:'CREATE_ACCOUNT_WHY'},function(result){
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');
		addPopupScrollClass('ajaxLogin');
	});
}

function check_shipper_consignee_feilds(type)
{
	if(type=='SHIPPER')
	{
		var shipper_arr = new Array();
		shipper_arr[0] = $("#szShipperCompanyName").attr('value');					
		shipper_arr[1] = $("#szShipperFirstName").attr('value');
		shipper_arr[2] = $("#szShipperLastName").attr('value');		
		shipper_arr[3] = $("#szShipperEmail").attr('value');					
		shipper_arr[4] = $("#szShipperPhone").attr('value');		
		shipper_arr[5] = $("#idShipperCountry").attr('value');					
		shipper_arr[6] = $("#szShipperCity").attr('value');
		shipper_arr[7] = $("#szShipperPostcode").attr('value');
		shipper_arr[8] = $("#szShipperAddress").attr('value');
		shipper_arr[9] = $("#szShipperAddress2").attr('value');
		shipper_arr[10] = $("#szShipperAddress3").attr('value');
		shipper_arr[11] = $("#szShipperState").attr('value');
		
		var shipper_arr_old = new Array();
		shipper_arr_old[0] = $("#szShipperCompanyName_old").attr('value');					
		shipper_arr_old[1] = $("#szShipperFirstName_old").attr('value');
		shipper_arr_old[2] = $("#szShipperLastName_old").attr('value');		
		shipper_arr_old[3] = $("#szShipperEmail_old").attr('value');					
		shipper_arr_old[4] = $("#szShipperPhone_old").attr('value');		
		shipper_arr_old[5] = $("#idShipperCountry_old").attr('value');					
		shipper_arr_old[6] = $("#szShipperCity_old").attr('value');
		shipper_arr_old[7] = $("#szShipperPostcode_old").attr('value');
		shipper_arr_old[8] = $("#szShipperAddress_old").attr('value');
		shipper_arr_old[9] = $("#szShipperAddress2_old").attr('value');
		shipper_arr_old[10] = $("#szShipperAddress3_old").attr('value');
		shipper_arr_old[11] = $("#szShipperState_old").attr('value');
		
		var shipper_detail_changed = false;
		for(i=0;i<12;i++)
		{
			if(jQuery.trim(shipper_arr_old[i])!=jQuery.trim(shipper_arr[i]))
			{
				shipper_detail_changed = true;
				break;
			}
		}		
		if(shipper_detail_changed)
		{
			$("#iAddRegistShipper").removeAttr("disabled");
			$('#idRegistShipper option[value="New"]').attr('selected', 'selected');			
		}
	}
	
	if(type=='CONSIGNEE')
	{
		var Consignee_arr = new Array();
		Consignee_arr[0] = $("#szConsigneeCompanyName").attr('value');					
		Consignee_arr[1] = $("#szConsigneeFirstName").attr('value');
		Consignee_arr[2] = $("#szConsigneeLastName").attr('value');		
		Consignee_arr[3] = $("#szConsigneeEmail").attr('value');					
		Consignee_arr[4] = $("#szConsigneePhone").attr('value');		
		Consignee_arr[5] = $("#idConsigneeCountry").attr('value');					
		Consignee_arr[6] = $("#szConsigneeCity").attr('value');
		Consignee_arr[7] = $("#szConsigneePostcode").attr('value');
		Consignee_arr[8] = $("#szConsigneeAddress").attr('value');
		Consignee_arr[9] = $("#szConsigneeAddress2").attr('value');
		Consignee_arr[10] = $("#szConsigneeAddress3").attr('value');
		Consignee_arr[11] = $("#szConsigneeState").attr('value');
		
		var Consignee_arr_old = new Array();
		Consignee_arr_old[0] = $("#szConsigneeCompanyName_old").attr('value');					
		Consignee_arr_old[1] = $("#szConsigneeFirstName_old").attr('value');
		Consignee_arr_old[2] = $("#szConsigneeLastName_old").attr('value');		
		Consignee_arr_old[3] = $("#szConsigneeEmail_old").attr('value');					
		Consignee_arr_old[4] = $("#szConsigneePhone_old").attr('value');		
		Consignee_arr_old[5] = $("#idConsigneeCountry_old").attr('value');					
		Consignee_arr_old[6] = $("#szConsigneeCity_old").attr('value');
		Consignee_arr_old[7] = $("#szConsigneePostcode_old").attr('value');
		Consignee_arr_old[8] = $("#szConsigneeAddress_old").attr('value');
		Consignee_arr_old[9] = $("#szConsigneeAddress2_old").attr('value');
		Consignee_arr_old[10] = $("#szConsigneeAddress3_old").attr('value');
		Consignee_arr_old[11] = $("#szConsigneeState_old").attr('value');
		
		var Consignee_detail_changed = false;
		for(i=0;i<12;i++)
		{
			if(jQuery.trim(Consignee_arr_old[i])!=jQuery.trim(Consignee_arr[i]))
			{
				Consignee_detail_changed = true;
				break;
			}
		}		
		if(Consignee_detail_changed)
		{
			$("#iAddRegistConsignee").removeAttr("disabled");
			$('#idRegistConsignee option[value="New"]').attr('selected', 'selected');			
		}
	}
}

function change_postcode_city_default_text_booking_details(idServiceType,val1,val2,val3)
{	
	var szOriginPostCode_disable = $("#szShipperPostcode").attr('disabled');
	if(szOriginPostCode_disable!='disabled')
	{
		if(idServiceType==1 || idServiceType==2 || idServiceType==5)
	    {
	    	var szOriginPostCode = $("#szShipperPostcode").attr("value");
	    	if(szOriginPostCode=='' || szOriginPostCode==val2)
	    	{
	    		 $("#szShipperPostcode").attr("value",val3);
	    	}	
	    	var szOriginCity = $("#szShipperCity").attr("value");
	    	if(szOriginCity=='')
	    	{
	    		 $("#szShipperCity").attr("value",val1);
	    	}
	    }
	    else
	    {
	    	var szOriginPostCode = $("#szShipperPostcode").attr("value");
	    	if(szOriginPostCode=='' || szOriginPostCode==val3)
	    	{
	    		 $("#szShipperPostcode").attr("value",val2);
	    	}	
	    	var szOriginCity = $("#szShipperCity").attr("value");
	    	if(szOriginCity=='')
	    	{
	    		 $("#szShipperCity").attr("value",val1);
	    	}
	    }
	}
	
	var szDestinationPostCode_disable = $("#szConsigneePostcode").attr('disabled');
	if(szDestinationPostCode_disable!='disabled')
	{
		if(idServiceType==1 || idServiceType==3 || idServiceType==8)
	    {
	    	var szDestinationPostCode = $("#szConsigneePostcode").attr("value");
	    	if(szDestinationPostCode=='' || szDestinationPostCode==val2)
	    	{
	    		 $("#szConsigneePostcode").attr("value",val3);
	    	}	
	    	
	    	var szDestinationCity = $("#szConsigneeCity").attr("value");
	    	if(szDestinationCity=='')
	    	{
	    		 $("#szConsigneeCity").attr("value",val1);
	    	}    	
	    }
	    else
	    {
	    	var szDestinationPostCode = $("#szConsigneePostcode").attr("value");
	    	if(szDestinationPostCode=='' || szDestinationPostCode==val3)
	    	{
	    		 $("#szConsigneePostcode").attr("value",val2);
	    	}
	    	
	    	var szDestinationCity = $("#szConsigneeCity").attr("value");
	    	if(szDestinationCity=='')
	    	{
	    		 $("#szConsigneeCity").attr("value",val1);
	    	} 
	    }
	}
}
function selectBillingSearchDetails(val)
{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_billing.php",{mode:val},function(result){
		n = result.split("|||||");
		$('#date_change').html(n[0]);
		$("#content").html(n[1]);
	});
}
function getInternetExplorerVersion()
{
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}
function checkVersion()
{
  var ver = getInternetExplorerVersion();
  var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;

  if ( ver <= 7 && ver >-1 )
  {
   return ver;
  }
  else if(is_safari)
  {
  	return 's';
  }
  else
  {
  	return 0;
  }
}

function booking_on_hold_zooz(szBookingStatus,aplkey)
{ 
    $("#iBookingStatus").attr("value",'3');
    $("#iBookingPayment").attr("value",'Zooz');
    $("#loader").attr('style','display:block;');
    
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$("#booking_confirmation_form").serialize(),function(result){
        res_ary=result.split("||||") ;
        if(res_ary[0]=='ERROR')
        {
            $("#loader").attr('style','display:none;');
            $("#booking_confirm_error").attr('style','display:block'); 
            $("#booking_confirm_error").html(res_ary[1]);         	
            $("#booking_confirm_error").focus(); 
            window.scrollTo(50,50);
        }
        else if(res_ary[0]=='CONFIRM')
        {
            $("#loader").attr('style','display:none;');
            $("#account_confirm_resend_email").attr('style','display:block'); 
            $("#account_confirm_resend_email").html(res_ary[1]);   
            window.scrollTo(50,50);
        }
        else if(res_ary[0]=='SUCCESS')
        {
            $("#booking_confirm_error").attr('style','display:none;');
            callzoozapi(res_ary[1],aplkey);
            //redirect_url(res_ary[1]);
        }		
        else if(res_ary[0]=='PRICE_CHANGED')
        {
            $("#loader").attr('style','display:none;');
            display_price_changed_popup(res_ary[1]);
        }	
    });
}

function callStripeApi(szApiKey,fChargeableAmount,szCurrency,szDescription,szEmail)
{    
    var site_path = __JS_ONLY_SITE_BASE__;  
    var stripe_path = site_path + "/stripe";  

    var handler = StripeCheckout.configure(
    {
        key: szApiKey,
        image: __JS_ONLY_SITE_BASE__+'/images/favicon.png',
        locale: 'auto',
        email: szEmail, 
        token: function(token) {
          // Use the token to create the charge with a server-side script.
          // You can access the token ID with `token.id`
          var szTokenId = token.id;
          var transactionID = token.card.id;
          if(szTokenId!='')
          {
            $("#pay_button").attr('style','opacity:0.4');
            $("#pay_button").removeAttr('onclick');
            $("#pay_button").addClass('get-price-button-clicked'); 
            var redirect_url = stripe_path + "/callback.php?stripeToken="+szTokenId+"&transactionID="+transactionID; 
            $(location).attr('href',redirect_url);
          }
        }
    }); 
    $(".continue-button" ).on( "click", function(e) {
        // Open Checkout with further options
        handler.open({
          name: 'Transporteca',
          description: szDescription,
          currency: szCurrency,
          amount: fChargeableAmount*100
        });  
        e.preventDefault();
    });
    $( ".continue-button" ).trigger("click" ); 
    $(".continue-button" ).unbind("click");
    // Close Checkout on page navigation
    $(window).on('popstate', function() {
      handler.close();
    }); 
}

function callzoozapi(var_value,aplkey,flag) 
{ 
    var flag = parseInt(flag);
    if(flag==1)
    {
        var flag=true;
    }
    else
    {
        var flag=false;
    }
    $("#loader").attr('style','display:none;');
    $.ajax({
        url: __JS_ONLY_SITE_BASE__+'/zooz/index.php?var_val='+var_value+'&opr_mode=OPEN_TRANSACTION',  // A call to server side to initiate the payment process
        dataType: 'html',
        cache: false,
        success: function(response) {
            var result_ary = response.split("||||"); 
            if(jQuery.trim(result_ary[0])=='ERROR')
            {
                $("#change_price_div").html(result_ary[1]);
                $("#change_price_div").attr('style','display:block;');
            }
            else
            {
                //https://app.zooz.com/mobile/js/zooz-ext-web-ajax.js
                $.getScript('https://app.zooz.com/mobile/js/zooz-ext-web.js', function() {  
                    eval(response); 
                    var path = __JS_ONLY_SITE_BASE__ + "/zooz";
                    zoozStartCheckout({
                        token : data.token,							// Session token recieved from server
                        statusCode: data.statusCode,
                        uniqueId : aplkey,					// unique ID as registered in the developer portal
                        isSandbox : flag,							// true = Sandbox environment
                        returnUrl : path + "/callback.php?utm_nooverride=1",					// return page URL
                        cancelUrl : path + "/callback.php?utm_nooverride=1"					// cancel page URL  
                    });  
                    //$("#zooz-close-button").attr('style','display:none;');   
                });
            }
        }
    });
}	
function transactionComplete(response) 
{ 
    console.log(response);
    // Zooz returns a string, so we will convert it into an object
    response = JSON.parse(response);
    // Now check the status code
    if (response.statusCode === 0) 
    {
        // If you want to avoid any risk of price manipulation, you MUST call an SSJ function to validate
        console.log(response);
    }
    else 
    {
        console.log('Communication error');
    } 
}
function test_func(transactionStatus)
{ 
   var html_icon =  $('#zooz-iframe').find(".icon").html(); 
 
    console.log("Yahooo apun called: "+html_icon);
    $("#zooz-close-button").attr('style','display:none;');  
    $(".icon").attr('style','display:none;');
    $(".total").removeClass('hide-for-tablet');
    $(".total").attr('style','font-size:14px;');
    
    $('.shopping-cart').find('.icon').attr('style','display:none;');;
     
    if($(".icon").length)
    {
        console.log("I M here");
    }
    else
    {
//        setTimeout(function(){
//            test_func();
//        },'1000'); 
    }
}
function updateEmptyPostcodeCity(form_id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#"+form_id).serialize(),function(result){
		result_ary = result.split("||||");

		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			submit_multi_region_city('landing_page_form',result_ary[1],result_ary[2],result_ary[3]);
		}
		else if(jQuery.trim(result_ary[0])=='ERROR')
		{
			$("#popupError").html(result_ary[1]);
			$("#popupError").attr('style','display:block;');			
		}
	});
}
		
function download_referral_invoice(id,user)
{
	//$.get(__JS_ONLY_SITE_BASE__+"/html2pdf/invoiceAdminForwarder.php",{id:id,user:user},function(result){
	 window.open ( __JS_ONLY_SITE_BASE__+"/forwarderReferralInvoice/"+user+"/"+id+"/","_blank"); 
	   //window.open ( __JS_ONLY_SITE_BASE__+"/html2pdf/"+result);       
	//});
	//.success(function(result) { $.post(__JS_ONLY_SITE_BASE__+"/html2pdf/invoiceAdminForwarder.php",{flag:"DELETE",mode:result});});
}	
function download_transfer_details(id,user)
{
	//$.get(__JS_ONLY_SITE_BASE__+"/html2pdf/transferDetails.php",{id:id,user:user},function(result){
	    window.open ( __JS_ONLY_SITE_BASE__+"/forwarderTransfer/"+user+"/"+id+"/","_blank");
	   // window.open ( __JS_ONLY_SITE_BASE__+"/html2pdf/"+result);     
	//});
	//.success(function(result) { $.post(__JS_ONLY_SITE_BASE__+"/html2pdf/transferDetails.php",{flag:"DELETE",mode:result});});
}			
function submitForm(e)
{	
	if(e && parseInt(e.keyCode) == 13)
   {	
      update_invoice_comment_confirm();
   }
	
}
function close_welcome_screen()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",{mode:'SAVE_WELCOME_USER_COOKIE'},function(result){		     
        $("#welcome_screen_div").attr('style','display:none;');
    });   
}
function loginPopUpForwarder()
{
	$("#forwarder_login_form").submit();
/*
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_login.php",function(result){
		$("#regError").html('');
		$("#regError").css('display','none');		     
        $("#ajaxLogin").html(result);
        $("#ajaxLogin").css('display','block');
    });  
 */
}
function forwarderLogin(formId)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_login.php",$("#"+formId).serialize(),function(result){		     
        $("#ajaxLogin").html(result);
    });  
}

function update_expacted_services(button_clicked,page_url,szBookingRandomNum)
{
	if(button_clicked=='back')
	{
		$("#opration_type").attr('value','BACK');
	}
	if(button_clicked=='REQUIREMENT')
	{
		$("#opration_type").attr('value','REQUIREMENT');
	}
	else
	{
		$("#opration_type").attr('value','NEW_SEARCH');
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#no_services_form").serialize(),function(result){		     
       
        result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS_POPUP')
		{
			$('#service_not_found').attr('style','display:none;');	 
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');	 	 	
		}
		else if(result_ary[0]=='ERROR')
		{
			 $("#email_error_message").html(result_ary[1]);
			 $('#email_error_message').attr('style','display:block;');	 	
		}
		else
		{
			if(button_clicked=='back')
			{
				page_url = page_url+"/"+szBookingRandomNum+"/";
				redirect_url(page_url);
			}
			if(button_clicked=='REQUIREMENT')
			{
				page_url = page_url+"/"+szBookingRandomNum+"/";
				redirect_url(page_url);
			}
			else
			{
				redirect_url(page_url);
			}
		}
    });
}
function update_expacted_services_email(page_url)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#update_expacted_services_form").serialize(),function(result){		     
       
        result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			 $("#success_message_expacted_services").html(result_ary[1]);
			 $("#regError").attr('style','display:none;');
			 
		}
		else
		{
			 $("#service_not_found").html(result);
		}
    });
}
function select_forwarder_billings_tr(tr_id,id,flag,iVersion)
{
	var rowCount = $('#booking_table tr').length;
	if(rowCount>0)
	{
		for(x=1;x<rowCount;x++)
		{
			new_tr_id="booking_data_"+x;
			if(new_tr_id!=tr_id)
			{
				$("#"+new_tr_id).attr('style','');
			}
			else
			{
				$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	if(flag=='CREDIT_NOTE')
	{
                if(iVersion==2){
		$("#download_invoice").attr("onclick","download_admin_self_credit_note_pdf('"+id+"')");
                }else{
                    $("#download_invoice").attr("onclick","download_credit_note_pdf('"+id+"')");
                }
	}
	else
	{
            if(iVersion==2){
		$("#download_invoice").attr("onclick","download_admin_self_invoice_pdf('"+id+"')");
            }else{
                $("#download_invoice").attr("onclick","download_invoice_pdf('"+id+"')");
            }
	}
	$("#download_booking").attr("onclick","download_booking_pdf('"+id+"','download_booking_pdf_form')");
	$("#pdf_booking_id").attr("value",id);
	$("#download_invoice").css("opacity",'1');
	$("#download_booking").attr("style","opacity:1;");
}
function download_invoice_pdf(booking_id)
{	
	window.open(__JS_ONLY_SITE_BASE__+'/invoice/'+booking_id+'/',"_blank");
}
function forwarderBillingSearchFormSubmit()
{	
	if($('#idForwarder'))
	{
	var forwarder = $('#idForwarder').val();
	var PATH = __JS_ONLY_SITE_BASE__+"/forwarderBulkExportImport/forwarderDetails/";
	}
	else
	{
	var forwarder = '';
	var PATH =__JS_ONLY_SITE_BASE__+"/forwarderBulkExportImport/forwarderDetails/";
	}
	var value=$('#forwarder_billing_search_form').val();
	
	$.post(__JS_ONLY_SITE_BASE__+"/downloadTable.php",{id:value,forwarder:forwarder},function(result){	
     // $("#hsbody").html(result);
     window.location.href = PATH+result+".xlsx";
    });  
//
}

function updateEmptyPostcodeCity_forwarder(form_id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#"+form_id).serialize(),function(result){
		result_ary = result.split("||||");
		 if(jQuery.trim(result_ary[0])=='SUCCESS')
	     {
	      	$("#regError").attr('style','display:none;');
	      	$("#Transportation_pop").attr('style','display:none;');
	       	$("#bottom_search_result").html(result_ary[1]);
	       	$("#bottom_search_result").attr('style','display:block;');
	     }
	});
}

function signup_affiliate()
{
	var value=decodeURIComponent($("#affiliateSignup").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_affiliateProgram.php",value,function(result){
	$("#affiliatesignup").html(result);});
}

function clear_affiliate_form()
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_affiliateProgram.php",function(result){
	$("#affiliatesignup").html(result);});
}

function ajaxAffliateLogin()
{	
	disableForm();
	
	$.get(__JS_ONLY_SITE_BASE__+"/affiliateLogin.php",function(result){
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr("style","display:block;");
		addPopupScrollClass('ajaxLogin');
	});
		
}
function compare_validate_home_page()
{
	var iCheck = $('#iCheck').attr('value');
	var inner_text = $('#compare_button_name_2').attr('value');
	//console.log("inner text "+inner_text);
	var inner_html = "<span style='left:8px'>"+inner_text+" <img src='"+__JS_ONLY_SITE_BASE__+"/images/325_next.gif' width='20' style='right:8px;top: 6px;' align='right'></span>";
	$("#campare_button_req_page").html(inner_html);
	$("#campare_button_req_page").attr("onclick","");
	
	//$("#campare_button_req_page").attr("style","opacity:0.4;");
	
	/*
	 * iCheck = 1 from landing page
	 * iCheck = 2 from searchMini.php
	 * iCheck = 3 from searchMini2.php
	 * 
	 */
	if(iCheck!=1 && iCheck!=2 && iCheck!=3)
	{
		change_next_step_button_html('NEXT STEP');
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#start_search_form").serialize(),function(result){		     
         result_ary = result.split("||||");  
        
         if(jQuery.trim(result_ary[0])=='ERROR')
         {
         	var iCheck = $('#iCheck').attr('value');
         	if(iCheck==1)
         	{
                    redirect_url(result_ary[1]);
         	}
         	else if(iCheck==2)
     		{
                    top.location.replace( result_ary[1] );
     		}
         	else if(iCheck==3)
     		{
                    /*
                    //This code wont worked in safari so that commenting it. 
                    alert("m here " + result_ary[1]);
                    //top.location.replace( result_ary[1] );
                    var a = parent.document.createElement('a');
                    a.href=result_ary[1];
                    a.target = '_blank';
                    a.id = 'search_min_anchor_tag';
                    document.getElementById('start_search_form').appendChild(a);
                    a.click();

                    $("#start_search_form").attr('action',result_ary[1]);
                    $("#start_search_form").attr('target',"_new");
                    $("#start_search_form").submit();
                    */
                    parent.document.location.href = result_ary[1] ;		
     		}
         	else
         	{
                    change_next_step_button_default_html('NEXT STEP');
                    $("#error_popup").html(result_ary[1]);
                        $('#error_popup').attr('style','display:block;');
                    }
         }
         else if(jQuery.trim(result_ary[0])=='NO_SERVICE_POPUP')
         {
         	var compareButtonName=$('#compare_button_name').attr('value');
         	
         	//var inner_text="SEARCHING SERVICES";
			var inner_html = "<span>"+compareButtonName+"</span>";
			$("#campare_button_req_page").html(inner_html);
			$("#campare_button_req_page").attr("onclick","");
			$("#campare_button_req_page").unbind("click");
			$("#campare_button_req_page").click(function(){compare_validate_home_page()});
			$("#campare_button_req_page").attr('style',"opacity:1");
			
         	
        	change_next_step_button_default_html('NEXT STEP');
         	$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
         }
         else
         {
         	var compareButtonName=$('#compare_button_name').attr('value');
         	var inner_html = "<span>"+compareButtonName+"</span>";
         	$("#campare_button_req_page").html(inner_html);
			$("#campare_button_req_page").attr("onclick","");
			$("#campare_button_req_page").unbind("click");
			$("#campare_button_req_page").click(function(){compare_validate_home_page()});
			$("#campare_button_req_page").attr('style',"opacity:1");
        	var iCheck = $('#iCheck').attr('value');
        
        	if(iCheck==2)
          	{
        		parent.document.location.href = result_ary[1] ;
          	}
        	else if(iCheck==3)
     		{    
     			/*    		
        		//parent.document.location.target = "_blank";
        		//parent.document.location.href = result_ary[1] ;
        		
        		var a = parent.document.createElement('a');
        		a.href=result_ary[1];
        		a.target = '_blank';
        		document.body.appendChild(a);
        		a.click();
        		
        		$("#start_search_form").attr('action',result_ary[1]);
        		$("#start_search_form").attr('target',"_new");
        		$("#start_search_form").submit();
        		*/
        		parent.document.location.href = result_ary[1] ;
        		//redirect_url(result_ary[1]);
     		}
          	else
      		{
          		redirect_url(result_ary[1]);
      		}
         }
      });
}


function compare_validate_home_page_234()
{
	var iCheck = $('#iCheck').attr('value');
	var inner_text="SEARCHING SERVICES";
	var inner_html = "<span style='left:8px'>"+inner_text+" <img src='"+__JS_ONLY_SITE_BASE__+"/images/325_next.gif' width='20' style='right:8px;top: 6px;' align='right'></span>";
	$("#campare_button_req_page").html(inner_html);
	$("#campare_button_req_page").attr("onclick","");
	
	//$("#campare_button_req_page").attr("style","opacity:0.4;");
	
	/*
	 * iCheck = 1 from landing page
	 * iCheck = 2 from searchMini.php
	 * iCheck = 3 from searchMini2.php
	 * 
	 */
	
	if(iCheck!=1 && iCheck!=2 && iCheck!=3)
	{
		change_next_step_button_html('NEXT STEP');
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#start_search_form").serialize(),function(result){		     
         result_ary = result.split("||||");  
        
         if(jQuery.trim(result_ary[0])=='ERROR')
         {
         	var iCheck = $('#iCheck').attr('value');
         	if(iCheck==1)
         	{
         		 redirect_url(result_ary[1]);
         	}
         	else if(iCheck==2)
     		{
         		top.location.replace( result_ary[1] );
     		}
         	else if(iCheck==3)
     		{
         		top.location.replace( result_ary[1] );
     		}
         	else
         	{
         	   change_next_step_button_default_html('NEXT STEP');
		       $("#error_popup").html(result_ary[1]);
			   $('#error_popup').attr('style','display:block;');
			}
         }
         else if(jQuery.trim(result_ary[0])=='NO_SERVICE_POPUP')
         {
         	var compareButtonName=$('#compare_button_name').attr('value');
         	
         	//var inner_text="SEARCHING SERVICES";
			var inner_html = "<span>"+compareButtonName+"</span>";
			$("#campare_button_req_page").html(inner_html);
			$("#campare_button_req_page").attr("onclick","");
			$("#campare_button_req_page").unbind("click");
			$("#campare_button_req_page").click(function(){compare_validate_home_page()});
			$("#campare_button_req_page").attr('style',"opacity:1");
			
         	
        	change_next_step_button_default_html('NEXT STEP');
         	$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
         }
         else
         {
         	var compareButtonName=$('#compare_button_name').attr('value');
         	var inner_html = "<span>"+compareButtonName+"</span>";
         	$("#campare_button_req_page").html(inner_html);
			$("#campare_button_req_page").attr("onclick","");
			$("#campare_button_req_page").unbind("click");
			$("#campare_button_req_page").click(function(){compare_validate_home_page()});
			$("#campare_button_req_page").attr('style',"opacity:1");
        	var iCheck = $('#iCheck').attr('value');
        
        	if(iCheck==2)
          	{
        		parent.document.location.href = result_ary[1] ;
          	}
        	else if(iCheck==3)
     		{
        		$(this).target = "_blank";
        		window.location=result_ary[1]; 
        		
        		//parent.document.location.target="_blank";
        		//parent.document.location.href = result_ary[1] ;  
     		}
          	else
      		{
          		redirect_url(result_ary[1]);
      		}
         }
      });
}
function autofill_cargo_dimentions(mode,checked_flag,kg_value,cm_value,cargo_value)
{
	var cb_flag = $("#iDoNotKnow").prop('checked');
	
	console.log("kg: "+kg_value+"cm: "+cm_value+" cargo: "+cargo_value);
	if(cb_flag)
    {
		$(".checkbox").attr('style','background-position: 0px -50px;')
		$("#iLength1").attr('value','100');
		$("#iWidth1").attr('value','100');
		$("#iHeight1").attr('value','100');
		$("#iQuantity1").attr('value','5');
		$("#iWeight1").attr('value','1000');
		
		$("#hiddenPosition").attr('value','1');
		$("#hiddenPosition1").attr('value','1');
		
		$('#idCargoMeasure1').prop('selectedIndex',0);
		$('#idWeightMeasure1').prop('selectedIndex',0);
		$('#iDangerCargo1').prop('selectedIndex',0);		
		$("#cargo").html("<div id='cargo1'></div>");
		 
		$("#iLength1").attr('readonly','readonly');
		$("#iWidth1").attr('readonly','readonly');
		$("#iHeight1").attr('readonly','readonly');
		$("#iQuantity1").attr('readonly','readonly');
		$("#iWeight1").attr('readonly','readonly'); 
		
		$("#idCargoMeasure1").attr('readonly','readonly');
		$("#idWeightMeasure1").attr('readonly','readonly');
		$("#iDangerCargo1").attr('readonly','readonly'); 
		
		$("#iLength1").attr('style','background-color: #eeeeee;');
		$("#iWidth1").attr('style','background-color: #eeeeee;');
		$("#iHeight1").attr('style','background-color: #eeeeee;');
		$("#iQuantity1").attr('style','background-color: #eeeeee;');
		$("#iWeight1").attr('style','background-color: #eeeeee;');
		
		$('#idCargoMeasure1').attr('onfocus','this.blur();');
		$('#idWeightMeasure1').attr('onfocus','this.blur();');
		$('#iDangerCargo1').attr('onfocus','this.blur();');		
		
		
		$('#idCargoMeasure_hidden').prop('name','searchAry[idCargoMeasure][1]');		
		$('#idWeightMeasure_hidden').prop('name','searchAry[idWeightMeasure][1]');
		$('#iDangerCargo_hidden').prop('name','searchAry[iDangerCargo][1]');
		
		$('#idCargoMeasure1').prop('disabled','disabled');
		$('#idWeightMeasure1').prop('disabled','disabled');
		$('#iDangerCargo1').prop('disabled','disabled');
		
		/*
			$('#idCargoMeasure1').prev().html(cm_value);
			$('#idWeightMeasure1').prev().html(kg_value);
			$('#iDangerCargo1').prev().html(cargo_value);
		*/
		$('#idCargoMeasure1').prev().html('cm');
		$('#idWeightMeasure1').prev().html('kg');
		$('#iDangerCargo1').prev().html('No');
		if(mode='REQUIREMENT_PAGE')
		{
			var szBookingRandomNum = $('#booking_key').attr('value');
			$("#cargo_detail_button_show_option").unbind("click");
			$("#cargo_detail_button_show_option").click(function(){ update_booking_service_type(false,szBookingRandomNum,'CARGO_DETAILS'); });
			$('#cargo_detail_button_show_option').attr('class','orange-button1');
		}
		else
		{
			$('#idCargoMeasure1').attr('style','background-color: #eeeeee;');
			$('#idWeightMeasure1').attr('style','background-color: #eeeeee;margin-left: 14px;min-width: 66px;');
			$('#iDangerCargo1').attr('style','background-color: #eeeeee;');	
		}
	}
	else
	{
		$("#iLength1").removeAttr('readonly');
		$("#iWidth1").removeAttr('readonly');
		$("#iHeight1").removeAttr('readonly');
		$("#iQuantity1").removeAttr('readonly');
		$("#iWeight1").removeAttr('readonly');
		
		$("#iLength1").attr('value','');
		$("#iWidth1").attr('value','');
		$("#iHeight1").attr('value','');
		$("#iQuantity1").attr('value','');
		$("#iWeight1").attr('value','');
		
		$("#iLength1").removeAttr('style');
		$("#iWidth1").removeAttr('style');
		$("#iHeight1").removeAttr('style');
		$("#iQuantity1").removeAttr('style');
		$("#iWeight1").removeAttr('style');
				
		$("#idCargoMeasure1").removeProp('selectedIndex');
		$("#idWeightMeasure1").removeProp('selectedIndex');
		$("#iDangerCargo1").removeProp('selectedIndex'); 
		
		$("#idCargoMeasure1").removeAttr('onfocus');
		$("#idWeightMeasure1").removeAttr('onfocus');
		$("#iDangerCargo1").removeAttr('onfocus');
		
		$('#idCargoMeasure_hidden').prop('name','idCargoMeasure_hidden');		
		$('#idWeightMeasure_hidden').prop('name','idWeightMeasure_hidden');
		$('#iDangerCargo_hidden').prop('name','iDangerCargo_hidden');
		
		$('#idCargoMeasure1').removeAttr('disabled');
		$('#idWeightMeasure1').removeAttr('disabled');
		$('#iDangerCargo1').removeAttr('disabled');
		
		if(mode='REQUIREMENT_PAGE')
		{
			$("#cargo_detail_button_show_option").unbind("click");
			$('#cargo_detail_button_show_option').attr('class','gray-button1');
		}
		else
		{
			$("#idCargoMeasure1").removeAttr('style');
			$("#idWeightMeasure1").attr('style','margin-left: 14px;min-width: 66px;');
			$("#iDangerCargo1").removeAttr('style');
		}
	}
}

function update_booking_cargo(szBookingRandomNum)
{
	if(szBookingRandomNum=='')
	{
            szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value'); 
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",$("#update_booking_cargo_details_form").serialize(),function(result){
		
            var result_ary = result.split("||||"); 
            if(jQuery.trim(result_ary[0])=='ERROR')
            {
        	$("#update_cargo_popup_error").attr('style','display:block'); 
        	$("#update_cargo_popup_error").html(result_ary[1]);
            }  
            else if(jQuery.trim(result_ary[0])=='CARGO_EXCEED_POPUP')
            {
                $("#update_cargo_details").attr('style','display:none');
                $("#all_available_service").attr('style','display:block');
          	$("#all_available_service").html(result_ary[1]);
            }		
            else if(jQuery.trim(result_ary[0])=='SUCCESS')
            {        	
                $("#booking_details_error_div").attr('style','display:none;');
                $("#update_cargo_details").attr('style','display:none;');
                $("#loader").attr('style','display:block;');
                var page_url =result_ary[1]+"/"+szBookingRandomNum +"/";
                display_price_changed_popup_cargo_changed(page_url,szBookingRandomNum,'PRICING_POPUP'); 
               $("#loader").attr('style','display:none;');
            }
	});
}

function update_default_cargo(szBookingRandomNum)
{
	if(szBookingRandomNum=='')
	{
		szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value'); 
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{booking_key:szBookingRandomNum,type:'UPDATE_DEFAULT_CARGO'},function(result){
		if(jQuery.trim(result)=='SUCCESS')
        {    
        	$("#update_cargo_details").attr('style','display:none;');
        }
	});
}

function select_forwarder_billings_referral_tr(tr_id,id,idForwarder,flag)
{
	var rowCount = $('#booking_table tr').length;
	if(rowCount>0)
	{
		for(x=1;x<rowCount;x++)
		{
			new_tr_id="booking_data_"+x;
			if(new_tr_id!=tr_id)
			{
				$("#"+new_tr_id).attr('style','');
			}
			else
			{
				$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	if(flag == "Referral")
	{
		$("#download_invoice").attr("onclick","download_referral_invoice('"+id+"','"+idForwarder+"')");
		$("#download_invoice").css("opacity",'1');
	}
        
        if(flag == "Labels Fee")
	{
		$("#download_invoice").attr("onclick","view_forwarder_courier_label('"+id+"','"+idForwarder+"')");
		$("#download_invoice").css("opacity",'1');
	}
        
	else if(flag == "Transfer")
	{
		$("#download_invoice").attr("onclick","download_transfer_details('"+id+"','"+idForwarder+"')");
		$("#download_invoice").css("opacity",'1');
	}
	else if(flag == "CREDIT_NOTE")
	{
            $("#download_invoice").attr("onclick","download_credit_note_pdf('"+id+"')");
            $("#download_invoice").css("opacity",'1');
	}
        else if(flag == "HANDLING_FEE")
	{
            $("#download_invoice").attr("onclick","download_handling_fee_pdf('"+id+"','"+idForwarder+"')");
            $("#download_invoice").css("opacity",'1');
	}
	else if(flag=='uploadService')
	{
		$("#download_invoice").attr("onclick","download_upload_service_invoice_pdf('"+id+"','"+idForwarder+"')");
		$("#download_invoice").css("opacity",'1');
	}
	$("#download_booking").attr("onclick","");
	$("#download_booking").attr("style","opacity:0.4;");
	
}
function view_forwarder_courier_label(id,idForwarder)
{ 
    window.open ( __JS_ONLY_SITE_BASE__+"/forwarderCourierLabelInvoice/"+idForwarder+"/"+id+"/","_blank");
} 
function download_credit_note_pdf(booking_id)
{	
    window.open(__JS_ONLY_SITE_BASE__+'/creditNote/'+booking_id+'/');
}

function download_handling_fee_pdf(id,idForwarder)
{
    window.open(__JS_ONLY_SITE_BASE__+"/viewHandlingFeeInvoice/"+idForwarder+"/"+id+"/","_blank");
}

function select_forwarder_billings_pend_tr(tr_id,id)
{
	var rowCount = $('#booking_table_pending tr').length;
	if(rowCount>0)
	{
            for(x=1;x<rowCount;x++)
            {
                new_tr_id="booking_data_pend_"+x;
                if(new_tr_id!=tr_id)
                {
                    $("#"+new_tr_id).attr('style','');
                }
                else
                {
                    $("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
                }
            }
	}
	$("#download_invoice_pend").attr("onclick","download_invoice_pdf('"+id+"')");
	$("#download_booking_pend").attr("onclick","download_booking_pdf('"+id+"','download_booking_pdf_form')");
	$("#pdf_booking_id").attr("value",id);
	$("#download_invoice_pend").css("opacity",'1');
	$("#download_booking_pend").attr("style","opacity:1;");
}

function blank_me_password(id,val,val2)
{	
	var inputFeildValue = $("#"+id).attr("value");
	if(inputFeildValue == val || (inputFeildValue == val2 && val2!='') )
	{
		$("#"+id).attr("value","");  
		$("#"+id).prop("type","password"); 
		//$('#'+id).get(0).type = 'password'; 
	}
}

function show_me_password(id,val,val2)
{
	var inputFeildValue = $("#"+id).attr("value");
	
	if(inputFeildValue == '')
	{
		$("#"+id).attr("value",val);
		//$('#'+id).get(0).type = 'text';
		$("#"+id).prop("type","text"); 
	}
}
function update_header_text()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateHeader.php",function(result){
		$("#header_user_name_container").html(result);
	});
}
function display_share_link_popup()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_shareLink.php",function(result){
		$("#share_link_popup_div").html(result);		
		$("#share_link_popup_div").attr("style",'display:block;');
		$("#szShareLinkEmail").focus().selectionEnd;
	});
}
function blank_text(id,val,style_flag)
{	
	var inputFeildValue = $("#"+id).attr("value");
	if(inputFeildValue == val )
	{
		$("#"+id).attr("value",""); 
		if(style_flag==1)
		{
			$("#"+id).removeAttr('style');
		}
	}
}
function show_text(id,val,style_flag)
{
	var inputFeildValue = $("#"+id).attr("value");
	if(inputFeildValue == '')
	{
		$("#"+id).attr("value",val);
		if(style_flag==1)
		{
			$("#"+id).attr('style','font-style:italic');
		}
	}
}
function send_share_site_link()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_shareLink.php",$("#sendUserEmail").serialize(),function(result){
		$("#share_link_popup_div").html(result);
		$("#share_link_popup_div").attr("style",'display:block;');
	});
}
function downloadFormSubmitTest()
{	
	document.bulkExportData.action = __JS_ONLY_SITE_BASE__+"/downloadExportData.php?flag=test";
	document.bulkExportData.method = "post"
	document.bulkExportData.submit();
}
function change_button(text,iUserType)
{
	if(iUserType==1)
	{
		$("#iExistingUser").prop('checked',true);
	}
	if(iUserType==2)
	{
		$("#iNewUser").prop('checked',true);
	}
	$("#sign_in_span").html(text);
}

function changeDownloadType(maxrowexcced)
{
	var downloadType = $("#downloadType").val();
	if(downloadType=='2')
	{
		var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
		var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
		var res_ary_originwarhousevalue = originWarehouseValueTotal.split(";");
		var res_ary_deswarhousevalue = desWarehouseValueTotal.split(";");
		var deswarehouselen=res_ary_deswarhousevalue.length;
		var originwarehouselen=res_ary_originwarhousevalue.length;	
		if(originwarehouselen>0 && deswarehouselen>0)
		{
			//totalCount=parseInt(originwarehouselen)*parseInt(deswarehouselen);
                        var totalCount = getDownloadableWarehouseCount(res_ary_originwarhousevalue,res_ary_deswarhousevalue);
			if(totalCount>maxrowexcced)
			{
				$("#row_excced_data_2").attr("style","display:none");
				$("#row_excced_data_1").attr("style","display:none");
				$("#download_button").unbind("click");
				$("#download_button").attr("style","opacity:0.4;");
				$("#row_excced_data").attr("style","display:block");
				$("#total_row_excced").html(number_format_js(totalCount));
				
			}
			else
			{
				var totaltime=(__TIME_PER_ROW_CREATE__*totalCount);
			 	if(totaltime>60)
			 	{
			 		var minutes = Math.floor(totaltime/60);
			 		var secondsleft = totaltime%60;
                                        secondsleft = Math.ceil(secondsleft);
			 		if(minutes=='1')
			 		{
			 			var minStr=minutes+" minute"; 
			 		}
			 		else
			 		{
			 			var minStr=minutes+" minutes"; 
			 		}
			 		if(secondsleft=='1')
			 		{
			 			var secstr=secondsleft+" second";
			 		}
			 		else
			 		{
			 			var secstr=Math.ceil(secondsleft)+" seconds";
			 		}
			 		
			 		var timestr=minStr+" "+secstr;
			 	}
			 	else
			 	{
			 		var timetaken=Math.ceil(totaltime)
			 		if(timetaken=='1')
			 		{
			 			var timestr=timetaken+" second";
			 		}
			 		else
			 		{
			 			var timestr=timetaken+" seconds";
			 		}
			 	}
				$("#total_row_excced_time").html((timestr));
				$("#row_excced_data").attr("style","display:none");
				$("#row_excced_data_1").attr("style","display:none");
				$("#row_excced_data_2").attr("style","display:block");
				$("#total_row_excced_1").html(number_format_js(totalCount));
			}
		}
	}
	else if(downloadType=='1')
	{
		var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
		var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
		if(originWarehouseValueTotal!='' && desWarehouseValueTotal!='')
		{
			//$("#row_excced_data").attr("style","display:none");
			updatedNote(maxrowexcced);
			//$("#download_button").click(function(){downloadFormSubmit()});
			//$("#download_button").attr("style","opacity:1;");
		}
	}
}


function checkRowCount(maxrowexcced)
{
	var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
	var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
	var res_ary_originwarhousevalue = originWarehouseValueTotal.split(";");
	var res_ary_deswarhousevalue = desWarehouseValueTotal.split(";");
	var deswarehouselen='0';
	var originwarehouselen='0';
	if(res_ary_deswarhousevalue!='')
	{
		var deswarehouselen=res_ary_deswarhousevalue.length;
	}
	if(res_ary_originwarhousevalue!='')
	{
		var originwarehouselen=res_ary_originwarhousevalue.length;	
	}
	var mainurl = $("#mainurl").attr('value');
	var changeDropDown = $("#changeDropDown").attr('value');
	if(originwarehouselen>0 && deswarehouselen>0)
	{
		totalCount=parseInt(originwarehouselen)*parseInt(deswarehouselen);
		if(totalCount>maxrowexcced)
		{
			$("#download_button").unbind("click");
			$("#download_button").attr("style","opacity:0.4;");
			
			//$("#total_row_excced").html(number_format_js(totalCount));
			
			if(totalCount>changeDropDown)
			{
				$("#downloadType").val("1");
			}
			updatedNote(maxrowexcced);
		}
		else
		{
			if(totalCount>changeDropDown)
			{
				$("#download_button").unbind("click");
				$("#download_button").attr("style","opacity:0.4;");
				$("#downloadType").val("1");
				updatedNote(maxrowexcced);
			}
			else
			{
				var totaltime=(__TIME_PER_ROW_CREATE__*totalCount);
			 	if(totaltime>60)
			 	{
			 		var minutes = Math.floor(totaltime/60);
			 		var secondsleft = totaltime%60;
                                        secondsleft = Math.ceil(secondsleft);
			 		if(minutes=='1')
			 		{
			 			var minStr=minutes+" minute"; 
			 		}
			 		else
			 		{
			 			var minStr=minutes+" minutes"; 
			 		}
			 		if(secondsleft=='1')
			 		{
			 			var secstr=secondsleft+" second";
			 		}
			 		else
			 		{
			 			var secstr=Math.ceil(secondsleft)+" seconds";
			 		}
			 		
			 		var timestr=minStr+" "+secstr;
			 	}
			 	else
			 	{
			 		var timetaken=Math.ceil(totaltime)
			 		if(timetaken=='1')
			 		{
			 			var timestr=timetaken+" second";
			 		}
			 		else
			 		{
			 			var timestr=timetaken+" seconds";
			 		}
			 	}
				$("#total_row_excced_time").html((timestr));
				$("#row_excced_data").attr("style","display:none");
				$("#row_excced_data_1").attr("style","display:none");
				$("#row_excced_data_2").attr("style","display:block");
				$("#total_row_excced_1").html(number_format_js(totalCount));
			}
		}
	}
}
	 function number_format_js(numero, params) //indica o nome do plugin que ser� criado com os parametros a serem informados
	 { 
		//parametros default
		var sDefaults = 
			{			
			numberOfDecimals: 2,
			decimalSeparator: '.',
			thousandSeparator: ',',
			symbol: ''
			}
 
		//fun��o do jquery que substitui os parametros que n�o foram informados pelos defaults
		var options = jQuery.extend(sDefaults, params);

		//CORPO DO PLUGIN
		var number = numero; 
		var decimals = options.numberOfDecimals;
		var dec_point = options.decimalSeparator;
		var thousands_sep = options.thousandSeparator;
		var currencySymbol = options.symbol;
		
		var exponent = "";
		var numberstr = number.toString ();
		var eindex = numberstr.indexOf ("e");
		if (eindex > -1)
		{
		exponent = numberstr.substring (eindex);
		number = parseFloat (numberstr.substring (0, eindex));
		}
		
		if (decimals != null)
		{
		var temp = Math.pow (10, decimals);
		number = Math.round (number * temp) / temp;
		}
		var sign = number < 0 ? "-" : "";
		var integer = (number > 0 ? 
		  Math.floor (number) : Math.abs (Math.ceil (number))).toString ();
		
		var fractional = number.toString ().substring (integer.length + sign.length);
		//dec_point = dec_point != null ? dec_point : ".";
		/*fractional = decimals != null && decimals > 0 || fractional.length > 1 ? 
				   (dec_point + fractional.substring (1)) : "";
		if (decimals != null && decimals > 0)
		{
		for (i = fractional.length - 1, z = decimals; i < z; ++i)
		  fractional += "0";
		}*/
		
		thousands_sep = (thousands_sep != dec_point || fractional.length == 0) ? 
					  thousands_sep : null;
		if (thousands_sep != null && thousands_sep != "")
		{
		for (i = integer.length - 3; i > 0; i -= 3)
		  integer = integer.substring (0 , i) + thousands_sep + integer.substring (i);
		}
		
		if (options.symbol == '')
		{
		return sign + integer + fractional + exponent;
		}
		else
		{
		return currencySymbol + ' ' + sign + integer + fractional + exponent;
		}
		//FIM DO CORPO DO PLUGIN	
		
	}


function updatedNote(maxrowexcced)
{
		
		$("#row_excced_data").attr("style","display:none");
		var mainurl = $("#mainurl").attr('value');
		$("#row_excced_data").attr("style","display:none");
		$("#row_excced_data_2").attr("style","display:none");
		$("#row_excced_data_1").attr("style","display:block;text-align:left;valign:middle;");
		$("#download_button").unbind("click");
		$("#download_button").attr("style","opacity:0.4;");
		var value= $("#bulkExportData").serialize();
		$("#row_excced_data_1").html("<span class='fl-15'>Note: Loading....</span><img style='margin: 0 0 0 10px;float:left;position:static;' src='"+mainurl+"/images/325.gif' alt='' />");
		$("#downloadType").attr("disabled","disabled");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",value,function(result){
		
		
	 		result_ary = result.split("||||");
	 
		 	if(parseInt(result_ary[1])>parseInt(maxrowexcced))
		 	{
			 	$("#row_excced_data").attr("style","display:block;");
			 	$("#total_row_excced").html(number_format_js(result_ary[1]));
			 	$("#row_excced_data_1").attr("style","display:none");
			 	$("#row_excced_data_2").attr("style","display:none");
			 }
			 else
			 {
			 	var totaltime=(__TIME_PER_ROW_CREATE__*result_ary[1]);
			 	if(totaltime>60)
			 	{
			 		var minutes = Math.floor(totaltime/60);
			 		var secondsleft = totaltime%60;
                                        secondsleft = Math.ceil(secondsleft);
			 		if(minutes=='1')
			 		{
			 			var minStr=minutes+" minute"; 
			 		}
			 		else
			 		{
			 			var minStr=minutes+" minutes"; 
			 		}
			 		if(secondsleft=='1')
			 		{
			 			var secstr=secondsleft+" second";
			 		}
			 		else
			 		{
			 			var secstr=secondsleft+" seconds";
			 		}
			 		
			 		var timestr=minStr+" "+secstr;
			 	}
			 	else
			 	{
			 		var timetaken=Math.ceil(totaltime)
			 		if(timetaken=='1')
			 		{
			 			var timestr=timetaken+" second";
			 		}
			 		else
			 		{
			 			var timestr=timetaken+" seconds";
			 		}
			 	}
			 	$("#row_excced_data_2").attr("style","display:block;");
			 	$("#total_row_excced_1").html(number_format_js(result_ary[1]));
			 	$("#total_row_excced_time").html((timestr));
			 	$("#row_excced_data_1").attr("style","display:none");
			 	$("#row_excced_data").attr("style","display:none");
			 	$("#download_button").click(function(){downloadFormSubmit()});
				$("#download_button").attr("style","opacity:1;");
			 }
			 $('#downloadType').removeAttr('disabled');
	 	});
}
function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
	  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
	  x=x.replace(/^\s+|\s+$/g,"");
	  if (x==c_name)
	    {
	    return unescape(y);
	    }
	  }
}

function agree_tncleft()
{
	//document.tnc.action = __JS_ONLY_SITE_BASE__+"/terms";
	document.tnc_left.method = "post"
	document.tnc_left.submit();
}
function toggle()
{
	$('#foo').toggle("slow");
	var a= $('#message').html();
	if(a=='Show less')
	{
		$('#message').html('Show more');
	}
	else
	{
		$('#message').html('Show less');
	}
	$.post(__JS_ONLY_SITE_BASE__+'/forwarderShowHideDetails.php',{FLAG:"SET_SHOW_HIDE",value:a},function(result){
	});
	
}

function show_forwarder_top_messges()
{
	if(adminForwarder==null)
	{
	$.post(__JS_ONLY_SITE_BASE__+"/showForwarderMessageDetails.php",function(result){
		$("#top_messages").html(result);
	});
	}
}
function removeInnerContent(id)
{
	$('#'+id).html('');
	removePopupScrollClass(id);
}
function submit_preference_control()
{
var id = $("[name='id']").val();
var szCurrency = $('#szCurrency').val();
var szPayment  = $('#szPayment').val();
var szRss = $('#szRss').val();
var szOldCurrency = $('#szCurrency_hidden').val();
$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{szCurrency:szCurrency,szOldCurrency:szOldCurrency,szPayment:szPayment,szRss:szRss,mode:'SAVE_SYSTEM_CONTROL',id:id},function(result){
removeInnerContent('ajaxLogin');
$("#forwarder_company_div").html(result);
});
}
function showLCLServices(id)
{
	if(id>0)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_dashboard.php",{id:id,mode:'LIST_LCL_SERVICES'},function(result){
		$("#ajaxLogin").html(result);
		addPopupScrollClass('ajaxLogin');
		});
	}
}

function showForwarderWarehouseOBONew(idForwarder,value,type,divid)
{
	var idCountry;
	var szCity;
	var showflag;
	if(type=='country')
	{
		$("#haulageWarehouse").attr("disabled",true);
		$("#idDirection").attr("disabled",true);
		$("#szCity").attr("disabled",true);
		$("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
  		$("#get_haulage_data").attr("onclick",'');
		var idCountry=value;
		var showflag="Warehouse_city";
	}
	else if(type=='city')
	{ 
		$("#haulageWarehouse").attr("disabled",true);
		szCity=value;
		idCountry=$('#haulageCountry').val();
		showflag="Warehouse";
	}
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_haulageOBO.php",{idForwarder:idForwarder,idCountry:idCountry,szCity:szCity,flag:showflag},function(result){
		$("#"+divid).html(result);
		
		if(type=='country')
		{
			var country_id=parseInt(value);
			if(country_id>0)
			{
				 $("#szCity").removeAttr("disabled");   
    			 $("#haulageWarehouse").removeAttr("disabled");
    			 $("#idDirection").removeAttr("disabled");
    			 
    			 var haulageCountry = $("#haulageCountry").attr('value');

				  var haulageWarehouse = $("#haulageWarehouse").attr('value');
				  if(haulageCountry!='' && haulageWarehouse!='')
				  {
    			   		$("#get_haulage_data").attr('style',"opacity:1.0;min-width:70px;");
    			  		$("#get_haulage_data").attr("onclick","('"+idForwarder+"');");
    			  }
    			  else
    			  {
    			  		$("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
    			  		$("#get_haulage_data").attr("onclick","");
    			  }
    			 
			}
			else
			{
				 $("#szCity").attr("disabled",'disabled');   
    			 $("#haulageWarehouse").attr("disabled",'disabled');
    			 $("#idDirection").attr("disabled",'disabled');
    			 $("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
    			 $("#get_haulage_data").attr("onclick",'');
			}
		}		
	});
}

function enableHaulageGetDataNew(idForwarder,text,text2)
{
	var haulageCountry = $("#haulageCountry").attr('value');

	  var haulageWarehouse = $("#haulageWarehouse").attr('value');
	  if(haulageCountry!='' && haulageWarehouse!='')
	  {
	  		$("#get_haulage_data").unbind("click");
	   		$("#get_haulage_data").attr('style',"opacity:1.0;min-width:70px;");
	  		//$("#get_haulage_data").attr("onclick","getOBOWarehouseHaulageData('"+idForwarder+"','"+text+"','"+text2+"');");
	  		$("#get_haulage_data").click(function(){getOBOWarehouseHaulageDataNew(idForwarder,text,text2)});
	  }
	  else
	  {
	  		$("#get_haulage_data").attr('style',"opacity:0.4;min-width:70px;");
	  		$("#get_haulage_data").unbind("click");
	  		//$("#get_haulage_data").attr("onclick","");
	  }
}


function getOBOWarehouseHaulageDataNew(idForwarder)
{
	var haulageCountry = $("#haulageCountry").attr('value');
	var szCity = $("#szCity").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idDirection = $("#idDirection").attr('value');
	var showflag="modellist";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idForwarder:idForwarder,idCountry:haulageCountry,szCity:szCity,haulageWarehouse:haulageWarehouse,idDirection:idDirection,flag:showflag},function(result){
	$("#model_list").html(result);
		$("#szCity").attr("disabled",'disabled');   
		$("#haulageWarehouse").attr("disabled",'disabled');
		$("#idDirection").attr("disabled",'disabled');
		$("#haulageCountry").attr("disabled",'disabled');
		document.getElementById("text_change").innerHTML='SELECT NEW';
		$("#get_haulage_data").unbind("click");
   		$("#get_haulage_data").attr('style',"opacity:1.0;min-width:70px;");
  		//$("#get_haulage_data").attr("onclick","getOBOWarehouseHaulageData('"+idForwarder+"','"+text+"','"+text2+"');");
  		$("#get_haulage_data").click(function(){change_form_haulage_model(idForwarder)});
	});
}

function sel_haulage_model_data(div_id,idForwarder,updownflag,idMapping,iPriorityLevel,idHaulageModel)
{
	var rowCount = $('#view_haulage_model tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_data_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	if(updownflag=='y')
	{
		var changeStatusFlag='I';
	}
	else
	{
		var changeStatusFlag='A';
	}
	
	if(updownflag=='y')
	{
		$("#change_status_button").html("<span style='min-width:70px;'>INACTIVATE</span>");
		
		var totalActive=$("#totalActive").attr('value');
		
		if(parseInt(totalActive)!=parseInt(iPriorityLevel))
		{
			$("#priority_down").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_down").click(function(){change_haulage_model_priority(idMapping,idForwarder,'down',iPriorityLevel)});
			$("#priority_down").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_down").unbind("click");
			$("#priority_down").attr('style',"opacity:0.4");
		}
		if(parseInt(iPriorityLevel)!='1')
		{
			$("#priority_up").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_up").click(function(){change_haulage_model_priority(idMapping,idForwarder,'up',iPriorityLevel)});
			$("#priority_up").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_up").unbind("click");
			$("#priority_up").attr('style',"opacity:0.4");
		}
	}
	else
	{
		$("#change_status_button").html("<span style='min-width:70px;'>ACTIVATE</span>");
		$("#priority_up").unbind("click");
		$("#priority_up").attr('style',"opacity:0.4");
		
		$("#priority_down").unbind("click");
		$("#priority_down").attr('style',"opacity:0.4");
	}
	$("#change_status_button").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#change_status_button").click(function(){change_haulage_model_status(div_id,idMapping,changeStatusFlag,idForwarder,iPriorityLevel)});
	$("#change_status_button").attr('style',"opacity:1");
	$("#openHaulageModelMapped").attr('value',idMapping);
	var idWarehouse = $("#haulageWarehouse").attr('value');
	//$("#edit_pricing_model_button").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	//$("#edit_pricing_model_button").click(function(){open_pricing_model_detail(idHaulageModel,idWarehouse)});
	//$("#edit_pricing_model_button").attr('style',"opacity:1");
	
		var disp=$("#tryitout_result_div_error").css('display');
		if(disp=='block')
		{
			$("#tryitout_result_div_error").attr('style','display:none');
		}
		open_pricing_model_detail(idHaulageModel,idWarehouse);
}


function change_haulage_model_status(div_id,idMapping,changeStatusFlag,idForwarder,iPriorityLevel)
{
	var showflag="ChangeStatusPopup";
	var haulageCountry = $("#haulageCountry").attr('value');
	var szCity = $("#szCity").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idDirection = $("#idDirection").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idForwarder:idForwarder,idCountry:haulageCountry,szCity:szCity,haulageWarehouse:haulageWarehouse,idDirection:idDirection,flag:showflag,idMapping:idMapping,changeStatusFlag:changeStatusFlag},function(result){
	$("#view_haulage_model_div_id").html(result);
	
			var totalActive=$("#totalActive").attr('value');
			var rowCount = $('#view_haulage_model tr').length;
			var t=parseInt(totalActive);
			if(changeStatusFlag=='A')
			{
				
				//var trcount=parseInt(rowCount)-1;
				var div_id="haulage_data_"+totalActive;			
			}
			else
			{
				var trcount=parseInt(rowCount)-1;
				var div_id="haulage_data_"+trcount;
			}
		
			if(rowCount>0)
			{
				for(k=1;k<rowCount;++k)
				{
					newdivid="haulage_data_"+k;
					if(newdivid!=div_id)
					{
						$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
					}
					else
					{
						$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
			}
			if(changeStatusFlag=='A')
			{
				var totalActive=$("#totalActive").attr('value');
				var updateflag='I';
				$("#change_status_button").html("<span style='min-width:70px;'>INACTIVATE</span>");
				if(parseInt(totalActive)!=parseInt(t))
				{
					$("#priority_down").unbind("click");
					$("#priority_down").click(function(){change_haulage_model_priority(idMapping,idForwarder,'down',t,updateflag)});
					$("#priority_down").attr('style',"opacity:1");
				}
				else
				{
					$("#priority_down").unbind("click");
					$("#priority_down").attr('style',"opacity:0.4");
				}
				if(parseInt(t)!='1')
				{
					$("#priority_up").unbind("click");
					$("#priority_up").click(function(){change_haulage_model_priority(idMapping,idForwarder,'up',t,updateflag)});
					$("#priority_up").attr('style',"opacity:1");
				}
				else
				{
					$("#priority_up").unbind("click");
					$("#priority_up").attr('style',"opacity:0.4");
				}
			}
			else
			{
				$("#change_status_button").html("<span style='min-width:70px;'>ACTIVATE</span>");
				var updateflag='A';
				$("#priority_up").unbind("click");
				$("#priority_up").attr('style',"opacity:0.4");
				
				$("#priority_down").unbind("click");
				$("#priority_down").attr('style',"opacity:0.4");
			}	
			
			$("#change_status_button").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#change_status_button").click(function(){change_haulage_model_status(div_id,idMapping,updateflag,idForwarder,t)});
			$("#change_status_button").attr('style',"opacity:1");
	});
}

function change_haulage_model_priority(idMapping,idForwarder,moveFlag,iPriorityLevel,updateflag)
{
	var showflag="moveupdown";
	var haulageCountry = $("#haulageCountry").attr('value');
	var szCity = $("#szCity").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idDirection = $("#idDirection").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idForwarder:idForwarder,idCountry:haulageCountry,szCity:szCity,haulageWarehouse:haulageWarehouse,idDirection:idDirection,flag:showflag,idMapping:idMapping,moveFlag:moveFlag},function(result){
	$("#view_haulage_model_div_id").html(result);
		
		var rowCount = $('#view_haulage_model tr').length;
		if(moveFlag=='down')
		{
			var t=parseInt(iPriorityLevel)+1;
			var div_id="haulage_data_"+t;
		}
		
		if(moveFlag=='up')
		{
			var t=parseInt(iPriorityLevel)-1;
			var div_id="haulage_data_"+t;			
		}
		
			if(rowCount>0)
			{
				for(k=1;k<rowCount;++k)
				{
					newdivid="haulage_data_"+k;
					if(newdivid!=div_id)
					{
						$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
					}
					else
					{
						$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
			}
		
			var totalActive=$("#totalActive").attr('value');
		
			if(parseInt(totalActive)!=parseInt(t))
			{
				$("#priority_down").unbind("click");
				$("#priority_down").click(function(){change_haulage_model_priority(idMapping,idForwarder,'down',t)});
				$("#priority_down").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_down").unbind("click");
				$("#priority_down").attr('style',"opacity:0.4");
			}
			if(parseInt(t)!='1')
			{
				$("#priority_up").unbind("click");
				$("#priority_up").click(function(){change_haulage_model_priority(idMapping,idForwarder,'up',t)});
				$("#priority_up").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_up").unbind("click");
				$("#priority_up").attr('style',"opacity:0.4");
			}
			var updateflag='I';
			$("#change_status_button").html("<span style='min-width:70px;'>INACTIVATE</span>");
			$("#change_status_button").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#change_status_button").click(function(){change_haulage_model_status(div_id,idMapping,updateflag,idForwarder,t)});
			$("#change_status_button").attr('style',"opacity:1");
	});
}

function change_form_haulage_model(idForwarder)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",function(result){
	$("#model_list").html(result);
		$("#szCity").attr("disabled",false);   
		$("#haulageWarehouse").attr("disabled",false);
		$("#idDirection").attr("disabled",false);
		$("#haulageCountry").attr("disabled",false);
		document.getElementById("text_change").innerHTML='Get Data';
		$("#get_haulage_data").unbind("click");
   		$("#get_haulage_data").attr('style',"opacity:1.0;min-width:70px;");
  		//$("#get_haulage_data").attr("onclick","getOBOWarehouseHaulageData('"+idForwarder+"','"+text+"','"+text2+"');");
  		$("#get_haulage_data").click(function(){getOBOWarehouseHaulageDataNew(idForwarder)});
  			var disp=$("#tryitout_result_div_error").css('display');
		if(disp=='block')
		{
			$("#tryitout_result_div_error").attr('style','display:none');
		}
  		
	});
}


function open_pricing_model_detail(idHaulageModel,idWarehouse,mainurl,count,idPricing,directionId,fWmFactor)
{
	var idDirection = $("#idDirection").attr('value');
	var disp = $("#loader").css('display');
	if(disp=='none')
	{
		$("#loader").attr("style","display:block");
	}
	if(idHaulageModel=='1')
	{
		var  divid="haulage_model_"+idHaulageModel;
		var showflag="pricing_zone_detail";
		var imgdiv="haulage_img_"+idHaulageModel;
		var link_div="haulage_model_link_"+idHaulageModel;
		var linkdata="<a href='javascript:void(0)' id='copy_link' onclick='open_copy_haulage_zone_popup(\""+idHaulageModel+"\",\""+idWarehouse+"\",\""+idDirection+"\");'>If  I would like to copy zones from another CFS</a>";
	}
	else if(idHaulageModel=='2')
	{
		var  divid="haulage_model_"+idHaulageModel;
		var showflag="pricing_city_detail";
		var imgdiv="haulage_img_"+idHaulageModel;
		var link_div="haulage_model_link_"+idHaulageModel;
		var linkdata="<a href='javascript:void(0)' id='copy_link' onclick='open_warehouse_city_circle_poup(\""+idWarehouse+"\",\""+idDirection+"\",\""+idHaulageModel+"\");'>Show current cities on map</a>";
	}
	else if(idHaulageModel=='3')
	{
		var  divid="haulage_model_"+idHaulageModel;
		var showflag="pricing_postcode_detail";
		var imgdiv="haulage_img_"+idHaulageModel;
		var link_div="haulage_model_link_"+idHaulageModel;
		var linkdata="";
	}
	else if(idHaulageModel=='4')
	{
		var  divid="haulage_model_"+idHaulageModel;
		var showflag="pricing_distance_detail";
		var imgdiv="haulage_img_"+idHaulageModel;
		var link_div="haulage_model_link_"+idHaulageModel;
		var linkdata="<a href='javascript:void(0)' id='copy_link' onclick='open_copy_haulage_distance_popup(\""+idHaulageModel+"\",\""+idWarehouse+"\",\""+idDirection+"\");'>I would like to copy distances and prices from another CFS</a>";
	}
	var prev_open_model=$("#openHaulageModel").attr('value');
	if(parseInt(prev_open_model)>0)
	{	
		closeHaulagePricingModel(prev_open_model,idWarehouse,idDirection,mainurl);
	}
	
	
	var url=mainurl+"/close.png";
	$("#"+imgdiv).html('<img src="'+url+'" id="'+imgdiv+'" width="15px;">');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
	$("#"+divid).attr("style","display:block");
	$("#"+divid).html(result);
	$("#"+imgdiv).attr("onclick",'');	
	$("#"+imgdiv).unbind("click");
	$("#"+imgdiv).click(function(){closeHaulagePricingModel(idHaulageModel,idWarehouse,idDirection,mainurl)});
	//$("#"+link_div).html(linkdata);
	var disp = $("#loader").css('display');
	if(disp=='block')
	{
		$("#loader").attr("style","display:none");
	}
		$("#openHaulageModel").attr('value',idHaulageModel);
		
			var disp=$("#tryitout_result_div_error").css('display');
		if(disp=='block')
		{
			$("#tryitout_result_div_error").attr('style','display:none');
		}
		if(parseInt(count)>0 && parseInt(idPricing)>0)
		{
			if(idHaulageModel=='1')
			{
				var newDivid="haulage_zone_"+count;
				setTimeout(function(){sel_haulage_model_zone_data(newDivid,idPricing,idHaulageModel,count,idWarehouse,directionId,fWmFactor);},'1500');
			}
			else if(idHaulageModel=='2')
			{
				var newDivid="haulage_city_"+count;
				setTimeout(function(){sel_haulage_model_city_data(newDivid,idPricing,idHaulageModel,count,idWarehouse,directionId,fWmFactor);},'1500');
			}
			else if(idHaulageModel=='3')
			{
				var newDivid="haulage_postcode_"+count;
				setTimeout(function(){sel_haulage_model_postcode_data(newDivid,idPricing,idHaulageModel,count,idWarehouse,directionId,fWmFactor);},'2500');
			}
			else if(idHaulageModel=='4')
			{
				var newDivid="haulage_distance_"+count;
				setTimeout(function(){sel_haulage_model_distance_data(newDivid,idPricing,idHaulageModel,count,idWarehouse,directionId);},'2500');
			}
		}	
	});
}

function sel_haulage_model_zone_data(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection,fWmFactor)
{
	var rowCount = $('#view_haulage_model_zone tr').length;
	
	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_zone_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
		var totalActive=$("#totalActive1").attr('value');
		if(parseInt(totalActive)!=parseInt(iPriorityLevel))
		{
			$("#priority_down_zone").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_down_zone").click(function(){change_haulage_model_zone_priority(id,idHaulageModel,idWarehouse,idDirection,'down',iPriorityLevel,fWmFactor)});
			$("#priority_down_zone").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_down_zone").unbind("click");
			$("#priority_down_zone").attr('style',"opacity:0.4");
		}
		if(parseInt(iPriorityLevel)!='1')
		{
			$("#priority_up_zone").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_up_zone").click(function(){change_haulage_model_zone_priority(id,idHaulageModel,idWarehouse,idDirection,'up',iPriorityLevel,fWmFactor)});
			$("#priority_up_zone").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_up_zone").unbind("click");
			$("#priority_up_zone").attr('style',"opacity:0.4");
		}
		
		$("#delete_zone").unbind("click");
		//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
		$("#delete_zone").click(function(){delete_haulage_zone(id,idHaulageModel,idWarehouse,idDirection)});
		$("#delete_zone").attr('style',"opacity:1");
		
		
		$("#edit_zone").unbind("click");
		//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
		$("#edit_zone").click(function(){edit_haulage_zone(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)});
		$("#edit_zone").attr('style',"opacity:1");
		
		//$("#copy_link").unbind("click");
		//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
		//$("#copy_link").click(function(){open_copy_haulage_zone_popup(id,idHaulageModel,idWarehouse,idDirection)});
		
		
		var showflag="pricing_zone_detail_list";
		var newdiv_id="zone_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:fWmFactor},function(result){
		$("#"+newdiv_id).html(result);
		
		
		//$("#save_zone_detail_line").unbind("click");
		//$("#save_zone_detail_line").attr('style',"opacity:0.4");
		//$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
		
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,flag:'show_ploygon_on_google_map'},function(result){
			$("#display_zone_map").html(result);
			});	
			
				$("#idHaulagePricingModel").attr('value',id);
				var disp = $("#loader").css('display');
				if(disp=='block')
				{
					$("#loader").attr("style","display:none");
				}
                                //updateHaulageZonesCountries();
		});
}

function closeHaulagePricingModel(idHaulageModel,idWarehouse,idDirection,mainurl)
{
	var  divid="haulage_model_"+idHaulageModel;
	var imgdiv="haulage_img_"+idHaulageModel;
	var link_div="haulage_model_link_"+idHaulageModel;
	$("#"+divid).html("");
	$("#"+link_div).html("");
	var url=mainurl+"/plus.png";
	$("#"+imgdiv).html('<img src="'+url+'" id="'+imgdiv+'" width="15px;">');	
	$("#"+imgdiv).unbind("click");
	$("#"+imgdiv).click(function(){open_pricing_model_detail(idHaulageModel,idWarehouse,mainurl)});
}

function change_haulage_model_zone_priority(id,idHaulageModel,idWarehouse,idDirection,moveflag,iPriorityLevel,fWmFactor)
{
	$("#loader").attr("style","display:block");
	var showflag="move_haulage_zone_model";
	var  divid="haulage_model_"+idHaulageModel;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,moveflag:moveflag,flag:showflag},function(result){
	$("#view_haulage_model_zone_div").html(result);
	
		var rowCount = $('#view_haulage_model_zone tr').length;
		if(moveflag=='down')
		{
			var t=parseInt(iPriorityLevel)+1;
			var div_id="haulage_zone_"+t;
		}
		
		if(moveflag=='up')
		{
			var t=parseInt(iPriorityLevel)-1;
			var div_id="haulage_zone_"+t;			
		}
		
			if(rowCount>0)
			{
				for(k=1;k<rowCount;++k)
				{
					newdivid="haulage_zone_"+k;
					if(newdivid!=div_id)
					{
						$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
					}
					else
					{
						$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
			}
		
			var totalActive=$("#totalActive1").attr('value');
			if(parseInt(totalActive)!=parseInt(t))
			{
				$("#priority_down_zone").unbind("click");
				//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
				$("#priority_down_zone").click(function(){change_haulage_model_zone_priority(id,idHaulageModel,idWarehouse,idDirection,'down',t,fWmFactor)});
				$("#priority_down_zone").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_down_zone").unbind("click");
				$("#priority_down_zone").attr('style',"opacity:0.4");
			}
			
			
			
			if(parseInt(t)!='1')
			{
				$("#priority_up_zone").unbind("click");
				//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
				$("#priority_up_zone").click(function(){change_haulage_model_zone_priority(id,idHaulageModel,idWarehouse,idDirection,'up',t,fWmFactor)});
				$("#priority_up_zone").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_up_zone").unbind("click");
				$("#priority_up_zone").attr('style',"opacity:0.4");
			}
		
		$("#loader").attr("style","display:none");
		//sel_haulage_model_zone_data(div_id,id,idHaulageModel,t,idWarehouse,idDirection,fWmFactor);
	});
}

function delete_haulage_zone_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="haulage_model_"+idHaulageModel;
	var showflag="delete_haulage_zone_confirm";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		addPopupScrollClass(divid);
		});
}


function delete_haulage_zone(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="zone_detail_delete_"+idHaulageModel;
	var showflag="delete_haulage_zone";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr("style","display:block");
		addPopupScrollClass(divid);
	});
}

function close_delete_confirm_popup(idHaulageModel)
{
	if(idHaulageModel=='1')
	{
		var  divid="zone_detail_delete_"+idHaulageModel;
	}
	else if(idHaulageModel=='2')
	{
		var  divid="city_detail_delete_"+idHaulageModel;
	}
	else if(idHaulageModel=='3')
	{
		var  divid="postcode_detail_delete_"+idHaulageModel;
	}
	else if(idHaulageModel=='4')
	{
		var  divid="distance_detail_delete_"+idHaulageModel;
	}
	$("#"+divid).html("");
	removePopupScrollClass(divid);
}

function sel_haulage_zone_detail_data(div_id,id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel,wmfactor)
{
	var rowCount = $('#view_haulage_model_zone_detail tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_pricing_zone_data_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#delete_zone_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_zone_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_zone_detail").attr('style',"opacity:1");
	
	
	$("#zone_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#zone_edit_detail").click(function(){edit_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg,wmfactor)});
	$("#zone_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_zone_detail_line").unbind("click");
	$("#cancel_zone_detail_line").attr('style',"opacity:0.4");
	
	
	$("#save_zone_detail_line").unbind("click");
	$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingZoneData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
}

function delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)
{
	if(idHaulageModel=='1')
	{
		var  divid="zone_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_zone_line";
	}
	else if(idHaulageModel=='2')
	{
		var  divid="city_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_city_line";
	}
	else if(idHaulageModel=='3')
	{
		var  divid="postcode_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_postcode_line";
	}
	else if(idHaulageModel=='4')
	{
		var  divid="distance_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_distance_line";
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,iUpToKg:iUpToKg,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).attr("style","display:block");
		$("#"+divid).html(result);
		addPopupScrollClass(divid);
	});
}

function delete_haulage_pricing_line_confirm(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)
{

	if(idHaulageModel=='1')
	{
		var  divid="zone_detail_list_"+idHaulageModel;
		var  div_id="zone_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_zone_line_confirm";
	}
	else if(idHaulageModel=='2')
	{
		var  divid="city_detail_list_"+idHaulageModel;
		var  div_id="city_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_city_line_confirm";
	}
	else if(idHaulageModel=='3')
	{
		var  divid="postcode_detail_list_"+idHaulageModel;
		var  div_id="postcode_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_postcode_line_confirm";
	}
	else if(idHaulageModel=='4')
	{
		var  divid="distance_detail_list_"+idHaulageModel;
		var  div_id="distance_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_distance_line_confirm";
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+div_id).attr("style","display:none");
		if(idHaulageModel=='1')
		{
			$("#save_zone_detail_line").unbind("click");
			$("#save_zone_detail_line").attr('style',"opacity:1");
			$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
		}
		else if(idHaulageModel=='2')
		{
			$("#save_city_detail_line").unbind("click");
			$("#save_city_detail_line").attr('style',"opacity:1");
			$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
		}
		else if(idHaulageModel=='3')
		{
			$("#save_postcode_detail_line").unbind("click");
			$("#save_postcode_detail_line").attr('style',"opacity:1");
			$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});

		}
		else if(idHaulageModel=='4')
		{
			$("#save_distance_detail_line").unbind("click");
			$("#save_distance_detail_line").attr('style',"opacity:1");
			$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
		}
	});
}

function edit_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg,wmfactor)
{
	var showflag="edit_haulage_zone_line";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,wmfactor:wmfactor},function(result){
		$("#zone_edit_form").html(result);
		$("#mode").attr('value','edit');		
		$("#delete_zone_detail").unbind("click");
		$("#delete_zone_detail").attr('style',"opacity:0.4");
	
	
		$("#zone_edit_detail").unbind("click");
		$("#zone_edit_detail").attr('style',"opacity:0.4")
		
		$("#cancel_zone_detail_line").unbind("click");
		$("#cancel_zone_detail_line").attr('style',"opacity:1");
		$("#cancel_zone_detail_line").click(function(){cancel_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
		
		$("#save_zone_detail_line").attr("onclick",'');	
		$("#save_zone_detail_line").unbind("click");
		$("#save_zone_detail_line").attr('style',"opacity:1");
		$("#save_zone_detail_line").click(function(){save_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	});
}
function point_it(event)
{
	pos_x = event.offsetX?(event.offsetX):event.pageX-document.getElementById("pointer_div").offsetLeft;
	pos_y = event.offsetY?(event.offsetY):event.pageY-document.getElementById("pointer_div").offsetTop;
	document.getElementById("cross").style.left = (pos_x-1) ;
	document.getElementById("cross").style.top = (pos_y-15) ;
	document.getElementById("cross").style.visibility = "visible" ;
	alert(" X = "+pos_x+" Y ="+pos_y);
	//document.pointform.form_x.value = pos_x;
	//document.pointform.form_y.value = pos_y;
}

function draw_canvas_image_obo(szOriginWhsName,szDestinationWhsName,image_path,mode)
{
	var canvas = document.getElementById("myCanvas");
     var context = canvas.getContext("2d");
     var imageObj = new Image();
     imageObj.onload = function()
     {
         context.drawImage(imageObj, 5, 5);
         context.font = "italic 12pt Calibri";
         context.fillStyle = '#000000';
         if(mode=='OBO')
         {
         	 context.fillStyle = 'Black';
         	 //context.fillText(szOriginWhsName, 100,63);
         	 //context.fillText(szDestinationWhsName, 530,63);
         }         
         if(mode=='HAULAGE_PRICING')
         {
         	 context.fillStyle = 'Black';
         	 context.fillText(szOriginWhsName, 100,63);
         	 context.fillText(szDestinationWhsName, 530,63);
         }
     };
     imageObj.src = image_path ; 
}

function draw_canvas_service_type_images(szOriginWhsName,szDestinationWhsName,image_path,mode,alt_text)
{
	var canvas = document.getElementById("myCanvas");
     var context = canvas.getContext("2d");
     var imageObj = new Image();
     imageObj.onload = function()
     {
         context.drawImage(imageObj, 5, 5);
         context.font = "12pt Calibri";
         context.fillStyle = '#FFFFFF';
         //context.fillText(szOriginWhsName, 2, 10);                  
         if(szDestinationWhsName.length > 20)
         {
         	//szDestinationWhsName = szDestinationWhsName.substr(0,18)+"..";
         }
         //context.fillText(szDestinationWhsName, 745,10);
     };
     imageObj.src = image_path ; 
}

function draw_canvas_image_bulk()
{
	var canvas = document.getElementById("myCanvas");
     var context = canvas.getContext("2d");
     var imageObj = new Image();
     imageObj.onload = function(){
         context.drawImage(imageObj, 10, 10);
         context.font = "11pt Calibri";
         context.fillStyle = 'Blue';
         context.fillText("Freight and Surcharges", 294, 18);
         context.font = "12pt Calibri";
         context.fillStyle = 'Blue';
         context.fillText("Shipper", 20, 45);
         context.fillText("Consignee", 640, 45);
         context.fillText("EXW", 35, 120);
         context.fillText("FCA", 120, 120);
         context.fillText("<?=$szOriginWhsName?>", 100,63);
         context.fillText("<?=$szDestinationWhsName?>", 530,63);
         context.fillText("CFR", 420, 120);
         context.fillText("DAT", 575, 120);
         context.fillText("DDU", 650, 120);
         context.fillText("Origin port", 240, 45);
         context.fillText("Destination port", 400, 45);
         context.fillText("Origin Charges", 150, 18);
         
         context.fillText("Destination Charges", 450, 18);
         context.fillText("CFS", 140, 45);
         context.fillText("CFS", 555, 45);
         context.fillText("FOB", 280, 120);
     };
     imageObj.src = "<?=__BASE_STORE_IMAGE_URL__?>/CargoFlowLCL.png"; 
}

function cancel_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	$("#delete_zone_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_zone_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_zone_detail").attr('style',"opacity:1");
	
	
	$("#zone_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#zone_edit_detail").click(function(){edit_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	$("#zone_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_zone_detail_line").unbind("click");
	$("#cancel_zone_detail_line").attr('style',"opacity:0.4");
	
	$("#save_zone_detail_line").unbind("click");
	//$("#save_zone_detail_line").attr('style',"opacity:0.4");
	$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingZoneData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
	var disp = $("#regError").css('display');
	if(disp=='block')
	{
		$("#regError").attr('style',"display:none");
	}
}

function save_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	if(idHaulageModel=='1')
	{
		var  divid="zone_edit_form";
		var value=$("#updatePricingZoneData").serialize();
		var showflag="add_zone_pricing_line";
	}
	else if(idHaulageModel=='2')
	{
		var  divid="city_edit_form";
		var showflag="add_city_pricing_line";
		var value=$("#updatePricingCityData").serialize();
	}
	else if(idHaulageModel=='3')
	{
		var  divid="postcode_edit_form";
		var showflag="add_postcode_pricing_line";
		var value=$("#updatePricingPostCodeData").serialize();
	}
	else if(idHaulageModel=='4')
	{
		var  divid="distance_edit_form";
		var showflag="add_distance_pricing_line";
		var value=$("#updatePricingDistanceData").serialize();
	}
	var newvalue=value+"&id="+id+"&idHaulageModel="+idHaulageModel+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&idHaulagePricingModel="+idHaulagePricingModel+"&iUpToKg1="+iUpToKg+"&flag="+showflag;
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
		$("#"+divid).html(result);
	});
}

function save_haulage_zone_line_confirm(id,idHaulageModel,idWarehouse,idDirection,wmfactor)
{
		var showflag="pricing_zone_detail_list";
		var newdiv_id="zone_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:wmfactor},function(result){
		$("#"+newdiv_id).html(result);
		$("#mode").attr('value','add');
		$("#idHaulagePricingModel").attr('value',id);
		$("#save_zone_detail_line").attr("style","opacity:0.4");
		$("#save_zone_detail_line").unbind("click");
		//$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
		$("#loader").attr('style','display:none;');
		});
}

function add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)
{
    if(idHaulageModel=='1')
    {
        var  divid="zone_edit_form";
        var value=$("#updatePricingZoneData").serialize();
        var showflag="add_zone_pricing_line";
    }
    else if(idHaulageModel=='2')
    {
        var  divid="city_edit_form";
        var showflag="add_city_pricing_line";
        var value=$("#updatePricingCityData").serialize();
    }
    else if(idHaulageModel=='3')
    {
        var  divid="postcode_edit_form";
        var showflag="add_postcode_pricing_line";
        var value=$("#updatePricingPostCodeData").serialize();
    }
    else if(idHaulageModel=='4')
    {
        var  divid="distance_edit_form";
        var showflag="add_distance_pricing_line";
        var value=$("#updatePricingDistanceData").serialize();
    }

    var newvalue=value+"&idHaulageModel="+idHaulageModel+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&idHaulagePricingModel="+idHaulagePricingModel+"&flag="+showflag;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
        $("#"+divid).html(result);
    });
} 
function edit_haulage_zone(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)
{
    var  divid="zone_detail_delete_"+idHaulageModel;
    var showflag="edit_zone";
	
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'edit',iPriorityLevel:iPriorityLevel},function(result){
        $("#"+divid).html(result);
        $("#"+divid).attr('style',"display:block");
        $("#save_zone").attr("onclick","");
        $("#save_zone").unbind("click");
        $("#save_zone").attr("style","opacity:1");
        $("#save_zone").click(function(){save_zone_data()});
        var totalActive=$("#mode").attr('value');
        $("#change_text").html('Save Zone');
        $("#szName").focus();
        addPopupScrollClass(divid);
    });
}
function download_cfs_file_format(mode,iWarehouseType)
{
    window.location.href = __JS_ONLY_SITE_BASE__+"/ajax_bulkCFSLocations.php?mode="+mode+"&iWarehouseType="+iWarehouseType;    
}
function upload_cfs_file()
{
    $('#uploadCfsFile').submit();
}
function close_popup_content(iWarehouseType)
{
    if(iWarehouseType==2)
    {
        window.location.href= __JS_ONLY_SITE_BASE__+"/airportWarehouseLocationBulk/";  
    }
    else
    {
        window.location.href= __JS_ONLY_SITE_BASE__+"/CFSLocationBulk/";  
    } 
}
function toolLatLongPopup()
{	
	//$('#google_map_hidden_form').submit();
	$("#popup_container_google_map").attr('style','display:block;');
	$("body").attr("id","mainClass");	
	addPopupScrollClass('popup_container_google_map');
}
function hideGoogleMapPopUp()
{
	$("#popup_container_google_map").attr('style','display:none;');
	removePopupScrollClass('popup_container_google_map');
}
function showLCLServicesSort(id,sortCode)
{
	if(id>0)
	{	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_dashboard.php",{id:id,mode:'LIST_LCL_SERVICES',sortCode:sortCode},function(result){
		$("#ajaxLogin").html(result);
		});
	}
}

function cancel_zone(idHaulageModel)
{
	if(idHaulageModel=='1')
	{
		var  divid="zone_detail_delete_"+idHaulageModel;
	}
	else if(idHaulageModel=='4')
	{
		var  divid="distance_detail_delete_"+idHaulageModel;
	}
	else if(idHaulageModel=='3')
	{
		var  divid="postcode_detail_delete_"+idHaulageModel;
	}
	else if(idHaulageModel=='2')
	{
		var  divid="city_detail_delete_"+idHaulageModel;
	}
	$("#"+divid).attr('style',"display:none");
	removePopupScrollClass(divid);
}

function clear_zone_map(id,idWareHouse)
{
	var iframe = window.parent.document.getElementById('draw_polygon_iframe');
	iframe.src=__JS_ONLY_SITE_BASE__+'/draw_polygon.php?idHaulagePricingModel='+id+'&idWareHouse='+idWareHouse;
}

function save_zone_data()
{	
	var value=$("#zoneAddEditForm").serialize();
	var newvalue=value+"&flag=update_zone";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_zone_form_new").html(result);
	});
}

function add_zone_popup(idHaulageModel,idDirection,idWarehouse)
{
	var  divid="zone_detail_delete_"+idHaulageModel;
	var showflag="open_add_zone_popup";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		$("#szName").focus();
		addPopupScrollClass(divid);
	});
}

function add_zone_data()
{
	var value=$("#zoneAddEditForm").serialize();
	var newvalue=value+"&flag=add_zone";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_zone_form_new").html(result);
	});
}

function open_copy_haulage_zone_popup(idHaulageModel,idWarehouse,idDirection)
{
	var  divid="zone_detail_delete_"+idHaulageModel;
	var showflag="open_copy_zone_popup";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		addPopupScrollClass(divid);
	});
}


function showCopyWarehouse(idForwarder,value,type,divid,idHaulageModel,iWarehouseType)
{
	var idCountry;
	var szCity;
	var showflag;
	var disp = $("#show_copy_data").css('display');
	if(disp=='block')
	{
		$("#show_copy_data").html('');
	}
	if(type=='country')
	{
            var idCountry=value;
            var showflag="Warehouse_city";
            $("#szCityZone").attr('disabled',true);
            $("#haulageWarehouseZone").attr('disabled',true);
            $("#idDirectionZone").attr('disabled',true);
	}
	else if(type=='city')
	{ 
            szCity=value;
            idCountry=$('#haulageCountryZone').val();
            showflag="Warehouse";
            $("#haulageWarehouseZone").attr('disabled',true);
	}
	var idZoneWarehouse=$('#idZoneWarehouse').val();
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idForwarder:idForwarder,idCountry:idCountry,szCity:szCity,flag:showflag,idZoneWarehouse:idZoneWarehouse,idHaulageModel:idHaulageModel},function(result){
		$("#"+divid).html(result);
		//$("#show_copy_data").attr("style","display:none");
		$("#haulageWarehouseZone").attr('disabled',false);
		if(type=='country')
		{
			var country_id=parseInt(value);
			if(country_id>0)
			{
				 $("#"+divid).attr('style','display:block');	
				 $("#szCityZone").removeAttr("disabled");   
    			 $("#haulageWarehouseZone").removeAttr("disabled");
    			 $("#idDirectionZone").removeAttr("disabled");
    			 
    			 var haulageCountry = $("#haulageCountryZone").attr('value');

				  var haulageWarehouse = $("#haulageWarehouseZone").attr('value');		
    			 
			}
			else
			{
				 //$("#"+divid).attr('style','display:none');
				 $("#szCityZone").attr("disabled",'disabled');   
    			 $("#haulageWarehouseZone").attr("disabled",'disabled');
    			 $("#idDirectionZone").attr("disabled",'disabled');
   			}
		}		
	});
}

function showWarehouseZoneData(idForwarder,idHaulageModel)
{
	var idCountry=$('#haulageCountryZone').val();
	var szCity=$('#szCityZone').val();
	var idWarehouse=$('#haulageWarehouseZone').val();
	var idDirection=$('#idDirectionZone').val();
	var haulageWarehouse=$('#haulageWarehouse').val();
	var haulageDirection=$('#idDirection').val();
	
	if(idHaulageModel=='1')
	{
		var showflag='show_copy_zone_data';
	}
	else if(idHaulageModel=='4')
	{
		var showflag='show_copy_distance_data';
	}
	else if(idHaulageModel=='3')
	{
		var showflag='show_copy_postcode_data';
	}
	else if(idHaulageModel=='2')
	{
		var showflag='show_copy_city_data';
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idForwarder:idForwarder,idCountry:idCountry,szCity:szCity,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,idHaulageModel:idHaulageModel,haulageWarehouse:haulageWarehouse,haulageDirection:haulageDirection},function(result){
            $("#show_copy_data").html(result);
            $("#show_copy_data").attr("style","display:block;height:119px;");
        });
}

function copy_zone()
{
	var idCountry=$('#haulageCountryZone').val();
	var szCity=$('#szCityZone').val();
	var idWarehouse=$('#haulageWarehouseZone').val();
	var idDirection=$('#idDirectionZone').val();
	var showflag='copy_zone_data';
	var value=$("#copyZone").serialize();
	var newvalue=value+"&idCountry="+idCountry+"&szCity="+szCity+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&flag="+showflag;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
		$("#show_copy_data").html(result);
		$("#show_copy_data").attr("style","display:block");
        });
}

function enable_copy_pricing(id,flag)
{
	var div_id="szCopyZone_"+id;
	var div_pricing_id="szCopyZonePricing_"+id;
	var iCheckedCount=$("#iCheckedCount").attr('value');
	
	if($("#"+div_id).is(':checked')==true)
    {
    	var newvalue=parseInt(iCheckedCount)+1;
    	$("#"+div_pricing_id).attr("disabled",false);
    	$("#iCheckedCount").attr('value',newvalue);
    }
    else
    {
    	var newvalue=parseInt(iCheckedCount)-1;
    	$("#"+div_id).removeProp('checked');
    	$("#"+div_pricing_id).attr("disabled",true);
    	$("#"+div_pricing_id).removeAttr('checked','');
    	$("#iCheckedCount").attr('value',newvalue);
    }
    
    var iCheckedCount=$("#iCheckedCount").attr('value');
  
    if(parseInt(iCheckedCount)>0)
    {
     	if(flag=='zone')
     	{
     		$("#copy_zone").unbind("click");
			$("#copy_zone").click(function(){copy_zone()});
			$("#copy_zone").attr('style',"opacity:1");
     	}
     	else if(flag=='distance')
     	{
     		$("#copy_distance").unbind("click");
			$("#copy_distance").click(function(){copy_distance()});
			$("#copy_distance").attr('style',"opacity:1");
     	}
     	else if(flag=='postcode')
     	{
     		$("#copy_postcode").unbind("click");
			$("#copy_postcode").click(function(){copy_postcode()});
			$("#copy_postcode").attr('style',"opacity:1");
     	}
     	else if(flag=='city')
     	{
     		$("#copy_city").unbind("click");
			$("#copy_city").click(function(){copy_city()});
			$("#copy_city").attr('style',"opacity:1");
     	}
    }
    else
    {
    	if(flag=='zone')
     	{
     		$("#copy_zone").unbind("click");
			$("#copy_zone").attr('style',"opacity:0.4");
     	}
     	else if(flag=='distance')
     	{
     		$("#copy_distance").unbind("click");
			$("#copy_distance").attr('style',"opacity:0.4");
     	}
     	else if(flag=='postcode')
     	{
     		$("#copy_postcode").unbind("click");
			$("#copy_postcode").attr('style',"opacity:0.4");
     	}
     	else if(flag=='city')
     	{
     		$("#copy_city").unbind("click");
			$("#copy_city").attr('style',"opacity:0.4");
     	}
    }
}
function submit_warehouse_data(iWarehouseType,batch_number)
{
	var CfsListing = $('#CFSListing').val();
	
	if(CfsListing == '')
	{ 
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SHOW_POP_UP'},function(result){
		$('#ajaxLogin').html(result);
		$('#ajaxLogin').css('display','block');
		//return false;
            });
	}
	else
	{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",$('#addEditWarehouse').serialize(),function(result){
		result_ary = result.split("|||||");
		if($.trim(result_ary[0])=='Error')
		{
                    $('#Error-log').css('display','block');
                    $('#Error-log').html(result_ary[1]);
                    check = true;
		}
		else
		{
                    $('#Error-log').html('');
                    //window.location.reload();
                    $.post(__JS_ONLY_SITE_BASE__+"/CFSBulkUploadUpdate.php",{mode:'CHECK',iWarehouseType:iWarehouseType,batch_number:batch_number},function(result){
			var result_ary = result.split("|||||");
			if(result_ary[0]=='SUCCESS')
			{
                            $(document).load().scrollTop(0);
                            $('.hsbody-2-right').html(result_ary[1]);
			}
			else
			{
                            window.location.href = window.location.href;
			}
                        
                    });
		}
		});	
	}
}
function delete_warehouse_data(id,batch,iWarehouseType)
{
    check = false;
    if(parseInt(id)>0)
    {
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'DELETE_WAREHOUSE',id:id,batch:batch,iWarehouseType:iWarehouseType},function(result){
            if(result=='')
            {
                window.location.href = window.location.href;
            }
        });	
    }
}
function checkPageRedirection(path)
{		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'LEAVE_PAGE',path:path},function(result){
			//check = true;
			$('#ajaxLogin').css('display','block');
			$('#ajaxLogin').html(result);
		});	
}
function showHidePopUp()
{
	$('#ajaxLogin').html('');
	$('#ajaxLogin').css('display','none');
	removePopupScrollClass('ajaxLogin');
}
function transfer(id,path)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{id:id,mode:'TRANSFER_BATCH'},function(result){
        window.location = path;
    });
}
function deleteCFS(batch,path,iWarehouseType)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'DELETE_BATCH',batch:batch,iWarehouseType:iWarehouseType},function(result){
        if(path !="javascript:void(0);")
        {
            window.location = path;
        }
        else
        {
            window.location.href = window.location.href;
        }
    });
}

function sel_haulage_model_postcode_data(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection,fWmFactor)
{

	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	var rowCount = $('#view_haulage_model_postcode tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_postcode_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
		var totalActive=$("#totalActive1").attr('value');
		if(parseInt(totalActive)!=parseInt(iPriorityLevel))
		{
			$("#priority_down_postcode").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_down_postcode").click(function(){change_haulage_model_postcode_priority(id,idHaulageModel,idWarehouse,idDirection,'down',fWmFactor,iPriorityLevel)});
			$("#priority_down_postcode").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_down_postcode").unbind("click");
			$("#priority_down_postcode").attr('style',"opacity:0.4");
		}
		if(parseInt(iPriorityLevel)!='1')
		{
			$("#priority_up_postcode").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_up_postcode").click(function(){change_haulage_model_postcode_priority(id,idHaulageModel,idWarehouse,idDirection,'up',fWmFactor,iPriorityLevel)});
			$("#priority_up_postcode").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_up_postcode").unbind("click");
			$("#priority_up_postcode").attr('style',"opacity:0.4");
		}
		
		$("#delete_postcode").unbind("click");
		$("#delete_postcode").click(function(){delete_haulage_postcode(id,idHaulageModel,idWarehouse,idDirection)});
		$("#delete_postcode").attr('style',"opacity:1");
		
		
		$("#edit_postcode").unbind("click");
		$("#edit_postcode").click(function(){edit_haulage_postcode(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)});
		$("#edit_postcode").attr('style',"opacity:1");
		
		var showflag="pricing_postcode_detail_list";
		var newdiv_id="postcode_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:fWmFactor},function(result){
		$("#"+newdiv_id).html(result);	
			//$("#save_postcode_detail_line").unbind("click");
			//$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			$('INPUT:text,SELECT', '#updatePricingPostCodeData').val(''); 
			$("#mode").attr('value','add');
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:'show_postcode_str'},function(result){
			$("#display_postcode_string").html(result);
			$("#idHaulagePricingModel").attr('value',id);
			var disp = $("#loader").css('display');
			if(disp=='block')
			{
				$("#loader").attr("style","display:none");
			}
			});	
			
		});
}

function change_haulage_model_postcode_priority(id,idHaulageModel,idWarehouse,idDirection,moveflag,fWmFactor,iPriorityLevel)
{
	$("#loader").attr("style","display:block");
	var showflag="move_haulage_postcode_model";
	var  divid="haulage_model_"+idHaulageModel;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,moveflag:moveflag,flag:showflag},function(result){
	$("#view_haulage_model_postcode_div").html(result);
	
		var rowCount = $('#view_haulage_model_postcode tr').length;
		if(moveflag=='down')
		{
			var t=parseInt(iPriorityLevel)+1;
			var div_id="haulage_postcode_"+t;
			//$("#haulage_zone_"+t).attr('style','background:#DBDBDB;color:#828282;');
		}
		
		if(moveflag=='up')
		{
			var t=parseInt(iPriorityLevel)-1;
			var div_id="haulage_postcode_"+t;
			//$("#haulage_zone_"+t).attr('style','background:#DBDBDB;color:#828282;');
		}
		if(rowCount>0)
			{
				for(k=1;k<rowCount;++k)
				{
					newdivid="haulage_postcode_"+k;
					if(newdivid!=div_id)
					{
						$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
					}
					else
					{
						$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
			}
		
			var totalActive=$("#totalActive1").attr('value');
			if(parseInt(totalActive)!=parseInt(t))
			{
				$("#priority_down_postcode").unbind("click");
				//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
				$("#priority_down_postcode").click(function(){change_haulage_model_postcode_priority(id,idHaulageModel,idWarehouse,idDirection,'down',t,fWmFactor)});
				$("#priority_down_postcode").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_down_postcode").unbind("click");
				$("#priority_down_postcode").attr('style',"opacity:0.4");
			}
			
			
			
			if(parseInt(t)!='1')
			{
				$("#priority_up_postcode").unbind("click");
				//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
				$("#priority_up_postcode").click(function(){change_haulage_model_postcode_priority(id,idHaulageModel,idWarehouse,idDirection,'up',t,fWmFactor)});
				$("#priority_up_postcode").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_up_postcode").unbind("click");
				$("#priority_up_postcode").attr('style',"opacity:0.4");
			}
		
			$("#loader").attr("style","display:none");
		//sel_haulage_model_postcode_data(div_id,id,idHaulageModel,t,idWarehouse,idDirection,fWmFactor);
	});
}


function delete_haulage_postcode_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="haulage_model_"+idHaulageModel;
	var showflag="delete_haulage_postcode_confirm";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		});
}


function delete_haulage_postcode(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="postcode_detail_delete_"+idHaulageModel;
	var showflag="delete_haulage_postcode";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr("style","display:block");
	});
}

function sel_haulage_postcode_detail_data(div_id,id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)
{
	var rowCount = $('#view_haulage_model_postcode_detail tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_pricing_postcode_data_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#delete_postcode_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_postcode_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_postcode_detail").attr('style',"opacity:1");
	
	
	$("#postcode_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#postcode_edit_detail").click(function(){edit_haulage_postcode_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	$("#postcode_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_postcode_detail_line").unbind("click");
	$("#cancel_postcode_detail_line").attr('style',"opacity:0.4");
	
	$("#save_postcode_detail_line").unbind("click");
	$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingPostCodeData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
}

function edit_haulage_postcode_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	var showflag="edit_haulage_postcode_line";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#postcode_edit_form").html(result);
		$("#mode").attr('value','edit');		
		$("#delete_postcode_detail").unbind("click");
		$("#delete_postcode_detail").attr('style',"opacity:0.4");
	
	
		$("#postcode_edit_detail").unbind("click");
		$("#postcode_edit_detail").attr('style',"opacity:0.4");
		
		$("#cancel_postcode_detail_line").unbind("click");
		$("#cancel_postcode_detail_line").attr('style',"opacity:1");
		$("#cancel_postcode_detail_line").click(function(){cancel_haulage_postcode_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
		
		$("#save_postcode_detail_line").attr("onclick",'');	
		$("#save_postcode_detail_line").unbind("click");
		$("#save_postcode_detail_line").attr('style',"opacity:1");
		$("#save_postcode_detail_line").click(function(){save_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	});
}

function cancel_haulage_postcode_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	$("#delete_postcode_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_postcode_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_postcode_detail").attr('style',"opacity:1");
	
	
	$("#postcode_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#postcode_edit_detail").click(function(){edit_haulage_postcode_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	$("#postcode_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_postcode_detail_line").unbind("click");
	$("#cancel_postcode_detail_line").attr('style',"opacity:0.4");
	
	$("#save_postcode_detail_line").unbind("click");
	//$("#save_zone_detail_line").attr('style',"opacity:0.4");
	$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingPostCodeData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
	var disp = $("#regError").css('display');
	if(disp=='block')
	{
		$("#regError").attr('style',"display:none");
	}
}


function save_haulage_postcode_line_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
		var showflag="pricing_postcode_detail_list";
		var newdiv_id="postcode_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+newdiv_id).html(result);
		$("#mode").attr('value','add');
		$("#idHaulagePricingModel").attr('value',id);
		$("#save_postcode_detail_line").unbind("click");
		$("#save_postcode_detail_line").attr("style","opacity:0.4");
		//$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
		$("#loader").attr('style','display:none;');
		});
}

function delete_haulage_postcode_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="haulage_model_"+idHaulageModel;
	var showflag="delete_haulage_postcode_confirm";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		});
}


function delete_haulage_postcode(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="postcode_detail_delete_"+idHaulageModel;
	var showflag="delete_haulage_postcode";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr("style","display:block");
	});
}

function add_postcode_popup(idHaulageModel,idDirection,idWarehouse)
{
	var  divid="postcode_detail_delete_"+idHaulageModel;
	var showflag="open_add_postcode_popup";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		$("#szName").focus();
		addPopupScrollClass(divid);
	});
}

function cancel_postcode(idHaulageModel)
{
	var  divid="postcode_detail_delete_"+idHaulageModel;
	$("#"+divid).attr('style',"display:none");
	removePopupScrollClass(divid);
}

function add_postcode_string(mode)
{
	var textvalue=$("#szPostCode").attr('value');
	var iChars = "!@#$%^&()+=[]\\\';,./{}|\":<>~_";
	for (var i = 0; i < textvalue.length; i++) {
  	if (iChars.indexOf(textvalue.charAt(i)) != -1) {
  	  	alert ("These special characters are not allowed (*?-).");
  		return false;
  		}
 	 }	 
	if(textvalue!='')
	{
		var result_ary = textvalue.split("*");
		if(result_ary.length>2)
		{
			alert ("Wrong format.");
			return false;
		}
		var result_ary_1 = textvalue.split("-");
		if(result_ary_1.length>2 || result_ary_1[0]=='' || result_ary_1[1]=='')
		{
			alert ("Wrong format.");
			return false;
		}
		var postStr=$("#szPostCodeStr1").attr('value');
		var postStrArr=postStr.split(",");
		var resp=jQuery.inArray(textvalue, postStrArr);
		if(resp!='' || resp=='0')
		{	
			if(resp!='-1')
			{	alert ("Already Added.");
				return false;
			}
		}
		var post_str=jQuery.trim(postStr);
		if(post_str!='')
		{
		
			$("#szPostCode").attr('readonly','readonly');
			var textvalue1=postStr+","+textvalue;
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{textvalue:textvalue1,flag:'add_postcode_string_sort',mode:mode},function(result){
			$("#list_postcode_arr").html(result);
			$("#szPostCodeStr1").attr('value',textvalue1);
			$("#szPostCode").attr('value','');
			$("#szPostCode").attr('readonly',false);
			$("#szPostCode").focus();
			enable_add_edit_button(mode);
			});
		}
		else
		{
			$("#szPostCode").attr('readonly','readonly');
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{textvalue:textvalue,postStr:postStr,flag:'add_postcode_string_sort',mode:mode},function(result){
			$("#list_postcode_arr").html(result);
			$("#szPostCodeStr1").attr('value',textvalue);
			$("#szPostCode").attr('value','');
			$("#szPostCode").attr('readonly',false);
			$("#szPostCode").focus();
			enable_add_edit_button(mode);
			});
		}
	}
}

function add_postcode_data()
{
	var value=$("#postcodeAddEditForm").serialize();
	var newvalue=value+"&flag=add_postcode";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_postcode_form").html(result);
	});
}
function delete_str_postcode(mode)
{
	var mode=$("#mode").attr('value');
	$("#delete_postcode_str").unbind("click");
	$("#delete_postcode_str").attr('style',"opacity:1;float:right;margin-top:70px;");
	$("#delete_postcode_str").click(function(){delete_postcode_string(mode)});
	//$("#delete_postcode_str").click(function(){enable_add_edit_button(mode)});
	
}
function delete_postcode_string(mode)
{
	var postStr=$("#szPostCodeStr1").attr('value');
	var szPostCodeStr = $("#szPostCodeStr").val();
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{textvalue:szPostCodeStr,postStr:postStr,flag:'delete_postcode_string_sort'},function(result){
	$("#list_postcode_arr").html(result);
	$("#delete_postcode_str").unbind("click");
	$("#delete_postcode_str").attr('style',"opacity:0.4;margin-top:70px;float:right;");
	enable_add_edit_button(mode);
	});
}

function save_postcode_data()
{
	var value=$("#postcodeAddEditForm").serialize();
	var newvalue=value+"&flag=update_postcode";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_postcode_form").html(result);
	});
}

function edit_haulage_postcode(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)
{
	var  divid="postcode_detail_delete_"+idHaulageModel;
	var showflag="edit_postcode";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'edit',iPriorityLevel:iPriorityLevel},function(result){
		$("#"+divid).html(result);
		addPopupScrollClass(divid);
		$("#"+divid).attr('style',"display:block;");
		$("#save_postcode").attr("onclick","");
		$("#save_postcode").unbind("click");
		$("#save_postcode").attr('style',"opacity:1");
		$("#save_postcode").click(function(){save_postcode_data()});
		$("#change_text").html('Save Set');
		$("#szName").focus();
	});
}

function sel_haulage_model_city_data(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection,fWmFactor)
{
	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	var rowCount = $('#view_haulage_model_city tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_city_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
		var totalActive=$("#totalActive1").attr('value');
		if(parseInt(totalActive)!=parseInt(iPriorityLevel))
		{
			$("#priority_down_city").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_down_city").click(function(){change_haulage_model_city_priority(id,idHaulageModel,idWarehouse,idDirection,'down',iPriorityLevel,fWmFactor)});
			$("#priority_down_city").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_down_city").unbind("click");
			$("#priority_down_city").attr('style',"opacity:0.4");
		}
		if(parseInt(iPriorityLevel)!='1')
		{
			$("#priority_up_city").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#priority_up_city").click(function(){change_haulage_model_city_priority(id,idHaulageModel,idWarehouse,idDirection,'up',iPriorityLevel,fWmFactor)});
			$("#priority_up_city").attr('style',"opacity:1");
		}
		else
		{
			$("#priority_up_city").unbind("click");
			$("#priority_up_city").attr('style',"opacity:0.4");
		}
		
		$("#delete_city").unbind("click");
		$("#delete_city").click(function(){delete_haulage_city(id,idHaulageModel,idWarehouse,idDirection)});
		$("#delete_city").attr('style',"opacity:1");
		
		
		$("#edit_city").unbind("click");
		$("#edit_city").click(function(){edit_haulage_city(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)});
		$("#edit_city").attr('style',"opacity:1");
		
		var showflag="pricing_city_detail_list";
		var newdiv_id="city_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:fWmFactor},function(result){
		$("#"+newdiv_id).html(result);	
			//$("#save_city_detail_line").unbind("click");
			//$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			$('INPUT:text,SELECT', '#updatePricingCityData').val(''); 
			$("#mode").attr('value','add');
			$("#idHaulagePricingModel").attr('value',id);
			var disp = $("#loader").css('display');
			if(disp=='block')
			{
				$("#loader").attr("style","display:none");
			}
		});
}

function change_haulage_model_city_priority(id,idHaulageModel,idWarehouse,idDirection,moveflag,iPriorityLevel,fWmFactor)
{
	$("#loader").attr("style","display:block");
	var showflag="move_haulage_city_model";
	var  divid="haulage_model_"+idHaulageModel;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,moveflag:moveflag,flag:showflag},function(result){
	$("#view_haulage_model_city_div").html(result);
	
		var rowCount = $('#view_haulage_model_city tr').length;
		if(moveflag=='down')
		{
			var t=parseInt(iPriorityLevel)+1;
			var div_id="haulage_city_"+t;
			//$("#haulage_zone_"+t).attr('style','background:#DBDBDB;color:#828282;');
		}
		
		if(moveflag=='up')
		{
			var t=parseInt(iPriorityLevel)-1;
			var div_id="haulage_city_"+t;
			//$("#haulage_zone_"+t).attr('style','background:#DBDBDB;color:#828282;');
		}
		
			if(rowCount>0)
			{
				for(k=1;k<rowCount;++k)
				{
					newdivid="haulage_city_"+k;
					if(newdivid!=div_id)
					{
						$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
					}
					else
					{
						$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
			}
		
			var totalActive=$("#totalActive1").attr('value');
			if(parseInt(totalActive)!=parseInt(t))
			{
				$("#priority_down_city").unbind("click");
				//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
				$("#priority_down_city").click(function(){change_haulage_model_city_priority(id,idHaulageModel,idWarehouse,idDirection,'down',t,fWmFactor)});
				$("#priority_down_city").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_down_city").unbind("click");
				$("#priority_down_city").attr('style',"opacity:0.4");
			}
			
			
			
			if(parseInt(t)!='1')
			{
				$("#priority_up_city").unbind("click");
				//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
				$("#priority_up_city").click(function(){change_haulage_model_city_priority(id,idHaulageModel,idWarehouse,idDirection,'up',t,fWmFactor)});
				$("#priority_up_city").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_up_city").unbind("click");
				$("#priority_up_city").attr('style',"opacity:0.4");
			}
		
		$("#loader").attr("style","display:none");
		//sel_haulage_model_city_data(div_id,id,idHaulageModel,t,idWarehouse,idDirection,fWmFactor);
	});
}

function save_haulage_city_line_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
		var showflag="pricing_city_detail_list";
		var newdiv_id="city_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+newdiv_id).html(result);
		$("#mode").attr('value','add');
		$("#idHaulagePricingModel").attr('value',id);
		$("#save_city_detail_line").unbind("click");
		$("#save_city_detail_line").attr('style','opacity:0.4');
		//$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
		$("#loader").attr('style','display:none;');
		});
}

function sel_haulage_city_detail_data(div_id,id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel,fWmFactor)
{
	var rowCount = $('#view_haulage_model_city_detail tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_pricing_city_data_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#delete_city_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_city_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel,fWmFactor)});
	$("#delete_city_detail").attr('style',"opacity:1");
	
	
	$("#city_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#city_edit_detail").click(function(){edit_haulage_city_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg,fWmFactor)});
	$("#city_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_city_detail_line").unbind("click");
	$("#cancel_city_detail_line").attr('style',"opacity:0.4");
	
	$("#save_city_detail_line").unbind("click");
	$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingCityData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
}


function edit_haulage_city_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	var showflag="edit_haulage_city_line";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#city_edit_form").html(result);
		$("#mode").attr('value','edit');		
		$("#delete_city_detail").unbind("click");
		$("#delete_city_detail").attr('style',"opacity:0.4");
	
	
		$("#city_edit_detail").unbind("click");
		$("#city_edit_detail").attr('style',"opacity:0.4")
		
		$("#cancel_city_detail_line").unbind("click");
		$("#cancel_city_detail_line").attr('style',"opacity:1");
		$("#cancel_city_detail_line").click(function(){cancel_haulage_city_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
		
		$("#save_city_detail_line").attr("onclick",'');	
		$("#save_city_detail_line").unbind("click");
		$("#save_city_detail_line").attr('style',"opacity:1");
		$("#save_city_detail_line").click(function(){save_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	});
}

function cancel_haulage_city_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	$("#delete_city_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_city_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_city_detail").attr('style',"opacity:1");
	
	
	$("#city_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#city_edit_detail").click(function(){edit_haulage_city_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	$("#city_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_city_detail_line").unbind("click");
	$("#cancel_city_detail_line").attr('style',"opacity:0.4");
	
	$("#save_city_detail_line").unbind("click");
	//$("#save_zone_detail_line").attr('style',"opacity:0.4");
	$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingCityData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
	var disp = $("#regError").css('display');
	if(disp=='block')
	{
		$("#regError").attr('style',"display:none");
	}
}


function delete_haulage_city_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="haulage_model_"+idHaulageModel;
	var showflag="delete_haulage_city_confirm";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		});
}


function delete_haulage_city(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="city_detail_delete_"+idHaulageModel;
	var showflag="delete_haulage_city";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr("style","display:block");
		addPopupScrollClass(divid);
	});
}
function submitCFSlisting()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",$('#addEditWarehouse').serialize(),function(result){
		result_ary = result.split("|||||");
		if($.trim(result_ary[0])=='Error')
		{
			showHidePopUp();
			$('#Error-log').css('display','block');
			$('#Error-log').html(result_ary[1]);
			check = true;
		}
		else
		{
			$('#Error-log').html('');
			window.location.href = window.location.href;
			
		}
		});	
}
function addCfsCountries(id)
{
	var selectedCountries = $('#CFSListing').val();
	var selectedFormCountries =	$('#'+id).val();
	 $('#'+id+' option:selected').remove();
	 $('#'+id).val($('#'+id+' option:first').val());
	var toBeSelected = selectedCountries+','+ selectedFormCountries;
	$('#CFSListing').attr('value',toBeSelected);
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SELECT_COUNTRIES_FORM_LIST',content:toBeSelected},function(result){
		$('#selectedCountries').html(result);
		});
}
function removeCfsCountries(id,country,latitude,longitude)
{
	$('#'+id+' option:selected').remove();
	var options = document.getElementById(id).options;
	var values = [];
	var i = 0, len = options.length;
	while (i < len)
	{
	  values.push(options[i++].value);
	}
	selectedCountriesToRemove = values.join(', ');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'REMOVE_COUNTRIES_FROM_LIST',content:selectedCountriesToRemove,country:country,latitude:latitude,longitude:longitude},function(result){
		$('#'+id+' option:selected').remove();
		$('#CFSListing').attr('value',selectedCountriesToRemove);
		$('#addFormList').html(result);
		});
}
function showCountriesByCountry(idCountry)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SHOW_MULTIPLE_COUNTRIES_SELECT',idCountry:idCountry},function(result){
	$('#bottom-form').html(result);
	$('#CFSListing').attr('value',idCountry);	
		});
}
function submit_warehouse_data_obo()
{
	var CfsListing = $('#CFSListing').val();
	
	if(CfsListing == '')
	{ 
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SHOW_POP_UP'},function(result){
		$('#ajaxLogin').html(result);
		$('#ajaxLogin').css('display','block');
		return false;
		});
	}
	else
	{
		var value=decodeURIComponent($('#addEditWarehouse').serialize());
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",$('#addEditWarehouse').serialize(),function(result){
		if(result =='')
		{
			window.location.href = window.location.href;
		}
		result_ary = result.split("|||||");
		if($.trim(result_ary[0])=='Error')
		{
			$('#Error-log').css('display','block');
			$('#Error-log').html(result_ary[1]);
			check = true;
		}
		else
		{
			$('#Error-log').html('');
			window.location.href = window.location.href;
			
		}
		});	
	}
}	
function update_warehouse_data_obo()
{
	var CfsListing = $('#CFSListing').val();
	
	if(CfsListing == '')
	{ 
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SHOW_POP_UP'},function(result){
		$('#ajaxLogin').html(result);
		$('#ajaxLogin').css('display','block');
		addPopupScrollClass('ajaxLogin');
		
		//return false;
		});
	}
	else
	{
		var value=decodeURIComponent($('#addEditWarehouse').serialize());
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",value,function(result){
		result_ary = result.split("|||||");
               
		if($.trim(result_ary[0])=='Error')
		{
			$('#Error-log').css('display','block');
			$('#Error-log').html(result_ary[1]);
		}
		else
		{
			$('#Error-log').html('');
                        if($("#iScrollValue").length>0)
                        {
                            $('.hsbody-2-right').html(result);
                        }
                        else{
                            window.location.href = window.location.href;
                        }
		}
		});	
	}
}

function add_city_popup(idHaulageModel,idDirection,idWarehouse)
{
	var  divid="city_detail_delete_"+idHaulageModel;
	var showflag="open_add_city_popup";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		$("#szName").focus();
		addPopupScrollClass(divid);
	});
}

function cancel_city(idHaulageModel)
{
	var  divid="city_detail_delete_"+idHaulageModel;
	$("#"+divid).attr('style',"display:none");
	removePopupScrollClass(divid);
}

function show_city(szLatitude,szLongitude,mazRadius,mode)
{
	var radius=$("#iDistance").attr('value');
	if(parseInt(mazRadius)<parseInt(radius))
	{
		alert("Radius should not be greater than "+mazRadius);
		return false;
	}
	if(mode=='add')
	{
		var flagsend="ADD";
	}
	var transittime=$("#idTransitTime").val();
	if(transittime!='' && parseInt(radius)>0)
	{
		$("#save_city").unbind("click");
  		$("#save_city").attr('style',"opacity:1.0");
  		if(mode=='add')
  		{
  			$("#save_city").click(function(){add_city_data()});
  		}
  		else if(mode=='edit')
  		{
  			$("#save_city").click(function(){save_city_data()});
  		}
  	}
  	else
  	{
  		$("#save_city").unbind("click");
  		$("#save_city").attr('style',"opacity:0.4");
  	}
	var szCity=$("#szName").attr('value');
	var iframe = window.parent.document.getElementById('draw_circle_iframe');
	iframe.src=__JS_ONLY_SITE_BASE__+'/drawCircle.php?szLatitude='+szLatitude+'&szLongitude='+szLongitude+'&radius='+radius+'&szCity='+szCity+'&flag='+flagsend;
	
}

function add_city_data()
{
	var value=$("#cityAddEditForm").serialize();
	var newvalue=value+"&flag=add_city";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_city_form_new").html(result);
	});
}


function edit_haulage_city(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)
{
	var  divid="city_detail_delete_"+idHaulageModel;
	var showflag="edit_city";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'edit',iPriorityLevel:iPriorityLevel},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		//$("#save_city").attr("onclick","");
		$("#save_city").unbind("click");
		$("#save_city").click(function(){save_city_data()});
		$("#change_name").html('Save City');
		$("#save_city").attr('style',"opacity:1.0");
		$("#szName").focus();
		addPopupScrollClass(divid);
	});
}

function save_city_data()
{
	var value=$("#cityAddEditForm").serialize();
	var newvalue=value+"&flag=update_city";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_city_form_new").html(result);
	});
}

function clear_city_circle_map(szLatitude,szLongitude,radius,flag)
{
	var iframe = window.parent.document.getElementById('draw_circle_iframe');
	iframe.src=__JS_ONLY_SITE_BASE__+'/drawCircle.php?szLatitude='+szLatitude+'&szLongitude='+szLongitude+'&radius='+radius+'&flag='+flag;
	
	if(radius==undefined || radius=='')
	{
		$("#szName").attr('value','');
		$("#iDistance").attr('value','');
	}
}


function open_warehouse_city_circle_poup(idWarehouse,idDirection,idHaulageModel)
{
	var  divid="city_detail_delete_"+idHaulageModel;
	var showflag="show_warehouse_city_circle";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		addPopupScrollClass(divid);
	});
}

function sel_haulage_model_distance_data(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection)
{
	var rowCount = $('#view_haulage_model_distance tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_distance_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
		var totalActive=$("#totalActive1").attr('value');
		
		
		$("#delete_distance").unbind("click");
		$("#delete_distance").click(function(){delete_haulage_distance(id,idHaulageModel,idWarehouse,idDirection)});
		$("#delete_distance").attr('style',"opacity:1");
		
		
		$("#edit_distance").unbind("click");
		$("#edit_distance").click(function(){edit_haulage_distance(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)});
		$("#edit_distance").attr('style',"opacity:1");
		
		var showflag="pricing_distance_detail_list";
		var newdiv_id="distance_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+newdiv_id).html(result);	
			//$("#save_distance_detail_line").unbind("click");
			//$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			$('INPUT:text,SELECT', '#updatePricingdistanceData').val(''); 
			$("#mode").attr('value','add');
			
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:'show_distance_str'},function(result){
			$("#display_distance_string").html(result);
			$("#idHaulagePricingModel").attr('value',id);
			});	
			
		});
}

function delete_haulage_distance_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="haulage_model_"+idHaulageModel;
	var showflag="delete_haulage_distance_confirm";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		});
}


function delete_haulage_distance(id,idHaulageModel,idWarehouse,idDirection)
{
	var  divid="distance_detail_delete_"+idHaulageModel;
	var showflag="delete_haulage_distance";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr("style","display:block");
	});
}

function sel_haulage_distance_detail_data(div_id,id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)
{
	var rowCount = $('#view_haulage_model_distance_detail tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_pricing_distance_data_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#delete_distance_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_distance_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_distance_detail").attr('style',"opacity:1");
	
	
	$("#distance_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#distance_edit_detail").click(function(){edit_haulage_distance_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	$("#distance_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_distance_detail_line").unbind("click");
	$("#cancel_distance_detail_line").attr('style',"opacity:0.4");
	
	$("#save_distance_detail_line").unbind("click");
	$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingDistanceData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
}

function edit_haulage_distance_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	var showflag="edit_haulage_distance_line";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#distance_edit_form").html(result);
		$("#mode").attr('value','edit');		
		$("#delete_distance_detail").unbind("click");
		$("#delete_distance_detail").attr('style',"opacity:0.4");
	
	
		$("#distance_edit_detail").unbind("click");
		$("#distance_edit_detail").attr('style',"opacity:0.4")
		
		$("#cancel_distance_detail_line").unbind("click");
		$("#cancel_distance_detail_line").attr('style',"opacity:1");
		$("#cancel_distance_detail_line").click(function(){cancel_haulage_distance_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
		
		$("#save_distance_detail_line").attr("onclick",'');	
		$("#save_distance_detail_line").unbind("click");
		$("#save_distance_detail_line").attr('style',"opacity:1");
		$("#save_distance_detail_line").click(function(){save_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	});
}

function cancel_haulage_distance_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	$("#delete_distance_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_distance_detail").click(function(){delete_haulage_pricing_line(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_distance_detail").attr('style',"opacity:1");
	
	
	$("#distance_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#distance_edit_detail").click(function(){edit_haulage_distance_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	$("#distance_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_distance_detail_line").unbind("click");
	$("#cancel_distance_detail_line").attr('style',"opacity:0.4");
	
	$("#save_distance_detail_line").unbind("click");
	//$("#save_zone_detail_line").attr('style',"opacity:0.4");
	$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingDistanceData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
	var disp = $("#regError").css('display');
	if(disp=='block')
	{
		$("#regError").attr('style',"display:none");
	}
}


function save_haulage_distance_line_confirm(id,idHaulageModel,idWarehouse,idDirection)
{
		var showflag="pricing_distance_detail_list";
		var newdiv_id="distance_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+newdiv_id).html(result);
		$("#mode").attr('value','add');
		$("#idHaulagePricingModel").attr('value',id);
		$("#save_distance_detail_line").unbind("click");
		$("#save_distance_detail_line").attr("style","opacity:0.4");
		//$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
		$("#loader").attr('style','display:none;');
		});
}

function check_applicable_charges_checkbox(mode)
{
	if(mode=='ORIGIN')
	{
		var rate = $("#fOriginRateWM").attr('value');
		var min_rate = $("#fOriginMinRateWM").attr('value');
		var book_rate = $("#fOriginRate").attr('value');
		
		
		if(jQuery.trim(rate)=='' && jQuery.trim(min_rate)=='' && jQuery.trim(book_rate)=='')
		{
			$("#szOriginFreightCurrency").attr('value','');
		}
		var currency = $("#szOriginFreightCurrency").attr('value');
		if(jQuery.trim(rate)!='')
		{
			$("#iOriginChargesApplicable").attr('value','1');
		}
		else if(jQuery.trim(min_rate)!='')
		{
			$("#iOriginChargesApplicable").attr('value','1');
		}
		else if(jQuery.trim(book_rate)!='')
		{
			$("#iOriginChargesApplicable").attr('value','1');
		}
		else if(jQuery.trim(currency)!='')
		{
			$("#iOriginChargesApplicable").attr('value','1');
		}
		else
		{
			$("#iOriginChargesApplicable").attr('value','0');
		}
	}
	else if(mode=='DESTINATION')
	{
		var rate = $("#fDestinationRateWM").attr('value');
		var min_rate = $("#fDestinationMinRateWM").attr('value');
		var book_rate = $("#fDestinationRate").attr('value');
		
		
		if(jQuery.trim(rate)=='' && jQuery.trim(min_rate)=='' && jQuery.trim(book_rate)=='')
		{
			$("#szDestinationFreightCurrency").attr('value','');
		}
		var currency = $("#szDestinationFreightCurrency").attr('value');
		if(jQuery.trim(rate)!='')
		{
			$("#iDestinationChargesApplicable").attr('value','1');
		}
		else if(jQuery.trim(min_rate)!='')
		{
			$("#iDestinationChargesApplicable").attr('value','1');
		}
		else if(jQuery.trim(book_rate)!='')
		{
			$("#iDestinationChargesApplicable").attr('value','1');
		}
		else if(jQuery.trim(currency)!='')
		{
			$("#iDestinationChargesApplicable").attr('value','1');
		}
		else
		{
			$("#iDestinationChargesApplicable").attr('value','0');
		}
	}
}

function edit_haulage_distance(id,idHaulageModel,idWarehouse,idDirection,iPriorityLevel)
{
	var  divid="distance_detail_delete_"+idHaulageModel;
	var showflag="edit_distance";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'edit',iPriorityLevel:iPriorityLevel},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		$("#save_distance").attr("onclick","");
		$("#save_distance").unbind("click");
		$("#save_distance").click(function(){save_distance_data()});
		$("#change_text").html('Save Line');
		var totalActive=$("#mode").attr('value');
		$("#iDistanceKm").focus();
		addPopupScrollClass(divid);
	});
}

function add_distance_popup(idHaulageModel,idDirection,idWarehouse)
{
	var  divid="distance_detail_delete_"+idHaulageModel;
	var showflag="open_add_distance_popup";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		$("#iDistanceKm").focus();
		addPopupScrollClass(divid);
	});
}

function cancel_distance(idHaulageModel)
{
	var  divid="distance_detail_delete_"+idHaulageModel;
	$("#"+divid).attr('style',"display:none");
	removePopupScrollClass(divid);
}

function add_countries(mode)
{
	var szCountriesStr=$("#szCountriesStr").attr('value');
	var szCountry = $("#szCountry").val();
	if(szCountry=='')
	{
		alert('Select the country from the given dropdown.');
		return false;
	}
	if(szCountriesStr!='')
	{
		var newvalue=szCountriesStr+','+szCountry;
	}
	else
	{
		var newvalue=szCountry;
	}
	$("#szCountry").attr("disabled",true);
	var idWarehouse=$("#haulageWarehouse").attr('value');
	$("#add_country_str").unbind("click");
	$("#add_country_str").attr('style',"opacity:0.4;float:right;");
	var idDirection = $("#idDirection").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{countriesArr:newvalue,flag:'add_counrty_string_sort',mode:mode},function(result){
	$("#list_countries_arr").html(result);
	$("#szCountriesStr").attr('value',newvalue);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{countriesArr:newvalue,idDirection:idDirection,flag:'remove_added_countries_from_dropdown',idWarehouse:idWarehouse},function(result){
		$("#list_countries_arr_remove").html(result);
		enable_add_edit_button_distance(mode);
			$("#add_country_str").unbind("click");
			$("#add_country_str").attr('style',"opacity:1;float:right;");
			$("#add_country_str").click(function(){add_countries(mode)});
			//$("#add_country_str").click(function(){enable_add_edit_button_distance(mode)});
			$("#szCountry").attr("disabled",false);
		});
		
	});
}

function delete_str_countries(mode)
{
	var mode=$("#mode").attr('value');
	$("#delete_country_str").unbind("click");
	$("#delete_country_str").attr('style',"float:right;opacity:1;margin-top:70px;");
	$("#delete_country_str").click(function(){delete_countries_string(mode)});
	//$("#delete_country_str").click(function(){enable_add_edit_button_distance(mode)});
	
}
function delete_countries_string(mode)
{
	var szCountriesStr=$("#szCountriesStr").attr('value');
	var szCourtriesNameStr = $("#szCourtriesNameStr").val();
	var szCountriesNewArr=[];
	var szCountriesArr=szCountriesStr.split(",");	
	
	
	if(szCourtriesNameStr.length>0)
	{
		var k=0;
		for(var i=0; i<szCourtriesNameStr.length;++i )
		{
			szCountriesArr.splice(szCountriesArr.indexOf(szCourtriesNameStr[i]), 1);
		}
		
	}
	var idWarehouse=$("#haulageWarehouse").attr('value');
	var newvalue= szCountriesArr.join(',');
	var idDirection = $("#idDirection").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{countriesArr:newvalue,flag:'add_counrty_string_sort',mode:mode},function(result){
	$("#list_countries_arr").html(result);
	$("#szCountriesStr").attr('value',newvalue);
	$("#delete_country_str").unbind("click");
	$("#delete_country_str").attr('style',"float:right;opacity:0.4;margin-top:70px;");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{countriesArr:newvalue,idDirection:idDirection,flag:'remove_added_countries_from_dropdown',idWarehouse:idWarehouse,mode:mode},function(result){
		enable_add_edit_button_distance(mode);
		$("#list_countries_arr_remove").html(result);
		
		});
	});
}

function add_distance_data()
{
	var value=$("#distanceAddEditForm").serialize();
	var newvalue=value+"&flag=add_distance";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_distance_form").html(result);
	});
}

function save_distance_data()
{
	var value=$("#distanceAddEditForm").serialize();
	var newvalue=value+"&flag=update_distance";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
	$("#update_distance_form").html(result);
	});
}

function copy_button_distance_countries(mode)
{
	var iCopyDistanceCountries = $("#iCopyDistanceCountries").val();
	if(iCopyDistanceCountries!='')
	{
		$("#copy_country_str").unbind("click");
		$("#copy_country_str").attr('style',"opacity:1;");
		$("#copy_country_str").click(function(){copy_distance_countries(mode)});
	}
	else
	{
		$("#copy_country_str").unbind("click");
		$("#copy_country_str").attr('style',"opacity:0.4");
	}
}

function copy_distance_countries(mode)
{
	var iCopyDistanceCountries = $("#iCopyDistanceCountries").val();
	var idDirection = $("#idDirection").attr('value');
	var idWarehouse=$("#haulageWarehouse").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{iCopyDistanceCountries:iCopyDistanceCountries,flag:'copy_counrty_string_sort',mode:mode},function(result){
		$("#list_countries_arr").html(result);
		$("#copy_country_str").unbind("click");
		$("#copy_country_str").attr('style',"opacity:0.4");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{iCopyDistanceCountries:iCopyDistanceCountries,flag:'remove_copy_countries_from_dropdown',idDirection:idDirection,idWarehouse:idWarehouse},function(result){
		$("#list_countries_arr_remove").html(result);
		enable_add_edit_button_distance(mode);
		});
	});
}

function open_copy_haulage_distance_popup(idHaulageModel,idWarehouse,idDirection)
{
	var  divid="distance_detail_delete_"+idHaulageModel;
	var showflag="open_copy_distance_popup";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		$("#"+divid).attr('style',"display:block");
		addPopupScrollClass(divid);
	});
}

function copy_distance()
{
	idCountry=$('#haulageCountryZone').val();
	szCity=$('#szCityZone').val();
	idWarehouse=$('#haulageWarehouseZone').val();
	idDirection=$('#idDirectionZone').val();
	var showflag='copy_distance_data';
	var value=$("#copyDistance").serialize();
	var newvalue=value+"&idCountry="+idCountry+"&szCity="+szCity+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&flag="+showflag;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
		$("#show_copy_data").html(result);
		$("#show_copy_data").attr("style","display:block");
		});
}
function obo_haulage_tryit_out()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_OBOPricingHaulageTryitout.php",$("#obo_haulage_tryit_out_form").serialize(),function(result){
		$("#tryitout_result_div").html(result);
		result_ary = result.split('||||');
		
		if(jQuery.trim(result_ary[0])=='ERROR')
        {
         	$("#tryitout_result_div_error").css('display','block'); 
         	$("#tryitout_result_div").css('display','none');   
         	$("#tryitout_result_div_error").html(result_ary[1]);
        }
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
        	$("#tryitout_result_div_error").css('display','none');       	
        	$("#tryitout_result_div").attr("style","display:block");
			$("#tryitout_result_div").html(result_ary[1]);
		}
	});
}

function open_google_help_popup()
{	
    var szPostCode = $("#szPostCode").attr('value');	
    var szLatitude = $("#szLatitude").attr('value');	
    var szLongitude = $("#szLongitude").attr('value');	
    var szWhsLatitude = $("#szWhsLatitude").attr('value');	
    var szWhsLongitude = $("#szWhsLongitude").attr('value');
    var idDirection = $("#idDirection").attr('value');
    var idWarehouse  = $("#idWarehouse").attr('value');
    var szLocation = $("#szLocation").attr('value');

    $("#szPostCode_hidden").attr('value',szPostCode);	
    $("#szLatitude_hidden").attr('value',szLatitude);
    $("#szLongitude_hidden").attr('value',szLongitude);	
    $("#szWhsLatitude_hidden").attr('value',szWhsLatitude);
    $("#szWhsLongitude_hidden").attr('value',szWhsLongitude);	
    $("#idDirection_hidden").attr('value',idDirection);	
    $("#idWarehouse_hidden").attr('value',idWarehouse);	
    $("#szLocation_hidden").attr('value',szLocation);	

    $('#obo_haulage_tryit_out_hidden_form').submit();	
    $("#popup_container_google_map").attr('style','display:block;');
    addPopupScrollClass('popup_container_google_map'); 
}
function insert_value_google_map_haulage(latitute,logitude,iForwarderSectionFlag)
{	
    console.log("Lat: "+latitute+" Long: "+logitude);
    window.top.window.document.getElementById('szLatitude').value = latitute;
    window.top.window.document.getElementById('szLongitude').value = logitude; 
    
    if(iForwarderSectionFlag==1)
    {
        window.top.window.document.getElementById('szLocation').value = 'Selected from map';
    }
    else
    {
        if(latitute!="" && logitude !="")
        {
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_OBOPricingHaulageTryitout.php",{szLatitude:latitute,szLongitude:logitude,mode:'COUNTRY_NAME'},function(result){

                var result_ary = result.split("||||");
                if(result_ary[0]=='SUCCESS')
                {
                    $('#szCountry option[value="'+result_ary[1]+'"]').attr("selected", "selected");
                }
                else
                {
                    alert("Please select contry.");
                }
                window.top.window.document.getElementById('szPostCodeCity').value = 'From Map';
            }); 
        }   
    } 
    enable_add_edit_button_try_test();
    hide_google_map();
}
function onSelected(id)
{
	if(id == 'show')
	{
		$('#example').css('display','block');
	}
	else if(id == 'hide')
	{
		$('#example').css('display','none');
	}
	
}


function uploadBulkService()
{
	$("#loader").attr('style','display:block;');
        $("#error_msg").attr('style','display:none;');
        var value=$("#formUp").serialize();
        var valuenew = value+"&flag=submit_form";
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkUploadServices.php",valuenew,function(result){
            var result_ary = result.split("||||");
            if(result_ary[0]=='SUCCESS')
            {
                addPopupScrollClass('success-msg');
            }
            else
            {
                 $("#error_msg").attr('style','display:block;');
                  $("#error_msg").html(result_ary[1]);
            }
            $("#loader").attr('style','display:none;');
	});
	/*document.formUp.action = __JS_ONLY_SITE_BASE__+"/BulkUploadServices/";
	document.formUp.method = "post"
	document.formUp.submit();*/
}

function show_select_data_format_detial(uploadServiceType)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkUploadServices.php",{uploadServiceType:uploadServiceType,flag:'show_format'},function(result){
		$("#show_file_upload_data_format").html(result);
	});
}

function blank_me_postcode(id,val,val2)
{	
	var inputFeildValue = $("#"+id).attr("value");
	if(inputFeildValue == val || (inputFeildValue == val2 && val2!='') )
	{
		$("#"+id).attr("value","");
		$("#"+id).attr("style","width:135px;"); 
	}
}

function show_me_postcode(id,val)
{
	var inputFeildValue = $("#"+id).attr("value");
	
	if(inputFeildValue=='')
	{
		$("#"+id).attr("style","color:grey;font-style:italic;width:135px;");
		$("#"+id).attr("value",val);
	}
}
function open_available_service_popup(idOriginCountry,idDestinationCountry,szBookingRandomNum)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_showAllAvailableService.php",{idOriginCountry:idOriginCountry,idDestinationCountry:idDestinationCountry,szBookingRandomNum:szBookingRandomNum},function(result){
		$("#all_available_service").html(result);
		$('#all_available_service').attr('style','display:block;');
		$("#boxscroll2").niceScroll({touchbehavior:false,cursorcolor:"#17375E",cursoropacitymax:0.7,cursorwidth:7,autohidemode:false}).show();
	});
}
function closePopup(div_id,szBookingRandomNum)
{	
	$('#'+div_id).attr('style','display:none;');
	if ($("#"+div_id).length)
	{
		$("#boxscroll2").niceScroll().hide();
	}	
	removePopupScrollClass(div_id);
	if(div_id=='all_available_service')
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_showAllAvailableService.php",{szBookingRandomNum:szBookingRandomNum,mode:'DELETE_SERVICE_TEMP_DATA'},function(result){
		});
	}
}

function closeScrollablePopup(div_id)
{
	
	$('#'+div_id).attr('style','display:none;');
	$("#boxscroll2").niceScroll().hide();
	removePopupScrollClass(div_id);
}
/*
function find_transportation(mode,szBookingRandomNum)
{
	if(szBookingRandomNum == '')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_showAllAvailableService.php",{idOriginCountry:idOriginCountry,idDestinationCountry:idDestinationCountry},function(result){
		$("#all_available_service").html(result);
		$('#all_available_service').attr('style','display:block;');
	});
}
*/
function find_transportation(mode,szBookingRandomNum)
{
	if(szBookingRandomNum == '' || szBookingRandomNum=='undefined')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_requirement.php",{mode:mode,szBookingRandomNum:szBookingRandomNum},function(result){
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#transportation-box").html(result_ary[1]);
			$('#transportation-box').attr('style','display:block;');
			$('#all_available_service').attr('style','display:none;');
		}
		else if(result_ary[0]=='POPUP')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
		}
		else if(result_ary[0]=='ERROR')
		{
			alert("This booking does not belongs to you ");
		}		
	});
}

function display_abandon_popup(page_url,login_flag)
{
	if(szBookingRandomNum == '' || szBookingRandomNum=='undefined')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_requirement.php",{mode:'DISPLAY_ABANDON_POPUP',page_url:page_url,login_flag:login_flag,szBookingRandomNum:szBookingRandomNum},function(result){
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#leave_page_div").html(result_ary[1]);
			$('#leave_page_div').attr('style','display:block;');
			$('#all_available_service').attr('style','display:none;');
		}
	});
}

function update_expacted_services_requirement(page_url)
{
	var iLoggedInUser=$("#iLoggedInUser").attr('value');
	var szCustomerPhone='';
	if(iLoggedInUser==1)
	{
		var szCustomerPhone=$("#szCustomerPhone_popup").attr('value');
		var szCustomerPhone=encode_string_msg(szCustomerPhone);
		var value=$("#no_services_form").serialize();
		
		var new_value=value+"&szCustomerPhone="+szCustomerPhone;
	}
	else
	{
		var new_value=$("#no_services_form").serialize();
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_requirement.php",new_value,function(result){		     
       
        result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			 $("#regError").attr('style','display:none;');
			 $("#all_available_service").html(result_ary[1]);
			 $('#all_available_service').attr('style','display:block;');
		}
		else if(jQuery.trim(result_ary[0])=='ERROR')
		{
			$("#notify_me_button").removeAttr("onclick");
			$("#notify_me_button").unbind("click");
			$('#notify_me_button').attr('class','gray-button1');
			
			var szCustomerName = $('#szCustomerName_popup').attr('value');
			var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
			
			if(!isValidEmail(szCustomerEmail))
			{
				$("#szCustomerEmail_popup_error").html("!");
				$('#szCustomerEmail_popup_error').css('display','inline-block');
			}			
			if(szCustomerName=='')
			{
				$("#szCustomerName_popup_error").html("!");
				$('#szCustomerName_popup_error').css('display','inline-block');
			}
		}
    });
}

function Valid_Form_Feild_Check(val,mode,szBookingRandomNum,idUser,called_from,page_url,page_url2)
{
	if(szBookingRandomNum == '' || szBookingRandomNum=='undefined')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_requirement.php",{val:val,mode:mode,szBookingRandomNum:szBookingRandomNum},function(result){
		result_ary = result.split("||||");
	
		$("#notify_me_button").unbind("click");
		$('#notify_me_button').attr('class','gray-button1');
		//alert(result_ary[0]);
		if(result_ary[0]=='SUCCESS')
		{
			if(called_from=='SEARCH_SERVICE')
			{
				enable_notify_me_popup_search_service(idUser,page_url,window.event,szBookingRandomNum,page_url2,1);
			}
			else if(called_from=='REQUIREMENT_PAGE')
			{
				//update_expacted_services_requirement(page_url);
				enable_notify_me_popup_search_service(idUser,page_url,window.event,szBookingRandomNum,page_url2,1,'REQUIREMENT');
				//enable_notify_me_popup(idUser);
			}
			else
			{
				enable_notify_me_popup(idUser);
			}
		}
		else if(result_ary[0]=='ERROR')
		{
			if(mode=='VALID_EMAIL_CHECK')
			{
				$("#szCustomerEmail_popup_error").html(result_ary[1]);
				$('#szCustomerEmail_popup_error').css('display','inline-block');
			}
			else
			{
				$("#szCustomerName_popup_error").html(result_ary[1]);
				$('#szCustomerName_popup_error').css('display','inline-block');
			}
			if(called_from=='SEARCH_SERVICE')
			{		
				enable_notify_me_popup_search_service(idUser,page_url,window.event,szBookingRandomNum,page_url2,1);
			}
			else if(called_from=='REQUIREMENT_PAGE')
			{
				//update_expacted_services_requirement(page_url);
				enable_notify_me_popup_search_service(idUser,page_url,window.event,szBookingRandomNum,page_url2,1,'REQUIREMENT');
			}
			else
			{
				$("#notify_me_button").unbind("click");
				$('#notify_me_button').attr('class','gray-button1');
				$('#notify_me_button').removeAttr('onclick');
			}
		}		
	});
}
function enable_next_step_country()
{
	var idOrigCountry = parseInt($('#szOriginCountry').attr('value'));
	var idDestCountry = parseInt($('#szDestinationCountry').attr('value'));
	
	if(idOrigCountry>0 && idDestCountry>0)
	{
		$("#country_next_step_button").unbind("click");
		$("#country_next_step_button").click(function(){ compare_validate_home_page() });
		$('#country_next_step_button').attr('class','orange-button1');
	}
	else
	{
		$("#country_next_step_button").unbind("click");
		$('#country_next_step_button').attr('class','gray-button1');
	}
}

function change_haulage_model_status_model_add(idMapping,changeStatusFlag,idForwarder,iPriorityLevel,idHaulageModel,idWarehouse,mainurl,count,idPricing,directionId,fWmFactor)
{
	var showflag="showModelData";
	var haulageCountry = $("#haulageCountry").attr('value');
	var szCity = $("#szCity").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idDirection = $("#idDirection").attr('value');
	if(idHaulageModel=='3')
	{
		$("#update_postcode_form").attr('style',"display:none");
		$("#update_postcode_form_add_edit").attr('style',"display:none");
		
	}
	else if(idHaulageModel=='4')
	{
		$("#update_distance_form").attr('style',"display:none");
		$("#update_distance_form_add_edit").attr('style',"display:none");
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idForwarder:idForwarder,idCountry:haulageCountry,szCity:szCity,haulageWarehouse:haulageWarehouse,idDirection:idDirection,flag:showflag,idMapping:idMapping,changeStatusFlag:changeStatusFlag,flag:showflag},function(result){
	$("#view_haulage_model_div_id").html(result);
			
			var totalActive=$("#totalActive").attr('value');
			var rowCount = $('#view_haulage_model tr').length;
			var t=parseInt(iPriorityLevel);
			if(changeStatusFlag=='A')
			{
				
				//var trcount=parseInt(rowCount)-1;
				var div_id="haulage_data_"+iPriorityLevel;			
			}
			else
			{
				var trcount=parseInt(rowCount)-1;
				var div_id="haulage_data_"+trcount;
			}
			if(rowCount>0)
			{
				for(k=1;k<rowCount;++k)
				{
					newdivid="haulage_data_"+k;
					if(newdivid!=div_id)
					{
						$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
					}
					else
					{
						$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
			}
			
			var totalActive=$("#totalActive").attr('value');
			var updateflag='I';
			$("#change_status_button").html("<span style='min-width:70px;'>INACTIVATE</span>");
			if(parseInt(totalActive)!=parseInt(t))
			{
				$("#priority_down").unbind("click");
				$("#priority_down").click(function(){change_haulage_model_priority(idMapping,idForwarder,'down',t,updateflag)});
				$("#priority_down").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_down").unbind("click");
				$("#priority_down").attr('style',"opacity:0.4");
			}
			if(parseInt(t)!='1')
			{
				$("#priority_up").unbind("click");
				$("#priority_up").click(function(){change_haulage_model_priority(idMapping,idForwarder,'up',t,updateflag)});
				$("#priority_up").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_up").unbind("click");
				$("#priority_up").attr('style',"opacity:0.4");
			}
			
			
			
			$("#change_status_button").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#change_status_button").click(function(){change_haulage_model_status(div_id,idMapping,updateflag,idForwarder,t)});
			$("#change_status_button").attr('style',"opacity:1");
			if(idHaulageModel=='1')
			{
				$("#zone_popup_add_edit").attr('style',"display:none");
				$("#zone_popup_add_edit").html('');
			}
			else if(idHaulageModel=='2')
			{
				$("#update_city_form_add_edit").attr('style',"display:none");
				$("#update_city_form_add_edit").html('');
			}
			else if(idHaulageModel=='3')
			{
				$("#update_postcode_form").attr('style',"display:none");
				$("#update_postcode_form_add_edit").attr('style',"display:none");
				$("#update_postcode_form_add_edit").html('');
				
			}
			else if(idHaulageModel=='4')
			{
				$("#update_distance_form").attr('style',"display:none");
				$("#update_distance_form_add_edit").attr('style',"display:none");
				$("#update_distance_form_add_edit").html('');
			}
			
			open_pricing_model_detail(idHaulageModel,idWarehouse,mainurl,count,idPricing,directionId,fWmFactor);
	
	});
}
function enable_notify_me_popup(idUser,page_url,kEvent,mode,booking_key)
{	
	var user_id = parseInt(idUser);
	if(user_id>0)
	{
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		if(isValidEmail(szCustomerEmail))
		{
			
			$("#notify_me_button").removeAttr("onclick");
			$("#notify_me_button").unbind("click");
			
			$("#notify_me_button").click(function(){
				 if(mode=='REQUIREMENT')
				 {
				 	update_expacted_services(mode,page_url,booking_key);
				 }
				 else
				 {
				 	update_expacted_services_requirement(page_url);
				 }  
			});
			$('#notify_me_button').attr('class','orange-button1');
			$('#szCustomerEmail_popup_error').css('display','none');
			if(kEvent)
			{
				if(kEvent.keyCode==13)
				{
					if(mode=='REQUIREMENT')
					 {
					 	update_expacted_services(mode,page_url,booking_key);
					 }
					 else
					 {
					 	update_expacted_services_requirement(page_url);
					 }  
				}
			}
		}
		else
		{	
			$("#notify_me_button").removeAttr("onclick");
			$("#notify_me_button").unbind("click");
			$('#notify_me_button').attr('class','gray-button1');
		}
	}
	else
	{
		var szCustomerName = $('#szCustomerName_popup').attr('value');
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		
		if(szCustomerName!='')
		{
			$('#szCustomerName_popup_error').css('display','none');
		}		
		
		if(isValidEmail(szCustomerEmail))
		{
			$('#szCustomerEmail_popup_error').css('display','none');
		}
		
		if(isValidEmail(szCustomerEmail) && szCustomerName!='')
		{
			
			$("#notify_me_button").removeAttr("onclick");
			$("#notify_me_button").unbind("click");
			
			$("#notify_me_button").click(function(){
			
				if(mode=='REQUIREMENT')
				{
					update_expacted_services(mode,page_url,booking_key);
				}
				else
				{
					update_expacted_services_requirement(page_url);
				}   
			});
			$('#notify_me_button').attr('class','orange-button1');
			$('#szCustomerEmail_popup_error').css('display','none');
			$('#szCustomerName_popup_error').css('display','none');
			if(kEvent)
			{
				if(kEvent.keyCode==13)
				{
					if(mode=='REQUIREMENT')
					{
						update_expacted_services(mode,page_url,booking_key);
					}
					else
					{
						update_expacted_services_requirement(page_url);
					}   
				}
			}
		}
		else
		{
			$("#notify_me_button").removeAttr("onclick");
			$("#notify_me_button").unbind("click");
			$('#notify_me_button').attr('class','gray-button1');
		}
	}
}

function disable_notify_me_popup_search_service(user_id)
{
	if(user_id>0)
	{
		$('#szCustomerEmail_popup_error').css('display','none');
		//$('#szCustomerEmail_popup').attr('value','');
	}
	else
	{
		$('#szCustomerEmail_popup_error').css('display','none');
		//$('#szCustomerEmail_popup').attr('value','');
		
		$('#szCustomerName_popup_error').css('display','none');
		//$('#szCustomerName_popup').attr('value','');
	}
	
	$("#try_step_by_step_button").removeAttr("onclick");
	$("#try_step_by_step_button").unbind("click");
		
	$("#go_back_change_button").removeAttr("onclick");
	$("#go_back_change_button").unbind("click");
	
	$("#make_new_search_button").removeAttr("onclick");
	$("#make_new_search_button").unbind("click");
	
	$('#try_step_by_step_button').attr('class','gray-button1');
	$('#go_back_change_button').attr('class','gray-button1');
	$('#make_new_search_button').attr('class','gray-button1');
}

function enable_notify_me_popup_search_service(idUser,page_url,kEvent,booking_key,page_url2,flag,mode)
{	
	var user_id = parseInt(idUser);
	if(page_url=='')
	{
		page_url = $('#szLandingPageUrl').attr('value');
	}
	if(page_url2=='')
	{
		page_url2 = $('#szReuirementPageUrl').attr('value');
	}
	if(user_id>0)
	{
		//alert('hi');
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		if(szCustomerEmail !='' && flag!=1)
		{
			$('#iSendNotification_search_service_popup').prop('checked','checked');
			$(".checkbox").attr('style','background-position: 0px -50px;');
		}		
		else
		{
			//$('#iSendNotification_search_service_popup').removeAttr('checked');
			//$(".checkbox").attr('style','');
		}
		
		var check_notify = $('#iSendNotification_search_service_popup').prop('checked');
		//alert(check_notify);
		
		if(check_notify || szCustomerEmail=='')
		{ 
			if((szCustomerEmail!='' && isValidEmail(szCustomerEmail) && check_notify) || (!check_notify))
			{			
				$("#try_step_by_step_button").removeAttr("onclick");
				$("#try_step_by_step_button").unbind("click");
				
				$("#go_back_change_button").removeAttr("onclick");
				$("#go_back_change_button").unbind("click");
				
				$("#make_new_search_button").removeAttr("onclick");
				$("#make_new_search_button").unbind("click");
				
				$("#try_step_by_step_button").click(function(){			
					update_expacted_services('REQUIREMENT',page_url2,booking_key);
				});
				
				$("#go_back_change_button").click(function(){			
					update_expacted_services('back',page_url,booking_key);
				});
				
				$("#make_new_search_button").click(function(){			
					update_expacted_services('clear',page_url,booking_key);
				});
				
				$('#try_step_by_step_button').attr('class','orange-button1');
				$('#go_back_change_button').attr('class','orange-button1');
				$('#make_new_search_button').attr('class','orange-button1');
				
				$("#notify_me_button").removeAttr("onclick");
				$("#notify_me_button").unbind("click");
				
				//alert('hi');
			
				if(szCustomerEmail !='' && szCustomerName!='')
				{
					$("#notify_me_button").click(function(){
					 if(mode=='REQUIREMENT')
					 {
					  	var page_url=$("#landingUrl").attr('value');
					 	update_expacted_services_requirement(page_url);
					 }   
					});
					$('#notify_me_button').attr('class','orange-button1');
				}
				$('#notify_me_button').attr('class','orange-button1');
				
				$('#szCustomerEmail_popup_error').css('display','none');
				
				
			}	
			else
			{	
				$("#try_step_by_step_button").removeAttr("onclick");
				$("#try_step_by_step_button").unbind("click");
				
				$("#go_back_change_button").removeAttr("onclick");
				$("#go_back_change_button").unbind("click");
				
				$("#make_new_search_button").removeAttr("onclick");
				$("#make_new_search_button").unbind("click");
				
				$('#try_step_by_step_button').attr('class','gray-button1');
				$('#go_back_change_button').attr('class','gray-button1');
				$('#make_new_search_button').attr('class','gray-button1');
			}
		}
		else
		{
			if((szCustomerEmail!='' && isValidEmail(szCustomerEmail) && check_notify) || (!check_notify))
			{			
				$("#try_step_by_step_button").removeAttr("onclick");
				$("#try_step_by_step_button").unbind("click");
				
				$("#go_back_change_button").removeAttr("onclick");
				$("#go_back_change_button").unbind("click");
				
				$("#make_new_search_button").removeAttr("onclick");
				$("#make_new_search_button").unbind("click");
				
				$("#try_step_by_step_button").click(function(){			
					update_expacted_services('REQUIREMENT',page_url2,booking_key);
				});
				
				$("#go_back_change_button").click(function(){			
					update_expacted_services('back',page_url,booking_key);
				});
				
				$("#make_new_search_button").click(function(){			
					update_expacted_services('clear',page_url,booking_key);
				});
				
				$('#try_step_by_step_button').attr('class','orange-button1');
				$('#go_back_change_button').attr('class','orange-button1');
				$('#make_new_search_button').attr('class','orange-button1');
				
				$("#notify_me_button").removeAttr("onclick");
				$("#notify_me_button").unbind("click");
				
			//	alert('hi');
			
				if(szCustomerEmail !='' && szCustomerName!='')
				{
					$("#notify_me_button").click(function(){
					 if(mode=='REQUIREMENT')
					 {
					  	var page_url=$("#landingUrl").attr('value');
					 	update_expacted_services_requirement(page_url);
					 }   
					});
					$('#notify_me_button').attr('class','orange-button1');
				}
				$('#notify_me_button').attr('class','orange-button1');
				
				$('#szCustomerEmail_popup_error').css('display','none');
				
				
			}	
			else
			{	
				$("#try_step_by_step_button").removeAttr("onclick");
				$("#try_step_by_step_button").unbind("click");
				
				$("#go_back_change_button").removeAttr("onclick");
				$("#go_back_change_button").unbind("click");
				
				$("#make_new_search_button").removeAttr("onclick");
				$("#make_new_search_button").unbind("click");
				
				$('#try_step_by_step_button').attr('class','gray-button1');
				$('#go_back_change_button').attr('class','gray-button1');
				$('#make_new_search_button').attr('class','gray-button1');
			}
		}
	}
	else
	{
		var szCustomerName = $('#szCustomerName_popup').attr('value');
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		var check_notify = $('#iSendNotification_search_service_popup').prop('checked');
		
		if(szCustomerName!='')
		{
			$('#szCustomerName_popup_error').css('display','none');
		}		
		
		if(isValidEmail(szCustomerEmail))
		{
			$('#szCustomerEmail_popup_error').css('display','none');
		}
		
		if((szCustomerEmail !='' || szCustomerName!='') && flag!=1)
		{
			$('#iSendNotification_search_service_popup').prop('checked','checked');
			$(".checkbox").attr('style','background-position: 0px -50px;');
		}		
		else
		{
			//$('#iSendNotification_search_service_popup').removeAttr('checked');
			//$(".checkbox").attr('style','');
		}
		//alert(szCustomerEmail);
		//alert(szCustomerName);
		var check_notify = $('#iSendNotification_search_service_popup').prop('checked');
		
		//if(check_notify || (szCustomerEmail=='' || szCustomerName==''))
		//{
			if(szCustomerEmail!='' && (isValidEmail(szCustomerEmail) && jQuery.trim(szCustomerName)!='' && check_notify) || (!check_notify))
			{						
				$("#try_step_by_step_button").removeAttr("onclick");
				$("#try_step_by_step_button").unbind("click");
				
				$("#go_back_change_button").removeAttr("onclick");
				$("#go_back_change_button").unbind("click");
				
				$("#make_new_search_button").removeAttr("onclick");
				$("#make_new_search_button").unbind("click");
				
				$("#try_step_by_step_button").click(function(){			
					update_expacted_services('REQUIREMENT',page_url2,booking_key);
				});
				
				$("#go_back_change_button").click(function(){			
					update_expacted_services('back',page_url,booking_key);
				});
				
				$("#make_new_search_button").click(function(){			
					update_expacted_services('clear',page_url,booking_key);
				});
				
				$('#try_step_by_step_button').attr('class','orange-button1');
				$('#go_back_change_button').attr('class','orange-button1');
				$('#make_new_search_button').attr('class','orange-button1');
				if(szCustomerEmail !='' && szCustomerName!='')
				{
					$("#notify_me_button").click(function(){
					 if(mode=='REQUIREMENT')
					 {
					  	var page_url=$("#landingUrl").attr('value');
					 	update_expacted_services_requirement(page_url);
					 }   
					});
					$('#notify_me_button').attr('class','orange-button1');
				}
				$('#szCustomerEmail_popup_error').css('display','none');
				$('#szCustomerName_popup_error').css('display','none');
			}
			else
			{
				$("#try_step_by_step_button").removeAttr("onclick");
				$("#try_step_by_step_button").unbind("click");
				
				$("#go_back_change_button").removeAttr("onclick");
				$("#go_back_change_button").unbind("click");
				
				$("#make_new_search_button").removeAttr("onclick");
				$("#make_new_search_button").unbind("click");
				
				$('#try_step_by_step_button').attr('class','gray-button1');
				$('#go_back_change_button').attr('class','gray-button1');
				$('#make_new_search_button').attr('class','gray-button1');
			}
		//}
	}
}
function isValidEmail(szEmail)
{
    if(szEmail=='' || szEmail=='undefined')
    {
        return false;
    }
    else
    {
        szEmail = jQuery.trim(szEmail);
        var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        if (filter.test(szEmail))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
function resetContentOfPage()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'RESET_SESSION_DETAILS'},function(result){
	if(result == true)
	{
            window.location.href = window.location.href ;
	}
    });
}

function update_booking_service_type_requirement(type_id,booking_key,mode,idOriginPostcode,szCityName,szOriginDestination)
{	
	change_next_step_button_html('NEXT STEP');
	
	var input_val ='';
	input_val = {type_id:type_id,booking_key:booking_key,mode:mode,idOriginPostcode:idOriginPostcode,szCityName:szCityName,szOriginDestination:szOriginDestination};
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",input_val,function(result){		     
       result_ary = result.split("||||");
       
       $("#boxscroll2").niceScroll().hide();
	   if(jQuery.trim(result_ary[0])=='SUCCESS')
	   {
			 $("#popupRegError").attr('style','display:none;');
			 if(mode == 'SERVICE_TYPE')
			 {			 	
				$("#transportation-box").removeClass('active');
				$("#transportation-box").html(result_ary[1]);
				$("#all_available_service").attr('style','display:none;');
				$("#shipper-consignee-box").removeClass('active');
				$("#shipper-consignee-box").html(result_ary[2]);				 	
				$("#cargo-box").addClass('active');
				$("#cargo-box").html(result_ary[3]);
				$("#import-box").attr("style","display:block;");
				$("#import-box").html(result_ary[4]);
			}
			else if(mode == 'VALIDATE_POSTCODE_AFTER_HAULAGE_POPUP')
			{
				$("#shipper-consignee-box").removeClass('active');
				$("#transportation-box").removeClass('active');
				$("#transportation-box").html(result_ary[1]);
				
				$("#cargo-box").addClass('active');
				$("#cargo-box").html(result_ary[2]);
				
				$("#shipper-consignee-box").html(result_ary[3]);		
				$("#import-box").attr("style","display:block;");
				$("#import-box").html(result_ary[4]);
				
				$("#all_available_service").attr('style','display:none;');
			}
			removePopupScrollClass("all_available_service");
		}
		if(jQuery.trim(result_ary[0])=='MULTI_EMPTY_POSTCODE')
        {
			change_next_step_button_default_html();
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	addPopupScrollClass('all_available_service');
         	if(szOriginDestination=='ORIGIN')
         	{
         		$("#transportation-box").html(result_ary[2]);
         	}
         	$("#all_available_service").attr('style','display:block');
         }
         if(jQuery.trim(result_ary[0])=='MULTI_PARTIAL_EMPTY_POSTCODE')
         {
        	 change_next_step_button_default_html();
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	addPopupScrollClass('all_available_service');
         	if(szOriginDestination=='ORIGIN')
         	{
         		$("#transportation-box").html(result_ary[2]);
         	}
         	$("#all_available_service").attr('style','display:block');
         }
         if(jQuery.trim(result_ary[0])=='MULTIREGIO')
         {
        	change_next_step_button_default_html();
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	if(szOriginDestination=='ORIGIN')
         	{
         		$("#transportation-box").html(result_ary[2]);
         	}
         	$("#all_available_service").attr('style','display:block');
         	addPopupScrollClass('all_available_service');
         }   
		 else if(result_ary[0]=='POPUP')
		 {
			change_next_step_button_default_html();
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		 }
		 else if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
		 {
			 change_next_step_button_default_html();
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		 }
    });
}
function update_booking_service_type(type_id,booking_key,mode,formId,update_srvice_flag,szCityName,szOriginDestination)
{	
	if(update_srvice_flag!='UPDATE_SERVICE')
	{
		if(mode=='CARGO_DETAILS')
		{
			//change_next_step_button_html('SHOW OPTION');
		}
		else
		{
			change_next_step_button_html('NEXT STEP');
		}
	}
	
	
	var input_val ='';
	if(mode == 'SERVICE_TYPE')
	{
		if(update_srvice_flag=='UPDATE_SERVICE')
		{
			input_val = {type_id:type_id,booking_key:booking_key,mode:mode,update_srvice_flag:update_srvice_flag,szCityName:szCityName,szOriginDestination:szOriginDestination};
		}
		else
		{
			input_val = {type_id:type_id,booking_key:booking_key,mode:mode,update_srvice_flag:update_srvice_flag};
		}
	}
	if(mode == 'CARGO_TIMING')
	{
		input_val = $("#timing_box_form").serialize()
	}
	if(mode == 'CUSTOM_CLEARANCE')
	{
		input_val = $("#custom_clearance_form").serialize()
	}
	if(mode == 'CARGO_DETAILS')
	{
		input_val = $("#cargo_details_form").serialize()
	}
	if(mode == 'CARGO_TIMING_UPDATE')
	{
		input_val = {type_id:type_id,booking_key:booking_key,mode:mode,dtTiming:formId};
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",input_val,function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			 $("#popupRegError").attr('style','display:none;');
			 if(mode == 'SERVICE_TYPE')
			 {			 	
				 $("#transportation-box").removeClass('active');
				 $("#transportation-box").html(result_ary[1]);
				 
				 //$("#transportation-box").unbind("click");
				 //$("#transportation-box").click(function(){ display_requirement_edit_box_popup('SERVICE_TYPE_EDIT_POPUP',booking_key) });
				
				if(update_srvice_flag=='UPDATE_SERVICE')
				{
					$("#all_available_service").attr('style','display:none;');
					$("#shipper-consignee-box").removeClass('active');
				 	$("#shipper-consignee-box").html(result_ary[2]);				 	
				 	$("#cargo-box").addClass('active');
				 	$("#cargo-box").html(result_ary[3]);
				 	$("#import-box").attr("style","display:block;");
				 	$("#import-box").html(result_ary[4]);
				}
				else if(update_srvice_flag=='DON_NOT_KNOW_POPUP')
				{
					$("#all_available_service").attr('style','display:none;');
					$("#shipper-consignee-box").addClass('active');
				 	$("#shipper-consignee-box").html(result_ary[2]);
				 	$("#import-box").attr("style","display:block;");
				 	$("#import-box").html(result_ary[3]);
				}
				else
				{		 
				 	$("#shipper-consignee-box").addClass('active');
				 	$("#shipper-consignee-box").html(result_ary[2]);
				 	$("#import-box").attr("style","display:block;");
				 	$("#import-box").html(result_ary[3]);
				}
				$("#service_type_hidden").attr("value",type_id);
				
				removePopupScrollClass("all_available_service");
			}
			if(mode == 'CARGO_TIMING')
			{
				$("#cargo-box").removeClass('active');
				$("#cargo-box").html(result_ary[1]);
				//$("#cargo-box").click(function(){ display_requirement_edit_box_popup('CARGO_TIMING_EDIT_POPUP',booking_key) });
			
				if(result_ary[2]=='NO_CC_EXIST')
				{
					$("#complete-box").addClass('active');
					$("#complete-box").html(result_ary[3]);
					
					$("#import-box").attr('style','display:none;');
				}
				else
				{
					$("#import-box").attr('style','display:block;');
					$("#import-box").addClass('active');
					$("#import-box").html(result_ary[3]);	
				}
			}
			if(mode == 'CARGO_TIMING_UPDATE')
			{
				$("#cargo-box").removeClass('active');
				$("#cargo-box").html(result_ary[1]);
				$("#all_available_service").attr('style','display:none;');
			
				if(result_ary[2]=='NO_CC_EXIST')
				{
					$("#complete-box").addClass('active');
					$("#complete-box").html(result_ary[3]);
					
					$("#import-box").attr('style','display:none;');
				}
				else
				{
					$("#import-box").attr('style','display:block;');
					$("#import-box").addClass('active');
					$("#import-box").html(result_ary[3]);	
				}
			}
			if(mode == 'CUSTOM_CLEARANCE')
			{
				$("#import-box").removeClass('active');
				$("#import-box").html(result_ary[1]);
				//$("#import-box").click(function(){ display_requirement_edit_box_popup('CARGO_TIMING_EDIT_POPUP',booking_key) });
				
				$("#complete-box").addClass('active');
				$("#complete-box").html(result_ary[2]);
							
				if(result_ary[3]=='EXPORT_POPUP')
				{
					$("#additional_cc_popup").html(result_ary[4]);
					$("#additional_cc_popup	").attr('style','display:block;');	
				}			
			}			
			if(mode == 'CARGO_DETAILS')
			{
				$("#regError").attr('style','display:none;');
				var idUser = result_ary[1] ;
	         	var user_id = parseInt(idUser);
	         	
	         	if(user_id==0 || isNaN(user_id) || user_id=='NaN')
		    	{
		    		show_currency_popup('cargo_details_form','REQUIREMENT_PAGE');
		    	}
		    	else
		    	{  
		    		var page_url = jQuery.trim(result_ary[2]);
		    		//var szBookingRandomNum = $("#szBookingRandomNum").attr('value');
		    		//var page_url_1 = page_url + "/" + szBookingRandomNum + "/";
		        	redirect_url(page_url);
		        }
			}
		}
		else if(result_ary[0]=='POPUP')
		{
			change_next_step_button_default_html('NEXT STEP');
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		}
		else if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
		{
			change_next_step_button_default_html('NEXT STEP');
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		}
    });
}
function display_additional_cc_popup(value)
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php?flag=DISPLAY_CC_ADDITIONAL_POPUP",$("#custom_clearance_form").serialize(),function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#additional_cc_popup").html(result_ary[1]);
			$('#additional_cc_popup').attr('style','display:block;');
		}
	 });
}
function stating_new_trade_requirement(num)
{
	var form_id = $("#form_key_"+num).attr('value');
	if(form_id !='')
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#"+form_id).serialize(),function(result){		     
	         result_ary = result.split("||||");  
	        
	         if(jQuery.trim(result_ary[0])=='ERROR')
	         {
	         	var iCheck = $('#iCheck').attr('value');
	         	if(iCheck==1)
	         	{
	         		 redirect_url(result_ary[1]);
	         	}
	         	else
	         	{
			       $("#error_popup").html(result_ary[1]);
				   $('#error_popup').attr('style','display:block;');
				}
	         }
	         else
	         {
	         	 redirect_url(result_ary[1]);
	         }
	      });
	  }
}
function check_empty_city_post_code(formId,default_value,type,kEvent,key_up_flag)
{
	var szOriginCityPostcode = $('#szOriginCityPostcode').attr('value');
	var szDestinationCityPostcode = $('#szDestinationCityPostcode').attr('value');
	
	var iOriginError =  $('#iOriginError').attr('value');
	var iDestinationError =  $('#iDestinationError').attr('value');
	
	if(type=='ORIGIN' && key_up_flag!=2 && kEvent.keyCode!=9)
    {
    	$("#origin_city_country_error_span").css('display','none'); 
    	$('#iOriginError').attr('value','');
    }
    if(type=='DESTINATION' && key_up_flag!=2 && kEvent.keyCode!=9)
    {
    	$("#destination_city_country_error_span").css('display','none'); 
    	$('#iDestinationError').attr('value','');
    }
    
	if((szOriginCityPostcode!='' && szOriginCityPostcode!=default_value ) && (szDestinationCityPostcode!='' && szDestinationCityPostcode != default_value) && (iOriginError!=1 && iDestinationError!=1))
	{
		$("#city_postcode_next_step_button").unbind("click");
		$("#city_postcode_next_step_button").click(function(){ update_city_postcode_requirement(formId) });
		
		if(kEvent && kEvent.keyCode==13)
		{
			on_enter_key_press(kEvent,'update_city_postcode_requirement',formId); 
		}
		$('#city_postcode_next_step_button').attr('class','orange-button1');
	}
	else
	{
		$("#szDestinationCityPostcode").unbind("keyup");		
		$("#city_postcode_next_step_button").unbind("click");
		$('#city_postcode_next_step_button').attr('class','gray-button1');
	}
}

function show_default_text(feild_id,default_value)
{
	var feild_value = $("#"+feild_id).attr('value');
	if(feild_value == "")
	{
		$('#'+feild_id).attr('value',default_value);
	}
}

function validate_city_postcode(szCityPostcode,type,booking_key,default_text)
{
    if(type=='ORIGIN')
    {
    	//$("#origin_city_country_error_span").css('display','none'); 
    	 var display_autofil = $("#szOriginCityPostcode_autofill:last").css('display');
    	 $('#iOriginError').attr('value','');
    }
    
    if(type=='DESTINATION')
    {
    	//$("#destination_city_country_error_span").css('display','none'); 
    	 var display_autofil = $("#szDestinationCityPostcode_autofill:last").css('display');
    	 $('#iDestinationError').attr('value','');
    } 
    if(display_autofil=='block')
    {
    	return false;
    }
    if(szCityPostcode=='' || szCityPostcode==default_text)
    {
    	return false;
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",{booking_key:booking_key,szCityPostcode:szCityPostcode,mode:'VALIDATE_CITY_POSTCODE',type:type},function(result){		     
         result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
         {         	
         	if(result_ary[1]=='ORIGIN')
         	{
         		if(jQuery.trim(result_ary[2])!='')
         		{
	         		$("#origin_city_country_error_span").css('display','block'); 
	         		$("#origin_city_country_error_span").html(result_ary[2]);
			 		$('#iOriginError').attr('value','1');
	         	}
         	}
         	if(result_ary[3]=='DESTINATION')
         	{
         		if(jQuery.trim(result_ary[4])!='')
         		{
	         		$("#destination_city_country_error_span").css('display','block'); 
	         		$("#destination_city_country_error_span").html(result_ary[4]);
	         		$('#iDestinationError').attr('value','1');
	         	}
         	}
         	
         	$("#szDestinationCityPostcode").unbind("keyup");
         	$("#city_postcode_next_step_button").unbind("click");
			$('#city_postcode_next_step_button').attr('class','gray-button1');
		
			check_empty_city_post_code('city_postcode_box_form',default_text,type,window.event,'2');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
         	 $("#popupRegError").attr('style','display:none;');
         	 $("#cargo-box").addClass('active');
			 $("#shipper-consignee-box").removeClass('active');
			 $("#cargo-box").html(result_ary[1]);
			 $("#shipper-consignee-box").html(result_ary[2]);
			 $("#import-box").attr("style","display:block;");
			 $("#import-box").html(result_ary[3]);
			 $('#iDestinationError').attr('value','');
			 $('#iOriginError').attr('value','');
			 check_empty_city_post_code('city_postcode_box_form',default_text,type,window.event,'2');
         }
         else
         {
         	$('#iDestinationError').attr('value','');
			 $('#iOriginError').attr('value','');
         	if(type=='ORIGIN')
		    {
		    	$("#origin_city_country_error_span").css('display','none'); 
		    }
		    if(type=='DESTINATION')
		    {
		    	$("#destination_city_country_error_span").css('display','none'); 
		    } 
         }
    });  
}
function update_city_postcode_requirement(formId)
{
	change_next_step_button_html('NEXT STEP');
    $("#origin_city_country_error_span").css('display','none');
    $("#destination_city_country_error_span").css('display','none'); 
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",$("#"+formId).serialize(),function(result){		     
         result_ary = result.split("||||");
       
         if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
         {  
        	 change_next_step_button_default_html('NEXT STEP');
        	 
         	if(result_ary[1]=='ORIGIN')
         	{
         		if(jQuery.trim(result_ary[2])!='')
         		{
	         		$("#origin_city_country_error_span").css('display','block'); 
	         		$("#origin_city_country_error_span").html(result_ary[2]);
				 	$('#iOriginError').attr('value','1');
	         	}
         	}
         	if(result_ary[3]=='DESTINATION')
         	{
         		if(jQuery.trim(result_ary[4])!='')
         		{
	         		$("#destination_city_country_error_span").css('display','block'); 
	         		$("#destination_city_country_error_span").html(result_ary[4]);
	         		$('#iDestinationError').attr('value','1');
	         	}
         	}
         	
         	$("#city_postcode_next_step_button").unbind("click");
			$('#city_postcode_next_step_button').attr('class','gray-button1');
         }
         else if(jQuery.trim(result_ary[0])=='POPUP')
         {
        	 change_next_step_button_default_html('NEXT STEP');
         	$("#boxscroll2").niceScroll().hide();
         	$("#all_available_service").css('display','block'); 
         	$("#all_available_service").html(result_ary[1]);
         	$('#iDestinationError').attr('value','');
			$('#iOriginError').attr('value','');
			addPopupScrollClass('all_available_service');
         }
         else if(jQuery.trim(result_ary[0])=='MULTI_EMPTY_POSTCODE')
         {
        	 change_next_step_button_default_html('NEXT STEP');
         	 $("#boxscroll2").niceScroll().hide();
         	 $("#all_available_service").attr('style','display:block');
         	 $("#regError").attr('style','display:none;');
         	 $("#all_available_service").html(result_ary[1]);
         	 $('#iDestinationError').attr('value','');
			 $('#iOriginError').attr('value','');
			 
			 addPopupScrollClass('all_available_service');
         }
         else if(jQuery.trim(result_ary[0])=='MULTI_PARTIAL_EMPTY_POSTCODE')
         {
        	 change_next_step_button_default_html('NEXT STEP');
         	 $("#boxscroll2").niceScroll().hide();
         
         	 $("#all_available_service").attr('style','display:block');
         	 $("#regError").attr('style','display:none;');
         	 $("#all_available_service").html(result_ary[1]);
         	 $('#iDestinationError').attr('value','');
			 $('#iOriginError').attr('value','');
			 
	         addPopupScrollClass('all_available_service');
         }
         else if(jQuery.trim(result_ary[0])=='MULTIREGIO')
         {
        	 change_next_step_button_default_html('NEXT STEP');
         	$("#boxscroll2").niceScroll().hide();
         	
         	$("#all_available_service").attr('style','display:block');
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	$('#iDestinationError').attr('value','');
			$('#iOriginError').attr('value','');
			
			addPopupScrollClass('all_available_service');
         }         
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
         	 $("#boxscroll2").niceScroll().hide();
         	 $("#popupRegError").attr('style','display:none;');
         	 $("#cargo-box").addClass('active');
			 $("#shipper-consignee-box").removeClass('active');
			 $("#cargo-box").html(result_ary[1]);
			 $("#shipper-consignee-box").html(result_ary[2]);
			 $("#import-box").attr("style","display:block;");
			 $("#import-box").html(result_ary[3]);
			 $('#iDestinationError').attr('value','');
			 $('#iOriginError').attr('value','');
			 
			 removePopupScrollClass("all_available_service");
         }
    });      
}

function updateEmptyPostcodeCity_requirement(form_id)
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",$("#"+form_id).serialize(),function(result){
		result_ary = result.split("||||");
		$("#boxscroll2").niceScroll().hide();
		
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#all_available_service").attr('style','display:none;');
			$("#shipper-consignee-box").removeClass('active');
		 	$("#shipper-consignee-box").html(result_ary[2]);	
		 	$("#import-box").attr("style","display:block;");
			$("#import-box").html(result_ary[3]);			 	
		 	$("#cargo-box").addClass('active');
		 	$("#cargo-box").html(result_ary[1]);
		 	
		 	removePopupScrollClass("all_available_service");
		}
		else if(jQuery.trim(result_ary[0])=='MULTIREGIO')
        {
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	$("#all_available_service").attr('style','display:block');
         	addPopupScrollClass('all_available_service');
        } 
		else if(jQuery.trim(result_ary[0])=='MULTI_EMPTY_POSTCODE')
        {
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	$("#all_available_service").attr('style','display:block');
         	addPopupScrollClass('all_available_service');
        }
        else if(jQuery.trim(result_ary[0])=='MULTI_PARTIAL_EMPTY_POSTCODE')
        {
        	$("#regError").attr('style','display:none;');
        	$("#all_available_service").html(result_ary[1]);
        	$("#all_available_service").attr('style','display:block');
        	addPopupScrollClass('all_available_service');
        }
		else if(jQuery.trim(result_ary[0])=='POPUP')
		{
			$("#regError").attr('style','display:none;');
			$("#all_available_service").html(result_ary[1]);
			$("#all_available_service").attr('style','display:block;');		
			addPopupScrollClass('all_available_service');
		}
		else if(jQuery.trim(result_ary[0])=='ERROR')
		{
			$("#popupError").html(result_ary[1]);
			$("#popupError").attr('style','display:block;');			
		}
	});
}

function updatePostcodeRegion_requirement(form_id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",$("#"+form_id).serialize(),function(result){
		result_ary = result.split("||||");
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#all_available_service").attr('style','display:none;');
			$("#shipper-consignee-box").removeClass('active');
		 	$("#shipper-consignee-box").html(result_ary[2]);	
		 	$("#import-box").attr("style","display:block;");
			$("#import-box").html(result_ary[3]);			 	
		 	$("#cargo-box").addClass('active');
		 	$("#cargo-box").html(result_ary[1]);
		 	
		 	removePopupScrollClass("all_available_service");
		}
		else if(jQuery.trim(result_ary[0])=='MULTIREGIO')
        {
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	$("#all_available_service").attr('style','display:block');
         	addPopupScrollClass('all_available_service');
        } 
		else if(jQuery.trim(result_ary[0])=='MULTI_EMPTY_POSTCODE')
        {
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	$("#all_available_service").attr('style','display:block');
         	addPopupScrollClass('all_available_service');
         }
         else if(jQuery.trim(result_ary[0])=='MULTI_PARTIAL_EMPTY_POSTCODE')
         {
         	$("#regError").attr('style','display:none;');
         	$("#all_available_service").html(result_ary[1]);
         	$("#all_available_service").attr('style','display:block');
         	addPopupScrollClass('all_available_service');
         }
		 else if(jQuery.trim(result_ary[0])=='POPUP')
		 {
			$("#all_available_service").html(result_ary[1]);
			$("#all_available_service").attr('style','display:block;');			
			addPopupScrollClass('all_available_service');
		 }
		 else if(jQuery.trim(result_ary[0])=='ERROR')
		 {
			$("#popupError").html(result_ary[1]);
			$("#popupError").attr('style','display:block;');	
			addPopupScrollClass('all_available_service');
		 }
		 $("#boxscroll2").niceScroll().hide();
	});
}

function enable_timing_box_button(booking_key)
{
	//var checked_flag = $("#szTiming").attr('value');
	var checked_flag =$('[name=timingAry[idTimingType]]:checked').val();
	var timing_date = $("#datepicker1_requirement").attr('value');

	if(isDate(timing_date) && (checked_flag>0))
	{
		$("#timing_box_next_step_button").unbind("click");
		$("#timing_box_next_step_button").click(function(){ update_booking_service_type(false,booking_key,'CARGO_TIMING','timing_box_form') });
		$('#timing_box_next_step_button').attr('class','orange-button1');
	}
	else
	{
		$("#timing_box_next_step_button").unbind("click");
		$('#timing_box_next_step_button').attr('class','gray-button1');
	}
}

function enable_submit_button(form_id,field_name)
{
	var checked_flag =$('[name='+field_name+']:checked').val();
	checked_flag = parseInt(checked_flag);
		
	if(checked_flag>0)
	{
		$("#submit_button").unbind("click");
		$("#submit_button").click(function(){
			if(form_id == 'multi_empty_postcode_from_form')
			{
		 		updateEmptyPostcodeCity_requirement(form_id);
		 	}
		 	else if(form_id == 'partial_empty_postcode_from_form')
			{
		 		updateEmptyPostcodeCity_requirement(form_id);
		 	}
		 	else
		 	{
		 		updatePostcodeRegion_requirement(form_id);
		 	} 
		 });
		 		
		$('#submit_button').attr('class','orange-button1');
	}
	else
	{
		$("#submit_button").unbind("click");
		$('#submit_button').attr('class','gray-button1');
	}
}

function isDate(txtDate)
{
	  var currVal = txtDate;
	  if(currVal == '' || currVal == 'undefined')
	    return false;
	  
	  //Declare Regex 
	  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	  var dtArray = currVal.match(rxDatePattern); // is format OK?
	 
	  if (dtArray == null)
	     return false;
	  
	  //Checks for mm/dd/yyyy format.
	  dtMonth = dtArray[3];
	  dtDay= dtArray[1];
	  dtYear = dtArray[5];
	  	 
	  if (dtMonth < 1 || dtMonth > 12)
	      return false;
	  else if (dtDay < 1 || dtDay> 31)
	      return false;
	  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
	      return false;
	  else if (dtMonth == 2)
	  {
	     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	     if (dtDay> 29 || (dtDay ==29 && !isleap))
	          return false;
	  }
	  return true;
}

function enable_cargo_show_option_button(kEvent,field_name)
{
	var num_cargo_line = $("#hiddenPosition1").attr('value');
	var empty_flag = 1 ;
	for(var i=1;i<=num_cargo_line;i++)
	{
		var length = $("#iLength"+i).attr('value');
		var width = $("#iWidth"+i).attr('value');
		var height = $("#iHeight"+i).attr('value');
		var iQuantity = $("#iQuantity"+i).attr('value');
		var weight = $("#iWeight"+i).attr('value');
		
		length = parseInt(length);
		width = parseInt(width);
		height = parseInt(height);
		weight = parseInt(weight);
		iQuantity = parseInt(iQuantity);
		if(isNaN(length) || isNaN(width) || isNaN(height) || isNaN(weight) || isNaN(iQuantity))
		{
			empty_flag = 2;
			break;
		}
		else if((length <= 0) || (width<=0) || (height <= 0) || (weight<=0) || (iQuantity<=0))
		{
			empty_flag = 2;
			break;
		}
	}
	
	if(empty_flag==1)
	{
		var szBookingRandomNum = $('#booking_key').attr('value');
		if(field_name=='Weight' && kEvent)
		{
			if(kEvent.keyCode == 13)
			{
				 update_booking_service_type(false,szBookingRandomNum,'CARGO_DETAILS'); 
			}
		}
		$("#cargo_detail_button_show_option").unbind("click");
		$("#cargo_detail_button_show_option").click(function(){ update_booking_service_type(false,szBookingRandomNum,'CARGO_DETAILS'); });
		$('#cargo_detail_button_show_option').attr('class','orange-button1');
	}
	else
	{
		$("#cargo_detail_button_show_option").unbind("click");
		$('#cargo_detail_button_show_option').attr('class','gray-button1');
	}
}
function validate_cargo_fields(fieldName,field_name,field_number)
{	
	var field_value = $("#"+field_name).attr('value');
	if(field_value=='')
	{
		$("#cargo_detail_button_show_option").unbind("click");
		$('#cargo_detail_button_show_option').attr('class','gray-button1');
		return false;
	}
	var input_val = $("#cargo_details_form").serialize()+"&fieldName="+fieldName+"&field_number="+field_number ;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_requirement.php",input_val,function(result){
		result_ary = result.split("||||");
		
		enable_cargo_show_option_button();
		if(jQuery.trim(result_ary[0])=='ERROR')
		{
			$("#cargo_measure_error_span").html(result_ary[1]);
			$("#cargo_measure_error_span").attr('style','display:block;');
			
			$("#cargo_detail_button_show_option").unbind("click");
			$('#cargo_detail_button_show_option').attr('class','gray-button1');
		}
		else
		{
			$("#cargo_measure_error_span").attr('style','display:none;');
		}
	});
	
	return false;
	/*
	for(var i=1;i<=num_cargo_line;i++)
	{
		var length = $("#iLength"+i).attr('value');
		var width = $("#iWidth"+i).attr('value');
		var height = $("#iHeight"+i).attr('value');
		var quantity = $("#iQuantity"+i).attr('value');
		var weight = $("#iWeight"+i).attr('value');
		
		if(fieldName=='Length' && length=='')
		{
			return 
		}
		if(fieldName=='Width' && width=='')
		{
			return 
		}
		if(fieldName=='Height' && height=='')
		{
			return 
		}
		if(fieldName=='Quantity' && quantity=='')
		{
			return 
		}
		if(fieldName=='Weight' && weight=='')
		{
			return 
		}
		
		length = parseInt(length);
		width = parseInt(width);
		height = parseInt(height);
		weight = parseInt(weight);
		quantity = parseInt(quantity);
		
		if((isNaN(length) || length<=0) && fieldName=='Length')
		{
			$("#cargo_measure_error_span").html(error_message1+" "+fieldName+" "+error_message2);
			$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
			cargo_empty_flag=2;
			$("#cargo_detail_button_show_option").unbind("click");
			$('#cargo_detail_button_show_option').attr('class','gray-button1');
			break;
		}
		else if((isNaN(width) || width<=0) && fieldName=='Width')
		{
			if((isNaN(length) || length<=0))
			{
				$("#cargo_measure_error_span").html(error_message1+" Length "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else
			{
				$("#cargo_measure_error_span").html(error_message1+" Width "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
		}
		else if((isNaN(height) || height<=0) && fieldName=='Height')
		{
			if((isNaN(length) || length<=0))
			{
				$("#cargo_measure_error_span").html(error_message1+" Length "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else if((isNaN(width) || width<=0))
			{
				$("#cargo_measure_error_span").html(error_message1+" Width "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else
			{			
				$("#cargo_measure_error_span").html(error_message1+" Height "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
		}
		else if((isNaN(quantity) || quantity<=0) && fieldName=='Quantity')
		{
			if((isNaN(length) || length<=0))
			{
				$("#cargo_measure_error_span").html(error_message1+" Length "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else if((isNaN(width) || width<=0))
			{
				$("#cargo_measure_error_span").html(error_message1+" Width "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else if((isNaN(height) || height<=0))
			{			
				$("#cargo_measure_error_span").html(error_message1+" Height "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else
			{
				$("#cargo_measure_error_span").html(error_message1+" Quantity "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
		}
		else if((isNaN(weight) || weight<=0) && fieldName=='Weight')
		{
			if((isNaN(length) || length<=0))
			{
				$("#cargo_measure_error_span").html(error_message1+" Length "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else if((isNaN(width) || width<=0))
			{
				$("#cargo_measure_error_span").html(error_message1+" Width "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else if((isNaN(height) || height<=0))
			{			
				$("#cargo_measure_error_span").html(error_message1+" Height "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else if((isNaN(height) || height<=0))
			{			
				$("#cargo_measure_error_span").html(error_message1+" Height "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				cargo_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
			else
			{
				$("#cargo_measure_error_span").html(error_message1+" Weight "+error_message2);
				$("#cargo_measure_error_span").attr('style','display:block;font-style:italic;');
				weight_empty_flag=2;
				$("#cargo_detail_button_show_option").unbind("click");
				$('#cargo_detail_button_show_option').attr('class','gray-button1');
				break;
			}
		}
	}
		
	if(cargo_empty_flag==1)
	{
		$("#cargo_measure_error_span").attr('style','display:none;');
	}
	*/
}
function submitPasswordUpdate()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_forgetPasswordUpdate.php",$("#chagePassword").serialize(),function(result){
		if($.trim(result)=='SUCCESS')
		{
			window.location.href = window.location.href
		}
		else
		{
			$('#Error-log').html(result);
		}	
	});
}

/*
 This is function will called when clicked on perticular image
*/
function display_requirement_edit_box(mode,booking_key)
{	
	$('#all_available_service').attr('style','display:none;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",{booking_key:booking_key,mode:mode},function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			 $("#popupRegError").attr('style','display:none;');
			 if(mode == 'SERVICE_TYPE_EDIT')
			 {
			 	 var pop_id = $(".active").attr('id');
			 	 
			 	 $("#shipper-consignee-box").removeClass('active');
			 	 $("#cargo-box").removeClass('active');
			 	 $("#import-box").removeClass('active');
			 	 $("#complete-box").removeClass('active');
			 	 $("#shipping-box").removeClass('active');
			 	
			 	 
			 	 $("#transportation-box").attr("style","cursor:default");
			 	 $("#transportation-box").unbind("click");
				 $("#transportation-box").addClass('active');
				 $("#transportation-box").html(result_ary[1]);
				 
				 //$("#transportation-box").click(function(){ display_requirement_edit_box('SERVICE_TYPE_EDIT',booking_key) });
				 
				 $("#shipper-consignee-box").html(result_ary[2]);
				 $("#cargo-box").html(result_ary[3]);
				 $("#import-box").attr("style","display:block;");
				 $("#import-box").html(result_ary[4]);
				 $("#complete-box").html(result_ary[5]);	
				 //$("#service_type_hidden").attr("value",'');			 
			}
			if(mode == 'COUNTRY_BOX_EDIT')
			{
				var pop_id = $(".active").attr('id');
			 	
				$("#shipper-consignee-box").removeClass('active');
			 	$("#cargo-box").removeClass('active');
			 	$("#import-box").removeClass('active');
			 	$("#complete-box").removeClass('active');
			 	$("#transportation-box").removeClass('active');
			 	 
			 	$("#shipping-box").attr("style","cursor:default");
			 	$("#shipping-box").unbind("click");
				$("#shipping-box").addClass('active');
				$("#shipping-box").html(result_ary[1]);
				$("#transportation-box").html(result_ary[2]); 				 
				$("#shipper-consignee-box").html(result_ary[3]);
				$("#cargo-box").html(result_ary[4]);
				$("#import-box").attr("style","display:block;");
				$("#import-box").html(result_ary[5]);
				$("#complete-box").html(result_ary[6]);	
			}
			if(mode == 'CITY_POSTCODE_EDIT')
			{
			 	// $(".active").removeClass('active');
				
				$("#shipping-box").removeClass('active');
			 	$("#cargo-box").removeClass('active');
			 	$("#import-box").removeClass('active');
			 	$("#complete-box").removeClass('active');
			 	$("#transportation-box").removeClass('active');
			 	 
			 	 $("#shipper-consignee-box").unbind("click");
				 $("#shipper-consignee-box").addClass('active');
				 
				 $("#transportation-box").html(result_ary[1]);
				 $("#shipper-consignee-box").html(result_ary[2]);
				 $("#cargo-box").html(result_ary[3]);
				 $("#import-box").attr("style","display:block;");
				 $("#import-box").html(result_ary[4]);
				 $("#complete-box").html(result_ary[5]);			 
			}
			if(mode == 'CUSTOM_CLEARANCE_EDIT')
			{
				 //$(".active").removeClass('active');
				
				$("#shipping-box").removeClass('active');
			 	$("#cargo-box").removeClass('active');
			 	$("#shipper-consignee-box").removeClass('active');
			 	$("#complete-box").removeClass('active');
			 	$("#transportation-box").removeClass('active');
			 	
				 $("#import-box").unbind("click");
				 $("#import-box").addClass('active');
			 	
				 $("#transportation-box").html(result_ary[1]);
				 $("#shipper-consignee-box").html(result_ary[2]);
				 $("#cargo-box").html(result_ary[3]);
				 $("#import-box").html(result_ary[4]);
				 $("#complete-box").html(result_ary[5]);
			}		
			if(mode == 'CARGO_TIMING_EDIT')
			{
				 //$(".active").removeClass('active');
				
				$("#shipping-box").removeClass('active');
			 	$("#import-box").removeClass('active');
			 	$("#shipper-consignee-box").removeClass('active');
			 	$("#complete-box").removeClass('active');
			 	$("#transportation-box").removeClass('active');
			 	
				 $("#cargo-box").unbind("click");
				 $("#cargo-box").addClass('active');
			 	
				 $("#transportation-box").html(result_ary[1]);
				 $("#shipper-consignee-box").html(result_ary[2]);
				 $("#cargo-box").html(result_ary[3]);
				 $("#import-box").attr("style","display:block;");
				 $("#import-box").html(result_ary[4]);
				 $("#complete-box").html(result_ary[5]);		
			}				
			if(mode == 'CARGO_DETAILS_EDIT')
			{
				 //$(".active").removeClass('active');
				$("#shipping-box").removeClass('active');
			 	$("#import-box").removeClass('active');
			 	$("#shipper-consignee-box").removeClass('active');
			 	$("#cargo-box").removeClass('active');
			 	$("#transportation-box").removeClass('active');
			 	
				 $("#complete-box").unbind("click");
				 $("#complete-box").addClass('active');
			 	
				 $("#transportation-box").html(result_ary[1]);
				 $("#shipper-consignee-box").html(result_ary[2]);
				 $("#cargo-box").html(result_ary[3]);
				 $("#import-box").attr("style","display:block;");
				 $("#import-box").html(result_ary[4]);
				 $("#complete-box").html(result_ary[5]);	
				 
				 removePopupScrollClass("all_available_service");
			}
		}
		else if(result_ary[0]=='POPUP')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		}
		else if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		}
    });
}

function display_requirement_edit_box_popup(mode,booking_key)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",{booking_key:booking_key,mode:mode},function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{			
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');	 
			addPopupScrollClass('all_available_service');
		}			
    });
}
function display_description_text(idTimingType,booking_key)
{
	var dtTiming = $("#datepicker1_requirement").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",{booking_key:booking_key,idTimingType:idTimingType,mode:'DISPLAY_TIMING_DESCRIPTION',dtTiming:dtTiming},function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#cargo-box").html(result_ary[1]);	
			$('#cargo-box').attr('style','display:block;');	 	
			$('#cargo_timing_description').attr('style','display:inline-block;');	 
			if(idTimingType==1)
			{
				 $("#szTiming_origin").attr('style','background-position: 0px -50px;');
			}
			else
			{
				 $("#szTiming_destination").attr('style','background-position: 0px -50px;');
			}
		}	
    });
}
function back_to_city_post_code_box(id,szOriginCityPostcode,szDestinationCityPostcode)
{
	
	if(szOriginCityPostcode !='')
	{
		$('#szOriginCityPostcode').attr('value',szOriginCityPostcode);
	}
	
	if(id=='szDestinationCityPostcode' && szDestinationCityPostcode !='')
	{
		$('#szDestinationCityPostcode').attr('value',szDestinationCityPostcode);
	}
	
	$('#all_available_service').attr('style','display:none;');
	$('#'+id).focus().selectionEnd;
}

function enable_cargo_exceed_popup(idUser,page_url,booking_key,mode,id)
{	
	var user_id = parseInt(idUser);
	if(user_id>0)
	{
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		if(isValidEmail(szCustomerEmail))
		{
			var check_notify = $('#iSendNotification_popup').attr('checked');
			if(check_notify)
			{
				if(mode=='DC')
				{
					$('#iSendNotification_popup').attr('checked','checked');
					$('#notify_me_button').removeAttr('onclick');
					$("#notify_me_button").unbind("click");
					
					$("#notify_me_button").click(function(){ send_danger_cargo_email_forwarder(booking_key) });
					$('#notify_me_button').attr('class','orange-button1');
				}
				else
				{
					$('#notify_me_button').removeAttr('onclick');
					$("#notify_me_button").unbind("click");
					
					$("#notify_me_button").click(function(){ send_quotation_to_forwarder(booking_key) });
					$('#notify_me_button').attr('class','orange-button1');
				}
				$('#szCustomerEmail_popup_error').attr('style','display:none;');
			}
			else
			{
				$("#notify_me_button").unbind("click");
				$('#notify_me_button').attr('class','gray-button1');
			}
		}
		else
		{
			$('#szCustomerEmail_popup_error').html("!");
			$('#szCustomerEmail_popup_error').attr('style','display:inline-block;');
			$("#notify_me_button").unbind("click");
			
			$('#notify_me_button').attr('class','gray-button1');
			$('#notify_me_button').removeAttr('onclick');
		}
	}
	else
	{
		var szCustomerName = $('#szCustomerName_popup').attr('value');
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		var check_flag = $('#iSendNotification_popup').attr('checked');
			
		if(szCustomerName=='' && id=='szCustomerName_popup' )
		{
			$('#szCustomerName_popup_error').attr('style','display:inline-block;');
			$('#szCustomerName_popup_error').html("!");
		}
		else if(szCustomerName!='')
		{
			$('#szCustomerName_popup_error').attr('style','display:none;');
		}
		
		if(!isValidEmail(szCustomerEmail) && id=='szCustomerEmail_popup')
		{
			$('#szCustomerEmail_popup_error').attr('style','display:inline-block;');
			$('#szCustomerEmail_popup_error').html("!");
		}
		else if(isValidEmail(szCustomerEmail))
		{
			$('#szCustomerEmail_popup_error').attr('style','display:none;');
		}
		
		if(!check_flag && id=='iSendNotification_popup')
		{
			$("#notify_me_button").unbind("click");
			$('#notify_me_button').removeAttr('onclick');
			$('#notify_me_button').attr('class','gray-button1');
		}
			
		if(isValidEmail(szCustomerEmail) && szCustomerName!='')
		{
			var check_notify = $('#iSendNotification_popup').attr('checked');
			if(check_notify)
			{
				if(mode=='DC')
				{
					$('#notify_me_button').removeAttr('onclick');
					$("#notify_me_button").unbind("click");
					
					$("#notify_me_button").click(function(){ send_danger_cargo_email_forwarder(booking_key) });
					$('#notify_me_button').attr('class','orange-button1');
				}
				else
				{
					$('#notify_me_button').removeAttr('onclick');
					$("#notify_me_button").unbind("click");
					
					$("#notify_me_button").click(function(){ send_quotation_to_forwarder(booking_key) });
					$('#notify_me_button').attr('class','orange-button1');
				}
			}
			else
			{
				$('#notify_me_button').removeAttr('onclick');
				$("#notify_me_button").unbind("click");
				$('#notify_me_button').attr('class','gray-button1');
			}
		}
		else
		{
			$('#notify_me_button').removeAttr('onclick');
			$("#notify_me_button").unbind("click");
			$('#notify_me_button').attr('class','gray-button1');
		}
	}
}
function enable_rk_popup_button(send_notification_flag,mode)
{		
	var idUser = $("#idUser_bottom_form_hidden").attr('value');
	var user_id = parseInt(user_id);
	
	if(user_id>0)
	{
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		if(isValidEmail(szCustomerEmail) && send_notification_flag)
		{
			var booking_key = $('#szBookingRandomNum_hidden').attr('value');
			$('#notify_me_button').removeAttr('onclick');
			$("#notify_me_button").unbind("click");
			
			if(mode=='DC')
			{
				$('#iSendNotification_popup').attr('checked','checked');
				$('#notify_me_button').removeAttr('onclick');
				$("#notify_me_button").unbind("click");
				
				$("#notify_me_button").click(function(){ send_danger_cargo_email_forwarder(booking_key) });
				$('#notify_me_button').attr('class','orange-button1');
			}
			else
			{
				$('#notify_me_button').removeAttr('onclick');
				$("#notify_me_button").unbind("click");
				
				$("#notify_me_button").click(function(){ send_quotation_to_forwarder(booking_key) });
				$('#notify_me_button').attr('class','orange-button1');
			}
		}
		else
		{
			$('#szCustomerEmail_popup_error').html("!");
			$('#szCustomerEmail_popup_error').attr('style','display:inline-block;');
			$("#notify_me_button").unbind("click");
			
			$('#notify_me_button').attr('class','gray-button1');
			$('#notify_me_button').removeAttr('onclick');
		}
	}
	else
	{
		var szCustomerName = $('#szCustomerName_popup').attr('value');
		var szCustomerEmail = $('#szCustomerEmail_popup').attr('value');
		
		if(isValidEmail(szCustomerEmail) && szCustomerName!='' && send_notification_flag)
		{
			var booking_key = $('#szBookingRandomNum_hidden').attr('value');
			if(mode=='DC')
			{
				$('#iSendNotification_popup').attr('checked','checked');
				$('#notify_me_button').removeAttr('onclick');
				$("#notify_me_button").unbind("click");
				
				$("#notify_me_button").click(function(){ send_danger_cargo_email_forwarder(booking_key) });
				$('#notify_me_button').attr('class','orange-button1');
			}
			else
			{
				$('#notify_me_button').removeAttr('onclick');
				$("#notify_me_button").unbind("click");
				
				$("#notify_me_button").click(function(){ send_quotation_to_forwarder(booking_key) });
				$('#notify_me_button').attr('class','orange-button1');
			}
		}
		else
		{
			$('#notify_me_button').removeAttr('onclick');
			$("#notify_me_button").unbind("click");
			$('#notify_me_button').attr('class','gray-button1');
		}
	}
}
function send_quotation_to_forwarder(booking_key)
{
	$("#notify_me_button").unbind("click");
	$('#notify_me_button').attr('class','gray-button1');
	
	$("#go_back_change_cargo_button").removeAttr("onclick");
	$('#go_back_change_cargo_button').attr('class','gray-button1');
	//$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",$("#cargo_exceed_form").serialize(),function(result){
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');	 
			addPopupScrollClass('all_available_service');
			//$("#loader").attr('style','display:none;');
		}	
      });
}
function send_danger_cargo_email_forwarder(booking_key)
{
	$("#notify_me_button").unbind("click");
	$('#notify_me_button').attr('class','gray-button1');
	
	$("#go_back_change_cargo_button").removeAttr("onclick");
	$('#go_back_change_cargo_button').attr('class','gray-button1');
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",$("#dangerous_cargo_form").serialize(),function(result){
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');	 
			addPopupScrollClass('all_available_service');
			//$("#loader").attr('style','display:none;');
		}	
      });
}
function open_do_not_know_popup(mode,booking_key,iOriginDestination)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",{mode:mode,booking_key:booking_key,iOriginDestination:iOriginDestination},function(result){
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');	 	 
			addPopupScrollClass('all_available_service');
		}	
      });
}
function press_enter_key_add_postcode_string(kEvent,mode)
{
	if(kEvent.keyCode==13)
	{
		add_postcode_string(mode);
	}
}
function change_service_type_image(idServiceType,load_danish)
{
	var image_name = getServiceTypeFilename(idServiceType,load_danish);
	var image_path = __JS_ONLY_SITE_BASE__ + "/images/" + image_name ;
	
	$("#search_service_image").attr('src',image_path);
	var html_result = "<img id='search_service_image' src='"+image_path+"'>";
	$("#service_type_image_div").html(html_result);
	$('#service_type_image_div').attr('style','display:block;');
	
	/*
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",{mode:'CHANGE_SERVICE_TYPE_IMAGE',id:idServiceType},function(result){
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#service_type_image_div").html(result_ary[1]);
			$('#service_type_image_div').attr('style','display:block;');	 	 	 
		}	
   });
   */
}

function getServiceTypeFilename($idServiceType,load_danish)
{
	var $szImageInitial = "search-service";
	if(load_danish==1)
	{
		$szImageInitial += "-da";
	}
	var $szImageName = '';
	if($idServiceType == 1)	//DTD
	{
		$szImageName += $szImageInitial+"-DTD.png";
	}
	else if($idServiceType == 5)	//DTD
	{
		$szImageName += $szImageInitial+"-DTP.png";
	}
	else if($idServiceType == 2)	//DTD
	{
		$szImageName += $szImageInitial+"-DTW.png";
	}
	else if($idServiceType == 4)	//DTD
	{
		$szImageName += $szImageInitial+"-WTW.png";
	}
	else if($idServiceType == 6)	//DTD
	{
		$szImageName += $szImageInitial+"-WTP.png";
	}
	else if($idServiceType == 3)	//DTD
	{
		$szImageName += $szImageInitial+"-WTD.png";
	}
	else if($idServiceType == 7)	//DTD
	{
		$szImageName += $szImageInitial+"-PTP.png";
	}
	else if($idServiceType == 8)	//DTD
	{
		$szImageName += $szImageInitial+"-PTD.png";
	}
	else if($idServiceType == 9)	//DTD
	{
		$szImageName += $szImageInitial+"-PTW.png";
	}
	return $szImageName;
}

function default_cursor(idUser)
{
	var user_id = parseInt(idUser);
	if(user_id>0)
	{
		//window.setTimeout(function (){$("#szCustomerEmail_popup").focus()},100);
	}
	else
	{
		window.setTimeout(function (){$("#szCustomerName_popup").focus()},100);
	}
}
function recalculate_pricing(page_url,szBookingRandomNum,type)
{
	if(szBookingRandomNum =='')
	{
		var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
	}
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{type:type,page_url:page_url,mode:'RECALCULATE_PRICING',szBookingRandomNum:szBookingRandomNum},function(result){
		
		result_ary = result.split('||||');
		if(result_ary[0]=='REDIRECT')
		{
			redirect_url(result_ary[1]);
		}
		else
		{
			$("#change_price_div").html(result_ary[1]);
			$("#change_price_div").attr('style','display:block');
			addPopupScrollClass('change_price_div');
		}
	});
}
function handleBackAction (arguments) {

  //IE hack
  var isIE = (function() {
     var div = document.createElement('div');
     div.innerHTML = '<!--[if IE]><i></i><![endif]-->';
     return (div.getElementsByTagName('i').length === 1);
  }());
  
  if(isIE){
    window.history.forward();
    return;
  }

  var COOKIE_BASE = "ck_";
  var isBackPressed = false;
  
  if(arguments){
    var handler = arguments.handler;
  }
  return {
    isBackPressed: function() {
      return isBackPressed;
    }
  };
}

function preload(arrayOfImages) 
{
    $(arrayOfImages).each(function(){
        $('<img/>')[0].src = this;
        // Alternatively you could use:
        // (new Image()).src = this;
    });
}
function searchContentSubmit(default_text)
{
	blank_me('searchContent',default_text);
	var content = $('#searchContent').val();
	if(content!='' && content !='Type word or phrase')
	{
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_searchExplanation.php",{searchField:content},function(result){
		var result_ary = result.split('|||||');
		console.log(" me called "+content);
			if(jQuery.trim(result_ary[0])=='ERROR')
			{
				
			}
			else
			{
				$('#showExplainLeftNav').html(result_ary['0']);
			    $('.hsbody-2-right').html(result_ary['1']);
			}
		
		});
	}
	else
	{
		show_me('searchContent',default_text);
	}
}

function showPage(page_num)
{	
	var szOriginDestination = $('#szOriginDestination').val();
	var booking_key = $('#szBookingRandomNum_hidden').val();
	var szCityName = $('#szCityName').val();
	var szRegion1 = $('#szRegion1').val();
	$("#boxscroll2").niceScroll().hide();
	
	input_val = {page_num:page_num,booking_key:booking_key,mode:'MULTI_PARTIAL_EMPTY_POSTCODE_NEXT_PAGE',szCityName:szCityName,szOriginDestination:szOriginDestination,szRegion1:szRegion1};
		
	$("#submit_button").unbind("click");
	$('#submit_button').attr('class','gray-button1');
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",input_val,function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#data_container").html(result_ary[1]);
			
			$("#boxscroll2").niceScroll().hide();
			$("#boxscroll2").niceScroll({
				touchbehavior:false,
				cursorcolor:"#17375E",
				cursoropacitymax:0.7,
				cursorwidth:7,
				autohidemode:false
			}).show();
		}
		else if(result_ary[0]=='POPUP')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		}
		else if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
			addPopupScrollClass('all_available_service');
		}
    });
}
function blank_me_comment(id,val,val2)
{	
	var inputFeildValue = $("#"+id).attr("value");
	if(inputFeildValue == val || (inputFeildValue == val2 && val2!='') )
	{
		$("#"+id).attr("value","");
		$("#"+id).attr("style","width:400px;"); 
	}
}

function reloadCargoDetail(booking_key)
{	
	input_val = {booking_key:booking_key,mode:'RELOAD_CARGO_DETAILS'};
		
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateBooking.php",input_val,function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#complete-box").html(result_ary[1]);
			$('#all_available_service').attr('style','display:none;');
		}
		else if(result_ary[0]=='POPUP')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
		}
		else if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
		{
			$("#all_available_service").html(result_ary[1]);
			$('#all_available_service').attr('style','display:block;');
		}
    });
}

function update_user_currency(page_url)
{
		
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateHeader.php",$("#currency_popup_form").serialize(),function(result){		     
       result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$('#Transporteca_popup').attr('style','display:none;');
			
			var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value');
			if(szBookingRandomNum!='' && szBookingRandomNum!='undefined')
			{
    			var page_url_1 = page_url + "/" + szBookingRandomNum + "/";
    		}
    		else
    		{
    			var page_url_1 = page_url ;
    		}
        	redirect_url(page_url_1);
		}
		else if(jQuery.trim(result_ary[0])=='REDIRECT')
		{
			redirect_url(page_url);
		}
    });
}
function download_upload_service_invoice_pdf(id,user)
{
	//$.get(__JS_ONLY_SITE_BASE__+"/html2pdf/transferDetails.php",{id:id,user:user},function(result){
	    window.open ( __JS_ONLY_SITE_BASE__+"/uploadServiceInvoice/"+user+"/"+id+"/");
	   // window.open ( __JS_ONLY_SITE_BASE__+"/html2pdf/"+result);     
	//});
	//.success(function(result) { $.post(__JS_ONLY_SITE_BASE__+"/html2pdf/transferDetails.php",{flag:"DELETE",mode:result});});
}
function submitDetailsSearch(event,default_text)
{
	var data = $('#searchContent').val();
	if(data != 'Type word or phrase' )
	{
		$('#searchContent').css({'font-style':'','color':'black'});
	}
	if(data == 'Type word or phrase'  || data.length == 0)
	{
		$('#searchContent').css({'font-style':'','color':'black'});
	}
	if(event.keyCode == 13)
	{
		searchContentSubmit(default_text);
	}
}

function back_to_booking(booking_key,page_url)
{
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'BACK_TO_BOOKING',booking_key:booking_key,page_url:page_url},function(result){
		res_ary=result.split("||||") ;
			if(res_ary[0]=='SUCCESS')
			{
				if(res_ary[1]!='')
				{
					redirect_url(res_ary[1]);
				}
				else
				{
					redirect_url(page_url);
				}
			}
			else if(res_ary[0]=='POPUP')
			{
				$("#notification_popup").html(res_ary[1]);
				$('#notification_popup').attr('style','display:block;');
				
			}	
		});
}

function display_confirmation_popup(page_url)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateHeader.php",{page_url:page_url,mode:'SHOW_CONFIRMATION'},function(result){
		$("#Transporteca_popup").html(result);
	});
}

function change_next_step_button_html(inner_text,button_id)
{
	var orange_button_text = $('.orange-button1').html();
	var next_step = $('#next_step_text_bottom').attr('value');
	console.log(" next st "+next_step+" butn txt "+orange_button_text);
	var inner_html = "<span>"+next_step+" <img src='"+__JS_ONLY_SITE_BASE__+"/images/325_next.gif' width='15' style='position: absolute;right:8px;top: 6px;' align='right'></span>";
	$('.orange-button1').html(inner_html);
}

function change_next_step_button_default_html(button_id,inner_text)
{
	var next_step = $('#next_step_text_bottom').attr('value');
	//console.log(" next st "+next_step);
	var inner_html = "<span>"+next_step+"</span>";
	$('.orange-button1').html(inner_html);
}

function checkPageRedirectionBulkService_feedback(path)
{		
	$.post(__JS_ONLY_SITE_BASE__+"/customerFeedback.php",{flag:'LEAVE_PAGE',path:path},function(result){
			//check = true;
			$('#ajaxLogin').css('display','block');
			$('#ajaxLogin').html(result);
		});
}

function close_feedback_popup()
{
	$('#ajaxLogin').css('display','none');
}

function redirect_page_customer_feedback(path)
{
	$('#ajaxLogin').css('display','none');
	window.location.href=path;
}

function enable_feedback_submit_button(value)
{
	$("#submit_feedback_button").unbind("click");
	$("#submit_feedback_button").click(function(){submit_feedback_form()});
	$("#submit_feedback_button").attr('style',"opacity:1");
}
function enable_feedback_submit_button_new(value)
{
	$("#submit_feedback_button").unbind("click");
	$("#submit_feedback_button").click(function(){submit_feedback_form_new()});
	$("#submit_feedback_button").attr('style',"opacity:1");
}

function submit_feedback_form()
{
	$("#submit_feedback_button").unbind("click");
	$("#submit_feedback_button").attr('style',"opacity:0.4");
	var value=$("#customerFeedBack").serialize();
	var newvalue=value+'&flag=submitFeedback'; 
	$.post(__JS_ONLY_SITE_BASE__+"/customerFeedback.php",newvalue,function(result){
		$('#ajaxLogin').html(result);	
		});
}

function submit_feedback_form_new()
{ 
	$("#submit_feedback_button").unbind("click");
	$("#submit_feedback_button").attr('style',"opacity:0.4");
	var value=$("#customerFeedBack").serialize();
	var newvalue=value+'&flag=submitFeedback';
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php?mode=CUSTOMER_FEEDBACK",newvalue,function(result){
	
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#booking-feedback-container-div").html(result_ary[1]); 	
			$("#header-search").attr('style','top: 0px; bottom: auto; display: block;');  
		}	 
		else
		{
			$("#booking-feedback-container-div").html(result_ary[1]); 	 
		}
	});
}

function close_otherTip()
{
	//document.getElementById("fileUpload").focus();
	
	var ctrl = document.getElementById("idForwarder");
    if (ctrl != null && ctrl.value == '') {
        ctrl.focus();
    }
	//document.getElementById("updateRegistComapnyForm").elements["idForwarder"].focus();
	$("#display_name").attr("style","display:none");
	$("#general_email").attr("style","display:none");
	$("#terms_standard").attr("style","display:none");
	$("#name").attr("style","display:none");
	$("#add").attr("style","display:none");
	$("#add2").attr("style","display:none");
	$("#add3").attr("style","display:none");
	$("#postcode").attr("style","display:none");
	$("#state").attr("style","display:none");
	$("#phone").attr("style","display:none");
	$("#cmp").attr("style","display:none");
	$("#reg").attr("style","display:none");
	
}

function hide_invalid_booking_popup()
{
	$("#invalid_booking").attr("style","display:none");
}

function close_invite_confirm_popup()
{
	$("#show_invite_msg").attr("style","display:none");
}

function send_email_non_registered_user(szEmail)
{
	$("#send_non_register_user").attr("style","opacity:0.4;");
	$("#send_non_register_user").attr("onclick","");
	
	$("#cancel_button").attr("style","opacity:0.4;");
	$("#cancel_button").attr("onclick","");
	
	$.post(__JS_ONLY_SITE_BASE__+"/mappedUsers.php",{szEmail:szEmail,flag:'NONREGISTERUSER'},function(result){
	$("#map_user_account").html(result);});
}

jQuery.base64 = ( function( $ ) {
  
  var _PADCHAR = "=",
    _ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
    _VERSION = "1.0";

  function _getbyte64( s, i ) {
    // This is oddly fast, except on Chrome/V8.
    // Minimal or no improvement in performance by using a
    // object with properties mapping chars to value (eg. 'A': 0)
    var idx = _ALPHA.indexOf( s.charAt( i ) );
    if ( idx === -1 ) {
      throw "Cannot decode base64";
    }
    return idx;
  }
  
  
  function _decode( s ) {
    var pads = 0,
      i,
      b10,
      imax = s.length,
      x = [];
    s = String( s );
    
    if ( imax === 0 ) {
      return s;
    }
    if ( imax % 4 !== 0 ) {
      throw "Cannot decode base64";
    }
    if ( s.charAt( imax - 1 ) === _PADCHAR ) {
      pads = 1;
      if ( s.charAt( imax - 2 ) === _PADCHAR ) {
        pads = 2;
      }
      // either way, we want to ignore this last block
      imax -= 4;
    }
    for ( i = 0; i < imax; i += 4 ) {
      b10 = ( _getbyte64( s, i ) << 18 ) | ( _getbyte64( s, i + 1 ) << 12 ) | ( _getbyte64( s, i + 2 ) << 6 ) | _getbyte64( s, i + 3 );
      x.push( String.fromCharCode( b10 >> 16, ( b10 >> 8 ) & 0xff, b10 & 0xff ) );
    }
    switch ( pads ) {
      case 1:
        b10 = ( _getbyte64( s, i ) << 18 ) | ( _getbyte64( s, i + 1 ) << 12 ) | ( _getbyte64( s, i + 2 ) << 6 );
        x.push( String.fromCharCode( b10 >> 16, ( b10 >> 8 ) & 0xff ) );
        break;
      case 2:
        b10 = ( _getbyte64( s, i ) << 18) | ( _getbyte64( s, i + 1 ) << 12 );
        x.push( String.fromCharCode( b10 >> 16 ) );
        break;
    }
    return x.join( "" );
  }
  
  
  function _getbyte( s, i ) {
    var x = s.charCodeAt( i );
    if ( x > 255 ) {
      throw "INVALID_CHARACTER_ERR: DOM Exception 5";
    }
    
    return x;
  }

  function _encode( s ) {
    if ( arguments.length !== 1 ) {
      throw "SyntaxError: exactly one argument required";
    }
    s = String( s );
    var i,
      b10,
      x = [],
      imax = s.length - s.length % 3;
    if ( s.length === 0 ) {
      return s;
    }
    for ( i = 0; i < imax; i += 3 ) {
      b10 = ( _getbyte( s, i ) << 16 ) | ( _getbyte( s, i + 1 ) << 8 ) | _getbyte( s, i + 2 );
      x.push( _ALPHA.charAt( b10 >> 18 ) );
      x.push( _ALPHA.charAt( ( b10 >> 12 ) & 0x3F ) );
      x.push( _ALPHA.charAt( ( b10 >> 6 ) & 0x3f ) );
      x.push( _ALPHA.charAt( b10 & 0x3f ) );
    }
    switch ( s.length - imax ) {
      case 1:
        b10 = _getbyte( s, i ) << 16;
        x.push( _ALPHA.charAt( b10 >> 18 ) + _ALPHA.charAt( ( b10 >> 12 ) & 0x3F ) + _PADCHAR + _PADCHAR );
        break;
      case 2:
        b10 = ( _getbyte( s, i ) << 16 ) | ( _getbyte( s, i + 1 ) << 8 );
        x.push( _ALPHA.charAt( b10 >> 18 ) + _ALPHA.charAt( ( b10 >> 12 ) & 0x3F ) + _ALPHA.charAt( ( b10 >> 6 ) & 0x3f ) + _PADCHAR );
        break;
    }
    return x.join( "" );
  }

  return {
    decode: _decode,
    encode: _encode,
    VERSION: _VERSION
  };
      
}( jQuery ) );

function encode_string(id,id2,id3,id4)
{
   var input_string='';
   var encoded_strs='';
   
   input_string=$("#"+id).attr('value');
   url_enc=encodeURIComponent(input_string);
   encoded_strs=$.base64.encode(url_enc);
   $("#"+id2).attr('value',''+encoded_strs+'');
   
   if(id3!="" && id3!=undefined)
   {
	   var input_string='';
	   var encoded_strs='';
	   
	   input_string=$("#"+id3).attr('value');
	   url_enc=encodeURIComponent(input_string);
	   encoded_strs=$.base64.encode(url_enc);
	   $("#"+id4).attr('value',''+encoded_strs+'');
   }
   //tinyMCE.get(id).setContent(encoded_strs);
   

   return encoded_strs;
}
function decode_string()
{
   var input_string='';
   var encoded_strs='';
   input_string=tinyMCE.get('pagedescription').getContent();
   encoded_strs=$.base64.decode(input_string);
   tinyMCE.activeEditor.setContent(encoded_strs);   
   return $.base64.decode(input_string);
}

function copyToClipboardCrossbrowser(feild_id) 
{           
    s = document.getElementById(feild_id).value;     
    
    if( window.clipboardData && clipboardData.setData )
    {
    	document.getElementById(feild_id).select();
    	window.clipboardData.setData('Text',str);
    }           
    else
    {
    	document.getElementById(feild_id).select();
    	/*
    	unsafeWindow.netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
    	const clipboardHelper = Components.classes
    	      ["@mozilla.org/widget/clipboardhelper;1"].
    	      getService(Components.interfaces.nsIClipboardHelper);
    	clipboardHelper.copyString(tc);
    	*/
    }
    
}

function update_rating_div(idBooking)
{
	var divid="rating_"+idBooking;
	$("#"+divid).attr('style','display:none;');
	$("#"+divid).html('');
	var div_id="rating_div_updated_"+idBooking;
	var div_id_link="rating_div_link_"+idBooking;
	$.post(__JS_ONLY_SITE_BASE__+"/bookingRating.php",{idBooking:idBooking,showFlag:'rating_update_link'},function(result){
	$("#"+div_id).html('');
	$("#"+div_id_link).html(result);
	});
}

function closePopupUpdatedService(div_id)
{	
	$('#'+div_id).attr('style','display:none;');	
	removePopupScrollClass(div_id);
}

function serviceFoundPagination(page)
{
	$("#boxscroll2").niceScroll().hide();
	var szBookingRandomNum=$("#szBookingRandomNum").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_showAllAvailableService.php",{szBookingRandomNum:szBookingRandomNum,page:page},function(result){
		$("#all_available_service").html(result);		
		$("#boxscroll2").niceScroll({touchbehavior:false,cursorcolor:"#17375E",cursoropacitymax:0.7,cursorwidth:7,autohidemode:false}).show();
	});
}

function change_language_redirect(szUri,iLanguage)
{
	$("#loader").attr('style','block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_showAllAvailableService.php",{mode:'CHANGE_LANGUAGE_REDIRECT',szUri:szUri,iLanguage:iLanguage},function(result){
		redirect_url(result);
		$("#loader").attr('style','none;');
	});
}
function encode_string_msg(input_string)
{
   //var input_string='';
   var encoded_strs='';
   
   ///input_string=$("#"+id).attr('value');
   url_enc=encodeURIComponent(input_string);
   encoded_strs=$.base64.encode(url_enc);
   //alert(encoded_strs);
   //$("textarea#"+id).attr('value',''+encoded_strs+'');
        

   return encoded_strs;
}


function change_service_type_image_new(idServiceType,load_danish)
{

	if(jQuery("#szPickBox").is(":checked"))
	{
		var idServiceType=idServiceType;
	}
	else
	{
		var idServiceType=jQuery("#dServiceType").val();
	}
	var image_name = getServiceTypeFilename(idServiceType,load_danish);
	var image_path = __JS_ONLY_SITE_BASE__ + "/images/" + image_name ;
	
	
	$("#search_service_image").attr('src',image_path);
	var html_result = "<img id='search_service_image' src='"+image_path+"'>";
	$("#service_type_image_div").html(html_result);
	$('#service_type_image_div').attr('style','display:block;');
	
	$("#szTransportation").val(idServiceType);
	
	/*
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",{mode:'CHANGE_SERVICE_TYPE_IMAGE',id:idServiceType},function(result){
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#service_type_image_div").html(result_ary[1]);
			$('#service_type_image_div').attr('style','display:block;');	 	 	 
		}	
   });
   */
}

function checkFromAddress_backup(szValue,flag,idUser,SzPageUrl,id_suffix,iServiceFlag)
{
    if(szValue!='')
    {
        if(flag=='FROM_COUNTRY')
        {
            $("#iCheckedForFromField").attr('value','1');
        }
        else
        {
            $("#iCheckedToFromField").attr('value','1'); 
        }	 
        var iBookingQuote = $("#iBookingQuote").val(); 

        $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'CHECK_COUNTRY_VALIDITION_OF_SIMPLE_SEARCH',szValue:szValue,flag:flag,iBookingQuote:iBookingQuote},function(result){ 
            var result_ary = result.split("||||"); 
            if(flag=='FROM_COUNTRY')
            {
                $("#iCheckedForFromField").attr('value','0');
            }
            else
            {
                $("#iCheckedToFromField").attr('value','0');
            }

            if(jQuery.trim(result_ary[0])=='ERROR')
            {
                var formId="landing_page_form"+id_suffix;
                if(flag=='FROM_COUNTRY')
                {
                    var input_field_id="szOriginCountryStr"+id_suffix+"_container";
                    $("#"+input_field_id).addClass('red_border');
                    displayFormFieldIsRequired(formId, input_field_id, result_ary[1],1,1);
                }
                else if(flag=='TO_COUNTRY')
                {
                    var input_field_id="szDestinationCountryStr"+id_suffix+"_container";
                    $("#"+input_field_id).addClass('red_border');
                    displayFormFieldIsRequired(formId, input_field_id, result_ary[1],1,1);
                }
                else if(flag=='VOLUME')
                { 
                    var input_field_id="iVolume"+id_suffix+"_container";
                    $("#"+input_field_id).addClass('red_border');
                    displayFormFieldIsRequired(formId, input_field_id, result_ary[1],1,1);
                }
                else if(flag=='WEIGHT')
                { 
                    var input_field_id="iWeight"+id_suffix+"_container";
                    $("#"+input_field_id).addClass('red_border');
                    displayFormFieldIsRequired(formId, input_field_id, result_ary[1],2,1);
                } 
                if(iServiceFlag)
                {
                    $("#update_search"+id_suffix).unbind("click");
                    $("#update_search"+id_suffix).removeAttr("onclick");
                }
                else
                {
                    $("#search_listing"+id_suffix).unbind("click");
                    $("#search_listing"+id_suffix).removeAttr("onclick");
                }
                $("#search-btn-container"+id_suffix).removeClass('btn');
                $("#search-btn-container"+id_suffix).addClass('btn-grey');
            }
            else if(jQuery.trim(result_ary[0])=='SUCCESS')
            {
                if(flag=='FROM_COUNTRY')
                {
                    $("#fromerror").html(result_ary[1]);
                    $("#fromCountryError").attr('value','0');
                    $("#fromerror").attr('style','display:none');
                }
                else if(flag=='TO_COUNTRY')
                {
                    $("#toerror").html(result_ary[1]);
                    $("#toCountryError").attr('value','0');
                    $("#toerror").attr('style','display:none');
                } 
                var fromCountryError=$("#fromCountryError").attr('value');
                var toCountryError=$("#toCountryError").attr('value');

                if(fromCountryError!='1' && toCountryError!='1')
                {
                    $("#search_simple_button").unbind("click");
                    $("#search_simple_button").removeClass('gray-button1');
                    $("#search_simple_button").addClass('button1');
                    $("#search_simple_button").click(function(){validateLandingPageForm('landing_page_form',idUser,SzPageUrl)});
                }
            }
        });
    }
}
function checkFromAddress(szValue,flag,idUser,SzPageUrl,id_suffix,iServiceFlag,iDonotCallAjax)
{
    if(szValue!='')
    {
        if(flag=='FROM_COUNTRY' || flag=='TO_COUNTRY') 
        {
            if(flag=='FROM_COUNTRY')
            {
                $("#iCheckedForFromField").attr('value','1');
            }
            else
            {
                $("#iCheckedToFromField").attr('value','1'); 
            }
            transporteca_geocode_service(szValue,flag,id_suffix,'',iDonotCallAjax);    
        }
        else
        {
            var iBookingQuote = $("#iBookingQuote").val(); 

            $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'CHECK_COUNTRY_VALIDITION_OF_SIMPLE_SEARCH',szValue:szValue,flag:flag,iBookingQuote:iBookingQuote},function(result){ 
                var result_ary = result.split("||||"); 
                if(flag=='FROM_COUNTRY')
                {
                    $("#iCheckedForFromField").attr('value','0');
                }
                else
                {
                    $("#iCheckedToFromField").attr('value','0');
                }

                if(jQuery.trim(result_ary[0])=='ERROR')
                {
                    var formId="landing_page_form"+id_suffix;
                    if(flag=='FROM_COUNTRY')
                    {
                        var input_field_id="szOriginCountryStr"+id_suffix+"_container";
                        $("#"+input_field_id).addClass('red_border');
                        displayFormFieldIsRequired(formId, input_field_id, result_ary[1],1,1);
                    }
                    else if(flag=='TO_COUNTRY')
                    {
                        var input_field_id="szDestinationCountryStr"+id_suffix+"_container";
                        $("#"+input_field_id).addClass('red_border');
                        displayFormFieldIsRequired(formId, input_field_id, result_ary[1],1,1);
                    }
                    else if(flag=='VOLUME')
                    { 
                        var input_field_id="iVolume"+id_suffix+"_container";
                        $("#"+input_field_id).addClass('red_border');
                        displayFormFieldIsRequired(formId, input_field_id, result_ary[1],1,1);
                    }
                    else if(flag=='WEIGHT')
                    { 
                        var input_field_id="iWeight"+id_suffix+"_container";
                        $("#"+input_field_id).addClass('red_border');
                        displayFormFieldIsRequired(formId, input_field_id, result_ary[1],2,1);
                    } 
                    if(iServiceFlag)
                    {
                        $("#update_search"+id_suffix).unbind("click");
                        $("#update_search"+id_suffix).removeAttr("onclick");
                    }
                    else
                    {
                        $("#search_listing"+id_suffix).unbind("click");
                        $("#search_listing"+id_suffix).removeAttr("onclick");
                    }
                    $("#search-btn-container"+id_suffix).removeClass('btn');
                    $("#search-btn-container"+id_suffix).addClass('btn-grey');
                }
                else if(jQuery.trim(result_ary[0])=='SUCCESS')
                {
                    if(flag=='FROM_COUNTRY')
                    {
                        $("#fromerror").html(result_ary[1]);
                        $("#fromCountryError").attr('value','0');
                        $("#fromerror").attr('style','display:none');
                    }
                    else if(flag=='TO_COUNTRY')
                    {
                        $("#toerror").html(result_ary[1]);
                        $("#toCountryError").attr('value','0');
                        $("#toerror").attr('style','display:none');
                    } 
                    var fromCountryError=$("#fromCountryError").attr('value');
                    var toCountryError=$("#toCountryError").attr('value');

                    if(fromCountryError!='1' && toCountryError!='1')
                    {
                        $("#search_simple_button").unbind("click");
                        $("#search_simple_button").removeClass('gray-button1');
                        $("#search_simple_button").addClass('button1');
                        $("#search_simple_button").click(function(){validateLandingPageForm('landing_page_form',idUser,SzPageUrl)});
                    }
                }
            });
        } 
    }
}

function transporteca_check_validation(type,flag,id_suffix,data,iDonotCallAjax)
{  
    console.log("id_suffix"+id_suffix+" Type: "+type);
    if(flag=='FROM_COUNTRY')
    {
        $("#iCheckedForFromField").attr('value','0');
    }
    else
    {
        $("#iCheckedToFromField").attr('value','0');
    } 
    if(type=='ERROR')
    {
        if(flag=='FROM_COUNTRY')
        {
            var input_field_id="szOriginCountryStr"+id_suffix+"_container";
            $("#"+input_field_id).addClass('red_border');  
        }
        else if(flag=='TO_COUNTRY')
        {
            var input_field_id="szDestinationCountryStr"+id_suffix+"_container";
            $("#"+input_field_id).addClass('red_border');  
        }
    }
    else if(type=='SUCCESS')
    {  
        var szCity = '';
        var szState = '';
        var szCountryISO = '';
        var szCountryName = '';
        var ret_ary = new Array();
        ret_ary = processGoogleResponse(data);  
        if(Array.isArray(ret_ary))
        { 
            szCity = ret_ary["szCity"];
            szState = ret_ary["szState"];
            szCountryName = ret_ary["szCountryName"];
            szCountryISO = ret_ary["szCountryISO"];
        }
        console.log("City: "+szCity+" Country: "+szCountryISO);
        if(!szCity.length)
        {
            szCity = szState ;
        } 
                
        if(__GLOBAL_SMALL_COUNTRY_LIST__.length)
        {
            //checking is country is small country;
            if(in_array(szCountryISO,__GLOBAL_SMALL_COUNTRY_LIST__) && !szCity.length)
            {
                szCity = szCountryName;
                console.log("Small Country: "+szCountryISO);
            }
        } 
        console.log("Length---"+szCity.length);
        if(szCity.length)
        {
            callAjaxFlag=true;
            if(flag=='FROM_COUNTRY')
            { 
                $("#fromCountryError").attr('value','0');
                $("#fromerror").attr('style','display:none');
                if($("#iServicePage").length>0)
                {
                    var szOriginOldCountryCode = $("#szOriginCountryCode").attr('value');
                    var szDestinationCountryCode=$("#szDestinationCountryCode").attr('value');
                    var szOriginCountryCode=szCountryISO;
                    $("#szOriginCountryCode").attr('value',szOriginCountryCode);
                    if(szOriginOldCountryCode==szOriginCountryCode)
                    {
                        callAjaxFlag=false;
                    }
                }
            }
            else if(flag=='TO_COUNTRY')
            { 
                $("#toCountryError").attr('value','0');
                $("#toerror").attr('style','display:none');
                
                if($("#iServicePage").length>0)
                {
                    var szDestinationOldCountryCode = $("#szDestinationCountryCode").attr('value');
                    var szOriginCountryCode=$("#szOriginCountryCode").attr('value');
                    var szDestinationCountryCode=szCountryISO;
                    $("#szDestinationCountryCode").attr('value',szDestinationCountryCode);
                    if(szDestinationOldCountryCode==szDestinationCountryCode)
                    {
                        callAjaxFlag=false;
                    }
                }
            }
           // alert(iDonotCallAjax+"iDonotCallAjax");
            if($("#iServicePage").length>0 && callAjaxFlag && (flag=='FROM_COUNTRY' || flag=='TO_COUNTRY') && iDonotCallAjax!='1')
            {
                console.log("szOriginCountryCode:"+szOriginCountryCode);
                console.log("szDestinationCountryCode:"+szDestinationCountryCode);
                
                showhideDropdownFromSearchForm(szOriginCountryCode,szDestinationCountryCode,id_suffix);
            }
        }
        else
        {
            transporteca_check_validation("ERROR",flag,id_suffix);
        }
    }
}

function processGoogleResponse(data)
{
    var szPostcode = '';
    var szCity='';
    var szCountryName='';
    var szCountryISO='';
    var szState='';
    var szPostcode = '';
    
    for(var i=0;i<data.address_components.length;i++) 
    {
        var arrType = data.address_components[i].types;  
        if(arrType[0]=='locality' && arrType[1]=='political')
        {
            szCity = data.address_components[i].long_name;
        }
        else if(arrType[0]=='colloquial_area' && arrType[1]=='locality')
        {
            szCity = data.address_components[i].long_name;
        }
        else if(arrType[0]=='postal_code')
        {
            szPostcode = data.address_components[i].long_name;
        }  
        else if(arrType[0]=='postal_town')
        {
            szCity = data.address_components[i].long_name;
        } 
        else if(!szCity.length && (in_array('sublocality_level_1',arrType) || in_array('sublocality',arrType)))
        {
            szCity = data.address_components[i].long_name;
        }
        else if(arrType[0]=='administrative_area_level_1' && arrType[1]=='political' )
        {
            szState = data.address_components[i].long_name;
        } 
        else if(arrType[0]=='country' && arrType[1]=='political' )
        {
            szCountryISO = data.address_components[i].short_name;
            szCountryName = data.address_components[i].short_name;
        } 
    } 
    var ret_ary = new Array(); 
    ret_ary["szPostcode"] = szPostcode;
    ret_ary["szCity"] = szCity;
    ret_ary["szState"] = szState;
    ret_ary["szCountryISO"] = szCountryISO;
    ret_ary["szCountryName"] = szCountryName; 
    return ret_ary;
}

function transporteca_geocode_service(address,flag,id_suffix,admin_flag,iDonotCallAjax) 
{ 
    if(address!='')
    {
        var szFieldID='';
        if(flag=='FROM_COUNTRY')
        {
            szFieldID = 'szOriginCountryStr';
        }
        else
        {
            szFieldID = 'szDestinationCountryStr';
        } 
    
        url = '//maps.googleapis.com/maps/api/geocode/json?sensor=false&address='+address;
        $.ajax({ 
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data && data.status) 
                {
                    if(data.status == 'OK') 
                    {  
                        if(admin_flag==1)
                        {
                            var ret_ary = new Array();
                            ret_ary = processGoogleResponse(data.results[0]); 
                            prefill_quick_quote_addres(ret_ary,flag);  
                            return "SUCCESS";
                        }
                        else
                        {
                            transporteca_check_validation('SUCCESS',flag,id_suffix,data.results[0],iDonotCallAjax);  
                        } 
                    } 
                    else 
                    {
                        if(admin_flag==1)
                        {  
                            $("#"+szFieldID).addClass('red_border');
                            return 'ERROR';
                        }
                        else
                        {
                            transporteca_check_validation('ERROR',flag,id_suffix,'',iDonotCallAjax);
                        } 
                    }
                } 
                else 
                {
                    if(admin_flag==1)
                    { 
                        $("#"+szFieldID).addClass('red_border');
                        return 'ERROR';
                    }
                    else
                    {
                        transporteca_check_validation('ERROR',flag,id_suffix,'',iDonotCallAjax);
                    }  
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                var fall_back_result = transporteca_geocoder_fallback(address);
                return fall_back_result;
            }
        });
    } 
} 

function transporteca_geocoder_fallback(address,flag) 
{
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'address': address
    }, function(results, status) 
    {
        if(status == google.maps.GeocoderStatus.OK) {
            transporteca_check_validation('SUCCESS',flag,id_suffix,data.results[0]); 
        } else {
          transporteca_check_validation('ERROR',flag,id_suffix);
        }
    });
} 

function isNumberKey(evt){
    
    var charCode = (evt.which) ? evt.which : ''; 
    if(charCode==46 || charCode==44) //44 = ',', 46='.'
    {
        return true;
    }
    else if (charCode > 31 && (charCode < 48 || charCode > 57)) 
    {
        return false;
    }
    return true;
}

function isIntegerKey(evt)
{ 
    var charCode = (evt.which) ? evt.which : ''; 
    if(charCode==46 || charCode==44) //44 = ',', 46='.'
    {
        return false;
    }
    else if (charCode > 31 && (charCode < 48 || charCode > 57)) 
    {
        return false;
    }
    return true;
}
function submitenterKeySimpleSerachForm(SzPageUrl,e,idUser)
{
    var keycode;
    keycode = e.which;
	 
    if(keycode == 13)
    {
        var fromCountryError=$("#fromCountryError").attr('value');
        var toCountryError=$("#toCountryError").attr('value');

        if(fromCountryError!='1' && toCountryError!='1')
        {
            validateLandingPageForm('landing_page_form',idUser,SzPageUrl);
        }
    }
}

function check_form_field_empty_standard_container(formId,inputId,container_id,display_bottom)
{	 
    console.log("ID: "+container_id);
    if(isFormInputElementEmpty(formId,inputId)) 
    {		
       $("#"+container_id).addClass('red_border');
       return false;
    }
    else
    {
    	$("#"+container_id).removeClass('red_border');
    }
}
function fill_data_to_hidden_form(hidden_val)
{
    var iHiddenChanged = $("#iHiddenChanged").val(); 
    //console.log("iHiddenChanged"+iHiddenChanged);
    if(iHiddenChanged==2)
    {
        var szOriginCountryStr = $("#szOriginCountryStr").val();
        var szDestinationCountryStr = $("#szDestinationCountryStr").val();
        var iPickup = $("#iPickup").val();
        var datepicker1_search_services = $("#datepicker1_search_services").val();		
        var iVolume = $("#iVolume").val();
        var iWeight = $("#iWeight").val();	
        var iServiceType = $("#iServiceType").val();	

        var hidden_flag = "_hiden";

        $("#szOriginCountryStr"+hidden_flag).attr('value',szOriginCountryStr);
        $("#szDestinationCountryStr"+hidden_flag).attr('value',szDestinationCountryStr);
        $("#datepicker1_search_services"+hidden_flag).attr('value',datepicker1_search_services);
        $("#iVolume"+hidden_flag).attr('value',iVolume);
        $("#iWeight"+hidden_flag).attr('value',iWeight);
        //$("#iPickup"+hidden_flag).attr('value',iPickup);
        $("#iServiceType"+hidden_flag).attr('value',iServiceType);
        $("#iHiddenChanged").attr('value','1');
        
        var option1 = $("#iPickupDropdownValue_option1").attr("value");
        var option2 = $("#iPickupDropdownValue_option2").attr("value");
        var option3 = $("#iPickupDropdownValue_option3").attr("value");
        //alert(option2+"###"+option1);
        //console.log("iPickupHidden"+iPickup);
        if(iPickup==1)
        {	
            $("#iPickup"+hidden_flag+" option[value='0']").removeAttr('selected', 'selected');
            
            if(option3!='')
            {
                $("#iPickup"+hidden_flag+" option[value='2']").removeAttr('selected', 'selected');
                $("#iPickup"+hidden_flag+" option[value='1']").attr('selected', 'selected');
                $("#selectiPickup_hiden").html(option3); 
            }
            else
            {
                $("#iPickup"+hidden_flag+" option[value='1']").attr('selected', 'selected'); 
                $("#selectiPickup_hiden").html(option2); 
            }
            
        }
        else if(iPickup==2)
        {
            $("#iPickup"+hidden_flag+" option[value='0']").removeAttr('selected', 'selected');
            $("#iPickup"+hidden_flag+" option[value='1']").removeAttr('selected', 'selected'); 
            if(option3!='')
            {
                $("#iPickup"+hidden_flag+" option[value='2']").attr('selected', 'selected');
            }
            $("#selectiPickup_hiden").html(option2);
        }
        else
        {
            $("#iPickup"+hidden_flag+" option[value='1']").removeAttr('selected', 'selected');
            $("#iPickup"+hidden_flag+" option[value='0']").attr('selected', 'selected'); 
            if(option3!='')
            {
                $("#iPickup"+hidden_flag+" option[value='2']").removeAttr('selected', 'selected');
            }
            $("#selectiPickup_hiden").html(option1);
        }
    }
    else
    {
        var hidden_flag = "_hiden";
        var szOriginCountryStr = $("#szOriginCountryStr"+hidden_flag).val();
        var szDestinationCountryStr = $("#szDestinationCountryStr"+hidden_flag).val();
        var iPickup = $("#iPickup"+hidden_flag).val();
        var datepicker1_search_services = $("#datepicker1_search_services"+hidden_flag).val();		
        var iVolume = $("#iVolume"+hidden_flag).val();
        var iWeight = $("#iWeight"+hidden_flag).val();
        var iServiceType = $("#iServiceType"+hidden_flag).val();

        $("#szOriginCountryStr").attr('value',szOriginCountryStr);
        $("#szDestinationCountryStr").attr('value',szDestinationCountryStr);
        $("#datepicker1_search_services").attr('value',datepicker1_search_services);
        $("#iVolume").attr('value',iVolume);
        $("#iWeight").attr('value',iWeight);
        //$("#iPickup").attr('value',iPickup); 
        $("#iServiceType").attr('value',iServiceType); 

        var option1 = $("#iPickupDropdownValue_option1").attr("value");
        var option2 = $("#iPickupDropdownValue_option2").attr("value");
        var option3 = $("#iPickupDropdownValue_option3").attr("value");
        //console.log("iPickup"+iPickup);
        //console.log("option3"+option3);
        if(iPickup==1)
        {	
            $("#iPickup option[value='0']").removeAttr('selected', 'selected');
            
            if(option3!='')
            {   
                $("#iPickup option[value='2']").removeAttr('selected', 'selected'); 
                $("#iPickup option[value='1']").attr('selected', 'selected');
                $("#selectiPickup").html(option3); 
            }
            else
            {
                $("#iPickup option[value='1']").attr('selected', 'selected'); 
                $("#selectiPickup").html(option2); 
            }
            
        }else if(iPickup==2)
        {
            //console.log("iPickup2"+iPickup);
            $("#iPickup option[value='0']").removeAttr('selected', 'selected');
            $("#iPickup option[value='1']").removeAttr('selected', 'selected'); 
            if(option3!='')
            {
                $("#iPickup option[value='2']").attr('selected', 'selected');
            }
            $("#selectiPickup").html(option2); 
        }
        else
        {
            $("#iPickup option[value='1']").removeAttr('selected', 'selected');
            //$("#iPickup option[value='0']").attr('selected', 'selected'); 
            if(option3!='')
            {   
                $("#iPickup option[value='0']").attr('selected', 'selected'); 
                $("#iPickup option[value='2']").removeAttr('selected', 'selected');
                $("#selectiPickup").html(option1); 
            }
            else
            {
                $("#iPickup option[value='0']").attr('selected', 'selected'); 
                $("#selectiPickup").html(option1); 
            }
        }
    }
} 
function validateNewLandingPageForm_v2(formId,idUser,page_url,hidden_flag)
{
    $("#iCalculateOnlyCourier").attr('value','1');
    validateNewLandingPageForm(formId,idUser,page_url,hidden_flag)
}

function validateNewLandingPageForm(formId,idUser,page_url,hidden_flag,search_mini,from_page,iDonotShowSearchNotificationPopup,iConvertRfq,iOfferType)
{	 
    if(search_mini>0)
    {
        $("#search-btn-container_V_"+search_mini).addClass('search-btn-clicked mobile-view');
    }
    else if(hidden_flag==1)
    {
        $("#search-btn-container_hiden").addClass('search-btn-clicked mobile-view');
        var id_suffix="_hiden";
    }
    else
    {
        $("#search-btn-container").addClass('search-btn-clicked mobile-view');
        var id_suffix="";
    } 
    var serialized_form_data = $("#"+formId).serialize();
    serialized_form_data += "&formId="+formId;
    serialized_form_data += "&page_url="+page_url;
    serialized_form_data += "&hidden_flag="+hidden_flag;
    serialized_form_data += "&search_mini="+search_mini;
    serialized_form_data += "&from_page="+from_page;
    serialized_form_data += "&iDonotShowSearchNotificationPopup="+iDonotShowSearchNotificationPopup;
    serialized_form_data += "&iConvertRfq="+iConvertRfq;
    serialized_form_data += "&iOfferType="+iOfferType;
    
    
    var lang = $("#iSiteLanguageFooter_hidden").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php?lang="+lang,serialized_form_data,function(result){		     
         result_ary = result.split("||||"); 
         if(jQuery.trim(result_ary[0])=='ERROR')
         {
            var szErrorString = jQuery.trim(result_ary[1]);
            var szErrorAry = szErrorString.split("$$$$");
            var ary_length = szErrorAry.length ; 

            for(var n=0;n<ary_length;n++) 
            {
                var input_field_Arr = szErrorAry[n].split("+++++");  
                var input_field_id=input_field_Arr[0]; 
                var input_field_id_err=input_field_Arr[1];  

                var errId=input_field_id+"_err";
                var inputId=input_field_Arr[2];
                //alert(input_field_id);
                if(input_field_id!='' && $("#"+input_field_id).length)
                {
                    $("#"+input_field_id).addClass('red_border'); 
                    if(input_field_Arr[1]=='NO_MESSAGE_TO_SHOW')
                    {
                        //We don't display error bubble for cargo fields
                    }
                    else
                    {
                        displayFormFieldIsRequired(formId, input_field_id, input_field_id_err,1);
                    }
                    //$("#"+input_field_id+"_err").html(input_field_id_err);  
                }
            }
            $(".btn").removeClass('search-btn-clicked'); 
            $(".btn").removeClass('mobile-view');  

             var iServiceFlag=$("#iServiceFlag").attr('value');	

             if(iServiceFlag)
            {
                   $("#update_search"+id_suffix).unbind("click");
                   $("#update_search"+id_suffix).removeAttr("onclick");
            }
            else
            {
                   $("#search_listing"+id_suffix).unbind("click");
                   $("#search_listing"+id_suffix).removeAttr("onclick");
            }

           if(hidden_flag==1)
           {
               //alert(#search-btn-container"+id_suffix);
               $("#search-btn-container"+id_suffix).removeClass('search-btn-clicked');
               $("#search-btn-container"+id_suffix).removeClass('mobile-view');
               $("#search-btn-container"+id_suffix).removeClass('btn');
               $("#search-btn-container"+id_suffix).addClass('btn-grey');
           }
           else
           {
               $("#search-btn-container"+id_suffix).removeClass('search-btn-clicked');
               $("#search-btn-container"+id_suffix).removeClass('mobile-view');
               $("#search-btn-container"+id_suffix).removeClass('btn');
               $("#search-btn-container"+id_suffix).addClass('btn-grey');
           }	 
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_LOCAL_SEARCH')
        {
            $(".btn").removeClass('search-btn-clicked');   
            var iServiceFlag=$("#iServiceFlag").attr('value');	 
            if(iServiceFlag)
            {
                $("#update_search"+id_suffix).unbind("click");
                $("#update_search"+id_suffix).removeAttr("onclick");
                $("#update_search"+id_suffix).click(function(){update_service_listing('landing_page_form'+id_suffix,hidden_flag)});
            }
            else
            {
                $("#search_listing"+id_suffix).unbind("click");
                $("#search_listing"+id_suffix).removeAttr("onclick");
                $("#search_listing"+id_suffix).click(function(){validateNewLandingPageForm('landing_page_form'+id_suffix,idUser,page_url,hidden_flag,search_mini)});
            }

            if(hidden_flag==1)
            {
                //alert(#search-btn-container"+id_suffix);
                $("#search-btn-container"+id_suffix).removeClass('search-btn-clicked'); 
            }
            else
            {
                $("#search-btn-container"+id_suffix).removeClass('search-btn-clicked'); 
            }
            $("#regError").attr('style','display:none;');
            $("#ajaxLogin").html(result_ary[1]);
            $("#ajaxLogin").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='NO_SERVICE_POPUP')
        {
            $(".btn").removeClass('search-btn-clicked');
            $(".btn").removeClass('mobile-view');
            $("#all_available_service").attr('style','display:block');
            $("#regError").attr('style','display:none;');
            $("#all_available_service").html(result_ary[1]);
        }         
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $(".btn").removeClass('search-btn-clicked');
            $(".btn").removeClass('mobile-view');
            var szPageLoaction=$("#szPageLoaction").attr('value');
            $("#regError").attr('style','display:none;');
            var user_id = parseInt(idUser);
            if(szPageLoaction==1)
            {
                var szBookingRandomNum = $("#szBookingRandomNum").attr('value');
                var page_url_1 = page_url + "/" + szBookingRandomNum + "/";
                redirect_url(page_url_1);
            }
            else
            {
                if(user_id==0 || isNaN(user_id) || user_id=='NaN')
                {
                    show_currency_popup(formId);
                }
                else
                {  
                    var szBookingRandomNum = $("#szBookingRandomNum").attr('value');
                    var page_url_1 = page_url + "/" + szBookingRandomNum + "/";
                    redirect_url(page_url_1);
                }
            }
        }
        else if(jQuery.trim(result_ary[0])=='REDIRECT')
        { 
           redirect_url(jQuery.trim(result_ary[1]));
        }    
        else if(jQuery.trim(result_ary[0])=='REDIRECT_IFRAME')
        {  
           window.top.location.replace(result_ary[1]);
        }   
        else if(jQuery.trim(result_ary[0])=='LINK_BUILDER')
        {
           $("#link-builder-url-container").html(result_ary[1]);
           $("#link-builder-url-container").attr('style','display:block;');

           $("#service-listing-container").html(result_ary[2]);
           $("#service-listing-container").attr('style','display:block;');
        } 
        else if(jQuery.trim(result_ary[0])=='LINK_BUILDER_NO_RECORD')
        {
           $("#link-builder-url-container").html(result_ary[1]);
           $("#link-builder-url-container").attr('style','display:block;');

           $("#service-listing-container").html(result_ary[2]);
           $("#service-listing-container").attr('style','display:block;');
        } 
    });      
    return false;      
}
function load_booking_detail_field(szBookingRandomNum,language)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php?lang="+language,{operation:'DISPLAY_BOOKING_DETAILS_FIELDS',szBookingRandomNum:szBookingRandomNum},function(result){ 
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#booking_details_container").html(result_ary[1]);  
            var iShow = $("#showBookingFieldsFlag").val();  
            if(iShow==1)
            {
                $("#booking_details_container").attr('style','display:block;');
            }
            else
            {
                $("#booking_details_container").attr('style','display:none;');
            }
        }
        else
        {
            console.log("Error");
        } 
    });
}

function update_service_listing(formId,hidden_flag,selected_button_flag,ignore_notification_popup)
{
	if(selected_button_flag==1)
	{
            $("#search-service-book-now-button").addClass('search-btn-clicked');
            $("#search-service-book-now-button").addClass('mobile-view'); 
	}
	else if(hidden_flag==1)
	{
            $("#search-btn-container_hiden").addClass('search-btn-clicked');
            $("#search-btn-container_hiden").addClass('mobile-view'); 
	}
	else
	{ 
            var iSearchMiniPage =$("#"+formId+" #iSearchMiniPage").attr('value');
            //alert(formId);
            if(parseInt(iSearchMiniPage)==5)
            {
                $("#search-btn-container_V_"+iSearchMiniPage).addClass('search-btn-clicked');
                $("#search-btn-container_V_"+iSearchMiniPage).addClass('mobile-view');
            }
            else
            {
                $("#search-btn-container").addClass('search-btn-clicked');
                $("#search-btn-container").addClass('mobile-view');
            }
	} 
	
	if( $('#booking-details-form-hidden').length > 0 && $('#booking_detail_form').length > 0)
        {
		//save_previous_data();
	}
	
	$("#showBookingFieldsFlag").attr('value','0'); 
        
        $("#additional_service_list_container").attr('style','display:block;');
        $("#service-listing-container").attr('style','display:none;');
        
	$("#booking_details_container").attr('style','display:none;');
        $("#update_cargo_details_container_div").attr('style','display:none;');
	$("#selected-service-listing-container").attr('style','display:none;');
	var szBookingRandomNum = $("#szBookingRandomNum").val(); 
        
        var lang = $("#iSiteLanguageFooter_hidden").val();
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php?operation=UPDATE_SERVICE_LISTING&lang="+lang+"&ignore_notification_popup="+ignore_notification_popup,$("#"+formId).serialize(),function(result){
		 
         var result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {
            $(".btn").removeClass('search-btn-clicked');
            $(".btn").removeClass('mobile-view');

            var szErrorString = jQuery.trim(result_ary[1]);
            var szErrorAry = szErrorString.split("$$$$");
            var ary_length = szErrorAry.length ; 

            for(var n=0;n<ary_length;n++) 
            {
                var input_field_Arr = szErrorAry[n].split("+++++"); 

                var input_field_id=input_field_Arr[0]; 
                var input_field_id_err=input_field_Arr[1];  
                var errId=input_field_id+"_err";
                var inputId=input_field_Arr[2]; 
                if(input_field_id!='' && $("#"+input_field_id).length)
                {
                    $("#"+input_field_id).addClass('red_border');
                    displayFormFieldIsRequired(formId, input_field_id, input_field_id_err,1,1);
                    //$("#"+input_field_id+"_err").html(input_field_id_err);  
                } 
            }
            
            $(".btn").removeClass('search-btn-clicked');
            $(".btn").removeClass('mobile-view'); 

            $("#update_search").unbind("click");
            $("#update_search").removeAttr("onclick");
            $("#search-btn-container").removeClass('btn');
            $("#search-btn-container").addClass('btn-grey');
         } 
         if(jQuery.trim(result_ary[0])=='SUCCESS_LOCAL_SEARCH')
         {
            $(".btn").removeClass('search-btn-clicked');   
            
            $("#update_search").unbind("click");
            $("#update_search").removeAttr("onclick");
            $("#update_search").click(function(){update_service_listing(formId,hidden_flag)}); 
            $("#search-btn-container").removeClass('search-btn-clicked');  
            
            $("#regError").attr('style','display:none;');
            $("#ajaxLogin").html(result_ary[1]);
            $("#ajaxLogin").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='POPUP')
         {
            $("#ajaxLogin").html(result_ary[1]); 
            $("#ajaxLogin").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='NO_SERVICE_POPUP')
         {
            $(".btn").removeClass('search-btn-clicked');    
            $(".btn").removeClass('mobile-view');     	
            $("#all_available_service").html(result_ary[1]);
            $("#all_available_service").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
            $(".btn").removeClass('search-btn-clicked');
            $(".btn").removeClass('mobile-view');
            $("#Transportation_pop").attr('style','display:none;');	
            $("#iVolume").attr('value',result_ary[2]);
            $("#iWeight").attr('value',result_ary[3]);
            $("#szOriginCountryStr").attr('value',result_ary[4]);
            $("#szDestinationCountryStr").attr('value',result_ary[5]);	
            $("#service-listing-container").html(result_ary[1]);         	
            $("#service-listing-container").attr('style','display:block');
            $("#additional_service_list_container").attr('style','display:none');
 
            var lang = $("#iSiteLanguageFooter_hidden").val();
            load_booking_detail_field(szBookingRandomNum,lang);
         }
         else if(jQuery.trim(result_ary[0])=='DISPLAY_QUOTATION_PAGE')
         {
             redirect_url(result_ary[1]);
         }
    });
}

function change_dropdown_value(select_box_id,on_blur)
{
    if(on_blur==1)
    {
        $('#'+select_box_id+' option').each(function() {
           $(this).removeAttr("style"); 
        });
    }
    else
    {
        $('#'+select_box_id+' option').each(function() { 
            $(this).attr("style",'background-color:#F0F0F0;color:#222222;'); 
        });
    }
 }
 
 function change_service_type(value,service_type_PTD,service_type_DTD,service_type_DTP)
 { 
    if(value==1)
    {
        $("#idServiceType").attr('value',service_type_DTD);
        $("#idServiceType_hiden").attr('value',service_type_DTD);
    }
    else if(value==2)
    {
        $("#idServiceType").attr('value',service_type_DTP);
        $("#idServiceType_hiden").attr('value',service_type_DTP);
    }
    else
    {
        $("#idServiceType").attr('value',service_type_PTD);
        $("#idServiceType_hiden").attr('value',service_type_PTD);
    }
 }
 function change_shipper_fields(cb_id,field_id,cb_flag)
 {
 	var cb_flag = $("#"+cb_id).prop('checked');
 
 	if(cb_flag==1)
 	{
 		$("#"+field_id).attr('value',"");
 		$("#"+field_id).removeClass('red_border');
 		$("#"+field_id).addClass('disable-field');
 		$("#"+field_id).attr('disabled',"disabled");
 	}
 	else
 	{
 		$("#"+field_id).removeAttr('disabled');
 		$("#"+field_id).removeClass('red_border');
 		$("#"+field_id).removeClass('disable-field');
 		$("#"+field_id).attr('value',"");
 	}
 }
 

function auto_fill_new_shipper_consignee(company_name,type)
{
	$("#loader").attr('style','display:block;');
	var idServiceType = $("#szTransportation").attr('value');
	var szBookingRandomNum = $("#szBookingRandomNum_hidden").attr('value'); 
	if(type=='SHIPPER')
	{
		$("#iAddRegistShipper").attr("disabled", true);
		$("#iAddRegistShipper").removeAttr("checked");
		var hidden_value = $("#iPickupAddress_hidden").attr("value");
	}
	else
	{
		$("#iAddRegistConsignee").attr("disabled", true);
		$("#iAddRegistConsignee").removeAttr("checked");
		var hidden_value = $("#iDeliveryAddress_hidden").attr("value");
	}
        var  iGetShipperPostcodeFromGoogle=''
        var iGetConsigneePostcodeFromGoogle='';
        if(type=='SHIPPER_NEW')
        {
           iGetShipperPostcodeFromGoogle =$("#iGetShipperPostcodeFromGoogle").attr('value');
        }
        else if(type=='CONSIGNEE_NEW')
        {
           iGetConsigneePostcodeFromGoogle = $("#iGetConsigneePostcodeFromGoogle").attr('value');
        }
	 
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingDetails.php",{iGetConsigneePostcodeFromGoogle:iGetConsigneePostcodeFromGoogle,iGetShipperPostcodeFromGoogle:iGetShipperPostcodeFromGoogle,company_name:company_name,type:type,idServiceType:idServiceType,visibility:hidden_value,szBookingRandomNum:szBookingRandomNum},function(result){			
		var result_ary = result.split("||||");
        
       	if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
        	if(type=='SHIPPER_NEW')
                {
                        $("#shipper_info_div").html(result_ary[1]);
                }
                else if(type=='CONSIGNEE_NEW')
                {
                        $("#consignee_info_div").html(result_ary[1]);
                } 
        }
        else if(jQuery.trim(result_ary[0])=='BOOKING_NOTIFICATION')
        {
			$("#change_price_div").html(result_ary[1]); 
        }
		
		$("#loader").attr('style','display:none;');
		$("#loader").attr('style','display:none;');
	}); 
}

 function level3WordSearch()
 {
 	szKeyWordValue = $("#szKeyWordValue").attr('value');
        idLanguage = $("#idLanguage").attr('value');
 	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_searchword.php?lang="+idLanguage,{mode:'SEARCH_KEYWORD',szKeyWordValue:szKeyWordValue},function(result){
		$("#level3SerachResult").html(result);
		$("#loader").attr('style','display:none;');
	});
 }
 
 function submitenterKeyValue(e,divValue,searchUrl)
{
    var keycode;
    keycode = e.which; 
    if(keycode == 13)
    {
        if(divValue=='wordSearchForm')
        {
           var szKeyWordValue = $("#szKeyWordValue").attr('value');

           if(szKeyWordValue.length>=1)
           {
               level3WordSearch();
           } 
           return false;
        }
        else if(divValue=='wordSearchLeftForm')
        {
           var szKeyWordValue = $("#szKeyWordLeftValue").attr('value');

           if(szKeyWordValue.length>=1)
           {
               level3WordLeftSearch(searchUrl,szKeyWordValue);
           } 
           return false;
        }
    }
    else
    {
       if(divValue=='wordSearchForm')
       {
           var szKeyWordValue = $("#szKeyWordValue").attr('value'); 
           if(szKeyWordValue.length==0)
           {
               $("#searchButtonId").attr('style','opacity:0.4;');
               $("#searchButtonId").unbind("click");
           }
           else
           {
               $("#searchButtonId").attr('style','opacity:1;cursor:pointer;');
               $("#searchButtonId").unbind("click");
               $("#searchButtonId").click(function(){level3WordSearch()});
           }
       }
       else if(divValue=='wordSearchLeftForm')
       {
           var szKeyWordValue = $("#szKeyWordLeftValue").attr('value');

           if(szKeyWordValue.length==0)
           {
               $("#searchButtonIdLeft").attr('style','opacity:0.4;');
               $("#searchButtonIdLeft").unbind("click");
           }
           else
           {
               $("#searchButtonIdLeft").attr('style','opacity:1;cursor:pointer;');
               $("#searchButtonIdLeft").unbind("click");
               $("#searchButtonIdLeft").click(function(){level3WordLeftSearch(searchUrl,szKeyWordValue)});
           }
       }
    }
}

function level3WordLeftSearch(searchUrl,key)
{ 
    ////console.log(searchUrl);
    //var szSearchUrl = searchUrl+"?search="+key;
   // $("#wordSearchLeftForm").attr('action',searchUrl);
    //$("#wordSearchLeftForm").attr('method',"post");
    $("#wordSearchLeftForm").submit();
    
//    document.wordSearchLeftForm.action = searchUrl;
//    document.wordSearchLeftForm.method = "post"
//    document.wordSearchLeftForm.submit();
}

function hidenav(id) {
	
	document.getElementById(id).style.display = "none";
	
}
function shownav(id) {
	document.getElementById(id).style.display = "block";
}


function enableDisableSearchButton(szPageUrl,id_suffix,iServiceFlag,idUser,iHiddenValue,e,iSearchMiniVersion)
{
    var validation_success = 1;
    if(iSearchMiniVersion==7 || iSearchMiniVersion==8 || iSearchMiniVersion==9)
    {
        var error = 1;
        var formId = "landing_page_form"+id_suffix; 
        if(isFormInputElementEmpty(formId,'szFirstName'+id_suffix)) 
        {		
            console.log('First Name');
            error++;  
        }
        if(isFormInputElementEmpty(formId,'szLastName'+id_suffix)) 
        {		
            console.log('Last Name');
            error++;  
        }
        if(isFormInputElementEmpty(formId,'szEmail'+id_suffix)) 
        {		
            console.log('Email');
            error++;  
        }
        if(isFormInputElementEmpty(formId,'szPhone'+id_suffix)) 
        {		
            console.log('szPhone');
            error++;  
        }
        
        if(iSearchMiniVersion==8 || iSearchMiniVersion==9)
        {
            if(isFormInputElementEmpty(formId,'szOriginCountryStr'+id_suffix)) 
            {		
                console.log('szOriginCountryStr');
                error++;  
            }
            if(isFormInputElementEmpty(formId,'szDestinationCountryStr'+id_suffix)) 
            {		
                console.log('szDestinationCountryStr');
                error++;  
            }
        }
        else
        {
            if(isFormInputElementEmpty(formId,'szAddress'+id_suffix)) 
            {		
                console.log('szAddress');
                error++;  
            }
            if(isFormInputElementEmpty(formId,'szPostcode'+id_suffix)) 
            {		
                console.log('szPostcode');
                error++;  
            }
            if(isFormInputElementEmpty(formId,'szCity'+id_suffix)) 
            {		
                console.log('szCity');
                error++;  
            } 
        } 
        //for now we are removing this empty field check
        var error = 1;
        if(error==1)
        {
            validation_success = 1;
        }
        else
        {
            validation_success = 2;
        }
    }
    else
    {
        var szOriginCountryStr=jQuery.trim($("#szOriginCountryStr"+id_suffix).attr('value'));
        var szDestinationCountryStr=jQuery.trim($("#szDestinationCountryStr"+id_suffix).attr('value'));
        var datepicker1_search_services=jQuery.trim($("#datepicker1_search_services"+id_suffix).attr('value'));

        var iVolume = '';
        var iWeight = '';
        var iNumColli = '';
        var iPalletType = '';

        if($("#iVolume"+id_suffix).length)
        {
            iVolume=jQuery.trim($("#iVolume"+id_suffix).attr('value'));
        }
        if($("#iWeight"+id_suffix).length)
        {
            iWeight = jQuery.trim($("#iWeight"+id_suffix).attr('value'));
        } 
        if($("#iNumColli"+id_suffix).length)
        {
            iNumColli = jQuery.trim($("#iNumColli"+id_suffix).attr('value'));
        } 
        if($("#iPalletType"+id_suffix).length)
        {
            iPalletType = jQuery.trim($("#iPalletType"+id_suffix).attr('value'));
        }

        var keycode;
        keycode = e.which; 
        iSearchMiniVersion = parseInt(iSearchMiniVersion);
        var validater_flag = 0;

        if(iSearchMiniVersion>2)
        {
            if(iSearchMiniVersion==3 || iSearchMiniVersion==4)
            {
                validater_flag = 1;
            }
            else if(iSearchMiniVersion==5 || iSearchMiniVersion==6)
            {
                if(iNumColli!='' && iNumColli!=0 && iPalletType!='' && iWeight!='' && iWeight!=0)
                {
                    validater_flag = 1;
                }
            }
        }
        else
        { 
            if(iWeight!='' && iWeight!=0 && iVolume!='' && iVolume!=0)
            {
                validater_flag = 1;
            }
        }

        if(validater_flag==1 && szOriginCountryStr!='' && szDestinationCountryStr!='' && datepicker1_search_services!='')
        {
            validation_success = 1;
        }
        else
        {
            validation_success = 2;
        }
    }
    console.log("Vali: "+validation_success+" Flag: "+validater_flag);
    if(validation_success==1)
    { 
        if(keycode == 13)
        {
            if(iServiceFlag)
            {
                update_service_listing('landing_page_form'+id_suffix,iHiddenValue);
            }
            else
            {
                validateNewLandingPageForm('landing_page_form'+id_suffix,idUser,szPageUrl,iHiddenValue,iSearchMiniVersion);
            } 
         }
         else
         {
            if(iServiceFlag)
            {
                $("#update_search"+id_suffix).unbind("click");
                $("#update_search"+id_suffix).removeAttr("onclick");
                $("#update_search"+id_suffix).click(function(){update_service_listing('landing_page_form'+id_suffix,iHiddenValue)});
            }
            else
            {
                $("#search_listing"+id_suffix).unbind("click");
                $("#search_listing"+id_suffix).removeAttr("onclick");
                $("#search_listing"+id_suffix).click(function(){validateNewLandingPageForm('landing_page_form'+id_suffix,idUser,szPageUrl,iHiddenValue,iSearchMiniVersion)});
            }
        }
        $("#search-btn-container"+id_suffix).removeClass('btn-grey');
        $("#search-btn-container"+id_suffix).addClass('btn');
    }
    else
    {
        if(iServiceFlag)
        {
            $("#update_search"+id_suffix).unbind("click");
            $("#update_search"+id_suffix).removeAttr("onclick");
        }
        else
        {
            $("#search_listing"+id_suffix).unbind("click");
            $("#search_listing"+id_suffix).removeAttr("onclick");
        }
        $("#search-btn-container"+id_suffix).removeClass('btn');
        $("#search-btn-container"+id_suffix).addClass('btn-grey'); 
    }
}

function check_form_field_empty_standard(formId,inputId,inputName,display_bottom)
{	
    if(isFormInputElementEmpty(formId,inputId)) 
    {
       displayFormFieldIsRequired(formId, inputId, inputName+' is required.',display_bottom); 
    }
    else
    {
    	//$("#"+inputId,"#"+formId).validationEngine('hide');
    	$("#"+inputId,"#"+formId).removeClass('red_border');
    }
}
function validate_email(formId,inputId,inputName,display_bottom)
{ 
    if(isFormInputElementEmpty(formId, inputId)) 
    {
       displayFormFieldIsRequired(formId, inputId, inputName+' is required.',display_bottom); 
       return false;
    }
    else if(!isValidEmail($('form#' + formId + ' #'+ inputId ).val())) 
    { 
       displayFormFieldIsRequired(formId, inputId, inputName+' is required.',display_bottom); 
       return false;
    }
    else
    {
    	$("#"+inputId,"#"+formId).validationEngine('hide');
    	$("#"+inputId,"#"+formId).removeClass('red_border');
        return true;
    } 
}
function isFormInputElementEmpty(formId, inputElementId) 
{
    if($.trim($('form#' + formId + ' #' + inputElementId).val()) == "") {    
        return true;
    }
    else {
        return false;
    }
}
function check_form_field_not_required(formId,inputId)
{
    $("#"+inputId).removeClass('red_border');
    $("#"+inputId,"#"+formId).validationEngine('hide'); 
}

function displayFormFieldIsRequired(formId, inputElementId, msg,display_bottom,display_poup)
{
    if(display_poup==1)
    {
        if(display_bottom==1)
        {
            $('form#' + formId + ' #' + inputElementId).validationEngine('showPrompt', msg, '',  'bottomLeft', true);
        }
        else if(display_bottom==2)
        {
            $('form#' + formId + ' #' + inputElementId).validationEngine('showPrompt', msg, '',  'topLeft', true);
        }
        else
        {
            $('form#' + formId + ' #' + inputElementId).validationEngine('showPrompt', msg, '',  '', true);
        }
     }
     else
     {
            $('form#' + formId + ' #' + inputElementId).addClass('red_border');	
     }
}
function change_isd_code(dial_code,mode)
{ 
	if(mode=='SHIPPER')
	{
		$('#idShipperDialCode').val(dial_code);
	}
	else
	{
		$('#idConsigneeDialCode').val(dial_code);
	}  
}

function open_terms_condition_popup(page_url)
{
	var win = window.open(page_url, '_blank',"width=700, height=500");
  	win.focus();
}
function display_payment_tooltip(buttontext1,buttontext2)
{ 
	var cb_flag = $('#iBankTransferPayment').prop('checked');
	if(cb_flag)
	{
		$("#pay_button").html(buttontext2);
		$("#bank_transfer_message_span").attr("style",'display:inline-block;font-size:12px;font-style:italic;');
	}
	else
	{
		$("#pay_button").html(buttontext1);
		$("#bank_transfer_message_span").attr("style",'display:none;');
	}
}
function toggle_billing_address(div_id,iShipperConsignee,iPrivateShipping)
{
    if(iShipperConsignee==1)
    {
        var cb_flag = $('#iThisIsMe_shipper').prop('checked');  
    }
    else
    {
        var cb_flag = $('#iThisIsMe_consignee').prop('checked'); 
    }  
    if(cb_flag)
    { 
        $("#billing-address-container").attr("style",'display:none;');
        $("#iShipperConsignee").val(iShipperConsignee);
        
        if(iShipperConsignee==1)
        {
            //$('#iThisIsMe_consignee').attr('disabled','disabled'); 
            $('#iThisIsMe_consignee').removeAttr('checked');
            
            if(parseInt(iPrivateShipping)=='1')
            {
                $("#szShipperCompanyName_p").addClass("disabled");
                
                $("#szShipperFirstName").removeAttr('onblur');
                $("#szShipperFirstName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szShipperFirstName");updateszCompanyName("Shipper");');
                 
                $('#szShipperCompanyName').unautocomplete("unautocomplete");
                $("#szShipperCompanyName").removeAttr('onblur');
                $("#szShipperCompanyName").attr("readonly","readonly");
                $("#szShipperCompanyName").removeClass('red_border');
            
                $("#szShipperLastName").attr('onblur','');
                $("#szShipperLastName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szShipperLastName");updateszCompanyName("Shipper");'); 
            }
        }
        else
        {
            //$('#iThisIsMe_shipper').attr('disabled','disabled'); 
            $('#iThisIsMe_shipper').removeAttr('checked');
            
            if(parseInt(iPrivateShipping)=='1')
            {
                $("#szConsigneeCompanyName_p").addClass("disabled");
                $('#szConsigneeCompanyName').unautocomplete("unautocomplete");
                $("#szConsigneeCompanyName").removeAttr('onblur');
                $("#szConsigneeCompanyName").attr("readonly","readonly");
                $("#szConsigneeCompanyName").removeClass('red_border');
                
                $("#szConsigneeFirstName").attr('onblur','');
                $("#szConsigneeFirstName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szConsigneeFirstName");updateszCompanyName("Consignee");');
                 
                $("#szConsigneeLastName").attr('onblur','');
                $("#szConsigneeLastName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szConsigneeLastName");updateszCompanyName("Consignee");');
            
            }
                        
        }
        
        if(parseInt(iPrivateShipping)=='1')
        { 
            $("#szBillingCompanyName_p").removeClass("disabled"); 
            $("#szBillingLastName").attr('onblur','');
             $("#szBillingLastName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szBillingLastName");'); 
            
            $("#szBillingCompanyName").attr("readonly",false);  
            $("#szBillingCompanyName").removeAttr('onblur');
            $("#szBillingCompanyName").attr("onblur",'check_form_field_empty_standard("booking_detail_form","szBillingCompanyName")'); 
                
            $("#szBillingFirstName").attr('onblur','');
            $("#szBillingFirstName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szBillingFirstName");');  
        }
    }
    else
    { 
        $("#billing-address-container").attr("style",'display:block'); 
        $("#iShipperConsignee").val(3);
        if($('#iThisIsMe_consignee').length)
        {
            //$('#iThisIsMe_consignee').removeAttr('disabled');
            $('#iThisIsMe_consignee').removeAttr('checked');
            
            if(parseInt(iPrivateShipping)=='1')
            {
                $("#szConsigneeCompanyName_p").removeClass("disabled");
                initAutodropDowns_consignee();
                $("#szConsigneeFirstName").attr('onblur','');
                $("#szConsigneeFirstName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szConsigneeFirstName");');
                
                $("#szConsigneeCompanyName").attr("readonly",false); 
                $("#szConsigneeCompanyName").removeAttr('onblur');
                $("#szConsigneeCompanyName").attr("onblur",'check_form_field_empty_standard("booking_detail_form","szConsigneeCompanyName")'); 
                       
                $("#szConsigneeLastName").attr('onblur','');
                $("#szConsigneeLastName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szConsigneeLastName");');
            
            }
        }
        if($('#iThisIsMe_shipper').length)
        {
            //$('#iThisIsMe_shipper').removeAttr('disabled');
            $('#iThisIsMe_shipper').removeAttr('checked');
            
            if(parseInt(iPrivateShipping)=='1')
            {
                $("#szShipperCompanyName_p").removeClass("disabled");                
                $("#szShipperFirstName").attr('onblur','');
                $("#szShipperFirstName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szShipperFirstName");');
                initAutodropDowns_shipper();
                
                $("#szShipperCompanyName").attr("readonly",false);
                $("#szShipperCompanyName").removeAttr('onblur');
                $("#szShipperCompanyName").attr("onblur",'check_form_field_empty_standard("booking_detail_form","szShipperCompanyName")'); 

                $("#szShipperLastName").attr('onblur','');
                $("#szShipperLastName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szShipperLastName");'); 
            } 
        }
        
        if(parseInt(iPrivateShipping)=='1')
        {
            var szBillingFirstName=$("#szBillingFirstName").attr('value');
            var szBillingLastName=$("#szBillingLastName").attr('value');
            var szCompanyName=szBillingFirstName+' '+szBillingLastName;
            $("#szBillingCompanyName").attr('value',szCompanyName);
            
             $("#szBillingLastName").attr('onblur','');
             $("#szBillingLastName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szBillingLastName");updateszCompanyName("Billing");');
             
            $("#szBillingFirstName").attr('onblur','');
            $("#szBillingFirstName").attr('onblur','check_form_field_empty_standard("booking_detail_form","szBillingFirstName");updateszCompanyName("Billing");');
            
            $("#szBillingCompanyName_p").addClass("disabled");
            $("#szBillingCompanyName").attr("readonly","readonly"); 
            $("#szBillingCompanyName").removeAttr('onblur');
            $("#szBillingCompanyName").removeClass('red_border');
        }
    } 
} 

function validate_shiiper_consignee()
{
    var error_count = 1;
    var shipperConsigneeFields = new Array(); 
    shipperConsigneeFields['0'] = 'szHandoverCity';
    shipperConsigneeFields['1'] = 'fCargoVolume';
    shipperConsigneeFields['2'] = 'fCargoWeight';
    shipperConsigneeFields['3'] = 'isMoving'; 
    shipperConsigneeFields['4'] = 'fTotalInsuranceCostForBookingCustomerCurrency';  
    
    $.each( notEditableInputsAry, function( index, input_fields ){
        $("#"+input_fields).css('background-color','#D3D3D3');
        $("#"+input_fields).attr('readonly','readonly');
        $("#"+input_fields).attr('disabled','disabled');
    }); 
}
function isNumberKeySearch(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    //alert(charCode);
    if(charCode==46 || charCode==44)
    {
    	return true;
    }
     if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
    	return false;
    }
    else
    {
    	return true;
    }
} 
function check_form_field_empty_standard_search(formId,inputId,idContainer,inputName,flag)
{	
    if(isFormInputElementEmpty(formId,inputId))
    {
        if(flag==1)
        {
            displayFormFieldIsRequired(formId, idContainer, inputName+' is required.','2');
            $("#"+idContainer,"#"+formId).addClass('red_border');
       	}
       	else
       	{
            displayFormFieldIsRequired(formId, idContainer, inputName+' is required.','1');
            $("#"+idContainer,"#"+formId).addClass('red_border');
       	} 
    }
    else
    {
    	$("#"+idContainer,"#"+formId).validationEngine('hide');
    	$("#"+idContainer,"#"+formId).removeClass('red_border');
    }
}

function openSearchPopup()
{
	var value=$("#openSearchPopUp").attr('value');
	if(value==1)
	{
		$("#search_header").removeClass('popsearch');
		$("#openSearchPopUp").attr('value','');
	}
	else
	{
		$("#search_header").addClass('popsearch');
		$("#openSearchPopUp").attr('value','1');
	}
}

function checkFromAddressWeightVolume(szValue,flag,idUser,SzPageUrl,id_suffix,iServiceFlag,szFirstMsg,szSecondMsg,iHiddenValue,event,iSearchMiniVersion)
{ 
    if(szValue!='')
    {		
        var formId="landing_page_form"+id_suffix;	
        if(flag=='VOLUME')
        {
            var fMaxVolume=$("#fMaxVolume").attr('value');

            if(parseFloat(szValue)>parseFloat(fMaxVolume))
            {
                var szErrorMsg=szFirstMsg+" "+fMaxVolume+" "+szSecondMsg;
                var input_field_id="iVolume"+id_suffix+"_container";
                $("#"+input_field_id).addClass('red_border');
                //display_cargo_exceed_popup();
                displayFormFieldIsRequired(formId, input_field_id, szErrorMsg,1,1);

                if(iServiceFlag)
                {
                   $("#update_search"+id_suffix).unbind("click");
                   $("#update_search"+id_suffix).removeAttr("onclick");
                }
                else
                {
                   $("#search_listing"+id_suffix).unbind("click");
                   $("#search_listing"+id_suffix).removeAttr("onclick");
                }
                $("#search-btn-container"+id_suffix).removeClass('btn');
                $("#search-btn-container"+id_suffix).addClass('btn-grey');
            }
            else
            {
                var input_field_id="iVolume"+id_suffix+"_container";
                $("#"+input_field_id,"#"+formId).validationEngine('hide');
                $("#"+input_field_id).removeClass('red_border');
                enableDisableSearchButton(SzPageUrl,id_suffix,iServiceFlag,idUser,iHiddenValue,event,iSearchMiniVersion);
            }
        }
        else if(flag=='WEIGHT')
        {
            var fMaxWeight=$("#fMaxWeight").attr('value');
            //alert(fMaxWeight);
            if(parseFloat(szValue)>parseFloat(fMaxWeight))
            {
                //alert(szValue);
                var szErrorMsg=szFirstMsg+" "+fMaxWeight+" "+szSecondMsg;

                var input_field_id="iWeight"+id_suffix+"_container";
                $("#"+input_field_id).addClass('red_border');
                displayFormFieldIsRequired(formId, input_field_id,szErrorMsg,2,1);

                if(iServiceFlag)
                 {
                    $("#update_search"+id_suffix).unbind("click");
                    $("#update_search"+id_suffix).removeAttr("onclick");
                 }
                 else
                 {
                    $("#search_listing"+id_suffix).unbind("click");
                    $("#search_listing"+id_suffix).removeAttr("onclick");
                 }
                 $("#search-btn-container"+id_suffix).removeClass('btn');
                 $("#search-btn-container"+id_suffix).addClass('btn-grey');
            }
            else
            {
                var input_field_id="iWeight"+id_suffix+"_container";
                $("#"+input_field_id,"#"+formId).validationEngine('hide');
                $("#"+input_field_id).removeClass('red_border');
                enableDisableSearchButton(SzPageUrl,id_suffix,iServiceFlag,idUser,iHiddenValue,event,iSearchMiniVersion);
            }
        } 
    }
}

function save_previous_data()
{ 
   var divid = document.getElementById('booking-details-form-hidden'); 
	$('#booking_detail_form').find('input').each(function(){ 
		var field_id = this.id ;
		var current_value = this.value ; 
		var new_field_id = field_id+"_hidden_flag" ;
		
		 if( $('#'+new_field_id).length > 0 )
         {
         	$("#"+new_field_id).val(current_value);
         }
         else
         { 
			var newDiv=document.createElement('input'); 
			newDiv.setAttribute('id', new_field_id);
			newDiv.setAttribute('type','hidden');
			newDiv.setAttribute('value',current_value);
			divid.appendChild(newDiv);
         }
	});	
}

function update_previous_data()
{ 
	$('#booking_detail_form').find('input').each(function(){ 
		var field_id = this.id ;
		var hidden_field_id = field_id+"_hidden_flag" ;
		
		if( $('#'+hidden_field_id).length > 0 )
        { 
			var field_type = this.type ;
			
			if(field_type=='checkbox')
			{ 
				var current_val = $("#"+hidden_field_id).val();
				if(current_val==1)
				{
					//$("#"+field_id).prop('checked','checked');
				} 
			}
			else
			{
				var current_val = $("#"+hidden_field_id).val();
				$("#"+field_id).val(current_val);
			}
		} 
	});	
}

function validateaddress(hidden_flag,user_id,page_url,suffix,service_page_flag)
{
    var iDonotCallAjax=0;
    if($("#iServicePage").length>0)
    {
        iDonotCallAjax=1;
    }
    var szOriginAddress_js = $("#szOriginCountryStr"+suffix).val();  
    checkFromAddress(szOriginAddress_js,'FROM_COUNTRY',user_id,page_url,suffix,service_page_flag,iDonotCallAjax);

    var szDestinationAddress_js = $("#szDestinationCountryStr"+suffix).val(); 
    checkFromAddress(szDestinationAddress_js,'TO_COUNTRY',user_id,page_url,suffix,service_page_flag,iDonotCallAjax);
}

function validate_verfify_email_field(kEvent,formId,inputId,flag)
{   
	if(isFormInputElementEmpty(formId, inputId)) 
	{  
       $("#email_verification_button").attr("style",'opacity:0.4;');
       $("#email_verification_button").unbind("click"); 
    }
    else if(!isValidEmail($('form#' + formId + ' #'+ inputId ).val())) 
    {   
       $("#email_verification_button").attr("style",'opacity:0.4;');
       $("#email_verification_button").unbind("click"); 
    }
    else
    { 
    	$("#email_verification_button").attr("class",'button1');
    	$("#email_verification_button").attr("style",'opacity:1;');
    	$("#email_verification_button").unbind("click"); 
		$("#email_verification_button").click(function(){send_email_verification()});
		if(flag!=1)
		{
	    	if(kEvent.keyCode==13)
			{
				send_email_verification();
			}
		}
    } 
}

function send_email_verification()
{ 
    $("#email_verification_button").attr("style",'opacity:0.4;');
    $("#email_verification_button").unbind("click"); 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_myAccount.php",$("#user-email-verification-form").serialize(),function(result){
	
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        { 
         	$("#email-verification-div").html(result_ary[1]); 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
        	$("#email-verification-div").html(result_ary[1]); 
        } 
    });
} 

function display_first_time_password_popup(type)
{  
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_updateUserPassword.php",{type:type,mode:'DISPLAY_FIRST_TIME_PASSWORD_POPUP'},function(result){
	
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        { 
            $("#user_account_login_popup").html(result_ary[1]); 
            $("#user_account_login_popup").attr("style",'display:block;');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#user_account_login_popup").html(result_ary[1]); 
            $("#szPassword").focus();			
            var $thisVal = $("#szPassword").val(); 
            $("#szPassword").val('').val($thisVal);
            $("#user_account_login_popup").attr("style",'display:block;');
        } 
        $("#loader").attr('style','display:none;');
    });
}  

function firstTimeChangeUserPassword()
{
    $("#one-time-popup-save-button").addClass('btn-clicked');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_updateUserPassword.php",$("#firstTimeChangePassword").serialize(),function(result){
	
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        { 
            $("#user_account_login_popup").html(result_ary[1]); 
            $("#user_account_login_popup").attr("style",'display:block;');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#user_account_login_popup").html(result_ary[1]); 
            $("#user_account_login_popup").attr("style",'display:block;');
        } 
        else if(jQuery.trim(result_ary[0])=='REDIRECT')
        {
            redirect_url(result_ary[1]);  
        } 
        $("#one-time-popup-save-button").removeClass('btn-clicked');
    });
}
function display_check_for_password_popup(type)
{  
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_updateUserPassword.php",{type:type,mode:'TYPE_PASSWORD_POPUP'},function(result){
	
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        { 
            $("#user_account_login_popup").html(result_ary[1]); 
            $("#user_account_login_popup").attr("style",'display:block;');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#user_account_login_popup").html(result_ary[1]);  
            $("#szPassword").focus();			
            var $thisVal = $("#szPassword").val(); 
            $("#szPassword").val('').val($thisVal);   
            $("#user_account_login_popup").attr("style",'display:block;');
        } 
        $("#loader").attr('style','display:none;');
    });
} 
function check_authentication_keyup(kEvent)
{
	if(kEvent.keyCode==13)
	{
		check_authentication();
	}
}

function check_authentication()
{
	$("#loader").attr('style','display:block;');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_updateUserPassword.php",$("#typePasswordForm").serialize(),function(result){
	
		result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        { 
         	$("#user_account_login_popup").html(result_ary[1]); 
         	$("#user_account_login_popup").attr("style",'display:block;');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
        	$("#user_account_login_popup").html(result_ary[1]); 
        	$("#user_account_login_popup").attr("style",'display:block;');
        } 
        else if(jQuery.trim(result_ary[0])=='REDIRECT')
        {
        	redirect_url(result_ary[1]);  
        } 
        $("#loader").attr('style','display:none;');
	});
}

function addInsuranceDetails(mode,on_blur_flag)
{
    var isMobile = isMobileDevice();
    if(isMobile)
    {
        //$("#loader").attr('style','display:block;');
    }
    if(mode=='REMOVE_INSURANCE_DETAILS')
    {
        mode = 'REMOVE_INSURANCE_DETAILS';
        $("#pay_button").removeAttr('style');
        $("#pay_button").attr('onclick',"booking_on_hold('CONFIRMED')"); 
        $("#pay_button").attr('class','button1');

        check_form_field_not_required('addInsuranceForm','fValueOfGoods');
    }
    else
    {
        mode = 'ADD_INSURANCE_DETAILS';
    } 
    $("#iInsuranceSelectedFlag").attr('value','1'); 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php?mode="+mode,$("#addInsuranceForm").serialize(),function(result){
        
        var res_ary = result.split("||||"); 
        if(res_ary[0]=='SUCCESS')
        { 
            if(on_blur_flag!=1)
            {
                $("#booking_price_span").html(res_ary[1]);
                $("#booking_confirmation_insurance_container").html(res_ary[2]);
                $("#total_invoice_conatiner").html(res_ary[3]); 

                $("#pay_button").removeAttr('style');
                $("#pay_button").attr('onclick',"booking_on_hold('CONFIRMED')"); 
                $("#pay_button").attr('class','button1');

                check_form_field_not_required('addInsuranceForm','fValueOfGoods');
            }
            if(isMobile)
            {
                if($("#stripe_refersh").length)
                {
                    var stripe_refersh = $("#stripe_refersh").val(); 
                    if(stripe_refersh == '1')
                    {
                        $("#pay_button").removeAttr('onclick');
                        $("#pay_button").attr('style',"opacity:0.4");  
                        
                        $("#loader").attr('style','display:block');
                        window.location.reload();
                    }
                }
            }
        }
        else if(res_ary[0]=='ERROR')
        { 
            if(on_blur_flag!=1)
            {
                $("#booking_confirmation_insurance_container").html(res_ary[1]);
                $("#pay_button").removeAttr('style');
                $("#pay_button").attr('onclick',"booking_on_hold('CONFIRMED')"); 
                $("#pay_button").attr('class','button1');
            } 
        }
        else if(res_ary[0]=='POPUP')
        {
           $("#change_price_div").html(res_ary[1]); 
           $("#change_price_div").attr('style','display:block');
        } 
        else
        {

        }	
        $("#loader").attr('style','display:none;');
    });
}

function format_decimat(form_field,evt)
{
    var charcode=(evt.which)?(evt.which):(event.keyCode);  
    if ((charcode>= 48 && charcode <=57) || (charcode==8))
    {
        return true;
    }
    return false;
}

function display_insurance_fields(element_id)
{
	if(element_id=='iInsurance_yes')
	{   
		$("#insurance-form-container").attr('style','display:block;'); 
		$("#fValueOfGoods").focus();
	}
	else
	{
		$("#insurance-form-container").attr('style','display:none;'); 
	}
} 
function validate_insurance_box(formId,inputId,on_blur_flag,cargo_require)
{ 
    /*
    var iIncludeInsuranceWithZeroCargo = $("#iIncludeInsuranceWithZeroCargo").val();
    iIncludeInsuranceWithZeroCargo = parseInt(iIncludeInsuranceWithZeroCargo);
    if(isFormInputElementEmpty(formId,inputId) && cargo_require==1 && iIncludeInsuranceWithZeroCargo!=1) 
    {		
       $("#"+inputId).addClass('red_border');
       return false;
    }
    else
    { 
    	addInsuranceDetails('',on_blur_flag);
    } 
    */
   addInsuranceDetails('',on_blur_flag);
}

function check_location(input_id,flag,idUser,SzPageUrl,id_suffix,iServiceFlag)
{ 
  setTimeout(function(){
   	$('.pac-container').each(function(){
		var style_display = $(this).css('display');
		if(flag=='FROM_COUNTRY')
		{
                    var iCheckedForFromField = $("#iCheckedForFromField").val();
		}
		else
		{
                    var iCheckedForFromField = $("#iCheckedToFromField").val();
		}
		if(style_display=='none' && iCheckedForFromField!=1)
		{ 
                    var szValue = $("#"+input_id).val();
                    checkFromAddress(szValue,flag,idUser,SzPageUrl,id_suffix,iServiceFlag)
		}
            });
	},'500'); 
}

function reste_search_flags(flag)
{
	if(flag=='FROM_COUNTRY')
	{
		$("#iCheckedForFromField").attr('value','0');
	}
	else
	{
		$("#iCheckedToFromField").attr('value','0');
	}
}

function display_cargo_exceed_popup()
{
	var szBookingRandomNum = $("#szBookingRandomNum").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",{mode:'DISPLAY_CARGO_EXCEED_POPUP',booking_key:szBookingRandomNum},function(result){		     
         result_ary = result.split("||||");
         
         if(jQuery.trim(result_ary[0])=='SUCCESS')
         { 
         	$("#ajaxLogin").html(result_ary[1]);
         	$("#ajaxLogin").css('display','block'); 
         }
    }); 
}

function update_booking_currency(booking_currency,booking_key,iQuickQuoteBooking)
{ 
    if(iQuickQuoteBooking==1)
    {
        var from_page_flag = 'QUICK_QUOTE_PAGE';
    }
    else
    {
        var from_page_flag = $("#from_page_flag").val(); 
        var iDisplayedBookingDetails = $("#showBookingFieldsFlag").val();
    }  
    
    if(from_page_flag!='SERVICE_QUOTE_PAGE' && from_page_flag != 'QUICK_QUOTE_PAGE')
    {
        $("#update_cargo_details_container_div").attr('style','display:none;');
        $("#booking_details_container").attr('style','display:none;');
    } 
	 
    var lang = $("#iSiteLanguageFooter_hidden").val();
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php?lang="+lang,{operation:'CHANGE_BOOKING_CURRENCY',booking_key:booking_key,booking_currency:booking_currency,from_page:from_page_flag,iQuickQuoteBooking:iQuickQuoteBooking},function(result){		     
        var result_ary = result.split("||||");
         
        if(jQuery.trim(result_ary[0])=='ERROR')
        { 
           $("#ajaxLogin").html(result_ary[1]);
           $("#ajaxLogin").css('display','block'); 
        }
        else if(jQuery.trim(result_ary[0])=='BOOKINGNOTIFICATION')
        { 	
           $("#all_available_service").html(result_ary[1]);
           $("#all_available_service").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
           $("#service-listing-container").html(result_ary[1]);      
           if(iDisplayedBookingDetails==1)
           {
               var idWTW = $("#szServiceUniqueKey").val();
               var booking_url='';
               var idUser = '';
               var new_booking_page='' ;

               toggleServiceList(idWTW,booking_url,idUser,new_booking_page,1);
           }  	
           $("#service-listing-container").attr('style','display:block'); 
        }
        $("#loader").attr('style','display:none;');
    }); 
}

function on_focus_currency_dropdown(select_box_id)
{
	$('#'+select_box_id+' option').each(function() { 
	    $(this).attr("style",'background-color:#F0F0F0;color:#222222;font-style:underline;text-decoration: underline;'); 
	});
}
function toggle_price_gurantee(action)
{
	if(action=='SHOW')
	{
		$("#price_gurantee_details_text").css('display','inline-block');
		$("#price_gurantee_less_span").css('display','inline-block');
		$("#price_gurantee_more_span").css('display','none'); 
	}
	else
	{
		$("#price_gurantee_details_text").css('display','none');
		$("#price_gurantee_less_span").css('display','none');
		$("#price_gurantee_more_span").css('display','inline-block'); 
	}	
}

function show_servicepage_tooltip(tool_tip_header,tool_tip_text,div_id,button_id)
{	 
	var iTop = $(".customer-services").offset().top ;
	var iLeft = $(".customer-services").offset().left ; 
	
			//top: '82%',
			//left: '80%'
	$("#"+div_id).css('display','block');	 
		tool_tip_text = tool_tip_text.charAt(0).toUpperCase() + tool_tip_text.slice(1);            // capitalising first char to uppercase 
        var header_text = "<span align='right'><a heref='javascript:void(0);' onclick='close_chat_bubble();'><img src='"+ __JS_ONLY_SITE_BASE__+"/images/close.png' style='right:-12px;top:-12px;width:26px;position:absolute;' alt='close'></a></span><span class='support-popup-heading'>"+tool_tip_header+"</span>" ;
          var text_content = "<div>"+tool_tip_text+"</div>" ;
          tool_tip_text= header_text + text_content ;  
                
          	$("#"+div_id).html("<div>"+tool_tip_text+"</div>");
          	
          	//$('html, body').animate({ scrollTop: ($("#"+button_id).offset().top - 10) }, "100000");
}
function close_chat_bubble()
{
	$("#support_heading_bubble").attr('style','display:none;');
}
function display_insurance_terms(flag)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'DISPLAY_INSURANCE_TERMS_CONDITION'},function(result){
		res_ary=result.split("||||");
		
		if(res_ary[0]=='SUCCESS')
		{ 
		    $("#insurance_terms_container").css('display','block');
		    $("#insurance_terms_container").html(res_ary[1]); 
		} 
		else
		{
			
		}	
		$("#loader").attr('style','display:none;');
	}); 
} 

function toggleLeftMenu()
{ 
    $(".menu").addClass("active");
    $("#sub-nav").animate({
            left: "0"
    });
    $(".sub-nav-overlay").show();
    $("body").css({overflow: "hidden"}); 
      
    $("#fourth-nav").addClass("active");
    $("#first-nav, #second-nav, #third-nav, #fifth-nav").removeClass("active");
}

function addInsuranceDetails_quotes(booking_key,flag)
{  
    if(flag=='REMOVE_INSURANCE_DETAILS_QUOTES')
    {
        flag = 'REMOVE_INSURANCE_DETAILS_QUOTES';  
        var szInvoiceTotal = $("#szInvoiceTotal_withoutinsurance").val();
        $("#insurance_price_yes").css('display','none');
        $("#insurance_price_no").css('display','block');
        
    }
    else
    {
        flag = 'ADD_INSURANCE_DETAILS_QUOTES';
        var szInvoiceTotal = $("#szInvoiceTotal_withinsurance").val();
        
        $("#insurance_price_no").css('display','none');
        $("#insurance_price_yes").css('display','block');
    } 
    $("#total_invoice_conatiner").html(szInvoiceTotal);
    
    $("#iInsuranceSelectedFlag").attr('value','1');

    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'UPDATE_QUOTE_INSURANCE',booking_key:booking_key,flag:flag},function(result){
        res_ary=result.split("||||");

        if(res_ary[0]=='SUCCESS')
        {  
            var isMobile = isMobileDevice();
            if(isMobile)
            {
                if($("#stripe_refersh").length)
                {
                    var stripe_refersh = $("#stripe_refersh").val(); 
                    if(stripe_refersh == '1')
                    {
                        $("#loader").attr('style','display:block');
                        window.location.reload();
                    }
                }
            }
        } 
        else if(res_ary[0]=='POPUP')
        {
           $("#change_price_div").html(res_ary[1]); 
           $("#change_price_div").attr('style','display:block');
        } 
        else
        {

        }	 
    });
}

function submit_courier_form(create_label)
{ 
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/courierService/ajax_courierService.php?create_label="+create_label,$("#calculate_courier_service_form").serialize(),function(result){
        var res_ary=result.split("||||"); 
        if(res_ary[0]=='SUCCESS')
        {  
            $("#courier_service_container_div").html(res_ary[1]); 
        }
        else if(res_ary[0]=='REDIRECT')
        {
            var redir_ul = jQuery.trim(res_ary[1]);
            redirect_url(redir_ul);
        } 
        else if(res_ary[0]=='ERROR')
        {
           $("#courier_service_container_div").html(res_ary[1]);  
        }  
        $("#loader").attr('style','display:none;');
    });
}

function beforeSubmitCheck()
{
    var error_count = 1;
    var formId = 'ups_courier_services_form' ;
    if(isFormInputElementEmpty(formId,'14_origCountry')) 
    {		 
        $("#14_origCountry").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#14_origCountry").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'origCity')) 
    {		 
        $("#origCity").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#origCity").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'15_origPostal')) 
    {		 
        $("#15_origPostal").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#15_origPostal").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'22_destCountry')) 
    {		 
        $("#22_destCountry").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#22_destCountry").removeClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'20_destCity')) 
    {		 
        $("#20_destCity").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#20_destCity").removeClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'19_destPostal')) 
    {		 
        $("#19_destPostal").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#19_destPostal").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'shipDate')) 
    {		 
        $("#shipDate").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#shipDate").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'47_rate_chart')) 
    {		 
        $("#47_rate_chart").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#47_rate_chart").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'48_container')) 
    {		 
        $("#48_container").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#48_container").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'23_weight')) 
    {		 
        $("#23_weight").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#23_weight").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'25_length')) 
    {		 
        $("#25_length").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#25_length").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty(formId,'26_width')) 
    {		 
        $("#26_width").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#26_width").removeClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'27_height')) 
    {		 
        $("#27_height").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#27_height").removeClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'23_weight')) 
    {		 
        $("#23_weight").addClass('red_border');
        error_count++;
    }
    else
    {
        $("#23_weight").removeClass('red_border');
    }
    
    if(error_count==1)
    {
        return true;
    }
    else
    {
        return false;
    } 
}  
function add_courier_service_rfq(booking_key,offer_type,lang,from_page)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",{lang:lang,operation:'GET_COURIER_RFQ',booking_key:booking_key,offer_type:offer_type,from_page:from_page},function(result){ 
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            var page_url = result_ary[1] ;
            $(location).attr('href',page_url);
        }
        else if(jQuery.trim(result_ary[0])=='ERROR')
        {
            //mark error here
            $("#contactPopup").html(result_ary[1]);
            $("#contactPopup").attr('style','display:block;'); 
        } 
    }); 
} 
function open_change_passward_form_popup()
{	  
	$.get(__JS_ONLY_SITE_BASE__+"/updatePassword.php",function(result){
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');
		$("#szNewPassword").focus();
		addPopupScrollClass("ajaxLogin");
	});
}

function updateMetricsPassword()
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/updatePassword.php",$("#changePassword").serialize(),function(result){
        $("#ajaxLogin").html(result);
        $("#ajaxLogin").attr('style','display:block;');
    });
}

function submit_cargo_details_form(lang)
{  
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php?lang="+lang,$("#cargo_details_form").serialize(),function(result){
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#update_cargo_details_container_div").html(result_ary[1]);
            $("#update_cargo_details_container_div").attr('style','display:none;');
            
            $("#booking_details_container").html(result_ary[2]);
            $("#booking_details_container").attr('style','display:block;');
            
            $("#iVolume").attr('value',result_ary[3]);
            $("#iWeight").attr('value',result_ary[4]);
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_UPDATE_SERVICE')
        {  
            $("#iVolume").attr('value',result_ary[3]);
            $("#iWeight").attr('value',result_ary[4]); 
            var idWtw = jQuery.trim(result_ary[5]);
             
            $("#select_services_tr_price_container_"+idWtw).html(result_ary[2]); 
            var booking_key = $("#szBookingRandomNum_hidden").val();
            
            var idWtw = jQuery.trim(result_ary[5]);
            var page_url = jQuery.trim(result_ary[6]);
            var user_id = jQuery.trim(result_ary[7]); 
                
            update_service_list_background(booking_key,idWtw,page_url,user_id);
            /* 
                $("#service-listing-container").html(result_ary[2]);         	
                $("#service-listing-container").attr('style','display:block');
                  
                toggleCourierServiceList(idWtw,page_url,user_id,1,1,1);
            */
            $("#booking_details_container").html(result_ary[1]);
            //$("#cargo_calculation_details_container").html(result_ary[1]);
            $("#booking_details_container").attr('style','display:block;'); 
            $("#update_cargo_details_container_div").attr('style','display:none;');
        }
        else if(jQuery.trim(result_ary[0])=='ERROR_UPDATE_SERVICE')
        {
            $("#search-result").html(result_ary[1]);
            $("#search-result").attr('style','display:block;');
            $("#update_cargo_details_container_div").attr('style','display:none;'); 
        }
        else if(jQuery.trim(result_ary[0])=='NO_RECORD_FOUND')
        {
            var redir_url = jQuery.trim(result_ary[1]);
            redirect_url(redir_url);
        } 
        else if(jQuery.trim(result_ary[0])=='ERROR_CARGO')
        {
            //mark error here
            $("#update_cargo_details_container_div").html(result_ary[1]);
            $("#update_cargo_details_container_div").attr('style','display:block;'); 
        }  
        $("#loader").attr('style','display:none');
    });
}

function update_service_list_background(booking_key,idWtw,page_url,user_id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",{operation:'UPDATE_SERVICE_LIST_AFTER_CARGO_CHANGE',booking_key:booking_key},function(result){ 
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#service-listing-container").html(result_ary[1]);         	
            $("#service-listing-container").attr('style','display:block'); 
            
            toggleCourierServiceList(idWtw,page_url,user_id,1,1,1);
            $("#booking_details_container").attr('style','display:block;'); 
            $("#update_cargo_details_container_div").attr('style','display:none;');
            
            var iShipperConsigneeSubmit = $("#iShipperConsigneeSubmit").val(); 
            if(iShipperConsigneeSubmit==2)
            {
                $("#iShipperConsigneeSubmit").val('1'); 
                submit_bookig_details();
            }
            else if(iShipperConsigneeSubmit==3)
            {
                $("#iShipperConsigneeSubmit").val('1'); 
            }  
            console.log("Background data updated");
        } 
    }); 
}

function display_more_service_result(page_number,sort_field,booking_key,lang)
{
    $("#iPageNumber").val(page_number);
    sort_service_list(sort_field,booking_key,lang);
//    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php?lang="+lang,{operation:'DISPLAY_MORE_SERVICE_LIST',booking_key:booking_key,page_number:page_number},function(result){
//        
//        var result_ary = result.split("||||");
//        if(jQuery.trim(result_ary[0])=='SUCCESS')
//        {            
//            $("#main-service-list-container").append(result_ary[1]); 
//        }
//        else if(jQuery.trim(result_ary[0])=='ERROR_CARGO')
//        {
//            //mark error here 
//        }  
//    }); 
}

function sort_service_list(sort_field,booking_key,lang)
{
    var szOldField = $("#szOldField").val();  
    var iPageNumber = $("#iPageNumber").val();
    
    if(szOldField==sort_field)
    {
        var szSortType = $("#szSortType").val(); 
        if(szSortType=='DESC')
        {
            var sort = 'ASC';
        }
        else
        {
            var sort = 'DESC';
        } 
    }
    else
    {
         var sort = 'DESC';
    }
    
    if(sort=='ASC')
    {
        var new_sort='DESC';
        var innr_htm = '<span class="sort-arrow-up">&nbsp;</span>';
    }
    else
    {
        var new_sort='ASC';
        var innr_htm = '<span class="sort-arrow-down">&nbsp;</span>';
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php?lang="+lang,{operation:'SORT_DISPLAYED_SERVICE_LIST',sort_by:sort,sort_field:sort_field,booking_key:booking_key,page_number:iPageNumber},function(result){
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {            
            $("#service-listing-container").html(result_ary[1]);
            $("#search-btn-container").removeClass('search-btn-clicked'); 
            
            $(".service-sorter-span").removeClass('sort-arrow-up');
            $(".service-sorter-span").removeClass('sort-arrow-down');
            
            if(sort=='ASC')
            {
                //$("#service_sorter_"+sort_field).addClass('sort-arrow-down');
            }
            else
            {
               // $("#service_sorter_"+sort_field).addClass('sort-arrow-up');
            }
            $("#szOldField").val(sort_field);
            $("#szSortType").val(sort);
        }
        else if(jQuery.trim(result_ary[0])=='ERROR_CARGO')
        {
            //mark error here
            $("#update_cargo_details_container_div").html(result_ary[1]);
            $("#update_cargo_details_container_div").attr('style','display:block;'); 
        }  
    });
}

function updateMetricsPassword()
{ 
	$.post(__JS_ONLY_SITE_BASE__+"/updatePassword.php",$("#changePassword").serialize(),function(result){
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');
	});
}

function downloadCourierLabelPDF()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_label.php",$("#courier_label_download_form").serialize(),function(result){
            var result_ary = result.split("||||");
            if(jQuery.trim(result_ary[0])=='SUCCESS')
            {
		$("#transporteca_search_form_container").html(result_ary[1]);
                //window.open(result_ary[2],'_blank');
                redirect_url(result_ary[2]);
            }
            else
            {
                $("#transporteca_search_form_container").html(result_ary[1]);
            }
	});
}

function updateCourierBookingStatus(idBooking,flag)
{
    if(flag=='Fed-Ex')
    {    
        $.post(__JS_ONLY_SITE_BASE__+"/fedexTrackWebServiceClient.php",{idBooking:idBooking},function(result){
           $("#courier_booking_status_"+idBooking).html(result);                                             
        });
    }
    else if(flag=='UPS')
    {
        $.post(__JS_ONLY_SITE_BASE__+"/upsTrackingApi.php",{idBooking:idBooking,courier_provider:flag},function(result){
           $("#courier_booking_status_"+idBooking).html(result);                                             
        });
    }
    else if(flag=='TNT')
    { 
        $.post(__JS_ONLY_SITE_BASE__+"/upsTrackingApi.php",{idBooking:idBooking,courier_provider:flag},function(result){
           $("#courier_booking_status_"+idBooking).html(result);                                             
        });
    }
}
 
function showHideCourierCal(id)
{
    var disp = $("#"+id).css('display');
    if(disp=='block')
    {
        $("#"+id).css('display','none');
       // removePopupScrollClass(id);
    }
    else
    {
        $("#"+id).css('display','block');
       // addPopupScrollClass(id);
    }
} 

function set_transporteca_cookies()
{
    $(".cookie-pop-up-msg").removeClass("cookie-pop-up-msg");
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",{mode:'SET_TRANSPORTECA_COOKIE'},function(result){		     
          
    });      
}

function lock_company_field(defualt_value,cb_flag)
{
    //var cb_flag = $('#iPrivateShipping').prop('checked');    
    var defualt_value = $("#szDefaultCompanyText").val();
    if(cb_flag)
    {
        $("#szShipperCompanyName").attr('disabled','disabled'); 
        $("#szShipperCompanyName").val(defualt_value);
        $("#szShipperCompanyName").removeClass('red_border');
    }
    else
    {
        $("#szShipperCompanyName").removeAttr('disabled');
        $("#szShipperCompanyName").val("");
    }
}
function focus_price_guarantee(div_id)
{
    $('html, body').animate({ scrollTop: ($("#"+div_id).offset().top - 10) }, "100000");
    //$('html, body').animate({ scrollTop: ($("#select_services_tr_"+idWTW).offset().top - 30) }, "100000");
}
function in_array(arr_value,dataAry)
{
    if(dataAry.length)
    {
        for(var i=0;i<dataAry.length;i++)
        {
            if(dataAry[i]==arr_value)
            {
                return true
            }
        }
        return false;
    }
    else
    {
        return false;
    }
}

function display_google_tag_manager(iPageNumber)
{  
}

function generate_service_list(booking_key,page_version)
{
    $(".get-rate-quote-submit-button").unbind("click"); 
    $(".get-rate-quote-submit-button").attr("style","opacity:0.4;"); 
    //$(".get-rate-quote-submit-button").addClass('get-price-button-clicked');
    
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",{mode:'GENERATE_SERVICE_LISTING',booking_key:booking_key},function(result){	 
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS' && page_version!=3)
        { 
           $("#forwarder_count").html(result_ary[1]);
           $("#service-available-container").html(result_ary[2]);
           $("#get-rate-page-heading").html(result_ary[3]); 
           $("#service-available-container").attr('style','display:block;');  
        }
        
        if(page_version==3)
        {
           var calculate_flag = $("#calculate_service_list_flag").val();
           calculate_flag = parseInt(calculate_flag);
           if(calculate_flag==2)
           {
               var mode = 'GET_RATES';
               submit_bookig_quote_contact_details(mode);
           }
           else
           {
               $("#calculate_service_list_flag").val('0');
           }
        }
        $(".get-rate-quote-submit-button").unbind("click"); 
        $(".get-rate-quote-submit-button").attr("style","opacity:1;"); 
        //$(".get-rate-quote-submit-button").removeClass('get-price-button-clicked');
        $(".get-rate-quote-submit-button").click(function(){ validate_booking_quote_form('booking_quote_contact_detail_form','GET_RATES'); });
    });      
}

function hideDatePicker()
{
    $(".hasDatepicker").on("blur", function(e) { $(this).datepicker("hide"); });
}

function lock_company_field_myaccount()
{
    var cb_flag = $('#iPrivate').prop('checked');    
    if(cb_flag)
    {
        $("#szCompanyName").attr('disabled','disabled');  
        $("#szCompanyRegNo").attr('disabled','disabled');  
        
        $("#szCompanyName").attr('value','');
        $("#szCompanyRegNo").attr('value','');
        
        $("#szCompanyName").css('background-color','rgb(211, 211, 211)');  
        $("#szCompanyRegNo").css('background-color','rgb(211, 211, 211)');  
    }
    else
    {
        $("#szCompanyName").removeAttr('disabled'); 
        $("#szCompanyRegNo").removeAttr('disabled'); 
        
        $("#szCompanyRegNo").css('background-color','');  
        $("#szCompanyName").css('background-color','');  
    }
}

function display_search_mini_pages(page_version,seo_page_id)
{ 
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'DISPLAY_SEARCH_MINI_VERSIONS',page_version:page_version,seo_page_id:seo_page_id},function(result){
        var res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        {
            $("#search_mini_global_conatiner_"+page_version).html(res_ary[1]); 
        } 
    });
}

function display_pallet_details_popup(search_mini_version)
{
    var iNumPallets = $("#iNumColli_V_"+search_mini_version).val(); 
    var szPalletDimensions = $("#szPalletDimensions_V_"+search_mini_version).val(); 
    iNumPallets = parseInt(iNumPallets); 
    if(iNumPallets<=0)
    {
        iNumPallets = 1;
    }
    $("#loader").attr('style','display:block;');
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'DISPLAY_PALLETS_POPUP',page_version:search_mini_version,num_pallets:iNumPallets,pallet_dimensions:szPalletDimensions},function(result){
        var res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        {
            $("#ajaxLogin").html(res_ary[1]); 
            $("#ajaxLogin").attr('style','display:block;');
        }
        $("#loader").attr('style','display:none;');
    });
}

function validate_pallet_fields(kEvent,search_mini_version)
{
    var id_suffix = "_POP_V_"+search_mini_version;
    var num_cargo_line = $("#iPalletCounter"+id_suffix).attr('value');
    var empty_flag = 1 ;
    for(var i=1;i<=num_cargo_line;i++)
    {
        var id_Suffix_loop = i+"_POP_V_"+search_mini_version;
        if($("#iLength"+id_Suffix_loop).length)
        {
            var length = $("#iLength"+id_Suffix_loop).attr('value');
            var width = $("#iWidth"+id_Suffix_loop).attr('value'); 
            
            console.log("ID: "+id_Suffix_loop+" Length: "+length+" Width: "+width);

            length = parseInt(length);
            width = parseInt(width); 
            if(isNaN(length) || isNaN(width))
            {
                empty_flag = 2;
                break;
            }
            else if((length <= 0) || (width<=0))
            {
                empty_flag = 2;
                break;
            }
        } 
    }  
    var formId = "pallet_details_form"+id_suffix ;
    if(empty_flag==1)
    {
//        if(kEvent)
//        {
//            if(kEvent.keyCode == 13)
//            {
//                submit_pallet_details_form(formId,search_mini_version);
//            }
//        }
        
        $("#pallet_popup_submit"+id_suffix).unbind("click");
        $("#pallet_popup_submit"+id_suffix).click(function(){ submit_pallet_details_form(formId,search_mini_version) });
        //$('#pallet_popup_submit'+id_suffix).attr('class','orange-button1');
        $("#pallet_popup_submit"+id_suffix).attr("style","opacity:1;");
    }
    else
    {
        $("#pallet_popup_submit"+id_suffix).unbind("click");
        //$('#pallet_popup_submit'+id_suffix).attr('class','gray-button1');
        $("#pallet_popup_submit"+id_suffix).attr("style","opacity:0.4;"); 
    }
}

function submit_pallet_details_form(formId,search_mini_version)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$("#"+formId).serialize(),function(result){
        
        var res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        { 
            $("#szPalletDescriptions_V_"+search_mini_version).val(res_ary[1]); 
            $("#iNumColli_V_"+search_mini_version).val(res_ary[2]); 
            $("#iNumColli_V_"+search_mini_version).attr('readonly','readonly');
            $("#szPalletDimensions_V_"+search_mini_version).val(res_ary[3]);
            $("#ajaxLogin").attr('style','display:none;');
        }
        else if(res_ary[0]=='ERROR')
        {
            $("#ajaxLogin").html(res_ary[1]); 
            $("#ajaxLogin").attr('style','display:block;');
        }
        $("#loader").attr('style','display:none;');
    });
}
function update_cargo_type(category_id,counter,search_mini,type,sub_category_id,admin_flag)
{   
    var idsuffix = "_V_"+search_mini 
    if(type=='CATEGORY')
    { 
        var createCategoryAry = __JS_ONLY_VOGA_CATEGORY_LISTING__;  
    
        var szTypeText = $('#voga_sub_category_type_field').val();   
        var select_field_id = 'idStandardCargoType'+counter+idsuffix; 
        var $el = $("#"+select_field_id);
        $el.empty(); // remove old options
        
        $("#select"+select_field_id).html(szTypeText);
        $el.append($("<option></option>").attr("value", '').text(szTypeText));
            
        category_id = parseInt(category_id);
        console.log(category_id);
        console.log(createCategoryAry);
        if(category_id>0 && createCategoryAry[category_id].length)
        {  
            var subCategoryAry = createCategoryAry[category_id]; 
            var iAryLength = subCategoryAry.length; 
            if(iAryLength)
            { 
                var $el = $("#"+select_field_id);
                $el.empty(); // remove old options
                 
                $("#select"+select_field_id).html(szTypeText);
                $el.append($("<option></option>").attr("value", '').text(szTypeText)); 
                for(var i=0;i<iAryLength;i++)
                {  
                    $el.append($("<option></option>")
                     .attr("value", subCategoryAry[i]['id']).text(subCategoryAry[i]['szLongName']));
                }
            } 
        } 
    } 
    else if(type=='SUBCATEGORY')
    {
        /*
         *  No longer in use
        var createCategoryAry = __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__; 
        if(category_id>0)
        { 
            var subCategoryAry = createCategoryAry[category_id];          
            var colli_field_id = 'iQuantity'+counter+idsuffix;  
            console.log(subCategoryAry);
              
            var iColli = subCategoryAry['iColli']; 
            console.log("ID: "+colli_field_id+" counter: "+counter+" Val: "+iColli); 
            if(iColli>0)
            {
                $('#'+colli_field_id).val(iColli); 
            } 
            else
            {
                var iColli = '';
                $('#'+colli_field_id).val(iColli); 
            }
        } */
    }
    
    /*
     * Following code was used to load subcategory menu data by ajax call
    var select_field_id = 'idStandardCargoType'+counter+"_V_"+search_mini;  
    var $el = $("#"+select_field_id);
    $el.empty(); // remove old options
     
    console.log(__JS_ONLY_VOGA_CATEGORY_LISTING__);
    
    var szTypeText = $('#voga_sub_category_type_field').val();
    var lang = $("#iSiteLanguageVogaPages_hidden").val(); 
    $("#select"+select_field_id).html(szTypeText);
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_showCargo.php",{category_id:category_id,counter:counter,search_mini:search_mini,lang:lang,mode:'DISPLAY_STANDARD_CARGO_TYPE'},function(result){
        
        var res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        {
            var container_id = "idStandardCargoType"+counter+"_V_"+search_mini+"_container"; 
            $("#"+container_id).html(res_ary[1]);   
        }
    });
    */
}

function stripeMethodRecalled(number)
{
    var diff = 0;
    var previousDate = new Date();

    while(diff < 5)
    {
        var nextDate = new Date();
        diff = ((nextDate-previousDate)/1000).toString();

        var data = $("#stripe_response").val();
        if(data != '')
        {
            var res_ary = data.split("||||");
            if(res_ary[0]=='SUCCESSZO0Z')
            {
                var szApiKey = res_ary[1];
                var fChargeableAmount = res_ary[2];
                var szCurrency = res_ary[3];
                var szDescription = res_ary[4]; 
                var szEmail = res_ary[5]; 
                callStripeApi(szApiKey,fChargeableAmount,szCurrency,szDescription,szEmail);
                break;
            }
            else if(res_ary[0] !='SUCCESSZO0Z')
            {
                break;
            }
        }
    }
}

function stripeMethodRecalled(number)
{
	if(number < 5)
	{
		setTimeout(function(){
			var data = $("#stripe_response").val();
			if(data != '')
			{
				var res_ary = data.split("||||");
				if(res_ary[0]=='SUCCESSZO0Z')
				{
					var szApiKey = res_ary[1];
					var fChargeableAmount = res_ary[2];
					var szCurrency = res_ary[3];
					var szDescription = res_ary[4]; 
					var szEmail = res_ary[5]; 
					callStripeApi(szApiKey,fChargeableAmount,szCurrency,szDescription,szEmail);
				}
			}
			else
			{
				var nextNum = parseInt(number) + 1; 
				stripeMethodRecalled(nextNum);
			}

		}, 1000);
	}
}

function isMobileDevice()
{
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    }; 
    return isMobile.any() ; 
}

function submit_partner_api_demo_form()
{ 
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/partner/ajax_partnerDemo.php",$("#get_api_service_form").serialize(),function(result){
        var res_ary=result.split("||||"); 
        if(res_ary[0]=='SUCCESS')
        {  
            $("#api_response_container").html(res_ary[1]); 
            $("#api_response_container").attr('style','display:inline-block;');
        }
        else if(res_ary[0]=='REDIRECT')
        {
            var redir_ul = jQuery.trim(res_ary[1]);
            redirect_url(redir_ul);
        } 
        else if(res_ary[0]=='ERROR')
        {
           $("#courier_service_container_div").html(res_ary[1]);  
        }  
        $("#loader").attr('style','display:none;');
    });
} 

function submit_mautic_api_demo_form()
{ 
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$("#get_api_service_form").serialize(),function(result){
        var res_ary=result.split("||||"); 
        if(res_ary[0]=='SUCCESS')
        {  
            $("#api_response_container").html(res_ary[1]); 
            $("#api_response_container").attr('style','display:inline-block;');
        } 
        else if(res_ary[0]=='ERROR')
        {
           $("#courier_service_container_div").html(res_ary[1]);  
        }  
        $("#loader").attr('style','display:none;');
    });
} 
function heighLightSelectedMenu(menu_id)
{
    $(".section").removeClass('active-section');
    $("."+menu_id).addClass('active-section');
}

function updateEmailNotificationStatus(idBooking,iSendTrackingUpdates)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_myBooking.php",{mode:"__UPDATE_EMAIL_NOTIFICATION_STATUS__",idBooking:idBooking,iSendTrackingUpdates:iSendTrackingUpdates},function(result){
//        alert(result);
        if(parseInt(iSendTrackingUpdates) == 0)
        {
            $("#cmn-toggle-"+idBooking).attr('checked','checked');
            $("#cmn-toggle-"+idBooking).val(1);
        }
        else
        {
            $("#cmn-toggle-"+idBooking).removeAttr('checked');
            $("#cmn-toggle-"+idBooking).val(0);
        }
    });
}

function display_email_notification_update_popup(booking_key)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_myBooking.php",{mode:"__UPDATE_EMAIL_NOTIFICATION_POP_UP__",booking_key:booking_key},function(result){
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=="SUCCESS_POPUP")
        {
            $("#cmn-toggle-"+booking_key).removeAttr('checked');
            $("#email_notification_description_div").html(result_ary[1]);
            $("#email_notification_description_div").attr('style','display:block');
        }
    });
}

//function for after ship tracking demo page

function submit_get_tracking_detail_form(mode)
{
    var value=$('#get_tracking_detail_form').serialize();
    
    var newvalue = value+"&mode="+mode;

    $.post(__JS_ONLY_SITE_BASE__+"/after_ship/ajax_trackingDetail.php",newvalue,function(result){
	
        var result_ary = result.split("||||");
        
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#courier_tracking_result_div").html(result_ary[1]);
        }
    });
}

function resetValueFromTextBoxs(divid,id_suffix)
{
    var dividRemoveIcon=divid+""+id_suffix+""+"_clear_box";
    console.log("dividRemoveIcon"+dividRemoveIcon);
    $('#'+divid+""+id_suffix).attr('value','');
    $("#search_listing"+id_suffix).unbind("click");
    $("#search_listing"+id_suffix).removeAttr("onclick");
    $("#search-btn-container"+id_suffix).removeClass('btn');
    $("#search-btn-container"+id_suffix).addClass('btn-grey');
    $("#"+dividRemoveIcon).attr('style','display:none');
}


function update_invoice_comment_billing(idBilling)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookings.php",{mode:'UPDATE_YOUR_REFERENCE_BILLING',idBilling:idBilling},function(result){
        $("#invoice_comment_div").html(result);
        $("#invoice_comment_div").attr('style','display:block;');
    });
}


function  update_invoice_billing_comment_confirm(idBilling)
{	
    var szBookingNotes=$("#szBookingNotes").val();
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bookings.php",{mode:'ADD_UPDATE_YOUR_REFERENCE_BILLING',idBilling:idBilling,szBookingNotes:szBookingNotes},function(result){
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#invoice_comment_"+idBilling).html(result_ary[1]);
			$("#invoice_comment_div").attr('style','display:none;');
		}
		else
		{
			$("#invoice_comment_"+idBilling).html(result_ary[1]);
			$("#invoice_comment_div").attr('style','display:none;');
		}	
		
	});
}

function updateszCompanyName(szFlag)
{
    if(szFlag=='Shipper')
    {
        var szShipperFirstName=$("#szShipperFirstName").attr('value');
        var szShipperLastName=$("#szShipperLastName").attr('value');
        var szCompanyName=szShipperFirstName+' '+szShipperLastName;
        $("#szShipperCompanyName").attr('value',szCompanyName);
    }
    else if(szFlag=='Consignee')
    {
        var szConsigneeFirstName=$("#szConsigneeFirstName").attr('value');
        var szConsigneeLastName=$("#szConsigneeLastName").attr('value');
        var szCompanyName=szConsigneeFirstName+' '+szConsigneeLastName;
        $("#szConsigneeCompanyName").attr('value',szCompanyName);
    }
    else if(szFlag=='Billing')
    {
        var szBillingFirstName=$("#szBillingFirstName").attr('value');
        var szBillingLastName=$("#szBillingLastName").attr('value');
        var szCompanyName=szBillingFirstName+' '+szBillingLastName;
        $("#szBillingCompanyName").attr('value',szCompanyName);
    }
}

function signupWithGooglePlus(szUrl)
{
    $.post(__JS_ONLY_SITE_BASE__+"/googleLogin/index.php",{szUrl:szUrl,iFlag:'1'},function(result){
		redirect_url(result);	
		
	});
}


function signupWithFacebook(szUrl)
{
    $.post(__JS_ONLY_SITE_BASE__+"/fblogin/index.php",{szUrl:szUrl,iFlag:'1'},function(result){
		redirect_url(result);	
		
	});
}

function fillAddressForIPAddress(idTextBox)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'GET_ADDRESS_FORM_IPADDRESS'},function(result){
        $("#"+idTextBox).attr('value',result);
    });
}


function swapFromToTextFieldValue(idValue)
{
    var szDestinationCountryStr =$("#szDestinationCountryStr"+idValue).attr('value');
    var szOriginCountryStr =$("#szOriginCountryStr"+idValue).attr('value');
    
    $("#szDestinationCountryStr"+idValue).attr('value',szOriginCountryStr);
     $("#szOriginCountryStr"+idValue).attr('value',szDestinationCountryStr);
}

function enableDiableVogaPageNewDeliveryAddress(id)
{
    var disp = $("#"+id).css('display');
    if(disp=='block')
    {
        $("#"+id).attr('style','display:none');
    }
    else
    {
        $("#"+id).attr('style','display:block');
    }
} 

function continueSearching(idBooking,szCountryStrUrl,iNumRecordFound,iSearchMiniPageVersion)
{
    //alert(iSearchMiniPageVersion);
     var iHiddenChanged=$("#iHiddenChanged").val();
    
     if(parseInt(iSearchMiniPageVersion)>0)
     {
         $("#search-btn-container_V_"+iSearchMiniPageVersion).addClass('search-btn-clicked mobile-view');
     }
     else
     {
        if(parseInt(iHiddenChanged)==1)
        {
            $("#search-btn-container_hidden").addClass('search-btn-clicked');
        }else{
            $("#search-btn-container").addClass('search-btn-clicked');
        }
    }
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'__CONTINUE_SEARCHING__',idBooking:idBooking,szCountryStrUrl:szCountryStrUrl,iNumRecordFound:iNumRecordFound},function(result){
        redirect_url(result);
    });
}

function search_for_notification(szFlag,iHiddenValue,iSearchMiniPage)
{
    var iSearchMiniPage =$("#"+szFlag+" #iSearchMiniPage").attr('value');
    //alert(formId);
    if(parseInt(iSearchMiniPage)==5)
    {
        $("#search-btn-container_V_"+iSearchMiniPage).addClass('search-btn-clicked');
        $("#search-btn-container_V_"+iSearchMiniPage).addClass('mobile-view');
    }
    else
    {
        $("#search-btn-container").addClass('search-btn-clicked');
        $("#search-btn-container").addClass('mobile-view');
    }
    var szBookingRandomNum=$("#szBookingRandomNum").val();
    var value=$("#"+szFlag).serialize();
    var newvalue=value+"&szBookingRandomNum="+szBookingRandomNum+"&szFlag="+szFlag+"&iHiddenValue="+iHiddenValue+"&mode=__SHOW_SEARCH_NOTIFICATION_POPUP__&iSearchMiniPage="+iSearchMiniPage;
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",newvalue,function(result){
        var result_ary = result.split("||||");
        if(result_ary[0]=='SUCCESS_LOCAL_SEARCH')
        {
            $("#regError").attr('style','display:none;');
            $("#ajaxLogin").html(result_ary[1]);
            $("#ajaxLogin").attr('style','display:block');
            
            if(parseInt(iSearchMiniPage)==5)
            {
                $("#search-btn-container_V_"+iSearchMiniPage).removeClass('search-btn-clicked');
                $("#search-btn-container_V_"+iSearchMiniPage).removeClass('mobile-view');
            }
            else
            {
                $("#search-btn-container").removeClass('search-btn-clicked');
                $("#search-btn-container").removeClass('mobile-view');
            }
         
        }
        else
        {
            update_service_listing(szFlag,iHiddenValue);
        }
    });
}

function calculate_voga_search(idBooking)
{ 
    var szAjaxUrl = __JS_ONLY_SITE_BASE__+"/ajax_misc.php";
    $.ajax({
        url: szAjaxUrl,
        global: false,
        type: 'POST',
        data: {mode:"CREATE_VOGA_AUTOMATED_BOOKINGS",idBooking:idBooking},
        async: true, //blocks window close
        success: function(result) {
            var res_ary=result.split("||||") ;
            if(jQuery.trim(res_ary[0])=='SUCCESS')
            { 
                //$("#haulage_zone_countries_calculation_logs").html(res_ary[1]);
                //$("#haulage_zone_countries_calculation_logs").attr('style','display:block');
            }
        }
    });
    /*
    console.log("Called voga...");
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'CREATE_VOGA_AUTOMATED_BOOKINGS',idBooking:idBooking},function(result){
        var res_ary=result.split("||||") ;
        if(res_ary[0]=='SUCCESS')
        {
            //redirect_url(res_ary[1]);
        }
        else
        {

        }	
    });
    */
    
} 
function calculateDutyRate(szFlag)
{
    var validation_success = 1;
    var error = 1;
    var formId = 'dutyCalculatorForm' ;
    if(isFormInputElementEmpty(formId,"fProductRate")) 
    {		
        error++;  
        //displayFormFieldIsRequired("fProductRate");
        $("#fProductRate").addClass('red_border');
    }
    else
    {
        $("#fProductRate").removeClass('red_border');
        $("#fProductRate").attr('style','');
    }
    
    if(isFormInputElementEmpty(formId,"idGoodCurrency")) 
    {		
        error++;  
        $("#idGoodCurrency").addClass('red_border');
    }
    else
    {
        $("#idGoodCurrency").removeClass('red_border');
        $("#idGoodCurrency").attr('style','');
        
//        if(szFlag=='GoodCurrencyFlag')
//        {
//            var idGoodCurrencyValue=$("#idGoodCurrency").val();
//            $("#idLoadCurrency").attr('value',idGoodCurrencyValue);
//        }
    }
    
    if(isFormInputElementEmpty(formId,"fGoodPrice")) 
    {		
        error++;  
        $("#fGoodPrice").addClass('red_border');
    }
    else
    {
        $("#fGoodPrice").removeClass('red_border');
        $("#fGoodPrice").attr('style','');
    }
    
    if(isFormInputElementEmpty(formId,"idLoadCurrency")) 
    {		
        error++;  
       $("#idLoadCurrency").addClass('red_border');
    }
    else
    {
        $("#idLoadCurrency").removeClass('red_border');
        $("#idLoadCurrency").attr('style','');
//        if(szFlag=='LoadCurrencyFlag')
//        {
//            var idLoadCurrencyValue=$("#idLoadCurrency").val();
//            $("#idGoodCurrency").attr('value',idLoadCurrencyValue);
//        }
        
    }
    var idLoadCurrencyValue=$("#idLoadCurrency").val();
    if(isFormInputElementEmpty(formId,"fLoadPrice")) 
    {		
        error++;  
        $("#fLoadPrice").addClass('red_border');
    }
    else
    {
        $("#fLoadPrice").removeClass('red_border');
        $("#fLoadPrice").attr('style','');
    }
    
    
    if(parseInt(error)==1)
    {
       var idGoodResultCurrency = $("#idGoodResultCurrency").val();
       var value =$("#dutyCalculatorForm").serialize();  
       var newValue = value+"&mode=CALCULATE_DUTY_VALUE&idGoodResultCurrency="+idGoodResultCurrency;
       $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",newValue,function(result){
       var res_ary=result.split("||||");
        $("#percentage-text").html(res_ary[0]); 
        $("#price-text").html(res_ary[1]); 

      });  
  }
  else
  {
     $("#percentage-text").html(""); 
     $("#price-text").html("");  
  }
}


function duty_calculator_show()
{ 
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'SHOW_DUTY_CALCULATOR'},function(result){
       
        $("#duty_calculator").html(result); 
        
    });
}

function showHideCountryDropDown()
{
    var disp = $(".flag-dropdown").css('display');
    if(disp=='block')
    {
       $(".flag-dropdown").attr('style','display:none');
    }
    else
    {
        $(".flag-dropdown").attr('style','display:block');
    }
}


function getDownloadableWarehouseCount(originWhsAry,destinationWhsAry)
{
    /* 
    * Filtering CFS and Airport warehouse list from the Origin Warehouse list
    */ 
    var originLclCfsListAry = new Array();
    var originAirportListAry = new Array();

    var destinationLclCfsListAry = new Array();
    var destinationAirportListAry = new Array();

    var originWarehouseListAry = filterWarehouseList(originWhsAry); 
    var destinationWarehouseListAry = filterWarehouseList(destinationWhsAry); 

    originLclCfsListAry = originWarehouseListAry['1'];
    originAirportListAry = originWarehouseListAry['2'];

    destinationLclCfsListAry = destinationWarehouseListAry['1'];
    destinationAirportListAry = destinationWarehouseListAry['2'];

    var iNumOriginLclCfs = originLclCfsListAry.length;
    var iNumOriginAirport = originAirportListAry.length;

    var iNumDestinationLclCfs = destinationLclCfsListAry.length;
    var iNumDestinationAirport = destinationAirportListAry.length;
 
    var totalCount = ((parseInt(iNumOriginLclCfs) * parseInt(iNumDestinationLclCfs)) + (parseInt(iNumOriginAirport) * parseInt(iNumDestinationAirport)));
    return totalCount;
}

function checkRowCountCC(maxrowexcced)
{
    var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
    var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
    var res_ary_originwarhousevalue = originWarehouseValueTotal.split(";");
    var res_ary_deswarhousevalue = desWarehouseValueTotal.split(";");
    var deswarehouselen='0';
    var originwarehouselen='0';
    if(res_ary_deswarhousevalue!='')
    {
        var deswarehouselen=res_ary_deswarhousevalue.length;
    }
    if(res_ary_originwarhousevalue!='')
    {
        var originwarehouselen=res_ary_originwarhousevalue.length;	
    }
    var mainurl = $("#mainurl").attr('value');
    var changeDropDown = $("#changeDropDown").attr('value');
    if(originwarehouselen>0 && deswarehouselen>0)
    { 
        var totalCount = getDownloadableWarehouseCount(res_ary_originwarhousevalue,res_ary_deswarhousevalue);
        //totalCount = parseInt(originwarehouselen)*parseInt(deswarehouselen);
        if(totalCount>maxrowexcced)
        {
            $("#download_button").unbind("click");
            $("#download_button").attr("style","opacity:0.4;");

            //$("#total_row_excced").html(number_format_js(totalCount));

            if(totalCount>changeDropDown)
            {
                $("#downloadType").val("1");
            }
            updatedNoteCC(maxrowexcced);
        }
        else
        {
            if(totalCount>changeDropDown)
            {
                $("#download_button").unbind("click");
                $("#download_button").attr("style","opacity:0.4;");
                $("#downloadType").val("1");
                updatedNoteCC(maxrowexcced);
            }
            else
            {
                var totaltime=(__TIME_PER_ROW_CREATE__*totalCount);
                if(totaltime>60)
                {
                    var minutes = Math.floor(totaltime/60);
                    var secondsleft = totaltime%60;
                    secondsleft = Math.ceil(secondsleft);
                    if(minutes=='1')
                    {
                        var minStr=minutes+" minute"; 
                    }
                    else
                    {
                        var minStr=minutes+" minutes"; 
                    }
                    if(secondsleft=='1')
                    {
                        var secstr=secondsleft+" second";
                    }
                    else
                    {
                        var secstr=Math.ceil(secondsleft)+" seconds";
                    } 
                    var timestr=minStr+" "+secstr;
                }
                else
                {
                    var timetaken=Math.ceil(totaltime)
                    if(timetaken=='1')
                    {
                        var timestr=timetaken+" second";
                    }
                    else
                    {
                        var timestr=timetaken+" seconds";
                    }
                }
                $("#total_row_excced_time").html((timestr));
                $("#row_excced_data").attr("style","display:none");
                $("#row_excced_data_1").attr("style","display:none");
                $("#row_excced_data_2").attr("style","display:block");
                $("#total_row_excced_1").html(number_format_js(totalCount));
            }
        }
    }
}

function filterWarehouseList(warehouseAry)
{
    var lclCfsAry = new Array();
    var airportWarehouseAry = new Array();
    var iWhsArrLength = warehouseAry.length;
    for(var i=0;i<iWhsArrLength;i++)
    {
        if(warehouseAry[i]!='')
        {
            var whsCountryAry = warehouseAry[i].split("_");
            var idOriginWarehouse = whsCountryAry[0]; 
            if(FORWARDER_WAREHOUSE_LIST[idOriginWarehouse]>0)
            {
                var iWarehouseType = FORWARDER_WAREHOUSE_LIST[idOriginWarehouse]; 
                if(iWarehouseType==1)
                {
                    lclCfsAry.push(idOriginWarehouse);
                }
                else if(iWarehouseType==2)
                {
                    airportWarehouseAry.push(idOriginWarehouse);
                }
            }
        } 
    }  
    var ret_ary = new Array();
    ret_ary['1'] = lclCfsAry;
    ret_ary['2'] = airportWarehouseAry;
    return ret_ary;
}

function updatedNoteCC(maxrowexcced)
{
		
		$("#row_excced_data").attr("style","display:none");
		var mainurl = $("#mainurl").attr('value');
		$("#row_excced_data").attr("style","display:none");
		$("#row_excced_data_2").attr("style","display:none");
		$("#row_excced_data_1").attr("style","display:block;text-align:left;valign:middle;");
		$("#download_button").unbind("click");
		$("#download_button").attr("style","opacity:0.4;");
		var value= $("#bulkExportData").serialize();
		$("#row_excced_data_1").html("<span class='fl-15'>Note: Loading....</span><img style='margin: 0 0 0 10px;float:left;position:static;' src='"+mainurl+"/images/325.gif' alt='' />");
		$("#downloadType").attr("disabled","disabled");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",value,function(result){
		
		
	 		result_ary = result.split("||||");
	 
		 	if(parseInt(result_ary[1])>parseInt(maxrowexcced))
		 	{
			 	$("#row_excced_data").attr("style","display:block;");
			 	$("#total_row_excced").html(number_format_js(result_ary[1]));
			 	$("#row_excced_data_1").attr("style","display:none");
			 	$("#row_excced_data_2").attr("style","display:none");
			 }
			 else
			 {
			 	var totaltime=(__TIME_PER_ROW_CREATE__*result_ary[1]);
			 	if(totaltime>60)
			 	{
                                    var minutes = Math.floor(totaltime/60);
                                    var secondsleft = totaltime%60;
                                    secondsleft = Math.ceil(secondsleft);
                                    if(minutes=='1')
                                    {
                                        var minStr=minutes+" minute"; 
                                    }
                                    else
                                    {
                                            var minStr=minutes+" minutes"; 
                                    }
                                    if(secondsleft=='1')
                                    {
                                        var secstr=secondsleft+" second";
                                    }
                                    else
                                    {
                                        var secstr=secondsleft+" seconds";
                                    }

                                    var timestr=minStr+" "+secstr;
			 	}
			 	else
			 	{
                                    var timetaken=Math.ceil(totaltime)
                                    if(timetaken=='1')
                                    {
                                        var timestr=timetaken+" second";
                                    }
                                    else
                                    {
                                        var timestr=timetaken+" seconds";
                                    }
			 	}
			 	$("#row_excced_data_2").attr("style","display:block;");
			 	$("#total_row_excced_1").html(number_format_js(result_ary[1]));
			 	$("#total_row_excced_time").html((timestr));
			 	$("#row_excced_data_1").attr("style","display:none");
			 	$("#row_excced_data").attr("style","display:none");
			 	$("#download_button").click(function(){downloadFormSubmitCustomClearance()});
				$("#download_button").attr("style","opacity:1;");
			 }
			 $('#downloadType').removeAttr('disabled');
	 	});
}
 
function changeDownloadTypeCC(maxrowexcced)
{
	var downloadType = $("#downloadType").val();
	if(downloadType=='2')
	{
            var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
            var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
            var res_ary_originwarhousevalue = originWarehouseValueTotal.split(";");
            var res_ary_deswarhousevalue = desWarehouseValueTotal.split(";");
            var deswarehouselen=res_ary_deswarhousevalue.length;
            var originwarehouselen=res_ary_originwarhousevalue.length;	
            if(originwarehouselen>1 && deswarehouselen>1)
            {
                //totalCount=parseInt(originwarehouselen)*parseInt(deswarehouselen);
                var totalCount = getDownloadableWarehouseCount(res_ary_originwarhousevalue,res_ary_deswarhousevalue);
                if(totalCount>maxrowexcced)
                {
                    $("#row_excced_data_2").attr("style","display:none");
                    $("#row_excced_data_1").attr("style","display:none");
                    $("#download_button").unbind("click");
                    $("#download_button").attr("style","opacity:0.4;");
                    $("#row_excced_data").attr("style","display:block");
                    $("#total_row_excced").html(number_format_js(totalCount));

                }
                else
                {
                    var totaltime = (__TIME_PER_ROW_CREATE__*totalCount);
                    if(totaltime>60) 
                    {
                        var minutes = Math.floor(totaltime/60);
                        var secondsleft = totaltime%60;
                        secondsleft = Math.ceil(secondsleft);
                        if(minutes=='1')
                        {
                            var minStr=minutes+" minute"; 
                        }
                        else
                        {
                            var minStr=minutes+" minutes"; 
                        }
                        if(secondsleft=='1')
                        {
                            var secstr=secondsleft+" second";
                        }
                        else
                        {
                            var secstr=Math.ceil(secondsleft)+" seconds";
                        } 
                        var timestr=minStr+" "+secstr;
                    }
                    else
                    {
                        var timetaken=Math.ceil(totaltime)
                        if(timetaken=='1')
                        {
                            var timestr=timetaken+" second";
                        }
                        else
                        {
                            var timestr = timetaken+" seconds";
                        }
                    }
                    $("#total_row_excced_time").html((timestr));
                    $("#row_excced_data").attr("style","display:none");
                    $("#row_excced_data_1").attr("style","display:none");
                    $("#row_excced_data_2").attr("style","display:block");
                    $("#total_row_excced_1").html(number_format_js(totalCount));
                }
            }
	}
	else if(downloadType=='1')
	{
            var desWarehouseValueTotal = $("#idDesWarehouse").attr('value');
            var originWarehouseValueTotal = $("#idOriginWarehouse").attr('value');
            if(originWarehouseValueTotal!='' && desWarehouseValueTotal!='')
            { 
                updatedNoteCC(maxrowexcced); 
            }
	}
}


function serch_forwarder_invoice_booking()
{
	document.forwarder_booking_seach_form.action = __JS_ONLY_SITE_BASE__+"/bookingInvoiceList.php";
	document.forwarder_booking_seach_form.method = "post"
	document.forwarder_booking_seach_form.submit();
}



function serch_transporteca_invoice_booking()
{
	document.transporteca_all_invoice_seach_form.action = __JS_ONLY_SITE_BASE__+"/downloadTransportecaInvoice.php";
	document.transporteca_all_invoice_seach_form.method = "post"
	document.transporteca_all_invoice_seach_form.submit();
}


function showhideDropdownFromSearchForm(szOriginCountryCode,szDestinationCountryCode,id_suffix)
{
    var idServiceType=$("#idServiceType").attr('value');
    var idCustomerCountry=$("#idCustomerCountry").attr('value');
    var iSiteLanguageFooter=$("#iSiteLanguageFooter_hidden").attr('value');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php?lang="+iSiteLanguageFooter,{id_suffix:id_suffix,mode:'SHOW_HIDE_DROP_DWON_IN_SEARCH_FROM',szOriginCountryCode:szOriginCountryCode,szDestinationCountryCode:szDestinationCountryCode,idCustomerCountry:idCustomerCountry,idServiceType:idServiceType},function(result){
       var res_ary=result.split("||||") ;
      var formid="landing_page_form"+id_suffix;
      
        if(res_ary[0]=='SHOWDROPDOWN')
        {
            $("#landing_page_form #iPickupDefaultValue_option1").attr('value',res_ary[1]);
            $("#landing_page_form #iPickupDefaultValue_option2").attr('value',res_ary[2]);
            $("#landing_page_form #iPickupDefaultValue_option3").attr('value',res_ary[3]);
            //$("#landing_page_form #iHiddenValue").attr('value','0');
            $("#landing_page_form #iPickupDropdownValue_option1").attr('value',res_ary[4]);
            $("#landing_page_form #iPickupDropdownValue_option2").attr('value',res_ary[5]);
            $("#landing_page_form #iPickupDropdownValue_option3").attr('value',res_ary[6]);
            
            $("#landing_page_form_hiden #iPickupDefaultValue_option1").attr('value',res_ary[1]);
            $("#landing_page_form_hiden #iPickupDefaultValue_option2").attr('value',res_ary[2]);
            $("#landing_page_form_hiden #iPickupDefaultValue_option3").attr('value',res_ary[3]);
            //$("#landing_page_form #iHiddenValue").attr('value','0');
            $("#landing_page_form_hiden #iPickupDropdownValue_option1").attr('value',res_ary[4]);
            $("#landing_page_form_hiden #iPickupDropdownValue_option2").attr('value',res_ary[5]);
            $("#landing_page_form_hiden #iPickupDropdownValue_option3").attr('value',res_ary[6]);
            
            
            $("#landing_page_form .dimensions-container .pickup").remove();
            $("#landing_page_form_hiden .dimensions-container .pickup").remove();
            $("#landing_page_form .dimensions-container").prepend(res_ary[7]);
            $("#landing_page_form_hiden .dimensions-container").prepend(res_ary[8]);
            $("#landing_page_form #search_header").removeClass("dtd-services-list");
            $("#landing_page_form_hiden #search_header").removeClass("dtd-services-list");
            
            if(res_ary[9]!='')
            {
                $("#pallet-id label").html(res_ary[9]);
            }
            
          
        }
        else
        {
            $("#landing_page_form .dimensions-container .pickup").remove();
            $("#landing_page_form_hiden .dimensions-container .pickup").remove();
            $("#landing_page_form #search_header").addClass("dtd-services-list");
            $("#landing_page_form_hiden #search_header").addClass("dtd-services-list");
            
            
            $("#landing_page_form #iPickupDefaultValue_option1").attr('value',res_ary[1]);
            $("#landing_page_form #iPickupDefaultValue_option2").attr('value',res_ary[2]);
            $("#landing_page_form #iPickupDefaultValue_option3").attr('value',res_ary[3]);
            //$("#landing_page_form #iHiddenValue").attr('value','0');
            $("#landing_page_form #iPickupDropdownValue_option1").attr('value',res_ary[4]);
            $("#landing_page_form #iPickupDropdownValue_option2").attr('value',res_ary[5]);
            $("#landing_page_form #iPickupDropdownValue_option3").attr('value',res_ary[6]);
            
            $("#landing_page_form_hiden #iPickupDefaultValue_option1").attr('value',res_ary[1]);
            $("#landing_page_form_hiden #iPickupDefaultValue_option2").attr('value',res_ary[2]);
            $("#landing_page_form_hiden #iPickupDefaultValue_option3").attr('value',res_ary[3]);
            //$("#landing_page_form #iHiddenValue").attr('value','0');
            $("#landing_page_form_hiden #iPickupDropdownValue_option1").attr('value',res_ary[4]);
            $("#landing_page_form_hiden #iPickupDropdownValue_option2").attr('value',res_ary[5]);
            $("#landing_page_form_hiden #iPickupDropdownValue_option3").attr('value',res_ary[6]);
            
            
            if(res_ary[7]!='')
            {
                $("#pallet-id label").html(res_ary[7]);
            }
        }
        
        
    });
}


function submitSignupForm(szHoldingThanksButtonText,iSearchPageVersion)
{
    var error_count = 1;
    var formId='userSignupForm'+iSearchPageVersion;
    if(isFormInputElementEmpty(formId,'szEmail'+iSearchPageVersion)) 
    {		
        $("#szEmail"+iSearchPageVersion).addClass('red_border');
        error_count = error_count+1 ; 
    }
    else
    {
        var szEmail = $("#szEmail"+iSearchPageVersion).attr('value');
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z]{2,4})+$/;
        if (!regex.test(szEmail)) {
          $("#szEmail"+iSearchPageVersion).addClass('red_border');
            error_count = error_count+1 ;   
        }else{
        $("#szEmail"+iSearchPageVersion).removeClass('red_border');
        }
    }
    
    if(isFormInputElementEmpty(formId,'idFromCountry'+iSearchPageVersion)) 
    {		
        $("#idFromCountryDD"+iSearchPageVersion).addClass('red_border');
        error_count = error_count+1 ; 
    }
    else
    {
        $("#idFromCountryDD"+iSearchPageVersion).removeClass('red_border');
    }
    
    
    if(isFormInputElementEmpty(formId,'idToCountry'+iSearchPageVersion)) 
    {		
        $("#idToCountryDD"+iSearchPageVersion).addClass('red_border');
        error_count = error_count+1 ; 
    }
    else
    {
        $("#idToCountryDD"+iSearchPageVersion).removeClass('red_border');
    }
    
    if(error_count==1)
    {
        var value = $("#"+formId).serialize();
        var newvalue=value+"&mode=USER_NEW_SIGNUP_FROM_HOLDING_PAGE&szHoldingThanksButtonText="+szHoldingThanksButtonText+"&iSearchPageVersion="+iSearchPageVersion;
       $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",newvalue,function(result){
       
         var res_ary=result.split("||||") ;
         if(jQuery.trim(res_ary[0])=='SUCCESS')
         {
             $("#holding_button_submit"+iSearchPageVersion).attr('value',jQuery.trim(res_ary[1]));
             if(parseInt(iSearchPageVersion)>0)
             {
                 $("#search_mini_global_conatiner_"+iSearchPageVersion).html(jQuery.trim(res_ary[2]));
             }
             else
             {
                $(".signup-container").html(jQuery.trim(res_ary[2]));
             }
             $("#holding_button_submit"+iSearchPageVersion).attr('value',jQuery.trim(res_ary[1]));
             $("#holding_button_submit"+iSearchPageVersion).addClass('button1');            
             
             reset_dropdown('idToCountry'+iSearchPageVersion);
             reset_dropdown('idFromCountry'+iSearchPageVersion);
             $("#szEmail"+iSearchPageVersion).attr('value','');
             $("#iSuccessSubmit"+iSearchPageVersion).attr('value','1');
             $("#holding_button_submit"+iSearchPageVersion).attr('onclick','');
             $("#holding_button_submit"+iSearchPageVersion).unbind("click");
         }
        
        });
    }
}


function submitEnterKeyHoldingPage(e,divValue,szHoldingThanksButtonText,szHoldingSubmitButtonText,iSearchPageVersion)
{
    var keycode;
    keycode = e.which; 
    if(keycode == 13)
    {
        if(divValue=='HoldingPage')
        {
            submitSignupForm(szHoldingThanksButtonText,iSearchPageVersion);
        }
    }
    else
    {
        var iSuccessSubmit = $("#iSuccessSubmit"+iSearchPageVersion).attr('value');
        if(iSuccessSubmit)
        {
            $("#iSuccessSubmit"+iSearchPageVersion).attr('value','0');
            $("#holding_button_submit"+iSearchPageVersion).attr('value',szHoldingSubmitButtonText);
            $("#holding_button_submit"+iSearchPageVersion).attr('onclick','');
            $("#holding_button_submit"+iSearchPageVersion).removeClass('button1');
            $("#holding_button_submit"+iSearchPageVersion).unbind("click");
            $("#holding_button_submit"+iSearchPageVersion).click(function(){submitSignupForm(szHoldingThanksButtonText,iSearchPageVersion);});
        }
    }
}

function setValueInTextField(inputType,iValue,iSearchPageVersion)
{
    var newArr=iValue.split("_");
    var classname="flagstrap-"+newArr[0];
    
    if(inputType=='TO')
    {
        $("#idToCountryDD"+iSearchPageVersion).attr('class','country-div');
        $("#idToCountry"+iSearchPageVersion).attr('value',newArr[0]);
        $("#idToCountryDD"+iSearchPageVersion).html(newArr[1]);
        $("#field-three"+iSearchPageVersion).addClass('flagstrap-icon');
        $("#idToCountryDD"+iSearchPageVersion).addClass(classname);
        //$('#idToCountryUl').attr('style','display:none;');
    }
    else if(inputType=='FROM')
    {
        $("#idFromCountryDD"+iSearchPageVersion).attr('class','country-div');
        $("#idFromCountry"+iSearchPageVersion).attr('value',newArr[0]);
        $("#idFromCountryDD"+iSearchPageVersion).html(newArr[1]);
        $("#field-two"+iSearchPageVersion).addClass('flagstrap-icon');
        $("#idFromCountryDD"+iSearchPageVersion).addClass(classname);
        //$('#idFromCountryUl').attr('style','display:none;');
    }
}
function display_private_customer_notification(idBooking,szCountryStrUrl,iNumRecordFound,iSearchMiniPageVersion,from_page,form_id,hidden_flag,page_url)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'__PRIVATE_CUSTOMER_CONTINUE_SEARCHING__',idBooking:idBooking,szCountryStrUrl:szCountryStrUrl,iNumRecordFound:iNumRecordFound,iSearchMiniPageVersion:iSearchMiniPageVersion,from_page:from_page,form_id:form_id,hidden_flag:hidden_flag,page_url:page_url},function(result){
        var res_ary=result.split("||||") ;
        if(jQuery.trim(res_ary[0])=='SUCCESS')
        { 
            $("#ajaxLogin").html(res_ary[1]);
            $("#ajaxLogin").attr('style','display:block');
        }
        else
        {
            continueSearching(idBooking,szCountryStrUrl,iNumRecordFound,iSearchMiniPageVersion);
        }
    });
}
function display_private_customer_notification_backup(idBooking,szCountryStrUrl,iNumRecordFound,iSearchMiniPageVersion)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'__PRIVATE_CUSTOMER_CONTINUE_SEARCHING__',idBooking:idBooking,szCountryStrUrl:szCountryStrUrl,iNumRecordFound:iNumRecordFound,iSearchMiniPageVersion:iSearchMiniPageVersion},function(result){
        var res_ary=result.split("||||") ;
        if(jQuery.trim(res_ary[0])=='SUCCESS')
        { 
            $("#ajaxLogin").html(res_ary[1]);
            $("#ajaxLogin").attr('style','display:block');
        }
        else
        {
            continueSearching(idBooking,szCountryStrUrl,iNumRecordFound,iSearchMiniPageVersion);
        }
    });
} 
function updateHaulageZonesCountries()
{
    var szAjaxUrl = __JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php";
    $.ajax({
        url: szAjaxUrl,
        global: false,
        type: 'POST',
        data: {flag:"AUTO_UPDATE_HAULAGE_ZONE_COUNTRIES"},
        async: true, //blocks window close
        success: function(result) {
            var res_ary=result.split("||||") ;
            if(jQuery.trim(res_ary[0])=='SUCCESS_LOG')
            { 
                $("#haulage_zone_countries_calculation_logs").html(res_ary[1]);
                $("#haulage_zone_countries_calculation_logs").attr('style','display:block');
            }
        }
    });
    
    /*
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{flag:"AUTO_UPDATE_HAULAGE_ZONE_COUNTRIES"},function(result){
        var res_ary=result.split("||||") ;
        if(jQuery.trim(res_ary[0])=='SUCCESS_LOG')
        { 
            $("#haulage_zone_countries_calculation_logs").html(res_ary[1]);
            $("#haulage_zone_countries_calculation_logs").attr('style','display:block');
        }  
    });
    */
}

function prefill_dimensions(pallet_type,id_suffix)
{
    if(pallet_type==1)
    {
        $("#iLength"+id_suffix).val('120');
        $("#iWidth"+id_suffix).val('80');
    }
    else if(pallet_type==2)
    {
        $("#iLength"+id_suffix).val('80');
        $("#iWidth"+id_suffix).val('60');
    }
    else if(pallet_type==4)
    {
        $("#iLength"+id_suffix).val('120');
        $("#iWidth"+id_suffix).val('100');
    }
}

function autoSelectTexts(input_id)
{
    if($("#"+input_id).length)
    {
        $("#"+input_id).select();
    }
}


function openSearchNotificationPopup(formid,idSuffix,page_url,from_page,iHiddenValue,iSearchMiniPage)
{
    return false;
    var szOriginDataDivId="szOriginCountryStr"+idSuffix;
    var szOriginData=$("#"+formid+" #"+szOriginDataDivId).attr('value');
    var szDesDataDivId="szDestinationCountryStr"+idSuffix;
    var szDesData=$("#"+formid+" #"+szDesDataDivId).attr('value');
    
    if(szDesData!='' && szOriginData!='')
    {
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{iSearchMiniPage:iSearchMiniPage,iHiddenValue:iHiddenValue,formid:formid,page_url:page_url,from_page:from_page,mode:"SHOW_NOTIFICATION_POPUP_FOR_VOGA_PAGE",szOriginData:szOriginData,szDesData:szDesData},function(result){
            var res_ary=result.split("||||") ;
            if(jQuery.trim(res_ary[0])=='SUCCESS_LOCAL_SEARCH')
            { 
                $("#regError").attr('style','display:none;');
                $("#ajaxLogin").html(res_ary[1]);
                $("#ajaxLogin").attr('style','display:block');
            }  
        });
    }
    
}

function callStripeApiToObtainToken(szApiKey,fChargeableAmount,szCurrency,szDescription,szEmail)
{    
    var site_path = __JS_ONLY_SITE_BASE__;  
    var stripe_path = site_path + "/stripe";  

    var handler = StripeCheckout.configure(
    {
        key: szApiKey,
        image: __JS_ONLY_SITE_BASE__+'/images/favicon.png',
        locale: 'auto',
        email: szEmail, 
        token: function(token) {
          // Use the token to create the charge with a server-side script.
          // You can access the token ID with `token.id`
          var szTokenId = token.id;
          var transactionID = token.card.id;
           console.log("Token: "+szTokenId+" Transaction Number: "+transactionID);
        }
    }); 
    $(".continue-button" ).on( "click", function(e) {
        // Open Checkout with further options
        handler.open({
          name: 'Transporteca',
          description: szDescription,
          currency: szCurrency,
          amount: fChargeableAmount*100
        });  
        e.preventDefault();
    });
    $( ".continue-button" ).trigger("click" ); 
    $(".continue-button" ).unbind("click");
    // Close Checkout on page navigation
    $(window).on('popstate', function() {
      handler.close();
    }); 
}