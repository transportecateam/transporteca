  
function auto_load_pending_tray_listing_backup()
{
    $("#iPendingTrayAutoRefreshCalled").val(2);
    var file_id = $("#hidden_selected_file_id").val();
    
    $("#iStatusChanged").val('1');
    
    var ajaxUrl = __JS_ONLY_SITE_BASE__ + "/ajax_reloadPendingTray.php?mode=AUTO_LOAD_PENDING_TRAY_TASK_LIST&file_id="+file_id;
    $.ajax({
        type: "POST",
        url: ajaxUrl,
        data: $("#transporteca_team_form").serialize(),
        dataType: 'JSON',
        success: function(response)
        {              
            var iStatusChanged = $("#iStatusChanged").val();
            iStatusChanged = parseInt(iStatusChanged); 
            if(iStatusChanged==1)
            {  
                printJsonResponse(response); 
//                var disp = $("#pending_task_main_list_container_div").css("display");
//                //$("#pending_task_listing_container").html(result_ary[1]); 
//                if(disp=='block')
//                {
//                    $("#pending_task_main_list_container_div").attr('style','display:block');
//                }
//                else
//                {
//                    $("#pending_task_main_list_container_div").attr('style','display:none');
//                } 

                var idFileForCloseCtrValue=$("#idFileForCloseCtr").val(); 
                var idFileForCloseCtrValueArr=idFileForCloseCtrValue.split(";");
                var len=idFileForCloseCtrValueArr.length;
                if(parseInt(len)>0)
                {
                    for(i=0;i<len;++i)
                    {
                        var divValue = "pendingTrayList_"+idFileForCloseCtrValueArr[i];  
                        $("#"+divValue).prop("checked","checked");
                    }
                } 
            } 
            else
            { 
                console.log("Already updated by Team selection...");
                //display_pending_task_list_byteam();
            }
            $("#iPendingTrayAutoRefreshCalled").val(1); 
        }
    }); 
} 
function printJsonResponse(response)
{ 
    console.log("Printing json response...");
    var ctr = 0;
    var len = response.length; 
    var szResponseTableStr = '';
    $("#insuranced_booking_table").html("");
    var idLastClickedBooking = $('#idLastClikedBookingFile').val();
    var szRow_id = "";
    var iSelectClass = 0;
    var selected_tr_id = "";
    var iScollPopup = 0;
    for(var i=0; i<len; i++)
    { 
        var idBookingFile = response[i].idBookingFile;
        var szTaskUpdatedOn = response[i].szTaskUpdatedOn;
        var szTask = response[i].szTask;
        var szCustomerName = response[i].szCustomerName;
        var idBookingFile = response[i].idBookingFile;
        var szTransportMode = response[i].szTransportMode;  
        var szOriginCity = response[i].szOriginCity;
        var szDestinationCity = response[i].szDestinationCity;
        var szVolWeight = response[i].szVolWeight;
        var szTaskStatus = response[i].szTaskStatus; 
        var iDisplayPopup = response[i].iDisplayPopup;
        var idBookingQuote = response[i].idBookingQuote;
        var iQuoteClosed = response[i].iQuoteClosed;
        var iBookingFlag = response[i].iBookingFlag;
        var iCrmEmail = response[i].iCrmEmail;
        var idReminderTask = response[i].idReminderTask;
        var idOfflineChat = response[i].idOfflineChat;
        var bOnlyHighlight = response[i].bOnlyHighlight;
        var idSelectedBookingFileId = response[i].idSelectedBookingFileId;
        
        var idSelectedBooking = parseInt(idSelectedBookingFileId);
        idLastClickedBooking = parseInt(idLastClickedBooking);
        
        //console.log("Selected: "+idSelectedBooking+" File: "+idBookingFile+" high: "+bOnlyHighlight);
        ctr = i+1; 
        
        if((idSelectedBooking>0) && (idBookingFile==idSelectedBooking))
        { 
            selected_tr_id = "booking_data_"+ctr ;  
            iScollPopup = 1;
            
            //console.log("Matched..." + selected_tr_id);
        } 
        
        if(bOnlyHighlight==1)
        {  
            if(idLastClickedBooking>0 && idSelectedBooking!=idLastClickedBooking)
            { 
                szRow_id = "pending_tray_listing_row_"+idLastClickedBooking;
                iSelectClass = 1;
                iScollPopup = 1;
            }
            else if(iScollPopup==1)
            { 
                szRow_id = selected_tr_id;
            }
        }
        else
        {
            szRow_id = selected_tr_id;
        } 
        var tr_id = "booking_data_"+ctr; 
        var szPendingTrayColumnClass = "pending-tray-td-"+idBookingFile;
        
        var szOnclickFunc = ' onclick="display_pending_task_details(\''+ tr_id + '\',\''+idBookingFile+'\',\''+iDisplayPopup+'\',\''+idBookingQuote+'\',\''+iQuoteClosed+'\',\''+iBookingFlag+'\',\''+iCrmEmail+'\',\''+idReminderTask+'\',\''+idOfflineChat+'\');"'
        
        var szResponseRowStr = '<tr id="'+tr_id+'" class="pending_tray_listing_row_'+ idBookingFile +'">' +
         '<td class="wd-10 '+szPendingTrayColumnClass+'" '+ szOnclickFunc +'>' + szTaskUpdatedOn + '</td>' +
         '<td class="wd-15 '+szPendingTrayColumnClass+'" '+ szOnclickFunc +'>' + szTask + '</td>' +
         '<td class="wd-15 '+szPendingTrayColumnClass+'" '+ szOnclickFunc +'>' + szCustomerName + '</td>'+
         '<td class="wd-10 '+szPendingTrayColumnClass+'" '+ szOnclickFunc +'>' + szTransportMode + '</td>'+ 
         '<td class="wd-15 '+szPendingTrayColumnClass+'" '+ szOnclickFunc +'>' + szOriginCity + '</td>' +
         '<td class="wd-15 '+szPendingTrayColumnClass+'" '+ szOnclickFunc +'>' + szDestinationCity + '</td>' + 
         '<td class="wd-18 '+szPendingTrayColumnClass+'" '+ szOnclickFunc +'>' + szVolWeight + '</td>' +
         '<td class="wd-2">';
        if(szTaskStatus!='T140')
        {
            var input_cb_id = "pendingTrayList_"+ctr;
            var flag = ctr;
            szResponseRowStr += '<input type="checkbox" class="pendingTrayList" name="pendingTrayList" id="'+input_cb_id+'" value="'+idBookingFile+'" onclick="selectLineForClosingTheFile(\''+idBookingFile+'\',\''+input_cb_id+'\',\''+flag+'\')"/>';  
        }
        szResponseRowStr += '</td></tr>';  
        $("#insuranced_booking_table").append(szResponseRowStr);  
        
        /*
        $("."+szPendingTrayColumnClass).unbind("click");
        $("."+szPendingTrayColumnClass).click(function(){ 
            display_pending_task_details(tr_id,idBookingFile,iDisplayPopup,idBookingQuote,iQuoteClosed,iBookingFlag,iCrmEmail,idReminderTask,idOfflineChat);
        });
        */
    } 
    if(iScollPopup==1)
    {
        scrollPendingTask(szRow_id,iSelectClass);
    } 
}

function scrollPendingTask(row_id,iSelectClass)
{
    if(iSelectClass==1)
    {
        $("."+row_id).attr('style','background:#DBDBDB;cursor:pointer;');  
    }
    else
    {
        $("#"+row_id).attr('style','background:#DBDBDB;cursor:pointer;');  
    } 
    var iScrollValue=$("#iScrollValue").attr('value'); 
    $("#pending_task_list_container_div .scrolling-div").scrollTop(iScrollValue);
}