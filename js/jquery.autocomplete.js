! function(e) {
    e.fn.extend({
        autocomplete: function(t, n) {
            var a = "string" == typeof t;
            return n = e.extend({}, e.Autocompleter.defaults, {
                url: a ? t : null,
                data: a ? null : t,
                delay: a ? e.Autocompleter.defaults.delay : 10,
                width: 50,
                max: n && !n.scroll ? 10 : 150
            }, n), n.highlight = n.highlight || function(e) {
                return e
            }, n.formatMatch = n.formatMatch || n.formatItem, this.each(function() {
                new e.Autocompleter(this, n)
            })
        },
        result: function(e) {
            return this.bind("result", e)
        },
        search: function(e) {
            return this.trigger("search", [e])
        },
        flushCache: function() {
            return this.trigger("flushCache")
        },
        setOptions: function(e) {
            return this.trigger("setOptions", [e])
        },
        unautocomplete: function() {
            return this.trigger("unautocomplete")
        }
    }), e.Autocompleter = function(t, n) {
        function a() {
            var a = T.selected();
            if (!a) return !1;
            var i = a.result;
            if (y = i, n.multiple) {
                var s = r(v.val());
                if (s.length > 1) {
                    var o, l = n.multipleSeparator.length,
                        c = e(t).selection().start,
                        h = 0;
                    e.each(s, function(e, t) {
                        return h += t.length, h >= c ? (o = e, !1) : void(h += l)
                    }), s[o] = i, i = s.join(n.multipleSeparator)
                }
                i += n.multipleSeparator
            } 
            
            if (("updatedTaskAry[szFirstName]" == t.name) || ("updatedTaskAry[szLastName]" == t.name) || ("updatedTaskAry[szCustomerCompanyName]" == t.name))
            {  
                var szInputFieldID = t.id;
                autofill_admin_validate_pane(i,'',false,szInputFieldID);
            } 
            if ("updatedTaskAry[szEmail]" == t.name) 
            { 
                var szInputFieldID = t.id;
                autofill_admin_validate_pane(i,'EMAIL',false,szInputFieldID);
            }  
            
            if(("quickQuoteShipperConsigneeAry[szFirstName]" == t.name) || ("quickQuoteShipperConsigneeAry[szLastName]" == t.name) || ("quickQuoteShipperConsigneeAry[szCustomerCompanyName]" == t.name))
            {
                var szInputFieldID = t.id;
                autofill_admin_validate_pane(i,'',1,szInputFieldID);
            } 
            if ("quickQuoteShipperConsigneeAry[szEmail]" == t.name)
            { 
                var szInputFieldID = t.id;
                autofill_admin_validate_pane(i,'EMAIL',1,szInputFieldID);
            }    
            return v.val(i), u(), v.trigger("result", [a.data, a.value]), !0
        }

        function i(e, a) {
            if (m == g.DEL) return void T.hide();
            var i = v.val(),
                r = i.length,
                o = 0;
            ("shipperConsigneeAry[szShipperCompanyName]" == t.name || "shipperConsigneeAry[szConsigneeCompanyName]" == t.name) && (o = 1), 2 >= r && 0 == o || (y = i, i = s(i), i.length >= n.minChars ? (v.addClass(n.loadingClass), n.matchCase || (i = i.toLowerCase()), h(i, c, u)) : m == g.DOWN ? (v.addClass(n.loadingClass), n.matchCase || (i = i.toLowerCase()), h(i, c, u, "key_down")) : (p(), T.hide()))
        }

        function r(t) {
            return t ? n.multiple ? e.map(t.split(n.multipleSeparator), function(n) {
                return e.trim(t).length ? e.trim(n) : null
            }) : [e.trim(t)] : [""]
        }

        function s(a) {
            if (!n.multiple) return a;
            var i = r(a);
            if (1 == i.length) return i[0];
            var s = e(t).selection().start;
            return i = r(s == a.length ? a : a.replace(a.substring(s), "")), i[i.length - 1]
        }

        function o(a, i) {
            n.autoFill && s(v.val()).toLowerCase() == a.toLowerCase() && m != g.BACKSPACE && (v.val(v.val() + i.substring(s(y).length)), e(t).selection(y.length, y.length + i.length))
        }

        function l() {
            clearTimeout(C), C = setTimeout(u, 200)
        }

        function u() {
            T.visible();
            T.hide(), clearTimeout(C), p(), n.mustMatch && v.search(function(e) {
                if (!e)
                    if (n.multiple) {
                        var t = r(v.val()).slice(0, -1);
                        v.val(t.join(n.multipleSeparator) + (t.length ? n.multipleSeparator : ""))
                    } else v.trigger("result", null)
            })
        }

        function c(e, t) {
            t && t.length && z ? (p(), T.display(t, e), o(e, t[0].value), T.show()) : u()
        }

        function h(a, i, r, o) {
            var l = "",
                u = "",
                c = "",
                h = "",
                p = "",
                C = "";
            n.matchCase || (a = a.toLowerCase());
            var m = A.load(a),
                d = a.length,
                g = 0;
            if (("shipperConsigneeAry[szShipperCompanyName]" == t.name || "shipperConsigneeAry[szConsigneeCompanyName]" == t.name) && (g = 1), d > 2 || g)
                if (m && m.length) i(a, m);
                else if ("string" == typeof n.url && n.url.length > 0) {
                var v = {
                    timestamp: +new Date
                };
                e.each(n.extraParams, function(e, t) {
                    v[e] = "function" == typeof t ? t() : t
                }), ("updatedTaskAry[szFirstName]" == t.name || "quickQuoteShipperConsigneeAry[szFirstName]" == t.name) && (l = e("#szFirstName").attr("value"), c = "FETCH_FIRST_NAME"), ("updatedTaskAry[szLastName]" == t.name || "quickQuoteShipperConsigneeAry[szLastName]" == t.name) && (l = e("#szLastName").attr("value"), c = "FETCH_LAST_NAME"),("updatedTaskAry[szCustomerCompanyName]" == t.name || "quickQuoteShipperConsigneeAry[szCustomerCompanyName]" == t.name) && (l = e("#szCustomerCompanyName").attr("value"), c = "FETCH_COMPANY_NAME"),("updatedTaskAry[szEmail]" == t.name || "quickQuoteShipperConsigneeAry[szEmail]" == t.name) && (l = e("#szEmail").attr("value"), c = "FETCH_EMAIL"), "searchAry[szOriginCity]" == t.name && (l = e("#szOriginCountry").attr("value"), c = "CITY"), "searchAry[szDestinationCity]" == t.name && (l = e("#szDestinationCountry").attr("value"), c = "CITY"), "searchAry[szOriginPostCode]" == t.name && (l = e("#szOriginCountry").attr("value"), u = e("#szOriginCity").attr("value"), c = "POST_CODE"), "searchAry[szDestinationPostCode]" == t.name && (l = e("#szDestinationCountry").attr("value"), u = e("#szDestinationCity").attr("value"), c = "POST_CODE"), "registerShipperCongArr[szCity]" == t.name && (l = e("#szCountry").attr("value"), u = e("#szDestinationCity").attr("value"), c = "CITY"), "registerShipperCongArr[szPostCode]" == t.name && (l = e("#szCountry").attr("value"), u = e("#szCity").attr("value"), c = "POST_CODE"), "cfsAddEditArr[szCity]" == t.name && (l = e("#szCountry").attr("value"), u = e("#szDestinationCity").attr("value"), c = "CITY"), "cfsAddEditArr[szPostCode]" == t.name && (l = e("#szCountry").attr("value"), u = e("#szCity").attr("value"), c = "POST_CODE"), "shipperConsigneeAry[szShipperCity]" == t.name && (l = e("#idShipperCountry").attr("value"), u = e("#szShipperCity").attr("value"), c = "CITY"), "shipperConsigneeAry[szShipperPostcode]" == t.name && (l = e("#idShipperCountry").attr("value"), u = e("#szShipperCity").attr("value"), c = "POST_CODE"), "shipperConsigneeAry[szConsigneeCity]" == t.name && (l = e("#idConsigneeCountry").attr("value"), u = e("#szConsigneeCity").attr("value"), c = "CITY"), "shipperConsigneeAry[szConsigneePostcode]" == t.name && (l = e("#idConsigneeCountry").attr("value"), u = e("#szConsigneeCity").attr("value"), c = "POST_CODE"), "tryoutArr[szPostCodeCity]" == t.name && (l = e("#szCountry").attr("value"), c = "POST_CODE"), "szOriginCityPostcode" == t.name && (p = e("#szOriginCityPostcode").attr("value"), l = e("#idOriginCountry").attr("value"), c = "CITY_POST_CODE"), "szDestinationCityPostcode" == t.name && (p = e("#szDestinationCityPostcode").attr("value"), l = e("#idDestinationCountry").attr("value"), c = "CITY_POST_CODE"), "shipperConsigneeAry[szShipperCompanyName]" == t.name && (l = e("#idShipperCountry").attr("value"), p = e("#szShipperPostcode").attr("value"), u = e("#szShipperCity").attr("value"), c = "SHIPPER_COMPANY_NAME", C = "ship"), "shipperConsigneeAry[szConsigneeCompanyName]" == t.name && (l = e("#idConsigneeCountry").attr("value"),p = e("#szConsigneePostcode").attr("value"), u = e("#szConsigneeCity").attr("value"), c = "SHIPPER_COMPANY_NAME", C = "con"), "key_down" == o && (h = "All"), e.ajax({
                    mode: "abort",
                    port: "autocomplete" + t.name,
                    dataType: n.dataType,
                    url: n.url,
                    data: e.extend({
                        idCountry: l,
                        szCity: u,
                        action: c,
                        default_action: h,
                        szFlag: C,
                        q: s(a),
                        szCityPostcode: p,
                        limit: n.max
                    }, v),
                    success: function(e) {
                        var t = n.parse && n.parse(e) || f(e);
                        i(a, t)
                    }
                })
            } else T.emptyList(), r(a)
        }

        function f(t) {
            for (var a = [], i = t.split("\n"), r = 0; r < i.length; r++) {
                var s = e.trim(i[r]);
                s && (s = s.split("|"), a[a.length] = {
                    data: s,
                    value: s[0],
                    result: n.formatResult && n.formatResult(s, s[0]) || s[0]
                })
            }
            return a
        }

        function p() {
            v.removeClass(n.loadingClass)
        }
        var C, m, d, g = {
                UP: 38,
                DOWN: 40,
                DEL: 46,
                TAB: 9,
                RETURN: 13,
                ESC: 27,
                COMMA: 188,
                PAGEUP: 33,
                PAGEDOWN: 34,
                BACKSPACE: 8
            },
            v = e(t).attr("autocomplete", "off").addClass(n.inputClass),
            y = "",
            A = e.Autocompleter.Cache(n),
            z = 0,
            S = {
                mouseDownOnSelect: !0
            },
            T = e.Autocompleter.Select(n, t, a, S);
        e.browser.opera && e(t.form).bind("submit.autocomplete", function() {
            return d ? (d = !1, !1) : void 0
        }), v.bind((e.browser.opera ? "keypress" : "keydown") + ".autocomplete", function(t) {
            switch (z = 1, m = t.keyCode, t.keyCode) {
                case g.UP:
                    t.preventDefault(), T.visible() ? T.prev() : i(0, !0);
                    break;
                case g.DOWN:
                    t.preventDefault(), T.visible() ? T.next() : i(0, !0);
                    break;
                case g.PAGEUP:
                    t.preventDefault(), T.visible() ? T.pageUp() : i(0, !0);
                    break;
                case g.PAGEDOWN:
                    t.preventDefault(), T.visible() ? T.pageDown() : i(0, !0);
                    break;
                case n.multiple && "," == e.trim(n.multipleSeparator) && g.COMMA:
                case g.TAB:
                case g.RETURN: 
                    if (a()) return t.preventDefault(), d = !0, !1;
                    break;
                case g.ESC:
                    T.hide();
                    break;
                default:
                    clearTimeout(C), C = setTimeout(i, n.delay)
            }
        }).focus(function() {
            z++
        }).blur(function() {
            z = 0, S.mouseDownOnSelect || l()
        }).click(function() {
            z++ > 1 && !T.visible() && i(0, !0)
        }).bind("search", function() {
            function t(e, t) {
                var a;
                if (t && t.length)
                    for (var i = 0; i < t.length; i++)
                        if (t[i].result.toLowerCase() == e.toLowerCase()) {
                            a = t[i];
                            break
                        }
                        "function" == typeof n ? n(a) : v.trigger("result", a && [a.data, a.value])
            }
            var n = arguments.length > 1 ? arguments[1] : null;
            e.each(r(v.val()), function(e, n) {
                h(n, t, t)
            })
        }).bind("flushCache", function() {
            A.flush()
        }).bind("setOptions", function() {
            e.extend(n, arguments[1]), "data" in arguments[1] && A.populate()
        }).bind("unautocomplete", function() {
            T.unbind(), v.unbind(), e(t.form).unbind(".autocomplete")
        })
    }, e.Autocompleter.defaults = {
        inputClass: "ac_input",
        resultsClass: "ac_results",
        resultsId: "iAutofill",
        loadingClass: "ac_loading",
        minChars: 0,
        delay: 400,
        matchCase: !1,
        matchSubset: !0,
        matchContains: !1,
        cacheLength: 10,
        max: 100,
        mustMatch: !1,
        extraParams: {},
        selectFirst: !0,
        formatItem: function(e) {
            return e[0]
        },
        formatMatch: null,
        autoFill: !1,
        width: 0,
        multiple: !1,
        multipleSeparator: ", ",
        highlight: function(e, t) {
            return e.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + t.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>")
        },
        scroll: !0,
        scrollHeight: 180
    }, e.Autocompleter.Cache = function(t) {
        function n(e, n) {
            t.matchCase || (e = e.toLowerCase());
            var a = e.indexOf(n);
            return "word" == t.matchContains && (a = e.toLowerCase().search("\\b" + n.toLowerCase())), -1 == a ? !1 : 0 == a || t.matchContains
        }

        function a(e, n) {
            o > t.cacheLength && r(), s[e] || o++, s[e] = n
        }

        function i() {
            if (!t.data) return !1;
            var n = {},
                i = 0;
            t.url || (t.cacheLength = 1), n[""] = [];
            for (var r = 0, s = t.data.length; s > r; r++) {
                var o = t.data[r];
                o = "string" == typeof o ? [o] : o;
                var l = t.formatMatch(o, r + 1, t.data.length);
                if (l !== !1) {
                    var u = l.charAt(0).toLowerCase();
                    n[u] || (n[u] = []);
                    var c = {
                        value: l,
                        data: o,
                        result: t.formatResult && t.formatResult(o) || l
                    };
                    n[u].push(c), i++ < t.max && n[""].push(c)
                }
            }
            e.each(n, function(e, n) {
                t.cacheLength++, a(e, n)
            })
        }

        function r() {
            s = {}, o = 0
        }
        var s = {},
            o = 0;
        return setTimeout(i, 25), {
            flush: r,
            add: a,
            populate: i,
            load: function(a) {
                if (!t.cacheLength || !o) return null;
                if (!t.url && t.matchContains) {
                    var i = [];
                    for (var r in s)
                        if (r.length > 0) {
                            var l = s[r];
                            e.each(l, function(e, t) {
                                n(t.value, a) && i.push(t)
                            })
                        }
                    return i
                }
                if (s[a]) return s[a];
                if (t.matchSubset)
                    for (var u = a.length - 1; u >= t.minChars; u--) {
                        var l = s[a.substr(0, u)];
                        if (l) {
                            var i = [];
                            return e.each(l, function(e, t) {
                                n(t.value, a) && (i[i.length] = t)
                            }), i
                        }
                    }
                return null
            }
        }
    }, e.Autocompleter.Select = function(t, n, a, i) {
        function r() {
            v && (p = e("<div/>").hide().addClass(t.resultsClass).attr("id", n.id + "_autofill").css("text-align", "left").css("position", "absolute").appendTo(document.body), C = e("<ul/>").appendTo(p).mouseover(function(t) {
                s(t).nodeName && "LI" == s(t).nodeName.toUpperCase() && (d = e("li", C).removeClass(m.ACTIVE).index(s(t)), e(s(t)).addClass(m.ACTIVE))
            }).click(function(t) {
                if (e(s(t)).addClass(m.ACTIVE), a(), n.focus(), "shipperConsigneeAry[szShipperCompanyName]" == n.name) {
                    var i = e("#szShipperCompanyName").attr("value");
                    auto_fill_new_shipper_consignee(i, "SHIPPER_NEW")
                }
                if ("shipperConsigneeAry[szConsigneeCompanyName]" == n.name) {
                    var i = e("#szConsigneeCompanyName").attr("value");
                    auto_fill_new_shipper_consignee(i, "CONSIGNEE_NEW")
                }
                if ("updatedTaskAry[szFirstName]" == n.name) {
                    var i = e("#szFirstName").attr("value"); 
                    //autofill_admin_validate_pane(i);
                }
                if ("updatedTaskAry[szLastName]" == n.name) 
                {
                    var i = e("#szLastName").attr("value"); 
                    //autofill_admin_validate_pane(i);
                }
                if ("updatedTaskAry[szCustomerCompanyName]" == n.name) 
                {
                    var i = e("#szCustomerCompanyName").attr("value"); 
                    //autofill_admin_validate_pane(i);
                }
                if ("updatedTaskAry[szEmail]" == n.name) 
                {
                    var i = e("#szEmail").attr("value"); 
                    //autofill_admin_validate_pane(i,'EMAIL');
                }
                
                return !1
            }).mousedown(function() {
                i.mouseDownOnSelect = !0
            }).mouseup(function() {
                i.mouseDownOnSelect = !1
            }), t.width = 200, t.width > 0 && p.css("width", t.width), v = !1)
        }

        function s(e) {
            for (var t = e.target; t && "LI" != t.tagName;) t = t.parentNode;
            return t ? t : []
        }

        function o(e) {
            h.slice(d, d + 1).removeClass(m.ACTIVE), l(e);
            var n = h.slice(d, d + 1).addClass(m.ACTIVE);
            if (t.scroll) {
                var a = 0;
                h.slice(0, d).each(function() {
                    a += this.offsetHeight
                }), a + n[0].offsetHeight - C.scrollTop() > C[0].clientHeight ? C.scrollTop(a + n[0].offsetHeight - C.innerHeight()) : a < C.scrollTop() && C.scrollTop(a)
            }
        }

        function l(e) {
            d += e, 0 > d ? d = h.size() - 1 : d >= h.size() && (d = 0)
        }

        function u(e) {
            return t.max && t.max < e ? t.max : e
        }

        function c() {
            C.empty();
            for (var n = u(f.length), a = 0; n > a; a++)
                if (f[a]) {
                    var i = t.formatItem(f[a].data, a + 1, n, f[a].value, g);
                    if (i !== !1) {
                        var r = e("<li/>").html(t.highlight(i, g)).addClass(a % 2 == 0 ? "ac_even" : "ac_odd").appendTo(C)[0];
                        e.data(r, "ac_data", f[a])
                    }
                }
            h = C.find("li"), t.selectFirst && (h.slice(0, 1).addClass(m.ACTIVE), d = 0), e.fn.bgiframe && C.bgiframe()
        }
        var h, f, p, C, m = {
                ACTIVE: "ac_over"
            },
            d = -1,
            g = "",
            v = !0;
        return {
            display: function(e, t) {
                r(), f = e, g = t, c()
            },
            next: function() {
                o(1)
            },
            prev: function() {
                o(-1)
            },
            pageUp: function() {
                o(0 != d && 0 > d - 8 ? -d : -8)
            },
            pageDown: function() {
                o(d != h.size() - 1 && d + 8 > h.size() ? h.size() - 1 - d : 8)
            },
            hide: function() {
                p && p.hide(), h && h.removeClass(m.ACTIVE), d = -1
            },
            visible: function() {
                return p && p.is(":visible")
            },
            current: function() {
                return this.visible() && (h.filter("." + m.ACTIVE)[0] || t.selectFirst && h[0])
            },
            show: function() {
                var a = e(n).offset();
                if (p.css({
                        width: "string" == typeof t.width || t.width > 0 ? t.width : e(n).width(),
                        top: a.top + n.offsetHeight,
                        left: a.left
                    }).show(), t.scroll && (C.scrollTop(0), C.css({
                        maxHeight: t.scrollHeight,
                        overflow: "auto"
                    }), e.browser.msie && "undefined" == typeof document.body.style.maxHeight)) {
                    var i = 0;
                    h.each(function() {
                        i += this.offsetHeight
                    });
                    var r = i > t.scrollHeight;
                    C.css("height", r ? t.scrollHeight : i), r || h.width(C.width() - parseInt(h.css("padding-left")) - parseInt(h.css("padding-right")))
                }
            },
            selected: function() {
                var t = h && h.filter("." + m.ACTIVE).removeClass(m.ACTIVE);
                return t && t.length && e.data(t[0], "ac_data")
            },
            emptyList: function() {
                C && C.empty()
            },
            unbind: function() {
                p && p.remove()
            }
        }
    }, e.fn.selection = function(e, t) {
        if (void 0 !== e) return this.each(function() {
            if (this.createTextRange) {
                var n = this.createTextRange();
                void 0 === t || e == t ? (n.move("character", e), n.select()) : (n.collapse(!0), n.moveStart("character", e), n.moveEnd("character", t), n.select())
            } else this.setSelectionRange ? this.setSelectionRange(e, t) : this.selectionStart && (this.selectionStart = e, this.selectionEnd = t)
        });
        var n = this[0];
        if (n.createTextRange) {
            var a = document.selection.createRange(),
                i = n.value,
                r = "<->",
                s = a.text.length;
            a.text = r;
            var o = n.value.indexOf(r);
            return n.value = i, this.selection(o, o + s), {
                start: o,
                end: o + s
            }
        }
        return void 0 !== n.selectionStart ? {
            start: n.selectionStart,
            end: n.selectionEnd
        } : void 0
    }
}(jQuery);