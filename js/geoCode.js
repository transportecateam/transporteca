/**************************************************

	Copyright 2013 Torch Products LLC
	@author Brian O'Connor

**************************************************/

var running = false;

function geocode() {
	addresses = $('#geocode_input').val().split("\n");
	$('#geocode_input').val('');
	for(var i=0; i<addresses.length; i++) {
		if(addresses[i] != '') {
                    $('.geocode_results').append('<tr><td class="address">'+addresses[i].replace(/\t/g, ' ')+'</td><td class="status pending">Pending...</td><td class="lat"></td><td class="lng"></td></tr>');
		}
	}
	if(!running) {
            running = true;
            geocode_service();
	}
}

function updateResult(el, success, data) {
	if(success) {
            console.log(data);
		el.html('Found');
		el.parent().find('.address').html(data.formatted_address);
		el.parent().find('.lat').html(data.geometry.location.lat);
		el.parent().find('.lng').html(data.geometry.location.lng);
	} else {
		el.html('Error');
	}
	fetchNextGeocode();
}

function fetchNextGeocode() 
{
    el = $('.geocode_results .pending').first();
    if(el.length > 0) 
    {
        el.html('Fetching...');
    }
    window.setTimeout('geocode_service();', 3000);
}

function geocode_service() {
	var el = $('.geocode_results .pending').first();
	if(el.length < 1) {
		running = false;
		return;
	}
	
	el.removeClass('pending');
	el.html('Fetching...');

	address = el.parent().find('.address').html();
	url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address='+address;
	$.ajax({
		dataType: 'json',
		url: url,
		success: function(data) {
			if(data && data.status) {
				if(data.status == 'OK') {
					updateResult(el, true, data.results[0])
				} else {
					updateResult(el, false);
				}
			} else {
				updateResult(el, false);
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			geocoder_fallback(el, address);
		}
	});
}

function geocoder_fallback(el, address) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({
		'address': address
	}, function(results, status) {
		if(status == google.maps.GeocoderStatus.OK) {
			updateResult(el, true, results[0]);
		} else {
			updateResult(el, false);
		}
	});
}
