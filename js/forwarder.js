var adminForwarder = null;
function sel_line_waiting_for_cost_approval(div_id,id,status,costApprovalStatus,filename)
{
	var rowCount = $('#wating_for_cost_approved_by_management tr').length;
	//alert(div_id);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="wating_for_cost_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
		if(status==costApprovalStatus)
		{
			$("#approved_cost").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#approved_cost").click(function(){approved_processing_cost(id)});
			$("#approved_cost").attr('style',"opacity:1");
		}
		else
		{
			$("#approved_cost").unbind("click");
			$("#approved_cost").attr('style',"opacity:0.4");
		}
		
		if(filename!='')
		{
			$("#view_files").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#view_files").click(function(){download_uploaded_file(filename)});
			$("#view_files").attr('style',"opacity:1");
		}
		else
		{
			$("#view_files").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#view_files").click(function(){view_uploaded_files(id)});
			$("#view_files").attr('style',"opacity:1");
		}
		if(status==costApprovalStatus)
		{
			$("#delete_files").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#delete_files").click(function(){delete_uploaded_files(id)});
			$("#delete_files").attr('style',"opacity:1");
		}
		else
		{
			$("#delete_files").unbind("click");
			$("#delete_files").attr('style',"opacity:0.4");
		}
}

function approved_processing_cost(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'approve_processing_cost'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
		addPopupScrollClass('open_processing_popup');
	});
}

function cancel_processing_cost()
{
	$("#open_processing_popup").attr('style',"display:none");
	removePopupScrollClass('open_processing_popup');
}

function confirm_processing_cost(id)
{
	$("#confirm_cost_processing").unbind("click");
	$("#confirm_cost_processing").attr('style',"opacity:0.4");
			
	$("#cancel_cost_processing").unbind("click");
	$("#cancel_cost_processing").attr('style',"opacity:0.4");
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'approved_processing_cost'},function(result){
		$("#waiting_for_approved_list").html(result);
		$("#open_processing_popup").attr('style',"display:none");
		show_forwarder_top_messges();
	});
}

function view_uploaded_files(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'view_files'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
		addPopupScrollClass('open_processing_popup');
	});
}

function download_uploaded_file(filename,id)
{
	//alert(__JS_ONLY_SITE_BASE__);
	var iframe = window.parent.document.getElementById('hssiframe');
	iframe.src=__JS_ONLY_SITE_BASE__+'/ajax_bulkServicesApproval.php?filename='+filename+'&flag=download_file&id='+id;
}

function open_comment_popup(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'open_comment_popup'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
		 $("#szComment").focus();      
	});
}

function active_add_comment_button(id)
{
	var szComment=$("#szComment").attr('value');
	var szComment_len=szComment.length;
	if(szComment_len>0)
	{
		$("#add_comment_cost_processing").unbind("click");
		//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
		$("#add_comment_cost_processing").click(function(){add_comment_bulk_upload(id)});
		$("#add_comment_cost_processing").attr('style',"opacity:1");
	}
	else
	{
		$("#add_comment_cost_processing").unbind("click");
		$("#add_comment_cost_processing").attr('style',"opacity:0.4");
	}
}

function add_comment_bulk_upload(id)
{
	var szComment=$("#szComment").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,szComment:szComment,flag:'add_comment'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
	    $("#szComment").focus();      
	});
}


function sel_line_completed_for_cost_approval(div_id,id,status,iReviewStatus,filename)
{
	var rowCount = $('#cost_pproved_by_management tr').length;
	//alert(div_id);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="approved_cost_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#review_top_data").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#review_top_data").click(function(){review_uploaded_files(id)});
	$("#review_top_data").attr('style',"opacity:1");	
	
	$("#approved_top_data").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#approved_top_data").click(function(){approved_uploaded_files_data(id)});
	$("#approved_top_data").attr('style',"opacity:1");
	
	
	
	$("#delete_top_data").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_top_data").click(function(){delete_approved_uploaded_files_data(id)});
	$("#delete_top_data").attr('style',"opacity:1");
	
	$("#download_top_data").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#download_top_data").click(function(){download_uploaded_file(filename,id)});
	$("#download_top_data").attr('style',"opacity:1");
}

function approved_uploaded_files_data(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'open_approved_popup'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
		addPopupScrollClass('open_processing_popup');
	});
}

function confirm_approved(id)
{
	$("#open_processing_popup").attr('style',"display:none");
	$("#loader").attr('style','display:block');	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'confirm_approved_data'},function(result){
		$("#processing_cost_approved_list").html(result);
		$("#open_processing_popup").attr('style',"display:none");
		$("#loader").attr('style','display:none');	
		//show_forwarder_top_messges();
	})
}

function addWareHousesHaulageNew(maxrowexcced)
{
	var haulageCountry = $("#haulageCountry").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var haulagePricingModel = $("#haulagePricingModel").attr('value');
	
	var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
	var count = $("#haulageWarehouseAdded").attr('value');
	var res_ary = haulageWarehouse.split("_");
	
	//alert(haulageWarehouse);	
	
	if(haulageWarehouse!="" && haulagePricingModel!="")
	{
	
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{haulageWarehouse:haulageWarehouse,flag:'checkHaulageService',haulagePricingModel:haulagePricingModel},function(result){
		
				result_ary_service = result.split("||||");
				//alert(result_ary_service);
				if(result_ary_service[0]=='SUCCESS')
				{
					var res_haulage_model_ary = haulagePricingModel.split("_");
					if(idhaulageWarehouse=='')
					{
						var newvalue=res_ary[0]+'_'+res_ary[3]+'_'+res_haulage_model_ary[0];
						$("#idhaulageWarehouse").attr('value',newvalue);
					}
					else
					{
						var newvalue=res_ary[0]+'_'+res_ary[3]+'_'+res_haulage_model_ary[0];
						
						 var res_ary_new = idhaulageWarehouse.split(";");
						 var add_ware=jQuery.inArray(newvalue, res_ary_new);
						// alert(add_ware);
						if(add_ware!='-1')
						{
							return false;
						}
						
						var value1=idhaulageWarehouse+';'+newvalue;
						$("#idhaulageWarehouse").attr('value',value1);
					}
					var newcount=parseInt(count)+1;
					
					$("#haulageWarehouseAdded").attr('value',newcount);
					var div_id="haulage_data_"+count;
					$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{haulageCountry:haulageCountry,haulageWarehouse:haulageWarehouse,flag:'haulageAdd',count:newcount,haulagePricingModel:haulagePricingModel},function(result){
					
					$("#"+div_id).html(result);
					
					// $("#"+div_id).attr("onclick","delete_ware_haulage('"+newvalue+"','"+div_id+"','Haulage');");
					 $("#"+div_id).click(function(){delete_ware_haulage(newvalue,div_id,'Destination')});
					var divid="haulage_data_"+newcount;
					$('#haulageTable').append('<tr id='+divid+' onclick=""></tr>');
						var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
						
						if(haulageWarehouseValue!='')
						{
							//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
							$("#download_button").click(function(){downloadHaulageFormSubmit()});
							$("#download_button").attr("style","opacity:1;");
						}
						else
						{
							$("#download_button").unbind("click");
							//$("#download_button").attr("onclick","");
							$("#download_button").attr("style","opacity:0.4;");
						}
						var downloadType = $("#downloadType").val();
						if(downloadType=='2')
						{
							checkRowCountHaulage(maxrowexcced);
						}
						else if(downloadType=='1')
						{
							updatedNoteHaulage(maxrowexcced);
						}
						
					});
			}
			else
			{
				$("#row_excced_data_popup").html(result_ary_service[0]);
				return false;
			}
		});
	}
	else if(haulageWarehouse!="" && (haulagePricingModel=='' || haulagePricingModel=='undefined'))
	{
		var itemsHaulageModel = $("#haulagePricingModel option").length;
			var  itemsHaulageModel =itemsHaulageModel-1;
			
			if(itemsHaulageModel!=0 && itemsHaulageModel!='')
			{
				var values = [];
				$('#haulagePricingModel option').each(function() { 
				    values.push( $(this).attr('value') );
				});
				$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{haulageWarehouse:haulageWarehouse,flag:'checkHaulageServiceModel',haulagePricingModel:values},function(result)
				{
					
						
					result_ary_service = result.split("||||");
					//alert(result_ary_service);
					if(jQuery.trim(result_ary_service[0])=='SUCCESS')
					{	
						//alert(result_ary_service[2]);
						if(result_ary_service[2]!='')
						{
							var values1 = result_ary_service[2].split(";");
							var arr_len=values1.length;
							if(arr_len>0)
							{
								var values=[];
								values[0]='';
								var t=1;
								for(j=0;j<arr_len;++j)
								{
									values[t]=values1[j];
									++t;
								}
							}
						}
						
						//var values= unserialize(result_ary_service[2]);						
						var arrlen=values.length;
						
						for(i=1;i<arrlen;++i)
						{
							var newcount=$("#haulageWarehouseAdded").attr('value');
							var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
							var res_value_ary = values[i].split("_");
							//alert(res_value_ary);
							//alert(idOriginWarehouse);
							if(idhaulageWarehouse=='')
							{
								var newValueWare=values[i];
								var new_value_ware=res_ary[0]+'_'+res_ary[3]+'_'+res_value_ary[0];
								var value1=new_value_ware;
								var newvalue=res_ary[0]+'_'+res_ary[3]+'_'+res_value_ary[0];
								//$("#idOriginWarehouse").attr('value',value1);
					 		}
					 		else
					 		{
					 			var newvalue=res_ary[0]+'_'+res_ary[3]+'_'+res_value_ary[0];
					 			var res_ary_new = idhaulageWarehouse.split(";");
					 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
					 			//alert(add_ware);
					 			if(add_ware!='-1')
								{
									var newValueWare='';
								}
								else
								{
									var newValueWare=values[i];
									var new_value_ware=res_ary[0]+'_'+res_ary[3]+'_'+res_value_ary[0];
									var value1=idhaulageWarehouse+';'+newvalue;
									//$("#idOriginWarehouse").attr('value',value1);
								}					
					 		}
					 		//alert(new_value_ware);
					 		//alert(newValueWare);
							if(newValueWare!='undefined' && newValueWare!='')
							{
									div_id="haulage_data_"+newcount;
									//alert(div_id);
									var haulage="haulage";
									$("#"+div_id).html('<th onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[2]+'</th><td onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[1]+'</td><td onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_value_ary[1]+'</td>');
		
									
									//document.getElementById(div_id).innerHTML='<td>'+res_ary[2]+'</td><td>'+res_ary[1]+'</td>';
									
									
									//$("#"+div_id).attr("onclick","delete_ware_haulage('"+newvalue+"','"+div_id+"','haulage');");
									newcount=parseInt(newcount)+1;
									var divid="haulage_data_"+newcount;
									$('#haulageTable').append('<tr id='+divid+' onclick=""></tr>');
									if(value1!='undefined' && value1!='')
										$("#idhaulageWarehouse").attr('value',value1);
									
									$("#haulageWarehouseAdded").attr('value',newcount);
									
								}
							}
							
								var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
				
								if(haulageWarehouseValue!='')
								{
									//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
									$("#download_button").click(function(){downloadHaulageFormSubmit()});
									$("#download_button").attr("style","opacity:1;");
								}
								else
								{
									$("#download_button").unbind("click");
									//$("#download_button").attr("onclick","");
									$("#download_button").attr("style","opacity:0.4;");
								}
								
								var downloadType = $("#downloadType").val();
								if(downloadType=='2')
								{
									checkRowCountHaulage(maxrowexcced);
								}
								else if(downloadType=='1')
								{
									updatedNoteHaulage(maxrowexcced);
								}
						}
						else
						{
							$("#row_excced_data_popup").html(result_ary_service[0]);
							return false;
						}
					});
				}
	}
	else if(haulagePricingModel!="" && (haulageWarehouse=='' || haulageWarehouse=='undefined'))
	{
			var res_haulage_model_ary = haulagePricingModel.split("_");
                        
			//alert(newcount);
			var items = $("#haulageWarehouse option").length;
			var  items =items-1;
			//alert(items);
			if(items!=0 && items!='')
			{
				var values = [];
				$('#haulageWarehouse option').each(function() { 
				    values.push( $(this).attr('value') );
				});
				//alert(values.length);
				$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{haulageWarehouse:values,flag:'checkHaulageServiceWarehouse',haulagePricingModel:haulagePricingModel},function(result)
				{
					result_ary_service = result.split("||||");
						//alert(result_ary_service[0]);
					if(jQuery.trim(result_ary_service[0])=='SUCCESS')
					{
					
						//alert(result_ary_service[0]);
						if(result_ary_service[2]!='')
						{
							var values1 = result_ary_service[2].split(";");
							var arr_len=values1.length;
							if(arr_len>0)
							{
								var values=[];
								values[0]='';
								var t=1;
								for(j=0;j<arr_len;++j)
								{
									values[t]=values1[j];
									++t;
								}
							}
						}
                                                if(result_ary_service[3]!='')
						{
							var values3 = result_ary_service[3].split(";");
							
						}
						var arrlen=values.length;
						for(i=1;i<arrlen;++i)
						{
                                                        var t =i-1;
							var newcount=$("#haulageWarehouseAdded").attr('value');
							var idhaulageWarehouse = $("#idhaulageWarehouse").attr('value');
							var res_ary = values[i].split("_");
                                                        var iWarehouseType=values3[t];
							//alert(idOriginWarehouse);
							if(idhaulageWarehouse=='')
							{
								var newValueWare=values[i];
								var new_value_ware=res_ary[0]+'_'+res_ary[3]+'_'+res_haulage_model_ary[0];
								var value1=new_value_ware;
								var newvalue=res_ary[0]+'_'+res_ary[3];
								//$("#idOriginWarehouse").attr('value',value1);
					 		}
					 		else
					 		{
					 			var newvalue=res_ary[0]+'_'+res_ary[3]+'_'+res_haulage_model_ary[0];
					 			var res_ary_new = idhaulageWarehouse.split(";");
					 			var add_ware=jQuery.inArray(newvalue, res_ary_new);
					 			//alert(add_ware);
					 			if(add_ware!='-1')
								{
									var newValueWare='';
								}
								else
								{
									var newValueWare=values[i];
									var new_value_ware=res_ary[0]+'_'+res_ary[3]+'_'+res_haulage_model_ary[0];
									var value1=idhaulageWarehouse+';'+newvalue;
									//$("#idOriginWarehouse").attr('value',value1);
								}					
					 		}
                                                        var textModel='';
					 		//alert(new_value_ware);
					 		//alert(newValueWare);
							if(newValueWare!='undefined' && newValueWare!='')
							{
                                                                        if(res_haulage_model_ary[0]=='4')
                                                                        {
                                                                            if(iWarehouseType==2)
                                                                            {
                                                                                 textModel = res_haulage_model_ary[1]+" from the airport warehouse";
                                                                            }
                                                                            else
                                                                            {
                                                                                 textModel = res_haulage_model_ary[1]+" from the CFS";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                             textModel = res_haulage_model_ary[1];
                                                                        }
									div_id="haulage_data_"+newcount;
									//alert(div_id);
									var haulage="haulage";
									$("#"+div_id).html('<th onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[2]+'</th><td onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+res_ary[1]+'</td><td onclick="delete_ware_haulage(\''+newvalue+'\',\''+div_id+'\',\''+haulage+'\')">'+textModel+'</td>');
		
									
									//document.getElementById(div_id).innerHTML='<td>'+res_ary[2]+'</td><td>'+res_ary[1]+'</td>';
									
									
									//$("#"+div_id).attr("onclick","delete_ware_haulage('"+newvalue+"','"+div_id+"','haulage');");
									newcount=parseInt(newcount)+1;
									var divid="haulage_data_"+newcount;
									$('#haulageTable').append('<tr id='+divid+' onclick=""></tr>');
									if(value1!='undefined' && value1!='')
										$("#idhaulageWarehouse").attr('value',value1);
									
									$("#haulageWarehouseAdded").attr('value',newcount);
									
								}
							}
							
							var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
				
							if(haulageWarehouseValue!='')
							{
								//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
								$("#download_button").click(function(){downloadHaulageFormSubmit()});
								$("#download_button").attr("style","opacity:1;");
							}
							else
							{
								$("#download_button").unbind("click");
								//$("#download_button").attr("onclick","");
								$("#download_button").attr("style","opacity:0.4;");
							}
							
							var downloadType = $("#downloadType").val();
							if(downloadType=='2')
							{
								checkRowCountHaulage(maxrowexcced);
							}
							else if(downloadType=='1')
							{
								updatedNoteHaulage(maxrowexcced);
							}
						}
						else
						{
							$("#row_excced_data_popup").html(result_ary_service[0]);
							return false;
						}
					});
				}
				
	}
	else 
	{
		//alert(haulageCountry);
		if(haulageCountry!='')
		{			
			var haulageWarehouseAdded=$("#haulageWarehouseAdded").attr('value');
			var idhaulageWarehouse=$("#idhaulageWarehouse").attr('value'); 
			var divid="haulage_data_"+haulageWarehouseAdded;
			//alert(divid);
			document.getElementById("haulageTable").deleteRow(haulageWarehouseAdded);
			$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{haulageCountry:haulageCountry,haulageWarehouseAdded:haulageWarehouseAdded,idhaulageWarehouse:idhaulageWarehouse,flag:'HaulageAddWarehouseAll'},function(result){
			
			$('#haulageTable').append(result);
			var haulageWarehouseValue = $("#idhaulageWarehouse").attr('value');
			
			if(haulageWarehouseValue!='')
			{
				//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
				$("#download_button").click(function(){downloadHaulageFormSubmit()});
				$("#download_button").attr("style","opacity:1;");
			}
			else
			{
				$("#download_button").unbind("click");
				//$("#download_button").attr("onclick","");
				$("#download_button").attr("style","opacity:0.4;");
			}
			var downloadType = $("#downloadType").val();
			if(downloadType=='2')
			{
				checkRowCountHaulage(maxrowexcced);
			}
			else if(downloadType=='1')
			{
				updatedNoteHaulage(maxrowexcced);
			}
			});
		}
		else
		{
			$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{flag:'HaulageAddAll'},function(result){
				
				$("#haulageTable").html(result);
				
				var haulageWarehouseValue = $("#idhaulagesWarehouse").attr('value');
				
				if(haulageWarehouseValue!='')
				{
					//$("#download_button").attr("onclick","downloadHaulageFormSubmit()");
					$("#download_button").click(function(){downloadHaulageFormSubmit()});
					$("#download_button").attr("style","opacity:1;");
				}
				else
				{
					$("#download_button").unbind("click");
					//$("#download_button").attr("onclick","");
					$("#download_button").attr("style","opacity:0.4;");
				}
				var downloadType = $("#downloadType").val();
				if(downloadType=='2')
				{
					checkRowCountHaulage(maxrowexcced);
				}
				else if(downloadType=='1')
				{
					updatedNoteHaulage(maxrowexcced);
				}
			});
		}
 	}
			
}

function checkRowCountHaulage(maxrowexcced)
{
	var haulageWarehouseAdded = $("#idhaulageWarehouse").attr('value');
	var res_ary_haulageWarehouseAdded = haulageWarehouseAdded.split(";");
	//alert(res_ary_haulageWarehouseAdded);
	var haulageWarehouseAddedlen='0';
	
	if(res_ary_haulageWarehouseAdded!='')
	{
		var haulageWarehouseAddedlen=res_ary_haulageWarehouseAdded.length;
	}
	//alert(deswarehouselen);
	
	var mainurl = $("#mainurl").attr('value');
	var changeDropDown = $("#changeDropDown").attr('value');
	//alert('hi3');
	if(haulageWarehouseAddedlen>0)
	{
		//alert(haulageWarehouseAddedlen);
		totalCount=parseInt(haulageWarehouseAddedlen);
		if(totalCount>maxrowexcced)
		{
			//alert($("#row_excced_data"));
			$("#download_button").unbind("click");
			$("#download_button").attr("style","opacity:0.4;");
			
			//$("#total_row_excced").html(number_format_js(totalCount));
			
			//alert(changeDropDown);
			if(totalCount>changeDropDown)
			{
				$("#downloadType").val("1");
			}
			updatedNoteHaulage(maxrowexcced);
		}
		else
		{
			//alert('hi');
			if(totalCount>changeDropDown)
			{
				$("#download_button").unbind("click");
				$("#download_button").attr("style","opacity:0.4;");
				$("#downloadType").val("1");
				updatedNoteHaulage(maxrowexcced);
			}
			else
			{
				var totaltime=(__TIME_PER_ROW_CREATE__*totalCount);
				//alert(totaltime);
			 	if(totaltime>60)
			 	{
			 		var minutes = Math.floor(totaltime/60);
			 		var secondsleft = totaltime%60;
                                        secondsleft = Math.ceil(secondsleft);
                                        
			 		if(minutes=='1')
			 		{
			 			var minStr=minutes+" minute"; 
			 		}
			 		else
			 		{
			 			var minStr=minutes+" minutes"; 
			 		}
			 		if(secondsleft=='1')
			 		{
			 			var secstr=secondsleft+" second";
			 		}
			 		else
			 		{
			 			var secstr=Math.ceil(secondsleft)+" seconds";
			 		}
			 		
			 		var timestr=minStr+" "+secstr;
			 	}
			 	else
			 	{
			 		var timetaken=Math.ceil(totaltime)
			 		if(timetaken=='1')
			 		{
			 			var timestr=timetaken+" second";
			 		}
			 		else
			 		{
			 			var timestr=timetaken+" seconds";
			 		}
			 	}
				$("#total_row_excced_time").html((timestr));
				$("#row_excced_data").attr("style","display:none");
				$("#row_excced_data_1").attr("style","display:none");
				$("#row_excced_data_2").attr("style","display:block");
				$("#total_row_excced_1").html(number_format_js(totalCount));
			}
		}
	}
}

function updatedNoteHaulage(maxrowexcced)
{
		
		$("#row_excced_data").attr("style","display:none");
		var mainurl = $("#mainurl").attr('value');
		$("#row_excced_data").attr("style","display:none");
		$("#row_excced_data_2").attr("style","display:none");
		$("#row_excced_data_1").attr("style","display:block;text-align:left;valign:middle;");
		$("#download_button").unbind("click");
		$("#download_button").attr("style","opacity:0.4;");
		var value= $("#bulkExportData").serialize();
		var new_value=value+'&flag=haulageDataMsg';
		$("#row_excced_data_1").html("<span class='fl-15' style='width:16%;'>Note: Loading....</span><img style='margin: 0 0 0 10px;float:left;position:static;' src='"+mainurl+"/images/325.gif' alt='' />");
		$("#downloadType").attr("disabled","disabled");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",new_value,function(result){
			//alert(result);
			var maxrowexcced=$("#maxrowsexcced").attr('value');
	 		result_ary = result.split("||||");
	 		//alert(result_ary[1]);
	 		//alert(maxrowexcced);
		 	if(parseInt(result_ary[1])>parseInt(maxrowexcced))
		 	{
		 		//alert('hi2');
			 	$("#row_excced_data").attr("style","display:block;");
			 	$("#total_row_excced").html(number_format_js(result_ary[1]));
			 	$("#row_excced_data_1").attr("style","display:none");
			 	$("#row_excced_data_2").attr("style","display:none");
			 }
			 else
			 {
			 	//alert('hi1');
			 	//alert(__TIME_PER_ROW_CREATE__);
			 	var totaltime=(__TIME_PER_ROW_CREATE__*result_ary[1]);
			 	if(totaltime>60)
			 	{
			 		var minutes = Math.floor(totaltime/60);
			 		var secondsleft = totaltime%60;
                                        secondsleft = Math.ceil(secondsleft);
			 		if(minutes=='1')
			 		{
			 			var minStr=minutes+" minute"; 
			 		}
			 		else
			 		{
			 			var minStr=minutes+" minutes"; 
			 		}
			 		if(secondsleft=='1')
			 		{
			 			var secstr=secondsleft+" second";
			 		}
			 		else
			 		{
			 			var secstr=secondsleft+" seconds";
			 		}
			 		
			 		var timestr=minStr+" "+secstr;
			 	}
			 	else
			 	{
			 		var timetaken=Math.ceil(totaltime)
			 		if(timetaken=='1')
			 		{
			 			var timestr=timetaken+" second";
			 		}
			 		else
			 		{
			 			var timestr=timetaken+" seconds";
			 		}
			 	}
			 	//alert(timestr);
			 	$("#download_button").unbind("click");
			 	$("#row_excced_data_2").attr("style","display:block;");
			 	$("#total_row_excced_1").html(number_format_js(result_ary[1]));
			 	$("#total_row_excced_time").html((timestr));
			 	$("#row_excced_data_1").attr("style","display:none");
			 	$("#row_excced_data").attr("style","display:none");
			 	$("#download_button").click(function(){downloadHaulageFormSubmit()});
				$("#download_button").attr("style","opacity:1;");
			 }
			 $('#downloadType').removeAttr('disabled');
	 	});
}

function close_popup_no_haulage_service()
{
	$("#no_haulage_service").attr("style","display:none;");
}

function show_add_edit_city_button(mode)
{

	var radius=$("#iDistance").attr('value');
	var transittime=$("#idTransitTime").val();
	//alert(transittime);
	if(transittime!='' && parseInt(radius)>0)
	{
		$("#save_city").unbind("click");
  		$("#save_city").attr('style',"opacity:1.0");
  		if(mode=='add')
  		{
  			$("#save_city").click(function(){add_city_data()});
  		}
  		else if(mode=='edit')
  		{
  			$("#save_city").click(function(){save_city_data()});
  		}
  	}
  	else
  	{
  		$("#save_city").unbind("click");
  		$("#save_city").attr('style',"opacity:0.4");
  	}
}

function sel_warehouse_for_haulage_model(idForwarder,idCountry,idWarehouse,divid)
{
	$("#haulagePricingModel").attr("disabled",true);
	$("#haulage_add").attr("style","opacity:0.4;");
	$("#haulage_add").attr("onclick","");
		
	var showflag ='update_haulage_pricing_model';
	$.get(__JS_ONLY_SITE_BASE__+"/ajax_bulkDataExportLCL.php",{idForwarder:idForwarder,idCountry:idCountry,idWarehouse:idWarehouse,flag:showflag},function(result){
	$("#"+divid).html(result);
	$("#haulagePricingModel").attr("disabled",false);
	$("#haulage_add").attr("style","opacity:1;");
	$("#haulage_add").unbind("click");
	//$("#haulage_add").attr("onclick","addWareHousesHaulage();");
	$("#haulage_add").click(function(){addWareHousesHaulageNew()});
	});
}

function enable_add_edit_button(mode)
{
	var szPostCodeStr1=$("#szPostCodeStr1").attr('value');
	var idTransitTime=$("#idTransitTime").attr('value');
	var szName=$("#szName").attr('value');
	
	if(szPostCodeStr1!='' && idTransitTime!='' && szName!='')
	{
		$("#save_postcode").unbind("click");
  		$("#save_postcode").attr('style',"opacity:1.0");
  		if(mode=='add')
  		{
  			$("#save_postcode").click(function(){add_postcode_data()});
  		}
  		else if(mode=='edit')
  		{
  			$("#save_postcode").click(function(){save_postcode_data()});
  		}
  	}
  	else
  	{
  		$("#save_postcode").unbind("click");
  		$("#save_postcode").attr('style',"opacity:0.4");
  	}
}

function enable_add_edit_button_distance(mode)
{
	var szCountriesStr=$("#szCountriesStr").attr('value');
	var idTransitTime=$("#idTransitTime").attr('value');
	var iDistanceKm=$("#iDistanceKm").attr('value');
	//var mode=$("#mode").attr('value');
	//alert(mode);
	//alert(szCountriesStr);
	if(szCountriesStr!='' && idTransitTime!='' && parseInt(iDistanceKm)>0)
	{
		//alert('hi');
		$("#save_distance").unbind("click");
  		$("#save_distance").attr('style',"opacity:1.0");
  		if(mode=='add')
  		{
  			$("#save_distance").click(function(){add_distance_data()});
  		}
  		else if(mode=='edit')
  		{
  			$("#save_distance").click(function(){save_distance_data()});
  		}
  	}
  	else
  	{
  		$("#save_distance").unbind("click");
  		$("#save_distance").attr('style',"opacity:0.4");
  	}
}

/*function on_enter_key_press(kEvent,functionName,params,page_url)
{
	if(kEvent.keyCode==13)
	{
		if(functionName=='obo_haulage_tryit_out')
		{
			obo_haulage_tryit_out();
		}	
	}
}*/

function enable_add_edit_button_try_test(kEvent)
{
    var szLocation = '';
    if($("#iForwarderTryitoutFlag").length)
    {
        szLocation = $("#szLocation").attr('value');
    }
    else
    {
        szLocation = $("#szPostCodeCity").attr('value');
    }
    
    var szCargoWieght=$("#szCargoWieght").attr('value');
    var szCargoVolume=$("#szCargoVolume").attr('value');

    if(szLocation!='' && szCargoVolume!='' && szCargoWieght!='')
    {
        if(kEvent)
        {
            if(kEvent.keyCode==13)
            {
                obo_haulage_tryit_out();
            }	
        }
        $("#test_try_out").unbind("click");
        $("#test_try_out").attr('style',"opacity:1.0");
        $("#test_try_out").click(function(){obo_haulage_tryit_out()});
    }
    else
    {
        $("#test_try_out").unbind("click");
        $("#test_try_out").attr('style',"opacity:0.4");
    }
}

function open_copy_haulage_postcode_popup(idHaulageModel,idWarehouse,idDirection)
{
	var  divid="postcode_detail_delete_"+idHaulageModel;
	var showflag="open_copy_postcode_popup";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		addPopupScrollClass(divid);
		$("#"+divid).attr('style',"display:block");
	});
}

function copy_postcode()
{
	idCountry=$('#haulageCountryZone').val();
	szCity=$('#szCityZone').val();
	idWarehouse=$('#haulageWarehouseZone').val();
	idDirection=$('#idDirectionZone').val();
	var showflag='copy_postcode_data';
	var value=$("#copyPostCode").serialize();
	var newvalue=value+"&idCountry="+idCountry+"&szCity="+szCity+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&flag="+showflag;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
		$("#show_copy_data").html(result);
		$("#show_copy_data").attr("style","display:block");
		});
}

function enable_add_edit_button_zone(mode)
{
	var idTransitTime=$("#idTransitTime").attr('value');
	var szName=$("#szName").attr('value');
	
	if(idTransitTime!='' && szName!='')
	{
		$("#save_zone").unbind("click");
  		$("#save_zone").attr('style',"opacity:1.0");
  		if(mode=='add')
  		{
  			$("#save_zone").click(function(){add_zone_data()});
  		}
  		else if(mode=='edit')
  		{
  			$("#save_zone").click(function(){save_zone_data()});
  		}
  	}
  	else
  	{
  		$("#save_zone").unbind("click");
  		$("#save_zone").attr('style',"opacity:0.4");
  	}
}

function view_management_uploaded_files(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'view_files_management'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
	});
}

function review_uploaded_files(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'review_and_confrim_data'},function(result){
		$("#approve_data_review").html(result);
	});
}


function open_save_button(idHaulageModel,idWarehouse,idDirection)
{
	var mode=$("#mode").attr('value');
	
	var fPricePer100Kg=$("#fPricePer100Kg").attr('value');
	var fMinimumPrice=$("#fMinimumPrice").attr('value');
	var fPricePerBooking=$("#fPricePerBooking").attr('value');
	var iUpToKg=$("#iUpToKg").attr('value');
	var idCurrency=$("#idCurrency").val();
	
	var id =$("#idHaulagePricingModel").attr('value');
	
	if(jQuery.trim(idCurrency)!='' && fPricePer100Kg!='' && fMinimumPrice!='' && fPricePerBooking!='' && parseInt(iUpToKg)>0)
	{
		if(mode=='add')
		{
			if(idHaulageModel=='3')
			{
				$("#save_postcode_detail_line").attr('style',"opacity:1");
				$("#save_postcode_detail_line").unbind("click");
				$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
			else if(idHaulageModel=='4')
			{
				$("#save_distance_detail_line").attr('style',"opacity:1");
				$("#save_distance_detail_line").unbind("click");
				$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
			else if(idHaulageModel=='2')
			{
				$("#save_city_detail_line").attr('style',"opacity:1");
				$("#save_city_detail_line").unbind("click");
				$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
			else if(idHaulageModel=='1')
			{
				$("#save_zone_detail_line").attr('style',"opacity:1");
				$("#save_zone_detail_line").unbind("click");
				$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
			
		}
		else if(mode=='edit')
		{
			var idEdit=$("#idEdit").attr('value');
			if(idHaulageModel=='3')
			{
				$("#save_postcode_detail_line").attr("onclick",'');	
				$("#save_postcode_detail_line").unbind("click");
				$("#save_postcode_detail_line").attr('style',"opacity:1");
				$("#save_postcode_detail_line").click(function(){save_haulage_zone_line(idEdit,idHaulageModel,idWarehouse,idDirection,id,iUpToKg)});
			}
			else if(idHaulageModel=='4')
			{
				$("#save_distance_detail_line").attr("onclick",'');	
				$("#save_distance_detail_line").unbind("click");
				$("#save_distance_detail_line").attr('style',"opacity:1");
				$("#save_distance_detail_line").click(function(){save_haulage_zone_line(idEdit,idHaulageModel,idWarehouse,idDirection,id,iUpToKg)});
			}
			else if(idHaulageModel=='2')
			{
				$("#save_city_detail_line").attr("onclick",'');	
				$("#save_city_detail_line").unbind("click");
				$("#save_city_detail_line").attr('style',"opacity:1");
				$("#save_city_detail_line").click(function(){save_haulage_zone_line(idEdit,idHaulageModel,idWarehouse,idDirection,id,iUpToKg)});
			}
			else if(idHaulageModel=='1')
			{
				$("#save_zone_detail_line").attr("onclick",'');	
				$("#save_zone_detail_line").unbind("click");
				$("#save_zone_detail_line").attr('style',"opacity:1");
				$("#save_zone_detail_line").click(function(){save_haulage_zone_line(idEdit,idHaulageModel,idWarehouse,idDirection,id,iUpToKg)});
			}
		}
	}
	else
	{
		if(idHaulageModel=='3')
			{
				$("#save_postcode_detail_line").attr('style',"opacity:0.4");
				$("#save_postcode_detail_line").unbind("click");
				//$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
			else if(idHaulageModel=='4')
			{
				$("#save_distance_detail_line").attr('style',"opacity:0.4");
				$("#save_distance_detail_line").unbind("click");
				//$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
			else if(idHaulageModel=='2')
			{
				$("#save_city_detail_line").attr('style',"opacity:0.4");
				$("#save_city_detail_line").unbind("click");
				//$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
			else if(idHaulageModel=='1')
			{
				$("#save_zone_detail_line").attr('style',"opacity:0.4");
				$("#save_zone_detail_line").unbind("click");
				//$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
			}
	}
}

function clear_haulage_tryitout()
{
	$('INPUT:text', '#obo_haulage_tryit_out_form').val(''); 
	
	$("#szLatitude").attr('value','');
	$("#szLongitude").attr('value','');
	
	$("#tryitout_result_div").html("");
	$("#tryitout_result_div_error").html("");
	$("#tryitout_result_div").css('display','none');
	$("#tryitout_result_div_error").css('display','none');
	$("#test_try_out").unbind("click");
 	$("#test_try_out").attr('style',"opacity:0.4");
}

function delete_pricing_cc_appoving_data_open_popup(id,idForwarder)
{
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_pricing_cc_approval_data_open_popup',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder},function(result){
		$("#show_pricing_approval_obo_delete").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:block');
	});
}

function approved_pricing_cc_data(id,idForwarder)
{
	var idService=$("#idService").attr('value');
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	var value=$("#update_obo_cc_form").serialize();
	var value1=$("#obo_cc_data_export_form").serialize();
	
	$("#loader").attr('style','display:block');
	var new_value=value+'&'+value1+'&id='+id+'&flag=approve_pricing_cc_approval_data&dataTotalCount='+dataTotalCount+'&dataCounter='+dataCounter+'&idForwarder='+idForwarder+'&idService='+idService;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",new_value,function(result){
	$("#show_pricing_approval_obo").html(result);
	//$("#loader").attr('style','display:none');
	window.scrollTo(0,0);
	});
}

function delete_pricing_cc_appoving_data(id,idForwarder)
{
	var idService=$("#idService").attr('value');
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	
	$("#delete_data_confirm_no").attr('style',"opacity:0.4");
	$("#delete_data_confirm_no").unbind("click");
				
	$("#delete_data_confirm").attr('style',"opacity:0.4");
	$("#delete_data_confirm").unbind("click");
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_pricing_cc_approval_data',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder,idService:idService},function(result){
		$("#show_pricing_approval_obo").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:none');
		window.scrollTo(0,0);
	});
}

function close_delete_confirm_popup_approval()
{
	$("#delete_confirm_popup").attr('style','display:none');
	removePopupScrollClass('delete_confirm_popup');
}

function delete_pricing_wtw_appoving_data_open_popup(id,idForwarder)
{
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_pricing_wtw_approval_data_open_popup',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder},function(result){
		$("#show_pricing_approval_obo_delete").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:block');
	});
}


function delete_pricing_wtw_appoving_data(id,idForwarder)
{
	var idService=$("#idService").attr('value');
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	
	$("#delete_data_confirm_no").attr('style',"opacity:0.4");
	$("#delete_data_confirm_no").unbind("click");
				
	$("#delete_data_confirm").attr('style',"opacity:0.4");
	$("#delete_data_confirm").unbind("click");
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_pricing_wtw_approval_data',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder,idService:idService},function(result){
		$("#show_pricing_approval_obo").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:none');
		window.scrollTo(0,0);
	});
}


function approved_pricing_wtw_data(id,idForwarder)
{
	var idService=$("#idService").attr('value');
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	var value=$("#obo_lcl_bottom_form").serialize();
	var value1=$("#obo_cc_data_export_form").serialize();
	$("#loader").attr('style','display:block');
	var new_value=value+'&'+value1+'&id='+id+'&flag=approve_pricing_wtw_approval_data&dataTotalCount='+dataTotalCount+'&dataCounter='+dataCounter+'&idForwarder='+idForwarder+'&idService='+idService;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",new_value,function(result){
	$("#show_pricing_approval_obo").html(result);
	//$("#loader").attr('style','display:none');
	window.scrollTo(0,0);
	});
}
function clear_post_code()
{
	$("#szLatitude").attr('value','');
	$("#szLongitude").attr('value','');
	$("#szPostCodeCity").attr('value','');
}

function sel_haulage_zone_detail_data_temp(div_id,id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel,wmfactor,idService)
{
	var rowCount = $('#view_haulage_model_temp_detail tr').length;
	//alert(div_id);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_pricing_temp_data_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#delete_temp_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_temp_detail").click(function(){delete_haulage_pricing_line_temp(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel,idService)});
	$("#delete_temp_detail").attr('style',"opacity:1");
	
	
	$("#temp_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#temp_edit_detail").click(function(){edit_haulage_zone_line_temp(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg,wmfactor,idService)});
	$("#temp_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_temp_detail_line").unbind("click");
	$("#cancel_temp_detail_line").attr('style',"opacity:0.4");
	
	
	$("#save_temp_detail_line").unbind("click");
	$("#save_temp_detail_line").click(function(){add_haulage_pricing_line_temp(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingZoneData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
}

function delete_haulage_pricing_line_temp(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel,idService)
{
	var showflag="delete_haulage_temp_line";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,iUpToKg:iUpToKg,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,idService:idService},function(result){
		$("#approve_haulage_data_popup").attr("style","display:block");
		$("#approve_haulage_data_popup").html(result);
	});
}

function delete_haulage_pricing_line_confirm_temp(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,idService)
{	
	var showflag="delete_haulage_temp_line_confirm";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,idService:idService},function(result){
		$("#approve_haulage_data_list").html(result);
		$("#approve_haulage_data_popup").attr("style","display:none");
		$("#save_temp_detail_line").unbind("click");
		$("#save_temp_detail_line").attr('style',"opacity:1");
		$("#save_temp_detail_line").click(function(){add_haulage_pricing_line_temp(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
		
	});
}

function edit_haulage_zone_line_temp(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg,wmfactor,idService)
{
	var showflag="edit_haulage_temp_line";
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,wmfactor:wmfactor,idService:idService},function(result){
		$("#temp_edit_form").html(result);
		$("#mode").attr('value','edit');		
		$("#delete_temp_detail").unbind("click");
		$("#delete_temp_detail").attr('style',"opacity:0.4");
	
	
		$("#temp_edit_detail").unbind("click");
		$("#temp_edit_detail").attr('style',"opacity:0.4")
		
		$("#cancel_temp_detail_line").unbind("click");
		$("#cancel_temp_detail_line").attr('style',"opacity:1");
		$("#cancel_temp_detail_line").click(function(){cancel_haulage_zone_line_temp(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
		
		$("#save_temp_detail_line").attr("onclick",'');	
		$("#save_temp_detail_line").unbind("click");
		$("#save_temp_detail_line").attr('style',"opacity:1");
		$("#save_temp_detail_line").click(function(){save_haulage_zone_line_temp(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	});
}

function cancel_haulage_zone_line(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	$("#delete_temp_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#delete_temp_detail").click(function(){delete_haulage_pricing_line_temp(id,idHaulageModel,iUpToKg,idWarehouse,idDirection,idHaulagePricingModel)});
	$("#delete_temp_detail").attr('style',"opacity:1");
	
	
	$("#temp_edit_detail").unbind("click");
	//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
	$("#temp_edit_detail").click(function(){edit_haulage_zone_line_temp(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)});
	$("#temp_edit_detail").attr('style',"opacity:1");
	
	$("#cancel_temp_detail_line").unbind("click");
	$("#cancel_temp_detail_line").attr('style',"opacity:0.4");
	
	$("#save_temp_detail_line").unbind("click");
	//$("#save_zone_detail_line").attr('style',"opacity:0.4");
	$("#save_temp_detail_line").click(function(){add_haulage_pricing_line_temp(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
	$('INPUT:text,SELECT', '#updatePricingZoneData').val(''); 
	$("#mode").attr('value','add');
	$("#idHaulagePricingModel").attr('value',idHaulagePricingModel);
	var disp = $("#regError").css('display');
	if(disp=='block')
	{
		$("#regError").attr('style',"display:none");
	}
}

function save_haulage_zone_line_temp(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel,iUpToKg)
{
	var  divid="temp_edit_form";
	var showflag="save_pricing_line_temp";
	var value=$("#updatePricingTempData").serialize();
	var newvalue=value+"&id="+id+"&idHaulageModel="+idHaulageModel+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&idHaulagePricingModel="+idHaulagePricingModel+"&iUpToKg1="+iUpToKg+"&flag="+showflag;
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",newvalue,function(result){
		$("#"+divid).html(result);
	});
}

function add_haulage_pricing_line_temp(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)
{
	var  divid="temp_edit_form";
	var value=$("#updatePricingTempData").serialize();
	var showflag="save_pricing_line_temp";
		
	var newvalue=value+"&idHaulageModel="+idHaulageModel+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&idHaulagePricingModel="+idHaulagePricingModel+"&flag="+showflag;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",newvalue,function(result){
		$("#"+divid).html(result);
	});
}

function close_delete_confirm_popup_temp()
{
	$("#approve_haulage_data_popup").html("");
}


function open_save_button_temp(idHaulageModel,idWarehouse,idDirection)
{
	var mode=$("#mode").attr('value');
	
	var fPricePer100Kg=$("#fPricePer100Kg").attr('value');
	var fMinimumPrice=$("#fMinimumPrice").attr('value');
	var fPricePerBooking=$("#fPricePerBooking").attr('value');
	var iUpToKg=$("#iUpToKg").attr('value');
	var idCurrency=$("#idCurrency").val();
	
	var id =$("#idHaulagePricingModel").attr('value');
	
	if(jQuery.trim(idCurrency)!='' && fPricePer100Kg!='' && fMinimumPrice!='' && fPricePerBooking!='' && parseInt(iUpToKg)>0)
	{
		if(mode=='add')
		{		
			$("#save_temp_detail_line").attr('style',"opacity:1");
			$("#save_temp_detail_line").unbind("click");
			$("#save_temp_detail_line").click(function(){add_haulage_pricing_line_temp(idHaulageModel,idWarehouse,idDirection,id)});
						
		}
		else if(mode=='edit')
		{
			var idEdit=$("#idEdit").attr('value');
			$("#save_temp_detail_line").attr("onclick",'');	
			$("#save_temp_detail_line").unbind("click");
			$("#save_temp_detail_line").attr('style',"opacity:1");
			$("#save_temp_detail_line").click(function(){save_haulage_zone_line_temp(idEdit,idHaulageModel,idWarehouse,idDirection,id,iUpToKg)});
			
		}
	}
	else
	{
		if(idHaulageModel=='3')
		{
			$("#save_temp_detail_line").attr('style',"opacity:0.4");
			$("#save_temp_detail_line").unbind("click");
			//$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
		}
	}
}

function save_haulage_temp_line_confirm(id,idHaulageModel,idWarehouse,idDirection,wmfactor,idService)
{
		var showflag="pricing_temp_detail_list";
		var newdiv_id="zone_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:wmfactor,idService:idService},function(result){
		$("#approve_haulage_data_list").html(result);
		$("#mode").attr('value','add');
		$("#idHaulagePricingModel").attr('value',id);
		$("#save_temp_detail_line").attr("style","opacity:0.4");
		$("#save_temp_detail_line").unbind("click");
		//$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,id)});
		$("#loader").attr('style','display:none;');
		});
}

function delete_data_haulage_approval_open_popup(idForwarder,idData)
{
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:idData,flag:'delete_haulage_approval_data_open_popup',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder},function(result){
		$("#show_pricing_approval_obo_delete").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:block');
	});
}

function delete_haulage_appoving_data(idService,idForwarder)
{
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	
	var idWarehouse=$("#haulageWarehouse").attr('value');
	var iDirection =$("#idDirection").attr('value');
	var idHaulageModel=$("#haulageModelApproveData").attr('value');
	
	var idService=$("#idService").attr('value');
	$("#delete_data_confirm_no").attr('style',"opacity:0.4");
	$("#delete_data_confirm_no").unbind("click");
				
	$("#delete_data_confirm").attr('style',"opacity:0.4");
	$("#delete_data_confirm").unbind("click");
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{idWarehouse:idWarehouse,iDirection:iDirection,idHaulageModel:idHaulageModel,flag:'delete_haulage_approval_data',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder,idService:idService},function(result){
		$("#show_pricing_approval_obo").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:none');
		window.scrollTo(0,0);
	});
}

function approving_haulage_data(idForwarder,idData)
{
	var dataCounter=$("#dataCounter").attr('value');
	var idService=$("#idService").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	
	
	var idWarehouse=$("#haulageWarehouse").attr('value');
	var iDirection =$("#idDirection").attr('value');
	var idHaulageModel=$("#haulageModelApproveData").attr('value');
	
	//var value=$("#obo_lcl_bottom_form").serialize();
	$("#loader").attr('style','display:block');
	var new_value='id='+idData+'&flag=approve_haulage_approval_data&dataTotalCount='+dataTotalCount+'&dataCounter='+dataCounter+'&idForwarder='+idForwarder+'&idService='+idService+'&idWarehouse='+idWarehouse+'&iDirection='+iDirection+'&idHaulageModel='+idHaulageModel;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",new_value,function(result){
	$("#show_pricing_approval_obo").html(result);
	//$("#loader").attr('style','display:none');
	window.scrollTo(0,0);
	});
}

function checkPageRedirectionBulkService(path)
{		
	//alert("hi");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{flag:'LEAVE_PAGE',path:path},function(result){
			//check = true;
			//$('#delete_confirm_popup').css('display','block');
			$('#show_pricing_approval_obo_delete').html(result);
			addPopupScrollClass('delete_confirm_popup');
		});	
}

function redirect_page_bulk_service(path)
{
	$("#delete_data_confirm_no").attr('style',"opacity:0.4");
	$("#delete_data_confirm_no").unbind("click");
				
	$("#delete_data_confirm").attr('style',"opacity:0.4");
	$("#delete_data_confirm").unbind("click");
	//removePopupScrollClass('delete_confirm_popup');
	window.location.href=path;
}

function mapLocationOnchangeCFSBulk(id)
{	
	var id = parseInt(id);
	var latLong;
	if(id>0)
	{		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'mapCountry',id:id},function(result){
		latLong = result.split(',');
		lat =  parseInt(latLong[0]);
		long = parseInt(latLong[1]);
		flag = 1;
		var iframe = window.parent.document.getElementById('draw_map_iframe');
		iframe.src=__JS_ONLY_SITE_BASE__+'/cfsLocationBulkMap.php?szLatitude='+lat+'&szLongitude='+long+'&flag='+flag;
		});
	}
}

function submit_warehouse_data_bulk(idForwarder,idData)
{
	var dataCounter=$("#dataCounter").attr('value');
	var idService=$("#idService").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	var idBatch=$("#idBatch").attr('value');
	$("#loader").attr('style','display:block');
	var value=$("#addEditWarehouse").serialize();
	var new_value=value+'&id='+idData+'&flag=approve_cfs_location_approval_data&dataTotalCount='+dataTotalCount+'&dataCounter='+dataCounter+'&idForwarder='+idForwarder+'&idService='+idService+'&idBatch='+idBatch;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",new_value,function(result){
	$("#show_pricing_approval_obo").html(result);
	//$("#loader").attr('style','display:none');
	window.scrollTo(0,0);
	});
}

function delete_warehouse_data_bulk_open_popup(idForwarder,idData)
{
	var dataCounter=$("#dataCounter").attr('value');
	var idService=$("#idService").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:idData,flag:'delete_cfs_location_approval_data_open_popup',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder},function(result){
		$("#show_pricing_approval_obo_delete").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:block');
	});
}

function delete_cfs_location_appoving_data(id,idForwarder)
{
	var dataCounter=$("#dataCounter").attr('value');
	var dataTotalCount= $("#dataTotalCount").attr('value');	
	var idBatch=$("#idBatch").attr('value');
	var idService=$("#idService").attr('value');
	$("#delete_data_confirm_no").attr('style',"opacity:0.4");
	$("#delete_data_confirm_no").unbind("click");
				
	$("#delete_data_confirm").attr('style',"opacity:0.4");
	$("#delete_data_confirm").unbind("click");
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_cfs_location_approval_data',dataTotalCount:dataTotalCount,dataCounter:dataCounter,idForwarder:idForwarder,idService:idService,idBatch:idBatch},function(result){
		$("#show_pricing_approval_obo").html(result);
		//$("#loader").attr('style','display:none');
		$("#delete_confirm_popup").attr('style','display:none');
		window.scrollTo(0,0);
	});
}

function sel_line_completed_for_cost_awaiting_approval(div_id,id,idForwarder)
{
	var rowCount = $('#cost_pproved_by_management tr').length;
	//alert(div_id);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="approved_cost_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
	$("#review_top_data").unbind("click");
	$("#review_top_data").click(function(){review_uploaded_files_cfs_bulk_upolad_by_forwarder(id,idForwarder)});
	$("#review_top_data").attr('style',"opacity:1");	
	
	
	$("#approved_top_data").unbind("click");
	$("#approved_top_data").attr('style',"opacity:0.4");
	
	$("#download_top_data").unbind("click");
	//$("#download_top_data").click(function(){view_management_uploaded_files(id)});
	$("#download_top_data").attr('style',"opacity:0.4");
	
	$("#delete_top_data").unbind("click");
	$("#delete_top_data").click(function(){delete_approved_uploaded_files_data_cfs_bulk_upload(id,idForwarder)});
	$("#delete_top_data").attr('style',"opacity:1.0");
}


function review_uploaded_files_cfs_bulk_upolad_by_forwarder(id,idForwarder)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idForwarder:idForwarder,flag:'review_and_confrim_data_awaiting_approval'},function(result){
		$("#approve_data_review").html(result);
	});
}

function delete_uploaded_files(id)
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_uploaded_file'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block;");
		addPopupScrollClass('open_processing_popup');
	});
}

function confirm_delete_upload_file(id,datatable)
{
	$("#yes_button").unbind("click");
	$("#yes_button").attr('style',"opacity:0.4");
			
	$("#no_button").unbind("click");
	$("#no_button").attr('style',"opacity:0.4");
	
	if(datatable=='top')
	{
		var div_id="processing_cost_approved_list";
		var datatablevalue="topdata";
	}
	else if(datatable=='bottom')
	{
		var div_id="waiting_for_approved_list";
		var datatablevalue="bottomdata";
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_upload_file_by_forwarder',datatablevalue:datatablevalue},function(result){
		$("#"+div_id).html(result);
		$("#open_processing_popup").attr('style',"display:none");
	});
}


function select_country_approval(idForwarder,cflag,idCountry,iWarehouseType)
{
	$("#approve_button").attr("onclick","");
	$("#approve_button").unbind("click");
	$("#approve_button").attr('style',"opacity:0.4");
	if(cflag=='origin')
	{
		var showflag="city_warehouse_cc_origin";
		var divid="city_warehouse_origin";
		$("#idOriginWarehouse").attr("disabled",true);
		$("#szOriginCity").attr("disabled",true);
	}
	else if(cflag=='des')
	{
		var showflag="city_warehouse_cc_des";
		var divid="city_warehouse_des";
		$("#idDestinationWarehouse").attr("disabled",true);
		$("#szDestinationCity").attr("disabled",true);
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{idForwarder:idForwarder,idCountry:idCountry,flag:showflag,iWarehouseType:iWarehouseType},function(result){
		$("#"+divid).html(result);
		
		if(cflag=='origin')
		{
			$("#idOriginWarehouse").attr("disabled",false);
			$("#szOriginCity").attr("disabled",false);
		}
		else if(cflag=='des')
		{
			$("#idDestinationWarehouse").attr("disabled",false);
			$("#szDestinationCity").attr("disabled",false);
		}
	});
}


function select_city_approval(idForwarder,cflag,city,iWarehouseType)
{
	$("#approve_button").attr("onclick","");
	$("#approve_button").unbind("click");
	$("#approve_button").attr('style',"opacity:0.4");
	
	if(cflag=='origin')
	{
		var idCountry=$("#idOriginCountry").val();
		var showflag="warehouse_cc_origin";
		var divid="warehouse_origin";
		$("#idOriginWarehouse").attr("disabled",true);
		
	}
	else if(cflag=='des')
	{
		var idCountry=$("#idDestinationCountry").val();
		var showflag="warehouse_cc_des";
		var divid="warehouse_des";
		$("#idDestinationWarehouse").attr("disabled",true);
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{idForwarder:idForwarder,idCountry:idCountry,flag:showflag,szCity:city,iWarehouseType:iWarehouseType},function(result){
		$("#"+divid).html(result);
		if(cflag=='origin')
		{
			$("#idOriginWarehouse").attr("disabled",false);
		}
		else if(cflag=='des')
		{
			$("#idDestinationWarehouse").attr("disabled",false);
		}
	});
}

function showCountriesByCountryCFSBulkUpload(idCountry)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CFSBulkUploadUpdate.php",{mode:'SHOW_MULTIPLE_COUNTRIES_SELECT',idCountry:idCountry},function(result){
	$('#bottom-form').html(result);
	$('#CFSListing').attr('value',idCountry);	
		});
	mapLocationOnchangeCFSBulk(idCountry);
}


function sel_warehouse_for_approval(idForwarder,cflag,idWarehouse)
{
    //alert(cflag);
    if(cflag=='origin')
    {
        $("#idOriginWarehouse_hidden").attr('value',idWarehouse);
    }
    else if(cflag=='des')
    {
        $("#idDestinationWarehouse_hidden").attr('value',idWarehouse);
    } 
    var idOriginWarehouse=$("#idOriginWarehouse_hidden").val(); 
    var idDesWarehouse=$("#idDestinationWarehouse_hidden").val();
	
    if(parseInt(idOriginWarehouse)>0 && parseInt(idDesWarehouse)>0 && parseInt(idOriginWarehouse)!=parseInt(idDesWarehouse))
    {
        var idEdit=$("#idEdit").val();
        $("#approve_button").unbind("click");
        $("#approve_button").attr('style',"opacity:1");
        $("#approve_button").click(function(){approved_pricing_wtw_data(idEdit,idForwarder)});
    }
    else
    {
        $("#approve_button").unbind("click");
        $("#approve_button").attr('style',"opacity:0.4");
    } 
}

function select_country_approval_cc(idForwarder,cflag,idCountry)
{
	$("#approve_button").attr("onclick","");
	$("#approve_button").unbind("click");
	$("#approve_button").attr('style',"opacity:0.4");
	if(cflag=='origin')
	{
		var showflag="city_warehouse_origin";
		var divid="city_warehouse_origin";
		$("#idOriginWarehouse").attr("disabled",true);
		$("#szOriginCity").attr("disabled",true);
	}
	else if(cflag=='des')
	{
		var showflag="city_warehouse_des";
		var divid="city_warehouse_des";
		$("#idDestinationWarehouse").attr("disabled",true);
		$("#szDestinationCity").attr("disabled",true);
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{idForwarder:idForwarder,idCountry:idCountry,flag:showflag},function(result){
		$("#"+divid).html(result);
		
		if(cflag=='origin')
		{
			$("#idOriginWarehouse").attr("disabled",false);
			$("#szOriginCity").attr("disabled",false);
		}
		else if(cflag=='des')
		{
			$("#idDestinationWarehouse").attr("disabled",false);
			$("#szDestinationCity").attr("disabled",false);
		}
	});
}


function select_city_approval_cc(idForwarder,cflag,city)
{
	$("#approve_button").attr("onclick","");
	$("#approve_button").unbind("click");
	$("#approve_button").attr('style',"opacity:0.4");
	
	if(cflag=='origin')
	{
		var idCountry=$("#idOriginCountry").val();
		var showflag="warehouse_origin";
		var divid="warehouse_origin";
		$("#idOriginWarehouse").attr("disabled",true);
		
	}
	else if(cflag=='des')
	{
		var idCountry=$("#idDestinationCountry").val();
		var showflag="warehouse_des";
		var divid="warehouse_des";
		$("#idDestinationWarehouse").attr("disabled",true);
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{idForwarder:idForwarder,idCountry:idCountry,flag:showflag,szCity:city},function(result){
		$("#"+divid).html(result);
		if(cflag=='origin')
		{
			$("#idOriginWarehouse").attr("disabled",false);
		}
		else if(cflag=='des')
		{
			$("#idDestinationWarehouse").attr("disabled",false);
		}
	});
}


function sel_warehouse_for_approval_cc(idForwarder,cflag,idWarehouse)
{
	if(cflag=='origin')
	{
		$("#idOriginWarehouse_hidden").attr('value',idWarehouse);
	}
	else if(cflag=='des')
	{
		$("#idDestinationWarehouse_hidden").attr('value',idWarehouse);
	}
	
	var idOriginWarehouse=$("#idOriginWarehouse_hidden").val();
	
	var idDesWarehouse=$("#idDestinationWarehouse_hidden").val();
	
	if(parseInt(idOriginWarehouse)>0 && parseInt(idDesWarehouse)>0 && parseInt(idOriginWarehouse)!=parseInt(idDesWarehouse))
	{
		var idEdit=$("#id").val();
		$("#approve_button").unbind("click");
		$("#approve_button").attr('style',"opacity:1");
		$("#approve_button").click(function(){approved_pricing_cc_data(idEdit,idForwarder)});
	}
	else
	{
		$("#approve_button").unbind("click");
		$("#approve_button").attr('style',"opacity:0.4");
	}
	
}

function delete_approved_uploaded_files_data(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'delete_approved_data_popup'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
		addPopupScrollClass('open_processing_popup');
	});
}

function show_haulage_pricing_model_data(idService,idForwarder)
{
	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	var idWarehouse=$("#haulageWarehouse").attr('value');
	var iDirection =$("#idDirection").attr('value');
	var idHaulageModel=$("#haulageModelApproveData").attr('value');
	//alert(idHaulageModel);
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{idService:idService,idForwarder:idForwarder,idWarehouse:idWarehouse,iDirection:iDirection,idHaulageModel:idHaulageModel,flag:'review_haulage_pricing_model_data'},function(result){
		$("#haulage_list_view").html(result);
		$("#haulage_list_view").attr('style',"display:block");
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{idService:idService,idForwarder:idForwarder,idWarehouse:idWarehouse,iDirection:iDirection,idHaulageModel:idHaulageModel,flag:'haulage_pricing_model_data_try_it_out'},function(result){
		$("#haulage_temp_try_it_out").html(result);
	});	
		var disp = $("#loader").css('display');
		if(disp=='block')
		{
			$("#loader").attr("style","display:none");
		}
	});
}

function sel_haulage_model_zone_data_temp(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection,fWmFactor,idService)
{
	var rowCount = $('#view_haulage_model_zone tr').length;
	
	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	//alert(div_id);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_zone_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
		
		
		var showflag="pricing_detail_list";
		var newdiv_id="zone_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:fWmFactor,idService:idService},function(result){
		$("#approve_haulage_data_list").html(result);
		
		
		
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,flag:'show_ploygon_on_google_map'},function(result){
			$("#display_zone_map").html(result);
			});	
			
				$("#idHaulagePricingModel").attr('value',id);
				var disp = $("#loader").css('display');
				if(disp=='block')
				{
					$("#loader").attr("style","display:none");
				}
		});
}


function sel_haulage_model_postcode_data_temp(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection,fWmFactor,idService)
{

	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	var rowCount = $('#view_haulage_model_postcode tr').length;
	//alert(rowCount);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_postcode_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
				
	var showflag="pricing_detail_list";
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:fWmFactor,idService:idService},function(result){
	$("#approve_haulage_data_list").html(result);
	
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:'show_postcode_str'},function(result){
		$("#display_postcode_string").html(result);
		
		$("#idHaulagePricingModel").attr('value',id);
		var disp = $("#loader").css('display');
		if(disp=='block')
		{
			$("#loader").attr("style","display:none");
		}
		});	
		
	});
}

function sel_haulage_model_distance_data_temp(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection,idService)
{
	var rowCount = $('#view_haulage_model_distance tr').length;
	
	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	//alert(div_id);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_distance_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
				
			var showflag="pricing_detail_list";
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,idService:idService},function(result){
			$("#approve_haulage_data_list").html(result);
			
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:'show_distance_str'},function(result){
			$("#display_distance_string").html(result);
			$("#idHaulagePricingModel").attr('value',id);
			var disp = $("#loader").css('display');
			if(disp=='block')
			{
				$("#loader").attr("style","display:none");
			}
			});	
			
		});
}

function sel_haulage_model_city_data_temp(div_id,id,idHaulageModel,iPriorityLevel,idWarehouse,idDirection,fWmFactor,idService)
{
	var disp = $("#loader").css('display');
	if(disp!='block')
	{
		$("#loader").attr("style","display:block");
	}
	var rowCount = $('#view_haulage_model_city tr').length;
	//alert(div_id);
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="haulage_city_"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
	
		
		
		var showflag="pricing_detail_list";
		var newdiv_id="city_detail_list_"+idHaulageModel;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,fWmFactor:fWmFactor,idService:idService},function(result){
		$("#approve_haulage_data_list").html(result);	
			
			var disp = $("#loader").css('display');
			if(disp=='block')
			{
				$("#loader").attr("style","display:none");
			}
		});
}

function delete_uploaded_image(filename,tempfilename)
{
	//alert(filename);
	//alert(tempfilename);
	var allFilename=$("#file_name").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkUploadServices.php",{filename:filename,tempfilename:tempfilename,allFilename:allFilename,flag:'delete_uploaded_file'},function(result){
		$("#fileList").html(result);
		var filecount=$("#filecount").attr('value');
		if(parseInt(filecount)<='5')
		{
			//$("#fileuploadlink").attr('style','display:block;text-align: right;border: solid 1px #CCC;padding-top: 3px;padding-right: 3px;');
		}		
		if(parseInt(filecount)==0)
		{
			$("#nofile-selected").attr('style','display:block;');
			$('#fileList').append('<span id="nofile-selected">No file selected - click browse to select file for upload (max 5MB per file)</span>')	
		}
	});
}

function open_copy_haulage_city_popup(idHaulageModel,idWarehouse,idDirection)
{
	var  divid="city_detail_delete_"+idHaulageModel;
	var showflag="open_copy_city_popup";	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag,mode:'add'},function(result){
		$("#"+divid).html(result);
		addPopupScrollClass(divid);
		$("#"+divid).attr('style',"display:block");
	});
}

function copy_city()
{
	idCountry=$('#haulageCountryZone').val();
	szCity=$('#szCityZone').val();
	idWarehouse=$('#haulageWarehouseZone').val();
	idDirection=$('#idDirectionZone').val();
	var showflag='copy_city_data';
	var value=$("#copyPostCode").serialize();
	var newvalue=value+"&idCountry="+idCountry+"&szCity="+szCity+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&flag="+showflag;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
		$("#show_copy_data").html(result);
		$("#show_copy_data").attr("style","display:block");
		});
}

function delete_approved_uploaded_files_data_cfs_bulk_upload(id,idForwarder)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idForwarder:idForwarder,flag:'delete_approved_data_popup_cfs_bulk_upload'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
		addPopupScrollClass('open_processing_popup');
	});
}

function confirm_delete_upload_file_cfs_upload(id,datatable,idForwarder)
{
	$("#yes_button").unbind("click");
	$("#yes_button").attr('style',"opacity:0.4");
			
	$("#no_button").unbind("click");
	$("#no_button").attr('style',"opacity:0.4");
	
	if(datatable=='top')
	{
		var div_id="processing_cost_approved_list";
		var datatablevalue="topdata";
	}
	
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_bulkServicesApproval.php",{id:id,idForwarder:idForwarder,flag:'delete_cfs_bulk_upload_file_by_forwarder',datatablevalue:datatablevalue},function(result){
		$("#"+div_id).html(result);
		$("#open_processing_popup").attr('style',"display:none");
	});
}

function selectBillingSearchDetails_new(val)
{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_newBillingDetails.php",{mode:val},function(result){
		n = result.split("|||||");
		$('#date_change').html(n[0]);
		$("#content").html(n[1]);
	});
}

function change_currency_select_box(flag)
{
	if(flag=='origin')
	{
		var fOriginPrice=$("#fOriginPrice").attr('value');
		//alert(fOriginPrice);
		if(parseFloat(fOriginPrice)==='')
		{
			$("#idOriginCurrency").attr('value','');
		}
	}
	else if(flag=='des')
	{
		var fDestinationPrice=$("#fDestinationPrice").attr('value');
		if(parseFloat(fDestinationPrice)==='')
		{
			$("#idDestinationCurrency").attr('value','');
		}
		
	}
}
	function checkCfsDetails()
	{
		//alert('hello');
		var szCFSName  = $('#szCFSName').val();
		var szAddressLine1 = $('#szAddressLine1').val();
		var szOriginCity = $('#szOriginCity').val();
		var szCountry = $('#szCountry').val();
		var szPhoneNumber = $('#szPhoneNumber').val();
		var szLatitude = $('#szLatitude').val();
		var szLongitude =  $('#szLongitude').val();
		var idWarehouse = $('#idWarehouse').val(); 
		var szContactPerson = $('#szContactPerson').val();
		var szEmail = $('#szEmail').val();
		
		if(szCFSName !='' &&  szAddressLine1!='' && szOriginCity!='' && szCountry!='' && szPhoneNumber!='' && szLatitude!='' && szLongitude!='' && szContactPerson!='' && szEmail!='') 
		{
			$('#add_edit_warehouse_button').css({'opacity':'1'});
			if(idWarehouse != null)
			{
				$('#add_edit_warehouse_button').attr('onclick','encode_string("szPhoneNumber","szPhoneNumberUpdate");update_warehouse_data_obo();');
			}
			else
			{
				$('#add_edit_warehouse_button').attr('onclick','encode_string("szPhoneNumber","szPhoneNumberUpdate");submit_warehouse_data_obo();');
			}
		}
		else
		{
			$('#add_edit_warehouse_button').css({'opacity':'0.4'});
			$('#add_edit_warehouse_button').attr('onclick','');
		}
	}	
	function checkCCField(valueCC,idCCCurrency)
	{
		if(valueCC == '')
		{
			$('#'+idCCCurrency+' option[value=""]').prop('selected', true)
		}
	}
	
	function close_upload_service_popup()
	{
		$("#leave_page_div_upload_service").attr("style","display:none");
	}
	function checkForwarderDetails()
	{
            
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",$('#updateRegistComapnyForm').serialize()+'&'+$('#updateRegistComapnyForm2').serialize(),function(result){
			var n = result.split('|||||');
			var message = $.trim(n[0]);
			if(message == 'SUCCESS')
			{
				$('#content').html(n[1]);
				removePopUpFormFill(1);
				}
			else if(message == 'ERROR')
			{
				$('#Error').html(n[1]);
			}
		});
	}
	function submitForwarderContactInfo()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",$('#ajax_preferences_email').serialize(),function(result){
			var n = result.split('|||||');
			var message = $.trim(n[0]);
			if(message == 'SUCCESS')
			{
				$('#content').html(n[1]);
				$('#closePopUp').removeAttr('onClick');
				$('#closePopUp').unbind('click');
				removePopUpFormFill(2);
				$('#closePopUp').click(function(){cancel_remove_popup_info('showFillForm','2');});
			
			}
			else if(message == 'ERROR')
			{
				$('#Error').html(n[1]);
			}
		});
	}
	function finishCompletingData(check,notCheck)
	{
		var valu = $('#checkBox').is(":checked");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",{mode:'REMOVE_POP_UP'},function(result){
		if(valu == false)
		{
			window.location.href = notCheck;
		}
		else if(valu == true)
		{
			window.location.href = check;
		}	
		});
			
	}
	function checkTheEnteties(id1,id2)
	{
		var allFilled = true;
		$('#'+id1+' :input:not(:hidden), #'+id2+' :input:not(:hidden)').each(function (index, element)
    	{
    		if(element.id == '')
    		{
    			if (element.value === '') {
            	allFilled = false;
        		}
        	}
    	});
    	logo = $('#fileUpload').val();
    	if(logo == '')
    	{
    		allFilled = false;
    	}
    	
    	var countryvalue = $('#idCountry').val();
		if(countryvalue == '')
		{
			allFilled = false;
		}
		if(allFilled == true)
		{
			$('#submitPopUp1').css({'opacity':'1'});
			$('#submitPopUp1').unbind('click');
			$('#submitPopUp1').click(function(){checkForwarderDetails()});
		}
		if(allFilled == false)
		{
			$('#submitPopUp1').css({'opacity':'0.4'});
			$('#submitPopUp1').unbind('onClick');
		}
	}
	function ConfirmPreferencesData(id)
	{
		var allFilled = true;
		$('#'+id+' :input:not(:hidden)').each(function (index, element)
    	{
    		if(element.name == 'updatePrefEmailAry[szEmailBooking][1]' || element.name == 'updatePrefEmailAry[szEmailPayment][1]' || element.name == 'updatePrefEmailAry[szEmailCustomer][1]')
    		if (element.value === '') {
            allFilled = false;
        }
    	});

		if(allFilled == true)
		{
			$('#submitPopUp1').css({'opacity':'1'});
			$('#submitPopUp1').unbind('click');
			$('#submitPopUp1').click(function(){submitForwarderContactInfo()});
		}
		if(allFilled == false)
		{
			$('#submitPopUp1').css({'opacity':'0.4'});
			$('#submitPopUp1').unbind('onClick');
		}
	}
	function cancel_remove_popup_info(id,mode)
	{
		$('#'+id).html('');
		var url = __JS_ONLY_SITE_BASE__+"/companyIntroduction/";
		if(mode == '2')
		{
			var url = __JS_ONLY_SITE_BASE__+"/PricingServicingIntroduction/";
		}
		showIncompletePopupMessage(id,url);
		//removePopUpFormFill();
	}
	function removePopUpFormFill(mode)
	{
		if(mode == 3)
		{
		
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",{mode:'REMOVE_POP_UP'});
			
		}
		else
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",{mode:'CHANGE_STATUS',value:mode});
		}
		
	}
	function removePopUpIncompleteFormFill()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",{mode:'REMOVE_POP_UP'},function(result){	
		$('#showFillForm').html('');
		});
		
	}
	function showIncompletePopupMessage(id,url)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",{mode:'SHOW_CHECK_MESSAGE'},function(result){
		$('#'+id).html(result);	
		$('#checkForwarderListing').unbind('click');
		removePopUpFormFill(3);
		$('#checkForwarderListing').click(function(){checkForwarderFormFill('showFillForm',url);});
		});
	}
	
	function open_service_form_booking_screen(id)
	{
		$.get(__JS_ONLY_SITE_BASE__+"/service_popup_booked_by_customer.php",function(result){
			 $("#Transportation_pop").attr('style','display:block;');
			 $("#Transportation_pop").html(result);
			 addPopupScrollClass('Transportation_pop');
		});
	}
	function checkForwarderFormFill(id,url)
	{
		$('#showFillForm').html('');
		window.location.href = url;
	}
	function deleteLogo()
	{	
		var values = $('#fileUpload').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",{mode:'UNLINK_FILE',fileName:values},function(result){
		$('#upload_file_success').html('');
		$('#fileUpload').val('');
		$('#file_uploadQueue').html('');
		checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');
		//$('#file_upload').uploadifyCancel();
		$('#uploadme').css({'display':'block'});
		$('.uploadfilelist').css({'display':'none'});
		});
		
	}
	function goPreviousPage(value)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_popupConfirmation.php",{mode:'GO_PREVIOUS_PAGE',value:value},function(result){
		$('#content').html(result);
		});
	}
	
	function close_haulage_video_popup()
	{
		var iframe = window.parent.document.getElementById('google_map_target_video_popup');
		//alert(iframe);
		iframe.src='';
		removePopupScrollClass('show_haulage_pop_up');
		$('#show_haulage_pop_up').html('');
		
	}
	
	function update_haulage_video()
	{		
		var updateFlag = $('#checkBox').is(":checked");
		if(updateFlag===true)
			updateFlag=0;
		else
			updateFlag=1;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{flag:'update_haulage_video',updateFlag:updateFlag},function(result){
		$('#update_haulage_video_flag').html(result);
		$('#show_haulage_pop_up').html('');
		var iframe = document.getElementById('google_map_target_video_popup');
		//alert(iframe);
		iframe.src="#";
		removePopupScrollClass('show_haulage_pop_up');
		});
	}	
	
	
	function show_haulage_video_popup(iHaulageVideo)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{flag:'show_haulage_video',iHaulageVideo:iHaulageVideo},function(result){
		$('#ajaxLogin').html(result);
		addPopupScrollClass('show_haulage_pop_up');
		$('#show_haulage_pop_up').css({'display':'block'});
		
		});
	}
	
	function show_lclservice_video_popup(iLCLServiceVideo)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",{flag:'show_lclservice_video',iLCLServiceVideo:iLCLServiceVideo},function(result){
		$('#ajaxLogin').html(result);
		addPopupScrollClass('show_lclservice_pop_up');
		$('#show_lclservice_pop_up').css({'display':'block'});
		
		});
	}
	
	
	function close_lclservice_video_popup()
	{
		var iframe = window.parent.document.getElementById('google_map_target_video_popup');
		//alert(iframe);
		iframe.src='';
		removePopupScrollClass('show_lclservice_pop_up');
		$('#show_lclservice_pop_up').html('');
		
	}
	
	function update_lclservice_video()
	{		
		var updateFlag = $('#checkBox').is(":checked");
		if(updateFlag===true)
			updateFlag=0;
		else
			updateFlag=1;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportLCL.php",{flag:'update_lclservice_video',updateFlag:updateFlag},function(result){
		$('#update_lclservice_video_flag').html(result);
		$('#show_lclservice_pop_up').html('');
		var iframe = document.getElementById('google_map_target_video_popup');
		//alert(iframe);
		iframe.src="#";
		removePopupScrollClass('show_lclservice_pop_up');
		});
	}
        
function addForwarderPreferences()
{
	var idPreferences=$("#idPreferences").val();
	if(idPreferences==0)
	{
            $("#add_forwarder_preference_button").html('<span>Add</span>');
            
            $('#forwarder_preferences_table tr').attr('style','background:#fff;color:#000;cursor:pointer;');
            
             $("#delete_forwarder_preference_button").unbind("click");
             $("#delete_forwarder_preference_button").attr('style','opacity:0.4');
	}
        else
        {
            $("#delete_forwarder_preference_button").html('<span>Cancel</span>');
            $("#delete_forwarder_preference_button").unbind("click");
            $("#delete_forwarder_preference_button").removeAttr('style');
            var page_url=__JS_ONLY_SITE_BASE__+"/Company/Preferences/";
            $("#delete_forwarder_preference_button").click(function(){ redirect_url(page_url); });
        }
    $("#add_forwarder_preference_button").unbind("click");    
    $("#add_forwarder_preference_button").attr("onclick",'');
    //$('.addInsurateRatesFields').removeAttr('onFocus'); 
    $('#add_forwarder_preference_button').removeAttr('style');
    $('#add_forwarder_preference_button').attr('onclick','submitForwarderPreferences();'); 
} 

function submitForwarderPreferences()
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$("#add_forwarder_preferences_form").serialize(),function(result){ 
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#forwarder_preferences_main_form_container").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#forwarder_preference_list_container").html(result_ary[1]);	 
        }  
    });
}

function select_forwarder_preference_tr(div_id,id)
{
    $("#add_forwarder_preferences_form")[0].reset();
    var rowCount = $('#forwarder_preferences_table tr').length; 
    //alert("count: "+rowCount);
    if(rowCount>0)
    {
        for(var t=1;t<rowCount;++t)
        {
            var newdivid="forwarder_preferences_tr_"+t;

            if(newdivid!=div_id)
            {
                $("#"+newdivid).attr('style','background:#ffffff;cursor:pointer;');
            }
            else
            {
                $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
            }
        }
    }
    
    $('.addInsurateRatesFields').removeClass('red_border');
	
    $("#delete_forwarder_preference_button").unbind("click");
    $("#delete_forwarder_preference_button").removeAttr('style');
    $("#delete_forwarder_preference_button").click(function(){ forwarder_preferences_email_popup(id,'DELETE_FORWARDER_PREFERENCES_EMAIL'); });
    
    $("#add_forwarder_preference_button").attr("onclick",'');
    $("#add_forwarder_preference_button").html('<span>Edit</span>');
    $("#add_forwarder_preference_button").unbind("click");
    $("#add_forwarder_preference_button").removeAttr('style');
    $("#add_forwarder_preference_button").click(function(){ forwarder_preferences_email_popup(id,'DISPLAY_EDIT_PREFERENCES_FORM'); }); 
}

function forwarder_preferences_email_popup(id,mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{role_id:id,mode:mode},function(result){ 
        var result_ary = result.split("||||"); 
        
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);
           $("#contactPopup").attr('style','display:block;');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            if(mode=='DISPLAY_EDIT_PREFERENCES_FORM')
            {
                $("#forwarder_preferences_main_form_container").html(result_ary[1]); 
                
                $("#add_forwarder_preference_button").html('<span>Save</span>');
                $("#add_forwarder_preference_button").unbind("click");
                $("#add_forwarder_preference_button").removeAttr('style');
                $("#add_forwarder_preference_button").click(function(){ submitForwarderPreferences(); }); 
                
                $("#delete_forwarder_preference_button").html('<span>Cancel</span>');
                $("#delete_forwarder_preference_button").unbind("click");
    			$("#delete_forwarder_preference_button").removeAttr('style');
    			var page_url=__JS_ONLY_SITE_BASE__+"/Company/Preferences/";
    			$("#delete_forwarder_preference_button").click(function(){ redirect_url(page_url); });
    
            }
            else
            {
               $("#forwarder_preference_list_container").html(result_ary[1]); 
               $("#contactPopup").attr('style','display:none;');
            } 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        { 	
           $("#contactPopup").html(result_ary[1]);
           $("#contactPopup").attr('style','display:block;');	 
        }  
    });
} 
function display_pending_task_details_forwarder(div_id,idBookingFile,idFileOwner,closed_task_flag,iCourierBooking,idBooking)
{	
    var rowCount = $('#insuranced_booking_table tr').length;

    if(rowCount>0)
    {
        for(t=1;t<rowCount;++t)
        {
            newdivid="booking_data_"+t;

            if(newdivid!=div_id)
            {
                $("#"+newdivid).removeClass('selected-row');
            }
            else
            {
                $("#"+div_id).addClass('selected-row');
            }
        }   
        display_pending_task_overview_forwarder('DISPLAY_PENDING_TASK_DETAILS',idBookingFile,'',closed_task_flag,iCourierBooking,idBooking); 
    } 
} 
function display_pending_task_overview_forwarder(mode,quote_id,from_page,closed_task_flag,iCourierBooking,idBooking)
{ 
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,quote_id:quote_id,from_page:from_page,closed_task_flag:closed_task_flag,iCourierBooking:iCourierBooking},function(result){
	
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            $("#contactPopup").attr('style','display:none');
            $("#pending_task_overview_main_container").html(result_ary[1]);		
            $("#pending_task_overview_main_container").attr('style','display:block;');   
            if(iCourierBooking=='1' && closed_task_flag=='1')
            {
                // window.open(__JS_ONLY_SITE_BASE__+'/viewCourierLabel/'+idBooking+'/',"_blank");   
            }
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_CLOSED_QUOTE')
        {
            $("#pending_task_overview_main_container").html(result_ary[1]);		
            $("#pending_task_overview_main_container").attr('style','display:block;');  
            
           $("#contactPopup").html(result_ary[2]);		
           $("#contactPopup").attr('style','display:block');
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_NEW')
        {
           $("#pending_task_listing_container").html(result_ary[1]);		
           $("#pending_task_listing_container").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
} 
function add_more_booking_quote(mode)
{  
    var number = document.getElementById('hiddenPosition').value; 
    var table_id = document.getElementById('forwarder_quote_main_container');;
    number=parseInt(number); 
    var hidden1 = document.getElementById('hiddenPosition1').value; 
    document.getElementById('hiddenPosition1').value=parseInt(hidden1)+1;
    
    document.getElementById('hiddenPosition').value =  parseInt(number)+1; 
        
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{number:number,mode:mode},function(result){ 
        $("#forwarder_quote_main_container").append(result); 
        
        if(number>=2)
        {
            $("#add_more_booking_quote_pricing").attr('style','display:none;'); 
        }
        else
        {	
            $("#add_more_booking_quote_pricing").attr('style','display:inline-block;float:right;margin-top:-16px;'); 
        }
    }); 
    
    /*var newTdId = 'add_booking_quote_container_'+(number+1) ;
    if($("#"+newTdId).length)
    {
        console.log("td already exists");
    }
    else
    {
        var newTd=document.createElement('div'); 
        newTd.setAttribute('id', 'add_booking_quote_container_'+(number+1));
        //newTd.setAttribute('style', 'width:130px;disaply:none;');
        table_id.appendChild(newTd); 
    }*/
}
function remove_booking_quote(mode)
{  
    var number = document.getElementById('hiddenPosition').value;
    var hidden1= document.getElementById('hiddenPosition1').value;
	//alert("number"+number+"hidden1"+hidden1);
    if(parseInt(number)>1)
    {
        var id=parseInt(hidden1)-1;
        var div_id="add_booking_quote_container_"+id;
        document.getElementById('hiddenPosition').value=parseInt(number)-1;
        document.getElementById('hiddenPosition1').value=parseInt(hidden1)-1; 
        var hidden_value = parseInt(hidden1)-1;
       // alert(div_id);
        if(hidden_value<3)
        {
            if(hidden_value>1)
            {
                    $("#add_more_booking_quote_pricing").attr('style','display:inline-block;float:right;margin-top:-16px;');
            }
            else
            {
                    $("#add_more_booking_quote_pricing").attr('style','display:inline-block;float:right;margin-top:-16px;');
            } 
        }
        $("#"+mode).remove(); 
        //$("#"+div_id).attr("style",'display:none;');
        //$("#"+div_id).html(" ");  
        enableSendForwarderQuoteButton();
    }
    else
    {
           $("#forwarder_quote_price_form")[0].reset();
    }
} 

function submitForwarderQuotePricing()
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",$("#forwarder_quote_price_form").serialize(),function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#pending_task_overview_main_container").html(result_ary[1]);
           
            $("#send_forwarder_quote_submit_button").attr("style","opacity:0.4");
            $("#send_forwarder_quote_submit_button").unbind("click");
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#pending_task_listing_container").html(result_ary[1]);
           $("#pending_task_overview_main_container").html(result_ary[2]);
        }  
    });
}

function format_decimat(form_field,evt)
{ 
    var charcode=(evt.which)?(evt.which):(event.keyCode); 
    if (charcode > 31 && (charcode < 45 || charcode > 57) && charcode!=47)  // 47 means /
    {
        return false;
    }
    return true;
} 

function enableSendForwarderQuoteButton()
{  
    var counter = 1;
    var error_count = 1 ;
    var formId = 'forwarder_quote_price_form';
    var commentFlag=false;
    $(".send-forwarder-quote-class").each(function(element)
    {   
        if($("#fTotalPriceForwarderCurrency_"+counter).length)
        {
            if(isFormInputElementEmpty(formId,'fTotalPriceForwarderCurrency_'+counter)) 
            {		
                console.log('fTotalPriceForwarderCurrency');
                error_count++;
            } 
            else
            {
            	commentFlag=true;
            }
            
            if(isFormInputElementEmpty(formId,'idForwarderCurrency_'+counter)) 
            {		
                console.log('idForwarderCurrency');
                error_count++;
            }
            //Removed 02-Feb-2017 Financial Rev map
            /*if(isFormInputElementEmpty(formId,'fTotalVat_'+counter)) 
            {		
                console.log('fTotalVat');
                error_count++;
            }
            else
            {
            	commentFlag=true;
            }*/
            if(isFormInputElementEmpty(formId,'iTransitHours_'+counter)) 
            {		
                console.log('iTransitHours');
                error_count++;
            }
            else
            {
            	commentFlag=true;
            }
            if(isFormInputElementEmpty(formId,'dtQuoteValidTo_'+counter)) 
            {		
                console.log('dtQuoteValidTo');
                error_count++;
            } 
           
            if(!isFormInputElementEmpty(formId,'szForwarderComment_'+counter)) 
            {		
               commentFlag=true;
            } 
        }
        counter++;
    });
  
    if(error_count==1)
    {
        $("#send_forwarder_quote_submit_button").attr("style","opacity:1");
        $("#send_forwarder_quote_submit_button").unbind("click");
        $("#send_forwarder_quote_submit_button").click(function(){ submitForwarderQuotePricing(); }); 
        
        
        $("#send_forwarder_quote_save_button").attr("style","opacity:1");
        $("#send_forwarder_quote_save_button").unbind("click");
        $("#send_forwarder_quote_save_button").click(function(){ saveForwarderQuotePricing(); });
    }
    else
    {
        $("#send_forwarder_quote_submit_button").attr("style","opacity:0.4");
        $("#send_forwarder_quote_submit_button").unbind("click");        
        
        $("#send_forwarder_quote_save_button").attr("style","opacity:0.4");
        $("#send_forwarder_quote_save_button").unbind("click");
    }
    if(!commentFlag)
    {
    	 $("#send_forwarder_quote_save_button").attr("style","opacity:0.4");
        $("#send_forwarder_quote_save_button").unbind("click");
        
        
    }	
    else
    {
    	$("#send_forwarder_quote_save_button").attr("style","opacity:1");
        $("#send_forwarder_quote_save_button").unbind("click");
        $("#send_forwarder_quote_save_button").click(function(){ saveForwarderQuotePricing(); });
    } 
}

function openProviderAgreementForm()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'ADD_COURIER_PROVIDER_AGREEMENT'},function(result){
		$('#contactPopup').html(result);
		addPopupScrollClass('contactPopup');
		$('#contactPopup').css({'display':'block'});
		});
}

function activateCheckLoginButton(addText,checkLogintext)
{
    var error_count=1;
    if(isFormInputElementEmpty("addCourierAgreementForm",'idCourierProvider')) 
    {		 
        error_count++;
    }
    else
    {
    	$("#idCourierProvider").removeClass('red_border');
    } 
    
    var idCourierProvider=$("#idCourierProvider").val();  
    if(idCourierProvider==1)
    {
        $("#user_name").html('Authentication Key');
        $("#acc_number").html('Account Number'); 
        $("#szMeterTr").html('Meter Number');
    	//$("#szMeterTr").attr('style','display:"";');
        $("#meter_td_Container").attr('style','display:"";');
        if(isFormInputElementEmpty("addCourierAgreementForm",'szMeterNumber')) 
        {		 
            error_count++;
        }
        else
        {
            $("#szMeterNumber").removeClass('red_border');
        }
    }
    else if(idCourierProvider==2)
    {
        $("#user_name").html('User Name');  
        $("#acc_number").html('Access Key'); 
        $("#szMeterTr").html('Account Number');
        $("#meter_td_Container").attr('style','display:"";');
    }    
    else
    {
        $("#user_name").html('User Name');  
        $("#acc_number").html('Account Number'); 
        $("#szMeterTr").html('Meter Number');
        $("#meter_td_Container").attr('style','display:none;');
    }
    
    if(isFormInputElementEmpty("addCourierAgreementForm",'szAccountNumber')) 
    {		
        //$("#szAccountNumber").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#szAccountNumber").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty("addCourierAgreementForm",'szUsername')) 
    {		
       // $("#szUsername").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#szUsername").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty("addCourierAgreementForm",'szPassword')) 
    {		
       // $("#szPassword").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#szPassword").removeClass('red_border');
    }
    
    if(error_count==1)
    {
        $("#check_login_button").attr("style","opacity:1");
        $("#check_login_button").html('<span id="loaderClass"><span>'+checkLogintext+'</span></span>');
        $("#check_login_button").unbind("click");
        $("#check_login_button").click(function(){ checkLoginValidation(addText); }); 
    }
    else
    {
        $("#check_login_button").attr("style","opacity:0.4");
        $("#check_login_button").html('<span id="loaderClass" ><span>'+checkLogintext+'</span></span>');
        $("#check_login_button").unbind("click");
    } 
}

function check_form_field_empty_standard_courier(formId,inputId,container_id,display_bottom)
{	
    if(isFormInputElementEmpty(formId,inputId)) 
    {		
       $("#"+container_id).addClass('red_border');
       return false;
    }
    else
    {
    	$("#"+container_id).removeClass('red_border');
    }
}

function check_form_field_not_required_courier(formId,inputId)
{
    $("#"+inputId).removeClass('red_border');
   // $("#"+inputId,"#"+formId).validationEngine('hide'); 
}

function checkLoginValidation(addText)
{
    $("#loaderClass span").addClass("tyni-loader");
    $("#check_login_button").attr("style","opacity:0.4");
    var value=$("#addCourierAgreementForm").serialize();
    var newValue=value+"&mode=CHECK_COURIER_AGREEMENT_DATA";
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",newValue,function(result){
        var result_ary = result.split("||||"); 
        $('#contactPopup').html(result_ary[1]);
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
            $("#check_login_button").attr("style","opacity:1");
        }
        else
        {
            $("#check_login_button").attr("style","opacity:1");
            $("#check_login_button").html('<span>'+addText+'</span>');
            $("#check_login_button").unbind("click");
            $("#check_login_button").click(function(){ addCourierAgreementData(addText); }); 
        } 
        //$("#loaderClass span").removeClass("tyni-loader");
    });
}

function addCourierAgreementData(addText)
{
    $("#check_login_button").attr("style","opacity:0.4");
    $("#check_login_button").unbind("click");
    var value=$("#addCourierAgreementForm").serialize();
    var newValue=value+"&mode=ADD_COURIER_AGREEMENT_DATA";
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",newValue,function(result){
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {  
            $('#provider_agreement').html(result_ary[1]);
            $('#provider_agreement_service').html(result_ary[2]);
            $('#provider_agreement_trades').html(result_ary[3]);
            $('#provider_agreement_price').html(result_ary[4]);
            $('#contactPopup').css({'display':'none'});
			
        }
        else
        {
            $('#contactPopup').html(result_ary[1]);
            $('#contactPopup').css({'display':'block'});
            addPopupScrollClass('contactPopup');

            $("#check_login_button").attr("style","opacity:1");
             $("#check_login_button").html('<span>'+addText+'</span>');
            $("#check_login_button").unbind("click");
            $("#check_login_button").click(function(){ addCourierAgreementData(addText); }); 
        } 	
    });
}

function activateAgreementServiceCovered()
{
    var error_count=1;
    if(isFormInputElementEmpty("addAgreementServiceCoverForm",'idProviderProduct')) 
    {		
        //$("#idProviderProduct").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#idProviderProduct").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty("addAgreementServiceCoverForm",'szDisplayName')) 
    {		
        //$("#szDisplayName").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#szDisplayName").removeClass('red_border');
    }
    var idCourierProvider=$("#addAgreementServiceCoverForm #idCourierAgreeProvider").val();
    //alert(idCourierProvider);
    if(idCourierProvider=='') 
    {		
        //$("#idProviderProduct").addClass('red_border');
        error_count++;
    }
    
    var idValue=$("#addAgreementServiceCoverForm #id").val();
    
    if(parseInt(idValue)>0)
    {
         $("#add_agree_service_covered").html('<span style="min-width:50px;">Save</span>');
        if(error_count==1)
        {
            $("#add_agree_service_covered").attr("style","opacity:1");
            $("#add_agree_service_covered").unbind("click");
            $("#add_agree_service_covered").click(function(){ addEditAgreementServiceData(); }); 
        }
        else
        {
            $("#add_agree_service_covered").attr("style","opacity:0.4");
            $("#add_agree_service_covered").unbind("click");
        }    
    }
    else
    {    
         $("#add_agree_service_covered").html('<span style="min-width:50px;">Add</span>');
        
          $(':input','#addAgreementPricingForm')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
        if(error_count==1)
        {
            $("#add_agree_service_covered").attr("style","opacity:1");
            $("#add_agree_service_covered").unbind("click");
            $("#add_agree_service_covered").click(function(){ addEditAgreementServiceData(); }); 
        }
        else
        {
            $("#add_agree_service_covered").attr("style","opacity:0.4");
            $("#add_agree_service_covered").unbind("click");
        }
        $("#detete_agree_service_covered").attr("style","opacity:0.4");
        $("#detete_agree_service_covered").unbind("click");
        $("#provider_agreement_service .selected-row").removeClass('selected-row');
    }
}

function addEditAgreementServiceData()
{
	var value = $('#addAgreementServiceCoverForm').serialize();
	var newValue=value+'&mode=ADD_EDIT_COURIER_SERVICE_COVERED_DATA';
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",newValue,function(result){
	var result_ary = result.split("||||"); 
		if($.trim(result_ary[0])=='SUCCESS')
		{
			$("#provider_agreement_service").html(result_ary[1]);
                        var idCourierAgreement=$("#idCourierAgreeProvider").val();
                 var divId="update_status_"+idCourierAgreement;
                 //alert(divId);
                 $("#"+divId).html(result_ary[2]);
		}
		else
		{
			$("#addEditServiceCoverdDiv").html(result_ary[1]);
		}
		$("#loader").attr('style','display:none');
		
	});
}

function selectAgreeCoveredData(idCourierProvider,id,idCourierAgreement,editText,saveText,cancelText)
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'SELECT_AGREE_SERVICE_COVERED',idCourierProvider:idCourierProvider,id:id,idCourierAgreement:idCourierAgreement},function(result){
		var result_ary = result.split("||||"); 
            $('#provider_agreement_service').html(result_ary[0]);
             $('#provider_agreement_price').html(result_ary[1]);
		$("#add_agree_service_covered").html('<span style="min-width:50px;">'+editText+'</span>');
        $("#add_agree_service_covered").unbind("click");
        $("#add_agree_service_covered").click(function(){editAgreeServiceCoveredData(idCourierProvider,id,idCourierAgreement,saveText,cancelText,editText);});
        $("#add_agree_service_covered").attr("style",'opacity:1;');
        
        
        $("#detete_agree_service_covered").unbind("click");
        $("#detete_agree_service_covered").click(function(){deleteAgreeServiceCoveredData(idCourierProvider,id,idCourierAgreement);});
        $("#detete_agree_service_covered").attr("style",'opacity:1;');
        
		$("#loader").attr('style','display:none');
		
		});
}

function editAgreeServiceCoveredData(idCourierProvider,id,idCourierAgreement,saveText,cancelText,editText)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'EDIT_AGREE_SERVICE_COVERED',idCourierProvider:idCourierProvider,id:id,idCourierAgreement:idCourierAgreement},function(result){
        $('#addEditServiceCoverdDiv').html(result);

        $("#add_agree_service_covered").html('<span style="min-width:50px;">'+saveText+'</span>');
        $("#add_agree_service_covered").unbind("click");
        $("#add_agree_service_covered").click(function(){addEditAgreementServiceData();});
        $("#add_agree_service_covered").attr("style",'opacity:1;');
        
        $("#detete_agree_service_covered").html('<span style="min-width:50px;">'+cancelText+'</span>');
        $("#detete_agree_service_covered").unbind("click");
        $("#detete_agree_service_covered").click(function(){selectAgreeCoveredData(idCourierProvider,id,idCourierAgreement,editText,saveText,cancelText);});
        $("#detete_agree_service_covered").attr("style",'opacity:1;'); 
        $("#loader").attr('style','display:none');
		
    });
}

function deleteAgreeServiceCoveredData(idCourierProvider,id,idCourierAgreement)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'DELETE_AGREE_SERVICE_COVERED',idCourierProvider:idCourierProvider,id:id,idCourierAgreement:idCourierAgreement},function(result){
        
        var result_ary = result.split("||||");  
        $('#provider_agreement_service').html(result_ary[0]);
        var idCourierAgreement=$("#idCourierAgreeProvider").val();
        $('#provider_agreement_price').html(result_ary[2]);

        var divId="update_status_"+idCourierAgreement;
        $("#"+divId).html(result_ary[1]);
        $("#loader").attr('style','display:none'); 
    });
}

function activateAgreementTradeOffered()
{
    var error_count=1;
    if(isFormInputElementEmpty("addAgreementTradesOfferForm",'idCountryFrom')) 
    {		
        //$("#idCountryFrom").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#idCountryFrom").removeClass('red_border');
    } 
    if(isFormInputElementEmpty("addAgreementTradesOfferForm",'iTrade')) 
    {		
        //$("#iTrade").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#iTrade").removeClass('red_border');
    }
    
    var idCourierAgreeProvider=$("#addAgreementTradesOfferForm #idCourierAgreeProvider").val();
    //alert(idCourierAgreeProvider);
    if(idCourierAgreeProvider=='') 
    {	
        //alert('hi');
        //$("#idProviderProduct").addClass('red_border');
        error_count++;
    }
    var idValue=$("#addAgreementTradesOfferForm #id").val();
     //alert(error_count);
   if(parseInt(idValue)>0)
   {
       $("#add_agree_trade_offered").html('<span style="min-width:50px;">Save</span>');
       if(error_count==1)
        {
            var value=$("#addAgreementTradesOfferForm").serialize();
            var newValue=value+"&mode=CHECKTRADE_ALREADY_EXISTS";
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",newValue,function(result){
            var result_ary = result.split("||||"); 
            if(result_ary[0]=='SUCCESS')
            {
                $("#add_agree_trade_offered").attr("style","opacity:1");
                $("#add_agree_trade_offered").unbind("click");
                $("#add_agree_trade_offered").click(function(){ addEditAgreementTradeOfferedData(); }); 
            }
            else
            {
               $("#add_agree_trade_offered").attr("style","opacity:0.4");
                $("#add_agree_trade_offered").unbind("click");     
            }
            });
        }
        else
        {
            $("#add_agree_trade_offered").attr("style","opacity:0.4");
            $("#add_agree_trade_offered").unbind("click");
        }
   }
   else
   {    
        $("#add_agree_trade_offered").html('<span style="min-width:50px;">Add</span>');
        if(error_count==1)
        {
             var value=$("#addAgreementTradesOfferForm").serialize();
            var newValue=value+"&mode=CHECKTRADE_ALREADY_EXISTS";
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",newValue,function(result){
                var result_ary = result.split("||||"); 
                if(result_ary[0]=='SUCCESS')
                {
                    $("#add_agree_trade_offered").attr("style","opacity:1");
                    $("#add_agree_trade_offered").unbind("click");
                    $("#add_agree_trade_offered").click(function(){ addEditAgreementTradeOfferedData(); }); 

                }
                else
                {
                   $("#add_agree_trade_offered").attr("style","opacity:0.4");
                    $("#add_agree_trade_offered").unbind("click");     
                }
            });
        }
        else
        {
            $("#add_agree_trade_offered").attr("style","opacity:0.4");
            $("#add_agree_trade_offered").unbind("click");
        } 
        
        $("#detete_agree_trade_offered").attr("style","opacity:0.4");
        $("#detete_agree_trade_offered").unbind("click");
        $("#detete_agree_trade_offered .selected-row").removeClass('selected-row');
        $("#provider_agreement_trades .selected-row").removeClass('selected-row');
   }
}

function addEditAgreementTradeOfferedData()
{
	var value = $('#addAgreementTradesOfferForm').serialize();
	var newValue=value+'&mode=ADD_EDIT_COURIER_TRADES_OFFERED_DATA';
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",newValue,function(result){
	var result_ary = result.split("||||"); 
		if($.trim(result_ary[0])=='SUCCESS')
		{
                        var idCourierAgreeProvider=$("#idCourierAgreeProvider").val();
                        var divId="update_status_"+idCourierAgreeProvider;
			$("#provider_agreement_trades").html(result_ary[1]);
                        $("#"+divId).html(result_ary[2]);
		}
		else
		{
			$("#addEditTradesOfferDiv").html(result_ary[1]);
		}
		$("#loader").attr('style','display:none');
		
	});
}

function selectAgreeTradeData(id,idCourierAgreement,editText,saveText,cancelText)
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'SELECT_AGREE_TRADE_OFFERED',id:id,idCourierAgreement:idCourierAgreement},function(result){
		$('#provider_agreement_trades').html(result);
		
		$("#add_agree_trade_offered").html('<span style="min-width:50px;">'+editText+'</span>');
        $("#add_agree_trade_offered").unbind("click");
        $("#add_agree_trade_offered").click(function(){editAgreeTradeOfferedData(id,idCourierAgreement,saveText,cancelText,editText);});
        $("#add_agree_trade_offered").attr("style",'opacity:1;');
        
        
        $("#detete_agree_trade_offered").unbind("click");
        $("#detete_agree_trade_offered").click(function(){deleteAgreeTradeOfferedData(id,idCourierAgreement);});
        $("#detete_agree_trade_offered").attr("style",'opacity:1;');
        
		$("#loader").attr('style','display:none');
		
		});
}

function editAgreeTradeOfferedData(id,idCourierAgreement,saveText,cancelText,editText)
{
		$("#loader").attr('style','display:block');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'EDIT_AGREE_TRADE_OFFERED',id:id,idCourierAgreement:idCourierAgreement},function(result){
		$('#addEditTradesOfferDiv').html(result);
		
		$("#add_agree_trade_offered").html('<span style="min-width:50px;">'+saveText+'</span>');
        $("#add_agree_trade_offered").unbind("click");
        $("#add_agree_trade_offered").click(function(){addEditAgreementTradeOfferedData();});
        $("#add_agree_trade_offered").attr("style",'opacity:1;');
        
        $("#detete_agree_trade_offered").html('<span style="min-width:50px;">'+cancelText+'</span>');
        $("#detete_agree_trade_offered").unbind("click");
        $("#detete_agree_trade_offered").click(function(){selectAgreeTradeData(id,idCourierAgreement,editText,saveText,cancelText);});
        $("#detete_agree_trade_offered").attr("style",'opacity:1;');
        
		$("#loader").attr('style','display:none');
		
		});
}

function deleteAgreeTradeOfferedData(id,idCourierAgreement)
{
		$("#loader").attr('style','display:block');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'DELETE_TRADE_OFFERED',id:id,idCourierAgreement:idCourierAgreement},function(result){
		var result_ary = result.split("||||"); 
                    $('#provider_agreement_trades').html(result_ary[0]);
                     var idCourierAgreeProvider=$("#idCourierAgreeProvider").val();
		 var divId="update_status_"+idCourierAgreeProvider;
                 $("#"+divId).html(result_ary[1]);
		
		$("#loader").attr('style','display:none');
		
		});
}

function activateAgreementPricing()
{
	var error_count=1;
	if(isFormInputElementEmpty("addAgreementPricingForm",'iBookingIncluded')) 
    {		
        $("#iBookingIncluded").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#iBookingIncluded").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty("addAgreementPricingForm",'fMarkupPercent')) 
    {		
        $("#fMarkupPercent").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#fMarkupPercent").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty("addAgreementPricingForm",'fMinimumMarkup')) 
    {		
        $("#fMinimumMarkup").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#fMinimumMarkup").removeClass('red_border');
    }
    
    if(isFormInputElementEmpty("addAgreementPricingForm",'fMarkupperShipment')) 
    {		
        $("#fMarkupperShipment").addClass('red_border');
        error_count++;
    }
    else
    {
    	$("#fMarkupperShipment").removeClass('red_border');
    }
    
     var idCourierProvider=$("#addAgreementPricingForm #idCourierServiceOffered").val();
    //alert(idCourierProvider);
    if(idCourierProvider=='') 
    {		
        //$("#idProviderProduct").addClass('red_border');
        error_count++;
    }
    
    if(error_count==1)
    {
        $("#add_agree_pricing").attr("style","opacity:1");
        $("#add_agree_pricing").unbind("click");
        $("#add_agree_pricing").click(function(){ saveAgreementPricingData(); }); 
    }
    else
    {
        $("#add_agree_pricing").attr("style","opacity:0.4");
        $("#add_agree_pricing").unbind("click");
    } 
}

function saveAgreementPricingData()
{
	var value = $('#addAgreementPricingForm').serialize();
	var newValue=value+'&mode=SHOW_COURIER_AGREE_PRICING_DATA';
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",newValue,function(result){
	var result_ary = result.split("||||"); 
       //alert(result_ary);
		if($.trim(result_ary[0])=='SUCCESS')
		{
			$('#provider_agreement_price').html(result_ary[1]);
			 var idCourierAgreeProvider=$("#idCourierAgreeProvider").val();
		 var divId="update_status_"+idCourierAgreeProvider;
                // alert(divId);
                 $("#"+divId).html(result_ary[2]);
			
		
		}
		else
		{
			$("#provider_agreement_price").html(result_ary[1]);
		}
		$("#loader").attr('style','display:none');
		
	});
}

function selectPricingServiceTradingData(id)
{
   // $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'SHOW_AGREEMENT_SERVICE_TRADES_PRICING',id:id},function(result){
        var result_ary = result.split("||||"); 
        $('#provider_agreement').html(result_ary[0]);
        $('#provider_agreement_service').html(result_ary[1]);
        $('#provider_agreement_trades').html(result_ary[2]);
        $('#provider_agreement_price').html(result_ary[3]);

        $("#loader").attr('style','display:none');
        if(parseInt(id)>0)
        {
            $("#add_edit_courier_button").attr("onclick",'');
           $("#add_edit_courier_button").unbind("click");
           $("#add_edit_courier_button").html("<span style='min-width:50px;'>Edit</span>");
           $("#add_edit_courier_button").click(function(){editCourierAgreementData(id);});
           $("#add_edit_courier_button").attr("style",'opacity:1;');


           $("#detete_provider_agreement_data").unbind("click");
           $("#detete_provider_agreement_data").click(function(){deleteCourierAgreeData(id);});
           $("#detete_provider_agreement_data").attr("style",'opacity:1;');
        }  
    });
}

function deleteCourierAgreeData(id)
{
	$("#loader").attr('style','display:block');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'DELETE_AGREEMENT_SERVICE_TRADES_PRICING',id:id},function(result){
		var result_ary = result.split("||||"); 
		$('#provider_agreement').html(result_ary[0]);
		$('#provider_agreement_service').html(result_ary[1]);
		$('#provider_agreement_trades').html(result_ary[2]);
		$('#provider_agreement_price').html(result_ary[3]);
		
		$("#loader").attr('style','display:none');
		
	
        $("#detete_provider_agreement_data").unbind("click");
        $("#detete_provider_agreement_data").click(function(){deleteCourierAgreeData(id);});
        $("#detete_provider_agreement_data").attr("style",'opacity:1;');
		
		});
}

function showPagePendingQuotes(page)
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'SHOW_PENDING_QUOTE_PAGINATION',page:page},function(result){
		$('#pending_quote_list').html(result);
	$("#loader").attr('style','display:none');
	});
	
}

function showPagePastQuotes(page)
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'SHOW_PAST_QUOTE_PAGINATION',page:page},function(result){
		$('#past_quote_list').html(result);
	$("#loader").attr('style','display:none');
	});
	
}

function saveForwarderQuotePricing()
{ 
	var value=$("#forwarder_quote_price_form").serialize();
	var newValue=value+'&saveFlag=1';
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#pending_task_overview_main_container").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           //$("#pending_task_listing_container").html(result_ary[1]);
           //$("#pending_task_overview_main_container").html(result_ary[2]);
           
           $("#send_forwarder_quote_save_button").attr("style","opacity:0.4");
           $("#send_forwarder_quote_save_button").unbind("click");
        }  
    });
}

function showPendingTask()
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'SHOW_PENDING_QUOTE_DATA'},function(result){
		$('#hsbody').html(result);
	$("#loader").attr('style','display:none');
	});
}

function showPastTask()
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'SHOW_PAST_QUOTE_DATA'},function(result){
		$('#hsbody').html(result);
	$("#loader").attr('style','display:none');
	});
}

function editCourierAgreementData(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'EDIT_COURIER_PROVIDER_AGREEMENT',id:id},function(result){
		$('#contactPopup').html(result);
		addPopupScrollClass('contactPopup');
		$('#contactPopup').css({'display':'block'});
		});
}

function showProviderDropDown(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'SHOW_COURIER_PROVIDER_DROPDOWN',id:id},function(result){
		$('#show_provider_dropdown').html(result);
		
		});
}

function delete_uploaded_courier_label(filename,tempfilename)
{
    //alert(filename);
    //alert(tempfilename);
    var allFilename=$("#file_name").attr('value');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{filename:filename,tempfilename:tempfilename,allFilename:allFilename,flag:'delete_uploaded_file'},function(result){
            $("#fileList").html(result);
            var filecount=$("#filecount").attr('value');
            //var newFileCount=filecount-1;
            var textpage='page';
            if(parseInt(filecount)>1)
            {
                textpage='pages';
            }
            textpage=filecount+" "+textpage; 
            $("#iTotalPage").attr('value',textpage);
            //$("#filecount").attr('value',newFileCount);
				
    });
}

function enableDisableDateTimeField(szValue)
{
    if(szValue=='Scheduled')
    {
        $("#dtCollectionStartTime").attr('disabled',false);
        $("#dtCollectionEndTime").attr('disabled',false);
        $("#dtCollection").removeAttr('disabled');
        var dtCollection = $("#dtCollection").val(); 
        if(dtCollection!="N/A" && isValidDate(dtCollection))
        {
            //@ TO DO 
        }
        else
        {
            var dtTodayDateTime = new Date();
            var day = dtTodayDateTime.getDate();
            var month = dtTodayDateTime.getMonth()+1;
            var year = dtTodayDateTime.getFullYear();
            
            if(day<10)
            {
                day = '0'+day;
            } 
            if(month<10)
            {
                month = '0'+ month;
            } 
            var dtCollectionDate = day+'/'+month+'/'+year; 
            $("#dtCollection").val(dtCollectionDate);
        } 
    }
    else
    {
        $("#dtCollectionStartTime").attr('disabled',true);
        $("#dtCollectionEndTime").attr('disabled',true);
        $("#dtCollection").attr('disabled',true);   
    }
}

function changeEndTimeValue(szValue,timeArr)
{
    var str='';  
    timeArr=timeArr.split(";");
    var lengthArr=timeArr.length;
   
    if(szValue<lengthArr)
    {   
        var t=0;
        for(var i=szValue;i<lengthArr;++i)
        {
            
            var optionStr='';
            if(szValue!='' || t>0)
            {
                optionStr="<option value='"+i+"'>"+timeArr[i]+"</option>";
                str =str+""+optionStr;
            }
            else
            {
                 optionStr="<option value='"+i+"'></option>";
                 str =str+""+optionStr;    
            }
            ++t;
        }
     }
    
     $("#dtCollectionEndTime").html(str);
}

function openPdfLightBox(url)
{ 
   $("#loader").attr('style','display:block'); 
   var iframe = window.parent.document.getElementById('showPdf');
   iframe.src=url;
   $("#pdfDiv").attr('style','display:block');
   $("#loader").attr('style','display:none');
}

function closePdfDiv()
{
    var iframe = window.parent.document.getElementById('showPdf');
   iframe.src='';
    $("#pdfDiv").attr('style','display:none'); 
}

function jquery_in_array(jsAry,szPattern)
{
    var iFoundPattern = false;
    for(var i=0;i<jsAry.length;i++)
    {
        //console.log("Js: "+jsAry[i]+" Pattern: "+szPattern);
        if(jsAry[i]!='' && jsAry[i]==szPattern)
        {
            iFoundPattern = 1;
            break;
        }
    }
    return iFoundPattern;
}

function enableSendForwarderCourierLabelButton()
{
    var counter = 1;
    var error_count = 1 ;
    var error_count_create_label = 1 ;
    var formId = 'forwarder_courier_label_form';
    var commentFlag=false;  
    $("#szMasterTrackingNumber").removeClass('red_border');   
    if(isFormInputElementEmpty(formId,'szMasterTrackingNumber')) 
    {		 
        error_count++; //s$("#szMasterTrackingNumber").addClass('red_border');
        //console.log("szMasterTrackingNumber");
    }
    else
    {
        var szMasterTrackingNumber=$("#szMasterTrackingNumber").val();
        if(!szMasterTrackingNumber.match("^[a-zA-Z0-9]*$"))
        {
            error_count++;
            $("#szMasterTrackingNumber").addClass('red_border');
            console.log("szMasterTrackingNumber2");
        }
        else
        {
            $("#szMasterTrackingNumber").removeClass('red_border');   
            commentFlag=true;
            //console.log("szMasterTrackingNumber3");
        }    
    }
     
    var iManualCourierBookingVaule = $("#iManualCourierBooking").val();

    if(iManualCourierBookingVaule==1)
    {
        if(isFormInputElementEmpty(formId,'idCourierProduct'))
        {
            error_count++;
            error_count_create_label++;
            //console.log("idCourierProduct");
        }
        else if($("#szTntServicesPipeLine").length)
        {
            var szTntServicesPipeLine = $("#szTntServicesPipeLine").val();
            var idCourierProduct = $("#idCourierProduct").val();

            if(szTntServicesPipeLine!='')
            {
                var tntServiceLineAry = szTntServicesPipeLine.split(";"); 
                if(jquery_in_array(tntServiceLineAry,idCourierProduct))
                {
                    //console.log("In array..."+idCourierProduct);
                }
                else
                {
                    error_count_create_label++;
                }
            }
            else
            {
                //If TNT pipe line is empty then also we don't allows to create automatic label as in that case don't know the eleigible products for creating labels @Ajay
                error_count_create_label++; 
            }
        } 
    }  
    else
    {
        /*
        * For automatic courier booking
        */
        var idCourierProviderCompany = $("#idCourierProviderCompany").val();
        if(idCourierProviderCompany==3)
        {
            /*
             * We only allowed to creale automatic label for 3. TNT
             */
            error_count_create_label = 1;
        }
        else
        {
            error_count_create_label = 2;
        } 
    }
    
    if(error_count==1)
    {
        var filecount=$("#filecount").val(); 
        if(parseInt(filecount)==0)
        {
            $("#iTotalPage").addClass('red_border');
             //console.log('iTotalPage');
             error_count++;
        }
        else
        {
           $("#iTotalPage").removeClass('red_border');   
        }
    }
    
    if(isFormInputElementEmpty(formId,'iCollection')) 
    {		 
        error_count++;
        error_count_create_label++;
        //console.log("iCollection");
    }
    else
    {
        var iCollection=$("#iCollection").val();
        //console.log("coll: "+iCollection);
        if(iCollection=='Scheduled')
        {
            if(isFormInputElementEmpty(formId,'dtCollection'))
            {		 
                error_count++;
                error_count_create_label++;
                //console.log("dtCollection");
            }
            else
            {
                var dtCollection = $("#dtCollection").val();
                if(isValidDate(dtCollection))
                {
                    //@TO DO 
                    //Date is valid
                }
                else
                {
                    error_count++;
                    //console.log("dtCollection2");
                    error_count_create_label++;
                }
            }
            
            if(isFormInputElementEmpty(formId,'dtCollectionStartTime')) 
            {		 
                error_count++;
                error_count_create_label++;
                //console.log("dtCollectionStartTime");
            }
            
            if(isFormInputElementEmpty(formId,'dtCollectionEndTime')) 
            {		 
                error_count++;
                error_count_create_label++;
                //console.log("dtCollectionEndTime");
            }
         }
         else
         {
             commentFlag=true;
         }
    }     
    
    if(error_count_create_label==1)
    {
        var idBooking=$("#idBooking").val();
        $("#create_admin_label_button").attr("style","opacity:1");
        $("#create_admin_label_button").unbind("click");
        $("#create_admin_label_button").click(function(){ openMailAddressDeliveryPopup(''+idBooking+''); }); 
    }
    else
    {
        $("#create_admin_label_button").attr("style","opacity:0.4");
        $("#create_admin_label_button").unbind("click");
    }
    
    if(error_count==1)
    { 
        if($("#send_forwarder_quote_submit_button").length)
        {
            $("#send_forwarder_quote_submit_button").attr("style","opacity:1");
            $("#send_forwarder_quote_submit_button").unbind("click");
            $("#send_forwarder_quote_submit_button").click(function(){ submitForwarderCourierLabel(); }); 
        }
        $("#send_forwarder_quote_save_button").attr("style","opacity:1");
        $("#send_forwarder_quote_save_button").unbind("click");
        $("#send_forwarder_quote_save_button").click(function(){ saveForwarderCourierLabel(); });
        return true;
    }
    else
    {
        $("#send_forwarder_quote_submit_button").attr("style","opacity:0.4");
        $("#send_forwarder_quote_submit_button").unbind("click");        
        
        $("#send_forwarder_quote_save_button").attr("style","opacity:0.4");
        $("#send_forwarder_quote_save_button").unbind("click");
        return false;
    } 
}

// Expect input as d/m/y
function isValidDate(dtDate)
{
  var bits = dtDate.split('/');
  var d = new Date(bits[2], bits[1] - 1, bits[0]);
  return d && (d.getMonth() + 1) == bits[1];
} 

function saveForwarderCourierLabel()
{
        var viewFlag=$("#viewFlag").val();
	var value=$("#forwarder_courier_label_form").serialize();
	var newValue=value+'&saveFlag=1';
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           if(viewFlag=='admin')
           {
               $("#pending_task_tray_container").html(result_ary[1]);
           }
           else
           {
                $("#pending_task_overview_main_container").html(result_ary[1]);
           }
           $("#send_forwarder_quote_save_button").attr("style","opacity:1");
           $("#send_forwarder_quote_save_button").unbind("click");
           $("#send_forwarder_quote_save_button").click(function(){ saveForwarderCourierLabel(); }); 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#send_forwarder_quote_save_button").attr("style","opacity:0.4");
           $("#send_forwarder_quote_save_button").unbind("click");
        }  
    });  
}

function submitForwarderCourierLabel()
{
    $("#loader").attr('style','display:block'); 
    var viewFlag=$("#viewFlag").val();
    var value=$("#forwarder_courier_label_form").serialize();
	var newValue=value;
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           if(viewFlag=='admin')
           {
               $("#pending_task_tray_container").html(result_ary[1]);
           }
           else
           { 
             $("#pending_task_overview_main_container").html(result_ary[1]);
           }
           $("#send_forwarder_quote_save_button").attr("style","opacity:1");
           $("#send_forwarder_quote_save_button").unbind("click");
           $("#send_forwarder_quote_save_button").click(function(){ saveForwarderCourierLabel(); }); 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#contactPopup").html(result_ary[1]);
           $("#contactPopup").attr('style','display:block;');
        }  
        $("#loader").attr('style','display:none'); 
    }); 
} 

function confirmAddCourierLevel()
{
    var iSendTrackingUpdates = 1; 
    var cb_flag = $("#iSendTrackingUpdates").prop("checked"); 
    if(cb_flag)
    {
         iSendTrackingUpdates = 0;
    } 

    var viewFlag=$("#viewFlag").val();
    var value=$("#forwarder_courier_label_form").serialize();
	var newValue=value+'&mode=SUBMIT_DATA&iSendTrackingUpdates='+iSendTrackingUpdates;
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){ 
            var result_ary = result.split("||||"); 
            
            if($.trim(result_ary) == "TRACKING_API_ERROR")
            {
                $("#contactPopup").attr('style','display:block;');
                $('#contactPopup').html(result_ary[1]);
            }
            else
            {
                if(viewFlag=='admin')
                {
                    $("#pending_task_tray_container").html(result_ary[1]);
                }
                else
                { 
                  $('#pending_task_overview_main_container').html(result_ary[1]);
                }
                $("#contactPopup").attr('style','display:none;');
                //$("#pending_task_overview_main_container").html(result_ary[1]);
                $(".selected-row").html('');
            }
    }); 
}

function showChangeCourierLabelPopup(idBooking)
{
    $("#loader").attr('style','display:block'); 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{idBooking:idBooking,mode:'OPEN_CHANGE_COURIER_LABEL_POPUP'},function(result){ 
           $("#contactPopup").html(result);
           $("#contactPopup").attr('style','display:block;');
    }); 
}

function changeCourierLabelStatus(idBooking)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{idBooking:idBooking,mode:'CHANGE_COURIER_LABEL_POPUP'},function(result){ 
           //$("#contactPopup").html(result);
        $("#contactPopup").attr('style','display:none;');
        location.reload();  
    });
} 

function checkCourierProviderAgreemet()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'CHECK_COURIER_PROVIDER_CREDENTIALS'},function(result)
    {  
        
    });
}

function showTrackingNumberStatus(idBooking,szMasterTrackingNumber,iType,idServiceProvider)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{idBooking:idBooking,iType:iType,szMasterTrackingNumber:szMasterTrackingNumber,mode:'TRACKING_NUMBER_STATUS',idServiceProvider:idServiceProvider},function(result){ 
           
       // alert(result);
       // $("#contactPopup").attr('style','display:none;');
        var result_ary = result.split("||||");  
        if($.trim(result_ary[0]) == "SUCCESS")
        {
            $("#confirm_button_popup").attr("style","opacity:1");
            $("#confirm_button_popup").unbind("click");
            $("#confirm_button_popup").click(function(){ confirmAddCourierLevel(); });
        }
        $("#loader_img").html(result_ary[1]); 
    });
}
function revalidate_all_courier_agreement()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'REVALIDATE_ALL_CARRIER_AGREEMENT'},function(result){
         
    });
}

function calculateVatValueForForwarder(iCounter)
{
    var fForwarderVatHidden = $("#fForwarderVatHidden").val();
    if(parseFloat(fForwarderVatHidden)>0.00)
    {
        var fTotalPriceCustomerCurrency =$("#fTotalPriceForwarderCurrency_"+iCounter).val();
        var fTotalVat = fTotalPriceCustomerCurrency * fForwarderVatHidden * .01 ; 
        fTotalVat = fTotalVat.toFixed(2);
        $("#fTotalVat_"+iCounter).attr('value',fTotalVat);
    }
    else
    {
        $("#fTotalVat_"+iCounter).attr('value','0.00');
    }
}

function clear_forwarder_courier_try_id_out()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'CLEAR_FORWARDER_COURIER_TRY_IT_OUT'},function(result){
         $("#try_it_out_form_container").html(result);
    });
}

function clearTryItOutSerachForm(iQuickQuotePage)
{
    if(iQuickQuotePage)
    {
        window.location.reload(true);
    }else{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{mode:'CLEAR_TRYITOUT_SEARCH_FORM'},function(result){
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#tryitout_search_form_container").html(resArr[1]); 
            $("#forwarder_try_it_out_search_result").html(" ");
        } 
    });
    }
}

function toggleTryItOutCargoFields(cb_id,cargo_type)
{
    if(cb_id=="szShipmentType_BREAK_BULK")
    {
        $("#tryitout_cargo_container_"+cargo_type).attr('style','display:block;');
        $("#tryitout_cargo_container_PARCEL").attr('style','display:none;');
        $("#tryitout_cargo_container_PALLET").attr('style','display:none;');
    }
    else if(cb_id=="szShipmentType_PARCEL")
    {
        $("#tryitout_cargo_container_"+cargo_type).attr('style','display:block;');
        $("#tryitout_cargo_container_BREAK_BULK").attr('style','display:none;');
        $("#tryitout_cargo_container_PALLET").attr('style','display:none;');
    }
    else if(cb_id=="szShipmentType_PALLET")
    {
        $("#tryitout_cargo_container_"+cargo_type).attr('style','display:block;');
        $("#tryitout_cargo_container_BREAK_BULK").attr('style','display:none;');
        $("#tryitout_cargo_container_PARCEL").attr('style','display:none;');
    }
}

function enableTryItOutGetRatesButton(kEevent,onLoad)
{
    var requiredFieldsAry = new Array(); 
    requiredFieldsAry['0'] = 'szOriginCountryStr';
    requiredFieldsAry['1'] = 'szDestinationCountryStr';
    requiredFieldsAry['2'] = 'idServiceTerms';
    requiredFieldsAry['3'] = 'dtShipmentDate';  
    requiredFieldsAry['4'] = 'idCustomerCurrency';   
    
    var formId = 'try_it_out_search_form';
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {		 
            if(onLoad==1)
            {
                //$("#"+input_fields).addClass('red_border');
                //console.log("Empty Field: "+input_fields);
            }
            error++;  
        } 
    }); 
    
    var iBreakBulk = 0;
    var iParcel = 0;
    var iPallet = 0;
    
    if($("#szShipmentType_BREAK_BULK").prop("checked"))
    {
        iBreakBulk =1;
    }
    if($("#szShipmentType_PARCEL").prop("checked"))
    {
        iParcel =1;
    }
    if($("#szShipmentType_PALLET").prop("checked"))
    {
        iPallet =1;
    } 
    
    if(iBreakBulk==0 && iParcel==0 && iPallet==0)
    { 
        $("#label_szShipmentType_BREAK_BULK").addClass('red_border'); 
        $("#label_szShipmentType_PARCEL").addClass('red_border'); 
        $("#label_szShipmentType_PALLET").addClass('red_border');  
        error++;
    } 
    else
    {
        $("#label_szShipmentType_BREAK_BULK").removeClass('red_border');
        $("#label_szShipmentType_PARCEL").removeClass('red_border');
        $("#label_szShipmentType_PALLET").removeClass('red_border');
        
        var cargo_valid_flag = validateTryItOutCargoDetails();
        //console.log("Cargo ctr: "+cargo_valid_flag);
        if(cargo_valid_flag!=1)
        {
            error++;
        }
    }  
    
    
    if(parseInt(error)==1)
    {
        $("#tryitout_get_price_button").unbind("click");
        $('#tryitout_get_price_button').attr('style','opacity:1');
        $("#tryitout_get_price_button").click(function(){ submitTryItOutForm(); });
        if(kEevent.which == 13)
        {
            submitTryItOutForm();
        } 
    }
    else
    {
        $("#tryitout_get_price_button").unbind("click");
        $('#tryitout_get_price_button').attr('style','opacity:0.4;');
    } 
}

function validateTryItOutCargoDetails()
{ 
    var iBreakBulk = $("#szShipmentType_BREAK_BULK").prop('checked');
    var iParcel = $("#szShipmentType_PARCEL").prop('checked');
    var iPallet = $("#szShipmentType_PALLET").prop('checked');
     
    var break_bulk_error = 1;
    var parcel_error = 1;
    var pallet_error = 1;
    var formId = "try_it_out_search_form";
    
    if(iBreakBulk)
    {
        if(isFormInputElementEmpty(formId,"fTotalVolume_BREAK_BULK"))
        {		  
            break_bulk_error++;  
           // console.log("Empty Field: fTotalVolume_BREAK_BULK");
        } 
        if(isFormInputElementEmpty(formId,"fTotalWeight_BREAK_BULK"))
        {		  
            break_bulk_error++;  
           // console.log("Empty Field: fTotalWeight_BREAK_BULK ");
        }
    } 
    if(iParcel)
    { 
        $.each($(".field_name_key_PARCEL"), function(field_key){
            
           var id_suffix = this.value;
           if(id_suffix!='')
           {
                var requiredFieldsAry = new Array(); 
                requiredFieldsAry['0'] = 'iLength'+id_suffix;
                requiredFieldsAry['1'] = 'iWidth'+id_suffix;
                requiredFieldsAry['2'] = 'iHeight'+id_suffix;
                requiredFieldsAry['3'] = 'iWeight'+id_suffix;
                requiredFieldsAry['4'] = 'iQuantity'+id_suffix;
                  
                $.each( requiredFieldsAry, function( index, input_fields ){
                    if(isFormInputElementEmpty(formId,input_fields))
                    {		  
                        parcel_error++;  
                       // console.log("Empty Field: "+input_fields);
                    } 
                }); 
            } 
        });
    }
    
    if(iPallet)
    { 
        $.each($(".field_name_key_PALLET"), function(field_key){
            
           var id_suffix = this.value;
           if(id_suffix!='')
           {
                var requiredFieldsAry = new Array(); 
                requiredFieldsAry['0'] = 'iLength'+id_suffix;
                requiredFieldsAry['1'] = 'iWidth'+id_suffix;
                requiredFieldsAry['2'] = 'szPalletType'+id_suffix;
                requiredFieldsAry['3'] = 'iWeight'+id_suffix;
                requiredFieldsAry['4'] = 'iQuantity'+id_suffix;
                  
                $.each( requiredFieldsAry, function( index, input_fields ){
                    if(isFormInputElementEmpty(formId,input_fields))
                    {		  
                        parcel_error++;  
                       // console.log("Empty Field: "+input_fields);
                    } 
                }); 
            } 
        }); 
    }
    
    if(break_bulk_error==1 && parcel_error==1 && pallet_error==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

function checkFromAddressForwarder(szValue,flag,on_blur,iQuickQuotePageGoogleFlag)
{
    var id_suffix="";
    var ret_ary = new Array();
    var szCity = '';
    var szState = '';
    var szCountryISO = '';
    var szCountryName = '';
    var error_count = 1; 
    
    var szFieldID='';
    var formId = 'try_it_out_search_form';
    if(flag=='FROM_COUNTRY')
    {
        szFieldID = 'szOriginCountryStr';
    }
    else
    {
        szFieldID = 'szDestinationCountryStr';
    } 
    if(isFormInputElementEmpty(formId,szFieldID))
    { 
        $("#"+szFieldID).addClass('red_border');
    }
    else
    { 
        var iQuickQuotePageGoogleValue=0;
        if(iQuickQuotePageGoogleFlag==true)
        {
            iQuickQuotePageGoogleValue=1;
        }
        transporteca_geocode_service(szValue,flag,id_suffix,iQuickQuotePageGoogleValue);     
    } 
}

function submitTryItOutForm()
{
//    $("#tryitout_get_price_button").addClass('get-rates-btn-clicked'); 
    //Removing displayed result
    $("#forwarder_try_it_out_search_result").html(" ");
    $("#loader_popup_1").attr('style','display:block');
    
    $("#forwarder_try_it_out_search_result").html(''); 
    $("#forwarder_try_it_out_search_result").attr('style','display:none'); 
    
    $("#quick_quote_send_quote_container").attr('style','display:none');
    $("#quick_quote_make_booking_container").attr('style','display:none');
    
    $("#booking_information_container .red_border").removeClass('red_border');
    
    close_open_div('booking_information_container','pending_task_overview_open_close_link','close',1);
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php?mode=SUBMIT_QUICK_QUOTE_FORM",$("#try_it_out_search_form").serialize(),function(result){
        var resArr=result.split("$$$$");
        if(resArr[0]=='SUCCESS')
        {
            $("#forwarder_try_it_out_search_result").html(resArr[1]); 
            $("#forwarder_try_it_out_search_result").attr('style','display:block');   
            $('html, body').animate({ scrollTop: ($("#forwarder_try_it_out_search_result").offset().top - 20) }, "100000");
            /*if(resArr[2]!='')
            {
                $("#quick_quote_booking_infomation_container").html(resArr[2]); 
                $("#quick_quote_booking_infomation_container").attr('style','display:block');
                $("#pending_task_overview_open_close_link").removeClass("close-icon");
                $("#pending_task_overview_open_close_link").addClass("open-icon");
                
                $("#pending_task_overview_open_close_link").attr("onclick","");
                $("#pending_task_overview_open_close_link").unbind("click");
                $("#pending_task_overview_open_close_link").click(function(){close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1)}); 
                $("#booking_information_container").slideUp();
            }*/
        }
        else if(jQuery.trim(resArr[0])=='ERROR')
        {
            $("#tryitout_search_form_container").html(resArr[1]); 
        }
        //$("#quick_quote_get_price_button").removeClass('get-rates-btn-clicked');
        $("#loader_popup_1").attr('style','display:none');
    });
}

function add_more_cargo_tryitout_quote(mode)
{  
    var szShipmentType = mode;
    var number = document.getElementById('hiddenPosition_'+mode).value;  
    var table_id = document.getElementById('horizontal-scrolling-div-id_'+mode); 
    number=parseInt(number); 
    var hidden1 = document.getElementById('hiddenPosition1_'+mode).value; 
    document.getElementById('hiddenPosition1_'+mode).value=parseInt(hidden1)+1; 
    document.getElementById('hiddenPosition_'+mode).value =  parseInt(number)+1; 
        
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{number:number,shipment_type:szShipmentType,mode:'ADD_MORE_TRYITOUT_QUOTE'},function(result){ 
        $("#add_booking_quote_container_"+number+"_"+mode).html(result); 
        $("#add_booking_quote_container_"+number+"_"+mode).attr("style",'display: block;');  
        $("#add_booking_quote_container_"+number+"_"+mode).addClass('request-quote-fields');  
        
        var active_counter=$('input.first-cargo-fields_V_'+mode).length ;  
        if(active_counter>1)
        {
            $("#cargo-line-remove-1-v"+mode).attr('style','opacity:0.4');
            $("#cargo-line-remove-1-v"+mode).removeAttr('onclick');
            $("#cargo-line-remove-1-v"+mode).unbind("click");
        }  
    }); 
    
    var newTdId = 'add_booking_quote_container_'+(number+1)+"_"+mode ;
    if($("#"+newTdId).length)
    {
        console.log("div already exists");
    }
    else
    {
        var newTd=document.createElement('div'); 
        newTd.setAttribute('id', 'add_booking_quote_container_'+(number+1)+"_"+mode); 
        newTd.setAttribute('style', 'display:none;');
        table_id.appendChild(newTd); 
    }
}

function remove_cargo_tryitout_quote(div_id,mode,number)
{  
    var number = $('#hiddenPosition_'+mode).attr('value'); 
    var hidden1= $('#hiddenPosition1_'+mode).attr('value');
    var hidden_value = parseInt(hidden1)-1;
    var szDivKey = "_V_"+mode; 
    var active_counter=$('input.first-cargo-fields'+szDivKey).length ;  
    if(active_counter>1)
    {
        $('#hiddenPosition_'+mode).attr('value',hidden_value);
        $('#hiddenPosition1_'+mode).attr('value',hidden_value);

        $("#"+div_id).attr("style",'display:none;');
        $("#"+div_id).html(" ");
        
        var active_counter = $('input.first-cargo-fields'+szDivKey).length ;  
        if(active_counter==1)
        {
            $("#cargo-line-remove-1-v"+mode).attr('style','opacity:1');
            $("#cargo-line-remove-1-v"+mode).removeAttr('onclick');
            $("#cargo-line-remove-1-v"+mode).unbind("click");
            $("#cargo-line-remove-1-v"+mode).click(function(){ remove_cargo_tryitout_quote('add_booking_quote_container_0_'+mode,mode,number); });
        } 
    }
    else
    {
        var field_count = number;   
        $("#iLength"+field_count+szDivKey).val("");
        $("#iWidth"+field_count+szDivKey).val("");
        $("#iHeight"+field_count+szDivKey).val("");
        $("#iWeight"+field_count+szDivKey).val("");
        $("#iQuantity"+field_count+szDivKey).val("");
        
        if(mode=='PALLET')
        {
            $("#szPalletType"+field_count+szDivKey).val("1"); 
            prefill_dimensions(1,field_count+szDivKey);
        } 
    } 
} 

function display_forwarder_try_it_out_pricing_details(token,service_id)
{
    $(".fwd_try_it_out_rows").removeClass("selected-row");
    
    var selected_row_id = "fwd_try_it_out_row_id_"+service_id;
    if($("#"+selected_row_id).length)
    {
        $("#"+selected_row_id).addClass("selected-row");
    } 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_serviceOffering.php",{token:token,service_id:service_id,mode:'SHOW_PRICE_DETAILS'},function(result){  
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#display_try_it_out_price_detailscontainer").html(resArr[1]);
        }
    }); 
}

function enablePrivateCustomerSetting()
{
    var requiredFieldsAry = new Array(); 
    requiredFieldsAry['0'] = 'iPrivateCustomerAvailable';
    requiredFieldsAry['1'] = 'fCustomerFee';
    requiredFieldsAry['2'] = 'idCurrency'; 
    
    var possibleProductAry = new Array();
    possibleProductAry[0] = 'LCL';
    possibleProductAry[1] = 'LTL';
    possibleProductAry[2] = 'COURIER';
    
    var formId = 'forwarder_private_customer_setting_form';
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){  
        $.each( possibleProductAry, function( index_product, product_code ){ 
            var iPrivateCustomerAvailable = "iPrivateCustomerAvailable_"+product_code;
            var cb_flag = $("#"+iPrivateCustomerAvailable).prop("checked"); 
            if(cb_flag)
            {
                var szInputFieldID = input_fields+"_"+product_code;
                if(isFormInputElementEmpty(formId,szInputFieldID))
                {		 
                    //console.log("Field: "+szInputFieldID);
                    error++;  
                } 
            } 
        }); 
    });  
    if(parseInt(error)==1)
    {
        $("#private_customer_setting_save_button").unbind("click");
        $('#private_customer_setting_save_button').attr('style','opacity:1');
        $("#private_customer_setting_save_button").click(function(){ submitPrivateCustomerSettingForm(); }); 
    }
    else
    {
        $("#private_customer_setting_save_button").unbind("click");
        $('#private_customer_setting_save_button').attr('style','opacity:0.4;');
    }
}

function submitPrivateCustomerSettingForm()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$("#forwarder_private_customer_setting_form").serialize(),function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#forwarder_private_customer_setting_container").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#forwarder_private_customer_setting_container").html(result_ary[1]);	 
        }  
    });
}

function sort_task_estimate_list_forwarder(sort_field,idOrginCountry,idDestinationCountry)
{
    var szOldField = $("#szOldFieldQuote").val();    
    if(szOldField==sort_field)
    {
        var szSortType = $("#szSortTypeQuote").val(); 
        if(szSortType=='ASC')
        {
            var sort = 'DESC';
        }
        else
        {
            var sort = 'ASC';
        } 
    }
    else
    {
         var sort = 'ASC';
    }    
    
    if(sort=='ASC')
    {
        var new_sort='DESC';
        var innr_htm = '<span class="sort-arrow-up">&nbsp;</span>';
    }
    else
    {
        var new_sort='ASC';
        var innr_htm = '<span class="sort-arrow-down">&nbsp;</span>';
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'SHOW_OLD_QUOTE_OF_FORWARDER',idDestinationCountry:idDestinationCountry,sort_by:sort,sort_field:sort_field,idOrginCountry:idOrginCountry},function(result){
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#forwarder_submit_quote_listing").html(result_ary[1]);   
            $(".moe-transport-sort-span-quote").removeClass('sort-arrow-up');
            $(".moe-transport-sort-span-quote").removeClass('sort-arrow-down'); 
            if(sort=='ASC')
            {
                $("#moe-transport-sort-span-id-"+sort_field).addClass('sort-arrow-down');
            }
            else
            {
                $("#moe-transport-sort-span-id-"+sort_field).addClass('sort-arrow-up');
            }
            $("#szOldFieldQuote").val(sort_field);
            $("#szSortTypeQuote").val(sort);
        } 
    });
}

function close_open_div_forwarder_pending_tray(divid,dividButton)
{
    var disp = $("#"+divid).css('display');
    if(disp=='block')
    {
        $("#"+divid).attr('style',"display:none");
        $("#"+dividButton).removeClass('close-icon');
        $("#"+dividButton).addClass('open-icon');
    }
    else
    {
        $("#"+dividButton).removeClass('open-icon');
        $("#"+dividButton).addClass('close-icon');        
        $("#"+divid).attr('style',"display:block");
    }
}


function number_format(number, decimals, dec_point, thousands_sep) 
{  
  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}

function enableBookingButtons(cb_id,szServiceID,convert_rfq_flag)
{
    var cb_flag = false;
    if(convert_rfq_flag==1)
    {
        cb_flag = true;
    }
    else
    {
        cb_flag = $("#"+cb_id).prop('checked');
    } 
    var szFormID = 'quick_quote_shipper_consignee_form';
    
    $("#quick_quote_send_quote_container").attr('style','display:none');
    $("#quick_quote_make_booking_container").attr('style','display:none');
    
    close_open_div('booking_information_container','pending_task_overview_open_close_link','close',1);
    $("#booking_information_container .red_border").removeClass('red_border');
     
    var standardFieldAry = new Array();
    standardFieldAry[0] = 'szAllServieIDS';
    standardFieldAry[0] = 'szToken'; 
    
    $.each(standardFieldAry, function(index, input_fields ){ 
        var szFieldId = input_fields+"_BI_"+szServiceID+""; 
        removeHiddenFields(szFieldId);
    }); 
    
    if(cb_flag)
    {
        $.each(standardFieldAry, function( index, input_fields ){ 
            var szFieldName = "quickQuoteShipperConsigneeAry["+input_fields+"]";
            var szFieldId = input_fields+"_BI";
            var szValue = $("#"+input_fields).val();
            addHiddenFields(szFormID, szFieldName, szFieldId, szValue);
        }); 
    }
    
    if(szServiceID!="")
    {
        var createFieldAry = new Array();
        createFieldAry[0] = 'idTransportMode';
        createFieldAry[1] = 'idManualFeeCurrency';
        createFieldAry[2] = 'fTotalManualFee';
        createFieldAry[3] = 'idShipmentType'; 
        createFieldAry[4] = 'fTotalForwarderManualFee'; 

        $.each(createFieldAry, function(index, input_fields ){ 
            var szFieldId = input_fields+"_BI_"+szServiceID+""; 
            removeHiddenFields(szFieldId);
        }); 

        var szServiceField = "szServiceIds_BI_"+szServiceID;
        removeHiddenFields(szServiceField);

        if(cb_flag)
        {
            $.each(createFieldAry, function( index, input_fields ){ 
                var szFieldName = "quickQuoteShipperConsigneeAry["+input_fields+"]["+szServiceID+"]";
                var szFieldId = input_fields+"_BI_"+szServiceID+"";
                var szValue = $("#"+input_fields+"_"+szServiceID).val();
                addHiddenFields(szFormID, szFieldName, szFieldId, szValue);
            });  
 
            if(convert_rfq_flag!=1)
            {
                var szFieldName = "quickQuoteShipperConsigneeAry[szServiceIds][]";
                var szFieldId = szServiceField;
                var szValue = szServiceID;
                addHiddenFields(szFormID, szFieldName, szFieldId, szValue); 
            } 
        }  
    } 
    
   
    var counter = check_multiple_selection();
    counter = parseInt(counter); 
    if(counter>0)
    {
        $("#quote_result_send_quote_button").unbind("click");
        $("#quote_result_send_quote_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','SEND_QUOTE'); }); 
        $("#quote_result_send_quote_button").attr("style","opacity:1"); 
        //display_booking_info('SEND_QUOTE');
    }
    else
    {
        $("#quote_result_send_quote_button").unbind("click");
        $("#quote_result_send_quote_button").attr("style","opacity:0.4;"); 
    } 
    if(counter==1)
    {
        $("#quote_result_make_booking_button").unbind("click");
        $("#quote_result_make_booking_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','CREATE_BOOKING'); }); 
        $("#quote_result_make_booking_button").attr("style","opacity:1"); 
    }
    else
    {
        $("#quote_result_make_booking_button").unbind("click");
        $("#quote_result_make_booking_button").attr("style","opacity:0.4;"); 
    }
    
} 

function display_booking_info(type)
{ 
    if(type=='CREATE_BOOKING')
    {
        $("#quick_quote_make_booking_container").attr('style','display:block;');
        $("#quick_quote_send_quote_container").attr('style','display:none;');
    }
    else
    {
        $("#quick_quote_make_booking_container").attr('style','display:none;');
        $("#quick_quote_send_quote_container").attr('style','display:block;');
    }
    close_open_div('booking_information_container','pending_task_overview_open_close_link','open');
    validateQQBookingInformation(1);
}

function check_multiple_selection(convert_rfq_quote)
{ 
    var counter = 0;
    var bNoRecordFound = 1;
    $.each($(".quick-quote-service-cb"), function(e){
        if(convert_rfq_quote==1)
        {
            var cb_id = this.id;
            var szServiceID = this.value;
            bNoRecordFound++;
            enableBookingButtons(cb_id,szServiceID,1);
        }
        else
        {
            var cb_id = this.id;
            var cb_flag = $("#"+cb_id).prop('checked');
            if(cb_flag)
            {
                counter++;
            } 
        } 
    }); 
    
    if(bNoRecordFound==1 && convert_rfq_quote==1)
    {
        enableBookingButtons(false,"",1);
    }
    return counter;
}

function validateQuickQuoteCargo(szShipmentType,counter)
{ 
    var requiredFieldsAry = new Array(); 
    var formId = 'quick_quote_search_form';
    if(szShipmentType=='BREAK_BULK')
    { 
        requiredFieldsAry['0'] = 'fTotalVolume_'+szShipmentType;  
        requiredFieldsAry['1'] = 'fTotalWeight_'+szShipmentType; 
    }
    else
    {
        requiredFieldsAry['0'] = 'iLength'+counter+"_V_"+szShipmentType; 
        requiredFieldsAry['1'] = 'iWidth'+counter+"_V_"+szShipmentType;  
        requiredFieldsAry['2'] = 'iQuantity'+counter+"_V_"+szShipmentType; 
        requiredFieldsAry['3'] = 'iWeight'+counter+"_V_"+szShipmentType; 
        if(szShipmentType=='PARCEL')
        {
            requiredFieldsAry['4'] = 'iHeight'+counter+"_V_"+szShipmentType; 
        }
    } 
    
    
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {	 
            $("#"+input_fields).addClass('red_border');
            error++;  
        } 
    }); 
    return error;
}

function submitSearchResultForm(formId,mode)
{
    var serialized_form_data = "";
    if(mode=='CONVERT_RFQ')
    {
        var counter = check_multiple_selection();
        counter = parseInt(counter);  
        if(counter==0)
        { 
            check_multiple_selection(1);
            
            var szAllServieIDS = $("#szAllServieIDS").val();
            $("#szAllServieIDS_BI").val(szAllServieIDS);
        } 
    }  
    if(mode=='CONVERT_RFQ_CONFIRM')
    {
        var szInternalComment = $("#szInternalComment_popup").val();
        $("#szInternalComment_BI").val(szInternalComment);
         
        var idPackingType =$('#idPackingType_popup :selected').val();
        $("#idPackingType_BI").val(idPackingType); 
         
        var serialized_form_1 = $("#quick_quote_shipper_consignee_form").serialize(); 
        serialized_form_data = serialized_form_1;
        //$("#quote_quote_convert_rfq_confirm_button").addClass('get-rates-btn-clicked');
    } 
    else
    {
        serialized_form_data = $("#quick_quote_shipper_consignee_form").serialize();
    }
    
    $("#quick_quote_send_quote_container").attr('style','display:none');
    $("#quick_quote_make_booking_container").attr('style','display:none');
    
    close_open_div('booking_information_container','pending_task_overview_open_close_link','close',1);
    
    if(mode=='SEND_QUOTE_CONFIRM' || mode=='CREATE_BOOKING_CONFIRM')
    { 
        if(mode=='SEND_QUOTE_CONFIRM')
        {
            var content = NiceEditorInstance.instanceById('szEmailBody_SEND_QUOTE').getContent();
            var description = encode_string_msg_str(content);
            serialized_form_data = serialized_form_data + "&quickQuoteShipperConsigneeAry[SEND_QUOTE][szEmailBody]="+description; 
        } 
        else
        {
            var content = NiceEditorInstance.instanceById('szEmailBody_CREATE_BOOKING').getContent();
            var description = encode_string_msg_str(content);
            serialized_form_data = serialized_form_data + "&quickQuoteShipperConsigneeAry[CREATE_BOOKING][szEmailBody]="+description; 
        }
    }
    if(mode=='SEND_QUOTE' || mode=='CREATE_BOOKING')
    {
        $("#booking_information_container .red_border").removeClass('red_border');
        $("#quote_result_make_booking_button").unbind("click");
        $("#quote_result_make_booking_button").attr("style","opacity:0.4;");
        
        $("#quote_result_send_quote_button").unbind("click");
        $("#quote_result_send_quote_button").attr("style","opacity:0.4;");
    }
    else
    {
        $("#loader_popup_1").attr('style','display:block');
    }
    
   
    if(mode=='SEND_QUOTE')
    {
        $("#quote_result_send_quote_button span").html("<img style='display:block;' src='"+__JS_ONLY_SITE_BASE_IMAGE__+"/images/loader.gif' alt='' /> Send Quote");
        $("#quote_result_send_quote_button").addClass("tyni-loader-quick-quote");
        $("#quote_result_make_booking_button").unbind("click");
        $("#quote_result_make_booking_button").attr("style","opacity:0.4;");
    }
    else if(mode=='CREATE_BOOKING')
    {
        $("#quote_result_make_booking_button span").html("<img style='display:block;' src='"+__JS_ONLY_SITE_BASE_IMAGE__+"/images/loader.gif' alt='' /> Make Booking");
        $("#quote_result_make_booking_button").addClass("tyni-loader-quick-quote");
        $("#quote_result_send_quote_button").unbind("click");
        $("#quote_result_send_quote_button").attr("style","opacity:0.4;");
    }
    /*
    * Adding little wheel on click of following buttons
    */
    /*if(mode=='SEND_QUOTE')
    { 
        $("#quote_result_send_quote_button").addClass('get-rates-btn-clicked');
    }
    else if(mode=='CREATE_BOOKING')
    {
        $("#quote_result_make_booking_button").addClass('get-rates-btn-clicked');
    } 
    else if(mode=='SEND_QUOTE_CONFIRM')
    {
        $("#quick_quote_send_quote_button").addClass('get-rates-btn-clicked');
    }
    else if(mode=='CREATE_BOOKING_CONFIRM')
    {
        $("#quick_quote_create_booking_button").addClass('get-rates-btn-clicked');
    } */
    var top=$("#quick_quote_booking_infomation_main_container").offset().top;
    //alert("top"+top);
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php?mode="+mode,serialized_form_data,function(result){
        var resArr=result.split("||||");
        if(mode=='CONVERT_RFQ')
        {
            if(resArr[0]=='SUCCESS')
            {
                $("#quick_quote_booking_popup").html(resArr[1]);  
                $("#quick_quote_booking_popup").attr('style','display:block;');  
            } 
        }
        else if(mode=='CONVERT_RFQ_CONFIRM')
        {
            if(resArr[0]=='SUCCESS')
            {
                redirect_url(resArr[1]);
            }
            else if(resArr[0]=='ERROR')
            {
                $("#quick_quote_booking_popup").html(resArr[1]); 
            }
        } 
        else if(mode=='SEND_QUOTE' || mode=='CREATE_BOOKING')
        {
            if(resArr[0]=='SUCCESS')
            {
                //$("#quick_quote_booking_infomation_container").html(resArr[1]); 
                
        
                $("#quick-quote-email-box-container").html(resArr[1]);  
                
                close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1,1);
                
                $(".quick-quote-action-button").unbind('click'); 
                $(".quick-quote-action-button").attr("style","opacity:0.4;"); 
                
                if(mode=='CREATE_BOOKING')
                {
                    $("#insurance_dropdoen_SEND_QUOTE").attr("style","display:none;"); 
                    $("#insurance_dropdoen_CREATE_BOOKING").attr("style","display:block;"); 
                }
            } 
            else
            {
                $("#quick_quote_booking_infomation_container").html(resArr[1]); 
                close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1,1);
            }
            
            if(mode=='SEND_QUOTE')
            {
                $("#quote_result_send_quote_button span").html('Send Quote');
                $("#quote_result_send_quote_button").removeClass("tyni-loader-quick-quote");
                
            }
            else if(mode=='CREATE_BOOKING')
            {
                $("#quote_result_make_booking_button span").html('Make Booking');
                $("#quote_result_make_booking_button").removeClass("tyni-loader-quick-quote");
            }

            validateQQBookingInformation(1);
            autofill_billing_address__new_rfq(1);
            buildQuickQuoteEmail(mode); 
        }
        else if(mode=='SEND_QUOTE_CONFIRM' || mode=='CREATE_BOOKING_CONFIRM')
        {
            if(resArr[0]=='SUCCESS')
            {
                if(mode=='SEND_QUOTE_CONFIRM')
                {  
                    //$("#quick_quote_booking_infomation_container").html(resArr[1]);  
                    $("#quick-quote-email-box-container").html(resArr[1]);
                    $("#contactPopup").html(resArr[2]);
                    $("#contactPopup").attr("style","display:block");
                }
                else
                {
                   $("#contactPopup").html(resArr[1]);
                    $("#contactPopup").attr("style","display:block");
                    //$("#success_message_span_create_booking").html("Booking Sent");
                } 
            } 
            else if(resArr[0]=='ERROR')
            {
                $("#quick_quote_booking_infomation_container").html(resArr[1]); 
                $("#booking_information_container").attr('style','display:block;');
                
                //close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1);
            }
        } 
        /*
        * Removing little wheel on click of following buttons
        */
        /*if(mode=='SEND_QUOTE')
        { 
            $("#quote_result_send_quote_button").removeClass('get-rates-btn-clicked');
        } 
        else if(mode=='CREATE_BOOKING')
        {
            $("#quote_result_make_booking_button").removeClass('get-rates-btn-clicked');
        }
        else if(mode=='SEND_QUOTE_CONFIRM')
        {
            $("#quote_result_make_booking_button").removeClass('get-rates-btn-clicked');
        }
        else if(mode=='CREATE_BOOKING_CONFIRM')
        {
            $("#quick_quote_create_booking_button").removeClass('get-rates-btn-clicked');
        }*/ 
        
       //$('html, body').animate({ scrollTop: (top - 20) }, "100");
        $("#loader_popup_1").attr('style','display:none');
    });
} 

function removeHiddenFields(szFieldId) 
{
    if($("#"+szFieldId).length)
    {
        $("#"+szFieldId).remove();
    }
}

function addHiddenFields(szFormID, szFieldName, szFieldId, szValue) 
{
    removeHiddenFields(szFieldId); 
    // Create a hidden input element, and append it to the form:
    var input = document.createElement('input');
    input.type = 'hidden';
    input.id = szFieldId;
    input.name = szFieldName;
    input.value = szValue;
    $("#"+szFormID).append(input);
}


function autofill_billing_address__new_rfq(quick_quote_flag)
{   
    if(quick_quote_flag==1)
    {
        var iShipperConsignee = $('input:radio[name="quickQuoteShipperConsigneeAry[iShipperConsignee]"]:checked').val();  
    } 
    else
    {
        var iShipperConsignee = $('input:radio[name="updatedTaskAry[iShipperConsignee]"]:checked').val();  
    }
    if((iShipperConsignee==1 || iShipperConsignee==2))
    {
        var szCompanyName = $("#szCustomerCompanyName").val();
        var szFirstName = $("#szFirstName").val();
        var szLastName = $("#szLastName").val();
        var szEmail = $("#szEmail").val();
        var idDialCode = $("#idCustomerDialCode").val();
        var szPhone = $("#szCustomerPhoneNumber").val();
        var szAddress = $("#szCustomerAddress1").val();
        var szPostcode = $("#szCustomerPostCode").val();
        var szCity = $("#szCustomerCity").val();
        var idCountry = $("#szCustomerCountry").val();
        
        if(iShipperConsignee==1)
        { 
            $("#szShipperCompanyName").val(szCompanyName);
            $("#szShipperFirstName").val(szFirstName);
            $("#szShipperLastName").val(szLastName);
            $("#szShipperEmail").val(szEmail);
            $("#idShipperDialCode").val(idDialCode); 
            $("#szShipperPhoneNumber").val(szPhone);
            
            $("#szOriginAddress").val(szAddress);
            $("#szOriginPostcode").val(szPostcode);
            $("#szOriginCity").val(szCity);
            $("#idOriginCountry").val(idCountry); 
            
            $(".shipper-fields").addClass('bg-grey');
            $(".shipper-fields").attr('readonly','readonly');
            $(".shipper-fields").removeClass('red_border');
            
            $(".consignee-fields").removeClass('bg-grey');
            $(".consignee-fields").removeAttr('readonly');
        }
        else
        {  
            $(".shipper-fields").removeClass('bg-grey');
            $(".shipper-fields").removeAttr('readonly');
            
            $(".consignee-fields").addClass('bg-grey');
            $(".consignee-fields").attr('readonly','readonly');
            $(".consignee-fields").removeClass('red_border');
            
            $("#szConsigneeCompanyName").val(szCompanyName);
            $("#szConsigneeFirstName").val(szFirstName);
            $("#szConsigneeLastName").val(szLastName);
            $("#szConsigneeEmail").val(szEmail);
            $("#idConsigneeDialCode").val(idDialCode); 
            $("#szConsigneePhoneNumber").val(szPhone);
            
            $("#szDestinationAddress").val(szAddress);
            $("#szDestinationPostcode").val(szPostcode);
            $("#szDestinationCity").val(szCity);
            $("#idDestinationCountry").val(idCountry); 
        }    
    } 
    else
    { 
        $(".shipper-fields").removeClass('bg-grey');
        $(".consignee-fields").removeClass('bg-grey');
        
        //$(".consignee-fields").css('background-color','');        
        //$(".shipper-fields").css('background-color','');
        
        $(".consignee-fields").removeAttr('readonly');
        $(".shipper-fields").removeAttr('readonly');
    }  
    if(quick_quote_flag==1)
    {
        //TO DO
    }
    else
    {
        update_terms_down();
    } 
}


function validateQQBookingInformation(onLoad)
{
    var requiredFieldsAry = new Array(); 
    requiredFieldsAry['0'] = 'szFirstName';
    requiredFieldsAry['1'] = 'szLastName';
    requiredFieldsAry['2'] = 'szEmail'; 
    var szBookingType = $("#szBookingType").val();
    /*
    * We only have above 3 fields are required in case of SEND_QUOTE @Ajay
    */
   
    var szShipperPostcodeRequiredFlag ='';
   var szConsigneePostcodeRequiredFlag ='';
   var szCollectionFlag='';
   var szDeliveryFlag='';
   
    if(szBookingType=='CREATE_BOOKING')
    { 
        $.each($(".quick-quote-service-cb"), function(e){
            var cb_id = this.id;
            
            var cb_flag = $("#"+cb_id).prop('checked');
            if(cb_flag)
            {
                var szServiceID= $("#"+cb_id).attr('value');
                //alert(szServiceID);
                szCollectionFlag = $("#szCollectionFlag_"+szServiceID).val();
                szDeliveryFlag = $("#szDeliveryFlag_"+szServiceID).val();
                szConsigneePostcodeRequiredFlag = $("#szConsigneePostcodeRequiredFlag_"+szServiceID).attr('value');
                //alert(szConsigneePostcodeRequiredFlag+"szConsigneePostcodeRequiredFlag");
                szShipperPostcodeRequiredFlag = $("#szShipperPostcodeRequiredFlag_"+szServiceID).val();
                //alert(szShipperPostcodeRequiredFlag+"szShipperPostcodeRequiredFlag");
            }
            
        });
        requiredFieldsAry['3'] = 'szCustomerPhoneNumber';   
        requiredFieldsAry['5'] = 'szCustomerAddress1';   
        requiredFieldsAry['6'] = 'szCustomerPostCode'; 
        requiredFieldsAry['7'] = 'szCustomerCity'; 
        requiredFieldsAry['8'] = 'szCustomerCountry';   
        requiredFieldsAry['9'] = 'szShipperFirstName'; 
        requiredFieldsAry['10'] = 'szShipperLastName'; 
        requiredFieldsAry['11'] = 'szShipperCompanyName'; 
        requiredFieldsAry['12'] = 'szShipperPhoneNumber'; 
        requiredFieldsAry['13'] = 'szShipperEmail'; 
        requiredFieldsAry['14'] = 'szOriginAddress'; 
        
        requiredFieldsAry['15'] = 'szOriginPostcode'; 
        
        requiredFieldsAry['16'] = 'szOriginCity'; 
        requiredFieldsAry['17'] = 'idOriginCountry';   
        requiredFieldsAry['18'] = 'szConsigneeFirstName'; 
        requiredFieldsAry['19'] = 'szConsigneeLastName';    
        requiredFieldsAry['20'] = 'szConsigneeEmail'; 
        requiredFieldsAry['21'] = 'szConsigneePhoneNumber';
        requiredFieldsAry['22'] = 'szConsigneeEmail';
        requiredFieldsAry['23'] = 'szDestinationAddress';
        
        requiredFieldsAry['24'] = 'szDestinationPostcode';
        
        requiredFieldsAry['25'] = 'szDestinationCity';
        requiredFieldsAry['26'] = 'idDestinationCountry'; 
        requiredFieldsAry['27'] = 'szCargoDescription';
        requiredFieldsAry['28'] = 'iInsuranceIncluded'; 
        var iInsuranceIncluded = $("#iInsuranceIncluded").val();
        if(iInsuranceIncluded!=2)
        {
            requiredFieldsAry['29'] = 'idGoodsInsuranceCurrency';
            requiredFieldsAry['30'] = 'fCargoValue';
        } 
        var iPrivateCustomer = $("#iPrivateCustomer").val();
        console.log("iPrivateCustomer"+iPrivateCustomer);
        if(iPrivateCustomer!=1)
        {
            console.log("iPrivateCustomer"+iPrivateCustomer);
            requiredFieldsAry['4'] = 'szCustomerCompanyName'; 
        } 
    } 
    
    var formId = 'quick_quote_shipper_consignee_form';
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
       // console.log("Error"+index);
        var checkflag=true;
        if(iPrivateCustomer==1 && index==4)
        {
            checkflag=false;
            $("#szCustomerCompanyName").removeClass('red_border');
        }
        if(szCollectionFlag=='No' && index==14){
             checkflag=false;
            $("#szOriginAddress").removeClass('red_border');
         }
         if(szShipperPostcodeRequiredFlag=='No' && index==15){
             checkflag=false;
            $("#szOriginPostcode").removeClass('red_border');
         }
         
         if(szDeliveryFlag=='No' && index==23){
             checkflag=false;
            $("#szDestinationAddress").removeClass('red_border');
         }
         if(szConsigneePostcodeRequiredFlag=='No' && index==24){
             checkflag=false;
            $("#szDestinationPostcode").removeClass('red_border');
         }
        if(checkflag){
            if(isFormInputElementEmpty(formId,input_fields))
            {		 
                if(onLoad==1)
                {
                    $("#"+input_fields).addClass('red_border');
                } 
               // console.log(index+"input_fields"+input_fields);
                error++;  
            } 
            else
            {
                $("#"+input_fields).removeClass('red_border');
            }
        }
    });  
    
    //@TO DO
    if(parseInt(error)==1)
    {
        if(szBookingType=='SEND_QUOTE')
        {
            $("#quick_quote_send_quote_button").unbind("click");
            $('#quick_quote_send_quote_button').attr('style','opacity:1');
            $("#quick_quote_send_quote_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','SEND_QUOTE_CONFIRM');});
        } 
        else if(szBookingType=='CREATE_BOOKING')
        {
            $("#quick_quote_create_booking_button").unbind("click");
            $('#quick_quote_create_booking_button').attr('style','opacity:1');
            $("#quick_quote_create_booking_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','CREATE_BOOKING_CONFIRM');});
        }
    }
    else
    {
        if(szBookingType=='SEND_QUOTE')
        {
            $("#quick_quote_send_quote_button").unbind("click");
            $('#quick_quote_send_quote_button').attr('style','opacity:0.4;');
        }
        else if(szBookingType=='CREATE_BOOKING')
        {
            $("#quick_quote_create_booking_button").unbind("click");
            $('#quick_quote_create_booking_button').attr('style','opacity:0.4;');
        } 
    }   
}


function close_open_div(divid,dividhref,type,quick_quote,quick_quote_flag)
{
    var iScrollUp = 0;
    if(quick_quote==1)
    {
        iScrollUp = 1;
    }
    else if(quick_quote==2)
    {
        iScrollUp = 0;
        quick_quote = 1;
    }
    if(type=='close')
    {
        $("#"+dividhref).attr("onclick","");
        $("#"+dividhref).unbind("click");
        $("#"+dividhref).click(function(){close_open_div(divid,dividhref,'open',quick_quote)}); 
        $("#"+divid).slideUp();
        //$("#"+divid).attr('style','display:none;');
        $("#"+dividhref).removeClass('close-icon').addClass('open-icon'); 
    }
    else if(type=='open')
    {
        $("#"+dividhref).attr("onclick","");
        $("#"+dividhref).unbind("click");
        $("#"+dividhref).click(function(){close_open_div(divid,dividhref,'close',quick_quote)});
        $("#"+divid).slideDown(); 
        $("#"+dividhref).removeClass('open-icon').addClass('close-icon');
        if(iScrollUp==1)
        {
            if(quick_quote_flag==1)
            {
                var document_height = $("#quick_quote_booking_infomation_main_container").offset().top; 
            }
            else
            {
                var document_height = $(document).height(); 
            }
            $('html, body').animate({ scrollTop: document_height }, "100000");
        }
    }
}

function prefill_selected_values(szServiceId)
{
    var cb_flag = $("#szServiceIs_"+szServiceId).prop('checked');
    if(cb_flag)
    {
        var cb_id = "szServiceIs_"+szServiceId;
        enableBookingButtons(cb_id,szServiceId);
    }
}

function check_form_field_empty_quick_quote(formId,inputId)
{
    var szBookingType = $("#szBookingType").val();
    
     var szShipperPostcodeRequiredFlag ='';
   var szConsigneePostcodeRequiredFlag ='';
   var szCollectionFlag='';
   var szDeliveryFlag='';
   
   if(szBookingType=='CREATE_BOOKING'){
    $.each($(".quick-quote-service-cb"), function(e){
            var cb_id = this.id;
            
            var cb_flag = $("#"+cb_id).prop('checked');
            if(cb_flag)
            {
                var szServiceID= $("#"+cb_id).attr('value');
                //alert(szServiceID);
                szCollectionFlag = $("#szCollectionFlag_"+szServiceID).val();
                szDeliveryFlag = $("#szDeliveryFlag_"+szServiceID).val();
                szConsigneePostcodeRequiredFlag = $("#szConsigneePostcodeRequiredFlag_"+szServiceID).attr('value');
                //alert(szConsigneePostcodeRequiredFlag+"szConsigneePostcodeRequiredFlag");
                szShipperPostcodeRequiredFlag = $("#szShipperPostcodeRequiredFlag_"+szServiceID).val();
                //alert(szShipperPostcodeRequiredFlag+"szShipperPostcodeRequiredFlag");
            }
            
        });
    }
    
    if(szBookingType=='SEND_QUOTE' || szBookingType=='CREATE_BOOKING')
    {
        if(isFormInputElementEmpty(formId,inputId))
        {		
            if(szBookingType=='SEND_QUOTE')
            {
                /*
                * In case of SEND_QUOTE we only have szFirstName, szLastName and szEmail will be required. 
                */
                if(inputId=='szFirstName' || inputId=='szLastName' || inputId=='szEmail')
                {
                    $("#"+inputId).addClass('red_border'); 
                } 
            }
            else
            {
                if(szCollectionFlag=='No' &&  inputId=='szOriginAddress')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else if(szDeliveryFlag=='No' &&  inputId=='szDestinationAddress')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else if(szShipperPostcodeRequiredFlag=='No' &&  inputId=='szOriginPostcode')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else if(szConsigneePostcodeRequiredFlag=='No' &&  inputId=='szDestinationPostcode')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else
                {
                    $("#"+inputId).addClass('red_border');
                }
            } 
        }
        else
        {
            $("#"+inputId).removeClass('red_border');
        }
    } 
}

function prefillCompanyField(cb_id)
{
    var cb_flag = $("#"+cb_id).prop('checked'); 
    if(cb_flag)
    {
        $("#iPrivateCustomer").val(1);
        var szFirstName = $("#szFirstName").val();
        var szLastName = $("#szLastName").val();
        var CustomerName = szFirstName+" "+szLastName;  
        $("#szCustomerCompanyName").val(CustomerName);
        $("#szCustomerCompanyName").attr('disabled','disabled');
        autofill_billing_address__new_rfq(1);
    } 
    else
    {
        $("#iPrivateCustomer").val(0);
        $("#szCustomerCompanyName").removeAttr('disabled');
    }
}

function buildQuickQuoteEmail(type)
{
    if(type=='SEND_QUOTE' || type=='CREATE_BOOKING')
    { 
        var idLanguage = $("#iBookingLanguage_"+type).val(); 
        //var szEmailBody = $("#szEmailBody_"+type).val(); 
        var szEmailSubject = $("#szEmailSubject_"+type).val(); 
        
        var emailbody_id = "szEmailBody_"+type;
        var szEmailBody = NiceEditorInstance.instanceById(emailbody_id).getContent();
          
        var szFirstName = $("#szFirstName").val();
        var szLastName = $("#szLastName").val();
        var szCargoDescription = $("#szCargoDescription").val();
        
        var szOriginPostcode = $("#szOriginPostcode").val();
        var szOriginCity = $("#szOriginCity").val();
        var idOriginCountry = $("#idOriginCountry").val();
        
        var szDestinationPostcode = $("#szDestinationPostcode").val();
        var szDestinationCity = $("#szDestinationCity").val();
        var idDestinationCountry = $("#idDestinationCountry").val();
 
//        var szOriginCountryName = CountryLanguageListAry[idLanguage][idOriginCountry]['szCountryName'];
//        var szDestinationCountryName = CountryLanguageListAry[idLanguage][idDestinationCountry]['szCountryName'];
        var szOriginCountryName = "";
        var szDestinationCountryName = "";
        if(idOriginCountry>0 && idLanguage>0)
        {
            szOriginCountryName = CountryLanguageListAry[idLanguage][idOriginCountry]['szCountryName'];
        } 
        if(idDestinationCountry>0 && idLanguage>0)
        {
            szDestinationCountryName = CountryLanguageListAry[idLanguage][idDestinationCountry]['szCountryName'];
        }
         
        var szFromLocation = "";
        if(szOriginPostcode!='')
        {
            szFromLocation = szOriginPostcode+ " "+szOriginCity;
        }
        else
        {
            szFromLocation = szOriginCity;
        }
        szFromLocation = szFromLocation + ", " + szOriginCountryName ;
        
        var szToLocation = "";
        if(szDestinationPostcode!='')
        {
            szToLocation = szDestinationPostcode+ " "+szDestinationCity;
        }
        else
        {
            szToLocation = szDestinationCity;
        }
        szToLocation = szToLocation + ", "+szDestinationCountryName ;
        console.log(szToLocation+"szToLocation");
        /*
        var replaceAry = new Array(); 
        replaceAry['szFirstName'] = jQuery.trim(szFirstName);
        replaceAry['szLastName'] = jQuery.trim(szLastName); 
        replaceAry['szCargoDescription'] = jQuery.trim(szCargoDescription); 
        replaceAry['szFromLocation'] = jQuery.trim(szFromLocation);
        replaceAry['szToLocation'] = jQuery.trim(szToLocation);
        */

        var replaceAry = {
            'szFirstName' : szFirstName,
            'szLastName' : szLastName,
            'szCargoDescription' : szCargoDescription,
            'szFromLocation' : szFromLocation,
            'szToLocation' : szToLocation
        };

        $.each( replaceAry, function( key, value ){  
            if(isEmpty(value) && value!='')
            {
                if(key=='szCargoDescription')
                {
                    value = ","+" "+value; 
                } 
                //console.log("Replacing: "+key+ " with "+value);
                szEmailBody = szEmailBody.replace(key, value);
                szEmailSubject = szEmailSubject.replace(key, value); 
            } 
        }); 
 
        //$("#szEmailBody_"+type).val(szEmailBody);
        NiceEditorInstance.instanceById(emailbody_id).setContent(szEmailBody);
        $("#szEmailSubject_"+type).val(szEmailSubject); 
    } 
}

function updateEmailData(idLanguage,type,field_name)
{ 
    if(type=="SEND_QUOTE")
    {
        var szEmailBody = $("#szEmailTemplateBody_"+type+"_"+idLanguage).val();
        var szEmailSubject = $("#szEmailTemplateSubject_"+type+"_"+idLanguage).val();
    
        $("#szEmailSubject_SEND_QUOTE").val(szEmailSubject);
        //$("#szEmailBody_SEND_QUOTE").val(szEmailBody);
        var emailbody_id = "szEmailBody_SEND_QUOTE";
        NiceEditorInstance.instanceById(emailbody_id).setContent(szEmailBody);
    }
    else if(type=='CREATE_BOOKING')
    {
        var szPaymentMode = '';
        var szLanguage = '';
        if(field_name=='PAYMENT_CODE')
        {
            szPaymentMode = idLanguage;
            szLanguage = $("#iBookingLanguage_CREATE_BOOKING").val();
        }
        else
        {
            szLanguage = idLanguage;
            szPaymentMode = $("#szPaymentType_CREATE_BOOKING").val();
        }
        
        var szEmailBody = $("#szEmailTemplateBody_"+type+"_"+szPaymentMode+"_"+szLanguage).val();
        var szEmailSubject = $("#szEmailTemplateSubject_"+type+"_"+szPaymentMode+"_"+szLanguage).val();
        
        $("#szEmailSubject_CREATE_BOOKING").val(szEmailSubject);
        //$("#szEmailBody_CREATE_BOOKING").val(szEmailBody);
        
        var emailbody_id = "szEmailBody_CREATE_BOOKING";
        NiceEditorInstance.instanceById(emailbody_id).setContent(szEmailBody);
    } 
    buildQuickQuoteEmail(type);
}

function check_insurance_required(formId,inputId)
{ 
    var iInsuranceCur = $("#iInsuranceIncluded").val();
    if(iInsuranceCur==1 || iInsuranceCur==3)
    {
        if(inputId=='fTotalInsuranceCostForBookingCustomerCurrency')
        {
            var fInsuranceVal = $("#fTotalInsuranceCostForBookingCustomerCurrency").val();
            fInsuranceVal = parseFloat(fInsuranceVal);
            if(fInsuranceVal>=0)
            {
                $("#"+inputId).removeClass('red_border');
            }
            else
            {
                $("#"+inputId).addClass('red_border');
            }
        }
        else
        {
            if(isFormInputElementEmpty(formId,inputId)) 
            {		
                $("#"+inputId).addClass('red_border');
            } 
        } 
    } 
}


function reload_quick_quote_email_template(booking_type,booking_id)
{ 
    var booking_language = $("#iBookingLanguage_"+booking_type).val();
    var szPaymentType = "";
    if(booking_type=='CREATE_BOOKING')
    {
        szPaymentType = $("#szPaymentType_"+booking_type).val();
        if(szPaymentType=='' || booking_language==0)
        {
            $("#szEmailSubject_CREATE_BOOKING").val('');
            var emailbody_id = "szEmailBody_CREATE_BOOKING";
            NiceEditorInstance.instanceById(emailbody_id).setContent('');
            return false;
        }
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php",{mode:'RELOAD_QUICK_QUOTE_EMAIL_TEMPLATE',booking_type:booking_type,booking_id:booking_id,booking_language:booking_language,payment_type:szPaymentType},function(result){
 
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#quick-quote-email-box-container").html(resArr[1]);
            buildQuickQuoteEmail(booking_type);
            validateQQBookingInformation();
        } 
    }); 
}


function selectDropdownByText(select_box_id,szText)
{ 
    $('#'+select_box_id+' option').each(function() {
        var text = $(this).html(); 
        if(szText==text)
        {
            $(this).attr('selected','selected');  
        }
    });
}
function isEmpty(variable) { 
    return (variable !== null && variable !== undefined);
}
function clearQuickQuotesForm()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php",{mode:'CLEAR_QUICK_QUOTE_FORM'},function(result){
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#quick_quote_main_container").html(resArr[1]); 
        } 
    });
} 
function submitQuickQuoteForm()
{
    $("#quick_quote_get_price_button").addClass('get-rates-btn-clicked');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php?mode=SUBMIT_QUICK_QUOTE_FORM",$("#quick_quote_search_form").serialize(),function(result){
        var resArr=result.split("$$$$");
        if(resArr[0]=='SUCCESS')
        {
            $("#quick_quote_search_result_container").html(resArr[1]); 
            $("#quick_quote_search_result_container").attr('style','display:block'); 
            var iScrollTopLine = parseInt(resArr[2]);
            if(iScrollTopLine==1)
            {
                $('html, body').animate({ scrollTop: ($("#quote_search_result_form").offset().top - 20) }, "100000");  
            }
            else
            {
                $('html, body').animate({ scrollTop: ($("#quick_quote_service_button_container").offset().top - 20) }, "100000");  
            } 
        }
        else if(jQuery.trim(resArr[0])=='ERROR')
        {
            $("#quick_quote_search_form_container").html(resArr[1]); 
        }
        $("#quick_quote_get_price_button").removeClass('get-rates-btn-clicked');
    });
    
} 

function toggleCargoFields(cb_id,cargo_type)
{
    if($("#"+cb_id).prop("checked"))
    {
        $("#quick_quote_cargo_container_"+cargo_type).attr('style','display:block;');
        $("#quick_quote_cargo_container_"+cargo_type).slideDown();
    }
    else
    {
        $("#quick_quote_cargo_container_"+cargo_type).attr('style','display:none;');
        $("#quick_quote_cargo_container_"+cargo_type).slideUp(); 
    }
}

function encode_string_msg_str(input_string)
{ 
   var encoded_strs='';

   url_enc=encodeURIComponent(input_string);
   encoded_strs=$.base64.encode(url_enc); 
   return encoded_strs;
}


function toggleInsuranceFields(iInsuranceIncluded,quick_quote)
{  
    if(iInsuranceIncluded==1 || iInsuranceIncluded==3) //1. Yes 3. Optional
    {
        if(quick_quote==1)
        {
            $("#idGoodsInsuranceCurrency").removeAttr('disabled');
            $("#fCargoValue").removeAttr('disabled');
        }
        else
        {
            $("#idInsuranceCurrency").removeAttr('disabled');
            $("#fTotalInsuranceCostForBookingCustomerCurrency").removeAttr('disabled');
        } 
    }
    else
    { 
        if(quick_quote==1)
        {
            $("#idGoodsInsuranceCurrency").attr('disabled','disabled');
            $("#fCargoValue").attr('disabled','disabled');
       
            $("#idGoodsInsuranceCurrency").removeClass('red_border');
            $("#fCargoValue").removeClass('red_border');
        }
        else
        {
            $("#idInsuranceCurrency").removeClass('red_border');
            $("#fTotalInsuranceCostForBookingCustomerCurrency").removeClass('red_border');
        }
    }
}

function prefill_quick_quote_addres(ret_ary,flag)
{
    var szCity = '';
    var szState = '';
    var szCountryName = '';
    var szCountryISO = '';
    var szPostcode = '';
    var idCurrency = '';
    var idCustomerCountry = "";
    var iShipperConsignee = $('input:radio[name="quickQuoteShipperConsigneeAry[iShipperConsignee]"]:checked').val();  
     
    if(Array.isArray(ret_ary))
    { 
        szPostcode = ret_ary["szPostcode"];
        szCity = ret_ary["szCity"]; 
        szCountryName = ret_ary["szCountryName"];
        szCountryISO = ret_ary["szCountryISO"];
    }
    if(flag=='FROM_COUNTRY')
    {
        var idCountry = transportecaCountryListAry[szCountryISO]['id']; 
        var iInternationDialCode = "+" + transportecaCountryListAry[szCountryISO]['iInternationDialCode']; 
        
        $("#szOriginPostcode").val(szPostcode);
        $("#szOriginCity").val(szCity); 
        $("#idOriginCountry").val(idCountry);
        //$("#idShipperDialCode").val(idCountry); 
        //$("#idShipperDialCode select option:contains("+iInternationDialCode+"\")").attr('selected', true);
        
        selectDropdownByText('idShipperDialCode',iInternationDialCode); 
        
        if(iShipperConsignee==1)
        {
            $("#szCustomerPostCode").val(szPostcode);
            $("#szCustomerCity").val(szCity);  
            $("#szCustomerCountry").val(idCountry);  
            //$("#idCustomerDialCode").val(idCountry); 
            selectDropdownByText('idCustomerDialCode',iInternationDialCode); 
            idCurrency = transportecaCountryListAry[szCountryISO]['idCurrency'];
            idCustomerCountry = idCountry;
        }  
        if(isEmpty(szCity))
        {
            $("#szOriginCountryStr").removeClass('red_border');
        }
        else
        {
            $("#szOriginCountryStr").addClass('red_border');
        } 
    }
    else if(flag=='TO_COUNTRY')
    {
        var idCountry = transportecaCountryListAry[szCountryISO]['id']; 
        var iInternationDialCode = "+" + transportecaCountryListAry[szCountryISO]['iInternationDialCode']; 
        
        $("#szDestinationPostcode").val(szPostcode);
        $("#szDestinationCity").val(szCity); 
        $("#idDestinationCountry").val(idCountry);
        //$("#idConsigneeDialCode").val(idCountry);
        selectDropdownByText('idConsigneeDialCode',iInternationDialCode); 
        
        if(iShipperConsignee==2)
        {
            $("#szCustomerPostCode").val(szPostcode);
            $("#szCustomerCity").val(szCity);  
            $("#szCustomerCountry").val(idCountry);  
            //$("#idCustomerDialCode").val(idCountry);  
            selectDropdownByText('idCustomerDialCode',iInternationDialCode); 
            
            idCurrency = transportecaCountryListAry[szCountryISO]['idCurrency'];
            idCustomerCountry = idCountry;
        } 
        if(isEmpty(szCity))
        {
            $("#szDestinationCountryStr").removeClass('red_border');
        }
        else
        {
            $("#szDestinationCountryStr").addClass('red_border');
        }  
    } 
    var dtdtradeFlag=false;
    var idOriginCountry=$("#idOriginCountry").val();
    var idDestinationCountry=$("#idDestinationCountry").val();
    var iUpdatedTermsDropDown =$("#iUpdatedTermsDropDown").val();
    if(dtdTradeStr!='' && idDestinationCountry>0 && idOriginCountry>0 && parseInt(iUpdatedTermsDropDown)==0)
    {
        from_to_country_str=idOriginCountry+"-"+idDestinationCountry;
         dtdTradeArr=dtdTradeStr.split(";");
         if(in_array(from_to_country_str,dtdTradeArr))
         {
             $('#idServiceTerms option').each(function() {
                var option_val = $(this).val(); 
                if(option_val==2 || option_val==4 || option_val==5) // 2.FOB 4.CFR 5.EXW-WTD
                {
                    $(this).hide();
                    $(this).attr('disabled','disabled');
                } 
                else
                {
                    $(this).show(); 
                    $(this).removeAttr('disabled');
                    if(option_val==1)
                    {
                        $(this).attr('selected','selected');
                        idServiceTerms_hidden = $(this).val();
                    }
                }
            });
         }
         else
         {
             $('#idServiceTerms option').each(function() {
                var option_val = $(this).val(); 
                 $(this).show(); 
                $(this).removeAttr('disabled');
                if(option_val==2)
                {
                    $(this).attr('selected','selected');
                    idServiceTerms_hidden = $(this).val();
                }
                
            });
         }
    }
    $("#iUpdatedTermsDropDown").attr('value','0');
    if(idCurrency>0)
    {
        $("#idCustomerCurrency").val(idCurrency); 
        $("#idCustomerCurrency").removeClass('red_border');
        
        $("#idGoodsInsuranceCurrency").val(idCurrency); 
        $("#idGoodsInsuranceCurrency").removeClass('red_border'); 
    }
    if(idCustomerCountry>0)
    {
        $("#idCustomerCountry").val(idCustomerCountry); 
        $("#idCustomerCountry").removeClass('red_border');
    }
    autofill_billing_address__new_rfq(1); 
    console.log("Count: "+idCountry+" Curr: "+idCurrency);
    validateQQBookingInformation();
}

function enableQQGetRatesButton(kEevent,onLoad)
{
    var requiredFieldsAry = new Array(); 
    requiredFieldsAry['0'] = 'szOriginCountryStr';
    requiredFieldsAry['1'] = 'szDestinationCountryStr';
    requiredFieldsAry['2'] = 'idServiceTerms';
    requiredFieldsAry['3'] = 'dtShipmentDate';  
    requiredFieldsAry['4'] = 'idCustomerCurrency';   
    
    var formId = 'quick_quote_search_form';
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {		 
            if(onLoad==1)
            {
                $("#"+input_fields).addClass('red_border');
            }
            error++;  
        } 
    }); 
    
    var iBreakBulk = 0;
    var iParcel = 0;
    var iPallet = 0;
    
    if($("#szShipmentType_BREAK_BULK").prop("checked"))
    {
        iBreakBulk =1;
    }
    if($("#szShipmentType_PARCEL").prop("checked"))
    {
        iParcel =1;
    }
    if($("#szShipmentType_PALLET").prop("checked"))
    {
        iPallet =1;
    } 
    
    if(iBreakBulk==0 && iParcel==0 && iPallet==0)
    { 
        $("#label_szShipmentType_BREAK_BULK").addClass('red_border'); 
        $("#label_szShipmentType_PARCEL").addClass('red_border'); 
        $("#label_szShipmentType_PALLET").addClass('red_border');  
        error++;
    } 
    else
    {
        $("#label_szShipmentType_BREAK_BULK").removeClass('red_border');
        $("#label_szShipmentType_PARCEL").removeClass('red_border');
        $("#label_szShipmentType_PALLET").removeClass('red_border');
        
        var cargo_valid_flag = validateQuickQuoteCargoDetails();
        if(cargo_valid_flag!=1)
        {
            error++;
        }
    } 
    
    if(parseInt(error)==1)
    {
        $("#quick_quote_get_price_button").unbind("click");
        $('#quick_quote_get_price_button').attr('style','opacity:1');
        $("#quick_quote_get_price_button").click(function(){ submitQuickQuoteForm(); });
        if(kEevent.which == 13)
        {
            submitQuickQuoteForm();
        } 
    }
    else
    {
        $("#quick_quote_get_price_button").unbind("click");
        $('#quick_quote_get_price_button').attr('style','opacity:0.4;');
    } 
}

function selectEdit(id,language_flag)
{
    if(language_flag==1)
    {
        id = $("#selectEditor").val();
    }

    if(id>0 && id!='')
    {
        $('#editTemp').html('<span>Edit</span>');
        $('#editTemp').removeAttr('style');
        $('#editTemp').attr('onclick','template()');
        $('#previewTemp').css('opacity','0.4');
        $('#previewTemp').removeAttr('onclick');
        $('#showdetails').html('');
        $('#Error-Log').html('');
    }
    else
    {
        $('#editTemp,#previewTemp').css('opacity','0.4');
        $('#editTemp,#previewTemp').removeAttr('onclick');
        $('#showdetails').html('');
        $('#Error-Log').html('');
    }
}
function template()
{
        var id	= $('#selectEditor').val();
        var iLanguage = $('#iLanguage').val();
        $('#previewTemp').removeAttr('style');
        $('#previewTemp').attr('onclick','previewTemplate()');
        $('#editTemp').html('<span>Save</span>');
        $('#editTemp').attr('onclick','saveTemplate()');
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_templateEmail.php",{flag:'SHOW_CONTENT',id:id,iLanguage:iLanguage},function(result){
                $('#showdetails').html(result);
        });
}
function previewTemplate()
{
    //var texts = $('#szDescription').val();
    var szSubject = $('#szSubject').val();

    var content = NiceEditorInstance.instanceById('szDescription').getContent();
    var texts = encode_string_msg_str(content);

    $.post(__JS_ONLY_SITE_BASE__+"/ajax_templateEmail.php",{flag:'SHOW_CONTENT_MESSAGE',message:texts,szSubject:szSubject},function(result){
        $('#previewText').html('<div>'+result+'</div>');
    });  
}
function saveTemplate()
{
    var serialized_form_data = $('#saveEmailTemplate').serialize(); 
    var content = NiceEditorInstance.instanceById('szDescription').getContent();
    var description = encode_string_msg_str(content);
    serialized_form_data = serialized_form_data + "&templateArr[szDescription]="+description;

    $.post(__JS_ONLY_SITE_BASE__+"/ajax_templateEmail.php",serialized_form_data,function(result){

        if(result!='')
        {
            $('#Error-Log').html(result);
        }
        else
        {
            $('#Error-Log').html('');
            $('#showdetails').html('');
            //window.location.reload(true);
            $('#editTemp').html('<span>Edit</span>');
            $('#editTemp').removeAttr('style');
            $('#editTemp').attr('onclick','template()');
            $('#previewTemp').css('opacity','0.4');
            $('#previewTemp').removeAttr('onclick');
        }
    });
}


function validateQuickQuoteCargoDetails()
{ 
    var iBreakBulk = $("#szShipmentType_BREAK_BULK").prop('checked');
    var iParcel = $("#szShipmentType_PARCEL").prop('checked');
    var iPallet = $("#szShipmentType_PALLET").prop('checked');
     
    //console.log("Bulk: "+iBreakBulk+ " Parc: "+iParcel+ " Pallet: "+iPallet);
     
    var break_bulk_error = 1;
    var parcel_error = 1;
    var pallet_error = 1;
    var formId = "quick_quote_search_form";
    
    if(iBreakBulk)
    {
        if(isFormInputElementEmpty(formId,"fTotalVolume_BREAK_BULK"))
        {		  
            break_bulk_error++;  
        } 
        if(isFormInputElementEmpty(formId,"fTotalWeight_BREAK_BULK"))
        {		  
            break_bulk_error++;  
        }
    } 
    if(iParcel)
    { 
        $.each($(".field_name_key_PARCEL"), function(field_key){
            
           var id_suffix = this.value;
           if(id_suffix!='')
           {
                var requiredFieldsAry = new Array(); 
                requiredFieldsAry['0'] = 'iLength'+id_suffix;
                requiredFieldsAry['1'] = 'iWidth'+id_suffix;
                requiredFieldsAry['2'] = 'iHeight'+id_suffix;
                requiredFieldsAry['3'] = 'iWeight'+id_suffix;
                requiredFieldsAry['4'] = 'iQuantity'+id_suffix;
                  
                $.each( requiredFieldsAry, function( index, input_fields ){
                    if(isFormInputElementEmpty(formId,input_fields))
                    {		  
                        parcel_error++;  
                    } 
                }); 
            } 
        });
    }
    
    if(iPallet)
    { 
        $.each($(".field_name_key__PALLET"), function(field_key){
            
           var id_suffix = this.value;
           if(id_suffix!='')
           {
                var requiredFieldsAry = new Array(); 
                requiredFieldsAry['0'] = 'iLength'+id_suffix;
                requiredFieldsAry['1'] = 'iWidth'+id_suffix;
                requiredFieldsAry['2'] = 'szPalletType'+id_suffix;
                requiredFieldsAry['3'] = 'iWeight'+id_suffix;
                requiredFieldsAry['4'] = 'iQuantity'+id_suffix;
                  
                $.each( requiredFieldsAry, function( index, input_fields ){
                    if(isFormInputElementEmpty(formId,input_fields))
                    {		  
                        parcel_error++;  
                    } 
                }); 
            } 
        }); 
    }
    
    if(break_bulk_error==1 && parcel_error==1 && pallet_error==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


function auto_forwarder_header_notification()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'AUTO_LOAD_FORWADER_HEADER_NOTIFICATION'},function(result){
        var result_ary = result.split('||||'); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            $("#top_messages").html(result_ary[1]);     
        }
    });
}

function update_dail_code(idCountry,szFlag)
{
    if(szFlag=='cust')
    {
        $("#idCustomerDialCode").attr('value',idCountry)
    }
    else if(szFlag=='ship')
    {
         $("#idShipperDialCode").attr('value',idCountry)
    }
    else if(szFlag=='con')
    {
         $("#idConsigneeDialCode").attr('value',idCountry)
    }
    
}

function update_ship_con(service_term)
{
    if(service_term>0)
    {
        /*
        * 1. EXW-DTD
        * 2. FOB
        * 3. DAP - Cleared
        * 4. CFR
        * 5. EXW-WTD
        * 6. DAP - No CC
        */
        var iShipperConsignee = '';
        if(service_term==1 || service_term==2 || service_term==5)
        {
            //consignee 
            $("#iShipperConsignee_2").prop("checked", true);
            iShipperConsignee = 2;
        }
        else
        {
            //Shipper
            $("#iShipperConsignee_1").prop("checked", true);
            iShipperConsignee = 1;
        } 
        $("#iUpdatedTermsDropDown").attr('value','1');
        if(iShipperConsignee==2)
        {
            var szDestinationAddress = $("#szDestinationCountryStr").val();
            checkFromAddressForwarder(szDestinationAddress,'TO_COUNTRY','','1');
        }
        else
        {
            var szOriginCountryStr = $("#szOriginCountryStr").val();
            checkFromAddressForwarder(szOriginCountryStr,'FROM_COUNTRY','','1'); 
        } 
        autofill_billing_address__new_rfq(1);
    }
}


function download_admin_self_credit_note_pdf(booking_id)
{	
    window.open(__JS_ONLY_SITE_BASE__+'/selfcreditNote/'+booking_id+'/',"_blank");
}

function download_admin_self_invoice_pdf(booking_id)
{	
    window.open(__JS_ONLY_SITE_BASE__+'/forwarderSelfInvoice/'+booking_id+'/',"_blank");
} 

function show_destination_country_dropdown(idWarehouse,idForwarder,szFromPage)
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",{idWarehouse:idWarehouse,idForwarder:idForwarder,szFromPage:szFromPage,mode:'DISPLAY_DESTINATION_COUNTRY'},function(result){
        var result_ary = result.split('||||'); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#destination_country_span").html(result_ary[1]); 
            
            $("#szDestinationCity").html('<option value="">All</option>');
            $("#szDestinationCity").attr('disabled',true);
                        
            $("#idDestinationWarehouse").html('<option value="">All</option>');
            $("#idDestinationWarehouse").attr('disabled',true);
            
            $("#obo_cc_getdata_button").unbind("click");
		
            //$("#obo_cc_getdata_button").attr('onclick','');
            $("#obo_cc_getdata_button").attr("style","opacity:0.4;"); 

            $("#obo_cc_cc_getdata_button").unbind("click");

            //$("#obo_cc_cc_getdata_button").attr('onclick','');
            $("#obo_cc_cc_getdata_button").attr("style","opacity:0.4;");
        }
    });
}

function enable_destination_country_dropdown(idWarehouse,idForwarder,szFromPage)
{
    if(szFromPage=='OBO_CUSTOM_CLEARANCE')
    {
        show_destination_country_dropdown(idWarehouse,idForwarder,szFromPage);
    }
}