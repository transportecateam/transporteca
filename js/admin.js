	
var idDivDetails = null;
var nameoFDiv = null;	
var globalTrHaulageid = null;	 
    function selectExchangeRates(id,name)
    {	
        if(id>0)
        {	
            var idDivDetails = id; 
            var nameoFDiv = name;
            $('tr').attr('style','background:#fff;color:#000;cursor:pointer;');
            $('#'+id).attr('style','background:#DBDBDB;color:#828282;cursor:pointer');
            $("#editExchangeRates").unbind("click");
            $("#editExchangeRates").click(function(){editExchangeRate(id);});
            $('#editExchangeRates').attr({'style':''});
            $("#deleteExchangeRates").unbind("click");
            $('#deleteExchangeRates').attr({'onclick':'cancelExchangeRate(0);','style':''});
            $('#editExchangeRates span').text('edit');
            $('#deleteExchangeRates span').text('cancel');
            $(".addExchangeOnFocus").unbind("click");
            $('.addExchangeOnFocus').attr('onclick','addExchangeRate();');
            $('.addExchangeOnChange').attr('onChange','addExchangeRate();');
        }
    }
	
    function editExchangeRate(id)
    {	
        if(id>0)
        {	
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",{mode:"VIEW_DETAIL",id:id},function(result){
                $("#editCurrency").html(result);
            });
            //$("#cancelExcangeRates").html('<a class="button1" onclick = "cancelExchangeRate(0)"><span>cancel</span></a>');
            $(".selectCurrency").attr('onclick','');
            $("#editExchangeRates").unbind("click");
            $("#editExchangeRates").click(function(){saveExchangeRate(id);});
            $('#editExchangeRates span').text('save');
            $('#deleteExchangeRates').attr('onclick','cancelExchangeRate(0)');
            $('#deleteExchangeRates span').text('cancel');
        }
    }
    function saveExchangeRate(id)
    {	
        if(id>=0)
        {	 
            var val=$('#SAVE_COUNTRIES').val(); 
            if(val=='SAVE_COUNTRIES')
            {
                $.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCountries.php",$('#sendExchange').serialize(),function(result){
                    var n=result.split("|||||");
                    if(jQuery.trim(n[0]))
                    {	
                        $("#error").attr('style','display:block');
                        $("#error").html(n[0]);
                    } 
                    $("#editCurrency").html(n[1]);
                    if(!jQuery.trim(n[0]))
                    {	
                        $("#error").html('');
                        $("#table").html(n[2]);
                        $('#editExchangeRates').attr('style','opacity:0.4');
                        $('#deleteExchangeRates').attr('style','opacity:0.4');
                        $('#editExchangeRates').removeAttr('onclick');
                        $('#deleteExchangeRates').removeAttr('onclick');
                        $('#editExchangeRates span').text('edit');
                        $('#deleteExchangeRates span').text('delete');
                    }
                });		
            }
            else
            {
                $.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",$('#sendExchange').serialize(),function(result){
                    var n=result.split("|||||");
                    var check = jQuery.trim(n[0]);
                    if(check)
                    {	
                        $("#error").attr('style','display:block');
                        $("#error").html(n[0]);
                    } 
                    $("#editCurrency").html(n[1]);
                    if(!check)
                    {	
                        $("#error").html('');
                        $('#cancelExcangeRates').html('');
                        $("#table").html(n[2]);
                        $('#editExchangeRates').attr('style','opacity:0.4');
                        $('#deleteExchangeRates').attr('style','opacity:0.4');
                        $('#editExchangeRates').removeAttr('onclick');
                        $('#deleteExchangeRates').removeAttr('onclick');
                        $('#editExchangeRates span').text('edit');
                        $('#deleteExchangeRates span').text('cancel');
                        $('#'+id).removeAttr('style');
                    //	$('#'+id).css({background:'#DBDBDB',color:'#828282',cursor:'pointer'});
                        selectExchangeRates(idDivDetails,nameoFDiv);
                    }
                });	
                }	
            }
	}
	function deleteExchangeRates(id)
	{	
		var id = parseInt(id);
		if(id==0)
		{
			$('#ajaxLogin').attr('style','display:none;');
			removePopupScrollClass();
		}
		if(id==1)
		{
			$('#ajaxLogin').attr('style','display:block;');
			addPopupScrollClass();
		}
		if(id==2)
		{
			$('#ajaxLogins').attr('style','display:none;');
			removePopupScrollClass();
		}	
	}
	function removeExchangeData(id)
	{	
		$('#ajaxLogin').attr('style','display:none;');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",{mode:'DELETE_RATES',id:id},function(result){
		var n = result.split('|||||');
		var check = jQuery.trim(n[0]);
		if(check)
			{
				$("#error").html(n[0]);
				$('#error').attr('style','display:block');
				$("#table").html(n[2]);
			} 
				$("#editCurrency").html(n[1]);
			if(!check)
			{	
				$('#ajaxLogin').attr('style','display:none;');
				$("#error").html('');
				$("#table").html(n[2]);
				$('#editExchangeRates span').text('edit');
				$('#editExchangeRates').attr('style','opacity:0.4');
				$('#deleteExchangeRates').attr('style','opacity:0.4');
				$('#editExchangeRates').removeAttr('onclick');
				$('#deleteExchangeRates').removeAttr('onclick');
				removePopupScrollClass();
			}
		});
	}
	function cancelExchangeRate(id)
	{
		if(id==0)
		{		var val=$('#SAVE_COUNTRIES').val();
				
			if(val=='SAVE_COUNTRIES')
			{
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCountries.php",{mode:'CANCEL_COUNTRIES',id:id},function(result){
				$('tr').attr('style','background:#fff;color:#000;cursor:pointer;');
				$('#editExchangeRates').removeAttr('onclick');
				$('#deleteExchangeRates').removeAttr('onclick');
				$('#editExchangeRates span').text('edit');
				//$('#deleteExchangeRates span').text('delete');
				$('#cancel').html('');
				$('#editExchangeRates').attr('style','opacity:0.4');
				$('#deleteExchangeRates').attr('style','opacity:0.4');
				var n = result.split('|||||');
				$('#editCurrency').html(n[0]);
				$("#table").html(n[1]);
				$('#error').attr('style','display:none;');
				});
			}
			else
			{
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",{mode:'CANCEL_DETAILS',id:id},function(result){
				$('tr').attr('style','background:#fff;color:#000;cursor:pointer;');
				$('#editExchangeRates').removeAttr('onclick');
				$('#deleteExchangeRates').removeAttr('onclick');
				$('#editExchangeRates span').text('edit');
				$('#deleteExchangeRates span').text('cancel');
				$('#editExchangeRates').attr('style','opacity:0.4');
				$('#deleteExchangeRates').attr('style','opacity:0.4');
				$('#cancelExcangeRates').html('');
				var n = result.split('|||||');
				$('#editCurrency').html(n[0]);
				$("#table").html(n[1]);
				$('#error').attr('style','display:none;');
				});
			}
		}	
	}
	function addExchangeRate()
	{	
		$('tr').attr('style','background:#fff;color:#000;cursor:pointer;');
		$('.addExchangeOnFocus').removeAttr('onFocus');
		$('.addExchangeOnChange').removeAttr('onChange');
		$('#editExchangeRates').attr('onclick','saveExchangeRate(0);');	
		$('#deleteExchangeRates').attr('onclick','cancelExchangeRate(0);');
		$('#editExchangeRates span').text('add');
		$('#deleteExchangeRates span').text('cancel');
		$('#editExchangeRates').removeAttr('style');
		$('#deleteExchangeRates').removeAttr('style');		
	}
	
	function addInsuranceRate(type)
	{
            var iAddRecord = true ;
            if(type==2)
            {
                var insuranceParentID = $("#idInsuranceParent_2").val();
                if(insuranceParentID>0)
                {
                    iAddRecord = true ;
                }
                else
                {
                    iAddRecord = false ;
                    alert("To add a sell rate please select a buy rate from left table.");
                }
            } 
            
            if(iAddRecord)
            {
                $('#insyrance_listing_table_'+type+' tr').attr('style','background:#fff;color:#000;cursor:pointer;');
                $('.addInsurateRatesFields_'+type).removeAttr('onFocus'); 
                $('#insurance_rate_add_edit_button_'+type).removeAttr('style');
                $('#insurance_rate_add_edit_button_'+type).attr('onclick','submitInsuranceRates('+type+');'); 
            } 
	} 
	
	function download_table(id)
	{
		if(id==1)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",{mode:'DOWNLOAD_DETAILS',idAdmin:id},function(result){
                           var n=result.split("||||");
                            if(jQuery.trim(n[0]) == "SUCCESS")
                            {	
                                window.location.href = __JS_ONLY_SITE_BASE__+"/exchangeRate/"+jQuery.trim(n[1])+".xlsx";
                            } 
                            else if(jQuery.trim(n[0])== "ERROR")
                            {	
                                alert("No record found for previous 90 days");
                            }
			})
			.success(function(result) { $.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",{flag:"DELETE",mode:result});});
		}
	}
	function download_country_table(id)
	{
		if(id==1)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCountries.php",{mode:'DOWNLOAD_DETAILS'},function(result){
			window.location.href = __JS_ONLY_SITE_BASE__+"/exchangeRate/"+result+".xlsx";})
			.success(function(result) { $.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",{flag:"DELETE",mode:result});});
		}
	} 
        function addslashes(str) 
        { 

            return (str + '')
              .replace(/[\\"']/g, '\\$&')
              .replace(/\u0000/g, '\\0');
        }
	function selectExchangeCountries(id,name)
	{	
		if(id>0)
		{
                    var name = addslashes(name);
                    $('tr').attr('style','background:#fff;color:#000;cursor:pointer;');
			$('#'+id).attr('style','background:#DBDBDB;color:#828282;cursor:pointer');
			$('#editExchangeRates').attr({'onclick':'editExchangeCountries('+id+',\''+name+'\');','style':''});
			//$('#deleteExchangeRates').attr({'onclick':'deleteExchangeRates('+2+');','style':''});
			//$('#editExchangeRates span').text('edit');
			//$('#deleteExchangeRates span').text('delete');
			$('#removeData').attr('onclick','removeCountry('+id+');');
			$('#view_message').text("Are you sure you wish to delete "+name);
			$('.addExchangeOnFocus').attr('onFocus','addExchangeRate();');
			$('.addExchangeOnChange').attr('onChange','addExchangeRate();');
		}
	}
	
	function editExchangeCountries(id,name)
	{	
		if(id>0)
		{	
                    var name = addslashes(name);
                    $('.countryDetals').attr('onclick','');
                    $.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCountries.php",{mode:"VIEW_DETAIL",id:id},function(result){
                    $("#editCurrency").html(result);
                    });
                    $('#editExchangeRates').attr('onclick','saveExchangeCountries('+id+',\''+name+'\');');
                    $('#editExchangeRates span').text('save');
                    $('#deleteExchangeRates').attr({'onclick':'cancelExchangeRate(0);','style':''});
                    //$('#deleteExchangeRates').attr('onclick','cancelExchangeRate(0)');
                    //$('#deleteExchangeRates span').text('cancel');
		}
	}
	function saveExchangeCountries(id,name)
	{
		if(id>=0)
		{	
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCountries.php",$('#sendExchange').serialize(),function(result){
			var n=result.split("|||||");
			if(jQuery.trim(n[0]))
			{	$("#error").attr('style','display:block;');
				$("#error").html(n[0]);
			} 
				$("#editCurrency").html(n[1]);
			if(!jQuery.trim(n[0]))
			{	
				$("#error").html('');
				$("#table").html(n[2]);
				$('#editExchangeRates').attr('style','opacity:0.4');
				$('#deleteExchangeRates').attr('style','opacity:0.4');
				$('#editExchangeRates').removeAttr('onclick');
				//$('#deleteExchangeRates').removeAttr('onclick');
				$('#editExchangeRates span').text('edit');
				//$('#deleteExchangeRates span').text('delete');
				$('#cancel').html('');
				selectExchangeCountries(id,name);
			}
			});		
		}
	}
	function removeCountry(id)
	{		
		$('#ajaxLogins').attr('style','display:none;');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCountries.php",{mode:'DELETE_COUNTRY',id:id},function(result){
		var n = result.split('|||||');
			if(n[0])
			{
				$("#error").html(n[0]);
				$("#error").attr('style','display:block');
			} 	
				$("#editCurrency").html(n[1]);
				$("#table").html(n[2]);
				if(!n[0])
				{
					hideMe();
				}
		});
	}
	
	function selectCustomerTextEditor()
	{
		var value = parseInt($("#select").attr('value')); 
		if(value>0)
		{	
			$("#preview").attr({'onclick':'PreviewText('+value+')','style':''});
			$("#save").unbind("click");
			$("#save").click(function(){saveEditTextDetails(value)});
			$('#save').attr({'style':'opacity:'});
			
			var iLanguage = $("#iLanguage").attr('value'); 
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{id:value,iLanguage:iLanguage,mode:'VIEW_DETAILS'},function(result){
					$('#viewContent').html(result);
					setup();
				});	
		}
		else
		{	
			$('#viewContent').html('<textarea name="content" id="content" style="width:100%;height:60%;" class="myTextEditor"></textarea>');
			setup();
			$('#save').attr({'onclick':'','style':'opacity:0.4'});
			$("#cancel").attr({'onclick':'','style':'opacity:0.4'});
		}
		$('#previewContent').html('');
		$('#previewContent').css('display','none');
	}
	function selectTextEditor(value)
	{
		var value = $("#select").attr('value'); 
		if(jQuery.trim(value)!="")
		{	
			
			
			var iLanguage = $("#iLanguage").attr('value'); 
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{id:value,iLanguage:iLanguage,mode:'VIEW_DETAILS'},function(result){
					$('#viewContent').html(result);
					setup();
                                        var checkId =$("#checkId").attr('value');
                                        $("#preview").attr({'onclick':'PreviewText('+checkId+')','style':''});
                                        $("#save").unbind("click");
                                        $("#save").click(function(){saveEditTextDetails(checkId)});
                                        $('#save').attr({'style':'opacity:'});
				});	
		}
		else
		{	
			$('#viewContent').html('<textarea name="content" id="content" style="width:100%;height:60%;" class="myTextEditor"></textarea>');
			setup();
			$('#save').attr({'onclick':'','style':'opacity:0.4'});
			$("#cancel").attr({'onclick':'','style':'opacity:0.4'});
		}
		$('#previewContent').html('');
		$('#previewContent').css('display','none');
	}
	function PreviewText(value)
	{	
		if(value>0)
		{		
			
				var content = tinyMCE.get('content');
				var n = content.getContent();
				n = n.replace('&nbsp;','');
				$('#previewContent').html(n);
				$('#previewContent').css('display','block');
				//$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{id:value,mode:'PREVIEW_DETAILS'},function(result){
				//	$('#previewContent').html(result);
				//	$('#previewContent').css('display','block');
				//});	
		}
	}
	function saveEditTextDetails(value)
	{
                
            var content = tinyMCE.get('content');
            var id	    = $('#checkId').val();
            var mode	= $('#checkMode').val();
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{content:content.getContent(),id:id,mode:mode},function(result){
            $("#error").html(result);
            if(jQuery.trim(result) == '')
            {
                    location.reload();
            }
            });  
			
		
	}
	function editTextEditor(mode,value)
	{	
		if(mode!='')
		{	
			var modes = parseInt(mode);
			if(modes>0)
			{
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{idExplain:mode,id:value,mode:'EDIT_EXPLAIN_SUBSECTION'},function(result){
				$('#preview_Publish').html('');
				$('#showDiv').html(result);
				setup();
				});	
			}
			else
			{
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{mode:mode,id:value},function(result){
				$('#preview_Publish').html('');
				$('#showDiv').html(result);
				setup();
				});	
			}
		}
	}
	function saveDetails(value,mode)
	{
		if(value>=0)
		{		
			 var ed = tinyMCE.get('content');
			 var heading = $('#heading').val();
			 var id= $('#checkId').val();
			 var iLanguage = $('#iLanguage').val();
	 		   ed.setProgressState(1); // Show progress
	 		   window.setTimeout(function() {
	        	ed.setProgressState(0); // Hide progress
	    		}, 1000);
	    		//var data=ed.getContent();
	    		//alert(data);
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{heading:heading,description:ed.getContent(),id:id,mode:mode,iLanguage:iLanguage},function(result){
			$("#error").html(result);
			})  
			.success(function(result) 
			{ 
				if(result == 0)
				location.reload();
			});
		}
	}
	function deleteTextData(data,id)
	{	
		//var id = parseInt(id);
		$('#ajaxLogins').attr('style','display:block;');
		addPopupScrollClass();
		$('#removeData').attr('onclick','editTextEditor("'+data+'","'+id+'")');
		$("#deleteTextEditor").unbind("click");
		$("#deleteTextEditor").click(function(){deleteTextEditor(data,id)});
	}
	function deleteTextEditor(mode,value)
	{	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{mode:mode,id:value},function(result){
		
		if(jQuery.trim(result)=='')
		{
			location.reload();
		}
		});  
	}
	function cancel_text_edit(value)
	{
		if(value==0)
		{
			location.reload();	
		}
	}
	function orderChange(id,mode_value,mode)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{mode:'CHANGE_ORDER_'+mode,id:id,mode_value:mode_value},function(result){
		$('#showUpdate').html(result);
		$('#publish').css('opacity','');
		$('#cancel').css('opacity','');	
		if(mode == 'CUSTOMER')
		{
			$('#publish').attr("onclick","preview_system('TNC_CUSTOMER','publish');");
			$('#cancel').attr("onclick","preview_system('TNC_CUSTOMER','cancel')");
			
		}
		if(mode == 'FAQ_CUSTOMER_')
		{
			$('#publish').attr("onclick","preview_system('FAQ_CUSTOMER','publish');");
			$('#cancel').attr("onclick","preview_system('FAQ_CUSTOMER','cancel')");
			
		}
		if(mode == 'FORWARDER')
		{
			$('#publish').attr("onclick","preview_system('TNC_FORWARDER','publish');");
			$('#cancel').attr("onclick","preview_system('TNC_FORWARDER','cancel')");
			
		}
		if(mode == 'FAQ')
		{
			$('#publish').attr("onclick","preview_system('FAQ_FORWARDER','publish');");
			$('#cancel').attr("onclick","preview_system('FAQ_FORWARDER','cancel')");
			
		}
		}); 
	}
	function view_body_details(value)
	{	
		if(value>0)
		{	
			addPopupScrollClass();
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_emailLog.php",{mode:'VIEW_EMAIL_CONTENT',id:value},function(result){
			$('#ajaxLogins').css('display','block');
			$('#view_message').html(result);
			});
		} 
	}
	function hideTemplates(id)
	{
		if(id==0)
		{	$('#ajaxLogins').css('display','none');
			removePopupScrollClass();
			$('#view_message').html('');
		}
	}
	function searchingData()
	{	
		var email = $("#emailSearch").val();
		var date = $("#datepicker1").val();
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_emailLog.php",{mode:"SEARCHING",email:email,date:date},function(result){	
			$('#showdetails').html(result);
			$("#1").css({'background-color' : '#006699'});
			});
		
	}
	function searchingDataPagination(page)
	{	
		var email = $("#emailSearch").val();
		var date = $("#datepicker1").val();
		$("#paging_button li").css({'background-color' : ''});
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_emailLog.php",{mode:"PAGINATION",email:email,date:date,Page:page},function(result){	
			$('#showdetails').html(result);
			$("#"+page).css({'background-color' : '#006699'});
			});
		
	}
	
	function hideMe()
	{	
		$('#ajaxLogin').attr('style','display:none');
		$('#showMessageNotdeleted').attr('style','display:none');
		$('#showMessageNotdeleted').remove();	
		$("#error").attr('style','display:none');	
		$("#error").html('');
		$('#editExchangeRates span').text('edit');
		$('#editExchangeRates').attr('style','opacity:0.4');
		$('#deleteExchangeRates').attr('style','opacity:0.4');
		$('#editExchangeRates').removeAttr('onclick');
		$('#deleteExchangeRates').removeAttr('onclick');
		removePopupScrollClass();	
	}
	
	function editMAnageVar(id)
	{
		if(id)
		{	
			addPopupScrollClass();	
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_manageVar.php",{mode:'EDIT_MANAGE_VAR',value:id},function(result){	
			var n =result.split('|||||')
			$('#ajaxLogin').html(n[0]);
			});
		}	
	}
	
	function saveEditManagevar(id)
	{
		var heading=$('#heading').val();
		var value=$('#value').val();	
			
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_manageVar.php",{mode:'SAVE_MANAGE_VAR',id:id,heading:heading,value:value},function(result){	
			var n =result.split('|||||');
			$('#error').html(n[0]);
			if(!n[0])
			{	
				$('#ajaxLogin').html('');
				$('#table').html(n[1]);
				removePopupScrollClass();	
			}
			});
	}
	function cancelEditMAnageVar(id)
	{
		if(id==0)
		{	
			removePopupScrollClass();	
			$('#ajaxLogin').html('');
		}
		if(id==2)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'forwarderComp'},function(result){	
			$('#content_body').html(result);
			});
		}
		if(id==3)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'forwarderProife'},function(result){	
			$('#content_body').html(result);
			});
		}
		if(id==4)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'Transporteca'},function(result){	
			$('#content_body').html(result);
			});
		}
	}
	function showConfirmationpopup(id,validator,idBooking)
	{
            if(validator>0)
            {
                $.post(__JS_ONLY_SITE_BASE__+"/ajax_billingDetails.php",{check:validator,id:id,mode:"SHOW_CONFIRM_POP_UP",idBooking:idBooking},function(result){
                    addPopupScrollClass();
                    $('#ajaxLogin').html(result);
                    $('#ui-datepicker-div').css('top', '301px');
                });
            }
	}
	function submit_payment(check,id,BookingId)
	{
		if(check>0)
		{	var date=$("#datepicker").val();
			var comment=$("#comment").val();
			$('#submitConfirm').attr('onclick','');
			$('#submitConfirm').css('opacity','0.4');
			$('#cancel').attr('onclick','');
			$('#cancel').css('opacity','0.4');
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_billingDetails.php",{check:check,date:date,comment:comment,id:id,mode:"CHANGE_STATUS",BookingId:BookingId},function(result){
			removePopupScrollClass();
			$("#ajaxLogin").html('');
                        var resultArr =result.split('||||');
			$("#checkDetails").html(resultArr[0]);
                        //$("#show_message").html(resultArr[1]);
                        auto_admin_header_notification();
			});
		}
	}
	
	function showDifferentProfile(flag)
	{
            $.get(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:flag},function(result){
		$("#content_body").html(result);
            });
	}
	function showDifferentProfileForCustomer(flag,idDiv)
	{
            $.get(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:flag},function(result){
		$("#content_body").html(result);
		$('#'+idDiv).css({background:'#DBDBDB',color:'#828282'});
		$('#'+idDiv).focus();
            });
	}
	function select_admin_tr(tr_id,id,mode)
	{
            var rowCount = $('#booking_table tr').length;
            if(rowCount>0)
            {
                for(x=1;x<rowCount;x++)
                {
                    new_tr_id="booking_data_"+x;
                    if(new_tr_id!=tr_id)
                    {
                        $("#"+new_tr_id).removeClass('selected-row');
                    }
                    else
                    {
                        $("#"+tr_id).addClass('selected-row');
                    }
                }
            }
            if(mode=='CREDIT_NOTE')
            {
                $("#download_invoice").attr("onclick","download_credit_note_pdf('"+id+"')");
            }
            else
            {
                $("#download_invoice").attr("onclick","download_admin_invoice_pdf('"+id+"')");
            }
            $("#download_booking").attr("onclick","download_admin_booking_pdf('"+id+"','download_booking_pdf_form')");
            $("#pdf_booking_id").attr("value",id);
            $("#download_invoice").css("opacity",'1');
            $("#download_booking").attr("style","opacity:1;");
	}
	function download_credit_note_pdf(booking_id)
	{	
		window.open(__JS_ONLY_SITE_BASE__+'/creditNote/'+booking_id+'/',"_blank");
	}
	function download_admin_invoice_pdf(booking_id)
	{	
		window.open(__JS_ONLY_SITE_BASE__+'/invoice/'+booking_id+'/',"_blank");
	}
	function download_admin_credit_note_pdf(booking_id)
	{	
		window.open(__JS_ONLY_SITE_BASE__+'/creditNote/'+booking_id+'/',"_blank");
	}
	function download_admin_booking_pdf(booking_id,form_id)
	{
		window.open(__JS_ONLY_SITE_BASE__+'/forwarderBooking/'+booking_id+'/',"_blank")
	}
        function download_admin_label_pdf(booking_id,form_id)
	{
            window.open(__JS_ONLY_SITE_BASE__+'/viewLabel/'+booking_id+'/',"_blank")
	}
	
	function download_admin_insurance_invoice_pdf(booking_id,version)
	{	
            if(version==2)
            {
                window.open(__JS_ONLY_SITE_BASE__+'/invoice/'+booking_id+'/',"_blank");
            }
            else
            {
                window.open(__JS_ONLY_SITE_BASE__+'/viewInsuranceInvoice/'+booking_id+'/',"_blank");
            } 
	}
	
	function download_admin_insurance_credit_note_pdf(booking_id,version,iCancelledBooking)
	{	
            if(version==2)
            {
                if(iCancelledBooking==1)
                {
                    window.open(__JS_ONLY_SITE_BASE__+'/creditNote/'+booking_id+'/',"_blank");
                }
                else
                {
                    window.open(__JS_ONLY_SITE_BASE__+'/viewInsuranceCreditNote/'+booking_id+'/',"_blank");
                }
            }
            else
            {
                window.open(__JS_ONLY_SITE_BASE__+'/viewInsuranceCreditNote/'+booking_id+'/',"_blank");
            } 
	}
	
	function showHide(id)
	{
		var disp = $("#"+id).css('display');
		if(disp=='block')
		{
                    $("#"+id).css('display','none');
                    $('body').removeClass('popup-body-scroll');
                    removePopupScrollClass();
		}
		else
		{
			$("#"+id).css('display','block');
			addPopupScrollClass();
		}
	}
	function open_forgot_passward_popup(div_id,redirect_url)
	{		
		$.get(__JS_ONLY_SITE_BASE__+"/forgotPassword.php",{redirect_url:redirect_url},function(result){
			$("#"+div_id).html(result);
			$("#"+div_id).attr('style','display:block;');
			$("#szEmail").focus();
			
		});
		
	}
	
	function validateInput(e,check,id)
	{	
		if(e.keyCode==13)
		{	
			submit_payment(check,id);
		}
	}
	
	function on_enter_key_press(kEvent,functionName,params)
	{
		if(kEvent.keyCode==13)
		{
			if(functionName=='validateLandingPageForm')
			{
				validateLandingPageForm('landing_page_form',params);
			}	
			if(functionName=='submit_currency_popup')
			{
				submit_currency_popup(params);
			}
			if(functionName=='forgot_passward_popup')
			{
				forgot_passward_popup('');
			}
			if(functionName=='forwarder_login_form')
			{
				$("#"+functionName).submit();
			}
			if(functionName=='add_forwarder_contact_details')
			{
				add_forwarder_contact_details();
			}		
		}
	}
	
	function addPopupScrollClass()
	{
		//$("body").addClass("popupscroll");
		//$("body").attr('style',"overflow-x:hidden;overflow-y:hidden;margin:0 17px 0 0;");
	}
	function removePopupScrollClass()
	{
		//$("body").removeClass("popupscroll");
		//$("body").attr('style',"");
	}
	function change_admin_password()
	{
		$.get(
		__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",
		{showflag:'change_pass'},
		function(result){
		$("#myprofile_info").html(result);
		});
	}
	function editAdminContactInformation()
	{	
	
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",{showflag:'contact_info'},function(result){
		$("#myprofile_info").html(result);});
	}
	function editAdminUserInformation()
	{
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",{showflag:'user_info'},function(result){
		$("#myprofile_info").html(result);});
	}
	function cancel_admin_info(value)
	{
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_myProfile.php",function(result){
		$("#myprofile_info").html(result);});
	}
	
	
	function showPaymentConfirmationpopup(validator,id,idForwarderCurrency)
	{
		if(validator>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderPayment.php",{check:validator,id:id,mode:"SHOW_CONFIRM_POP_UP",idForwarderCurrency:idForwarderCurrency},function(result){
			addPopupScrollClass();
			$('#ajaxLogin').html(result);
			$('#ui-datepicker-div').css('top', '301px');
			});
		}
	}
	
	function confirm_forwarder_billing_amount()
	{
		$('#confirm_bill_payment').removeAttr('onclick');
		$('#confirm_bill_payment').css('opacity','0.4');
		$('#cancel').removeAttr('onclick');
		$('#cancel').css('opacity','0.4');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderPayment.php",$('#paymentTransactions').serialize(),function(result){
		addPopupScrollClass();
		$('#ajaxLogin').html(result);
		$('#ui-datepicker-div').css('top', '301px');
		});
	}
	
	function update_payment_comment(idPayment,szPaymentComment,szBooking)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/updatePaymentComment.php",{id:idPayment,szComment:szPaymentComment,ref:szBooking},function(result){
			$("#invoice_comment_div").html(result);
			$("#invoice_comment_div").attr('style','display:block;');
			var value=$("#fwdReference").val();
			$("#fwdReference").focus().selectionEnd;
			//document.forms['updateInvoiceAry[szForwarderReference]'].elements['input'].focus();
		});
	}
	
	function update_payment_comment_confirm()
	{	
		var szComment  = $("#fwdReference").val();
		var idPayment = $("#idPayment").val();
		var szRef   = $("#szRef").val();
		$.post(__JS_ONLY_SITE_BASE__+"/updatePaymentComment.php",{szComment:szComment,idPayment:idPayment,updateInvoiceAry:szRef},function(result){
			result_ary = result.split("||||");
			//alert(result_ary);
			if(result_ary[0]=='SUCCESS')
			{
				$("#payment_comment_"+result_ary[1]).html(result_ary[2]);
				$("#invoice_comment_div").attr('style','display:none;');
				$("#update_comment_link_"+result_ary[1]).attr('onclick','');
				$("#update_comment_link_"+result_ary[1]).attr('onclick','update_payment_comment("'+idPayment+'","'+szComment+'","'+szRef+'")');
			}
			else
			{
				$("#invoice_comment_div").html(result_ary[1]);
				$("#invoice_comment_div").attr('style','display:block;');
			}	
			
		});
	}
	
	
	function select_unselect_forwarder_payment(ftotal,referralfee,idvalue,iDebitCredit,bookingLabelFee,handlingFee,handlingFeeVat)
	{
		var grandtotal  = $("#grandtotal").val();
		var farwordertotal  = $("#farwordertotal").val();
		var farwordertotal1  = $("#farwordertotal1").val();
		var totalReferralFee  = $("#totalReferralFee").val();
		var uploadServiceAmount  = $("#uploadServiceAmount").val();
                var totalcourierLabelFee =$("#totalcourierLabelFee").val();
                var totalBookingHandlingFee =$("#totalBookingHandlingFee").val();
                var totalhandlingFeeVat =$("#handlingFeeVat").val();
                console.log("Total Handling: "+totalBookingHandlingFee);
                
		var divid="transfer_id_"+idvalue;
		//alert(uploadServiceAmount);
		var szCurrency=$("#szCurrencyName").attr('value');
		
		//alert("g"+grandtotal);
		//alert("ft"+farwordertotal);
		//alert("ft1"+ftotal);
		if(iDebitCredit=='6' || iDebitCredit=='7')
		{
			//alert('hi');
			if((document.getElementById(divid).checked!=true))
			{
				var newfarwordertotal=parseFloat(farwordertotal)+parseFloat(ftotal);
				
				var newtotalReferralFee=parseFloat(totalReferralFee)+parseFloat(referralfee);
                              
                                var newtotalLabelFee=parseFloat(totalcourierLabelFee)+parseFloat(bookingLabelFee);
                                var newTotalBookingHandlingFee = parseFloat(totalBookingHandlingFee)+parseFloat(handlingFee); 
                                
                                var newTotalBookingHandlingFeeVat = parseFloat(totalhandlingFeeVat)+parseFloat(handlingFeeVat);
                                
				var newbalancetotal=parseFloat(grandtotal)-((parseFloat(newfarwordertotal)+parseFloat(newtotalReferralFee)+parseFloat(newtotalLabelFee)+parseFloat(newTotalBookingHandlingFee)));
				//alert(newbalancetotal);
				
				var newbalancetotal1=parseFloat(newfarwordertotal)+parseFloat(uploadServiceAmount);
				if(parseFloat(newbalancetotal)>='0.00')
				{
					var new_balance_total=newbalancetotal.toFixed(2);
					//alert('uncheck_cancel_pos');
					var new_text=szCurrency+" "+number_format(new_balance_total,'2','.',',');
				}
				else
				{
					//alert('uncheck_cancel_neg');
					var new_balance_total=newbalancetotal.toFixed(2);
					var newbalance_total=''+new_balance_total+'';
					var newbalance_total = newbalance_total.replace('-','');
					
					var new_text="("+szCurrency+" "+number_format(newbalance_total,'2','.',',')+")";
				}
				
				$("#account_balance_cancel").html(new_text);
				$("#handlingFeeVat").attr('value',newTotalBookingHandlingFeeVat);
				var formated_value = number_format(newfarwordertotal);
			
				
				$("#farwordertotal").attr('value',newfarwordertotal.toFixed(2));
				$("#totalReferralFee").attr('value',newtotalReferralFee.toFixed(2));
			//	$("#forwarder_total").html(number_format(newfarwordertotal));
				if(parseFloat(newtotalReferralFee)>='0.00')
				{
					var text_referralfee=szCurrency+" "+number_format(newtotalReferralFee,'2','.',',');
				}
				else
				{
					var newtotalReferralFee=''+newtotalReferralFee+'';
					var newtotalReferralFee = newtotalReferralFee.replace('-','');
					var text_referralfee="("+szCurrency+" "+ number_format(newtotalReferralFee,'2','.',',')+")";
				}
                                console.log(" Label Fee "+newtotalLabelFee);
                                 $("#totalcourierLabelFee").attr('value',newtotalLabelFee.toFixed(2));
                                if(parseFloat(newtotalLabelFee)>='0.00')
				{
                                    var text_labelfee=szCurrency+" "+number_format(newtotalLabelFee,'2','.',',');
				}
				else
				{
					var newtotalLabelFee=''+newtotalLabelFee+'';
					var newtotalLabelFee = newtotalLabelFee.replace('-','');
					var text_labelfee="("+szCurrency+" "+ number_format(newtotalLabelFee,'2','.',',')+")";
				} 
                                $("#courier_label_fee").html(text_labelfee);
                                 
                                 
                                $("#totalBookingHandlingFee").attr('value',newTotalBookingHandlingFee.toFixed(2));
                                if(parseFloat(newTotalBookingHandlingFee)>='0.00')
				{
                                    var text_handling_fee=szCurrency+" "+number_format(newTotalBookingHandlingFee,'2','.',',');
				}
				else
				{
                                    var newTotalBookingHandlingFee=''+newTotalBookingHandlingFee+'';
                                    var newTotalBookingHandlingFee = newTotalBookingHandlingFee.replace('-','');
                                    var text_handling_fee="("+szCurrency+" "+ number_format(newTotalBookingHandlingFee,'2','.',',')+")";
				} 
                                $("#booking_handling_fee").html(text_handling_fee);
                                 
	
				//$("#forwarder_total").html(number_format(newfarwordertotal));
				$("#referral_fee").html(text_referralfee);
				//$("#forwarder_total1").html(number_format(newfarwordertotal));
				
				if(parseFloat(newbalancetotal1)>='0.00')
				{
					var text_forwarder1=szCurrency+" "+number_format(newbalancetotal1,'2','.',',');
				}
				else
				{
					var newbalancetotal1=''+newbalancetotal1+'';
					var newbalancetotal1 = newbalancetotal1.replace('-','');
					var text_forwarder1="("+szCurrency+" "+ number_format(newbalancetotal1,'2','.',',')+")";
				}
				
				if(parseFloat(newfarwordertotal)>='0.00')
				{
					var text_forwarder=szCurrency+" "+number_format(newfarwordertotal,'2','.',',');
				}
				else
				{
					var newfarwordertotal=''+newfarwordertotal+'';
					var newfarwordertotal = newfarwordertotal.replace('-','');
					var text_forwarder="("+szCurrency+" "+ number_format(newfarwordertotal,'2','.',',')+")";
				}
				$("#forwarder_total1").html(text_forwarder1);
				$("#forwarder_total").html(text_forwarder);
							
			}
			else
			{
				//alert(ftotal);
				//alert(farwordertotal);
				var newfarwordertotal=parseFloat(farwordertotal)-parseFloat(ftotal);
				
				var newtotalReferralFee=parseFloat(totalReferralFee)-parseFloat(referralfee);
				
                                console.log(" Label Fee "+newtotalLabelFee+ " Label Fee: "+bookingLabelFee);
                                var newtotalLabelFee=parseFloat(totalcourierLabelFee)-parseFloat(bookingLabelFee); 
                                
                                var newTotalBookingHandlingFee = parseFloat(totalBookingHandlingFee) - parseFloat(handlingFee); 
                                
                                var newTotalBookingHandlingFeeVat = parseFloat(totalhandlingFeeVat)- parseFloat(handlingFeeVat);
                               
                                var newbalancetotal=parseFloat(grandtotal)-(parseFloat(newfarwordertotal)+parseFloat(newtotalReferralFee)+parseFloat(newtotalLabelFee)+parseFloat(newTotalBookingHandlingFee));
				
				
				var newbalancetotal1=parseFloat(newfarwordertotal)+parseFloat(uploadServiceAmount);
				
				//alert("ft2"+newfarwordertotal);
				//alert("ft3"+newtotalReferralFee);
				//alert("new"+newbalancetotal);
				//alert(newbalancetotal);
				if(parseFloat(newbalancetotal)>='0.00')
				{
					//alert('check_cancel_pos');
					var new_balance_total=newbalancetotal.toFixed(2);
					var new_text=szCurrency+" "+number_format(new_balance_total,'2','.',',');
				}
				else
				{
					var new_balance_total=newbalancetotal.toFixed(2);
					//alert('check_cancel_neg');
					var newbalance_total=''+new_balance_total+'';
					var newbalance_total = newbalance_total.replace('-','');
					//alert(newbalance_total);
					var new_text="("+szCurrency+" "+number_format(newbalance_total,'2','.',',')+")";
				}
				$("#account_balance_cancel").html(new_text);
                                $("#handlingFeeVat").attr('value',newTotalBookingHandlingFeeVat);
				$("#farwordertotal").attr('value',newfarwordertotal.toFixed(2));
				$("#totalReferralFee").attr('value',newtotalReferralFee.toFixed(2));
				
				
				if(parseFloat(newtotalReferralFee)>='0.00')
				{
					var text_referralfee=szCurrency+" "+number_format(newtotalReferralFee,'2','.',',');
				}
				else
				{
					var newtotalReferralFee=''+newtotalReferralFee+'';
					var newtotalReferralFee = newtotalReferralFee.replace('-','');
					var text_referralfee="("+szCurrency+" "+ number_format(newtotalReferralFee,'2','.',',')+")";
				}
                                console.log(" Label Fee "+newtotalLabelFee);
                                 $("#totalcourierLabelFee").attr('value',newtotalLabelFee.toFixed(2));
                                if(parseFloat(newtotalLabelFee)>='0.00')
				{
					var text_labelfee=szCurrency+" "+number_format(newtotalLabelFee,'2','.',',');
				}
				else
				{
					var newtotalLabelFee=''+newtotalLabelFee+'';
					var newtotalLabelFee = newtotalLabelFee.replace('-','');
					var text_labelfee="("+szCurrency+" "+ number_format(newtotalLabelFee,'2','.',',')+")";
				}  
                                $("#courier_label_fee").html(text_labelfee); 
                                
                                
                                $("#totalBookingHandlingFee").attr('value',newTotalBookingHandlingFee.toFixed(2));
                                if(parseFloat(newTotalBookingHandlingFee)>='0.00')
				{
                                    var booking_handling_fee = szCurrency+" "+number_format(newTotalBookingHandlingFee,'2','.',',');
				}
				else
				{
                                    var newTotalBookingHandlingFee=''+newTotalBookingHandlingFee+'';
                                    var newTotalBookingHandlingFee = newTotalBookingHandlingFee.replace('-','');
                                    var booking_handling_fee = "("+szCurrency+" "+ number_format(newTotalBookingHandlingFee,'2','.',',')+")";
				}  
                                $("#booking_handling_fee").html(booking_handling_fee);  
	
				//$("#forwarder_total").html(number_format(newfarwordertotal));
				$("#referral_fee").html(text_referralfee);
				$("#account_balance_cancel").html(new_text);
				//$("#forwarder_total1").html(number_format(newfarwordertotal));
				
				if(parseFloat(newbalancetotal1)>='0.00')
				{
					var text_forwarder1=szCurrency+" "+number_format(newbalancetotal1,'2','.',',');
				}
				else
				{
					var newbalancetotal1=''+newbalancetotal1+'';
					var newbalancetotal1 = newbalancetotal1.replace('-','');
					var text_forwarder1="("+szCurrency+" "+ number_format(newbalancetotal1,'2','.',',')+")";
				}
				
				if(parseFloat(newfarwordertotal)>='0.00')
				{
					var text_forwarder=szCurrency+" "+number_format(newfarwordertotal,'2','.',',');
				}
				else
				{
					var newfarwordertotal=''+newfarwordertotal+'';
					var newfarwordertotal = newfarwordertotal.replace('-','');
					var text_forwarder="("+szCurrency+" "+ number_format(newfarwordertotal,'2','.',',')+")";
				}
				$("#forwarder_total1").html(text_forwarder1);
				$("#forwarder_total").html(text_forwarder);
			}
			var checkflag=false;
			$('#confirm_bill_payment').attr('onclick','');
			$("#confirm_bill_payment").attr("style","opacity:0.4;");
			var totalcheckbox  = $("#totalcheckbox").val();
			if(parseInt(totalcheckbox)>0)
			{
                            for(var i=1;i<=totalcheckbox;++i)
                            {
                                var newdivid="transfer_id_"+i;
                                if(document.getElementById(newdivid).checked==true)
                                {
                                        checkflag=true;
                                        break;
                                }
                            }
			}
			if(checkflag)
			{
				$('#confirm_bill_payment').attr('onclick','confirm_forwarder_billing_amount()');
				$("#confirm_bill_payment").attr("style","opacity:1;");
			}
			
			var farwordertotal  = $("#farwordertotal").val();
			
			if(parseInt(farwordertotal)>0)
			{
				$('#confirm_bill_payment').removeAttr('onclick');
				$('#confirm_bill_payment').attr('onclick','confirm_forwarder_billing_amount()');
				$("#confirm_bill_payment").attr("style","opacity:1;");
			}
			else
			{
				$('#confirm_bill_payment').removeAttr('onclick');
				//$('#confirm_bill_payment').attr('onclick','confirm_forwarder_billing_amount()');
				$("#confirm_bill_payment").attr("style","opacity:0.4");
			}
			
		}
		else
		{
			if((document.getElementById(divid).checked==true))
			{
				//alert("g"+grandtotal);
				//alert("ft"+farwordertotal);
				//alert("ft1"+ftotal);
				var newfarwordertotal=parseFloat(farwordertotal)+parseFloat(ftotal);
				//alert("ft2"+newfarwordertotal);
				var newtotalReferralFee=parseFloat(totalReferralFee)+parseFloat(referralfee);
                                
                                 
                                var newtotalLabelFee=parseFloat(totalcourierLabelFee)+parseFloat(bookingLabelFee);
                                 
                                var newTotalBookingHandlingFee = parseFloat(totalBookingHandlingFee) + parseFloat(handlingFee); 
                               
                                var newTotalBookingHandlingFeeVat = parseFloat(totalhandlingFeeVat)+parseFloat(handlingFeeVat);
                                
				//alert("ft3"+newtotalReferralFee);
				var newbalancetotal=parseFloat(grandtotal)-((parseFloat(newfarwordertotal)+parseFloat(newtotalReferralFee)+parseFloat(newtotalLabelFee)+parseFloat(newTotalBookingHandlingFee)));
				//alert(newbalancetotal);
				var newbalancetotal1=parseFloat(newfarwordertotal)+parseFloat(uploadServiceAmount);
				
				//alert(newbalancetotal);
				if(parseFloat(newbalancetotal)>='0.00')
				{
					var new_balance_total=newbalancetotal.toFixed(2);
					var new_text=szCurrency+" "+number_format(new_balance_total,'2','.',',');
				}
				else
				{
					//alert('check_neg');
					var new_balance_total=newbalancetotal.toFixed(2);
					var newbalance_total=''+new_balance_total+'';
					var newbalance_total = newbalance_total.replace('-','');
					var new_text="("+szCurrency+" "+number_format(newbalance_total,'2','.',',')+")";
				}			
				
				
				//var formated_value = number_for(newfarwordertotal);
				$("#handlingFeeVat").attr('value',newTotalBookingHandlingFeeVat);
				$("#farwordertotal").attr('value',newfarwordertotal.toFixed(2));
				$("#totalReferralFee").attr('value',newtotalReferralFee.toFixed(2));
				
				if(parseFloat(newtotalReferralFee)>='0.00')
				{
					var text_referralfee=szCurrency+" "+number_format(newtotalReferralFee,'2','.',',');
				}
				else
				{
					var newtotalReferralFee=''+newtotalReferralFee+'';
					var newtotalReferralFee = newtotalReferralFee.replace('-','');
					var text_referralfee="("+szCurrency+" "+ number_format(newtotalReferralFee,'2','.',',')+")";
				}
                                console.log(" Label Fee "+newtotalLabelFee);
                                $("#totalcourierLabelFee").attr('value',newtotalLabelFee.toFixed(2));
                                if(parseFloat(newtotalLabelFee)>='0.00')
				{
					var text_labelfee=szCurrency+" "+number_format(newtotalLabelFee,'2','.',',');
				}
				else
				{
					var newtotalLabelFee=''+newtotalLabelFee+'';
					var newtotalLabelFee = newtotalLabelFee.replace('-','');
					var text_labelfee="("+szCurrency+" "+ number_format(newtotalLabelFee,'2','.',',')+")";
				}
                                
                                $("#totalBookingHandlingFee").attr('value',newTotalBookingHandlingFee.toFixed(2));
                                if(parseFloat(newTotalBookingHandlingFee)>='0.00')
				{
                                    var text_handling_fee = szCurrency+" "+number_format(newTotalBookingHandlingFee,'2','.',',');
				}
				else
				{
                                    var newTotalBookingHandlingFee=''+newTotalBookingHandlingFee+'';
                                    var newTotalBookingHandlingFee = newTotalBookingHandlingFee.replace('-','');
                                    var text_handling_fee = "("+szCurrency+" "+ number_format(newTotalBookingHandlingFee,'2','.',',')+")";
				}
	
				//$("#forwarder_total").html(number_format(newfarwordertotal));
				$("#referral_fee").html(text_referralfee);
				$("#account_balance_cancel").html(new_text);
                                $("#courier_label_fee").html(text_labelfee);
                                $("#booking_handling_fee").html(text_handling_fee);
                                 
                                
				//alert(newbalancetotal1);
				if(parseFloat(newbalancetotal1)>='0.00')
				{
					var text_forwarder1=szCurrency+" "+number_format(newbalancetotal1,'2','.',',');
				}
				else
				{
					var newbalancetotal1=''+newbalancetotal1+'';
					var newbalancetotal1 = newbalancetotal1.replace('-','');
					var text_forwarder1="("+szCurrency+" "+ number_format(newbalancetotal1,'2','.',',')+")";
				}
				//alert(text_forwarder1);
				if(parseFloat(newfarwordertotal)>='0.00')
				{
					var text_forwarder=szCurrency+" "+number_format(newfarwordertotal,'2','.',',');
				}
				else
				{
					var newfarwordertotal=''+newfarwordertotal+'';
					var newfarwordertotal = newfarwordertotal.replace('-','');
					var text_forwarder="("+szCurrency+" "+ number_format(newfarwordertotal,'2','.',',')+")";
				}
				$("#forwarder_total1").html(text_forwarder1);
				$("#forwarder_total").html(text_forwarder);		
			}
			else
			{
				//alert(grandtotal);
				var newfarwordertotal=parseFloat(farwordertotal)-parseFloat(ftotal);
				
				var newtotalReferralFee=parseFloat(totalReferralFee)-parseFloat(referralfee);
                                
                                var newTotalBookingHandlingFeeVat = parseFloat(totalhandlingFeeVat)- parseFloat(handlingFeeVat);
                                
                                var newtotalLabelFee=parseFloat(totalcourierLabelFee)-parseFloat(bookingLabelFee);
                                var newTotalBookingHandlingFee = parseFloat(totalBookingHandlingFee) - parseFloat(handlingFee); 
                               
				var newbalancetotal=parseFloat(grandtotal)-(parseFloat(newfarwordertotal)+parseFloat(newtotalReferralFee)+parseFloat(newtotalLabelFee)+parseFloat(newTotalBookingHandlingFee));
				
				var newbalancetotal1=parseFloat(newfarwordertotal)+parseFloat(uploadServiceAmount);
				//alert(newbalancetotal);
				
				if(parseFloat(newbalancetotal)>='0.00')
				{
					//alert('uncheck_pos');
					var new_balance_total=newbalancetotal.toFixed(2);
					//alert(new_balance_total);
					var new_text=szCurrency+" "+number_format(new_balance_total,'2','.',',');
				}
				else
				{
					//alert('uncheck_neg');
					var new_balance_total=newbalancetotal.toFixed(2);
					var newbalance_total=''+new_balance_total+'';
					var newbalance_total = newbalance_total.replace('-','');
					var new_text="("+szCurrency+" "+number_format(parseFloat(newbalance_total),'2','.',',')+")";
				}			
				
				$("#handlingFeeVat").attr('value',newTotalBookingHandlingFeeVat);
				console.log(" Label Fee "+newtotalLabelFee);
				$("#farwordertotal").attr('value',newfarwordertotal.toFixed(2));
				$("#totalReferralFee").attr('value',newtotalReferralFee.toFixed(2));
                                $("#totalcourierLabelFee").attr('value',newtotalLabelFee.toFixed(2));
                                
                                
                                if(parseFloat(newtotalReferralFee)>='0.00')
				{
					var text_referralfee=szCurrency+" "+number_format(newtotalReferralFee,'2','.',',');
				}
				else
				{
					var newtotalReferralFee=''+newtotalReferralFee+'';
					var newtotalReferralFee = newtotalReferralFee.replace('-','');
					var text_referralfee="("+szCurrency+" "+ number_format(newtotalReferralFee,'2','.',',')+")";
				}
	
				//$("#forwarder_total").html(number_format(newfarwordertotal));
				if(parseFloat(newtotalLabelFee)>='0.00')
				{
					var text_labelfee=szCurrency+" "+number_format(newtotalLabelFee,'2','.',',');
				}
				else
				{
					var newtotalLabelFee=''+newtotalLabelFee+'';
					var newtotalLabelFee = newtotalLabelFee.replace('-','');
					var text_labelfee="("+szCurrency+" "+ number_format(newtotalLabelFee,'2','.',',')+")";
				}
                                
                                $("#totalBookingHandlingFee").attr('value',newTotalBookingHandlingFee.toFixed(2));
                                if(parseFloat(newTotalBookingHandlingFee)>='0.00')
				{
                                    var text_handling_fee = szCurrency+" "+number_format(newTotalBookingHandlingFee,'2','.',',');
				}
				else
				{
                                    var newTotalBookingHandlingFee=''+newTotalBookingHandlingFee+'';
                                    var newTotalBookingHandlingFee = newTotalBookingHandlingFee.replace('-','');
                                    var text_handling_fee = "("+szCurrency+" "+ number_format(newTotalBookingHandlingFee,'2','.',',')+")";
				}
				$("#booking_handling_fee").html(text_handling_fee);
	
				//$("#forwarder_total").html(number_format(newfarwordertotal));
				$("#referral_fee").html(text_referralfee);
				$("#account_balance_cancel").html(new_text);
                                $("#courier_label_fee").html(text_labelfee);
				//$("#forwarder_total1").html(number_format(newfarwordertotal));
				//alert(newbalancetotal1);
				if(parseFloat(newbalancetotal1)>='0.00')
				{
					var text_forwarder1=szCurrency+" "+number_format(newbalancetotal1,'2','.',',');
				}
				else
				{
					var newbalancetotal1=''+newbalancetotal1+'';
					var newbalancetotal1 = newbalancetotal1.replace('-','');
					var text_forwarder1="("+szCurrency+" "+ number_format(newbalancetotal1,'2','.',',')+")";
				}
				//alert(text_forwarder1);
				if(parseFloat(newfarwordertotal)>='0.00')
				{
					var text_forwarder=szCurrency+" "+number_format(newfarwordertotal,'2','.',',');
				}
				else
				{
					var newfarwordertotal=''+newfarwordertotal+'';
					var newfarwordertotal = newfarwordertotal.replace('-','');
					var text_forwarder="("+szCurrency+" "+ number_format(newfarwordertotal,'2','.',',')+")";
				}
				//alert(text_forwarder);
				$("#forwarder_total1").html(text_forwarder1);
				$("#forwarder_total").html(text_forwarder);
			}
			var checkflag=false;
			$('#confirm_bill_payment').attr('onclick','');
			$("#confirm_bill_payment").attr("style","opacity:0.4;");
			var totalcheckbox  = $("#totalcheckbox").val();
			if(parseInt(totalcheckbox)>0)
			{
				for(var i=1;i<=totalcheckbox;++i)
				{
					var newdivid="transfer_id_"+i;
					if(document.getElementById(newdivid).checked==true)
					{
						checkflag=true;
						break;
					}
				}
			}
			if(checkflag)
			{
				$('#confirm_bill_payment').attr('onclick','confirm_forwarder_billing_amount()');
				$("#confirm_bill_payment").attr("style","opacity:1;");
			}
			
			var farwordertotal  = $("#farwordertotal").val();
			//alert(farwordertotal);
			if(parseInt(farwordertotal)>0)
			{
				$('#confirm_bill_payment').removeAttr('onclick');
				$('#confirm_bill_payment').attr('onclick','confirm_forwarder_billing_amount()');
				$("#confirm_bill_payment").attr("style","opacity:1;");
			}
			else
			{
				$('#confirm_bill_payment').removeAttr('onclick');
				//$('#confirm_bill_payment').attr('onclick','confirm_forwarder_billing_amount()');
				$("#confirm_bill_payment").attr("style","opacity:0.4");
			}
		}
	}
	
	
	function confirm_referral_payment()
	{
            $('#confirm_referral_payment_admin').removeAttr('onclick');
            $('#confirm_referral_payment_admin').css('opacity','0.4');
            $('#cancel_referral_payment_admin').removeAttr('onclick');
            $('#cancel_referral_payment_admin').css('opacity','0.4');
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderPayment.php",$('#forwarderPayment').serialize(),function(result){

                var result_ary = result.split("||||"); 
                if(result_ary[0]=='DATE_ERROR')
                {
                    $("#dtPaymentScheduled").addClass("red_border");

                    $('#confirm_referral_payment_admin').attr('onclick','confirm_referral_payment()');
                    $('#confirm_referral_payment_admin').css('opacity','1');

                    $('#cancel_referral_payment_admin').attr('onclick','cancelEditMAnageVar(0)');
                    $('#cancel_referral_payment_admin').css('opacity','1'); 
                }
                else if(result_ary[0]=='SUCCESS_REDIRECT')
                {
                    redirect_url(result_ary[1]);
                }
                else
                {
                    $('#ajaxLogin').html(result);
                }  
            });
	}
	function viewNoticeTransactionDetails(tr_id,id,pass,mode,iInvoiceType)
	{
		var rowCount = $('#booking_table tr').length;
		if(rowCount>0)
		{
                    for(x=1;x<rowCount;x++)
                    {
                        new_tr_id="booking_data_"+x;
                        if(new_tr_id!=tr_id)
                        {
                            $("#"+new_tr_id).attr('style','');
                        }
                        else
                        {
                            $("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
                        }
                    }
		} 
                
                if(iInvoiceType==1)
                {
                    $("#view_notice").unbind("click");
                    $("#view_notice").attr('style','opacity:1;float:right;');
                    $("#view_notice").click(function(){ download_admin_invoice_pdf(id,pass,mode) }); 
                    //Customer Invoice
                    //$("#view_notice").attr("onclick","download_admin_invoice_pdf('"+id+"','"+pass+"','"+mode+"')");
                    //$("#view_notice").css("opacity",'1');
                }
                else if(iInvoiceType==2)
                {
                    $("#view_notice").unbind("click");
                    $("#view_notice").attr('style','opacity:1;float:right;');
                    $("#view_notice").click(function(){ download_credit_note_pdf(id,pass,mode) });
                    
                    //Customer Credit Note
                    //$("#view_notice").attr("onclick","download_credit_note_pdf('"+id+"','"+pass+"','"+mode+"')");
                    //$("#view_notice").css("opacity",'1');
                }
                else
                {
                    $("#view_notice").unbind("click");
                    $("#view_notice").attr('style','opacity:1;float:right;');
                    $("#view_notice").click(function(){ view_forwarder_notice(id,pass,mode) });
                    
                    //$("#view_notice").attr("onclick","view_forwarder_notice('"+id+"','"+pass+"','"+mode+"')");
                    //$("#view_notice").css("opacity",'1');
                }
		
	}
	function view_forwarder_notice(id,pass,mode)
	{	
		if(mode==1)
		window.open(__JS_ONLY_SITE_BASE__+"/forwarderTransfer/"+pass+"/"+id+"/","_blank");
		if(mode==2)
		window.open(__JS_ONLY_SITE_BASE__+"/invoice/"+pass+"/"+id+"/","_blank");
	}
	function selectAdminBillingSearchDetails()
	{	
		id = $("#idForwarder").val();
		mode = $("#forwarder_billing_search_form").val();
		var forwarderName = $("#idForwarder option:selected").text();
		$("#nameForwarder").html(forwarderName);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_transactionHistory.php",{mode:mode,id:id},function(result){
		n = result.split("|||||");
		$("#date_change").html(n[0]);
		$("#content").html(n[1]);
		});
	}
	
	function confirm_forwarder_transfer_due(id,batchno,name,idForwarder)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderTransfer.php",{id:id,batchno:batchno,forwardername:name,idForwarder:idForwarder},function(result){
			$("#ajaxLogin").html(result);
		});
	}
	
	
	function confirm_forwarder_payment_transfer(id,batchno,idForwarder)
	{ 
            $("#cancel_payment_transfer").attr("style","opacity:0.4;");
            $("#cancel_payment_transfer").attr("onclick","");

            $("#confirm_payment_transfer").attr("style","opacity:0.4;");
            $("#confirm_payment_transfer").attr("onclick","");

            $.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderTransfer.php",{id:id,batchno:batchno,idForwarder:idForwarder,mode:'confirm'},function(result){
                    $("#ajaxLogin").html(result);
            });
	}
	function changeManagementActiveStatus(id,status,mode,tr_id,investor_flag)
	{
            if(mode==0)
            {	
                var rowCount = $('#management tr').length;
                
                if(rowCount>0)
                {
                    for(x=1;x<rowCount;x++)
                    {
                        new_tr_id="admin"+x; 
                        if(new_tr_id!=tr_id)
                        {
                            $("#"+new_tr_id).removeClass('selected-row');
                        }
                        else
                        {
                            $("#"+tr_id).addClass('selected-row');
                        }
                    }
                }
                var status = parseInt(status);
                $("#changeStatus").css("opacity",'1');
                if(status==0)
                {
                    $("#changeStatus").html('<span>Active</span>');
                }
                if(status==1)
                {
                    $("#changeStatus").html('<span>Inactive</span>');
                }
                
                if(investor_flag==1)
                {
                    $("#new_admin").attr("onclick","editAdminDetails('"+id+"','EDIT_TRANSPORTECA_INVESTOR_DETAILS')");
                    $("#new_admin").html("<span>Edit</span>");
                    $("#changeStatus").attr("onclick","changeStatusAdmin('"+id+"','CONFIRM_TRANSPORTECA_INVESTOR_STATUS')");
                }
                else
                {
                    $("#new_admin").attr("onclick","editAdminDetails('"+id+"','EDIT_TRANSPORTECA_ADMIN_DETAILS')");
                    $("#new_admin").html("<span>Edit</span>");
                    $("#changeStatus").attr("onclick","changeStatusAdmin('"+id+"','CONFIRM_TRANSPORTECA_STATUS','"+status+"')");
                } 
            }
	}
	function editAdminDetails(id,mode)
	{
		var id = parseInt(id);
		if(id>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:mode},function(result){
				$("#ajaxLogin").html(result);
			});
		}
	}
	function editAdminProfileDetails()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",$("#createNewAdminProfile").serialize(),function(result){
			var n = result.split('|||||');
			var check = jQuery.trim(n[0]);
			if(check=='ERROR')
			{
				$('#Error').html("");
				$('#Error').html(n[1]);
			}
			if(check=='SUCCESS')
			{
				$('#ajaxLogin').html('');
				$('#content_body').html(n[1]);
			}
		});
	}
	function changeStatusAdmin(id,flag,status)
	{
            var id=parseInt(id);
            if(id>0)
            {
                $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:flag,status:status},function(result){
                    if(result!='' && (flag=='CONFIRM_TRANSPORTECA_STATUS' || flag=='CONFIRM_TRANSPORTECA_INVESTOR_STATUS'))
                    {
                        $("#ajaxLogin").html(result);
                    }
                    if(result!='' && (flag=='CHANGE_TRANSPORTECA_STATUS' || flag=='CHANGE_TRANSPORTECA_INVESTOR_STATUS'))
                    {	
                        $("#ajaxLogin").html('');
                        $("#content_body").html(result);
                    }
                });	
            }	
	}
	function createNewAdminProfile(id,flag)
	{
		var id = parseInt(id);
		if(id>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:flag},function(result){
				$("#ajaxLogin").html(result);
			});
		}		
	}
	function submitAdminProfileDetails(id)
	{
		var id = parseInt(id);
		if(id>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",$("#createNewAdminProfile").serialize(),function(result){
				var n = result.split('|||||');
				if(n[0]=='ERROR')
				{
					$('#Error').html("");
					$('#Error').html(n[1]);
				}
				if(n[0]=='SUCCESS')
				{
					$('#ajaxLogin').html('');
					$('#content_body').html(n[1]);
				}
			});
		}
	}
	
	function select_forwarder_management(id,tr_id,idForwarderContactRole,szFlag)
	{
		if(id>0)
		{	
			var rowCount = $('#forwarderManagement tr').length;
                        
			if(rowCount>0)
			{
				for(x=1;x<=rowCount;x++)
				{
					new_tr_id="forwarder_profile_"+x;
					if(new_tr_id!=tr_id)
					{
						$("#"+new_tr_id).attr('style','');
					}
					else
					{
						$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
                                
                                if(szFlag=='FROM_HEADER')
                                {
                                    $("#delete_contact_forwarder").unbind("click");
                                    $("#delete_contact_forwarder").attr("style","opacity:0.4");
                                    if(parseInt(idForwarderContactRole)==7)
                                    {
                                        $("#new_contact_forwarder").unbind("click");
                                        $("#new_contact_forwarder").attr("onclick","");
                                        $("#new_contact_forwarder span").html("Edit");
                                        $("#new_contact_forwarder").click(function(){openNewContactOfForwarder(id,'','',szFlag);});
                                        var searchStr=$("#szSearchString").attr('value');
                                        $("#delete_contact_forwarder").unbind("click");
                                        $("#delete_contact_forwarder").attr("style","opacity:1");
                                        $("#delete_contact_forwarder").click(function(){$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:id,flag:"DELETE_FORWARDER_PROFILE",szFlag:szFlag,szSearchStr:searchStr},function(result){
                                                    $("#ajaxLogin").html(result);
                                                    if(szFlag=='FROM_HEADER')
                                                    {
                                                        $("#contactPopup").html('');
                                                        $("#contactPopup").attr('style','display:none');
                                                    }

                                            });});
                                    }
                                    else
                                    {
                                        
                                        var szSearchStr='';
                                        $('#edit_forwarder').unbind();
                                        $("#new_contact_forwarder").unbind("click");
                                        $("#new_contact_forwarder").attr("onclick","");
                                        $("#new_contact_forwarder span").html("Edit");
                                        if(szFlag=='FROM_HEADER')
                                        {
                                            szSearchStr = $("#szSearchString").attr('value');
                                        }
                                        $('#new_contact_forwarder').click(function(){
                                                $.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:id,flag:"EDIT_FORWARDER_PROFILE",szFlag:szFlag,szSearchStr:szSearchStr},function(result){
                                                        $("#ajaxLogin").html(result);
                                                        if(szFlag=='FROM_HEADER')
                                                        {
                                                            $("#contactPopup").html('');
                                                            $("#contactPopup").attr('style','display:none');
                                                        }

                                                });
                                        });
                                    }
                                }
                                else
                                {
                                    $('#forwarder_history').unbind();
                                    $('#edit_forwarder').unbind();
                                    $('#delete_forwarder').unbind();

                                    if(parseInt(idForwarderContactRole)==7)
                                    {                                    
                                        $("#new_contact_forwarder").unbind("click");
                                        $("#new_contact_forwarder").attr("onclick","");
                                        $("#new_contact_forwarder span").html("Edit Contact");
                                        $("#new_contact_forwarder").click(function(){openNewContactOfForwarder(id);});

                                        $('#edit_forwarder').css('opacity','0.4');
                                        $('#forwarder_history').css('opacity','0.4');
                                    }
                                    else
                                    {
                                        $('#edit_forwarder').css('opacity','1');
                                        $('#edit_forwarder').click(function(){
                                                $.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:id,flag:"EDIT_FORWARDER_PROFILE"},function(result){
                                                        $("#ajaxLogin").html(result);

                                                });
                                        });
                                        $("#new_contact_forwarder span").html("New Contact");
                                        $("#new_contact_forwarder").unbind("click");
                                        $("#new_contact_forwarder").attr("onclick","");
                                        $("#new_contact_forwarder").click(function(){openNewContactOfForwarder();});

                                        $('#forwarder_history').css('opacity','1');
                                        $('#forwarder_history').click(function(){
                                                $.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:id,flag:"FORWARDER_PROFILE_ACCESS_HISTORY"},function(result){
                                                        $("#ajaxLogin").html(result);

                                                });
                                        });
                                    }

                                    $('#delete_forwarder').css('opacity','1');
                                    $('#delete_forwarder').click(function(){
                                            $.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:id,flag:"DELETE_FORWARDER_PROFILE"},function(result){
                                                    $("#ajaxLogin").html(result);

                                            });
                                    });
                                }
			}
		}
	}
	function ConfirmFormSubmit()
	{
                encode_string('szPhone','szPhoneUpdate','szMobile','szMobileUpdate');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",$("#updateUserInfo").serialize(),function(result){
						var n = result.split('|||||');
						if(jQuery.trim(n[0])=='ERROR')
						{	
							$("#ajaxLogin").html(n[1]);
						}
						if(jQuery.trim(n[0])=='SUCCESS')
						{	
                                                    var szFlag =$("#szFlag").attr('value');
                                                    var szSearchStr='';
                                                    szSearchStr = $("#szSearchStr").attr('value');
							$("#ajaxLogin").html('');
                                                        if(szFlag=='FROM_HEADER')
                                                        {
                                                           
                                                            display_remind_pane_add_popup('','','','','FROM_HEADER',''+szSearchStr+'')
                                                        }
                                                        else
                                                        {
                                                            viewAjax('forwarderProife');
                                                        }
							//$("#content_body").html(n[1]);
						}
						
					});
	}
	function changePasswordForwarderContact(idForwarder)
	{
		$("#forgotPassword").removeAttr("onclick");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{flag:"SEND_CHANGE_PASSWORD_EMAIL",idForwarder:idForwarder},function(result){	
				if(jQuery.trim(result)=='SEND')
				{	
					$("#forgotPassword").css({"color":"#000","text-decoration":" none"});
					$("#forgotPassword").html("<i>New password successfully sent</i>");
					$("#regError").html('');
				}
				else
				{
					$("#forgotPassword").attr("onclick","changePasswordForwarderContact("+idForwarder+")");
				}	
		});
	}
	function forwarder_profile_edit(idForwarder)
	{		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:idForwarder,flag:'EDIT_FORWARDER_PROFILE'},function(result){
			$("#content_body").html(result);	
		});
	}
	function deleteForwarderProfile(idForwarder)
	{	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:idForwarder,flag:'DELETE_FORWARDER_PROFILE'},function(result){
			$("#delete-profile").html(result);	
		});
	}
	function cancelDeleteForwarderProfile(id)
	{
		id = parseInt(id);
		if(id==0)
		{
			$("#ajaxLogin").html('');
		}
	}
	function deleteForwarderProfileConfirm(id,szFlag,szSearchStr)
	{
		id = parseInt(id);
		if(id>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:id,flag:'DELETE_FORWARDER_PROFILE_DONE'},function(result){
						var n = result.split('|||||');
						if(jQuery.trim(n[0])=='ERROR')
						{	
							$("#ajaxLogin").html(n[1]);
						}
						if(jQuery.trim(n[0])=='SUCCESS')
						{	
							$("#ajaxLogin").html('');
                                                        if(szFlag=='FROM_HEADER')
                                                        {
                                                            display_remind_pane_add_popup('','','','','FROM_HEADER',''+szSearchStr+'')
                                                        }
                                                        else
                                                        {
                                                            $("#content_body").html(n[1]);
                                                        }
						}
		});
		}
	}
	function createNewForwarderProfile()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{newflag:'CREATE_NEW_FORWARDER_PROFILE'},function(result){
			$("#ajaxLogin").html(result);
		});
						
	}
	function submitForwarderProfileDetails()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",$('#createNewAdminProfile').serialize(),function(result){
		var n = result.split('|||||');
		if(jQuery.trim(n[0])=='ERROR')
		{	
			$("#Error").html('');
			$("#Error").html(n[1]);
		}
		if(jQuery.trim(n[0])=='SEND')
		{	
			$("#ajaxLogin").html('');
			$("#content_body").html(n[1]);
		}
		});
	}
	
	function user_detail(id,idDiv)
	{
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:id,flag:'setting',idDiv:idDiv},function(result){
		$("#content_body").html(result);});
	}
	
	
	function update_userinfo_detail_by_admin()
	{
		var value=decodeURIComponent($("#updateUserInfo").serialize());
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",value,function(result){
		$("#content_body").html(result);});
	}
	
	function remove_value_select_box_admin(value,idUser)
	{
		if(value=='shipper')
		{
			$("#regCon").val(''); 
			var value=$("#regShipper").val();
			var n =value.length;
		}
		else if(value=='consignes')
		{
			$("#regShipper").val(''); 
			var value=$("#regCon").val();
			var n =value.length;
		}
		$("#remove").attr("style","opacity:1;");
		$("#remove").attr("onclick","remove_register_shipper_consigness_by_admin()");
		$('INPUT:text,SELECT', '#registerShipperConsigness').val('');
		$("#shipper_consignee_button").attr("style","opacity:0.4;");
		$("#shipper_consignee_button").attr("onclick","");
		
		var disp = $("#regError").css('display');
		if(disp=='block')
		{
			$("#regError").attr('style','display:none;');
		}
		
		if(n==1)
		{
			//$("#edit").attr("style","opacity:1;");
			//$("#edit").attr("onclick","edit_register_shipper_consigness_by_admin('"+value+"','"+idUser+"')");
		}
		else
		{
			//$("#edit").attr("style","opacity:0.4;");
			//$("#edit").attr("onclick","");
		}
		//edit_register_shipper_consigness_by_admin(value,idUser);
	}
	
	function remove_register_shipper_consigness_by_admin()
	{
	
		var value=$("#removeShipperConsigness").serialize();
		var new_value=value+"&flag=deleteShipperCongign";
	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",new_value,function(result){
		$("#registerShipConsignList").html(result);});
	}
	
	function send_customer_password(idUser)
	{
		$("#send_password").attr("style","display:block;");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:idUser,flag:'sendCustomerPassword'},function(result){
		$("#send_password").html(result);});
	}
	
	function cancel_sending_password()
	{
		$("#send_password").attr("style","display:none;");
		$("#currentflag").attr("value","setting");
	}
	
	function send_change_password(idUser)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:idUser,flag:'sendCustomerPassword',confirm:'confirm'},function(result){
		$("#send_password").html(result);
		$("#send_password").attr("style","display:none;");
		$("#currentflag").attr("value","setting");
		});
	}
	
	function cancel_remove_popup_admin(flag)
	{	
		if(flag=='shipperConsign')
		{
			$("#remove_ship_con_id").attr("style","display:none;");
			$("#currentflag").attr("value","shipperConsign");
		}
		else if(flag=='multiUser')
		{
			$("#multiUserList").attr("style","display:none;");
			$("#currentflag").attr("value","multiUser");
		}
		else if(flag=='deleteAcc')
		{
			$("#ajaxLogin").attr("style","display:none;");
			$("#currentflag").attr("value","deleteAcc");
		}
		else if(flag=='accountHis')
		{
			$("#ajaxLogin").attr("style","display:none;");
			$("#currentflag").attr("value","accountHis");
		}
	}
	
	function remove_shipper_con_data_admin()
	{
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",$("#remove_confirm").serialize(),function(result){
		$("#content_body").html(result);
		$("#currentflag").attr("value","shipperConsign");
		});
		enableForm('hsbody-2');
	}
	
	
	function sort_customer_table(sort_by,sort,divid,value)
	{
		new_value="sortby="+sort_by+"&sort="+sort+"&flag="+value;
		//alert(divid);
		if(sort=='ASC')
		{
			var new_sort='DESC';
			var innr_htm = '<span class="sort-arrow-up">&nbsp;</span>';
		}
		else
		{
			var new_sort='ASC';
			var innr_htm = '<span class="sort-arrow-down">&nbsp;</span>';
		}
		$.get(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",new_value,function(result){
		        $("#content_body").html(result);
		$("#"+divid).attr("onclick","sort_customer_table('"+sort_by+"','"+new_sort+"','"+divid+"','"+value+"');");      
		$("#"+divid).html(innr_htm);
		if(divid!='joined_date')
		{
			$('#joined_date').html('<span class="sort-arrow-up-grey"> </span>');
			$("#joined_date").attr("onclick","sort_customer_table('u.dtCreateOn','ASC','joined_date','cust');");
		}      
		});
	}
	
	function select_to_edit_user(div_id,id)
	{
		var rowCount = $('#usertable tr').length;
		
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="user_data_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		$("#"+div_id).unbind("dblclick");
		$("#"+div_id).dblclick(function(){user_detail(id,div_id)});
		
		$("#edit_button").attr('style','opacity:1.0;');
		$("#edit_button").click(function(){
					user_detail(id,div_id)
				});
	}
	
	function showEditCustomerPages(flag,id)
	{
		var currentflgvalue=$("#currentflag").val();
		
		var new_value="flag="+flag+"&idUser="+id+"&oldflgvalue="+currentflgvalue;
		if(flag=='deleteAcc' || flag=='reActive')
		{
			var divid="ajaxLogin";
		}
		else
		{
			var divid="content_body";
		}
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",new_value,function(result){
		$("#"+divid).html(result);
		if(flag=='deleteAcc' || flag=='reActive')
			{	
				$("#ajaxLogin").attr("style","display:block;");
			}
		});
	}
	
	function  edit_register_shipper_consigness_by_admin(idShipperConsignees,idUser)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:idUser,idShipperConsignees:idShipperConsignees,flag:'editShipperCongign'},function(result){
		$("#registerShipConsignEdit").html(result);
		
		$("#remove").attr("style","opacity:0.4;");
		$("#remove").attr("onclick","");
		
		$("#edit").attr("style","opacity:0.4;");
		$("#edit").attr("onclick","");
		
		$("#currentflag").attr("value","shipperConsign");
		
		$("#shipper_consignee_button").attr("style","opacity:1;");
		$("#shipper_consignee_button").attr("onclick","encode_string('szPhoneNumber','szPhoneNumberUpdate');save_register_shiper_consign()");
		});
	}
	function detailsOrderByForwarder(sortBy,sorting,id)
	{	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'forwarderProife',mode:sortBy,sort:sorting},function(result){
				
			$("#content_body").html(result);
			if(sorting=='ASC')
			{ 	
				$('#'+id).unbind("click");
				$('#'+id).attr('onclick','detailsOrderByForwarder('+sortBy+',\'DESC\',\''+id+'\')');
				$('#'+id).html('<span class="sort-arrow-up">&nbsp;</span>');
				
			}
			if(sorting=='DESC')
			{ 
				$('#'+id).unbind("click");
				$('#'+id).attr('onclick','detailsOrderByForwarder('+sortBy+',\'ASC\',\''+id+'\')');
				$('#'+id).html('<span class="sort-arrow-down">&nbsp;</span>');
			}
			if(sortBy!=1)
			{
				$('#sort_by_forwarder_name').attr('onclick','detailsOrderByForwarder(1,\'ASC\',\'sort_by_forwarder_name\')');
				$('#sort_by_forwarder_name').html('<span class="sort-arrow-up-grey">&nbsp;</span>');
			}
		});
	}
	
	function addNewForwarder()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'ADD_NEW_FORWARDER'},function(result){
			$('#ajaxLogin').html('');
			$('#ajaxLogin').html(result);
		});
	}
	function addNewForwarderConfirmValidate()
	{
		$('#createNewForwarder').removeAttr('onclick');
		$('#createNewForwarder').css('opacity','0.4');
		
		$('#cancel_button').removeAttr('onclick');
		$('#cancel_button').css('opacity','0.4');
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",$('#addNewForwarderProfile').serialize(),function(result){
			var n=result.split("|||||");
			if(jQuery.trim(n[0])=='ERROR')
			{	
				$('#createNewForwarder').attr('onclick','addNewForwarderConfirmValidate();');
				$('#createNewForwarder').css('opacity','1');
				
				$('#cancel_button').attr('onclick','cancelEditMAnageVar(0);');
				$('#cancel_button').css('opacity','1');
		
				$('#Error').html('');
				$('#Error').html(n[1]);
			}
			if(n[0]=='SUCCESS')
			{
				$('#ajaxLogin').html(n[1]);
				//$('#content_body').html(n[1]);
			}
			
		});
	}
	
	function select_to_Forwarder_companies(div_id,id,status)
	{
		var rowCount = $('#usertable tr').length;
		
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="user_data_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
			$("#admin_edit_button").css({opacity:'0.9',float: 'right'});
			$("#admin_edit_button").attr('onclick','edit_forwarder('+id+')');
			
			if(status=='activate')
			{
				$("#admin_delete_button").css({opacity:'0.9',float: 'right'});
				$("#admin_delete_button").attr('onclick','activate_forwarder('+id+')');
				$("#admin_delete_span").html('Activate');
			}
			else
			{
				$("#admin_delete_button").css({opacity:'0.9',float: 'right'});
				$("#admin_delete_button").attr('onclick','delete_forwarder('+id+')');
				$("#admin_delete_span").html('Delete');
			}
	}
	
	function edit_forwarder(id)
	{	
		var id = parseInt(id);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'EDIT_FORWARDER_PROFILE',id:id},function(result){
		$('#content_body').html(result);
		});
	}
	
	function delete_forwarder(id)
	{	
		var id = parseInt(id);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'DELETE_FORWARDER_PROFILE',id:id},function(result){
			//$('#content_body').html(result);
			$('#confirmation_popup').html(result);
			$("#confirmation_popup").css('display','block');
		});
	}
	
	function deleteFrorwarderConfirm(id)
	{	
		var id = parseInt(id);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'DELETE_FORWARDER_PROFILE_CONFIRM',id:id},function(result){
			$('#content_body').html(result);
			$("#confirmation_popup").css('display','none');
		});
	}
	
	function inactivateFrorwarderConfirm(id)
	{
		var id = parseInt(id);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'INACTIVATE_FORWARDER_PROFILE_CONFIRM',id:id},function(result){
			$('#content_body').html(result);
			$("#confirmation_popup").css('display','none');
		});
	}
	
	function activate_forwarder(id)
	{
		var id = parseInt(id);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'ACTIVATE_FORWARDER_PROFILE_CONFIRM',id:id},function(result){
			$('#content_body').html(result);
			$("#confirmation_popup").css('display','none');
		});
	}
	
	function setFocus()
	{
		$("#email").focus();
	}
	function cancelDelete()
	{
		$('#ajaxLogins').attr('style','display:none;');
		removePopupScrollClass();
	}	
	function preview_system(mode,option)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_systemTextPreviewPublish.php",{mode:mode,option:option},function(result){
			$('#preview_data').html('');
			if(option=='preview')
			{
				$('#preview_data').css('display','block');
				$('#preview_data').html(result);
			}
			if(	(mode=='TNC_CUSTOMER' || mode == 'FAQ_CUSTOMER' || mode =='FAQ_FORWARDER') && option!='preview' )
			{	
				$('#preview_data').html('');
				$('#contactPopup').html(result);
			}
			if(mode=='TNC_FORWARDER' && option!='preview')
			{	
				$('#preview_data').html('');
				$('#contactPopup').html(result);
				$('#publishContent').attr('onclick','submitVersionUpdates(\'FORWARDER\');');
			}
			$('#contactPopup').css({display:'block'});
		});
	}	
	
	function save_register_shiper_consign()
	{
		var value=decodeURIComponent($("#registerShipperConsigness").serialize());
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",value,function(result){
		$("#registerShipConsignEdit").html(result);});
	}
	
	
	function sel_remove_multi_access(idUser,idRemoveUser,accessflag,id)
	{
		var rowCount = $('#multiUsertable tr').length;
		div_id="multi_user_tr_"+id;
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="multi_user_tr_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
				
		$("#remove_multi_user_access").attr("style","opacity:1;");
		$("#remove_multi_user_access").unbind("click");
		$("#remove_multi_user_access").click(function(){remove_multi_access(idUser,accessflag,idRemoveUser)});
		
	}
	
	
	function remove_multi_access(idUser,accessflag,idRemoveUser)
	{
		var new_value="flag=deleteMultiAccess&idUser="+idUser+"&idRemoveUser="+idRemoveUser+"&accessflag="+accessflag;
		$("#multiUserList").attr("style","display:block;");		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",new_value,function(result){
		$("#multiUserList").html(result);
		$("#currentflag").attr("value","multiUser");
		});
	}
	
	
	function confirm_remove_user()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",$("#removeMultiAccess").serialize(),function(result){
		$("#multiUserList").html(result);
		$("#currentflag").attr("value","multiUser");
		});
	}
	
	
	function check_email_address_by_admin()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",$("#addMultiUser").serialize(),function(result){
		$("#multiUserAdd").html(result);
		$("#currentflag").attr("value","multiUser");
		});
	}
	
	function confirm_mapped_user_by_admin()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",$("#linkedUser").serialize(),function(result){
		$("#multiUserAdd").html(result);
		$("#currentflag").attr("value","multiUser");
		});
	}
	
	function confirm_delete_user(idUser,actionFlag)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:idUser,actionFlag:actionFlag,flag:'deleteAcc'},function(result){
		$("#delete_reactivate").html(result);
		$("#currentflag").attr("value","deleteAcc");
		});
	}
	
	
	function confirm_reactive_user(idUser,actionFlag)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:idUser,actionFlag:actionFlag,flag:'reActive'},function(result){
		$("#ajaxLogin").html(result);
		$("#currentflag").attr("value","deleteAcc");
		});
	}
	
	function send_email(id,idUser)
	{
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:idUser,idEmailLog:id,flag:'sendMail'},function(result){
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr("style","display:block;");
		$("#currentflag").attr("value","accountHis");
		});
	}
	
	function email_send_user()
	{
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",$("#sendEmail").serialize(),function(result){
		$("#ajaxLogin").html(result);
		$("#currentflag").attr("value","accountHis");
		});
	}
	function closeConactPopUp(option)
	{
            if(option==0)
            {
                $('#contactPopup').html('');
            }
	}
	function submitVersionUpdates(option)
	{	
		$('#publishContent').removeAttr('onclick');
		$('#publishContent').css('opacity','0.4');
		var comment = $('#comment').val();
		$('#contactPopup').css({display:'none'});
		$("#loader").attr('style','display:block;');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_systemTextPreviewPublish.php",{mode:option,comment:comment},function(result){
			var n = result.split('|||||');
			if(jQuery.trim(n[0])=='ERROR')
			{
				$('#contactPopup').css({display:'block'});
				$('#publishContent').css('opacity','1');
				$('#publishContent').attr('onclick','submitVersionUpdates(\'FORWARDER\');');
				$('#error-Ver').html(n[1]);
			}
			if(jQuery.trim(n[0])=='SUCCESS')
			{	$("#loader").attr('style','display:none;');
				$('#contactPopup').html('');
				$('#publish').attr('onclick','');
				$('#publish').css('opacity','0.4');
				$('#showUpdate').html(n[1]);
				$('#cancel').attr('onclick','');
				$('#cancel').css('opacity','0.4');
				
			}
			
		});
	}
	/*
	function select_history(div_id,id)
	{
		var rowCount = $('#history_table tr').length;
		
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="history_tr_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_historyForwarderTermsCondition.php",{id:id},function(result){
			var n = result.split('|||||');
			$('#showMe').html(n[0]);
			$('#pdfviewer').attr('src',n[1]+'#scrollbar=0')
		});
		}
	}
	*/
	function select_history(div_id,id)
	{
		var rowCount = $('#history_table tr').length;
		
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="history_tr_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
			/*$.post(__JS_ONLY_SITE_BASE__+"/ajax_historyForwarderTermsCondition.php",{id:id},function(result){
			var n = result.split('|||||');
			//$('#showMe').html("<b>Comment- </b> \""+n[0]+"\"");
			if(n[1]!='')
			{
				$('#pdfviewer').attr('src',n[1]+'#scrollbar=0');
				$('#pdfviewer').css('border','thin solid');
			}
			else
			{
					$('#pdfviewer').attr('src','');
					$('#pdfviewer').css('border','');
			}	
		}); */
		}
	}
	
	function send_email_to_user(idUser)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{idUser:idUser,flag:'userSendEmail'},function(result){
		$("#delete_reactivate").html(result);
		$("#delete_reactivate").attr("style","display:block;");
		$("#currentflag").attr("value","settings");
		});
	}
	
	function confirm_send_email_user()
	{
		$("#confirm_email").attr("style","opacity:0.4;");
		$("#confirm_email").attr("onclick","");
		
		$("#cancel_email").attr("style","opacity:0.4;");
		$("#cancel_email").attr("onclick","");
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",$("#sendUserEmail").serialize(),function(result){
		$("#delete_reactivate").html(result);
		$("#currentflag").attr("value","settings");
		});
	}
	
	function forwarder_profile_show_history(idProfile)
	{		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{idForwarder:idProfile,flag:'FORWARDER_PROFILE_ACCESS_HISTORY'},function(result){
			$("#content_body").html(result);	
		});
	}
	function  showForwarderCompany(id,mode)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:mode},function(result){
			$("#content_body").html(result);	
		});
	}
	function  inactiveForwarderCompany(id,mode)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:'Inactive',mode:mode},function(result){
			$("#ajaxLogin").html(result);	
		});
	}
	function editForwarderPref(div_id,id)
	{	
		var rowCount = $('#history_table tr').length;
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="history_tr_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
			
			$('#editPreferences, #removePreferences').removeAttr("style");
			$('#Error-log').html('');
			$('#editPreferences').attr("onclick",'editForwarderPreferences('+id+')');
			$('#removePreferences').attr("onclick",'removeForwarderPreferencesCheck(\''+div_id+'\',\''+id+'\')');
		}
	}
	function editForwarderPreferences(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:'EDIT_FORWARDER_PREFERENCES'},function(result){
					$('#Error-log').html('');
					$("#addForwarderProfiler").html(result);	
				});
	}
	function removeForwarderPreferencesCheck(div_id,id)
	{
		var email =$('#'+div_id+' td:nth-child(2)').text();
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,email:email,flag:'REMOVE_FORWARDER_PREFERENCES'},function(result){
					$('#ajaxLogin').html(result);
				});
	}
	function addForwarderPreferences()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",$("#updateForwarderContact").serialize(),function(result){
			var n = result.split('|||||');
			if(n[0]=='ERROR')
			{
				$("#Error-log").html(n[1]);	
			}
			else if(n[0]=='SUCCESS')
			{
				$("#content_body").html(n[1]);	
			}	
			});	
	}
	function removeForwarderPreferences(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'REMOVE_FORWARDER_PREFERNCES_CONFIRM',id:id},function(result){
			var n = result.split('|||||');
			if(n[0]=='ERROR')
			{
				$("#Error-log").html(n[1]);	
			}
			else if(n[0]=='SUCCESS')
			{
				$('#ajaxLogin').html('');
				$("#content_body").html(n[1]);	
			}
		});	
	}
	function submit_registered_company(id)
	{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",$(".updateRegistComapnyForm").serialize(),function(result){
			var n = result.split('|||||');
			
			if(n[0]=='ERROR')
			{
				$("#Error-log").html(n[1]);	
			}
			else if(n[0]=='SUCCESS')
			{
				$('#Error-log').html('');
				showDifferentProfile('forwarderComp');
			}
		});	
	}
	function submitBankDetails(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",$("#updateRegistBankForm").serialize(),function(result){
			var n = result.split('|||||');
			
			if(n[0]=='ERROR')
			{
				$("#Error-log").html(n[1]);	
			}
			else if(n[0]=='SUCCESS')
			{
				$('#Error-log').html('');
				showDifferentProfile('forwarderComp');
			}
		});	
	}
	function submit_preference_controls_del()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",$("#ajax_prefrence_control").serialize(),function(result){
			var n = result.split('|||||');	
			if(n[0]=='ERROR')
			{
				$('#Error-log').html('');
				$("#Error").html('');	
				$("#Error").html(n[1]);	
			}
			else if(n[0]=='SUCCESS')
			{
				$('#Error-log').html('');
				$("#Error").html('');	
				showDifferentProfile('forwarderComp');
			}
		});
	}
	function removeForwarder(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'INACTIVE_FORWARDER_PROFILER',id:id},function(result){
			$('#ajaxLogin').html('');
			showDifferentProfile('forwarderComp');
		});
	}
	
	function select_all_checkbox(flag)
	{
		if(document.getElementById('recevier').checked==true)
		{
			$('#szCountry option').attr('selected', 'selected');
			var countryIdArr = $("#szCountry").val();
		}
		else
		{
			$('#szCountry option').removeAttr('selected');
		}
		getUserDataByCountryData();
	}
	
	
	function select_country(value)
	{
		/*if(document.getElementById('recevier').checked==true && flag==undefined)
		{
			$('#recevier').removeAttr('checked');
		}*/
		//var szCountryArr='';
		var szCountryArr = $("#szCountry").val();
		
		if(szCountryArr!=undefined)
		{
			var arrlen=szCountryArr.length;
			var newCountryArr=[];
			//alert(arrlen);
			//alert(szCountryArr[0]);
			if(arrlen>0 && szCountryArr!='All')
			{
				//alert(arrlen);
				$('#szCountry option:selected').removeAttr("selected");
				var j=0;
				for(i=0;i<arrlen;i++)
				{
					
					if(szCountryArr[i]!='All')
					{
						newCountryArr[j]=szCountryArr[i];
						++j;
					}
				}
				
				var arrlenNew=newCountryArr.length;
				if(arrlenNew>0)
				{
					$("#szCountry").val(newCountryArr);
				}
			}
			getUserDataByCountryData();
		}
	}
	
	function getUserDataByCountryData()
	{
	
		//if(document.getElementById('recevier').checked==false)
		//{
			var szCountryArr = $("#szCountry").val();
		/*}
		else
		{
			var szCountryArr ='All';
		}*/
		var iActiveFlag='';
		
		if(document.getElementById('iActiveFlag1').checked)
		{
			var iActiveFlag1=$('#iActiveFlag1').val();
			var iActiveFlag=iActiveFlag1;
		}
		
		if(document.getElementById('iActiveFlag2').checked)
		{
			var iActiveFlag2=$('#iActiveFlag2').val();
			if(iActiveFlag!='')
			{
				iActiveFlag2='0';
				iActiveFlag=iActiveFlag+"_"+iActiveFlag2; 
			}
			else
			{
				iActiveFlag2='0';
				iActiveFlag=iActiveFlag2;
			}
		}
		//alert(iActiveFlag);
		var iNewsUpdate='';
		if(document.getElementById('iNewsUpdate1').checked)
		{
			var iNewsUpdate1=$('#iNewsUpdate1').val();
			var iNewsUpdate=iNewsUpdate1;
		}
		if(document.getElementById('iNewsUpdate2').checked)
		{
			var iNewsUpdate2=$('#iNewsUpdate2').val();
			if(iNewsUpdate!='')
			{
				iNewsUpdate2='0';
				iNewsUpdate=iNewsUpdate+"_"+iNewsUpdate2; 
			}
			else
			{
				iNewsUpdate2='0';
				iNewsUpdate=iNewsUpdate2;
			}
		}
		
			
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendCustomerMsg.php",{szCountryArr:szCountryArr,iActiveFlag:iActiveFlag,iNewsUpdate:iNewsUpdate},function(result){
			$("#user_count").html(result);	
		});
	}
	
	
	function confirm_send_email_to_selected_user()
	{
		//if(document.getElementById('recevier').checked==false)
		//{
			var szCountryArr = $("#szCountry").val();
		/*}
		else
		{
			var szCountryArr ='All';
		}*/
		var iActiveFlag='';
		
		if(document.getElementById('iActiveFlag1').checked)
		{
			var iActiveFlag1=$('#iActiveFlag1').val();
			var iActiveFlag=iActiveFlag1;
		}
		
		if(document.getElementById('iActiveFlag2').checked)
		{
			var iActiveFlag2=$('#iActiveFlag2').val();
			if(iActiveFlag!='')
			{
				iActiveFlag2='0';
				iActiveFlag=iActiveFlag+"_"+iActiveFlag2; 
			}
			else
			{
				iActiveFlag2='0';
				iActiveFlag=iActiveFlag2;
			}
		}
		//alert(iActiveFlag);
		var iNewsUpdate='';
		if(document.getElementById('iNewsUpdate1').checked)
		{
			var iNewsUpdate1=$('#iNewsUpdate1').val();
			var iNewsUpdate=iNewsUpdate1;
		}
		if(document.getElementById('iNewsUpdate2').checked)
		{
			var iNewsUpdate2=$('#iNewsUpdate2').val();
			if(iNewsUpdate!='')
			{
				iNewsUpdate2='0';
				iNewsUpdate=iNewsUpdate+"_"+iNewsUpdate2; 
			}
			else
			{
				iNewsUpdate2='0';
				iNewsUpdate=iNewsUpdate2;
			}
		}
		
		var szSubject=$('#szSubject').val();
		var szMessage=$('#szMessage').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendCustomerMsg.php",{szCountryArr:szCountryArr,iActiveFlag:iActiveFlag,iNewsUpdate:iNewsUpdate,szSubject:szSubject,szMessage:szMessage,flag:'sendMail'},function(result){
		$("#content_body").html(result);
		});
	}
	
	function preview_send_email_to_selected_user()
	{
		var szMessage=encode_string_msg('szMessage');
		//alert(szMessage);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendCustomerMsg.php",{szMessage:szMessage,showflag:'Preview'},function(result){
		$("#message_preview").html(result);
		$("#message_preview").attr("style","display:block;");
		//addPopupScrollClass();
		});
	}
	
	function cancelPreviewMsg(divid)
	{
		removePopupScrollClass();
		$("#"+divid).attr("style","display:none;");
	}
	
	function confirm_send_msg_to_user()
	{
		$("#no").attr("style","opacity:0.4;");
		$("#no").attr("onclick","");
		
		$("#yes").attr("style","opacity:0.4;");
		$("#yes").attr("onclick","");
		var szSubject=$('#szSubject').val();
		var szMessage=encode_string_msg('szMessage');
		var szCustomerArr=$('#szCustomerArr').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendCustomerMsg.php",{szCustomerArr:szCustomerArr,szSubject:szSubject,szMessage:szMessage,sendflag:'ConfirmFlag'},function(result){
		$("#content_body").html(result);
		removePopupScrollClass();
		});
	}
	
	function open_html_var(flag)
	{
		if(flag=='user')
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendCustomerMsg.php",{showflag:'HtmlVar'},function(result){
			$("#show_preview").html(result);
			$("#show_preview").attr("style","displayblock;");
			addPopupScrollClass();
			});
		}
		else if(flag=='forwarder')
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendForwarderMsg.php",{showflag:'HtmlVar'},function(result){
			$("#show_preview").html(result);
			$("#show_preview").attr("style","displayblock;");
			addPopupScrollClass();
			});
		}
	}
	
	 function number_format_backup(numero, params) //indica o nome do plugin que ser� criado com os parametros a serem informados
	 { 
		//parametros default
		var sDefaults = 
			{			
			numberOfDecimals: 2,
			decimalSeparator: '.',
			thousandSeparator: ',',
			symbol: ''
			}
 
		//fun��o do jquery que substitui os parametros que n�o foram informados pelos defaults
		var options = jQuery.extend(sDefaults, params);
                alert(options.numberOfDecimals+"----"+numero);
		//CORPO DO PLUGIN
		var number = numero; 
		var decimals = options.numberOfDecimals;
		var dec_point = options.decimalSeparator;
		var thousands_sep = options.thousandSeparator;
		var currencySymbol = options.symbol;
		
		var exponent = "";
		var numberstr = number.toString ();
		var eindex = numberstr.indexOf ("e");
		if (eindex > -1)
		{
		exponent = numberstr.substring (eindex);
		number = parseFloat (numberstr.substring (0, eindex));
		}
		
		if (decimals != null)
		{
		var temp = Math.pow (10, decimals);
		number = Math.round (number * temp) / temp;
		}
		var sign = number < 0 ? "-" : "";
		var integer = (number > 0 ? 
		  Math.floor (number) : Math.abs (Math.ceil (number))).toString ();
		
		var fractional = number.toString ().substring (integer.length + sign.length);
		dec_point = dec_point != null ? dec_point : ".";
		fractional = decimals != null && decimals > 0 || fractional.length > 1 ? 
				   (dec_point + fractional.substring (1)) : "";
		if (decimals != null && decimals > 0)
		{
		for (i = fractional.length - 1, z = decimals; i < z; ++i)
		  fractional += "0";
		}
		
		thousands_sep = (thousands_sep != dec_point || fractional.length == 0) ? 
					  thousands_sep : null;
		if (thousands_sep != null && thousands_sep != "")
		{
		for (i = integer.length - 3; i > 0; i -= 3)
		  integer = integer.substring (0 , i) + thousands_sep + integer.substring (i);
		}
		
		if (options.symbol == '')
		{
		return sign + integer + fractional + exponent;
		}
		else
		{
		return currencySymbol + ' ' + sign + integer + fractional + exponent;
		}
		//FIM DO CORPO DO PLUGIN	
		
	}
	/*function showCityPostcode(value)
	{	
		var countryId = $('select').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{flag:'SEARCH_POSTCODE',szCityPostcode:value,countryId:countryId},function(result){
			
		});
	}
	*/
	function searchPostCodeCountry()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",$('#searchPostcode').serialize(),function(result){
			var n = result.split('|||||');
			if(jQuery.trim(n[0])=='SUCCESS')
			{	
				$('#countryId').attr('value',$('#szOriginCountry').val());
				var country = $('#szOriginCountry').val();
				var city = $('#szOriginCity').val();
				var postcode = $('#szOriginPostCode').val();
				$('#error').html('');
				//$('#szOriginCountry').detach();
				$('#szOriginCountry').each(function(element) {
   				$(this).attr('disabled','disabled');
				});
				$('#szOriginPostCode').attr('disabled','disabled');
				$('#szOriginCity').attr('disabled','disabled');
				$("#headings tr").slice(1).remove();
				$('#searchPostCode').attr('onclick','showDefaultPostCode()');
				$('#searchPostCode').html('<span>Change</span>');
				$('#add_post_code').css('opacity','1');
				$('#add_post_code').attr('onclick','add_new_postcode()');
				$('#szHiddenCountry').attr('value',country);
				$('#szHiddenCity').attr('value',city);
				$('#szHiddenPostCode').attr('value',postcode);
				if(jQuery.trim(jQuery.trim(n[1]))=='ERROR')
				{
					$('#headings tr').after('<tr><td colspan="8" style="text-align:center;"><b>No match</b></td></tr>');
				}
				else
				{
					$('#content').html(n[1]);	
					
				}
				
			}
			if(jQuery.trim(n[0])=='ERROR')
			{
				$('#error').html('');
				$('#error').html(n[1]);
			}
		});
	}
	function showPostCodeTable(div_id,id)
	{
		var rowCount = $('#headings tr').length;
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="postcode_details_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}	
			}
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'SHOW_ME_POST_CODE'},function(result){	
			$('#show_me_post_code').html(result);
			});
			$('#add_post_code').attr('onclick','edit_postcode(\''+id+'\',\''+div_id+'\')');
			$('#add_post_code').css('opacity','1');
			$('#add_post_code').html('<span>edit</span>');
			$('#delete_post_code').html('<span>Delete</span>');
			$('#delete_post_code').css('opacity','1');
			$('#delete_post_code').attr('onclick','delete_postcode(\''+id+'\')');
		}
	}
	function showDefaultPostCode()
	{
		var country = $('#szHiddenCountry').val();
		var city = $('#szHiddenCity').val();
		var postcode = $('#szHiddenPostCode').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'SHOW_DEFAULT_POST_CODE'},function(result){
			$('.hsbody-2-right').html('');	
			$('.hsbody-2-right').html(result);
			$('#szOriginCountry').attr('value',country);
			$('#szOriginCity').attr('value',city);
			$('#szOriginPostCode').attr('value',postcode);
			
		});
	}
	function edit_postcode(id,div_id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'SHOW_EACH_POST_CODE',id:id},function(result){
			$('#show_me_post_code').html('');	
			$('#add_post_code').html('<span>save</span>');
			$('#add_post_code').attr('onclick','save_new_postcode('+id+',"'+div_id+'");');
			$('#delete_post_code').html('<span>Cancel</span>');
			$('#delete_post_code').attr('onclick','showPostcodeEmptyForm('+id+')');
			$('#show_me_post_code').html(result);
			$('#countryId').attr('value',$('#szOriginCountry').val());
		});	
	}
	function showPostcodeEmptyForm(id,div_id)
	{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'SHOW_EMPTY_POST_CODE'},function(result){
			$('#show_me_post_code').html(result);
			$('#add_post_code').attr('onclick','edit_postcode(\''+id+'\',\''+div_id+'\')');
			$('#add_post_code').css('opacity','1');
			$('#add_post_code').html('<span>edit</span>');
			$('#delete_post_code').html('<span>delete</span>');
			$('#delete_post_code').css('opacity','1');
			$('#delete_post_code').attr('onclick','delete_postcode(\''+id+'\')');
			});
	}
	function add_new_postcode()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",$('#postCodeTable').serialize(),function(result){
		var n = result.split('|||||');
			if(n[0]=='ERROR')
			{	
				$('#Error-log').html('');
				$("#Error").html('');	
				$("#Error-log").html(n[1]);	
			}
			else if(n[0]=='SUCCESS')
			{
				showDefaultPostCode();	
			
			}
		});	
	}
	function save_new_postcode(id,div_id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",$('#postCodeTable').serialize(),function(result){
		var n = result.split('|||||');
			if(jQuery.trim(n[0])=='ERROR')
			{	
				$('#Error-log').html('');
				$("#Error").html('');	
				$("#Error-log").html(n[1]);	
			}
			else if(jQuery.trim(n[0])=='SUCCESS')
			{
				//showDefaultPostCode();
				var country = $('#szHiddenCountry').val();
				var city = $('#szHiddenCity').val();
				var postcode = $('#szHiddenPostCode').val();
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'POSTCODE_CONTENT',country:country,city:city,postcode:postcode},function(result){
					var n = result.split('|||||');
					if(jQuery.trim(n[0])=='SUCCESS')
					{	
						$('#headings').find("tr:gt(0)").remove();
						$('#headings tr').after(n[1]);	
						showPostcodeEmptyForm(id,div_id);
						$("#"+div_id).css({'background':'#DBDBDB','color':'#828282'});
					}
					});	
			
			}
		});	
	}
	function delete_postcode(id,city)
	{	
		var city = $('#szCity').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'DELETE_POST_CODE',id:id,szCity:city},function(result){
		$('#ajaxLogin').html(result);
		});
	}
	function removepostcodeCity(id)
	{	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'DELETE_POST_CODE_CONFIRM',id:id},function(result){
			var n = result.split("|||||");
			if(n[0]=='SUCCESS')
			{
				$('#ajaxLogin').html('');
				showDefaultPostCode();	
			}
		});
	}
	function save_crown_jobs(id)
	{
		var crown_jobs = $('#crown_jobs').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_crownJobs.php",{crownJobs:crown_jobs,id:id,mode:'UPDATE_CR_JOBS'},function(result){
		});	
	}
	function showMeLocation(id,id_table,iLanguage)
	{
		if(id>0)
		{	
			$(".HideMe").removeAttr('onclick');
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_locationDescription.php",{id:id,iLanguage:iLanguage,mode:'SHOW_LOCATION'},function(result){
			$('#'+id_table).html(result);
		});		
		}
	}
	function saveLocationDescription(id,iLanguage)
	{
		var exactLoc = $('#exactLoc').val();
		var nonExactLoc = $('#nonExactLoc').val();
		if(id>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_locationDescription.php",{id:id,iLanguage:iLanguage,exactLoc:exactLoc,nonExactLoc:nonExactLoc,mode:'SAVE_LOCATION'},function(result){
			$("#booking_table tr:gt(0)").remove();
			$('#booking_table_location').after(result);
			//$('#'+id_table).html(result);
		});		
		}
	}
	function clearCityNotFound()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'CLEAR_CITY_NOT_FOUND'},function(result){
				$('#ajaxLogin').html(result);
				//showDefaultPostCode();	
		});
	}
	function removecity()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'CONFIRM_CLEAR_CITY_NOT_FOUND'},function(result){
				$('#ajaxLogin').html('');
				showDefaultPostCode();	
		});	
	}
	function select_forwarder_confirmation_tr(tr_id,id,mode)
	{
		var rowCount = $('#booking_table tr').length;
		if(rowCount>0)
		{
                    for(x=1;x<rowCount;x++)
                    {
                        new_tr_id="booking_data_"+x;
                        if(new_tr_id!=tr_id)
                        {
                            $("#"+new_tr_id).attr('style','');
                            $("#"+new_tr_id).removeClass('selected-row');
                        }
                        else
                        {
                            //'background:#DBDBDB;color:#828282;'
                            $("#"+tr_id).addClass('selected-row');
                        }
                    }
		}
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationNonAcknowledge.php",{id:id,mode:'CHECK_EMAIL_DATE_STATUS'},function(result){
                    if(jQuery.trim(result) == true)
                    {
                            $("#resend_email_forwarder").attr("style","opacity:1");
                            $('#resend_email_forwarder').attr('onclick','resend_email_forwarder(\''+id+'\')');
                    }
                    else
                    {
                            $("#resend_email_forwarder").attr("style","opacity:0.4");
                            $('#resend_email_forwarder').attr('onclick','');
                    }
		});
		$("#booking_cancel").unbind("click");
		$("#booking_cancel").click(function(){cancel_booking(id,mode)});
		var courierUrlLabel=__JS_ONLY_SITE_BASE__+"/viewCourierLabel/"+id+"/";
		$("#booking_cancel").attr("style","opacity:1");
		$("#download_booking_confirmation").attr('onclick','download_booking_confirmation_pdf(\''+id+'\')');
		$("#download_booking_invoice").attr('onclick','download_admin_invoice_pdf(\''+id+'\')');
		$("#download_booking_lcl_booking").attr('onclick','download_admin_booking_pdf(\''+id+'\')');
		$("#label_button").attr('onclick','upload_booking_label(\''+id+'\')');
		$("#label_button_sent").attr('onclick','upload_booking_label_sent(\''+id+'\')');
		$("#label_button_new_sent").attr('onclick','upload_booking_label_new_sent(\''+id+'\')');
                $("#label_resend").attr('onclick','resend_courier_label_email(\''+id+'\')');
                 $("#label_download").attr('onclick','download_courier_label_email(\''+id+'\')');
                 $("#courier_label_button_sent").attr('onclick','sent_courier_label_email(\''+id+'\')');
                 $("#see_booking_courier_label").attr('href',courierUrlLabel);
		$('#download_booking_confirmation, #download_booking_invoice, #download_booking_lcl_booking, #label_button,#label_button_new_sent, #label_button_sent, #label_resend, #label_download, #courier_label_button_sent, #see_booking_courier_label').removeAttr('style');
	}
	function download_booking_confirmation_pdf(id)
	{
		window.open(__JS_ONLY_SITE_BASE__+'/viewForwarderBookingConfirmation/'+id+'/',"_blank")
	}
	function viewNextOperationManagementDetail(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationManagementDetails.php",{action:'SELECT_FORWARDER_DETAILS',id:id},function(result){
			$('#showForwarderFeedback').html(result);
		});

	}
	
	function select_all_checkbox_forwarder()
	{
		if(document.getElementById('recevier').checked==true)
		{
			$('#szForwarder option').attr('selected', 'selected');
			var szForwarderArr = $("#szForwarder").val();
		}
		else
		{
			$('#szForwarder option').removeAttr('selected');
		}
		getForwarderUserData();
	}
	
	function select_all_checkbox_forwarder_profiles()
	{
		if(document.getElementById('profile').checked==true)
		{
			$('#szProfile option').attr('selected', 'selected');
			var szProfileArr = $("#szProfile").val();
		}
		else
		{
			$('#szProfile option').removeAttr('selected');
		}
		getForwarderUserData();
	}
	
	function getForwarderUserData()
	{
	
		var szForwarderArr = $("#szForwarder").val();
		
		var szProfileArr = $("#szProfile").val();
		
		
		var iForwarderStatusFlag='';
		
		if(document.getElementById('iForwarderStatus1').checked)
		{
			var iForwarderStatusFlag1=$('#iForwarderStatus1').val();
			var iForwarderStatusFlag=iForwarderStatusFlag1;
		}
		if(document.getElementById('iForwarderStatus2').checked)
		{
			var iForwarderStatusFlag2=$('#iForwarderStatus2').val();
			var iForwarderStatusFlag2=0;
			if(iForwarderStatusFlag!='')
			{
				var iForwarderStatusFlag=iForwarderStatusFlag+"_"+iForwarderStatusFlag2;
			}
			else
			{
				var iForwarderStatusFlag=iForwarderStatusFlag2;
			}
		}
		
		
		var iForwarderUserStatusFlag='';
		if(document.getElementById('iForwarderUserStatus1').checked)
		{
			var iForwarderUserStatusFlag1=$('#iForwarderUserStatus1').val();
			var iForwarderUserStatusFlag=iForwarderUserStatusFlag1;
		}
		if(document.getElementById('iForwarderUserStatus2').checked)
		{
			var iForwarderUserStatusFlag2=$('#iForwarderUserStatus2').val();
			var iForwarderUserStatusFlag2=0;
			if(iForwarderUserStatusFlag!='')
			{
				var iForwarderUserStatusFlag=iForwarderUserStatusFlag+"_"+iForwarderUserStatusFlag2;
			}
			else
			{
				var iForwarderUserStatusFlag=iForwarderUserStatusFlag2;
			}
		}
		
			
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendForwarderMsg.php",{szForwarderArr:szForwarderArr,iForwarderStatusFlag:iForwarderStatusFlag,iForwarderUserStatusFlag:iForwarderUserStatusFlag,szProfileArr:szProfileArr},function(result){
			$("#user_count").html(result);	
		});
	}
	
	function select_forwarder()
	{
		var szForwarderArr = $("#szForwarder").val();
		if(szForwarderArr!=undefined)
		{
			var arrlen=szForwarderArr.length;
			var newForwarderArr=[];
			//alert(arrlen);
			//alert(szCountryArr[0]);
			if(arrlen>0 && szForwarderArr!='All')
			{
				//alert(arrlen);
				$('#szForwarder option:selected').removeAttr("selected");
				var j=0;
				for(i=0;i<arrlen;i++)
				{
					if(szForwarderArr[i]!='All')
					{
						newForwarderArr[j]=szForwarderArr[i];
						++j;
					}
				}
				
				var arrlenNew=newForwarderArr.length;
				if(arrlenNew>0)
				{
					$("#szForwarder").val(newForwarderArr);
				}
			}
			getForwarderUserData();
		}
	}
	
	function select_forwarder_profile()
	{
		var szProfileArr = $("#szProfile").val();
		if(szProfileArr!=undefined)
		{
			var arrlen=szProfileArr.length;
			var newProfileArr=[];
			//alert(arrlen);
			//alert(szCountryArr[0]);
			if(arrlen>0 && szProfileArr!='All')
			{
				//alert(arrlen);
				$('#szProfile option:selected').removeAttr("selected");
				var j=0;
				for(i=0;i<arrlen;i++)
				{
					if(szProfileArr[i]!='All')
					{
						newProfileArr[j]=szProfileArr[i];
						++j;
					}
				}
				
				var arrlenNew=newProfileArr.length;
				if(arrlenNew>0)
				{
					$("#szProfile").val(newProfileArr);
				}
			}
			getForwarderUserData();
		}
	}
	
	function preview_send_email_to_selected_forwarder()
	{
		var szMessage=encode_string_msg('szMessage');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendForwarderMsg.php",{szMessage:szMessage,showflag:'Preview'},function(result){
		$("#forwarder_preview_msg").html(result);
		$("#forwarder_preview_msg").attr("style","display:block;");
		//addPopupScrollClass();
		});
	}
	
	
	function confirm_send_email_to_selected_forwarder()
	{
		
		var szForwarderArr = $("#szForwarder").val();
		
		var szProfileArr = $("#szProfile").val();
		
		
		var iForwarderStatusFlag='';
		
		if(document.getElementById('iForwarderStatus1').checked)
		{
			var iForwarderStatusFlag1=$('#iForwarderStatus1').val();
			var iForwarderStatusFlag=iForwarderStatusFlag1;
		}
		if(document.getElementById('iForwarderStatus2').checked)
		{
			var iForwarderStatusFlag2=$('#iForwarderStatus2').val();
			var iForwarderStatusFlag2=0;
			if(iForwarderStatusFlag!='')
			{
				var iForwarderStatusFlag=iForwarderStatusFlag+"_"+iForwarderStatusFlag2;
			}
			else
			{
				var iForwarderStatusFlag=iForwarderStatusFlag2;
			}
		}
		
		
		var iForwarderUserStatusFlag='';
		if(document.getElementById('iForwarderUserStatus1').checked)
		{
			var iForwarderUserStatusFlag1=$('#iForwarderUserStatus1').val();
			var iForwarderUserStatusFlag=iForwarderUserStatusFlag1;
		}
		if(document.getElementById('iForwarderUserStatus2').checked)
		{
			var iForwarderUserStatusFlag2=$('#iForwarderUserStatus2').val();
			var iForwarderUserStatusFlag2=0;
			if(iForwarderUserStatusFlag!='')
			{
				var iForwarderUserStatusFlag=iForwarderUserStatusFlag+"_"+iForwarderUserStatusFlag2;
			}
			else
			{
				var iForwarderUserStatusFlag=iForwarderUserStatusFlag2;
			}
		}
		
		var szSubject=$('#szSubject').val();
		var szMessage=$('#szMessage').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendForwarderMsg.php",{szForwarderArr:szForwarderArr,iForwarderStatusFlag:iForwarderStatusFlag,iForwarderUserStatusFlag:iForwarderUserStatusFlag,szProfileArr:szProfileArr,szSubject:szSubject,szMessage:szMessage,flag:'sendMail'},function(result){
		$("#content_body").html(result);
		});
	}
	
	
	function confirm_send_msg_to_forwarder()
	{
		$("#no").attr("style","opacity:0.4;");
		$("#no").attr("onclick","");
		
		$("#yes").attr("style","opacity:0.4;");
		$("#yes").attr("onclick","");
		var szSubject=$('#szSubject').val();
		var szMessage=encode_string_msg('szMessage');
		var szForwarderIdArr=$('#szForwarderIdArr').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendForwarderMsg.php",{szForwarderIdArr:szForwarderIdArr,szSubject:szSubject,szMessage:szMessage,sendflag:'ConfirmFlag'},function(result){
		$("#content_body").html(result);
		removePopupScrollClass();
		});
	}
	function saveAvailableSelection()
	{
		var szValue = $('#szSelectionAvailable').val();
		var id		= $('#iSelection').val();
		var iLanguage = $('#iLanguage').val();
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_locationDescription.php",{mode:'LOCATION_DESCRIPTION',szSelectionAvailable:szValue,iSelection:id,iLanguage:iLanguage},function(result){
		var n = result.split('|||||');
		if(n[0]=='ERROR')
		{
			$("#show-Err").html(n[1]);
		}
		if(n[0]=='SUCCESS')
		{
			$("#show-Err").html('');
			$("#showAvailableSelections").html(n[1]);
		}
		});
	}
	function selectAvailableSelection(id,tr_id)
	{
	
		var rowCount = $('#select_selection tr').length;

		if(rowCount>0)
		{
			for(x=1;x<=rowCount;x++)
			{
				new_tr_id="select_option"+x;
				if(new_tr_id!=tr_id)
				{
					$("#"+new_tr_id).attr('style','');
				}
				else
				{
					$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		$('#saveEdit').removeAttr('style');
		$('#saveEdit').html('<span>EDIT</span>');
		$('#saveEdit').attr('onclick','selectAvailable('+id+')');
	}
	function selectAvailable(id)
	{
		$('#saveEdit').html('<span>SAVE</span>');
		$('#saveEdit').attr('onclick','saveAvailableSelection()');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_locationDescription.php",{mode:'SELECT_LOCATION_DESCRIPTION',iSelection:id},function(result){
		var n = result.split('|||||');
		$('#szSelectionAvailable').attr('value',n['1']);
		$('#iSelection').attr('value',n['0']);
		});
		//$('#'+this_id).css({'background':'#DBDBDB','color':'#828282'});
	}
	function addMe()
	{
			
		var id		= parseInt($('#iSelection').val());
		if(id==0)
		{
			$('#saveEdit').html('<span>ADD</span>');
			$('#saveEdit').removeAttr('style');
			$('#saveEdit').attr('onclick','saveAvailableSelection()');
		}
	}
	
	function resend_messages(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idMessage:id,sendflag:'Resend'},function(result){
		$("#content_body").html(result);
		$("#content_body").attr("style","display:block;");
		addPopupScrollClass();
		});
	}
	
	function privew_messages(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idMessage:id,sendflag:'Privew'},function(result){
		$("#show_message_preview").html(result);
		$("#show_message_preview").attr("style","display:block;");
		//addPopupScrollClass();
		});
	}
	
	
	function select_message_send_tr(tr_id,id)
	{
		var rowCount = $('#send_message_list tr').length;
		if(rowCount>0)
		{
			for(x=1;x<rowCount;x++)
			{
				new_tr_id="message_data_"+x;
				if(new_tr_id!=tr_id)
				{
					$("#"+new_tr_id).attr('style','');
				}
				else
				{
					$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		
		$('#preview_button').attr('onclick','privew_messages("'+id+'")');
		$('#preview_button').css('opacity','1');
	}
	
	
	function resend_message_to_user()
	{	
		$('#send_button').attr('onclick','');
		$('#send_button').css('opacity','0.4');
		
		$('#cancel_button').attr('onclick','');
		$('#cancel_button').css('opacity','0.4');
		
		var idMessage=$('#idMessage').val();
		var szEmail=$('#szEmail').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idMessage:idMessage,szEmail:szEmail,sendflag:'Confirm'},function(result){
		$("#content_body").html(result);
		
		removePopupScrollClass();
		});
	}
	function selectEdit(id,language_flag)
	{
            if(language_flag==1)
            {
                id = $("#selectEditor").val();
            }
    
            if(id>0 && id!='')
            {
                $('#editTemp').html('<span>Edit</span>');
                $('#editTemp').removeAttr('style');
                $('#editTemp').attr('onclick','template()');
                $('#previewTemp').css('opacity','0.4');
                $('#previewTemp').removeAttr('onclick');
                $('#showdetails').html('');
                $('#Error-Log').html('');
            }
            else
            {
                $('#editTemp,#previewTemp').css('opacity','0.4');
                $('#editTemp,#previewTemp').removeAttr('onclick');
                $('#showdetails').html('');
                $('#Error-Log').html('');
            }
	}
	function template()
	{
		var id	= $('#selectEditor').val();
		var iLanguage = $('#iLanguage').val();
		$('#previewTemp').removeAttr('style');
		$('#previewTemp').attr('onclick','previewTemplate()');
		$('#editTemp').html('<span>Save</span>');
		$('#editTemp').attr('onclick','saveTemplate()');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_templateEmail.php",{flag:'SHOW_CONTENT',id:id,iLanguage:iLanguage},function(result){
			$('#showdetails').html(result);
		});
	}
	function previewTemplate()
	{
            //var texts = $('#szDescription').val();
            var szSubject = $('#szSubject').val();
            
            var content = NiceEditorInstance.instanceById('szDescription').getContent();
            var texts = encode_string_msg_str(content);
            
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_templateEmail.php",{flag:'SHOW_CONTENT_MESSAGE',message:texts,szSubject:szSubject},function(result){
                $('#previewText').html('<div>'+result+'</div>');
            });  
	}
	function saveTemplate()
	{
            var serialized_form_data = $('#saveEmailTemplate').serialize(); 
            var content = NiceEditorInstance.instanceById('szDescription').getContent();
            var description = encode_string_msg_str(content);
            serialized_form_data = serialized_form_data + "&templateArr[szDescription]="+description;
            
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_templateEmail.php",serialized_form_data,function(result){
                
                if(result!='')
                {
                    $('#Error-Log').html(result);
                }
                else
                {
                    $('#Error-Log').html('');
                    $('#showdetails').html('');
                    //window.location.reload(true);
                    $('#editTemp').html('<span>Edit</span>');
                    $('#editTemp').removeAttr('style');
                    $('#editTemp').attr('onclick','template()');
                    $('#previewTemp').css('opacity','0.4');
                    $('#previewTemp').removeAttr('onclick');
                }
            });
	}
	function add_cfs_listing(id,td_id)
	{
			var value=decodeURIComponent($("#addEditWarehouse").serialize());
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",value,function(result){
			if(result!='')
			{
				$('#Error-Log').html(result);
			}
			else
			{
				$('#loader').css({'display':'block'});
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",$('#currentCFS').serialize(),function(result){
				var n = result.split('|||||');
				$('#lisingTop').html(n[0]);
				$('#detailsBottom').html(n[1]);
				$('#loader').css({'display':'none'});
				select_cfs_tr(td_id,id);
				});
			}
			});
	}
	function closed(id)
	{
		$('#'+id).css('display','none');
		window.location.href = __JS_ONLY_SITE_BASE__+"/guideForwarder/";
	}
	function checkLimit(id)
	{
		length	=	$('#'+id).val().length;
		value	=	400 -length;
		if(length<=400)
		{
			$('#char_left').html(value);
		}
		else
		{
			if(length > 400)
			{
				text	=	$('#'+id).val();
				textNew = 	text.substring(0,400);
				$('#char_left').html(0);
				$('#'+id).val(textNew);
			}
		}
	}
	function select_cfs_tr(tr_id,id)
	{
		var rowCount = $('#Cfs_list tr').length;
		if(rowCount>0)
		{
			for(x=1;x<rowCount;x++)
			{
				new_tr_id="Cfs_list_tr_"+x;
				if(new_tr_id!=tr_id)
				{
					$("#"+new_tr_id).attr('style','');
				}
				else
				{
					$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}

		$('#editCfsLocation ,#removeCfsLocation').css('opacity','');
		$('#editCfsLocation').attr('onclick','editCfs('+id+',"'+tr_id+'")');
		$('#removeCfsLocation').attr('onclick','removeCfs('+id+')');
	}
	function editCfs(id,tr_id)
	{	
		$('#add_edit_warehouse_button').removeAttr('onclick');
		$('#add_edit_warehouse_button').unbind('Click');
		$("#add_edit_warehouse_button").click(function(){encode_string("szPhoneNumber","szPhoneNumberUpdate");add_cfs_listing(id,tr_id)});
		$('#add_edit_warehouse_button').html('<span style="min-width: 79px;">Save</span>');
		$('#clear_cfs_location_button').html('<span style="min-width: 79px;">cancel</span>');
		$('#Error-Log').html('');
		if(parseInt(id)>0)
		{ 
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",{flag:'EDIT_fORWARDER_CFS',id:id},function(result){
			var obj = $.parseJSON(result);
			$('#idForwarder').attr('disabled','disabled');
			$('#idEditWarehouse').val(obj.id);
			$('#szCFSName').val(obj.szWareHouseName);
			$('#szAddressLine1').val(obj.szAddress);
			$('#szAddressLine2').val(obj.szAddress2);
			$('#szAddressLine3').val(obj.szAddress3);
			$('#szOriginPostCode').val(obj.szPostCode);
			$('#szOriginCity').val(obj.szCity);
			$('#szState').val(obj.szState);
			$('#szPhoneNumber').val(obj.szPhone);
			$('#szLatitude').val(obj.szLatitude);
			$('#szLongitude').val(obj.szLongitude);
			$('#idForwarder').val(obj.idForwarder);
			$('#idForwarderHidden').val(obj.idForwarder);
			$('#szCountry').val(obj.idCountry);
				$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",{flag:'SHOW_COUNTRIES_DETAILS',id:obj.id,idCountry:obj.idCountry,latitude:obj.szLatitude,longitude:obj.szLongitude},function(result){
				$('#bottom-form').html(result);	
				});
			});
			
		}	
	}
	function removeCfs(id)
	{
		if(parseInt(id)>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",{flag:'DELETE_fORWARDER_CFS',id:id},function(result){
			$('#ajaxLogin').html(result);
			});
		}
	}
	function removeCFS(id)
	{
		if(parseInt(id)>0)
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",{flag:'DELETE_fORWARDER_CFS_CONFIRM',id:id},function(result){
			if(result=='')
			location.reload();
			});
		}
	}
	function clear_form_data_warehouse(form_id)
	{
		if(form_id=='')
		{
			form_id ='addeditHaulageData';
		}
		$('INPUT:text,SELECT,hidden', '#'+form_id).val(''); 
		$('#idForwarder').removeAttr('disabled');
		$('#add_edit_warehouse_button').html('<span style="min-width: 79px;">add</span>');
		$('#clear_cfs_location_button').html('<span style="min-width: 79px;">clear</span>');
		$('#Error-Log').html('');
	}
	function forwarderIdValue(id)
	{
		var value = $('#idForwarder').val();
		$('#idForwarderHidden').val(value);
	}
function activate_city_field(idCountry)
{
	var country_id = parseInt(idCountry);
	if(country_id>0)
	{
		$("#szOriginPostCode").removeAttr('disabled');
		$("#szOriginPostCode").val('');
		$("#szOriginCity").val('');	
	}
	else
	{
		$("#szOriginPostCode").attr("disabled","disabled");
	}
}
function addNewUer(idForwarderRole,idForwarder)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",{role:idForwarderRole,idForwarder:idForwarder,flag:'ADD_USER'},function(result){		
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');		
		$("#szEmail").focus();
	});
}
function addNewForwaredrDetails()
{
	var form_value=decodeURIComponent($("#addForwarderContactInfo").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",form_value,function(result){		
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');		
	});
}

function update_forwarder_users(idForwarderContact,idForwarder)
{	addPopupScrollClass();
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",{idForwarderContact:idForwarderContact,flag:'EDIT_USER',idForwarder:idForwarder},function(result){		
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');		
	});
}
function update_forwarder_contact_detail()
{
	var form_value=decodeURIComponent($("#addForwarderContactInfo").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",form_value,function(result){		
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');		
	});
}

function delete_forwarder_contacts(idForwarderContact,idForwarder)
{	addPopupScrollClass();
	//var conf_flag = confirm(szConfirmMessage);
	//if(conf_flag)
	//{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",{idForwarderContact:idForwarderContact,flag:'DELETE_USER',idForwarder:idForwarder},function(result){		
			$("#ajaxLogin").html(result);
			$("#ajaxLogin").attr('style','display:block;');		
		});
	//}	
}
function confirm_delete_profile()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",$("#delete_forwarder_contact_form").serialize(),function(result){		
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');		
	});
}
function add_forwarder_contact_detail()
{	addPopupScrollClass();
	var form_value=decodeURIComponent($("#addForwarderContactInfo").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_myCompany.php",form_value,function(result){		
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style','display:block;');		
	});
}
function showPreferences(id,mode)
{
	if(mode == 'preferences' )
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:id,mode:'DISPLAY_EMAIL_ADDRESS'},function(result){		
			$("#content_body").html(result);
		});
	}
}
function edit_preferences_systems(id)
{
	
$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:id,mode:'EDIT_SYSTEM_CONTROL'},function(result){
	$("#content_body").html(result);
});
}
function edit_preferences_emails(idForwarder)
{
$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:idForwarder,mode:'EDIT_EMAIL_ADDRESS'},function(result){
$("#content_body").html(result);	
});
}
function submit_preference_emails_details()
{	
var check=$("#removeMails").val();	
$.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",$("#ajax_preferences_email").serialize(),function(result){
$("#content_body").html(result);
$("#removeMails").attr('value',check);
});
}
function show_preference_emailss(idForwarder)
{	
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:idForwarder,mode:'DISPLAY_EMAIL_ADDRESS'},function(result){
        $("#content_body").html(result);	
    });
}
function submit_preference_control(flag)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php?confirm_flag="+flag,$("#ajax_prefrence_control").serialize(),function(result){
        //$("#ajaxLogin").html(result);
        var n = result.split('|||||');
        if(jQuery.trim(n['0'])=='SUCCESS')
        {
            $("#content_body").html(n['1']);
            if(flag==1)
            {
                removeInnerContent('ajaxLogin');
            }
        }
        else if(jQuery.trim(n['0'])=='CONFIRM')
        {
            $("#ajaxLogin").html(n['1']);
        }
    });
}
function show_preference_control(idForwarder)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_preferences.php",{id:idForwarder,mode:'VIEW_SYSTEM_CONTROL'},function(result){
        $("#content_body").html(result);
    });
}
function showCompanyInformation(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_companyInformation.php",{id:id},function(result){
$("#content_body").html(result);	
});
}
function setAdminForwarder()
{
	adminForwarder = 1;
}
function showForwarderBAnkDetails(id)
{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bankDetails.php",{id:id,mode:'DISPLAY_FORWARWARDER_BANK_DETAILS_ACCESS'},function(result){
$("#content_body").html(result);	
});
}
function setting_pricing(id,div_id,mode)
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:'settings_pricing'},function(results){		
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_setting_pricing.php",{id:id,mode:mode},function(result){
	$("#content_body").html(results);
	$("#"+div_id).html(result);	
	});
	});
}
function viewSettingPricing(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:'settings_pricing'},function(result){
	$("#content_body").html(result);	
	});
}
function submit_setting_pricing(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_setting_pricing.php",$('#pricingAndSetting').serialize(),function(result){
	var n = result.split('|||||');
	if(jQuery.trim(n[0])=='ERROR')
	$("#error_log").html(n[1]);	
	else if(jQuery.trim(n[0])=='SUCCESS')
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{id:id,flag:'settings_pricing'},function(result){
		$("#content_body").html(result);	
		});
	}
	});
}
function hideEditForwarderProfile()
{
	$('#ajaxLogin').html('');
}
function viewAjax(mode)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:mode},function(result){
	$("#content_body").html(result);	
	});
}
function editCustomerInformation(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",{showflag:'user_info',id:id},function(result){
	$("#myaccount_info").html(result);});
}
function cancel_customer_info(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",{id:id},function(result){
	$("#myaccount_info").html(result);});
}
function edit_customer_contact_information(id)
{	

	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",{showflag:'contact_info',id:id},function(result){
	$("#myaccount_info").html(result);});
}
function change_customer_password(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",{showflag:'change_pass',id:id},function(result){
	$("#myaccount_info").html(result);});
}
function update_customerinfo_details()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",$("#updateUserInfo").serialize(),function(result){
	$("#myaccount_info").html(result);});
}
function edit_customer_contact_info()
{
	var value=decodeURIComponent($("#edit_contact_information_data").serialize());
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",value,function(result){
	$("#myaccount_info").html(result);});
}
function changeCustomerPassword()
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",$("#changePassword").serialize(),function(result){
	$("#myaccount_info").html(result);});
}
function sel_line_waiting_for_cost_approval_admin(div_id,id,butttonName,editbutton,deletebutton)
{
	var rowCount = $('#wating_for_cost_approved_by_management__TOP__ tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="wating_for_cost___TOP__"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
			$("#submit_files").unbind("click");
			$("#submit_files").attr('style',"opacity:0.4");
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'FIND_SUBMIT_STATUS'},function(result){
				if(result==1)
				{
					$("#submit_files").click(function(){submit_processing_cost(id)});
					$("#submit_files").attr('style',"opacity:1");
				}
			});
			
			$('#iRecords').attr('disabled',true);
			$('#szEstimatedTime').attr('disabled',true);
			$('#szProcessingCost').attr('disabled',true);
			
			$('#iRecords').attr('value','');
			$('#szEstimatedTime').attr('value','');
			$('#szProcessingCost').attr('value','');
			
			$('#szRecord_type').attr('style','display:none');
			$('#duration').attr('style','display:none');
			$('#szcurrency_value').attr('style','display:none');
	
			$("#delete_cancel").html(deletebutton);
			$("#edit_name").html(editbutton);
			$("#delete_files").unbind("click");
			$("#view_files").unbind("click");
			$("#edit_files").unbind("click");
			$("#delete_files").click(function(){delete_processing_cost(id)});
			$("#view_files").click(function(){view_processing_file(id)});
			$("#edit_files").click(function(){edit_processing_cost(id,butttonName)});
			$("#delete_files,#edit_files,#view_files").attr('style',"opacity:1");
			
}
function edit_processing_cost(id,butttonName)
{
	$("#edit_files").html("<span id='edit_name'>Save</span>");
	$("#delete_files").css('opacity','0.4');
	$("#delete_files").unbind("click");
	$("#edit_files").unbind("click");
	
	//$("#delete_files").click(function(){delete_processing_cost(id)});
	$("#edit_files").click(function(){save_processing_cost(id)});
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'EDIT_PRICE_DETAILS'},function(result){
	$("#EstimationComtent").html(result);
	$('#mode').attr('value','SAVE_PRICE_DETAILS');
	$('#iRecords').attr('disabled',false);
	$('#szEstimatedTime').attr('disabled',false);
	$('#szProcessingCost').attr('disabled',false);
	$('#duration').attr('style','display:blocks');
	$("#submit_files").unbind("click");
	$("#submit_files").attr('style',"opacity:0.4");
	$("#delete_cancel").html(butttonName);
	$("#delete_files").unbind("click");
	$("#delete_files").click(function(){cancel_processing_cost()});
	$("#delete_files").attr('style',"opacity:1");
	
	});
}
function save_processing_cost()
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",$("#EstimationComtent").serialize(),function(result){
	var n = result.split('|||||'); 
	var check = jQuery.trim(n[0]);
	if(check=='ERROR')
	{
		$("#Error").html(n[1]);
	}
	else if(check=='SUCCESS')
	{
		window.location.reload();
	}
	});
}
function view_processing_file(id)
{
	addPopupScrollClass('open_processing_popup');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'VIEW_FILE'},function(result){
		$("#open_processing_popup").html(result);
		$("#open_processing_popup").attr('style',"display:block");
	});
}
function download_uploaded_file_admin(filename)
{
	var iframe = window.parent.document.getElementById('hssiframe');
	iframe.src=__JS_ONLY_SITE_BASE__+'/ajax_servicing_url.php?filename='+filename+'&mode=download_file';
}
function submit_processing_cost(id)
{
	$("#delete_files").unbind("click");
	$("#delete_files").css('opacity','0.4');
	
	$("#view_files").unbind("click");
	$("#view_files").css('opacity','0.4');
	
	$("#edit_files").unbind("click");
	$("#edit_files").css('opacity','0.4');
	
	$("#submit_files").unbind("click");
	$("#submit_files").css('opacity','0.4');
	
	$("#loader").attr('style','display:block');
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'SUBMIT_PROCESSING_COST'},function(result){
	var re = jQuery.trim(result);
	$("#loader").attr('style','display:none');
	if(re=='')
	{
		window.location.reload();			
	}	
	});
}
function bot_line_waiting_for_cost_approval_admin(div_id,id)
{
	var rowCount = $('#wating_for_cost_approved_by_management__BOTTOM__ tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="wating_for_cost___BOTTOM__"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
			
			
			$("#delete_files_forwarder_awaiting").unbind("click");
			$("#move_files").unbind("click");
			$("#delete_files_forwarder_awaiting").click(function(){delete_processing_cost(id)});
			$("#move_files").click(function(){move_back_processing_cost(id)});
			$("#move_files,#delete_files_forwarder_awaiting").attr('style',"opacity:1");
			
}
function move_back_processing_cost(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'MOVE_BACK_SERVICE_CHECK'},function(result){
	$('#ajaxLogin').html(result);	
	});
}
function move_back_processing_cost_confirm(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'MOVE_BACK_SERVICE'},function(result){
	var re = jQuery.trim(result);
	if(re=='')
	{
		window.location.reload();			
	}	
	});
}
function waiting_line_waiting_for_cost_approval_admin(div_id,id,szFilename)
{
	var rowCount = $('#wating_for_cost_approved_by_management__WTOP__ tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="wating_for_cost___WTOP__"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
		$("#submit_files").unbind("click");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'CHECK_ACTUAL_RECORDS'},function(result){
		var re = jQuery.trim(result);
		if(re == 1)
		{
			$("#submit_files").click(function(){submit_working_file(id)});
			$("#submit_files").css('opacity','1');
		}
		else
		{
			$("#submit_files").css('opacity','0.4');	
		}
		});
		$("#view_records").unbind("click");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'CHECK_RECORDS_LISTING'},function(result){
		var re = jQuery.trim(result);
		if(re == 1)
		{
			$("#view_records").click(function(){download_working_record_file(szFilename)});
			$("#view_records").css('opacity','1');
		}
		else
		{
			$("#view_records").css('opacity','0.4');	
		}
		});
			$("#view_files").unbind("click");
			$("#move_files").unbind("click");
			$("#fileID").attr('value',id);
			$("#upload").css("display",'block');
			$("#button_upload").css("opacity",'1');
			$("#view_files").click(function(){view_processing_file(id)});
			$("#move_files").click(function(){move_working_file(id)});
			$("#view_files,#move_files").attr('style',"opacity:1");
			
}
function submit_working_file(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'COMPLETE_SUBMIT_RECORDS'},function(result){
		$("#ajaxLogin").html(result);
	});
}
function complete_submission(id)
{
	$("#complete_submission_confirm").attr("onclick","");
	$("#complete_submission_confirm").attr("style","opacity:0.4");
	
	$("#complete_submission_cancel").attr("onclick","");
	$("#complete_submission_cancel").attr("style","opacity:0.4");
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'COMPLETE_SUBMIT_CONFIRMED'},function(result){
	var re = jQuery.trim(result);
	if(re=='')
	{
		window.location.reload();			
	}	
	});
}
function move_working_file(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'MOVE_BACK_SUBMISSION'},function(result){
	$("#ajaxLogin").html(result);
	});
}
function move_back_processing_cost_submission(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'MOVE_BACK_SUBMISSION_CONFIRMED'},function(result){
	var re = jQuery.trim(result);
	if(re=='')
	{
		window.location.reload();			
	}	
	});
}
function upload_working_file()
{
	var id = $("#fileID").val();
	if(id>0)
	{
	$("#loader").attr('style','display:block;');
	document.uploadFile.submit();
	//$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",$('#uploadFile').serialize(),function(result){
	//$("#ajaxLogin").html(result);
	//});
	}
}
function comp_line_waiting_for_cost_approval_admin(div_id,id,filename)
{
	var rowCount = $('#wating_for_cost_approved_by_management__WBOTTOM__ tr').length;
	if(rowCount>0)
	{
		for(t=1;t<rowCount;++t)
		{
			newdivid="wating_for_cost___WBOTTOM__"+t;
			if(newdivid!=div_id)
			{
				$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
			}
			else
			{
				$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
			}
		}
	}
			$("#view_file").unbind("click");
			$("#view_record").unbind("click");
			$("#move_file").unbind("click");
			$("#delete_file").unbind("click");
			$("#view_file").click(function(){view_processing_file(id)});
			$("#view_record").click(function(){download_working_record_file(filename)});
			$("#move_file").click(function(){move_working_files(id)});
			$("#delete_file").click(function(){delete_processing_cost(id)});
			$("#view_file,#view_record,#move_file,#delete_file").attr('style',"opacity:1");
}
function delete_processing_cost(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'DELETE_POPUP'},function(result){
	$('#ajaxLogin').html(result);
	addPopupScrollClass('ajaxLogin');
	});	
}
function delete_confirm(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'DELETE_POPUP_CONFIRM'},function(result){
		var re = jQuery.trim(result);
		if(re=='')
		{
			window.location.reload();			
		}	
	});	
}
function download_working_record(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'VIEW_RECORDS'},function(result){
	$("#ajaxLogin").html(result);
	})
}
function download_working_record_file(filename)
{
	var iframe = window.parent.document.getElementById('hiframe');
	iframe.src=__JS_ONLY_SITE_BASE__+'/ajax_servicing_url.php?filename='+filename+'&mode=download_record_file';
}
function move_working_files(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'MOVE_BACK_COMPLETE'},function(result){
	var re = jQuery.trim(result);
	if(re=='')
	{
		window.location.reload();			
	}	
	});
}
function openCommentDescription(id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,mode:'OPEN_COMMENTS'},function(result){
	$("#ajaxLogin").html(result);
	})
}
function add_comment_bulk_upload(id)
{
	var szComment=$("#szComment").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_servicing_url.php",{id:id,szComment:szComment,mode:'add_comment'},function(result){
	$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr('style',"display:block");
	    $("#szComment").focus();   
	});
}
function changePasswordCustomerContact(idCustomer)
	{
		$("#forgotPassword").removeAttr("onclick");
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_forwarderProfile.php",{flag:"SEND_CHANGE_PASSWORD_EMAIL",idForwarder:idForwarder},function(result){	
				if(jQuery.trim(result)=='SEND')
				{	
					$("#forgotPassword").css({"color":"#000","text-decoration":" none"});
					$("#forgotPassword").html("<i>New password successfully sent</i>");
					$("#regError").html('');
				}
				else
				{
					$("#forgotPassword").attr("onclick","changePasswordForwarderContact("+idForwarder+")");
				}	
		});
	}
	function send_new_password_customer(id)
	{	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{flag:"editPassword",idUser:id},function(result){	
		if(jQuery.trim(result)!='')
		{
			$('#ajaxLogin').html(result);	
		}
			
		});
		
	}
	function send_new_password_customer_confirm(id)
	{	
		$('#ajaxLogin').html('');
		$('#customerSend').html('<u>Send user new password</u>');	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_userDetails.php",{flag:"changePassword",idUser:id},function(result){	
		if(jQuery.trim(result)=='')
		{
			$('#customerSend').html('New password sent successfully');
			$('#customerSend').css({cursor:'text'});	
		}
			
		});
		
	}
	function editManagementVar(mode)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_manageVar.php",{mode:mode},function(result){	
		$('#content').html(result);
			
		});
	}
	function updateVariablesDetails()
	{	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_manageVar.php",$('#manageVar').serialize(),function(result){	
		$('#content').html(result);
			
		});
	}
	function cancelVariablesDetails()
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_manageVar.php",{mode:''},function(result){	
		$('#content').html(result);
			
		});
	}
	function showPage(page)
	{
		var country = $('#szHiddenCountry').val();
		var city = $('#szHiddenCity').val();
		var postcode = $('#szHiddenPostCode').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'POSTCODE_CONTENT_NEXT_PAGE',country:country,city:city,postcode:postcode,page:page},function(result){
				$('#content').html(result);	
			});	
	}
	function save_rss_feed()
	{	
			var content   = tinyMCE.get('szDescription');
			var szHeading = $('#szHeading').val();
			var szUrl	  = $('#szUrl').val();
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_rss.php",{content:content.getContent(),szHeading:szHeading,szUrl:szUrl},function(result){
			var result_arr = result.split('|||||');
			if(jQuery.trim(result_arr[0])=='SUCCESS')
			{
				window.location.href = window.location.href;
			}
			else
			{
				$('#Error').html(result_arr[1]);
			}
			});  	
	}
	function resend_activation_code(idUser)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_CustomerProfile.php",{action:'RESEND_CODE',id:id},function(result){
				$('#content').html(result);	
			});	
	}
	function submit_currentWTW_form()
	{
		$('#loader').css({'display':'block'});
		//$("#bottomFormEdit input[type=text]").attr('value',"");
		//$("#bottomFormEdit select").val('0');
		//$("#bottomFormEdit input[type=text],#bottomFormEdit select").attr('disabled',true);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",$('#currentWTW').serialize(),function(result){
		$('#loader').css({'display':'none'});
		$('#bottomForm').html(result);
		$('#editButton').css({opacity:'0.4'});
		$('#bottomFormEdit').html('');
			});	
	}
	function showBottomTableToEditWTWContent(div_id,id)
	{
		$('#loader').css({'display':'block'});
		//$('#canvas').html('');
		select_div_id(div_id);
		$('#editButton').css({
			opacity:'1'
			});
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",{idWarehouse:id},function(result){
				$('#bottomFormEdit').html(result);	
				$('#loader').css({'display':'none'});
				$('#save_lcl').css({opacity:'0.4'});
				$('#save_lcl').removeAttr('onclick');
				$("#bottomFormEdit input[type=text],#bottomFormEdit select").attr("disabled", true);
			});
			$('#editButton').unbind('click');
			$('#editButton').click(function(){editBottomTable(id,div_id)});
	}
	function editBottomTable(id,idTable)
	{	
		$('#loader').css({'display':'block'});
		//$('#editButton').css({opacity:'0.4'});
		//$('#editButton').unbind('click');
		$("#bottomFormEdit input,#bottomFormEdit select").attr("disabled", false);
		$('#idAvailableDay').attr("disabled", true);
		$('#save_lcl').css({opacity:'1'});
		$('#save_lcl').click(function(){update_obo_lcl_data('Get Data','Change',id,idTable)});
		$('#loader').css({'display':'none'});
	}
	function select_div_id(div_id)
	{
		var rowCount = $('#usertable tr').length;
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="showEachContent"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
	}
	function update_obo_lcl_data(text,text2,id,idTable)
	{
		$('#loader').css({'display':'block'});
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",$("#obo_lcl_bottom_form").serialize(),function(result){
			
			result_ary=result.split("||||");
			result_ary[0]=jQuery.trim(result_ary[0]);
			if(result_ary[0]=='SUCCESS')
			{
				submit_currentWTW_formAjax(idTable,id);
				/*$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",{idWarehouse:id},function(result){
				$('#bottomFormEdit').html(result);	
				$('#save_lcl').css({opacity:'0.4'});
				$('#save_lcl').removeAttr('onclick');
				$("#bottomFormEdit input[type=text],#bottomFormEdit select").attr("disabled", true);
				});*/
			
			}
			else
			{
				$("#obo_lcl_error").html(result_ary[1]);
				$("#obo_lcl_error").attr('style','display:block;');
				$('#loader').css({'display':'none'});
			}	
		});
	}
	function cancel_obo_lcl(id)
	{
		$('#bottomFormEdit').html('');
		select_div_id(id);
	}
	function resetDetails(mode,value,iWarehouseType)
	{
		$('#usertable tr[2]').html('');
		if(mode == 'idForwarder')
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",{mode:'FORWARDER',id:value,iWarehouseType:iWarehouseType},function(result){
				if(jQuery.trim(result)!='')
				{	
					var result_spl = result.split('|||||');
					$('#idOriginCountry').find('option').remove().end().append(result_spl[0]);
					$('#idDestinationCountry').find('option').remove().end().append(result_spl[1]);
				}
				else
				{
					$('#idOriginCountry').find('option').remove().end().append('<option>All</option>');
					$('#idDestinationCountry').find('option').remove().end().append('<option>All</option>');
				}
			});
		}
		if(mode == 'idOriginCountry')
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",{mode:'ORIGIN',id:value},function(result){
				if(jQuery.trim(result)!='')
				{	
					var result_spl = result.split('|||||');
					//$('#idForwarder').find('option').remove().end().append(result_spl[0]);
					$('#idDestinationCountry').find('option').remove().end().append(result_spl[1]);
				}
				else
				{
					//$('#idForwarder').find('option').remove().end().append('<option>No content</option>');
					$('#idDestinationCountry').find('option').remove().end().append('<option>All</option>');
				}
			});
		}
		if(mode == 'idDestinationCountrys')
		{		//if(mode == 'idDestinationCountry')
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",{mode:'DESTINATION',id:value},function(result){
			if(jQuery.trim(result)!='')
				{	
					var result_spl = result.split('|||||');
					//$('#idForwarder').find('option').remove().end().append(result_spl[0]);
					//$('#idOriginCountry').find('option').remove().end().append(result_spl[1]);
				}
				else
				{
					//$('#idForwarder').find('option').remove().end().append('<option>No content</option>');
					//$('#idOriginCountry').find('option').remove().end().append('<option>No content</option>');
				}
			});
		}
	}
	
	function cancel_booking(id,mode)
	{
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_cancelBooking.php",{id:id,flag:'open_cancel_booking_popup',mode:mode},function(result){
                $('#cancel_booking_popup').html(result);
                    $("#cancel_booking_popup").attr('style','display:block');	
            });	
	}
	
	function close_cancel_booking_popup()
	{
		$("#cancel_booking_popup").attr('style','display:none');
	}
	
	
	function cancel_booking_data()
	{
		var value=$("#cancelBooking").serialize();
		var page = $("#page").attr('value');
                var limit = $("#limit").attr('value');
                var mode=$("#mode").attr('value');
		var newvalue=value+'&flag=cancel_booking_data&page='+page+"&limit="+limit;
		$("#cancel_booking_no").removeAttr('onclick');	
		$("#cancel_booking_yes").removeAttr('onclick');	
		
		$("#cancel_booking_yes").attr('style','opacity:0.4');
		$("#cancel_booking_no").attr('style','opacity:0.4');	
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_cancelBooking.php",newvalue,function(result){
			 	result_ary = result.split("||||");
				if(jQuery.trim(result_ary[0])=='ERROR')
		        {
		        	$("#regError").css('display','block'); 
		        	$("#regError").html(result_ary[1]);
		        	
		        	$("#cancel_booking_no").attr('onclick','close_cancel_booking_popup()');	
					$("#cancel_booking_yes").attr('onclick','cancel_booking_data()');	
		        }
		        if(jQuery.trim(result_ary[0])=='SUCCESS')
		        {
                            if(mode=='PENDING_TRAY')
                            {
                                $('#pending_task_tray_container').html(result_ary[2]);
                                $('#pending_task_overview_main_container').html(result_ary[1]);
                                $("#cancel_booking_popup").attr('style','display:none;');
                            }
                            else
                            {
                                $('#cance_booking_html').html(result_ary[1]);
                                $("#cancel_booking_popup").attr('style','display:none;');	
                            }
		        }				
			});	
	}
	
	function submit_forwarder_bank_details(idForwarder)
	{
		$("#loader").attr('style','display:block;');
		$("#forwarder_company_div").attr('style','display:none;');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_bankDetails.php",$("#updateRegistBankForm").serialize(),function(result){
			
			if(jQuery.trim(result)=='SUCCESS')
			{
				show_forwarder_bank_details(idForwarder);
				//show_forwarder_top_messges();
			}
			else
			{
				$("#forwarder_bank_details").html(result);
			}
			$("#loader").attr('style','display:none;');
		});
	}
	function showComment(comment)
	{
		var comment = jQuery.trim(comment); 
		if(comment!='')
		{
			
		}
	}
	function showPriorityBasedList()
	{
		$('#contactPopup').html('');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'PRIORITY'},function(result){
			$('#contactPopup').html(result);
		});	
	}
	function sort_priority(szBy,szMode,id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'SORT_PRIORITY',by:szBy,mode:szMode},function(result){
			$('#contactPopup').html(result);
			if(szMode == 'DESC')
			{
				mode = 'ASC';
				arrow= 'sort-arrow-down';	
			}
			else
			{
				mode = 'DESC';
				arrow= 'sort-arrow-up';	
			}
			$('#'+id).html('<span onclick="sort_priority(\''+szBy+'\',\''+mode+'\',\''+id+'\');" class="'+arrow+'">&nbsp;</span>');
			if(id== 'city')
			{
				$('#country').html('<span class="sort-arrow-down-grey" onclick="sort_priority(\'szCountryName\',\'DESC\',\'country\');"> </span>');
			
			}
		});	
	}
	function deletePriority(div_id,value,country,city)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'DELETE_PRIORITY',value:value,country:country,city:city,div_id:div_id},function(result){
			$('#contactPopup').css({display:'none'});
			$('#ajaxLogin').html('');
			$('#ajaxLogin').html(result);
		});	
	}
	function removePriority(id,div_id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_postcode.php",{action:'DELETE_PRIORITY_CONFIRM',id:id},function(result){
			if(jQuery.trim(result)=='SUCCESS')
			{
				$('#'+div_id).remove('');
				cancelDeletePriority();
			}
		});	
	}
	function cancelDeletePriority()
	{
		$('#ajaxLogin').html('');
		$('#contactPopup').css({display:'block'});
	}
	function showCustomClearence(dev_id,exports,imports)
	{ 
		var rowCount = $('#customClearence tr').length;
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="cc_"+t;
				if(newdivid!=dev_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+dev_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		$('#obo_cc_bottom_span input[type=text]').attr('value','');
		$('#obo_cc_bottom_span input[type=text],#obo_cc_bottom_span select').attr('disabled',true);
		$("#obo_cc_bottom_span select").val('0');
		$('#editCustomClearence').css('opacity','0.4');
		$('#editCustomClearence').unbind('onclick');
		$('#editCustomClearence').unbind('click');
		$('#editCustomClearence').css({opacity:'1'});
		$('#editCustomClearence').click(function(){ editCustomClearence(exports,imports,dev_id)});	
		$('#idSelect').val(dev_id);
	}
	function showCustomClearenceAdminSelect(dev_id,exports,imports)
	{ 
		$('#obo_cc_bottom_span input[type=text]').attr('value','');
		$('#obo_cc_bottom_span input[type=text],#obo_cc_bottom_span select').attr('disabled',true);
		$("#obo_cc_bottom_span select").val('0');
		$('#editCustomClearence').css('opacity','0.4');
		$('#editCustomClearence').unbind('onclick');
		$('#editCustomClearence').unbind('click');
		$('#editCustomClearence').css({opacity:'1'});
		$('#editCustomClearence').click(function(){ editCustomClearence(exports,imports,dev_id)});	
		$('#idSelect').val(dev_id);
		$("#"+dev_id).attr('style','background:#DBDBDB;color:#828282;');
	}
	function editCustomClearence(exports,imports,dev_id)
	{
		$('#loader').css({'display':'block'});
		$('#editCustomClearence').unbind('click');
		$('#editCustomClearence').css({opacity:'0.4'});
		var idWhFrom = $('#idOriginWarehouse').val();
		var idWhTo = $('#idDestinationWarehouse').val();
		var idSelectedField = $('#idSelect').val();
		$.post(__JS_ONLY_SITE_BASE__+'/ajax_oboDataExportCC.php',{mode:'UPDATE_CC_DETAILS',exportId:exports,importId:imports,div_id:dev_id},function(result){
		$('#obo_cc_bottom_span').html(result);
		$('#idSelect').val(idSelectedField);
		$('#loader').css({'display':'none'});
		});
	}
	function update_obo_cc_data(text,text2,div_id)
	{
		$('#loader').css({'display':'block'});
		var idWhFrom = $('#idOriginWarehouse').val();
		var idWhTo = $('#idDestinationWarehouse').val();
		var idSelectedField = $('#idSelect').val();
		
		var fOriginPrice = $('#fOriginPrice').val();
		var idOriginCurrency = $('#idOriginCurrency').find(":selected").text();
		var fDestinationPrice = $('#fDestinationPrice').val();
		var idDestinationCurrency = $('#idDestinationCurrency').find(":selected").text();
		if(idOriginCurrency=='Select')
		{
			var idOriginCurrency='';
		} 
		
		if(idDestinationCurrency=='Select')
		{
			var idDestinationCurrency='';
		}
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",$("#update_obo_cc_form").serialize(),function(result){
			result_ary=result.split("||||");
			if(jQuery.trim(result_ary[0])=='SUCCESS')
			{
				//showCheckedCustomClearence();
				$('#'+div_id+' td:nth-child(6)').html(idOriginCurrency);
				$('#'+div_id+' td:nth-child(7)').html(fOriginPrice);
				$('#'+div_id+' td:nth-child(8)').html(idDestinationCurrency);
				$('#'+div_id+' td:nth-child(9)').html(fDestinationPrice);
				$("#obo_cc_error").attr('style','display:none;');
				disableOBOTopForm(false,'',text,text2);
				$("#obo_cc_bottom_span").html('');
				$("#obo_cc_bottom_span").html(result_ary[1]);
				$("#obo_cc_getdata_button").focus();
				$("#text_change").html(text);
				$("#obo_cc_cc_getdata_button").unbind("click");
				//$("#obo_cc_cc_getdata_button").attr('onclick','submit_obo_cc_form("'+text2+'","'+text+'")');
				$("#obo_cc_cc_getdata_button").click(function(){submit_obo_cc_form(text2,text)});	
				//showCustomClearence('\''+idSelectedField+'\'',idWhFrom,idWhTo);
				//showCustomClearenceAdminSelect(idSelectedField,idWhFrom,idWhTo);
			}
			else
			{
				$("#obo_cc_error").html(result_ary[1]);
				$("#obo_cc_error").attr('style','display:block;');
				$("#obo_cc_error").focus();
			}	
			$('#loader').css({'display':'none'});
		});
	}
	function showexpiryCC (dev_id,id)
	{
		var rowCount = $('#customClearence tr').length;
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="cc_"+t;
				if(newdivid!=dev_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+dev_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		$('#editButton').html('<span>edit</span>');	
		$('#datepicker1').attr('value','');
		$('#datepicker2').attr('value','');
		$('#editButton').css({opacity:'1'});
		$('#editButton').unbind('click');
		$('#editButton').click(function(){editexpiryWTW(dev_id,id)});
	}
	
	function editexpiryWTW(dev_id,id)
	{
		$.post(__JS_ONLY_SITE_BASE__+'/ajax_expiringWTW.php',{mode:'SHOW_EXPIRY_WTW_DATE',id:id},function(result){
		var n = result.split('|||||');
		$('#datepicker1').attr('value',jQuery.trim(n[0]));
		$('#datepicker2').attr('value',jQuery.trim(n[1]));
		$('#editButton').html('<span>save</span>');	
		$('#editButton').unbind('click');
		$('#editButton').click(function(){saveDateExpiry(dev_id,id)});
		});
	}
	function saveDateExpiry(dev_id,id)
	{
		var value1 = $('#datepicker1').val();
		var value2 = $('#datepicker2').val();
		var id = id;
		$.post(__JS_ONLY_SITE_BASE__+'/ajax_expiringWTW.php',{mode:'SAVE_DATE_VALIDATION',id:id,value1:value1,value2:value2},function(result){
		var n =  result.split('|||||');
		if(jQuery.trim(n[0])=='SUCCESS')
		{
			$('#Error-log').html('');	
			$('#topTable').html(n[1]);
			$('#datepicker1').attr('value','');
			$('#datepicker2').attr('value','');
			$('#editButton').html('<span>edit</span>');	
			showexpiryCC (dev_id,id);
		}
		else if(jQuery.trim(n[0])=='ERROR')
		{	
			$('#Error-log').html(n[1]);	
		}
			
		});
	}
	function validateLandingPageForm_Management(formId)
{
	$("#loader").attr('style','display:block;');
	 $("#bottom_search_result").html('');
     $("#calculations_details_div").attr('style','display:none;');
     
    if(isFormInputElementHasDefaultValue('szOriginCity','Type name')) 
	{
       $("#szOriginCity").attr('value',"");
    }
    if(isFormInputElementHasDefaultValue('szOriginPostCode','Optional') || isFormInputElementHasDefaultValue('szOriginPostCode','Type code')) 
	{
        $("#szOriginPostCode").attr('value',"");
    }    
    if(isFormInputElementHasDefaultValue('szDestinationCity','Type name')) 
	{
       $("#szDestinationCity").attr('value',"");
    }
    if(isFormInputElementHasDefaultValue('szDestinationPostCode','Optional') || isFormInputElementHasDefaultValue('szDestinationPostCode','Type code')) 
	{
       $("#szDestinationPostCode").attr('value',"");
    }    
    if(isFormInputElementHasDefaultValue('datepicker1','dd/mm/yyyy'))
	{
       $("#datepicker1").attr('value',"");
    }
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_landingPage.php",$("#"+formId).serialize(),function(result){
    
    	 //show_forwarder_top_messges();		     
         result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {
         	$("#regError").attr('style','display:block'); 
         	$("#regError").html(result_ary[1]);
         	window.scrollTo(50,50);
         }
         if(jQuery.trim(result_ary[0])=='MULTI_EMPTY_POSTCODE')
         {
         	addPopupScrollClass();
         	$("#Transportation_pop").attr('style','display:block');
         	$("#regError").attr('style','display:none;');
         	$("#Transportation_pop").html(result_ary[1]);
         }
         if(jQuery.trim(result_ary[0])=='MULTIREGIO')
         {         	
         	$("#regError").attr('style','display:none;');
         	$("#Transportation_pop").html(result_ary[1]);
         	$("#Transportation_pop").attr('style','display:block;');
         }         
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
         	$("#regError").attr('style','display:none;');
         	$("#bottom_search_result").html(result_ary[1]);
         	$("#bottom_search_result").attr('style','display:block;');
         }
         $("#loader").attr('style','display:none;');
        
    });      
    return false;      
}
	function showCountriesByCountryForManagement(idCountry)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",{mode:'SHOW_MULTIPLE_COUNTRIES_SELECT',idCountry:idCountry},function(result){
		$('#bottom-form').html(result);
		$('#CFSListing').attr('value',idCountry);	
			});
	}
	function addCfsCountries(id)
	{
		var selectedCountries = $('#CFSListing').val();
		var selectedFormCountries =	$('#'+id).val();
		 $('#'+id+' option:selected').remove();
		 $('#'+id).val($('#'+id+' option:first').val());
		var toBeSelected = selectedCountries+','+ selectedFormCountries;
		$('#CFSListing').attr('value',toBeSelected);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",{mode:'SELECT_COUNTRIES_FORM_LIST',content:toBeSelected},function(result){
			$('#selectedCountries').html(result);
			});
	}
	function removeCfsCountries(id,country,latitude,longitude)
	{
		$('#'+id+' option:selected').remove();
		var options = document.getElementById(id).options;
		var values = [];
		var i = 0, len = options.length;
		while (i < len)
		{
		  values.push(options[i++].value);
		}
		selectedCountriesToRemove = values.join(', ');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",{mode:'REMOVE_COUNTRIES_FROM_LIST',content:selectedCountriesToRemove,country:country,latitude:latitude,longitude:longitude},function(result){
			$('#'+id+' option:selected').remove();
			$('#CFSListing').attr('value',selectedCountriesToRemove);
			$('#addFormList').html(result);
			});
	}
	function haulageListing()
	{
		var check = $('#chunchHaulage').is(":checked");
		var status;
		if( check == true)
		{
			status = 0;
		}
		else if(check == false)
		{
			status = 1;
		}
		$('#model_list').html('');
		$('#try_it_out').html('');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationHaulage.php",{mode:'SHOW_CHANGED_LISTING',status:status},function(result){
			$('#frameHaulage').html(result);
			$('#chunchHaulage').attr('checked',!status);	
		});	
	}
	function selectHaulageField(div_id,id,priority,iActive,idCountry,szCityName,idWarehouse,idDirection)
	{
		var idWarehouseOld = $('#idWarehouse').val();
		var idDirectionOld = $('#idDirection').val();
		
		globalTrHaulageid = div_id;
		$('#makeHaulageActivated').html('<span>ACTIVE</span>');
		$('#model_list').html('');
		
		if(jQuery.trim(idDirectionOld) =='' || idWarehouseOld=='undefined' ||  idDirectionOld != idDirection || idWarehouseOld != idWarehouse || iActive!='1')
		{
			$('#try_it_out').html('');
		}
		
		var rowCount = $('#haulageDetails tr').length;
		if(rowCount>0)
		{
			for(t=1;t<rowCount;++t)
			{
				newdivid="show_haulage_details_"+t;
				if(newdivid!=div_id)
				{
					$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
				}
				else
				{
					$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		
		iActive = parseInt(iActive);
		
		$('#makeHaulageActivated, #moveHauageDown, #moveHauageUp, #haulageEdit').css({opacity:'0.4'});
		$('#makeHaulageActivated, #moveHauageDown, #moveHauageUp, #haulageEdit').unbind('click');
		if(iActive == true)
		{
			$('#makeHaulageActivated').css({opacity:'1'});
			$('#makeHaulageActivated').html('<span>INACTIVATE</span>');
			
			priority = parseInt(priority);
			if( priority >=1 && priority <= 4 )
			{
				var flagdown = 0;
				var flagup = 1;
				
				$('#moveHauageDown').unbind('click');
				$('#moveHauageUp').unbind('click');
				
				if(priority == 1)
				{
					$('#moveHauageDown').css({opacity:'1'});
					$('#moveHauageDown').click(function(){changePriority(id,flagdown)});
				}
				else if( priority == 2 ||  priority == 3 )
				{	
					
					$('#moveHauageDown').css({opacity:'1'});
					$('#moveHauageDown').click(function(){changePriority(id,flagdown)});
					$('#moveHauageUp').css({opacity:'1'});
					$('#moveHauageUp').click(function(){changePriority(id,flagup)});
				}
				else if( priority == 4 )
				{	
					$('#moveHauageUp').css({opacity:'1'});
					$('#moveHauageUp').click(function(){changePriority(id,flagup)});
				}
				$('#haulageEdit').css({opacity:'1'});
				$('#haulageEdit').unbind('click');
				$('#haulageEdit').click(function(){editHaulageDetails(id,idCountry,szCityName,idWarehouse,idDirection,idWarehouseOld,idDirectionOld)});
				
				$('#makeHaulageActivated').unbind('click');
				$('#makeHaulageActivated').click(function(){activateHaulage(id)});
			}
		}
		if(iActive == false)
		{
			$('#makeHaulageActivated').css({opacity:'1'});
			$('#makeHaulageActivated').click(function(){activateHaulage(id)});
		}
	}
	function activateHaulage(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationHaulage.php",{mode:'ACTIVATE_FORWARDER_HAULAGE',id:id,status:status},function(result){
			$('#ajaxLogin').html('');
			$('#ajaxLogin').html(result);
			$('#ajaxLogin').css({'display':'block'});
			});
	}
	function changePriority(id,flag)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationHaulage.php",{mode:'CHANGE_PRIORITY',id:id,flag:flag},function(result){
			$('#frameHaulage').html(result);
			//$('#chunchHaulage').attr('checked',!status);
			submit_currentHaulage_form();	
		});
	}
	function editHaulageDetails(id,idCountry,szCityName,idWarehouse,idDirection,idWarehouseOld,idDirectionOld)
	{
		$('#loader').css({'display':'block'});
		$('#haulageCountry').val(idCountry);
		$('#szCity').val(szCityName);
		$('#haulageWarehouse').val(idWarehouse);
		$('#idDirection').val(idDirection);
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationHaulage.php",{mode:'EDIT_HAULAGE_DETAILS',id:id},function(result){
			var n  = result.split('|||||');
			$('#model_list').html(n[0]);
			if(jQuery.trim(idDirectionOld) == '' || idWarehouseOld=='undefined' ||  idDirectionOld != idDirection || idWarehouseOld != idWarehouse || iActive!='1')
			{
				$('#try_it_out').html(n[1]);
			}
			
			//$('#chunchHaulage').attr('checked',!status);	
			$('#loader').css({'display':'none'});
		});
	}
	function select_cancelled_booking_tr(tr_id,id)
	{
            var rowCount = $('#booking_table tr').length;
            if(rowCount>0)
            {
                for(x=1;x<rowCount;x++)
                {
                    new_tr_id="booking_data_"+x;
                    if(new_tr_id!=tr_id)
                    {
                        $("#"+new_tr_id).removeClass('selected-row');
                    }
                    else
                    {
                        $("#"+tr_id).addClass('selected-row');
                    }
                }
            }
            $("#booking_cancel").unbind("click");
            $("#booking_cancel").click(function(){cancel_booking(id)});
            $("#booking_cancel").attr("style","opacity:1");
            
            $("#download_booking_confirmation").attr('onclick','download_booking_confirmation_pdf(\''+id+'\')'); 
            $("#download_booking_invoice").attr('onclick','download_admin_invoice_pdf(\''+id+'\')');
            $("#download_booking_credit_note").attr('onclick','download_admin_credit_note_pdf(\''+id+'\')');
            $("#download_booking_lcl_booking").attr('onclick','download_admin_booking_pdf(\''+id+'\')');
            $('#download_booking_confirmation, #download_booking_invoice, #download_booking_lcl_booking, #download_booking_credit_note').removeAttr('style');
	}
	
	function cancel_processing_cost()
	{
		window.location.reload();
	}
	
	function cancel_inactivate_curreny()
	{
		 $("#inactivate_services").attr('style','display:none;');
	}
	
	function update_confirm_button()
	{
		var text=$("#szText").attr('value');
		
		if(jQuery.trim(text)!='')
		{
			$('#confirm_button').css({opacity:'1'});
			$('#confirm_button').unbind('click');
			$('#confirm_button').click(function(){inactivate_service_for_currency()});
		}
		else
		{
			$('#confirm_button').css({opacity:'0.4'});
			$('#confirm_button').unbind('click');
		}
	}
	
	function inactivate_service_for_currency()
	{
		var value1=$('#sendExchange').serialize();
		var value2=$('#inActivateService').serialize();
		$("#inactivate_services").html('');
		var settling=$("#settling").attr('value');
		$("#loader").attr('style','display:block;')
		var value=value1+'&'+value2+'&mode=INACTIVATE_SERVICES&settling='+settling;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",value,function(result){
			$('#editCurrency').html(result);
    		 	
		});
	}
	function editExplainDetails(mode,value)
	{	
		if(mode)
		{	
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{mode:mode,id:value},function(result){
			
			$('#preview_Publish').html('');
			$('#showDiv').html(result);
			setup();
			});	
		}
	}
	function saveExplainDetails()
	{		
		// var ed = tinyMCE.get('content');
		 var heading = $('#heading').val();
		 var id= $('#id').val();
		 var flag = $('#flag').val();
			
	   		var content = tinyMCE.get('content');
    		var n =content.getContent();
   			// alert(n);
			var description=encode_string_msg_str(n);
	   	var value1=$('#textEditorSubmit').serialize();
	   	var newValue=value1+"&description="+description+"&id="+id+"&flag="+flag;	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",newValue,function(result){
		$("#error").html(result);
		})  
		.success(function(result) 
		{ 
			if(jQuery.trim(result) == ''){location.reload();}
			
		});
	}
	function orderChangeExplain(id,order,mode_value)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{mode:'CHANGE_EXPLAIN_ORDER',id:id,mode_value:mode_value,order:order},function(result){
		$('#showUpdate').html(result);
		$('#publish').css('opacity','');
		$('#cancel').css('opacity','');	
			$('#publish').attr("onclick","preview_explain_data('"+mode_value+"','publish');");
			$('#cancel').attr("onclick","preview_explain_data('"+mode_value+"','cancel')");
		}); 
	}
	function preview_explain_data(id,mode)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_systemTextPreviewPublish.php",{option:mode,id:id},function(result){
		if(mode == 'preview')
		{
			$('#preview_data').html(result);
			$('#preview_data').css({display:'block'});
		}
		else
		{
		$('#preview_data').html('');	
		$('#contactPopup').html(result);
		$('#contactPopup').css({display:'block'});
		}
		});
	}	
	function submitExplainVersion(option,id)
	{	
		$("#loader").attr('style','display:block;');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_systemTextPreviewPublish.php",{mode:option,id:id},function(){
		location.reload();
		});
	}
	function editExplainationDet(idData,value)
	{	
		if(idData > 0)
		{	
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{mode:'ADD_NEW_EXPLAIN_SECTION',idData:idData,id:value},function(result){
			$('#preview_Publish').html('');
			$('#showDiv').html(result);
			setup();
			});	
		}
	}
	function addNewExpanationPage(mode,previewer_url)
	{
		var add_mode_url = '';
		if(mode=='PREVIEW_EXPLAIN_PAGE')
		{
			add_mode_url = "?mode=PREVIEW_EXPLAIN_PAGE";
		}
		var iExplainPageType=$("#iExplainPageType").attr('value');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php"+add_mode_url,$("#addExplainPage").serialize(),function(result){
			var res = jQuery.trim(result);
			if(res=='SUCCESS' && mode=='PREVIEW_EXPLAIN_PAGE')
			{
				var win = window.open(previewer_url, '_blank');
  				win.focus();
				//redirect_url(previewer_url)
			}
			else if(res!='')
			{
				$('#Error-log').html(result);
			}
			else
			{
				if(iExplainPageType==1)
				{
					window.location.href  = __JS_ONLY_SITE_BASE__+"/manageExplainPages/";
				}
				else if(iExplainPageType==2)
				{
					window.location.href  = __JS_ONLY_SITE_BASE__+"/manageCompanyPages/";
				}
				else if(iExplainPageType==3)
				{
					window.location.href  = __JS_ONLY_SITE_BASE__+"/manageTransportpediaPages/";
				}
			}
		});
	}
	function orderChangeExplainPage(id,type,order)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{id:id,move:order,type:type,mode:'CHANGE_ORDER_EXPLAIN'},function(result){
			if(result!='')
			var n = result.split('|||||');
			
			$('.hsbody-2-right').html(n[0]);
			if(type==1)
			{
				$('#showLeftNavContent_how_it_work').html(n[1]);
			}
			else if(type==2)
			{
				$('#showLeftNavContent_company').html(n[1]);
			}
			else if(type==3)
			{
				$('#showLeftNavContent_pedia').html(n[1]);
			}			
		});
	}
	function editExplainPage(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{id:id,mode:'EDIT_EXPLAIN_PAGES'},function(result){
			if(result!='')
			$('.hsbody-2-right').html(result);
		});
	}
	function deleteExpalinPage(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{id:id,mode:'DELETE_EXPLAIN_PAGE'},function(result){
			if(result!='')
			$('#contactPopup').html(result);
		});
	}
	function deleteExplainPageConfirm(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{id:id,mode:'DELETE_EXPLAIN_PAGE_CONFIRM'},function(result){
			if(jQuery.trim(result)=='SUCCESS||||')
			{
				window.location.href = window.location.href ;
			}
		});
	}
	function cancelAddingPage()
	{
		window.location.href  = __JS_ONLY_SITE_BASE__+"/manageExplainPages/";
	}
	function toggleMe()
	{
		$('#foo').toggle("slow");
		var a= $('#message').html();
		if(a=='Show less')
		{
			$('#message').html('Show more');
		}
		else
		{
			$('#message').html('Show less');
		}
		$.post(__JS_ONLY_SITE_BASE__+'/ajax_showMessage.php',{FLAG:"SET_SHOW_HIDE",value:a},function(result){
		});
	}
	function showCheckedCustomClearence()
	{
		var check = $('#activeCustomClearence').is(":checked");
		var idWhFrom = $('#idOriginWarehouse').val();
		var idWhTo = $('#idDestinationWarehouse').val();
		var idSelectedField = $('#idSelect').val();
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",$('#currentCC').serialize(),function(result){
			var res  = result.split("|||||");
				$('#bottomContent').html(res[0]);
				$('#obo_cc_bottom_span').html(res[1]);
				});	
	}
	function resend_email_forwarder(id)
	{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationNonAcknowledge.php",{id:id,mode:'RESEND_BOOKING_CONFIRMATION_EMAIL'},function(result){
				$('#ajaxLogin').html(result);
			});
	}
	function checkDescriptionLength(id)
	{
		length	=	$('#'+id).val().length;
		$('#char_left').html(length);
	}
	function deleteBlogPage(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createBlogPages.php",{id:id,mode:'DELETE_BLOG_PAGE'},function(result){
			if(result!='')
			$('#contactPopup').html(result);
		});
	}
	function deleteBlogPageConfirm(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createBlogPages.php",{id:id,mode:'DELETE_BLOG_PAGE_CONFIRM'},function(result){
			if(jQuery.trim(result)=='')
			{
				window.location.href = window.location.href ;
			}
		});
	}
	function orderChangeBlogPage(id,order)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createBlogPages.php",{id:id,move:order,mode:'CHANGE_ORDER_BLOG'},function(result){
			if(result!='')
			{
				
				$('.hsbody-2-right').html(result);
			
			}
		});
	}
	function editBlogPage(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createBlogPages.php",{id:id,mode:'EDIT_BLOG_PAGES'},function(result){
			if(result!='')
			$('.hsbody-2-right').html(result);
		});
	}
	function addNewBlogPage()
	{	
		var content = tinyMCE.get('content');		
		//var data = $("#addBlogPage").serialize();
		var szHeading = $('#szHeading').val();
		var szLink = $('#szLink').val();
		var szMetaTitle = $('#szMetaTitle').val();
		var szMetaKeyword = $('#szMetaKeyword').val();
		var szMetaDescription = $('#szMetaDescription').val();
		var iActive = $('#iActive').val();
		var iLanguage = $('#iLanguage').val();
		var mode = $('#mode').val();
		var id = $('#id').val();
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createBlogPages.php",{szHeading:szHeading,szLink:szLink,szMetaTitle:szMetaTitle,szMetaKeyword:szMetaKeyword,szMetaDescription:szMetaDescription,iActive:iActive,iLanguage:iLanguage,id:id,mode:mode,szContent:content.getContent()},function(result){
			var res = jQuery.trim(result);
			if(res!='')
			{
				$('#Error-log').html(result);
			}
			else
			{
				window.location.href  = __JS_ONLY_SITE_BASE__+"/manageBlogPages/";
			}
		});
	}
	function cancelAddingBlogPage()
	{
		window.location.href  = __JS_ONLY_SITE_BASE__+"/manageBlogPages/";
	}
	function previewPageData()
	{
		var content = tinyMCE.get('content');
		$('#preview').css({display:'block'});
		$('#preview').html(content.getContent());
	}
	function change_haulage_model_status_model_add(idMapping,changeStatusFlag,idForwarder,iPriorityLevel,idHaulageModel,idWarehouse,mainurl,count,idPricing,directionId,fWmFactor)
{
		//alert(idHaulageModel);
	//alert(idForwarder);
	//alert(idMapping);
	var showflag="showModelData";
	var haulageCountry = $("#haulageCountry").attr('value');
	var szCity = $("#szCity").attr('value');
	var haulageWarehouse = $("#haulageWarehouse").attr('value');
	var idDirection = $("#idDirection").attr('value');
	if(idHaulageModel=='3')
	{
		$("#update_postcode_form").attr('style',"display:none");
		$("#update_postcode_form_add_edit").attr('style',"display:none");
		
	}
	else if(idHaulageModel=='4')
	{
		$("#update_distance_form").attr('style',"display:none");
		$("#update_distance_form_add_edit").attr('style',"display:none");
	}
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{idForwarder:idForwarder,idCountry:haulageCountry,szCity:szCity,haulageWarehouse:haulageWarehouse,idDirection:idDirection,flag:showflag,idHaulageModel:idHaulageModel,changeStatusFlag:changeStatusFlag,flag:showflag},function(result){
	//$("#view_haulage_model_div_id").html(result);
	//alert(globalTrHaulageid);
	//alert(result);
		$("#"+globalTrHaulageid+' #active').html(result);	
			var totalActive=$("#totalActive").attr('value');
			var rowCount = $('#view_haulage_model tr').length;
			var t=parseInt(iPriorityLevel);
			//alert(t);
			//alert(idMapping);
			//alert(changeStatusFlag);
			if(changeStatusFlag=='A')
			{
				
				//var trcount=parseInt(rowCount)-1;
				var div_id="haulage_data_"+iPriorityLevel;			
			}
			else
			{
				var trcount=parseInt(rowCount)-1;
				var div_id="haulage_data_"+trcount;
			}
		//	alert(div_id);
			//alert(t);
			if(rowCount>0)
			{
				for(k=1;k<rowCount;++k)
				{
					newdivid="haulage_data_"+k;
					if(newdivid!=div_id)
					{
						$("#"+newdivid).attr('style','background:#ffffff;color:#000000;');
					}
					else
					{
						$("#"+div_id).attr('style','background:#DBDBDB;color:#828282;');
					}
				}
			}
			
			var totalActive=$("#totalActive").attr('value');
			var updateflag='I';
			$("#change_status_button").html("<span style='min-width:70px;'>INACTIVATE</span>");
			if(parseInt(totalActive)!=parseInt(t))
			{
				$("#priority_down").unbind("click");
				$("#priority_down").click(function(){change_haulage_model_priority(idMapping,idForwarder,'down',t,updateflag)});
				$("#priority_down").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_down").unbind("click");
				$("#priority_down").attr('style',"opacity:0.4");
			}
			if(parseInt(t)!='1')
			{
				$("#priority_up").unbind("click");
				$("#priority_up").click(function(){change_haulage_model_priority(idMapping,idForwarder,'up',t,updateflag)});
				$("#priority_up").attr('style',"opacity:1");
			}
			else
			{
				$("#priority_up").unbind("click");
				$("#priority_up").attr('style',"opacity:0.4");
			}
			
			
			
			$("#change_status_button").unbind("click");
			//$("#haulage_remove_button").attr("onclick","delete_haulage_data('"+newvalue+"','"+div_id+"','"+idForwarder+"')");
			$("#change_status_button").click(function(){change_haulage_model_status(div_id,idMapping,updateflag,idForwarder,t)});
			$("#change_status_button").attr('style',"opacity:1");
			if(idHaulageModel=='1')
			{
				$("#zone_popup_add_edit").attr('style',"display:none");
				$("#zone_popup_add_edit").html('');
			}
			else if(idHaulageModel=='2')
			{
				$("#update_city_form_add_edit").attr('style',"display:none");
				$("#update_city_form_add_edit").html('');
			}
			else if(idHaulageModel=='3')
			{
				$("#update_postcode_form").attr('style',"display:none");
				$("#update_postcode_form_add_edit").attr('style',"display:none");
				$("#update_postcode_form_add_edit").html('');
				
			}
			else if(idHaulageModel=='4')
			{
				$("#update_distance_form").attr('style',"display:none");
				$("#update_distance_form_add_edit").attr('style',"display:none");
				$("#update_distance_form_add_edit").html('');
			}
			
			open_pricing_model_detail(idHaulageModel,idWarehouse,mainurl,count,idPricing,directionId,fWmFactor);
	
	});
}
function delete_haulage_pricing_line_confirm(id,idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)
{

	if(idHaulageModel=='1')
	{
		var  divid="zone_detail_list_"+idHaulageModel;
		var  div_id="zone_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_zone_line_confirm";
	}
	else if(idHaulageModel=='2')
	{
		var  divid="city_detail_list_"+idHaulageModel;
		var  div_id="city_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_city_line_confirm";
	}
	else if(idHaulageModel=='3')
	{
		var  divid="postcode_detail_list_"+idHaulageModel;
		var  div_id="postcode_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_postcode_line_confirm";
	}
	else if(idHaulageModel=='4')
	{
		var  divid="distance_detail_list_"+idHaulageModel;
		var  div_id="distance_detail_delete_"+idHaulageModel;
		var showflag="delete_haulage_distance_line_confirm";
	}
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{id:id,idHaulagePricingModel:idHaulagePricingModel,idHaulageModel:idHaulageModel,idWarehouse:idWarehouse,idDirection:idDirection,flag:showflag},function(result){
		$("#"+divid).html(result);
		$("#"+div_id).attr("style","display:none");
		if(idHaulageModel=='1')
		{
			$("#save_zone_detail_line").unbind("click");
			$("#save_zone_detail_line").attr('style',"opacity:1");
			$("#save_zone_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
		}
		else if(idHaulageModel=='2')
		{
			$("#save_city_detail_line").unbind("click");
			$("#save_city_detail_line").attr('style',"opacity:1");
			$("#save_city_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
		}
		else if(idHaulageModel=='3')
		{
			$("#save_postcode_detail_line").unbind("click");
			$("#save_postcode_detail_line").attr('style',"opacity:1");
			$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});

		}
		else if(idHaulageModel=='4')
		{
			$("#save_distance_detail_line").unbind("click");
			$("#save_distance_detail_line").attr('style',"opacity:1");
			$("#save_distance_detail_line").click(function(){add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)});
		}
		//var globalTrHaulageid = '';
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{haulageWarehouse:idWarehouse,idDirection:idDirection,idHaulageModel:idHaulageModel,flag:'showModelData'},function(result){
			$("#"+globalTrHaulageid+' #active').html(result);
		});	
	});
}
function add_haulage_pricing_line(idHaulageModel,idWarehouse,idDirection,idHaulagePricingModel)
{
	if(idHaulageModel=='1')
	{
		var  divid="zone_edit_form";
		var value=$("#updatePricingZoneData").serialize();
		var showflag="add_zone_pricing_line";
	}
	else if(idHaulageModel=='2')
	{
		var  divid="city_edit_form";
		var showflag="add_city_pricing_line";
		var value=$("#updatePricingCityData").serialize();
	}
	else if(idHaulageModel=='3')
	{
		var  divid="postcode_edit_form";
		var showflag="add_postcode_pricing_line";
		var value=$("#updatePricingPostCodeData").serialize();
	}
	else if(idHaulageModel=='4')
	{
		var  divid="distance_edit_form";
		var showflag="add_distance_pricing_line";
		var value=$("#updatePricingDistanceData").serialize();
	}
	
	var newvalue=value+"&idHaulageModel="+idHaulageModel+"&idWarehouse="+idWarehouse+"&idDirection="+idDirection+"&idHaulagePricingModel="+idHaulagePricingModel+"&flag="+showflag;
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",newvalue,function(result){
		$("#"+divid).html(result);

		$.post(__JS_ONLY_SITE_BASE__+"/ajax_viewOBOPricingHaulage.php",{haulageWarehouse:idWarehouse,idDirection:idDirection,idHaulageModel:idHaulageModel,flag:'showModelData'},function(result){
			$("#"+globalTrHaulageid+' #active').html(result);
		});
	});
}


	function cancel_inactivate_forwarder()
	{
		 window.location.href  = __JS_ONLY_SITE_BASE__+"/currencyVariable/";
	}
	
	function confirm_update_forwarder_currency(idCurrency)
	{
		$("#confirm_button").unbind("click");
		$("#confirm_button").attr('style',"opacity:0.4");
		
		$("#cancel_button").unbind("click");
		$("#cancel_button").attr('style',"opacity:0.4");
		
		$("#inactivate_services").attr('style','display:none;');
		//$("#loader").attr('style','display:block;')
		$("#inactivate_services").attr('style','display:none;')
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCurrency.php",{idCurrency:idCurrency,mode:'UPDATE_FORWARDER'},function(result){
			window.location.href  = __JS_ONLY_SITE_BASE__+"/currencyVariable/";	
		});
	}
	
	function cancel_processing_cost_by_admin()
	{
		$("#open_processing_popup").attr('style',"display:none");
		removePopupScrollClass('open_processing_popup');
	}
	function submit_currentHaulage_form()
	{
		$('#loader').css({'display':'block'});
		$('#model_list').html('');
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationHaulage.php",$('#currentHaulage').serialize(),function(result){
		
		$('#bottomForm').html(result);
		//$('#editButton').css({opacity:'0.4'});
		$('#loader').css({'display':'none'});
			});		
	}
	
	function viewNoticeTransactionDetailsUploadServic(tr_id,id,pass,mode,status)
	{
            var rowCount = $('#booking_table tr').length;
            if(rowCount>0)
            {
                for(x=1;x<rowCount;x++)
                {
                    var new_tr_id="booking_data_"+x;
                    if(new_tr_id!=tr_id)
                    {
                            $("#"+new_tr_id).attr('style','');
                    }
                    else
                    {
                            $("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
                    }
                }
            }
            if(mode==3)
            {
                if(status=='CANCELLED_INSURED_BOOKING')
                {
                    $("#view_notice").unbind("click"); 
                    $("#view_notice").attr('style','opacity:1;float:right;');
                    $("#view_notice").click(function(){ download_admin_insurance_credit_note_pdf(id); }); 
                }
                else
                {
                    $("#view_notice").unbind("click");
                    $("#view_notice").attr('style','opacity:1;float:right;');
                    $("#view_notice").click(function(){ download_admin_insurance_invoice_pdf(id) });  
                }  
            }
            else
            { 
                $("#view_notice").unbind("click");
                $("#view_notice").attr('style','opacity:1;float:right;');
                $("#view_notice").click(function(){ download_upload_service_invoice_pdf(id,pass) });  
            }
	}
	function deleteHaulage(id)
	{
			data =  $('#currentHaulage').serialize()+"&mode=ACTIVATE_FORWARDER_HAULAGE_CONFIRM&id="+id;
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationHaulage.php",data,function(result){
			$('#bottomForm').html('');
			$('#ajaxLogin').html('');
			$('#bottomForm').html(result);
			$('#bottomForm').css({'display':'block'});
			});	
	}
	function submit_currentCC_form()
	{
		$('#loader').css({'display':'block'});
		$('#bottomContent').html('');
		$('#obo_cc_bottom_span').html('');	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",$('#currentCC').serialize(),function(result){
		var n  = result.split('|||||');
		$('#bottomContent').html(n[0]);
		$('#obo_cc_bottom_span').html(n[1]);	
		$('#editButton').css({opacity:'0.4'});
		$('#loader').css({'display':'none'});
			});	
	}
	function submit_currentCFS_form()
	{
		$('#loader').css({'display':'block'});
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_warehouseSearch.php",$('#currentCFS').serialize(),function(result){
		var n = result.split('|||||');
		$('#lisingTop').html(n[0]);
		$('#detailsBottom').html(n[1]);
		$('#loader').css({'display':'none'});
			});	
	}
	function submit_currentWTW_formAjax(idTable,id)
	{
		$("#bottomFormEdit input[type=text]").attr('value',"");
		$("#bottomFormEdit select").val('0');
		$("#bottomFormEdit input[type=text],#bottomFormEdit select").attr('disabled',true);
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_currentWTW.php",$('#currentWTW').serialize(),function(result){
		$('#bottomForm').html(result);
		$('#editButton').css({opacity:'0.4'});
		showBottomTableToEditWTWContent(idTable,id);
		$('#loader').css({'display':'none'});		
			});	
	}
	function saveDetailsCheck(keyEvent)
	{	
		if (keyEvent.keyCode == 13) {keyEvent.preventDefault(); return false; }
	}
	
	function close_create_forwarder_popup()
	{
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'forwarderComp'},function(result){	
			$('#content_body').html(result);
			$('#ajaxLogin').html('');
			});
	}
	function sortCountryDetails(div_id,fieldForSorting,order)
	{
		var newOrder = 'DESC';
		if(order == 'DESC')
		{
			newOrder = 'ASC';
		}
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_detailCountries.php",{mode:'SORT_COUNTRY_BY_ORDER',field:fieldForSorting,order:order},function(result){	
		$('#table').html(result);
		$('#'+div_id).attr("onclick","sortCountryDetails('"+div_id+"','"+fieldForSorting+"','"+newOrder+"');");
		});
	}
	function sort_expiring_table(sort_by,sort,divid,value)
	{
		new_value="sortby="+sort_by+"&sort="+sort+"&mode=SORT_TABLE";
		//alert(divid);
		if(sort=='ASC')
		{
			var new_sort='DESC';
			var innr_htm = '<span class="sort-arrow-up">&nbsp;</span>';
		}
		else
		{
			var new_sort='ASC';
			var innr_htm = '<span class="sort-arrow-down">&nbsp;</span>';
		}
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_expiringWTW.php",new_value,function(result){
		        $("#topTable").html(result);
		$("#"+divid).attr("onclick","sort_expiring_table('"+sort_by+"','"+new_sort+"','"+divid+"','"+value+"');");      
		$("#"+divid).html(innr_htm);
		if(divid!='expiring')
		{
			$('#expiring').html('<span class="sort-arrow-up-grey"> </span>');
			$("#expiring").attr("onclick","sort_expiring_table('pw.dtExpiry','ASC','expiring');");
		}      
		});
	}
	
function update_country_dropdown(forwarder_id)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_cfsList.php",{mode:'UPDATE_COUNTRY_LIST',forwarder_id:forwarder_id},function(result){
		$('#country_drop_down_span').html(result);
	});
}
	function checkCfsDetails()
	{
		//alert('hello');
		var szCFSName  = $('#szCFSName').val();
		var szAddressLine1 = $('#szAddressLine1').val();
		var szOriginCity = $('#szOriginCity').val();
		var szCountry = $('#szCountry').val();
		var szPhoneNumber = $('#szPhoneNumber').val();
		var szLatitude = $('#szLatitude').val();
		var szLongitude =  $('#szLongitude').val();
		var idWarehouse = $('#idWarehouse').val();
		
		$('#add_edit_warehouse_button').css({'opacity':'1'});
		if(idWarehouse != null)
		{
			$('#add_edit_warehouse_button').attr('onclick','encode_string("szPhoneNumber","szPhoneNumberUpdate");submit_warehouse_data_obo();');
		}
		else
		{
			$('#add_edit_warehouse_button').attr('onclick','encode_string("szPhoneNumber","szPhoneNumberUpdate");add_cfs_listing();');
		}
		
	}
	
	function display_indexed_count_by_booking_step(booking_step)
	{
		$('#loader').css({'display':'block'});

		$.post(__JS_ONLY_SITE_BASE__+"/ajax_salesFunnel.php",{booking_step:booking_step,mode:'SHOW_MONTHLY_REPORT'},function(result){
			
			$('#loader').css({'display':'none'});
			$('#bottom_div').html(result);
			$('#bottom_div').css('display','block');
			
		});	
	}
	
	function submit_visualization_form()
	{
		$('#loader').css({'display':'block'});
	
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_salesFunnel.php",$('#sales_funnel_visualization_form').serialize(),function(result){
			
			$('#loader').css({'display':'none'});
			$('#bottom_div').html(result);
			$('#bottom_div').css('display','block');
			
		});	
	}
	function CheckLastDataFirst(arg,fieldId,classId)
	{
		if(classId == 'tableNoOne')
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationStatistics.php",{mode:'SHOW_TABLE_ONE_DATA',field:arg},function(result){
				$('#tableForwarder').html(result);
				$('.'+classId).html('');
				$('#'+fieldId).html('*');
			});	
		}
		if(classId == 'tableNoTwo')
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationStatistics.php",{mode:'SHOW_TABLE_TWO_DATA',field:arg},function(result){
				$('#tableCoustomerTable').html(result);
				$('.'+classId).html('');
				$('#'+fieldId).html('*');
			});	
		}
		if(classId == 'tableNoThree')
		{
			$.post(__JS_ONLY_SITE_BASE__+"/ajax_operationStatistics.php",{mode:'SHOW_TABLE_THREE_DATA',field:arg},function(result){
				$('#tableTraders').html(result);
				$('.'+classId).html('');
				$('#'+fieldId).html('*');
			});	
		}
	}
	function sort_cc_table(flag,id,order)
	{
		var idForwarder = $('#idForwarder').val();
		var idOriginCountry = $('#idOriginCountry').val();
		var idDestinationCountry = $('#idDestinationCountry').val();
		$('#loader').css({'display':'block'});
		$.post(__JS_ONLY_SITE_BASE__+'/ajax_oboDataExportCC.php',{mode:'SORT_TABLE_ORDER',flag:flag,idForwarder:idForwarder,idOriginCountry:idOriginCountry,idDestinationCountry:idDestinationCountry,order:order},function(result){
		var n = result.split('|||||');
		$('#bottomContent').html(n[0]);
		$('#obo_cc_bottom_span').html(n[1]);
		$('#loader').css({'display':'none'});
		orderId(order,id,flag);
		});
	}
	function orderId(order,id,flag)
	{
		if(order == 'ASC')
		{
			var newOrder = 'DESC';
			var position = 'sort-arrow-up';
		}
		else
		{
			var newOrder = 'ASC';
			var position = 'sort-arrow-down';
		}
		if(id == 'from')
		{
			$('#to').html('<span class="sort-arrow-up-grey"></span>');
			$('#to').attr("onclick","sort_cc_table('2','to','ASC');");
			
			$("#"+id).attr("onclick","sort_cc_table('1','from','"+newOrder+"');");
			$("#"+id).attr("onclick","sort_cc_table('1','from','"+newOrder+"');");
			$("#"+id).html('<span class="'+position+'"></span>');
		}
		if(id == 'to')
		{
			$('#from').html('<span class="sort-arrow-up-grey"></span>');
			$('#from').attr("onclick","sort_cc_table('1','from','ASC');");
			
			$("#"+id).attr("onclick","sort_cc_table('2','to','"+newOrder+"');");
			$("#"+id).attr("onclick","sort_cc_table('2','to','"+newOrder+"');");
			$("#"+id).html('<span class="'+position+'"></span>');
		}
		$('#sortBy').val(flag);
		$('#orderBy').val(order);
	}
	function pageCC(page)
	{
		var flag = $('#sortBy').val();
		var order = $('#orderBy').val();
		var idForwarder = $('#idForwarder').val();
		var idOriginCountry = $('#idOriginCountry').val();
		var idDestinationCountry = $('#idDestinationCountry').val();
		$('#loader').css({'display':'block'});
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_oboDataExportCC.php",{mode:'PAGE_TABLE_ORDER',flag:flag,idForwarder:idForwarder,idOriginCountry:idOriginCountry,idDestinationCountry:idDestinationCountry,order:order,page:page},function(result){
			var n = result.split('|||||');
			$('#bottomContent').html(n[0]);
			$('#obo_cc_bottom_span').html(n[1]);
			$('#loader').css({'display':'none'});
			if(flag ==1 )
			{
				var id = 'from';
				orderId(order,id,flag);
			}
			if(flag == 2)
			{
				var id = 'to';
				orderId(order,id,flag);
			}
		});	
	}
	function sort_searched_trade(feild_name)
	{
		$('#loader').css({'display':'block'});
                var szSortBy=$("#szSortBy").attr('value');
                var szSortField=$("#szSortField").attr('value');
                var sort_by='';
                if(szSortField!=feild_name)
                {
                    sort_by='DESC';
                }
                else
                {
                    if(szSortBy=='' || szSortBy=='DESC')
                    {
                        sort_by='ASC';
                    }
                    else if(szSortBy=='ASC')
                    {
                        sort_by='DESC';
                    }
                }
		
		var szOriginDestination = '';
		
		if(feild_name=='szTradeName')
		{
			szOriginDestination = $("#szOriginDestination").attr('value');
		}
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_salesFunnel.php",{sort_key:feild_name,sort_by:sort_by,mode:'SORT_SEARCHED_TRADE_REPORT'},function(result){
			
			$('#loader').css({'display':'none'});
			$('#searched_trade_div').html(result);
			$("#szSortBy").attr('value',sort_by);
                        $("#szSortField").attr('value',feild_name)
			if(szOriginDestination=='ORIGIN')
			{
				$("#szOriginDestination").attr('value','DESTINATION');
			}
			else
			{
				$("#szOriginDestination").attr('value','ORIGIN');
			}
		});	
	}
	
	function addNewLandingPage(form_id,new_landing_page_flag)
	{
		$("#loader").attr('style','display:block');
		if(new_landing_page_flag==1)
		{ 
                    var content = tinyMCE.get('szSeoDescription');
                    var content_data = content.getContent();
                    content_data = content_data.replace('&nbsp;','');

                    var form_value = $("#"+form_id).serialize()+"&landingPageArr[szSeoDescription]="+encodeURIComponent(content_data) ;
		}
		else
		{
                    var content = tinyMCE.get('content');
                    var content_data = content.getContent();
                    content_data = content_data.replace('&nbsp;','');

                    var form_value = $("#"+form_id).serialize()+"&landingPageArr[szTextD]="+encodeURIComponent(content_data) ;
		}
		
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",form_value,function(result){
			
                result_ary = result.split("||||");
                if(jQuery.trim(result_ary[0])=='ERROR')
	        {
	        	$("#Error-log").css('display','block'); 
	        	$("#Error-log").html(result_ary[1]);
	        	window.scrollTo(100,150);
	        }
	        else if(jQuery.trim(result_ary[0])=='SUCCESS')
	        {
	        	$("#Error-log").css('display','none');
	        	redirect_url(__JS_ONLY_SITE_BASE__+"/manageLandingPages/");
	        }
	        else if(jQuery.trim(result_ary[0])=='REDIRECT')
	        {
	        	$("#Error-log").css('display','none');
	        	redirect_url(result_ary[1]);
	        }		
	        $("#loader").attr('style','display:none');	
		});
	}
	
	function previewLandingPage(form_id,new_landing_page_flag)
	{
		if(new_landing_page_flag==1)
		{
			var szMod_value = $("#mode").attr('value');
			$("#mode").attr('value','PREVIEW_NEW_LANDING_PAGE');
			
			var form_value = $("#"+form_id).serialize();
		}
		else
		{
			var szMod_value = $("#mode").attr('value');
			$("#mode").attr('value','PREVIEW');
			
			var content = tinyMCE.get('content');
			var content_data = content.getContent();
			content_data = content_data.replace('&nbsp;','');
			
			var form_value = $("#"+form_id).serialize()+"&landingPageArr[szTextD]="+encodeURIComponent(content_data);
		} 
		
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",form_value,function(result){
			result_ary = result.split("||||");
			if(jQuery.trim(result_ary[0])=='ERROR')
	        {
	        	$("#Error-log").css('display','block'); 
	        	$("#Error-log").html(result_ary[1]);
	        }
			
	        if(jQuery.trim(result_ary[0])=='SUCCESS')
	        {
	        	$("#Error-log").css('display','none');
	        	$("#"+form_id).submit();
	        }	        
	        $("#mode").attr('value',szMod_value);
		});
	}
	
	function editNewLandingPage(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/editLandingPage.php",{id:id,mode:'EDIT_LADING_PAGE'},function(result){
			
			result_ary = result.split("||||");
			if(jQuery.trim(result_ary[0])=='ERROR')
	        {
	        	$("#Error-log").css('display','block'); 
	        	$("#Error-log").html(result_ary[1]);
	        }
			
	        if(jQuery.trim(result_ary[0])=='SUCCESS')
	        {
	        	$("#Error-log").css('display','none');
	        	redirect_url();
	        }			
		});
	}
	
	function orderChangeLandingPage(id,order,page_name)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",{id:id,move:order,mode:'CHANGE_ORDER_LANDING_PAGE',page_name:page_name},function(result){
			
			var result_ary = result.split('||||');
			if(jQuery.trim(result_ary[0])=='SUCCESS')
			{
				$('#manage_landing_page_content').html(result_ary[1]);
			}
		});
	}
	
	function deleteLandingPage(id,page_name)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",{id:id,page_name:page_name,mode:'DELETE_LANDING_PAGE'},function(result){
			
			$("#contactPopup").css('display','block');
			$('#contactPopup').html(result);
		});
	}
	
	function deleteLandingPageConfirm(id,page_name)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",{id:id,page_name:page_name,mode:'DELETE_LANDING_PAGE_CONFIRM'},function(result){
			var result_ary = result.split('||||');
			if(jQuery.trim(result_ary[0])=='SUCCESS')
			{
				$("#contactPopup").css('display','none');
				$('#manage_landing_page_content').html(result_ary[1]);
			}
			
		});
	}
	
	function addNewSeoPage(form_id)
	{
            var content = tinyMCE.get('content');
            var content_data = content.getContent();
            content_data = content_data.replace('&nbsp;','');

            var form_value = $("#"+form_id).serialize()+"&landingPageArr[szContent]="+encodeURIComponent(content_data);

            $.post(__JS_ONLY_SITE_BASE__+"/ajax_addEditSeoPage.php",form_value,function(result){

                result_ary = result.split("||||");
                if(jQuery.trim(result_ary[0])=='ERROR')
	        {
                    $("#Error-log").css('display','block'); 
                    $("#Error-log").html(result_ary[1]);
	        }
	        if(jQuery.trim(result_ary[0])=='SUCCESS')
	        {
                    $("#Error-log").css('display','none');
                    redirect_url(__JS_ONLY_SITE_BASE__+"/manageSEOPages/");
	        }			
            });
	}
	
	function previewSeoPage(form_id)
	{
            var szMod_value = $("#mode").attr('value');
            $("#mode").attr('value','PREVIEW');

            var content = tinyMCE.get('content');
            var content_data = content.getContent();
            content_data = content_data.replace('&nbsp;','');

            var form_value = $("#"+form_id).serialize()+"&landingPageArr[szContent]="+encodeURIComponent(content_data);

            $.post(__JS_ONLY_SITE_BASE__+"/ajax_addEditSeoPage.php",form_value,function(result){
                result_ary = result.split("||||");
                if(jQuery.trim(result_ary[0])=='ERROR')
	        {
                    $("#Error-log").css('display','block'); 
                    $("#Error-log").html(result_ary[1]);
	        }
                else if(jQuery.trim(result_ary[0])=='SUCCESS')
	        {
                    $("#Error-log").css('display','none');
                    $("#preview_seo_page").css('display','block');
                    $("#preview_seo_page").html(result_ary[1]);
	        }	        
                $("#mode").attr('value',szMod_value);
            });
	}
	
	function editNewSeoPage(id)
	{
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_addEditSeoPage.php",{id:id,mode:'EDIT_SEO_PAGE'},function(result){ 
                result_ary = result.split("||||");
                if(jQuery.trim(result_ary[0])=='ERROR')
	        {
                    $("#Error-log").css('display','block'); 
                    $("#Error-log").html(result_ary[1]);
	        } 
	        if(jQuery.trim(result_ary[0])=='SUCCESS')
	        {
                    $("#Error-log").css('display','none');
	        }			
            });
	}
	
	function orderChangeSeoPage(id,order)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_addEditSeoPage.php",{id:id,move:order,mode:'CHANGE_ORDER_SEO_PAGE'},function(result){
			
			var result_ary = result.split('||||');
			if(jQuery.trim(result_ary[0])=='SUCCESS')
			{
				$('#manage_landing_page_content').html(result_ary[1]);
			}
		});
	}
	
	function deleteSeoPage(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_addEditSeoPage.php",{id:id,mode:'DELETE_SEO_PAGE'},function(result){
			
			$("#contactPopup").css('display','block');
			$('#contactPopup').html(result);
		});
	}
	
	function deleteSeoPageConfirm(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_addEditSeoPage.php",{id:id,mode:'DELETE_SEO_PAGE_CONFIRM'},function(result){
			var result_ary = result.split('||||');
			if(jQuery.trim(result_ary[0])=='SUCCESS')
			{
				$("#contactPopup").css('display','none');
				$('#manage_landing_page_content').html(result_ary[1]);
			}
			
		});
	}
	
	function downloadFeedBackDataDetails()
	{
		document.downloadFeedBackData.action = __JS_ONLY_SITE_BASE__+"/NPS/";
		document.downloadFeedBackData.method = "post"
		document.downloadFeedBackData.submit();
	}
	
	
	function open_download_button()
	{
		setTimeout('open_download_button_10sec()','500');
	}
	
	function open_download_button_10sec()
	{
		var fromDate=$('#datepicker1').val();
		var toDate=$('#datepicker2').val();
		//alert(fromDate);
		if(fromDate!='' || toDate!='')
		{
			$("#download_button").unbind("click");
			$("#download_button").click(function(){downloadFeedBackDataDetails();});
			$("#download_button").attr('style',"opacity:1");
		}
		else
		{
			$("#download_button").unbind("click");
			$("#download_button").attr('style',"opacity:0.4");
		}
	}
	
	function selectRssTemplate(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_templateRss.php",{id:id,mode:'SHOW_CONTENT'},function(result){
			var result_ary = result.split('||||');
			if(jQuery.trim(result_ary[0])=='SUCCESS')
			{
				$('#showdetails').html(result_ary[1]);
			}
		});
	}
	
	function update_rss_template()
	{		
		var szMessage=encode_string_msg('szDescriptionHTML');
		var value=$("#rss_template_form").serialize();
		var new_value=value+'&szMessage='+szMessage;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_templateRss.php",new_value,function(result){
			var result_ary = result.split('||||');
			if(jQuery.trim(result_ary[0])=='SUCCESS')
			{
				$('#showdetails').html(result_ary[1]);
				$('#rss_template_top').html(result_ary[2]);
				$("#Error-Log").attr('style',"display:none;");
			}
			else if(jQuery.trim(result_ary[0])=='ERROR')
			{
				$('#Error-Log').html(result_ary[1]);
				$("#Error-Log").attr('style',"display:block;");
			}
			
		});
	}
	
	function get_customer_email_list()
	{
		var szCountryArr = $("#szCountry").val();
		
		var iActiveFlag='';
		
		if(document.getElementById('iActiveFlag1').checked)
		{
			var iActiveFlag1=$('#iActiveFlag1').val();
			var iActiveFlag=iActiveFlag1;
		}
		
		if(document.getElementById('iActiveFlag2').checked)
		{
			var iActiveFlag2=$('#iActiveFlag2').val();
			if(iActiveFlag!='')
			{
				iActiveFlag2='0';
				iActiveFlag=iActiveFlag+"_"+iActiveFlag2; 
			}
			else
			{
				iActiveFlag2='0';
				iActiveFlag=iActiveFlag2;
			}
		}
		//alert(iActiveFlag);
		var iNewsUpdate='';
		if(document.getElementById('iNewsUpdate1').checked)
		{
			var iNewsUpdate1=$('#iNewsUpdate1').val();
			var iNewsUpdate=iNewsUpdate1;
		}
		if(document.getElementById('iNewsUpdate2').checked)
		{
			var iNewsUpdate2=$('#iNewsUpdate2').val();
			if(iNewsUpdate!='')
			{
				iNewsUpdate2='0';
				iNewsUpdate=iNewsUpdate+"_"+iNewsUpdate2; 
			}
			else
			{
				iNewsUpdate2='0';
				iNewsUpdate=iNewsUpdate2;
			}
		}
		
			
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendCustomerMsg.php",{szCountryArr:szCountryArr,iActiveFlag:iActiveFlag,iNewsUpdate:iNewsUpdate,flag:'CUSTOMER_EMAIL_LIST'},function(result){
			$("#ajaxLogin").html(result);	
		});
	}
	
	
	function get_forwarder_email_list()
	{
	
		var szForwarderArr = $("#szForwarder").val();
		
		var szProfileArr = $("#szProfile").val();
		
		
		var iForwarderStatusFlag='';
		
		if(document.getElementById('iForwarderStatus1').checked)
		{
			var iForwarderStatusFlag1=$('#iForwarderStatus1').val();
			var iForwarderStatusFlag=iForwarderStatusFlag1;
		}
		if(document.getElementById('iForwarderStatus2').checked)
		{
			var iForwarderStatusFlag2=$('#iForwarderStatus2').val();
			var iForwarderStatusFlag2=0;
			if(iForwarderStatusFlag!='')
			{
				var iForwarderStatusFlag=iForwarderStatusFlag+"_"+iForwarderStatusFlag2;
			}
			else
			{
				var iForwarderStatusFlag=iForwarderStatusFlag2;
			}
		}
		
		
		var iForwarderUserStatusFlag='';
		if(document.getElementById('iForwarderUserStatus1').checked)
		{
			var iForwarderUserStatusFlag1=$('#iForwarderUserStatus1').val();
			var iForwarderUserStatusFlag=iForwarderUserStatusFlag1;
		}
		if(document.getElementById('iForwarderUserStatus2').checked)
		{
			var iForwarderUserStatusFlag2=$('#iForwarderUserStatus2').val();
			var iForwarderUserStatusFlag2=0;
			if(iForwarderUserStatusFlag!='')
			{
				var iForwarderUserStatusFlag=iForwarderUserStatusFlag+"_"+iForwarderUserStatusFlag2;
			}
			else
			{
				var iForwarderUserStatusFlag=iForwarderUserStatusFlag2;
			}
		}
		
			
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_sendForwarderMsg.php",{szForwarderArr:szForwarderArr,iForwarderStatusFlag:iForwarderStatusFlag,iForwarderUserStatusFlag:iForwarderUserStatusFlag,szProfileArr:szProfileArr,flag:'FORWARDER_EMAIL_LIST'},function(result){
			$("#ajaxLogin").html(result);	
		});
	}
	
	
	function showPageMessageReview(page)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{sendflag:'MESSAGES_CONTENT_NEXT_PAGE',page:page},function(result){
				$('#content').html(result);	
			});	
	}
	
	function submit_CFS_map_form()
	{
		$("#popup_container_google_map").attr('style',"display:block;");
		$('#cfs_location_map_form').submit();
	}
	
	
	function encode_string_msg(id)
	{
	   var input_string='';
	   var encoded_strs='';
	   
	   input_string=$("#"+id).attr('value');
	   url_enc=encodeURIComponent(input_string);
	   encoded_strs=$.base64.encode(url_enc);
	   //alert(encoded_strs);
	   //$("textarea#"+id).attr('value',''+encoded_strs+'');
	        
	
	   return encoded_strs;
	}
	
	
	function select_message_queue_send_tr(tr_id,id)
	{
		var rowCount = $('#send_message_list tr').length;
		if(rowCount>0)
		{
			for(x=1;x<rowCount;x++)
			{
				new_tr_id="message_data_"+x;
				if(new_tr_id!=tr_id)
				{
					$("#"+new_tr_id).attr('style','');
				}
				else
				{
					$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		
		$('#preview_button').attr('onclick','privew_queue_messages("'+id+'")');
		$('#preview_button').css('opacity','1');
	}
	
	
	
	function privew_queue_messages(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idMessage:id,sendflag:'QueuePrivew'},function(result){
		$("#show_message_preview").html(result);
		$("#show_message_preview").attr("style","display:block;");
		//addPopupScrollClass();
		});
	}
	
	
	function delete_messages(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idMessage:id,sendflag:'DeleteMsg'},function(result){
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr("style","display:block;");
		//addPopupScrollClass();
		});
	}
	
	function confirm_delete_queue_msg(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idMessage:id,sendflag:'ConfirmDeleteMsg'},function(result){
		$("#content").html(result);
		$("#ajaxLogin").attr("style","display:none;");
		//addPopupScrollClass();
		});
	}
	
	function  delete_news_feed(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editNews.php",{idNewFeed:id,flag:'deleteNewFeed'},function(result){
		$("#ajaxLogin").html(result);
		$("#ajaxLogin").attr("style","display:block;");
		//addPopupScrollClass();
		});
	}
	
	function  confirm_delete_news_feed(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editNews.php",{idNewFeed:id,flag:'confirmDelete'},function(result){
		$("#content").html(result);
		$("#ajaxLogin").attr("style","display:none;");
		//addPopupScrollClass();
		});
	}
	
	
	function  edit_news_feed(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editNews.php",{idNewFeed:id,flag:'edit_news_form'},function(result){
		$("#content").html(result);
		});
	}
	
	function show_preview_text()
	{
		var content = tinyMCE.get('szDescription');
		var n = content.getContent();
		//alert(n);
		$("#previewText").html(n);
	}
	
	function preview_rss_feed()
	{
		var content = tinyMCE.get('szDescription');
		var n = content.getContent();
		//alert(n);
		$("#previewText").html(n);
	}
	
	function  save_news(id)
	{
		var input_string='';
	    var encoded_strs='';
		var content = tinyMCE.get('szDescription');
		var n = content.getContent();
		url_enc=encodeURIComponent(n);
	   	encoded_strs=$.base64.encode(url_enc);
		var value=$('#saveNewFeed').serialize();
		var new_value=value+'&tinyContent='+encoded_strs;
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_editNews.php",new_value,function(result){
		$("#content").html(result);
		});
	}
	
	function showPageSystemMessage(page)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{sendflag:'SYSTEM_MESSAGES_CONTENT_NEXT_PAGE',page:page},function(result){
				$('#content').html(result);	
			});	
	}
	
	function select_message_system_send_tr(tr_id,id)
	{
		var rowCount = $('#send_message_list tr').length;
		if(rowCount>0)
		{
			for(x=1;x<rowCount;x++)
			{
				new_tr_id="message_data_"+x;
				if(new_tr_id!=tr_id)
				{
					$("#"+new_tr_id).attr('style','');
				}
				else
				{
					$("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
				}
			}
		}
		
		$('#preview_button').attr('onclick','privew_system_messages("'+id+'")');
		$('#preview_button').css('opacity','1');
	}
	
	function privew_system_messages(id)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idEmail:id,sendflag:'SystemMessagePrivew'},function(result){
		$("#show_message_preview").html(result);
		$("#show_message_preview").attr("style","display:block;");
		//addPopupScrollClass();
		});
	}
	
	function resend_system_messages(id,from_page,booking_file)
	{
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idEmail:id,sendflag:'ResendSystemMessage',from_page:from_page,booking_file:booking_file},function(result){
		$("#content_body").html(result);
		$("#content_body").attr("style","display:block;");
		addPopupScrollClass();
            });
	}
	
	function resend_system_message_to_user()
	{	
            $('#send_button').attr('onclick','');
            $('#send_button').css('opacity','0.4');

            $('#cancel_button').attr('onclick','');
            $('#cancel_button').css('opacity','0.4');

            var idEmail=$('#idEmail').val();
            var szToAddress=$('#szToAddress').val();
            var from_page = $('#from_page').val(); 
            var idBookingFile = $('#booking_file_id_popup').val();   

            $.post(__JS_ONLY_SITE_BASE__+"/ajax_reviewMessages.php",{idEmail:idEmail,szEmail:szToAddress,sendflag:'ConfirmSystem',from_page:from_page,booking_file:idBookingFile},function(result){
                var res_ary = result.split("||||");
                if(res_ary[0]=='SUCCESS')
                {
                    if(from_page=='PENDING_TRAY_LOG')
                    {
                        display_task_menu_details('LOG',idBookingFile);
                    }
                    else if(from_page=='PENDING_TRAY_CUSTOMER_LOG')
                    { 
                        display_task_menu_details('CUSTOMER_LOG',idBookingFile);
                    }
                    $("#content_body").attr("style","display:none;");
                } 
                else if(res_ary[0]=='REDIRECT')
                {
                    redirect_url(res_ary[1]);
                }
                else
                {
                    $("#content_body").html(result); 
                } 
                removePopupScrollClass();
            });
	}
	
	function limit_characters(input_id,iMaxLength)
	{	
		var field_value = $("#"+input_id).val();	 
		var iLength = field_value.length + 1;  
		var div_id = input_id+"_span";
		$("#"+div_id).html("("+ iLength+")"); 
	}
	
	function form_field_length()
	{
		//loop through all input elements
		$("form :input").each(function(){
			var input_id = $(this).attr('id');
			if(input_id!='')
			{
				var span_id = input_id+"_span";
				if($("#"+span_id).length)
				{
					iMaxLength = 160;
					limit_characters(input_id,iMaxLength);
				}
			}		    
		});
	}
	
	function delete_explain_level2_image(szFileName,flag,idExplanPage,image_type)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{mode:'DELETE_LEVEL2_IMAGE',szFileName:szFileName,flag:flag,idExplanPage:idExplanPage,image_type:image_type},function(result){
			if(flag=='temp')
			{
				if(image_type=='OPEN_GRAPH')
				{
					jQuery("#og_preview").html('');
					jQuery("#og_file_upload").val('');
					jQuery("#szUploadOGFileName").val('');
					jQuery("#iOGFileUpload").val('');
					$('#og_file_upload').replaceWith($('#og_file_upload').clone());
					$('#og_file_upload').attr('style','display:block;width:99%;');
				}
				else
				{
					jQuery("#preview").html('');
					jQuery("#file_upload").val('');
					jQuery("#szUploadFileName").val('');
					jQuery("#iFileUpload").val('');
					$('#file_upload').replaceWith($('#file_upload').clone());
					$('#file_upload').attr('style','display:block;width:99%;');
				}
				
			}
			else
			{
				$("#contactPopup").html(result);
				$("#contactPopup").attr('style','display:block;');
			}
		});
	}
	
	function deleteExplainPageImageConfirm(idExplanPage,szFileName,image_type)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{mode:'DELETE_LEVEL2_IMAGE_CONFIRM',szFileName:szFileName,idExplanPage:idExplanPage,image_type:image_type},function(result){
			
			if(image_type=='OPEN_GRAPH')
			{
				jQuery("#og_preview").html('');
				jQuery("#og_file_upload").val('');
				jQuery("#szUploadOGFileName").val('');
				jQuery("#iOGFileUpload").val('');
				$('#og_file_upload').replaceWith($('#og_file_upload').clone());
				$("#contactPopup").attr('style','display:none;');
				$('#og_file_upload').attr('style','display:block;width:99%;');
			}
			else
			{
				jQuery("#preview").html('');
				jQuery("#file_upload").val('');
				jQuery("#szUploadFileName").val('');
				jQuery("#iFileUpload").val('');
				$('#file_upload').replaceWith($('#file_upload').clone());
				$("#contactPopup").attr('style','display:none;');
				$('#file_upload').attr('style','display:block;width:99%;');
			}
			
		});
	}
	
	function delete_explain_level3_image(szFileName,flag,idExplanPage)
	{
		$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{mode:'DELETE_LEVEL3_IMAGE',szFileName:szFileName,flag:flag,idExplanPage:idExplanPage},function(result){
			if(flag=='temp')
			{
				jQuery("#preview").html('');
				jQuery("#file_upload").val('');
				jQuery("#szUploadFileName").val('');
				jQuery("#iFileUpload").val('');
				$('#file_upload').replaceWith($('#file_upload').clone());
				$('#file_upload').attr('style','display:block;width:99%;');
			}
			else
			{
				$("#contactPopup").html(result);
				$("#contactPopup").attr('style','display:block;');
			}
		});
	}
	
	function deleteExplainPageImageLevel3Confirm(idExplanPage,szFileName)
	{
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{mode:'DELETE_LEVEL3_IMAGE_CONFIRM',szFileName:szFileName,idExplanPage:idExplanPage},function(result){ 
                jQuery("#preview").html('');
                jQuery("#file_upload").val('');
                jQuery("#szUploadFileName").val('');
                jQuery("#iFileUpload").val('');
                $('#file_upload').replaceWith($('#file_upload').clone());
                $("#contactPopup").attr('style','display:none;');
                $('#file_upload').attr('style','display:block;width:99%;'); 
            });
	}
	
	function encode_string_msg_str(input_string)
	{ 
	   var encoded_strs='';
	   input_string = input_string.replace(/\~/g, '-');
	   var url_enc=encodeURIComponent(input_string);
	   encoded_strs=$.base64.encode(url_enc); 
	   return encoded_strs;
	}
	
	
function add_more_partner_logo(mode)
{ 
    if(mode=='ADD_MORE_TESTIMONIAL')
    {
        var number = document.getElementById('hiddenPosition_k').value; 
	var divid = document.getElementById('testimonial_main_container_div');;
	number=parseInt(number); 
	var hidden1 = document.getElementById('hiddenPosition1_k').value; 
	document.getElementById('hiddenPosition1_k').value=parseInt(hidden1)+1;
	document.getElementById('remove_k').style.display='block';  
	document.getElementById('hiddenPosition_k').value =  parseInt(number)+1;
    }
    else
    {
        var number = document.getElementById('hiddenPosition').value; 
	var divid = document.getElementById('partner_logo_container_main_div');;
	number=parseInt(number); 
	var hidden1 = document.getElementById('hiddenPosition1').value; 
	document.getElementById('hiddenPosition1').value=parseInt(hidden1)+1;
	document.getElementById('remove').style.display='block';  
	document.getElementById('hiddenPosition').value =  parseInt(number)+1;
    } 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",{number:number,mode:mode},function(result){
        if(mode=='ADD_MORE_TESTIMONIAL')
        {
            $("#testimonial_container_div_"+number).html(result);
            $("#testimonial_container_div_"+number).attr("style",'display:block;');
        }
        else
        {
            $("#partner_logo_container_div_"+number).html(result);
            $("#partner_logo_container_div_"+number).attr("style",'display:block;');
        } 
    });
    
    if(mode=='ADD_MORE_TESTIMONIAL')
    {        
        var newDiv=document.createElement('div'); 
        newDiv.setAttribute('id', 'testimonial_container_div_'+(number+1));
        divid.appendChild(newDiv);
    }
    else
    {        
        var newDiv=document.createElement('div'); 
        newDiv.setAttribute('id', 'partner_logo_container_div_'+(number+1));
        divid.appendChild(newDiv);
    }
}
function remove_partner_logo(mode)
{ 
    if(mode=='ADD_MORE_TESTIMONIAL')
    {
        var number = document.getElementById('hiddenPosition_k').value;
	var hidden1=document.getElementById('hiddenPosition1_k').value;
	
	var id=parseInt(hidden1)-1;
	div_id="testimonial_container_div_"+id;
	document.getElementById('hiddenPosition_k').value=parseInt(number)-1;
	document.getElementById('hiddenPosition1_k').value=parseInt(hidden1)-1;
	document.getElementById(div_id).innerHTML='';
	if(id==1)
	{
            document.getElementById('remove_k').style.display='none';
	} 
    }
    else
    {
        var number = document.getElementById('hiddenPosition').value;
	var hidden1=document.getElementById('hiddenPosition1').value;
	
	var id=parseInt(hidden1)-1;
	div_id="partner_logo_container_div_"+id;
	document.getElementById('hiddenPosition').value=parseInt(number)-1;
	document.getElementById('hiddenPosition1').value=parseInt(hidden1)-1;
	document.getElementById(div_id).innerHTML='';
	if(id==1)
	{
            document.getElementById('remove').style.display='none';
	} 
    } 
} 

function choose_language(lang_id,szPageUrl)
{
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_createExplainPage.php",{mode:'CHANGE_EXPLAING_PAGE_LANGUAGE',lang_id:lang_id,szPageUrl:szPageUrl},function(result)
	{
		res_ary = result.split("||||");
		if(res_ary[0]=='SUCCESS')
		{
			redirect_url(szPageUrl);
		} 
	});
}

function change_editor_background(color_code,descriptor_id)
{
    if(color_code.length)
    { 
        if(descriptor_id=='szSeoDescription')
        {
            descriptor_id = 'szSeoDescription' ;
        }
        else
        {
            descriptor_id = 'content' ;
        }
        
        var first_char = color_code.substring(0,1)
        if(first_char!='#')
        {
            color_code = "#"+color_code ;
            $("#szBackgroundColor").attr('value',color_code);	 
        }  
        if(color_code.length>1 && color_code.length<=7)
        {
            var editor = tinyMCE.getInstanceById(descriptor_id);
            editor.getBody().style.backgroundColor = color_code;
        }
        else
        {
            alert("Invalid color code.");
        }
    } 
}

function preview_data_level3(szPageUrl)
{
	var heading = $('#heading').val();
		 var id= $('#id').val();
		 var idExplain = $('#flag').val();
			
                var content = tinyMCE.get('content');
    		var n =content.getContent();
   			// alert(n);
                var description=encode_string_msg_str(n);
	   	var value1=$('#textEditorSubmit').serialize();
	   	var newValue=value1+"&description="+description+"&mode=PREVIEW_DATA_LEVEL3&idExplain="+idExplain;	
	   	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",newValue,function(result)
	{
		var win = window.open(szPageUrl, '_blank');
  				win.focus(); 
	});
}

function change_link_color(color_code)
{
	if(color_code.length)
	{
		var first_char = color_code.substring(0,1)
		if(first_char!='#')
		{
			color_code = "#"+color_code ;
			$("#szLinkColor").attr('value',color_code);	 
		} 
		
		if(color_code.length>1 && color_code.length<=7)
		{
			return true;
		} 
		else
		{
			alert("Invalid color code.");
		}
	} 
}

function select_insurance_rate_tr(div_id,id,type,insurance_id)
{   
    $(".insurance_rate_tr_type_"+type).attr('style','background:#ffffff;cursor:pointer;');
    $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
    
    if(type==1)
    {
        $("#insurance_rate_reuse_button_"+type).unbind("click");
        $("#insurance_rate_reuse_button_"+type).removeAttr('style');
        $("#insurance_rate_reuse_button_"+type).click(function(){delete_insurance_rate(id,type,'REUSE_INSURANCE_RATE')});
        
        $("#insurance_rate_delete_button_"+type).unbind("click");
        $("#insurance_rate_delete_button_"+type).removeAttr('style');
        $("#insurance_rate_delete_button_"+type).click(function(){delete_insurance_rate(id,type,'DELETE_INSURANCE_RATE')});

        $("#insurance_rate_add_edit_button_"+type).removeAttr('onclick');
        $("#insurance_rate_add_edit_button_"+type).unbind("click");
        $("#insurance_rate_add_edit_button_"+type).removeAttr('style');
        $("#insurance_rate_add_edit_button_"+type+" span").html('Edit');

        $("#insurance_rate_add_edit_button_"+type).click(function(){ edit_insurance_rate(id,type,'EDIT_INSURANCE') });  
        display_sell_rate_listing(id,2);
    }
    else
    {
        $("#insurance_rate_delete_button_"+type).unbind("click");
        $("#insurance_rate_delete_button_"+type).removeAttr('style');
        $("#insurance_rate_delete_button_"+type).click(function(){delete_insurance_rate(id,type,'DELETE_INSURANCE_RATE')});

        $("#insurance_rate_add_edit_button_"+type).removeAttr('onclick');
        $("#insurance_rate_add_edit_button_"+type).unbind("click");
        $("#insurance_rate_add_edit_button_"+type).removeAttr('style');
        $("#insurance_rate_add_edit_button_"+type+" span").html('Edit');

        $("#insurance_rate_add_edit_button_"+type).click(function(){ edit_insurance_rate(id,type,'EDIT_INSURANCE_RATE') }); 
    }  
}

function display_sell_rate_listing(idInsurance,iType,deleted_all)
{
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",{idInsurance:idInsurance,deleted_all:deleted_all,mode:'DISPLAY_SELL_RATE_LISTING'},function(result){

           var result_ary = result.split("||||");
           if(jQuery.trim(result_ary[0])=='ERROR')
           {  
              alert("Invalid insurance id.");
           }
           else if(jQuery.trim(result_ary[0])=='SUCCESS')
           {
               $("#insurance_buyer_list_container_"+iType).html(result_ary[1]);

               $("#insurance_rate_add_edit_button_"+iType).unbind("click"); 
               $("#insurance_rate_add_edit_button_"+iType+" span").html('Add'); 
            }
            $("#loader").attr('style','display:none;');
       }); 
}
function select_insuarance_tr(div_id,id,type,insurance_id)
{
    var rowCount = $('#insurance_rate_table_'+type+' tr').length;

    if(rowCount>0)
    {
        for(t=1;t<rowCount;++t)
        {
            var newdivid="insurance_rate_"+type+"_"+t;

            if(newdivid!=div_id)
            {
                $("#"+newdivid).attr('style','background:#ffffff;cursor:pointer;');
            }
            else
            {
                $(".insurance_rate_tr_type_").attr('style','background:#ffffff;cursor:pointer;');
                $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
            }
        }
    } 
}

function edit_insurance_rate(idInsurateRate,iType,mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",{idInsurateRate:idInsurateRate,iType:iType,mode:mode},function(result){

             result_ary = result.split("||||");
     if(jQuery.trim(result_ary[0])=='ERROR')
     {  
            alert("Invalid insurance rate id.");
     }
     else if(jQuery.trim(result_ary[0])=='SUCCESS')
     {
            $("#insurance_buyer_form_container_"+iType).html(result_ary[1]);

            $("#insurance_rate_add_edit_button_"+iType).unbind("click");
                    $("#insurance_rate_add_edit_button_"+iType).removeAttr('style');
                    $("#insurance_rate_add_edit_button_"+iType).removeAttr('onclick');
                    $("#insurance_rate_add_edit_button_"+iType+" span").html('Save');

                    $("#insurance_rate_add_edit_button_"+iType).click(function(){ submitInsuranceRates(iType) });
     }
    });
}

function delete_insurance_rate(idInsurateRate,iType,mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",{idInsurateRate:idInsurateRate,iType:iType,mode:mode},function(result){
			
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
            $("#insurance_rate_popup").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {	
            if(mode=='DELETE_INSURANCE_RATE_CONFIRM')
            {
                $("#insurance_rate_popup").attr('style','display:none;');
                $("#insurance_buyer_list_container_"+iType).html(result_ary[1]);
                if(iType==1)
                {
                    $("#insurance_buyer_list_container_2").html(result_ary[2]);
                }
            }
            else
            {         	
                $("#insurance_rate_popup").html(result_ary[1]);		
                $("#insurance_rate_popup").attr('style','display:block');
            }
        }
    });
} 

function submitInsuranceRates(iInsuranceType)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php?iType="+iInsuranceType,$('#insurance_form_type_'+iInsuranceType).serialize(),function(result){

        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#insurance_buyer_form_container_"+iInsuranceType).html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#insurance_buyer_list_container_"+iInsuranceType).html(result_ary[1]);
            if(iInsuranceType==1)
            {
                $("#insurance_buyer_list_container_2").html(result_ary[2]);
            }
           
        }
    });
}

function format_decimat(form_field,evt)
{
    var charcode=(evt.which)?(evt.which):(event.keyCode); 
    if (charcode > 31 && (charcode < 45 || charcode > 57) && charcode!=47)  // 47 means /
    {
        return false;
    }
    return true;
} 
function select_insured_booking_confirmation_tr(div_id,idBooking,from_page,cancelled_insurance,version,iCancelledBooking)
{	
    var rowCount = $('#insuranced_booking_table tr').length;

    if(rowCount>0)
    {
        for(t=1;t<rowCount;++t)
        {
            newdivid="booking_data_"+t;

            if(newdivid!=div_id)
            {
                $("#"+newdivid).attr('style','background:#ffffff;cursor:pointer;');
            }
            else
            {
                $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
            }
        }
    } 
	
    if(from_page=='CANCELLED_INSURED_BOOKING')
    {
        $("#insured_booking_cancel_button").unbind("click");
        $("#insured_booking_cancel_button").removeAttr('style');
        $("#insured_booking_cancel_button").click(function(){ download_admin_insurance_credit_note_pdf(idBooking,version,iCancelledBooking); });
    }
    else
    {
        if(cancelled_insurance>0)
        {
            $("#insured_booking_cancel_button").unbind("click");
            $("#insured_booking_cancel_button").removeAttr('style');
            $("#insured_booking_cancel_button").attr('class','button1');
            $("#insured_booking_cancel_button").html('<span>Credit Note</span>');
            $("#insured_booking_cancel_button").click(function(){ download_admin_insurance_credit_note_pdf(idBooking,version,iCancelledBooking); });
        }
        else
        {
            $("#insured_booking_cancel_button").unbind("click");
            $("#insured_booking_cancel_button").removeAttr('style');
            $("#insured_booking_cancel_button").attr('class','button2');
            $("#insured_booking_cancel_button").html('<span>cancel</span>');
            $("#insured_booking_cancel_button").click(function(){ cancel_insured_booking(idBooking,'CANCEL_NEW_INSURED_BOOKING',from_page); });
        } 
    }  
    $("#insured_booking_invoice_button").unbind("click");
    $("#insured_booking_invoice_button").removeAttr('style');  
    $("#insured_booking_invoice_button").click(function(){ download_admin_insurance_invoice_pdf(idBooking,version) }); 
}

function cancel_insured_booking(booking_id,mode,from_page)
{
	var iSendCreditNote='';
	if(mode=='CANCEL_NEW_INSURED_BOOKING_CONFIRM')
	{
		var send_credit_note = $("#iSendCreditNote").prop("checked");
		if(send_credit_note)
		{
			iSendCreditNote = 1
		}
	} 
        var page=$("#page").attr('value');
        var limit=$("#limit").attr('value');
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",{page:page,limit:limit,booking_id:booking_id,from_page:from_page,mode:mode,iSendCreditNote:iSendCreditNote},function(result){
			
		 result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
         	$("#insurance_rate_popup").html(result_ary[1]);		
         	$("#insurance_rate_popup").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {	 
         	if(mode=='CANCEL_NEW_INSURED_BOOKING_CONFIRM')
         	{
         	   $("#insurance_rate_popup").attr('style','display:none;');
        	   $("#insured_booking_list_container").html(result_ary[1]); 
         	}
         	else
         	{
         		$("#insurance_rate_popup").html(result_ary[1]);		
         		$("#insurance_rate_popup").attr('style','display:block;');
         	} 
         }
	});
}

function select_all_checkboxes(cb_id,from_page)
{
	var cb_flag = $("#"+cb_id).prop("checked"); 
	if(cb_flag)
	{
		$(".insured-booking-check-boxes").prop("checked",'checked');
		enable_send_button(from_page);
	}
	else
	{
		$(".insured-booking-check-boxes").removeProp("checked");
		enable_send_button(from_page);
	}
}

function enable_send_button(from_page)
{
    var count = 1 ;
    var t =0;
    $(".insured-booking-check-boxes").each(function(element){
        var cb_id = this.id
        var cb_flag = $("#"+cb_id).prop("checked");
        if(cb_flag){
         $("#iCheckValueFlag_"+t).attr("checked",true);
        }else
        {
            $("#iCheckValueFlag_"+t).attr("checked",false);
        }
        if(cb_flag)
        {
            count = 2 ; 
        }
        ++t;
    }); 

    if(count==2)
    {
        if(from_page=='SENT_BOOKING')
        {
            $("#insured_booking_send_button").unbind("click");
            $("#insured_booking_send_button").removeAttr('style');  
            $("#insured_booking_send_button").click(function(){ confirm_insured_booking() }); 
        }
        else
        {
            $("#insured_booking_send_button").unbind("click");
            $("#insured_booking_send_button").removeAttr('style');  
            $("#insured_booking_send_button").click(function(){ send_insured_booking() }); 
        } 
    } 
    else
    {
        $("#insured_booking_send_button").unbind("click");
        $("#insured_booking_send_button").attr('style','opacity:0.4');
    }
}

function confirm_insured_booking()
{  
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",$('#confirm_insured_booking_form').serialize(),function(result){
	
		 result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
         	$("#insurance_rate_popup").html(result_ary[1]);		
         	$("#insurance_rate_popup").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
         	$("#insurance_rate_popup").html(result_ary[1]);		
         	$("#insurance_rate_popup").attr('style','display:block');
         }
	});
}


function send_insured_booking()
{ 
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",$('#send_new_insured_booking').serialize(),function(result){
	
		 result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
         	$("#insurance_rate_popup").html(result_ary[1]);		
         	$("#insurance_rate_popup").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
         	$("#insurance_rate_popup").html(result_ary[1]);		
         	$("#insurance_rate_popup").attr('style','display:block');
         }
	});
}

function check_insurace_send_email(kEvent,formId,inputId)
{	
    if(isFormInputElementEmpty(formId, inputId)) 
    {  
       $("#email_vendor_send_button").attr("onclick",''); 
       $("#email_vendor_send_button").attr("style",'opacity:0.4;');
       $("#email_vendor_send_button").unbind("click"); 
    }
    else if(!isValidEmail($('form#' + formId + ' #'+ inputId ).val())) 
    {   
       $("#email_vendor_send_button").attr("onclick",''); 
       $("#email_vendor_send_button").attr("style",'opacity:0.4;');
       $("#email_vendor_send_button").unbind("click"); 
    }
    else
    { 
        $("#email_vendor_send_button").attr("onclick",'');
    	$("#email_vendor_send_button").attr("class",'button1');
    	$("#email_vendor_send_button").attr("style",'opacity:1;');
    	$("#email_vendor_send_button").unbind("click"); 
        $("#email_vendor_send_button").click(function(){send_insurance_email()});
		 
    	if(kEvent.keyCode==13)
		{
			send_insurance_email();
		} 
    } 
}

function send_insurance_email()
{	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",$('#send_isurance_vendor_email').serialize(),function(result){
	
		 result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
         	$("#insurance_rate_popup").html(result_ary[1]);		
         	$("#insurance_rate_popup").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
         	$("#insured_booking_list_container").html(result_ary[1]);		
         	$("#insurance_rate_popup").attr('style','display:none');
         	$("#insured_booking_list_container").attr('style','display:block');
         }
	});
}

function confirm_insurance_booking_submit()
{	
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",$('#confirm_insured_booking_form_popup').serialize(),function(result){ 
        result_ary = result.split("||||");
        
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
            $("#insurance_rate_popup").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#insured_booking_list_container").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:none');
            $("#insured_booking_list_container").attr('style','display:block');
        }
    });
}

function select_all()
{
    var obj = $("#ZeroClipboardMovie_1")
    var text_val=eval(obj);
    text_val.focus();
    text_val.select(); 
} 

function select_booking_quote_confirmation_tr(div_id,idBooking,from_page,iQuotationReady)
{	
    var rowCount = $('#insuranced_booking_table tr').length;

    if(rowCount>0)
    {
        for(t=1;t<rowCount;++t)
        {
            newdivid="booking_data_"+t;

            if(newdivid!=div_id)
            {
                    $("#"+newdivid).attr('style','background:#ffffff;cursor:pointer;');
            }
            else
            {
                    $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
            }
        }
    }  

    $("#booking_quote_edit_button").unbind("click");
    $("#booking_quote_edit_button").removeAttr('style');  
    $("#booking_quote_edit_button").click(function(){ display_manual_quote_form(idBooking,'DISPLAY_MANUAL_QUOTE_FORM') }); 
    
    if(iQuotationReady==1)
    {
        $("#booking_quote_send_button").unbind("click");
        $("#booking_quote_send_button").removeAttr('style');  
        $("#booking_quote_send_button").click(function(){ display_manual_quote_form(idBooking,'DISPLAY_SEND_QUEOTS_POPUP') }); 
    }
    else
    {
        $("#booking_quote_send_button").unbind("click");
        $("#booking_quote_send_button").attr('style','opacity:0.4');  
    }
    $("#booking_quote_delete_button").unbind("click");
    $("#booking_quote_delete_button").removeAttr('style');  
    $("#booking_quote_delete_button").click(function(){ display_manual_quote_form(idBooking,'DELETE_MANUAL_QUEOTS_POPUP') });
}

function select_booking_quote_sent_tr(div_id,idBooking,from_page,expire_flag)
{	
    var rowCount = $('#insuranced_booking_table tr').length;

    if(rowCount>0)
    {
        for(t=1;t<rowCount;++t)
        {
            newdivid="booking_data_"+t;

            if(newdivid!=div_id)
            {
                $("#"+newdivid).attr('style','background:#ffffff;cursor:pointer;');
            }
            else
            {
                $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
            }
        }
    }  

    if(expire_flag==1)
    {
        $("#booking_quote_view_button").unbind("click");
        $("#booking_quote_view_button").removeAttr('style');  
        $("#booking_quote_view_button").click(function(){ display_manual_quote_form(idBooking,'VIEW_MANUAL_QUEOTS',from_page) }); 
        
        $("#booking_quote_copy_button").unbind("click");
        $("#booking_quote_copy_button").removeAttr('style');  
        $("#booking_quote_copy_button").click(function(){ display_manual_quote_form(idBooking,'COPY_MANUAL_QUEOTS') }); 
    }
    else
    { 
        $("#booking_quote_edit_button").unbind("click");
        $("#booking_quote_edit_button").removeAttr('style');  
        $("#booking_quote_edit_button").click(function(){ display_manual_quote_form(idBooking,'DISPLAY_MANUAL_QUOTE_FORM',from_page) }); 

        $("#booking_quote_send_button").unbind("click");
        $("#booking_quote_send_button").removeAttr('style');  
        $("#booking_quote_send_button").click(function(){ display_manual_quote_form(idBooking,'DISPLAY_SEND_QUEOTS_POPUP',from_page) });  

        $("#booking_quote_expire_button").unbind("click");
        $("#booking_quote_expire_button").removeAttr('style');  
        $("#booking_quote_expire_button").click(function(){ display_manual_quote_form(idBooking,'DISPLAY_EXPIRE_QUEOTS_POPUP') });

        $("#booking_quote_copy_button").unbind("click");
        $("#booking_quote_copy_button").removeAttr('style');  
        $("#booking_quote_copy_button").click(function(){ display_manual_quote_form(idBooking,'COPY_MANUAL_QUEOTS') }); 

        $("#booking_quote_delete_button").unbind("click");
        $("#booking_quote_delete_button").removeAttr('style');  
        $("#booking_quote_delete_button").click(function(){ display_manual_quote_form(idBooking,'DELETE_MANUAL_QUEOTS_POPUP',from_page) });
    }
}

function autofill_billing_address(inputId,flag)
{ 
    var iShipperConsignee = $("#iShipperConsignee").val();
    if((iShipperConsignee==1 || iShipperConsignee==2))
    {
        var szCompanyName = '';
        var szFirstName = '';
        var szLastName = ''; 
        var szEmail = '';
        var idDialCode = '';
        var szPhone = '';
        var szAddress = '';
        var szPostcode = '';
        var szCity = '';  
        var idCountry = '';
        
        if(iShipperConsignee==1)
        { 
            szCompanyName = $("#szShipperCompanyName").val();
            szFirstName = $("#szShipperFirstName").val();
            szLastName = $("#szShipperLastName").val();
            szEmail = $("#szShipperEmail").val();
            idDialCode = $("#idShipperDialCode").val();
            
            szPhone = $("#szShipperPhone").val();
            szAddress = $("#szShipperAddress1").val();
            szPostcode = $("#szShipperPostcode").val();
            szCity = $("#szShipperCity").val();
            idCountry = $("#idShipperCountry").val(); 
        }
        else
        { 
            szCompanyName = $("#szConsigneeCompanyName").val();
            szFirstName = $("#szConsigneeFirstName").val();
            szLastName = $("#szConsigneeLastName").val();
            szEmail = $("#szConsigneeEmail").val();
            idDialCode = $("#idConsigneeDialCode").val(); 
            szPhone = $("#szConsigneePhone").val();
            szAddress = $("#szConsigneeAddress1").val();
            szPostcode = $("#szConsigneePostcode").val();
            szCity = $("#szConsigneeCity").val();
            idCountry = $("#idConsigneeCountry").val(); 
        } 
        $("#szBillingCompanyName").val(szCompanyName);
        $("#szBillingFirstName").val(szFirstName);
        $("#szBillingLastName").val(szLastName);
        $("#szBillingEmail").val(szEmail);
        $("#idBillingDialCode").val(idDialCode);
        $("#szBillingPhone").val(szPhone);
        $("#szBillingAddress1").val(szAddress);
        $("#szBillingPostcode").val(szPostcode);
        $("#szBillingCity").val(szCity); 
        $("#idBillingCountry").val(idCountry); 
        
        $("#szBillingCompanyName").css('background-color','#D3D3D3');
        $("#szBillingFirstName").css('background-color','#D3D3D3');
        $("#szBillingLastName").css('background-color','#D3D3D3');
        $("#szBillingEmail").css('background-color','#D3D3D3');
        $("#idBillingDialCode").css('background-color','#D3D3D3');
        $("#szBillingPhone").attr('style','background-color:#D3D3D3;width:120px;'); 
        $("#szBillingAddress1").css('background-color','#D3D3D3');
        $("#szBillingPostcode").css('background-color','#D3D3D3');
        $("#szBillingCity").css('background-color','#D3D3D3');
        $("#idBillingCountry").css('background-color','#D3D3D3');
        
        $("#szBillingCompanyName").attr('readonly','readonly');
        $("#szBillingFirstName").attr('readonly','readonly');
        $("#szBillingLastName").attr('readonly','readonly');
        $("#szBillingEmail").attr('readonly','readonly');
        $("#idBillingDialCode").attr('readonly','readonly');
        $("#szBillingPhone").attr('readonly','readonly');
        $("#szBillingAddress1").attr('readonly','readonly');
        $("#szBillingPostcode").attr('readonly','readonly');
        $("#szBillingCity").attr('readonly','readonly');
        $("#idBillingCountry").attr('readonly','readonly'); 
    } 
    else
    { 
        $("#szBillingCompanyName").removeAttr('readonly');
        $("#szBillingFirstName").removeAttr('readonly');
        $("#szBillingLastName").removeAttr('readonly');
        $("#szBillingEmail").removeAttr('readonly');
        $("#idBillingDialCode").removeAttr('readonly');
        $("#szBillingPhone").removeAttr('readonly');
        $("#szBillingAddress1").removeAttr('readonly');
        $("#szBillingPostcode").removeAttr('readonly');
        $("#szBillingCity").removeAttr('readonly');
        $("#idBillingCountry").removeAttr('readonly');
        
        
        $("#szBillingCompanyName").css('background-color','');
        $("#szBillingFirstName").css('background-color','');
        $("#szBillingLastName").css('background-color','');
        $("#szBillingEmail").css('background-color','');
        $("#idBillingDialCode").css('background-color',''); 
        $("#szBillingPhone").attr('style','width:120px;'); 
        $("#szBillingAddress1").css('background-color','');
        $("#szBillingPostcode").css('background-color','');
        $("#szBillingCity").css('background-color','');
        $("#idBillingCountry").css('background-color','');
    } 
} 

function display_manual_quote_form(idBooking,mode,from_page)
{
    if(mode=='COPY_MANUAL_QUEOTS')
    {
        $(".manual-quotes-li").removeClass('active');
        $("#manual-quotes-li-new").addClass('active');
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingQuotes.php",{idBooking:idBooking,mode:mode,from_page:from_page},function(result){
	
        result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
            $("#insurance_rate_popup").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         { 
            $("#booking_quotes_list_container").html(result_ary[1]);	 
            $("#insurance_rate_popup").attr('style','display:none');
            $("#booking_quotes_list_container").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='POPUP')
         {
            $("#insurance_rate_popup").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:block');
         }
    });
}
function display_manual_quote_listings(from_page)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingQuotes.php",{mode:'DISPLAY_MANUAL_QUEOTS_LISTING',from_page:from_page},function(result){
	
        result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
            $("#insurance_rate_popup").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
            $("#booking_quotes_list_container").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:none');
            $("#booking_quotes_list_container").attr('style','display:block');
         }
    });
}

function update_mandatory_field(sell_price)
{
    var fSellPrice= parseFloat(sell_price);
    
    if(!isNaN(fSellPrice))
    {
        $("#iInsuranceMandatory").removeAttr('disabled'); 
    }
    else
    {
       // $("#iInsuranceMandatory").attr('disabled','disabled');
    }
}

function validate_send_booking_quotes(formId,from_page,shipping_date)
{
    if(shipping_date==1)
    {        
        if(isFormInputElementEmpty(formId,'dtTimingDate')) 
        {		
           $("#dtTimingDate").addClass('red_border');
           return false;
        } 
    }
    else
    { 
        if(isFormInputElementEmpty(formId,'szUserEmail')) 
        {		
           $("#szUserEmail").addClass('red_border');
           return false;
        }
        else if(!isValidEmail($('form#' + formId + ' #szUserEmail' ).val())) 
        {   
           $("#szUserEmail").addClass('red_border');
           return false;
        }
    } 
    
    send_booking_quotes(formId,from_page); 
}
function send_booking_quotes(formId,from_page)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingQuotes.php?from_page="+from_page,$("#"+formId).serialize(),function(result){
	
        result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
            $("#insurance_rate_popup").html(result_ary[1]);		
            $("#insurance_rate_popup").attr('style','display:block');
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         { 	
            $("#insurance_rate_popup").attr('style','display:none');
            $("#booking_quotes_list_container").html(result_ary[1]);
         }
    });
}
function submit_manual_quote_form()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingQuotes.php",$("#manual_booking_quote_details_form").serialize(),function(result){
	
        result_ary = result.split("||||");
         if(jQuery.trim(result_ary[0])=='ERROR')
         {  
            $("#booking_quotes_list_container").html(result_ary[1]);
         }
         else if(jQuery.trim(result_ary[0])=='SUCCESS')
         {
            $("#booking_quotes_list_container").html(result_ary[1]);	 
         }
    });
}
function display_verify_popup(user_id,mode,from_page,file_id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingQuotes.php",{mode:mode,user_id:user_id,from_page:from_page,file_id:file_id},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block;'); 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_CONFIRM') 
        { 
           showEditCustomerPages('setting',user_id);
           $("#contactPopup").attr('style','display:none;'); 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_TASK') 
        {  
           $("#pending_task_overview_main_container").html(result_ary[1]); 
           $("#pending_task_overview_main_container").attr('style','display:block;'); 
           
           $("#verify_email_button").unbind("click");  
           $("#verify_email_button").attr('class','gray-btn'); 
           
           $("#confirmed_email_task_no").attr('style','display:none;');
           $("#confirmed_email_task_yes").attr('style','display:inline-block;');
           var szEmail = $("#szEmail").val();
           $("#szCustomerOldEmail").val(szEmail);
           $("#contactPopup").attr('style','display:none;'); 
        }
    });
}

function close_open_div(divid,dividhref,type,quick_quote,quick_quote_flag)
{
    var iScrollUp = 0;
    if(quick_quote==1)
    {
        iScrollUp = 1;
    }
    else if(quick_quote==2)
    {
        iScrollUp = 0;
        quick_quote = 1;
    }
    if(type=='close')
    {
        $("#"+dividhref).attr("onclick","");
        $("#"+dividhref).unbind("click");
        $("#"+dividhref).click(function(){close_open_div(divid,dividhref,'open',quick_quote)}); 
        $("#"+divid).slideUp();
        //$("#"+divid).attr('style','display:none;');
        $("#"+dividhref).removeClass('close-icon').addClass('open-icon'); 
    }
    else if(type=='open')
    {
        $("#"+dividhref).attr("onclick","");
        $("#"+dividhref).unbind("click");
        $("#"+dividhref).click(function(){close_open_div(divid,dividhref,'close',quick_quote)});
        $("#"+divid).slideDown(); 
        $("#"+dividhref).removeClass('open-icon').addClass('close-icon');
        if(iScrollUp==1)
        {
            if(quick_quote_flag==1)
            {
                var document_height = $("#quick_quote_booking_infomation_main_container").offset().top; 
                console.log("document_height"+document_height);
            }
            else
            {
                var document_height = $(document).height();
            }
            $('html, body').animate({ scrollTop: document_height }, "100000");
        }
    }
} 

function display_pending_task_details(div_id,idBookingFile,idFileOwner,idBookingQuote,iQuoteClosed,iCourierBooking,szCopyMode,crm_email,reminder_notes,offline_chat_id)
{	
    var rowCount = $('#insuranced_booking_table tr').length;  
    if(rowCount>0)
    {
        $("#button_close_file").unbind("click");
        $('#button_close_file').attr('style','opacity:0.4');
        $("#idFileForCloseCount").attr('value','0');
        $("#idFileForClose").attr('value','');
        $("#idFileForCloseCtr").attr("value",'');
        $(".pendingTrayList").attr('checked',false);
        for(t=1;t<=rowCount;++t)
        {
            newdivid="booking_data_"+t;

            if(newdivid!=div_id)
            {
                $("#"+newdivid).attr('style','background:#ffffff;cursor:pointer;');
                $("#pendingTrayList_"+t).attr('checked',false); 
            }
            else
            {
                $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
                if($("#pendingTrayList_"+t).length>0)
                {
                    //$("#pendingTrayList_"+t).attr('checked',true);

                    $("#button_close_file").unbind("click");
                    $('#button_close_file').attr('style','opacity:1');
                    $("#button_close_file").click(function(){ openMultipleCloseBookingFile(); });
                    $("#button_close_file").removeClass('button2').addClass('button1');

                     $("#idFileForClose").attr('value',idBookingFile);
                     $("#idFileForCloseCount").attr('value','1');
                     $("#idFileForCloseCtr").attr("value",t);
                 }
            }
        }
        $("#button_copy_file_header").unbind("click");
        $("#button_copy_file_header").attr('style','opacity:0.4');  
           
        $("#button_quick_quote_file_header").unbind("click");
        $("#button_quick_quote_file_header").removeAttr('style');  
        $("#button_quick_quote_file_header").click(function(){ showCopyConfirmationPopUp('COPY_QUICK_QUOTE_BOOKING',idBookingFile,'0'); });
        
        //if(parseInt(iCourierBooking)==0)
        //{
            $("#button_copy_file_header").unbind("click");
            $("#button_copy_file_header").removeAttr('style');  
            $("#button_copy_file_header").click(function(){ showCopyConfirmationPopUp('COPY_PENDING_TASK_POP_UP',idBookingFile,'0'); }); 
        //} 
        $("#hidden_selected_file_id").val(idBookingFile);
        $("#idLastClikedBookingFile").val(idBookingFile);
        if(idFileOwner==1)
        { 
            display_pending_task_overview('DISPLAY_FILE_OWNER_CONFIRM_POPUP',idBookingFile,'',idBookingQuote,iQuoteClosed,iCourierBooking,'','','',crm_email,reminder_notes,offline_chat_id);
        }
        else
        {
            if(iCourierBooking==1)
            {
                display_pending_task_overview('DISPLAY_PENDING_TASK_COURIER_LABEL_UPLOAD_FORM',idBookingFile);
            }
            else
            { 
                display_pending_task_overview('DISPLAY_PENDING_TASK_DETAILS',idBookingFile,'',idBookingQuote,iQuoteClosed,'','','',szCopyMode,crm_email,reminder_notes,offline_chat_id);
            } 
        } 
    } 
    $("#loader").attr('style','display:none;');
}
function showCopyConfirmationPopUp(mode,file_id,from_page)
{
    if(mode=='COPY_QUICK_QUOTE_BOOKING')
    {
        $("#button_quick_quote_file_header").addClass('get-rates-btn-clicked');
        $("#recent_task_quick_quote_button").addClass('get-rates-btn-clicked'); 
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,file_id:file_id,from_page:from_page},function(result){
        
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_REDIRECT')
        {
            redirect_url(result_ary[1]);
        }
        if(mode=='COPY_QUICK_QUOTE_BOOKING')
        {
            $("#button_quick_quote_file_header").removeClass('get-rates-btn-clicked');
            $("#recent_task_quick_quote_button").removeClass('get-rates-btn-clicked');
        }
    });
}

function enableQuickQuotebutton(idBookingFile)
{
    $("#button_quick_quote_file_header").unbind("click");
    $("#button_quick_quote_file_header").removeAttr('style');  
    $("#button_quick_quote_file_header").click(function(){ showCopyConfirmationPopUp('COPY_QUICK_QUOTE_BOOKING',idBookingFile,'0'); });
    
    $("#button_copy_file_header").unbind("click");
    $("#button_copy_file_header").removeAttr('style');  
    $("#button_copy_file_header").click(function(){ showCopyConfirmationPopUp('COPY_PENDING_TASK_POP_UP',idBookingFile,'0'); }); 
} 
function display_pending_task_overview(mode,file_id,from_page,idBookingQuote,iQuoteClosed,iCourierBooking,iClosed,idBooking,szCopyMode,crm_email,reminder_notes,offline_chat_id)
{  
    $('body').removeClass('popup-body-scroll');
    $("#loader").attr('style','display:block');
    $("#contactPopup").attr('style','display:none');
      
    if(mode=='COPY_PENDING_TASK' || mode=='COPY_PENDING_TASK_ANOTHER')
    {
        close_open_div('pending_task_main_list_container_div','pending_task_list_open_close_link','close');
        close_open_div('pending_task_overview_container','pending_task_overview_open_close_link','close');
        
        $("#button_close_file").unbind("click");
        $('#button_close_file').attr('style','opacity:0.4');
        $("#button_close_file").removeClass('button1').addClass('button2');
    }
    
    if(szCopyMode=='copy')
    {
        $("#pending_task_list_open_close_link").removeClass('close-icon').addClass('open-icon');
        $("#pending_task_list_open_close_link").attr("onclick","");
        $("#pending_task_list_open_close_link").unbind("click");
        $("#pending_task_list_open_close_link").click(function(){close_open_div("pending_task_main_list_container_div","pending_task_list_open_close_link",'open')});
        $("#pending_task_main_list_container_div").attr('style','display:none;');

        $("#pending_task_overview_open_close_link").removeClass('close-icon').addClass('open-icon');
        $("#pending_task_overview_open_close_link").attr("onclick","");
        $("#pending_task_overview_open_close_link").unbind("click");
        $("#pending_task_overview_open_close_link").click(function(){close_open_div("pending_task_overview_container","pending_task_overview_open_close_link",'open')});
       // $("#pending_task_overview_container").attr('style','display:none;');

    } 
    enableQuickQuotebutton(file_id);
    var iReminderNotes = reminder_notes; 
    var idOfflineChatMessage = offline_chat_id;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,file_id:file_id,from_page:from_page,idBookingQuote:idBookingQuote,iQuoteClosed:iQuoteClosed,iCourierBooking:iCourierBooking,szCopyMode:szCopyMode},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            $("#contactPopup").attr('style','display:none');
            var szOverViewContainer = result_ary[1];
            $("#pending_task_overview_main_container").html(result_ary[1]);	
            
             
            $("#pending_task_tray_main_container").html(result_ary[2]);		
            $("#pending_task_tray_main_container").attr('style','display:block;'); 
           
            if(szCopyMode!='copy')
            {
                $("#pending_task_overview_main_container").attr('style','display:block;'); 
                $('html, body').animate({ scrollTop: ($("#task_pending_tray_main_container").offset().top - 10) }, "100000"); 
            } 
            if(jQuery.trim(szOverViewContainer)=='')
            {
                $("#pending_task_overview_main_container").attr('style','display:none;'); 
            }
            if(iReminderNotes>0)
            { 
                /*
                var reminder_notes_div = "overview_task_pending_tray_hidden_span_"+iReminderNotes;
                if($("#"+reminder_notes_div).length)
                {
                    var div_scroll_height = $("#"+reminder_notes_div).offset().top - $(window).height(); 
                    $('html, body').animate({ scrollTop: div_scroll_height }, "500000");   
                } 
                */
            }
            else if(idOfflineChatMessage>0) 
            { 
                /*
                var reminder_notes_div = "customer_log_chat_history_details_"+idOfflineChatMessage; 
                if($("#"+reminder_notes_div).length)
                {
                    var div_scroll_height = $("#"+reminder_notes_div).offset().top - $(window).height(); 
                    $('html, body').animate({ scrollTop: div_scroll_height }, "100000");
                } 
                */
            } 
            else if(crm_email==1)
            {
                /*
                if($("#pending_task_overview_main_container").length)
                {
                    $('html, body').animate({ scrollTop: ($("#pending_task_overview_main_container").offset().top - 20) }, "100000");   
                } 
                */
            }  
            
            if(mode=='COPY_PENDING_TASK_ANOTHER')
            {
                $("#pending_task_list_open_close_link").removeClass('close-icon').addClass('open-icon');
                $("#pending_task_list_open_close_link").attr("onclick","");
                $("#pending_task_list_open_close_link").unbind("click");
                $("#pending_task_list_open_close_link").click(function(){close_open_div("pending_task_main_list_container_div","pending_task_list_open_close_link",'open')});
                $("#pending_task_main_list_container_div").attr('style','display:none;');
                $("#pending_task_overview_open_close_link").removeClass('close-icon').addClass('open-icon');
                $("#pending_task_overview_open_close_link").attr("onclick","");
                $("#pending_task_overview_open_close_link").unbind("click");
                $("#pending_task_overview_open_close_link").click(function(){close_open_div("pending_task_overview_container","pending_task_overview_open_close_link",'open')});
                $("#pending_task_overview_container").attr('style','display:none;'); 
            }
            
            if(szCopyMode=='copy')
            {
                $("#pending_task_list_open_close_link").removeClass('close-icon').addClass('open-icon');
                $("#pending_task_list_open_close_link").attr("onclick","");
                $("#pending_task_list_open_close_link").unbind("click");
                $("#pending_task_list_open_close_link").click(function(){close_open_div("pending_task_main_list_container_div","pending_task_list_open_close_link",'open')});
                $("#pending_task_main_list_container_div").attr('style','display:none;');
                
                $("#pending_task_overview_open_close_link").removeClass('close-icon').addClass('open-icon');
                $("#pending_task_overview_open_close_link").attr("onclick","");
                $("#pending_task_overview_open_close_link").unbind("click");
                $("#pending_task_overview_open_close_link").click(function(){close_open_div("pending_task_overview_container","pending_task_overview_open_close_link",'open')});
              //  $("#pending_task_overview_container").attr('style','display:none;'); 
            } 
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_LIST')
        { 	
           $("#contactPopup").attr('style','display:none');
           updatePendingTrayListing(result_ary[1]);
           //$("#pending_task_listing_container").html(result_ary[1]);		
           $("#pending_task_listing_container").attr('style','display:block;');
           
           $("#pending_task_overview_main_container").html(result_ary[2]);		
           $("#pending_task_overview_main_container").attr('style','display:block;'); 
           
           $("#pending_task_tray_main_container").html(result_ary[3]);		
           $("#pending_task_tray_main_container").attr('style','display:block;');  
           
           $('html, body').animate({ scrollTop: ($("#task_pending_tray_main_container").offset().top - 10) }, "100000"); 
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_NEW')
        {
            updatePendingTrayListing(result_ary[1]);
           //$("#pending_task_listing_container").html(result_ary[1]);		
           $("#pending_task_listing_container").attr('style','display:block');
           
           if(mode=='COPY_PENDING_TASK')
            {
                $("#pending_task_list_open_close_link").removeClass('close-icon').addClass('open-icon');
                $("#pending_task_list_open_close_link").attr("onclick","");
                $("#pending_task_list_open_close_link").unbind("click");
                $("#pending_task_list_open_close_link").click(function(){close_open_div("pending_task_main_list_container_div","pending_task_list_open_close_link",'open')});
                $("#pending_task_main_list_container_div").attr('style','display:none;');
                $("#pending_task_overview_open_close_link").removeClass('close-icon').addClass('open-icon');
                $("#pending_task_overview_open_close_link").attr("onclick","");
                $("#pending_task_overview_open_close_link").unbind("click");
                $("#pending_task_overview_open_close_link").click(function(){close_open_div("pending_task_overview_container","pending_task_overview_open_close_link",'open')});
                $("#pending_task_overview_container").attr('style','display:none;'); 
            } 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_TRAY')
        {
           //$("#pending_task_listing_container").html(result_ary[1]);		
           updatePendingTrayListing(result_ary[1]);
           $("#pending_task_listing_container").attr('style','display:block');
           
           $("#pending_task_overview_main_container").html(" ");
           $("#pending_task_tray_main_container").html(" ");
           
           $("#pending_task_overview_main_container").attr('style','display:none;'); 
           $("#pending_task_tray_main_container").attr('style','display:none;'); 
           
           $("#idLastClikedBookingFile").val("");
           $("#contactPopup").html(result_ary[2]);		
           $("#contactPopup").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
}

function taks_step_select(step)
{
    $(".task-tray-header").removeClass('active-th');
    $("#task_tray_header_id_"+step).addClass('active-th');
} 

function check_verified_email(newEmail,user_id,file_id,rfq_flag)
{
    var oldEmail = $("#szCustomerOldEmail").val();
    var isVerified = $("#iCustomerVerified").val();
     
    var iDisableButton = 2; 
    if(rfq_flag==1)
    {
        var formId = 'pending_task_tray_form'
        if(isFormInputElementEmpty(formId,'szEmail')) 
        {		 
           iDisableButton = 1;
        }
        else if(!isValidEmail($('form#' + formId + ' #szEmail' ).val())) 
        {    
           iDisableButton = 1;
        }
    }
        
    if(iDisableButton==1)
    {
        $("#verify_email_button").unbind("click");  
        $("#verify_email_button").attr('class','gray-btn');
    }
    else
    { 
        if(oldEmail==newEmail)
        {
            if(isVerified==1)
            {
                $("#confirmed_email_no").attr('style','display:none;');
                $("#confirmed_email_yes").attr('style','display:inline-block;');

                $("#confirmed_email_task_no").attr('style','display:none;');
                $("#confirmed_email_task_yes").attr('style','display:inline-block;');

                $("#verify_email_button").unbind("click");  
                $("#verify_email_button").attr('class','gray-btn');
            }
            else
            {
                $("#confirmed_email_yes").attr('style','display:none;');
                $("#confirmed_email_no").attr('style','display:inline-block;');

                $("#confirmed_email_task_yes").attr('style','display:none;');
                $("#confirmed_email_task_no").attr('style','display:inline-block;');

                $("#verify_email_button").unbind("click");
                $("#verify_email_button").click(function(){
                    if(user_id>0)
                    {
                        display_verify_popup(user_id,'VERIFY_USER_EMAIL_CONFIRM','PENDING_TASK',file_id); 
                    }
                    else if(rfq_flag==1)
                    {
                        createAndConfirmUser();
                    }
                    else
                    {
                        display_verify_popup(user_id,'VERIFY_USER_EMAIL_CONFIRM','PENDING_TASK',file_id); 
                    }
                });
                $("#verify_email_button").attr('class','button1');
            }
        }
        else
        {
            $("#confirmed_email_yes").attr('style','display:none;');
            $("#confirmed_email_no").attr('style','display:inline-block;');

            $("#confirmed_email_task_yes").attr('style','display:none;');
            $("#confirmed_email_task_no").attr('style','display:inline-block;');

            $("#verify_email_button").unbind("click");
            $("#verify_email_button").click(function(){ 
                if(user_id>0)
                {
                    display_verify_popup(user_id,'VERIFY_USER_EMAIL_CONFIRM','PENDING_TASK',file_id); 
                }
                else if(rfq_flag==1)
                {
                    createAndConfirmUser();
                }
                else
                {
                    display_verify_popup(user_id,'VERIFY_USER_EMAIL_CONFIRM','PENDING_TASK',file_id); 
                } 
            });
            $("#verify_email_button").attr('class','button1');
        }
    }
}

function createAndConfirmUser()
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_bookingQuotes.php?mode=VERIFY_USER_EMAIL_CONFIRM",$("#pending_task_tray_form").serialize(),function(result){
        
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {  
           $("#verify_email_button").unbind("click");  
           $("#verify_email_button").attr('class','gray-btn'); 
           
           $("#confirmed_email_task_no").attr('style','display:none;');
           $("#confirmed_email_task_yes").attr('style','display:inline-block;');
           var idUser = result_ary[1];
           var szEmail = $("#szEmail").val();
           $("#szCustomerOldEmail").val(szEmail);
           $("#idUser").val(idUser);
        }
        $("#loader").attr('style','display:none');
    });
}
function activate_colli_field(idTransportMode)
{
    if(idTransportMode==4) //Courier
    {
        $("#iNumColli").removeAttr('readonly'); 
        $("#iNumColli").removeClass("disabled-input");
        var formId = 'pending_task_tray_form';
        var inputId = 'iNumColli'; 
        validate_numeric_fields(formId,inputId);
    }
    else
    {
        $("#iNumColli").removeAttr('readonly');
        $("#iNumColli").attr('readonly','readonly'); 
        $("#iNumColli").removeClass("red_border");
        $("#iNumColli").addClass("disabled-input");
    }
}

function validate_numeric_fields(formId,inputId)
{
    if(isFormInputElementEmpty(formId,inputId)) 
    {		
        $("#"+inputId).addClass('red_border');
    }
    else 
    {
        var iNumColli = parseInt($("#"+inputId).val());
        if(iNumColli>0)
        {
            //do nothing
        }
        else
        {
            $("#"+inputId).addClass('red_border');
        }
    }
} 
function check_postcode_field(formId,inputId)
{
    var idTransportMode = $("#idTransportMode").val();  
    if(idTransportMode==3) //Road
    {
        //check_form_field_empty_standard(formId,inputId); 
    }
}

function check_insurance_required(formId,inputId)
{ 
    var iInsuranceCur = $("#iInsuranceIncluded").val();
    if(iInsuranceCur==1 || iInsuranceCur==3)
    {
        if(inputId=='fTotalInsuranceCostForBookingCustomerCurrency')
        {
            var fInsuranceVal = $("#fTotalInsuranceCostForBookingCustomerCurrency").val();
            fInsuranceVal = parseFloat(fInsuranceVal);
            if(fInsuranceVal>=0)
            {
                $("#"+inputId).removeClass('red_border');
            }
            else
            {
                $("#"+inputId).addClass('red_border');
            }
        }
        else
        {
            if(isFormInputElementEmpty(formId,inputId)) 
            {		
                $("#"+inputId).addClass('red_border');
            } 
        } 
    } 
}
function enableAskforquoteButton(formId)
{
    var error = 1;
    var estimate_error = 1;
     
    var iAlreadyPaidBooking = $("#iAlreadyPaidBooking").val(); 
    if(isFormInputElementEmpty(formId,'idTransportMode')) 
    { 
        error++;
        estimate_error = estimate_error + 1; 
        $("#idTransportMode").addClass('red_border');
    }
    else 
    {
        var idTransportMode = $("#idTransportMode").val(); 
        if(idTransportMode==3)
        {
            if(isFormInputElementEmpty(formId,'szOriginPostcode')) 
            {		
                //console.log('Origin postcode');
                //error++;
            }
            if(isFormInputElementEmpty(formId,'szDestinationPostcode')) 
            {		
               // console.log('Destination postcode');
                //error++;
            }
        }
        else if(idTransportMode==4)
        {
            if(isFormInputElementEmpty(formId,'iNumColli')) 
            {		
                //console.log('Num colli');
                error++;
                estimate_error = estimate_error + 1; 
                $("#iNumColli").addClass('red_border');
            }
            else
            {
                var iNumColli = parseFloat($("#iNumColli").val());
                if(iNumColli>0)
                {
                    //its fine
                }
                else
                {
                    error++;
                    estimate_error = estimate_error + 1; 
                    $("#iNumColli").addClass('red_border');
                }
            }
        }
    }
    
    if(isFormInputElementEmpty(formId,'szFirstName')) 
    {		
        //console.log('First Name');
        error++;
        estimate_error = estimate_error + 1;  
        $("#szFirstName").addClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'szLastName')) 
    {		
        //console.log('Last Name');
        error++;
        estimate_error = estimate_error + 1;  
        $("#szLastName").addClass('red_border');
    } 
    if(isFormInputElementEmpty(formId,'idCustomerDialCode')) 
    {		
        //console.log('dial code');
        error++;
        estimate_error = estimate_error + 1;  
        $("#idCustomerDialCode").addClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'szCustomerPhoneNumber')) 
    {		
        //console.log('Phone');
        error++;
        estimate_error = estimate_error + 1;  
        $("#szCustomerPhoneNumber").addClass('red_border');
    } 
    else
    {
        var phone_number  = $("#szCustomerPhoneNumber").val();
        phone_number = parseInt(phone_number);
        if(phone_number<=0) 
        {		
            //console.log('Phone');
            error++;
            estimate_error = estimate_error + 1;  
            $("#szCustomerPhoneNumber").addClass('red_border');
        } 
    }
    if(isFormInputElementEmpty(formId,'szEmail')) 
    {		
        //console.log('Email');
        error++;
        estimate_error = estimate_error + 1;  
        $("#szEmail").addClass('red_border');
    }
    if($("input:radio[name='updatedTaskAry[iShipperConsignee]']").is(":checked")) 
    {
      //console.log("Role selcted"); 
      $("#customer_role_radio_container").removeClass('red_border');
    }
    else
    {
        //console.log("Role not selcted");
        error++;
        estimate_error = estimate_error + 1;  
        $("#customer_role_radio_container").addClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'szCustomerCity')) 
    {		
        //console.log('Customer City');
        error++;
        estimate_error = estimate_error + 1;  
        $("#szCustomerCity").addClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'szCustomerCountry')) 
    {		
        //console.log('Customer Country');
        error++;
        estimate_error = estimate_error + 1;  
        $("#szCustomerCountry").addClass('red_border');
    } 
    
    var cb_flag = $("#iPrivateShipping").prop('checked'); 
    if(cb_flag)
    {
        $("#szCustomerCompanyName").removeClass('red_border');
    }
    else
    {
        if(isFormInputElementEmpty(formId,'szCustomerCompanyName')) 
        {		
            //console.log('Customer Company');
            error++;
            estimate_error = estimate_error + 1;  
            $("#szCustomerCompanyName").addClass('red_border');
        }  
    }
    
    if(isFormInputElementEmpty(formId,'iAcceptNewsUpdate')) 
    {		
        //console.log('iAcceptNewsUpdate');
        error++;
        $("#iAcceptNewsUpdate").addClass('red_border');
    } 
    if(isFormInputElementEmpty(formId,'idCustomerOwner')) 
    {		
        //console.log('idCustomerOwner');
        //error++;
        //$("#idCustomerOwner").addClass('red_border');
    } 
    if(isFormInputElementEmpty(formId,'idFileOwner')) 
    {		
        //console.log('idFileOwner');
        error++;
        $("#idFileOwner").addClass('red_border');
    }
    else
    {
        $("#idFileOwner").removeClass('red_border');
    }
//    var isMoving = $("#isMoving").prop('checked');   
//    if(isMoving)
//    {
//        //If moving is checked then cargo description is not mandatory
//        $("#szCargoDescription").removeClass('red_border');
//    }
//    else
//    {
//        if(isFormInputElementEmpty(formId,'szCargoDescription')) 
//        {		
//            console.log('szCargoDescription');
//            error++;
//            $("#szCargoDescription").addClass('red_border');
//        } 
//    } 
     
     var iHandover_flag = 1;
    
    if(isFormInputElementEmpty(formId,'szHandoverCity')) 
    {		 
        iHandover_flag++;  
    }   
    
    if(isFormInputElementEmpty(formId,'szHandoverCityHidden') && iHandover_flag!=1) 
    { 
        iHandover_flag++;
    }
    else
    {
         iHandover_flag = 1;
    } 
    if(iHandover_flag==1)
    { 
        $("#szHandoverCity").removeClass('red_border');
    }
    else
    {
        error++;
        estimate_error = estimate_error + 1; 
        $("#szHandoverCity").addClass('red_border');
    }
     
    if(isFormInputElementEmpty(formId,'idServiceTerms')) 
    {		
        //console.log('Service terms');
        error++;
        $("#idServiceTerms").addClass('red_border');
    } 
    if(isFormInputElementEmpty(formId,'szOriginCity')) 
    {		
        //console.log('Origin city');
        error++;
        $("#szOriginCity").addClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'idOriginCountry')) 
    {		
        error++;
        estimate_error = estimate_error + 1; 
        $("#idOriginCountry").addClass('red_border');
       // console.log('Origin country');
    }
    if(isFormInputElementEmpty(formId,'szDestinationCity')) 
    {		
        error++;
        //console.log('dest city');
        $("#szDestinationCity").addClass('red_border');
    } 
    if(isFormInputElementEmpty(formId,'idDestinationCountry')) 
    {		
        error++;
        estimate_error = estimate_error + 1; 
        $("#idDestinationCountry").addClass('red_border');
        //console.log('dest country');
    }
    if(isFormInputElementEmpty(formId,'fCargoVolume')) 
    {		
        error++;
        estimate_error = estimate_error + 1; 
        $("#fCargoVolume").addClass('red_border');
    }
    else
    {
        var fCargoVolume = $("#fCargoVolume").val();
        fCargoVolume = parseFloat(fCargoVolume);
        if(fCargoVolume>0)
        {
            //console.log('Volume is OK');
        }
        else
        {
            error++;
            estimate_error = estimate_error + 1; 
            $("#fCargoVolume").addClass('red_border');
        }
    }
    if(isFormInputElementEmpty(formId,'fCargoWeight')) 
    {		
        error++;
        estimate_error = estimate_error + 1; 
        $("#fCargoWeight").addClass('red_border');
       // console.log('weight');
    }
    else
    {
        var fCargoWeight = $("#fCargoWeight").val();
        fCargoWeight = parseFloat(fCargoWeight);
        if(fCargoWeight>0)
        {
           // console.log('weight is OK');
        }
        else
        {
            error++;
            estimate_error = estimate_error + 1; 
            $("#fCargoWeight").addClass('red_border');
        }
    }
    if(isFormInputElementEmpty(formId,'dtTimingDate')) 
    {		
        error++;
       // console.log('ship date');
        $("#dtTimingDate").addClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'iBookingLanguage')) 
    {		
        error++;
        estimate_error = estimate_error + 1; 
        $("#iBookingLanguage").addClass('red_border');
        //console.log('ship date');
    } 
    
    if(isFormInputElementEmpty(formId,'iInsuranceIncluded')) 
    {		
        error++;
       // console.log('insurance curre');
        $("#iInsuranceIncluded").addClass('red_border');
    }
    else
    {
        $("#iInsuranceIncluded").removeClass('red_border');
        var iInsuranceIncluded = $("#iInsuranceIncluded").val(); 
       // console.log("Included: "+iInsuranceIncluded);
        
        if(iInsuranceIncluded==1 || iInsuranceIncluded==3) //1. Yes 3. Optional
        {
            if(isFormInputElementEmpty(formId,'idInsuranceCurrency')) 
            {		
                error++;
                $("#idInsuranceCurrency").addClass('red_border');
                console.log('insurance curre');
            } 
            if(isFormInputElementEmpty(formId,'fTotalInsuranceCostForBookingCustomerCurrency')) 
            {		
                error++;
                $("#fTotalInsuranceCostForBookingCustomerCurrency").addClass('red_border');
                console.log('insurance amount');
            }
        }
    } 
    var iAlreadyPaidBooking = $("#iAlreadyPaidBooking").val();
    
    if(error==1)
    {
        if(iAlreadyPaidBooking==1)
        {
            $("#validate_button_ask_for_quote").attr("class","button2");
        }
        else
        {
            $("#validate_button_ask_for_quote").unbind("click");
            $("#validate_button_ask_for_quote").click(function(){
                submit_pending_task_form('ASK_FOR_QUOTE');
            });
            $("#validate_button_ask_for_quote").attr('style','opacity:1');
        }
    }
    else
    {
        $("#validate_button_ask_for_quote").unbind("click");
        $("#validate_button_ask_for_quote").attr('style','opacity:0.4');
    } 
    
    if(estimate_error==1)
    {
        if(iAlreadyPaidBooking==1)
        {
            $("#validate_button_ask_for_quote").attr("class","button2");
        }
        else
        {
            $("#validate_button_estimate").unbind("click");
            $("#validate_button_estimate").click(function(){
                submit_pending_task_form('ESTIMATE');
            });
            $("#validate_button_estimate").attr('style','opacity:1');
        }
    }
    else
    {
        if(iAlreadyPaidBooking==1)
        {
            $("#validate_button_ask_for_quote").attr("class","button2");
        }
        else
        {
            $("#validate_button_estimate").unbind("click");
            $("#validate_button_estimate").attr('style','opacity:0.4');
        }
    }
}

function toggleInsuranceFields(iInsuranceIncluded,quick_quote)
{  
    if(iInsuranceIncluded==1 || iInsuranceIncluded==3) //1. Yes 3. Optional
    {
        if(quick_quote==1)
        {
            $("#idGoodsInsuranceCurrency").removeAttr('disabled');
            $("#fCargoValue").removeAttr('disabled');
        }
        else
        {
            $("#idInsuranceCurrency").removeAttr('disabled');
            $("#fTotalInsuranceCostForBookingCustomerCurrency").removeAttr('disabled');
        } 
    }
    else
    { 
        if(quick_quote==1)
        {
            $("#idGoodsInsuranceCurrency").attr('disabled','disabled');
            $("#fCargoValue").attr('disabled','disabled');
       
            $("#idGoodsInsuranceCurrency").removeClass('red_border');
            $("#fCargoValue").removeClass('red_border');
        }
        else
        {
            $("#idInsuranceCurrency").removeClass('red_border');
            $("#fTotalInsuranceCostForBookingCustomerCurrency").removeClass('red_border');
        }
    }
}
function display_task_menu_details(type,file_id,mode,template_id,email_id)
{ 
    if(mode=='DISPLAY_TASK_QUOTE_MANUAL_EDIT_FORM')
    {
        mode = 'DISPLAY_TASK_QUOTE_MANUAL_EDIT_FORM';
    }
    else if(mode=='RESET_TASK_QUOTE_PANE')
    {
        mode = 'RESET_TASK_QUOTE_PANE';
    }
    else if(mode=='CONFIRM_RESET_TASK_QUOTE_PANE')
    {
        mode = 'CONFIRM_RESET_TASK_QUOTE_PANE';
    }
    else
    {
        mode = 'DISPLAY_TASK_FORM_MENU';
    }
    
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,type:type,file_id:file_id,template_id:template_id,email_id:email_id},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#contactPopup").attr('style','display:none;');
           $(".task-tray-header").removeClass('active-th');
           $("#task_tray_header_id_"+type).addClass('active-th');
           //result_ary[1] = "Hello welcome...";
           $("#pending_task_tray_container").html(result_ary[1]); 
           if(email_id!='')
           {
               $("#szReminderSubject").focus();
               $("#szReminderSubject").focus().selectionEnd; 
               $('html, body').animate({ scrollTop: ($("#pending_task_tray_container").offset().top - 20) }, "100000");  
           }
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
}

function submit_pending_task_form(type)
{ 
    $("#loader").attr('style','display:block');
    var serialized_form_data = $("#pending_task_tray_form").serialize();
    
    if(type=='SEND_REQUEST_QUOTES' || type=='SAVE_REQUEST_QUOTES')
    {
        var content = NiceEditorInstance.instanceById('szEmailBody').getContent();
        var description = encode_string_msg_str(content);
        serialized_form_data = serialized_form_data + "&askForQuoteAry[szEmailBody]="+description; 
    } 
    else if(type=='SAVE_BOOKING_QUOTES' || type=='PREVIEW_REQUEST_QUOTES' || type=='SEND_CUSTOMER_QUOTES')
    {
        var content = NiceEditorInstance.instanceById('szEmailBody').getContent();
        var description = encode_string_msg_str(content);
        serialized_form_data = serialized_form_data + "&quotePricingAry[szEmailBody]="+description; 
    }
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?type="+type,serialized_form_data,function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#pending_task_tray_container").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            updatePendingTrayListing(result_ary[1]);
           //$("#pending_task_listing_container").html(result_ary[1]); 
           $("#pending_task_overview_main_container").html(result_ary[2]); 
           $("#pending_task_overview_main_container").attr('style','display:block;'); 
            
           $("#pending_task_tray_container").html(result_ary[3]);
           $("#pending_task_tray_container").attr('style','display:block;');   
           $("#pending_tray_task_heading_container").html(result_ary[4]); 
           
           
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_QUOTE')
        { 	
           $("#pending_task_tray_container").html(result_ary[1]); 
           if(result_ary[2]!='' && result_ary[2]!=undefined)
           {
                $("#file_status_text").html(result_ary[2]); 
            }
            if(result_ary[3]!='' && result_ary[3]!=undefined)
           {
               updatePendingTrayListing(result_ary[3]);
               // $("#pending_task_listing_container").html(result_ary[3]);
                $("#pending_task_listing_container").attr('style','display:block;');
            }
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_PREVIEW')
        { 	
           $("#quote_email_previewer_div").html(result_ary[1]); 
           $("#quote_email_previewer_div").attr('style','display:block;');  
        } 
        
        if(type=='ASK_FOR_QUOTE')
        {
            taks_step_select('ASK_FOR_QUOTE');
        }
        else if(type=='ESTIMATE')
        {
            taks_step_select('ESTIMATE');
        }
        else if(type=='ESTIMATE')
        {
            taks_step_select('ESTIMATE');
        }
        else if(type=='SAVE_REQUEST_QUOTES')
        {
            taks_step_select('ASK_FOR_QUOTE');
        }
        else if(type=='SAVE_BOOKING_QUOTES')
        {
            taks_step_select('QUOTE');
        }
        else if(type=='SEND_REQUEST_QUOTES')
        {
            taks_step_select('ASK_FOR_QUOTE');
        }
        else if(type=='SEND_CUSTOMER_QUOTES')
        {
            taks_step_select('QUOTE');
        }
        else if(type=='PREVIEW_REQUEST_QUOTES')
        {
            taks_step_select('QUOTE');
        }
        else
        {
            taks_step_select('VALIDATE');
        }

        $("#loader").attr('style','display:none');
    });
}

function toggleQuoteFields(number)
{
    var cb_flag = $("#iActive_"+number).prop('checked'); 
    if(cb_flag)
    {
        $("#idTransportMode_"+number).removeAttr('disabled');
        $("#idForwarder_"+number).removeAttr('disabled');
        $("#szForwarderContact_"+number).removeAttr('disabled');
        
        $("#idTransportMode_"+number).removeClass('red_border');
        $("#idForwarder_"+number).removeClass('red_border');
        $("#szForwarderContact_"+number).removeClass('red_border'); 
    }
    else
    { 
        $("#idTransportMode_"+number).attr('disabled','disabled');
        $("#idForwarder_"+number).attr('disabled','disabled');
        $("#szForwarderContact_"+number).attr('disabled','disabled');
        
        $("#idTransportMode_"+number).removeClass('red_border');
        $("#idForwarder_"+number).removeClass('red_border');
        $("#szForwarderContact_"+number).removeClass('red_border');
    }
}

function add_more_booking_quote(mode,file_id)
{  
    var number = document.getElementById('hiddenPosition').value; 
    if(mode=='ADD_MORE_BOOKING_QUOTES_PRICING')
    {
        var table_id = document.getElementById('horizontal-scrolling-div-id');
    }
    else
    {
        var table_id = document.getElementById('horizontal-scrolling-div-id');
        var hiddenPosition2 = document.getElementById('hiddenPosition2').value; 
        document.getElementById('hiddenPosition2').value =  parseInt(hiddenPosition2)+1; 
        var newHiddenPos = parseInt(hiddenPosition2)+1; 
        var iMaxQuoteAllowedPerFile = $("#iMaxNumQuotePerfile").val();
        console.log("Hidden pos: "+newHiddenPos+" allowd "+iMaxQuoteAllowedPerFile);
        if(newHiddenPos>=iMaxQuoteAllowedPerFile)
        {
            $("#request_quote_add_more_button_container").attr('style','display:none;');
        }
    } 
    
    number=parseInt(number); 
    var hidden1 = document.getElementById('hiddenPosition1').value; 
    document.getElementById('hiddenPosition1').value=parseInt(hidden1)+1; 
    document.getElementById('hiddenPosition').value =  parseInt(number)+1; 
        
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{number:number,mode:mode,file_id:file_id},function(result){ 
        $("#add_booking_quote_container_"+number).html(result);
        
        if(mode=='ADD_MORE_BOOKING_QUOTES_PRICING')
        {
            $("#add_booking_quote_container_"+number).attr("style",'display:  block; ');   
            $("#add_booking_quote_container_"+number).addClass('quote-pricing-fields');
        }
        else
        { 
            $("#add_booking_quote_container_"+number).attr("style",'display: block;');  
            $("#add_booking_quote_container_"+number).addClass('request-quote-fields'); 
        } 
    }); 
    
    var newTdId = 'add_booking_quote_container_'+(number+1) ;
    if($("#"+newTdId).length)
    {
        console.log("td already exists");
    }
    else
    {
        var newTd=document.createElement('div'); 
        newTd.setAttribute('id', 'add_booking_quote_container_'+(number+1)); 
        newTd.setAttribute('style', 'display:none;');
        table_id.appendChild(newTd); 
    }
}

function remove_booking_quote(div_id,mode,row_counter,booking_quote_id)
{    
    if(booking_quote_id>0)
    {
        $("#loader").attr('style','display:block');
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'DELETE_REQUESTED_QUOTE',booking_quote_id:booking_quote_id},function(result){ 

            var result_ary = result.split("||||"); 
            if(jQuery.trim(result_ary[0])=='SUCCESS')
            {
                removeQuoteFromRequestPane(div_id,mode,row_counter);
            }
            $("#loader").attr('style','display:none');
        });
    } 
    else
    {
        removeQuoteFromRequestPane(div_id,mode,row_counter);
    }
}

function removeQuoteFromRequestPane(div_id,mode,row_counter)
{
    var active_counter=$('.pendingTaskRequestQuote').length ;   
    if(active_counter>1)
    {
        var number = $('#hiddenPosition_ref').attr('value'); 
        var hidden1= $('#hiddenPosition1_ref').attr('value');
        var hidden_value = parseInt(hidden1)-1;

        $('#hiddenPosition_ref').attr('value',hidden_value);
        $('#hiddenPosition1_ref').attr('value',hidden_value);

        if(mode == 'REMOVE_BOOKING_QUOTES')
        { 
            var hiddenPosition2 = document.getElementById('hiddenPosition2').value;
            document.getElementById('hiddenPosition2').value = parseInt(hiddenPosition2)-1;

            var newHiddenPos = parseInt(hiddenPosition2)-1; 
            var iMaxQuoteAllowedPerFile = $("#iMaxNumQuotePerfile").val(); 
            if(newHiddenPos<=iMaxQuoteAllowedPerFile)
            {
                $("#request_quote_add_more_button_container").attr('style','display:block;');
            }
        } 

        $("#"+div_id).attr("style",'display:none;');
        $("#"+div_id).html(" ");
    } 
    else if(active_counter==1)
    { 
        $("#idTransportMode_"+row_counter).val("");
        $("#idForwarder_"+row_counter).val("");
        $("#szForwarderContact_"+row_counter).val("");
    }
}
function format_reminder_time(reminder_time,remind_pane,email_id)
{  
    var time_string = '';
    var left_string = '';
    var right_string = '';
    var left_done = 0;
    var left_done_counter = '';
    var counter = 0;
    var first_digit = reminder_time[0]; 
    if(first_digit==0)
    {
        reminder_time = reminder_time.substr(1);
    } 
    for (var i = 0, len = reminder_time.length; i < len; i++) 
    {
        counter = i+1 ;
        var char_1 = parseInt(reminder_time[i]); 

        if(isNaN(char_1))
        {
            //Invalid character
            console.log("Not a number: "+ char_1);
        }
        else
        {
            if((counter==1 || counter==2) && left_done!=1)
            {
                if(char_1 >2 && counter==1)
                {
                    //left_string += "0"+char_1  ;
                    left_string += char_1  ;
                    left_done = 1;
                    left_done_counter = counter;
                }
                else
                { 
                    left_string = left_string +""+char_1;
                } 
                if(counter==2)
                {
                    left_done = 1;
                    left_done_counter = counter;
                } 
            }
            else
            {
                if((left_done_counter+1)==counter && (char_1>=6))
                {
                    right_string += "0"+char_1  ; 
                }
                else
                {
                    right_string = right_string +""+ char_1; 
                } 
            } 
        }
    }

    if(right_string.length===1)
    {
        right_string = right_string +"0";
    }
    else if(right_string.length>2)
    {
        right_string = right_string.substring(0,2);
    }

    var left_check = parseInt(left_string);
    var right_check = parseInt(right_string);

    //date should be max 23:59 

    if(left_check>=24)
    {
        alert("Invalid date format");
        return false;
    } 
    if(right_check>=60)
    {
        alert("Invalid date format");
        return false;
    } 
    time_string = left_string + ":"+right_string ;  
    
    if(remind_pane==1)
    {
        $("#szEmailReminderTime").val(time_string);
    }
    else if(remind_pane==2)
    {
        $("#dtTaskTime").val(time_string);
    }
    else if(remind_pane==3)
    {
        $("#szCrmReminderTime_"+email_id).val(time_string);
    }
    else if(remind_pane==4)
    {
        $("#szEmailReminderTime_"+email_id).val(time_string);
    }
    else
    {
        $("#szReminderTime").val(time_string);
    } 
}

function display_pending_task_list_byteam()
{   
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",$("#transporteca_team_form").serialize(),function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#pending_task_tray_container").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            $("#iStatusChanged").val('2');
            updatePendingTrayListing(result_ary[1]);
            //$("#pending_task_listing_container").html(result_ary[1]);	
            $("#pending_task_listing_container").attr('style','display:block;');  

            $("#pending_task_overview_main_container").html(""); 
            $("#pending_task_overview_main_container").attr('style','display:none;');  

            $("#pending_task_tray_main_container").html(""); 
            $("#pending_task_tray_main_container").attr('style','display:none;');  
            $("#idLastClikedBookingFile").val("");
        } 
        $("#loader").attr('style','display:none');
    });
}

function updatePendingTrayListing(szJsonResponseString)
{ 
    var scope = angular.element(document.getElementById('pending_task_listing_container')).scope();
    scope.$apply(function(){
        var jSonobj = $.parseJSON(szJsonResponseString);
        scope.pendingTasks = jSonobj; 
        
        if(jSonobj.length)
        { 
            for (var key in jSonobj) 
            {   
                if (jSonobj.hasOwnProperty(key)) 
                {
                    var pendingTaskAry = jSonobj[key];
                    var idBookingFile = pendingTaskAry['idBookingFile'];
                    var idSelectedBookingFileId = pendingTaskAry['idSelectedBookingFileId'];
                    var bOnlyHighlight = pendingTaskAry['bOnlyHighlight']; 
                    var szCopyMode = pendingTaskAry['szCopyMode']; 
                   
                    idBookingFile = parseInt(idBookingFile);
                    bOnlyHighlight = parseInt(bOnlyHighlight);
                    idSelectedBookingFileId = parseInt(idSelectedBookingFileId);
                    //console.log("Selected File: "+idSelectedBookingFileId+ " File ID: "+idBookingFile+" hl: "+bOnlyHighlight);
                    if(idSelectedBookingFileId>0 && bOnlyHighlight==0 && idBookingFile==idSelectedBookingFileId)
                    {
                        $("#loader").attr('style','display:block');
                        var tr_id = pendingTaskAry['szTrId']; 
                        display_pending_task_details(tr_id,idBookingFile,'','','','',szCopyMode);
                        //console.log("Selected File: "+idSelectedBookingFileId);
                        break;
                    } 
                }
            }
        }
    });
}

function toggleTeamPopup(id)
{ 
    var disp = $("#"+id).css('display');
    if(disp=='block')
    {
        $("#"+id).css('display','none');   
    }
    else
    {
        $("#"+id).css('display','block');   
    }
}

function display_recent_files_by_owner(mode,iToggleType,opration_type)
{
    $("#loader").attr('style','display:block');
    $("#contactPopup").html(" ");
    $("#contactPopup").attr('style','display:none;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,iToggleType:iToggleType,opration_type:opration_type},function(result){
	
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block'); 
           $('body').addClass('popup-body-scroll');
           $("#contactPopup").focus();
           var openDiv = 'task-popup-id-recent'; 
           $(document).click(function(e) {
                if (!$(e.target).closest('#'+openDiv).length) {
                    
                    var disp = $("#ui-datepicker-div").css('display');
                    if(disp=='block')
                    {
                        console.log("Date picker is active");
                    }
                    else
                    {
                        $("#contactPopup").attr('style','display:none;'); 
                        $(document).unbind('click');
                    } 
                    $('body').removeClass('popup-body-scroll'); 
                }
            }); 
            $("#szFreeText").focus();
            $("#szFreeText").focus().selectionEnd; 
        } 
        $("#loader").attr('style','display:none');
    });
}

function togglePendingTrayPopup(divID) 
{
    var openDiv
    $("#"+divID).fadeToggle(200, function() {
        openDiv = $(this).is(':visible') ? divID : null;
    });
    return openDiv ;
}

function activate_recent_task_details(div_id,idBookingFile,form_page,iBookingFlag,iSearchForm,szFileReference)
{ 
    var rowCount = $('#pending_task_tray_popup tr').length;
    console.log("Row count: "+rowCount+" div_id "+div_id+" file: "+idBookingFile+" From page: "+form_page);
    
    if(iSearchForm && cntrlIsPressed)
    {
        cntrlIsPressed=false;
        $("#szFreeFieldHeader").attr('value',szFileReference);
        $('#headerSearchForm').attr('target', "_blank");
        $('#headerSearchForm').attr('action', __JS_ONLY_SITE_BASE__+'/pendingTray/');
        $("#headerSearchForm").submit();
    }
    else
    {
        if(parseInt(rowCount)>0)
        {
            for(var t=1;t<=rowCount;++t)
            {
                var newdivid="recent_task_popup_"+t; 
                if(newdivid!=div_id)
                {
                    $("#"+newdivid).attr('style','background:#ffffff;cursor:pointer;');
                }
                else
                {
                    $("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;');
                }
            } 
        }

    //    $("#recent_task_delete_button").unbind("click");
    //    $("#recent_task_delete_button").removeAttr('style');  
    //    $("#recent_task_delete_button").click(function(){ display_pending_task_overview('DELETE_PENDING_TASK',idBookingFile,form_page); }); 

        $("#recent_task_open_button").unbind("click");
        $("#recent_task_open_button").removeAttr('style');  
        $("#recent_task_open_button").click(function(){ display_pending_task_overview('DISPLAY_PENDING_TASK_DETAILS',idBookingFile,form_page); close_open_div('pending_task_main_list_container_div','pending_task_list_open_close_link','close'); });

        $("#recent_task_quick_quote_button").unbind("click");
        $("#recent_task_quick_quote_button").removeAttr('style');  
        $("#recent_task_quick_quote_button").click(function(){ showCopyConfirmationPopUp('COPY_QUICK_QUOTE_BOOKING',idBookingFile,form_page); });
        //20161109 Small items in management
        //if(parseInt(iBookingFlag)==0)
        //{
            $("#recent_task_copy_button").unbind("click");
            $("#recent_task_copy_button").removeAttr('style');  
            $("#recent_task_copy_button").click(function(){ showCopyConfirmationPopUp('COPY_PENDING_TASK_POP_UP',idBookingFile,form_page); });  
        /*}
        else
        {
            $("#recent_task_copy_button").unbind("click");
            $("#recent_task_copy_button").attr('style','opacity:0.4;');   
        } */
    }
}

function search_pending_task(page)
{ 
    if(page==0)
    { 
        $("#pending_task_search_popup_button").addClass('tyni-loader');
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?page="+page,$("#pending_task_serch_form").serialize(),function(result){
	
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#pending_task_search_form_container").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#pending_task_searched_list_container").html(result_ary[1]);	 
        } 
        $("#pending_task_search_popup_button").removeClass('tyni-loader');
        $("#search_task_loader").attr('style','display:none');
    });
}

function undo_disqualification_task(file_id)
{
    $("#loader").attr('style','display:block');
    $("#contactPopup").html(" ");
    $("#contactPopup").attr('style','display:none;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode},function(result){
	
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block'); 
        } 
        $("#loader").attr('style','display:none');
    });
}

function cancelRegionOpr(id)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'CANCEL_REGION'},function(result){ 
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#region_list_container_div").html(result_ary[1]);
            $("#region_form_container_div").html(result_ary[2]);
        } 
    }); 
}

function addRegion()
{	
    $('tr').attr('style','background:#fff;color:#000;cursor:pointer;');
    $('.addExchangeOnFocus').removeAttr('onFocus');
    $('.addExchangeOnChange').removeAttr('onChange');
    $('#editExchangeRates').attr('onclick','saveRegionName();');	
    $('#deleteExchangeRates').attr('onclick','cancelRegionOpr();');
    $('#editExchangeRates span').text('add');
    $('#deleteExchangeRates span').text('cancel');
    $('#editExchangeRates').removeAttr('style');
    $('#deleteExchangeRates').removeAttr('style');		
} 

function saveRegionName()
{  
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$("#add_edit_region_form").serialize(),function(result){ 
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#region_form_container_div").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#region_form_container_div").html(result_ary[1]);	 
           $("#region_list_container_div").html(result_ary[2]);	 
        }  
    });
}

function displayEditRegionForm(idRegion)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'DISPLAY_EDIT_REGION_FORM',idRegion:idRegion},function(result){ 
        
        var result_ary = result.split('||||');
        
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            $("#region_form_container_div").html(result_ary[1]);
            
            $("#editExchangeRates").attr("style","opacity:1");
            $("#editExchangeRates").unbind("click");
            $("#editExchangeRates").html("<span>Save</span>");
            $("#editExchangeRates").click(function(){ saveRegionName(); });
        } 
    }); 
}
function select_region_tr(tr_id,id)
{
    var rowCount = $('#regionTable tr').length;
    $('#regionTable tr').attr('style','');
    $("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
     
    $("#deleteExchangeRates").attr("style","opacity:1");
    $("#deleteExchangeRates").unbind("click");
    $("#deleteExchangeRates").click(function(){ cancelRegionOpr(); });
    
    $("#editExchangeRates").attr("style","opacity:1");
    $("#editExchangeRates").unbind("click");
    $("#editExchangeRates").html("<span>edit</span>");
    $("#editExchangeRates").click(function(){ displayEditRegionForm(id); }); 
}  
function prefill_forwarder_contact(num,type)
{ 
    $("#request_quote_send_button").attr("style","opacity:0.4");
    $("#request_quote_send_button").unbind("click"); 
    
    var idForwarder = $("#idForwarder_"+num).val();
    var idTransportMode = $("#idTransportMode_"+num).val();
    var idBooking = $("#idBookingOverView").val();
    
    if(type=='CHANGED_MODE')
    {
        $("#idForwarder_"+num).attr('disabled','disabled');
    }
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'PREFILL_FORWARDER_CONTACT_EMAIL',type:type,forwarder_id:idForwarder,transport_mode_id:idTransportMode,booking_id:idBooking,index_num:num},function(result){
	
        var result_ary = result.split("||||");  
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            $("#szForwarderContact_"+num).val(result_ary[1]);
            var iOffLine = jQuery.trim(result_ary[2]);
            iOffLine = parseInt(iOffLine);
            if(iOffLine==1)
            {
                $("#iOffLine_"+num).attr("checked",true);
                $("#szEmailSubject").attr('value',result_ary[3]);
                NiceEditorInstance.instanceById('szEmailBody').setContent(result_ary[4]);
                $("#szEmailBody").attr('value',result_ary[4]);
                
                $("#idLanguage").val(result_ary[5]);
            }
            else
            {
                $("#iOffLine_"+num).attr("checked",false);
            }
            showHideSubjectBodyDiv();
        }  
        else if(jQuery.trim(result_ary[0])=='SUCCESS_MODE')
        { 	
            $("#forwarder_container_span_"+num).html(result_ary[1]);
        } 
        if(type=='CHANGED_MODE')
        {
            $("#idForwarder_"+num).removeAttr('disabled');
        }
        enableSendButton();
    });
} 

function enableSendButton()
{
    var error_count = 1 ;
    var formId = 'pending_task_tray_form' ;
    
    $("#idInsuranceCurrency").removeClass('red_border');
    var iAlreadyPaidBooking = $("#iAlreadyPaidBooking").val();
    //var active_counter=$('input.pendingTaskRequestQuote:checked').length ; 
    
    /*
     * 
     * As we have deleted Actuve checkbox from REQUEST pane so all the fields of REQUEST pane will always be enabled.
     */
    var active_counter = 1;
    if(active_counter>0)
    {    
        $("#szCommentToForwarder").removeAttr('disabled');
        $("#szEmailSubject").removeAttr('disabled');
        $("#szEmailBody").removeAttr('disabled');
        $("#dtReminderDate").removeAttr('disabled');
        $("#szReminderTime").removeAttr('disabled'); 
        
        $("#request_quote_save_button").attr("style","opacity:1");
        $("#request_quote_save_button").unbind("click");
        $("#request_quote_save_button").click(function(){ submit_pending_task_form('SAVE_REQUEST_QUOTES'); }); 
        /*
        $("#request_quote_preview_button").attr("style","opacity:1");
        $("#request_quote_preview_button").unbind("click");
        $("#request_quote_preview_button").click(function(){ submit_pending_task_form('PREVIEW_REQUEST_QUOTES'); }); 
        */
    }
    else
    {
        error_count++;
        $("#szCommentToForwarder").attr('disabled','disabled');
        $("#szEmailSubject").attr('disabled','disabled');
        $("#szEmailBody").attr('disabled','disabled'); 
        $("#dtReminderDate").attr('disabled','disabled');
        $("#szReminderTime").attr('disabled','disabled');
        
        $("#request_quote_save_button").attr("style","opacity:0.4");
        $("#request_quote_save_button").unbind("click"); 
        
        $("#request_quote_preview_button").attr("style","opacity:0.4");
        $("#request_quote_preview_button").unbind("click"); 
    }
    var existingRecordAry = new Array();
    var counter = 0;
    
    var showBodySubjectDivFlag=false;
    var i=1;
    $(".pendingTaskRequestQuote").each(function(element)
    {   
        var cb_id = this.id; 
        //var cb_flag = $("#"+cb_id).prop("checked");   
        //if(cb_flag)
        //{
            var cb_id_ary =  cb_id.split("_");
            var number = cb_id_ary[1];
            if(isFormInputElementEmpty(formId,'idTransportMode_'+number)) 
            {		
                //console.log('Transport mode number: '+number);
                error_count++;
            }
            if(isFormInputElementEmpty(formId,'idForwarder_'+number)) 
            {		
                //console.log('Forwarder empty number: '+number);
                error_count++;
            }
            if(isFormInputElementEmpty(formId,'szForwarderContact_'+number)) 
            {		
                //console.log('Forwarder contact number: '+number);
                error_count++;
            } 
            
            div_id ="iOffLine_"+i;
            if($("#"+div_id).is(':checked')==true)
            {
                showBodySubjectDivFlag=true;
            }
            ++i;

            var idForwarder = $("#idForwarder_"+number).val();
            var idTransportMode = $("#idTransportMode_"+number).val();
            var arrayString = idForwarder+""+idTransportMode ;
            console.log(counter+"arrayString"+arrayString);
            existingRecordAry[counter] = arrayString;  
            counter++;
        //}
    });
    
    if( arrHasDupes( existingRecordAry ) )   
    { 
	 error_count++;
         console.log("duplicate record ");
    }   
    
    if(showBodySubjectDivFlag)
    {
        if(isFormInputElementEmpty(formId,'szEmailSubject')) 
        {		
            console.log('Subject empty');
            error_count++;
        }
        if(isFormInputElementEmpty(formId,'szEmailBody')) 
        {		
            console.log('Email body');
            error_count++;
        }
    }
    if(error_count==1)
    {
        if(iAlreadyPaidBooking==1)
        {
            $("#request_quote_send_button").attr("class","button2");
        }
        else
        {
            $("#request_quote_send_button").attr("style","opacity:1");
            $("#request_quote_send_button").unbind("click");
            $("#request_quote_send_button").click(function(){ submit_pending_task_form('SEND_REQUEST_QUOTES'); });
        } 
    }
    else
    {
        if(iAlreadyPaidBooking==1)
        {
            $("#request_quote_send_button").attr("class","button2");
        }
        else
        {
            $("#request_quote_send_button").attr("style","opacity:0.4");
            $("#request_quote_send_button").unbind("click"); 
        }
    }
}
function arrHasDupes(arr) 
{     
    var i, j, n;
    n=arr.length;           
    for (i=0; i<n; i++) 
    {                
        for (j=i+1; j<n; j++) 
        {   
            if (arr[i]==arr[j]) return true;
        }	
    }
    return false;
}

function submit_reuse_popup_form()
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",$('#reuse_insurance_rate_form').serialize(),function(result){ 
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#insurance_rate_popup").html(result_ary[1]);		
           $("#insurance_rate_popup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='REDIRECT')
        {
            redirect_url(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_BUYRATE')
        {
            $("#insurance_buyer_list_container_1").html(result_ary[1]);
            
            $("#insurance_rate_popup").html(' ');		
            $("#insurance_rate_popup").attr('style','display:none;');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
           $("#insurance_rate_popup").html(result_ary[1]);		
           $("#insurance_rate_popup").attr('style','display:block');
        }
    });
}
function toggleTaskQuoteContainer(number)
{
    var cb_flag = $("#iActive_"+number).prop('checked'); 
    if(cb_flag)
    {
        $("#add_booking_quote_sub_container_"+number).attr('class','active-box'); 
        $(".active-box :input").attr("readonly", false);
        $(".active-box :input").removeAttr("readonly");
        $("#iTransitHours_"+number).unbind("focus");
        $("#fTotalPriceForwarderCurrency_"+number).unbind("focus");
        $("#fTotalVatForwarder_"+number).unbind("focus");
        $("#fReferalPercentage_"+number).unbind("focus");
        $("#fReferalAmount_"+number).unbind("focus");
        $("#fTotalPriceCustomerCurrency_"+number).unbind("focus");
        $("#fTotalVat_"+number).unbind("focus");
        $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).unbind("focus");
        $("#szForwarderComment_"+number).unbind("focus");
    }
    else
    {  
        $("#add_booking_quote_sub_container_"+number).attr('class','disabled-box'); 
        $(".disabled-box :input").attr("readonly", true);
        $(".quoteActiveCb").removeAttr('readonly');
        $("#idBookingQuotePricing_"+number).removeAttr('disabled');
        
        $(".disabled-box :input").focus(function() {
            this.blur();
            console.log("Focus got called");
        });
    }
}  

function enableQuoteSendButton()
{
    var error_count = 1 ;  
    var error_count = 1 ;
    var formId = 'pending_task_tray_form';
    
    var active_counter=$('input.quoteActiveCb:checked').length ;
    active_counter = parseInt(active_counter);
    var iAlreadyPaidBooking = $("#iAlreadyPaidBooking").val();
    
    if(active_counter>0)
    {
        $("#request_quote_save_button").attr("style","opacity:1");
        $("#request_quote_save_button").unbind("click");
        $("#request_quote_save_button").click(function(){ submit_pending_task_form('SAVE_BOOKING_QUOTES'); }); 
        
        $("#request_quote_preview_button").attr("style","opacity:1");
        $("#request_quote_preview_button").unbind("click");
        $("#request_quote_preview_button").click(function(){ submit_pending_task_form('PREVIEW_REQUEST_QUOTES'); }); 
    }
    else
    { 
        $("#request_quote_preview_button").attr("style","opacity:0.4");
        $("#request_quote_preview_button").unbind("click"); 
    }
    
    $(".quoteActiveCb").each(function(element)
    {   
        var cb_id = this.id; 
        var cb_flag = $("#"+cb_id).prop("checked");   
        if(cb_flag)
        {
            var cb_id_ary =  cb_id.split("_");
            var number = cb_id_ary[1];
            if(isFormInputElementEmpty(formId,'fReferalPercentage_'+number)) 
            {		
                //console.log('Referral percentage '+number);
                error_count++;
            }
            if(isFormInputElementEmpty(formId,'fReferalAmount_'+number)) 
            {		
               // console.log('Referral amount '+number);
                error_count++;
            }
            if(isFormInputElementEmpty(formId,'fTotalPriceCustomerCurrency_'+number)) 
            {		
              //  console.log('fTotalPriceCustomerCurrency: '+number);
                error_count++;
            } 
            if(isFormInputElementEmpty(formId,'fTotalVat_'+number)) 
            {		
                //console.log('fTotalVat: '+number);
                error_count++;
            } 
        }
    });
    
    if(isFormInputElementEmpty(formId,'szEmailSubject')) 
    {		
        //console.log('Email Subject');
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'szEmailBody')) 
    {		
        //console.log('Email Body');
        error_count++;
    }
    var iStandardPricing=$("#iStandardPricing").val();
    if(parseInt(iStandardPricing)==0 && isFormInputElementEmpty(formId,'szQuotationBody')) 
    {		
        //console.log('Quotation Body');
        error_count++;
    }
     
    if(error_count==1)
    {
        if(iAlreadyPaidBooking==1)
        {
            $("#request_quote_send_button").attr("class","button2");
        }
        else
        {
            $("#request_quote_send_button").attr("style","opacity:1");
            $("#request_quote_send_button").unbind("click");
            $("#request_quote_send_button").click(function(){ submit_pending_task_form('SEND_CUSTOMER_QUOTES'); }); 
        }
    }
    else
    {
        $("#request_quote_send_button").attr("style","opacity:0.4");
        $("#request_quote_send_button").unbind("click");
    }
}

function toggleCompanyField(cb_id,crm_email,email_id)
{
    var cb_flag = $("#"+cb_id).prop('checked'); 
    
    var szFirstName_id = "szFirstName";
    var szLastName_id = "szLastName";
    var szCustomerCompanyRegNo_id = "szCustomerCompanyRegNo";
    var szCustomerCompanyName_id = "szCustomerCompanyName";
    
    if(crm_email==1)
    {
        szFirstName_id = "szFirstName_"+email_id;
        szLastName_id = "szLastName_"+email_id;
        szCustomerCompanyRegNo_id = "szCustomerCompanyRegNo_"+email_id;
        szCustomerCompanyName_id = "szCustomerCompanyName_"+email_id;
    }  
    
    if(cb_flag)
    {
        var szFirstName = $("#"+szFirstName_id).val();
        var szLastName = $("#"+szLastName_id).val();
        var CustomerName = szFirstName+" "+szLastName; 
        $("#"+szCustomerCompanyRegNo_id).attr('disabled','disabled');
        $("#"+szCustomerCompanyName_id).val(CustomerName);
        
    }
    else
    {  
        $("#"+szCustomerCompanyName_id).removeAttr('disabled');
        $("#"+szCustomerCompanyRegNo_id).removeAttr('disabled'); 
    }
} 

function toggleCargoDescriptionField()
{
    var iCargoDescriptionLocked_flag = $("#iCargoDescriptionLocked").prop('checked'); 
    if(iCargoDescriptionLocked_flag)
    {
        $("#szCargoDescription").removeClass('red_border');
        $("#szCargoDescription").addClass('bg-grey');
        $("#szCargoDescription").attr('readonly','readonly');
    }
    else
    {
        $("#szCargoDescription").removeClass('bg-grey');
        $("#szCargoDescription").removeAttr('readonly');
    }
}

function toggleCargoDescriptionLockedField()
{
    var szCargoDescription = $("#szCargoDescription").val();
    if(szCargoDescription!="")
    {
        $("#iCargoDescriptionLocked").removeAttr('disabled');
    }
    else
    {
        $("#iCargoDescriptionLocked").attr('disabled','disabled');
    }
}

function autofill_admin_validate_pane(szSelectedStr,type,quick_quote,field_name)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'AUTOFILL_USER_DATA',type:type,selected_str:szSelectedStr,quick_quote:quick_quote,field_name:field_name},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {  
           $("#contactPopup").html(result_ary[1]); 
           $("#contactPopup").attr('style','display:none');
        } 
        $("#loader").attr('style','display:none');
    });
} 

function autofill_customer_data(result_ary,user_id,quick_quote,field_name)
{  
    $(".red_border").removeClass('red_border'); 
    var file_id = $("#idBookingFile").val();
    $("#szFirstName").val(result_ary[0]);
    $("#szLastName").val(result_ary[1]);
    $("#szCustomerCompanyName").val(result_ary[2]);
    $("#idCustomerDialCode").val(result_ary[3]);
    $("#szCustomerPhoneNumber").val(result_ary[4]); 
    $("#szEmail").val(result_ary[5]); 
    
    if(quick_quote==1)
    {
        //TO DO
    }
    else
    {
        $('#idCustomerCurrency option[value='+result_ary[6]+']').attr('selected','selected');
        var iConfirmed = result_ary[7]; 
        $("#szCustomerOldEmail").val(result_ary[5]);
        
        if(iConfirmed==1)
        { 
            $("#iCustomerVerified").val(1);
            $("#verify_email_button").unbind("click");  
            $("#verify_email_button").attr('class','gray-btn');
        }
        else
        {  
            $("#iCustomerVerified").val(0);
            $("#verify_email_button").unbind("click");
            $("#verify_email_button").click(function(){
                display_verify_popup(user_id,'VERIFY_USER_EMAIL_CONFIRM','PENDING_TASK',file_id); 
            });
            $("#verify_email_button").attr('class','button1');
        }  

        if(result_ary[8]==1)
        {
           $('#iAcceptNewsUpdate option[value=1]').attr('selected','selected'); 
        }
        else
        {
            $('#iAcceptNewsUpdate option[value=2]').attr('selected','selected');
        } 
        var iPrivate = result_ary[9] ;
        if(iPrivate==1)
        {
            $("#iPrivateShipping").prop('checked','checked');
        }
        else
        {
            $("#iPrivateShipping").removeProp('checked');
        }
        $('#idCustomerOwner option[value='+result_ary[10]+']').attr('selected','selected'); 
        $('#idFileOwner option[value='+result_ary[10]+']').attr('selected','selected'); 
        
        if(result_ary[12]>0)
        {
            $('#iBookingLanguage option[value='+result_ary[12]+' ]').attr('selected','selected'); 
        }
        else
        {
            if(result_ary[11]==61) //Denmark
            {
                //if country is denmark then language will be danish. 
                $('#iBookingLanguage option[value=2]').attr('selected','selected'); 
            }
            else
            {
                $('#iBookingLanguage option[value=1]').attr('selected','selected'); 
            }
        }
    }
    
    var szCustomerPostCode = result_ary[14];
    var szCustomerCity = result_ary[15];
    var idCustomerCountry = result_ary[16];
    
    $("#szCustomerAddress1").val(result_ary[13]); 
    $("#szCustomerPostCode").val(szCustomerPostCode); 
    $("#szCustomerCity").val(szCustomerCity); 
    $("#szCustomerCountry").val(idCustomerCountry); 
    $("#szCustomerCompanyRegNo").val(result_ary[17]); 
    
    if(quick_quote==1)
    {
        //var iShipperConsignee = $('input:radio[name="quickQuoteShipperConsigneeAry[iShipperConsignee]"]:checked').val(); 
        var idOriginCountry = $("#idOriginCountry").val();
        var idDestinationCountry = $("#idDestinationCountry").val();
        
        if(idCustomerCountry==idOriginCountry)
        {
            iShipperConsignee = 1;
        }
        else if(idCustomerCountry==idDestinationCountry)
        {
            iShipperConsignee = 2;
        }
        else
        {
            iShipperConsignee = 3;
        }
        $('input:radio[name="quickQuoteShipperConsigneeAry[iShipperConsignee]"]').removeProp('checked');
        $("#iShipperConsignee_"+iShipperConsignee).prop("checked",'true');
        //console.log("Cust country: "+idCustomerCountry+" Org: "+idOriginCountry+" Dest: "+idDestinationCountry+ " ship: "+iShipperConsignee);
    }
    else
    {
        var iShipperConsignee = $('input:radio[name="updatedTaskAry[iShipperConsignee]"]:checked').val();  
    }
    
    var szFormPostcode = '';
    var szFormCity = '';
    var idFormCountry = ''; 
    var check_ship_con = false;
    //alert("Shipcon: "+iShipperConsignee);
    if(iShipperConsignee==1)
    {
        szFormPostcode = $("#szOriginPostcode").val();
        szFormCity = $("#szOriginCity").val();
        idFormCountry = $("#idOriginCountry").val(); 
        check_ship_con = true;
    }
    else if(iShipperConsignee==2)
    {
        szFormPostcode = $("#szDestinationPostcode").val();
        szFormCity = $("#szDestinationCity").val();
        idFormCountry = $("#idDestinationCountry").val();
        check_ship_con = true;
    }

    /*
    *   i. If postcode is prefilled (so not empty), and the postcode of the user we select is different from what is prefilled, then change customer role to “Billing”, so we don’t overwrite shipper/consignee details from the search
    *   ii. As above, same for City
    *   iii. As above, same for Country 
    *   iv. If Postcode, City and Country is the same (or empty for user or empty in prefilled), then we do not change customer status
    */

    console.log("Code: "+szFormPostcode+" City: "+szFormCity+" Count: "+idFormCountry);
    var bChangeShipperConsignee = false;
    if(check_ship_con)
    {
        if(szFormPostcode!="" && szFormPostcode!=szCustomerPostCode)
        {
            //bChangeShipperConsignee = true;
        }
        if(szFormCity!="" && szFormCity!=szCustomerCity)
        {
            bChangeShipperConsignee = true;
        }
        if(idFormCountry!="" && idFormCountry!=idCustomerCountry)
        {
            bChangeShipperConsignee = true;
        }
    } 

    if(bChangeShipperConsignee)
    {  
        if(idCustomerCountry=="" && szCustomerPostCode=="" && szCustomerCity=="")
        {
            // IF customer details are empty then we don't change customer role.
        }
        else
        {
            if(quick_quote==1)
            {
                $('input:radio[name="quickQuoteShipperConsigneeAry[iShipperConsignee]"]:checked').removeAttr('checked');
            }
            else
            {
                $('input:radio[name="updatedTaskAry[iShipperConsignee]"]:checked').removeAttr('checked'); 
            }
            $("#iShipperConsignee_3").prop('checked',true);
            
            //If we change customer to billing then we kind of delete half filled data from shipper/consignee
            if(iShipperConsignee==1)
            {
                if(field_name=='szFirstName')
                {
                    $("#szShipperFirstName").val("");
                }
                else if(field_name=='szLastName')
                {
                    $("#szShipperLastName").val("");
                }
                else if(field_name=='szCustomerCompanyName')
                {
                    $("#szShipperCompanyName").val("");
                }
                else if(field_name=='szEmail')
                {
                    $("#szShipperEmail").val("");
                }
            }
            else if(iShipperConsignee==2)
            {
                if(field_name=='szFirstName')
                {
                    $("#szConsigneeFirstName").val("");
                }
                else if(field_name=='szLastName')
                {
                    $("#szConsigneeLastName").val("");
                }
                else if(field_name=='szCustomerCompanyName')
                {
                    $("#szConsigneeCompanyName").val("");
                }
                else if(field_name=='szEmail')
                {
                    $("#szConsigneeEmail").val("");
                }
            } 
        } 
    }
    
    if(quick_quote==1)
    {
        prefillCompanyField('szCustomerType');
        autofill_billing_address__new_rfq(1);
        $("#idUser").val(user_id); 
        var szBookingType = $("#szBookingType").val();
        var iLoad = 0;
        if(szBookingType=='SEND_QUOTE' || szBookingType=='CREATE_BOOKING')
        {
            iLoad = 1;
        }
        validateQQBookingInformation(iLoad);
    }
    else
    {
        check_verified_email(result_ary[5],user_id,file_id);
        toggleCompanyField('iPrivateShipping');
        autofill_billing_address__new_rfq();
        $("#idUser").val(user_id); 
        enableAskforquoteButton('pending_task_tray_form');
    } 
    
}
function setDefaultReminderDate(hours)
{ 
    var curre_date = new Date();   
    var iHours = hours;
    iHours = parseInt(iHours); 
    var total_minuts = iHours * 60 ;
    curre_date = addMinutes(curre_date,total_minuts);
     
    /*
    *  Rounding off minutes nearest to 30
    */ 
    var new_minutes = curre_date.getMinutes(); 
    new_minutes = parseInt(new_minutes);
    var curre_date_new = new Date();   
    console.log("date: "+curre_date+" now: "+curre_date_new);
    if(new_minutes<30)
    {
        var min_difference = 30 - new_minutes ; 
        curre_date_new = addMinutes(curre_date,min_difference);  
    }
    else if(new_minutes>30)
    { 
        if(new_minutes<45)
        {
            var min_difference = new_minutes - 30 ; 
            curre_date_new = addMinutes(curre_date,min_difference,'SUBSTRACT');  
        }
        else
        {
            var min_difference = 60 - new_minutes ; 
            curre_date_new = addMinutes(curre_date,min_difference);  
        } 
    }

    var iDays = curre_date_new.getDay();

    if(iDays==0) //Sunday
    {
        //If sunday then add 24 hour in this date
        var min_difference = 24*60 ;
        curre_date_new = addMinutes(curre_date_new,min_difference);
    }
    else if(iDays==6) //Saturday
    {
        //If sunday then add 48 hour in this date
        var min_difference = 48*60 ;
        curre_date_new = addMinutes(curre_date_new,min_difference);
    }

    var iYear = curre_date_new.getFullYear();
    var iMonth = curre_date_new.getMonth();
    var iDate = curre_date_new.getDate();
    var iHours = curre_date_new.getHours();
    var iMinutes = curre_date_new.getMinutes(); 

    iMonth = iMonth+1 ;
    if(iMonth<10)
    {
        iMonth = "0"+iMonth ;
    }
    if(iMinutes<10)
    {
        iMinutes = "0"+iMinutes;
    }
    var reminder_date = iDate + "/" + iMonth + "/" + iYear ;
    var reminder_time = iHours + ":" + iMinutes ; 


    var weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday"; 

    console.log("Date: "+ reminder_date + " Time: "+ reminder_time+ " dow: "+weekday[iDays]);
    $("#dtReminderDate").val(reminder_date);
    $("#szReminderTime").val(reminder_time);
}

function addMinutes(date, minutes,operation) 
{
    if(operation=='SUBSTRACT')
    {
        return new Date(date.getTime() - minutes*60000);
    }
    else
    {
        return new Date(date.getTime() + minutes*60000);
    } 
}   
function update_operation_statistics_script()
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_salesFunnel.php",{mode:'UPDATE_OPERATION_STATISTICS_SCREEN'},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {   
            redirect_url(result_ary[1]);
        } 
        $("#loader").attr('style','display:none');
    });
}  
function add_forwarder_contact_dropdown(forwarder_id)
{
    //$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'DISPLAY_FORWARDER_CONTACT_DROPDOWN',forwarder_id:forwarder_id},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {   
            $("#forwarder_contact_container_span").html(result_ary[1]);		
        } 
        $("#loader").attr('style','display:none');
    });
}

function deleteBookingQuote(record_number,quote_id,quote_pricing_id,mode)
{  
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,quote_id:quote_id,quote_pricing_id:quote_pricing_id,record_number:record_number},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {   
            if(mode=='CONFIRM_DELETE_QUOTE_PRICING')
            {
                $("#contactPopup").attr('style','display:none;');
                
                var div_id = 'add_booking_quote_sub_container_'+(record_number) ;  
                remove_booking_quote(div_id);
                $("#"+div_id).parent().attr('style','display:none;');
                updateEmailBody(record_number);
                enableQuoteSendButton();
            }
            else
            {
                $("#contactPopup").html(result_ary[1]);		
                $("#contactPopup").attr('style','display:block');	
            }   
        } 
        $("#loader").attr('style','display:none');
    });
} 

function check_form_field_not_required_courier(formId,inputId)
{
    $("#"+inputId).removeClass('red_border');
   // $("#"+inputId,"#"+formId).validationEngine('hide'); 
}

function checkProviderData()
{
	var value = $('#courier_provider_product_form').serialize();
	var newValue=value+'&mode=CHECKING_COURIER_PROVIDER_PRODUCT';
	var id=$("#id").attr('value');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
	
		$("#add_edit_provider_proudct").unbind("click");
		$("#add_edit_provider_proudct").attr("style",'opacity:0.4');
		if(jQuery.trim(result)=='Both')
		{
			$("#szName").addClass('red_border');
			$("#fPrice").addClass('red_border');
		}
		else if(jQuery.trim(result)=='Name')
		{
			$("#szName").addClass('red_border');
			$("#fPrice").removeClass('red_border');
		}
		else if(jQuery.trim(result)=='Price')
		{
			$("#szName").removeClass('red_border');
			$("#fPrice").addClass('red_border');
		}
		else
		{
			if(id>0)
			{
				$("#szName").removeClass('red_border');
				$("#fPrice").removeClass('red_border');
				
				$("#add_edit_provider_proudct").unbind("click");
	            $("#add_edit_provider_proudct").click(function(){addEditCourierProvider();});
	            $("#add_edit_provider_proudct").attr("style",'opacity:1');
	        }
                
		}
	});
}

function addEditCourierProvider()
{
	var value = $('#courier_provider_product_form').serialize();
	var newValue=value+'&mode=ADD_EDIT_COURIER_PROVIDER_PRODUCT';
	 $("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
		var result_ary = result.split("||||"); 
		if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#courier_provider_form").html(result_ary[1]);		
          
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {   
            $("#courier_provider_listing").html(result_ary[1]);		
        } 
        $("#loader").attr('style','display:none');
	});
}

function check_form_field_empty_standard_courier(formId,inputId,container_id,display_bottom)
{	
    if(isFormInputElementEmpty(formId,inputId)) 
    {		
       $("#"+container_id).addClass('red_border');
       return false;
    }
    else
    {
    	$("#"+container_id).removeClass('red_border');
    }
} 
function showEditDeleteDataoFProvider(id,textEdit,textSave,textcancel)
{
    var idDiv="courier_provider_"+id;
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,mode:'SELECT_PROVIDER_DATA_FROM_LIST'},function(result){
        var result_ary = result.split("||||"); 
        //alert(result_ary[2]);
        $("#courier_provider_listing").html(result_ary[0]);	
        $("#courier_provider_product_listing").html(result_ary[1]);
        $("#courier_product_group").html(result_ary[2]);
        $("#cargo_limitation").html(result_ary[3]);
        $("#courier_cargo_limitation_terms_instruction_data").html(result_ary[4]);
        	
        $("#loader").attr('style','display:none');
        $('#'+idDiv).focus();
        $("#add_edit_provider_proudct").html('<span style="min-width:50px;">'+textEdit+'</span>');
        $("#add_edit_provider_proudct").unbind("click");
        $("#add_edit_provider_proudct").click(function(){editCourierProvider(id,textSave,textcancel);});
        $("#add_edit_provider_proudct").attr("style",'opacity:1;'); 
    });
}

function editCourierProvider(id,textSave,textcancel)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,mode:'EDIT_PROVIDER_DATA_FROM'},function(result){

        $("#courier_provider_product").html(result);	
        $(".courier-service-provider-input-fields").removeAttr('disabled');
        
        $("#add_edit_provider_proudct").html('<span style="min-width:50px;">'+textSave+'</span>');
        $("#add_edit_provider_proudct").unbind("click");
        $("#add_edit_provider_proudct").click(function(){addEditCourierProvider();}); 
    
        $("#detete_provider_proudct").html('<span style="min-width:50px;">'+textcancel+'</span>');	
        $("#detete_provider_proudct").unbind("click");
        $("#detete_provider_proudct").click(function(){cancelCourierEdit();});
        $("#detete_provider_proudct").attr("style",'opacity:1;');
        $("#loader").attr('style','display:none');
    });
}


function cancelCourierEdit(id)
{
    var idDiv="courier_provider_"+id;
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,mode:'SELECT_PROVIDER_DATA_FROM_LIST'},function(result){
        var result_ary = result.split("||||");  
        $("#courier_provider_listing").html(result_ary[0]);	
        $("#courier_provider_product_listing").html(result_ary[1]);
        $("#courier_product_group").html(result_ary[2]);
        $("#loader").attr('style','display:none');
    });
}

function deleteCourierProvider(id)
{
	$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'DELETE_PROVIDER_DATA',id:id},function(result){
	     $("#contactPopup").html(result);		
           $("#contactPopup").attr('style','display:block');
         
        $("#loader").attr('style','display:none');
    });
}

function delete_courier_provider(id)
{
	$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'CONFIRM_DELETE_PROVIDER_DATA',id:id},function(result){
	      var result_ary = result.split("||||"); 
        
        $("#courier_provider_listing").html(result_ary[0]);	
        $("#courier_provider_product_listing").html(result_ary[1]);
        		
           $("#contactPopup").attr('style','display:none');
         
        $("#loader").attr('style','display:none');
    });
}


function checkProviderProductData()
{ 
    var formId = 'courierProviderProductRightForm';
    var error_count = 1;
    if(isFormInputElementEmpty(formId,'szProductName')) 
    {		 
        console.log("Product Name Empty");
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'szApiCode')) 
    {		 
        console.log("Api code Empty");
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'idPacking')) 
    {		 
        console.log("Packing Empty");
        error_count++;
    } 
    
    if(isFormInputElementEmpty(formId,'idGroup')) 
    {		 
        console.log("Group Empty");
        error_count++;
    } 
    var iDays = $("#iDaysStd").val();
    iDays = parseInt(iDays);
    
    if(isFormInputElementEmpty(formId,'iDaysStd')) 
    {		 
        console.log("Days Std Empty");
        error_count++;
    } 
    else if(iDays>0)
    {
        $("#iDaysStd").removeClass('red_border');
    }
    else
    {
        console.log("Days Std Empty");
        error_count++;
    }  
    
    var iDaysMin = $("#iDaysMin").val();
    iDaysMin = parseInt(iDaysMin);
    if(isFormInputElementEmpty(formId,'iDaysMin')) 
    {		 
        console.log("Days Min Empty");
        error_count++;
    } 
    else if(iDaysMin>0)
    {
        $("#iDaysMin").removeClass('red_border');
    }
    else
    {
        console.log("Days Min Empty");
        error_count++;
    }
    
    
    if(isFormInputElementEmpty(formId,'idCourierProvider')) 
    {		 
        console.log("Provider ID Empty");
        error_count++;
    }
    
    var id=$("#courierProviderProductRightForm #id").val();
    
    if(id>0)
    {
        $("#add_edit_provider_proudct_right").html("<span style='min-width:50px;'>Save</span>");
    }
    else
    {
        $("#add_edit_provider_proudct_right").html("<span style='min-width:50px;'>Add</span>");
        
        $("#detete_provider_proudct").unbind("click");
        $("#detete_provider_proudct").attr("style",'opacity:0.4;');
    }
    
    if(error_count==1)
    {
        $("#add_edit_provider_proudct_right").unbind("click");
        $("#add_edit_provider_proudct_right").click(function(){addEditCourierProviderProduct();});
        $("#add_edit_provider_proudct_right").attr("style",'opacity:1');
    }
    else
    {
        $("#add_edit_provider_proudct_right").unbind("click");
        $("#add_edit_provider_proudct_right").attr("style",'opacity:0.4');
    }   
}

function addEditCourierProviderProduct()
{
	var value = $('#courierProviderProductRightForm').serialize();
	var newValue=value+'&mode=ADD_EDIT_COURIER_PROVIDER_PRODUCT_DATA';
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
	var result_ary = result.split("||||"); 
		$("#courier_provider_product_right").html(result_ary[1]);
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			 $("#courier_provider_listing").html(result_ary[1]);	
        	$("#courier_provider_product_listing").html(result_ary[2]);
        
			$("#add_edit_provider_proudct_right").unbind("click");
		    $("#add_edit_provider_proudct_right").attr("style",'opacity:0.4');
		}
		else
		{
			$("#add_edit_provider_proudct_right").unbind("click");
		    $("#add_edit_provider_proudct_right").attr("style",'opacity:0.4');
		}
		
		$("#loader").attr('style','display:none');
		
	});
}


function showEditDeleteDataoFProviderProduct(id,idCourierProvider,textEdit,textSave,textcancel)
{
    var idDiv="courier_provider_"+id;
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,idCourierProvider:idCourierProvider,mode:'SELECT_COURIER_PRODUCT_DATA'},function(result){
    var result_ary = result.split("||||"); 
        
        	
        $("#courier_provider_product_listing").html(result_ary[0]);
        //$("#cargo_limitation").html(result_ary[1]);
        $("#courier_cargo_limitation_terms_instruction_data").html(result_ary[1]);
        	
        $("#loader").attr('style','display:none');
        $('#'+idDiv).focus();
        $("#add_edit_provider_proudct_right").html('<span style="min-width:50px;">'+textEdit+'</span>');
        $("#add_edit_provider_proudct_right").unbind("click");
        $("#add_edit_provider_proudct_right").click(function(){editCourierProviderProduct(id,idCourierProvider,textSave,textcancel,textEdit);});
        $("#add_edit_provider_proudct_right").attr("style",'opacity:1;');
        
        
        $("#detete_provider_proudct_right").unbind("click");
        $("#detete_provider_proudct_right").click(function(){deleteCourierProviderProduct(id,idCourierProvider);});
        $("#detete_provider_proudct_right").attr("style",'opacity:1;');
    });
}

function editCourierProviderProduct(id,idCourierProvider,textSave,textcancel,textEdit)
{
	 $("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,idCourierProvider:idCourierProvider,mode:'EDIT_PROVIDER_PRODUCT_DATA_FROM'},function(result){
	
	$("#courier_provider_product_right").html(result);	
	
	$("#add_edit_provider_proudct_right").html('<span style="min-width:50px;">'+textSave+'</span>');
    $("#add_edit_provider_proudct_right").unbind("click");
    $("#add_edit_provider_proudct_right").click(function(){addEditCourierProviderProduct();});
    
    
     $("#detete_provider_proudct_right").html('<span style="min-width:50px;">'+textcancel+'</span>');	
     $("#detete_provider_proudct_right").unbind("click");
     $("#detete_provider_proudct_right").click(function(){showEditDeleteDataoFProviderProduct(id,idCourierProvider,textEdit,textSave,textcancel);});
     $("#detete_provider_proudct_right").attr("style",'opacity:1;');
      $("#loader").attr('style','display:none');
     });
}

function deleteCourierProviderProduct(id,idCourierProvider)
{

$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'DELETE_PROVIDER_DATA_DATA',id:id,idCourierProvider:idCourierProvider},function(result){
	     $("#contactPopup").html(result);		
           $("#contactPopup").attr('style','display:block');
         
        $("#loader").attr('style','display:none');
    });
}

function confirm_delete_courier_provider_product(id,idCourierProvider)
{
	$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'CONFIRM_DELETE_PROVIDER_PRODUCT_DATA',id:id,idCourierProvider:idCourierProvider},function(result){
	      //var result_ary = result.split("||||"); 
        
        $("#courier_provider_product_listing").html(result);	
        
        		
           $("#contactPopup").attr('style','display:none');
         
        $("#loader").attr('style','display:none');
    });
}

function checkCourierCargoLimitData()
{
    var value = $('#courierCargoLimitationForm').serialize();
    var newValue=value+'&mode=CHECKING_COURIER_CARGO_LIMITATION_DATA';
    
    var idValue=$("#courierCargoLimitationForm #id").val();

    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
        var result_ary = result.split("||||"); 
        //$("#courier_cargo_limitation_addedit_data").html(result_ary[1]);
        if(idValue>0)
        {
            $("#add_edit_cargo_limitation_data").html("<span>Save</span>");    
        }
        else
        {
            $("#add_edit_cargo_limitation_data").html("<span>Add</span>");
        }
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#add_edit_cargo_limitation_data").unbind("click");
            $("#add_edit_cargo_limitation_data").click(function(){addEditCourierCargoLimit();});
            $("#add_edit_cargo_limitation_data").attr("style",'opacity:1');
        }
        else
        {
            $("#add_edit_cargo_limitation_data").unbind("click");
            $("#add_edit_cargo_limitation_data").attr("style",'opacity:0.4');
        }
    });
}

function addEditCourierCargoLimit()
{
	var value = $('#courierCargoLimitationForm').serialize();
	var newValue=value+'&mode=ADD_EDIT_COURIER_CARGO_LIMIT_DATA';
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
	var result_ary = result.split("||||"); 
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#cargo_limitation").html(result_ary[1]);
		}
		else
		{
			$("#courier_cargo_limitation_addedit_data").html(result_ary[1]);
		}
		$("#loader").attr('style','display:none');
		
	});
}

function showEditDeleteDataoFCargoLimit(id,idGroup,textEdit,textSave,textcancel)
{
	var idDiv="courier_provider_"+id;
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,idGroup:idGroup,mode:'SELECT_COURIER_CARGO_LIMITATION_DATA'},function(result){
        var result_ary = result.split("||||"); 
        
        	
  
        $("#cargo_limitation").html(result);
        	
        $("#loader").attr('style','display:none');
        $('#'+idDiv).focus();
        $("#add_edit_cargo_limitation_data").html('<span style="min-width:50px;">'+textEdit+'</span>');
        $("#add_edit_cargo_limitation_data").unbind("click");
        $("#add_edit_cargo_limitation_data").click(function(){editCourierCargoLimit(id,idGroup,textSave,textcancel);});
        $("#add_edit_cargo_limitation_data").attr("style",'opacity:1;');
        
        
        $("#detete_cargo_limitation_data").unbind("click");
        $("#detete_cargo_limitation_data").click(function(){deleteCourierCargoLimitation(id,idGroup);});
        $("#detete_cargo_limitation_data").attr("style",'opacity:1;');
	});
}

function editCourierCargoLimit(id,idGroup,textSave,textcancel)
{
	// $("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,idGroup:idGroup,mode:'EDIT_CARGO_LIMITATION_DATA_FROM'},function(result){
	
	$("#courier_cargo_limitation_addedit_data").html(result);	
	
	$("#add_edit_cargo_limitation_data").html('<span style="min-width:50px;">'+textSave+'</span>');
    $("#add_edit_cargo_limitation_data").unbind("click");
    $("#add_edit_cargo_limitation_data").click(function(){addEditCourierCargoLimit();});
    $("#add_edit_cargo_limitation_data").attr("style",'opacity:1;');
    
     $("#detete_cargo_limitation_data").html('<span style="min-width:50px;">'+textcancel+'</span>');	
     $("#detete_cargo_limitation_data").unbind("click");
     $("#detete_cargo_limitation_data").click(function(){cancelCourierCargoLimitEdit(idGroup);});
     $("#detete_cargo_limitation_data").attr("style",'opacity:1;');
      $("#loader").attr('style','display:none');
     });
}

function cancelCourierCargoLimitEdit(idGroup)
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{idGroup:idGroup,mode:'SELECT_COURIER_CARGO_LIMITATION_DATA'},function(result){
        $("#cargo_limitation").html(result);	
       $("#loader").attr('style','display:none');
	});
}

function deleteCourierCargoLimitation(id,idGroup)
{
    //$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,idGroup:idGroup,mode:'DELETE_COURIER_CARGO_LIMITATION_DATA'},function(result){
        $("#cargo_limitation").html(result);	
       $("#loader").attr('style','display:none');
    });
}


function checkProviderProductTermInsData()
{ 
    var error_count=1;  
//    if(isFormInputElementEmpty("courierProductTermsInstructionForm",'szInstruction') && isFormInputElementEmpty("courierProductTermsInstructionForm",'idCourierProviderProduct')) 
//    {		
//        error_count++;
//    }  
//    if(isFormInputElementEmpty("courierProductTermsInstructionForm",'szTerms')) 
//    {		
//        error_count++;
//    }
//From now onwards save button is always active because both textfields are optional
error_count = 1;
    var idCourierProduct=$("#courierProductTermsInstructionForm #idCourierProviderProduct").val();
    if(idCourierProduct=='' || idCourierProduct==0) 
    {		
        error_count++;
    }
    
    if(error_count==1)
    {
        $("#add_edit_terms_instruction_data").unbind("click");
        $("#add_edit_terms_instruction_data").click(function(){addEditProductTermsInstruction();});
        $("#add_edit_terms_instruction_data").attr("style",'opacity:1');
    }
    else
    {
        $("#add_edit_terms_instruction_data").unbind("click");
        $("#add_edit_terms_instruction_data").attr("style",'opacity:0.4');
    } 
}

function addEditProductTermsInstruction()
{
	var value = $('#courierProductTermsInstructionForm').serialize();
	var newValue=value+'&mode=ADD_EDIT_COURIER_PRODUCT_INS_TERM_DATA';
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
	var result_ary = result.split("||||"); 
		$("#courier_cargo_limitation_terms_instruction_data").html(result_ary[1]);
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
				$("#add_edit_terms_instruction_data").unbind("click");
		   		 $("#add_edit_terms_instruction_data").attr("style",'opacity:0.4');
		}
		else
		{
			
		    
		    $("#add_edit_terms_instruction_data").unbind("click");
		    $("#add_edit_terms_instruction_data").click(function(){addEditProductTermsInstruction();});
		    $("#add_edit_terms_instruction_data").attr("style",'opacity:1');
		}
	});
}

function getTermInstructionByidLanguage(idCourierProviderProduct,idLanguage)
{
	$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{idLanguage:idLanguage,idCourierProviderProduct:idCourierProviderProduct,mode:'GET_INSTRUCTION_TERM_BY_LANGUAGE'},function(result){
        $("#courier_cargo_limitation_terms_instruction_data").html(result);	
       $("#loader").attr('style','display:none');
	});
}

function enableModeTransportForm()
{
    var error_count = 1;
    var formId = 'modeTrasnportAddForm';
    if(isFormInputElementEmpty(formId,'idFromCountry')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'idToCountry')) 
    {		 
        error_count++;
    }
    if(error_count==1)
    {
        return true;
    }
    else
    {
        $("#add_edit_mode_transport_data").unbind("click");
        $("#add_edit_mode_transport_data").attr("style",'opacity:0.4'); 
        return false;
    }
}

function checkCountryIsSelected()
{
    var ajax_callflag = enableModeTransportForm();
    if(ajax_callflag)
    {
        var value = $('#modeTrasnportAddForm').serialize();
        var newValue=value+'&mode=CHECKING_COURIER_MODE_TRANSPORT_DATA';

        $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){

            var result_ary = result.split("||||");   
            if($.trim(result_ary[0])=='SUCCESS')
            {
                $("#add_edit_mode_transport_data").unbind("click");
                $("#add_edit_mode_transport_data").click(function(){addEditModeTransportData();});
                $("#add_edit_mode_transport_data").attr("style",'opacity:1');
            }
            else
            {
                $("#add_edit_mode_transport_data").unbind("click");
                $("#add_edit_mode_transport_data").attr("style",'opacity:0.4');
            }
        });
    } 
}

function addEditModeTransportData()
{
    var value = $('#modeTrasnportAddForm').serialize();
    var newValue=value+'&mode=ADD_EDIT_COURIER_MODE_TRANSPORT_DATA';
	 
    var idPrimarySortKey=$("#idPrimarySortKey").val();
    var szSortValue=$("#idPrimarySortValue").val();
    var idOtherSortKeyArr=$("#idOtherSortKey").val();
     
     var newValue=value+'&mode=ADD_EDIT_COURIER_MODE_TRANSPORT_DATA&idPrimarySortKey='+idPrimarySortKey+'&szSortValue='+szSortValue+'&idOtherSortKeyArr='+idOtherSortKeyArr;
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
		var result_ary = result.split("||||"); 
		
		if($.trim(result_ary[0])=='SUCCESS')
		{
			$("#add_edit_mode_transport_data").unbind("click");
		    $("#add_edit_mode_transport_data").attr("style",'opacity:0.4');
		    $("#mode_transport_div").html(result_ary[1]);
		}
		else
		{
			$("#courier_cargo_limitation_addedit_data").html(result_ary[1]);
			$("#add_edit_mode_transport_data").unbind("click");
		    $("#add_edit_mode_transport_data").click(function(){addEditModeTransportData();});
		    $("#add_edit_mode_transport_data").attr("style",'opacity:1');
			
		}
	});
}

function seletTheRowForDeleteTruckicon(id)
{ 
    var divId1="from_to_country_1_"+id;
    var divId2="from_to_country_2_"+id;
    var divId3="from_to_country_3_"+id;
    var divId4="from_to_country_4_"+id;

    var deleteTruckiconIdArr=$("#deleteTruckiconId").attr('value');
	
    if(deleteTruckiconIdArr=='')
    {
        $("#deleteTruckiconId").attr('value',id);

        $("#"+divId1).attr('style','background:#DBDBDB;color:#828282;');
        $("#"+divId2).attr('style','background:#DBDBDB;color:#828282;');
        $("#"+divId3).attr('style','background:#DBDBDB;color:#828282;');
        $("#"+divId4).attr('style','background:#DBDBDB;color:#828282;');
    }
    else
    {
        var res_ary_new = deleteTruckiconIdArr.split(";");
        var add_truck=jQuery.inArray(id, res_ary_new);
       if(add_truck!='-1')
       {			
            res_ary_new = $.grep(res_ary_new, function(value) {
                return id != value;
            });

            var value1=res_ary_new.join(';');
            $("#deleteTruckiconId").attr('value',value1);

            $("#"+divId1).attr('style','');
            $("#"+divId2).attr('style','');
            $("#"+divId3).attr('style','');
            $("#"+divId4).attr('style','');
       }
       else
       {
            var value1=deleteTruckiconIdArr+';'+id;
            $("#deleteTruckiconId").attr('value',value1);

            $("#"+divId1).attr('style','background:#DBDBDB;color:#828282;');
            $("#"+divId2).attr('style','background:#DBDBDB;color:#828282;');
            $("#"+divId3).attr('style','background:#DBDBDB;color:#828282;');
            $("#"+divId4).attr('style','background:#DBDBDB;color:#828282;');
       }
    } 
    
    var deleteTruckiconIdArr=$("#deleteTruckiconId").attr('value'); 
    if(deleteTruckiconIdArr!='')
    {
        $("#detete_mode_transport_data").unbind("click");
        $("#detete_mode_transport_data").click(function(){deleteTruckIconData();});
        $("#detete_mode_transport_data").attr("style",'opacity:1');
    }
    else
    {
        $("#detete_mode_transport_data").unbind("click");
        $("#detete_mode_transport_data").attr("style",'opacity:0.4');
    }
}

function deleteTruckIconData()
{
    var deleteTruckiconIdArr=$("#deleteTruckiconId").attr('value');

    var idPrimarySortKey=$("#idPrimarySortKey").val();
    var szSortValue=$("#idPrimarySortValue").val();
    var idOtherSortKeyArr=$("#idOtherSortKey").val();
    var iDoor2DoorTrades=$("#iDoor2DoorTrades").val();
	
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'DELETE_TRCK_ICON_DATA',deleteIdArr:deleteTruckiconIdArr,idOtherSortKeyArr:idOtherSortKeyArr,szSortValue:szSortValue,idPrimarySortKey:idPrimarySortKey,iDTDFlag:iDoor2DoorTrades},function(result){
        $("#mode_transport_div").html(result);
        $("#deleteTruckiconId").attr('value','');
    });
}

function on_enter_key_press_shift(kEvent,szValue)
{ 
    var szSortValue=''; 
    var idPrimarySortKey=$("#idPrimarySortKey").val();
     var szSortValue=$("#idPrimarySortValue").val();
     
     var idOtherSortKeyArr=$("#idOtherSortKey").attr('value'); 
     
        if (kEvent.shiftKey) 
        { 
            if(idPrimarySortKey==szValue)
            {
	     	if(szSortValue=='ASC')
	     	{
                    szSortValue='DESC';
	     	}
	     	else
	     	{
                    szSortValue='ASC';
	     	} 
	     }  
        if(idPrimarySortKey=='')
        {
            $("#idPrimarySortKey").attr('value',szValue);
            $("#idPrimarySortValue").attr('value',szSortValue);
        }
        if(idPrimarySortKey==szValue)
     	{
     		$("#idPrimarySortValue").attr('value',szSortValue);
     	}
     	else
     	{
     	
	     	if(idOtherSortKeyArr=='')
			{
				var ascValue=szValue+"_ASC";
				$("#idOtherSortKey").attr('value',ascValue);				
			}
			else
			{
				var descValue=szValue+"_DESC";
				var ascValue=szValue+"_ASC";
			
				
				var res_ary_new = idOtherSortKeyArr.split(";");
				var add_truck=jQuery.inArray(descValue, res_ary_new);
				if(add_truck!='-1')
				{			
					res_ary_new = $.grep(res_ary_new, function(value) {
					    return descValue != value;
					});
									
					var value1=res_ary_new.join(';');
					$("#idOtherSortKey").attr('value',value1);
					var idOtherSortKeyArr=$("#idOtherSortKey").val();
					if(idOtherSortKeyArr=='')
					{
						$("#idOtherSortKey").attr('value',ascValue);
					}
					else
					{
						var value1=idOtherSortKeyArr+';'+ascValue;
						$("#idOtherSortKey").attr('value',value1);
					}
				}
				else
				{
					var add_truck=jQuery.inArray(ascValue, res_ary_new);
					if(add_truck!='-1')
					{			
						res_ary_new = $.grep(res_ary_new, function(value) {
						    return ascValue != value;
						});
										
						var value1=res_ary_new.join(';');
						$("#idOtherSortKey").attr('value',value1);
						var idOtherSortKeyArr=$("#idOtherSortKey").val();
						if(idOtherSortKeyArr=='')
						{
							$("#idOtherSortKey").attr('value',descValue);
						}
						else
						{
							var value1=idOtherSortKeyArr+';'+descValue;
							$("#idOtherSortKey").attr('value',value1);
						}
					}
					else
					{
						var value1=idOtherSortKeyArr+';'+ascValue;
						$("#idOtherSortKey").attr('value',value1);
					}
				}
			}
		}
     
    } else {
    	
    	if(idPrimarySortKey==szValue)
     	{
	    	if(szSortValue=='ASC')
	     	{
	     		szSortValue='DESC';
	     	}
	     	else
	     	{
	     		szSortValue='ASC';
	     	}
	     	
	     	$("#idPrimarySortValue").attr('value',szSortValue);
	     	$("#idOtherSortKey").attr('value','');
	     }
	     else
	     {
	     
        	$("#idPrimarySortKey").attr('value',szValue);
        	$("#idPrimarySortValue").attr('value','ASC');
        	$("#idOtherSortKey").attr('value','');
        }
    }
    
     var idPrimarySortKey=$("#idPrimarySortKey").val();
     var szSortValue=$("#idPrimarySortValue").val();
     var idOtherSortKeyArr=$("#idOtherSortKey").val();
     var iDoor2DoorTrades=$("#iDoor2DoorTrades").val();
     
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'SORT_MODE_TRANSPORT',idOtherSortKeyArr:idOtherSortKeyArr,szSortValue:szSortValue,idPrimarySortKey:idPrimarySortKey,iDTDFlag:iDoor2DoorTrades},function(result){
		$("#mode_transport_div").html(result);
		$("#deleteTruckiconId").attr('value','');
                
                $(".moe-transport-sort-span").removeClass('sort-arrow-up');
                $(".moe-transport-sort-span").removeClass('sort-arrow-down');
                if(szSortValue=='ASC')
	     	{
                    $("#moe-transport-sort-span-id-"+szValue).addClass('sort-arrow-up');
                }
                else
                { 
                    $("#moe-transport-sort-span-id-"+szValue).addClass('sort-arrow-down');
                }
	});
   
}
function close_pending_task(file_id,mode)
{ 
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,file_id:file_id},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {   
           $("#contactPopup").html(result_ary[1]); 
           $("#contactPopup").attr('style','display:block;');
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            $("#contactPopup").attr('style','display:none');
            
            updatePendingTrayListing(result_ary[1]);
            
            //$("#pending_task_listing_container").html(result_ary[1]);	
            $("#pending_task_listing_container").attr('style','display:block;'); 
            
            $("#pending_task_overview_main_container").html("");
            $("#pending_task_overview_main_container").attr('style','display:none;');  
            
            $("#pending_task_tray_main_container").html("");
            $("#pending_task_tray_main_container").attr('style','display:none;');  
            $("#idLastClikedBookingFile").val("");
        }
        $("#loader").attr('style','display:none');
    });
} 

function update_description(cb_id,prefill_text)
{
    var cb_flag = $("#"+cb_id).prop("checked"); 
    if(cb_flag)
    {
        $("#szCargoDescription").attr('disabled','disabled');
        $("#szCargoDescription").val(prefill_text);
        $("#szCargoDescriptionHidden").val(prefill_text);
    }
    else
    {
        $("#szCargoDescription").removeAttr('disabled');
    }
}

function checkVatValue()
{
	var fVATRate=$("#fVATRate").val();
	var fVATRateArr = fVATRate.split(".");
	if(fVATRateArr.length>1)
	{
		fVATRate1=$("#fVATRate").val();
		var n = parseFloat(fVATRate1).toFixed(2); 
		$("#fVATRate").attr('value',n);
	}
}

function checkForwarderCountrySeleted()
{
	var value=$('#forwarderCountryForm').serialize();
	var newValue=value+'&mode=CHECK_FORWARDER_COUNTRY_EXISTS';
	$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",newValue,function(result){
    	 var result_ary = result.split("||||"); 
    	 $("#left_form_div").html(result_ary[1]);
		if(result_ary[0]=='SUCCESS')
		{
			$("#add_edit_forwarder_country_data").unbind("click");
		    $("#add_edit_forwarder_country_data").click(function(){addForwarderCourtyData();});
		    $("#add_edit_forwarder_country_data").attr("style",'opacity:1');
		}
		else
		{
			$("#add_edit_forwarder_country_data").unbind("click");
			$("#add_edit_forwarder_country_data").attr("style",'opacity:0.4');
		}
		$("#loader").attr('style','display:none');
	});
}

function addForwarderCourtyData()
{
	var value=$('#forwarderCountryForm').serialize();
	var newValue=value+'&mode=ADD_FORWARDER_COUNTRY';
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",newValue,function(result){
    	 var result_ary = result.split("||||"); 
		if(result_ary[0]=='SUCCESS')
		{
			 $("#vat_appliaction_left").html(result_ary[1]);
			$("#add_edit_forwarder_country_data").unbind("click");
			$("#add_edit_forwarder_country_data").attr("style",'opacity:0.4');
		}
		else
		{
			 $("#left_form_div").html(result_ary[1]);
			$("#add_edit_forwarder_country_data").unbind("click");
			$("#add_edit_forwarder_country_data").attr("style",'opacity:0.4');
		}
		$("#loader").attr('style','display:none');
	});
}

function selectForwarderCountryData(id,szCountryName,flag)
{
	var divid="sel_forwarder_"+id;
	$('.selected-row').removeClass('selected-row');
	$('.selected-row-red').removeClass('selected-row-red');
	if(flag==1)
	{
		$("#"+divid).addClass('selected-row-red');
	}
	else
	{
		$("#"+divid).addClass('selected-row');
	}
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'SHOW_TRADE_DATA',id:id},function(result){
    	$("#vat_appliaction_right").html(result);
	   
                /*$("#detete_forwarder_country_data").unbind("click");
		$("#detete_forwarder_country_data").click(function(){deleteForwarderCourtyData(id,szCountryName);});
		$("#detete_forwarder_country_data").attr("style",'opacity:1');*/
		
		if(parseInt(id)>0)
                {
//                    $("#copy_forwarder_country_trade_data").unbind("click");
//                    $("#copy_forwarder_country_trade_data").click(function(){copyForwarderCourtyData(id,szCountryName);});
//                    $("#copy_forwarder_country_trade_data").attr("style",'opacity:1');
                    $("#idCountryTradeForwarderName").attr('value',szCountryName);
                }
		//$("#loader").attr('style','display:none');
    	
	});
	
	
}

function deleteForwarderCourtyData()
{
        var deleteCountryForwardersId=$("#deleteCountryForwardersId").attr('value');
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'DELETE_FORWARDER_COUNTRY_CONFIRM',deleteCountryForwardersId:deleteCountryForwardersId},function(result){
    	$("#contactPopup").html(result);
    	$("#contactPopup").attr('style','display:block');
		$("#loader").attr('style','display:none');
	});
}

function delete_forwarder_country_data(id)
{
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'DELETE_FORWARDER_COUNTRY',id:id},function(result){
	 	 var result_ary = result.split("||||"); 
    	$("#vat_appliaction_left").html(result_ary[0]);
    	$("#vat_appliaction_right").html(result_ary[1]);
    	$("#contactPopup").attr('style','display:none');
		$("#loader").attr('style','display:none');
	});
}

function copyForwarderCourtyData(id,szCountryName)
{
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'COPY_FORWARDER_COUNTRY_CONFIRM',id:id,szCountryName:szCountryName},function(result){
    	$("#contactPopup").html(result);
    	$("#contactPopup").attr('style','display:block');
		$("#loader").attr('style','display:none');
	});
	
}

function activateCopyButton(idCountry,id,szCountryName)
{
	if(parseInt(idCountry)>0)
	{
		$("#copy_button").unbind("click");
		$("#copy_button").click(function(){copyForwarderCourtyDataConfirm(id,szCountryName,idCountry);});
		$("#copy_button").attr("style",'opacity:1');
	}
	else
	{
		$("#copy_button").unbind("click");
		$("#copy_button").attr("style",'opacity:0.4');
	}
}

function copyForwarderCourtyDataConfirm(id,szCountryName,idCountry)
{
        $("#copy_button").unbind("click");
        $("#copy_button").attr("style",'opacity:0.4');
        
        $("#copy_cancel_button").unbind("click");
        $("#copy_cancel_button").attr("style",'opacity:0.4');
                
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'COPY_FORWARDER_COUNTRY_DATA',id:id,szCountryName:szCountryName,idCountry:idCountry},function(result){
    	var result_ary = result.split("||||"); 
    	$("#vat_appliaction_left").html(result_ary[0]);
    	$("#vat_appliaction_right").html(result_ary[1]);
    	$("#show_message").html(result_ary[2]);
    	$("#contactPopup").attr('style','display:none');
    	
    	$("#detete_forwarder_country_data").unbind("click");
		$("#detete_forwarder_country_data").click(function(){deleteForwarderCourtyData(id,szCountryName);});
		$("#detete_forwarder_country_data").attr("style",'opacity:1');
		
		
//		$("#copy_forwarder_country_trade_data").unbind("click");
//		$("#copy_forwarder_country_trade_data").click(function(){copyForwarderCourtyData(id,szCountryName);});
//		$("#copy_forwarder_country_trade_data").attr("style",'opacity:1');
		
		$("#loader").attr('style','display:none');
	});

}

function activeTradeFromToButton()
{
    var idFromCountry=$("#idCountryForm").val();
    var idToCountry=$("#idToCountry").val();
    //var idCountryForwarder=$("#idCountryTradeForwarder").val();
    if(parseInt(idFromCountry)>0 && parseInt(idToCountry)>0)
    {
        $("#add_edit_forwarder_country_trade_data").unbind("click");
        $("#add_edit_forwarder_country_trade_data").click(function(){addVatTradeData();});
        $("#add_edit_forwarder_country_trade_data").attr("style",'opacity:1');
    }
    else
    {
        $("#add_edit_forwarder_country_trade_data").unbind("click");
        $("#add_edit_forwarder_country_trade_data").attr("style",'opacity:0.4');
    }
}

function addVatTradeData()
{
    var value=$('#forwarderCountryTradeForm').serialize();
    var newValue=value+'&mode=ADD_FORWARDER_COUNTRY_TRADE';
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",newValue,function(result){
        var result_ary = result.split("||||"); 
        if(result_ary[0]=='SUCCESS')
        {
            $("#vat_appliaction_right").html(result_ary[1]); 
            $("#show_message").html(result_ary[2]);
            $("#add_edit_forwarder_country_trade_data").unbind("click");
            $("#add_edit_forwarder_country_trade_data").attr("style",'opacity:0.4'); 
        }
        else
        {
            $("#right_form_div").html(result_ary[1]);
            $("#add_edit_forwarder_country_trade_data").unbind("click");
            $("#add_edit_forwarder_country_trade_data").attr("style",'opacity:0.4');
        }
        $("#loader").attr('style','display:none');
    });
}

function selectVatTradeLine(id)
{
	var divid="vat_trade_"+id;
	$('.selected-row').removeClass('selected-row');
	
	$("#"+divid).addClass('selected-row');
	
	$("#detete_forwarder_country_trade_data").unbind("click");
	$("#detete_forwarder_country_trade_data").click(function(){deleteForwarderCourtyTradeData(id);});
	$("#detete_forwarder_country_trade_data").attr("style",'opacity:1');
}

function deleteForwarderCourtyTradeData(szCountryName)
{
	var idCountryForwarder=$("#idCountryTradeForwarder").val();
        var id=$("#deleteCountryForwardersTradesId").val();
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'DELETE_VAT_TRADE_DATA',id:id,idCountryForwarder:idCountryForwarder},function(result){
    	var result_ary = result.split("||||");  
    	$("#vat_appliaction_right").html(result_ary[1]);
    	$("#show_message").html(result_ary[0]); 
    	$("#loader").attr('style','display:none');
    });
}

function upload_booking_label(idBooking)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"UPLOAD_SELECTED_BOOKING_LABEL",idBooking:idBooking},function(result){
        redirect_url(result);
    });
}

function upload_booking_label_sent(idBooking)
{
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'BOOKING_LABELS_SENT',idBooking:idBooking},function(result){
    	$("#contactPopup").html(result);
    	$("#contactPopup").attr('style','display:block');
		$("#loader").attr('style','display:none');
	});
}

function sendBookingLabel(idBooking)
{
    var szTrackingNumber=$("#szTrackingNumber").val();
    var modeHidden=$("#modeHidden").val(); 
    var cb_flag = $("#iSendTrackingUpdates").prop("checked"); 
    var limit = $("#limit").val();
    var page = $("#page").val();
    var iSendTrackingUpdates = 1;
    if(cb_flag)
    {
        iSendTrackingUpdates = 0;
    }
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{modeHidden:modeHidden,mode:'BOOKING_LABELS_SENT_CONFIRM',iSendTrackingUpdates:iSendTrackingUpdates,idBooking:idBooking,szTrackingNumber:szTrackingNumber,page:page,limit:limit},function(result){
    	var result_ary = result.split("||||");
    	if(result_ary[0]=='REDIRECT')
    	{
            redirect_url(result_ary[1]);
    	}
        else if(result_ary[0]=='SUCESS')
    	{
            $("#contactPopup").html('');
            $("#contactPopup").attr('style','display:none');
    	}
    	else
    	{ 
            $("#contactPopup").html(result_ary[1]);
            $("#contactPopup").attr('style','display:block');
    	} 
    });	
}

function upload_booking_label_new_sent(idBooking)
{
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'BOOKING_LABELS_NEW_SENT',idBooking:idBooking},function(result){
    	$("#contactPopup").html(result);
    	$("#contactPopup").attr('style','display:block');
		$("#loader").attr('style','display:none');
	});
}

function sendNewLabelForBooking(idBooking)
{
        $("#popup-container .button1").unbind("click");
        $("#popup-container .button1").attr("style","opacity:0.4");
         var pageName=$("#pageName").val();
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'BOOKING_LABELS_SENT_NEW',idBooking:idBooking,pageName:pageName},function(result){
    	redirect_url(result);		
	});	
}

function resend_courier_label_email(idBooking)
{
        $("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'COURIER_BOOKING_LABELS_RESEND',idBooking:idBooking},function(result){
    	$("#contactPopup").html(result);
    	$("#contactPopup").attr('style','display:block');
		$("#loader").attr('style','display:none');
	});
}

function resend_courier_label_email_confirm()
{
        var idBooking=$("#idBooking").val();
        var szEmail=$("#szEmail").val();
        var pageName=$("#pageName").val();
        $("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'CONFIRM_COURIER_BOOKING_LABELS_RESEND',idBooking:idBooking,szEmail:szEmail,pageName:pageName},function(result){
           
            var result_ary = result.split("||||");
            if(result_ary[0]=='SUCCESS')
            {
                    redirect_url(result_ary[1]);
            }
            else
            { 
                $("#contactPopup").html(result_ary[1]);
                $("#contactPopup").attr('style','display:block');
            } 
            $("#loader").attr('style','display:none');
	});
}

function download_courier_label_email(idBooking)
{
     $("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'COURIER_BOOKING_LABELS_DOWNLOAD',idBooking:idBooking},function(result){
    	$("#contactPopup").html(result);
    	$("#contactPopup").attr('style','display:block');
		$("#loader").attr('style','display:none');
	});
}

function sent_courier_label_email(idBooking)
{
	$("#loader").attr('style','display:block');
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'BOOKING_LABELS_SENT',idBooking:idBooking},function(result){
    	$("#contactPopup").html(result);
    	$("#contactPopup").attr('style','display:block');
        $("#modeHidden").attr('value','sent');
		$("#loader").attr('style','display:none');
	});
}

function downloadBookingLabel()
{
     $("#loader").attr('style','display:block');
     var value=$('#downloadCourierLabelForm').serialize();
     var newValue=value+"&mode=CONFIRM_DOWNLOAD_LABEL";
	 $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",newValue,function(result){
    	 var result_ary = result.split("||||");
            if(result_ary[0]=='SUCCESS')
            {
                    redirect_url(result_ary[1]);
            }
            else
            { 
                $("#contactPopup").html(result_ary[1]);
                $("#contactPopup").attr('style','display:block');
            } 
            $("#loader").attr('style','display:none');
	});
}


function viewCourierLabelDetails(tr_id,id,pass,mode,type)
{
        var rowCount = $('#booking_table tr').length;
        if(rowCount>0)
        {
                for(x=1;x<rowCount;x++)
                {
                        new_tr_id="booking_data_"+x;
                        if(new_tr_id!=tr_id)
                        {
                                $("#"+new_tr_id).attr('style','');
                        }
                        else
                        {
                                $("#"+tr_id).attr('style','background:#DBDBDB;color:#828282;');
                        }
                }
        }
        $("#view_notice").attr("onclick","view_forwarder_courier_label('"+id+"','"+pass+"','"+mode+"','"+type+"')");
        $("#view_notice").css("opacity",'1');
}
function view_forwarder_courier_label(id,pass,mode,type)
{	
    if(type=='HANDLING_FEE')
    {
        window.open(__JS_ONLY_SITE_BASE__+"/viewHandlingFeeInvoice/"+pass+"/"+id+"/","_blank");
    }
    else
    {
        window.open(__JS_ONLY_SITE_BASE__+"/forwarderCourierLabelInvoice/"+pass+"/"+id+"/","_blank");
    } 
} 
function open_close_log(divid,emai_id)
{ 
    var disp = $("#"+divid).css('display');
    if(disp=='block')
    {
        $("#"+divid).css('display','none');
        $("#"+divid).slideUp(); 
        $("#email_view_details_link_"+emai_id).html("View details"); 
    }
    else
    {
        $("#"+divid).css('display','block');
        $("#"+divid).slideDown(); 
        $("#email_view_details_link_"+emai_id).html("Hide details");
    } 
    
}

function number_format(number, decimals, dec_point, thousands_sep) 
{  
  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}

function sort_task_estimate_list(sort_field,file_id,mode)
{
    if(mode=='SORT_ESTIMATE_QUOTE_LIST')
    {
        var szOldField = $("#szOldFieldQuote").val();    
        if(szOldField==sort_field)
        {
            var szSortType = $("#szSortTypeQuote").val(); 
            if(szSortType=='ASC')
            {
                var sort = 'DESC';
            }
            else
            {
                var sort = 'ASC';
            } 
        }
        else
        {
             var sort = 'ASC';
        }
    }
    else
    {
        var szOldField = $("#szOldField").val();    
        if(szOldField==sort_field)
        {
            var szSortType = $("#szSortType").val(); 
            if(szSortType=='ASC')
            {
                var sort = 'DESC';
            }
            else
            {
                var sort = 'ASC';
            } 
        }
        else
        {
             var sort = 'ASC';
        }
    }
    
    
    if(sort=='ASC')
    {
        var new_sort='DESC';
        var innr_htm = '<span class="sort-arrow-up">&nbsp;</span>';
    }
    else
    {
        var new_sort='ASC';
        var innr_htm = '<span class="sort-arrow-down">&nbsp;</span>';
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,sort_by:sort,sort_field:sort_field,file_id:file_id},function(result){
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {            
            if(mode=='SORT_ESTIMATE_QUOTE_LIST')
            {
                $("#estimated-quote-container").html(result_ary[1]);   
                $(".moe-transport-sort-span-quote").removeClass('sort-arrow-up');
                $(".moe-transport-sort-span-quote").removeClass('sort-arrow-down'); 
                if(sort=='ASC')
                {
                    $("#moe-transport-sort-span-quote-id-"+sort_field).addClass('sort-arrow-down');
                }
                else
                {
                    $("#moe-transport-sort-span-quoteid-"+sort_field).addClass('sort-arrow-up');
                }
                $("#szOldFieldQuote").val(sort_field);
                $("#szSortTypeQuote").val(sort);
            }
            else
            {
                $("#estimated-booking-container").html(result_ary[1]);   
                $(".moe-transport-sort-span").removeClass('sort-arrow-up');
                $(".moe-transport-sort-span").removeClass('sort-arrow-down'); 
                if(sort=='ASC')
                {
                    $("#moe-transport-sort-span-id-"+sort_field).addClass('sort-arrow-down');
                }
                else
                {
                    $("#moe-transport-sort-span-id-"+sort_field).addClass('sort-arrow-up');
                }
                $("#szOldField").val(sort_field);
                $("#szSortType").val(sort);
            }
        } 
    });
} 

function autofill_billing_address__new_rfq(quick_quote_flag)
{   
    if(quick_quote_flag==1)
    {
        var iShipperConsignee = $('input:radio[name="quickQuoteShipperConsigneeAry[iShipperConsignee]"]:checked').val();  
    } 
    else
    {
        var iShipperConsignee = $('input:radio[name="updatedTaskAry[iShipperConsignee]"]:checked').val();  
    }
    
    var iPendingTrayPageLength = $('#iPendingTrayPage').length; 
        
    iPendingTrayPageFlag=false;
    if(iPendingTrayPageLength>0)
    {
        iPendingTrayPageFlag=true;
    }
        
    if((iShipperConsignee==1 || iShipperConsignee==2))
    {
        var szCompanyName = $("#szCustomerCompanyName").val();
        var szFirstName = $("#szFirstName").val();
        var szLastName = $("#szLastName").val();
        var szEmail = $("#szEmail").val();
        var idDialCode = $("#idCustomerDialCode").val();
        var szPhone = $("#szCustomerPhoneNumber").val();
        var szAddress = $("#szCustomerAddress1").val();
        var szPostcode = $("#szCustomerPostCode").val();
        var szCity = $("#szCustomerCity").val();
        var idCountry = $("#szCustomerCountry").val();
        
        
        
        if(iShipperConsignee==1)
        { 
            $("#szShipperCompanyName").val(szCompanyName);
            $("#szShipperFirstName").val(szFirstName);
            $("#szShipperLastName").val(szLastName);
            $("#szShipperEmail").val(szEmail);
            $("#idShipperDialCode").val(idDialCode); 
            $("#szShipperPhoneNumber").val(szPhone);
            
            $("#szOriginAddress").val(szAddress);
            $("#szOriginPostcode").val(szPostcode);
            $("#szOriginCity").val(szCity);
            
            
            $(".shipper-fields").addClass('bg-grey');
            $(".shipper-fields").attr('readonly','readonly');
            $(".shipper-fields").removeClass('red_border');
            
            $(".consignee-fields").removeClass('bg-grey');
            $(".consignee-fields").removeAttr('readonly');
            
            if(iPendingTrayPageFlag)
            {
                $("#idOriginCountry").html('<option value='+idCountry+' >'+countriesArr[idCountry]+'</option>');
                $("#idOriginCountry").val(idCountry); 

                var idDestinationCountryValue=$("#idDestinationCountry").val();

                $("#idDestinationCountry").html('');
                $("#idDestinationCountry").append($("<option></option>")
                             .attr("value", '').text('Select')); 
                $.each(countriesArr, function(key,szNameCountry) {
                    if(parseInt(key)>0 && jQuery.trim(szNameCountry)!='')
                    {
                        $("#idDestinationCountry").append($("<option></option>")
                             .attr("value", key).text(szNameCountry));     
                    }
                });            
                $("#idDestinationCountry").val(idDestinationCountryValue);
            }
            else
            {
                $("#idOriginCountry").val(idCountry); 
            }
        }
        else
        {  
            $(".shipper-fields").removeClass('bg-grey');
            $(".shipper-fields").removeAttr('readonly');
            
            $(".consignee-fields").addClass('bg-grey');
            $(".consignee-fields").attr('readonly','readonly');
            $(".consignee-fields").removeClass('red_border');
            
            $("#szConsigneeCompanyName").val(szCompanyName);
            $("#szConsigneeFirstName").val(szFirstName);
            $("#szConsigneeLastName").val(szLastName);
            $("#szConsigneeEmail").val(szEmail);
            $("#idConsigneeDialCode").val(idDialCode); 
            $("#szConsigneePhoneNumber").val(szPhone);
            
            $("#szDestinationAddress").val(szAddress);
            $("#szDestinationPostcode").val(szPostcode);
            $("#szDestinationCity").val(szCity);
            //$("#idDestinationCountry").val(idCountry); 
            if(iPendingTrayPageFlag)
            {
                $("#idDestinationCountry").html('<option value='+idCountry+' >'+countriesArr[idCountry]+'</option>');
                $("#idDestinationCountry").val(idCountry);



                var idOriginCountryValue=$("#idOriginCountry").val();


                $("#idOriginCountry").html('');
                $("#idOriginCountry").append($("<option></option>")
                             .attr("value", '').text('Select'));
                $.each(countriesArr, function(key,szNameCountry) {
                    if(parseInt(key)>0  && jQuery.trim(szNameCountry)!='')
                    {
                        $("#idOriginCountry").append($("<option></option>")
                             .attr("value", key).text(szNameCountry));

                    }
                });

                $("#idOriginCountry").val(idOriginCountryValue);
            }
            else
            {
                $("#idDestinationCountry").val(idCountry);
            }
            
        }    
    } 
    else
    { 
        $(".shipper-fields").removeClass('bg-grey');
        $(".consignee-fields").removeClass('bg-grey');
        
        //$(".consignee-fields").css('background-color','');        
        //$(".shipper-fields").css('background-color','');
        
        $(".consignee-fields").removeAttr('readonly');
        $(".shipper-fields").removeAttr('readonly');
        
        if(iPendingTrayPageFlag)
        {
            var idDestinationCountryValue=$("#idDestinationCountry").val();
            var idOriginCountryValue=$("#idOriginCountry").val();


            $("#idOriginCountry").html('');
            $("#idDestinationCountry").html('');

            $("#idOriginCountry").append($("<option></option>")
                             .attr("value", '').text('Select'));
            $("#idDestinationCountry").append($("<option></option>")
                             .attr("value", '').text('Select'));         
            $.each(countriesArr, function(key,szNameCountry) {
                if(parseInt(key)>0  && jQuery.trim(szNameCountry)!='')
                {
                    $("#idOriginCountry").append($("<option></option>")
                         .attr("value", key).text(szNameCountry));

                    $("#idDestinationCountry").append($("<option></option>")
                         .attr("value", key).text(szNameCountry));     
                }
            });

            $("#idOriginCountry").val(idOriginCountryValue);
            $("#idDestinationCountry").val(idDestinationCountryValue);
        }
    }  
    if(quick_quote_flag==1)
    {
        //TO DO
    }
    else
    {
        update_terms_down();
    } 
}

function submit_snooze_form(operation_type)
{  
    $("#pending_task_reminder_email_preview_container").attr('style','display:none;'); 
     
    var formId = 'rfq-reminder-notes-form';
    var serialized_form_data = $('#'+formId).serialize();
     
    var content = NiceEditorInstance.instanceById('szReminderEmailBody').getContent();
    var description = encode_string_msg_str(content);
    serialized_form_data = serialized_form_data + "&addSnoozeAry[szReminderEmailBody]="+description;
     
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?operation_type="+operation_type,serialized_form_data,function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]);
        if(check=='ERROR')
        {  
            $('#pending_task_tray_container').html(res_ary[1]); 
        }
        if(check=='SUCCESS')
        {  
            //$("#pending_task_listing_container").html(res_ary[1]);	
            updatePendingTrayListing(res_ary[1]);
            $("#pending_task_listing_container").attr('style','display:block;'); 
            
            if(operation_type=='CLOSE_TASK' || operation_type=='SEND_CLOSE_TASK' || operation_type=='SEND_SNOOZE' || operation_type=='SNOOZE')
            {
                $("#pending_task_overview_main_container").html("");
                $("#pending_task_overview_main_container").attr('style','display:none;');  

                $("#pending_task_tray_main_container").html("");
                $("#pending_task_tray_main_container").attr('style','display:none;');  
                $("#idLastClikedBookingFile").val("");
            } 
            else
            { 
                $('#pending_task_tray_container').html(res_ary[2]); 
            }
        }
        else if(check=='SUCCESS_PREVIEW')
        {
            $('#pending_task_reminder_email_preview_container').html(res_ary[1]); 
            $("#pending_task_reminder_email_preview_container").attr('style','display:block;');  
        }
    });
}

function submit_rfq_note_form()
{  
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",$("#rfq-reminder-notes-form").serialize(),function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='ERROR')
        {  
            $('#rfq-reminder-notes-container').html(res_ary[1]);
        }
        if(check=='SUCCESS')
        { 
            $('#rfq-reminder-notes-container').html(res_ary[1]);
        }
    });
} 


function selectCourierProductGroup(id,idCourierProvider,textEdit,textSave,textcancel)
{
	var idDiv="courier_provider_"+id;
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,idCourierProvider:idCourierProvider,mode:'SELECT_COURIER_PRODUCT_GROUP_DATA'},function(result){
        var result_ary = result.split("||||"); 
        
        	//alert(result_ary);
  
        $("#courier_product_group").html(result_ary[0]);
        $("#cargo_limitation").html(result_ary[1]);
        	
        $("#loader").attr('style','display:none');
        $('#'+idDiv).focus();
        $("#add_edit_group_limitation_data").html('<span style="min-width:50px;">'+textEdit+'</span>');
        $("#add_edit_group_limitation_data").unbind("click");
        $("#add_edit_group_limitation_data").click(function(){editCourierProductGroup(id,idCourierProvider,textSave,textcancel);});
        $("#add_edit_group_limitation_data").attr("style",'opacity:1;');
        
        
        $("#detete_group_limitation_data").unbind("click");
        $("#detete_group_limitation_data").click(function(){deleteCourierProductGroup(id,idCourierProvider);});
        $("#detete_group_limitation_data").attr("style",'opacity:1;');
	});
}


function checkProductGroupData()
{ 
    var formId = 'courierProductGroupForm';
    var error_count = 1;
    if(isFormInputElementEmpty(formId,'szName')) 
    {		 
        console.log("Name Empty");
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'idCourierProvider')) 
    {		 
        console.log("Courier Provider Id");
        error_count++;
    }
    else
    {
        if(parseInt($("#"+formId+" #idCourierProvider").val())==0)
        {
           console.log("Courier Provider Id");
            error_count++;    
        }
    }    
    var id = $("#courierProductGroupForm #id").val(); 
    if(id==0) 
    {
         $("#add_edit_group_limitation_data").html("<span>Add</span>");
         
         $("#detete_group_limitation_data").unbind("click");
         $("#detete_group_limitation_data").attr("style",'opacity:0.4;');
         
    }
    else
    {
          $("#add_edit_group_limitation_data").html("<span>Save</span>");
    }
       
    if(error_count==1)
    {
        $("#add_edit_group_limitation_data").unbind("click");
        $("#add_edit_group_limitation_data").click(function(){addEditCourierProductGroup();});
        $("#add_edit_group_limitation_data").attr("style",'opacity:1');
    }
    else
    {
        $("#add_edit_group_limitation_data").unbind("click");
        $("#add_edit_group_limitation_data").attr("style",'opacity:0.4');
    }   
}

function editCourierProductGroup(id,idCourierProvider,textSave,textcancel)
{
	// $("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{id:id,idCourierProvider:idCourierProvider,mode:'EDIT_COURIER_GROUP_DATA_FROM'},function(result){
	
	$("#courier_product_group_addedit_data").html(result);	
	
	$("#add_edit_group_limitation_data").html('<span style="min-width:50px;">'+textSave+'</span>');
    $("#add_edit_group_limitation_data").unbind("click");
    $("#add_edit_group_limitation_data").click(function(){addEditCourierProductGroup();});
    $("#add_edit_group_limitation_data").attr("style",'opacity:1;');
    
     $("#detete_group_limitation_data").html('<span style="min-width:50px;">'+textcancel+'</span>');	
     $("#detete_group_limitation_data").unbind("click");
     $("#detete_group_limitation_data").click(function(){cancelCourierProductGroupEdit(idCourierProvider);});
     $("#detete_group_limitation_data").attr("style",'opacity:1;');
      $("#loader").attr('style','display:none');
     });
}

function cancelCourierProductGroupEdit(idCourierProvider)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{idCourierProvider:idCourierProvider,mode:'SELECT_COURIER_PRODUCT_GROUP_DATA'},function(result){
       var result_ary = result.split("||||");  
        $("#courier_product_group").html(result_ary[0]);
        $("#cargo_limitation").html(result_ary[1]);    
    }); 
}

function addEditCourierProductGroup()
{
	var value = $('#courierProductGroupForm').serialize();
	var newValue=value+'&mode=ADD_EDIT_COURIER_PRODUCT_GROUP_DATA';
	//$("#loader").attr('style','display:block');
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
	var result_ary = result.split("||||"); 
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#courier_product_group").html(result_ary[1]);
                        $("#cargo_limitation").html(result_ary[2]);
		}
		else
		{
			$("#courier_product_group_addedit_data").html(result_ary[1]);
		}
		$("#loader").attr('style','display:none');
		
	});
}

function deleteCourierProductGroup(id,idCourierProvider)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{idCourierProvider:idCourierProvider,mode:'DELETE_COURIER_PRODUCT_GROUP_DATA',id:id},function(result){
     $("#courier_product_group").html(result);    
    });
}

function update_dial_code(type,country_id)
{
    if(type=='SHIPPER')
    {
        var idShipperDialCode = $("#idShipperDialCode").val();
        if(idShipperDialCode==40 || idShipperDialCode==89 || idShipperDialCode==236 || idShipperDialCode==179) //+1
        {
            $('#idShipperDialCode option[value='+country_id+']').attr('selected','selected');
        }
    }
    else
    {
        var idConsigneeDialCode = $("#idConsigneeDialCode").val();
        if(idConsigneeDialCode==40 || idConsigneeDialCode==89 || idConsigneeDialCode==236 || idConsigneeDialCode==179) //+1
        {
            $('#idConsigneeDialCode option[value='+country_id+']').attr('selected','selected');
        }
    }
}

function checkCourierExludedTradesData()
{ 
    var error = 1; 
    var formId = 'courierExcludeTradesForm';
    if(isFormInputElementEmpty(formId,'idCourierProvider')) 
    { 
        console.log('provider');
        error++;  
    } 
    if(isFormInputElementEmpty(formId,'idForwarder')) 
    {		 
        console.log('forwarder');
        error++; 
    }  
    if(isFormInputElementEmpty(formId,'idOriginCountry')) 
    {		 
        console.log('country');
        error++; 
    } 
    if(isFormInputElementEmpty(formId,'idDestinationCountry')) 
    {		 
        console.log('dest country');
        error++; 
    } 
    if(isFormInputElementEmpty(formId,'iCustomerType'))
    {		 
        console.log('customer type');
        error++; 
    } 
    
    if(error==1)
    {
        //var iReturnval = check_duplicate_values(); 
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php?mode=CHECK_DUPLICATE_EXCLUDED_TRADES",$('#courierExcludeTradesForm').serialize(),function(result){
            var result_ary = result.split("||||"); 
            if(jQuery.trim(result_ary[0])=='SUCCESS')
            {
                $("#add_edit_excluded_trades_data").unbind("click");
                $("#add_edit_excluded_trades_data").attr('style','opacity:0.4');
            }
            else
            {
                $("#add_edit_excluded_trades_data").unbind("click");
                $("#add_edit_excluded_trades_data").click(function(){
                    submit_excluded_trades_form();
                });
                $("#add_edit_excluded_trades_data").attr('style','opacity:1');
            } 
        }); 
    } 
    
    var idExludedTrades = $("#idExludedTrades").val();
    idExludedTrades = parseInt(idExludedTrades);
    if(idExludedTrades>0)
    {
        $("#add_edit_excluded_trades_data_span").html('Save');
    }
    else
    {
        $("#add_edit_excluded_trades_data_span").html('Add');
    }
}

function submit_excluded_trades_form()
{   
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",$('#courierExcludeTradesForm').serialize(),function(result){
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#courier_excluded_trade_list").html(result_ary[1]); 
        }
        else
        {
            $("#courier_excluded_trades_addedit_data").html(result_ary[1]);
        }
        $("#loader").attr('style','display:none'); 
    });
}

function show_edit_excluded_trades_list(excluded_trade_id)
{ 
    var rowCount = $('#couier_excluded_trades_list_table tr').length; 
    var div_id = "courier_excluded_trades_"+excluded_trade_id ;
    
    if(rowCount>0)
    {  
        reset_courier_excluded_form();
        
       // $(".excluded-service-tr").attr('style','background:#ffffff;cursor:pointer;');
        //$("#"+div_id).attr('style','background:#DBDBDB;cursor:pointer;'); 
        /*
        $("#add_edit_excluded_trades_data").unbind("click");
        $("#add_edit_excluded_trades_data").click(function(){
            update_courier_excluded_trades(excluded_trade_id,'EDIT_EXCLUDED_TRADES');
        });
        $("#add_edit_excluded_trades_data").attr('style','opacity:1');
        $("#add_edit_excluded_trades_data_span").html('Edit');
        
        $("#detete_excluded_trades_data").unbind("click");
        $("#detete_excluded_trades_data").click(function(){
            update_courier_excluded_trades(excluded_trade_id,'DELETE_EXCLUDED_TRADES_CONFIRM');
        });
        $("#detete_excluded_trades_data").attr('style','opacity:1');
        $("#detete_excluded_trades_data_span").html('Delete');
        */
    }  
} 

function reset_courier_excluded_form()
{
    $('#idCourierProvider option[value=""]').attr('selected','selected');
    $('#idForwarder option[value=""]').attr('selected','selected');
    $('#idOriginCountry option[value=""]').attr('selected','selected');
    $('#idDestinationCountry option[value=""]').attr('selected','selected');
    $('#iCustomerType option[value=""]').attr('selected','selected');
}

function update_courier_excluded_trades(excluded_trade_id,mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{excluded_trade_id:excluded_trade_id,mode:mode},function(result){
        var resultAry = result.split("||||");
        if(jQuery.trim(resultAry[0])=='SUCCESS_EDIT')
        { 
           $("#courier_excluded_trades_addedit_data").html(resultAry[1]); 
           $("#add_edit_excluded_trades_data_span").html('Save');
           checkCourierExludedTradesData();

           $("#detete_excluded_trades_data").unbind("click");
           $("#detete_excluded_trades_data").click(function(){
               update_courier_excluded_trades(excluded_trade_id,'CANCEL_EXCLUDED_TRADES');
           });

           $("#detete_excluded_trades_data").attr('style','opacity:1');
           $("#detete_excluded_trades_data_span").html('Cancel');
        }
        else if(jQuery.trim(resultAry[0])=='SUCCESS_LIST')
        {
            $("#contactPopup").attr('style','display:none;');
            $("#courier_excluded_trade_list").html(resultAry[1]); 
        }
        else if(jQuery.trim(resultAry[0])=='SUCCESS_POPUP')
        {
            $("#contactPopup").html(resultAry[1]);  
            $("#contactPopup").attr('style','display:block;');
        }
        else if(jQuery.trim(resultAry[0])=='SUCCESS_DROP_DOWN')
        {
            $("#excluded_trade_forwarder_container").html(resultAry[1]);   
        }
    });
} 
var windows_close = function(){  alert("closing");  };
function open_test_search(file_id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{file_id:file_id,mode:"OPEN_PRIVATE_BOOKING_WINDOW"},function(result){
        var resultAry = result.split("||||");
        if(jQuery.trim(resultAry[0])=='SUCCESS')
        {  
            var testwindow = window.open(resultAry[1], "mywindow", "location=1,status=1,scrollbars=1,width=700,incognito=1"); 
            //testwindow.onbeforeunload = function(){ reset_browser_session();} 
             //window.onclose = windows_close;
             //refreshPopupWindow(testwindow);
        }
    });
} 


function refreshPopupWindow(popupWindow) {
    //var popupWindow = null;
  if (popupWindow && !popupWindow.closed) {
    // popupWindow is open, refresh it
    //popupWindow.location.reload(true);
    alert("in if");
  } else {
    // Open a new popup window
    alert("in if");
    popupWindow = window.open("popup.html","dataWindow");
  }
}
 
function handleBrowserCloseButton(event) 
{ 
   if (($(window).width() - window.event.clientX) < 35 && window.event.clientY < 0) 
    {
      //Call method by Ajax call
      alert('Browser close button clicked');    
    } 
} 
function reset_browser_session()
{
    alert("I am going to die ...... ");
}

function enable_reminder_tab_buttons()
{ 
    var formId = 'rfq-reminder-notes-form';
    var save_count = 1;
    var snooze_count = 1;
    var send_count = 1;
    var send_snooze_count = 1;
    
    if(isFormInputElementEmpty(formId,'szReminderNotes')) 
    {		  
        save_count++;
    }
      
    if(isFormInputElementEmpty(formId,'dtReminderDate')) 
    {		  
        snooze_count++;
    }
    /*if(isFormInputElementEmpty(formId,'idTemplate')) 
    {		 
        console.log("Template Empty");
        save_count++;
    }*/
    if(isFormInputElementEmpty(formId,'szCustomerEmail')) 
    {		  
        send_count++;
        send_snooze_count++;
    }
    
    if(isFormInputElementEmpty(formId,'szReminderTime')) 
    {		  
        snooze_count++;
    }
    
    if(isFormInputElementEmpty(formId,'szReminderSubject')) 
    {		  
        send_count++;
        send_snooze_count++;
    }
    var content = NiceEditorInstance.instanceById('szReminderEmailBody').getContent();
    var description = encode_string_msg_str(content);
    if(content=='' || content=='<br>') 
    {		  
        send_count++;
        send_snooze_count++;
    }
    
    if(isFormInputElementEmpty(formId,'dtEmailReminderDate')) 
    {		  
        send_snooze_count++;
    }
    if(isFormInputElementEmpty(formId,'szEmailReminderTime')) 
    {		  
        send_snooze_count++;
    }    
    
    $("#request_reminder_email_save_draft_button").unbind("click");
    $("#request_reminder_email_save_draft_button").click(function(){ saveAsDraftRemindEmailData(formId);});
    $("#request_reminder_email_save_draft_button").attr("style",'opacity:1');
    
    if(save_count==1)
    {
        $("#request_reminder_save_button").unbind("click");
        $("#request_reminder_save_button").click(function(){ submit_snooze_form('SAVE');});
        $("#request_reminder_save_button").attr("style",'opacity:1');
    }
    else
    {
        $("#request_reminder_save_button").unbind("click");
        $("#request_reminder_save_button").attr("style",'opacity:0.4');
    }    
    if(snooze_count==1)
    {
        $("#request_reminder_snooze_button").unbind("click");
        $("#request_reminder_snooze_button").click(function(){ submit_snooze_form('SNOOZE');});
        $("#request_reminder_snooze_button").attr("style",'opacity:1');
    }
    else
    {
        $("#request_reminder_snooze_button").unbind("click");
        $("#request_reminder_snooze_button").attr("style",'opacity:0.4');
    } 
    
    if(send_snooze_count==1)
    {
        $("#request_reminder_email_snooze_button").unbind("click");
        $("#request_reminder_email_snooze_button").click(function(){ submit_snooze_form('SEND_SNOOZE');});
        $("#request_reminder_email_snooze_button").attr("style",'opacity:1');
    }
    else
    {
        $("#request_reminder_email_snooze_button").unbind("click");
        $("#request_reminder_email_snooze_button").attr("style",'opacity:0.4');
    }  
    
    var iLabelStatus = $("#iLabelStatus").attr('value');
    var szTaskStatusRemind = $("#szTaskStatusRemind").attr('value');
    
    if(send_count==1)
    {
        $("#request_reminder_email_send_button").unbind("click");
        $("#request_reminder_email_send_button").click(function(){ submit_snooze_form('SEND');});
        $("#request_reminder_email_send_button").attr("style",'opacity:1');
        
        if(parseInt(iLabelStatus)==1 && szTaskStatusRemind=='T140')
        {
             $("#request_reminder_email_send_close_task_button").unbind("click");
            $("#request_reminder_email_send_close_task_button").attr("style",'opacity:0.4');
        }
        else
        {
            $("#request_reminder_email_send_close_task_button").unbind("click");
            $("#request_reminder_email_send_close_task_button").click(function(){ submit_snooze_form('SEND_CLOSE_TASK');});
            $("#request_reminder_email_send_close_task_button").attr("style",'opacity:1');
        }
        
//        $("#request_reminder_email_preview_button").unbind("click");
//        $("#request_reminder_email_preview_button").click(function(){ submit_snooze_form('PREVIEW_REMINDER_EMAIL');});
//        $("#request_reminder_email_preview_button").attr("style",'opacity:1');
        
    }
    else
    {
        $("#request_reminder_email_send_button").unbind("click");
        $("#request_reminder_email_send_button").attr("style",'opacity:0.4');
        
        $("#request_reminder_email_send_close_task_button").unbind("click");
        $("#request_reminder_email_send_close_task_button").attr("style",'opacity:0.4');
        
//        $("#request_reminder_email_preview_button").unbind("click");
//        $("#request_reminder_email_preview_button").attr("style",'opacity:0.4'); 
    }
     
}

function toggleReminderContent(div_1,div_2)
{
    $("#"+div_2).attr('style','display:none;');
    $("#"+div_1).attr('style','display:block;');
}
function toggle_text_field(cb_id,div_id)
{
    var cb_flag = $("#"+cb_id).prop("checked"); 
    if(cb_flag)
    {
        $("#"+div_id).attr('disabled','disabled'); 
        $("#"+div_id).val("");
    }
    else
    {
        $("#"+div_id).removeAttr('disabled');
    }
}

function display_customer_log_pagination(page_number,file_id)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'DISPLAY_CUSTOMER_LOG_PAGINATION',page:page_number,file_id:file_id},function(result){
	
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#contactPopup").attr('style','display:none;'); 
           $("#pending_task_tray_container").html(result_ary[1]); 
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
}

function reopen_booking_file(file_id,mode)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,file_id:file_id},function(result){ 
        
        result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            $("#contactPopup").attr('style','display:none');
            //$("#pending_task_listing_container").html(result_ary[1]);	
            updatePendingTrayListing(result_ary[1]);
            $("#pending_task_listing_container").attr('style','display:block;');

            $("#pending_task_overview_main_container").html(result_ary[2]);		
            $("#pending_task_overview_main_container").attr('style','display:block;'); 

            $("#pending_task_tray_main_container").html(result_ary[3]);		
            $("#pending_task_tray_main_container").attr('style','display:block;'); 
           
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
}

function pay_forwarder_invoice(booking_id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'DISPLAY_PAY_FORWARDER_INVOICE_FORM',booking_id:booking_id},function(result){ 
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#contactPopup").html(result_ary[1]);		
            $("#contactPopup").attr('style','display:block');
            
            $('body').addClass('popup-body-scroll');
            $("#contactPopup").focus();
            var openDiv = 'task-popup-id-recent'; 
            $(document).click(function(e) {
                if (!$(e.target).closest('#'+openDiv).length) { 
                    var disp = $("#ui-datepicker-div").css('display');
                    if(disp=='block')
                    {
                        console.log("Date picker is active");
                    }
                    else
                    {
                        $("#contactPopup").attr('style','display:none;'); 
                    } 
                    $('body').removeClass('popup-body-scroll'); 
                }
            }); 
        } 
    }); 
}

function enableConfirmButton()
{
    var error_count=1; 
    var formId = 'payForwarderInvoiceForm';
    if(isFormInputElementEmpty(formId,'idForwarderQuoteCurrency')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'fForwarderTotalQuotePrice')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'fTotalVatForwarderCurrency')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'dtMarkupPaid')) 
    {		 
        error_count++;
    }
    if(error_count==1)
    {
        $("#confirm_referral_payment_admin").unbind("click");
        $("#confirm_referral_payment_admin").click(function(){ confirm_forwarder_invoice_payment();});
        $("#confirm_referral_payment_admin").attr("style",'opacity:1');
    }
    else
    {
        $("#confirm_referral_payment_admin").unbind("click");
        $("#confirm_referral_payment_admin").attr("style",'opacity:0.4');
    } 
}

function confirm_forwarder_invoice_payment()
{
     var page = $("#page").attr('value');
                var limit = $("#limit").attr('value');
                var value=$("#payForwarderInvoiceForm").serialize();
                var newValue=value+"&page="+page+"&limit="+limit;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",newValue,function(result){  
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#checkDetails").html(result_ary[1]);		
            $("#checkDetails").attr('style','display:block'); 
            $("#contactPopup").attr('style','display:none;'); 
        } 
        else if(jQuery.trim(result_ary[0])=='ERROR')
        {
            $("#contactPopup").html(result_ary[1]);		
            $("#contactPopup").attr('style','display:block'); 
        }
    }); 
}

function open_pending_google_map(szAddressType,szAddressString)
{  
    var szAddressLine = '';
    if(szAddressType=='FROM')
    {
        szAddressLine = $("#szOverviewFromField").val();
    }
    else if(szAddressType=='TO')
    {
        szAddressLine = $("#szOverviewToField").val();
    }
    else
    {
        szAddressLine = szAddressString;
    }
    $("#szAddressLine_hidden").attr('value',szAddressLine);  
    $('#google_map_hidden_form').submit(); 
    $("#popup_container_google_map").attr('style','display:block;'); 
}

function showProfilePagination(page,flag)
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{page:page,flag:flag},function(result){
        $("#content_body").html(result);
    });
}

function showBookingQuotePagination(page,from_page)
{
    $.get(__JS_ONLY_SITE_BASE__+"/ajax_bookingQuotes.php",{mode:'BACK_TO_MANUAL_QUEOTS_LISTING',page:page,from_page:from_page},function(result){
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#booking_quotes_list_container").html(result_ary[1]);
        }
    });
}

function validate_aid_word_form_data()
{  
    var error_count = 1;
    var formId = 'search_aid_words_add_edit_form';
    
    if(isFormInputElementEmpty(formId,'szOriginStr')) 
    {		
       $("#szOriginStr").addClass('red_border'); 
       error_count++;
    } 
//    if(isFormInputElementEmpty(formId,'szDestinationStr')) 
//    {		
//       $("#szDestinationStr").addClass('red_border'); 
//       error_count++;
//    } 
    if(isFormInputElementEmpty(formId,'szOriginKey')) 
    {		
       $("#szOriginKey").addClass('red_border'); 
       error_count++;
    } 
//    if(isFormInputElementEmpty(formId,'szDestinationKey')) 
//    {		
//       $("#szDestinationKey").addClass('red_border'); 
//       error_count++;
//    } 
    
    if(error_count==1)
    {
        add_update_search_aid_words();  
    }  
}

function add_update_search_aid_words()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",$("#search_aid_words_add_edit_form").serialize(),function(result){ 
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#add_edit_landing_page_search_aid_words_content").html(result_ary[1]);		
           $("#add_edit_landing_page_search_aid_words_content").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#add_edit_landing_page_search_aid_words_content").attr('style','display:block;');
           $("#add_edit_landing_page_search_aid_words_content").html(result_ary[1]);
           
           $("#manage_landing_page_search_aid_words_content").attr('style','display:block;');
           $("#manage_landing_page_search_aid_words_content").html(result_ary[2]);
        }
    });
}
function update_aid_words(aid_word_id,mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_editLandingPage.php",{aid_word_id:aid_word_id,mode:mode},function(result){ 
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	 
            if(mode=='EDIT_SEARCH_AID_WORDS')
            { 
                $("#add_edit_landing_page_search_aid_words_content").attr('style','display:block;');
                $("#add_edit_landing_page_search_aid_words_content").html(result_ary[1]); 
            }
            else if(mode=='DELETE_SEARCH_AID_WORDS_CONFIRM')
            { 
                $("#add_edit_landing_page_search_aid_words_content").attr('style','display:block;');
                $("#add_edit_landing_page_search_aid_words_content").html(result_ary[1]);
                
                $("#manage_landing_page_search_aid_words_content").attr('style','display:block;');
                $("#manage_landing_page_search_aid_words_content").html(result_ary[2]);
                
                $("#contactPopup").attr('style','display:none;');
            }
            else
            {
                $("#contactPopup").attr('style','display:block;');
                $("#contactPopup").html(result_ary[1]);  
            }
        }
    });
}  
function disabled_non_editable_fields_for_paid_booking(iShipperConsignee)
{
    var notEditableDropdownssAry = new Array();
    notEditableDropdownssAry['0'] = 'idCustomerCurrency';
    //notEditableDropdownssAry['1'] = 'idCustomerOwner';
    notEditableDropdownssAry['2'] = 'idTransportMode';
    notEditableDropdownssAry['3'] = 'idServiceTerms';
    notEditableDropdownssAry['4'] = 'iInsuranceIncluded';
    notEditableDropdownssAry['5'] = 'idInsuranceCurrency';
    notEditableDropdownssAry['6'] = 'idOriginCountry';
    notEditableDropdownssAry['7'] = 'idDestinationCountry';
    notEditableDropdownssAry['8'] = 'szCustomerCountry';
    notEditableDropdownssAry['9'] = 'szCargoType';
     
    var notEditableInputsAry = new Array(); 
    notEditableInputsAry['0'] = 'szHandoverCity';
    notEditableInputsAry['1'] = 'fCargoVolume';
    notEditableInputsAry['2'] = 'fCargoWeight';
    notEditableInputsAry['3'] = 'isMoving'; 
    notEditableInputsAry['4'] = 'fTotalInsuranceCostForBookingCustomerCurrency';  
    
    $.each( notEditableInputsAry, function( index, input_fields ){
        $("#"+input_fields).css('background-color','#D3D3D3');
        $("#"+input_fields).attr('readonly','readonly');
        $("#"+input_fields).attr('disabled','disabled');
    }); 

    $.each( notEditableDropdownssAry, function( index, input_fields ){
        $("#"+input_fields).css('background-color','#D3D3D3');
        $("#"+input_fields).attr('disabled','disabled');
        $("#"+input_fields).focus(function() {
            this.blur(); 
        });
    }); 
    $("#isMoving").attr('disabled','disabled');
    $("#idServiceTerms").attr('disabled','disabled'); 
    
    if(iShipperConsignee==1)
    {
        $("#iShipperConsignee_2").attr('disabled','disabled');
    }
    else if(iShipperConsignee==2)
    {
        $("#iShipperConsignee_1").attr('disabled','disabled');
    }
    else
    {
        $("#iShipperConsignee_1").attr('disabled','disabled');
        $("#iShipperConsignee_2").attr('disabled','disabled');
        $("#iShipperConsignee_3").attr('disabled','disabled'); 
    }  
}

function show_more_files_search(formId,crm_link,email_id)
{ 
    var idCrmLogs = email_id;
    
    console.log("b4 crm_flag: "+crm_link);
    console.log("b4 crm mail: "+idCrmLogs);
        
    if(crm_link==1)
    {
        $("#pending_task_show_more_files_"+idCrmLogs).addClass('tyni-loader');  
        var page = $("#iPageCounter_"+idCrmLogs).val();
    }
    else
    {
        $("#pending_task_show_more_files").addClass('tyni-loader');  
        var page = $("#iPageCounter").val();
    } 
    
    page = parseInt(page);
    page = page+1;
    
    if(formId=='')
    {
        formId = 'pending_task_serch_form';
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?mode=SHOW_MORE_FILES_SEARCH_POPUP&page="+page,$("#"+formId).serialize(),function(result){
	
        var result_ary = result.split("||||"); 
        console.log("crm_flag: "+crm_link);
        console.log("crm mail: "+idCrmLogs);
        
        if(crm_link==1)
        {
            if(jQuery.trim(result_ary[0])=='SUCCESS')
            { 	
               $("#pending_task_tray_popup_"+idCrmLogs).append(result_ary[1]);	 
            } 
            $("#iPageCounter_"+idCrmLogs).val(page);
            $("#pending_task_show_more_files_"+idCrmLogs).removeClass('tyni-loader'); 
        }
        else
        {
            if(jQuery.trim(result_ary[0])=='SUCCESS')
            { 	
               $("#pending_task_tray_popup").append(result_ary[1]);	 
            } 
            $("#iPageCounter").val(page);
            $("#pending_task_show_more_files").removeClass('tyni-loader'); 
        } 
    });
}

function check_insuarnce_availability(booking_id)
{
    //$("#loader").attr('style','display:block');
    var idOriginCountry = $("#idOriginCountry").val();
    var idDestinationCountry = $("#idDestinationCountry").val();
    var idTransportMode = $("#idTransportMode").val();
    var iInsuranceIncluded = $("#iInsuranceIncluded").val();  
    var szCargoType = $("#szCargoType").val();
    
    var isMoving = 0;
    if(szCargoType=='__PERSONAL_EFFECTS__' || szCargoType=='__ART__')
    {
        isMoving = 1;
    } 
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"CHECK_INSURANCE_AVAILABILITY",booking_id:booking_id,origin_country:idOriginCountry,destination_country:idDestinationCountry,transport_mode:idTransportMode,moving:isMoving,insurance_included:iInsuranceIncluded,cargo_type:szCargoType},function(result){ 
        
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	 
            $("#insurance_dropdown_container").html(result_ary[1]);		 
        }
        else if(jQuery.trim(result_ary[0])=='NOT_AVAILABLE')
        {
            $("#insurance_dropdown_container").html(result_ary[1]);	
        }
        $("#loader").attr('style','display:none');
    });
}

function changeRemindEmailTemplateBody(mode,file_id,idTemplate,iLanguage,task_type,from_page,email_id,operation_type)
{
    var type = 'REMIND';
    
    if(email_id>0)
    {
        if($.trim(task_type) == "CHANGE_TEMPLATE")
        {
            iLanguage = $("#iRemindTemplateLanguage_"+email_id).val();
        }
        else if($.trim(task_type) == "CHANGE_LANGUAGE")
        {
            idTemplate = $("#idTemplate_"+email_id).val();
        }
    }
    else
    {
        if($.trim(task_type) == "CHANGE_TEMPLATE")
        {
            iLanguage = $("#iRemindTemplateLanguage").val();
        }
        else if($.trim(task_type) == "CHANGE_LANGUAGE")
        {
            idTemplate = $("#idTemplate").val();
        }
    }
    
    
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,file_id:file_id,idTemplate:idTemplate,iLanguage:iLanguage,from_page:from_page,email_id:email_id,operation_type:operation_type},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#remindEmailPopup").attr('style','display:none;');
//           $(".task-tray-header").removeClass('active-th');
//           $("#task_tray_header_id_"+type).addClass('active-th');
            if(email_id>0)
            {
                $("#send_reminder_email_container_"+email_id).html(result_ary[1]); 
            }
            else
            {
                $("#send_reminder_email_container").html(result_ary[1]); 
            }
        }
        $("#loader").attr('style','display:none');
    });
}

function add_edit_RemindEmailTemplate(type,file_id,operationType,pop_language,email_id,from_page,action)
{
    var mode = 'ADD_EDIT_REMINDER_EMAIL_TEMPLATE';
    
    var idTemplate = '';
    var iLanguage = 1;
    
    if(email_id>0)
    {
        iLanguage = $("#iRemindTemplateLanguage_"+email_id).val();
    }
    else
    {
        iLanguage = $("#iRemindTemplateLanguage").val();
    } 
    
    if($.trim(operationType) == "edit_template")
    {
        var idCrmEmail = parseInt(email_id);
        if(idCrmEmail>0)
        {
            idTemplate = $("#idTemplate_"+idCrmEmail).val();
        }
        else
        {
            idTemplate = $("#idTemplate").val();
        }  
        if($.trim(pop_language) != '')
        {
            iLanguage = pop_language;
        }
        else
        {
            if(idCrmEmail>0)
            {
                iLanguage = $("#iRemindTemplateLanguage_"+idCrmEmail).val();
            }
            else
            {
                iLanguage = $("#iRemindTemplateLanguage").val();
            } 
        } 
    }
    
    $("#loader").attr('style','display:block;z-index:999999;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,type:type,file_id:file_id,email_id:email_id,idTemplate:idTemplate,iLanguage:iLanguage,from_page:from_page,action:action},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {
           $("#remindEmailPopup").html(result_ary[1]);		
           $("#remindEmailPopup").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
}
function saveRemindEmailTemplateDetail(mode,type,file_id,idTemplate,iLanguage,closeFlag)
{
    $("#loader").attr('style','display:block;z-index:999999;');
    
    var value=$('#addEditRemindEmailTemplateForm').serialize();
    var iLanguage=$("#iRemindEmailLanguage").attr('value');
    
    $("#contactPopup").html('');
    $("#contactPopup").attr('style','display:none');
    
    var content = NiceEditorInstance_popup.instanceById('szReminderEmailBodyPopup').getContent();
    var description = encode_string_msg_str(content); 
    
    var newValue=value+"&mode="+mode+"&type="+type+"&file_id="+file_id+"&idTemplate="+idTemplate+"&iLanguage="+iLanguage+"&closeFlag="+closeFlag + "&addEditRemindEmailTemplate[szReminderEmailBody]="+description; 
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){
	
        var result_ary = result.split("||||"); 
        
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#remindEmailPopup").html(result_ary[1]);		
           $("#remindEmailPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#remindEmailPopup").attr('style','display:none;');
           $(".task-tray-header").removeClass('active-th');
           $("#task_tray_header_id_"+type).addClass('active-th');
           $("#pending_task_tray_container").html(result_ary[1]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_CRM')
        {
           var crm_mail_id = result_ary[1];
           $("#remindEmailPopup").attr('style','display:none;');  
           $("#crm_buttons_operations_main_container_"+crm_mail_id).html(result_ary[2]);
           $("#crm_buttons_operations_main_container_"+crm_mail_id).attr('style','display:block;');  
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {  
           $("#remindEmailPopup").attr('style','display:block');
           $(".task-tray-header").removeClass('active-th');
           $("#task_tray_header_id_"+type).addClass('active-th');
           $("#pending_task_tray_container").html(result_ary[1]);
           $("#remindEmailPopup").html(result_ary[2]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_CRM_POPUP')
        {   
           var crm_mail_id = result_ary[1];
           $("#remindEmailPopup").attr('style','display:block');
           $("#crm_buttons_operations_main_container_"+crm_mail_id).html(result_ary[2]);
           $("#crm_buttons_operations_main_container_"+crm_mail_id).attr('style','display:block;');  
           $("#remindEmailPopup").html(result_ary[3]);
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_OVERWRITE_POPUP')
        {
            $("#remindEmailPopup").attr('style','display:none');
            $("#contactPopup").html(result_ary[1]);
            $("#contactPopup").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
}

function deleteRemindEmailTemplate(type,file_id,idTemplate)
{
    var mode = "DELETE_REMIND_EMAIL_POP_UP";
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,type:type,file_id:file_id,idTemplate:idTemplate},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {
           $("#delete-msg").html(result_ary[1]);		
           $("#delete-msg").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='ERROR_POPUP')
        {
            $("#remindEmailPopup").html(result_ary[1]);		
            $("#remindEmailPopup").attr('style','display:block');
        }
        $("#loader").attr('style','display:none');
    });
}
function confirmDeleteRemindEmailTemplate(type,file_id,idTemplate)
{
    var mode = "CONFIRM_DELETE_REMIND_EMAIL_POP_UP";
    
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:mode,type:type,file_id:file_id,idTemplate:idTemplate},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#delete-msg").html(result_ary[1]);		
           $("#delete-msg").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            $("#remindEmailPopup").attr('style','display:none');
            $(".task-tray-header").removeClass('active-th');
            $("#task_tray_header_id_"+type).addClass('active-th');
            $("#pending_task_tray_container").html(result_ary[1]); 
            $("#delete-msg").attr('style','display:none');
        }
        
        $("#loader").attr('style','display:none');
    });
}

function enable_reminder_template_tab_buttons(mode,type,file_id,idTemplate,iLanguage)
{ 
    var formId = 'addEditRemindEmailTemplateForm';
    var save_count = 1;
     
    var iRemindScreen = $("#iRemindScreen").prop("checked"); 
    var iFileLog = $("#iFileLog").prop("checked"); 
    var iCustomerLog = $("#iCustomerLog").prop("checked"); 
     
    if(iRemindScreen || iFileLog || iCustomerLog)
    {
        //If any of the 3 checkboxes is checked then we enable save buttons
    }
    else
    {
        save_count++;
    }
    if(isFormInputElementEmpty(formId,'szFriendlyName')) 
    {		 
        console.log("Name Empty");
        save_count++;
    }
        
    if(save_count==1)
    {
        $("#request_reminder_email_template_save_button").unbind("click");
        $("#request_reminder_email_template_save_button").click(function(){ saveRemindEmailTemplateDetail(mode,type,file_id,idTemplate,iLanguage,'CLOSE_POPUP_FALSE');});
        $("#request_reminder_email_template_save_button").attr("style",'opacity:1');
        
        $("#request_reminder_email_template_save_close_button").unbind("click");
        $("#request_reminder_email_template_save_close_button").click(function(){ saveRemindEmailTemplateDetail(mode,type,file_id,idTemplate,iLanguage,'CLOSE_POPUP_TRUE');});
        $("#request_reminder_email_template_save_close_button").attr("style",'opacity:1');
        
    }
    else
    {
        $("#request_reminder_email_template_save_button").unbind("click");
        $("#request_reminder_email_template_save_button").attr("style",'opacity:0.4');
        
        $("#request_reminder_email_template_save_close_button").unbind("click");
        $("#request_reminder_email_template_save_close_button").attr("style",'opacity:0.4');
         
    }
     
}
function closeRemindEmailPopUp(option)
{
    if(option==0)
    {
        $('#remindEmailPopup').html('');
    }
}
function createShippingLabel(idBooking)
{ 
    $("#loader").attr('style','display:block'); 
    var idCourierProduct=$("#idCourierProduct").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"CREATE_SHIPPING_LABEL",idBooking:idBooking,idCourierProduct:idCourierProduct},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='ERROR')
        {  
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        } 
        else if(jQuery.trim(result_ary[0])=='SUCCESS_POPUP')
        {
           $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            var uploadedFileName = result_ary[1];
            var szMasterTrackingNumber = result_ary[2];
            
            $("#szMasterTrackingNumber").val(szMasterTrackingNumber);
            upload_label_files(uploadedFileName);
        } 
        $("#loader").attr('style','display:none');
    });
}

function openCargoDetailUpdateForm(idBooking,iSearchMiniVersion,iAlreadyPaidBooking)
{
     $("#loader").attr('style','display:block'); 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"OPEN_PENDING_TRAY_UPDATE_CARGO_POPUP",idBooking:idBooking,iSearchMiniVersion:iSearchMiniVersion,iAlreadyPaidBooking:iAlreadyPaidBooking},function(result){
	
        
        $("#contactPopup").html(result);		
           $("#contactPopup").attr('style','display:block');
           
        $("#loader").attr('style','display:none');
    });
}



function removeCargoLinePendingTray(idCargoLine)
{
    var divValue="add_booking_quote_container_"+idCargoLine;
    var idBooking=$("#idBooking").val();
    var totalCargoLineLeft=$("#totalCargoLineLeft").val();
    console.log("totalCargoLineLeft"+totalCargoLineLeft);
    if(totalCargoLineLeft==1)
    {
         $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"REMOVE_PENDING_TRAY_UPDATE_CARGO_POPUP",idBooking:idBooking},function(result){
	   $("#contactPopup").html(result);		
           $("#contactPopup").attr('style','display:block');
           enableSaveButtonForAddingCargoLine();
        });
    }
    else
    {
        console.log("divValue"+divValue);
        $("#"+divValue).attr('style','display:none');
        $("#"+divValue).html('');
        totalCargoLineLeft=totalCargoLineLeft-1;
        $("#totalCargoLineLeft").attr('value',totalCargoLineLeft);
        enableSaveButtonForAddingCargoLine();
    }
}

function addMoreCargoLinePendingTray(iSearchMiniVersion)
{
    
    var totalCargoLine=$("#totalCargoLine").val();
    var divValue="add_booking_quote_container_"+totalCargoLine;
    console.log("divValue"+divValue);
    var totalCargoLineLeft=$("#totalCargoLineLeft").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{iSearchMiniVersion:iSearchMiniVersion,mode:"ADD_MORE_PENDING_TRAY_UPDATE_CARGO_POPUP",iValue:totalCargoLine},function(result){
	   $("#"+divValue).html(result);	
           var totalCargoLineNew=parseInt(totalCargoLine)+1;
           var totalCargoLineLeftNew=parseInt(totalCargoLineLeft)+1;
           $("#totalCargoLine").attr('value',totalCargoLineNew);
           $("#totalCargoLineLeft").attr('value',totalCargoLineLeftNew);
           var divValueNew="add_booking_quote_container_"+totalCargoLineNew;
           $("#horizontal-scrolling-div-id").append("<tr id='"+divValueNew+"'></tr>");
            $("#save_cargo_line_pending_tray").attr('style','opacity:0.4;');
            $("#save_cargo_line_pending_tray").unbind("click");
        });
}

function enableSaveButtonForAddingCargoLine()
{
    var totalCargoLineLeft=$("#totalCargoLine").val();
    
    var totalCargoLineLeftVal=$("#totalCargoLineLeft").val();
    
    var counter = 1;
    var error_count = 1 ;
    var formId = 'cargo_details_form';
    var commentFlag=true;
    console.log(parseInt(totalCargoLineLeftVal));
    if(parseInt(totalCargoLineLeftVal)==1)
    {
        commentFlag=false;
    }
    for(var i=1;i<=totalCargoLineLeft;++i)
    {
        console.log("idCargoFlagValue"+"#idCargoFlag"+i); 
        var idCargoFlagValue=$("#idCargoFlag"+i).val();
        if(parseInt(idCargoFlagValue)==1)
        {
             console.log("idCargoFlagValue"+idCargoFlagValue);  
            if(isFormInputElementEmpty(formId,'iLength'+i)) 
            {		
                //$("#iLength"+i).addClass('red_border');
                console.log('iLength');
                //error_count++;
            }
            else
            {
                $("#iLength"+i).removeClass('red_border');   
                 commentFlag=true;
            }

            if(isFormInputElementEmpty(formId,'iWidth'+i)) 
            {		
                //$("#iWidth"+i).addClass('red_border');
                console.log('iWidth');
                //error_count++;
            }
            else
            {
                $("#iWidth"+i).removeClass('red_border');   
                 commentFlag=true;
            }

            if(isFormInputElementEmpty(formId,'iHeight'+i)) 
            {		
                //$("#iHeight"+i).addClass('red_border');
                console.log('iHeight');
                //error_count++;
            }
            else
            {
                $("#iHeight"+i).removeClass('red_border');   
                 commentFlag=true;
            }

            if(isFormInputElementEmpty(formId,'iWeight'+i)) 
            {		
                //$("#iWeight"+i).addClass('red_border');
                console.log('iWeight');
                //error_count++;
            }
            else
            {
                $("#iWeight"+i).removeClass('red_border');   
                 commentFlag=true;
            }

            if(isFormInputElementEmpty(formId,'iQuantity'+i)) 
            {		
                //$("#iQuantity"+i).addClass('red_border');
                console.log('iQuantity');
                error_count++;
            }
            else
            {
                $("#iQuantity"+i).removeClass('red_border');   
                 commentFlag=true;
            }
            
            
            if(isFormInputElementEmpty(formId,'szCommodity'+i)) 
            {		
                //$("#iQuantity"+i).addClass('red_border');
                console.log('szCommodity');
                error_count++;
            }
            else
            {
                $("#szCommodity"+i).removeClass('red_border');
                commentFlag=true;
                 //commentFlag=true;
            }


            if(isFormInputElementEmpty(formId,'iColli'+i)) 
            {		
                //$("#iColli"+i).addClass('red_border');
                console.log('iColli');
                //error_count++;
            }
            else
            {
                $("#iColli"+i).removeClass('red_border');   
                 commentFlag=true;
            }
        }
    }
        console.log("error_count"+error_count);
        $("#iNoLineUpdated").attr('value','0');
        if(error_count==1)
        {
            $("#save_cargo_line_pending_tray").attr('style','opacity:1;');
            $("#save_cargo_line_pending_tray").unbind("click");
            $("#save_cargo_line_pending_tray").click(function(){updateCargoDetailFromPendingTray();});
        }
        else
        {
            $("#save_cargo_line_pending_tray").attr('style','opacity:0.4;');
            $("#save_cargo_line_pending_tray").unbind("click");
        }
        console.log("commentFlag"+commentFlag);
        if(!commentFlag)
        {
            $("#iNoLineUpdated").attr('value','1');
            $("#save_cargo_line_pending_tray").attr('style','opacity:1;');
            $("#save_cargo_line_pending_tray").unbind("click");
            $("#save_cargo_line_pending_tray").click(function(){updateCargoDetailFromPendingTray();});
            
            $("#horizontal-scrolling-div-id .red_border").removeClass("red_border");
        }
}

function updateCargoDetailFromPendingTray()
{   
     
    $("#save_cargo_line_pending_tray").attr('style','opacity:0.4;');
     $("#save_cargo_line_pending_tray").unbind("click");
    var value=$('#cargo_details_form').serialize();
    var newValue=value+"&mode=UPDATE_CARGO_LINE_FROM_PENDING_TRAY_UPDATE_CARGO_POPUP";
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){
        var result_ary = result.split("||||"); 
        if(result_ary[0]=='ERROR')
        {
            $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block');

           $("#loader").attr('style','display:none'); 
           $("#save_cargo_line_pending_tray").attr('style','opacity:0.4;');
            $("#save_cargo_line_pending_tray").unbind("click");
       }
       else if(result_ary[0]=='SUCCESS')
       {           
           $("#contactPopup").html("");
           $("#cargo_line_text").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:none');
       }
    });
}

function updateCargoLineDataIntoBooking(idBooking)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"UPDATE_CARGO_LINE_CONFIRMATION",idBooking:idBooking},function(result){
	   $("#contactPopup").html(result);		
           $("#contactPopup").attr('style','display:block');
        });
}

function updateCargoLineConfirmed(idBooking,iSearchMiniVersion)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"UPDATE_CARGO_LINE",idBooking:idBooking,iSearchMiniVersion:iSearchMiniVersion},function(result){
	   //$("#contactPopup").html(result);	
           //alert(result);
           $("#contactPopup").attr('style','display:none');
           var result_ary = result.split("||||"); 
           //console.log(result_ary);
           $("#fCargoVolume").attr('value',result_ary[1]);
           $("#fCargoWeight").attr('value',result_ary[0]);
           $("#iNumColli").attr('value',result_ary[2]);
           if(parseFloat(result_ary[1])>0)
           {
               $("#fCargoVolume").removeClass('red_border');
           }
           if(parseFloat(result_ary[0])>0)
           {
               $("#fCargoWeight").removeClass('red_border');
           }
           if(parseFloat(result_ary[2])>0)
           {
               $("#iNumColli").removeClass('red_border');
           }
          // $("#cargo_text_div").attr('title',result_ary[3]);
          // $("#cargo_text_div").html(result_ary[3]);
        });
}

function upload_label_files(data)
{ 
    var IS_JSON = true;
    try
    {
        var json = $.parseJSON(data);
    }
    catch(err)
    {
        IS_JSON = false;
    } 

    if(IS_JSON)
    { 
        var obj = JSON.parse(data); 
        var iFileCounter = 1;

        var filecount = $("#filecount").attr('value');
        var filecountOld = $("#filecount").attr('value');

        $("#deleteArea").attr('style','display:block');  
        $(".file_list_con").attr('style','display:block');

        for (var i = 0, len = obj.length; i < len; i++)
        { 
            var newfilecount = parseInt(filecount) + iFileCounter;  
            var uploadedFileName = obj[i]['name']; 
            var FileUrl= __JS_ONLY_SITE_BASE_IMAGE__ + "/courierLabel/" + uploadedFileName; 

            var container_li_id = "id_"+newfilecount;

            $('#fileList #namelist').append( '<li id="'+container_li_id+'" onclick="openPdfLightBox(\''+FileUrl+'\')" >&nbsp;</li>');  
            
            var szImageHtml = '<img src="'+__JS_ONLY_SITE_BASE_IMAGE__+'/image.php?img=Page.png&temp=2&w=50&h=50" border="0" />';
            $('#'+container_li_id).html(szImageHtml+'<span class="page">'+newfilecount+'</span>');

            iFileCounter++; 
            var files="";
            var filename_all=$("#file_name").val();  
            if(filename_all!='')
            { 
                var newvalue=filename_all+";"+files+"#####"+uploadedFileName;
                $("#fileuploadlink").attr('style','display:block');
                $("#file_uploade_plus_link").attr('style','display:block');
            }
            else
            { 
                var newvalue=files+"#####"+uploadedFileName;
                $("#fileuploadlink").attr('style','display:block');	
                $("#file_uploade_plus_link").attr('style','display:block');
            } 

            var textpage='page';
            if(parseInt(newfilecount)>1)
            {
                textpage='pages';
            }
            textpage=newfilecount+" "+textpage; 
            $("#iTotalPage").attr('value',textpage); 
            $("#file_name").attr('value',newvalue);
        } 
        $("#filecount").attr('value',newfilecount);  
        $('div.ajax-upload-dragdrop:gt(0)').hide ();  
        return true;
    }
    else
    {  
        //$("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='showHide(\"upload_process_list\")'>close</a></p>");
        return false;
    }
}

function upload_label_files_backup(uploadedFileName)
{
    console.log("File Name: "+uploadedFileName);
    var iFileCounter = 1;

    var filecount = $("#filecount").attr('value');
    var filecountOld = $("#filecount").attr('value');

    $("#deleteArea").attr('style','display:block'); 
    $("#upload_process_list").attr('style','display:none');
    $("#upload_process_list").html(' ');
    $(".file_list_con").attr('style','display:block');
    var i = 0;
    var newfilecount = parseInt(filecount) + iFileCounter;   
    var FileUrl=__JS_ONLY_SITE_BASE_IMAGE__ + "/courierLabel/"+uploadedFileName; 

    var container_li_id = "id_"+newfilecount;

    var szPageImageUrl = __JS_ONLY_SITE_BASE_IMAGE__+ "/images/Page2.png";

    $('#fileList #namelist').append( '<li id="'+container_li_id+'" onclick="openPdfLightBox(\''+FileUrl+'\')" >&nbsp;</li>');  
    $('#'+container_li_id).html( '<img src="'+szPageImageUrl+'" border="0" /><span class="page">'+newfilecount+'</span>');

    iFileCounter++;
    var files = "";

    var filename_all=$("#file_name").val();  
    if(filename_all!='')
    { 
        var newvalue=filename_all+";"+files+"#####"+uploadedFileName;
        $("#fileuploadlink").attr('style','display:block');
        $("#file_uploade_plus_link").attr('style','display:block');
    }
    else
    { 
        var newvalue=files+"#####"+uploadedFileName;
        $("#fileuploadlink").attr('style','display:block');	
        $("#file_uploade_plus_link").attr('style','display:block');
    } 
    var textpage='page';
    if(parseInt(newfilecount)>1)
    {
        textpage='pages';
    }
    textpage=newfilecount+" "+textpage; 
    $("#iTotalPage").attr('value',textpage); 
    $("#file_name").attr('value',newvalue); 
    $("#filecount").attr('value',newfilecount); 

    /*
    if(parseInt(newfilecount) == totalQuantity;)
    {
        $("#fileuploadlink").attr('style','display:none');
        $("#file_uploade_plus_link").attr('style','display:none');
    } 
    */
    $('div.ajax-upload-dragdrop:gt(0)').hide();
} 




function checkLCLExludedTradesData()
{ 
    var error = 1; 
    var formId = 'courierExcludeTradesForm';
    
    if(isFormInputElementEmpty(formId,'idForwarder')) 
    {		 
        console.log('forwarder');
        error++; 
    }  
    if(isFormInputElementEmpty(formId,'idOriginCountry')) 
    {		 
        console.log('country');
        error++; 
    } 
    if(isFormInputElementEmpty(formId,'idDestinationCountry')) 
    {		 
        console.log('dest country');
        error++; 
    } 
    if(isFormInputElementEmpty(formId,'iCustomerType'))
    {		 
        console.log('customer type');
        error++; 
    } 
    
    if(error==1)
    {
        //var iReturnval = check_duplicate_values(); 
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php?mode=CHECK_DUPLICATE_EXCLUDED_TRADES_LCL",$('#courierExcludeTradesForm').serialize(),function(result){
            var result_ary = result.split("||||"); 
            if(jQuery.trim(result_ary[0])=='SUCCESS')
            {
                $("#add_edit_excluded_trades_data").unbind("click");
                $("#add_edit_excluded_trades_data").attr('style','opacity:0.4');
            }
            else
            {
                $("#add_edit_excluded_trades_data").unbind("click");
                $("#add_edit_excluded_trades_data").click(function(){
                    submit_excluded_trades_lcl_form();
                });
                $("#add_edit_excluded_trades_data").attr('style','opacity:1');
            } 
        }); 
    } 
    
    var idExludedTrades = $("#idExludedTrades").val();
    idExludedTrades = parseInt(idExludedTrades);
    if(idExludedTrades>0)
    {
        $("#add_edit_excluded_trades_data_span").html('Save');
    }
    else
    {
        $("#add_edit_excluded_trades_data_span").html('Add');
    }
}

function submit_excluded_trades_lcl_form()
{   
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",$('#courierExcludeTradesForm').serialize(),function(result){
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#courier_excluded_trade_list").html(result_ary[1]); 
        }
        else
        {
            $("#courier_excluded_trades_addedit_data").html(result_ary[1]);
        }
        $("#loader").attr('style','display:none'); 
    });
}

function update_lcl_excluded_trades(excluded_trade_id,mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{excluded_trade_id:excluded_trade_id,mode:mode},function(result){
        var resultAry = result.split("||||");
        if(jQuery.trim(resultAry[0])=='SUCCESS_EDIT')
        { 
           $("#courier_excluded_trades_addedit_data").html(resultAry[1]); 
           $("#add_edit_excluded_trades_data_span").html('Save');
           checkLCLExludedTradesData();

           $("#detete_excluded_trades_data").unbind("click");
           $("#detete_excluded_trades_data").click(function(){
               update_lcl_excluded_trades(excluded_trade_id,'CANCEL_EXCLUDED_TRADES_LCL');
           });

           $("#detete_excluded_trades_data").attr('style','opacity:1');
           $("#detete_excluded_trades_data_span").html('Cancel');
        }
        else if(jQuery.trim(resultAry[0])=='SUCCESS_LIST')
        {
            $("#contactPopup").attr('style','display:none;');
            $("#courier_excluded_trade_list").html(resultAry[1]); 
        }
        else if(jQuery.trim(resultAry[0])=='SUCCESS_POPUP')
        {
            $("#contactPopup").html(resultAry[1]);  
            $("#contactPopup").attr('style','display:block;');
        }
        else if(jQuery.trim(resultAry[0])=='SUCCESS_DROP_DOWN')
        {
            $("#excluded_trade_forwarder_container").html(resultAry[1]);   
        }
    });
}  

function submit_forwarder_turn_over_form(mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php?mode="+mode,$("#forwarder_turnover_form").serialize(),function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#forwarder_turn_over_list_container").html(result_ary[1]);	  
        }  
        else if(jQuery.trim(result_ary[0])=='REDIRECT')
        {
            redirect_url(result_ary[1]);
        }
    });
}

function enableHandlerSaveButton()
{
    var error_count = 1;
    var formId = 'standard_cargo_handler_form';
    if(isFormInputElementEmpty(formId,'idFileOwner')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'iQuoteValidity')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'szQuoteEmailSubject')) 
    {		 
        error_count++;
    }
    
    var content = NiceEditorInstance.instanceById('szQuoteEmailBody').getContent();
    
    if(content=='<br>' || content=='') 
    {		 
        error_count++;
    }  
    if(error_count==1)
    {
        $("#standard_cargo_save").unbind("click");
        $("#standard_cargo_save").click(function(){ submit_cargo_handler_form(); });
        $('#standard_cargo_save').attr('style','opacity:1;'); 
    }
    else
    {
        $("#standard_cargo_save").unbind("click");
        $('#standard_cargo_save').attr('style','opacity:0.4;'); 
    }
} 
function enable_product_pricing(idForwarder)
{
    var forwarder_id = parseInt(idForwarder);
    $("#szTransitTime").attr('disabled','disabled');
    $("#idHandlingCurrency").attr('disabled','disabled');
    $("#fHandlingFeePerBooking").attr('disabled','disabled');
    $("#fHandlingMarkupPercentage").attr('disabled','disabled');
    $("#idHandlingMinMarkupCurrency").attr('disabled','disabled');
    $("#fHandlingMinMarkupPrice").attr('disabled','disabled');
    if(forwarder_id>0)
    { 
        $('#iBookingType').removeAttr('disabled'); 
        $('#idCourierProvider').attr('disabled','disabled'); 
        $('#idCourierProviderProduct').attr('disabled','disabled'); 
        
        $('#iBookingType').removeClass("red_border");
        $('#idCourierProvider').removeClass("red_border");
        $('#idCourierProviderProduct').removeClass("red_border");
        
        $('#iBookingType option:selected').removeAttr("selected");
        $('#idCourierProvider option:selected').removeAttr("selected");
        $('#idCourierProviderProduct option:selected').removeAttr("selected");
    }
    else
    {
        $('#iBookingType').attr('disabled','disabled'); 
        $('#idCourierProvider').attr('disabled','disabled'); 
        $('#idCourierProviderProduct').attr('disabled','disabled'); 
        
        $('#iBookingType').removeClass("red_border");
        $('#idCourierProvider').removeClass("red_border");
        $('#idCourierProviderProduct').removeClass("red_border");
        
        $('#iBookingType option:selected').removeAttr("selected");
        $('#idCourierProvider option:selected').removeAttr("selected");
        $('#idCourierProviderProduct option:selected').removeAttr("selected");
    }
}

function enable_standard_quote_select_button(idLandingPage)
{
    var page_id = parseInt(idLandingPage);
    if(page_id>0)
    {
        $("#standard_cargo_select_button").unbind("click");
        $("#standard_cargo_select_button").click(function(){ display_pricing_handler(page_id); });
        $('#standard_cargo_select_button').attr('style','opacity:1;'); 
    }
    else
    {
        $("#standard_cargo_select_button").unbind("click");
        $("#standard_cargo_select_button_span").html('SELECT');
        $('#standard_cargo_select_button').attr('style','opacity:0.4;'); 
    }
}

function change_standard_quote_selection()
{
    var page_id=$("#idCustomLandingPage").val();
    $("#standard_cargo_select_button").unbind("click");
    $("#standard_cargo_select_button_span").html('SELECT');
    //$('#standard_cargo_select_button').attr('style','opacity:0.4;'); 
    $("#idCustomLandingPage").removeAttr('disabled');
    //$('#idCustomLandingPage option:selected').removeAttr("selected");
    $('#standard_cargo_quote_pricing_container').attr('style','display:none;');
    $('#standard_cargo_quote_pricing_container').html('');
    $("#standard_cargo_select_button").click(function(){ display_pricing_handler(page_id); });
    
}
function display_pricing_handler()
{ 
    var page_id = $("#idAutomatedRfqResponse").val();
    
    var idAutomatedRfqResponseHidden=$("#idAutomatedRfqResponseHidden").attr('value');
    if(parseInt(idAutomatedRfqResponseHidden)>0)
    {
        $('#warning_msg').attr('style','display:block;');
    }
    else
    {
        if(parseInt(page_id)>0)
        {
            $("#loader").attr('style','display:block');
            $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'DIAPLAY_QUOTE_PRICING_HANDLER',page_id:page_id},function(result){ 
                var result_ary = result.split("||||"); 
                //console.log(result_ary);
                if(jQuery.trim(result_ary[0])=='SUCCESS')
                { 	
                   $("#standard_cargo_quote_pricing_container").attr('style','display:block');
                   
                   $("#idAutomatedRfqResponseHidden").attr('value',page_id);
                   //$("#standard_cargo_select_button_span").html('CHANGE');

                   //$("#idCustomLandingPage").attr('disabled','disabled'); 

                   //$("#standard_cargo_select_button").unbind("click");
                   //$("#standard_cargo_select_button").click(function(){ change_standard_quote_selection(); });
                   $("#standard_cargo_quote_pricing_container").html(result_ary[1]);	

                   $('#standard_cargo_select_new_button').attr('onclick','');
                   $("#standard_cargo_select_new_button").unbind("click");
                   $("#standard_cargo_select_new_button").click(function(){ open_automated_rfq_popup(page_id); });
                   $("#standard_cargo_select_new_button_text").html('Edit');


                    $("#standard_cargo_delete_new_button").unbind("click");
                    $("#standard_cargo_delete_new_button").click(function(){ delete_vaga_page_pricing_details(page_id); });
                    $('#standard_cargo_delete_new_button').attr('style','opacity:1;'); 
                }    
                $("#loader").attr('style','display:none;');
            });
        }
        else
        {   
            $("#standard_cargo_delete_new_button").unbind("click");
            $("#standard_cargo_delete_new_button").attr('style','opacity:0.4');
            $("#standard_cargo_quote_pricing_container").html('');

            $('#standard_cargo_select_new_button').attr('onclick','');
            $("#standard_cargo_select_new_button").unbind("click");
            $("#standard_cargo_select_new_button").click(function(){ open_automated_rfq_popup(); });
            $("#standard_cargo_select_new_button_text").html('New');
        }
    }
}

function submit_cargo_handler_form()
{
    var content = NiceEditorInstance.instanceById('szQuoteEmailBody').getContent();
    var description = encode_string_msg_str(content);
    var serialize_data=$("#standard_cargo_handler_form").serialize()+"&addStandardCargoHandler[szQuoteEmailBody]="+description;        
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",serialize_data,function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#standard_cargo_handler_container").html(result_ary[1]);	  
        }   
        else if(jQuery.trim(result_ary[0])=='ERROR')
        { 	
           $("#standard_cargo_handler_container").html(result_ary[1]);	  
        }
    });
}

function enableProducOptions(idBookingType)
{
    /*
     * idBookingType = 1 means LCL
     * idBookingType = 2 means Courier
     */
    
    $("#szTransitTime").attr('disabled','disabled');
    $("#idHandlingCurrency").attr('disabled','disabled');
    $("#fHandlingFeePerBooking").attr('disabled','disabled');
    $("#fHandlingMarkupPercentage").attr('disabled','disabled');
    $("#idHandlingMinMarkupCurrency").attr('disabled','disabled');
    $("#fHandlingMinMarkupPrice").attr('disabled','disabled');

    var booking_type = parseInt(idBookingType);
    if(booking_type==1)
    {
        $('#idCourierProvider').attr('disabled','disabled'); 
        $('#idCourierProviderProduct').attr('disabled','disabled'); 
        
        $('#idCourierProvider').removeClass("red_border");
        $('#idCourierProviderProduct').removeClass("red_border");
        
        $('#idCourierProvider option:selected').removeAttr("selected");
        $('#idCourierProviderProduct option:selected').removeAttr("selected");
        
        
        $("#szTransitTime").removeAttr('disabled');
        $("#idHandlingCurrency").removeAttr('disabled');
        $("#fHandlingFeePerBooking").removeAttr('disabled');
        $("#fHandlingMarkupPercentage").removeAttr('disabled');
        $("#idHandlingMinMarkupCurrency").removeAttr('disabled');
        $("#fHandlingMinMarkupPrice").removeAttr('disabled');
    }
    else if(booking_type==2)
    {
        $('#idCourierProviderProduct').attr('disabled','disabled'); 
        $('#idCourierProviderProduct option:selected').removeAttr("selected");
        var idForwarder = $("#idForwarder").attr('value');
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'ADD_COURIER_PROVIDER_DROPDOWN',idForwarder:idForwarder},function(result){ 
           var result_ary = result.split("||||"); 
           if(result_ary[0]=='SUCCESS_PROVIDER_DROP_DOWN')
           {
                $('#idCourierProvider').removeAttr('disabled'); 
               // $('#idCourierProviderProduct').removeAttr('disabled'); 

                $('#idCourierProvider option:selected').removeAttr("selected");
                $("#idCourierProviderDiv").html(result_ary[1]);
            }
            else
            {
                $('#idCourierProvider').attr('disabled','disabled'); 
                $('#idCourierProviderProduct').attr('disabled','disabled'); 

                $('#idCourierProvider').removeClass("red_border");
                $('#idCourierProviderProduct').removeClass("red_border");

                $('#idCourierProvider option:selected').removeAttr("selected");
                $('#idCourierProviderProduct option:selected').removeAttr("selected");
            }
            //$('#idCourierProviderProduct option:selected').removeAttr("selected");
        });
    }
} 
function display_add_quote_pricing_popup(page_id,quote_pricing_id)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'DIAPLAY_ADD_QUOTE_PRICING_POPUP',page_id:page_id,quote_pricing_id:quote_pricing_id},function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#add_standard_quote_pricing_form_container").html(result_ary[1]);	  
           $("#add_standard_quote_pricing_form_container").attr('style','display:block;'); 
        }    
    });
}

function submit_add_quote_pricing()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",$("#add_standard_quote_pricing").serialize(),function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#standard_cargo_quotes_container").html(result_ary[1]);	  
           $("#standard_cargo_quotes_container").attr('style','display:block;'); 
           $("#add_standard_quote_pricing_form_container").attr('style','display:none;'); 
        }   
        else if(jQuery.trim(result_ary[0])=='ERROR')
        { 	
           $("#add_standard_quote_pricing_form_container").html(result_ary[1]);	  
        }
    });
}

function enableQuotePricingSubmitButton()
{
    var error_count = 1;
    var formId = 'add_standard_quote_pricing';
    if(isFormInputElementEmpty(formId,'idForwarder')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'iBookingType')) 
    {		 
        error_count++;
    }
    else
    {
        var iBookingType = $('#iBookingType').attr('value');
        if(iBookingType==2)
        {
            if(isFormInputElementEmpty(formId,'idCourierProviderProduct')) 
            {		 
                error_count++;
            }
            if(isFormInputElementEmpty(formId,'idCourierProvider')) 
            {		 
                error_count++;
            } 
        }
    }
    if(isFormInputElementEmpty(formId,'szTransitTime')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'idHandlingCurrency')) 
    {		 
        error_count++;
    }  
    if(isFormInputElementEmpty(formId,'fHandlingFeePerBooking')) 
    {		 
        error_count++;
    }  
    if(isFormInputElementEmpty(formId,'fHandlingMarkupPercentage')) 
    {		 
        error_count++;
    }  
    if(isFormInputElementEmpty(formId,'idHandlingMinMarkupCurrency')) 
    {		 
        error_count++;
    }  
    if(isFormInputElementEmpty(formId,'fHandlingMinMarkupPrice')) 
    {		 
        error_count++;
    }   
    if(error_count==1)
    {
        $("#standard_quote_pricing_submit_button").unbind("click");
        $("#standard_quote_pricing_submit_button").click(function(){ submit_add_quote_pricing(); });
        $('#standard_quote_pricing_submit_button').attr('style','opacity:1;'); 
    }
    else
    {
        $("#standard_quote_pricing_submit_button").unbind("click");
        $('#standard_quote_pricing_submit_button').attr('style','opacity:0.4;'); 
    }
}

function enableProviderProducOptions(idCourierProvider)
{
    $('#idCourierProviderProduct').attr('disabled','disabled'); 
    $('#idCourierProviderProduct option:selected').removeAttr("selected");
    
    $("#szTransitTime").attr('disabled','disabled');
    $("#idHandlingCurrency").attr('disabled','disabled');
    $("#fHandlingFeePerBooking").attr('disabled','disabled');
    $("#fHandlingMarkupPercentage").attr('disabled','disabled');
    $("#idHandlingMinMarkupCurrency").attr('disabled','disabled');
    $("#fHandlingMinMarkupPrice").attr('disabled','disabled');
    
    var idForwarder = $("#idForwarder").attr('value');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'ADD_COURIER_PROVIDER_PRODUCT_DROPDOWN',idForwarder:idForwarder,idCourierProvider:idCourierProvider},function(result){ 
       var result_ary = result.split("||||"); 
       if(result_ary[0]=='SUCCESS_PROVIDER_PRODUCT_DROP_DOWN')
       {
            //$('#idCourierProvider').removeAttr('disabled'); 
            $('#idCourierProviderProduct').removeAttr('disabled'); 
             $('#idCourierProviderProduct option:selected').removeAttr("selected");
            //$('#idCourierProvider option:selected').removeAttr("selected");
            $("#idCourierProviderProductDiv").html(result_ary[1]);
        }
        else
        {
            $('#idCourierProviderProduct').attr('disabled','disabled'); 
            $('#idCourierProviderProduct option:selected').removeAttr("selected");
        }
       
    });
}

function select_standard_quote_pricing_row(idStandardPricing,iSequence,iTotalCount)
{
    $("#standardPricingTable .selected-row").removeClass("selected-row");
    var divId="sel_pricing_row_"+idStandardPricing;
    
    $("#"+divId).addClass("selected-row");
    
    $("#standard_cargo_down").unbind("click");
    $("#standard_cargo_down").attr('style','opacity:0.4');
    
    $("#standard_cargo_up").unbind("click");
    $("#standard_cargo_up").attr('style','opacity:0.4');
    
    $("#standard_cargo_delete").unbind("click");
    $("#standard_cargo_delete").attr('style','opacity:0.4');
    
    $("#standard_cargo_add").unbind("click");
    $("#standard_cargo_add").attr('style','opacity:0.4');
    
    if(parseInt(iTotalCount)>1)
    {
        if(parseInt(iSequence)==1)
        {
            $("#standard_cargo_down").unbind("click");
            $("#standard_cargo_down").click(function(){ move_up_down_standard_quote_pricing(idStandardPricing,iSequence,'down'); });
            $("#standard_cargo_down").attr('style','opacity:1');
        }
        else if(parseInt(iSequence)==parseInt(iTotalCount))
        {
            $("#standard_cargo_up").unbind("click");
            $("#standard_cargo_up").click(function(){ move_up_down_standard_quote_pricing(idStandardPricing,iSequence,'up'); });
            $("#standard_cargo_up").attr('style','opacity:1');           
            
        }
        else
        {
            $("#standard_cargo_up").unbind("click");
            $("#standard_cargo_up").click(function(){ move_up_down_standard_quote_pricing(idStandardPricing,iSequence,'up'); });
            $("#standard_cargo_up").attr('style','opacity:1');
            
            $("#standard_cargo_down").unbind("click");
            $("#standard_cargo_down").click(function(){ move_up_down_standard_quote_pricing(idStandardPricing,iSequence,'down'); });
            $("#standard_cargo_down").attr('style','opacity:1');
        }
         
    }
    
    $("#standard_cargo_add").attr('onclick','');
    $("#standard_cargo_add").unbind("click");
    $("#standard_cargo_add").click(function(){ open_edit_standard_quote_pricing(idStandardPricing); });
    $("#standard_cargo_add").attr('style','opacity:1');
    $("#standard_cargo_add").html('<span>Edit</span>');

    $("#standard_cargo_delete").unbind("click");
    $("#standard_cargo_delete").click(function(){ delete_standard_quote_pricing(idStandardPricing,iSequence); });
    $("#standard_cargo_delete").attr('style','opacity:1');
    
}

function move_up_down_standard_quote_pricing(idStandardPricing,iSequence,szFlag)
{
    var idCustomLandingPage=$("#idAutomatedRfqResponse").attr('value');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'MOVE_UP_DOWN_STANDARD_PRICING',idStandardPricing:idStandardPricing,iSequence:iSequence,szFlag:szFlag,idCustomLandingPage:idCustomLandingPage},function(result){ 
       $("#standard_cargo_quotes_container").html(result);
       
    });
}

function delete_standard_quote_pricing(idStandardPricing,iSequence)
{
    var idCustomLandingPage=$("#idAutomatedRfqResponse").attr('value');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'DELETE_STANDARD_PRICING',idStandardPricing:idStandardPricing,iSequence:iSequence,idCustomLandingPage:idCustomLandingPage},function(result){ 
       $("#standard_cargo_quotes_container").html(result);
       
    });
}

function open_edit_standard_quote_pricing(idStandardPricing)
{
    var idCustomLandingPage=$("#idAutomatedRfqResponse").attr('value');
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'OPEN_EDIT_STANDARD_PRICING',idStandardPricing:idStandardPricing,idCustomLandingPage:idCustomLandingPage},function(result){ 
       $("#add_standard_quote_pricing_form_container").html(result);	  
           $("#add_standard_quote_pricing_form_container").attr('style','display:block;');
       enableQuotePricingSubmitButton();
    });
}



function add_more_cargo_details_management()
{ 
    var idCustomLandingPage = $("#idLandingPage").val();
    var counter=$("#hiddenPosition_7").val();
    var newCounter=parseInt(counter)+1;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'ADD_NEW_CARGO_DETAIL_LINE_FOR_VOGA_PAGES',idCustomLandingPage:idCustomLandingPage,newCounter:newCounter},function(result){ 
      $("#hiddenPosition_7").attr('value',newCounter);
      $("#update_cargo_details_container_div").append(result);
    });
}

function enableQuotePricingFileds(iValue)
{
    if(parseInt(iValue)>0)
    {
        $("#szTransitTime").removeAttr('disabled');
        $("#idHandlingCurrency").removeAttr('disabled');
        $("#fHandlingFeePerBooking").removeAttr('disabled');
        $("#fHandlingMarkupPercentage").removeAttr('disabled');
        $("#idHandlingMinMarkupCurrency").removeAttr('disabled');
        $("#fHandlingMinMarkupPrice").removeAttr('disabled');
        
        enableQuotePricingSubmitButton();
    }
    else
    {
        $("#szTransitTime").attr('disabled','disabled');
        $("#idHandlingCurrency").attr('disabled','disabled');
        $("#fHandlingFeePerBooking").attr('disabled','disabled');
        $("#fHandlingMarkupPercentage").attr('disabled','disabled');
        $("#idHandlingMinMarkupCurrency").attr('disabled','disabled');
        $("#fHandlingMinMarkupPrice").attr('disabled','disabled');
    } 
}

function reset_standard_tryit_out(page_id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'RESET_STANDARD_TRY_IT_OUT',page_id:page_id},function(result){ 
        var result_ary = result.split("||||"); 
        if(result_ary[0]=='SUCCESS')
        {
            $("#add_standard_quote_pricing_try_id_container").html(result_ary[1]);
        } 
        $("#standard_cargo_try_it_out_result_container").html(" ");
        $("#standard_cargo_try_it_out_result_container").attr('style','display:none;');
    });
}

function enable_voga_try_it_out_submit()
{
    var error_count = 1;
    var iVogaPageType = $("#iVogaPageType").val();
    var formId = 'standardPricingTryItOutForm';
    if(iVogaPageType==5 || iVogaPageType==6)
    {
        if(isFormInputElementEmpty(formId,'szOriginCountry')) 
        {		  
            error_count++;
        }
        if(isFormInputElementEmpty(formId,'szDestinationCountry'))
        {		  
            error_count++;
        }
    }
    else
    {
        if(isFormInputElementEmpty(formId,'szConsigneePostcode')) 
        {		  
            error_count++;
        }
        if(isFormInputElementEmpty(formId,'szConsigneeCity'))
        {		  
            error_count++;
        }
        if(isFormInputElementEmpty(formId,'idConsigneeCountry')) 
        {		  
            error_count++;
        } 
    }
    
    if(error_count==1)
    {
        $("#standard-cargo-test-button").unbind("click");
        $("#standard-cargo-test-button").click(function(){ submit_voga_try_it_out_form(); });
        $('#standard-cargo-test-button').attr('style','opacity:1;'); 
    }
    else
    {
        $("#standard-cargo-test-button").unbind("click");
        $('#standard-cargo-test-button').attr('style','opacity:0.4;'); 
    }
}

function submit_voga_try_it_out_form()
{ 
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",$("#standardPricingTryItOutForm").serialize(),function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#standard_cargo_try_it_out_result_container").html(result_ary[1]);	  
           $("#standard_cargo_try_it_out_result_container").attr('style','display:block;');  
        }   
        else if(jQuery.trim(result_ary[0])=='ERROR')
        { 	
           $("#add_standard_quote_pricing_try_id_container").html(result_ary[1]);	  
        }
        $("#loader").attr('style','display:none');
    }); 
}

function selectSearchNotificationRow(idSearchNoti,iPrivate)
{
    if(iPrivate==1)
    {
        $("#private-customer-search-notification .selected-row").removeClass("selected-row");
        $("#search_private_customer_notification_"+idSearchNoti).addClass("selected-row");

        $("#search_noti_pc_delete").unbind("click");
        $('#search_noti_pc_delete').attr('style','opacity:1;');
        $("#search_noti_pc_delete").click(function(){ delete_serach_notification_data(idSearchNoti,1); });


        $('#search_noti_pc_addedit').attr('onclick','');
        $("#search_noti_pc_addedit").unbind("click");
        $('#search_noti_pc_addedit').attr('style','opacity:1;');
        $("#search_noti_pc_addedit").html("<span>Edit</span>");
        $("#search_noti_pc_addedit").click(function(){openAddEditSearchNotificationForm(idSearchNoti,'OPEN_ADD_SEARCH_NOTIFICTION_FORM_PRIVATE_CUSTOMER'); });
    }
    else
    {
        $("#mode-transport-table .selected-row").removeClass("selected-row");
        $("#search_noti_"+idSearchNoti).addClass("selected-row");

        $("#search_noti_delete").unbind("click");
        $('#search_noti_delete').attr('style','opacity:1;');
        $("#search_noti_delete").click(function(){ delete_serach_notification_data(idSearchNoti); });


        $('#search_noti_addedit').attr('onclick','');
        $("#search_noti_addedit").unbind("click");
        $('#search_noti_addedit').attr('style','opacity:1;');
        $("#search_noti_addedit").html("<span>Edit</span>");
        $("#search_noti_addedit").click(function(){openAddEditSearchNotificationForm(idSearchNoti,'OPEN_ADD_SEARCH_NOTIFICTION_FORM'); });
    }  
}

function delete_serach_notification_data(idSearchNoti,iPrivate)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_searchNotification.php",{mode:'OPEN_SEARCH_NOTIFICATION_DELETE_CONFIRMATION_POPUP',idSearchNoti:idSearchNoti,iPrivate:iPrivate},function(result){ 
       $("#search_noftification_popup").html(result);
       $("#search_noftification_popup").attr("style","display:block"); 
    });
}


function deleteSearchNotification(idSearchNoti,mode)
{
    $("#delete-button").unbind("click");
    $('#delete-button').attr('style','opacity:0.4;'); 

    $("#cancel-button").unbind("click");
    $('#cancel-button').attr('style','opacity:0.4;'); 
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_searchNotification.php",{mode:mode,idSearchNoti:idSearchNoti},function(result){ 
        
        if(mode=='DELETE_PRIVATE_CUSTOMER_SEARCH_NOTIFICATION')
        { 
            $("#search_noftification_listing_private_customer").html(result); 
        }
        else
        {
            $("#search_noftification_listing").html(result); 
        } 
        $("#search_noftification_popup").attr("style","display:none");
    });
}


function openAddEditSearchNotificationForm(idSearchNoti,mode)
{
    if(mode=='')
    {
        mode = 'OPEN_ADD_SEARCH_NOTIFICTION_FORM';
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_searchNotification.php",{mode:mode,idSearchNoti:idSearchNoti},function(result){ 
       $("#search_noftification_popup").html(result);
       $("#search_noftification_popup").attr("style","display:block");
       if(parseInt(idSearchNoti)>0)
       {
           enableSaveButtonForSearchNotification('addSearchNotificationForm');
       } 
    });
}

function enableTextUrlField(iButtonType)
{
    console.log("Butt ID: "+iButtonType);
    if(iButtonType==2 || iButtonType==4 || iButtonType==5 || iButtonType==6) //Continue button from Private customer notification popup
    {
        var button_id = '';
        var input_field_id = '';
        if(iButtonType==2)
        {
            button_id = 'idButtonType2';
            input_field_id = 'szContinueButtonText';
        }
        else if(iButtonType==4)
        {
            button_id = 'idButtonType4';
            input_field_id = 'szButtonTextRfq';
        }
        else if(iButtonType==5)
        {
            button_id = 'idButtonType5';
            input_field_id = 'szContinue2ButtonText';
        }
        else if(iButtonType==6)
        {
            button_id = 'idButtonType6';
            input_field_id = 'szContinue3ButtonText';
        }
        
        if(jQuery("#"+button_id).is(":checked"))
        {
            $("#"+input_field_id).attr('disabled',false); 
        }
        else
        {
           $("#"+input_field_id).attr('disabled',true); 
        }
    }
    else
    {
        if(jQuery("#idButtonType3").is(":checked"))
        {
            $("#szButtonText").attr('disabled',false);
            $("#szButtonUrl").attr('disabled',false);
        }
        else
        {
           $("#szButtonText").attr('disabled',true);
           $("#szButtonUrl").attr('disabled',true);
        }
    } 
}


function enableSaveButtonForSearchNotification(formId,iPrivate)
{
    var error = 1;
    var estimate_error = 1;
    
    if(iPrivate!=1)
    {
        if(isFormInputElementEmpty(formId,'idFromCountry')) 
        {		
            console.log('From Country');
            error++; 
        }
        else
        {
            var idFromCountry = $("#idFromCountry").attr('value');            
        }
        
        if(idFromCountry=='All')
        {
            $("#szFromContains").attr("readonly",true);
            $("#szFromContains").attr("value","*");
            $("#szFromContains").attr("style","background: rgba(0, 0, 0, 0.2) none repeat scroll 0 0;cursor: auto;border: 1px solid rgba(0, 0, 0, 0);");
        }
        else
        {
            $("#szFromContains").attr("readonly",false);
            $("#szFromContains").attr("style","");           
        }
        if(isFormInputElementEmpty(formId,'szFromContains')) 
        {		
            console.log('From Contains');
            error++; 
        } 
        if(isFormInputElementEmpty(formId,'idToCountry')) 
        {		
            console.log('To Country');
            error++; 
        }        
        else
        {
            var idToCountry = $("#idToCountry").attr('value');            
        }
        
        if(idToCountry=='All')
        {
            $("#szToContains").attr("readonly",true);
            $("#szToContains").attr("value","*");
            $("#szToContains").attr("style","background: rgba(0, 0, 0, 0.2) none repeat scroll 0 0;cursor: auto;border: 1px solid rgba(0, 0, 0, 0);");
        }
        else
        {
            $("#szToContains").attr("readonly",false);
            $("#szToContains").attr("style","");
        }
        
        if(isFormInputElementEmpty(formId,'szToContains')) 
        {		
            console.log('To Contains');
            error++; 
        } 
    } 
    
    if(isFormInputElementEmpty(formId,'iLanguage')) 
    {		
        console.log('Language');
        error++; 
    }
    
    if(isFormInputElementEmpty(formId,'szHeading')) 
    {		
        console.log('Heading');
        error++; 
    }
    
    if(isFormInputElementEmpty(formId,'szMessage')) 
    {		
        console.log('Message');
        error++; 
    }
    
    if(isFormInputElementEmpty(formId,'szMessage')) 
    {		
        console.log('Message');
        error++; 
    }
    
    if(jQuery("#idButtonType4").is(":checked") || jQuery("#idButtonType3").is(":checked") || jQuery("#idButtonType2").is(":checked") || jQuery("#idButtonType1").is(":checked"))
    {
        if(iPrivate==1)
        {
            if(jQuery("#idButtonType2").is(":checked"))
            {
                if(isFormInputElementEmpty(formId,'szContinueButtonText')) 
                {		
                    console.log('Continue Button Text');
                    error++; 
                }
            }
            if(jQuery("#idButtonType4").is(":checked"))
            {
                if(isFormInputElementEmpty(formId,'szButtonTextRfq')) 
                {		
                    console.log('Continue Button Text');
                    error++; 
                }
            }
            
            
            if(jQuery("#idButtonType3").is(":checked"))
            {
                if(isFormInputElementEmpty(formId,'szButtonText')) 
                {		
                    console.log('Button Text');
                    error++; 
                } 
                
                if(isFormInputElementEmpty(formId,'szButtonUrl')) 
                {		
                    console.log('Button Url');
                    error++; 
                }
                else
                {
                    var szButtonUrl=$("#szButtonUrl").val();
                    var szFlagValue=/([a-z]+\:\/+)([^\/\s]*)([a-z0-9\-@\^=%&;\/~\+]*)[\?]?([^ \#]*)#?([^ \#]*)/ig.test(szButtonUrl);
                    console.log("szFlagValue"+szFlagValue);
                    if (!szFlagValue) {
                        console.log('Invalid Button Url');
                        error++; 
                      }
                }
            } 
        }
        else
        {
            if(jQuery("#idButtonType3").is(":checked"))
            {
                if(isFormInputElementEmpty(formId,'szButtonText')) 
                {		
                    console.log('Button Text');
                    error++; 
                }

                if(isFormInputElementEmpty(formId,'szButtonUrl')) 
                {		
                    console.log('Button Url');
                    error++; 
                }
                else
                {
                    var szButtonUrl=$("#szButtonUrl").val();
                    var szFlagValue=/([a-z]+\:\/+)([^\/\s]*)([a-z0-9\-@\^=%&;\/~\+]*)[\?]?([^ \#]*)#?([^ \#]*)/ig.test(szButtonUrl);
                    console.log("szFlagValue"+szFlagValue);
                    if (!szFlagValue) {
                        console.log('Invalid Button Url');
                        error++; 
                      }
                }
            } 
        } 
    }
    else
    {
        console.log('Message');
        error++; 
    }
    
    if(parseInt(error)==1)
    {
        var mode = 'ADD_UPDATE_DATA_SUBMIT';
        if(iPrivate==1)
        {
            mode = "ADD_UPDATE_PRIVATE_CUSTOMER_DATA_SUBMIT";
        } 
        $("#save-button").unbind("click");
        $('#save-button').attr('style','opacity:1');
        $("#save-button").click(function(){ saveSearchNotification(mode); });
    }
    else
    {
        $("#save-button").unbind("click");
        $('#save-button').attr('style','opacity:0.4;');
    }
}

function saveSearchNotification(mode)
{
    var value=$('#addSearchNotificationForm').serialize();
    var newvalue=value+"&mode="+mode;
    
    $("#save-button").unbind("click");
    $('#save-button').attr('style','opacity:0.4;');
    
    $("#cancel-button").unbind("click");
    $('#cancel-button').attr('style','opacity:0.4;');
        
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_searchNotification.php",newvalue,function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
            if(mode=='ADD_UPDATE_PRIVATE_CUSTOMER_DATA_SUBMIT')
            {
                $("#search_noftification_listing_private_customer").html(result_ary[1]); 
            }
            else
            {
                $("#search_noftification_listing").html(result_ary[1]); 
            } 
            $("#search_noftification_popup").attr("style","display:none");
        }
        else
        {
            $("#save-button").unbind("click");
            $('#save-button').attr('style','opacity:1');
            $("#save-button").click(function(){ saveSearchNotification(mode); });
            
            $("#cancel-button").unbind("click");
            $('#cancel-button').attr('style','opacity:1;');
            $("#cancel-button").click(function(){ closeTip('search_noftification_popup'); });
        
            $("#search_noftification_popup").html(result_ary[1]);
            $("#search_noftification_popup").attr("style","display:block");
        }
       
    });
    
}


function selectLineForClosingTheFile(idFile,idCheckBox,iCtr)
{
    var idFileForClose=$("#idFileForClose").val();
    var idFileForCloseCtrValue=$("#idFileForCloseCtr").val();
    //console.log("File: "+idFile+" cb id: "+idCheckBox);
    if(jQuery("#"+idCheckBox).is(":checked"))
    {
        if(idFileForClose!='')
        {
           var newValue=idFileForClose+";"+idFile;
           var newValueCtr=idFileForCloseCtrValue+";"+iCtr;
        }
        else
        {
            var newValue=idFile;
            var newValueCtr=iCtr;
        }
        $("#idFileForClose").attr('value',""+newValue+"");
        $("#idFileForCloseCtr").attr('value',""+newValueCtr+"");
    }
    else
    {
        var idFileForCloseArr=idFileForClose.split(";");
        var idFileForCloseCtrValueArr=idFileForCloseCtrValue.split(";");
        var len=idFileForCloseArr.length;
        var values=[];
        var valuesCtr=[];
        for(var i=0;i<len;++i)
        {
            if(idFileForCloseArr[i]!=idFile)
            {
                values.push(idFileForCloseArr[i]);
                valuesCtr.push(idFileForCloseCtrValueArr[i]);
            }
        }
        var newValue = values.join(';');
        $("#idFileForClose").attr('value',""+newValue+"");
        
        var newValueCtr = valuesCtr.join(';');
        $("#idFileForCloseCtr").attr('value',""+newValueCtr+"");
    }
    
    var idFileForClose=$("#idFileForClose").val();
    
    var idFileForCloseArr=idFileForClose.split(";");
    var len=idFileForCloseArr.length;
    $("#idFileForCloseCount").attr('value',""+len+"");
    //console.log(idFileForCloseArr);
    
    if(parseInt(len)>0 && idFileForCloseArr!='')
    {
        $("#button_close_file").unbind("click");
        $('#button_close_file').attr('style','opacity:1');
        $("#button_close_file").click(function(){ openMultipleCloseBookingFile(); });
        $("#button_close_file").removeClass('button2').addClass('button1');
    }
    else
    {
        $("#button_close_file").unbind("click");
        $('#button_close_file').attr('style','opacity:0.4');
        $("#button_close_file").removeClass('button1').addClass('button2'); 
    }
}

function openMultipleCloseBookingFile()
{
    var totalCount= $("#idFileForCloseCount").val();
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{totalCount:totalCount,mode:'OPEN_MULTIPLE_CLOSE_BOOKING_FILE'},function(result){
	
        $("#contactPopup").html(result);		
        $("#contactPopup").attr('style','display:block');
        
    });
} 

function closeMultipleBookingFiles()
{
    $("#yes_button").unbind("click");
    $('#yes_button').attr('style','opacity:0.4');
        
    $("#no_button").unbind("click");
    $('#no_button').attr('style','opacity:0.4');   
    var idFileForClose=$("#idFileForClose").val(); 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{idFileForClose:idFileForClose,mode:'CONFIRM_MULTIPLE_CLOSE_BOOKING_FILE'},function(result)
    { 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            $("#contactPopup").attr('style','display:none;');
            var filesAry = idFileForClose.split(";");  
            $.each( filesAry, function( index, file_id ){ 
                if(file_id>0)
                {
                    if($(".pending_tray_listing_row_"+file_id).length)
                    {
                        $(".pending_tray_listing_row_"+file_id).remove();
                    }
                } 
            });   
            $("#idFileForClose").val(""); 
            $("#idFileForCloseCount").val(0);
            $("#idFileForCloseCtr").val(""); 
        } 
       //redirect_url(result); 
    }); 
}

function selectDeselectAllPendingTray()
{
    
    if(jQuery("#iAllCheckbox").is(":checked"))
    {
        var values=[];
        $(".pendingTrayList").attr('checked',true);
        
             
                     // var favorite = [];

            $.each($("input[name='pendingTrayList']:checked"), function(){            

                values.push($(this).val());

            });
          var newValue = values.join(';');
        $("#idFileForClose").attr('value',""+newValue+"");
    }
    else
    {
        $(".pendingTrayList").attr('checked',false);
        $("#idFileForClose").attr('value',"");
    }
    
    var idFileForClose=$("#idFileForClose").val();
    
    var idFileForCloseArr=idFileForClose.split(";");
    var len=idFileForCloseArr.length;
    $("#idFileForCloseCount").attr('value',""+len+"");
    
    if(parseInt(len)>0 && idFileForCloseArr!='')
    {
        $("#button_close_file").unbind("click");
        $('#button_close_file').attr('style','opacity:1');
        $("#button_close_file").click(function(){ openMultipleCloseBookingFile(); });
        $("#button_close_file").removeClass('button2').addClass('button1');
    }
    else
    {
        $("#button_close_file").unbind("click");
        $('#button_close_file').attr('style','opacity:0.4');
        $("#button_close_file").removeClass('button1').addClass('button2');
        
    }
}

function display_booking_list_pagination(page)
{
    var limit =$("#limit").val();
    var szBookingStatusFlag=$("#szBookingStatusFlag").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_cancelBooking.php",{szBookingStatusFlag:szBookingStatusFlag,limit:limit,page:page,flag:'CUSTOMER_BOOKING_LIST_PAGINATION'},function(result){
	
        if(szBookingStatusFlag=='InsuranceConfirmed' || szBookingStatusFlag=='InsuranceCancelled' || szBookingStatusFlag=='InsuranceSent')
        {
            $("#insured_booking_list_container").html(result)  
        }
        else if(szBookingStatusFlag=='SettledPaymentDetails' || szBookingStatusFlag=='paidForwarderInvoice' || szBookingStatusFlag=='payForwarderInvoice')
        {
            $("#checkDetails").html(result) 
        }
        else
        {    
            $("#cance_booking_html").html(result);
        }   
    });
}

function display_label_list_pagination(page)
{
     var pageName=$("#pageName").val();
     var limit =$("#limit").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_cancelBooking.php",{limit:limit,pageName:pageName,page:page,flag:'CUSTOMER_LABEL_LIST_PAGINATION'},function(result){
	
       $("#courier_booking_list_container").html(result);
        
    });
}


function submitEnterHeaderSearch(e)
{
    var keycode;
    keycode = e.which; 
    var szFreeFieldHeader = jQuery.trim($("#szFreeFieldHeader").val());
    $("#szFreeFieldHeader").removeClass('red_border');
    if(keycode == 13)
    {
        if(szFreeFieldHeader!='')
        {
            // $('#headerSearchForm').attr('target', "_blank");
             $('#headerSearchForm').attr('action', __JS_ONLY_SITE_BASE__+'/pendingTray/');
             $("#headerSearchForm").submit();
        }
        else
        {
            $("#szFreeFieldHeader").addClass('red_border');
           return false;
        }
    }
}


function openSnoozePopup(szFlag,idBooking)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_cancelBooking.php",{idBooking:idBooking,szFlag:szFlag,flag:'OPEN_SNOOZE_POPUP'},function(result){
	
       $("#contactPopup").html(result);
       $("#contactPopup").attr('style','display:block');
        
    });
}


function confirm_snooze_data(szFlag,idBooking)
{
        var dtSnooze = jQuery.trim($("#dtSnooze").val());
        var dtSnoozeTime = jQuery.trim($("#dtSnoozeTime").val());
        
        $("#cancel_booking_yes").unbind("click");
        $('#cancel_booking_yes').attr('style','opacity:0.4');
        
        $("#cancel_booking_no").unbind("click");
        $('#cancel_booking_no').attr('style','opacity:0.4');
        
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_cancelBooking.php",{idBooking:idBooking,dtSnooze:dtSnooze,dtSnoozeTime:dtSnoozeTime,szFlag:szFlag,flag:'CONFIRM_SNOOZE_POPUP_MSG'},function(result){
	
       $("#show_message").html(result);
       $("#contactPopup").attr('style','display:none');
       $("#"+szFlag+"_"+idBooking).removeClass('snoozeDate');
      //$("#show_message").attr('style','display:none');
    });
}

function setCurrencyValueForHandlingFee(iValue,divid)
{
    $("#"+divid).attr('value',iValue);
}

function setPermissionForRole(szRoleType,idRole)
{
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{szRoleType:szRoleType,flag:"EDIT_ROLE_PERMISSION",idRole:idRole},function(result){
            $("#ajaxLogin").html(result);
            $("#ajaxLogin").attr('style','display:block');
    });
    
}



function confrimRolePermission(idRole)
{
    var value = $("#setRolePermissionForm").serialize();
    var newValue= value+"&idRole="+idRole+"&flag=ADD_UPDATE_ROLE_PERMISSION";
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",newValue,function(result){
            $("#ajaxLogin").html('');
    });
}

function enableDiablePagePermissionBlock(idMenu)
{
    var divid="check_box_list_"+idMenu;
    var divmain="per_"+idMenu;
    var cb_flag = $("#"+divmain).prop("checked"); 
    if(cb_flag)
    {
        $("#"+divid).attr('style','display:');
         $("#"+divid +" input").attr('checked','checked');
    }
    else
    {
        $("#"+divid).attr('style','display:none');
        $("#"+divid +" input").attr('checked',false);
        
    } 
}

function auto_select_search_box(e)
{  
    if(e.keyCode==191)
    {
        //console.log("Forwarder Slash clicked...");
        //console.log("Input Type: " + szFocusedElementId);
        
        var szFocusedElementId = $("*:focus").attr("id");  
        var element_count = 1;
        if($("#"+szFocusedElementId).length)
        {
            if($("#"+szFocusedElementId).is("input"))
            {
                console.log("IS INPUT");
                element_count++;
            } 
            else if($("#"+szFocusedElementId).is("textarea"))
            {
                console.log("IS TEXTAREA");
                element_count++;
            } 
            else
            {
                console.log("Something else");
            } 
        }        
        if(element_count==1)
        {  
            $('#szFreeFieldHeader').focus().selectionEnd;
            $("#szFreeFieldHeader").select();
        } 
    }
}

function createNewAdminRole(szFlag)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:szFlag},function(result){
        $("#ajaxLogin").html(result);
        $("#ajaxLogin").attr('style','display:block');
    });
}

function enableAddRoleButton(id,iValue)
{
    $("#szRoleType").removeClass('red_border');
    if(jQuery.trim(iValue)!='')
    {
         $("#add_role_button").unbind("click");
        $('#add_role_button').attr('style','opacity:1');
        $("#add_role_button").click(function(){ addNewAdminRole(); });
        
    }
    else
    {
         $("#add_role_button").unbind("click");
        $('#add_role_button').attr('style','opacity:0.4');
        
    }
}

function addNewAdminRole()
{
    var value = $("#addAdminRoleForm").serialize();
    var newValue= value+"&flag=ADD_NEW_ROLE_SUBMIT";
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",newValue,function(result){
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#content_body").html(resArr[1]);
            $("#ajaxLogin").html('');
        }
        else
        {
            $("#ajaxLogin").html(resArr[1]);
            $("#szRoleType").addClass('red_border');
        }
    });
}


function selectRoleTypeRow(id)
{
    $(".selected-row").removeClass("selected-row");
    
    $("#delete_role").unbind("click");
    $('#delete_role').attr('style','opacity:0.4');
    
    $("#role_type_"+id).addClass("selected-row");
    
    
    $("#delete_role").unbind("click");
    $('#delete_role').attr('style','opacity:1');
    $("#delete_role").click(function(){ deleteAdminRole(id); });
    
}

function deleteAdminRole(id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:'DELETE_ROLE_TYPE',idRole:id},function(result){
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#ajaxLogin").html(resArr[1]);
            $("#ajaxLogin").attr('style','display:block');
        }
        else
        {     
            $("#content_body").html(resArr[1]);
            $("#ajaxLogin").html(''); 
        }
    });
}  

function validateQQBookingInformation(onLoad)
{
    var requiredFieldsAry = new Array(); 
    requiredFieldsAry['0'] = 'szFirstName';
    requiredFieldsAry['1'] = 'szLastName';
    requiredFieldsAry['2'] = 'szEmail'; 
    var szBookingType = $("#szBookingType").val();
    /*
    * We only have above 3 fields are required in case of SEND_QUOTE @Ajay
    */
   var szShipperPostcodeRequiredFlag ='';
   var szConsigneePostcodeRequiredFlag ='';
   var szCollectionFlag='';
   var szDeliveryFlag='';
    if(szBookingType=='CREATE_BOOKING')
    { 
        $.each($(".quick-quote-service-cb"), function(e){
            var cb_id = this.id;
            
            var cb_flag = $("#"+cb_id).prop('checked');
            if(cb_flag)
            {
                var szServiceID= $("#"+cb_id).attr('value');
                //alert(szServiceID);
                szCollectionFlag = $("#szCollectionFlag_"+szServiceID).val();
                szDeliveryFlag = $("#szDeliveryFlag_"+szServiceID).val();
                szConsigneePostcodeRequiredFlag = $("#szConsigneePostcodeRequiredFlag_"+szServiceID).attr('value');
                //alert(szConsigneePostcodeRequiredFlag+"szConsigneePostcodeRequiredFlag");
                szShipperPostcodeRequiredFlag = $("#szShipperPostcodeRequiredFlag_"+szServiceID).val();
                //alert(szShipperPostcodeRequiredFlag+"szShipperPostcodeRequiredFlag");
            }
            
        });    
        requiredFieldsAry['3'] = 'szCustomerPhoneNumber';   
        requiredFieldsAry['5'] = 'szCustomerAddress1';   
        requiredFieldsAry['6'] = 'szCustomerPostCode'; 
        requiredFieldsAry['7'] = 'szCustomerCity'; 
        requiredFieldsAry['8'] = 'szCustomerCountry';   
        requiredFieldsAry['9'] = 'szShipperFirstName'; 
        requiredFieldsAry['10'] = 'szShipperLastName'; 
        requiredFieldsAry['11'] = 'szShipperCompanyName'; 
        requiredFieldsAry['12'] = 'szShipperPhoneNumber'; 
        requiredFieldsAry['13'] = 'szShipperEmail'; 
       
        requiredFieldsAry['14'] = 'szOriginAddress'; 
        
        requiredFieldsAry['15'] = 'szOriginCity'; 
        requiredFieldsAry['16'] = 'idOriginCountry';   
        requiredFieldsAry['17'] = 'szConsigneeFirstName'; 
        requiredFieldsAry['18'] = 'szConsigneeLastName';    
        requiredFieldsAry['19'] = 'szConsigneeEmail'; 
        requiredFieldsAry['20'] = 'szConsigneePhoneNumber';
        requiredFieldsAry['21'] = 'szConsigneeEmail';
        requiredFieldsAry['22'] = 'szDestinationAddress';
        
        requiredFieldsAry['23'] = 'szDestinationCity';
        requiredFieldsAry['24'] = 'idDestinationCountry'; 
        requiredFieldsAry['25'] = 'szCargoDescription';
        requiredFieldsAry['26'] = 'iInsuranceIncluded'; 
        
        var iShipperPostCode=false;
        //alert(szShipperPostcodeRequiredFlag+"iShipperPostCode");
        if(szShipperPostcodeRequiredFlag!='No')
        {
            iShipperPostCode=true;
            requiredFieldsAry['27'] = 'szOriginPostcode';
        }
        
        var iConsigneePostCode=false;
        if(szConsigneePostcodeRequiredFlag!='No')
        {
            iConsigneePostCode=true;
            if(iShipperPostCode) 
            {
                requiredFieldsAry['28'] = 'szDestinationPostcode';
            }
            else
            {
                requiredFieldsAry['27'] = 'szDestinationPostcode';
            }
        }
        
        var iInsuranceIncluded = $("#iInsuranceIncluded").val();
        if(iInsuranceIncluded!=2)
        {
            if(iConsigneePostCode && iShipperPostCode)
            {
                requiredFieldsAry['29'] = 'idGoodsInsuranceCurrency';
                requiredFieldsAry['30'] = 'fCargoValue';
            }
            else if(!iConsigneePostCode && !iShipperPostCode)
            {
                requiredFieldsAry['27'] = 'idGoodsInsuranceCurrency';
                requiredFieldsAry['28'] = 'fCargoValue';
            }
            else if(iConsigneePostCode && !iShipperPostCode)
            {
                requiredFieldsAry['28'] = 'idGoodsInsuranceCurrency';
                requiredFieldsAry['29'] = 'fCargoValue';
            }
            else if(!iConsigneePostCode && iShipperPostCode)
            {
                requiredFieldsAry['28'] = 'idGoodsInsuranceCurrency';
                requiredFieldsAry['29'] = 'fCargoValue';
            }
            
        } 
        var iPrivateCustomer = $("#iPrivateCustomer").val();
        console.log("iPrivateCustomer"+iPrivateCustomer);
        if(iPrivateCustomer!=1)
        {
            console.log("iPrivateCustomer"+iPrivateCustomer);
            requiredFieldsAry['4'] = 'szCustomerCompanyName'; 
        } 
        
        
    } 
    
    var formId = 'quick_quote_shipper_consignee_form';
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
       console.log("Error"+index);
        var checkflag=true;
        if(iPrivateCustomer==1 && index==4)
        {
            checkflag=false;
            $("#szCustomerCompanyName").removeClass('red_border');
        }
        
         if(szCollectionFlag=='No' && index==14){
             checkflag=false;
            $("#szOriginAddress").removeClass('red_border');
         }
         
         if(szDeliveryFlag=='No' && index==22){
             checkflag=false;
            $("#szDestinationAddress").removeClass('red_border');
         }
        
        
        if(checkflag){
            if(isFormInputElementEmpty(formId,input_fields))
            {		 
                if(onLoad==1)
                {
                    $("#"+input_fields).addClass('red_border');
                } 
              console.log(index+"input_fields"+input_fields);
                error++;  
            } 
            else
            {
                $("#"+input_fields).removeClass('red_border');
            }
        }
    });  
    console.log("error count"+error);
    //@TO DO
    if(parseInt(error)==1)
    {
        if(szBookingType=='SEND_QUOTE')
        {
            $("#quick_quote_send_quote_button").unbind("click");
            $('#quick_quote_send_quote_button').attr('style','opacity:1');
            $("#quick_quote_send_quote_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','SEND_QUOTE_CONFIRM');});
        } 
        else if(szBookingType=='CREATE_BOOKING')
        {
            $("#quick_quote_create_booking_button").unbind("click");
            $('#quick_quote_create_booking_button').attr('style','opacity:1');
            $("#quick_quote_create_booking_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','CREATE_BOOKING_CONFIRM');});
        }
    }
    else
    {
        if(szBookingType=='SEND_QUOTE')
        {
            $("#quick_quote_send_quote_button").unbind("click");
            $('#quick_quote_send_quote_button').attr('style','opacity:0.4;');
        }
        else if(szBookingType=='CREATE_BOOKING')
        {
            $("#quick_quote_create_booking_button").unbind("click");
            $('#quick_quote_create_booking_button').attr('style','opacity:0.4;');
        } 
    }   
}

function enableQQGetRatesButton(kEevent,onLoad)
{
    var requiredFieldsAry = new Array(); 
    requiredFieldsAry['0'] = 'szOriginCountryStr';
    requiredFieldsAry['1'] = 'szDestinationCountryStr';
    requiredFieldsAry['2'] = 'idServiceTerms';
    requiredFieldsAry['3'] = 'dtShipmentDate';  
    requiredFieldsAry['4'] = 'idCustomerCurrency';   
    requiredFieldsAry['5'] = 'idCustomerCountry';   
    
    var formId = 'quick_quote_search_form';
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {		 
            if(onLoad==1)
            {
                $("#"+input_fields).addClass('red_border');
            }
            error++;  
        }
        else
        {
            $("#"+input_fields).removeClass('red_border');
        }
    }); 
    
    var iBreakBulk = 0;
    var iParcel = 0;
    var iPallet = 0;
    
    if($("#szShipmentType_BREAK_BULK").prop("checked"))
    {
        iBreakBulk =1;
    }
    if($("#szShipmentType_PARCEL").prop("checked"))
    {
        iParcel =1;
    }
    if($("#szShipmentType_PALLET").prop("checked"))
    {
        iPallet =1;
    } 
    
    if(iBreakBulk==0 && iParcel==0 && iPallet==0)
    { 
        $("#label_szShipmentType_BREAK_BULK").addClass('red_border'); 
        $("#label_szShipmentType_PARCEL").addClass('red_border'); 
        $("#label_szShipmentType_PALLET").addClass('red_border');  
        error++;
    } 
    else
    {
        $("#label_szShipmentType_BREAK_BULK").removeClass('red_border');
        $("#label_szShipmentType_PARCEL").removeClass('red_border');
        $("#label_szShipmentType_PALLET").removeClass('red_border');
        
        var cargo_valid_flag = validateQuickQuoteCargoDetails();
        if(cargo_valid_flag!=1)
        {
            error++;
        }
    } 
    
    if(parseInt(error)==1)
    {
        $("#quick_quote_get_price_button").unbind("click");
        $('#quick_quote_get_price_button').attr('style','opacity:1');
        $("#quick_quote_get_price_button").click(function(){ submitQuickQuoteForm(); });
        if(kEevent.which == 13)
        {
            submitQuickQuoteForm();
        } 
    }
    else
    {
        $("#quick_quote_get_price_button").unbind("click");
        $('#quick_quote_get_price_button').attr('style','opacity:0.4;');
    } 
}
function checkFromAddressAdmin(szValue,flag,on_blur)
{
    var id_suffix="";
    var ret_ary = new Array();
    var szCity = '';
    var szState = '';
    var szCountryISO = '';
    var szCountryName = '';
    var error_count = 1; 
    
    var szFieldID='';
    var formId = 'quick_quote_search_form';
    if(flag=='FROM_COUNTRY')
    {
        szFieldID = 'szOriginCountryStr';
    }
    else
    {
        szFieldID = 'szDestinationCountryStr';
    } 
    if(isFormInputElementEmpty(formId,szFieldID))
    { 
        $("#"+szFieldID).addClass('red_border');
    }
    else
    { 
        transporteca_geocode_service(szValue,flag,id_suffix,1);  
    } 
} 

function prefill_quick_quote_addres(ret_ary,flag)
{
    var szCity = '';
    var szState = '';
    var szCountryName = '';
    var szCountryISO = '';
    var szPostcode = '';
    var idCurrency = '';
    var idCustomerCountry = "";
    var iShipperConsignee = $('input:radio[name="quickQuoteShipperConsigneeAry[iShipperConsignee]"]:checked').val();  
     
    if(Array.isArray(ret_ary))
    { 
        szPostcode = ret_ary["szPostcode"];
        szCity = ret_ary["szCity"]; 
        szCountryName = ret_ary["szCountryName"];
        szCountryISO = ret_ary["szCountryISO"];
    }
    if(flag=='FROM_COUNTRY')
    {
        var idCountry = transportecaCountryListAry[szCountryISO]['id']; 
        var iInternationDialCode = "+" + transportecaCountryListAry[szCountryISO]['iInternationDialCode']; 
        
        $("#szOriginPostcode").val(szPostcode);
        $("#szOriginCity").val(szCity); 
        $("#idOriginCountry").val(idCountry);
        //$("#idShipperDialCode").val(idCountry); 
        //$("#idShipperDialCode select option:contains("+iInternationDialCode+"\")").attr('selected', true);
        
        $("#idOriginCountryHidden").val(idCountry);
        
        selectDropdownByText('idShipperDialCode',iInternationDialCode); 
        
        if(iShipperConsignee==1)
        {
            $("#szCustomerPostCode").val(szPostcode);
            $("#szCustomerCity").val(szCity);  
            $("#szCustomerCountry").val(idCountry);  
            //$("#idCustomerDialCode").val(idCountry); 
            selectDropdownByText('idCustomerDialCode',iInternationDialCode); 
            idCurrency = transportecaCountryListAry[szCountryISO]['idCurrency'];
            idCustomerCountry = idCountry;
        }  
        if(isEmpty(szCity))
        {
            $("#szOriginCountryStr").removeClass('red_border');
        }
        else
        {
            $("#szOriginCountryStr").addClass('red_border');
        } 
    }
    else if(flag=='TO_COUNTRY')
    {
        var idCountry = transportecaCountryListAry[szCountryISO]['id']; 
        var iInternationDialCode = "+" + transportecaCountryListAry[szCountryISO]['iInternationDialCode']; 
        
        $("#szDestinationPostcode").val(szPostcode);
        $("#szDestinationCity").val(szCity); 
        $("#idDestinationCountry").val(idCountry);
        //$("#idConsigneeDialCode").val(idCountry);
        selectDropdownByText('idConsigneeDialCode',iInternationDialCode);  
        $("#idDestinationCountryHidden").val(idCountry);
        
        if(iShipperConsignee==2)
        {
            $("#szCustomerPostCode").val(szPostcode);
            $("#szCustomerCity").val(szCity);  
            $("#szCustomerCountry").val(idCountry);  
            //$("#idCustomerDialCode").val(idCountry);  
            selectDropdownByText('idCustomerDialCode',iInternationDialCode); 
            
            idCurrency = transportecaCountryListAry[szCountryISO]['idCurrency'];
            idCustomerCountry = idCountry;
        } 
        if(isEmpty(szCity))
        {
            $("#szDestinationCountryStr").removeClass('red_border');
        }
        else
        {
            $("#szDestinationCountryStr").addClass('red_border');
        }  
    } 
    var dtdtradeFlag=false;
    idOriginCountry=$("#idOriginCountry").val();
    idDestinationCountry=$("#idDestinationCountry").val();
    var iUpdatedTermsDropDown =$("#iUpdatedTermsDropDown").val();
    if(dtdTradeStr!='' && idDestinationCountry>0 && idOriginCountry>0  && parseInt(iUpdatedTermsDropDown)==0)
    {
        from_to_country_str=idOriginCountry+"-"+idDestinationCountry;
         dtdTradeArr=dtdTradeStr.split(";");
         if(in_array(from_to_country_str,dtdTradeArr))
         {
             $('#idServiceTerms option').each(function() {
                var option_val = $(this).val(); 
                if(option_val==2 || option_val==4 || option_val==5) // 2.FOB 4.CFR 5.EXW-WTD
                {
                    $(this).hide();
                    $(this).attr('disabled','disabled');
                } 
                else
                {
                    $(this).show(); 
                    $(this).removeAttr('disabled');
                    if(option_val==1)
                    {
                        $(this).attr('selected','selected');
                        idServiceTerms_hidden = $(this).val();
                    }
                }
            });
         }
         else
         {
             $('#idServiceTerms option').each(function() {
                var option_val = $(this).val(); 
                 $(this).show(); 
                $(this).removeAttr('disabled');
                if(option_val==2)
                {
                    $(this).attr('selected','selected');
                    idServiceTerms_hidden = $(this).val();
                }
                
            });
         }
    }
    //$("#iUpdatedTermsDropDown").attr('value','0');
    var iCopyQuickQuoteBookingFlag='0';
    if($("#iCopyQuickQuoteBookingFlag").length>0){
    iCopyQuickQuoteBookingFlag= $("#iCopyQuickQuoteBookingFlag").attr('value');
    }
    if(idCurrency>0 && iCopyQuickQuoteBookingFlag=='0')
    {
        $("#idCustomerCurrency").val(idCurrency); 
        $("#idCustomerCurrency").removeClass('red_border');
        
        $("#idGoodsInsuranceCurrency").val(idCurrency); 
        $("#idGoodsInsuranceCurrency").removeClass('red_border'); 
    }
    if($("#iCopyQuickQuoteBookingFlag").length>0){
    $("#iCopyQuickQuoteBookingFlag").attr('value','0');
    }
    
    if(idCustomerCountry>0)
    {
        $("#idCustomerCountry").val(idCustomerCountry); 
        $("#idCustomerCountry").removeClass('red_border');
    } 
    autofill_billing_address__new_rfq(1); 
    console.log("Count: "+idCountry+" Curr: "+idCurrency);
    validateQQBookingInformation();
}

function selectDropdownByText(select_box_id,szText)
{ 
    $('#'+select_box_id+' option').each(function() {
        var text = $(this).html(); 
        if(szText==text)
        {
            $(this).attr('selected','selected');  
        }
    });
}
function isEmpty(variable) { 
    return (variable !== null && variable !== undefined);
}
function clearQuickQuotesForm()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php",{mode:'CLEAR_QUICK_QUOTE_FORM'},function(result){
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#quick_quote_main_container").html(resArr[1]); 
        } 
    });
} 
function submitQuickQuoteForm()
{
    $("#quick_quote_get_price_button").addClass('get-rates-btn-clicked');
    
    $("#quick_quote_search_result_container").html(''); 
    $("#quick_quote_search_result_container").attr('style','display:none'); 
    
    $("#quick_quote_send_quote_container").attr('style','display:none');
    $("#quick_quote_make_booking_container").attr('style','display:none');
    
    close_open_div('booking_information_container','pending_task_overview_open_close_link','close',1);
    
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php?mode=SUBMIT_QUICK_QUOTE_FORM",$("#quick_quote_search_form").serialize(),function(result){
        var resArr=result.split("$$$$");
        if(resArr[0]=='SUCCESS')
        {
            $("#quick_quote_search_result_container").html(resArr[1]); 
            $("#quick_quote_search_result_container").attr('style','display:block'); 
            var iScrollTopLine = parseInt(resArr[2]);
            if(iScrollTopLine==1)
            {
                $('html, body').animate({ scrollTop: ($("#quote_search_result_form").offset().top - 20) }, "100000");  
            }
            else
            {
                $('html, body').animate({ scrollTop: ($("#quick_quote_service_button_container").offset().top - 20) }, "100000");  
            } 
        }
        else if(jQuery.trim(resArr[0])=='ERROR')
        {
            $("#quick_quote_search_form_container").html(resArr[1]); 
        }
        $("#quick_quote_get_price_button").removeClass('get-rates-btn-clicked');
    });
    
} 

function toggleCargoFields(cb_id,cargo_type)
{
    if($("#"+cb_id).prop("checked"))
    {
        $("#quick_quote_cargo_container_"+cargo_type).attr('style','display:block;');
        $("#quick_quote_cargo_container_"+cargo_type).slideDown();
    }
    else
    {
        $("#quick_quote_cargo_container_"+cargo_type).attr('style','display:none;');
        $("#quick_quote_cargo_container_"+cargo_type).slideUp(); 
    }
}

function update_ship_con_by_country_selection(customer_country)
{
    if(customer_country>0)
    {
        var idOriginCountry = $("#idOriginCountryHidden").val();
        var idDestinationCountry = $("#idDestinationCountryHidden").val();
        //console.log("Country: "+customer_country+" Shipper: "+idOriginCountry+" Cons: "+idDestinationCountry);
        
        var iUpdateBilling = false;
        if(idOriginCountry==customer_country)
        {
            //Shipper 
            $("#iShipperConsignee_1").prop('checked',true);
            var szOriginAddress_js = $("#szOriginCountryStr").val();   
            checkFromAddressAdmin(szOriginAddress_js,'FROM_COUNTRY');
        }
        else if(idDestinationCountry==customer_country)
        {
            //Consignee  
            $("#iShipperConsignee_2").prop('checked',true);
            var szDestinationAddress_js = $("#szDestinationCountryStr").val();  
            checkFromAddressAdmin(szDestinationAddress_js,'TO_COUNTRY');
        }  
        else
        {
            $("#iShipperConsignee_3").prop('checked',true);
            iUpdateBilling = 1;
        }
        autofill_billing_address__new_rfq(1);
        
        if(iUpdateBilling==1)
        { 
            $("#szCustomerAddress1").val("");
            $("#szCustomerPostCode").val("");
            $("#szCustomerCity").val("");  
            $("#szCustomerCountry").val(customer_country); 
            $("#idCustomerDialCode").val(customer_country); 
        }
    }    
}
function update_ship_con(service_term)
{
    if(service_term>0)
    { 
        /*
        * 1. EXW-DTD
        * 2. FOB
        * 3. DAP - Cleared
        * 4. CFR
        * 5. EXW-WTD
        * 6. DAP - No CC
        */
        var iShipperConsignee = '';
        if(service_term==1 || service_term==2 || service_term==5)
        {
            //consignee 
            $("#iShipperConsignee_2").prop("checked", true);
            iShipperConsignee = 2;
        }
        else
        {
            //Shipper
            $("#iShipperConsignee_1").prop("checked", true);
            iShipperConsignee = 1;
        } 
        $("#iUpdatedTermsDropDown").attr('value','1');
        if(iShipperConsignee==2)
        {
            var szDestinationAddress = $("#szDestinationCountryStr").val();
            checkFromAddressAdmin(szDestinationAddress,'TO_COUNTRY');
        }
        else
        {
            var szOriginCountryStr = $("#szOriginCountryStr").val();
            checkFromAddressAdmin(szOriginCountryStr,'FROM_COUNTRY'); 
        } 
        autofill_billing_address__new_rfq(1);
    }
}

function prefillCompanyField(cb_id)
{
    var cb_flag = $("#"+cb_id).prop('checked'); 
    if(cb_flag)
    {
        $("#iPrivateCustomer").val(1);
        var szFirstName = $("#szFirstName").val();
        var szLastName = $("#szLastName").val();
        var CustomerName = szFirstName+" "+szLastName;  
        $("#szCustomerCompanyName").val(CustomerName);
        $("#szCustomerCompanyName").attr('disabled','disabled');
        autofill_billing_address__new_rfq(1);
    } 
    else
    {
        $("#iPrivateCustomer").val(0);
        $("#szCustomerCompanyName").removeAttr('disabled');
    }
}

function openChangePasswordPopup()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",{flag:"OPEN_CHANGE_PASSWORD_POPUP"},function(result){
        $("#ajaxLogin").html(result);
    });
}
function changeForwarderPasswordNew()
{
    var value=$("#changePassword").serialize();
    var newvalue=value+"&flag=CHANGE_PASSWORD_SUBMIT";
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_allProfileDetail.php",newvalue,function(result){
        $("#ajaxLogin").html(result);
    }); 
}
 
function add_more_cargo_quick_quote(mode)
{  
    var  szShipmentType = mode;
    var number = document.getElementById('hiddenPosition_'+mode).value;  
    var table_id = document.getElementById('horizontal-scrolling-div-id_'+mode); 
    number=parseInt(number); 
    var hidden1 = document.getElementById('hiddenPosition1_'+mode).value; 
    document.getElementById('hiddenPosition1_'+mode).value=parseInt(hidden1)+1; 
    document.getElementById('hiddenPosition_'+mode).value =  parseInt(number)+1; 
        
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php",{number:number,shipment_type:szShipmentType,mode:'ADD_MORE_QUICK_QUOTE'},function(result){ 
        $("#add_booking_quote_container_"+number+"_"+mode).html(result); 
        $("#add_booking_quote_container_"+number+"_"+mode).attr("style",'display: block;');  
        $("#add_booking_quote_container_"+number+"_"+mode).addClass('request-quote-fields');  
        
        var active_counter=$('input.first-cargo-fields_V_'+mode).length ;  
        if(active_counter>1)
        {
            $("#cargo-line-remove-1-v"+mode).attr('style','opacity:0.4');
            $("#cargo-line-remove-1-v"+mode).removeAttr('onclick');
            $("#cargo-line-remove-1-v"+mode).unbind("click");
        }  
    }); 
    
    var newTdId = 'add_booking_quote_container_'+(number+1)+"_"+mode ;
    if($("#"+newTdId).length)
    {
        console.log("div already exists");
    }
    else
    {
        var newTd=document.createElement('div'); 
        newTd.setAttribute('id', 'add_booking_quote_container_'+(number+1)+"_"+mode); 
        newTd.setAttribute('style', 'display:none;');
        table_id.appendChild(newTd); 
    }
}

function remove_cargo_quick_quote(div_id,mode,number)
{  
    var number = $('#hiddenPosition_'+mode).attr('value'); 
    var hidden1= $('#hiddenPosition1_'+mode).attr('value');
    var hidden_value = parseInt(hidden1)-1;
    var szDivKey = "_V_"+mode; 
    var active_counter=$('input.first-cargo-fields'+szDivKey).length ;  
    if(active_counter>1)
    {
        $('#hiddenPosition_'+mode).attr('value',hidden_value);
        $('#hiddenPosition1_'+mode).attr('value',hidden_value);

        $("#"+div_id).attr("style",'display:none;');
        $("#"+div_id).html(" ");
        
        var active_counter = $('input.first-cargo-fields'+szDivKey).length ;  
        if(active_counter==1)
        {
            $("#cargo-line-remove-1-v"+mode).attr('style','opacity:1');
            $("#cargo-line-remove-1-v"+mode).removeAttr('onclick');
            $("#cargo-line-remove-1-v"+mode).unbind("click");
            $("#cargo-line-remove-1-v"+mode).click(function(){ remove_cargo_quick_quote('add_booking_quote_container_0_'+mode,mode,number); });
        } 
    }
    else
    {
        var field_count = number;   
        $("#iLength"+field_count+szDivKey).val("");
        $("#iWidth"+field_count+szDivKey).val("");
        $("#iHeight"+field_count+szDivKey).val("");
        $("#iWeight"+field_count+szDivKey).val("");
        $("#iQuantity"+field_count+szDivKey).val("");
        
        if(mode=='PALLET')
        {
            $("#szPalletType"+field_count+szDivKey).val("1"); 
            prefill_dimensions(1,field_count+szDivKey);
        } 
    } 
} 

function validateQuickQuoteCargoDetails()
{ 
    var iBreakBulk = $("#szShipmentType_BREAK_BULK").prop('checked');
    var iParcel = $("#szShipmentType_PARCEL").prop('checked');
    var iPallet = $("#szShipmentType_PALLET").prop('checked');
     
    //console.log("Bulk: "+iBreakBulk+ " Parc: "+iParcel+ " Pallet: "+iPallet);
     
    var break_bulk_error = 1;
    var parcel_error = 1;
    var pallet_error = 1;
    var formId = "quick_quote_search_form";
    
    if(iBreakBulk)
    {
        if(isFormInputElementEmpty(formId,"fTotalVolume_BREAK_BULK"))
        {		  
            break_bulk_error++;  
        } 
        if(isFormInputElementEmpty(formId,"fTotalWeight_BREAK_BULK"))
        {		  
            break_bulk_error++;  
        }
    } 
    if(iParcel)
    { 
        $.each($(".field_name_key_PARCEL"), function(field_key){
            
           var id_suffix = this.value;
           if(id_suffix!='')
           {
                var requiredFieldsAry = new Array(); 
                requiredFieldsAry['0'] = 'iLength'+id_suffix;
                requiredFieldsAry['1'] = 'iWidth'+id_suffix;
                requiredFieldsAry['2'] = 'iHeight'+id_suffix;
                requiredFieldsAry['3'] = 'iWeight'+id_suffix;
                requiredFieldsAry['4'] = 'iQuantity'+id_suffix;
                  
                $.each( requiredFieldsAry, function( index, input_fields ){
                    if(isFormInputElementEmpty(formId,input_fields))
                    {		  
                        parcel_error++;  
                    } 
                }); 
            } 
        });
    }
    
    if(iPallet)
    { 
        $.each($(".field_name_key__PALLET"), function(field_key){
            
           var id_suffix = this.value;
           if(id_suffix!='')
           {
                var requiredFieldsAry = new Array(); 
                requiredFieldsAry['0'] = 'iLength'+id_suffix;
                requiredFieldsAry['1'] = 'iWidth'+id_suffix;
                requiredFieldsAry['2'] = 'szPalletType'+id_suffix;
                requiredFieldsAry['3'] = 'iWeight'+id_suffix;
                requiredFieldsAry['4'] = 'iQuantity'+id_suffix;
                  
                $.each( requiredFieldsAry, function( index, input_fields ){
                    if(isFormInputElementEmpty(formId,input_fields))
                    {		  
                        parcel_error++;  
                    } 
                }); 
            } 
        }); 
    }
    
    if(break_bulk_error==1 && parcel_error==1 && pallet_error==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
function enableManualFeeButton(deault_fee)
{
    var requiredFieldsAry = new Array(); 
    if(deault_fee==1)
    { 
        requiredFieldsAry['0'] = 'idCurrency';  
        requiredFieldsAry['1'] = 'fManualFee';
        var formId = 'default_manual_fee_form';
    }
    else
    {
        requiredFieldsAry['0'] = 'idForwarder';
        requiredFieldsAry['1'] = 'szProduct';
        requiredFieldsAry['2'] = 'szCargo';
        requiredFieldsAry['3'] = 'idCurrency';  
        requiredFieldsAry['4'] = 'fManualFee';
        var formId = 'manual_fee_form';
    } 
    
    
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {	 
            error++;  
        } 
    }); 
     
    var buttonId = '';
    if(deault_fee==1)
    {
       buttonId = 'default_manual_fee_save_button';
    }
    else
    {
       buttonId = 'manual_fee_add_button'; 
    }
    if(parseInt(error)==1)
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:1');
        $("#"+buttonId).click(function(){ submitManualFeeForm(formId,deault_fee); });
    }
    else
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:0.4;');
    } 
} 
function submitManualFeeForm(formId,default_fee)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php",$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||");
        if(default_fee==1)
        {
            $("#default_manual_fee_container").html(resArr[1]);  
        }
        else
        {
            if(resArr[0]=='SUCCESS')
            {
                $("#manual_princing_listing").html(resArr[1]);  
            }
            else if(resArr[0]=='ERROR')
            {
                $("#manual_fee_form_container").html(resArr[1]); 
            }
        } 
    });
}

function select_manual_fee_tr(tr_id,id)
{
    var rowCount = $('#manual_fee-table tr').length;
    if(rowCount>0)
    {
        for(x=1;x<rowCount;x++)
        {
            var new_tr_id="manual_fee_"+x;
            if(new_tr_id!=tr_id)
            {
                $("#"+new_tr_id).attr('style','');
                $("#"+new_tr_id).removeClass('selected-row');
            }
            else
            {
                //'background:#DBDBDB;color:#828282;'
                $("#"+tr_id).addClass('selected-row');
            }
        }
    } 

    $("#manual_fee_add_button").unbind("click");
    $("#manual_fee_add_button").click(function(){ manual_fee_standard_function(id,"EDIT_MANUAL_FEE");  }); 
    $("#manual_fee_add_button").attr("style","opacity:1"); 
    $("#manual_fee_add_button_span").html('Edit');
    
    $("#manual_fee_delete_button").unbind("click");
    $("#manual_fee_delete_button").click(function(){ manual_fee_standard_function(id,"DELETE_MANUAL_FEE");  }); 
    $("#manual_fee_delete_button").attr("style","opacity:1"); 
}

function manual_fee_standard_function(manual_fee_id,mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php",{manual_fee_id:manual_fee_id,mode:mode},function(result){
        var resArr=result.split("||||"); 
        if(mode=='DELETE_MANUAL_FEE')
        {
            $("#manual_princing_listing").html(resArr[1]);  
        }
        else if(mode=='EDIT_MANUAL_FEE')
        {
            $("#manual_fee_form_container").html(resArr[1]); 
        } 
    });
} 

function enableBookingButtons(cb_id,szServiceID,convert_rfq_flag)
{
    var cb_flag = false;
    if(convert_rfq_flag==1)
    {
        cb_flag = true;
    }
    else
    {
        cb_flag = $("#"+cb_id).prop('checked');
    } 
    var szFormID = 'quick_quote_shipper_consignee_form';
     
    var standardFieldAry = new Array();
    standardFieldAry[0] = 'szAllServieIDS';
    standardFieldAry[0] = 'szToken'; 
    
    $.each(standardFieldAry, function(index, input_fields ){ 
        var szFieldId = input_fields+"_BI_"+szServiceID+""; 
        removeHiddenFields(szFieldId);
    }); 
    
    if(cb_flag)
    {
        $.each(standardFieldAry, function( index, input_fields ){ 
            var szFieldName = "quickQuoteShipperConsigneeAry["+input_fields+"]";
            var szFieldId = input_fields+"_BI";
            var szValue = $("#"+input_fields).val();
            addHiddenFields(szFormID, szFieldName, szFieldId, szValue);
        }); 
    }
    
    if(szServiceID!="")
    {
        var createFieldAry = new Array();
        createFieldAry[0] = 'idTransportMode';
        createFieldAry[1] = 'idManualFeeCurrency';
        createFieldAry[2] = 'fTotalManualFee';
        createFieldAry[3] = 'idShipmentType'; 

        $.each(createFieldAry, function(index, input_fields ){ 
            var szFieldId = input_fields+"_BI_"+szServiceID+""; 
            removeHiddenFields(szFieldId);
        }); 

        var szServiceField = "szServiceIds_BI_"+szServiceID;
        removeHiddenFields(szServiceField);

        if(cb_flag)
        {
            $.each(createFieldAry, function( index, input_fields ){ 
                var szFieldName = "quickQuoteShipperConsigneeAry["+input_fields+"]["+szServiceID+"]";
                var szFieldId = input_fields+"_BI_"+szServiceID+"";
                var szValue = $("#"+input_fields+"_"+szServiceID).val();
                addHiddenFields(szFormID, szFieldName, szFieldId, szValue);
            });  
 
            if(convert_rfq_flag!=1)
            {
                var szFieldName = "quickQuoteShipperConsigneeAry[szServiceIds][]";
                var szFieldId = szServiceField;
                var szValue = szServiceID;
                addHiddenFields(szFormID, szFieldName, szFieldId, szValue); 
            } 
        }  
    } 
    
    if(convert_rfq_flag==1)
    {
        return true;
    }
    else
    {
        var counter = check_multiple_selection();
        counter = parseInt(counter); 
        if(counter>0)
        {
            $("#quote_result_send_quote_button").unbind("click");
            $("#quote_result_send_quote_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','SEND_QUOTE'); }); 
            $("#quote_result_send_quote_button").attr("style","opacity:1"); 
            //display_booking_info('SEND_QUOTE');
        }
        else
        {
            $("#quote_result_send_quote_button").unbind("click");
            $("#quote_result_send_quote_button").attr("style","opacity:0.4;"); 
        } 
        if(counter==1)
        {
            $("#quote_result_make_booking_button").unbind("click");
            $("#quote_result_make_booking_button").click(function(){ submitSearchResultForm('quick_quote_shipper_consignee_form','CREATE_BOOKING'); }); 
            $("#quote_result_make_booking_button").attr("style","opacity:1"); 
        }
        else
        {
            $("#quote_result_make_booking_button").unbind("click");
            $("#quote_result_make_booking_button").attr("style","opacity:0.4;"); 
        }
    } 
} 

function display_booking_info(type)
{ 
    if(type=='CREATE_BOOKING')
    {
        $("#quick_quote_make_booking_container").attr('style','display:block;');
        $("#quick_quote_send_quote_container").attr('style','display:none;');
    }
    else
    {
        $("#quick_quote_make_booking_container").attr('style','display:none;');
        $("#quick_quote_send_quote_container").attr('style','display:block;');
    }
    close_open_div('booking_information_container','pending_task_overview_open_close_link','open');
    validateQQBookingInformation(1);
}
function check_multiple_selection(convert_rfq_quote)
{ 
    var counter = 0;
    var bNoRecordFound = 1;
    $.each($(".quick-quote-service-cb"), function(e){
        if(convert_rfq_quote==1)
        {
            var cb_id = this.id;
            var szServiceID = this.value;
            bNoRecordFound++;
            enableBookingButtons(cb_id,szServiceID,1);
        }
        else
        {
            var cb_id = this.id;
            var cb_flag = $("#"+cb_id).prop('checked');
            if(cb_flag)
            {
                counter++;
            } 
        } 
    }); 
    
    if(bNoRecordFound==1 && convert_rfq_quote==1)
    {
        enableBookingButtons(false,"",1);
    }
    return counter;
}
function validateQuickQuoteCargo(szShipmentType,counter)
{ 
    var requiredFieldsAry = new Array(); 
    var formId = 'quick_quote_search_form';
    if(szShipmentType=='BREAK_BULK')
    { 
        requiredFieldsAry['0'] = 'fTotalVolume_'+szShipmentType;  
        requiredFieldsAry['1'] = 'fTotalWeight_'+szShipmentType; 
    }
    else
    {
        requiredFieldsAry['0'] = 'iLength'+counter+"_V_"+szShipmentType; 
        requiredFieldsAry['1'] = 'iWidth'+counter+"_V_"+szShipmentType;  
        requiredFieldsAry['2'] = 'iQuantity'+counter+"_V_"+szShipmentType; 
        requiredFieldsAry['3'] = 'iWeight'+counter+"_V_"+szShipmentType; 
        if(szShipmentType=='PARCEL')
        {
            requiredFieldsAry['4'] = 'iHeight'+counter+"_V_"+szShipmentType; 
        }
    } 
    
    
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {	 
            $("#"+input_fields).addClass('red_border');
            error++;  
        } 
    }); 
    return error;
}

function submitSearchResultForm(formId,mode)
{
    var serialized_form_data = "";
    if(mode=='CONVERT_RFQ')
    {
        var counter = check_multiple_selection();
        counter = parseInt(counter);  
        if(counter==0)
        { 
            check_multiple_selection(1);
            
            var szAllServieIDS = $("#szAllServieIDS").val();
            $("#szAllServieIDS_BI").val(szAllServieIDS);
        } 
    }  
    if(mode=='CONVERT_RFQ_CONFIRM')
    {
        var szInternalComment = $("#szInternalComment_popup").val();
        $("#szInternalComment_BI").val(szInternalComment);
         
        var idPackingType =$('#idPackingType_popup :selected').val();
        $("#idPackingType_BI").val(idPackingType); 
         
        var serialized_form_1 = $("#quick_quote_shipper_consignee_form").serialize(); 
        serialized_form_data = serialized_form_1;
        $("#quote_quote_convert_rfq_confirm_button").addClass('get-rates-btn-clicked');
    } 
    else
    {
        serialized_form_data = $("#quick_quote_shipper_consignee_form").serialize();
    }
    
    if(mode=='SEND_QUOTE_CONFIRM' || mode=='CREATE_BOOKING_CONFIRM')
    { 
        $("#loader").attr('style','display:block;');
        if(mode=='SEND_QUOTE_CONFIRM')
        {
            var content = NiceEditorInstance.instanceById('szEmailBody_SEND_QUOTE').getContent();
            var description = encode_string_msg_str(content);
            serialized_form_data = serialized_form_data + "&quickQuoteShipperConsigneeAry[SEND_QUOTE][szEmailBody]="+description; 
        } 
        else
        {
            var content = NiceEditorInstance.instanceById('szEmailBody_CREATE_BOOKING').getContent();
            var description = encode_string_msg_str(content);
            serialized_form_data = serialized_form_data + "&quickQuoteShipperConsigneeAry[CREATE_BOOKING][szEmailBody]="+description; 
        }
    }
    
    /*
    * Adding little wheel on click of following buttons
    */
    if(mode=='SEND_QUOTE')
    { 
        $("#quote_result_send_quote_button").addClass('get-rates-btn-clicked');
    }
    else if(mode=='CREATE_BOOKING')
    {
        $("#quote_result_make_booking_button").addClass('get-rates-btn-clicked');
    } 
    else if(mode=='SEND_QUOTE_CONFIRM')
    {
        $("#quick_quote_send_quote_button").addClass('get-rates-btn-clicked');
    }
    else if(mode=='CREATE_BOOKING_CONFIRM')
    {
        $("#quick_quote_create_booking_button").addClass('get-rates-btn-clicked');
    } 
    
    $("#quick_quote_send_quote_container").attr('style','display:none');
    $("#quick_quote_make_booking_container").attr('style','display:none');
    
    close_open_div('booking_information_container','pending_task_overview_open_close_link','close',1);
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php?mode="+mode,serialized_form_data,function(result){
        var resArr=result.split("||||");
        if(mode=='CONVERT_RFQ')
        {
            if(resArr[0]=='SUCCESS')
            {
                $("#quick_quote_booking_popup").html(resArr[1]);  
                $("#quick_quote_booking_popup").attr('style','display:block;');  
            } 
        }
        else if(mode=='CONVERT_RFQ_CONFIRM')
        {
            if(resArr[0]=='SUCCESS')
            {
                redirect_url(resArr[1]);
            }
            else if(resArr[0]=='ERROR')
            {
                $("#quick_quote_booking_popup").html(resArr[1]); 
            }
        } 
        else if(mode=='SEND_QUOTE' || mode=='CREATE_BOOKING')
        {
            if(resArr[0]=='SUCCESS')
            {
                //$("#quick_quote_booking_infomation_container").html(resArr[1]); 
                $("#quick-quote-email-box-container").html(resArr[1]);  
                close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1,1);
                
                $(".quick-quote-action-button").unbind('click'); 
                $(".quick-quote-action-button").attr("style","opacity:0.4;"); 
                
                if(mode=='CREATE_BOOKING')
                {
                    $("#insurance_dropdoen_SEND_QUOTE").attr("style","display:none;"); 
                    $("#insurance_dropdoen_CREATE_BOOKING").attr("style","display:block;"); 
                }
            } 
            else
            {
                $("#quick_quote_booking_infomation_container").html(resArr[1]); 
                close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1,1);
            }
            validateQQBookingInformation(1);
            autofill_billing_address__new_rfq(1);
            buildQuickQuoteEmail(mode); 
        }
        else if(mode=='SEND_QUOTE_CONFIRM' || mode=='CREATE_BOOKING_CONFIRM')
        {
            
            if(resArr[0]=='SUCCESS')
            {
                if(mode=='SEND_QUOTE_CONFIRM')
                {  
                    //$("#quick_quote_booking_infomation_container").html(resArr[1]);  
                    //$("#quick-quote-email-box-container").html(resArr[1]);
                    redirect_url(resArr[1]);
                }
                else
                {
                    redirect_url(resArr[1]);
                } 
            } 
            else if(resArr[0]=='ERROR')
            {
                $("#quick_quote_booking_infomation_container").html(resArr[1]); 
                $("#booking_information_container").attr('style','display:block;');
                $("#loader").attr('style','display:none;');
                //close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1);
            }
        } 
        /*
        * Removing little wheel on click of following buttons
        */
        if(mode=='SEND_QUOTE')
        { 
            $("#quote_result_send_quote_button").removeClass('get-rates-btn-clicked');
        } 
        else if(mode=='CREATE_BOOKING')
        {
            $("#quote_result_make_booking_button").removeClass('get-rates-btn-clicked');
        }
        else if(mode=='SEND_QUOTE_CONFIRM')
        {
            $("#quote_result_make_booking_button").removeClass('get-rates-btn-clicked');
        }
        else if(mode=='CREATE_BOOKING_CONFIRM')
        {
            $("#quick_quote_create_booking_button").removeClass('get-rates-btn-clicked');
        } 
    });
}  
function removeHiddenFields(szFieldId) 
{
    if($("#"+szFieldId).length)
    {
        $("#"+szFieldId).remove();
    }
}

function addHiddenFields(szFormID, szFieldName, szFieldId, szValue) 
{
    removeHiddenFields(szFieldId); 
    // Create a hidden input element, and append it to the form:
    var input = document.createElement('input');
    input.type = 'hidden';
    input.id = szFieldId;
    input.name = szFieldName;
    input.value = szValue;
    $("#"+szFormID).append(input);
}

function setPaypalCountries()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'__OPEN_PAYPAL_COUNRTY_POPUP__'},function(result){
        $("#ajaxLogin").html(result);
        $("#ajaxLogin").attr('style','display:block');
    });
}
 
function checkCountryAlreadyAdded(idCountry)
{
    if(jQuery.trim(idCountry)=='')
    {
        $("#add_button_paypal").attr('style','opacity:0.4');
         $("#add_button_paypal").unbind("click");
    }
    else
    {
        $("#add_button_paypal").attr('style','opacity:0.4');
         $("#add_button_paypal").unbind("click");
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'CHECK_COUNTRY_ALREADY_EXISTS',idCountry:idCountry},function(result){
            if(jQuery.trim(result)=='SUCCESS')
            {
                $("#add_button_paypal").attr('style','opacity:1');
                $("#add_button_paypal").unbind("click");
                $("#add_button_paypal span").html('Add');
                $("#add_button_paypal").click(function(){ addToPaypalCountryList(idCountry); });
            }
            else
            {
                $("#add_button_paypal").attr('style','opacity:1');
                $("#add_button_paypal").unbind("click");
                $("#add_button_paypal span").html('Remove');
                $("#add_button_paypal").click(function(){ removeFromPaypalCountryList(idCountry); });
            }
        });
    }
} 
function addToPaypalCountryList(idCountry)
{
    $("#add_button_paypal").attr('style','opacity:0.4');
    $("#add_button_paypal").unbind("click");
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'ADD_TO_PAYPAL_COUNTRY_LIST',idCountry:idCountry},function(result){
        $("#ajaxLogin").html(result);
        $("#ajaxLogin").attr('style','display:block');
    });
}

function removeFromPaypalCountryList(idCountry)
{
    $("#add_button_paypal").attr('style','opacity:0.4');
    $("#add_button_paypal").unbind("click");
    $("#idCountry").attr('disabled',true);
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'REMOVE_FROM_PAYPAL_COUNTRY_LIST',idCountry:idCountry},function(result){
        $("#ajaxLogin").html(result);
        $("#ajaxLogin").attr('style','display:block');
    }); 
}

function selectPaypalCountry(idCountry)
{
    var divid="sel_paypal_"+idCountry;
    $(".selected-row").removeClass("selected-row");
    $("#"+divid).addClass("selected-row");
    $("#add_button_paypal").attr('style','opacity:1');
    $("#add_button_paypal").unbind("click");
    $("#add_button_paypal span").html('Remove');
    $("#add_button_paypal").click(function(){ removeFromPaypalCountryList(idCountry); });
}

function buildQuickQuoteEmail(type)
{
    if(type=='SEND_QUOTE' || type=='CREATE_BOOKING')
    { 
        var idLanguage = $("#iBookingLanguage_"+type).val(); 
        //var szEmailBody = $("#szEmailBody_"+type).val(); 
        var szEmailSubject = $("#szEmailSubject_"+type).val(); 
        
        var emailbody_id = "szEmailBody_"+type;
        var szEmailBody = NiceEditorInstance.instanceById(emailbody_id).getContent();
          
        var szFirstName = $("#szFirstName").val();
        var szLastName = $("#szLastName").val();
        var szCargoDescription = $("#szCargoDescription").val();
        
        var szOriginPostcode = $("#szOriginPostcode").val();
        var szOriginCity = $("#szOriginCity").val();
        var idOriginCountry = $("#idOriginCountry").val();
        
        var szDestinationPostcode = $("#szDestinationPostcode").val();
        var szDestinationCity = $("#szDestinationCity").val();
        var idDestinationCountry = $("#idDestinationCountry").val();
 
        var szOriginCountryName = "";
        var szDestinationCountryName = "";
        if(idOriginCountry>0 && idLanguage>0)
        {
            szOriginCountryName = CountryLanguageListAry[idLanguage][idOriginCountry]['szCountryName'];
        } 
        if(idDestinationCountry>0 && idLanguage>0)
        {
            szDestinationCountryName = CountryLanguageListAry[idLanguage][idDestinationCountry]['szCountryName'];
        }
        
        var szFromLocation = "";
        if(szOriginPostcode!='')
        {
            szFromLocation = szOriginPostcode+ " "+szOriginCity;
        }
        else
        {
            szFromLocation = szOriginCity;
        }
        szFromLocation = szFromLocation + ", " + szOriginCountryName ;
        
        var szToLocation = "";
        if(szDestinationPostcode!='')
        {
            szToLocation = szDestinationPostcode+ " "+szDestinationCity;
        }
        else
        {
            szToLocation = szDestinationCity;
        }
        szToLocation = szToLocation + ", "+szDestinationCountryName ;
        
        /*
        var replaceAry = new Array(); 
        replaceAry['szFirstName'] = jQuery.trim(szFirstName);
        replaceAry['szLastName'] = jQuery.trim(szLastName); 
        replaceAry['szCargoDescription'] = jQuery.trim(szCargoDescription); 
        replaceAry['szFromLocation'] = jQuery.trim(szFromLocation);
        replaceAry['szToLocation'] = jQuery.trim(szToLocation);
        */

        if(szCargoDescription!='')
        {
            szCargoDescription = ", "+szCargoDescription;
        }
        
        var replaceAry = {
            'szFirstName' : szFirstName,
            'szLastName' : szLastName,
            'szCargoDescription' : szCargoDescription,
            'szFromLocation' : szFromLocation,
            'szToLocation' : szToLocation
        };

        $.each( replaceAry, function( key, value ){  
            if(isEmpty(value) && value!='')
            { 
                //console.log("Replacing: "+key+ " with "+value);
                //console.log("Data: "+szEmailSubject);
                //alert("Replacing: "+key+ " with "+value);
                szEmailBody = szEmailBody.replace(key, value);
                szEmailSubject = szEmailSubject.replace(key, value); 
            } 
        }); 
 
        //$("#szEmailBody_"+type).val(szEmailBody);
        NiceEditorInstance.instanceById(emailbody_id).setContent(szEmailBody);
        $("#szEmailSubject_"+type).val(szEmailSubject); 
    } 
}

function updateEmailData(idLanguage,type,field_name)
{ 
    if(type=="SEND_QUOTE")
    {
        var szEmailBody = $("#szEmailTemplateBody_"+type+"_"+idLanguage).val();
        var szEmailSubject = $("#szEmailTemplateSubject_"+type+"_"+idLanguage).val();
    
        $("#szEmailSubject_SEND_QUOTE").val(szEmailSubject);
        //$("#szEmailBody_SEND_QUOTE").val(szEmailBody);
        var emailbody_id = "szEmailBody_SEND_QUOTE";
        NiceEditorInstance.instanceById(emailbody_id).setContent(szEmailBody);
    }
    else if(type=='CREATE_BOOKING')
    {
        var szPaymentMode = '';
        var szLanguage = '';
        if(field_name=='PAYMENT_CODE')
        {
            szPaymentMode = idLanguage;
            szLanguage = $("#iBookingLanguage_CREATE_BOOKING").val();
        }
        else
        {
            szLanguage = idLanguage;
            szPaymentMode = $("#szPaymentType_CREATE_BOOKING").val();
        }
        
        var szEmailBody = $("#szEmailTemplateBody_"+type+"_"+szPaymentMode+"_"+szLanguage).val();
        var szEmailSubject = $("#szEmailTemplateSubject_"+type+"_"+szPaymentMode+"_"+szLanguage).val();
        
        $("#szEmailSubject_CREATE_BOOKING").val(szEmailSubject);
        //$("#szEmailBody_CREATE_BOOKING").val(szEmailBody);
        
        var emailbody_id = "szEmailBody_CREATE_BOOKING";
        NiceEditorInstance.instanceById(emailbody_id).setContent(szEmailBody);
    } 
    buildQuickQuoteEmail(type);
}
function prefill_selected_values(szServiceId)
{
    var cb_flag = $("#szServiceIs_"+szServiceId).prop('checked');
    if(cb_flag)
    {
        var cb_id = "szServiceIs_"+szServiceId;
        enableBookingButtons(cb_id,szServiceId);
    }
}

function check_form_field_empty_quick_quote(formId,inputId)
{
    var szBookingType = $("#szBookingType").val();
    
    var szShipperPostcodeRequiredFlag ='';
   var szConsigneePostcodeRequiredFlag ='';
   var szCollectionFlag='';
   var szDeliveryFlag='';
   
    if(szBookingType=='CREATE_BOOKING'){
    $.each($(".quick-quote-service-cb"), function(e){
            var cb_id = this.id;
            
            var cb_flag = $("#"+cb_id).prop('checked');
            if(cb_flag)
            {
                var szServiceID= $("#"+cb_id).attr('value');
                //alert(szServiceID);
                szCollectionFlag = $("#szCollectionFlag_"+szServiceID).val();
                szDeliveryFlag = $("#szDeliveryFlag_"+szServiceID).val();
                szConsigneePostcodeRequiredFlag = $("#szConsigneePostcodeRequiredFlag_"+szServiceID).attr('value');
                //alert(szConsigneePostcodeRequiredFlag+"szConsigneePostcodeRequiredFlag");
                szShipperPostcodeRequiredFlag = $("#szShipperPostcodeRequiredFlag_"+szServiceID).val();
                //alert(szShipperPostcodeRequiredFlag+"szShipperPostcodeRequiredFlag");
            }
            
        });
    }
        
    if(szBookingType=='SEND_QUOTE' || szBookingType=='CREATE_BOOKING')
    {
        if(isFormInputElementEmpty(formId,inputId))
        {		
            if(szBookingType=='SEND_QUOTE')
            {
                /*
                * In case of SEND_QUOTE we only have szFirstName, szLastName and szEmail will be required. 
                */
                if(inputId=='szFirstName' || inputId=='szLastName' || inputId=='szEmail')
                {
                    $("#"+inputId).addClass('red_border'); 
                } 
            }
            else
            {
                if(szCollectionFlag=='No' &&  inputId=='szOriginAddress')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else if(szDeliveryFlag=='No' &&  inputId=='szDestinationAddress')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else if(szShipperPostcodeRequiredFlag=='No' &&  inputId=='szOriginPostcode')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else if(szConsigneePostcodeRequiredFlag=='No' &&  inputId=='szDestinationPostcode')
                {
                    $("#"+inputId).removeClass('red_border');
                }
                else
                {
                    $("#"+inputId).addClass('red_border');
                }
            } 
        }
        else
        {
            $("#"+inputId).removeClass('red_border');
        }
    } 
}


function selectTextEditorNew(value)
{
        var value = $("#select").attr('value'); 
        var idLanguage = $("#idLanguage").attr('value'); 
        if(jQuery.trim(value)!="" && parseInt(idLanguage)>0)
        {	
                //$("#preview").attr({'onclick':'PreviewText('+value+')','style':''});
                $("#save").unbind("click");
                $("#save").click(function(){saveEditTextDetails(value)});
                $('#save').attr({'style':'opacity:'});

                var iLanguage = $("#iLanguage").attr('value'); 
                $.post(__JS_ONLY_SITE_BASE__+"/ajax_textEditor.php",{value:value,iLanguage:iLanguage,mode:'VIEW_DETAILS',idLanguage:idLanguage},function(result){
                                $('#viewContent').html(result);
                                setup();
                                 var id	    = $('#checkId').val();
                                $("#preview").attr({'onclick':'PreviewText('+id+')','style':''});
                                
                        });	
        }
        else
        {	
                $('#viewContent').html('<textarea name="content" id="content" style="width:100%;height:60%;" class="myTextEditor"></textarea>');
                setup();
                $('#save').attr({'onclick':'','style':'opacity:0.4'});
                $("#cancel").attr({'onclick':'','style':'opacity:0.4'});
        }
        $('#previewContent').html('');
        $('#previewContent').css('display','none');
}

function selectLanguageConfigurationData()
{
    var idLanguage=$("#idLanguage").val();
    var idLangConfigType=$("#idLangConfigType").val();
    
    $("#cancel").unbind("click");
    $('#cancel').attr({'onclick':'','style':'opacity:0.4'});
        
    $("#save span").html("Edit");
    
    $("#previewContent").html('');    
    $("#previewContent").attr("style","display:none");
    
    $("#save").unbind("click");
    $('#save').attr({'onclick':'','style':'opacity:0.4'});
          
    if(parseInt(idLanguage)>0 && jQuery.trim(idLangConfigType)!='')
    {
        $("#save").unbind("click");
         $('#save').attr({'onclick':'','style':'opacity:1'});
         $("#save").click(function(){saveEditTextLangConfigData(idLangConfigType,idLanguage)});    
    }
}

function saveEditTextLangConfigData(idLangConfigType,idLanguage)
{
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_languageConfiguration.php",{idLanguage:idLanguage,idLangConfigType:idLangConfigType,mode:'OPEN_EDIT_CONFIGURATION_DATA'},function(result){
            $("#previewContent").html(result);    
            $("#previewContent").attr("style","display:block");
            
            $("#save").unbind("click");
            $('#save').attr({'onclick':'','style':'opacity:1'});
            $("#save").click(function(){saveLangConfigData(idLangConfigType,idLanguage)});
            $("#save span").html("Save");
            
            $("#cancel").unbind("click");
            $('#cancel').attr({'onclick':'','style':'opacity:1'});
            $("#cancel").click(function(){cancelLanguageConfiguration(idLangConfigType,idLanguage)});
            
        });
}

function cancelLanguageConfiguration(idLangConfigType,idLanguage)
{
    $("#save").unbind("click");
    $('#save').attr({'onclick':'','style':'opacity:1'});
    $("#save").click(function(){saveEditTextLangConfigData(idLangConfigType,idLanguage)});
    
    $("#cancel").unbind("click");
    $('#cancel').attr({'onclick':'','style':'opacity:0.4'});

     $("#previewContent").html('');    
     $("#previewContent").attr("style","display:none");
     
     $("#save span").html("Edit");
}


function saveLangConfigData(idLangConfigType,idLanguage)
{
    $("#cancel").unbind("click");
    $('#cancel').attr({'onclick':'','style':'opacity:0.4'});

    $("#save").unbind("click");
    $('#save').attr({'onclick':'','style':'opacity:0.4'});


    var value = $("#languageConfigurationForm").serialize();
    var newValue = value+"&mode=SAVE_LANGUAGE_CONFIGURATION_DATA&idLangConfigType="+idLangConfigType+"&idLanguage="+idLanguage;
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_languageConfiguration.php",newValue,function(result){
            
            $("#previewContent").html(result);    
            $("#previewContent").attr("style","display:block");
            
            $("#save").unbind("click");
            $('#save').attr({'onclick':'','style':'opacity:1'});
            $("#save").click(function(){saveLangConfigData(idLangConfigType,idLanguage)});
            $("#save span").html("Save");
            
            $("#cancel").unbind("click");
            $('#cancel').attr({'onclick':'','style':'opacity:1'});
            $("#cancel").click(function(){cancelLanguageConfiguration(idLangConfigType,idLanguage)});
    });
}

function reload_quick_quote_email_template(booking_type,booking_id)
{ 
    var booking_language = $("#iBookingLanguage_"+booking_type).val();
    var szPaymentType = "";
    if(booking_type=='CREATE_BOOKING')
    {
        szPaymentType = $("#szPaymentType_"+booking_type).val();
        if(szPaymentType=='' || booking_language=='0')
        {
            $("#szEmailSubject_CREATE_BOOKING").val('');
            var emailbody_id = "szEmailBody_CREATE_BOOKING";
            NiceEditorInstance.instanceById(emailbody_id).setContent('');
            return false;
        }
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_quickQuote.php",{mode:'RELOAD_QUICK_QUOTE_EMAIL_TEMPLATE',booking_type:booking_type,booking_id:booking_id,booking_language:booking_language,payment_type:szPaymentType},function(result){
 
        var resArr=result.split("||||");
        if(resArr[0]=='SUCCESS')
        {
            $("#quick-quote-email-box-container").html(resArr[1]);
            buildQuickQuoteEmail(booking_type);
            validateQQBookingInformation();
        } 
    }); 
}


function on_enter_key_press_shift_dummy_service(kEvent,szValue,exception)
{ 
    var idPrimarySortKeyInputId = "";
    var idPrimarySortValueInputId = "";
    var idOtherSortKeyInputId = "";
    
    if(exception==1 || exception==2)
    {
        idPrimarySortKeyInputId = 'idPrimarySortKey_exception';
        idPrimarySortValueInputId = "idPrimarySortValue_exception";
        idOtherSortKeyInputId = "idOtherSortKey"; 
    }
    else
    {
        idPrimarySortKeyInputId = 'idPrimarySortKey';
        idPrimarySortValueInputId = "idPrimarySortValue";
        idOtherSortKeyInputId = "idOtherSortKey";
    }
    var szSortValue=''; 
    var idPrimarySortKey=$("#"+idPrimarySortKeyInputId).val();
    var szSortValue=$("#"+idPrimarySortValueInputId).val(); 
    var idOtherSortKeyArr=$("#"+idOtherSortKeyInputId).attr('value'); 
     
    if (kEvent.shiftKey) 
    { 
        if(idPrimarySortKey==szValue)
        {
            if(szSortValue=='ASC')
            {
                szSortValue='DESC';
            }
            else
            {
                szSortValue='ASC';
            } 
        }  
        if(idPrimarySortKey=='')
        {
            $("#"+idPrimarySortKeyInputId).attr('value',szValue);
            $("#"+idPrimarySortValueInputId).attr('value',szSortValue);
        }
        if(idPrimarySortKey==szValue)
     	{
            $("#"+idPrimarySortValueInputId).attr('value',szSortValue);
     	}
     	else
     	{
     	
            if(idOtherSortKeyArr=='')
            {
                var ascValue=szValue+"_ASC";
                $("#"+idOtherSortKeyInputId).attr('value',ascValue);				
            }
            else
            {
                var descValue=szValue+"_DESC";
                var ascValue=szValue+"_ASC";


                var res_ary_new = idOtherSortKeyArr.split(";");
                var add_truck=jQuery.inArray(descValue, res_ary_new);
                if(add_truck!='-1')
                {			
                    res_ary_new = $.grep(res_ary_new, function(value) {
                        return descValue != value;
                    });

                    var value1=res_ary_new.join(';');
                    $("#"+idOtherSortKeyInputId).attr('value',value1);
                    var idOtherSortKeyArr=$("#"+idOtherSortKeyInputId).val();
                    if(idOtherSortKeyArr=='')
                    {
                            $("#"+idOtherSortKeyInputId).attr('value',ascValue);
                    }
                    else
                    {
                            var value1=idOtherSortKeyArr+';'+ascValue;
                            $("#"+idOtherSortKeyInputId).attr('value',value1);
                    }
                }
                else
                {
                    var add_truck=jQuery.inArray(ascValue, res_ary_new);
                    if(add_truck!='-1')
                    {			
                        res_ary_new = $.grep(res_ary_new, function(value) {
                            return ascValue != value;
                        });

                        var value1=res_ary_new.join(';');
                        $("#"+idOtherSortKeyInputId).attr('value',value1);
                        var idOtherSortKeyArr=$("#"+idOtherSortKeyInputId).val();
                        if(idOtherSortKeyArr=='')
                        {
                            $("#"+idOtherSortKeyInputId).attr('value',descValue);
                        }
                        else
                        {
                            var value1=idOtherSortKeyArr+';'+descValue;
                            $("#"+idOtherSortKeyInputId).attr('value',value1);
                        }
                    }
                    else
                    {
                        var value1=idOtherSortKeyArr+';'+ascValue;
                        $("#"+idOtherSortKeyInputId).attr('value',value1);
                    }
                }
            }
        } 
    } 
    else 
    { 
        if(idPrimarySortKey==szValue)
        {
            if(szSortValue=='ASC')
            {
                szSortValue='DESC';
            }
            else
            {
                szSortValue='ASC';
            } 
            $("#"+idPrimarySortValueInputId).attr('value',szSortValue);
            $("#"+idOtherSortKeyInputId).attr('value','');
         }
         else
         {
	     
            $("#"+idPrimarySortKeyInputId).attr('value',szValue);
            $("#"+idPrimarySortValueInputId).attr('value','ASC');
            $("#"+idOtherSortKeyInputId).attr('value','');
        }
    } 
    var idPrimarySortKey=$("#"+idPrimarySortKeyInputId).val();
    var szSortValue=$("#"+idPrimarySortValueInputId).val();
    var idOtherSortKeyArr=$("#idOtherSortKey").val();
    var iServiceDummy=$("#iServiceDummy").val();
     
    var mode = "";
    if(exception==1)
    {
        mode = 'SORT_DUMMY_SERVICES_EXCEPTION';
    }
    else if(exception==2)
    {
        mode = 'SORT_RAIL_TRANSPORT_SERVICES';
    }
    else
    {
        mode = 'SORT_DUMMY_SERVICES';
    } 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",{mode:mode,idOtherSortKeyArr:idOtherSortKeyArr,szSortValue:szSortValue,idPrimarySortKey:idPrimarySortKey,iServiceDummy:iServiceDummy},function(result){
            
        if(exception==1)
        {
            $("#dummy_serviceexception_main_container").html(result);
            $("#deleteDummyServiceExceptionId").attr('value','');
        }
        else if(exception==2)
        {
            $("#rail_transport_main_listing_container_div").html(result);
            $("#railTransportIds").attr('value','');
        }
        else
        {
            $("#dummy_service_main_container").html(result);
            $("#deleteTruckiconId").attr('value','');
            
            $(".moe-transport-sort-span").removeClass('sort-arrow-up');
            $(".moe-transport-sort-span").removeClass('sort-arrow-down');
            var szValueArr=idPrimarySortKey.split(".");
            if(szSortValue=='ASC')
            {
                $("#moe-transport-sort-span-id-"+szValueArr[1]).addClass('sort-arrow-up');
            }
            else
            { 
                $("#moe-transport-sort-span-id-"+szValueArr[1]).addClass('sort-arrow-down');
            }
            
        }  
    }); 
}

function enableDummyServiceButton(idDummyService)
{
    var requiredFieldsAry = new Array();  
    requiredFieldsAry['0'] = 'idCountry';
    requiredFieldsAry['1'] = 'idTrade';
    requiredFieldsAry['2'] = 'idProduct';
    requiredFieldsAry['3'] = 'fPriceIncrease';  
    requiredFieldsAry['4'] = 'iTransitTimeIncrease';
    var formId = 'dummy_service_form';  
    
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {	 
            error++;  
        } 
    }); 
      
    var buttonId = 'dummy_services_add_button';  
    if(parseInt(error)==1)
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:1');
        $("#"+buttonId).click(function(){ submitDummyServicesForm(formId); });
    }
    else
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:0.4;');
    } 
}
function submitDummyServicesForm(formId)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#dummy_service_main_container").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#dummy_service_form_container").html(resArr[1]); 
        } 
    });
} 
function clearDummyServicesForm(mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",{mode:mode},function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#dummy_service_form_container").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#dummy_service_form_container").html(resArr[1]); 
        } 
    });
}


function deleteDummyServiceData()
{
    var deleteDummyServiceId=$("#deleteDummyServiceId").val();
    
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",{deleteDummyServiceId:deleteDummyServiceId,mode:'DELETE_DAMMY_SERVICE'},function(result){
        $("#dummy_service_main_container").html(result);
    });
}

function editDummyServiceData()
{
    var idDummyService=$("#deleteDummyServiceId").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",{idDummyService:idDummyService,mode:'EDIT_DAMMY_SERVICE'},function(result){
        $("#dummy_service_form_container").html(result);
    });
}

function cancelDummyServicesForm(mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",{mode:mode},function(result){
        
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#dummy_service_form_container").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#dummy_service_form_container").html(resArr[1]); 
        }
        
        $("#dummy_services_add_button").unbind("click");
        $("#dummy_services_add_button").click(function(){editDummyServiceData();});
        $("#dummy_services_add_button").attr("style",'opacity:1');
        $("#dummy_services_add_button_span").html("Edit"); 
        
        $("#dummy_services_clear_button").unbind("click");
        $("#dummy_services_clear_button").click(function(){deleteDummyServiceData();});
        $("#dummy_services_clear_button").attr("style",'opacity:1'); 
        $("#dummy_services_clear_button span").html("Delete");
    });
}

function enableDummyServiceExceptionButton(idDummyService)
{
    var requiredFieldsAry = new Array();  
    requiredFieldsAry['0'] = 'idOriginCountry';
    requiredFieldsAry['1'] = 'idDestinationCountry';
    requiredFieldsAry['2'] = 'idProduct'; 
    var formId = 'dummy_service_exception_form';  
    
    var error = 1;
    $.each( requiredFieldsAry, function( index, input_fields ){
        if(isFormInputElementEmpty(formId,input_fields))
        {	  
            error++;  
        }
    }); 
      
    var buttonId = 'dummy_services_exception_add_button';    
    if(parseInt(error)==1)
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:1');
        $("#"+buttonId).click(function(){ submitDummyServicesExceptionForm(formId); });
    }
    else
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:0.4;');
    } 
} 

function submitDummyServicesExceptionForm(formId)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#dummy_serviceexception_main_container").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#dummy_service_exception_form_container").html(resArr[1]); 
        } 
    });
}  
function deleteDummyServiceDataException()
{
    var deleteDummyServiceExceptionId = $("#deleteDummyServiceExceptionId").val(); 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_dummyService.php",{deleteDummyServiceExceptionId:deleteDummyServiceExceptionId,mode:'DELETE_DUMMY_SERVICE_EXCEPTION'},function(result){
        $("#dummy_serviceexception_main_container").html(result);
    });
}

function preview_gmail_data(gmail_id)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'PREVIEW_GMAIL_INBOX_DATA',gmail_id:gmail_id},function(result){ 
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        { 
            $("#message_data_"+gmail_id).removeClass('unread-email');
            $("#show_message_preview").html(resArr[1]);  
        } 
    });
}
function sync_transporteca_gmail()
{
    $("#sync_with_gmail_button").addClass('get-rates-btn-clicked');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'SYNC_WITH_GMAIL'},function(result){
        
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#gmail_inbox_mail_container").html(resArr[1]);  
            $("#show_message_preview").html("");
        }
        $("#sync_with_gmail_button").removeClass('get-rates-btn-clicked');
    });
}
function resetForm(formId)
{
    if(formId)
    {
        //$("#"+formId).get().reset();
        // iterate over all of the inputs for the form
        // element that was passed in
        $(':input', "#"+formId).each(function() {
          var type = this.type;
          var tag = this.tagName.toLowerCase(); // normalize case
          // it's ok to reset the value attr of text inputs,
          // password inputs, and textareas
          if (type == 'text' || type == 'password' || tag == 'textarea')
            this.value = "";
          // checkboxes and radios need to have their checked state cleared
          // but should *not* have their 'value' changed
          else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
          // select elements need to have their 'selectedIndex' property set to -1
          // (this works for both single and multiple select elements)
          else if (tag == 'select')
          {
            $(this).val("");
          } 
        });
    }
}

function display_chat_transcript(chat_id)
{
     /*
     * At first time when user clicks on 'View Details' link then we load data by ajax call. Data is loaded then on click of this link we just toggling the divs display
     */
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'DISPLAY_CHAT_TRANSCRIPT',chat_id:chat_id},function(result){
        
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
           var div_id = "zopim_chat_transcript_"+chat_id;
           $("#"+div_id).html(result_ary[1]);		
           $("#"+div_id+"_link").removeAttr('onclick');
           showHidechatmessage(div_id,"SHOW"); 
        } 
    });
}

function close_cancel_booking_popup_already_cancelled()
{
    var limit =$("#limit").val();
    var page =$("#page").val();
    var szBookingStatusFlag=$("#szBookingStatusFlag").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_cancelBooking.php",{szBookingStatusFlag:szBookingStatusFlag,limit:limit,page:page,flag:'CUSTOMER_BOOKING_LIST_PAGINATION'},function(result){
	
     if(szBookingStatusFlag=='InsuranceConfirmed' || szBookingStatusFlag=='InsuranceCancelled')
     {
         $("#insured_booking_list_container").html(result)  
     }
     else if(szBookingStatusFlag=='SettledPaymentDetails' || szBookingStatusFlag=='paidForwarderInvoice' || szBookingStatusFlag=='payForwarderInvoice')
     {
         $("#checkDetails").html(result) 
     }
     else
     {    
         $("#cance_booking_html").html(result);
      }  
      $("#cancel_booking_popup").attr('style','display:none');
    });
}

function showHidechatmessage(div_id,type)
{ 
    if(type=='SHOW')
    { 
        $("#"+div_id).attr('style','display:block;');
        
        $("#"+div_id+"_link").unbind("click"); 
        $("#"+div_id+"_link").click(function(){ showHidechatmessage(div_id,'HIDE'); });
    }
    else
    { 
        $("#"+div_id).attr('style','display:none;');
        
        $("#"+div_id+"_link").unbind("click"); 
        $("#"+div_id+"_link").click(function(){ showHidechatmessage(div_id,'SHOW'); });
    }
}


function closeTask(file_id)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{file_id:file_id,mode:'CONFIRM_CLOSE_BOOKING_FILE'},function(result){
	
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            //$("#pending_task_listing_container").html(result_ary[1]);		
            updatePendingTrayListing(result_ary[1]);
            $("#pending_task_listing_container").attr('style','display:block;');
            
            $("#pending_task_overview_main_container").html("");
            $("#pending_task_overview_main_container").attr('style','display:none;');  

            $("#pending_task_tray_main_container").html("");
            $("#pending_task_tray_main_container").attr('style','display:none;');  
            $("#idLastClikedBookingFile").val("");
        } 
    });
}

function enableSearchUserListButton(formId)
{ 
    var estimate_error = 1;
    console.log(formId+'Form Id');
    if(isFormInputElementEmpty(formId,'idFromCountry')) 
    {		
        //console.log('From Country'); 
        estimate_error = estimate_error + 1;  
        //$("#idFromCountry").addClass('red_border');
    }
    if(isFormInputElementEmpty(formId,'idToCountry')) 
    {		
        //console.log('To Country'); 
        estimate_error = estimate_error + 1;  
        //$("#szFromContains").addClass('red_border');
    } 
    if(isFormInputElementEmpty(formId,'dtSearch')) 
    {		
        //console.log('Days History'); 
        estimate_error = estimate_error + 1;  
        //$("#szFromContains").addClass('red_border');
    }
    
    if(estimate_error==1)
    {
        $("#search_user_list_download").unbind("click");
        $("#search_user_list_download").click(function(){downloadSearchUserListData();});
        $("#search_user_list_download").attr("style",'opacity:1');
        
        $("#search_user_list_download_clear").unbind("click");
        $("#search_user_list_download_clear").click(function(){deleteDummyServiceData();});
        $("#search_user_list_download_clear").attr("style",'opacity:1');
    }
    else
    {
        $("#search_user_list_download").unbind("click");
        $("#search_user_list_download").attr("style",'opacity:0.4');
        
        $("#search_user_list_download_clear").unbind("click");
        $("#search_user_list_download_clear").attr("style",'opacity:0.4');
    }
}

function downloadSearchUserListData()
{
    document.searchUserList.action = __JS_ONLY_SITE_BASE__+"/searchuserlist/";
    document.searchUserList.method = "post"
    document.searchUserList.submit();
}
function enableRailTransportButton()
{
    var requiredFieldsAry = new Array();  
    requiredFieldsAry['0'] = 'idForwarder';
    requiredFieldsAry['1'] = 'idFromWarehouse';
    requiredFieldsAry['2'] = 'idToWarehouse'; 
    var formId = 'railTrasnportAddForm';  
    
    var error = 1;
    $.each( requiredFieldsAry, function(index,input_fields){
        if(isFormInputElementEmpty(formId,input_fields))
        {	 
            error++;  
        }
    }); 
      
    var buttonId = 'dummy_services_add_button';  
    if(parseInt(error)==1)
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:1');
        $("#"+buttonId).click(function(){ submitRailTransportForm(formId); });
    }
    else
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:0.4;');
    } 
}

function updateWarehouseDrodown(forwarder_id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_railTransport.php",{mode:'DISPLAY_WAREHOUSE_DROPDOWN',forwarder_id:forwarder_id},function(result){
        var resArr = result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        { 
            $("#from_warehouse_container").html(resArr[1]);  
            $("#to_warehouse_container").html(resArr[2]);  
        } 
    });
}

function submitRailTransportForm(formId)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_railTransport.php",$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        { 
            $("#rail_transport_main_listing_container_div").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#rail_transport_addedit_data").html(resArr[1]); 
        } 
    });
} 

function clearRailTransportForm(mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_railTransport.php",{mode:mode},function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#rail_transport_addedit_data").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#rail_transport_addedit_data").html(resArr[1]); 
        } 
    });
}

function editRailTransportData(mode)
{
    var idRailTransport = $("#railTransportIds").val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_railTransport.php",{idRailTransport:idRailTransport,mode:mode},function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#rail_transport_addedit_data").html(resArr[1]);  
        }
        if(resArr[0]=='SUCCESS_DELETE')
        {
            $("#rail_transport_main_listing_container_div").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#rail_transport_addedit_data").html(resArr[1]); 
        } 
    });
} 

function remove_crm_messages(id,from_page,booking_file)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{idEmail:id,mode:'REMOVE_CRM_EMAIL_MESSAGE',from_page:from_page,booking_file:booking_file},function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            $("#content_body").html(result);
            $("#content_body").attr("style","display:block;"); 
        } 
    });
}

function submit_crm_email_form()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",$("#remove_crm_email_form").serialize(),function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        { 
            $("#content_body").attr('style','display:none;');
            $(".task-tray-header").removeClass('active-th');
            $("#content_body").html('');
            
            var type = resArr[1];
            $("#task_tray_header_id_"+type).addClass('active-th');
            $("#pending_task_tray_container").html(resArr[2]); 
        }
        else if(resArr[0]=='REFRESH_PENDING_TRAYS')
        {
            $("#content_body").attr('style','display:none;');
            $("#content_body").html('');
            //$("#pending_task_listing_container").html(resArr[1]);
            updatePendingTrayListing(resArr[1]);
            $("#pending_task_listing_container").attr('style','display:block;'); 
            
            $("#pending_task_overview_main_container").html("");
            $("#pending_task_overview_main_container").attr('style','display:none;');  

            $("#pending_task_tray_main_container").html("");
            $("#pending_task_tray_main_container").attr('style','display:none;');  
            $("#idLastClikedBookingFile").val("");
        }
        else if(resArr[0]=='ERROR')
        {
            $("#rail_transport_addedit_data").html(resArr[1]); 
        } 
    });
}

function emptyValue(field_id)
{
    $("#"+field_id).val("");
    updateButtonText();
} 
function updateButtonText()
{ 
    var formId  = 'remove_crm_email_form';
    var inputId = 'szCrmEmail';
    var iErrorCount = 1;
    if(isFormInputElementEmpty(formId,inputId)) 
    {
        $("#remove_crm_email_button_span").html("REMOVE DO NOT FORWARD");
    }
    else
    {
        $("#remove_crm_email_button_span").html("REMOVE AND FORWARD");
        if(!isValidEmail($('form#' + formId + ' #'+ inputId ).val())) 
        {
            iErrorCount++;
        }
    }
    
    if(iErrorCount==1)
    {
        $("#remove_crm_email_button").attr("style","opacity:1;");
        $("#remove_crm_email_button").unbind("click");
        $("#remove_crm_email_button").click(function(){ submit_crm_email_form(); });
    }
    else
    {
        $("#remove_crm_email_button").attr("style","opacity:0.4;");
        $("#remove_crm_email_button").unbind("click");
    }
}

function snooze_crm_email_message(formId,action,email_id)
{
    if(action=='CRM_CLOSE')
    {
        $("#crm_email_buttons_"+email_id+"_CRM_CLOSE").addClass('tyni-loader');
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?action="+action,$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||"); 
        var szCloseOverviewPane = resArr[3];
        if(resArr[0]=='SUCCESS')
        { 
            if(action=='CRM_SNOOZE' || action=='CRM_CLOSE')
            {
                //$("#pending_task_listing_container").html(resArr[1]);	
                updatePendingTrayListing(resArr[1]);
                $("#pending_task_listing_container").attr('style','display:block;'); 

                if(action=='CRM_CLOSE' && szCloseOverviewPane=='CLOSE_OVERVIEW_PANE')
                {
                    $("#pending_task_overview_main_container").html("");
                    $("#pending_task_overview_main_container").attr('style','display:none;');  

                    $("#pending_task_tray_main_container").html("");
                    $("#pending_task_tray_main_container").attr('style','display:none;'); 
                    $("#idLastClikedBookingFile").val("");
                } 
                else if(action=='CRM_SNOOZE')
                {
                    $("#pending_task_overview_main_container").html("");
                    $("#pending_task_overview_main_container").attr('style','display:none;');  

                    $("#pending_task_tray_main_container").html("");
                    $("#pending_task_tray_main_container").attr('style','display:none;');  
                    $("#idLastClikedBookingFile").val("");
                }
                else
                { 
                    $("#content_body").attr('style','display:none;');
                    /*$(".task-tray-header").removeClass('active-th');

                    var type = resArr[2];
                    $("#task_tray_header_id_"+type).addClass('active-th');

                    $('#pending_task_tray_container').html(resArr[3]);*/ 
                    
                    $("#pending_task_overview_main_container").html("");
                    $("#pending_task_overview_main_container").attr('style','display:none;');  

                    $("#pending_task_tray_main_container").html("");
                    $("#pending_task_tray_main_container").attr('style','display:none;');  
                    $("#idLastClikedBookingFile").val("");
                }
            }
            else
            {
                $("#content_body").attr('style','display:none;');
                $(".task-tray-header").removeClass('active-th');

                var type = resArr[1];
                $("#task_tray_header_id_"+type).addClass('active-th');
                $("#pending_task_tray_container").html(resArr[2]); 
            } 
        }
        else if(resArr[0]=='ERROR')
        {
            $("#crm_buttons_main_container_"+email_id).html(resArr[1]); 
        } 
        if(action=='CRM_CLOSE')
        {
            $("#crm_email_buttons_"+email_id+"_CRM_CLOSE").removeClass('tyni-loader');
        }
    });
}

function display_crm_operations(booking_file,from_page,email_id,action,szCrmEmail)
{ 
    var container_div_id = "crm_buttons_operations_main_container_"+email_id;
    var scolling_div_id = "crm_operations_scrolling_div_container_"+email_id;
    $("#loader").attr('style','display:none;');
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{idEmail:email_id,mode:'CRM_BUTTON_OPERATION_DETAILS',from_page:from_page,booking_file:booking_file,action:action,szCrmEmail:szCrmEmail},function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {  
            $("#"+container_div_id).html(resArr[1]);
            $("#"+container_div_id).attr("style","display:block;"); 
            crm_operation_menu_selector(action,email_id);
             
            if(action=='CRM_NOTE')
            {
                $("#szReminderNotes").focus();
                $("#szReminderNotes").focus().selectionEnd;
            }
            else if(action=='CRM_REPLY_ALL' || action=='CRM_REPLY' || action=='CRM_FORWARD' || action=='CRM_NEW_EMAIL')
            { 
                $("#szReminderEmailBody_"+email_id).focus();
                $("#szReminderEmailBody_"+email_id).focus().selectionEnd; 
            } 
            var windowTop = $(window).scrollTop();
            var windowBottom = $(window).scrollTop() + $(window).height();
            
            var divTop = $("#"+container_div_id).offset().top;
            var divBottom = divTop + $("#"+container_div_id).outerHeight();
            
            if(windowTop < divTop && windowBottom > divBottom)
            {
                console.log("In view port ");
            }
            else
            { 
                var scrolling_height = $("#"+scolling_div_id).offset().top - $(window).height();
                
                console.log("Not in view port"+scrolling_height); 
                $('html, body').animate({ scrollTop: scrolling_height }, "100000");   
            }  
            if(action=='CRM_REPLY')
            {
                $("#"+container_div_id+" .nicEdit-main").focus();
            }
            else if(action=='CRM_FORWARD')
            {
                $("#szCustomerEmail_"+email_id).focus();
            }
            else if(action=='CRM_NEW_EMAIL')
            {
                $("#szCustomerEmail_"+email_id).focus();
            }
            else if(action=='CRM_REPLY_ALL')
            {
                $("#"+container_div_id+" .nicEdit-main").focus();
            }    
            if(szCrmEmail!='')
            {
                showHide('content_body');
            }
        } 
        $("#loader").attr('style','display:none;');
    });
}

function crm_operation_menu_selector(action,email_id)
{ 
    $(".crm_email_buttons_"+email_id).removeClass('button1');
    $(".crm_email_buttons_"+email_id).addClass('button2');
    
    $("#crm_email_buttons_"+email_id+"_"+action).removeClass('button2');
    $("#crm_email_buttons_"+email_id+"_"+action).addClass('button1');
    
    if(action=='CRM_REPLY' || action=='CRM_FORWARD' || action=='CRM_NEW_EMAIL')
    {
        $("#crm_email_buttons_"+email_id+"_CRM_PARENT_REPLY_BUTTON").removeClass('button2'); 
        $("#crm_email_buttons_"+email_id+"_CRM_PARENT_REPLY_BUTTON").addClass('button1'); 
        $("#crm_operation_reply_options_container_"+email_id).css('display','none'); 
    }
}

function enableCrmSnoozeButton(formId,operation,email_id)
{ 
    var snooze_count = 1;
    if(isFormInputElementEmpty(formId,'dtReminderDate')) 
    {		  
        snooze_count++;
    } 
    if(isFormInputElementEmpty(formId,'szReminderTime')) 
    {		  
        snooze_count++;
    } 
    if(operation==1)
    {
        var save_count = 1; 
        if(isFormInputElementEmpty(formId,'szReminderNotes')) 
        {		  
            save_count++;
        } 
        if(save_count==1)
        {
            $("#request_reminder_save_button").unbind("click");
            $("#request_reminder_save_button").click(function(){ submit_crm_snooze_form(formId,'SAVE',email_id);});
            $("#request_reminder_save_button").attr("style",'opacity:1');
        }
        else
        {
            $("#request_reminder_save_button").unbind("click");
            $("#request_reminder_save_button").attr("style",'opacity:0.4');
        } 
        var szSnoozeButtonId = "request_reminder_snooze_button";
    }
    
    if(snooze_count==1)
    {
        $("#"+szSnoozeButtonId).unbind("click");
        $("#"+szSnoozeButtonId).click(function(){ submit_crm_snooze_form(formId,'SNOOZE',email_id);});
        $("#"+szSnoozeButtonId).attr("style",'float:left;opacity:1');
    }
    else
    {
        $("#"+szSnoozeButtonId).unbind("click");
        $("#"+szSnoozeButtonId).attr("style",'float:left;opacity:0.4');
    } 
}

function submit_crm_snooze_form(formId,operation_type,email_id)
{    
    var idCrmLogs = email_id;
    if(operation_type=='SEARCH_FILE')
    {
        $("#crm_operation_link_file_search_"+idCrmLogs).addClass('tyni-loader');
        $("#iPageCounter_"+idCrmLogs).val('0');
    }
    else
    {
        $("#loader").attr('style','display:block;');
    } 
    var serialized_form_data = $('#'+formId).serialize();
    
    var divValue="#crm_buttons_operations_container_"+email_id;
    
    if(email_id>0 && $("#szReminderEmailBody_"+email_id).length)
    {
        var content = NiceEditorInstanceCRM[email_id].instanceById('szReminderEmailBody_'+email_id).getContent();
        var description = encode_string_msg_str(content);
        serialized_form_data = serialized_form_data + "&addCrmOperationAry[szReminderEmailBody]="+description;
    }  
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?operation_type="+operation_type,serialized_form_data,function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='ERROR')
        {  
            var email_id = res_ary[1];
            $('#crm_buttons_operations_container_'+email_id).html(res_ary[2]); 
        }
        else if(check=='SUUCESS_LINK')
        {  
            $('#pending_task_searched_list_container_'+idCrmLogs).html(res_ary[1]);  
        }  
        else if(check=='SUCCESS_POPUP')
        {
            $("#content_body").html(res_ary[1]); 
            $("#content_body").attr("style","display:block;"); 
            $("#szSearchString").focus();
            $("#szSearchString").focus().selectionEnd;
        }
        else if(check=='SUCCESS')
        {  
            //$("#pending_task_listing_container").html(res_ary[1]);	
            updatePendingTrayListing(res_ary[1]);
            $("#pending_task_listing_container").attr('style','display:block;'); 
            
            if(operation_type=='CLOSE_TASK' || operation_type=='SEND_CLOSE_TASK' || operation_type=='SEND_SNOOZE' || operation_type=='SNOOZE')
            {
                $("#pending_task_overview_main_container").html("");
                $("#pending_task_overview_main_container").attr('style','display:none;');  

                $("#pending_task_tray_main_container").html("");
                $("#pending_task_tray_main_container").attr('style','display:none;');  
                $("#idLastClikedBookingFile").val("");
            } 
            else
            { 
                $("#content_body").attr('style','display:none;');
                $(".task-tray-header").removeClass('active-th');

                var type = res_ary[2];
                $("#task_tray_header_id_"+type).addClass('active-th');
            
                $('#pending_task_tray_container').html(res_ary[3]); 
                if($("#crm_buttons_main_container_"+idCrmLogs).length)
                {
                    $('html, body').animate({ scrollTop: ($("#crm_buttons_main_container_"+idCrmLogs).offset().top - 30) }, "100000");  
                } 
            }
        }
        else if(check=='SUCCESS_UPDATE_USER')
        {
            //$("#pending_task_listing_container").html(res_ary[1]);	
            updatePendingTrayListing(res_ary[1]);
            $("#pending_task_listing_container").attr('style','display:block;'); 
            
            $("#pending_task_overview_main_container").html(res_ary[2]);
            $("#pending_task_overview_main_container").attr('style','display:block;'); 
            
            $(".task-tray-header").removeClass('active-th');

            var type = res_ary[3];
            $("#task_tray_header_id_"+type).addClass('active-th');

            $('#pending_task_tray_container').html(res_ary[4]); 
        }
        else if(check=='SUCCESS_PREVIEW')
        {
            $('#pending_task_reminder_email_preview_container').html(res_ary[1]); 
            $("#pending_task_reminder_email_preview_container").attr('style','display:block;');  
        }
        
        if(operation_type=='SEARCH_FILE')
        {
            $("#crm_operation_link_file_search_"+idCrmLogs).removeClass('tyni-loader');
        }
        else
        {
            $("#loader").attr('style','display:none;');
        } 
    });
} 

function crm_operation_search_on_enter_key_press(kEvent,formId,operation_type,email_id)
{
    if(kEvent.keyCode==13)
    {
        submit_crm_snooze_form(formId,operation_type,email_id);
    }
    return false
} 
function reassign_crm_email_to_file(file_id,formId)
{ 
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?operation_type=REASSIGN_CRM_EMAIL_TO_FILE&new_file_id="+file_id,$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        { 
            $("#content_body").attr('style','display:none;');
            $(".task-tray-header").removeClass('active-th');
            
            var type = resArr[1];
            $("#task_tray_header_id_"+type).addClass('active-th');
            $("#pending_task_tray_container").html(resArr[2]); 
        }
        if(resArr[0]=='SUCCESS_FILE_REASSIGNED')
        { 
            //$("#pending_task_listing_container").html(resArr[1]);		
            updatePendingTrayListing(resArr[1]);
            $("#pending_task_listing_container").attr('style','display:block;'); 
            
            $("#pending_task_overview_main_container").html(resArr[2]);
            $("#pending_task_overview_main_container").attr('style','display:block;'); 
            
            $(".task-tray-header").removeClass('active-th');

            var type = resArr[3];
            
            $("#task_tray_header_id_"+type).addClass('active-th');
            
            $('#pending_task_tray_container').html(resArr[4]); 
           
            $('#pending_tray_task_heading_container').html(resArr[5]); 
        }
        else if(resArr[0]=='ERROR')
        {
            $("#crm_buttons_main_container").html(resArr[1]); 
        } 
        $("#loader").attr('style','display:none;');
    });
} 

function submit_link_customer_popup_form(formId,email_id)
{   
    szFlag= $("#cms_operation_search_user_email_form #szFlag").attr('value');
    if(szFlag=='FROM_HEADER'){
        $("#new_contact_forwarder span").html("New");
        $("#new_contact_forwarder").unbind("click");
        $("#new_contact_forwarder").attr("onclick","");
        $("#new_contact_forwarder").click(function(){openNewContactOfForwarder('','','','FROM_HEADER');});
    }
    $("#link_customer_search_button").addClass('search-popup-button');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",$('#'+formId).serialize(),function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS')
        {  
            $("#content_body").attr('style','display:block;');
            $("#link_customer_list_container").attr('style','display:block;');  
            $('#link_customer_list_container').html(res_ary[1]);  
        } 
        $("#link_customer_search_button").removeClass('search-popup-button');
    });
} 

function submit_link_customer_by_enter(kEvent,formId,email_id)
{
    if(kEvent.keyCode==13)
    {
        submit_link_customer_popup_form(formId,email_id);
    }
}

function crm_operation_link_customer(email_id,customer_id,file_id,from_page)
{    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{email_id:email_id,customer_id:customer_id,file_id:file_id,from_page:from_page,mode:'LINK_CUSTOMER_CRM_EMAILS'},function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS')
        {  
            $("#content_body").attr('style','display:none;');
             
            $(".task-tray-header").removeClass('active-th');
            
            var type = res_ary[1];
            $("#task_tray_header_id_"+type).addClass('active-th');
            $("#pending_task_tray_container").html(res_ary[2]); 
            
            $('#pending_tray_task_heading_container').html(res_ary[3]); 
        } 
        $("#link_customer_search_button").removeClass('tyni-loader');
    });
}

function enableCrmOperationCreateUser(szFormId,email_id)
{
    var szIdSufix = '_'+email_id;
    var requiredFieldsAry = new Array();   
    requiredFieldsAry['0'] = 'iBookingLanguage'+szIdSufix;  
    requiredFieldsAry['1'] = 'szEmail'+szIdSufix;
    requiredFieldsAry['2'] = 'szCustomerCountry'+szIdSufix;
    /*
    requiredFieldsAry['3'] = 'szFirstName';
    requiredFieldsAry['4'] = 'szLastName';
    requiredFieldsAry['5'] = 'idCustomerDialCode'; 
    requiredFieldsAry['6'] = 'szCustomerPhoneNumber';  
    requiredFieldsAry['7'] = 'szCustomerCity'; 
    requiredFieldsAry['8'] = 'idCustomerCurrency';
    */
    
    var formId = szFormId;  
    
    var error = 1;
    $.each( requiredFieldsAry, function(index,input_fields){
        if(isFormInputElementEmpty(formId,input_fields))
        {	  
            $("#"+input_fields).addClass('red_border'); 
            error++;  
        }
    }); 
      
    var buttonId = 'crm_options_createuser_save_button';  
    if(parseInt(error)==1)
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:1');
        $("#"+buttonId).click(function(){ submit_crm_snooze_form(formId,'SAVE_CREATE_FORM',email_id);});
    }
    else
    { 
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:0.4;');
    } 
}
function submit_transporteca_turn_over_form(mode)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php?mode="+mode,$("#transporteca_turnover_form").serialize(),function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#transporteca_turn_over_list_container").html(result_ary[1]);	  
        }  
        else if(jQuery.trim(result_ary[0])=='REDIRECT')
        {
            redirect_url(result_ary[1]);
        }
    });
} 
function cancel_crm_user_details(email_id)
{
    $(".crm_email_buttons_"+email_id).removeClass('button2'); 
    $(".crm_email_buttons_"+email_id).removeClass('button1'); 
    $(".crm_email_buttons_"+email_id).addClass('button1'); 
    $("#crm_buttons_operations_container_"+email_id).html(" ");
}

function update_file_owner(admin_id,email_id,file_id)
{
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{email_id:email_id,admin_id:admin_id,file_id:file_id,mode:'UPDATE_TASK_OWNER'},function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS')
        {  
            updatePendingTrayListing(res_ary[1]);
            //$("#pending_task_listing_container").html(res_ary[1]);		
            $("#pending_task_listing_container").attr('style','display:block;');  
            
            $("#pending_task_overview_main_container").html("");
            $("#pending_task_overview_main_container").attr('style','display:none;');  

            $("#pending_task_tray_main_container").html("");
            $("#pending_task_tray_main_container").attr('style','display:none;');   
            $("#idLastClikedBookingFile").val("");
        }  
        $("#loader").attr('style','display:none;');
    });
}

function showHideReplyButton(id,email_id)
{
    var disp = $("#"+id).css('display');
    //var container_id = "crm_email_buttons_main_reply_button_"+email_id;
    if(disp=='block')
    {
        $("#"+id).css('display','none'); 
        //$("#"+container_id).css('display','block'); 
        
    }
    else
    {
        $("#"+id).css('display','block'); 
       // $("#"+container_id).css('display','none'); 
    }
} 
function closeFile(ctr,type)
{
    if(type=='FORWARD_EMAIL')
    {
        $("#forwarded_attachment_li_"+ctr).css('display','none'); 
        $("#forwardedAttachments_"+ctr).val(" "); 
    }
    else
    { 
        $("#image_li_"+ctr).css('display','none'); 
        var szFileName = $("#uploadedFileName_"+ctr).val();
        console.log("File Name: "+szFileName);
        if(szFileName==='BOOKING_INVOICE' || szFileName==='BOOKING_NOTICE' || szFileName==='BOOKING_CONFIRMATION')
        {
            $("#add_document_attachment_popup_"+szFileName).attr('style','display:inline-block;');
        }
        $("#uploadedFileName_"+ctr).val(" ");
        $("#originalFileName_"+ctr).val(" ");
        $("#originalFileSize_"+ctr).val(" ");
        var iAttachmentCtr = $("#iAttachmentCounter").val();
        iAttachmentCtr = parseInt(iAttachmentCtr); 
        if(iAttachmentCtr>1)
        {
            iAttachmentCtr = iAttachmentCtr - 1;
        }
        else
        {
            iAttachmentCtr = 0;
        }
        $("#iAttachmentCounter").val(iAttachmentCtr);
        $("#image_li_"+ctr).remove();
    } 
} 

function enable_crm_operation_reply_tab_buttons(formId,email_id)
{  
    //alert("email_id"+email_id);
    var save_count = 1;
    var snooze_count = 1;
    var send_count = 1;
    var send_snooze_count = 1;
      
    if(isFormInputElementEmpty(formId,'szCustomerEmail_'+email_id)) 
    {		  
        send_count++;
        send_snooze_count++;
    } 
    if(isFormInputElementEmpty(formId,'szReminderSubject_'+email_id)) 
    {		  ;
        send_count++;
        send_snooze_count++;
    }
    if(isFormInputElementEmpty(formId,'szReminderEmailBody_'+email_id)) 
    {		 
        console.log("Body Empty");
        send_count++;
        send_snooze_count++;
    } 
    if(isFormInputElementEmpty(formId,'dtEmailReminderDate_'+email_id)) 
    {		 
        console.log("Date Empty");
        send_snooze_count++;
    }
    if(isFormInputElementEmpty(formId,'szEmailReminderTime_'+email_id)) 
    {		 
        console.log("Time Empty");
        send_snooze_count++;
    }
    
    
    $("#request_reminder_email_save_draft_button_"+email_id).unbind("click");
    $("#request_reminder_email_save_draft_button_"+email_id).click(function(){ saveAsDraftCRMEmailData(formId,email_id);});
    $("#request_reminder_email_save_draft_button_"+email_id).attr("style",'opacity:1');
    
    if(save_count==1)
    {
        $("#request_reminder_save_button_"+email_id).unbind("click");
        $("#request_reminder_save_button_"+email_id).click(function(){ submit_crm_snooze_form(formId,'SAVE',email_id);});
        $("#request_reminder_save_button_"+email_id).attr("style",'opacity:1');
    }
    else
    {
        $("#request_reminder_save_button_"+email_id).unbind("click");
        $("#request_reminder_save_button_"+email_id).attr("style",'opacity:0.4');
    }    
    if(snooze_count==1)
    {
        $("#request_reminder_snooze_button_"+email_id).unbind("click");
        $("#request_reminder_snooze_button_"+email_id).click(function(){ submit_crm_snooze_form(formId,'SNOOZE',email_id);});
        $("#request_reminder_snooze_button_"+email_id).attr("style",'opacity:1');
    }
    else
    {
        $("#request_reminder_snooze_button_"+email_id).unbind("click");
        $("#request_reminder_snooze_button_"+email_id).attr("style",'opacity:0.4');
    } 
    
    if(send_snooze_count==1)
    {
        $("#request_reminder_email_snooze_button_"+email_id).unbind("click");
        $("#request_reminder_email_snooze_button_"+email_id).click(function(){ submit_crm_snooze_form(formId,'SEND_SNOOZE',email_id);});
        $("#request_reminder_email_snooze_button_"+email_id).attr("style",'opacity:1');
    }
    else
    {
        $("#request_reminder_email_snooze_button_"+email_id).unbind("click");
        $("#request_reminder_email_snooze_button_"+email_id).attr("style",'opacity:0.4');
    }  
    var iLabelStatus = $("#iLabelStatus_"+email_id).attr('value');
    var szTaskStatusRemind = $("#szTaskStatusRemind_"+email_id).attr('value');
    if(send_count==1)
    {
        $("#request_reminder_email_send_button_"+email_id).unbind("click");
        $("#request_reminder_email_send_button_"+email_id).click(function(){ submit_crm_snooze_form(formId,'SEND',email_id);});
        $("#request_reminder_email_send_button_"+email_id).attr("style",'opacity:1');
        
        if(parseInt(iLabelStatus)==1 && szTaskStatusRemind=='T140')
        {
            $("#request_reminder_email_send_close_task_button_"+email_id).unbind("click");
            $("#request_reminder_email_send_close_task_button_"+email_id).attr("style",'opacity:0.4');
        }
        else
        {
            $("#request_reminder_email_send_close_task_button_"+email_id).unbind("click");
            $("#request_reminder_email_send_close_task_button_"+email_id).click(function(){ submit_crm_snooze_form(formId,'SEND_CLOSE_TASK',email_id);});
            $("#request_reminder_email_send_close_task_button_"+email_id).attr("style",'opacity:1'); 
        }
    }
    else
    {
        $("#request_reminder_email_send_button_"+email_id).unbind("click");
        $("#request_reminder_email_send_button_"+email_id).attr("style",'opacity:0.4');
        
        $("#request_reminder_email_send_close_task_button_"+email_id).unbind("click");
        $("#request_reminder_email_send_close_task_button_"+email_id).attr("style",'opacity:0.4'); 
    } 
} 

function display_remind_pane_add_popup(type,file_id,from_page,email_id,szFlag,szSearchStr,idForwarderContact)
{
    $("#loader").attr('style','display:block;');
     if(szFlag=='FROM_HEADER')
    {
        $("#contactPopup").html(''); 
        $("#contactPopup").attr("style","display:none;");
        
        $("#ajaxLogin").html(''); 
        
        
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{szFlag:szFlag,type:type,email_id:email_id,file_id:file_id,from_page:from_page,mode:'DISPLAY_REPLY_ADD_EMAIL_POPUP',szSearchStr:szSearchStr,idForwarderContact:idForwarderContact},function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS_POPUP')
        {
            if(szFlag=='FROM_HEADER')
            {
                $("#contactPopup").html(res_ary[1]); 
                $("#contactPopup").attr("style","display:block;");
            }
            else
            {
                $("#content_body").html(res_ary[1]); 
                $("#content_body").attr("style","display:block;");
            }
             
            $("#szSearchString").focus();
            $("#szSearchString").focus().selectionEnd;
        }  
        $("#loader").attr('style','display:none;');
    });
}

function update_crm_operation_reply(email,email_id,operation)
{
    var szInputFieldId = '';
    if(operation=='CUSTOMER_EMAIL')
    {  
        if(email_id>0)
        {
            szInputFieldId = "szCustomerEmail_"+email_id;
        }
        else
        {
            szInputFieldId = "szCustomerEmail"; 
        } 
        updateRemindLogsEmails(szInputFieldId,email,email_id); 
    }
    else if(operation=='CUSTOMER_CC_EMAIL')
    {
        if(email_id>0)
        { 
            szInputFieldId = "szCustomerCCEmail_"+email_id;
        }
        else
        { 
            szInputFieldId = "szCustomerCCEmail";
        } 
        updateRemindLogsEmails(szInputFieldId,email,email_id); 
    }
    else if(operation=='CUSTOMER_BCC_EMAIL')
    { 
        if(email_id>0)
        { 
            szInputFieldId = "szCustomerBCCEmail_"+email_id;
        }
        else
        { 
            szInputFieldId = "szCustomerBCCEmail";
        } 
        updateRemindLogsEmails(szInputFieldId,email,email_id); 
    }
    $("#content_body").html(""); 
    $("#content_body").attr("style","display:none;"); 
}

function updateRemindLogsEmails(szInputFieldId,email,email_id)
{
    var szEmail = $("#"+szInputFieldId).val();
    var szCustomerEmail = '';
    if(jQuery.trim(szEmail).length)
    {
        szCustomerEmail = szEmail+", "+email;
        
    }
    else
    {
        szCustomerEmail = email;
    }
    $("#"+szInputFieldId).val(szCustomerEmail);
    
    if(email_id>0){
        formidcrm="crm_email_operations_form_"+email_id;
        enable_crm_operation_reply_tab_buttons(formidcrm,email_id);
    }else
    {
        enable_reminder_tab_buttons();
    }
}
function display_email_fields(operation_type,email_id)
{
    if(operation_type=='SHOW_CC')
    { 
        $("#crm_operation_cc_link_"+email_id).attr('style','display:none;');
        $("#crm_operation_cc_fields_container_"+email_id).attr('style','display:block;');
        $("#iAddCCEmail_"+email_id).val(1);
    }
    else if(operation_type=='HIDE_CC')
    {
        $("#crm_operation_cc_link_"+email_id).attr('style','display:inline-block;');
        $("#crm_operation_cc_fields_container_"+email_id).attr('style','display:none;');
        $("#iAddCCEmail_"+email_id).val(0);
    }
    if(operation_type=='SHOW_BCC')
    { 
        $("#crm_operation_bcc_link_"+email_id).attr('style','display:none;');
        $("#crm_operation_bcc_fields_container_"+email_id).attr('style','display:block;');
        $("#iAddBCCEmail_"+email_id).val(1);
    }
    else if(operation_type=='HIDE_BCC')
    {
        $("#crm_operation_bcc_link_"+email_id).attr('style','display:inline-block;');
        $("#crm_operation_bcc_fields_container_"+email_id).attr('style','display:none;');
        $("#iAddBCCEmail_"+email_id).val(0);
    }
} 

function display_emails_attachemnts_popup(email_id,from_page,template_id)
{
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{email_id:email_id,template_id:template_id,from_page:from_page,mode:'DISPLAY_EMAIL_ATTACHMENTS_POPUP'},function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS_POPUP')
        {
            $("#content_body").html(res_ary[1]); 
            $("#content_body").attr("style","display:block;");  
        }  
        $("#loader").attr('style','display:none;');
    });
} 
 
function display_remind_pane_add_new_task_popup(file_id)
{
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{file_id:file_id,mode:'DISPLAY_ADD_TRANSPORTECA_TASK_POPUP'},function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS_POPUP')
        {
            $("#content_body").html(res_ary[1]); 
            $("#content_body").attr("style","display:block;"); 
            $("#dtTaskDate").focus();
            $("#dtTaskDate").focus().selectionEnd; 
        }  
        $("#loader").attr('style','display:none;');
    });
}

function submit_new_transporteca_task_form(formId,admin_id)
{    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?admin_id="+admin_id,$('#'+formId).serialize(),function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS')
        {  
            updatePendingTrayListing(res_ary[1]);
            //$("#pending_task_listing_container").html(res_ary[1]); 
            
            $("#content_body").attr('style','display:none;');  
            $(".task-tray-header").removeClass('active-th');
            $("#task_tray_header_id_LOG").addClass('active-th');
            $("#pending_task_tray_container").html(res_ary[2]); 
             
            var idReminderNotes = parseInt(res_ary[3]);
            
            if(idReminderNotes>0)
            {
                var document_height = $(document).height();  
                var scrollHeight = $(document).height();
                var windowHeight = $(window).height();
                var scrollTop = $(window).scrollTop();
/*
                console.log("Document height: "+document_height);
                console.log("scroll height: "+scrollHeight);
                console.log("wind height: "+windowHeight);
                console.log("scroll top: "+scrollTop);
*/
                var empty_div_id = "overview_task_pending_tray_"+idReminderNotes;
                var div_top = $("#"+empty_div_id).offset().top;
                //console.log("Div top: "+div_top);
                
                $('html, body').animate({ scrollTop: ($('#'+empty_div_id).prop("scrollHeight")) }, "100000");   
            } 
        } 
        else if(check=='ERROR')
        {
            $("#content_body").attr('style','display:block;'); 
            $('#content_body').html(res_ary[1]);  
        } 
    });
} 
function close_reminder_task(from_page,reminder_task_id,file_id,operation_type)
{
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{file_id:file_id,from_page:from_page,operation_type:operation_type,reminder_task_id:reminder_task_id,mode:'CLOSE_REMINDER_TASK'},function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS')
        {
            updatePendingTrayListing(res_ary[1]);
            //$("#pending_task_listing_container").html(res_ary[1]); 
            
            $("#content_body").attr('style','display:none;');
            $(".task-tray-header").removeClass('active-th');
            
            var type = res_ary[2];
            $("#task_tray_header_id_"+type).addClass('active-th');
            $("#pending_task_tray_container").html(res_ary[3]); 
        }  
        $("#loader").attr('style','display:none;');
    });
}

function mark_crm_email_read(gmail_id,file_id)
{ 
    $(".unread-email-common-"+gmail_id).removeClass('unread-email'); 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{email_id:gmail_id,file_id:file_id,mode:'CRM_EMAIL_MARK_READ'},function(result){
        var res_ary = result.split('||||');
        var check = jQuery.trim(res_ary[0]); 
        if(check=='SUCCESS')
        {
            //auto_load_pending_tray_listing();
        }
    });
}
function update_crm_dial_code(email_id,idCountry)
{
    console.log("Email ID: "+email_id+" country: "+idCountry);
    $("#idCustomerDialCode_"+email_id).val(idCountry);
}
function showHideReAssignDrodown(div_id,email_id)
{
    //var div_id = "crm-reassign-owner-list-container-"+email_id;
    var disp = $("#"+div_id).css('display');
    if(disp=='block')
    {
        $("#"+div_id).css('display','none'); 
    }
    else
    {
        $("#"+div_id).css('display','block'); 
    }
} 

function showHideReplyButtons(div_id,email_id,type)
{
    if(type=='SHOW')
    {
        $("#"+div_id).css('display','block');
    }
    else if(type=='HIDE')
    {
        $("#"+div_id).css('display','none');
    }
}
function display_document_attachments(szAttachmentType)
{  
    var iFileUploadInProgress = $("#iFileUploadInProgress").val();  
    if(iFileUploadInProgress==1)
    {
        console.log("File uploading is in progress. Please try again later...");
        return false;
    }
    var iCreateUl = "";
    var iAttachmentCtr = "";
    var iAttachmentCtr = $("#iAttachmentCounter").val();
    iAttachmentCtr = parseInt(iAttachmentCtr); 
    if(iAttachmentCtr>0)
    {
        iAttachmentCtr = iAttachmentCtr + 1;
    }
    else
    {
        iAttachmentCtr = 1;
    }
    if($(".add-edit-popup-ul").length)
    { 
        iCreateUl = 0; 
    }
    else
    { 
        iCreateUl = 1; 
    }
    var szAttachmentLinkText = '';
    if(szAttachmentType==='BOOKING_INVOICE')
    {
        szAttachmentLinkText = "Booking Invoice (<i>if applicable</i>)";
    }
    else if(szAttachmentType==='BOOKING_NOTICE')
    {
        szAttachmentLinkText = "Booking Notice (<i>if applicable</i>)";
    }
    else if(szAttachmentType==='BOOKING_CONFIRMATION')
    {
        szAttachmentLinkText = "Booking Confirmation (<i>if applicable</i>)";
    }
    else
    {
        return false;
    } 
    var szHtmlString = ''; 
    if(iCreateUl===1)
    {
        szHtmlString = '<ul class="reorder_ul reorder-photos-list add-edit-popup-ul">';
    } 
    szHtmlString += '<li id="image_li_'+iAttachmentCtr+'" class="ui-sortable-handle">'; 
    szHtmlString += '<a href="javascript:void(0);" style="float:none;" class="image_link">Attachment '+iAttachmentCtr+': '+szAttachmentLinkText+'</a>';
    szHtmlString += '&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="closeFile('+ iAttachmentCtr +')" class="red_text" style="text-decoration:none;"><strong>X</strong></a>';
    szHtmlString += '<input type="hidden" name="addEditRemindEmailTemplate[uploadedFileName][]" id="uploadedFileName_'+iAttachmentCtr+'" value="'+szAttachmentType+'">';
    szHtmlString += '<input type="hidden" name="addEditRemindEmailTemplate[originalFileName][]" id="originalFileName_'+iAttachmentCtr+'" value="'+szAttachmentType+'">';
    szHtmlString += '<input type="hidden" name="addEditRemindEmailTemplate[originalFileSize][]" id="originalFileSize_'+iAttachmentCtr+'" value="">';
    szHtmlString += '</li>';
     
    if(iCreateUl===1)
    { 
        szHtmlString += '<input id="iAttachmentCounter" name="addEditRemindEmailTemplate[iAttachmentCounter]" value="1" type="hidden">';
        szHtmlString += '</ul>';
        $("#images_preview_popup").html(szHtmlString);
    }
    else
    {
        $(".add-edit-popup-ul").append(szHtmlString);
    } 
    $("#iAttachmentCounter").val(iAttachmentCtr);
    $("#add_document_attachment_popup_"+szAttachmentType).attr('style','display:none;');
}

function showHideHoldingSection(iHoldingPage)
{
    if(iHoldingPage==1)
    {
        $("#custom_holding_page_container").css('display','block');
        $("#nonholdingPageVariable").css('display','none');
    }
    else
    {
        $("#custom_holding_page_container").css('display','none');
        $("#nonholdingPageVariable").css('display','block');
    }
}

function selectlanguageVersions(id)
{	
    $('#languageVersionsTable tr').attr('style','');
    $("#"+id).attr('style','background:#DBDBDB;color:#828282;');
     
    //$("#cancelLanguageSubversions").attr("style","opacity:1");
    //$("#cancelLanguageSubversions").unbind("click");
    //$("#cancelLanguageSubversions").click(function(){ cancelLanguageVersions(); });
    
    $("#editLanguageSubversions").attr("style","opacity:1");
    $("#editLanguageSubversions").unbind("click");
    $("#editLanguageSubversions").html("<span>edit</span>");
    $("#editLanguageSubversions").click(function(){ displayEditLanguageVersionsForm(id); }); 
}

function displayEditLanguageVersionsForm(id)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_languageConfiguration.php",{mode:'DISPLAY_EDIT_LANGUAGE_VERSIONS_FORM',id:id},function(result){ 
        
        var result_ary = result.split('||||');
        
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            $("#language_version_form_container_div").html(result_ary[1]);
            
            $("#cancelLanguageSubversions").attr("style","opacity:1");
            $("#cancelLanguageSubversions").unbind("click");
            $("#cancelLanguageSubversions").click(function(){ cancelLanguageVersions(); });
    
            
            $("#editLanguageSubversions").attr("style","opacity:1");
            $("#editLanguageSubversions").unbind("click");
            $("#editLanguageSubversions").html("<span>Save</span>");
            $("#editLanguageSubversions").click(function(){ saveLanguageVersions(id); });
        } 
    }); 
}

function saveLanguageVersions(id)
{
    $("#cancelLanguageSubversions").unbind("click");
    $('#cancelLanguageSubversions').attr({'onclick':'','style':'opacity:0.4'});

    $("#editLanguageSubversions").unbind("click");
    $('#editLanguageSubversions').attr({'onclick':'','style':'opacity:0.4'});


    var value = $("#edit_language_versions_form").serialize();
    var newValue = value+"&mode=SAVE_LANGUAGE_VESRIONS_DATA&&id="+id;
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_languageConfiguration.php",newValue,function(result){
           
        var result_ary = result.split('||||');
        
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
           $("#language_version_list_container_div").html(result_ary[1]);
           $("#language_version_form_container_div").html(result_ary[2]);
        }
        else
        {
            $("#language_version_form_container_div").html(result_ary[1]);
            
            
            $("#cancelLanguageSubversions").attr("style","opacity:1");
            $("#cancelLanguageSubversions").unbind("click");
            $("#cancelLanguageSubversions").click(function(){ cancelLanguageVersions(); });
    
            
            $("#editLanguageSubversions").attr("style","opacity:1");
            $("#editLanguageSubversions").unbind("click");
            $("#editLanguageSubversions").html("<span>Save</span>");
            $("#editLanguageSubversions").click(function(){ saveLanguageVersions(id); });
        }
    });
}

function cancelLanguageVersions()
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_languageConfiguration.php",{mode:'CANCEL_LANGUAGE_VERSIONS'},function(result){ 
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#language_version_list_container_div").html(result_ary[1]);
            $("#language_version_form_container_div").html(result_ary[2]);
        } 
    }); 
}

function saveAsDraftCRMEmailData(formid,idCrm)
{
    var value=$("#"+formid).serialize(); 
    var divcontent='szReminderEmailBody_'+idCrm;
    var content = NiceEditorInstanceCRM[idCrm].instanceById(divcontent).getContent();
    var description = encode_string_msg_str(content); 
	 
    var newValue=value+"&idCrm="+idCrm+"&mode=SAVE_EMAIL_AS_DRAFT&addCrmOperationAry[szReminderEmailBody]="+description;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){ 
     var result_ary = result.split('||||');
    if(jQuery.trim(result_ary[0])=='SUCCESS')
    {
        //alert("SUCCESS");
        $("#draft_email_msg_"+idCrm).attr("style","color:green");
    }    
    });
}


function saveEmailAsDraftLoad()
{
    console.log("hello");
    saveAsDraftCRMEmailData("crm_email_operations_form_1009","1009");
    return false;
}

function manually_sync_with_gmail()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'MANULLY_SYNC_WITH_GMAIL_INBOX'},function(result){ 
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#admin_header_gmail_sync_error_container").html(result_ary[1]); 
        } 
    }); 
}
function enableInsuranceBuySellButton(iType)
{
    var requiredFieldsAry = new Array();  
    requiredFieldsAry['0'] = 'idOriginCountry';
    requiredFieldsAry['1'] = 'idDestinationCountry';
    requiredFieldsAry['2'] = 'idTransportMode'; 
    requiredFieldsAry['3'] = 'idInsuranceCurrency'; 
    requiredFieldsAry['4'] = 'fInsuranceUptoPrice'; 
    requiredFieldsAry['5'] = 'fInsuranceRate'; 
    requiredFieldsAry['6'] = 'fInsuranceMinPrice'; 
    requiredFieldsAry['7'] = 'szCargoType'; 
    var formId = 'insurance_form_type_'+iType;  
    
    var error = 1;
    $.each( requiredFieldsAry, function(index,input_fields){
        var szInputField = input_fields+"_"+iType;
        if(isFormInputElementEmpty(formId,szInputField))
        {	 
            error++;  
        }
    }); 
      
    var buttonId = 'insurance_rates_add_button_'+iType;  
    if(parseInt(error)==1)
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:1');
        $("#"+buttonId).click(function(){ submitInsuranceBuySellForm(formId,iType); });
    }
    else
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:0.4;');
    } 
}

function submitInsuranceBuySellForm(formId,iType)
{
    $("#insurance_rates_add_button_"+iType).addClass('admin-button1-clicked');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        { 
            if(iType==1)
            {
                $("#insurance_buy_rates_main_container_div").html(resArr[1]);  
                if(jQuery.trim(resArr[2])!='')
                {
                    var insuranceRateAry = resArr[2].split(";");
                    $.each( insuranceRateAry, function( key, value ) {
                        if($("#truckin_tr____"+value).length)
                        {
                            //$("#truckin_tr____"+value).removeClass('red_text');
                        }
                    }); 
                }
                if(jQuery.trim(resArr[3])!='')
                {
                    var insuranceRateAry = resArr[3].split(";");
                    $.each( insuranceRateAry, function( key, value ) {
                        if($("#truckin_tr____"+value).length)
                        {
                            //$("#truckin_tr____"+value).addClass('red_text');
                        }
                    }); 
                }
            }
            else if(iType==2)
            {
                $("#insurance_sell_rates_main_container_div").html(resArr[1]);  
            }
        }
        else if(resArr[0]=='ERROR')
        {
            $("#insurance_add_edit_form_container_"+iType).html(resArr[1]); 
        } 
    });
} 
function editInsuranceRates(iType,mode)
{
    if(mode=='CLEAR_ISURANCE_FORM_DATA')
    {
        $("#insurance_rates_clear_button_"+iType).addClass('admin-button2-clicked');
    }
    else
    {
        $("#insurance_rates_add_button_"+iType).addClass('admin-button1-clicked');
    }
    
    var idInsurateRate = $("#insuranceRateIds_"+iType).val();
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",{idInsurateRate:idInsurateRate,iType:iType,mode:mode},function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            if(mode=='EDIT_INSURANCE')
            {
                $("#insurance_add_edit_form_container_"+iType).html(resArr[1]); 
            }
            else
            {
                if(iType==1)
                {
                    $("#insurance_buy_rates_main_container_div").html(resArr[1]);  
                    
                    if(jQuery.trim(resArr[2])!='')
                    {
                        var insuranceRateAry = resArr[2].split(";");
                        $.each( insuranceRateAry, function( key, value ) {
                            if($("#truckin_tr____"+value).length)
                            {
                               // $("#truckin_tr____"+value).addClass('red_text');
                            }
                        }); 
                    }
                    
                     if(jQuery.trim(resArr[3])!='')
                    {
                        var insuranceRateAry = resArr[3].split(";");
                        $.each( insuranceRateAry, function( key, value ) {
                            if($("#truckin_tr____"+value).length)
                            {
                                //$("#truckin_tr____"+value).removeClass('red_text');
                            }
                        }); 
                    }
                }
                else if(iType==2)
                {
                    $("#insurance_sell_rates_main_container_div").html(resArr[1]);  
                }
            } 
        }
        if(resArr[0]=='SUCCESS_DELETE')
        {
            $("#rail_transport_main_listing_container_div").html(resArr[1]);  
        }
        else if(resArr[0]=='ERROR')
        {
            $("#insurance_rate_popup").html(resArr[1]); 
            $("#insurance_rate_popup").attr("style",'display:block;');
        } 
    });
}
 
function sort_insurance_rates(kEvent,szValue,iType)
{ 
    var idPrimarySortKeyInputId = "";
    var idPrimarySortValueInputId = "";
    var idOtherSortKeyInputId = "";
     
    idPrimarySortKeyInputId = 'idPrimarySortKey_'+iType;
    idPrimarySortValueInputId = "idPrimarySortValue_"+iType;
    idOtherSortKeyInputId = "idOtherSortKey_"+iType; 
    
    var szSortValue=''; 
    var idPrimarySortKey=$("#"+idPrimarySortKeyInputId).val();
    var szSortValue=$("#"+idPrimarySortValueInputId).val(); 
    var idOtherSortKeyArr=$("#"+idOtherSortKeyInputId).attr('value'); 
      
    if(idPrimarySortKey==szValue)
    {
        if(szSortValue=='ASC')
        {
            szSortValue='DESC';
        }
        else
        {
            szSortValue='ASC';
        } 
        $("#"+idPrimarySortValueInputId).attr('value',szSortValue);
        $("#"+idOtherSortKeyInputId).attr('value','');
    }
    else
    { 
        $("#"+idPrimarySortKeyInputId).attr('value',szValue);
        $("#"+idPrimarySortValueInputId).attr('value','ASC');
        $("#"+idOtherSortKeyInputId).attr('value','');
    } 
    
    var idPrimarySortKey = $("#"+idPrimarySortKeyInputId).val();
    var szSortValue = $("#"+idPrimarySortValueInputId).val(); 
      
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",{mode:"SORT_INSURANCE_RATE_LISTING",idOtherSortKeyArr:idOtherSortKeyArr,szSortValue:szSortValue,idPrimarySortKey:idPrimarySortKey,iType:iType},function(result){
             
        var resArr = result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {
            if(iType==1)
            {
                $("#insurance_buy_rates_main_container_div").html(resArr[1]);  
            }
            else if(iType==2)
            {
                $("#insurance_sell_rates_main_container_div").html(resArr[1]);  
            }
            
            $(".moe-transport-sort-span_"+iType).removeClass('sort-arrow-up');
            $(".moe-transport-sort-span_"+iType).removeClass('sort-arrow-down');
                      
            if(szSortValue=='ASC')
            {
                $("#moe-transport-sort-span-id-"+szValue+"-"+iType).addClass('sort-arrow-up');
            }
            else
            { 
                $("#moe-transport-sort-span-id-"+szValue+"-"+iType).addClass('sort-arrow-down');
            }
        }
    }); 
}

function clearInsuranceTryitOutForm()
{
    $("#insurance_try_it_out_results_container_div").html("");
    resetForm("insurance_try_it_out_form");
}

function enableInsuranceTryitoutForm()
{
    var requiredFieldsAry = new Array();  
    requiredFieldsAry['0'] = 'szOriginCountryStr';
    requiredFieldsAry['1'] = 'szDestinationCountryStr';
    requiredFieldsAry['2'] = 'idTransportMode'; 
    requiredFieldsAry['3'] = 'szCargoType'; 
    requiredFieldsAry['4'] = 'idGoodsInsuranceCurrency'; 
    requiredFieldsAry['5'] = 'idCustomerCurrency'; 
    requiredFieldsAry['6'] = 'idCustomerCurrency'; 
    requiredFieldsAry['7'] = 'fTotalPriceCustomerCurrency'; 
    requiredFieldsAry['8'] = 'idForwarderCountry'; 
    requiredFieldsAry['9'] = 'idBookingCurrency'; 
    
    var formId = 'insurance_try_it_out_form';  
    
    //var error = 1;
    $.each( requiredFieldsAry, function(index,input_fields){ 
        if(isFormInputElementEmpty(formId,input_fields))
        {	 
            error++;  
        }
    }); 
      var error=1;
    var buttonId = 'insurance_try_it_out_test_button';  
    if(parseInt(error)==1)
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:1');
        $("#"+buttonId).click(function(){ submitInsuranceTryitoutForm(formId); });
    }
    else
    {
        $("#"+buttonId).unbind("click");
        $('#'+buttonId).attr('style','opacity:0.4;');
    } 
}
function submitInsuranceTryitoutForm(formId)
{
    $("#loader").attr("style",'display:block');
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php",$("#"+formId).serialize(),function(result){
        var resArr=result.split("||||"); 
        if(resArr[0]=='SUCCESS')
        {  
            $("#insurance_try_it_out_results_container_div").html(resArr[1]);   
        }
        else if(resArr[0]=='ERROR')
        {
            $("#insurance_try_it_out_form_container_div").html(resArr[1]); 
            $("#insurance_try_it_out_results_container_div").html("");
        } 
        $("#loader").attr("style",'display:none');
    });
}

function openCsvuploadpopup(validator)
{
    if(validator>0)
    {
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_billingDetails.php",{check:validator,mode:"OPEN_CSV_UPLOAD_POP_UP"},function(result){
          var result_ary = result.split('||||');
          if(jQuery.trim(result_ary[0])=='SUCCESS')
          {
            $("#contactPopup").html(result_ary[1]);		
            $("#contactPopup").attr('style','display:block');
          }   
        });
    }
}

function submitStripePaymentCsv(validator)
{
    var value=$("#submitStripePaymentCsvForm").serialize();
    var new_value=value+"&check="+validator+"&mode=SUBMIT_STRIPE_PAYMENT_CSV";
    //$(".upload-stripe-csv").addClass('tyni-loader');
    //$('#submit_button').attr('onclick','');
    //$('.file_list_con').html('<strong>Your upload stripe payment is in progress.....</strong>');
    //$('.upload-stripe-csv').html('<span>Processing</span>');
    //$('.upload-stripe-csv').attr('style','opacity:0.4');
    //$('#close_button').attr('style','opacity:0.4');
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_billingDetails.php",new_value,function(result){
        var result_ary = result.split('||||');
        $("#loader").attr('style','display:none;');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
          $("#contactPopup").html('');
          $("#contactPopup").html(result_ary[1]);		
          $("#contactPopup").attr('style','display:block');
        }
        else if(jQuery.trim(result_ary[0])=='ERROR')
        {
          $("#contactPopup").html('');
          $("#contactPopup").html(result_ary[1]);		
          $("#contactPopup").attr('style','display:block'); 
        }
    });
}

function removeStripePaymentCsv(validator)
{
    if(validator>0)
    {
        $('#submit_button').attr('onclick','');
        $('#submit_button').attr('style','opacity:0.4');
        $("#deleteArea").attr('style','display:none'); 
        $(".file_list_con").attr('style','display:none');
        $('#fileList').html('');
        $("#filecount").attr('value','0');
        $("#file_name").attr('value','');
        $(".upload").attr('style','position: relative; overflow: hidden; cursor: default;');
    } 
}

function selectLanguageConfigurationMetaData()
{
    var idLanguage=$("#idLanguage").val();
    var idLangConfigType=$("#idLangConfigType").val();
    
    $("#cancel").unbind("click");
    $('#cancel').attr({'onclick':'','style':'opacity:0.4'});
        
    $("#save span").html("Edit");
    
    $("#previewContent").html('');    
    $("#previewContent").attr("style","display:none");
    
    $("#save").unbind("click");
    $('#save').attr({'onclick':'','style':'opacity:0.4'});
          
    if(parseInt(idLanguage)>0 && jQuery.trim(idLangConfigType)!='')
    {
        $("#save").unbind("click");
         $('#save').attr({'onclick':'','style':'opacity:1'});
         $("#save").click(function(){saveEditTextLangConfigMetaData(idLangConfigType,idLanguage)});    
    }
}


function saveEditTextLangConfigMetaData(idLangConfigType,idLanguage)
{
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_languageConfiguration.php",{idLanguage:idLanguage,idLangConfigType:idLangConfigType,mode:'OPEN_EDIT_CONFIGURATION_META_DATA'},function(result){
            $("#previewContent").html(result);    
            $("#previewContent").attr("style","display:block");
            
            $("#save").unbind("click");
            $('#save').attr({'onclick':'','style':'opacity:1'});
            $("#save").click(function(){saveLangConfigMetaData(idLangConfigType,idLanguage)});
            $("#save span").html("Save");
            
            $("#cancel").unbind("click");
            $('#cancel').attr({'onclick':'','style':'opacity:1'});
            $("#cancel").click(function(){cancelLanguageConfigurationMeta(idLangConfigType,idLanguage)});
        });
}


function cancelLanguageConfigurationMeta(idLangConfigType,idLanguage)
{
    $("#save").unbind("click");
    $('#save').attr({'onclick':'','style':'opacity:1'});
    $("#save").click(function(){saveEditTextLangConfigMetaData(idLangConfigType,idLanguage)});
    
    $("#cancel").unbind("click");
    $('#cancel').attr({'onclick':'','style':'opacity:0.4'});

     $("#previewContent").html('');    
     $("#previewContent").attr("style","display:none");
     
     $("#save span").html("Edit");
}


function saveLangConfigMetaData(idLangConfigType,idLanguage)
{
    $("#cancel").unbind("click");
    $('#cancel').attr({'onclick':'','style':'opacity:0.4'});

    $("#save").unbind("click");
    $('#save').attr({'onclick':'','style':'opacity:0.4'});


    var value = $("#languageConfigurationForm").serialize();
    var newValue = value+"&mode=SAVE_LANGUAGE_CONFIGURATION_META_DATA&idLangConfigType="+idLangConfigType+"&idLanguage="+idLanguage;
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_languageConfiguration.php",newValue,function(result){
            
            $("#previewContent").html(result);    
            $("#previewContent").attr("style","display:block");
            
            $("#save").unbind("click");
            $('#save').attr({'onclick':'','style':'opacity:1'});
            $("#save").click(function(){saveLangConfigMetaData(idLangConfigType,idLanguage)});
            $("#save span").html("Save");
            
            $("#cancel").unbind("click");
            $('#cancel').attr({'onclick':'','style':'opacity:1'});
            $("#cancel").click(function(){cancelLanguageConfigurationMeta(idLangConfigType,idLanguage)});
    });
}

function auto_admin_header_notification()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'AUTO_LOAD_ADMIN_HEADER_NOTIFICATION'},function(result){
        var result_ary = result.split('||||'); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 
            $("#admin_header_notification_container_div").html(result_ary[1]);     
        }
    });
}


function saveAsDraftRemindEmailData(formid)
{
    var idTemplate = $("#idTemplate").attr('value');
    var value=$("#"+formid).serialize(); 
    var divcontent='szReminderEmailBody';
    var content = NiceEditorInstance.instanceById(divcontent).getContent();
    var description = encode_string_msg_str(content); 
    var idBookingOverView = $("#idBookingOverView").attr('value');
    var newValue=value+"&mode=SAVE_EMAIL_REMIND_AS_DRAFT&addSnoozeAry[szReminderEmailBody]="+description+"&idBookingOverView="+idBookingOverView+"&idTemplate="+idTemplate;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){ 
     var result_ary = result.split('||||');
    if(jQuery.trim(result_ary[0])=='SUCCESS')
    {
        //alert("SUCCESS");
        $("#draft_email_msg").attr("style","color:green");
    }    
    });
}
function openSearchPopupClickOnHistory(idUser,iFlag)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'OPEN_SEARCH_POPUP_CLICK_ON_HISTORY',idUser:idUser,iFlag:iFlag},function(result){ 
     var result_ary = result.split('||||');
    if(jQuery.trim(result_ary[0])=='SUCCESS')
    {
        $("#contactPopup").html(result_ary[1]);		
           $("#contactPopup").attr('style','display:block'); 
           $('body').addClass('popup-body-scroll');
           $("#contactPopup").focus();
           var openDiv = 'task-popup-id-recent'; 
           $(document).click(function(e) {
                if (!$(e.target).closest('#'+openDiv).length) {
                    
                    var disp = $("#ui-datepicker-div").css('display');
                    if(disp=='block')
                    {
                        console.log("Date picker is active");
                    }
                    else
                    {
                        $("#contactPopup").attr('style','display:none;'); 
                        $(document).unbind('click');
                    } 
                    $('body').removeClass('popup-body-scroll'); 
                }
            }); 
            $("#szFreeText").focus();
            $("#szFreeText").focus().selectionEnd;
    }    
    $("#loader").attr('style','display:none');
    });
}


function openNewContactOfForwarder(id,idCrmEmail,szOperationType,szFlag)
{
    var szSearchStr='';
    if(szFlag=='PENDING_TRAY' || szFlag=='FROM_HEADER')
    {
        $("#loader").attr('style','display:block;');
        if(szFlag=='PENDING_TRAY')
        {
            $("#content_body").attr("style","display:none;");
            $("#content_body").html(''); 
        }
        else if(szFlag=='FROM_HEADER')
        {
            szSearchStr = $("#szSearchString").attr('value');
            $("#contactPopup").html('');
            $("#contactPopup").attr('style','display:none');            
        }
        
        
    }
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{szFlag:szFlag,szOperationType:szOperationType,idCrmEmail:idCrmEmail,idForwarderContact:id,mode:"ADD_EDIT_CONTACT_ONLY_FORWARDER_PROFILE",szSearchStr:szSearchStr},function(result){
        $("#contactPopup").html(result);
        $("#contactPopup").attr('style','display:block');
        
        if(szFlag!='PENDING_TRAY')
        {
            if(parseInt(id)>0)
            {
                enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');
            }
            if(szFlag=='FROM_HEADER')
            {
                $("#loader").attr('style','display:none;');
            }
        }
        else
        {
            $("#loader").attr('style','display:none;');
        }
    });
}

function enableSaveButtonForContactOnlyForwarderProfile(formId)
{
    var error = 1;
    var estimate_error = 1;
    
    if(isFormInputElementEmpty(formId,'idForwarder')) 
    {		
        console.log('Forwarder ID');
        error++; 
    }
    
    /*if(isFormInputElementEmpty(formId,'szMobile')) 
    {		
        console.log('Mobile');
        error++; 
    }
    
    if(isFormInputElementEmpty(formId,'szPhone')) 
    {		
        console.log('Phone');
        error++; 
    }*/
    
    if(isFormInputElementEmpty(formId,'szEmail')) 
    {		
        console.log('Email');
        error++; 
    }
    else if(!isValidEmail($('form#' + formId + ' #szEmail' ).val())) 
    {   
        console.log('Email');
        error++; 
    }
    
    
    /*if(isFormInputElementEmpty(formId,'szResponsibility')) 
    {		
        console.log('Responsibility');
        error++; 
    }
    
    if(isFormInputElementEmpty(formId,'szFirstName')) 
    {		
        console.log('First Name');
        error++; 
    }
    
    if(isFormInputElementEmpty(formId,'szLastName')) 
    {		
        console.log('Last Name');
        error++; 
    }*/
    
    
    if(parseInt(error)==1)
    {
        $("#save-button").attr('style','opacity:1');
        $("#save-button").unbind("click");
        $("#save-button").click(function(){saveContactOnlyProfileData();});
            
    }
    else
    {
         $("#save-button").attr('style','opacity:0.4');
         $("#save-button").unbind("click");
    }
    
}

function saveContactOnlyProfileData()
{
    encode_string("szPhone","szPhoneUpdate","szMobile","szMobileUpdate")
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$('#addNewForwarderContactForm').serialize(),function(result){
         var resArr=result.split("|||||");
        if(jQuery.trim(resArr[0])=='SUCCESS')
        {
            var szOperation_Type = $("#szOperationType").attr('value');
            var szEmail = $("#szEmail").attr('value');
            var idCrmEmail = $("#idCrmEmail").attr('value');
            var szFlag = $("#szFlag").attr('value');
            var szSearchStr = $("#szSearchStr").attr('value');
           
            $("#contactPopup").html('');
            $("#contactPopup").attr('style','display:none');
            
            if(szFlag=='PENDING_TRAY')
            {
                update_crm_operation_reply(''+szEmail+'',''+idCrmEmail+'',''+szOperation_Type+'');
            }
            else if(szFlag=='FROM_HEADER')
            {
                var idForwarderContact=jQuery.trim(resArr[1]);
                display_remind_pane_add_popup('','','','','FROM_HEADER',''+szSearchStr+'',''+idForwarderContact+'');
            }
            else
            {                
                viewAjax('forwarderProife')
            }
        }
        else
        {
            $("#contactPopup").html(resArr[1]);
            $("#contactPopup").attr('style','display:block');
        }
    });
}

function changeCountryAfterSelectingForwarder(idForwarder)
{
    var countryForwarderArr=countryForwarderStr.split(";");
    var countryForwarderlen=countryForwarderArr.length;
    if(countryForwarderlen>0)
    {
        for(i=0;i<countryForwarderlen;++i)
        {
            country_ForwarderArr = countryForwarderArr[i].split("-");
            if(country_ForwarderArr.length>0)
            {
                if(country_ForwarderArr[0]==idForwarder)
                {
                    $("#idCountry").val(country_ForwarderArr[1]);
                }
            }
        }
    }
}

function openMailAddressDeliveryPopup(idBooking)
{
    var iCollection = $("#iCollection").attr('value');
    var dtCollection = $("#dtCollection").attr('value');
    var dtCollectionStartTime = $("#dtCollectionStartTime").attr('value');
    var dtCollectionEndTime = $("#dtCollectionEndTime").attr('value');
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'OPEN_MAILING_ADDESS_DELIVERY_INSTRUCTION',idBooking:idBooking,iCollection:iCollection,dtCollection:dtCollection,dtCollectionStartTime:dtCollectionStartTime,dtCollectionEndTime:dtCollectionEndTime},function(result){
        $("#contactPopup").html(result);
        $("#contactPopup").attr('style','display:block');
    });
}

function addMailAddressDilveryInstruction(idBooking)
{
    //var szMailAddress = $("#szMailAddress").attr('value');
    //var szDeliveryInstructions = $("#szDeliveryInstructions").attr('value');
    var iCollection=$("#iCollection").attr('value');
    var dtCollection=$("#dtCollection").attr('value');
    var dtCollectionStartTime=$("#dtCollectionStartTime").attr('value');
    var dtCollectionEndTime=$("#dtCollectionEndTime").attr('value');
    $("#add_shipping_label_instruction_continue_button").addClass('tyni-loader');
    
    var idCourierProduct = "";
    if($("#idCourierProduct").length)
    {
        idCourierProduct = $("#idCourierProduct").val();
    }
    
    var form_data_serialized = $("#addDeliveryInstructionForm").serialize();
    form_data_serialized += "&addMailAddressInstructionArr[iCollection]="+iCollection;
    form_data_serialized += "&addMailAddressInstructionArr[dtCollection]="+dtCollection;
    form_data_serialized += "&addMailAddressInstructionArr[dtCollectionStartTime]="+dtCollectionStartTime;
    form_data_serialized += "&addMailAddressInstructionArr[dtCollectionEndTime]="+dtCollectionEndTime;
    form_data_serialized += "&addMailAddressInstructionArr[idBooking]="+idBooking;
    form_data_serialized += "&addMailAddressInstructionArr[idCourierProduct]="+idCourierProduct;
    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php?mode=ADD_MAILING_ADDESS_DELIVERY_INSTRUCTION",form_data_serialized,function(result){
        var resArr=result.split("||||");
        if(jQuery.trim(resArr[0])=='SUCCESS')
        {
            var uploadedFileName = resArr[1];
            var szMasterTrackingNumber = resArr[2];
            split_pdf_labels(uploadedFileName,szMasterTrackingNumber);
            
            $("#szMasterTrackingNumber").val(szMasterTrackingNumber); 
            //upload_label_files(uploadedFileName); 
        } 
        else
        {
            $("#contactPopup").html(resArr[1]);
            $("#contactPopup").attr('style','display:block');  
            //alert("Due to internal server error we could not process your request. Please try again later.");
        }
    });
}

function split_pdf_labels(szLabelFileName,szTrackingNumber)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"SPLIT_PDF_LABEL_FILES",file_name:szLabelFileName,tracking_code:szTrackingNumber},function(result){ 
        
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {  
            var uploadedFileName = result_ary[1];
            if(upload_label_files(uploadedFileName))
            { 
                submitForwarderCourierLabel();
            }
            $("#contactPopup").attr('style','display:none;');
        }
        else if(jQuery.trim(result_ary[0])=='SUCCESS_NOTIFICATION')
        {
            var uploadedFileName = result_ary[1];
            upload_label_files(uploadedFileName);
            
            $("#contactPopup").html(result_ary[2]);
            $("#contactPopup").attr('style','display:block;'); 
        }
        else if(jQuery.trim(result_ary[0])=='ERROR')
        { 
            $("#contactPopup").html(result_ary[1]);
            $("#contactPopup").attr('style','display:block;'); 
        }
    });
}

function submitEnterKeySearchPopup(e,divValue)
{
     var keycode;
    keycode = e.which; 
    
    if(keycode==13)
    {
        if(divValue=='SEARCH_POPUP_PENDINGTRAY')
        {
            search_pending_task(0);
        }
    } 
}

function preview_insurance_sheet_before_sending()
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_insurance.php?mode=PREVIEW_INSURANCE_SHEET",$('#send_isurance_vendor_email').serialize(),function(result){
        
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0]) == "SUCCESS")
        {	
            var url = result_ary[1]; 
            redirect_url(url);
            //window.location.href = __JS_ONLY_SITE_BASE__+"/insuranceSheets/"+jQuery.trim(n[1])+".xlsx";
        } 
        else if(jQuery.trim(result_ary[0])== "ERROR")
        {	
            //alert("Nothing available to preview");
            $("#contactPopup").html(resArr[1]);
            $("#contactPopup").attr('style','display:block');  
        }
        else
        {
            alert("Nothing available to preview");
        }
    }) 
}


function showHideSubjectBodyDiv(iNumber)
{
    var i=1;
    var showBodySubjectDivFlag=false;
    $(".pendingTaskRequestQuote").each(function(element)
    {
        div_id ="iOffLine_"+i;
        if($("#"+div_id).is(':checked')==true)
        {
            showBodySubjectDivFlag=true;
        }
        ++i;
    });
    
    div_id ="iOffLine_"+iNumber;
    if($("#"+div_id).is(':checked')==true)
    {
        idForwarder=$("#idForwarder_"+iNumber).val();
        idBooking = $("#idBooking").attr('value');
        $("#loader").attr('style','display:block;');
        $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"SHOW_EMAIL_BODY_SUBJECT_OFF_LINE",idForwarder:idForwarder,idBooking:idBooking},function(result){ 
        var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0]) == "SUCCESS")
        {
            $("#subject-div").attr('style','display:');
            $("#body-div").attr('style','display:');
            
            $("#szEmailSubject").attr('value',result_ary[1]);
            NiceEditorInstance.instanceById('szEmailBody').setContent(result_ary[2]);
            $("#szEmailBody").attr('value',result_ary[2]);

            $("#idLanguage").val(result_ary[3]);
            
        }
        
        $("#loader").attr('style','display:none;');
        });
    }
    else
    {
        if(!showBodySubjectDivFlag)
        {
            $("#subject-div").attr('style','display:none');
            $("#body-div").attr('style','display:none');
        }else
        {
            $("#subject-div").attr('style','display:');
            $("#body-div").attr('style','display:');
        }
    }
}

function updateEmailTemplateForOffLineForwarder(idLanguage)
{    
    idBooking = $("#idBooking").attr('value');
    $("#loader").attr('style','display:block;');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:"SHOW_EMAIL_BODY_SUBJECT_OFF_LINE_BY_LANGUAGE",idLanguage:idLanguage,idBooking:idBooking},function(result){ 
    var result_ary = result.split("||||");
        if(jQuery.trim(result_ary[0]) == "SUCCESS")
        {
            $("#szEmailSubject").attr('value',result_ary[1]);
            NiceEditorInstance.instanceById('szEmailBody').setContent(result_ary[2]);
            $("#szEmailBody").attr('value',result_ary[2]);
            $("#loader").attr('style','display:none;');
        }
    });
}

function loadMoreCustomerLogs(file_id,iHistorical,from_page)
{   
    if(iHistorical==1)
    {
        $("#no_record_found_notification_div").attr('style','display:none;');
        $("#pending_task_show_more_files_customer_log").attr('style','display:none;');
        $("#pending_task_show_more_files_historical").addClass('tyni-loader');  
        var page = $("#iPageCounterHistorical").val();    
    }
    else
    {
        $("#pending_task_show_more_files_historical").attr('style','display:none;');
        $("#pending_task_show_more_files_customer_log").addClass('tyni-loader');  
        var page = $("#iPageCounter").val();   
    } 
    page = parseInt(page);
    page = page+1;
     
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'SHOW_MORE_CUSTOMER_LOGS_ROWS',page:page,file_id:file_id,iHistorical:iHistorical,from_page:from_page},function(result){
	
        var result_ary = result.split("||||");  
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#pending_tray_customer_log_main_container").append(result_ary[1]);	 
        } 
        if(iHistorical==1)
        {
            $("#iPageCounterHistorical").val(page);
            $("#pending_task_show_more_files_historical").removeClass('tyni-loader');  
            $("#pending_task_show_more_files_historical").html('Show More');   
        }
        else
        {
            $("#iPageCounter").val(page);
            $("#pending_task_show_more_files_customer_log").removeClass('tyni-loader');  
        } 
    });
} 

function display_crm_email_details(email_id,file_id)
{    
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",{mode:'DISPLAY_CRM_EMAIL_DETAILS',email_id:email_id,file_id:file_id},function(result){
	
        var result_ary = result.split("||||");  
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#email_body_"+email_id).html(result_ary[1]);	 
           $("#email_body_"+email_id).attr('style','display:block;');
        }   
    });
}

function display_char_counter(input_id,iMaxLength)
{  
    iMaxLength = parseInt(iMaxLength);
    if(iMaxLength>0)
    {
        var szContentStr = $('#'+input_id).val(); 
        var iLength = countUtf8Bytes(szContentStr);
        iLength = parseInt(iLength);
        
        var szLengthStr = "(0/"+iMaxLength+")"; 
        if(iLength>0)
        {
            szLengthStr = "("+iLength+"/"+iMaxLength+")";
        } 
        $('#char_counter_span_'+input_id).html(szLengthStr); 
        if(iLength>iMaxLength)
        {
            $('#char_counter_span_'+input_id).addClass('red_text');
        }
        else
        {
            $('#char_counter_span_'+input_id).removeClass('red_text');
        }
    } 
} 
function countUtf8Bytes(s)
{
    var b = 0, i = 0, c
    for(;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
    return b
}

function enableDeliveryInstructionForm(idBooking)
{
    var szMailAddress = $("#szMailAddress").val();
    var szDeliveryInstructions = $("#szDeliveryInstructions").val();
    var iDisplayCargoDescField = $("#iDisplayCargoDescField").val();
    var iDisplayIndividualLineCargoDescField = $("#iDisplayIndividualLineCargoDescField").val();
    var iCheckShipperCityFlag=$("#iCheckShipperCityFlag").val();
    var iCheckConsigneeCityFlag=$("#iCheckConsigneeCityFlag").val();
    var error_counter = 1;
    if(iCheckShipperCityFlag)
    {
        var szShipperCity=$("#szShipperCity").val();
        if(szShipperCity!='')
        {
            $("#szShipperCity").removeClass("red_border");
        }
        else
        {
            $("#szShipperCity").addClass("red_border");
            error_counter = 2;
        }
    }
    
    if(iCheckConsigneeCityFlag)
    {
        var szConsigneeCity=$("#szConsigneeCity").val();
        if(szConsigneeCity!='')
        {
            $("#szConsigneeCity").removeClass("red_border");
        }
        else
        {
            $("#szConsigneeCity").addClass("red_border");
            error_counter = 2;
        }
    }
    
    
    if(szMailAddress.length>60)
    {
        error_counter = 2;
    }
    if(szDeliveryInstructions.length>60)
    {
        error_counter = 2;
    }
    if(iDisplayCargoDescField==1)
    {
        var szCargoDescription = $("#szCargoDescription").val();
        var iLDesLen = countUtf8Bytes(szCargoDescription);
        if(iLDesLen>30)
        {
            error_counter = 2;
        }
    } 
    if(iDisplayIndividualLineCargoDescField==1)
    {
        var ret_flag = validate_individual_rows();
        if(ret_flag==2)
        {
            error_counter = 2;
        } 
    }
    
    var ret_piece_flag = validate_individual_rows_piecReference();
    if(ret_piece_flag==2)
    {
        error_counter = 2;
    }
        
    var misc_ret_flag = validate_label_shipper_consignee_fields();
    if(misc_ret_flag!=1)
    {
        error_counter = 2;
    }
   
    if(error_counter==1)
    { 
        $("#add_shipping_label_instruction_continue_button").attr('style','opacity:1.0');
        $("#add_shipping_label_instruction_continue_button").unbind("click");
        $("#add_shipping_label_instruction_continue_button").click(function(){
            addMailAddressDilveryInstruction(idBooking);
        });
    }
    else
    {
        $("#add_shipping_label_instruction_continue_button").attr('style','opacity:0.4');
        $("#add_shipping_label_instruction_continue_button").unbind("click");
    }
} 
function validate_individual_rows_piecReference()
{
    var error_counter = 1;
    $.each($(".field_name_key_SELLER_DESCRIPTION"), function(field_key){ 
        var input_id = this.id; 
        
        var SpltSry = input_id.split("szSellerReference_");  
        var id_cargo = SpltSry[1];
                            
        var iColli = $("#iColli_"+id_cargo).val();
        var iMaxLength = 24;
        if(iColli>1)
        {
            iMaxLength = 20;
        } 
        display_char_counter(input_id,iMaxLength);
        var szPieceDescription = this.value;
        var iLDesLen = countUtf8Bytes(szPieceDescription);  
        
        if(iLDesLen>iMaxLength)
        {
            error_counter = 2;
        }
    });
    return error_counter;
}
 
function validate_individual_rows()
{
    var error_counter = 1;
    $.each($(".field_name_key_INDIVIUAL_DESCRIPTION"), function(field_key){ 
        var input_id = this.id 
        display_char_counter(input_id,"30");
        
        var szCargoDescription = this.value;
        var iLDesLen = countUtf8Bytes(szCargoDescription); 
        if(iLDesLen>30)
        {
            error_counter = 2;
        }
    });
    return error_counter;
}

function display_voga_email_data()
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",$("#vogaQuoteEmailForm").serialize(),function(result){ 
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#display_voga_email_contents").html(result_ary[1]); 
        }
    }); 
}

function calculate_insurance_buy_rate()
{
    var fTotalInsuranceBuyRate = 0;
    var iSelectedFlag = 1;
    var iSelectedFlagDiv = 0;
    $(".insured-booking-check-boxes").each(function(element){
        var cb_id = this.id
        var cb_value = this.value;
        var cb_flag = $("#"+cb_id).prop("checked"); 
        
        if(cb_flag)
        {
            
            iSelectedFlag++;
            var fBuyRate = $("#booking_data_"+iSelectedFlagDiv+" #fInsuranceBuyRateDkk_"+cb_value).val();
            fBuyRate = parseFloat(fBuyRate);
            if(!isNaN(fBuyRate))
            {
                fTotalInsuranceBuyRate += fBuyRate;
            }
        }
        ++iSelectedFlagDiv;
    }); 
    
    fTotalInsuranceBuyRate = parseFloat(fTotalInsuranceBuyRate);
    if(!isNaN(fTotalInsuranceBuyRate) && iSelectedFlag!=1)
    {
        var formated_value = number_format(fTotalInsuranceBuyRate);
        $("#insurance_total_buy_rate_container_span").html("DKK "+formated_value);
    }
    else
    {
        $("#insurance_total_buy_rate_container_span").html("None selected");
    }
}


function showEmailTemplatepopupHideOverWritePopup()
{
    $("#remindEmailPopup").attr('style','display:block');
    $("#contactPopup").html('');
    $("#contactPopup").attr('style','display:none');
}

function sort_vat_application(szSortField,szSortOrder)
{ 
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_vatApplication.php",{mode:'SORT_VAT_APPLICATION',szSortField:szSortField,szSortOrder:szSortOrder},function(result){
        
        var result_ary = result.split("||||");  
        if(result_ary[0]=='SUCCESS')
        { 
            $("#vat_appliaction_right").html(result_ary[1]);
        }
    });
}

function selectSearchMiniVerision(idSearchMini)
{
    $("#idCargo").html('<option>Select</option>');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{idSearchMini:idSearchMini,mode:'GET_CATEGORY_LIST_BY_SEARCH_MINI'},function(result){ 
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#idCategory").html(result_ary[1]); 
        }
        else
        {
            $("#idCategory").html(result_ary[1]); 
        }
    }); 
}


function selectCategoryForCargoData(idCategory)
{
    var idSearchMini = $("#idSearchMini").val();
    $("#idCargo").html('<option>Select</option>');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{idCategory:idCategory,mode:'GET_CATEGORY_CARGO_LIST_BY_ID_CATEGORY',idSearchMini:idSearchMini},function(result){ 
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#idCargo").html(result_ary[1]); 
        }
        else
        {
            $("#idCargo").html(result_ary[1]); 
        }
    });
}


function changeCustomerServiceAgentStatus(idAdmin)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{idAdmin:idAdmin,mode:'UPDATE_MANAGEMENT_CUSTOMER_SERVICE_AGENT'},function(result){ 
        
        var result_ary = result.split('||||');
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        {
            $("#link_"+idAdmin).html(result_ary[1]); 
        }
    });
}


function on_enter_key_press_shift_cc(kEvent,szValue)
{
    var szSortValue=''; 
    var idPrimarySortKey=$("#idPrimarySortKey").val();
     var szSortValue=$("#idPrimarySortValue").val();
     
     var idOtherSortKeyArr=$("#idOtherSortKey").attr('value'); 
     
        if (kEvent.shiftKey) 
        { 
            if(idPrimarySortKey==szValue)
            {
	     	if(szSortValue=='ASC')
	     	{
                    szSortValue='DESC';
	     	}
	     	else
	     	{
                    szSortValue='ASC';
	     	} 
	     }  
        if(idPrimarySortKey=='')
        {
            $("#idPrimarySortKey").attr('value',szValue);
            $("#idPrimarySortValue").attr('value',szSortValue);
        }
        if(idPrimarySortKey==szValue)
     	{
     		$("#idPrimarySortValue").attr('value',szSortValue);
     	}
     	else
     	{
     	
	     	if(idOtherSortKeyArr=='')
			{
				var ascValue=szValue+"_ASC";
				$("#idOtherSortKey").attr('value',ascValue);				
			}
			else
			{
				var descValue=szValue+"_DESC";
				var ascValue=szValue+"_ASC";
			
				
				var res_ary_new = idOtherSortKeyArr.split(";");
				var add_truck=jQuery.inArray(descValue, res_ary_new);
				if(add_truck!='-1')
				{			
					res_ary_new = $.grep(res_ary_new, function(value) {
					    return descValue != value;
					});
									
					var value1=res_ary_new.join(';');
					$("#idOtherSortKey").attr('value',value1);
					var idOtherSortKeyArr=$("#idOtherSortKey").val();
					if(idOtherSortKeyArr=='')
					{
						$("#idOtherSortKey").attr('value',ascValue);
					}
					else
					{
						var value1=idOtherSortKeyArr+';'+ascValue;
						$("#idOtherSortKey").attr('value',value1);
					}
				}
				else
				{
					var add_truck=jQuery.inArray(ascValue, res_ary_new);
					if(add_truck!='-1')
					{			
						res_ary_new = $.grep(res_ary_new, function(value) {
						    return ascValue != value;
						});
										
						var value1=res_ary_new.join(';');
						$("#idOtherSortKey").attr('value',value1);
						var idOtherSortKeyArr=$("#idOtherSortKey").val();
						if(idOtherSortKeyArr=='')
						{
							$("#idOtherSortKey").attr('value',descValue);
						}
						else
						{
							var value1=idOtherSortKeyArr+';'+descValue;
							$("#idOtherSortKey").attr('value',value1);
						}
					}
					else
					{
						var value1=idOtherSortKeyArr+';'+ascValue;
						$("#idOtherSortKey").attr('value',value1);
					}
				}
			}
		}
     
    } else {
    	
    	if(idPrimarySortKey==szValue)
     	{
	    	if(szSortValue=='ASC')
	     	{
	     		szSortValue='DESC';
	     	}
	     	else
	     	{
	     		szSortValue='ASC';
	     	}
	     	
	     	$("#idPrimarySortValue").attr('value',szSortValue);
	     	$("#idOtherSortKey").attr('value','');
	     }
	     else
	     {
	     
        	$("#idPrimarySortKey").attr('value',szValue);
        	$("#idPrimarySortValue").attr('value','ASC');
        	$("#idOtherSortKey").attr('value','');
        }
    }
    
     var idPrimarySortKey=$("#idPrimarySortKey").val();
     var szSortValue=$("#idPrimarySortValue").val();
     var idOtherSortKeyArr=$("#idOtherSortKey").val();
     var iDoor2DoorTrades=$("#iDoor2DoorTrades").val();
     
     $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'SORT_CUSTOMER_CLEARANCE',idOtherSortKeyArr:idOtherSortKeyArr,szSortValue:szSortValue,idPrimarySortKey:idPrimarySortKey,iDTDFlag:iDoor2DoorTrades},function(result){
		$("#mode_transport_div").html(result);
		$("#deleteTruckiconId").attr('value','');
                
                $(".moe-transport-sort-span").removeClass('sort-arrow-up');
                $(".moe-transport-sort-span").removeClass('sort-arrow-down');
                if(szSortValue=='ASC')
	     	{
                    $("#moe-transport-sort-span-id-"+szValue).addClass('sort-arrow-up');
                }
                else
                { 
                    $("#moe-transport-sort-span-id-"+szValue).addClass('sort-arrow-down');
                }
	});
}


function deleteCustomerClearanceData()
{
    var deleteTruckiconIdArr=$("#deleteTruckiconId").attr('value');

    var idPrimarySortKey=$("#idPrimarySortKey").val();
    var szSortValue=$("#idPrimarySortValue").val();
    var idOtherSortKeyArr=$("#idOtherSortKey").val();
    var iDoor2DoorTrades=$("#iDoor2DoorTrades").val();
	
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",{mode:'DELETE_CUSTOMER_CLEARANCE_DATA',deleteIdArr:deleteTruckiconIdArr,idOtherSortKeyArr:idOtherSortKeyArr,szSortValue:szSortValue,idPrimarySortKey:idPrimarySortKey,iDTDFlag:iDoor2DoorTrades},function(result){
        $("#mode_transport_div").html(result);
        $("#deleteTruckiconId").attr('value','');
    });
}


function checkCountryIsSelectedForCC()
{
    var ajax_callflag = enableCustomerClearForm();
    if(ajax_callflag)
    {
        var value = $('#customerClearanceAddForm').serialize();
        var newValue=value+'&mode=CHECKING_CC_DATA';

        $.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){

            var result_ary = result.split("||||");   
            if($.trim(result_ary[0])=='SUCCESS')
            {
                $("#add_edit_mode_transport_data").unbind("click");
                $("#add_edit_mode_transport_data").click(function(){addEditCCData();});
                $("#add_edit_mode_transport_data").attr("style",'opacity:1');
            }
            else
            {
                $("#add_edit_mode_transport_data").unbind("click");
                $("#add_edit_mode_transport_data").attr("style",'opacity:0.4');
            }
        });
    } 
}

function enableCustomerClearForm()
{
    var error_count = 1;
    var formId = 'customerClearanceAddForm';
    if(isFormInputElementEmpty(formId,'idFromCountry')) 
    {		 
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'idToCountry')) 
    {		 
        error_count++;
    }
    if(error_count==1)
    {
        return true;
    }
    else
    {
        $("#add_edit_mode_transport_data").unbind("click");
        $("#add_edit_mode_transport_data").attr("style",'opacity:0.4'); 
        return false;
    }
}


function addEditCCData()
{
    var value = $('#customerClearanceAddForm').serialize();
    var newValue=value+'&mode=ADD_EDIT_CUSTOMER_CLEARANCE_DATA';
	 
    var idPrimarySortKey=$("#idPrimarySortKey").val();
    var szSortValue=$("#idPrimarySortValue").val();
    var idOtherSortKeyArr=$("#idOtherSortKey").val();
     
     var newValue=value+'&mode=ADD_EDIT_CUSTOMER_CLEARANCE_DATA&idPrimarySortKey='+idPrimarySortKey+'&szSortValue='+szSortValue+'&idOtherSortKeyArr='+idOtherSortKeyArr;
	
	$.post(__JS_ONLY_SITE_BASE__+"/ajax_courierDetails.php",newValue,function(result){
		var result_ary = result.split("||||"); 
		
		if($.trim(result_ary[0])=='SUCCESS')
		{
			$("#add_edit_mode_transport_data").unbind("click");
		    $("#add_edit_mode_transport_data").attr("style",'opacity:0.4');
		    $("#mode_transport_div").html(result_ary[1]);
		}
		else
		{
                    $("#courier_cargo_limitation_addedit_data").html(result_ary[1]);
                    $("#add_edit_mode_transport_data").unbind("click");
		    $("#add_edit_mode_transport_data").click(function(){addEditCCData();});
		    $("#add_edit_mode_transport_data").attr("style",'opacity:1'); 
		}
	});
} 


function download_admin_self_credit_note_pdf(booking_id)
{	
        window.open(__JS_ONLY_SITE_BASE__+'/selfcreditNote/'+booking_id+'/',"_blank");
}

function download_admin_self_invoice_pdf(booking_id)
{	
        window.open(__JS_ONLY_SITE_BASE__+'/forwarderSelfInvoice/'+booking_id+'/',"_blank");
}


function showCourierProductAttentionPopup(iValue,iCurrentValue)
{
    if(iValue!='' && iCurrentValue!=iValue)
    {
        $('#courier_product_attention_popup').attr('style','display:block;');
    }
}


function downloadGraphDataList(szFlag,szSiteUrl)
{
    if(szFlag=='READ_MESSAGE')
    {
        window.open(szSiteUrl+'/crm/readMessageTaskData.php',"_blank");
    }
    else if(szFlag=='CALL_UESR')
    {
        window.open(szSiteUrl+'/crm/callUserTaskData.php',"_blank");
    }
    else if(szFlag=='MANUAL_RFQ')
    {
        window.open(szSiteUrl+'/crm/manualRfqBookingData.php',"_blank");
    }
    else if(szFlag=='BOOKING_LIST')
    {
        window.open(szSiteUrl+'/testScript/dashboardGraphFor6Month.php',"_blank");
    }
}

function delete_vaga_page_pricing_details(idAutomatedRfq)
{
    $("#loader").attr('style','display:block;')
    $("#contactPopup").html('');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'CONFIRM_DELETE_AUTOMATED_RFQ_RESPONSE_POP',idAutomatedRfq:idAutomatedRfq},function(result){
        $("#contactPopup").attr('style','display:block');
        $("#contactPopup").html(result);
        $("#loader").attr('style','display:none;')
        
    })
}

function open_automated_rfq_popup(idAutomatedRfq)
{
    $("#loader").attr('style','display:block;')
    $("#contactPopup").html('');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'OPEN_AUTOMATED_RFQ_RESPONSE_POP',idAutomatedRfq:idAutomatedRfq},function(result){
        $("#contactPopup").attr('style','display:block');
        $("#contactPopup").html(result);
        $("#loader").attr('style','display:none;')
        if(parseInt(idAutomatedRfq)>0)
        {
            checkLimitForAll('szCommentText',256);
            enable_save_button_for_auto_rfq('szCommentText');
        }
    });
}

function checkLimitForAll(id,limit)
{
        length	=	$('#'+id).val().length;
        value	=	parseInt(limit) -length;
        if(length<=parseInt(limit))
        {
                $('#char_left').html(value);
        }
        else
        {
            if(length > parseInt(limit))
            {
                    text	=	$('#'+id).val();
                    textNew = 	text.substring(0,parseInt(limit));
                    $('#char_left').html(0);
                    $('#'+id).val(textNew);
            }
        }
}


function enable_save_button_for_auto_rfq(id)
{
    input_string=$("#"+id).attr('value');
    if(input_string!='')
    {
        $("#save_automated_rfq").unbind("click");
        $("#save_automated_rfq").click(function(){saveAutomatedRfqResponse();});
        $("#save_automated_rfq").attr("style",'opacity:1'); 
    }
    else
    {
        $("#save_automated_rfq").unbind("click");
        $("#save_automated_rfq").attr("style",'opacity:0.4');
    }
    
}

function saveAutomatedRfqResponse()
{
      $("#save_automated_rfq").unbind("click");
        $("#save_automated_rfq").attr("style",'opacity:0.4');
        
         $("#cancel_button_automated_rfq").unbind("click");
        $("#cancel_button_automated_rfq").attr("style",'opacity:0.4');
        
    var szCommentText=encode_string_msg('szCommentText');
    var idAutomatedRfq=$("#id").attr('value');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'ADD_UPDATE_AUTOMATED_RFQ_RESPONSE',szCommentText:szCommentText,idAutomatedRfq:idAutomatedRfq},function(result){
        var result_ary = result.split("||||"); 
         $("#idAutomatedRfqResponse").html(result_ary[1]);
         $("#contactPopup").html('');
    });
}

function confrimedDeleteAutomatedRfqResponse(idAutomatedRfq)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'CONFIRMED_DELETE_AUTOMATED_RFQ_RESPONSE',idAutomatedRfq:idAutomatedRfq},function(result){
        var result_ary = result.split("||||"); 
         $("#idAutomatedRfqResponse").html(result_ary[1]);
         $("#contactPopup").html('');
         
        $("#standard_cargo_delete_new_button").unbind("click");
        $("#standard_cargo_delete_new_button").attr('style','opacity:0.4');
        $("#standard_cargo_quote_pricing_container").html('');
        
        $('#standard_cargo_select_new_button').attr('onclick','');
        $("#standard_cargo_select_new_button").unbind("click");
        $("#standard_cargo_select_new_button").click(function(){ open_automated_rfq_popup(); });
        $("#standard_cargo_select_new_button_text").html('New');
    });
}

function closeAutomatedRfqResponseInformationSection()
{
    page_id=$("#idAutomatedRfqResponse").attr('value');
    $("#idAutomatedRfqResponseHidden").attr('value','');
    $('#warning_msg').attr('style','display:none;');
    display_pricing_handler(page_id);
    
    
}


function add_more_cargo_details_management_new()
{ 
    var idCustomLandingPage = $("#idLandingPage").val();
    var counter=$("#hiddenPosition_7").val();
    var newCounter=parseInt(counter)+1;
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'ADD_NEW_CARGO_DETAIL_LINE_FOR_VOGA_PAGES_NEW',idCustomLandingPage:idCustomLandingPage,newCounter:newCounter},function(result){ 
      $("#hiddenPosition_7").attr('value',newCounter);
      $("#update_cargo_details_container_div").append(result);
      enable_voga_try_it_out_submit_new();
    });
}


function reset_standard_tryit_out_new(page_id)
{
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'RESET_STANDARD_TRY_IT_OUT_NEW',page_id:page_id},function(result){ 
        var result_ary = result.split("||||"); 
        if(result_ary[0]=='SUCCESS')
        {
            $("#add_standard_quote_pricing_try_id_container").html(result_ary[1]);
        } 
        $("#standard_cargo_try_it_out_result_container").html(" ");
        $("#standard_cargo_try_it_out_result_container").attr('style','display:none;');
    });
}

function enable_voga_try_it_out_submit_new()
{
    var error_count = 1;
    var iVogaPageType = $("#iVogaPageType").val();
    var formId = 'standardPricingTryItOutForm';
   
    if(isFormInputElementEmpty(formId,'szOriginCountry')) 
    {		  
        error_count++;
    }
    if(isFormInputElementEmpty(formId,'szDestinationCountry'))
    {		  
        error_count++;
    }
    var counter=$("#hiddenPosition_7").val();
    for(var i=1;i<=counter;++i)
    {
        var quantityDiv="iQuantity"+i+"_V_7";
        if(isFormInputElementEmpty(formId,quantityDiv))
        {		  
            error_count++;
        }
    }
    
    
    if(error_count==1)
    {
        $("#standard-cargo-test-button").unbind("click");
        $("#standard-cargo-test-button").click(function(){ submit_voga_try_it_out_form_new(); });
        $('#standard-cargo-test-button').attr('style','opacity:1;'); 
    }
    else
    {
        $("#standard-cargo-test-button").unbind("click");
        $('#standard-cargo-test-button').attr('style','opacity:0.4;'); 
    }
}

function submit_voga_try_it_out_form_new()
{ 
    $("#loader").attr('style','display:block');
    var value = $("#standardPricingTryItOutForm").serialize();
    var newValue = value+"&iNewAutoRFQ=1";
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",newValue,function(result){ 
        var result_ary = result.split("||||"); 
        if(jQuery.trim(result_ary[0])=='SUCCESS')
        { 	
           $("#standard_cargo_try_it_out_result_container").html(result_ary[1]);	  
           $("#standard_cargo_try_it_out_result_container").attr('style','display:block;');  
        }   
        else if(jQuery.trim(result_ary[0])=='ERROR')
        { 	
           $("#add_standard_quote_pricing_try_id_container").html(result_ary[1]);	  
        }
        $("#loader").attr('style','display:none');
    }); 
}

function update_cargo_type_management(idCategory,iCounter,iSearchMiniPage)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'SHOW_CATEGORY_CARGO_TYPE',idCategory:idCategory,iCounter:iCounter},function(result){ 
     var divvalue="idStandardCargoType"+iCounter+"_V_7_container";
        $("#"+divvalue).html(result);
        $("#loader").attr('style','display:none');
   });  
}


function update_category_type_management(idSearchMini,iCounter,iSearchMiniPage)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_starndardCargoPricing.php",{mode:'SHOW_CATEGORY_TYPE',idSearchMini:idSearchMini,iCounter:iCounter},function(result){ 
        var result_ary = result.split("||||"); 
        
        var divvalue="idCargoCategory"+iCounter+"_V_7_container";
        $("#"+divvalue).html(result_ary[0]);
        
        var divvalueType="idStandardCargoType"+iCounter+"_V_7_container";
        $("#"+divvalueType).html(result_ary[1]);
        
        $("#loader").attr('style','display:none');
   });  
}

function enableCountrtDropForShipperConsignee(idShipperConsignee)
{
    if(idShipperConsignee==1)
    {
        $("#idOriginCountry").attr('style','');
        $("#idOriginCountry").attr('disabled',false);
    }
    else if(idShipperConsignee==2)
    {
        $("#idDestinationCountry").attr('style','');
        $("#idDestinationCountry").attr('disabled',false);
    } 
    $("#szCustomerCountry").attr('style','');
    $("#szCustomerCountry").attr('disabled',false);
}

function openRequestCallForApi(szErrorCode)
{
    $("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'SHOW_REQUEST_CALL_DATA',szErrorCode:szErrorCode,page:'1'},function(result){ 
            $("#show-request-call").html(result);
            $("#show-request-call").attr('style','display:block');
        $("#loader").attr('style','display:none');
        $("#szErrorCode").attr('value',''+szErrorCode+'');
   });
}

function showPageApiRequest(page)
{
     var szErrorCode = $("#szErrorCode").attr('value');
     //$("#loader").attr('style','display:block');
    $.post(__JS_ONLY_SITE_BASE__+"/ajax_misc.php",{mode:'SHOW_REQUEST_CALL_DATA',szErrorCode:szErrorCode,page:page},function(result){ 
            $("#show-request-call").html(result);
            $("#show-request-call").attr('style','display:block');
       // $("#loader").attr('style','display:none');
   });
}