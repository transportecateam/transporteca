<?php 
/**
  *BILLING DETAILS
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Courier Services";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once(__APP_PATH_LAYOUT__ ."/header_new.php"); 
  
$kCourierService = new cCourierServices();
$kConfig = new cConfig();

$data['szFirstName'] = 'Ajay';
$data['szLastName'] = 'Jha';
$data['szCity'] = 'Herndon';
$data['szState'] = 'VA';
$data['szCountry'] = 'US';
$data['szAddress1'] = 'A-40';
$data['szAddress2'] = 'sector 57';
$data['szPostCode'] = '20171';

$data['szSFirstName'] = 'Ajay';
$data['szSLastName'] = 'Jha';
$data['szSCity'] = 'Collierville';
$data['szSState'] = 'TN';
$data['szSCountry'] = 'US';
$data['szSAddress1'] = 'Address Line 1';
$data['szSAddress2'] = 'sector 57';
$data['szSPostCode'] = '38017';

//$kCourierService->calculateShippingDetails($data);
  
if(!empty($_SESSION['getCourierRateAry']))
{
    $_POST['getCourierRateAry'] = $_SESSION['getCourierRateAry'] ;
}
$_POST['getCourierRateAry']['iProviderType'] = 1; 

?>

<div id="ajaxLogin"></div>
<div id="hsbody-2"> 
    <div class="hsbody-2-right"> 
        <h2>Fedex Courier Service Demo</h2>
        <div id="courier_service_container_div"> 
            <?php echo display_courier_service_demo_form($kCourierService,1); ?>
        </div>
    </div>
</div>

<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
