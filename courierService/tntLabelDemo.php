<?php 
/**
  *BILLING DETAILS
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
$szMetaTitle="Transporteca | Courier Services";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once(__APP_PATH_LAYOUT__ ."/header_new.php");  

$kLabels = new cLabels(); 
echo "<br> Sending TNT Label Request <br>";
$kLabels->createLabelFromTNTApi();
echo "<br> After TNT Label Request <br>";
die;

require_once (__APP_PATH__ ."/TNT/labelConfig.php");

use TNTExpress\Client\TNTClient;
use TNTExpress\Client\SoapClientBuilder;
use TNTExpress\Exception\ClientException;
use TNTExpress\Exception\ExceptionManager;
use TNTExpress\Model\Sender;
use TNTExpress\Model\Receiver;
use TNTExpress\Model\ParcelRequest;
use TNTExpress\Model\PickUpRequest;
 

$builder = new SoapClientBuilder('TransportA', 'tnt12345');
$soapClient = $builder->createClient(true);

$TNTClient = new TNTClient($soapClient, new ExceptionManager());
 
$sender = new Sender();
$sender->setName('Ajay Jha');
$sender->setAddress1('Bandra Street');
$sender->setZipCode('400050');
$sender->setCity('Mumbai');

$receiver = new Receiver();
$receiver->setAddress1('Dansk Street');
$receiver->setZipCode('2300');
$receiver->setCity('Denmark');
$receiver->setContactFirstName('Ajay');
$receiver->setContactLastName('Jha');
$receiver->setEmailAddress('ajay@whiz-solutions.com');
$receiver->setPhoneNumber('0235760912');
$receiver->setType('INDIVIDUAL');
//$receiver->setName('Fleuriste');
//$receiver->setTypeId('K2023');

$parcelRequest1 = new ParcelRequest();
$parcelRequest1->setSequenceNumber(1);
$parcelRequest1->setWeight('5');

$parcelRequest2 = new ParcelRequest();
$parcelRequest2->setSequenceNumber(2);
$parcelRequest2->setWeight('6');

$pickupRequest = new PickUpRequest('0235760912', 'ajay@whiz-solutions.com', '18:00');

$expeditionRequest = new \TNTExpress\Model\ExpeditionRequest();
$expeditionRequest->setShippingDate(new \Datetime('2016-12-20'));
$expeditionRequest->setAccountNumber('2013579');
$expeditionRequest->setSender($sender);
$expeditionRequest->setReceiver($receiver);
     
try {  
    $feasibility = $TNTClient->getFeasibility($expeditionRequest, $filter);
echo "Feasility <br>";
print_R($feasibility);
die;
    var_dump($feasibility);
} catch (\SoapFault $e) {
    echo "exception... <br><br>";
    print_R($e);
    var_dump($e->getMessage());
}

$expeditionRequest->setQuantity(2);
$expeditionRequest->setParcelsRequest(array($parcelRequest1, $parcelRequest2));
$expeditionRequest->setServiceCode($feasibility[0]->getServiceCode());
$expeditionRequest->setPickupRequest($pickupRequest);

try {
    $result = $TNTClient->createExpedition($expeditionRequest);

    print_R($result);
    file_put_contents('test.pdf', $result->getPDFLabels());
    var_dump($result);
} catch (ClientException $e) {
    var_dump($e->getMessage());
}
