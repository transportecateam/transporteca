<?php 
/**
  *BILLING DETAILS
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Courier Services";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once(__APP_PATH_LAYOUT__ ."/header_new.php"); 

$kCourierService = new cCourierServices(); 

if(!empty($_SESSION['getCourierRateAry']))
{
    $_POST['getCourierRateAry'] = $_SESSION['getCourierRateAry'] ;
}
$_POST['getCourierRateAry']['iProviderType'] = 4; 
?>
<style type="text/css">
.format-2 td{text-align: left;padding:10px;}
</style>
<div id="ajaxLogin"></div>
<div id="hsbody-2"> 
    <div class="hsbody-2-right"> 
        <h2>TNT Courier Service Demo</h2>
        <div id="courier_service_container_div">  
            <?php echo display_courier_service_new_demo_form($kCourierService,4); ?>
        </div>
    </div>
</div>

<?php
//include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>