<?php
ob_start();
session_start();
 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$display_abandon_message = true;
$iDonotIncludeHiddenSearchHeader = 1; 
$szMetaTitle = __BOOKING_CONFIRMATION_META_TITLE__;
$szMetaKeywords = __BOOKING_CONFIRMATION_META_KEYWORDS__;
$szMetaDescription = __BOOKING_CONFIRMATION_META_DESCRIPTION__;

require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingConfirmation/";
$t_base_details = "BookingDetails/";

$iLanguage = getLanguageId();

$kBooking = new cBooking();
$kConfig = new cConfig();
$kRegisterShipCon = new cRegisterShipCon();
$kWHSSearch = new cWHSSearch(); 

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
 
if((int)$_SESSION['user_id']<=0)
{ 
    $postSearchAry = $kBooking->getBookingDetails($idBooking); 
    $kConfig_new = new cConfig(); 
    $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig_new->szCountryISO ;  
    $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
    $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
    
    $redirect_url=  __BOOKING_GET_RATES_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
    header("Location: ".$redirect_url);
    exit(); 
}

$_SESSION['load-booking-details-page-for-booking-id'] = $szBookingRandomNum ;

$userSessionData = array();
$userSessionData = get_user_session_data(true);
$idUserSession = $userSessionData['idUser'];

constantApiKey();
 
$postSearchAry = array();
$postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);

if($postSearchAry['iAnonymusBooking']==1 && $_SESSION['user_id']!=$postSearchAry['idUser'])
{
    //This means this booking was creatde by Transporteca's anonymous user and it must be assigned to Logged user
    $kWHSSearch->updateAnonymousBookingFlag($idBooking,true); 
}

/**
 * From now we have enabled a feature that any user can see any other user's booking so we just assign booking to logged in user always
 * 
if(!$kBooking->isBookingBelongsToCustomer($idBooking,$idUserSession))
{
    echo display_booking_invalid_user_popup($t_base_details);
    die;
}  
 * */ 
if((int)$idUserSession<=0)
{
    header("Location:".__NEW_BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum.'/');
    die;
}

$postSearchAry = array();
$postSearchAry = $kBooking->getBookingDetails($idBooking);
if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
{
    echo display_booking_already_paid($t_base,$idBooking);
    die;
} 
if($_SESSION['booking_id']==$idBooking)
{
    $kBooking->updatePaymentFlag($idBooking,0);
    $_SESSION['booking_id'] = '';
    unset($_SESSION['booking_id']);
}
if((int)$_REQUEST['idBooking']>0)
{
    $idBooking = (int)$_REQUEST['idBooking'] ;
    //$_SESSION['booking_id'] = $idBooking ;
}
if((int)$idBooking>0)
{
    $serviceTypeAry= array();
    $postSearchAry = array();
    $cargoAry = array();
    $shipperConsigneeAry = array();

    if((int)$idBooking>0 && (int)$idUserSession>0)
    {
        $kUser->getUserDetails($idUserSession);
        
        $res_ary=array();
        $res_ary['idUser'] = $kUser->id ;				
        $res_ary['szFirstName'] = $kUser->szFirstName ;
        $res_ary['szLastName'] = $kUser->szLastName ;
        $res_ary['szEmail'] = $kUser->szEmail ;	
        $res_ary['szCustomerAddress1'] = $kUser->szAddress1;
        $res_ary['szCustomerAddress2'] = $kUser->szAddress2;
        $res_ary['szCustomerAddress3'] = $kUser->szAddress3;
        $res_ary['szCustomerPostCode'] = $kUser->szPostcode;
        $res_ary['szCustomerCity'] = $kUser->szCity;
        $res_ary['szCustomerCountry'] = $kUser->szCountry;
        $res_ary['iPrivateShipping'] = $kUser->iPrivate;
        $res_ary['szCustomerState'] = $kUser->szState;
        $res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
        $res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
        $res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;	
        if((int)$postSearchAry['iBookingStep']<11)
        {
            $res_ary['iBookingStep'] = 11 ;
        }

        if(!empty($res_ary))
        {
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }		
        $update_query = rtrim($update_query,",");
        $kBooking->updateDraftBooking($update_query,$idBooking);
    }
    $iLanguage = getLanguageId();
    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking,$iLanguage);

    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig->szCountryISO ;
    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
    $szCountryStrUrl .= $kConfig->szCountryISO ; 

    $redirect_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
 
    if($postSearchAry['iCourierBooking']==1)
    { 
        if($postSearchAry['iPaymentProcess']==1)
        { 
            /*
                ?>
                <div id="change_price_div">
                <?php
                display_booking_notification($redirect_url);
                echo "</div>"; 
             */
        }
    }
    else if(($postSearchAry['idForwarder']<=0) || ($postSearchAry['idShipperConsignee']<=0) || ($postSearchAry['idWarehouseTo']<=0) || ($postSearchAry['idWarehouseFrom']<=0) || ($postSearchAry['iPaymentProcess']==1))
    {  /*
        ?>
        <div id="change_price_div">
        <?php
        display_booking_notification($redirect_url);
        echo "</div>"; */
    }
    else
    {
        echo '<div id="change_price_div" style="display:none;"></div>';
    }

    //print_r($postSearchAry);

    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
    // geting  service type 
    $serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
    $serviceTypeAry=$serviceTypeAry[$postSearchAry['idServiceType']];         
    $szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br> ".$postSearchAry['szOriginCountry'];
    
    $szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br> ".$postSearchAry['szDestinationCountry']; 
}
else
{
    //header("Location:".__HOME_PAGE_URL__);
    //die;
    $redirect_url = __HOME_PAGE_URL__ ;
    ?>
    <div id="change_price_div">
    <?php
    display_booking_notification($redirect_url);
    echo "</div>";
}

if(((int)$idUserSession>0) && ($_REQUEST['err']<=0))
{
    checkUserPrefferedCurrencyActive();
}

/**
* This function recalculate pricing after a perticular interval set in constants.
* 
**/
$red_url = __BOOKING_CONFIRMATION_PAGE_URL__.'/'.$szBookingRandomNum.'/'; 
  

//$cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($idBooking);
$szCargoCommodity = $postSearchAry['szCargoDescription'];

$cargo_volume = format_volume($postSearchAry['fCargoVolume']);
$cargo_weight = number_format_custom((float)ceil($postSearchAry['fCargoWeight'])) ;

if($iLanguage==__LANGUAGE_ID_DANISH__)
{
    $szSecureImageName	= "secure-payment-da.png";
    //$cargo_weight = number_format((float)ceil($postSearchAry['fCargoWeight']),0,'.','.');
}
else
{
    $szSecureImageName = "secure-payment.png";
    //$cargo_weight = number_format((float)ceil($postSearchAry['fCargoWeight']),0,'.',',');
}
$cargo_weight=  number_format_custom($postSearchAry['fCargoWeight'], $iLanguage,0);

if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
{
    $szBankTransferDueDateTime = strtotime($postSearchAry['dtCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60); 

    $iMonth = date('m',$szBankTransferDueDateTime);

    $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime); 
    $szMonthName = getMonthName($iLanguage,$iMonth);
    $szBankTransferDueDateTime .=" ".$szMonthName ;
}
else
{
    $szBankTransferDueDateTime = strtotime($postSearchAry['dtWhsCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);

    $iMonth = date('m',$szBankTransferDueDateTime); 
    $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);  

    $szMonthName = getMonthName($iLanguage,$iMonth);
    $szBankTransferDueDateTime .=" ".$szMonthName ;
}     
$szClassName_1 = "fl-40";
$szClassName_2 = "fl-60";

$idCurrency = $postSearchAry['idCustomerCurrency'];
$currencyDetailsAry = array();
$currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);  

$szTermsAndConditionText = terms_condition_popup_html($postSearchAry,$t_base,$iLanguage,true); 
  
if(empty($postSearchAry['szCargoType']))
{
    $postSearchAry['szCargoType'] = '__GENERAL_CODE__';
}

$iShoInsuranceFlag = false; 
$insuranceDataAry = array();
$insuranceDataAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
$insuranceDataAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry']; 
$insuranceDataAry['idTransportMode'] = $postSearchAry['idTransportMode']; 
$insuranceDataAry['iPrivate'] = (int)$postSearchAry['isMoving']; 
$insuranceDataAry['szCargoType'] = $postSearchAry['szCargoType']; 
 
$idTransportMode = $postSearchAry['idTransportMode']; 

$kInsurance = new cInsurance();
$buyRateListAry = array();
$buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_BUY_RATE_TYPE__);

if(!empty($buyRateListAry))
{
   if(!empty($buyRateListAry[$idTransportMode]))
   {
       $buyRateListAry[$idTransportMode] = $buyRateListAry[$idTransportMode] ;
   }
   else if(!empty($buyRateListAry[__ALL_WORLD_TRANSPORT_MODE_ID__]))
   {
       $buyRateListAry[$idTransportMode] = $buyRateListAry[__ALL_WORLD_TRANSPORT_MODE_ID__] ;
   }
   else if(!empty($buyRateListAry[__REST_TRANSPORT_MODE_ID__]))
   {
       $buyRateListAry[$idTransportMode] = $buyRateListAry[__REST_TRANSPORT_MODE_ID__] ;
   }
} 

$insuranceSellRateAry = array();
$insuranceSellRateAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_SELL_RATE_TYPE__);
 
if(!empty($insuranceSellRateAry))
{
    if(!empty($insuranceSellRateAry[$idTransportMode]))
    {
        $insuranceSellRateAry[$idTransportMode] = $insuranceSellRateAry[$idTransportMode] ;
    }
    else if(!empty($insuranceSellRateAry[__ALL_WORLD_TRANSPORT_MODE_ID__]))
    {
        $insuranceSellRateAry[$idTransportMode] = $insuranceSellRateAry[__ALL_WORLD_TRANSPORT_MODE_ID__] ;
    }
    else if(!empty($insuranceSellRateAry[__REST_TRANSPORT_MODE_ID__]))
    {
        $insuranceSellRateAry[$idTransportMode] = $insuranceSellRateAry[__REST_TRANSPORT_MODE_ID__] ;
    }
}  
/*
* If we have both insurance sell and buy rate for trade then only we display insurance options
*/
if(!empty($buyRateListAry[$idTransportMode]) && !empty($insuranceSellRateAry[$idTransportMode]))
{
    $iShoInsuranceFlag = true;
} 
if($iShoInsuranceFlag && ($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceUpdatedFlag']==1))
{  
    /*
    * If Insurance optionas are available to show and customer has opted Insurance: Yes before then we recalculate insurance price
    */
    if($postSearchAry['idGoodsInsuranceCurrency']>0)
    {
        /*
         * We only update Insurance values if customer is revisiting /overview/ page and already have updated insurance value in last visit. 
         * $postSearchAry['idGoodsInsuranceCurrency']>0 means customer has already been updated insurance value 
         */
        $insuranceInputAry = array();
        $insuranceInputAry['iInsurance'] = 1;
        $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
        $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fValueOfGoods'];
        $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry);$insuranceInputAry = array();
        $insuranceInputAry['iInsurance'] = 1;
        $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
        $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fValueOfGoods'];
        $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry);
    } 
} 

//unset($_SESSION['booking_id']);
$iDisplayCCOption = true;

$iShipperConsignee = $postSearchAry['iShipperConsignee']; 
?> 
<script src="https://checkout.stripe.com/checkout.js"></script>        
<script src="<?php echo __BASE_STORE_SECURE_JS_URL__;?>/nanoslider.js?v=2" type="text/javascript"></script>
<script type="text/javascript">
    $().ready(function() {	
        $(".nano").nanoScroller({scroll: 'top'});
    });
</script>
<!-- These are used to contain data from diffrent popups -->
<div id="customs_clearance_pop" class="help-pop"></div>
<div id="customs_clearance_pop_right" class="help-pop right"></div>
<div id="rating_popup"></div>
<div id="change_price_div" style="display:none">
<?php
if(!empty($_SESSION['booking_id']) && ($_SESSION['booking_id'] != $idBooking))
{
    echo display_booking_not_found_popup(__MAIN_SITE_HOME_PAGE_URL__."".$_SERVER['REQUEST_URI']);
}
?>
</div>	
<div id="booking_confirm_error" class="errorBox " style="display:none;"></div>
<div id="account_confirm_resend_email" style="display:none;"></div>	
	
<div id="hsbody-2">	  
        <div class="hsbody-2-right"> 
        <?php  
            if($_SESSION['statusFailed']=='Failed')
            {
                unset($_SESSION['statusFailed']);
                $_SESSION['statusFailed']='';
        ?>
                <div id="regError" class="errorBox ">
                    <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
                    <div id="regErrorList">
                        <?php
                        if($_SESSION['szErrorMsg']!='')
                        {
                           ?>
                            <ul>
                                <li><?=t($t_base_details.'messages/payment_declined').": ".$_SESSION['szErrorMsg'];?></li>
                            </ul>
                        <?php
                            unset($_SESSION['szErrorMsg']);
                            $_SESSION['szErrorMsg']='';
                        }else{?>
                        <ul>
                            <li><?=t($t_base_details.'messages/payment_transaction_is_failed');?></li>
                        </ul>
                        <?php }?>
                    </div>
                </div> 
        <?php } ?>
        <div id="booking_details_container" class="clearfix"> 
            <div class="oh even">
                <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/forwarder');?>:</p>
                <p class="<?php echo $szClassName_2; ?>">
                    <a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$postSearchAry['idForwarder']?>')">
                        <?php if($postSearchAry['szForwarderLogo']!='') { ?>
                            <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$postSearchAry['szForwarderLogo']."&w=180&h=70"?>" alt="<?=$postSearchAry['szForwarderDispName']?>" border="0" />
                        <?php } else{ echo $postSearchAry['szForwarderDispName']; } ?>
                    </a>
                </p>
            </div>
            <div class="oh odd">
                <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/service');?>:</p>
                <p class="<?php echo $szClassName_2; ?>">
                <?php 	
                    $szServiceDescription = getServiceDescription($postSearchAry);
                    echo $szServiceDescription;
                ?>	
                </p>
            </div>
            <div class="oh even">
                <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/cargo_details');?>:</p>
                <p class="<?php echo $szClassName_2; ?>">
                <?php 
                    $szCourierCargo = "";
                    if($postSearchAry['iCourierBooking']==1)
                    {
                        $szCourierCargo = "".$postSearchAry['iNumColli']." ".t($t_base.'fields/colli').", ";
                    }
                    if($iLanguage==__LANGUAGE_ID_DANISH__)
                    {
                         $cargoVolumeText=$cargo_volume." m<sup>3</sup>";
                    }
                    else
                    {
                        $cargoVolumeText=$cargo_volume." ".t($t_base.'fields/cbm');
                    }
                    echo $cargoVolumeText.", ".$cargo_weight." ".t($t_base.'fields/kg').", ".$szCourierCargo.utf8_decode($szCargoCommodity); 
                ?>
                </p>
            </div> 
            <div class="oh odd">
                <p class="<?php echo $szClassName_1; ?>">
                    <?php  
                            echo t($t_base.'fields/expacted_delivery').":"; 
                    ?>
                </p>
                <p class="<?php echo $szClassName_2; ?>">
                <?php 
                    $dtExpactedDelivery = date('j.',strtotime($postSearchAry['dtAvailable'])); 
                    $iMonth = date('m',strtotime($postSearchAry['dtAvailable']));
                    $szMonthName = getMonthName($iLanguage,$iMonth);
                    $dtExpactedDelivery .=" ".$szMonthName ;
                    echo $dtExpactedDelivery; 
                ?>
                </p>
            </div>  
            <div class="oh even">
                <p class="<?php echo $szClassName_1; ?>">
                <?php 
                    if($postSearchAry['iCourierBooking']==1)
                    {
                        echo t($t_base.'fields/pickup_after').":";
                    }
                    else
                    {
                        if(($iShipperConsignee==1 || $iShipperConsignee==3) && ($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__))
                        {
                          echo t($t_base.'fields/shipper').":";  
                        }else{
                            echo t($t_base.'fields/supplier').":";
                        }
                    }
                ?>
                </p>
                <p class="<?php echo $szClassName_2; ?>">
                    <?php
                        $shipperAddress = '';
                        if(!empty($postSearchAry['szShipperCompanyName']))
                        {
                            $shipperAddress.=$postSearchAry['szShipperCompanyName'];
                        }  
                        if(!empty($postSearchAry['szShipperAddress_pickup']))
                        {
                            $shipperAddress.= ", ".$postSearchAry['szShipperAddress_pickup'];
                        }
                        if(!empty($postSearchAry['szShipperAddress2']))
                        {
                            $shipperAddress .=", ".$postSearchAry['szShipperAddress2'] ;
                        }
                        if(!empty($postSearchAry['szShipperAddress3']))
                        {
                            $shipperAddress .=", ".$postSearchAry['szShipperAddress3'];
                        }
                        if(!empty($postSearchAry['szShipperPostCode']) || !empty($postSearchAry['szShipperCity']))
                        {
                            if(!empty($postSearchAry['szShipperPostCode']) && !empty($postSearchAry['szShipperCity']))
                            {
                                $shipperAddress.=", ".$postSearchAry['szShipperPostCode']." ".$postSearchAry['szShipperCity']."";
                            }
                            else if(!empty($postSearchAry['szShipperPostCode']))
                            {
                                $shipperAddress.=", ".$postSearchAry['szShipperPostCode'];
                            }
                            else if(!empty($postSearchAry['szShipperCity']))
                            {
                                $shipperAddress.=", ".$postSearchAry['szShipperCity'];
                            }
                        } 
                        echo $shipperAddress
                    ?>
                    </p>
                </div>
                <div class="oh odd">
                    <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/delivery');?>:</p>
                    <p class="<?php echo $szClassName_2; ?>">
                        <?php
                            $ConsigneeAddress = '';
                            if(!empty($postSearchAry['szConsigneeCompanyName']))
                            {
                                $ConsigneeAddress.=$postSearchAry['szConsigneeCompanyName'].", ";
                            }  
                            if(!empty($postSearchAry['szConsigneeAddress']))
                            {
                                $ConsigneeAddress.=$postSearchAry['szConsigneeAddress'];
                            }
                            if(!empty($postSearchAry['szConsigneeAddress2']))
                            {
                                $ConsigneeAddress .=", ".$postSearchAry['szConsigneeAddress2'];
                            }
                            if(!empty($postSearchAry['szConsigneeAddress3']))
                            {
                                $ConsigneeAddress .=", ".$postSearchAry['szConsigneeAddress3'];
                            }
                            if(!empty($postSearchAry['szConsigneePostCode']) || !empty($postSearchAry['szConsigneeCity']))
                            {
                                $ConsigneeAddress.=", ".$postSearchAry['szConsigneePostCode']." ".$postSearchAry['szConsigneeCity']."";
                            } 
                            echo $ConsigneeAddress
                        ?>
                    </p>
                </div>  
                <div class="oh even">
                    <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/price');?>:</p> 
                    <p class="<?php echo $szClassName_2; ?>" id="booking_price_span"> 
                        <?php  
                            if($postSearchAry['iInsuranceIncluded']==1)
                            {
                                echo $postSearchAry['szCurrency']." ".number_format((float)($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']))." (".t($t_base.'fields/including_insurance')." ".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).")"; 
                            }
                            else
                            {
                                echo $postSearchAry['szCurrency']." ".number_format((float)($postSearchAry['fTotalPriceCustomerCurrency']));
                            }
                        ?>
                    </p>
                </div>
                <?php 
                $vatFlag=0;
                if($postSearchAry['iCourierBooking']==1) {
                    $bankTransferText=t($t_base.'messages/bank_transfer_text_courier');
                }
                else {
                    $bankTransferText=t($t_base.'messages/bank_transfer_text')." ".$szBankTransferDueDateTime;
                }

                if($postSearchAry['fTotalVat']>0) {
                    $vatFlag=1;
                    $fTotalVAT = number_format((float)$postSearchAry['fTotalVat'],2); 

                    ?>
                <div class="oh odd">
                    <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/vat');?>:</p> 
                    <p class="<?php echo $szClassName_2; ?>"> 
                        <?php   
                            echo $postSearchAry['szCurrency']." ".$fTotalVAT; 
                        ?>
                    </p>
                </div>
                <?php } if($iShoInsuranceFlag){  
                    if($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceUpdatedFlag']==1)
                    {
                        $iInsuranceSelectedFlag = 1;
                    }
                ?>
                <div class="oh <?php if($vatFlag==1){?>even <?php }else{?> odd <?php }?>">
                    <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/insurance_main_heading');?>?</p>
                    <p class="<?php echo $szClassName_2; ?>"> 
                        <?php echo t($t_base.'fields/insurance_heading');?> 
                        <span class="pg-more"><a href="javascript:void(0);" onclick="display_insurance_terms(1);"><?php echo t($t_base.'fields/read_more_insurance');?></a></span>
                    </p>
                </div>	
                <div id="booking_confirmation_insurance_container"> 
                    <?php 

                        echo display_booking_insurance_form($kBooking,$postSearchAry);
                    ?> 
                </div>
                <?php } else {   $iInsuranceSelectedFlag = 1; ?>
                    <div class="oh <?php if($vatFlag==1){?>even <?php }else{?> odd <?php }?>">
                        <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/insurance'); ?>:</p> 
                        <p class="<?php echo $szClassName_2; ?>"><?php echo t($t_base.'fields/not_included'); ?></p>
                    </div>
                <?php }?>
                <div class="oh <?php if($vatFlag==1){?>odd <?php }else{?> even <?php }?>">
                    <p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/price_guarntee_included');?>:</p>
                    <div class="<?php echo $szClassName_2; ?>"> 
                        <p class="price-guarantee-container">
                            <img src="<?php echo __BASE_STORE_IMAGE_URL__."/CONFIRMATION_Award.png"; ?>">
                            <?php echo t($t_base.'fields/find_the_service');?>  
                            <span class="pg-more"><a href="javascript:void(0);" id="price_gurantee_more_span" onclick="toggle_price_gurantee('SHOW')"><?php echo t($t_base.'fields/read_more'); ?></a></span>
                        </p>
                        <div id="price_gurantee_details_text" style="display:none;"> 
                            <p>
                                <?php echo t($t_base.'fields/price_guarantee_details_text_1');?> 
                            </p> 
                            <p class="mrgtop8">
                                <?php echo t($t_base.'fields/price_guarantee_details_text_2');?>
                                <span class="pg-more"><a href="javascript:void(0);" id="price_gurantee_less_span" onclick="toggle_price_gurantee('HIDE')" style="display:none;"><?php echo t($t_base.'fields/read_less'); ?></a></span>
                            </p>
                        </div> 
                    </div>
                </div>		  
                <form action="" name="booking_confirmation_form" id="booking_confirmation_form" method="post">
                    <div class="oh <?php if($vatFlag==1){?>even <?php }else{?> odd <?php }?>">
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/payment');?>:</p>
                        <p class="<?php echo $szClassName_2; ?> insurance-checkbox">
                            <?php if($postSearchAry['szCurrency']!='HKD' && $iDisplayCCOption){ ?>
                                <span class="checkbox-container">
                                    <input type="radio" onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" name="bookingConfirmAry[iBookingPayment]" id="ZoozPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_1__; ?>" checked="checked"/><?=t($t_base.'fields/credit_card');?> 
                                </span> 
                            <?php } ?>
                            <?php //if(!empty($currencyDetailsAry['szBankName'])){ ?>
                            <span class="checkbox-container">
                                <input type="radio"  onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" name="bookingConfirmAry[iBookingPayment]" id="iBankTransferPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_3__; ?>"><?=t($t_base.'fields/bank_transfer');?>
                                <span id="bank_transfer_message_span" style="display:none;"><?php echo $bankTransferText; ?></span>
                            </span> 
                            <?php //} 
                            
                            $kWhsSearch = new cWHSSearch();
                            $ShowPaypalOption=false;
                            $iPaypalStatus = $kWhsSearch->getManageMentVariableByDescription('__PAYPAL_STATUS__');
                            if($iPaypalStatus=='ENABLED')
                            {
                                $kAdmin = new cAdmin();
                                if(!$kAdmin->getListPaypalCoutries($postSearchAry['szCustomerCountry']))
                                {
                                    $ShowPaypalOption=true;
                                }
                                //echo $postSearchAry['szCustomerCountry'];
                            }
                            ?>
                            <input type="hidden" name="bookingConfirmAry[szPaybuttonName]" value="<?php echo t($t_base.'fields/pay'); ?>" id="szPaybuttonName">
                            <input type="hidden" name="bookingConfirmAry[szConfirmbuttonName]" value="<?php echo t($t_base.'fields/confirm'); ?>" id="szConfirmbuttonName">
                            <?php if($ShowPaypalOption){?>
                            <span class="checkbox-container">
                                <input type="radio"  name="bookingConfirmAry[iBookingPayment]" onclick="display_payment_tooltip('<?php echo t($t_base.'fields/pay'); ?>','<?php echo t($t_base.'fields/confirm'); ?>')" id="PaypalPayment" value="<?php echo __TRANSPORTECA_PAYMENT_TYPE_2__; ?>"><?=t($t_base.'fields/paypal');?>
                            </span>
                            <?php }?>
                        </p>
                    </div>  
                    <div class="oh <?php if($vatFlag==1){?>odd <?php }else{?> even <?php }?>"> 
                        <p class="<?php echo $szClassName_1; ?>"><?=t($t_base.'fields/terms');?>:</p>
                        <div class="<?php echo $szClassName_2; ?>">
                            <div class="terms-text"> 
                                <div class="nano has-scrollbar">
                                    <div class="content">
                                        <?php echo $szTermsAndConditionText; ?>
                                    </div>
                                </div> 
                            </div>
                            <p class="payment-options-checkbox clearfix">
                                <span class="checkbox-container"><input type="checkbox" id='iTerms' onclick="hide_tool_tip('error_message_container_div','1')" name="bookingConfirmAry[iTerms]" <? if((int)$postSearchAry['iAcceptedTerms']==1){ ?>checked<? } ?>  value="1" /><?=t($t_base.'fields/read_terms_condition');?></span>
                                <a href="javascript:void(0);" class="print-icon" onclick="open_terms_condition_popup('<?php echo __NEW_BOOKING_TERMS_PAGE_URL__."/".$_REQUEST['booking_random_num']."/"?>');">print</a>
                            </p> 
                            <input type="hidden" name="bookingConfirmAry[iRemindToRate]" value="1"  />
                            <input type="hidden" name="bookingConfirmAry[iInsuranceSelectedFlag]" id="iInsuranceSelectedFlag" value="<?php echo $iInsuranceSelectedFlag; ?>"  />
                            <input type="hidden" name="bookingConfirmAry[szBookingRandomNum]" id="szBookingRandomNum_booking_conf" value="">
                            <input type="hidden" name="bookingConfirmAry[iBookingStatus]" id="iBookingStatus" value="3"> 
                        </div>
                    </div>	
                </form> 
                <div class="error_message_container" id="error_message_container_div" style="visibility:hidden;">
                    <ul>
                        <li>Please accept our terms to complete the booking</li>
                    </ul>
                </div> 
                <br> 
                <div class="booking_details_buttons clearfix">
                    <p class="back-button"><a href="<?php echo $redirect_url; ?>"><span><< <?=t($t_base.'fields/back');?></span></a></p>
                    <p class="continue-button">
                        <a  href="javascript:void(0)" onclick="booking_on_hold('CONFIRMED');" class="button1" id="pay_button"><span><?=t($t_base.'fields/pay');?></span></a>
                        <span class="pay-later-button">
                            <a href="javascript:void(0)" onclick="booking_on_hold('ON_HOLD');"><span><?=t($t_base.'fields/or_save_for_later');?></span></a>
                        </span>
                    </p> 
                    <?php
                        $item_ref_text = createStripeDescription($postSearchAry); 
                        if($postSearchAry['iInsuranceIncluded']==1)
                        {
                            $fTotalCostForBooking =  $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] ; 
                        }
                        else
                        {
                            $fTotalCostForBooking = $postSearchAry['fTotalPriceCustomerCurrency'];
                        } 

                        $fTotalCostForBooking = ($fTotalCostForBooking + $postSearchAry['fTotalVat']); 
                        $fTotalCostForBookingCent = $fTotalCostForBooking * 100;
                    ?>
                    <input type="hidden" name="stripe_refersh" value="1" id="stripe_refersh">
                    <div id="pay_stripe_button" style="display:none;">
                            <form action="<?=__MAIN_SITE_HOME_PAGE_URL__?>/stripe/callback.php" method="POST" id="payForm"><script src="https://checkout.stripe.com/checkout.js" class="stripe-button" data-key="<?php echo __STRIPE_API_KEY__; ?>" data-amount="<?php echo $fTotalCostForBookingCent?>" data-name="Transporteca" data-description="<?=$item_ref_text?>" data-image="http://staging.transporteca.com/images/favicon.png" data-currency="<?=$postSearchAry['szCurrency']?>" data-email="<?=$kUser->szEmail?>" data-locale="auto"></script></form>
                    </div>
                </div> 
            </div>		
        </div>

        <!-- 
        <div id="booking_confirmation_insurance_container">
                <?php
                        //echo display_booking_insurance_form($kBooking,$postSearchAry);
                ?>
        </div>   
        <div class="booking_details_buttons clearfix fixed-body-container">
                <p class="back-button"><a href="<?php echo __NEW_SERVICES_PAGE_URL__.'/'.$_REQUEST['booking_random_num'].'/'?>"><span><< <?=t($t_base.'fields/back');?></span></a></p>
                <p class="continue-button">						
                        <a  href="javascript:void(0)" onclick="booking_on_hold('CONFIRMED');" class="button1" id="pay_button"><span><?=t($t_base.'fields/pay');?></span></a>
                        <span class="pay-later-button">
                                <a href="javascript:void(0)" onclick="booking_on_hold('ON_HOLD');"><span><?=t($t_base.'fields/or_save_for_later');?></span></a>
                        </span>
                </p> 
        </div>	
         <br>
        -->
</div> 

<div id="insurance_terms_container" style="display:none;"> 
</div>
<?php
echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>		