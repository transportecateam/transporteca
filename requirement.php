<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle = __REQUIREMENT_PAGE_META_TITLE__;
$szMetaKeywords = __REQUIREMENT_PAGE_META_KEYWORDS__;
$szMetaDescription = __REQUIREMENT_PAGE_META_DESCRIPTION__ ;
require_once(__APP_PATH_LAYOUT__."/header.php");

$t_base = "home/homepage/";
$t_base_landing_page = "LandingPage/";
$kConfig=new cConfig();
$kWhsSearch = new cWHSSearch();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

unset($_SESSION['valid_confirmation']);
$_SESSION['__BOOKING_TIMESTAMP__'] = '';
unset($_SESSION['__BOOKING_TIMESTAMP__']);

$_SESSION['came_from_landing_page'.'_'.$szBookingRandomNum] = 1;

$iLanguage = getLanguageId();


$iPopupDisplayed = true;
if($_REQUEST['err']==1)
{
	$kBooking->addExpactedServiceData($idBooking);
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$idExpactedService = $kBooking->idExpactedService ;
	$iPopupDisplayed = false;
	?>
	<script type="text/javascript">
            addPopupScrollClass('all_available_service');
	</script>
	<div id="all_available_service" style="display:block;">	
	<?php
	echo display_no_service_available_popup_requirement($t_base_landing_page,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
	echo "</div>";
}

if($idBooking>0)
{
	$kBooking = new cBooking();
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	$cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
	
	if((int)$postSearchAry['idUser']<=0 && (int)$_SESSION['user_id']>0)
	{
		$kUser=new cUser();
		if($_SESSION['user_id']>0)
		{
			$kUser->getUserDetails($_SESSION['user_id']);
		}
			
		$res_ary=array();			
		$res_ary['idUser'] = $kUser->id ;				
		$res_ary['szFirstName'] = $kUser->szFirstName ;
		$res_ary['szLastName'] = $kUser->szLastName ;
		$res_ary['szEmail'] = $kUser->szEmail ;
		$res_ary['szCustomerAddress1'] = $kUser->szAddress1;
		$res_ary['szCustomerAddress2'] = $kUser->szAddress2;
		$res_ary['szCustomerAddress3'] = $kUser->szAddress3;
		$res_ary['szCustomerPostCode'] = $kUser->szPostcode;
		$res_ary['szCustomerCity'] = $kUser->szCity;
		$res_ary['szCustomerCountry'] = $kUser->szCountry;
		$res_ary['szCustomerState'] = $kUser->szState;
		$res_ary['szCustomerPhoneNumber'] = $kUser->szPhoneNumber;
		$res_ary['szCustomerCompanyName'] = $kUser->szCompanyName;	
		$res_ary['szCustomerCompanyRegNo'] = $kUser->szCompanyRegNo;
		
		if(!empty($res_ary))
		{
			$update_query = '';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}
		$update_query = rtrim($update_query,",");
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			//echo "SUCCESS";
		}
	}
	
	if(($postSearchAry['iPaymentProcess']==1) && ($booking_mode==2))
	{
		header("Location:".__LANDING_PAGE_URL__);
		exit;
	}
	else if(($_SESSION['user_id']>0) && (!$kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id'])))
	{
		$iPopupDisplayed = false;
		echo display_booking_invalid_user_popup($t_base_details);
		die;
	}
	else if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
	{
		$iPopupDisplayed = false;
		echo display_booking_already_paid($t_base,$idBooking);
		die;
	}
	else if($_REQUEST['err'] !=4) // That means user donot come from cargo changed price popup
	{
		/**
		* we are updating idForwarder,idWarehouseFrom and idWarehouseTo to prevent Multiple tab problem.
		**/
		
		$res_ary=array();
		$res_ary['idForwarder']= '';
		$res_ary['idWarehouseFrom']= '';
		$res_ary['idWarehouseTo']= '';
		
		if(!empty($res_ary))
		{
			$update_query = '';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}
		$update_query = rtrim($update_query,",");
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			//echo "SUCCESS";
		}
	}	
}
else
{
	$_SESSION['booking_register_shipper_id']='';
	$_SESSION['booking_register_shipper_id'] = '';
	unset($_SESSION['booking_register_shipper_id']);
	unset($_SESSION['booking_register_shipper_id']);
	
	//header("Location:".__LANDING_PAGE_URL__);
	//exit;
}

if(((int)$_SESSION['user_id']>0) && ($_REQUEST['err']<=0) && $iPopupDisplayed)
{
	checkUserPrefferedCurrencyActive();
}

if(!empty($_POST['landingPageAry']))
{
	$postSearchAry['idOriginCountry'] = $_POST['landingPageAry']['szOriginCountry'];
	$postSearchAry['idDestinationCountry'] = $_POST['landingPageAry']['szDestinationCountry'];
	
	$countryNameAry=array();
	$countryNameAry[0] = $postSearchAry['idOriginCountry'] ;
	$countryNameAry[1] = $postSearchAry['idDestinationCountry'];
	$iLanguage = getLanguageId();
	$orignCountrAry = $kConfig->getAllCountryInKeyValuePair(false,false,$countryNameAry,$iLanguage);
	
	$postSearchAry['szOriginCountry'] = $orignCountrAry[$postSearchAry['idOriginCountry']]['szCountryName'];
	$postSearchAry['szDestinationCountry'] = $orignCountrAry[$postSearchAry['idDestinationCountry']]['szCountryName'];
}

$countryAry=$kConfig->getAllCountries(false,$iLanguage);
$kWHSSearch = new cWHSSearch();
$imgName = 

// geting all service type 
$serviceTypeAry=$kConfig->getConfigurationData('__TABLE_SERVICE_TYPE__');
if(!empty($serviceTypeAry))
	{
		$preload_image_str = '';
		foreach($serviceTypeAry as $serviceTypeArys)
		{
			if(!empty($preload_image_str))
			{
				$preload_image_str .=", '".display_service_type_canvas($postSearchAry,$serviceTypeArys['id'],true)."',";
				$preload_image_str .= "'".display_service_type_image_search_service($postSearchAry,$serviceTypeArys['id'],$serviceTypeArys['szDescription'],'service-type',true)."'";
			}
			else
			{
				$preload_image_str .= "'".display_service_type_canvas($postSearchAry,$serviceTypeArys['id'],true)."',";
				$preload_image_str .= "'".display_service_type_image_search_service($postSearchAry,$serviceTypeArys['id'],$serviceTypeArys['szDescription'],'service-type',true)."'";
				
			}
		}
	}
?>
<script type="text/javascript">
<?
	if(!empty($preload_image_str))
	{
		?>
		preload([
		    <?=$preload_image_str?>
		]);
		<?
	}
?>
</script>
<div id="additional_cc_popup" style="display:none;">
</div>
<div id="all_available_service" style="display:none;">	
	<?
		echo display_no_service_available_popup($t_base_landing_page,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
	?>
</div>

	<div id="hsbody">
				<?
					if((int)$postSearchAry['idServiceType']>0 && (int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
					{
						$kBooking = new cBooking();
						if(!empty($_REQUEST['booking_random_num']))
						{
							$szBookingRandomNum = $_REQUEST['booking_random_num'] ;
						}
						$iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
						
						if($iAvailableServiceCount==0)
						{
							$landing_page_url = __HOME_PAGE_URL__."/home/service_not_found/".$szBookingRandomNum.'/';
							header("Location:".$landing_page_url);
							exit;							
						}
						else if($iAvailableServiceCount)
						{
							
						}
						$landing_page_url = __LANDING_PAGE_URL__."/".$szBookingRandomNum.'/';
						?>
						<script type="text/javascript">
							$().ready(function(){
								$("#shipping-box").attr("style","cursor:pointer");
								$("#shipping-box").unbind("click");
								$("#shipping-box").click(function(){ display_requirement_edit_box_popup('COUNTRY_BOX_EDIT_POPUP','<?=$szBookingRandomNum?>') });
							});
						</script>					
						<div id="shipping-box" class="transporteca-box">
						<div class="clearfix nor-form">
							<p class="text"><?=t($t_base.'fields/shipping_from');?></p>
							<p><input type="text" class="disabled_text_field" name="" id="" value="<?=$postSearchAry['szOriginCountry']?>" disabled /></p>
							<p class="text" align="right"><?=t($t_base.'fields/shipping_to');?></p>
							<p align="right"><input class="disabled_text_field" type="text"  value="<?=$postSearchAry['szDestinationCountry']?>" disabled/></p>
						</div>
						</div>
						<div id="transportation-box" class="transporteca-box">
						<?
							$idServiceType = $postSearchAry['idServiceType'] ;
							echo display_service_image($postSearchAry,$idServiceType,$szBookingRandomNum);
						?>
						</div>						
						<?
					}
					else if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
					{
						$kBooking = new cBooking();
						if(!empty($_REQUEST['booking_random_num']))
						{
							$szBookingRandomNum = $_REQUEST['booking_random_num'] ;
						}
						$iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
						
						if($iAvailableServiceCount==0)
						{
							$landing_page_url = __HOME_PAGE_URL__."/home/service_not_found/".$szBookingRandomNum.'/';
							header("Location:".$landing_page_url);
							exit;							
						}
						else if($iAvailableServiceCount)
						{
							$res_ary=array();
							if((int)$postSearchAry['iBookingStep']<2)
							{
								$res_ary['iBookingStep'] = 2 ;
							}
							
							if(!empty($res_ary))
							{
								$update_query = '';
								foreach($res_ary as $key=>$value)
								{
									$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
								}
							}
							$update_query = rtrim($update_query,",");
							if($kBooking->updateDraftBooking($update_query,$idBooking))
							{
								//booking moves on to second step means Trade Available.
							}
						}
						$landing_page_url = __LANDING_PAGE_URL__."/".$szBookingRandomNum.'/';
						
				?>
					<script type="text/javascript">
							$().ready(function(){
								$("#shipping-box").attr("style","cursor:pointer");
								$("#shipping-box").unbind("click");
								$("#shipping-box").click(function(){ display_requirement_edit_box_popup('COUNTRY_BOX_EDIT_POPUP','<?=$szBookingRandomNum?>') });
							});
						</script>		
					<div id="shipping-box" class="transporteca-box">
						<div class="clearfix nor-form">
							<p class="text"><?=t($t_base.'fields/shipping_from');?></p>
							<p><input type="text" name="" class="disabled_text_field" id="" value="<?=$postSearchAry['szOriginCountry']?>" disabled /></p>
							<p class="text" align="right"><?=t($t_base.'fields/shipping_to');?></p>
							<p align="right"><input type="text" class="disabled_text_field"  value="<?=$postSearchAry['szDestinationCountry']?>" disabled/></p>
						</div>
					</div>
					<div id="transportation-box" class="active transporteca-box">
					<div class="requirement-content">
						<a href="<?=$landing_page_url?>" class="act-question"><?=t($t_base_landing_page.'title/dont_want_to_answer_these_question');?></a>
						<?
							echo Y1_popup_top($iAvailableServiceCount,$postSearchAry);
							echo display_service_type_box_active($postSearchAry);
						?>
					</div>
				<? }else{ 
				?>
					<div id="shipping-box" class=" active transporteca-box">
					<?=display_country_drop_down_active($countryAry,$postSearchAry)?>
					</div>
				<div id="transportation-box" class="transporteca-box">
					<p><?=t($t_base_landing_page.'title/select_transportation_default_text')?></p>	
				</div>
			<? } 
			   if($postSearchAry['idOriginCountry']>0 && $postSearchAry['idDestinationCountry']>0 && $postSearchAry['idServiceType']>0 && (!empty($postSearchAry['szOriginPostCode']) || !empty($postSearchAry['szOriginCity'])) && (!empty($postSearchAry['szDestinationPostCode']) || !empty($postSearchAry['szDestinationCity'])))
			   {
					?>
					<div id="shipper-consignee-box" class="transporteca-box">
					<?
					echo city_postcode_active_box($postSearchAry,true);
					$step_count = 4;
			   }
			   elseif($postSearchAry['idOriginCountry']>0 && $postSearchAry['idDestinationCountry']>0 && $postSearchAry['idServiceType']>0)
			   {
					?>
					<div id="shipper-consignee-box" class="active transporteca-box">
					<?
					echo city_postcode_active_box($postSearchAry);
				}
				else
				{
					?>
					<div id="shipper-consignee-box" class="transporteca-box">					
					<p><?=t($t_base_landing_page.'title/specify_location')?></p>
					<?
				}
					?>
			</div>
			<?
				if($step_count==4 && ($postSearchAry['idTimingType']>0) && (!empty($postSearchAry['dtTimingDate']) && $postSearchAry['dtTimingDate']!='0000-00-00 00:00:00'))
				{
					$step_count = 5;
					?>
					<div id="cargo-box" class="transporteca-box clearfix">
					<?
					echo display_timing_box($postSearchAry);
				}
				else if($step_count==4)
				{
					?>
					<div id="cargo-box" class="active transporteca-box">
					<?
					echo display_timing_box_active($postSearchAry);
				}
				else
				{
			?>
				<div id="cargo-box" class="transporteca-box clearfix">
				<p><?=t($t_base_landing_page.'title/select_timing')?> </p>
			<?  
				}		
			?>
			</div>
			<?
				$kWhsSearch = new cWHSSearch();
				$kWhsSearch->isCustomClearanceExist($postSearchAry);
				if($step_count==5 && ($postSearchAry['iOriginCC']==1 || $postSearchAry['iDestinationCC']==2 || $postSearchAry['iCustomClearance']>0 ))
				{
					$step_count = 6;
					?>
					<div id="import-box" class="transporteca-box">
					<?
					echo display_custom_clearance_box($postSearchAry);
					echo "</div>";
				}
				else if($step_count==5 && ($kWhsSearch->iOriginCC==1 || $kWhsSearch->iDestinationCC==1))
				{
					?>
					<div id="import-box" class="active transporteca-box">
					<?
					echo display_custom_clearance_box_active($postSearchAry);
					echo "</div>";
				}
				else if($kWhsSearch->iOriginCC==1 || $kWhsSearch->iDestinationCC==1)
				{
			?>
				<div id="import-box" class="transporteca-box">
					<p><?=t($t_base_landing_page.'title/cc_default_text')?> </p>
				</div>
			 <? }
			 else if($step_count==5)
			 {
			 	?>
			 	<div id="import-box" style="display:none;" class="transporteca-box">
			 	</div>
			 	<?
			 	$step_count = 6;
			  } 
				if($step_count==6)
				{
					?>
					<div id="complete-box" class="active transporteca-box">
					<?php
					echo cargo_details_box_active($postSearchAry,false,false,true);
				}
				else
				{
			?>
				<div id="complete-box" class="transporteca-box">
					<p><?=t($t_base_landing_page.'title/complete_cargo')?> </p>
			 <? } ?>
			</div>			
		</div>	
<div id="all_available_service" style="display:none;"></div>
<div id="error_popup" style="display:none;"></div>
<div id="Transportation_pop" style="display:none;"></div>
<input type="hidden" name="service_type_hidden" id="service_type_hidden" value="<?=$postSearchAry['idServiceType']?>">
<?
	echo html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);
	require_once(__APP_PATH_LAYOUT__."/footer.php");
?>		