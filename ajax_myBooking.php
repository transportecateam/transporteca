<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$t_base = "Booking/MyBooking/";
$t_base_error = "Error";
$archiveFlag=false;
$activeFlag=false;
$deleteFlag=false;
$format = 'UTF-8';
$userSessionData = array();
$userSessionData = get_user_session_data(true);
$idUserSession = $userSessionData['idUser'];

if((int)$_SESSION['user_id']<=0)
{
?>
<script type="text/javascript">
ajaxLogin();
</script>
<?php	
}
$kBooking = new cBooking();
$kForwarder = new cForwarder();
$mode = trim($_POST['mode']);
if((int)$_REQUEST['idBooking']>0 && $_REQUEST['flag']=='archive')
{
    if($kBooking->updateBookingStatusArchiveActive($_REQUEST['idBooking'],$_REQUEST['flag'],$kUser->id,$kUser->idGroup))
    {
        $archiveFlag=true;
    }
}
else if((int)$_REQUEST['idBooking']>0 && $_REQUEST['flag']=='active')
{
    if($kBooking->updateBookingStatusArchiveActive($_REQUEST['idBooking'],$_REQUEST['flag'],$kUser->id,$kUser->idGroup))
    {
        $activeFlag=true;
    }
}
else if((int)$_REQUEST['idBooking']>0 && $_REQUEST['flag']=='delete')
{
    if($kBooking->deletingCustomerBooking($_REQUEST['idBooking'],$kUser->id,$kUser->idGroup))
    {
        $deleteFlag=true;
    }
}
if(!empty($_POST['myBookingSearchArr']))
{
  $searachArray=$_POST['myBookingSearchArr'];
}
else
{
	 $searachArray=array();
}
$showBookingFlag=$_REQUEST['showBookingFlag'];
if($showBookingFlag=='')
{
    $showBookingFlag='active';
}

$idUserArr=array();
if((int)$kUser->id>0){
$userMutliArr='';
if((int)$kUser->iInvited=='1')
{
    $userMutliArr=$kUser->getMultiUserData($kUser->idGroup,true);
}
if(!empty($userMutliArr))
{
    foreach($userMutliArr as $userMutliArrs)
    {
        $idUserArr[]=$userMutliArrs['id'];		
    }
    $idUserStr=implode(",",$idUserArr);
}
else
{
	$idUserStr=$kUser->id;
}
$myBookingArr=$kBooking->getAllMyBooking($idUserStr,$showBookingFlag,$searachArray);
}

if($mode == "__UPDATE_EMAIL_NOTIFICATION_STATUS__")
{
    $idBooking = (int)$_POST['idBooking'];
    $iSendTrackingUpdates = (int)$_POST['iSendTrackingUpdates'];
    
    if((int)$iSendTrackingUpdates>0)
    {
        $newTrackingUpdate = 0;
    }
    else
    {
        $newTrackingUpdate = 1;
    } 
    if($kBooking->updateEmailNewNotificationStatus($idBooking,$newTrackingUpdate))
    {
        echo "SUCCESS||||";
    }
}
else if($mode == "__UPDATE_EMAIL_NOTIFICATION_POP_UP__")
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_POST['booking_key'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);  
    $bookingDetailsAry = array();
    $bookingDetailsAry = $kBooking->getBookingDetails($idBooking); 
    $t_base_details = "BookingDetails/"; 
    
    if(!$kBooking->isBookingBelongsToCustomer($idBooking,$idUserSession))
    { 
        echo "SUCCESS_POPUP||||";
        echo display_booking_invalid_user_popup($t_base_details);
        die;
    }  
    else if($kBooking->updateEmailNewNotificationStatus($idBooking,$newTrackingUpdate,true))
    {
        echo "SUCCESS_POPUP||||";
        echo display_booking_notification_popup($bookingDetailsAry);
        die;
    }
    die;
}
$ctr=0;
//print_r($myBookingArr);
?>
		<div id="mybooking_list">
		
			<div class="hsbody-2-right mybookings">
				<?php
				if($activeFlag || $archiveFlag || $deleteFlag)
				{?>
				<script type="text/javascript">
					//my_booking_left_menu('<?=$showBookingFlag?>');
				</script>
				<?php	
				}
				/*
				if($activeFlag)
				{
					echo "<p>".t($t_base.'messages/booking_archive_to_active')."</p>";
				}
				if($archiveFlag)
				{
					echo "<p>".t($t_base.'messages/booking_active_to_archive')."</p>";
				}
				if($deleteFlag)
				{
					echo "<p>".t($t_base.'messages/booking_deleted')."</p>";
				}
				*/
				if(!empty($kBooking->arErrorMessages)){
				?>
				<div id="regError" class="errorBox ">
				<div class="header"><?=t($t_base_error.'/please_following');?></div>
				<div id="regErrorList">
				<ul>
				<?php
					foreach($kBooking->arErrorMessages as $key=>$values)
					{
					?><li><?=$values?></li>
					<?php	
					}
				?>
				</ul>
				</div>
				</div>
				<?php }?>
				<?php //require_once( __APP_PATH_LAYOUT__ ."/bookingRightNav.php" ); ?>
				<br />
				<!--  <p class="f-size-12" style="margin-bottom:5px;"><?=count($myBookingArr)?> <?=t($t_base.'messages/booking_meet_filiter');?></p>-->			
				<?php 
                                $t_base_booking_conf = "BookingConfirmation/";
					if(!empty($myBookingArr))
					{
                                            
                                                $iLanguage = getLanguageId(); 
						foreach($myBookingArr as $myBookingArrs)
						{	
							$ccmsg="";
							if($myBookingArrs['iOriginCC']==1 && $myBookingArrs['iDestinationCC']==2)
							{
								$ccmsg="customs clearance at origin and destination";
							}
							else if($myBookingArrs['iOriginCC']==1)
							{
								$ccmsg="customs clearance at origin";
							}
							else if($myBookingArrs['iDestinationCC']==2)
							{
								$ccmsg="customs clearance at destination";
							}
							else
							{
								$ccmsg="no customs clearance";
							}
						
							if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTD__)
							{
								$servicename="DTD,";
								if(strlen($myBookingArrs['szShipperCity_pickup'])>12)
								{
									$originCity=mb_substr_replace($myBookingArrs['szShipperCity_pickup'],'...',12,strlen($myBookingArrs['szShipperCity_pickup']),$format);
								}
								else
								{
									$originCity=$myBookingArrs['szShipperCity_pickup'];
								}
								if(strlen($myBookingArrs['szConsigneeCity_pickup'])>12)
								{
									$desCity=mb_substr_replace($myBookingArrs['szConsigneeCity_pickup'],'...',12,strlen($myBookingArrs['szConsigneeCity_pickup']),$format);
								}
								else
								{
									$desCity=$myBookingArrs['szConsigneeCity_pickup'];
								}
								$szCutOffStr = t($t_base.'fields/pickup_after').":";
								$szAvailableStr = t($t_base.'fields/delivery_before').":"; 
							}
							else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTW__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_DTP__)
							{
								if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTW__)
								{
									$servicename="DTP,";
								}
								else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTP__)
								{
									$servicename="DTP,";
								}
								
								if(strlen($myBookingArrs['szShipperCity_pickup'])>12)
								{
									$originCity=mb_substr_replace($myBookingArrs['szShipperCity_pickup'],'...',12,strlen($myBookingArrs['szShipperCity_pickup']),$format);
								}
								else
								{
									$originCity=$myBookingArrs['szShipperCity_pickup'];
								}
								if(strlen($myBookingArrs['szWarehouseToCity'])>12)
								{
									$desCity=mb_substr_replace($myBookingArrs['szWarehouseToCity'],'...',12,strlen($myBookingArrs['szWarehouseToCity']),$format);
								}
								else
								{
									$desCity=$myBookingArrs['szWarehouseToCity'];
								}
								
								$szCutOffStr =  t($t_base.'fields/pickup_after').":";
								$szAvailableStr =  t($t_base.'fields/available').":";
							}
							else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTD__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_PTD__)
							{
								if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTD__)
								{
                                                                    $servicename="WTD,";
								}
								else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_PTD__)
								{
                                                                    $servicename="PTD,";
								}
								
								if(strlen($myBookingArrs['szWarehouseFromCity'])>12)
								{
                                                                    $originCity=mb_substr_replace($myBookingArrs['szWarehouseFromCity'],'...',12,strlen($myBookingArrs['szWarehouseFromCity']),$format);
								}
								else
								{
                                                                    $originCity=$myBookingArrs['szWarehouseFromCity'];
								}
								
								if(strlen($myBookingArrs['szConsigneeCity_pickup'])>12)
								{
                                                                    $desCity=mb_substr_replace($myBookingArrs['szConsigneeCity_pickup'],'...',12,strlen($myBookingArrs['szConsigneeCity_pickup']),$format);
								}
								else
								{
                                                                    $desCity=$myBookingArrs['szConsigneeCity_pickup'];
								}
								
								$szCutOffStr =  t($t_base.'fields/cut_off').":";
								$szAvailableStr =  t($t_base.'fields/delivery_before').":";
							}
							else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTW__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_WTP__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_PTW__ || $myBookingArrs['idServiceType']==__SERVICE_TYPE_PTP__)
							{
								if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTW__)
								{
									$servicename="WTW,";
								}
								else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_PTW__)
								{
									$servicename="PTW,";
								}
								else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_WTP__)
								{
									$servicename="WTP,";
								}
								else if($myBookingArrs['idServiceType']==__SERVICE_TYPE_PTP__)
								{
									$servicename="PTP,";
								}
								
								if(strlen($myBookingArrs['szWarehouseFromCity'])>12)
								{
                                                                    $originCity=mb_substr_replace($myBookingArrs['szWarehouseFromCity'],'...',12,strlen($myBookingArrs['szWarehouseFromCity']),$format);
								}
								else
								{
                                                                    $originCity=$myBookingArrs['szWarehouseFromCity'];
								}
								if(strlen($myBookingArrs['szWarehouseToCity'])>12)
								{
									$desCity=mb_substr_replace($myBookingArrs['szWarehouseToCity'],'...',12,strlen($myBookingArrs['szWarehouseToCity']),$format);
								}
								else
								{
									$desCity=$myBookingArrs['szWarehouseToCity'];
								}
								
								$szCutOffStr =  t($t_base.'fields/cut_off').":";
								$szAvailableStr =  t($t_base.'fields/available').": ";		
							}
							
							if($originCity=="" || empty($originCity))
							{
                                                            if(strlen($myBookingArrs['szShipperCity'])>12)
                                                            {
                                                                $originCity=mb_substr_replace($myBookingArrs['szShipperCity'],'...',12,strlen($myBookingArrs['szOriginCity']),$format);
                                                            }
                                                            else
                                                            {
                                                                $originCity=$myBookingArrs['szShipperCity'];
                                                            } 
							}
							
							if($desCity=="" || empty($desCity))
							{
								if(strlen($myBookingArrs['szDestinationCity'])>12)
								{
									$desCity= mb_substr_replace($myBookingArrs['szDestinationCity'],'...',12,strlen($myBookingArrs['szDestinationCity']),$format);
								}
								else
								{
									$desCity=$myBookingArrs['szDestinationCity'];
								}
								//$desCity=$myBookingArrs['szDestinationCity'];
							}
                                                        
                                                        if($myBookingArrs['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
                                                        {
                                                            $szServiceName = $myBookingArrs['szServiceDescription'];
                                                        }
                                                        else
                                                        {
                                                            if($myBookingArrs['idServiceType']==__SERVICE_TYPE_DTD__)
                                                            {					
                                                                $szServiceName =  t($t_base_booking_conf.'fields/transportation_from_the')." ".t($t_base_booking_conf.'fields/supplier_door')." ".$myBookingArrs['szShipperCity']." ".t($t_base_booking_conf.'fields/to_your_door')." ".$myBookingArrs['szConsigneeCity'].", ".t($t_base_booking_conf.'fields/all_inclusive').". ".t($t_base_booking_conf.'fields/this_is_exw');
                                                            }
                                                            else
                                                            {
                                                                $szServiceName =  t($t_base_booking_conf.'fields/transportation_from_the')." ".t($t_base_booking_conf.'fields/port')." ".$myBookingArrs['szWarehouseFromCity']." ".t($t_base_booking_conf.'fields/to_your_door')." ".$myBookingArrs['szConsigneeCity'].", ".t($t_base_booking_conf.'fields/all_inclusive').". ".t($t_base_booking_conf.'fields/this_is_fob');
                                                            }  
                                                        }
                                                        $courerBookingArr=array();
                                                        
						?>
				<div id="my_booking_<?=$myBookingArrs['id']?>">	
				<div class="booking-box" style="margin-bottom:0px;">
					<div class="logo-container">
						<p class="forwarder_logo" align="left">						
						<?php 
                                                    if((int)$myBookingArrs['idForwarder']>0)
                                                    {
                                                        $kForwarder->load($myBookingArrs['idForwarder']);
                                                        $szLogoFileName = $kForwarder->szLogo ;
                                                        $szDisplayName = $kForwarder->szDisplayName;
                                                    }
							
						if(!empty($szLogoFileName)) {?>
								<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$szLogoFileName."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$myBookingArrs['szDisplayName']?>" border="0" />
								<? }else if(!empty($szDisplayName)) {?>
								<?=$szDisplayName?>
								<? }else {?>
								<?=t($t_base.'fields/to_be_decided');?>
								<? }?>
						</p>
						<?php if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){?>
						<p><strong><?=t($t_base.'fields/reference');?></strong>: <?=$myBookingArrs['szBookingRef']?></p>
						<? }?>
						<?php if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){?>
						<p>
							<a href="<?=__BASE_URL__?>/viewInvoice/<?=$myBookingArrs['id']?>/" target="_blank" class="f-size-12"><?=t($t_base.'links/view_invice');?></a><br />
							<a href="<?=__BASE_URL__?>/viewBookingConfirmation/<?=$myBookingArrs['id']?>/" target="_blank" class="f-size-12"><?=t($t_base.'links/view_confirmation');?></a>
						</p>
						<?php }?>
						<p class="paid_amount">
						<?php if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){
                                                    $totalAmountPaid=0;   
                                                   
                                                    $totalAmountPaid= $myBookingArrs['fTotalPriceCustomerCurrency']+$myBookingArrs['fTotalInsuranceCostForBookingCustomerCurrency'];
                                                    ?>
						<strong><?=t($t_base.'fields/paid');?></strong>:<?php }else {?><strong><?=t($t_base.'fields/current_price');?></strong>:<br> <?}?> <?=strlen($myBookingArrs['fTotalPriceCustomerCurrency'])>7?"<br>":''?><?=($totalAmountPaid>0)?$myBookingArrs['szCurrency']." ".number_format_custom((float)$totalAmountPaid,$iLanguage):'N/A'?>
						</p>
					</div>
					<div class="details">
						<?php if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){?>
						<div class="oh">
							<p class="fl-15"><strong><?=t($t_base.'fields/date');?></strong>:</p>
							<p class="fl-30"><?php
							if(!empty($myBookingArrs['dtBookingConfirmed']) && $myBookingArrs['dtBookingConfirmed']!='0000-00-00 00:00:00')
								echo date('d/m/Y',strtotime($myBookingArrs['dtBookingConfirmed']));
							?></p>
                                                        <?php if($myBookingArrs['idTransportMode']==4){?>
                                                            <p class="fl-40"><strong><?=t($t_base.'fields/email_notofication');?></strong>:</p>
                                                            <p class="fl-10">
                                                            <?php
                                                                if($myBookingArrs['iSendTrackingUpdates'] == 1)
                                                                {
                                                                ?>
                                                                    <input type="checkbox" class="cmn-toggle cmn-toggle-round" checked="" value="1" id="cmn-toggle-<?php echo $myBookingArrs['id'];?>" onclick="updateEmailNotificationStatus('<?php echo $myBookingArrs['id'];?>',this.value);">
                                                                    <label for="cmn-toggle-<?php echo $myBookingArrs['id'];?>"></label>
                                                                <?php
                                                                }
                                                                else
                                                                {
                                                                ?>
                                                                   <input type="checkbox" class="cmn-toggle cmn-toggle-round" value="0" id="cmn-toggle-<?php echo $myBookingArrs['id'];?>" onclick="updateEmailNotificationStatus('<?php echo $myBookingArrs['id'];?>',this.value);">
                                                                    <label for="cmn-toggle-<?php echo $myBookingArrs['id'];?>"></label>
                                                                <?php
                                                                }
                                                            ?></p>
                                                        <?php } ?>
						</div>
						<?php }else if($myBookingArrs['idBookingStatus']=='1' || $myBookingArrs['idBookingStatus']=='2'){?>
							<div class="oh">
							<p class="fl-65"><strong><?=t($t_base.'fields/modifed_date');?></strong>:</p>
							<p class="fl-25"><?php
							if(!empty($myBookingArrs['dtUpdatedOn']) && $myBookingArrs['dtUpdatedOn']!='0000-00-00 00:00:00')
								echo date('d/m/Y',strtotime($myBookingArrs['dtUpdatedOn']));
							?></p>
						</div>
						<?php }?>
						<div class="oh">
							<p class="fl-15"><strong><?=t($t_base.'fields/service');?></strong>:</p>
							<p class="fl-85"><?php echo $szServiceName; ?></p>
						</div>					
                                                <?php 
                                                if($myBookingArrs['iCourierBooking']==1)
                                                {?>
                                                 <div class="oh" style="padding-bottom:5px;">
							<p class="fl-15"><strong><?=t($t_base.'fields/from');?></strong>:</p>
							<p class="fl-30"><?=$myBookingArrs['szOriginCity']?></p>
							<p class="fl-20"><strong><?=t($t_base.'fields/pickup_after')?></strong>:</p>
							<p class="fl-33"><?php
                                                            if(!empty($myBookingArrs['dtCutOff']) && $myBookingArrs['dtCutOff']!='0000-00-00 00:00:00')
                                                                    echo date('d/m/Y',strtotime($myBookingArrs['dtCutOff']));
							?></p>
						</div>						
						<div class="oh">
							<p class="fl-15"><strong><?=t($t_base.'fields/to');?></strong>:</p>
							<p class="fl-30"><?=$myBookingArrs['szDestinationCity']?></p>
							<p class="fl-20"><strong><?=t($t_base.'fields/delivery_before')?></strong>:</p>
							<p class="fl-33"><?php
                                                            if(!empty($myBookingArrs['dtAvailable']) && $myBookingArrs['dtAvailable']!='0000-00-00 00:00:00')
                                                            {
                                                                echo date('d/m/Y',strtotime($myBookingArrs['dtAvailable']));
                                                            }
							?></p>
						</div>
                                                   <?php 
                                                }
                                                else
                                                {    
                                                if($myBookingArrs['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__){ ?> 
                                                    <div class="oh" style="padding-bottom:5px;">
                                                        <p class="fl-15"><strong><?=t($t_base.'fields/from');?></strong>:</p>
                                                        <p class="fl-30"><?php echo $myBookingArrs['szShipperCity']; ?></p> 
                                                    </div>
                                                    <div class="oh" style="padding-bottom:5px;">
                                                        <p class="fl-15"><strong><?=t($t_base.'fields/to');?></strong>:</p>
                                                        <p class="fl-30"><?php echo $myBookingArrs['szConsigneeCity']?></p> 
                                                    </div>
                                                <?php } else {?>
                                                    
						<div class="oh" style="padding-bottom:5px;">
							<p class="fl-15"><strong><?=t($t_base.'fields/from');?></strong>:</p>
							<p class="fl-30"><?=$originCity?></p>
							<p class="fl-20"><strong><?=$szCutOffStr?></strong></p>
							<p class="fl-33"><?php
                                                            if(!empty($myBookingArrs['dtCutOff']) && $myBookingArrs['dtCutOff']!='0000-00-00 00:00:00')
                                                                    echo date('d/m/Y',strtotime($myBookingArrs['dtCutOff']));
							?></p>
						</div>						
						<div class="oh">
							<p class="fl-15"><strong><?=t($t_base.'fields/to');?></strong>:</p>
							<p class="fl-30"><?=$desCity?></p>
							<p class="fl-20"><strong><?=$szAvailableStr?></strong></p>
							<p class="fl-33"><?php
                                                            if(!empty($myBookingArrs['dtAvailable']) && $myBookingArrs['dtAvailable']!='0000-00-00 00:00:00')
                                                            {
                                                                echo date('d/m/Y',strtotime($myBookingArrs['dtAvailable']));
                                                            }
							?></p>
						</div>
						
                                                <?php }}?> 
						<div class="oh">
							<p class="fl-15"><strong><?=t($t_base.'fields/cargo');?></strong>:</p>
							<p class="fl-85">
							<?php 	
							
								
                                                                $szCargoCommodity = $myBookingArrs['szCargoDescription'];
								
								$cargo_volume = format_volume($myBookingArrs['fCargoVolume'],$iLanguage);
								//echo $iLanguage."iLanguage";
                                                                //$cargo_weight = get_formated_cargo_measure((float)($myBookingArrs['fCargoWeight'])) ;
								$cargo_weight=number_format_custom($myBookingArrs['fCargoWeight'],$iLanguage);
                                                               // die();
//								if($iLanguage==__LANGUAGE_ID_DANISH__)
//								{ 
//									$cargo_weight = number_format((float)$myBookingArrs['fCargoWeight'],0,'.','.');
//								}
//								else
//								{ 
//									$cargo_weight = number_format((float)$myBookingArrs['fCargoWeight'],0,'.',',');
//								}
                                                                
                                                                if($myBookingArrs['iCourierBooking']==1)
                                                                {    
                                                                    $textPackage=t($t_base.'fields/package');
                                                                    if((int)$myBookingArrs['iTotalQuantity']>1)
                                                                        $textPackage=t($t_base.'fields/packages');
                                                                    
                                                                    echo number_format((int)$myBookingArrs['iTotalQuantity'])." ".$textPackage.", ".$cargo_volume." ".t($t_base.'fields/cbm').", ".$cargo_weight." ".t($t_base.'fields/kg').", ".utf8_decode($szCargoCommodity);
                                                                }
                                                                else
                                                                {
                                                                    echo $cargo_volume." ".t($t_base.'fields/cbm').", ".$cargo_weight." ".t($t_base.'fields/kg').", ".utf8_decode($szCargoCommodity);
                                                                }    
							?>	
							</p>
						</div>
                                                <?php
                                                if($myBookingArrs['iCourierBooking']==1)
                                                {
                                                    $kCourierServices = new cCourierServices();
                                                    $courerBookingArr=$kCourierServices->getCourierBookingData($myBookingArrs['id']);
                                                    $newCourierBookingArr=$kCourierServices->getAllNewBookingWithCourier($myBookingArrs['id']); 
                                                    
                                                    if($courerBookingArr[0]['szCourierBookingStatusText']!='Delivered' && $myBookingArrs['szTrackingNumber']!='')
                                                    {
                                                        $idServiceProvider = $myBookingArrs['idServiceProvider'];
                                                        $courierAggrementAry = array();
                                                        $courierAggrementAry = $kCourierServices->loadCourierAgreementDetails($idServiceProvider);
                                                        
                                                        $notDeliveredBookingArr[$ctr]['idBooking']=$myBookingArrs['id'];
                                                        $notDeliveredBookingArr[$ctr]['szProviderName']=$newCourierBookingArr[0]['szProviderName'];
                                                        $notDeliveredBookingArr[$ctr]['idCourierServiceProvider'] = $courierAggrementAry['idCourierProvider']; 
                                                        ++$ctr;
                                                    }    
                                                }
                                                ?>
					</div>
					<div class="buttons">
						<?php if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){?>
							<?php if(!$kBooking->getBookingRatingDetails($myBookingArrs['id'])){ ?>
						<div id="rating_div_updated_<?=$myBookingArrs['id']?>">	
						<p><a href="javascript:void(0)" class="button1" onclick="booking_rating('<?=$myBookingArrs['id']?>')"><span><?=t($t_base.'links/rate_forwarders');?></span></a></p>
						</div>
						<?php } ?>
						
						<?php if($myBookingArrs['idBookingStatus']=='3') {?>
						<p><a href="javascript:void(0)" class="button1" onclick="change_booking_status('<?=$myBookingArrs['id']?>','archive','<?=$showBookingFlag?>');"><span><?=t($t_base.'fields/archive_booking');?></span></a></p>
						<?php }?>
						<?php if($myBookingArrs['idBookingStatus']=='4') {?>
						<p><a href="javascript:void(0)" class="button1" onclick="change_booking_status('<?=$myBookingArrs['id']?>','active','<?=$showBookingFlag?>');"><span><?=t($t_base.'links/move_to_actve');?></span></a></p>
						<?php }?>
						<p><a href="javascript:void(0);" onclick="repeat_active_booking('<?php echo $myBookingArrs['id']; ?>'); " class="button1"><span><?=t($t_base.'links/repeat_booking');?></span></a></p>
						<?php if($kBooking->getBookingRatingDetails($myBookingArrs['id'])){ 
							$width=$kBooking->iRating * __RATING_STAR_WIDTH__;
							?>
						<div class="oh booking_ratings">
						<p align="center" class="fl-49"><a href="javascript:void(0)" onclick="show_booking_rating_details('<?=$myBookingArrs['id']?>','<?=$kBooking->id?>')"><span><?=t($t_base.'links/your_rateing');?></span></a></p>
						
						<div id="ratings-star" class="fl-49">
                                                <span style="width:<?=$width?>px;">&nbsp;</span>
						</div>
						
						 </div>
						<? }?>
						<?php }?>
						<?php if($myBookingArrs['idBookingStatus']=='2'){?>
						<p><a href="javascript:void(0);" onclick="chage_old_bookings('<?=__BOOKING_CONFIRMATION_PAGE_URL__?>','<?=$myBookingArrs['id']?>');" class="button1"><span><?=t($t_base.'links/confirm_booking');?></span></a></p>
						<p><a href="javascript:void(0)" class="button2" onclick="change_booking_status('<?=$myBookingArrs['id']?>','delete','<?=$showBookingFlag?>');"><span><?=t($t_base.'links/delete_booking');?></span></a></p>
						<p><a href="javascript:void(0);" onclick="chage_old_bookings('<?=__BOOKING_DETAILS_PAGE_URL__?>','<?=$myBookingArrs['id']?>');" class="button2"><span><?=t($t_base.'links/booking_details');?></span></a></p>
						<?php }?>
						<?php if($myBookingArrs['idBookingStatus']=='1'){?>
						<p><a href="javascript:void(0)" onclick="resume_booking('<?=$myBookingArrs['id']?>');" class="button1"><span><?=t($t_base.'links/resume_booking');?></span></a></p>
						<p><a href="javascript:void(0)" class="button2" onclick="change_booking_status('<?=$myBookingArrs['id']?>','delete','<?=$showBookingFlag?>');"><span><?=t($t_base.'links/delete_booking');?></span></a></p>
						<?php }?>
						<?php if($myBookingArrs['idBookingStatus']=='3' || $myBookingArrs['idBookingStatus']=='4'){?>
						<div id="rating_div_link_<?=$myBookingArrs['id']?>"></div> 
						<?php }?>
                                                <?php if(!empty($myBookingArrs['szTrackingNumber']) && $myBookingArrs['idTransportMode'] == 4){?>
                                                <p><a href="<?php echo __AFTERSHIP_SHIPPMENT_TRACKING_LINKK__."/".$myBookingArrs['szTrackingNumber'];?>" target="_blank" class="button3"><span><?=t($t_base.'links/track_shippment');?></span></a></p>
						<?php }?>
					</div>
				</div>
				<div id="rating_<?=$myBookingArrs['id']?>" style="display:none;">
				</div>
				
			</div>	
			
				<br/>
				<?php
				}
			}
			else
			{
				echo display_no_booking_details($showBookingFlag);
			}
		?>	
			</div>
		</div>
		<?php
                if($email_notification_flag != 1)
                {
			if((int)$idBooking>0 && $showRatingFlag)
			{
                            $iLanguage = getLanguageId(); 
			?>
                            <script type="text/javascript">
				booking_rating('<?=$idBooking?>','<?php echo $iLanguage; ?>');
				//var divid="rating_<?=$idBooking?>";
				//$("#"+divid).focus();
                            </script>
			<?php
			}
			else if((int)$idBooking>0 && !$showRatingFlag)
			{?>
				<script type="text/javascript">
				show_booking_rating_details('<?=$idBooking?>','<?=$rate_id?>');
				</script>
			<?php	
			}
                }
                  
                        ?>
                        <script type="text/javascript">
                           setTimeout("callCourierApi()", 3000);
                           function callCourierApi()
                           {
                               <?php
                               if(!empty($notDeliveredBookingArr))
                               { 
                                   /* 
                                   $timeout_ctr = 0;
                                   foreach($notDeliveredBookingArr as $notDeliveredBookingArrs)
                                   {
                                       if($notDeliveredBookingArrs['idCourierServiceProvider']== __FEDEX_API__)
                                       {?>
                                            var ajax_counter = '<?php echo $timeout_ctr*3000 ;?>';
                                            ajax_counter = parseInt(ajax_counter);
                                            if(isNaN(ajax_counter))
                                            {
                                                ajax_counter = 0;
                                            }
                                            setTimeout(function(){
                                                updateCourierBookingStatus('<?=$notDeliveredBookingArrs['idBooking']?>','Fed-Ex');
                                            },ajax_counter); 
                                         <?php  
                                       }
                                       else 
                                       {
                                          ?>
                                            var ajax_counter = '<?php echo $timeout_ctr*30000 ;?>';
                                            ajax_counter = parseInt(ajax_counter);
                                            if(isNaN(ajax_counter))
                                            {
                                                ajax_counter = 0;
                                            }
                                            setTimeout(function(){
                                                updateCourierBookingStatus('<?=$notDeliveredBookingArrs['idBooking']?>','UPS');        
                                            },ajax_counter);  
                                         <?php  
                                       }
                                       $timeout_ctr++;
                                    } 
                                    * 
                                    */
                                } 
                              ?> 
                           }
                        </script>