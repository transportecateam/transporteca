<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code));
$kHaulagePricing = new cHaulagePricing();
$idHaulageModel = (int)$_REQUEST['idHaulagePricingModel'];
$verticesAry = array();
$verticesAry = $kHaulagePricing->getAllVerticesOfZone($idHaulageModel);
constantApiKey();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
        <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>		
<script src="http://maps.google.com/maps?file=api&amp;v=3.x&amp;key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true" type="text/javascript"></script>
<script type="text/javascript">
/* Developed by: Ajay Jha */ 
//Global variables
var global = this;
var map;
marker2 = new GMarker(new GLatLng('38.91013665250293','-94.69905853271484'), {draggable: true});

marker3 = new GMarker(new GLatLng('38.91013665250293','-94.69905853271484'), {draggable: true});

var PolygonMarkers = []; //Array for Map Markers
var PolygonPoints = [] //Array for Polygon Node Markers
var bounds = new GLatLngBounds; //Polygon Bounds
var Polygon; //Polygon overlay object
var polygon_resizing = false; //To track Polygon Resizing

//Polygon Marker/Node icons
var redpin = new GIcon(); //Red Pushpin Icon
redpin.image = "http://maps.google.com/mapfiles/ms/icons/red-pushpin.png";
redpin.iconSize = new GSize(32, 32);
redpin.iconAnchor = new GPoint(10, 32);
var bluepin = new GIcon(); //Blue Pushpin Icon
bluepin.image = "http://maps.google.com/mapfiles/ms/icons/blue-pushpin.png";
bluepin.iconSize = new GSize(32, 32);
bluepin.iconAnchor = new GPoint(10, 32);

function initialize() { //Initialize Google Map
    if (GBrowserIsCompatible()) {
    
        map = new GMap2(document.getElementById("map_canvas")); //New GMap object
        map.setCenter(new GLatLng(28.535516, 77.391026), 8);
		
		var ui = new GMapUIOptions(); //Map UI options
        ui.maptypes = { normal:true, satellite:true, hybrid:true, physical:false }
        ui.zoom = {scrollwheel:true, doubleclick:true};
        ui.controls = { largemapcontrol3d:true, maptypecontrol:true, scalecontrol:true };
        map.setUI(ui); //Set Map UI options
        
        <?
        	if(!empty($verticesAry))
        	{
        		foreach($verticesAry as $verticesArys)
        		{
        			?>
        			var old_points = new GLatLng('<?=$verticesArys['szLatitude']?>','<?=$verticesArys['szLongitude']?>');
					addMarker(old_points); 
        			<?
        		}
        	}
        ?>
		
        //Add Click event to add Polygon markers
        GEvent.addListener(map, "click", function(overlay, point, overlaypoint) {
            var p = (overlaypoint) ? overlaypoint : point;
            
            //Add polygon marker if overlay is not an existing marker and shift key is pressed
            if (!checkPolygonMarkers(overlay)) 
            {
            	 addMarker(p); 
            }
        });
    }
}

// Adds a new Polygon boundary marker
function addMarker(point) {
    var markerOptions = { icon: bluepin, draggable: true };
    var marker = new GMarker(point, markerOptions);
    PolygonMarkers.push(marker); //Add marker to PolygonMarkers array
    map.addOverlay(marker); //Add marker on the map
   
    GEvent.addListener(marker,'dragstart',function(){ //Add drag start event
    	remove_marker_position(marker);
        marker.setImage(redpin.image);
        polygon_resizing = true;
    });
    GEvent.addListener(marker,'drag',function()
    {
        drawPolygon(1); 
     
    }); //Add drag event
    GEvent.addListener(marker,'dragend',function(){   //Add drag end event
        marker.setImage(bluepin.image);
        polygon_resizing = false;
        add_marker_position(marker);
        drawPolygon(1);
        fitPolygon();
    });
    GEvent.addListener(marker,'click',function(point) { //Add Ctrl+Click event to remove marker
        if (global.ctrlKey) { removeMarker(point); }
    });
    drawPolygon();

    //If more then 2 nodes then automatically fit the polygon
    if(PolygonMarkers.length > 2) fitPolygon();
}

function remove_marker_position(marker)
{
	var poib = marker.getPoint();
	var hidden_value = document.getElementById("szLatLongPipeline").value;
	if(hidden_value!='')
	{
	 	var input_value_ary = hidden_value.split("||");
	 	var input_value='';
	 	for(var i=0;i<input_value_ary.length;i++)
	 	{
	 		if(jQuery.trim(poib) == jQuery.trim(input_value_ary[i]))
	 		{
	 			
	 		}
	 		else
	 		{
	 			if(input_value=='')
			    {
			    	input_value = input_value_ary[i];
			    }
			    else 
			    {
			    	input_value += "||"+input_value_ary[i];	
			    }    
	 		}
	 	}
	}   
    document.getElementById("szLatLongPipeline").value = input_value;
	
}
function add_marker_position(marker)
{
	var poib = marker.getPoint();
	var input_value = document.getElementById("szLatLongPipeline").value;
    if(input_value=='')
    {
    	input_value = poib;
    }
    else 
    {
    	input_value += "||"+poib;	
    }    
    document.getElementById("szLatLongPipeline").value = input_value;
}
// Removes a Polygon boundary marker
function removeMarker(point) {
    if(PolygonMarkers.length == 1){ //Only one marker in the array
        map.removeOverlay(PolygonMarkers[0]);
        map.removeOverlay(PolygonMarkers[0]);
        PolygonMarkers = [];
        if(Polygon){map.removeOverlay(Polygon)};
    }
      else //More then one marker
      {
        var RemoveIndex = -1;
        var Remove;
        //Search for clicked Marker in PolygonMarkers Array
        for(var m=0; m<PolygonMarkers.length; m++)
        {
            if(PolygonMarkers[m].getPoint().equals(point))
            {
                RemoveIndex = m; Remove = PolygonMarkers[m]
                break;
            }
        }
        //Shift Array elemeents to left
        for(var n=RemoveIndex; n<PolygonMarkers.length-1; n++)
        {
            PolygonMarkers[n] = PolygonMarkers[n+1];
        }
        PolygonMarkers.length = PolygonMarkers.length-1 //Decrease Array length by 1
        map.removeOverlay(Remove); //Remove Marker
        drawPolygon(); //Redraw Polygon
      }
}

//Draw Polygon from the PolygonMarkers Array
function drawPolygon(flag)
{
    PolygonPoints.length=0;
    for(var m=0; m<PolygonMarkers.length; m++)
    {
    	if(flag!=1)
		{
		    var input_value = document.getElementById("szLatLongPipeline").value;
		    if(input_value=='')
		    {
		    	input_value = PolygonMarkers[m].getPoint();
		    }
		    else 
		    {
		    	input_value += "||"+PolygonMarkers[m].getPoint();	
		    }    
	    }
        PolygonPoints.push(PolygonMarkers[m].getPoint()); //Add Markers to PolygonPoints node array
     }
     if(flag!=1)
	 {
    	document.getElementById("szLatLongPipeline").value = input_value;
     }
  
    //Add first marker in the end to close the Polygon
    PolygonPoints.push(PolygonMarkers[0].getPoint());
    if(Polygon){ map.removeOverlay(Polygon); } //Remove existing Polygon from Map
    var fillColor = (polygon_resizing) ? 'red' : 'blue'; //Set Polygon Fill Color
    Polygon = new GPolygon(PolygonPoints, '#FF0000', 2, 1, fillColor, 0.2); //New GPolygon object
    map.addOverlay(Polygon); //Add Polygon to the Map

    //TO DO: Function Call triggered after Polygon is drawn
}

//Fits the Map to Polygon bounds
function fitPolygon()
{
    bounds = Polygon.getBounds();
 
    map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
}
//check is the marker is a polygon boundary marker
function checkPolygonMarkers(marker) 
{
    var flag = false;
    for (var m = 0; m < PolygonMarkers.length; m++) {
        if (marker == PolygonMarkers[m])
        { flag = true; break; }
    }
    return flag;
}

//////////////////[ Key down event handler ]/////////////////////
//Event handler class to attach events
var EventUtil = {
      addHandler: function(element, type, handler){
            if (element.addEventListener){
                    element.addEventListener(type, handler, false);
            } else if (element.attachEvent){
                    element.attachEvent("on" + type, handler);
            } else {
                    element["on" + type] = handler;
            }
      }
};

// Attach Key down/up events to document
EventUtil.addHandler(document, "keydown", function(event){keyDownHandler(event)});
EventUtil.addHandler(document, "keyup", function(event){keyUpHandler(event)});

//Checks for shift and Ctrl key press
function keyDownHandler(e)
{
      if (!e) var e = window.event;
      var target = (!e.target) ? e.srcElement : e.target;
      if (e.keyCode == 16 && !global.shiftKey) { //Shift Key
            global.shiftKey = true;
      }
      if (e.keyCode == 17 && !global.ctrlKey) { //Ctrl Key
            global.ctrlKey = true;
      }
}
//Checks for shift and Ctrl key release
function keyUpHandler(e)
{
      if (!e) var e = window.event;
      if (e.keyCode == 16 && global.shiftKey) { //Shift Key
            global.shiftKey = false;
      }
      if (e.keyCode == 17 && global.ctrlKey) { //Ctrl Key
            global.ctrlKey = false;
      }
}
</script> 
</head>
<body onload="initialize()" onunload="GUnload()">
<div id="map_canvas" style="width:100%; height:450px"></div>
<br />
<p align="left">Note: Please use (ctrl + mouse click) to draw polygon </p>
<textarea id="szLatLongPipeline" name="szLatLongPipeline" rows="5" cols="70"></textarea>
</body> 
</html>