<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __BOOKING_GET_QUOTE_META_TITLE__;
$szMetaKeywords = __BOOKING_GET_QUOTE_META_KEYWORDS__;
$szMetaDescription = __BOOKING_GET_QUOTE_META_DESCRIPTION__;

$iDonotIncludeHiddenSearchHeader = 1;
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingDetails/";
$t_base_user = "Users/AccountPage/";
$t_base_error = "Error";
$kBooking = new cBooking();
$kConfig = new cConfig();
$kWHSSearch=new cWHSSearch();

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
  
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
$postSearchAry = $kBooking->getBookingDetails($idBooking);
$idLandingPage = $postSearchAry['idLandingPage'] ;
 
if($postSearchAry['iQuotesStatus']>0)
{ 
    //It means this RFQ has already been sent so no need to send it again, we simply redirected user to landing page.
    ob_end_clean();
    header("Location:".__BASE_URL__);
    die;
}
if($idLandingPage<=0)
{
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
        $idLandingPage = 2; 
    }
    else
    {
        $idLandingPage = 1; 
    }  
} 
$userSessionData = array();
$userSessionData = get_user_session_data();
$idUserSession = $userSessionData['idUser'];

$iLanguage = getLanguageId();
 
if((int)$_REQUEST['idBooking']>0)
{
    $idBooking = (int)$_REQUEST['idBooking'] ;
    $idBooking = $idBooking ;

    $kBooking = new cBooking();
    if($idUserSession>0)
    {
        if($kBooking->isBookingBelongsToCustomer($idBooking,$idUserSession))
        {
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
            $booking_mode = 2;
        }
        else
        {
            header("Location:".__NEW_SEARCH_PAGE_URL__);
            die;
        }
    }
}
else if((int)$idBooking>0)
{
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);	

    if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
        echo display_booking_already_paid($t_base,$idBooking);
        die;
    }
}
else
{
    $redirect_url = __NEW_SEARCH_PAGE_URL__ ;
    ?>
    <div id="change_price_div">
    <?php
    display_booking_notification($redirect_url);
    echo "</div>";
} 
if(((int)$idUserSession>0) && ($_REQUEST['err']<=0))
{
    checkUserPrefferedCurrencyActive();
}
 
$kUser = new cUser();  
$kRegisterShipCon = new cRegisterShipCon(); 

//if($iLanguage==__LANGUAGE_ID_DANISH__) 
//{ 
//    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__'); 
//} 
//else 
//{
    $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage); 
//} 

$customerTestimonialAry = array();
$customerTestimonialAry = $kExplain->getAllTestimonials($idLandingPage);

$partnerLogoAry = array();
$partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);

$kConfig = new cConfig();
$kConfig->loadCountry($postSearchAry['idOriginCountry']);
$szOriginCountryName = $kConfig->szCountryName ;

$kConfig->loadCountry($postSearchAry['idDestinationCountry']);
$szDestinationCountryName = $kConfig->szCountryName ;
 
$iWeekDay = date('w');
if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
{
    $szPageHeading = t($t_base.'fields/booking_quotes_heading_old')." ".t($t_base.'fields/latest_by_old') ;
    $szPageSubHeadingDetails = t($t_base.'fields/booking_quotes_sub_heading_2')." ".t($t_base.'fields/latest_by_details')." ".t($t_base.'fields/booking_quotes_sub_heading_3') ;
}
else
{
    $szPageHeading = t($t_base.'fields/booking_quotes_heading_old')." ".t($t_base.'fields/with_in_24_old') ; 
    $szPageSubHeadingDetails = t($t_base.'fields/booking_quotes_sub_heading_2')." ".t($t_base.'fields/with_in_24_details')." ".t($t_base.'fields/booking_quotes_sub_heading_3') ;
}
?>
<div id="hsbody-2" class="quote-page"> 
<div id="all_available_service" style="display:none;"></div>
	<div id="customs_clearance_pop_right" class="help-pop right"></div> 
	<div id="customs_clearance_pop" class="help-pop"></div>
	<div id="change_price_div" style="display:none"></div>  
	<div class="hsbody-2-right"> 
            <div class="booking-quote-heading-old">
                <h2><?php echo $szPageHeading; ?></h2>
                <p><?php echo t($t_base.'fields/booking_quotes_sub_heading_1')." ".$szOriginCountryName." ".t($t_base.'title/to')." ".$szDestinationCountryName; ?>.</p>
                <p><?php echo $szPageSubHeadingDetails; ?></p>
            </div>  
            <div id="booking_quotes_details_container" class="booking-quote-main-container clearfix">
            <?php
                echo display_booking_quotes_details_form_old($postSearchAry,$kRegisterShipCon,'GET_QUOTES');
            ?>
            </div>
            
            <div class="quote-price-guarantee-container" > 
                <div class="quote-price-guarantee-box"> 
                    <h2><?php echo t($t_base.'fields/price_guarantee'); ?></h2>
                    <ol>
                        <li><?php echo t($t_base.'fields/price_guarantee_details_text_1'); ?></li>
                        <li><?php echo t($t_base.'fields/price_guarantee_details_text_2'); ?></li>
                    </ol>   
                </div>
            </div>
            
            <div class="booking-quotes">  
                <ul class="clearfix">
                <?php
                    if(!empty($partnerLogoAry))
                    { 
                        $i=1;
                        foreach($partnerLogoAry as $partnerLogoArys)
                        {
                            $idImage="img_".$i;
                            ?>
                            <li id="<?=$idImage?>">
                                <img class="normal" src="<?php echo $partnerLogoArys['szLogoImageUrl']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
                                <img class="hover" src="<?php echo $partnerLogoArys['szWhiteLogoImageURL']; ?>" alt="<?php echo $partnerLogoArys['szImageDesc']; ?>">
                                <div><div><?php echo $partnerLogoArys['szToolTipText']; ?></div></div>
                            </li>
                            <?php
                            ++$i;
                        }
                    }
                ?>
                </ul> 
            </div>
	</div>
</div>
        
        <div class="contact-deails booking-feedback-container"><?php echo t($t_base.'fields/contact_details_text')." "; ?><span class="booking-quote-support-phone"><?php echo $szCustomerCareNumer; ?></span></div>

<section id="testimonials" class="pages" style="padding:30px 0; ">  
    <div class="testimonials-container" id="testimonials-container"> 
        <ul class="sl-slider">	
            <?php 
                if(!empty($customerTestimonialAry)) 
                {
                    foreach($customerTestimonialAry as $customerTestimonialArys)
                    {
                        ?>
                        <li>
                            <div class="sl-slider-div" style="visibility:hidden;">
                                <div class="deco"><img src="<?php echo $customerTestimonialArys['szImageURL']; ?>" alt="<?php echo $customerTestimonialArys['szImageTitle']?>" title="<?php echo $customerTestimonialArys['szImageDesc']; ?>"></div>
                                <h3><?php echo $customerTestimonialArys['szHeading']; ?></h3>
                                <blockquote><p><?php echo nl2br($customerTestimonialArys['szTextDesc']); ?></p><cite><?php echo $customerTestimonialArys['szTitleName']; ?></cite></blockquote>
                            </div>
                        </li>
                        <?php
                    }
                }
            ?>
        </ul>  
    </div> 
</section>
 <script type="text/javascript">
    $().ready(function(){
        if($('.sl-slider').length)
        {
            $('.sl-slider').bxSlider(); 
            console.log("called in if");
        }
        else
        {
            setTimeout(function(){
                $('.sl-slider').bxSlider(); 
                console.log("called after 100ms");
            },'100');
        }
         
    }); 
  </script>  
<?php
    echo html_form_random_booking_number(__BOOKING_CONFIRMATION_PAGE_URL__,$_REQUEST['booking_random_num']);
?> 
<?php
  require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>			
 <script type="text/javascript">
$().ready(function(){	  
   setTimeout(function(){
        $(".sl-slider-div").css('visibility','visible'); 
        console.log("me called");
    },'100');   
}); 
</script>   
  