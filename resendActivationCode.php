<?php
/**
 * Resending Activation Code
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/AccountPage/";
 
$kUser= new cUser();
if((int)$_SESSION['idUser']>0 ||  $_SESSION['user_id'])
{
	if($_SESSION['idUser']>0)
	{
		$idUser=$_SESSION['idUser'];
	}
	else if($_SESSION['user_id'])
	{
		$idUser=$_SESSION['user_id'];
	}
	
	$szBookingRandomNum = trim($_REQUEST['szBookingRandomNum']);
	if($kUser->resendActivationCode($idUser,$szBookingRandomNum))
	{
		$kUser->getUserDetails($_SESSION['idUser']);
		$szUserEmail = $kUser->szEmail ;
		$msg_success=t($t_base.'messages/resend_activation_key_success');
	}
	else
	{
	 	$msg_error=t($t_base.'messages/resend_activation_key_error');		
	}
}
else
{
	$msg_error=t($t_base.'messages/invalid_user');
}

if(!empty($msg_success)){
//ob_flush();
 //flush();	
$redirect_url=urlencode($_REQUEST['redirect_uri']);	
if(__ENVIRONMENT__ == "LIVE")
{
	setcookie(__COOKIE_NAME__ , $redirect_url,time() + __COOKIE_TIME__ , '/',$_SERVER['HTTP_HOST'],true);
}
else
{
	setcookie(__COOKIE_NAME__ , $redirect_url,time() + __COOKIE_TIME__ , '/');
}
echo "SUCCESS||||";
?>
<div id="popup-bg"></div>
<div id="popup-container">
	<div class="popup signin-popup signin-popup-verification">
		<p class="close-icon" align="right">
			<a onclick="redirect_url('<?=__HOME_PAGE_URL__?>')" href="javascript:void(0);">
				<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
		</p>
		<h5><?php echo t($t_base.'messages/verification_email_sent')?></h5>
		<p><?=t($t_base.'messages/verification_message_new_1')." ".$szUserEmail.". ".t($t_base.'messages/verification_message_new_2')?></p>
		<br />
		<p align="center">
			<a href="javascript:void(0)" class="button1" onclick="redirect_url('<?=__HOME_PAGE_URL__?>')"><span><?=t($t_base.'fields/close')?></span></a>
		</p>
	</div>
</div>

<?php }else if(!empty($msg_error)){
	echo "ERROR||||";
	?>
<p style="color:#000"><?=$msg_error?></p>
<?php }?>
			