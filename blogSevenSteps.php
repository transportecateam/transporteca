<?php
/**
 * Blog  
 */
 ob_start();
session_start();

$szMetaTitle = __BLOG_SEVEN_STEP_META_TITLE__;
$szMetaKeywords = __BLOG_SEVEN_STEP_META_KEYWORDS__ ;
$szMetaDescription = __BLOG_SEVEN_STEP_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/header.php" );
$t_base="blogContent/";
$kExplain = new cExplain();
$blogContent = $kExplain->serviceBlog(1);
?>

<div id="hsbody-2">
	<div class="hsbody-2-left account-links">
			<?php require_once( __APP_PATH__ ."/layout/blog_left_nav.php" ); ?>
	</div>	

	<div class="hsbody-2-right">	
		
			<?=$blogContent['szContent']?>
			<? /*<!-- 
			<h5><strong><?=t($t_base.'title/title1')?></strong></h5>
			<p >	
			<?=t($t_base.'message/message1')?>
			<br/><br/>
			<?=t($t_base.'message/message2_1')?> <a href="<?=__BLOG_LCL_SHIPPING_PAGE_URL__?>"><?=t($t_base.'message/message2_2_1')?></a><?=t($t_base.'message/message2_2')?>
			<br/><br/>
			<img src="<?=__BASE_STORE_IMAGE_URL__?>/seven_steps.png" title="Introducing four key players in LCL shipping: Shipper, Consignee, Freight Forwarder and Shipping Line" alt="Transporteca" />
			<br/><br/>
			<?=t($t_base.'message/message3')?>
			<br/><br/>
			<?=t($t_base.'message/message4')?>
			</p>
			<br />
			<br />
			
			<h4><strong><?=t($t_base.'title/title2')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message5')?>
			<br/><br/>
			<img src="<?=__BASE_STORE_IMAGE_URL__?>/physical_two_document_steps.png" title="The seven steps of international shipping: Export Haulage, Origin Handling, Export Customs Clearance, Ocean Freight, Import Customs Clearance, Destination Handling and Import Haulage" alt="Transporteca" />
			<br />
			<br />
			<?=t($t_base.'message/message6')?>
			<br />
			<br />
			</p>
			
			<h4><strong><?=t($t_base.'title/title3')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message7')?>
			<br />
			<br />
			<?=t($t_base.'message/message8')?>
			<br />
			<br />
			</p>
			
			<h4><strong><?=t($t_base.'title/title4')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message9')?>
			<br />
			<br />
			<?=t($t_base.'message/message10')?>
			<br />
			<br />
			</p>
			
			<h4><strong><?=t($t_base.'title/title5')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message11')?>
			<br />
			<br />
			<?=t($t_base.'message/message12')?>
			<br />
			<br />
			</p>
			
			<h4><strong><?=t($t_base.'title/title6')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message13')?>
			<br />
			<br />
			<?=t($t_base.'message/message14')?>
			<br />
			<br />
			</p>
			
			<h4><strong><?=t($t_base.'title/title7')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message15')?>
			<br />
			<br />
			<?=t($t_base.'message/message16')?>
			<br />
			<br />
			</p>
			
			<h4><strong><?=t($t_base.'title/title8')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message17')?>
			<br />
			<br />
			<?=t($t_base.'message/message18')?>
			<br />
			<br />
			</p>
			
			<h4><strong><?=t($t_base.'title/title9')?></strong></h4>
			<p >	
			<?=t($t_base.'message/message19')?>
			<br />
			<br />
			</p>
			
			<p style="font-style: italic;"><?=t($t_base.'message/message20')?></p>
		 -->
	*/ ?>
	<br />
	
	<br />
	
	</div>

</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer.php" );
?>