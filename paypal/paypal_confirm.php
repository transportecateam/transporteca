<?php
//echo "hell";
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");

$f = fopen(__APP_PATH_PAYPAL__, "a");
fwrite($f, "\n------".date("d-m-Y h:i:s")."---------\n");
fwrite($f, "\n------Paypal Confirm Page---------\n");
fwrite($f, "\n------Booking Id:".$_SESSION['booking_id']."---------\n");
if((int)$_SESSION['booking_id']>0)
{

    $idTranscation = $_GET['tx'];

    fwrite($f, "\n------Transcation Id:".$idTranscation."---------\n");	


    $idBooking = (int)$_SESSION['booking_id'] ;
    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $kForwarderContact= new cForwarderContact();
    $kUser = new cUser();
    $kWHSSearch = new cWHSSearch();
    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
    $bQuickQuoteBooking = false;
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
    {
        $bQuickQuoteBooking = true;
    }
    
    fwrite($f, "\n------Booking Status:".$postSearchAry['szPaymentStatus']."---------\n");

    $res_arr=checkPaypalTranscationAmount($idTranscation);
    fwrite($f, "\n--------Payment Data Transfer Start-------\n");
    fwrite($f, "\n#######".date('d-m-Y H:i:s')."############\n");
    fwrite($f, print_r($res_arr,true)."\n");
    fwrite($f, "\n--------Payment Data Transfer End-------\n");

    if($idTranscation!='' && $postSearchAry['szPaymentStatus']=='' && ($postSearchAry['idBookingStatus']!=3 || $bQuickQuoteBooking))
    { 
        $kPayPal= new cPayPal();
        $kPayPal->add($res_arr);
    }
    //$postSearchAry['szPaymentStatus']='';
    $kConfig = new cConfig();
    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
    $szCountryStrUrl = $kConfig->szCountryISO ;
    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
    $szCountryStrUrl .= $kConfig->szCountryISO ; 

    $thank_you_page_url =  __THANK_YOU_PAGE_URL__."/".__THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
    if($postSearchAry['szPaymentStatus']=='Completed')
    {
        fwrite($f, "\n------Booking Payment Status Completed---------\n");
        confirmBookingAfterPayment($postSearchAry);

        if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
        {
            ob_end_clean();
            header("Location:".$thank_you_page_url);
            die();
        }
        else
        {			
            ob_end_clean();
            header("Location:".__BOOKING_RECEIPT_PAGE_URL__);
            die();
        }
    }
    else if($postSearchAry['szPaymentStatus']=='Pending')
    {
        fwrite($f, "\n------Booking Payment Status Pending---------\n");
        if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
        {
            ob_end_clean();
            header("Location:".$thank_you_page_url);
            die();
        }
        else
        {			
            ob_end_clean();
            header("Location:".__BOOKING_RECEIPT_PAGE_URL__);
            die();
        }
    }
    else if($postSearchAry['szPaymentStatus']=='Failed')
    {
        fwrite($f, "\n------Booking Payment Status Failed---------\n");
        if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
        {
            ob_end_clean();
            header("Location:".$thank_you_page_url);
            die();
        }
        else
        {			
            ob_end_clean();
            header("Location:".__BOOKING_RECEIPT_PAGE_URL__);
            die();
        }
    }
    else
    {	
        ?>
        <?php
        fwrite($f, "\n------Booking Failed---------\n");
        for($i=1;$i<=10;++$i)
        {
            $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
            if($postSearchAry['szPaymentStatus']=='Completed')
            {
                fwrite($f, "\n------Booking Payment Status Completed---------\n");
                confirmBookingAfterPayment($postSearchAry);
                header("Location:".__BOOKING_RECEIPT_PAGE_URL__);
                die();
            }
            sleep(1);
        }
        fwrite($f, "\n------Booking Failed After 10 Second Check---------\n");

        $szBookingRandomNum = __BOOKING_CONFIRMATION_PAGE_URL__.'/'.$postSearchAry['szBookingRandomNum'].'/';
        header("Location:".$szBookingRandomNum);
        $_SESSION['statusFailed']='Failed';
        die();
    }
}
else
{
	$_SESSION['sessionBookingId']='Not Found';
	header("Location:".__HOME_PAGE_URL__);
	die();
}
?>