<?php 
/**
 * View Invoice
 */ 
ob_start();
session_start();
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL & ~E_NOTICE); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/Imap/PHPMailerAutoload.php" );
//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
 
//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "whizsolutions16@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "Test1234!*";
//Set who the message is to be sent from
$mail->setFrom('ajay@whiz-solutions.com', 'Ajay Jha');
//Set an alternative reply-to address
$mail->addReplyTo('ajay@whiz-solutions.com', 'Ajay Jha');
//Set who the message is to be sent to
$mail->addAddress('coolajay20@gmail.com', 'Ajay Gmail');
//Set the subject line
$mail->Subject = 'Imap GMail SMTP test with Attachments';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$szEmailMessage = " Dear DSV Transporteca,

You have received a new courier booking from 400050, Mumbai, India to 1064, Copenhagen, Denmark on Transporteca. Please click the below link to retrieve the booking information.

CLICK HERE

If this link does not work, you can paste the below URL into your browser:

http://devforwarder.transporteca.com/booking/5344__88669bcfc2a9784a748dcf0366e0f8ab/

Next step is to create the shipping labels and tracking number, and to upload them on Transporteca. We have created a task for you in your Pending Tray. We will ensure labels are well received by the shipper. Please upload labels and tracking number here:

UPLOAD LABLES AND TRACKING NUMBER HERE

If this link does not work, you can paste the below URL into your browser:

http://devforwarder.transporteca.com/pendingQuotes/createLabel/5344__88669bcfc2a9784a748dcf0366e0f8ab/

Please note that Transporteca's Terms & Conditions apply to this booking, as available from your Transporteca Control Panel on Terms & Conditions, and agreed by Freddie Forwarder on 19/04/2013.

Kind regards,

Transporteca ";
$szEmailMessage = nl2br($szEmailMessage);
$mail->msgHTML($szEmailMessage);
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';

//Attach an image file
$mail->addAttachment(__APP_PATH__.'/images/5star.png');
//send the message, check for errors
 
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
} 