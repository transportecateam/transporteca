<?php 
/**
 * View Invoice
 */ 
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH__ . "/inc/functions.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php");  
ini_set (max_execution_time,360000);

$kMautic = new cMautic();  
if($_POST['addMauticAry']['szSubmit']!='')
{
    unset($_POST['addMauticAry']['szSubmit']);
    $ApiResponseAry=$kMautic->CreateMauticLead($_POST['addMauticAry'],false,3);
} 
if(!empty($ApiResponseAry)) {?>
    <p style="text-align:left;border:1px solid;">
    <strong>Mautic Api Response</strong>
    <?php     
        $message = isset($ApiResponseAry['error']) ? $ApiResponseAry['error']['message'] : '';
        if($message!='')
        {
            echo "<br ><strong>Error Msg: ".$message."</strong>";
        }
        else
        {
            echo "<br ><strong>Mautic Account ID: ".$ApiResponseAry['lead']['id']."</strong>";
        }
    ?>
    </p>
    <?php
}
$style_str = 'style="text-align: left;padding:5px;"';
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2"> 
    <div class="hsbody-2-right"> 
        <h2>Mautic API Demo</h2>
        <div id="courier_service_container_div"> 
            <form name="mauticApiDemoForm" id="mauticApiDemoForm" method="post">
                <table class="format-2" style="width:90%;" cellpadding="5" cellspacing="3">
                    <tr>
                        <th style="width: 15%;">First Name</th>
                        <td style="width: 85%;text-align: left;padding:5px;"><input type="text" name="addMauticAry[firstname]" id="firstname" value="<?php echo $_POST['addMauticAry']['firstname'];?>"></td> 
                    </tr> 
                    <tr>
                        <th>Last Name</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[lastname]" id="lastname" value="<?php echo $_POST['addMauticAry']['lastname'];?>"></td> 
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[company]" id="company" value="<?php echo $_POST['addMauticAry']['company'];?>"></td> 
                    </tr>
                    <tr>
                        <th>Email Address *</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[email]" id="email" value="<?php echo $_POST['addMauticAry']['email'];?>"><?php if($kMautic->arErrorMessages['szEmail']!=''){?><br /><span style="color: red"><?php echo $kMautic->arErrorMessages['szEmail'];?></span> <?php }?></td> 
                    </tr>
                     <tr>
                        <th>Phone Number</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[phone]" id="phone" value="<?php echo $_POST['addMauticAry']['phone'];?>"></td> 
                    </tr>
                    <tr>
                        <th>Address 1</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[address1]" id="address1" value="<?php echo $_POST['addMauticAry']['address1'];?>"></td> 
                    </tr>
                    <tr>
                        <th>Address 2</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[address2]" id="address2" value="<?php echo $_POST['addMauticAry']['address2'];?>"></td> 
                    </tr>
                    <tr>
                        <th>City</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[city]" id="city" value="<?php echo $_POST['addMauticAry']['city'];?>"></td> 
                    </tr>
                    <tr>
                        <th>State</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[state]" id="state" value="<?php echo $_POST['addMauticAry']['state'];?>"></td> 
                    </tr>
                     <tr>
                        <th>Country</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[country]" id="country" value="<?php echo $_POST['addMauticAry']['country'];?>"></td> 
                    </tr>
                    <tr>
                        <th>IP Address</th>
                        <td <?php echo $style_str; ?>><input type="text" name="addMauticAry[ipaddress]" id="ipaddress" value="<?php echo getRealIP(); ?>"></td> 
                    </tr>
                    <tr>
                        <td colspan="2" style="align:center"> 
                            <input type="submit" name="addMauticAry[szSubmit]" value="Submit" >  
                            <input name="addMauticAry[szMauticAccountID]" id='szMauticAccountID' type='text' value="">
                        </td>
                    </tr>
                </table> 
            </form>
        </div>
    </div>
</div>
<?php   
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
