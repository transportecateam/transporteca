<?php
/**
 * Customer Account Confirmation 
 */
 ob_start();
session_start();
$page_title= __INVITE_CONFIRMATION_META_TITLE__ ;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base = "Users/MultiUserAccess/";


$szConfirmationKey=trim($_REQUEST['confirmationKey']);
if(!empty($szConfirmationKey))
{
	$userArr=$kUser->checkInviteConfirmationKey($szConfirmationKey);
	if(!empty($userArr))
	{
		// if not logged in redirect it to landing page with flag 2 means force to login at landing page
		if($_SESSION['user_id']<=0) 
		{
			$_SESSION['multi_user_key'] = $szConfirmationKey ;
			header('Location:'.__HOME_PAGE_URL__."/invitation_confirmed/");
			exit();
		}
		else if($userArr[0]['IdInvitedUser']!=$_SESSION['user_id'])
		{
			// if confirmation key is not associate with the logged in user then simply redirect him to landing page 
			header('Location:'.__HOME_PAGE_URL__);
			exit();
		}
		else
		{
			$kUser->getUserDetails($userArr[0]['IdInvitedUser']);
			
			$kUser_new = new cUser();
			$kUser_new->getUserDetails($userArr[0]['idUser']);
			
			if($kUser->idGroup>0)
			{
				// if user already belongs to any other group then following error message will occur at landing page.
				$kUser->deleteInvitedHistory($kUser->id);				
				$_SESSION['multi_user_success_message'] = "<h5><strong>".t($t_base.'messages/sorry')."!</strong></h5> <p>".t($t_base.'messages/account_cannot_linked_with');
				$_SESSION['multi_user_success_message'] .= " ".$kUser_new->szFirstName." ".$kUser_new->szLastName ;
				$_SESSION['multi_user_success_message'] .= t($t_base.'messages/belongs_to_another_group')."</p>";
				
				$_SESSION['multi_user_key'] = '' ;
				unset($_SESSION['multi_user_key']);
				
				header('Location:'.__HOME_PAGE_URL__);
				exit();
				
				?><!-- 
				<div id="hsbody">
				<?
				/*
					echo ;
					echo ;
					echo "<p>".t($t_base.'messages/account_cannot_linked_with')." ".$kUser_new->szFirstName." ".$kUser_new->szLastName."</p>";
					echo "<br/><p>".t($t_base.'messages/belongs_to_another_group')."</p>";
*/
				?>
				</div> -->
				<?php
			}
			else
			{
				// if everything goes fine than add multiple user access and redirect to landing page with success message.
				$kUser->updateInvitationStatus($userArr[0]);
				$kUser->getUserDetails($userArr[0]['IdInvitedUser']);
				$_SESSION['multi_user_success_message'] = "<h5><strong>".t($t_base.'messages/congratulations')."</strong></h5> <p>".t($t_base.'messages/success_message1');
				$_SESSION['multi_user_success_message'] .= " ".$kUser_new->szFirstName." ".$kUser_new->szLastName.". ".t($t_base.'messages/success_message2')."</p>";

				$_SESSION['multi_user_key'] = '' ;
				unset($_SESSION['multi_user_key']);
				
				header('Location:'.__HOME_PAGE_URL__);
				exit();
			/*
				?>
				<div id="hsbody">
				<?php
				echo "<p>".t($t_base.'messages/congratulations')."</p>";
				echo "<br/><br/><br/>";
				echo "<p>".t($t_base.'messages/account_linked')." ".$kUser_new->szFirstName." ".$kUser_new->szLastName."</p>";
				echo "<br/><br/>";
				echo "<p>".t($t_base.'messages/this_means_that_u_access')." ".$kUser_new->szFirstName." ".$kUser_new->szLastName."'s  ".t($t_base.'messages/book_and_can')." ".$kUser_new->szFirstName." ".$kUser_new->szLastName."'s ".t($t_base.'messages/register_shipper_consign')."</p>";
				echo "<br/><br/>";
				echo "<p>".t($t_base.'messages/u_can_revoke_under_multi_user')."</p>";
				?></div>
				<?php
*/
			}
		}
	}
	else
	{
		$_SESSION['multi_user_success_message'] = t($t_base.'messages/invalid_con_key');

		$_SESSION['multi_user_key'] = '' ;
		unset($_SESSION['multi_user_key']);
		
		header('Location:'.__HOME_PAGE_URL__);
		exit();
		/*
		?>
		<div id="regError" class="errorBox ">
		<div id="regErrorList">
		<ul>
		<li><?=t($t_base.'messages/invalid_con_key');?></li>
		</ul>
		</div>
		</div>
		<?php
*/
	}
}
else
{
	$_SESSION['multi_user_success_message'] = t($t_base.'messages/invalid_con_key');
	$_SESSION['multi_user_key'] = '' ;
	unset($_SESSION['multi_user_key']);
	
	header('Location:'.__HOME_PAGE_URL__);
	exit();
	
	?>
	<div id="regError" class="errorBox ">
		<div id="regErrorList">
		<ul>
		<li><?=t($t_base.'messages/invalid_con_key');?></li>
		</ul>
		</div>
		</div>
		<?php
}
?>