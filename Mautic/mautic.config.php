<?php

// Require this file if you're not using composer's vendor/autoload

// Required PHP extensions
if (!function_exists('curl_init')) {
  throw new Exception('Mautic needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
  throw new Exception('Mautic needs the JSON PHP extension.');
} 
//Mautic base class
require(dirname(__FILE__) . '/tests/Api/MauticApiTestCase.php'); 

// Auth utility classes
require(dirname(__FILE__) . '/lib/Auth/ApiAuth.php');
require(dirname(__FILE__) . '/lib/Auth/AuthInterface.php'); 
require(dirname(__FILE__) . '/lib/Auth/OAuth.php'); 
 
//Api library classes
require(dirname(__FILE__) . '/lib/Api/Api.php'); 
require(dirname(__FILE__) . '/tests/Api/AssetsTest.php'); 
require(dirname(__FILE__) . '/tests/Api/FormsTest.php'); 
require(dirname(__FILE__) . '/tests/Api/LeadsTest.php');  

//Exception classes
require(dirname(__FILE__) . '/lib/Exception/ActionNotSupportedException.php');
require(dirname(__FILE__) . '/lib/Exception/ContextNotFoundException.php');
require(dirname(__FILE__) . '/lib/Exception/IncorrectParametersReturnedException.php');
require(dirname(__FILE__) . '/lib/Exception/UnexpectedResponseFormatException.php');

//Logger files
/*
require(dirname(__FILE__) . '/psr/Log/AbstractLogger.php');
require(dirname(__FILE__) . '/psr/Log/InvalidArgumentException.php');
require(dirname(__FILE__) . '/psr/Log/LoggerAwareInterface.php');
require(dirname(__FILE__) . '/psr/Log/LoggerAwareTrait.php');
require(dirname(__FILE__) . '/psr/Log/LoggerInterface.php');
require(dirname(__FILE__) . '/psr/Log/LoggerTrait.php');
require(dirname(__FILE__) . '/psr/Log/LogLevel.php');
require(dirname(__FILE__) . '/psr/Log/NullLogger.php');
 * 
 */