<?php
ob_start();
session_start();

//ini_set('display_error',1);
//error_reporting(E_ALL);
//ini_set('error_reporting',E_ALL);

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = __SERVICES_PAGE_META_TITLE__;
$szMetaKeywords = __SERVICES_PAGE_META_KEYWORDS__;
$szMetaDescription = __SERVICES_PAGE_META_DESCRIPTION__;
require_once(__APP_PATH_LAYOUT__."/header.php");

$t_base = "SelectService/";
constantApiKey();
/*
if(!empty($_POST['searchAry']))
{
	$_SESSION['sess_search_ary_ajax'] = $_POST['searchAry'];
}
*/
if(!empty($_REQUEST['booking_random_num']))
{
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num']));
}
else
{
	//header("Location:");
}
 
if((int)$_SESSION['user_id']>0)
{	
	$kBooking= new cWHSSearch();
	
	//get customer currency
	$kBooking->updateBookingWithCurrency($kUser->szCurrency,$szBookingRandomNum);
}

$szResultPageMetaTitle = t($t_base.'messages/SERVICE_PAGE_META_TITLE');
?>

<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.blue.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.plastic.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.plastic.css" type="text/css" />
<link rel="stylesheet" href="<?=__BASE_STORE_CSS_URL__?>/jslider.round.plastic.css" type="text/css" />


<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jshashtable-2.1_src.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>//jquery.numberformatter-1.2.3.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>//tmpl.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jquery.dependClass-0.1.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/draggable-0.1.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__;?>/jquery.slider.js"></script>
<style type="text/css">
      #map_canvas { height: 100% }     
      .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
      .map-content p{font-size:12px !important;}
      .map_note p{font-size:12px;font-style:italic;display:inline-block;margin:1px 0 18px;}
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
    </script>

<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>
<script type="text/javascript">
$().ready(function() {
$("#Transportation_pop").attr("style","display:block;");
$.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",{szBookingRandomNum:'<?=$szBookingRandomNum?>'},function(result){		
	//setTimeout(function(){
	    $("#Transportation_pop").attr('style','display:none;');		
		result_ary = result.split("||||");
		if(jQuery.trim(result_ary[0])=='SUCCESS')
		{
			$("#hsbody-2").html(result_ary[1]);
		}
		if(result_ary[0]=='NO_RECORD_FOUND')
		{
			$("#customs_clearance_pop1").html(result_ary[1]);
		}	
		document.title = '<?php echo $szResultPageMetaTitle; ?>';		
  	//},1000);		
   });	
});
</script>
 <script type="text/javascript">
	  var destinationIcon = __JS_ONLY_SITE_BASE__ + "/images/red-marker.png";
      var originIcon = __JS_ONLY_SITE_BASE__ + "/images/blue-marker.png";
	  var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
    var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(pipe_line_string)
    {
    	addPopupScrollClass('map_popup_div');
    	var result_ary = pipe_line_string.split("|");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	initialize(result_ary);
    }
    	  
	function initialize(result_ary) {
	  var olat = result_ary[0];
	  var olang = result_ary[1];
	  var dlat = result_ary[2];
	  var dlang = result_ary[3];
	  var distance = result_ary[4];
	  var origin_address = result_ary[5];
	  var destination_address = result_ary[6];
	  
	  var origin = new google.maps.LatLng(olat, olang);      
      var destination = new google.maps.LatLng(dlat, dlang);
	  var myLatLng = new google.maps.LatLng(olat, olang);
	  var myOptions = {
		zoom: 6,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	 $("#distance_div").html('<?=t($t_base.'fields/distance_to_warehouse');?> ' + dollarAmount(distance) + ' ' +'Km');
	  map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		// for setting zoom level
	   var bounds = new google.maps.LatLngBounds();
	   bounds.extend(origin);
	   bounds.extend(destination);
	   map.setCenter(bounds.getCenter(),map.fitBounds(bounds)) ; 
	   
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(olat, olang),
		new google.maps.LatLng(dlat, dlang)   
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);
	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
	  
	  var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"<?=t($t_base.'fields/origin');?>"
	  }); 
		
	  var oaddress = '<div class="map-content"><p>'+'<?=t($t_base.'fields/your_cargo');?>'+':<br />'+origin_address+'</p></div>';
	  var daddress = '<div class="map-content"><p>'+'<?=t($t_base.'fields/forwarders_warehouse');?>'+':<br />'+destination_address+'</p></div>';

	  var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker, 
		'click', 
		infoCallback(oinfowindow, originmarker)
	  );
      //oinfowindow.open(map,originmarker);        

	  var destinationmarker = new google.maps.Marker({
		  position: destination,
		  map: map,
		  icon: destinationIcon,
		  visible: true,
		  title:"<?=t($t_base.'fields/destination');?>"
	  });    
	  
	  var dinfowindow = new google.maps.InfoWindow();
	  dinfowindow.setContent(daddress);
	  google.maps.event.addListener(
		destinationmarker, 
		'click', 
		infoCallback(dinfowindow, destinationmarker)
	  );
	  	
	  //dinfowindow.open(map,destinationmarker);        
}
</script>
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="change_price_div" style="display:none"></div>
<div id="update_cargo_details" style="display:none;">
</div>

<div id="customs_clearance_pop_right" class="help-pop right">
</div>
<div id="all_available_service" style="display:none;"></div>
<div id="customs_clearance_pop1">
</div>

<div id="map_popup_div" style="display:none;">		
		<div id="popup-bg"></div>
		 <div id="popup-container"  class="popup-scroller">			
		  <div class="popup map-popup">
		  <p class="close-icon" align="right">
			<a onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
		    <div id="distance_div" style="font-weight:bold;margin-bottom:5px;"></div>
		  	<div id="map_canvas" style="width:500px;height:480px;border: 1px solid #C4BDA1;"></div>
		  	<div class="map_note">
			<p><?=t($t_base.'fields/notes');?>: <?=t($t_base.'fields/google_map_notes');?></p>
			</div>
		  	<p align="center"><a href="javascript:void(0);" onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
		  </div>
	   </div>
</div>


<div id="Transportation_pop">
<h3 style="padding:20px 0 0;margin-bottom:5px;"><?=t($t_base.'messages/PROCESSING_MESSAGE_HEADER');?></h3>
<p align="center"><?=t($t_base.'messages/PROCESSING_MESSAGE_TEXT');?></p>
<br />
 <img src="<?=__BASE_STORE_IMAGE_URL__?>/Intermediate.gif" alt="Intermediate.gif" />
<br />
</div>
<div id="hsbody-2">						
</div>
<?
//$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
echo html_form_random_booking_number(__BOOKING_DETAILS_PAGE_URL__,$szBookingRandomNum);

require_once(__APP_PATH_LAYOUT__."/footer.php");

?>