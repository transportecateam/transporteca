<?php 
session_start();
$szMetaTitle="Transporteca API Demo";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once(__APP_PATH_LAYOUT__ ."/header_new.php");
 
if(!empty($_SESSION['mauticApiRequestAry']))
{
    $_POST['partnerApiRequestAry'] = $_SESSION['mauticApiRequestAry'] ;
    $szApiRequestParams = $_POST['partnerApiRequestAry']['szApiRequestParams'];
}
else
{
    if($_SESSION['user_id']>0)
    {
        $idCustomer = $_SESSION['user_id']; 
    }
    else
    {
        $idCustomer = 1; 
    } 
    $kMautic = new cMautic();
    $userDetailsAry = $kMautic->buildApiRequestForMautic($idCustomer);
    $userDetailsAry['ipaddress'] = getRealIP();
    unset($userDetailsAry['currency']);
    unset($userDetailsAry['szMauticAccountID']); 
    $szApiRequestParams = json_encode($userDetailsAry);
} 
?>
<div id="hsbody-2"> 
    <style type="text/css">
        .format-2 td {
            background-color: #ffffff;
            font-size: 14px;
            padding: 4px 10px;
            text-align: left;
        }
        .api_content {
            background: #f4f4f4 none repeat scroll 0 0;
            border: 1px solid #dadada;
            border-radius: 6px;
            display: table;
            font-size: 100%;
            letter-spacing: 1px;
            margin: 12px auto;
            padding: 5px 0 5px 5px;
            width: 100%;
        }

        .api_subheading_1 {
            background: #ebf3f9 none repeat scroll 0 0;
            border: 1px solid #c3d9ec;
            border-radius: 6px;
            display: block;
            margin: auto;
            overflow: auto;
            padding: 10px;
            width: 92%;
        } 
        .api_subheading {
            background: #e7f6ec none repeat scroll 0 0;
            border: 1px solid #c3e8d1;
            border-radius: 6px;
            display: block;
            margin: auto;
            padding: 10px;
            width: 95%;
        }
        .api_area_heading {
            color: #666;
            font-weight: normal;
        }
        .api_area_heading h3 span {
            display: block;
            padding: 10px 0;
        }
        .api-flo {
            float: left;
            font-size: 13px;
            font-weight: normal;
            margin-left: 16px;
            width: 85%;
        }  
        .api_txtara {
            width: 25%;
        }
        .api_hdng1 {
            color: #333;
            display: block;
            float: left;
            font: bold 13px/1.5 "Helvetica Neue",Arial,"Liberation Sans",FreeSans,sans-serif;
            padding-left: 20px;
            text-align: right;
            width: 12%;
        } 
        .api_dscrarea span {
            border: 0 solid #333;
            float: left;
            font-size: 13px;
            margin-bottom: 3px;
        }
        .api_ori_hdng {
            color: #333;
            display: block;
            float: left;
            font-size: 13px;
            font-weight: bold;
            padding-right: 10px;
            text-align: right;
            width: 13%;
        }
        .api_dp_menu_1 {
            color: #666;
            display: block;
            font-size: 13px;
            font-weight: normal;
            width: 85%;
        }
        .api_clear
        {
            clear:both;
            display:block;
        }
    </style>
    <div class="hsbody-2-right"> 
        <h2>Transporteca Mautic API Demo</h2>
        <br>
        <div id="courier_service_container_div">  
            <form id="get_api_service_form" name="get_api_service_form" method="post"> 
                <div class="api_content">
                    <div class="api_subheading_1"> 
                        <h2>API Request</h2>
                        <span class="api_dscrarea">
                            <textarea cols="100" style="min-height:200px;" rows="10" name="partnerApiRequestAry[szApiRequestParams]" id="szApiRequestParams"><?php echo $szApiRequestParams; ?></textarea> 
                        </span> 
                    </div> 
                    <br>
                    <div style="text-align:center;">
                        <input type="button" name="getCourierRateAry[szSubmit]" value="Get Details" onclick="submit_mautic_api_demo_form();"> 
                    </div>
                </div>
            </form>
        </div>
        <div class="api_content" id="api_response_container" style="display:none;"> 
        </div>
    </div>
    
</div>

<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>
