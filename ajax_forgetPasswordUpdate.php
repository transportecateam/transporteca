<?php
/**
 * Change Customer Account Password
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$kConfig = new cConfig();
$case = 'CUSTOMER';
$id = (int)$_SESSION['user_id'];
$data = $_POST['update'];
if($kConfig->updateForgotPassword($id,$data,$case) == true)
{
	echo "SUCCESS";
}
$t_base_error = "management/Error/";
if(!empty($kConfig->arErrorMessages))
{
?>
<div id="regError" class="errorBox" style="display:block;">
<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
      foreach($kConfig->arErrorMessages as $key=>$values)
      {
      ?><li><?=$values?></li>
      <?php 
      }
?>
</ul>
</div>
</div>
<?
	}
?>