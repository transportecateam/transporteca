<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
<xsl:output method="html" encoding="UTF-8" omit-xml-declaration="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transistional//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
    indent="yes" />
<xsl:variable name="hostName" select="'https://my.tnt.com/'"/> 
<xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <head>
            <title>TNT Manifest PDF</title>
            <style><![CDATA[  
                div.data {	color : black; background-color : white; font-family : arial, helvetica "sans-serif"; font-size : 6pt; }
                div.title { color : black; background-color : white; font-weight : bold;	font-family : arial, helvetica "sans-serif"; font-size : 9pt; text-decoration:underline; }
                div.pagebreak { page-break-after : always; }
                .senderAddress{ line-height: 2mm; }
                .deliveryAddress{ line-height: 3mm; }
                .table1{ height: 25mm; }
                .table1td1{ width: 56mm; height: 25mm; border-right: 1px solid #000000; }
                .table1td1table{ width: 56mm; height: 25mm; }
                .table1td2{ height: 17mm; border-bottom: 1px solid #000000; }
                .table1td3{ height: 8mm; }
                .table2td1{ width: 64mm; border-right: 1px solid #000000; }
                .table2td1table{ width: 64mm; }
                .table3{ height: 8mm; }
                .table3td1{ width: 84mm; height: 8mm; border-right: 1px solid #000000; }
                .table3td2{ height: 8mm; }
                .table4td1{ width: 67mm; border-right: 1px solid #000000; }
                .table4td2{ width: 31mm; border-right: 1px solid #000000; }
                div.addressHeader { color : black; font-weight : bold; font-family : "arial"; font-size : 6pt; } 
                div.addressHeaderCode { color : black; font-weight : bold; font-family : "arial"; font-size : 8pt; letter-spacing: 0.2cm } 
                div.barcode { color : black; font-weight : bold; font-family : "arial"; font-size : 8pt; letter-spacing: 0.1cm } 
                div.addressHeaderRec { color : black; font-weight : bold; font-family : "arial"; font-size : 6pt; } 
                div.addressData { color : black; font-family : "courier new"; font-size : 8pt; } 
                div.addressDataRec { color : black; font-weight : bold; font-family : "courier new"; }
                div.addressDataWeight { color : black; font-weight : bold; font-family : "courier new"; font-size : 11pt; }
                div.addressSmallPrint { font-family : "courier new"; font-size : 4pt; }
                div.addressSmallPrintLink { font-family : "courier new"; font-size : 5pt; }
                div.header { color : black; font-weight : bold; font-family : arial, helvetica "sans-serif"; font-size : 8pt; } 
                div.invoiceHeader { color : black; background-color : white; font-weight : bold; font-family : arial, helvetica "sans-serif"; font-size : 10pt; } 
                div.data { color : black; font-family : arial , "sans-serif"; font-size : 8pt; }
                div.smallprint { color : black; background-color : white; font-family : arial, "sans-serif"; font-size : 6pt; }
                div.smallprintlink { color : black; background-color : white; font-family : arial, "sans-serif"; font-size : 7pt; }
                div.auSmallPrint { color : black; background-color : white; font-family : arial, "sans-serif"; font-size : 7pt; }
                div.auSmallPrintLink { color : black; background-color : white; font-family : arial, "sans-serif"; font-size : 7pt; text-decoration:underline; }
                div.carrierLicence { color : black; font-family : "courier new"; font-size : 7pt; }
                .normalService { font-size: x-large; }
                .premiumService { font-size: xx-large; font-weight: bold; }
                .tntTelephone { font-size: small; }
                .deliveryDepot { font-size: 96px; }
                .data { }
                .dataBold { font-weight: bold; }
                .label { }
                .deliveryPostcode { font-size: xx-large; font-weight: bold; } 
                table.outLine { border: 1px solid #656566; border-collapse : collapse; padding : 1px; background-color: #FFFFFF; }
                td.outLineCell { border: 1px solid #656566; } 
                div.pagebreak { page-break-before : always; }
                div.sm-data { color : black; background-color : white; font-family : arial, helvetica "sans-serif"; font-size : 6pt; }
                div.sm-field { color : black; background-color : white; font-weight : bold; font-family : arial, helvetica "sans-serif"; font-size : 6pt; }
                div.sm-title { color : black; background-color : white; font-weight : bold; font-family : arial, helvetica "sans-serif"; font-size : 9pt; text-decoration:underline; }
                div.serviceCode { color : black; font-weight : bold; font-family : arial; font-size : 18pt; }
                div.operationsFlow { color : black; font-weight : bold; font-family : arial; font-size : 24pt; }
                div.productCode { color : black; font-weight : bold; font-family : arial; font-size : 12pt; }
                div.postProduct { color : black; font-family : arial; font-size : 10pt; }
                div.domestic { color : black; font-weight : bold; font-family : arial; font-size : 16pt; }
                td.border_bottom { border-bottom: 0.1 solid #000000; } 
                td.border { border: 0.1 solid #000000; } 
                ]]>
                </style>
        </head>
        <body> 
            <xsl:for-each select="CONSIGNMENTBATCH/CONSIGNMENT"> 
                <xsl:choose>
                    <xsl:when test="@marketType='DOMESTIC'"> 
                        <xsl:choose>
                            <xsl:when test="@originCountry='GB'">
                                <xsl:call-template name="ukDomesticManifest"/>
                            </xsl:when> 
                            <xsl:otherwise> 
                                <xsl:call-template name="defaultManifest"/> 
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise> 
                        <xsl:call-template name="internationalManifest"/> 
                    </xsl:otherwise>
                </xsl:choose>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template> 
 
	<xsl:template name="defaultManifest"> 
		<xsl:call-template name="internationalManifest"/>
	</xsl:template>
        
	<xsl:template name="internationalManifest">  
            <table cellspacing="0" cellpadding="3" style="width:100%;">
                <tr>
                    <td style="width: 14%">TRANPORTECA_SPACE_BAR</td>
                    <td style="width: 20%" >TRANPORTECA_SPACE_BAR</td>
                    <td style="width: 25%">TRANPORTECA_SPACE_BAR</td>
                    <td style="width: 20%">TRANPORTECA_SPACE_BAR</td>
                    <td style="width: 21%">TRANPORTECA_SPACE_BAR</td>
                </tr>
                <tr>
                    <td colspan="1" valign="top">TNT_LOGO_IMAGE_TAG</td>
                    <td align="center" colspan="3" valign="top"> 
                        <xsl:choose>
                            <xsl:when test="PAYMENTIND/text() = 'S'">
                                <div class="title">COLLECTION MANIFEST (DETAIL) - OTHERS (SENDER PAYS)-1</div>
                            </xsl:when>
                            <xsl:otherwise>
                                <div class="title">COLLECTION MANIFEST (DETAIL) - OTHERS (RECEIVER PAYS)-1</div>
                            </xsl:otherwise>
                        </xsl:choose>  
                        <div class="data">TNT Express<br/>Shipment Date : <xsl:value-of select="HEADER/SHIPMENTDATE"/><br/>Pickup id : </div>
                    </td>
                    <td align="right">
                        <div class="data"></div>
                    </td>
                </tr> 
                <tr>
                    <td colspan="1" class="border_bottom">
                        <div class="data">Sender Account<br/>Sender Name<br/>&amp; Address</div>
                    </td>
                    <td colspan="3" class="border_bottom">
                        <div class="data">: <xsl:value-of select="HEADER/SENDER/ACCOUNT"/><br/>: <xsl:value-of select="HEADER/SENDER/COMPANYNAME"/><br/>: <xsl:value-of select="HEADER/SENDER/STREETADDRESS1"/>,<xsl:if test="HEADER/SENDER/STREETADDRESS2/text()"><xsl:value-of select="HEADER/SENDER/STREETADDRESS2"/>,</xsl:if><xsl:if test="HEADER/SENDER/STREETADDRESS3/text()"><xsl:value-of select="HEADER/SENDER/STREETADDRESS3"/>,</xsl:if><xsl:if test="HEADER/SENDER/CITY/text()"><xsl:value-of select="HEADER/SENDER/CITY"/>,</xsl:if><xsl:if test="HEADER/SENDER/PROVINCE/text()"><xsl:value-of select="HEADER/SENDER/PROVINCE"/>,</xsl:if><xsl:if test="HEADER/SENDER/POSTCODE/text()"><xsl:value-of select="HEADER/SENDER/POSTCODE"/>,</xsl:if><xsl:value-of select="HEADER/SENDER/COUNTRY"/></div>
                    </td>
                    <td colspan="1" class="border_bottom">
                        <div class="data">Printed on: TNT_INVOICE_DATE</div>
                    </td>
                </tr>  
                <tr>
                    <td colspan="2" align="center">
                        <div class="data">
                            TNT_BARCODE_IMAGE_TAG <br/>
                            *<xsl:value-of select="CONNUMBER"/>*
                        </div> 
                    </td>
                    <td valign="top">
                        <div class="data">Sending Depot: <br/>Receiving Depot:<br/></div>
                    </td>
                    <td valign="top" colspan="2">
                        <div class="data">
                            <u>Special Instructions</u>
                            <xsl:if test="DELIVERYINST/text()">
                                <br/>
                                <xsl:value-of select="DELIVERYINST"/>
                            </xsl:if> 
                            <xsl:choose>
                                <xsl:when test="PAYMENTIND/text() = 'S'">
                                    <br/>SENDER PAYS
                                </xsl:when>
                                <xsl:otherwise>
                                    <br/>RECEIVER PAYS
                                </xsl:otherwise>
                            </xsl:choose>
                        </div> 
                    </td>
                </tr> 
                <tr>
                    <td colspan="1">
                        <div class="data">Sender Contact<br/>Sender Tel:<br/>Sender Ref<br/>Receiver Vat Nr<br/>Receiver Acc Number</div>
                    </td> 
                    <td valign="top" colspan="4">
                        <div class="data">: <xsl:value-of select="HEADER/SENDER/CONTACTNAME"/><br/>: <xsl:value-of select="HEADER/SENDER/CONTACTDIALCODE"/> <xsl:value-of select="HEADER/SENDER/CONTACTTELEPHONE"/><br/>: <xsl:value-of select="CUSTOMERREF"/><br/>: <xsl:value-of select="RECEIVER/VAT"/><br/>: <xsl:value-of select="RECEIVER/ACCOUNT"/></div>
                    </td> 
                </tr> 
                <tr>
                    <td valign="top" colspan="1">
                        <div class="data">Receiver Name<br/>&amp; Address<br/>Receiver Tel<br/>Collection<br/>&amp; Address<br/>Delivery<br/>&amp; Address</div>
                    </td>
                    <td valign="top" colspan="3">
                        <div class="data">: <xsl:value-of select="RECEIVER/COMPANYNAME"/>,<xsl:value-of select="RECEIVER/STREETADDRESS1"/>,<xsl:if test="RECEIVER/STREETADDRESS2/text()"><xsl:value-of select="RECEIVER/STREETADDRESS2"/>,</xsl:if><xsl:if test="RECEIVER/STREETADDRESS3/text()"><xsl:value-of select="RECEIVER/STREETADDRESS3"/></xsl:if>  Receiver Contact : <xsl:value-of select="RECEIVER/CONTACTNAME"/><br/>: <xsl:if test="RECEIVER/CITY/text()"><xsl:value-of select="RECEIVER/CITY"/>,</xsl:if><xsl:if test="RECEIVER/PROVINCE/text()"><xsl:value-of select="RECEIVER/PROVINCE"/>,</xsl:if><xsl:if test="RECEIVER/POSTCODE/text()"><xsl:value-of select="RECEIVER/POSTCODE"/>,</xsl:if><xsl:value-of select="RECEIVER/COUNTRY"/><br/>: <xsl:if test="RECEIVER/CONTACTTELEPHONE/text()"><xsl:value-of select="RECEIVER/CONTACTDIALCODE"/><xsl:value-of select="RECEIVER/CONTACTTELEPHONE"/></xsl:if><br/>: <xsl:if test="HEADER/COLLECTION/COMPANYNAME/text()"><xsl:value-of select="HEADER/COLLECTION/COMPANYNAME"/>,</xsl:if><xsl:if test="HEADER/COLLECTION/STREETADDRESS1/text()"><xsl:value-of select="HEADER/COLLECTION/STREETADDRESS1"/>,</xsl:if><xsl:if test="HEADER/COLLECTION/STREETADDRESS2/text()"><xsl:value-of select="HEADER/COLLECTION/STREETADDRESS2"/>,</xsl:if><xsl:if test="HEADER/COLLECTION/STREETADDRESS3/text()"><xsl:value-of select="HEADER/COLLECTION/STREETADDRESS3"/></xsl:if><br/>: <xsl:if test="HEADER/COLLECTION/CITY/text()"><xsl:value-of select="HEADER/COLLECTION/CITY"/>,</xsl:if><xsl:if test="HEADER/COLLECTION/PROVINCE/text()"><xsl:value-of select="HEADER/COLLECTION/PROVINCE"/>,</xsl:if><xsl:if test="HEADER/COLLECTION/POSTCODE/text()"><xsl:value-of select="HEADER/COLLECTION/POSTCODE"/>,</xsl:if><xsl:value-of select="HEADER/COLLECTION/COUNTRY"/><br/>: <xsl:if test="DELIVERY/COMPANYNAME/text()"><xsl:value-of select="DELIVERY/COMPANYNAME"/>,</xsl:if><xsl:if test="DELIVERY/STREETADDRESS1/text()"><xsl:value-of select="DELIVERY/STREETADDRESS1"/>,</xsl:if><xsl:if test="DELIVERY/STREETADDRESS2/text()"><xsl:value-of select="DELIVERY/STREETADDRESS2"/>,</xsl:if><xsl:if test="DELIVERY/STREETADDRESS3/text()"><xsl:value-of select="DELIVERY/STREETADDRESS3"/></xsl:if><br/>: <xsl:if test="DELIVERY/CITY/text()"><xsl:value-of select="DELIVERY/CITY"/>,</xsl:if><xsl:if test="DELIVERY/PROVINCE/text()"><xsl:value-of select="DELIVERY/PROVINCE"/>,</xsl:if><xsl:if test="DELIVERY/POSTCODE/text()"><xsl:value-of select="DELIVERY/POSTCODE"/>,</xsl:if><xsl:value-of select="DELIVERY/COUNTRY"/></div>
                    </td>
                    <td valign="top" colspan="1">
                        <div class="data">Serv : <xsl:value-of select="SERVICE"/><br/>Opts : <xsl:value-of select="OPTION1"/><xsl:if test="OPTION2/text()"><br/><xsl:value-of select="OPTION2"/></xsl:if><xsl:if test="OPTION3/text()"><br/><xsl:value-of select="OPTION3"/></xsl:if><xsl:if test="OPTION4/text()"><br/><xsl:value-of select="OPTION4"/></xsl:if><xsl:if test="OPTION5/text()"><br/><xsl:value-of select="OPTION5"/></xsl:if></div>
                    </td>
                </tr> 
                <tr>
                    <td colspan="1">
                        <div class="data">No Pieces : <xsl:value-of select="TOTALITEMS"/></div>
                    </td>
                    <td colspan="1">
                        <div class="data">Weight : <xsl:value-of select="TOTALWEIGHT"/><xsl:value-of select="TOTALWEIGHT/@units"/></div>
                    </td>
                    <td colspan="1">
                        <div class="data">Insurance Value : <xsl:value-of select="INSURANCEVALUE"/><xsl:value-of select="INSURANCECURRENCY"/></div>
                    </td>
                    <td colspan="2">
                        <div class="data">Invoice Value : <xsl:value-of select="GOODSVALUE"/><xsl:value-of select="CURRENCY"/></div>
                    </td> 
                </tr> 
                <tr>
                    <td colspan="2" valign="bottom" class="border_bottom">
                        <div class="data">Description (including packing and marks)</div>
                    </td>
                    <td colspan="2" align="center" valign="bottom" class="border_bottom">
                        <div class="data">Dimensions (L x W x H)</div>
                    </td>
                    <td colspan="1" align="center" valign="top" class="border_bottom">
                        <div class="data">Total Consignment Volume<br/><xsl:value-of select="TOTALVOLUME"/><xsl:value-of select="PACKAGE/VOLUME/@units"/></div>
                    </td>
                </tr> 
                <xsl:for-each select="PACKAGE/ARTICLE">
                    <tr>
                        <td colspan="2">
                            <div class="data">
                                <xsl:value-of select="INVOICEDESC"/>
                            </div>
                        </td>
                        <td colspan="2" align="center" valign="top">
                            <div class="data">
                                <xsl:value-of select="../LENGTH"/>
                                <xsl:value-of select="../LENGTH/@units"/><xsl:value-of select="../WIDTH"/><xsl:value-of select="../WIDTH/@units"/><xsl:value-of select="../HEIGHT"/><xsl:value-of select="../HEIGHT/@units"/></div>
                        </td>
                        <td colspan="1" align="center" valign="top">
                            <div class="data"></div>
                        </td>
                    </tr>
                </xsl:for-each> 
                <tr>
                    <td colspan="5"> 
                    </td>
                </tr>
            </table>
            <xsl:choose>
                <xsl:when test="HEADER/@last[.='false']">
                    <div>
                        <div size="1" color="#5FFFFF">.</div>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                </xsl:otherwise>
            </xsl:choose>
            <div class="pagebreak"></div>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="CombinedManifest" userelativepaths="yes" externalpreview="no" url="..\xml\manifest&#x2D;doc.xml" htmlbaseurl="" outputurl="..\html\newCombinedManifest.html" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->