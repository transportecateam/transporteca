<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
<xsl:output method="html" encoding="UTF-8" omit-xml-declaration="yes" doctype-public="-//W3C//DTD XHTML 1.0 Transistional//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
    indent="yes"
    /> 
<xsl:template match="/">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	<title>TNT Commercial Invoice</title>
	<style> 
		div.header
		{
                    color : black;
                    background-color : white;
                    font-weight : bold;
                    font-family : arial, helvetica "sans-serif";
                    font-size : 12px; 
                } 
		div.data
		{
                    color : black;
                    background-color : white;
                    font-family : arial , "sans-serif";
                    font-size : 10px; 
		}
            span.header
		{
                    color : black;
                    background-color : white;
                    font-weight : bold;
                    font-family : arial, helvetica "sans-serif";
                    font-size : 10px; 
                } 
		span.data
		{
                    color : black;
                    background-color : white;
                    font-family : arial , "sans-serif";
                    font-size : 9px; 
		} 
                td { border: 0.1 solid #000000; } 
		</style>
		</head>
		<body>
			<xsl:choose>
			<xsl:when test="CONSIGNMENTBATCH/CONSIGNMENT/@copies">
				<xsl:call-template name="Invoice">
					<xsl:with-param name="Copies" select="/CONSIGNMENTBATCH/CONSIGNMENT/@copies"/>
				</xsl:call-template>
			</xsl:when>
			
			<xsl:otherwise>
				<xsl:call-template name="Invoice">
					<xsl:with-param name="Copies" select="'1'"/>
				</xsl:call-template>
			</xsl:otherwise>
			</xsl:choose>
			
			</body>
			</html>

			
		</xsl:template>
		
		<xsl:template name="Invoice">
			<xsl:param name="Copies"/>
			<xsl:param name="Copy" select="1"/> 

			<xsl:if test="$Copy &lt;= $Copies">
		

		<xsl:for-each select="CONSIGNMENTBATCH/CONSIGNMENT"> 
                    <table cellpadding="3" cellspacing="0" style="width:100%;border: 0.1 solid #000000;">
                        <tr> 	
                            <td style="width:50%" valign="top"><span class="header">SENDER (Seller/Exporter)</span><br/><span class="data"><xsl:value-of select="HEADER/SENDER/COMPANYNAME"/><br/><xsl:value-of select="HEADER/SENDER/STREETADDRESS1"/><br/><xsl:if test="HEADER/SENDER/STREETADDRESS2/text()"><xsl:value-of select="HEADER/SENDER/STREETADDRESS2"/><br/></xsl:if><xsl:if test="HEADER/SENDER/STREETADDRESS3/text()"><xsl:value-of select="HEADER/SENDER/STREETADDRESS3"/><br/></xsl:if><xsl:value-of select="HEADER/SENDER/CITY"/><br/><xsl:if test="HEADER/SENDER/PROVINCE/text()"><xsl:value-of select="HEADER/SENDER/PROVINCE"/><br/></xsl:if><xsl:if test="HEADER/SENDER/POSTCODE/text()"><xsl:value-of select="HEADER/SENDER/POSTCODE"/><br/></xsl:if><xsl:value-of select="HEADER/SENDER/COUNTRY"/><br/><xsl:value-of select="HEADER/SENDER/CONTACTNAME"/><br/><xsl:value-of select="HEADER/SENDER/CONTACTDIALCODE"/>TRANPORTECA_SPACE_BAR<xsl:value-of select="HEADER/SENDER/CONTACTTELEPHONE"/></span></td> 
                            <td style="width:50%">
                                <table style="width:100%;border: 0.1 solid #000000;" cellpadding="3" cellspacing="0">
                                    <tr>
                                        <td colspan="2" style="text-align:center;font-size:20px;font-weight:bold;">Invoice</td>
                                    </tr>
                                    <tr>
                                        <td><span class="header">Invoice Number : </span></td>
                                        <td align="center"><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="header">Date : </span></td>
                                        <td align="center"><span class="data">TNT_INVOICE_DATE</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="header">Consignment Note No : </span></td>
                                        <td align="center"><span class="data"><xsl:value-of select="CONNUMBER"/></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="header">Purchase Order No : </span></td>
                                        <td align="center"><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="header">Invoice Currency : </span></td>
                                        <td align="center"><span class="data"><xsl:value-of select="CURRENCY"/></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="header">Reason for Export : </span></td>
                                        <td align="center"><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                                    </tr>
                                </table>
                            </td>
			</tr> 
			<tr>
                            <td valign="top">
                                <span class="header">RECEIVER (Buyer/Importer)</span><br/>
                                <span class="data">
                                    <xsl:value-of select="RECEIVER/COMPANYNAME"/>
                                    <br/>
                                    <xsl:value-of select="RECEIVER/STREETADDRESS1"/>
                                    <br/>
                                    <xsl:if test="RECEIVER/STREETADDRESS2/text()">
                                        <xsl:value-of select="RECEIVER/STREETADDRESS2"/>
                                        <br/>
                                    </xsl:if>
                                    <xsl:if test="RECEIVER/STREETADDRESS3/text()">
                                        <xsl:value-of select="RECEIVER/STREETADDRESS3"/>
                                        <br/>
                                    </xsl:if>
                                    <xsl:value-of select="RECEIVER/CITY"/>
                                    <br/>
                                    <xsl:if test="RECEIVER/PROVINCE/text()">
                                        <xsl:value-of select="RECEIVER/PROVINCE"/>
                                        <br/>
                                    </xsl:if>
                                    <xsl:if test="RECEIVER/POSTCODE/text()">
                                        <xsl:value-of select="RECEIVER/POSTCODE"/>
                                        <br/>
                                    </xsl:if>
                                    <xsl:value-of select="RECEIVER/COUNTRY"/>
                                    <br/>
                                    <xsl:value-of select="RECEIVER/CONTACTNAME"/>
                                    <br/>
                                    <xsl:value-of select="RECEIVER/CONTACTDIALCODE"/>TRANPORTECA_SPACE_BAR<xsl:value-of select="RECEIVER/CONTACTTELEPHONE"/>
                                </span>
                            </td>
                            <xsl:choose>
                                <xsl:when test="DELIVERY/COMPANYNAME/text()">
                                    <td valign="top"><span class="header">DELIVER TO (if different from receiver)</span><br/><span class="data"><xsl:value-of select="DELIVERY/COMPANYNAME"/><br/><xsl:value-of select="DELIVERY/STREETADDRESS1"/><br/><xsl:if test="DELIVERY/STREETADDRESS2/text()"><xsl:value-of select="DELIVERY/STREETADDRESS2"/><br/></xsl:if><xsl:if test="DELIVERY/STREETADDRESS3/text()"><xsl:value-of select="DELIVERY/STREETADDRESS3"/><br/></xsl:if><xsl:value-of select="DELIVERY/CITY"/><br/><xsl:if test="DELIVERY/PROVINCE/text()"><xsl:value-of select="DELIVERY/PROVINCE"/><br/></xsl:if><xsl:if test="DELIVERY/POSTCODE/text()"><xsl:value-of select="DELIVERY/POSTCODE"/><br/></xsl:if><xsl:value-of select="DELIVERY/COUNTRY"/><br/><xsl:value-of select="DELIVERY/CONTACTNAME"/><br/><xsl:value-of select="DELIVERY/CONTACTDIALCODE"/>TRANPORTECA_SPACE_BAR<xsl:value-of select="DELIVERY/CONTACTTELEPHONE"/></span></td>
                                </xsl:when> 
                                <xsl:otherwise>
                                    <td valign="top"><span class="header">DELIVER TO (if different from receiver)</span></td>
                                </xsl:otherwise>
                            </xsl:choose>
			</tr>   					
                    </table>
            <br/> <br/>
                <table cellpadding="3" cellspacing="0" style="width:100%;border: 0.1 solid #000000;">
                    <tr>
                        <td><span class="header">Quantity</span></td>
                        <td><span class="header">Units</span></td>
                        <td><span class="header">Weight</span></td>
                        <td colspan="3"><span class="header">Description of Goods</span></td>
                        <td><span class="header">HS Tariff Code</span></td>
                        <td><span class="header">Country of Origin</span></td>
                        <td><span class="header">Unit Value</span></td>
                        <td><span class="header">Total Value</span></td>
                    </tr>
                    <xsl:for-each select="PACKAGE/ARTICLE">  
                    <tr>
                        <td><span class="data"><xsl:value-of select="ITEMS"/></span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data"><xsl:value-of select="WEIGHT"/></span></td>
                        <td colspan="3"><span class="data"><xsl:value-of select="INVOICEDESC"/></span></td>
                        <td><span class="data"><xsl:value-of select="HTS"/></span></td>
                        <td><span class="data"><xsl:value-of select="ORIGINCOUNTRY"/></span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data"><xsl:value-of select="INVOICEVALUE"/></span></td>
                    </tr>
                    </xsl:for-each> 
                    <tr>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td colspan="3"><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                    </tr>
                    <tr>
                        <td colspan="1"><span class="header">Total Weight</span></td>
                        <td colspan="1"><span class="data"><xsl:value-of select="TOTALWEIGHT"/></span></td>
                        <td colspan="1"><span class="header">Units of Weight</span></td>
                        <td colspan="1" align="center"><span class="data"><xsl:value-of select="TOTALWEIGHT/@units"/></span></td>
                        <td colspan="2"><span class="header">Total Number of Packages</span></td>
                        <td colspan="1"><span class="data"><xsl:value-of select="TOTALITEMS"/></span></td>
                        <td colspan="2"><span class="header">Invoice Line Total</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                    </tr> 
                    <tr>
                        <td rowspan="5" colspan="7" valign="top"><span class="header">Declaration(s)</span></td> 
                        <td colspan="2" align="right">
                            <span class="data">Discount</span>
                        </td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <span class="data">Invoice Sub-total</span>
                        </td>
                        <td><span class="data"><xsl:value-of select="GOODSVALUE"/>TRANPORTECA_SPACE_BAR<xsl:value-of select="INSURANCECURRENCY"/></span></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <span class="data">Freight Charges</span>
                        </td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <span class="data">Insurance</span>
                        </td>
                        <td><span class="data"><xsl:value-of select="INSURANCEVALUE"/>TRANPORTECA_SPACE_BAR<xsl:value-of select="INSURANCECURRENCY"/></span></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <span class="data">Other Charges</span>
                        </td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                    </tr> 
                    <tr>
                        <td colspan="6" align="center"><span class="header">INCO Terms</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                        <td colspan="2" align="right"><span class="header">Invoice Total</span></td>
                        <td><span class="data">TRANPORTECA_SPACE_BAR</span></td>
                    </tr> 
                </table> 
		<br/><br/>
		<table cellpadding="3" cellspacing="0" style="width:100%;border: 0.1 solid #000000;">
                    <tr>
                        <td><span class="header">Shipper Name and Job Title</span></td>
                        <td><span class="header">Shipper Signature</span></td>
                        <td><span class="header">Date</span></td>
                    </tr>
                    <tr>
                        <td>TRANPORTECA_SPACE_BAR</td>
                        <td>TRANPORTECA_SPACE_BAR</td>
                        <td>
                            <span class="data">TNT_INVOICE_DATE</span>
                        </td>
                    </tr>
		</table>
		<xsl:choose>
                    <xsl:when test="HEADER/@last ='false'">
                        <div>
                            <div style="color:#5FFFFF">.</div>
                        </div>
                    </xsl:when>
                    <xsl:otherwise> 
                    </xsl:otherwise>
                </xsl:choose> 
</xsl:for-each>

                <xsl:if test="$Copy &lt; $Copies">
                <div>
                    <div style="color:#5FFFFF">.</div>
                </div>
                </xsl:if> 

<xsl:call-template name="Invoice">
	<xsl:with-param name="Copy" select="$Copy+1"/>
	<xsl:with-param name="Copies" select="$Copies"/>
</xsl:call-template>
</xsl:if>
</xsl:template>	
			
		
		

</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="Scenario2" userelativepaths="yes" externalpreview="no" url="file://c:\Documents and Settings\g466ahe\Desktop\invoice.xml" htmlbaseurl="" outputurl="" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->