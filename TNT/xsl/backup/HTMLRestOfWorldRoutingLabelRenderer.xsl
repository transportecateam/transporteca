<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="RestOfWorldHtml">
    <style>
        span#originDepotHeader {
            float:left;
            font-size: 8px;
        }
        span#pickupDateHeader {
            font-size: 8px;
        } 
        span#pickupDateDetail {
            font-size: 8px;
        }
        span#accountNumberHeader {
            font-size: 8px;
        } 
        span#accountNumberDetail { 
            font-size: 8px;
        }
        span#postcodeHeader {
            position: absolute;   
            font-size: 8px;
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-color: black;
            text-align: left;
        }
        span#originAddressHeader { 
            font-size: 8px;
        }
        span#originAddressDetail { 
            font-size: 6px;
        }
        span#deliveryAddressHeader { 
            font-size: 8px;
        }
        span#deliveryAddressDetail { 
            font-size: 6px;
        } 
        span#marketAndTransportType{
            font-size: 12px;
            font-weight:bold; 
            text-align: left;
        } 
        span#pieceHeader {
            font-size: 8px;
        }
        span#pieceDetail{
            font-size: 10px;
            font-weight: bold;
        }
        span#weightHeader {
            font-size: 8px;
        } 
        span#weightDetail{
            font-size: 10px;
            font-weight: bold;
        }
        span#conNumberHeader { 
            font-size: 8px;
        }
        span#conNumberDetail{
            font-weight: bold;
            font-size: 14px; 
        }
        span#destinationDepotHeader{font-size: 8px;}
        span#destinationDepotDetail{
            font-size: 18px;
            font-weight: bold;
        } 
        span#sortHeader{font-size: 8px;}
        span#sortDetail{
            font-size: 18px;
            font-weight: bold;
        } 
        span#routingDetail{
            font-size: 18px;
            font-weight: bold; 
        } 
        span#originDepotDetail{
            font-size: 18px;
            font-weight: bold;
        } 
        span#serviceHeader{font-size: 8px;}
        span#serviceDetail{
            font-size: 14px; 
        } 
        span#postcode{
            font-size: 20px;
            font-weight: bold;
            color:#FFFFFF; 
        }
        span#customerReferenceHeader{font-size: 8px;} 
        span#customerReferenceDetail{font-size: 8px;font-weight:bold;}  
    </style>
<page style="width:330px;height:485px;">
<table cellspacing="0" cellpadding="0" border="1" style="width:290px;height:485px;text-align:left;" bgcolor="#ffffff">
    <tr>
        <td colspan="2" style="width:100%;height:35px;" bgcolor="#ffffff">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" height="35">
                <tr>
                    <td style="text-align:center;border-right:1px solid #000000;width:40%;" valign="middle" height="35" width="40%">
                        <!--Logo-->
                        <span id="logo">
                            <img src='TNT_LABEL_IMAGE_PATH/logo.jpg' alt='logo' id="tntLogo" height="27" style="text-align: center;" vspace="3"/>
                        </span>
                    </td>
                    <td style="text-align:center;padding-left:4px;" width="40%" valign="middle" align="center" height="35">
                        <!--Market & Transport Type-->
                        <span id="marketAndTransportType">
                          <xsl:value-of select="../consignmentLabelData/marketDisplay"/>
                          <xsl:text>/</xsl:text>
                          <xsl:value-of select="../consignmentLabelData/transportDisplay"/>
                        </span>
                    </td> 
                    <td style="background-color: #000000;color: #000000;" bgcolor="#000000" width="10%" height="35">
                        <!--Free Circulation Display--> 
                        <xsl:choose>
                          <xsl:when test="string-length(../consignmentLabelData/freeCirculationDisplay)>0">
                            <span id="freeCirculationIndicator" >
                              <xsl:value-of select="../consignmentLabelData/freeCirculationDisplay"/>
                            </span>
                          </xsl:when>
                          <xsl:otherwise>
                            <span id="freeCirculationIndicator" >
                            <xsl:text> </xsl:text>
                            </span>
                          </xsl:otherwise>
                        </xsl:choose>
                    </td> 
                    <td style="font-weight: bold;font-size: 25px;text-align: center;line-height:27px;" width="10%" valign="middle">
                        <!--Sort Split Indicator-->
                        <xsl:choose>
                          <xsl:when test="string-length(../consignmentLabelData/sortSplitText)>0">
                              <span id="sortSplitIndicator">
                                <xsl:value-of select="../consignmentLabelData/sortSplitText" />
                              </span>
                          </xsl:when>
                          <xsl:otherwise>
                              <span id="sortSplitIndicator">
                                  <xsl:text> </xsl:text>
                              </span>
                          </xsl:otherwise>
                        </xsl:choose>
                    </td> 
                </tr>
            </table>
        </td> 
    </tr>
    <tr>
        <td style="width:50%" bgcolor="#ffffff">
            <table width="100%" cellspacing="0" cellpadding="2" border="0">
                <tr>
                    <td colspan="2"  style="text-align:left;border-bottom: 1px solid #000000;height:45px;">
                        <!--Consignment Number-->
                        <span id="conNumber">
                          <span id="conNumberHeader">Con No<br/></span>
                          <span id="conNumberDetail"> <xsl:value-of select="../consignmentLabelData/consignmentNumber"/></span>
                        </span> <br/>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%" style="text-align:left;">
                                    <!--Pieces-->
                                    <span id="piece">
                                      <span id="pieceHeader">Piece</span><br/> 
                                      <span id="pieceDetail"><xsl:value-of select="pieceNumber"/> of <xsl:value-of select="../consignmentLabelData/totalNumberOfPieces"/></span>
                                    </span>  
                                </td>
                                <td width="50%" style="text-align:left;">
                                    <!--Weight-->
                                    <span id="weight">
                                      <span id="weightHeader">Weight</span><br/>
                                      <xsl:choose>
                                          <xsl:when test="weightDisplay/@renderInstructions='highlighted'">
                                              <span id="weightDetailHighlighted">
                                                  <xsl:value-of select="weightDisplay" />
                                              </span>
                                          </xsl:when>
                                          <xsl:otherwise>
                                              <span id="weightDetail" >
                                                  <xsl:value-of select="weightDisplay" />
                                              </span>
                                          </xsl:otherwise>
                                      </xsl:choose>
                                      <xsl:text> </xsl:text>
                                    </span> 
                                </td>
                            </tr>
                        </table>
                    </td> 
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 1px solid #000000;text-align:left;height:25px;line-height:10px;">
                        <!--Customer Reference -->
                        <span id="customerReference">
                            <span id="customerReferenceHeader">Customer Reference<br/></span>
                            <span id="customerReferenceDetail"><xsl:value-of select="pieceReference" disable-output-escaping="yes" /></span>
                        </span><br/>
                        <!--Account Number--> 
                        <span id="accountNumber">
                            <span id="accountNumberHeader">S/R Account No </span>
                            <span id="accountNumberDetail"> <xsl:value-of select="../consignmentLabelData/account/accountNumber" /></span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 0.1 dashed #000000;text-align:left;height:40px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2" style="height:10px;"><span id="originAddressHeader">Sender Address</span></td>
                            </tr>
                            <tr>
                                <td style="width:5%;height:40px;"> </td>
                                <td style="width:95%;">
                                    <!--Origin Address & Delivery Address-->
                                    <span id="originAddress"> 
                                        <span id="originAddressDetail"> 
                                            <xsl:value-of select="../consignmentLabelData/sender/name" disable-output-escaping="yes"/><br /> 
                                            <xsl:value-of select="../consignmentLabelData/sender/addressLine1" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/sender/addressLine2" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/sender/town" disable-output-escaping="yes"/><xsl:text>  </xsl:text>    
                                            <xsl:value-of select="../consignmentLabelData/sender/postcode"/><br /> 
                                            <xsl:value-of select="../consignmentLabelData/sender/country"/>        
                                        </span>
                                    </span>
                                </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 1px solid #000000;text-align:left;height:40px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2" style="height:10px;"><span id="deliveryAddressHeader">Delivery Address</span></td>
                            </tr>
                            <tr>
                                <td style="width:5%;height:40px;"> </td>
                                <td style="width:95%;">
                                    <span id="deliveryAddress" style="clear:left"> 
                                        <span id="deliveryAddressDetail">
                                            <xsl:value-of select="../consignmentLabelData/delivery/name" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/addressLine1" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/addressLine2" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/town" disable-output-escaping="yes"/><xsl:text>   </xsl:text>    
                                            <xsl:value-of select="../consignmentLabelData/delivery/postcode"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/country"/>   
                                        </span>
                                    </span> 
                                </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td width="40%" style="border-bottom: 0.1 solid #000000;text-align:left;height:20px;">
                        <!--Postcode/Cluster code-->
                        <span id="postcodeHeader">Postcode / <br/>Cluster Code</span>
                    </td>
                    <td width="60%" bgcolor="#000000" style="border-right: 0.1 solid #000000;border-bottom: 0.1 solid #000000;text-align:left;">
                        <span id="postcode">
                            <xsl:choose>
                            <!--If the length of the Cluster code is greater than 3 then the post code is being displayed
                            instead, so different rendering applies-->
                              <xsl:when test="string-length(../consignmentLabelData/clusterCode)>3">
                                <span id="postcodeDetail"><xsl:value-of select="../consignmentLabelData/delivery/postcode"/></span>
                              </xsl:when>
                              <xsl:otherwise>
                                <span id="clustercodeDetail"><xsl:value-of select="../consignmentLabelData/clusterCode"/></span>
                              </xsl:otherwise>
                            </xsl:choose>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%" style="text-align:left;" bgcolor="#ffffff">
            <table width="100%" cellspacing="0" cellpadding="2" border="0">
                <tr>
                    <td bgcolor="#000000" style="color:#ffffff;border-bottom: 0.1 solid #000000;height:40px;" valign="middle">
                        <!--Service-->
                        <span id="service">
                          <span id="serviceHeader">Service<br/></span>
                          <xsl:choose>
                              <xsl:when test="string-length(../consignmentLabelData/product)>15">
                                  <span id="serviceDetail">
                                       <xsl:value-of select="../consignmentLabelData/product" />
                                  </span>
                              </xsl:when>
                              <xsl:otherwise>
                                  <span id="serviceDetail">
                                       <xsl:value-of select="../consignmentLabelData/product" />
                                  </span>
                              </xsl:otherwise>
                          </xsl:choose>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 0.1 dashed #000000;height:30px;">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="width:20%" valign="top">
                                    <!--Origin Depot & Pickup Date--> 
                                    <span id="originDepotHeader">Origin </span> 
                                </td>
                                <td style="width:40%;text-align:left;" valign="bottom">
                                    <span id="originDepotDetail"> 
                                        <xsl:value-of select="../consignmentLabelData/originDepot/depotCode" />
                                    </span>
                                </td>
                                <td style="width:40%">
                                    <span id="pickupDate">
                                        <span id="pickupDateHeader">Pickup Date </span>  
                                    </span>
                                    <br/><span id="pickupDateDetail">
                                        <xsl:call-template name="FormatDate">    
                                            <xsl:with-param name="DateTime" select="../consignmentLabelData/collectionDate"/> 
                                        </xsl:call-template>    
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 0.1 dashed #000000;height:70px;">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="width:28%;text-align:left;" valign="top">
                                    <span id="routingHeader">Routing</span>
                                </td>
                                <td style="width:72%;text-align:left;" valign="top"> 
                                    <span id="routingDetail"> 
                                        <!-- Check if route includes any transit depots-->
                                        <xsl:if test="count(../consignmentLabelData/transitDepots/*)=0">
                                            <xsl:text> </xsl:text>
                                        </xsl:if> 
                                       <xsl:for-each select="../consignmentLabelData/transitDepots/*"> 
                                           <xsl:if test="name(self::node()[position()])='transitDepot'">
                                               <xsl:value-of select="depotCode" />
                                               <br />
                                           </xsl:if> 
                                           <xsl:if test="name(self::node()[position()])='actionDepot'">
                                               <xsl:value-of select="depotCode" />
                                               <xsl:text>-</xsl:text>
                                               <xsl:value-of select="actionDayOfWeek" />
                                               <br />
                                           </xsl:if> 
                                           <xsl:if test="name(self::node()[position()])='sortDepot'">
                                               <xsl:value-of select="depotCode" />
                                               <xsl:text>-</xsl:text>
                                               <xsl:value-of select="sortCellIndicator" />
                                               <br />
                                           </xsl:if> 
                                       </xsl:for-each>
                                   </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height:40px;"> 
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="border-bottom: 0.1 dashed #000000; height:20px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="width:20%" valign="middle"><span id="sortHeader">Sort </span></td>
                                            <td style="width:80%" valign="middle">
                                                <span id="sortDetail">
                                                    <xsl:value-of select="../consignmentLabelData/transitDepots/sortDepot/depotCode" />
                                                    <xsl:text> </xsl:text>
                                                </span>
                                            </td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr> 
                            <tr>
                                <td valign="top" style="height:20px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="width:20%" valign="middle"><span id="destinationDepotHeader">Dest<br/>Depot</span></td>
                                            <td style="width:80%" valign="middle">
                                                <!--Destination Depot-->
                                                <span id="destinationDepotDetail"> 
                                                    <xsl:choose> 
                                                      <xsl:when test="../consignmentLabelData/destinationDepot/dueDayOfWeek/@renderInstructions='highlighted'">
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/depotCode"/>
                                                        <xsl:text>-</xsl:text>
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/dueDayOfMonth"/>
                                                      </xsl:when>
                                                      <xsl:otherwise>
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/depotCode"/>
                                                        <xsl:text>-</xsl:text>
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/dueDayOfMonth"/>
                                                      </xsl:otherwise>   
                                                    </xsl:choose>
                                                </span> <br/>
                                            </td>
                                        </tr>
                                    </table> 
                                </td> 
                            </tr>
                        </table> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center;" bgcolor="#ffffff">
            <table width="100%" cellspacing="1" cellpadding="0" border="0">
                <tr>
                    <td style="text-align:center;">
                        <!--Barcode-->
                        <xsl:variable name="barcode_url" select='"https://express.tnt.com/barbecue/barcode?type=Code128C&amp;height=140&amp;width=2&amp;data="' />
                        <span id="barcode" name="barcode"> <br />
                          <img>
                             <xsl:attribute name="src">
                               <xsl:value-of select="concat($barcode_url,barcode)" />
                             </xsl:attribute> 
                          </img>
                        </span>
                        <br/>
                        <span id="barcodeLabel" style="font-weight:bold">
                            <xsl:value-of select="barcode" />
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>    
</page> 
<br style="page-break-before:always"/>
    
</xsl:template>

<xsl:template name="FormatDate">
    <!-- expected date format 2008 06 16 -->
    <xsl:param name="DateTime" />
    <!-- new date format 20 June 2007 -->
    <xsl:variable name="year">
        <xsl:value-of select="substring-before($DateTime,'-')" />
    </xsl:variable>
    <xsl:variable name="mo-temp">
        <xsl:value-of select="substring-after($DateTime,'-')" />
    </xsl:variable>
    <xsl:variable name="mo">
        <xsl:value-of select="substring-before($mo-temp,'-')" />
    </xsl:variable>
    <xsl:variable name="day">
        <xsl:value-of select="substring-after($mo-temp,'-')" />
    </xsl:variable>

    <xsl:value-of select="$day" />
    <xsl:text> </xsl:text>
    <xsl:choose>
        <xsl:when test="$mo = '1' or $mo = '01'">Jan</xsl:when>
        <xsl:when test="$mo = '2' or $mo = '02'">Feb</xsl:when>
        <xsl:when test="$mo = '3' or $mo = '03'">Mar</xsl:when>
        <xsl:when test="$mo = '4' or $mo = '04'">Apr</xsl:when>
        <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
        <xsl:when test="$mo = '6' or $mo = '06'">Jun</xsl:when>
        <xsl:when test="$mo = '7' or $mo = '07'">Jul</xsl:when>
        <xsl:when test="$mo = '8' or $mo = '08'">Aug</xsl:when>
        <xsl:when test="$mo = '9' or $mo = '09'">Sep</xsl:when>
        <xsl:when test="$mo = '10'">Oct</xsl:when>
        <xsl:when test="$mo = '11'">Nov</xsl:when>
        <xsl:when test="$mo = '12'">Dec</xsl:when>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year" />
</xsl:template>

</xsl:stylesheet>