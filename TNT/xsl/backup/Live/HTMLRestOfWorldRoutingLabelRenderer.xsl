<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="RestOfWorldHtml">  
<table cellspacing="0" cellpadding="0" border="1" style="width:100%;text-align:center;">
    <tr>
        <td colspan="2">
            <table width="100%" cellspacing="1" cellpadding="4" border="0">
                <tr>
                    <td style="text-align:center;border-right:0.1 solid #000000;" width="40%">
                        <!--Logo-->
                        <span id="logo">
                            <img src='TNT_LABEL_IMAGE_PATH/logo_orig.jpg' alt='logo' id="tntLogo" style="text-align: center;height: 56px;"/>
                        </span>
                    </td>
                    <td style="text-align:center;" width="40%">
                        <!--Market & Transport Type-->
                        <span id="marketAndTransportType" style="font-weight: plain;font-size: 18px;text-align: left;">
                          <xsl:value-of select="../consignmentLabelData/marketDisplay"/>
                          <xsl:text>/</xsl:text>
                          <xsl:value-of select="../consignmentLabelData/transportDisplay"/>
                        </span>
                    </td> 
                    <td style="background-color: #000000;color: #000000;" bgcolor="#000000" width="10%">
                        <!--Free Circulation Display--> 
                        <xsl:choose>
                          <xsl:when test="string-length(../consignmentLabelData/freeCirculationDisplay)>0">
                            <span id="freeCirculationIndicator" >
                              <xsl:value-of select="../consignmentLabelData/freeCirculationDisplay"/>
                            </span>
                          </xsl:when>
                          <xsl:otherwise>
                            <span id="freeCirculationIndicator" >
                            <xsl:text> </xsl:text>
                            </span>
                          </xsl:otherwise>
                        </xsl:choose>
                    </td> 
                    <td style="font-weight: bold;font-size: 45px;text-align: center;" width="10%">
                        <!--Sort Split Indicator-->
                        <xsl:choose>
                          <xsl:when test="string-length(../consignmentLabelData/sortSplitText)>0">
                              <span id="sortSplitIndicator">
                                <xsl:value-of select="../consignmentLabelData/sortSplitText" />
                              </span>
                          </xsl:when>
                          <xsl:otherwise>
                              <span id="sortSplitIndicator">
                                  <xsl:text> </xsl:text>
                              </span>
                          </xsl:otherwise>
                        </xsl:choose>
                    </td> 
                </tr>
            </table>
        </td> 
    </tr>
    <tr>
        <td width="50%" >
            <table width="100%" cellspacing="1" cellpadding="2" border="0">
                <tr>
                    <td colspan="2"  style="text-align:left;border-bottom: 1px solid #000000;height:62px;">
                        <!--Consignment Number-->
                        <span id="conNumber">
                          <span id="conNumberHeader" style="font-size: 10px;">Con No: <br/>  </span>
                          <span id="conNumberDetail" style="font-weight: bold;font-size: 16px;line-height: 100%;"> <xsl:value-of select="../consignmentLabelData/consignmentNumber"/></span>
                        </span> <br/><br/>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%" style="text-align:left;">
                                    <!--Pieces-->
                                    <span id="piece">
                                      <span id="pieceHeader">Piece: </span> 
                                      <span id="pieceDetail" style="font-size: 12px;font-weight: bold;"><xsl:value-of select="pieceNumber"/> of <xsl:value-of select="../consignmentLabelData/totalNumberOfPieces"/></span>
                                    </span>  
                                </td>
                                <td width="50%" style="text-align:left;">
                                    <!--Weight-->
                                    <span id="weight">
                                      <span id="weightHeader">Weight: </span>
                                      <xsl:choose>
                                          <xsl:when test="weightDisplay/@renderInstructions='highlighted'">
                                              <span id="weightDetailHighlighted">
                                                  <xsl:value-of select="weightDisplay" />
                                              </span>
                                          </xsl:when>
                                          <xsl:otherwise>
                                              <span id="weightDetail" style="font-size: 12px;font-weight: bold;">
                                                  <xsl:value-of select="weightDisplay" />
                                              </span>
                                          </xsl:otherwise>
                                      </xsl:choose>
                                      <xsl:text> </xsl:text>
                                    </span> 
                                </td>
                            </tr>
                        </table>
                    </td> 
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 1px solid #000000;text-align:left;height:42px;">
                        <!--Customer Reference -->
                        <span id="customerReference">
                            <span id="customerReferenceHeader">Customer Reference: <br/></span>
                            <span id="customerReferenceDetail" style="font-weight:bold"><xsl:value-of select="pieceReference" disable-output-escaping="yes" /></span>
                        </span><br/>
                        <!--Account Number--> 
                        <span id="accountNumber">
                            <span id="accountNumberHeader">S/R Account No: </span>
                            <span id="accountNumberDetail"> <xsl:value-of select="../consignmentLabelData/account/accountNumber" /></span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 0.1 dashed #000000;text-align:left;height:90px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2"><span id="originAddressHeader">Sender Address:</span> </td>
                            </tr>
                            <tr>
                                <td style="width:5%;"> </td>
                                <td style="width:95%;">
                                    <!--Origin Address & Delivery Address-->
                                    <span id="originAddress"> 
                                        <span id="originAddressDetail"> 
                                            <xsl:value-of select="../consignmentLabelData/sender/name" disable-output-escaping="yes"/><br /> 
                                            <xsl:value-of select="../consignmentLabelData/sender/addressLine1" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/sender/addressLine2" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/sender/town" disable-output-escaping="yes"/><xsl:text>  </xsl:text>    
                                            <xsl:value-of select="../consignmentLabelData/sender/postcode"/><br /> 
                                            <xsl:value-of select="../consignmentLabelData/sender/country"/>        
                                        </span>
                                    </span>
                                </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 1px solid #000000;text-align:left;height:90px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2"><span id="deliveryAddressHeader">Delivery Address:</span></td>
                            </tr>
                            <tr>
                                <td style="width:5%;"> </td>
                                <td style="width:95%;">
                                    <span id="deliveryAddress" style="clear:left"> 
                                        <span id="deliveryAddressDetail">
                                            <xsl:value-of select="../consignmentLabelData/delivery/name" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/addressLine1" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/addressLine2" disable-output-escaping="yes"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/town" disable-output-escaping="yes"/><xsl:text>   </xsl:text>    
                                            <xsl:value-of select="../consignmentLabelData/delivery/postcode"/><br />  
                                            <xsl:value-of select="../consignmentLabelData/delivery/country"/>   
                                        </span>
                                    </span> 
                                </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td width="30%" style="border-bottom: 0.1 solid #000000;text-align:left;height:35px;">
                        <!--Postcode/Cluster code-->
                        <span id="postcodeHeader">Postcode / Cluster Code: </span>
                    </td>
                    <td width="70%" bgcolor="#000000" style="border-right: 0.1 solid #000000;border-bottom: 0.1 solid #000000;text-align:left;">
                        <span id="postcode" style="font-size: 15px;font-weight: bold;color:#FFFFFF;">
                            <xsl:choose>
                            <!--If the length of the Cluster code is greater than 3 then the post code is being displayed
                            instead, so different rendering applies-->
                              <xsl:when test="string-length(../consignmentLabelData/clusterCode)>3">
                                <span id="postcodeDetail"><xsl:value-of select="../consignmentLabelData/delivery/postcode"/></span>
                              </xsl:when>
                              <xsl:otherwise>
                                <span id="clustercodeDetail"><xsl:value-of select="../consignmentLabelData/clusterCode"/></span>
                              </xsl:otherwise>
                            </xsl:choose>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%" style="text-align:left;">
            <table width="100%" cellspacing="1" cellpadding="2" border="0">
                <tr>
                    <td bgcolor="#000000" style="color:#ffffff;border-bottom: 0.1 solid #000000;height:63px;">
                        <!--Service-->
                        <span id="service">
                          <span id="serviceHeader" style="font-size: 10px;">Service: <br/>  </span>
                          <xsl:choose>
                              <xsl:when test="string-length(../consignmentLabelData/product)>15">
                                  <span id="serviceDetail" style="font-size: 15px;">
                                       <xsl:value-of select="../consignmentLabelData/product" />
                                  </span>
                              </xsl:when>
                              <xsl:otherwise>
                                  <span id="serviceDetail" style="font-size: 15px;">
                                       <xsl:value-of select="../consignmentLabelData/product" />
                                  </span>
                              </xsl:otherwise>
                          </xsl:choose>
                        </span><br/>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 0.1 dashed #000000;height:32px;">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="width:15%" valign="top">
                                    <!--Origin Depot & Pickup Date--> 
                                    <span id="originDepotHeader">Origin: </span> 
                                    <br/>
                                </td>
                                <td style="width:40%;text-align:left;" valign="bottom">
                                    <span id="originDepotDetail" style="font-size: 22px;font-weight: bold;"> 
                                        <xsl:value-of select="../consignmentLabelData/originDepot/depotCode" />
                                    </span><br/>
                                </td>
                                <td style="width:45%">
                                    <span id="pickupDate">
                                        <span id="pickupDateHeader">Pickup Date: </span>  
                                    </span><br/>  
                                    <span id="pickupDateDetail">
                                        <xsl:call-template name="FormatDate">    
                                            <xsl:with-param name="DateTime" select="../consignmentLabelData/collectionDate"/> 
                                        </xsl:call-template>    
                                    </span><br/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 0.1 dashed #000000;height:122px;">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="width:20%;text-align:left;" valign="top">
                                    <span id="routingHeader">Routing</span>
                                </td>
                                <td style="width:80%;text-align:left;" valign="top">
                                    <span id="routingDetail" style="font-size: 30px;font-weight: bold;line-height:31px;"> 
                                        <!-- Check if route includes any transit depots-->
                                        <xsl:if test="count(../consignmentLabelData/transitDepots/*)=0">
                                            <xsl:text> </xsl:text>
                                        </xsl:if> 
                                       <xsl:for-each select="../consignmentLabelData/transitDepots/*"> 
                                           <xsl:if test="name(self::node()[position()])='transitDepot'">
                                               <xsl:value-of select="depotCode" />
                                               <br />
                                           </xsl:if> 
                                           <xsl:if test="name(self::node()[position()])='actionDepot'">
                                               <xsl:value-of select="depotCode" />
                                               <xsl:text>-</xsl:text>
                                               <xsl:value-of select="actionDayOfWeek" />
                                               <br />
                                           </xsl:if> 
                                           <xsl:if test="name(self::node()[position()])='sortDepot'">
                                               <xsl:value-of select="depotCode" />
                                               <xsl:text>-</xsl:text>
                                               <xsl:value-of select="sortCellIndicator" />
                                               <br />
                                           </xsl:if> 
                                       </xsl:for-each>
                                   </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td> 
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="border-bottom: 0.1 dashed #000000; height:32px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="width:20%" valign="top"><span id="sortHeader">Sort: </span></td>
                                            <td style="width:80%">
                                                <span id="sortDetail" style="font-size: 22px;font-weight: bold;">
                                                    <xsl:value-of select="../consignmentLabelData/transitDepots/sortDepot/depotCode" />
                                                    <xsl:text> </xsl:text>
                                                </span>
                                            </td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr> 
                            <tr>
                                <td valign="top" style="height:31px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="width:20%" valign="top"><span id="destinationDepotHeader">Dest Depot: </span></td>
                                            <td style="width:80%">
                                                <!--Destination Depot-->
                                                <span id="destinationDepotDetail" style="font-size: 22px;font-weight: bold;"> 
                                                    <xsl:choose> 
                                                      <xsl:when test="../consignmentLabelData/destinationDepot/dueDayOfWeek/@renderInstructions='highlighted'">
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/depotCode"/>
                                                        <xsl:text>-</xsl:text>
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/dueDayOfMonth"/>
                                                      </xsl:when>
                                                      <xsl:otherwise>
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/depotCode"/>
                                                        <xsl:text>-</xsl:text>
                                                        <xsl:value-of select="../consignmentLabelData/destinationDepot/dueDayOfMonth"/>
                                                      </xsl:otherwise>   
                                                    </xsl:choose>
                                                </span> <br/>
                                            </td>
                                        </tr>
                                    </table> 
                                </td> 
                            </tr>
                        </table> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align:center;">
            <table width="100%" cellspacing="1" cellpadding="0" border="0">
                <tr>
                    <td style="text-align:center;">
                        <!--Barcode-->
                        <xsl:variable name="barcode_url" select='"https://express.tnt.com/barbecue/barcode?type=Code128C&amp;height=140&amp;width=2&amp;data="' />
                        <span id="barcode" name="barcode"> <br />
                          <img>
                             <xsl:attribute name="src">
                               <xsl:value-of select="concat($barcode_url,barcode)" />
                             </xsl:attribute> 
                          </img>
                        </span>
                        <br/>
                        <span id="barcodeLabel" style="font-weight:bold">
                            <xsl:value-of select="barcode" />
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>     
<br style="page-break-before:always"/>
    
</xsl:template>

<xsl:template name="FormatDate">
    <!-- expected date format 2008 06 16 -->
    <xsl:param name="DateTime" />
    <!-- new date format 20 June 2007 -->
    <xsl:variable name="year">
        <xsl:value-of select="substring-before($DateTime,'-')" />
    </xsl:variable>
    <xsl:variable name="mo-temp">
        <xsl:value-of select="substring-after($DateTime,'-')" />
    </xsl:variable>
    <xsl:variable name="mo">
        <xsl:value-of select="substring-before($mo-temp,'-')" />
    </xsl:variable>
    <xsl:variable name="day">
        <xsl:value-of select="substring-after($mo-temp,'-')" />
    </xsl:variable>

    <xsl:value-of select="$day" />
    <xsl:text> </xsl:text>
    <xsl:choose>
        <xsl:when test="$mo = '1' or $mo = '01'">Jan</xsl:when>
        <xsl:when test="$mo = '2' or $mo = '02'">Feb</xsl:when>
        <xsl:when test="$mo = '3' or $mo = '03'">Mar</xsl:when>
        <xsl:when test="$mo = '4' or $mo = '04'">Apr</xsl:when>
        <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
        <xsl:when test="$mo = '6' or $mo = '06'">Jun</xsl:when>
        <xsl:when test="$mo = '7' or $mo = '07'">Jul</xsl:when>
        <xsl:when test="$mo = '8' or $mo = '08'">Aug</xsl:when>
        <xsl:when test="$mo = '9' or $mo = '09'">Sep</xsl:when>
        <xsl:when test="$mo = '10'">Oct</xsl:when>
        <xsl:when test="$mo = '11'">Nov</xsl:when>
        <xsl:when test="$mo = '12'">Dec</xsl:when>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year" />
</xsl:template>

</xsl:stylesheet>