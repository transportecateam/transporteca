<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


	<xsl:variable name="absoluteResourcesDir" select="'http://iconnection.tnt.com:81/Shipper/NewStyleSheets'"/>
	<xsl:variable name="hostName" select="'https://my.tnt.com/'"/>

	<xsl:template match="/">
		<html>
			<head>

				<script type="text/javascript">
					var firstPagePrinted = false; 
					function includePageBreak() 
                                        { 
                                            if (firstPagePrinted) 
                                            {
                                                document.writeln('<div class="pagebreak">');
                                                document.writeln('<div size="1" color="#FFFFFF">.</div>');
                                                document.writeln('</div>');
                                            } 
                                            else 
                                            {
                                                firstPagePrinted = true;
                                            } 
					} 
				</script>

				<style> 
				
				/* International Styles /* 

				div.header { color : black; background-color : white; font-weight : bold; font-family : arial, helvetica "sans-serif"; font-size : 8pt; } 

				div.data { color : black; background-color : white; font-family : arial , "sans-serif"; font-size : 8pt; }

				div.sender { color : black; background-color : white; font-family : arial , "sans-serif"; font-size : 6pt; }

				div.smallprint { color : black; background-color : white; font-family : arial, "sans-serif"; font-size : 8pt; }

				div.customs { color : black; background-color : white; font-weight : bold; font-family : arial, "sans-serif"; font-size : 12pt; } 

				div.received { color : black; background-color : white; font-family : arial, , "sans-serif"; font-size : 8pt; }
                                span.header { color : black; background-color : white; font-weight : bold; font-family : arial, helvetica "sans-serif"; font-size : 8pt;} 
                                    
				
				/* UK Domestic Styles */
				.senderAddress{ line-height: 2mm; }

				.deliveryAddress{ line-height: 3mm; }
				
				.table1{  height: 25mm;	 }

				.table1td1{  width: 56mm; height: 25mm; border-right: 1px solid #000000; }

				.table1td1table{ width: 56mm; height: 25mm; }

				.table1td2{ height: 17mm; border-bottom: 1px solid #000000; }

				.table1td3{ height: 8mm; }

				.table2td1{  width: 64mm; border-right: 1px solid #000000; }

				.table2td1table{  width: 64mm; }

				.table3{ height: 8mm; }

				.table3td1{ width: 84mm; height: 8mm; border-right: 1px solid #000000; }

				.table3td2{  height: 8mm; }

				.table4td1{  width: 67mm; border-right: 1px solid #000000; }

				.table4td2{  width: 31mm; border-right: 1px solid #000000;  }


				div.addressHeader  {  color : black;  font-weight : bold;  font-family :  "arial";  font-size : 6pt;   } 

				div.addressHeaderCode  {  color : black;  font-weight : bold;  font-family :  "arial";  font-size : 8pt;  letter-spacing: 0.2cm   } 

				div.barcode  {  color : black;  font-weight : bold;  font-family :  "arial";  font-size : 8pt;  letter-spacing: 0.1cm   } 

				div.addressHeaderRec  {  color : black;  font-weight : bold;  font-family : "arial";  font-size : 6pt;   } 

				div.addressData  {  color : black;  font-family : "courier new";  font-size : 8pt;  }

				div.addressDataRec  {  color : black;  font-weight : bold;  font-family : "courier new";  } 

				div.addressDataWeight  {  color : black;  font-weight : bold;  font-family : "courier new";  font-size : 11pt;  }

				div.addressSmallPrint  {  font-family : "courier new";  font-size : 4pt;  }

				div.addressSmallPrintLink  {  font-family : "courier new";  font-size : 5pt;  }

				div.header {  color : black;  font-weight : bold;  font-family : arial, helvetica "sans-serif";  font-size : 8pt;   } 

				div.invoiceHeader {  color : black;  background-color : white;  font-weight : bold;  font-family : arial, helvetica "sans-serif";  font-size : 10pt;   } 

				div.data {  color : black;  font-family : arial , "sans-serif";  font-size : 8pt;  }

				div.smallprint {  color : black;  background-color : white;  font-family : arial, "sans-serif";  font-size : 6pt;  }

				div.smallprintlink {  color : black;  background-color : white;  font-family : arial, "sans-serif";  font-size : 7pt;  }

				div.auSmallPrint {  color : black;  background-color : white;  font-family : arial, "sans-serif";  font-size : 7pt;  }

				div.auSmallPrintLink {  color : black;  background-color : white;  font-family : arial, "sans-serif";  font-size : 7pt;   text-decoration:underline;  }

				div.carrierLicence  {  color : black;  font-family : "courier new";  font-size : 7pt;  } 

				.normalService {  font-size: x-large;  }

				.premiumService {   font-size: xx-large;  font-weight: bold;  }

				.tntTelephone {  font-size: small;  }

				.deliveryDepot {  font-size: 96px;  }

				.data {  }

				.dataBold {  font-weight: bold;  }

				.label {  }

				.deliveryPostcode {  font-size: xx-large;  font-weight: bold;  }

				table.outLine {  border: 1px solid #656566;  border-collapse : collapse;  padding : 1px;  background-color: #FFFFFF;  }

				td.outLineCell {  border: 1px solid #656566;  } 

				div.pagebreak {  page-break-before : always;  }

				div.sm-data {   color : black;   background-color : white;   font-family : arial, helvetica "sans-serif";   font-size : 6pt;  }

				div.sm-field {   color : black;   background-color : white;   font-weight : bold;   font-family : arial, helvetica "sans-serif";   font-size : 6pt;  }

				div.sm-title {   color : black;   background-color : white;   font-weight : bold;   font-family : arial, helvetica "sans-serif";   font-size : 9pt;   text-decoration:underline;  }

				div.serviceCode {    color : black;  font-weight : bold;  font-family : arial;  font-size : 18pt;   }

				div.operationsFlow {    color : black;  font-weight : bold;  font-family : arial;  font-size : 24pt;   }

				div.productCode {    color : black;  font-weight : bold;  font-family : arial;  font-size : 12pt;   }  

				div.postProduct {    color : black;  font-family : arial;  font-size : 10pt;   }  

				div.domestic {    color : black;  font-weight : bold;  font-family : arial;  font-size : 16pt;   } 
                                table.left_table td { border: 0.1 solid #000000; } 
                                table.right_table td { border: 0.1 solid #000000; } 
                                hr.solid-line {width:100%;line-height:2px;margin-top:5px;margin-bottom:5px;}
				</style>
			</head>
			<body>

				<xsl:for-each select="/CONSIGNMENTBATCH/CONSIGNMENT"> 
					<xsl:choose>
						<xsl:when test="@marketType='DOMESTIC'">
							<xsl:choose>
								<xsl:when test="@originCountry='GB'">
									<xsl:call-template name="UKDomesticConNote">
										<xsl:with-param name="copyFor">C</xsl:with-param>
										<xsl:with-param name="returns">false</xsl:with-param>
									</xsl:call-template>

									<xsl:call-template name="UKDomesticConNote">
										<xsl:with-param name="copyFor">P</xsl:with-param>
										<xsl:with-param name="returns">false</xsl:with-param>
									</xsl:call-template>
								</xsl:when>

								<xsl:otherwise>

									<xsl:call-template name="InternationalTemplate">
										<xsl:with-param name="whosCopy" select="'C'"/>
									</xsl:call-template>

									<xsl:call-template name="InternationalTemplate">
										<xsl:with-param name="whosCopy" select="'R'"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>

						<xsl:otherwise>
							<xsl:call-template name="InternationalTemplate">
								<xsl:with-param name="whosCopy" select="'C'"/>
							</xsl:call-template>

							<xsl:call-template name="InternationalTemplate">
								<xsl:with-param name="whosCopy" select="'R'"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template> 

	<xsl:template name="InternationalTemplate"> 
            <xsl:param name="whosCopy"/>   
		<table cellpadding="0" cellspacing="0" style="width:100%;">
                    <tr>
                        <!-- left table column start -->
                        <td valign="top" style="width:50%;height:100%;">
                            <table cellpadding="5" cellspacing="0" style="width:100%;" class="left_table">
                                <!-- start of 1. From --> 
                                <xsl:choose>
                                    <xsl:when test="HEADER/COLLECTION/COMPANYNAME/text()">
                                        <tr>
                                            <td colspan="4" style="width:100%;line-height:10px;">
                                                <span class="header">1.From (Collection Address)</span>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="4">
                                                <table width="100%" height="44">
                                                    <tr>
                                                        <td colspan="2">
                                                            <div class="data">Sender's Account No : <xsl:value-of select="HEADER/SENDER/ACCOUNT"/><br/>Name : <xsl:value-of select="HEADER/COLLECTION/COMPANYNAME"/><br/>Address : <xsl:value-of select="HEADER/COLLECTION/STREETADDRESS1"/><br/><xsl:if test="HEADER/COLLECTION/STREETADDRESS2/text()"><xsl:value-of select="HEADER/COLLECTION/STREETADDRESS2"/></xsl:if><xsl:if test="HEADER/COLLECTION/STREETADDRESS3/text()"><xsl:value-of select="HEADER/COLLECTION/STREETADDRESS3"/></xsl:if></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-right-color:white;">
                                                            <div class="data">City : <xsl:value-of select="HEADER/COLLECTION/CITY"/><br/>Province : <xsl:value-of select="HEADER/COLLECTION/PROVINCE"/><br/>Contact Name : <xsl:value-of select="HEADER/COLLECTION/CONTACTNAME"/></div>
                                                        </td> 
                                                        <td style="border-left-color:white;">
                                                            <div class="data">Postal/Zip Code : <xsl:value-of select="HEADER/COLLECTION/POSTCODE"/><br/>Country : <xsl:value-of select="HEADER/COLLECTION/COUNTRY"/><br/>Tel No : <xsl:value-of select="HEADER/COLLECTION/CONTACTDIALCODE"/>&#160;<xsl:value-of select="HEADER/COLLECTION/CONTACTTELEPHONE"/></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td> 
                                        </tr>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <tr>
                                            <td colspan="4" style="width:100%;line-height:10px;">
                                                <span class="header">1.From (Collection Address)</span>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="4" height="50">
                                                <table width="100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <div class="data">Sender's Account No : <xsl:value-of select="HEADER/SENDER/ACCOUNT"/><br/>Name : <xsl:value-of select="HEADER/SENDER/COMPANYNAME"/><br/>Address : <xsl:value-of select="HEADER/SENDER/STREETADDRESS1"/><br/><xsl:if test="HEADER/SENDER/STREETADDRESS2/text()"><xsl:value-of select="HEADER/SENDER/STREETADDRESS2"/><br/></xsl:if><xsl:if test="HEADER/SENDER/STREETADDRESS3/text()"><xsl:value-of select="HEADER/SENDER/STREETADDRESS3"/></xsl:if></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-right-color:white;">
                                                            <div class="data">City : <xsl:value-of select="HEADER/SENDER/CITY"/><br/>Province : <xsl:value-of select="HEADER/SENDER/PROVINCE"/><br/>Contact Name : <xsl:value-of select="HEADER/SENDER/CONTACTNAME"/></div>
                                                        </td> 
                                                        <td style="border-left-color:white;">
                                                            <div class="data">Postal/Zip Code : <xsl:value-of select="HEADER/SENDER/POSTCODE"/><br/>Country : <xsl:value-of select="HEADER/SENDER/COUNTRY"/><br/>Tel No : <xsl:value-of select="HEADER/SENDER/CONTACTDIALCODE"/>&#160;<xsl:value-of select="HEADER/SENDER/CONTACTTELEPHONE"/></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td> 
                                        </tr> 
                                    </xsl:otherwise>
                                </xsl:choose> 
                                    <tr> 
                                        <td style="line-height: 10px;" colspan="4">
                                            <span class="header">2.To (Receiver Address)</span>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="4">
                                            <table width="100%">
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="data">Name : <xsl:value-of select="RECEIVER/COMPANYNAME"/><br/>Address : <xsl:value-of select="RECEIVER/STREETADDRESS1"/><xsl:if test="RECEIVER/STREETADDRESS2/text()"><xsl:value-of select="RECEIVER/STREETADDRESS2"/></xsl:if><xsl:if test="RECEIVER/STREETADDRESS3/text()"><xsl:value-of select="RECEIVER/STREETADDRESS3"/></xsl:if></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="data">City : <xsl:value-of select="RECEIVER/CITY"/><br/>Province : <xsl:value-of select="RECEIVER/PROVINCE"/><br/>Contact Name : <xsl:value-of select="RECEIVER/CONTACTNAME"/></div>
                                                    </td> 
                                                    <td>
                                                        <div class="data">Postal/Zip Code : <xsl:value-of select="RECEIVER/POSTCODE"/><br/>Country : <xsl:value-of select="RECEIVER/COUNTRY"/><br/>Tel No : <xsl:value-of select="RECEIVER/CONTACTDIALCODE"/>&#160;<xsl:value-of select="RECEIVER/CONTACTTELEPHONE"/></div>
                                                    </td>
                                                </tr>
                                            </table> 
                                        </td>
                                    </tr> 
                                    <!-- start of 3. Goods -->
                                    <tr>
                                        <td colspan="4">
                                            <div class="header">3.Goods</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="data">General Description :<br/><xsl:value-of select="GOODSDESC1"/><br/><xsl:if test="GOODSDESC2/text()"><xsl:value-of select="GOODSDESC2"/></xsl:if><xsl:if test="GOODSDESC3/text()"><xsl:value-of select="GOODSDESC3"/></xsl:if></div>
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <div class="data">Stat No :</div>
                                                    </td>
                                                    <td colspan="3">
                                                        <div class="data"><xsl:value-of select="STATCODE"/></div>
                                                    </td> 
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        <div class="data">Total Packages:</div>
                                                    </td>
                                                    <td>
                                                        <div class="data">Total Weight:</div>
                                                    </td>
                                                    <td>
                                                        <div class="data">Total Volume:</div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="data"><xsl:value-of select="TOTALITEMS"/></div>
                                                    </td>
                                                    <td>
                                                        <div class="data"><xsl:value-of select="TOTALWEIGHT"/>&#160;<xsl:value-of select="TOTALWEIGHT/@units"/></div>
                                                    </td> 
                                                    <td>
                                                        <div class="data"><xsl:value-of select="TOTALVOLUME"/>&#160;<xsl:value-of select="TOTALVOLUME/@units"/></div>
                                                    </td> 
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td colspan="4">
                                            <div class="header">4. Services</div> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="data">Service : <xsl:value-of select="SERVICE"/><br/>Options : <xsl:value-of select="OPTION1"/><br/><xsl:if test="OPTION2/text()"><xsl:value-of select="OPTION2"/>&#160;</xsl:if><xsl:if test="OPTION3/text()"><xsl:value-of select="OPTION3"/></xsl:if></div>
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr> 
                                                    <td>
                                                        <div class="sender">
                                                           <xsl:if test="PAYMENTIND = 'S'">Sender Pays</xsl:if>
                                                           <xsl:if test="PAYMENTIND='R'">Receiver Pays</xsl:if>
                                                        </div>
                                                    </td>
                                               </tr>
                                               <tr>
                                                    <td>
                                                        <div class="data">Insurance Currency : <xsl:value-of select="INSURANCECURRENCY"/></div>
                                                    </td>
                                                    <td>
                                                        <div class="data">Value : <xsl:value-of select="INSURANCEVALUE"/></div>
                                                    </td>
                                               </tr> 
                                            </table>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="4" style="line-height:12px;">
                                            <div class="received"><br/>Sender's Signature : <br/>______________________________</div>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="4" style="line-height:12px;">
                                            <div class="received"><br/>Date : ____/____/____</div>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="4">
                                            <div class="smallprint"><br/>LIABILITY FOR LOSS, DAMAGE AND DELAY IS LIMITED BY THE CMR CONVENTION OR THE WARSAW CONVENTION WHICHEVER IS APPLICABLE.THE SENDER AGREES THAT THE GENERAL CONDITIONS, ACCESSIBLE VIA THE HELP TEXT THAT ARE ACCEPTABLE AND GOVERN THIS CONTRACT.IF NO SERVICE OR BILLING OPTION IS SELECTED THE FASTEST AVAILABLE SERVICE WILL BE CHARGED TO THE SENDER</div>
                                        </td>
                                    </tr>
                                </table>
                            </td> 
                            <!-- left table column end--> 
                            <!-- right table column start-->
                            <td valign="top" style="width:50%;">
                                <table cellpadding="5" cellspacing="0" style="width:100%;" class="right_table">
                                    <!-- start of TNT logo -->
                                    <tr>
                                        <td colspan="2" align="center">
                                            TNT_LOGO_IMAGE_TAG<br/> 
                                            TNT_BARCODE_IMAGE_TAG
                                            <div class="data">*<xsl:value-of select="CONNUMBER"/>*<br/>Please quote this number if you have an enquiry.</div>
                                        </td>
                                    </tr> 
                                    <!-- start of A. Delivery -->
                                    <tr>
                                        <td colspan="2">
                                            <div class="header">A. Delivery Address</div> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr> 
                                                    <td colspan="2"><div class="data">Name : <xsl:value-of select="DELIVERY/COMPANYNAME"/><br/>Address : <xsl:value-of select="DELIVERY/STREETADDRESS1"/><br/><xsl:if test="DELIVERY/STREETADDRESS2/text()"><xsl:value-of select="DELIVERY/STREETADDRESS2"/><br/></xsl:if><xsl:if test="DELIVERY/STREETADDRESS3/text()"><xsl:value-of select="DELIVERY/STREETADDRESS3"/></xsl:if></div></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="1">
                                                        <div class="data">City : <xsl:value-of select="DELIVERY/CITY"/><br/>Province : <xsl:value-of select="DELIVERY/PROVINCE"/>	<br/>Contact Name : <xsl:value-of select="DELIVERY/CONTACTNAME"/></div>
                                                    </td> 
                                                    <td colspan="1">
                                                        <div class="data">Postal/Zip Code : <xsl:value-of select="DELIVERY/POSTCODE"/><br/>Country : <xsl:value-of select="DELIVERY/COUNTRY"/><br/>Tel No : <xsl:value-of select="DELIVERY/CONTACTDIALCODE"/>&#160;<xsl:value-of select="DELIVERY/CONTACTTELEPHONE"/></div>
                                                    </td>
                                                </tr> 
                                            </table>
                                        </td>
                                    </tr>   
                                    <!-- start of B. Dutible Shipment Details -->
                                    <tr>
                                        <td colspan="4">
                                            <div class="header">B. Dutiable Shipment Details</div> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr> 
                                                    <td colspan="2"><div class="data">Receivers VAT/TVA/BTW/MWST No. : <xsl:value-of select="RECEIVER/VAT"/><br/>Invoice Value of Dutiables</div><br/></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="1">
                                                        <div class="data">Currency:<xsl:value-of select="CURRENCY"/></div>
                                                    </td>
                                                    <td colspan="1">
                                                        <div class="data">Value:<xsl:value-of select="GOODSVALUE"/></div>
                                                    </td>
                                                </tr> 
                                            </table> 
                                        </td>
                                    </tr>  
                                    <!-- start of C. Special Delivery Instructions -->
                                    <tr>
                                        <td colspan="2">
                                            <div class="header">C. Special Delivery Instructions</div> 
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="2">
                                            <div class="data">
                                                <xsl:value-of select="DELIVERYINST"/>
                                            </div>
                                            <!-- start of D. Customer Reference -->
                                            <div class="header">D. Customer Reference</div> 
                                            <div class="data">
                                                <xsl:value-of select="CUSTOMERREF"/>
                                            </div>
                                            <!-- start of E. Invoice DELIVERY (DELIVERY's Account Number) -->
                                            <div class="header">E. Invoice Receiver (Receiver's Account Number)</div> 
                                            <div class="data">
                                                <xsl:value-of select="RECEIVER/ACCOUNT"/>
                                            </div> 
                                            <div class="received">Received by TNT <br/>by (Name) : _____________________________________                                            </div>
                                            <br/>
                                            <div class="received">Date : ____/____/____ &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Time : ____:____</div>

                                            <div class="customs"> 
                                                <xsl:choose>
                                                    <xsl:when test="$whosCopy = 'C'">Custom&apos;s Copy</xsl:when>
                                                    <xsl:when test="$whosCopy = 'R'">Receiver&apos;s Copy</xsl:when>
                                                </xsl:choose>
                                            </div>
                                            Please Keep For Reference
                                        </td>
                                    </tr> 
                                </table>
                            </td>
			</tr> 
			<!--horizontal line at bottom of table -->

			<!--end of main table custom's copy-->
		</table>
                <div class="pagebreak"></div>
	</xsl:template>


	<xsl:template name="UKDomesticConNote">
		<xsl:param name="copyFor"/>
		<xsl:param name="returns"/>
		 
		 
			<script>includePageBreak();</script>
		 

		<table bgcolor="#ff6600" border="0" cellspacing="1" cellpadding="0" width="600">
			<tr>
				<td valign="top" width="50%">
					<!-- LEFT HAND COLUMN -->
					<table width="100%" cellpadding="0" cellspacing="1" border="0" bordercolor="#33FF33">
						<tr>
							<td>
								<table width="100%" class="outLine">
									<tr>
										<td nowrap="nowrap">
											<img src="{$absoluteResourcesDir}/images/lg_tnt_uk_sml.gif" width="52" height="20"/>
											<br class=""/>
											<div class="smallprint">TNT EXPRESS HOUSE, HOLLY LANE,<br class=""/>
                  ATHERSTONE, WARWICKSHIRE CV9 2RY <br class=""/>
                  TELEPHONE : 01827 303030</div>
										</td>
										<td nowrap="nowrap">
											<div class="header">EXPRESS</div>
											<br class=""/>
											<xsl:choose>
												<xsl:when test="$copyFor = 'C'">
													<div class="data">Customer Copy</div>
												</xsl:when>
												<xsl:otherwise>
													<div class="data">Pricing Copy</div>
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td nowrap="nowrap" class="outLineCell" align="center">
											<div class="data">Div</div>
											<br class=""/>
											<div class="dataBold">010</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" class="outLine" style="background-color: #FDBA8D">
									<tr>
										<td width="65%">
											<div class="header">SENDER&apos;S ACCOUNT NUMBER</div>
										</td>
										<td width="15%">
											<div class="data">
												<xsl:value-of select="HEADER/SENDER/ACCOUNT"/>
											</div>
										</td>
										<td width="20%">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td>
								<div class="header">FROM (Sender)</div>
							</td>
						</tr>
						<tr>
							<td>
								<xsl:choose>
									<xsl:when test="$returns != 'true'">
										<xsl:choose>

											<xsl:when test="HEADER/COLLECTION/COMPANYNAME/text()">
												<xsl:call-template name="ukDomesticCollectionAddress"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="ukDomesticSenderAddress"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<!-- Reverse the Collection/delivery and Sender/Receiver addresses for a returns consignment -->
										<xsl:choose>

											<xsl:when test="DELIVERY/COMPANYNAME/text()">
												<xsl:call-template name="UKDomesticDeliveryAddress"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="UKDomesticReceiverAddress"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" class="outLine" style="background-color: #FDBA8D">
									<tr>
										<td>
											<div class="header">CUSTOMER REFERENCE</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="data">
												<xsl:value-of select="substring(CUSTOMERREF, 1, 15)"/>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td height="18" valign="bottom">
								<div class="header">TO (Receiver)</div>
							</td>
						</tr>
						<tr>
							<td>
								<xsl:choose>
									<xsl:when test="$returns != 'true'">
										<xsl:choose>

											<xsl:when test="DELIVERY/COMPANYNAME/text()">
												<xsl:call-template name="UKDomesticDeliveryAddress"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="UKDomesticReceiverAddress"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="UKDomesticReceiverAddress"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td>

								<table height="67" class="outLine" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<div class="header">SENDER&apos;S SIGNATURE</div>
										</td>
										<td>
											<div class="header">(Please Print)</div>
										</td>
									</tr>
									<tr>
										<td>
											<img height="30" width="1" src="{$absoluteResourcesDir}/images/1x1.gif"/>
										</td>
									</tr>

									<tr>
										<td>
											<div class="header">Date</div>
											<div class="smallPrint">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;/&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;/&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</div>
											<div class="smallprint">(Day/Month/Year)</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END OF LEFT HAND COLUMN -->
				</td>
				<td valign="top" width="50%">
					<!-- RIGHT HAND COLUMN -->
					<table cellspacing="1" cellpadding="0" width="100%">
						<tr>
							<td>
								<table height="85" cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="outLineCell" bgcolor="#FFFFFF" align="center">
											<div class="header">CONSIGNMENT NOTE NUMBER<br class=""/>

												<img src="{$hostName}/barcode/barbecue?data={CONNUMBER}&amp;type=Code128&amp;height=70" width="160"/>
												<br class=""/>
												<xsl:value-of select="CONNUMBER"/>
											</div>
										</td>
										<td>
											<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="30"/>
										</td>
										<td class="outLineCell" bgcolor="#FFFFFF" align="center">
											<img src="{$absoluteResourcesDir}/images/lg_tnt_uk_sml.gif" width="52" height="20"/>
											<br class=""/>
											<div class="smallprint">BOOK COLLECTIONS<br class=""/>ONLINE<br class=""/></div>
											<div class="smallprint">www.tnt.co.uk<br class=""/></div>
											<div class="smallprint">OR CALL CUSTOMER<br class=""/>SERVICES ON<br class=""/>0800 100 600</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td align="center" class="outLineCell" bgcolor="#FDBA8D">
								<div class="header">UK 
        DELIVERY OPTIONS</div>
							</td>
						</tr>
						<tr>
							<td>
								<table class="outLine" width="100%">
									<tr>
										<td>
											<xsl:choose>
 
												<xsl:when test="HEADER/CARRIAGEFORWARD='Y'">
													<div class="header">Service (Carriage Forward):</div>
												</xsl:when>
												<xsl:otherwise>
													<div class="header">Service:&#160;</div>
												</xsl:otherwise>
											</xsl:choose>

											<div class="data">
												<xsl:value-of select="SERVICE"/>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="data">
												<xsl:if test="OPTION1/text()">
													<xsl:value-of select="OPTION1"/>
												</xsl:if>
												<xsl:if test="OPTION2/text()">
													<br class=""/>
													<xsl:value-of select="OPTION2"/>
												</xsl:if>
												<xsl:if test="OPTION3/text()">
													<br class=""/>
													<xsl:value-of select="OPTION3"/>
												</xsl:if>
												<xsl:if test="OPTION4/text()">
													<br class=""/>
													<xsl:value-of select="OPTION4"/>
												</xsl:if>


												<xsl:if test="../Header/DangerousGoodsOption/Code/text()">
													<br class=""/>
													<xsl:value-of select="../Header/DangerousGoodsOption/Code"/>
													<xsl:value-of select="../Header/DangerousGoodsOption/Name"/>
												</xsl:if>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td>
								<table class="outLine" width="100%">
									<tr>
										<td>
											<xsl:choose>
												<xsl:when test="UKDOMESTICITLLOPTION">
													<div class="header">IMPORTANT:</div>
													<div class="data">increased transit liability cover is required</div>
												</xsl:when>
												<xsl:otherwise>
													<div class="data">increased transit liability cover is not required</div>
												</xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<xsl:choose>

							<xsl:when test="DANGEROUSGOODS = 'Y'">
								<tr>
									<td>
										<table class="outLine" width="100%">
											<tr>
												<td colspan="2">
													<div class="data">DOES THE CONSIGNMENT CONTAIN <div class="header">DANGEROUS GOODS?</div></div>
												</td>
											</tr>
											<tr>
												<td>
													&#160;<img src="{$absoluteResourcesDir}/images/uk_domestic_dangerous_goods_off.gif" width="14" height="13"/>
													<div class="data">&#160;NO&#160;</div>
													<img src="{$absoluteResourcesDir}/images/uk_domestic_dangerous_goods_on.gif" width="14" height="13"/>
													<div class="data">&#160;YES</div>
												</td>
												<td>
													<div class="data">UN NUMBER:&#160;</div>
													<div class="data">
														<xsl:value-of select="UNNUMBER"/>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" align="center">
													<div class="data">DANGEROUS GOODS DELIVERIES ARE NOT GUARANTEED</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</xsl:when>
							<xsl:otherwise>
								<tr>
									<td>
										<table class="outLine" width="100%">
											<tr>
												<td colspan="2">
													<div class="data">DOES THE CONSIGNMENT CONTAIN <div class="header">DANGEROUS GOODS?</div></div>
												</td>
											</tr>
											<tr>
												<td>
													&#160;<img src="{$absoluteResourcesDir}/images/uk_domestic_dangerous_goods_on.gif" width="14" height="13"/>
													<div class="data">&#160;NO&#160;</div>
													<img src="{$absoluteResourcesDir}/images/uk_domestic_dangerous_goods_off.gif" width="14" height="13"/>
													<div class="data">&#160;YES</div>
												</td>
												<td>
													<div class="data">UN NUMBER:</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" align="center">
													<div class="data">DANGEROUS GOODS DELIVERIES ARE NOT GUARANTEED</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</xsl:otherwise>
						</xsl:choose>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td>
								<table class="outLine" width="100%" border="1" bordercolor="#CCCCCC" cellpadding="0" cellspacing="0">
									<tr>
										<td rowspan="2" width="40%" valign="top">
											<div class="header">Goods Description</div>
										</td>
										<td rowspan="2" width="10%">
											<div class="header">Number of items</div>
										</td>
										<td width="10%" align="center">
											<div class="header">Weight</div>
										</td>
										<td colspan="3" align="center" width="30%">
											<div class="header">Dimensions (cms)</div>
										</td>
									</tr>
									<tr>
										<td align="center">
											<div class="header">Kilos</div>
										</td>
										<td align="center">
											<div class="header">Length</div>
										</td>
										<td align="center">
											<div class="header">Width</div>
										</td>
										<td align="center">
											<div class="header">Height</div>
										</td>
									</tr>
									<xsl:for-each select="PACKAGES/PACKAGE">
										<tr>
											<td>
												<div class="data">
													<xsl:value-of select="GOODSDESC"/>
												</div>
											</td>
											<td>
												<div class="data">
													<xsl:value-of select="ITEMS"/>
												</div>
											</td>
											<td>
												<div class="data">
													<xsl:value-of select="WEIGHT"/>
												</div>
											</td>
											<td>
												<div class="data">
													<xsl:value-of select="number(round(LENGTH * 100))"/>
												</div>
											</td>
											<td>
												<div class="data">
													<xsl:value-of select="number(round(WIDTH * 100))"/>
												</div>
											</td>
											<td>
												<div class="data">
													<xsl:value-of select="number(round(HEIGHT * 100))"/>
												</div>
											</td>
										</tr>
									</xsl:for-each>

									<xsl:choose>
										<xsl:when test="count(PACKAGES/PACKAGE) = 1">
											<xsl:call-template name="blankRow"/>
											<xsl:call-template name="blankRow"/>
										</xsl:when>
										<xsl:when test="count(PACKAGES/PACKAGE) = 2">
											<xsl:call-template name="blankRow"/>
										</xsl:when>

										<xsl:otherwise>
										</xsl:otherwise>
									</xsl:choose>



									<tr>
										<td>
											<div class="header">Total number of items and weight</div>
										</td>
										<td>
											<div class="data">
												<xsl:value-of select="TOTALITEMS"/>
											</div>
										</td>
										<td>
											<div class="data">
												<xsl:value-of select="TOTALWEIGHT"/>
											</div>
										</td>
										<td colspan="3" align="center">
											<div size="-2">Consignment subject to volumetric<br class=""/> measurement</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<img src="{$absoluteResourcesDir}/images/1x1.gif" width="1" height="1"/>
							</td>
						</tr>
						<tr>
							<td height="85" bgcolor="#ffffff" class="outLineCell" style="padding-top: 15px; padding-left: 15px; padding-right: 15px;">
								<!--
								        We display the barcode at half of its original size.
								        This encourages the browser to display it at a smaller
								        size but maintain the accuracy of the image when
								        printing on a printer. There doesn't
								        seem to be a way to tell the browser that this is an
								        image that should not be scaled when printing.
								        
								        Internet Explorer has some weird behaviour when you try
								        to resize to 50% so I've had to specify the exact number
								        of pixels.
								    -->
								<img width="296" src="{$hostName}/barcode/pdf417?BARCODE={PDFBARCODEDATA}"/>
							</td>
						</tr>
						<tr>
							<td>
								<table height="67" class="outLine" width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<div class="header">RECEIVED BY TNT</div>
										</td>
										<td>
											<div class="header">(Please Print)</div>
										</td>
									</tr>
									<tr>
										<td>
											<img height="30" width="1" src="{$absoluteResourcesDir}/images/1x1.gif"/>
										</td>
									</tr>

									<tr>
										<td>
											<div class="header">Date</div>
											<div class="smallPrint">&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;/&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;/&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</div>
											<div class="smallprint">(Day/Month/Year)</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END OF RIGHT HAND COLUMN -->
				</td>
			</tr>
			<tr>
				<td class="outLineCell" colspan="3" bgcolor="#ffffff">
					<div class="smallprint">ALL 
        CONSIGNMENTS ARE CARRIED SUBJECT TO TNT EXPRESS SERVICES CONDITIONS OF 
        CARRIAGE, THE PRINCIPAL CONDITIONS ARE SHOWN BELOW.</div>
				</td>
			</tr>
		</table>
		<p class=""/>
		 
		<table border="0" cellspacing="1" cellpadding="0" width="600">
			<tr>
				<td class="outLineCell">
					<table width="100%" cellspacing="4" cellpadding="1" border="0">
						<tr>
							<td>
								<div class="header">TNT Express Services Principal Conditions 
              of Carriage</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="data">All goods are carried subject to our Conditions 
              of Carriage which are available on request. It is in your interest 
              to read them as your rights may be affected.</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="data">Under these conditions our liability is limited 
              to £15.00 per Kilo up to 1,000 kilos per consignment and is excluded 
              in certain circumstances. For European Road Service carriage of 
              goods is governed by the Convention on the Contract for the International 
              Carriage of Goods by Road 1956 (CMR). Our liability for loss or 
              damage to your goods is limited to 8.33 SDR&apos;s per kilo or £15.00 
              per kilo whichever is the greater.</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="data">We recommend that you arrange sufficient insurance 
              cover to protect your interest.We can arrange increased transit 
              liability cover on your behalf on request.</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="data">We are not responsible for loss to you of any 
              profit, loss of customer, or indirect loss of a monetary or consequential 
              nature.</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="data">Payment for carriage charges is due no later 
              than the 15th day of the month following the month of invoice.</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="data">Next working days do not include Saturdays 
              or public holidays.Our Saturday delivery service is available on 
              days which are not public holidays.</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="data">PLEASE NOTE THE FOLLOWING INFORMATION
									<ul style="margin-top: 1px;">
										<li>Please give the items and the consignment note to the collection 
                  driver.</li>
									</ul>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="blankRow">

		<tr>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
			<td>&#160;</td>
		</tr>
	</xsl:template>


	<xsl:template name="ukDomesticCollectionAddress">

		<table width="100%" class="outLine" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/COMPANYNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/STREETADDRESS1"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/STREETADDRESS2/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/STREETADDRESS2"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/STREETADDRESS3/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/STREETADDRESS3"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/CITY"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/PROVINCE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/PROVINCE"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/POSTCODE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/POSTCODE"/>
						</div>
					</td>
				</tr>
			</xsl:if>
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/COUNTRY"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="header">CONTACT NAME:</div>&#160;
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/CONTACTNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="header">TEL NO:</div>&#160;
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/CONTACTDIALCODE"/>&#160;
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/CONTACTTELEPHONE"/>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>


	<xsl:template name="ukDomesticSenderAddress">

		<table width="100%" class="outLine" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/COMPANYNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/STREETADDRESS1"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/STREETADDRESS2/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/COLLECTION/STREETADDRESS2"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/STREETADDRESS3/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/STREETADDRESS3"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/SENDER/COLLECTION/CITY"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/PROVINCE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/PROVINCE"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/POSTCODE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/POSTCODE"/>
						</div>
					</td>
				</tr>
			</xsl:if>
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/COUNTRY"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="header">CONTACT NAME:&#160;</div>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/CONTACTNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="header">TEL NO:&#160;</div>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/CONTACTDIALCODE"/>&#160;
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/HEADER/SENDER/CONTACTTELEPHONE"/>
					</div>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="UKDomesticDeliveryAddress">

		<table width="100%" class="outLine" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/COMPANYNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/STREETADDRESS1"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/STREETADDRESS2/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/STREETADDRESS2"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/STREETADDRESS3/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/STREETADDRESS3"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/CITY"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/PROVINCE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/PROVINCE"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/POSTCODE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/POSTCODE"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<tr>
				<td colspan="2">
					<div class="header">CONTACT NAME:&#160;</div>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/CONTACTNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="header">TEL NO:</div>&#160;
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/CONTACTDIALCODE"/>&#160;
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/CONTACTTELEPHONE"/>
					</div>						
				</td>
			</tr>
		</table>
	</xsl:template>


	<xsl:template name="UKDomesticReceiverAddress">

		<table width="100%" class="outLine" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/COMPANYNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/STREETADDRESS1"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/STREETADDRESS2/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/STREETADDRESS2"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/STREETADDRESS3/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/DELIVERY/STREETADDRESS3"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<tr>
				<td>
					<div class="data">
						<xsl:value-of select="CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/CITY"/>
					</div>
				</td>
			</tr>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/PROVINCE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/PROVINCE"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<xsl:if test="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/POSTCODE/text()">
				<tr>
					<td>
						<div class="data">
							<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/POSTCODE"/>
						</div>
					</td>
				</tr>
			</xsl:if>

			<tr>
				<td colspan="2">
					<div class="header">CONTACT NAME:&#160;</div>
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/CONTACTNAME"/>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="header">TEL NO:</div>&#160;
					<div class="data">
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/CONTACTDIALCODE"/>&#160;
						<xsl:value-of select="/CONSIGNMENTBATCH/CONSIGNMENT/RECEIVER/CONTACTTELEPHONE"/>
					</div>						
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c)1998-2003 Copyright Sonic Software Corporation. All rights reserved.
<metaInformation>
<scenarios ><scenario default="yes" name="CombinedConnote22" userelativepaths="yes" externalpreview="no" url="UK_Domestic_Shipping_Example&#x2D;result.xml" htmlbaseurl="" outputurl="..\html\ukDOmesticConnote.html" processortype="internal" commandline="" additionalpath="" additionalclasspath="" postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext=""/></scenarios><MapperInfo srcSchemaPath="" srcSchemaRoot="" srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
</metaInformation>
-->