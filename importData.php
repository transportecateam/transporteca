<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php");  
require_once( __APP_PATH_CLASSES__ . "/import.class.php");  

$kImport = new cImportData();


//// Transport Mode 
$kImport->importTransportModeData();


//Weight Measuse
$kImport->importWeightMeasureData();


//Cargo Measuse
$kImport->importCargoMeasureData();

//Pallet Type Data
$kImport->importPalletTypeData();

//Courier Provider Product Packing Data
$kImport->importCourierProviderProductPackingData();


//Import Standard Courier Data
$kImport->importStandardCargoData();

//Import Standard Cargo Category
$kImport->importStandardCargoCategoryData();

//Import Service Type
$kImport->importServiceTypeData();

//Import Ship Con Group
$kImport->importShipConGroupData();


//Origin Destination Data
$kImport->importOriginDestinationData();

//Timing Type Data
$kImport->importTimingTypeData();
?>

