<?php 
/**
  *  admin---postcode
  */
define('PAGE_PERMISSION','__SYSTEM_POSTCODES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Variables - Postcodes";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/postcode/";
validateManagement();
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
		<?=showPostcode($t_base);?>
		
	</div>

</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>