<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base="management/temp_email_mgmt/";
$t_base_error="management/Error/";
$mode=sanitize_all_html_input($_REQUEST['mode']);
//print_r($_REQUEST['rssTemplateAry']);
$kRSS = new cRSS();
if(!empty($_REQUEST['rssTemplateAry']))
{
	$szMessage=(urldecode(base64_decode($_REQUEST['szMessage'])));
	$_REQUEST['rssTemplateAry']['szDescriptionHTML']=$szMessage;
	if($kRSS->updateRssTemplate($_REQUEST['rssTemplateAry']))
	{
		$rssFeedAry = array();
		$rssFeedAry = $kRSS->getAllRssTemplates();
		
		echo "SUCCESS||||";
		echo display_rss_edit_form(true);
		echo "||||";
		echo rss_template_top_form($rssFeedAry);
	}
	else if(!empty($kRSS->arErrorMessages))
	{
		echo "ERROR||||";
		?>
		<div id="regError" class="errorBox" style="display:block;">
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kRSS->arErrorMessages as $key=>$values)
		      {
		      	?>
		      		<li><?=$values?></li>
		        <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?
	}
}
else if($mode=='SHOW_CONTENT')
{
	$idTemplate	=	sanitize_all_html_input($_REQUEST['id']);	
	echo "SUCCESS||||";
	if((int)$idTemplate>0)
	{
		echo display_rss_edit_form(false,$idTemplate);
	}
	else
	{
		echo display_rss_edit_form(true);
	}
	die;
}
