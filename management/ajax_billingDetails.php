<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
require_once( __APP_PATH_CLASSES__ . "/gmail.class.php" );
$t_base="management/billings/";
validateManagement();
$kBooking = new cBooking;
 $kCourierServices= new cCourierServices();
 $kZooz = new cZooz();
if(isset($_POST))
{
	$id = sanitize_all_html_input($_POST['id']);
	$check = sanitize_all_html_input($_POST['check']);
	$mode = sanitize_all_html_input($_POST['mode']);
	$BookingId= sanitize_all_html_input($_POST['idBooking']);
	if($check>0)
	{
		if($mode=="SHOW_CONFIRM_POP_UP")
		{	
			echo ' 
                            <div id="popup-bg"></div>
                            <div id="popup-container">
                            <div id="datepickUp"></div>
                            <script type="text/javascript">
                                    function datepick(){	
                                        $("#datepicker").datepicker();					
                                    }
                                    $("#comment").focus();
                            </script>	
                            <div class="popup signin-popup signin-popup-verification" onkeypress="validateInput(event,\''.$check.'\',\''.$id.'\');">		
                                    <h5>'.t($t_base.'title/are_you_sure').'</h5>
                                    <!--Use current or select date--> <input type="hidden" id="datepicker" readonly value="'.date('d/m/Y').'" >
                                    <p>'.t($t_base.'title/any_comment_to_be_included').'</p>
                                    <input type="text" id="comment" style="width: 275px;"><br/>';
			?>
                                    <br/>	
                                <p>
                                    <a href="javascript:void(0)" class="button1" id ="submitConfirm" onclick="submit_payment('<?=$check?>','<?=$id?>','<?=$BookingId?>');"><span><?=t($t_base.'fields/save')?></span></a> 
                                    <a href="javascript:void(0)" class="button2" id ="cancel" onclick="cancelEditMAnageVar(0);"><span><?=t($t_base.'fields/cancel')?></span></a>
                                </p>
                                </div>
                        </div>
			<?php		
		}
                
		$bExceptionFlag = false;
		if($mode=="CHANGE_STATUS")
		{ 
                    $bookingId=base64_decode($id);

                    $comment = sanitize_all_html_input($_POST['comment']);
                    $date    = substr(sanitize_all_html_input($_POST['date']),0,10);
                    $Booking_Id    = sanitize_all_html_input($_POST['BookingId']);

                    $szLogingLogStr = 'Received Request for Mode: CHANGE_STATUS for booking ID: '.$Booking_Id;
                    
                    if($date==date('d/m/Y'))
                    {
                        $date=date('Y-m-d H:i:s');
                    }
                    else
                    {
                        $date=explode('/',$date);
                        $date=implode('-',$date);
                        $date = date("Y-m-d 12:00:00", strtotime($date));
                    }
                    
                    if($bookingId>0)
                    {
                        $bookingArr = array();
                        $bookingArr=$kBooking->getExtendedBookingDetails($Booking_Id);
                        $isManualCourierBooking = $bookingArr['isManualCourierBooking'];
                        $idBooking = $bookingArr['id'];
                        $iPaymentTypePartner=$bookingArr['iPaymentTypePartner'];
                        $iUpdateBankTransfer = false;
                        if($bookingArr['iTransferConfirmed']==1 && $bookingArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                        {
                            $szLogingLogStr .= "This is a bank trasfer booking.".PHP_EOL;
                            $iUpdateBankTransfer = true;
                            if($bookingArr['iForwarderGPType']!=__FORWARDER_PROFIT_TYPE_MARK_UP__)
                            {
                                $szSelfInvoice = $kBooking->generateInvoice($bookingArr['idForwarder'],true);
                            }
                        } 
                        
                        $iMarkupPaymentReceived = false;
                        if($bookingArr['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                        {
                            $szLogingLogStr .= "This is a Markup booking.".PHP_EOL;
                            $iMarkupPaymentReceived = true;
                        } 
                        else
                        {
                            $szLogingLogStr .= "This is a Referal booking.".PHP_EOL;
                        }
                        $status=$kAdmin->changePaymentRecievedStatus($bookingId,$check,$date,$comment,$iMarkupPaymentReceived,$iUpdateBankTransfer,$szSelfInvoice);
                        
                        if($iMarkupPaymentReceived)
                        { 
                            $fTotalPriceUSD = $bookingArr['fTotalPriceCustomerCurrency'] * $bookingArr['fExchangeRate'];
                            $fTotalVATUSD = $bookingArr['fTotalVat'] * $bookingArr['fExchangeRate'];
                            $fReferralAmountUSD = $bookingArr['fReferalAmountUSD'];
                            
                            //$fTotalInvoiceValueUSD = $fTotalPriceUSD + $fTotalVATUSD ;
                            $fTotalInvoiceValueUSD = $fTotalPriceUSD ;
                            
                            //we always add value in USD to display on Management=>Dashboard=> Actual Revenue Graph
                            $referaalAmountArr = array();
                            $referaalAmountArr['idBooking'] = $bookingArr['idBooking'];
                            $referaalAmountArr['idForwarder'] = $bookingArr['idForwarder']; 
                            $referaalAmountArr['fTotalAmount'] = $fTotalInvoiceValueUSD;
                            $referaalAmountArr['idCurrency'] = 1;
                            $referaalAmountArr['fExchangeRate'] = 1;
                            $referaalAmountArr['szCurrency'] = 'USD'; 
                            $referaalAmountArr['szBooking'] = 'Transporteca mark-up: '.$bookingArr['szBookingRef'];  
                            $referaalAmountArr['fUSDReferralFee'] = $fReferralAmountUSD;
                            $referaalAmountArr['szBookingNotes'] = "We always keep this value in USD to display on Actual Revenue graph. ";
  
                            $szLogingLogStr .= PHP_EOL." Registered mark-up price details in tblforwardertransactions ".PHP_EOL;
                                    
                            $kBilling = new cBilling();
                            $kBilling->insertTransportecaMarkupFee($referaalAmountArr);
                        }
                        
                        if($status==1)
                        {	
                            $bQuickQuote = false;
                            if($bookingArr['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
                            {
                                $bQuickQuote = true;
                            } 
                            
                            $szLogingLogStr .= " Successfully updated payment status in tblforwardertransactions ".PHP_EOL;
                            $kForwarder = new cForwarder(); 
                            $idForwarder = $kAdmin->findForwarderId($bookingId);
                            $kAdmin->updateCustomerPaidAmount($Booking_Id,$iUpdateBankTransfer,$iMarkupPaymentReceived,$bQuickQuote);
                            $checkComapnyBankDetils = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,1);
                            
                            if((($isManualCourierBooking==1 && $bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__) || (int)$bookingArr['iBookingType']==__BOOKING_TYPE_COURIER__) && $iUpdateBankTransfer)
                            {
                                
                                if($bookingArr['iBookingType']!=__BOOKING_TYPE_RFQ__)
                                {
                                    $szLogingLogStr .= " Handling case of Booking type: Courier and Payment: Bank Trasfer ".PHP_EOL;

                                    $kCourierServices= new cCourierServices();
                                    $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($Booking_Id);

                                    $shipperDetailsArr=$kCourierServices->getShipperNameById($bookingArr['idShipperConsignee'],true);

                                    if((int)$newCourierBookingAry[0]['iBookingIncluded']==1)
                                         $szSender=$newCourierBookingAry[0]['szDisplayName'];
                                    else
                                    {    
                                        if($bookingArr['iBookingLanguage']==2)
                                          $szSender="Vi";
                                        else
                                            $szSender="we have";
                                    }
                                    $kForwarderContact= new cForwarderContact();
                                    $bookingServiceEmailArr=$kForwarderContact->getAllForwarderContactsEmail($idForwarder,__CUSTOMER_PROFILE_ID__);
                                    $szLogingLogStr .= " Forwarder Emails to be sent on ".print_R($bookingServiceEmailArr,true).PHP_EOL;

                                    if(!empty($bookingServiceEmailArr))
                                    { 
                                        $ctr=0;
                                        $customerServiceEmailStr='';
                                        foreach($bookingServiceEmailArr as $key=>$customerServiceEmailAry)
                                        { 
                                            $iTotalCounter = count($customerServiceEmailAry);
                                            foreach($customerServiceEmailAry as $customerServiceEmailArys)
                                            {    
                                                if(empty($customerServiceEmailStr))
                                                {
                                                     $customerServiceEmailStr = $customerServiceEmailArys['szEmail'] ; 
                                                }
                                                else
                                                {
                                                    $customerServiceEmailLink .= ', '.$customerServiceEmailArys['szEmail']; 
                                                    if($ctr>0 && $ctr==($iTotalCounter-1))
                                                    {
                                                        $customerServiceEmailStr .= ' and '.$customerServiceEmailArys['szEmail'];
                                                    }
                                                    else
                                                    {
                                                        $customerServiceEmailStr .= ', '.$customerServiceEmailArys['szEmail'];
                                                    }
                                               }							
                                               $ctr++;
                                            }
                                        }
                                    }
                                }
                                //echo $customerServiceEmailStr;
                                //die();
                                $replace_ary=array();
                                $replace_ary['szBookingRef']=$bookingArr['szBookingRef'];
                                $replace_ary['iBookingLanguage']=$bookingArr['iBookingLanguage'];
                                $replace_ary['szOriginCountry']=$bookingArr['szOriginCountry'];
                                $replace_ary['szDestinationCountry']=$bookingArr['szDestinationCountry'];
                                $replace_ary['szSender']=$szSender;
                                $replace_ary['szShipperCompany']=$shipperDetailsArr['szShipperCompanyName'];
                                $replace_ary['szSuuportEmail']=__STORE_SUPPORT_EMAIL__;
                                $replace_ary['szSupportPhone']=__STORE_SUPPORT_PHONE_NUMBER__;
                                $replace_ary['szForwarderDispName']=$bookingArr['szForwarderDispName'];
                                $replace_ary['szForwarderCustServiceEmail']=$customerServiceEmailStr;
                                $replace_ary['szName']=$bookingArr['szFirstName']." ".$bookingArr['szLastName'];
                                $replace_ary['szEmail']=$bookingArr['szEmail']; 
                                $replace_ary['iAdminFlag']=1;
                                $replace_ary['idBooking']=$bookingArr['id'];
                                $replace_ary['szFirstName']=$bookingArr['szFirstName'];
                                $replace_ary['szLastName']=$bookingArr['szLastName'];
                                if((int)$iPaymentTypePartner==0)
                                {
                                    try
                                    {
                                        $szLogingLogStr .= " Sending E-mail with details: ".print_R($replace_ary,true).PHP_EOL;
                                        if($bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__)
                                        {
                                           createEmail(__MANUAL_BANK_TRANSFER_RECEIVED_CONFIRMATION_EMAIL__, $replace_ary,$bookingArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$bookingArr['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__,true,$bookingArr['id']); 
                                        }else{
                                        createEmail(__COURIER_BOOKING_CONFIRNATION_AFTER_TRANSFER_EMAIL__, $replace_ary,$bookingArr['szEmail'], "", __STORE_SUPPORT_EMAIL__,$bookingArr['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__,true,$bookingArr['id']); 
                                        }
                                    } catch (Exception $ex) {
                                        $bExceptionFlag = true;
                                        $szLogingLogStr .= " Got Exception in sending email: __COURIER_BOOKING_CONFIRNATION_AFTER_TRANSFER_EMAIL__ ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                    }
                                }
                            } 
                            if(($isManualCourierBooking==1 && $bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__) || ($bookingArr['iBookingType']==__BOOKING_TYPE_COURIER__))
                            {
                                $szLogingLogStr .= " Handling case of Booking type: RFQ ".PHP_EOL;
                                
                                try
                                {
                                    $kBooking = new cBooking();
                                    $dtResponseTime = $kBooking->getRealNow();   
                                    if($iUpdateBankTransfer)
                                    {   
                                        $courierBookingArr=$kCourierServices->getCourierBookingData($Booking_Id); 
                                        $kBooking = new cBooking();
                                        $dtResponseTime = $kBooking->getRealNow(); 
                                        
                                        if($bookingArr['idQuotePricingDetails']>0)
                                        {
                                            $kBooking->expireOtherQuotes($bookingArr); 
                                        }

                                        $iSearchType = 1;
                                        if($isManualCourierBooking==1)
                                        {
                                            $iSearchType = 2;
                                        }
                                        if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                                        {
                                           $fileLogsAry = array();
                                           $fileLogsAry['szTransportecaStatus'] = "S7";
                                           $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                                           $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                                           $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                                           $fileLogsAry['szForwarderBookingStatus'] = '';                                    
                                           $fileLogsAry['szForwarderBookingTask'] = 'T140'; 
                                           $fileLogsAry['idBooking'] = (int)$Booking_Id;
                                           $fileLogsAry['idUser'] = (int)$bookingArr['idUser']; 
                                           $fileLogsAry['iSearchType'] = $iSearchType; 

                                           $kBooking->insertCourierBookingFileDetails($fileLogsAry); 
                                        }
                                        else
                                        {
                                           $fileLogsAry = array();
                                           $fileLogsAry['szTransportecaStatus'] = "S7";
                                           $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                                           $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                                           $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                                           //$fileLogsAry['szForwarderBookingStatus'] = '';                                    
                                           $fileLogsAry['szTransportecaTask'] = 'T140';  
                                           $fileLogsAry['idBooking'] = (int)$Booking_Id;        
                                           $fileLogsAry['idUser'] = (int)$bookingArr['idUser'];
                                           $fileLogsAry['iSearchType'] = $iSearchType;

                                           $kBooking->insertCourierBookingFileDetails($fileLogsAry);
                                        }
                                    }
                                } catch (Exception $ex) {
                                    $bExceptionFlag = true;
                                    $szLogingLogStr .= " Got Exception in processing Manual Bookings: ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                } 
                            }
                            
                            if(($isManualCourierBooking==1 && $bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__) || ($bookingArr['iBookingType']==__BOOKING_TYPE_COURIER__))
                            { 
                                if($iUpdateBankTransfer)
                                {    
                                    try
                                    {  
                                        $kConfig = new cConfig();
                                        $data = array();
                                        $data['idForwarder'] = $bookingArr['idForwarder'];
                                        $data['idTransportMode'] = $bookingArr['idTransportMode'];
                                        $data['idOriginCountry'] = $bookingArr['idOriginCountry'];
                                        $data['idDestinationCountry'] = $bookingArr['idDestinationCountry'];
                                        $data['iBookingSentFlag'] = '1';
                                        $kForwarder = new cForwarder();
                                        $kForwarder->load($bookingArr['idForwarder']);
                                        $iProfitType = $kForwarder->iProfitType;
                                        $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($bookingArr['idForwarder']);

                                        $forwarderContactAry = array();
                                        $forwarderContactAry = $kConfig->getForwarderContact($data);

                                        if(!empty($forwarderContactAry))
                                        {
                                           foreach($forwarderContactAry as $forwarderContactArys)
                                           {
                                                $replace_ary=array(); 
                                                $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$Booking_Id."__".md5(time())."/"; 

                                                $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
                                                $replace_ary['szUrl'] = $szControlPanelUrl; 

                                                $quotesAry =$kCourierServices->getAllBookingQuotesByFileByBookingId($Booking_Id);

                                                $szControlPanelLabelUrl='http://'.$kForwarder->szControlPanelUrl."/pendingQuotes/createLabel/".$Booking_Id."__".md5(time())."/";


                                                $replace_ary['szLabelLink'] = '<a href="'.$szControlPanelLabelUrl.'" target="_blank"> UPLOAD LABLES AND TRACKING NUMBER HERE</a>';
                                                $replace_ary['szLabelUrl'] = $szControlPanelLabelUrl;

                                                $replace_ary['szBookingRef']=$bookingArr['szBookingRef'];
                                                $replace_ary['iBookingLanguage']=$bookingArr['iBookingLanguage'];
                                                $replace_ary['szOriginCountry']=$bookingArr['szOriginCountry'];
                                                $replace_ary['szDestinationCountry']=$bookingArr['szDestinationCountry'];
                                                $replace_ary['szForwarderDispName']=$bookingArr['szForwarderDispName'];

                                                $replace_ary['szFirstName'] = utf8_encode($acceptedByUserAry['szFirstName']);
                                                $replace_ary['szLastName'] = utf8_encode($acceptedByUserAry['szLastName']);
                                                $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));
                                                $replace_ary['szEmail']=$forwarderContactArys;   
                                                $replace_ary['iAdminFlag']=1;
                                                $replace_ary['idBooking']=$bookingArr['id'];
                                                if($bookingArr['iCourierBookingType']==1)
                                                {
                                                    $szBookingType="Courier";
                                                }
                                                else
                                                {
                                                    $szBookingType="Courier";
                                                }        
                                                $replace_ary['szBookingType']=$szBookingType;

                                                $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>';
                                                
                                                $kWhsSearch = new cWHSSearch();  
                                                $forwarderBookingEmail = $kWhsSearch->getManageMentVariableByDescription('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__');  
                                                //$forwarderBookingEmail = __FINANCE_CONTACT_EMAIL__;
                                                
                                                if($iProfitType==1)
                                                {
                                                    $kConfig = new cConfig();
                                                    $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
                                                    $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
                                                    $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
                                                    $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
                                                    $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
                                                    $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
                                                }
                                                else
                                                {
                                                    $replace_ary['szTermsConditionForReferralForwarderText']='';
                                                }
                        
                                                if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                                                {
                                                    createEmail(__COURIER_BOOKING_RECEIVED_CREATE_LABEL_FORWARDER_EMAIL__, $replace_ary, $replace_ary['szEmail'],'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                                }
                                                else 
                                                {
                                                    if(doNotSendMailToThisDomainEmail($replace_ary['szEmail']))
                                                    {
                                                        createEmail(__FORWADER_COURIER_BOOKING_RECEIVED__, $replace_ary, $replace_ary['szEmail'],'' ,$forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                                    }
                                                    /*
                                                    * FW: E-mail change 19-01-2017
                                                    */
                                                    //createEmail(__CREATE_LABEL_FOR_COURIER_BY_TRANSPORTECA_EMAIL__, $replace_ary, __STORE_SUPPORT_EMAIL__,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                                }
                                            }  
                                        } 
                                    } catch (Exception $ex) {
                                        $bExceptionFlag = true;
                                        $szLogingLogStr .= " Got Exception in processing Courier Booking with bank trasfer ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                    } 
                                }
                            }
                            else
                            {
                                if($iUpdateBankTransfer)
                                {  
                                    try
                                    {
                                        $kConfig = new cConfig();
                                        $kForwarderContact= new cForwarderContact();
                                        $kUser = new cUser();
                                        $kWhsSearch = new cWHSSearch();
                                        $kBooking->SendTransferConfirmation($bookingArr);
                                    } catch (Exception $ex) { 
                                        $bExceptionFlag = true;
                                        $szLogingLogStr .= " Got Exception in sending confirmation email: ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                    } 
                                } 
                                if($checkComapnyBankDetils)
                                {
                                    $replace_ary=array();
                                    $ForwarderAdminDetails = $kAdmin->findForwarderAdminsByBookingId($idForwarder);
                                    $replace_ary['szDate']=findPaymentFreq($ForwarderAdminDetails[0]['szPaymentFrequency']);
                                    if($ForwarderAdminDetails!= array() && $bookingArr['idBookingStatus']=='3')
                                    {
                                        foreach($ForwarderAdminDetails AS $key=>$value)
                                        {
                                            $replace_ary['szFirstName']=$value['szFirstName'];
                                            $replace_ary['szDisplayName']=$value['szDisplayName'];
                                            $replace_ary['szHere']='<a href ="http://'.$value['szControlPanelUrl'].'/Company/BankDetails/">here</a>';
                                            $to =  $value['szEmail'];
                                            try
                                            {
                                                createEmail(__UPDATE_BANK_DETAILS_FORWARDER__, $replace_ary, $to,'' , __FINANCE_CONTACT_EMAIL__,$value['id'] , __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__);
                                            }
                                            catch (Exception $ex) { 
                                                $szLogingLogStr .= " Got Exception sending email: __UPDATE_BANK_DETAILS_FORWARDER__ ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                                $bExceptionFlag = true;
                                            }
                                        }
                                    }
                                } 
                            }
                            $filename = __APP_PATH_ROOT__."/logs/bookingReceivedLogs.log";
                            $strdata=array();
                            $strdata[0]=" \n\n /******************** Booking# ".$Booking_Id." Received on ".date('Y-m-d H:i:s')." \n\n";
                            $strdata[1] = $szLogingLogStr."\n\n"; 
                            file_log(true, $strdata, $filename);
                            
                            $billingDetails=$kAdmin->showBookingConfirmedTransaction();
                            echo showCustomerAdminBilling($billingDetails,$check,$bExceptionFlag);
                            //echo "||||";
                            //echo showAdminChecks();
                            die();
                        }
                    } 
		}
	
                if($mode=="OPEN_CSV_UPLOAD_POP_UP")
		{
                    echo 'SUCCESS||||';
                    ?>
                    <script type="text/javascript">
                    $(document).ready(function() {
                        var settings = {
                        url: "<?php echo __MANAGEMENT_URL__?>/outstandingPayment/uploadStripePayments.php",
                        method: "POST",
                        allowedTypes:"csv",
                        fileName: "myfile",
                        multiple: false,
                        dragDrop: false,
                        onSuccess:function(files,data,xhr)
                        { 
                            var IS_JSON = true;
                            try
                            {
                                var json = $.parseJSON(data);
                            }
                            catch(err)
                            {
                                IS_JSON = false;
                            } 
                            if(IS_JSON)
                            { 
                                var obj = JSON.parse(data); 
                                
                                var iFileCounter = 1;

                                var filecount=$("#filecount").attr('value');
                                
                                $("#deleteArea").attr('style','display:block'); 
                                $("#upload_process_list").attr('style','display:none');
                                $("#upload_process_list").html('');
                                $(".file_list_con").attr('style','display:block');

                                var newfilecount = parseInt(filecount) + iFileCounter;  
                                var uploadedFileName = obj[0]['name']; 
                                  
                                $('#fileList').html('<br><strong>'+uploadedFileName+'</strong>');
                                var newvalue=files+"#####"+uploadedFileName;
                                $("#filecount").attr('value',newfilecount);
                                $("#file_name").attr('value',newvalue);

                                if(parseInt(newfilecount) >0)
                                {
                                    var vaildator ="<?php echo $check;?>";
                                    $(".upload").attr('style','display:none');
                                    $('#submit_button').attr('style','opacity:1');
                                    $('#submit_button').attr('onclick','submitStripePaymentCsv('+vaildator+');');
                                } 
                            }
                            else
                            {  
                                $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='showHide(\"upload_process_list\")'>close</a></p>");
                            }

                        },onSelect:function(s){
                            $("#upload_process_list").attr('style','display:block;top:33%;width:345px');
                            $("#upload_error_container").attr('style','display:none');
                        },
                            afterUploadAll:function()
                            {
                                //alert("all images uploaded!!");
                            },
                            onError: function(files,status,errMsg)
                            {
                                //$("#status").html("<font color='red'>Upload is Failed</font>");
                                alert('hi');
                            }
                        }
                        $("#upload_csv_file").uploadFile(settings);
                    });
                </script>
                        <div id="popup-bg"></div>
                        <div id="popup-container" >
                            <div class="popup" > 
                                <h2>Upload CSV</h2>
                                <div class="clearfix" style="width:100%;">
                                <div class="clearfix email-fields-container">
                                        <div id="upload_error_container" style="display:none;"></div>
                                        <div id="upload_csv_file" class="upload_label">
                                                Upload
                                        </div>
                                        <div id="upload_process_list" style="display:none;"></div>
                                        <!--<div id="upload_csv_loader" style="display:none;">
                                            <label>&nbsp;</label>
                                            <img src="<?php echo __BASE_STORE_IMAGE_URL__."/uploading.gif"; ?>" alt="uploading..."/>
                                        </div>-->
                                        <div class="file_list_con" style="display:none;">
                                            <div id="fileList">
                                            </div>
                                            <div id="deleteArea" style="display:none;">
                                                <img onclick="removeStripePaymentCsv('<?php echo $check;?>')"style="cursor:pointer;margin-bottom:30px;margin-right:50px;" src="<?php echo __BASE_STORE_INC_URL__?>/image.php?img=Trash.png&temp=2&w=20&h=20" title="Click here to delete." alert="Click here to delete."/>
                                            </div>
                                        </div>
                                        <form name="submitStripePaymentCsvForm" id="submitStripePaymentCsvForm" method="post" action="javascript:void(0);">
                                            <input id="filecount" name="filecount" value="0" type="hidden">
                                            <input id="file_name" name="file_name" value="" type="hidden">
                                        </form>
                                    </div>
                                    <div class="btn-container"> 
                                        <p align="center">
                                            <a href="javascript:void(0);" id="submit_button" class="button1" style="opacity:0.4;"><span>SUBMIT</span></a>
                                            <a href="javascript:void(0);" id="close_button" class="button1" onclick="closeConactPopUp(0);"><span>CLOSE</span></a>
                                        </p>
                                    </div> 
                                </div>
                            </div>
                        </div>
			<?php		
		}
                if($mode=="SUBMIT_STRIPE_PAYMENT_CSV")
                { 
                    $errorflag=false;
                    $filecount =(int)$_POST['filecount'];
                    $file_name =trim($_POST['file_name']);
                    if($filecount >0 && $file_name!='')
                    {
                        $nameAry=explode('#####',$file_name);
                        $originalname=trim($nameAry[0]);
                        $uploadedFileName =trim($nameAry[1]);
                        $uploaded_file_path= __UPLOAD_STRIPE_PAYMENT_CSV_MANAGEMENT__.'/'.$uploadedFileName;
                        if(file_exists($uploaded_file_path))
                        {
                            if (($handle = fopen($uploaded_file_path, "r")) !== FALSE) 
                            {
                                $stripePaymentdata=array();
                                $row = 0;
                                while (($data = fgetcsv($handle,"", ",")) !== FALSE) {
                                  $data = array_map("utf8_encode", $data);
                                  $num = count($data);
                                  for ($c=0; $c < $num; $c++) {
                                      $stripePaymentdata[$row][]= trim($data[$c]);
                                  }
                                  $row++;
                                }
                                fclose($handle);
                                
                                if(!empty($stripePaymentdata) && count($stripePaymentdata) >1)
                                {
                                    if(count($stripePaymentdata[0]) == __STRIPE_PAYMENT_CSV_COLUMN_COUNT__)
                                    {
                                       $errorAry=array();
                                       $j=0;
                                       $k=0;
                                       for($i=1; $i < count($stripePaymentdata); $i++)
                                       {
                                           $Booking_IdAry=array();
                                           $bookupdated=true;
                                           $Booking_IdAry = $kZooz->getPaymentDetailsByTransactionId(trim($stripePaymentdata[$i]['1']));
                                           
                                           
                                           if(!empty($Booking_IdAry))
                                           {
                                               $Booking_Id=(int)$Booking_IdAry[0]['idBooking'];
                                               $BookingDetailsAry=array();
                                               $BookingDetailsAry=$kZooz->getPaymentDetailsByBookingId($Booking_Id);
                                               if(!empty($BookingDetailsAry))
                                               {
                                                    $stripePaymentdata[$i][11]=$BookingDetailsAry[0]['szBookingRef'];
                                                    $stripePaymentdata[$i][12]=$BookingDetailsAry[0]['idBookingStatus'];
                                                    $stripePaymentdata[$i][13]=$BookingDetailsAry[0]['szBookingRandomNum'];
                                                    $show_currency=  strtolower($BookingDetailsAry[0]['szCurrency']);
                                                    $fInsurancePrice = 0;
                                                    if($BookingDetailsAry[0]['iInsuranceIncluded']==1)
                                                    {
                                                        $fInsurancePrice = $BookingDetailsAry[0]['fTotalInsuranceCostForBookingCustomerCurrency'];
                                                    }
                                                    $show_amount=number_format((float)($BookingDetailsAry[0]['fTotalAmount']+$fInsurancePrice),2,',','');
                                                    // checking for amount & currency
                                                    if(($show_amount==$stripePaymentdata[$i]['4']) && ($show_currency==$stripePaymentdata[$i]['5']))
                                                    {
                                                        $bookingId=(int)$BookingDetailsAry[0]['id'];
                                                        //$comment = sanitize_all_html_input('Uploaded');
                                                        $date    = date('d/m/Y');
                                                        
                                                        $szLogingLogStr = 'Received Request for Mode: SUBMIT_STRIPE_PAYMENT_CSV for booking ID: '.$Booking_Id;
                                                        
                                                        if($date==date('d/m/Y'))
                                                        {
                                                            $date=date('Y-m-d H:i:s',strtotime('-1 HOUR'));
                                                        }
                                                        else
                                                        {
                                                            $date=explode('/',$date);
                                                            $date=implode('-',$date);
                                                            $date = date("Y-m-d 12:00:00", strtotime($date));
                                                        }
                                                        
                                                        if($bookingId>0)
                                                        {
                                                            $bookupdated=true;
                                                            $bookingArr = array();
                                                            $bookingArr=$kBooking->getExtendedBookingDetails($Booking_Id);
                                                            $isManualCourierBooking = $bookingArr['isManualCourierBooking'];
                                                            $idBooking = $bookingArr['id'];

                                                            $iUpdateBankTransfer = false;
                                                            $szSelfInvoice='';
                                                            if($bookingArr['iTransferConfirmed']==1 && $bookingArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                                                            {
                                                                $szLogingLogStr .= "This is a bank trasfer booking.".PHP_EOL;
                                                                $iUpdateBankTransfer = true;
                                                                if($bookingArr['iForwarderGPType']!=__FORWARDER_PROFIT_TYPE_MARK_UP__)
                                                                {
                                                                    $szSelfInvoice = $kBooking->generateInvoice($bookingArr['idForwarder'],true);
                                                                }
                                                            } 

                                                            $iMarkupPaymentReceived = false;
                                                            if($bookingArr['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                                                            {
                                                                $szLogingLogStr .= "This is a Markup booking.".PHP_EOL;
                                                                $iMarkupPaymentReceived = true;
                                                            } 
                                                            else
                                                            {
                                                                $szLogingLogStr .= "This is a Referal booking.".PHP_EOL;
                                                            }
                                                            $status=$kAdmin->changePaymentRecievedStatus($bookingId,$check,$date,$comment,$iMarkupPaymentReceived,$iUpdateBankTransfer,$szSelfInvoice);

                                                            if($iMarkupPaymentReceived)
                                                            { 
                                                                $fTotalPriceUSD = $bookingArr['fTotalPriceCustomerCurrency'] * $bookingArr['fExchangeRate'];
                                                                $fTotalVATUSD = $bookingArr['fTotalVat'] * $bookingArr['fExchangeRate'];
                                                                $fReferralAmountUSD = $bookingArr['fReferalAmountUSD'];

                                                                //$fTotalInvoiceValueUSD = $fTotalPriceUSD + $fTotalVATUSD ;
                                                                $fTotalInvoiceValueUSD = $fTotalPriceUSD ;

                                                                //we always add value in USD to display on Management=>Dashboard=> Actual Revenue Graph
                                                                $referaalAmountArr = array();
                                                                $referaalAmountArr['idBooking'] = $bookingArr['idBooking'];
                                                                $referaalAmountArr['idForwarder'] = $bookingArr['idForwarder']; 
                                                                $referaalAmountArr['fTotalAmount'] = $fTotalInvoiceValueUSD;
                                                                $referaalAmountArr['idCurrency'] = 1;
                                                                $referaalAmountArr['fExchangeRate'] = 1;
                                                                $referaalAmountArr['szCurrency'] = 'USD'; 
                                                                $referaalAmountArr['szBooking'] = 'Transporteca mark-up: '.$bookingArr['szBookingRef'];  
                                                                $referaalAmountArr['fUSDReferralFee'] = $fReferralAmountUSD;
                                                                $referaalAmountArr['szBookingNotes'] = "We always keep this value in USD to display on Actual Revenue graph. ";

                                                                $szLogingLogStr .= PHP_EOL." Registered mark-up price details in tblforwardertransactions ".PHP_EOL;

                                                                $kBilling = new cBilling();
                                                                $kBilling->insertTransportecaMarkupFee($referaalAmountArr);
                                                            }

                                                            if($status==1)
                                                            {	
                                                                $bQuickQuote = false;
                                                                if($bookingArr['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
                                                                {
                                                                    $bQuickQuote = true;
                                                                } 

                                                                $szLogingLogStr .= " Successfully updated payment status in tblforwardertransactions ".PHP_EOL;
                                                                $kForwarder = new cForwarder(); 
                                                                $idForwarder = $kAdmin->findForwarderId($bookingId);
                                                                $kAdmin->updateCustomerPaidAmount($Booking_Id,$iUpdateBankTransfer,$iMarkupPaymentReceived,$bQuickQuote);
                                                                $checkComapnyBankDetils = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,1);

                                                                if((($isManualCourierBooking==1 && $bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__) || (int)$bookingArr['iBookingType']==__BOOKING_TYPE_COURIER__) && $iUpdateBankTransfer)
                                                                {
                                                                    if($bookingArr['iBookingType']!=__BOOKING_TYPE_RFQ__)
                                                                    {
                                                                        $szLogingLogStr .= " Handling case of Booking type: Courier and Payment: Bank Trasfer ".PHP_EOL;

                                                                        $kCourierServices= new cCourierServices();
                                                                        $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($Booking_Id);

                                                                        $shipperDetailsArr=$kCourierServices->getShipperNameById($bookingArr['idShipperConsignee'],true);

                                                                        if((int)$newCourierBookingAry[0]['iBookingIncluded']==1)
                                                                             $szSender=$newCourierBookingAry[0]['szDisplayName'];
                                                                        else
                                                                        {    
                                                                            if($bookingArr['iBookingLanguage']==2)
                                                                              $szSender="Vi";
                                                                            else
                                                                                $szSender="we have";
                                                                        }
                                                                        $kForwarderContact= new cForwarderContact();
                                                                        $bookingServiceEmailArr=$kForwarderContact->getAllForwarderContactsEmail($idForwarder,__CUSTOMER_PROFILE_ID__);
                                                                        $szLogingLogStr .= " Forwarder Emails to be sent on ".print_R($bookingServiceEmailArr,true).PHP_EOL;

                                                                        if(!empty($bookingServiceEmailArr))
                                                                        { 
                                                                            $ctr=0;
                                                                            $customerServiceEmailStr='';
                                                                            foreach($bookingServiceEmailArr as $key=>$customerServiceEmailAry)
                                                                            { 
                                                                                $iTotalCounter = count($customerServiceEmailAry);
                                                                                foreach($customerServiceEmailAry as $customerServiceEmailArys)
                                                                                {    
                                                                                    if(empty($customerServiceEmailStr))
                                                                                    {
                                                                                         $customerServiceEmailStr = $customerServiceEmailArys['szEmail'] ; 
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        $customerServiceEmailLink .= ', '.$customerServiceEmailArys['szEmail']; 
                                                                                        if($ctr>0 && $ctr==($iTotalCounter-1))
                                                                                        {
                                                                                            $customerServiceEmailStr .= ' and '.$customerServiceEmailArys['szEmail'];
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            $customerServiceEmailStr .= ', '.$customerServiceEmailArys['szEmail'];
                                                                                        }
                                                                                   }							
                                                                                   $ctr++;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    //echo $customerServiceEmailStr;
                                                                    //die();
                                                                    $replace_ary=array();
                                                                    $replace_ary['szBookingRef']=$bookingArr['szBookingRef'];
                                                                    $replace_ary['iBookingLanguage']=$bookingArr['iBookingLanguage'];
                                                                    $replace_ary['szOriginCountry']=$bookingArr['szOriginCountry'];
                                                                    $replace_ary['szDestinationCountry']=$bookingArr['szDestinationCountry'];
                                                                    $replace_ary['szSender']=$szSender;
                                                                    $replace_ary['szShipperCompany']=$shipperDetailsArr['szShipperCompanyName'];
                                                                    $replace_ary['szSuuportEmail']=__STORE_SUPPORT_EMAIL__;
                                                                    $replace_ary['szSupportPhone']=__STORE_SUPPORT_PHONE_NUMBER__;
                                                                    $replace_ary['szForwarderDispName']=$bookingArr['szForwarderDispName'];
                                                                    $replace_ary['szForwarderCustServiceEmail']=$customerServiceEmailStr;
                                                                    $replace_ary['szName']=$bookingArr['szFirstName']." ".$bookingArr['szLastName'];
                                                                    $replace_ary['szEmail']=$bookingArr['szEmail']; 
                                                                    $replace_ary['iAdminFlag']=1;
                                                                    $replace_ary['idBooking']=$bookingArr['id'];                                                                    
                                                                    $replace_ary['szFirstName']=$bookingArr['szFirstName'];
                                                                    $replace_ary['szLastName']=$bookingArr['szLastName'];

                                                                    try
                                                                    {
                                                                        $szLogingLogStr .= " Sending E-mail with details: ".print_R($replace_ary,true).PHP_EOL;
                                                                        if($bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__)
                                                                        {
                                                                           createEmail(__MANUAL_BANK_TRANSFER_RECEIVED_CONFIRMATION_EMAIL__, $replace_ary,$bookingArr['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$bookingArr['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__,true,$bookingArr['id']); 
                                                                        }else{
                                                                        createEmail(__COURIER_BOOKING_CONFIRNATION_AFTER_TRANSFER_EMAIL__, $replace_ary,$bookingArr['szEmail'], "", __STORE_SUPPORT_EMAIL__,$bookingArr['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_CUSTOMER__,true,$bookingArr['id']); 
                                                                        }
                                                                    } catch (Exception $ex) {
                                                                        $bExceptionFlag = true;
                                                                        $szLogingLogStr .= " Got Exception in sending email: __COURIER_BOOKING_CONFIRNATION_AFTER_TRANSFER_EMAIL__ ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                                                    } 
                                                                } 
                                                                if(($isManualCourierBooking==1 && $bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__) || ($bookingArr['iBookingType']==__BOOKING_TYPE_COURIER__))
                                                                {
                                                                    $szLogingLogStr .= " Handling case of Booking type: RFQ ".PHP_EOL;

                                                                    try
                                                                    {
                                                                        $kBooking = new cBooking();
                                                                        $dtResponseTime = $kBooking->getRealNow();   
                                                                        if($iUpdateBankTransfer)
                                                                        {   
                                                                            $courierBookingArr=$kCourierServices->getCourierBookingData($Booking_Id); 
                                                                            $kBooking = new cBooking();
                                                                            $dtResponseTime = $kBooking->getRealNow(); 
                                                                            
                                                                            if($bookingArr['idQuotePricingDetails']>0)
                                                                            {
                                                                                $kBooking->expireOtherQuotes($bookingArr); 
                                                                            }

                                                                            $iSearchType = 1;
                                                                            if($isManualCourierBooking==1)
                                                                            {
                                                                                $iSearchType = 2;
                                                                            }
                                                                            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                                                                            {
                                                                               $fileLogsAry = array();
                                                                               $fileLogsAry['szTransportecaStatus'] = "S7";
                                                                               $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                                                                               $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                                                                               $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                                                                               $fileLogsAry['szForwarderBookingStatus'] = '';                                    
                                                                               $fileLogsAry['szForwarderBookingTask'] = 'T140'; 
                                                                               $fileLogsAry['idBooking'] = (int)$Booking_Id;
                                                                               $fileLogsAry['idUser'] = (int)$bookingArr['idUser']; 
                                                                               $fileLogsAry['iSearchType'] = $iSearchType; 

                                                                               $kBooking->insertCourierBookingFileDetails($fileLogsAry); 
                                                                            }
                                                                            else
                                                                            {
                                                                               $fileLogsAry = array();
                                                                               $fileLogsAry['szTransportecaStatus'] = "S7";
                                                                               $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
                                                                               $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                                                                               $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
                                                                               //$fileLogsAry['szForwarderBookingStatus'] = '';                                    
                                                                               $fileLogsAry['szTransportecaTask'] = 'T140';  
                                                                               $fileLogsAry['idBooking'] = (int)$Booking_Id;        
                                                                               $fileLogsAry['idUser'] = (int)$bookingArr['idUser'];
                                                                               $fileLogsAry['iSearchType'] = $iSearchType;

                                                                               $kBooking->insertCourierBookingFileDetails($fileLogsAry);
                                                                            }
                                                                        }
                                                                    } catch (Exception $ex) {
                                                                        $bExceptionFlag = true;
                                                                        $szLogingLogStr .= " Got Exception in processing Manual Bookings: ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                                                    } 
                                                                }

                                                                if(($isManualCourierBooking==1 && $bookingArr['iBookingType']==__BOOKING_TYPE_RFQ__) || ($bookingArr['iBookingType']==__BOOKING_TYPE_COURIER__))
                                                                { 
                                                                    if($iUpdateBankTransfer)
                                                                    {    
                                                                        try
                                                                        {  
                                                                            $kConfig = new cConfig();
                                                                            $data = array();
                                                                            $data['idForwarder'] = $bookingArr['idForwarder'];
                                                                            $data['idTransportMode'] = $bookingArr['idTransportMode'];
                                                                            $data['idOriginCountry'] = $bookingArr['idOriginCountry'];
                                                                            $data['idDestinationCountry'] = $bookingArr['idDestinationCountry'];
                                                                            $data['iBookingSentFlag'] = '1';
                                                                            $kForwarder = new cForwarder();
                                                                            $kForwarder->load($bookingArr['idForwarder']);
                                                                            $iProfitType = $kForwarder->iProfitType;
                                                                            $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($bookingArr['idForwarder']);

                                                                            $forwarderContactAry = array();
                                                                            $forwarderContactAry = $kConfig->getForwarderContact($data);

                                                                            if(!empty($forwarderContactAry))
                                                                            {
                                                                               foreach($forwarderContactAry as $forwarderContactArys)
                                                                               {
                                                                                    $replace_ary=array(); 
                                                                                    $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$Booking_Id."__".md5(time())."/"; 

                                                                                    $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
                                                                                    $replace_ary['szUrl'] = $szControlPanelUrl; 

                                                                                    $quotesAry =$kCourierServices->getAllBookingQuotesByFileByBookingId($Booking_Id);

                                                                                    $szControlPanelLabelUrl='http://'.$kForwarder->szControlPanelUrl."/pendingQuotes/createLabel/".$Booking_Id."__".md5(time())."/";


                                                                                    $replace_ary['szLabelLink'] = '<a href="'.$szControlPanelLabelUrl.'" target="_blank"> UPLOAD LABLES AND TRACKING NUMBER HERE</a>';
                                                                                    $replace_ary['szLabelUrl'] = $szControlPanelLabelUrl;

                                                                                    $replace_ary['szBookingRef']=$bookingArr['szBookingRef'];
                                                                                    $replace_ary['iBookingLanguage']=$bookingArr['iBookingLanguage'];
                                                                                    $replace_ary['szOriginCountry']=$bookingArr['szOriginCountry'];
                                                                                    $replace_ary['szDestinationCountry']=$bookingArr['szDestinationCountry'];
                                                                                    $replace_ary['szForwarderDispName']=$bookingArr['szForwarderDispName'];

                                                                                    $replace_ary['szFirstName'] = utf8_encode($acceptedByUserAry['szFirstName']);
                                                                                    $replace_ary['szLastName'] = utf8_encode($acceptedByUserAry['szLastName']);
                                                                                    $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));
                                                                                    $replace_ary['szEmail']=$forwarderContactArys;   
                                                                                    $replace_ary['iAdminFlag']=1;
                                                                                    $replace_ary['idBooking']=$bookingArr['id'];
                                                                                    if($bookingArr['iCourierBookingType']==1)
                                                                                    {
                                                                                        $szBookingType="Courier";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        $szBookingType="Courier";
                                                                                    }        
                                                                                    $replace_ary['szBookingType']=$szBookingType;

                                                                                    $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>';
                                                                                    
                                                                                    $kWhsSearch = new cWHSSearch();  
                                                                                    $forwarderBookingEmail = $kWhsSearch->getManageMentVariableByDescription('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__');  
                                                                                    //$forwarderBookingEmail = __FINANCE_CONTACT_EMAIL__;
                                                                                    
                                                                                    if($iProfitType==1)
                                                                                    {
                                                                                        $kConfig = new cConfig();
                                                                                        $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
                                                                                        $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
                                                                                        $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
                                                                                        $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
                                                                                        $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
                                                                                        $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        $replace_ary['szTermsConditionForReferralForwarderText']='';
                                                                                    }
                        
                                                                                    if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==1)
                                                                                    {
                                                                                        createEmail(__COURIER_BOOKING_RECEIVED_CREATE_LABEL_FORWARDER_EMAIL__, $replace_ary, $replace_ary['szEmail'],'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                                                                    }
                                                                                    else 
                                                                                    {
                                                                                        if(doNotSendMailToThisDomainEmail($replace_ary['szEmail']))
                                                                                        {
                                                                                            createEmail(__FORWADER_COURIER_BOOKING_RECEIVED__, $replace_ary, $replace_ary['szEmail'],'' ,$forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                                                                        }
                                                                                        /*
                                                                                        * FW: E-mail change 19-01-2017
                                                                                        */
                                                                                        //createEmail(__CREATE_LABEL_FOR_COURIER_BY_TRANSPORTECA_EMAIL__, $replace_ary, __STORE_SUPPORT_EMAIL__,'' , $forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                                                                                    }
                                                                                }  
                                                                            } 
                                                                        } catch (Exception $ex) {
                                                                            $bExceptionFlag = true;
                                                                            $szLogingLogStr .= " Got Exception in processing Courier Booking with bank trasfer ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                                                        } 
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if($iUpdateBankTransfer)
                                                                    {  
                                                                        try
                                                                        {
                                                                            $kConfig = new cConfig();
                                                                            $kForwarderContact= new cForwarderContact();
                                                                            $kUser = new cUser();
                                                                            $kWhsSearch = new cWHSSearch();
                                                                            $kBooking->SendTransferConfirmation($bookingArr);
                                                                        } catch (Exception $ex) { 
                                                                            $bExceptionFlag = true;
                                                                            $szLogingLogStr .= " Got Exception in sending confirmation email: ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                                                        } 
                                                                    } 
                                                                    if($checkComapnyBankDetils)
                                                                    {
                                                                        $replace_ary=array();
                                                                        $ForwarderAdminDetails = $kAdmin->findForwarderAdminsByBookingId($idForwarder);
                                                                        $replace_ary['szDate']=findPaymentFreq($ForwarderAdminDetails[0]['szPaymentFrequency']);
                                                                        if($ForwarderAdminDetails!= array() && $bookingArr['idBookingStatus']=='3')
                                                                        {
                                                                            foreach($ForwarderAdminDetails AS $key=>$value)
                                                                            {
                                                                                $replace_ary['szFirstName']=$value['szFirstName'];
                                                                                $replace_ary['szDisplayName']=$value['szDisplayName'];
                                                                                $replace_ary['szHere']='<a href ="http://'.$value['szControlPanelUrl'].'/Company/BankDetails/">here</a>';
                                                                                $to =  $value['szEmail'];
                                                                                try
                                                                                {
                                                                                    createEmail(__UPDATE_BANK_DETAILS_FORWARDER__, $replace_ary, $to,'' , __FINANCE_CONTACT_EMAIL__,$value['id'] , __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__);
                                                                                }
                                                                                catch (Exception $ex) { 
                                                                                    $szLogingLogStr .= " Got Exception sending email: __UPDATE_BANK_DETAILS_FORWARDER__ ".PHP_EOL.PHP_EOL.print_R($ex,true).PHP_EOL;
                                                                                    $bExceptionFlag = true;
                                                                                }
                                                                            }
                                                                        }
                                                                    } 
                                                                }
                                                                $filename = __APP_PATH_ROOT__."/logs/bookingReceivedLogs.log";
                                                                $strdata=array();
                                                                $strdata[0]=" \n\n /******************** Booking# ".$Booking_Id." Received on ".date('Y-m-d H:i:s')." \n\n";
                                                                $strdata[1] = $szLogingLogStr."\n\n"; 
                                                                file_log(true, $strdata, $filename);
                                                            }
                                                            $k++;
                                                        } 
                                                        else
                                                        {
                                                          $errorAry[$j]=$stripePaymentdata[$i];
                                                          $j++;
                                                        }
                                                    }
                                                    else
                                                    {
                                                       $errorAry[$j]=$stripePaymentdata[$i];
                                                       $j++;
                                                    }
                                               }
                                               else
                                               {
                                                   $bookingArr=array();
                                                   $bookingArr=$kBooking->getExtendedBookingDetails($Booking_Id);
                                                   $stripePaymentdata[$i][11]=$bookingArr['szBookingRef'];
                                                    $stripePaymentdata[$i][12]=$bookingArr['idBookingStatus'];
                                                    $stripePaymentdata[$i][13]=$bookingArr['szBookingRandomNum'];
                                                    
                                                   $errorAry[$j]=$stripePaymentdata[$i];
                                                   $j++;
                                               }
                                           }
                                           else
                                           {
                                                $errorAry[$j]=$stripePaymentdata[$i];
                                                $j++;
                                           }
                                        }
                                    }
                                    else
                                    {
                                       $errorflag = true;
                                       $errormsg='This seems uploaded csv is invaild .Currently only Stripe CSV files are supported .'; 
                                    }
                                }
                                else
                                {
                                    $errorflag = true;
                                    if(count($stripePaymentdata) == 1)
                                    {
                                      $errormsg='There are no payments found in the csv .';  
                                    }
                                    else
                                    {
                                        $errormsg='The uploaded csv is empty .';
                                    }
                                }
                            }
                            else 
                            {
                                $errorflag = true;
                                $errormsg='There is some issue in reading csv .';
                            }
                        }
                        else
                        {
                           $errorflag = true;
                           $errormsg='There is some upload error .';
                        }
                    }
                    else
                    {
                        $errorflag = true;
                        $errormsg='There is some upload error .';
                    }
                    if($errorflag)
                    {
                       echo 'ERROR||||';
                        ?>
                        <div id="popup-bg"></div>
                        <div id="popup-container" >
                            <div class="popup" > 
                                <h2>Upload CSV</h2>
                                <div class="clearfix" style="width:100%;">
                                    <div class="clearfix email-fields-container">
                                        <p style="color:red;"><?php echo $errormsg; ?></p>
                                    </div>
                                    <div class="btn-container"> 
                                        <p align="center">
                                            <a href="javascript:void(0);" id="close_button" class="button1" onclick="closeConactPopUp(0);"><span>CLOSE</span></a>
                                        </p>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php  
                    }
                    else if($k >0 || !empty($errorAry))
                    {
                        echo 'SUCCESS||||';
                        $style='';
                        if(!empty($errorAry))
                        {
                            $style='style="width:1000px;"';
                        }
                        ?>
                        <div id="popup-bg"></div>
                        <div id="popup-container" >
                            <div class="popup" <?php echo $style;?>> 
                                <h2>Complete</h2>
                                <div class="clearfix" >
                                    <div class="clearfix email-fields-container">
                                        <?php
                                            if($k>0)
                                            {
                                                $updatedmsg=$k.' bookings updated as payment received.';
                                                if($k==1)
                                                {
                                                    $updatedmsg=$k.' booking updated as payment received.';
                                                }
                                            }
                                            else
                                            {
                                                $updatedmsg='0 booking updated as payment received.';
                                            }
                                            $notupdatedmsg='';
                                            if(!empty($errorAry))
                                            {
                                               $notupdatedmsg ='Following '.count($errorAry).' lines not found .';
                                            }
                                        ?>
                                        <p><?php echo $updatedmsg.' '.$notupdatedmsg;?></p><br>
                                        <?php 
                                        if(!empty($errorAry))
                                        {
                                            ?>
                                        <div class="scrolling-div">
                                        <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;">
                                            <tr>
                                                <td width="5%" style="text-align:left"><strong>Type</strong></td>
                                                <td width="10%" style="text-align:left"><strong>ID</strong></td>
                                                <td width="10%" style="text-align:left"><strong>Created</strong></td>
                                                <td width="30%" style="text-align:left"><strong>Description</strong></td>
                                                <td width="10%" style="text-align:right"><strong>Amount</strong></td>
                                                <td width="5%" style="text-align:left"><strong>Currency</strong></td>
                                                <td width="10%" style="text-align:right"><strong>Booking</strong></td>
                                                <td width="10%" style="text-align:right"><strong>Booking status</strong></td>
                                            </tr> <?php
                                            foreach($errorAry as $error)
                                            {
                                                if($error['12']=='3' || $error['12']=='4')
                                                {
                                                    $BookingStatus="Booking Confirmed";
                                                }
                                                else
                                                {
                                                    $BookingStatus="Booking Cancelled";
                                                }
                                                ?>
                                                <tr>
                                                    <td width="5%" style="text-align:left"><?php echo $error['0']; ?></td>
                                                    <td width="10%" style="text-align:left"><?php echo $error['1']; ?></td>
                                                    <td width="10%" style="text-align:left"><?php echo $error['2']; ?></td>
                                                    <td width="30%" style="text-align:left"><?php echo mb_substr($error['3'],0,50,'UTF8'); ?></td>
                                                    <td width="10%" style="text-align:right"><?php echo $error['4']; ?></td>
                                                    <td width="10%" style="text-align:left"><?php echo $error['5']; ?></td>
                                                    <td width="10%" style="text-align:right"><a href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__."".$error['13']?>/" style="font-weight:normal;"><?php echo $error['11']; ?></a></td>
                                                    <td width="10%" style="text-align:right"><?php echo $BookingStatus; ?></td> 
                                                </tr>
                                                <?php
                                            }
                                            ?> 
                                            </table>
                                        </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="btn-container"> 
                                        <p align="center">
                                            <?php 
                                            if($k>0)
                                            {
                                                $outstandingPaymentUrl=__MANAGEMENT_URL__.'/outstandingPayment/';
                                              ?>
                                                <a href="javascript:void(0);" id="close_button" class="button1" onclick="redirect_url('<?php echo $outstandingPaymentUrl;?>');"><span>CLOSE</span></a>
                                               <?php  
                                            }
                                            else
                                            {
                                                ?>
                                                <a href="javascript:void(0);" id="close_button" class="button1" onclick="closeConactPopUp(0);"><span>CLOSE</span></a>
                                                <?php
                                            }
                                            ?>
                                        </p>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php  
                    }
                }
            }
	
	
}	