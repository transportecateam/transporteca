<?php 
/**
  *  admin--- Search Notification management
  */
define('PAGE_PERMISSION','__DUMMY_SERVICES__'); 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Dummy Services";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base = "management/uploadService/";

?>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

<div id="hsbody-2">
    <div id="search_noftification_popup" style="display: none;"></div>
    <?php require_once(__APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php"); ?> 
    <div class="hsbody-2-right">
        <p><?php echo t($t_base.'messages/dummy_services_note'); ?></p>
        <br>
        <div class="clearfix">
            <div style="min-height:300px;" id="dummy_service_main_container"> 
                <?php echo displayDummyServiceListAddEditForm(); ?>
            </div>
            <hr class="cargo-top-line"> 
            <p><?php echo t($t_base.'title/dummy_service_title_exception'); ?></p><br>
            <div style="min-height:300px;" id="dummy_serviceexception_main_container"> 
                <?php echo displayDummyServiceExceptionDetails(); ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="deleteDummyServiceId" id="deleteDummyServiceId" value="">
    <input type="hidden" name="idPrimarySortKey" id="idPrimarySortKey" value="">
    <input type="hidden" name="idPrimarySortValue" id="idPrimarySortValue" value="ASC">
    <input type="hidden" name="idOtherSortKey" id="idOtherSortKey" value="">
    <input type="hidden" name="iServiceDummy" id="iServiceDummy" value="1">
    
    <input type="hidden" name="deleteDummyServiceExceptionId" id="deleteDummyServiceExceptionId" value="">
    <input type="hidden" name="idPrimarySortKey" id="idPrimarySortKey_exception" value="">
    <input type="hidden" name="idPrimarySortValue" id="idPrimarySortValue_exception" value="ASC">
    <input type="hidden" name="idOtherSortKey" id="idOtherSortKey_exception" value="">
    <input type="hidden" name="iServiceDummy" id="iServiceDummy_exception" value="1">
</div>    
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); 
?>