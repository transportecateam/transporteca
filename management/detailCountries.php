<?php 
/**
  *  admin--- maintanance -- contries
  */
define('PAGE_PERMISSION','__SYSTEM__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Variables - Countries";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/country/";
$countryDetails=$kAdmin->countries($idAdmin);

$countrriessPageFlag=checkManagementPermission($kAdmin,"__SYSTEM_COUNTRY__",2);
?>
<div id="ajaxLogins" style="display: none;">
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup">
            <h5><?=t($t_base.'messages/remove_link');?></h5>
            <p id="view_message"><?=t($t_base.'messages/would_you_like');?></p><br/>
            <p align="center"><a href="javascript:void(0)" id="removeData" class="button1" ><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancelDelete();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
        </div>
    </div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
            <?php
                if($countrriessPageFlag){?>
		<div id="table">
                    <?php echo countriesView($countryDetails); ?>		
		</div>		
		<div style="padding-top: 10px;">
			<div style="float: right;">
				 <span id="cancel"></span>
				 <a class="button2" id="deleteExchangeRates" style="opacity:0.4;"><span><?=t($t_base.'fields/cancel');?></span></a>
				<a class="button1" id="editExchangeRates" style="opacity:0.4;"><span><?=t($t_base.'fields/edit');?></span></a>
			</div>
			<div style="clear: both;padding-top: 10px;"></div>
			<div id="error" ></div>
			<form id="sendExchange">
			<div id="editCurrency">
				<?=countriesText();?>
			</div>
			</form>
		</div>
                <?php }?>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>