<?php 
/**
  *  Send Message to forwarder
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kUploadBulkService = new cUploadBulkService();
$t_base_error ="Forwarders/BulkUpload/";
$t_base="management/uploadService/";
if(isset($_REQUEST['mode']))
{
	$mode = trim(sanitize_all_html_input($_REQUEST['mode']));
	SWITCH($mode)
	{
		CASE 'EDIT_PRICE_DETAILS':
		{
			$id  = (int)sanitize_all_html_input($_POST['id']);
			$bottomTable=$kUploadBulkService->getDetailsPricingServicing($id);
			estimationContent($bottomTable);
			BREAK;
		}
		CASE 'SAVE_PRICE_DETAILS':
		{
			$id  = (int)sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->saveDetailsPricingServicing($_POST['updatePricingAry'],$id);
				
				if(!empty($kUploadBulkService->arErrorMessages))
				{
					echo 'ERROR|||||';
				?>
				<div id="regError" class="errorBox" style="display:block;">
				<div class="header"><?=t($t_base_error.'title/please_following');?></div>
				<div id="regErrorList">
				<ul>
				<?php
				      foreach($kUploadBulkService->arErrorMessages as $key=>$values)
				      {
				      ?><li><?=$values?></li>
				      <?php 
				      }
				?>
				</ul>
				</div>
				</div>
				<?
				}
				else
				{	
					echo "SUCCESS|||||";
					estimationContent(array());
				}	
			BREAK;
		}
		CASE 'VIEW_FILE': 
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->load($id);
			$filenameArr=$kUploadBulkService->bulkUploadServiceFiles($id);
			?>
				<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup signin-popup signin-popup-verification" style="width:400px;">
						<p><b><?=t($t_base.'fields/view_file_submitted')?> <?=date('j. F Y',strtotime($kUploadBulkService->dtSubmitted))?></b></p>
						<br/>
						<table width="100%" border="0">
						<?
							if(!empty($filenameArr))
							{
								foreach($filenameArr as $filenameArrs)
								{
								
									$filenameexist=__UPLOAD_FORWARDER_BULK_SERVICES__."/".$filenameArrs['szFileName'];
									
									if(file_exists($filenameexist))
									{
										$newFileName='';
										$file_nameArr=explode(".",$filenameArrs['szOriginalName']);
										if(strlen($file_nameArr[0])>40)
										{
											$newFileName=substr($file_nameArr[0],0,30)."... .".$file_nameArr[1];
										}
										else
										{
											$newFileName=$filenameArrs['szOriginalName'];
										}
							?>
										<tr>
											<td align="left"><a href="javascript:void(0)" onclick="download_uploaded_file_admin('<?=$filenameArrs['szFileName']?>')"><img src="<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>/images/forwarders/Download.png" width="20px"></a></td>
											<td align="left"><?=$newFileName?> (<?=round($filenameArrs['iSize'])?> kB)</td>
										</tr>
						<? }}}
						else{ ?>
							<tr>
								<td colspan="2"><?php echo "No File Found."; ?></td>
							</tr> 
						<? }?>
						</table>
						<br/>
						<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancel_processing_cost_by_admin();" id="cancel_cost_processing"><span><?=t($t_base.'fields/close')?></span></a></p>
						
						</div>
					</div>	
<?		
			BREAK;
		}
		CASE 'download_file':
		{
			$filename=$_REQUEST['filename'];
			$fileurl=__UPLOAD_FORWARDER_BULK_SERVICES__."/$filename";
			header("Content-Type: application/octet-stream");
			header('Content-Disposition: attachment: attachment; filename='.$filename);
			ob_clean();
			flush();
			readfile($fileurl);
			exit;
			BREAK;
		}
		CASE 'VIEW_RECORDS': 
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->load($id);
			$filenameArr=$kUploadBulkService->bulkUploadServiceRecords($id);
			?>
				<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup signin-popup signin-popup-verification" style="width:400px;">
						<p><b>View files submitted <?=date('j. F Y',strtotime($filenameArr['dtUploaded']))?></b></p>
						<br/><br/>
						<table width="100%" border="0">
						<?
							if(!empty($filenameArr))
							{
						?>
									<tr>
										<td align="left"><?=$filenameArr['szActualFileName']?></td>
										<td align="left"><a href="javascript:void(0)" onclick="download_working_record_file('<?=$filenameArr['szFileName']?>')"><img src="<?=__BASE_URL_FORWARDER_LOGO__?>/Download.png" width="25px"></a></td>
									</tr>
						<? }
						else{ ?>
							<tr>
								<td colspan="2"><?php echo "No File Found."; ?></td>
							</tr> 
						<? }?>
						</table>
						<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancelEditMAnageVar(0);" id="cancel_cost_processing"><span><?=t($t_base.'fields/close')?></span></a></p>
						
						</div>
					</div>	
<?		
			BREAK;
		}
		CASE 'download_record_file':
		{
			$filename=$_REQUEST['filename'];
			$fileurl=__UPLOAD_FORWARDER_BULK_SERVICES__."/$filename";
			header("Content-Type: ".mime_content_type($fileurl));
			header('Content-Disposition: attachment: attachment; filename='.$filename);
			ob_clean();
			flush();
			readfile($fileurl);
			exit;
			BREAK;
		}
		CASE 'FIND_SUBMIT_STATUS':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->load($id);
			if($kUploadBulkService->iTotalRecords!=0 && $kUploadBulkService->iFileCount!=0 && $kUploadBulkService->iStatus==1)
			{
				echo true;
			}
			else
			{
				echo false;
			}
			BREAK;
		}
		CASE 'SUBMIT_PROCESSING_COST':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$status = $kUploadBulkService->findSubmitDtataProcessingCost($id);
			$kUploadBulkService->bulkSubmitServiceFiles($id,$status);
			if($status == __COST_AWAITING_APPROVAL__)
			{	
				$kUploadBulkService->sendEmailsToForwarderAdminAndPricing($id);
			}
			else if($status == __COST_APPROVED_BY_FORWARDER__)
			{	
				$kUploadBulkService->sendEmailsToForwarderAdminAndPricingFree($id);
			}
			BREAK;
		}
		CASE 'MOVE_BACK_SERVICE':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->bulkMoveBackServiceFiles($id);
			BREAK;		
		}
		CASE 'MOVE_BACK_SERVICE_CHECK':
		{
			$id=sanitize_all_html_input($_POST['id']);
			echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/move_sub_one').'</b></h5>
							<p>'.t($t_base.'title/are_u_sure').'</p>
							<br />
							<div align="center">
							<a class="button1" onclick="move_back_processing_cost_confirm(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
							</div>
						</div>
					</div>';
			BREAK;	
		}
		CASE 'MOVE_BACK_SUBMISSION':
		{
			$id=sanitize_all_html_input($_POST['id']);
			echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/move_sub_one_back').'</b></h5>
							<p>'.t($t_base.'title/are_u_sure_you_wish').'</p>
							<br />
							<div align="center">
							<a class="button1" onclick="move_back_processing_cost_submission(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
							</div>
						</div>
					</div>';
			BREAK;	
		}
		CASE 'MOVE_BACK_SUBMISSION_CONFIRMED':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->bulkMoveBackSubmissionFiles($id);
			BREAK;		
		}
		CASE 'CHECK_ACTUAL_RECORDS':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->load($id);
			if($kUploadBulkService->iActualRecords>0)
			{	
				
				echo true;
			}
			else
			{
				echo false;
			}	 
			BREAK;
		}
		CASE 'COMPLETE_SUBMIT_RECORDS':
		{
			$id=sanitize_all_html_input($_POST['id']);
			echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/submit_for_review').'</b></h5>
							<p>'.t($t_base.'title/are_you_sure_to_approve_for_forwarder').'</p>
							<br />
							<p>'.t($t_base.'title/if_there_is_processing_fee').'</p>
							<br />
							<div align="center">
							<a class="button1" id="complete_submission_confirm" onclick="complete_submission(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" id="complete_submission_cancel" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
							</div>
						</div>
					</div>';
			BREAK;
		}
		CASE 'COMPLETE_SUBMIT_CONFIRMED':
		{
			$id=sanitize_all_html_input($_POST['id']);
			if($kUploadBulkService->completeSubmissionFiles($id))
			{
				$status = $kUploadBulkService->findSubmitDtataProcessingCostApproval($id);
				$kUploadBulkService->load($id);
				if($kUploadBulkService->iCompleted==0)
				{
					if($status>0.00)
					{
						$kUploadBulkService->forwarderTansaction($id);
						
					}
					else
					{
						$kUploadBulkService->sendEmailsToForwarderWorkingProfile($id,__COST_CONFIRMED_FOR_UPLOAD_SERVICE_FROM_TRANSPORTECA_FREE__);
					}
				}
			}
			
			BREAK;
		}
		CASE 'MOVE_BACK_COMPLETE':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->bulkMoveBackCompletedFiles($id);
			BREAK;
		}
		CASE 'OPEN_COMMENTS':
		{
			
			$id=sanitize_all_html_input($_POST['id']);
			$allCommentArr=$kUploadBulkService->bulkUploadServiceComments($id);
			$kForwarder = new  	cForwarder();
			showUploadServiceCommentForAdmin($allCommentArr,$id);
			BREAK;
		}
		CASE 'add_comment':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$szComment=sanitize_all_html_input($_POST['szComment']);
			$kUploadBulkService->addComment($id,$szComment);
			
			$allCommentArr=$kUploadBulkService->bulkUploadServiceComments($id);
			$kForwarder = new  	cForwarder();
			showUploadServiceCommentForAdmin($allCommentArr,$id);
			BREAK;
		}
		CASE 'DELETE_POPUP':
		{
			$id=sanitize_all_html_input($_POST['id']);
			echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/delete_submission').'</b></h5>
							<p>'.t($t_base.'title/are_u_sure_delete').'</p>
							<br />
							<div align="center">
							<a class="button1" onclick="delete_confirm(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
							</div>
						</div>
					</div>';
			BREAK;	
		}
		CASE 'DELETE_POPUP_CONFIRM':
		{
			$id=sanitize_all_html_input($_POST['id']);
			//$kUploadBulkService->deleteUploadService($id);
			$kUploadBulkService->updateUploadService($id);
			BREAK;
		}
		CASE 'CHECK_RECORDS_LISTING':
		{
			$id=sanitize_all_html_input($_POST['id']);
			$kUploadBulkService->load($id);
			if($kUploadBulkService->iActualRecords>0)
			{
				echo true;
			}
			else
			{
				echo false;
			}
			BREAK;
		}
		DEFAULT :
		{
			BREAK;
		}
	}

}

?>