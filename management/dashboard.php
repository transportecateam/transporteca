<?php 
/**
  *  Admin--- Dashboard
  */
define('PAGE_PERMISSION','__DASHBOARD__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Dashboard";
define('PAGE_NAME','ADMIN_DASHBOARD');
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();

$t_base	= "management/dashboard/";
$kDashboardAdmin = new cDashboardAdmin();
$imgArr = $kDashboardAdmin->listAllDashboardDetails(); 
?> 
<div id="hsbody">
    <div style="direction: ltr;">
        <div style="float:left;">
            <h4><strong><?=t($t_base."title/title1");?></strong></h4>
            <img style="margin-left:-19px;" src="<?=__MANAGEMENT_URL__."/".$imgArr[0]."?n=".time(); ?>" alt="<?=t($t_base."title/title1");?>">
        </div>
        <div style="float:right;">
            <h4><strong><?=t($t_base."title/title9");?></strong></h4>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[8]."?n=".time(); ?>" alt="<?=t($t_base."title/title9");?>">
        </div>
    </div>
    <div class="clear-all"></div>
    <br />
    <div style="direction: ltr;">
        <div style="float:left;">
            <h4><strong><?=t($t_base."title/title5");?></strong></h4>
            <img style="margin-left:-19px;" src="<?=__MANAGEMENT_URL__."/".$imgArr[4]."?n=".time();?>" alt="<?=t($t_base."title/title5");?>">
        </div>
        <div style="float:right;">
            <h4><strong><?=t($t_base."title/title3");?></strong></h4>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[2]."?n=".time(); ?>" alt="<?=t($t_base."title/title3");?>">
        </div> 
    </div>
    <div class="clear-all"></div>
    <br /> 
    <div style="direction: ltr;">
        <div style="float:left;">
            <h4><strong><?=t($t_base."title/title7");?></strong></h4>
            <img style="margin-left:-19px;" src="<?=__MANAGEMENT_URL__."/".$imgArr[6]."?n=".time();?>" alt="<?=t($t_base."title/title7");?>">
        </div> 
        <div style="float:right;">
            <h4><strong><?=t($t_base."title/title12_new");?></strong>&nbsp;<a href="javascript:void(0)" style="font-style: italic;font-weight: normal;font-size:12px;" onclick="downloadGraphDataList('MANUAL_RFQ','<?php echo __MAIN_SITE_HOME_PAGE_SECURE_URL__;?>');">6 month data</a></h4>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[11]."?n=".time();?>" id="img_i" alt="<?=t($t_base."title/title12_new");?>">
        </div>
    </div> 
    <div class="clear-all"></div>
    <br /> 
    <div style="direction: ltr;">
        <div style="float:left;">
            <h4><strong><?=t($t_base."title/title11_new");?></strong>&nbsp;<a href="javascript:void(0)" style="font-style: italic;font-weight: normal;font-size:12px;" onclick="downloadGraphDataList('CALL_UESR','<?php echo __MAIN_SITE_HOME_PAGE_SECURE_URL__;?>');">6 month data</a> </h4>
            <img style="margin-left:-19px;" src="<?=__MANAGEMENT_URL__."/".$imgArr[10]."?n=".time();?>" alt="<?=t($t_base."title/title11_new");?>">
        </div> 
        <div style="float:right;">
            <h4><strong><?=t($t_base."title/title13_new");?></strong>&nbsp;<a href="javascript:void(0)" style="font-style: italic;font-weight: normal;font-size:12px;" onclick="downloadGraphDataList('READ_MESSAGE','<?php echo __MAIN_SITE_HOME_PAGE_SECURE_URL__;?>');">6 month data</a></h4>
            <img src="<?=__MANAGEMENT_URL__."/".$imgArr[12]."?n=".time();?>" id="img_i" alt="<?=t($t_base."title/title13_new");?>">
        </div>
    </div> 
    <div class="clear-all"></div> 
    <br /> 
    <div style="direction: ltr;">
        <div style="float:left;">
            <h4><strong><?=t($t_base."title/title14_new");?></strong>&nbsp;<a href="javascript:void(0)" style="font-style: italic;font-weight: normal;font-size:12px;" onclick="downloadGraphDataList('BOOKING_LIST','<?php echo __MAIN_SITE_HOME_PAGE_SECURE_URL__;?>');">6 month data</a></h4>
            <img style="margin-left:-19px;" src="<?=__MANAGEMENT_URL__."/".$imgArr[13]."?n=".time();?>" alt="<?=t($t_base."title/title14_new");?>">
        </div> 
        
    </div> 
    <div class="clear-all"></div>
</div> 

<script type="text/javascript">
    var height = document.getElementById("img_i").naturalHeight; 
    var width = document.getElementById("img_i").naturalWidth;  
</script>
    
<?php

include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );

?>
