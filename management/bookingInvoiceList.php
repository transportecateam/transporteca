<?php 
/**
  *BILLING DETAILS
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Accounts Receivable - Settled";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/billings/";
$checkPostFlag=true;
if(empty($_POST['bookingSearchInvoiceAry'])){
$_POST['bookingSearchInvoiceAry']['dtToDate']="30/06/2016";
    $_POST['bookingSearchInvoiceAry']['dtFromDate']="01/07/2015";
    $checkPostFlag=false;
}
$billingSettledDetails=$kAdmin->showBookingConfirmedInvoiceList($_POST['bookingSearchInvoiceAry']); 

$pdf=new HTML2FPDFBOOKING();
?>
<div id="hsbody-2">
	
	<?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>
	
		<div id="checkDetails" class="hsbody-2-right">
                <form  id="forwarder_booking_seach_form" method="post" name="forwarder_booking_seach_form">
	
		<div class="oh">
				
			
		<p class="fl-30">
			<span class="f-size-12"><?=t($t_base.'fields/from_date');?></span><br />
			<span id="origin_warehouse_span">
                            <input readonly="" type="text" id="datepicker1" name="bookingSearchInvoiceAry[dtFromDate]" value="<?=$_POST['bookingSearchInvoiceAry']['dtFromDate']?>">
			</span>
		</p>	
		
		<p class="fl-30">
			<span class="f-size-12"><?=t($t_base.'fields/to_date');?></span><br />
			<span id="origin_warehouse_span">
				<input readonly="" type="text" id="datepicker2" name="bookingSearchInvoiceAry[dtToDate]" value="<?=$_POST['bookingSearchInvoiceAry']['dtToDate']?>">
			</span>
		</p>	
				
			
		
		<p class="fr-40" align="right"><br />
			<a href="javascript:void(0)" onclick="serch_forwarder_invoice_booking()" class="button1"><span><?=t($t_base.'fields/search');?></span></a>
			
		</p>
	</div>	
</form>    
    <script type="text/javascript">
	function initDatePicker() 
	{
		$("#datepicker1").datepicker();
		$("#datepicker2").datepicker();
	}
	initDatePicker();
</script>
		<?php
                //print_r($billingSettledDetails);
                if(!empty($billingSettledDetails) && $checkPostFlag)
                {
                    foreach($billingSettledDetails as $data)
                    {
                        $kBooking = new cBooking();
                        $kBooking->load($data['idBooking']);
                        
                        if($kBooking->iFinancialVersion==2)
                        {
                            $bookingInvoiceHtml = getInvoiceConfirmationPdfFileHTML_v2($data['idBooking'],$pdfhtmlflag,false,true);
                        }else{
                            $bookingInvoiceHtml = getInvoiceConfirmationPdfFileHTML($data['idBooking'],$pdfhtmlflag,false,true);
                        }
                        $pdf->AddPage('',true);
		
                        $pdf->WriteHTML($bookingInvoiceHtml,true);
		
                    }
                    
                    $file_name = __APP_PATH_ROOT__."/invoice/Transporteca-Forwarder-Booking-Invoice.pdf"; 
                    if(file_exists($file_name))
                    {
                            @unlink($file_name);
                    }

                    $pdf->Output($file_name,'F');
                    
                    ob_clean();
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename=Transporteca-Forwarder-Booking-Invoice.pdf');
                    ob_clean();
                    flush();
                    readfile($file_name);
                }
                
                if($checkPostFlag && empty($billingSettledDetails))
                {
                    echo "No Record Found";
                }
                ?>
</div>
</div>
<input type="hidden" id="szBookingStatusFlag" name="szBookingStatusFlag" value="SettledPaymentDetails">
<div id="ajaxLogin"></div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>