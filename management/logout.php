<?php 
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH_LAYOUT__."/ajax_admin_header.php" );
 
$_SESSION['admin_id']='';
unset($_SESSION['admin_id']);
header("Pragma","no-cache");
header("Cache-Control","no-store");
header("Expires", 0);
header('Location:'.__MANAGEMENT_URL__);
exit;
?>