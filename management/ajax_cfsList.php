<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement_ajax();
$t_base="management/cfsLoctation/";

$mode = sanitize_all_html_input($_REQUEST['mode']);
if($mode=='UPDATE_COUNTRY_LIST')
{
	$kExport_Import = new cExport_Import();
	$idForwarder = sanitize_all_html_input($_REQUEST['forwarder_id']);
	if($idForwarder>0)
	{
		$forwardCountriesArr=$kExport_Import->getForwaderCountries($idForwarder);
		?>
		<select name="currentHaulage[idCountry]" id="idOriginCountry"  style="width: 150px;">
			<option value=""><?=t($t_base.'fields/all');?></option>
			<?php
				if(!empty($forwardCountriesArr))
				{
					foreach($forwardCountriesArr as $forwardCountriesArrs)
					{
						?>
						<option value="<?=$forwardCountriesArrs['idCountry']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
						<?
					}
				}
			?>
		</select>
		<?php 
	}
	else 
	{
		$kConfig = new cConfig();
		$forwardCountriesArr=$kConfig->getAllCountryInPair();
		
		?>
		<select name="currentHaulage[idCountry]" id="idOriginCountry"  style="width: 150px;">
			<option value=""><?=t($t_base.'fields/all');?></option>
			<?php
				if(!empty($forwardCountriesArr))
				{
					foreach($forwardCountriesArr as $forwardCountriesArrs)
					{
						?>
						<option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
						<?
					}
				}
			?>
		</select>
		<?php 
	}
}
?>