<?php 
/**
  *  Add,Edit  Blog page 
  * 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Setup Knowledge Centre Section";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/layout/admin_header.php");
validateManagement();
$t_base="management/blog/";
?>

<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
			<?=createNewBlogPage($t_base);?>	
	</div>
	
<?php include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); ?>	