<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$t_base="management/Error/";
validateManagement();
if(isset($_POST))
{
    $id=sanitize_all_html_input($_POST['id']);
    if($_POST['mode']=='VIEW_DETAIL')
    {	
        $detail=$kAdmin->countriesView($idAdmin,$id);
        countriesDetails($detail);
    }
    if($_POST['mode']=='SAVE_COUNTRIES')
    {	
        if($_POST['arrCountry'] && $id>0)
        {	
            $exchange =$_POST['arrCountry'];
            $exchange['id'] = $_POST['id'];
            $data=$kAdmin->saveCountry($idAdmin,$exchange,$id);
        }
        if($_POST['arrCountry'] && $id==0)
        {	
            $exchange = $_POST['arrCountry'];
            $data=$kAdmin->addCountry($idAdmin,$exchange);
        } 
    }
    if($_POST['mode']=='DELETE_COUNTRY')
    {
        $deleteStatus=$kAdmin->deleteCountry($idAdmin,$id);
        if(!$deleteStatus)
        {
            showErrorMessage();
        }

    }
    if($_POST['mode']=='DOWNLOAD_DETAILS')
    {
        echo $kAdmin->downloadCountryDetails($idAdmin);
    }

    if($_POST['mode']=='SORT_COUNTRY_BY_ORDER')
    {
        $order = sanitize_all_html_input($_POST['order']);
        if($order == 'DESC')
        {
                $orderBy = true;
        }
        else
        {
                $orderBy = false;
        }

        $fieldForSorting = sanitize_all_html_input($_POST['field']);
        $countryDetails=$kAdmin->countries($idAdmin);
        /* print_r($fieldForSorting);
        echo '<br>';
        print_r($countryDetails[0]);	
        echo '<br>';
        print_r($orderBy);	
        */
        $countryDetails = sortArrayInsenstive($countryDetails,$fieldForSorting,$orderBy);
        countriesView($countryDetails);
    }
}
if(!empty($kAdmin->arErrorMessages))
{ 
    ?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
				foreach($kAdmin->arErrorMessages as $key=>$values)
				{
				?><li><?=$values?></li>
				<?php	
				}
			?>
			</ul>
			</div>
			</div>
			<?php  }
			if(!empty($kAdmin->arErrorMessages) && ($_POST['mode']=='SAVE_COUNTRIES'))
			{	
                            //$detail=$kAdmin->countriesView($idAdmin,$id);
                            echo "|||||";
                            countriesDetails($exchange);
			}
			if(empty($kAdmin->arErrorMessages) && (($_POST['mode']=='SAVE_COUNTRIES') ||($_POST['mode']=='DELETE_COUNTRY') ) )
			{	
				
				echo "|||||";
				countriesText();
				echo "|||||";
				$countryDetails=$kAdmin->countries($idAdmin);
				countriesView($countryDetails);
			}
			if($_POST['mode']=='CANCEL_COUNTRIES')
			{
				countriesText();
				echo "|||||";
				$countryDetails=$kAdmin->countries($idAdmin);
				countriesView($countryDetails);
			}
			?>