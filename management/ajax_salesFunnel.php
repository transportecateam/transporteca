<?php 
/**
  *  SHOW WAREHOUSE DETAILS 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement_ajax();

$kReport = new cReport();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
if($mode=='SHOW_MONTHLY_REPORT')
{
    $monthlyReportAry = array();
    $iBookingStep = (int)sanitize_all_html_input(trim($_REQUEST['booking_step']));
    $monthlyReportAry = $kReport->getMonthlyReport($iBookingStep);
    echo showMonthlyReportBottomTable($monthlyReportAry,$iBookingStep);
    die;
}
else if($mode=='SORT_SEARCHED_TRADE_REPORT')
{
    $monthlyReportAry = array();
    $szSortFeildName = sanitize_all_html_input(trim($_REQUEST['sort_key']));
    $szSortBy = sanitize_all_html_input(trim($_REQUEST['sort_by']));

    $searchedTradeAry = $kReport->getAllSearchedTradeSnapshot($szSortFeildName,$szSortBy);
    echo displaySearchedTradesData($searchedTradeAry,$szSortFeildName);
    die;
} 
else if($mode=='UPDATE_OPERATION_STATISTICS_SCREEN')
{
    $kReport = new cReport();
    if($kReport->updateOperationStatisticsScreen())
    {
        echo "SUCCESS||||";
        echo __MANAGEMENT_OPERATIONS__STATISTICS_URL__ ;
        die;
    }
}
else if(!empty($_REQUEST['visualizationAry']) && $mode=='VISUALIZATION')
{
	$iUniqueUserCount = 0;
	
	$customerImage = $kReport->getVisuaalizationReport($_REQUEST['visualizationAry']);
	$iUniqueUserCount = $kReport->getUniqueUserCount($_REQUEST['visualizationAry']);
	
	$notificationAry = $kReport->getNotificationCount($_REQUEST['visualizationAry']);
	
	$expactedCargoAry = $kReport->getExpectedCargoNotificationCount($_REQUEST['visualizationAry']);
	
	if(!empty($customerImage))
	{
		$t_base="management/visualization/";
		?>
		<img src="<?=__MANAGEMENT_URL__."/".$customerImage?>">
		<br /> 
		<br /> 
		<br /> 
		<div id="iBookingGates" class="oh">		
			<table border="1" class="fl-60" align="left">
				<tr>
					<td><strong><?=t($t_base.'titles/gate');?></strong></td>
				</tr>
				<tr>
					<td>1. <?=t($t_base.'titles/booking_step_1');?></td>
				</tr>
				<tr>
					<td>2. <?=t($t_base.'titles/booking_step_2');?></td>
				</tr>
				<tr>
					<td>3. <?=t($t_base.'titles/booking_step_3');?></td>
				</tr>
				<tr>
					<td>4. <?=t($t_base.'titles/booking_step_4');?></td>
				</tr>
				<tr>
					<td>5. <?=t($t_base.'titles/booking_step_5');?></td>
				</tr>
				<tr>
					<td>6. <?=t($t_base.'titles/booking_step_6');?></td>
				</tr>
				
				<tr>
					<td>7. <?=t($t_base.'titles/booking_step_7');?></td>
				</tr>
				<tr>
					<td>8. <?=t($t_base.'titles/booking_step_8');?></td>
				</tr>
				<tr>
					<td>9. <?=t($t_base.'titles/booking_step_9');?></td>
				</tr>
				<tr>
					<td>10. <?=t($t_base.'titles/booking_step_10');?></td>
				</tr>
				<tr>
					<td>11. <?=t($t_base.'titles/booking_step_11');?></td>
				</tr>
				<tr>
					<td>12. <?=t($t_base.'titles/booking_step_12');?></td>
				</tr>
				<tr>
					<td>13. <?=t($t_base.'titles/booking_step_13');?></td>
				</tr>
				<tr>
					<td>14. <?=t($t_base.'titles/booking_step_14');?></td>
				</tr>
			</table>
			<p class="fl-40">
				<table class="fl-40" align="right">
					<tr>
						<td><h3></h3>&nbsp;</td>
					</tr>
					<tr>
						<td>-----> <?=t($t_base.'titles/unique_user_text');?>: <?php echo number_format((int)$iUniqueUserCount)?> </td>
					</tr>
					<tr>
						<td>-----> <?=t($t_base.'titles/notify_when_available');?>: <?php echo number_format((int)$notificationAry['iCountNotificationType_1'])?> <!-- (<?=t($t_base.'titles/type')." ".__NOTIFICATION_TYPE_NO_SERVICE_COUNTRY_TO_COUNTRY___?>) --> </td>
					</tr>
					<tr>
						<td> -----> <?=t($t_base.'titles/notify_when_available');?>: <?php echo number_format((int)$notificationAry['iCountNotificationType_2_3_4'])?><!--(<?=t($t_base.'titles/type')." ".__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___.",".__NOTIFICATION_TYPE_NO_PICKUP_FROM_ORIGIN_COUNTRY___.",".__NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___?>)  --> </td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><br />-----> <?=t($t_base.'titles/notify_when_available');?>: <?php echo number_format((int)$notificationAry['iCountNotificationType_6_7'])?><!--(<?=t($t_base.'titles/type')." ".__NOTIFICATION_TYPE_S1_S2___.",".__NOTIFICATION_TYPE_T1_T2___?>)--> </td>
					</tr>
					<tr>
						<td>-----> <?=t($t_base.'titles/oversize_sent_to_forwarder');?>: <?php echo number_format((int)$expactedCargoAry['iCountExpactedEacgo'])?></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>-----> <?=t($t_base.'titles/notify_when_available');?>: <?php echo number_format((int)$notificationAry['iCountNotificationType_5'])?><!--(<?=t($t_base.'titles/type')." ".__NOTIFICATION_TYPE_S1_S2___.",".__NOTIFICATION_TYPE_T1_T2___?>)--> </td>
					</tr>
				</table>
			</p>
		</div>
		<?php
		
		$file_path = __APP_PATH__.'/management/'.$customerImage;
		if(file_exists($file_path))
		{
			//unlink($file_path);
		}
	}
	else
	{
		?>
		<h4>No data to display.</h4>
		<?php 
	}
}
else if(!empty($_REQUEST['visualizationAry']) && $mode=='SELLING_TRADE')
{
	$sellingTradeAry = array();
	$sellingTradeAry = $kReport->getSellingTradeReport($_REQUEST['visualizationAry']);
	echo showSellingTradeBottomTable($sellingTradeAry);
	die;
}
?>