<?php 
/**
  *  Admin--- Cancel Booking
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");

require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kBooking = new cBooking;
$t_base="management/operations/confirmation/";
if($_REQUEST['flag']=='open_cancel_booking_popup')
{
    $idBooking=$_REQUEST['id'];
    $mode = trim($_REQUEST['mode']);
    $bookingAry = $kBooking->getExtendedBookingDetails($idBooking); 

    $courLabelFee='0.00';
    if($bookingAry['iCourierBooking']==1)
    {
        $kCourierServices = new cCourierServices();
        $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);

        if($courierBookingArr[0]['iCourierAgreementIncluded']==1)
        {
            $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate'];
            if($courierBookingArr[0]['idBookingLabelFeeCurrency']!=1)
            {
                $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate']/$courierBookingArr[0]['fBookingLabelFeeROE'];
            }

        }

    }
    if($bookingAry['iTransferConfirmed']==1 && $mode!='PENDING_TRAY')
    {
            $mode = 'NOT_CONFIRMED_BOOKING';
    }
?> 
    <div id="popup-bg"></div>
    <div id="popup-container">
    <div class="popup signin-popup signin-popup-verification" style="width:500px;">
        <form name="cancelBooking" id="cancelBooking" method="post">
            <h5><?=t($t_base.'title/cancel_booking')?> <?=$bookingAry['szBookingRef']?></h5>
            <div id="regError" class="errorBox" style="display:none;"></div>
            <?php if($bookingAry['idBookingStatus']==__BOOKING_STATUS_CANCELLED__){
                ?>
                <p><?=t($t_base.'title/booking_is_already_cancelled')?></p><br />
                <p align="center"><a href="javascript:void(0)" class="button1" onclick="close_cancel_booking_popup_already_cancelled();" id="cancel_booking_no"><span><?=t($t_base.'fields/close')?></span></a></p>
                <?php
            }else{
		 if($bookingAry['iTransferConfirmed']==1){?>
			<p><?=t($t_base.'title/are_you_sure_you_wish_to_cancel_this_booking')?></p><br>
			<p ><?=t($t_base.'title/are_you_sure_you_wish_to_cancel_this_booking_and_transfer')?></p><br>
			<input type="hidden" name="cancelBooingArr[iReferralFee]" id="iReferralFee" value="1">
		<?php } else {?>
		<p><?=t($t_base.'title/are_you_sure_you_wish_to_cancel_this_booking_and')?>:</p>
		<ol style="margin:0;padding:0 0 0 20px">
			<li><?=t($t_base.'title/title_line_1')?></li>
			<li><?=t($t_base.'title/title_line_2')?></li>
			<li><?=t($t_base.'title/title_line_3')?></li>
		</ol>
		<p><?=t($t_base.'title/title_line_4')?></p><br />
		<p><?=t($t_base.'title/title_line_6')?><br>
                    <textarea rows="3" cols="40" name="cancelBooingArr[szInvoiceComment]" id="szInvoiceComment" style="width:491px;"></textarea>
		</p>
                <input type="hidden" name="cancelBooingArr[iReferralFee]" id="iReferralFee" value="1"><br> 
		<?php }?>
		<p align="center">
		<input type="hidden" name="cancelBooingArr[idBooking]" id="idBooking" value="<?=$idBooking?>">
		<input type="hidden" name="mode" id="mode" value="<?=$mode?>">
		<input type="hidden" name="cancelBooingArr[szBookingRefNo]" id="szBookingRefNo" value="<?=$bookingAry['szBookingRef']?>">
		<input type="hidden" name="cancelBooingArr[idUser]" id="idUser" value="<?=$bookingAry['idUser']?>">
		<input type="hidden" name="cancelBooingArr[idForwarder]" id="idForwarder" value="<?=$bookingAry['idForwarder']?>">
		<input type="hidden" name="cancelBooingArr[fReferralFee]" id="fReferralFee" value="<?=round((float)$bookingAry['fReferalAmount'],2)?>">
                <input type="hidden" name="cancelBooingArr[fLabelFee]" id="fLabelFee" value="<?=round((float)$courLabelFee,2)?>">
		<a href="javascript:void(0)" class="button2" onclick="close_cancel_booking_popup();" id="cancel_booking_no"><span><?=t($t_base.'fields/no')?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_booking_data();" id="cancel_booking_yes"><span><?=t($t_base.'fields/yes')?></span></a></p>
            <?php }?>	
        </form>
	</div>
</div>
<?php
}
else if($_REQUEST['flag']=='cancel_booking_data')
{
	$mode = trim($_REQUEST['mode']); 
        $page=(int)$_POST['page'];
        $limit=$_POST['limit'];
	$t_base = "home/homepage/"; 
	if($kBooking->cancelBooking($_POST['cancelBooingArr']))
	{
		echo "SUCCESS||||";
		$t_base="management/operations/confirmation/";
		$kBooking = new cBooking;
		if($mode=='NON_ACKNOWLEDGEMENT')
		{
			$booking_searched_data = array();
			$t_base="management/operations/confirmation/";
			$booking_searched_data = $kBooking->search_all_forwarder_bookings_non_acknowledge();
			echo all_forwarder_booking_non_acknowledge_html($booking_searched_data,$t_base,true,$page,$limit);
			die;
		}
		else if($mode=='NOT_CONFIRMED_BOOKING')
		{
			$booking_searched_data = array();
			$t_base="management/operations/confirmation/";
			$booking_searched_data = $kBooking->search_all_forwarder_bookings(false,true);
			echo all_forwarder_booking_html($booking_searched_data,$t_base,false,true,$page,$limit);
			die;
		}
                else if($mode=='PENDING_TRAY')
                {
                    $bookingAry = $kBooking->getExtendedBookingDetails($_POST['cancelBooingArr']['idBooking']); 
                    $idBookingFile = $bookingAry['idFile'];
                    $kBooking->loadFile($idBookingFile); 
                    
                    echo display_pending_tray_overview($kBooking);
                    echo "||||";
                    echo display_pending_task_validate_form($kBooking);
                    die();
                }
		else
		{
			$booking_searched_data = array();
			$booking_searched_data = $kBooking->search_all_forwarder_bookings();
                        $booking_searched_data =sortArray($booking_searched_data,'dtBookingConfirmed',true);
			echo all_forwarder_booking_html($booking_searched_data,$t_base,true,false,$page,$limit);
			die;
		}
                
	}
	else if(!empty($kBooking->arErrorMessages))
	{
		echo "ERROR ||||" ;
	?>
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kBooking->arErrorMessages as $key=>$values)
	      {
		      ?><li><?=$values?></li>
		      <?php 
	      }
	?>
	</ul>
	</div>
	<?php
	}
}
else if($_REQUEST['flag']=='CUSTOMER_BOOKING_LIST_PAGINATION')
{
    $page=(int)$_POST['page'];
    $szBookingStatusFlag=trim($_POST['szBookingStatusFlag']);
    $limit=$_POST['limit'];
    if($szBookingStatusFlag=='Confirmed')
    {
        $booking_searched_data = array();
        $booking_searched_data = $kBooking->search_all_forwarder_bookings();
       // print_r($booking_searched_data);
        $booking_searched_data =sortArray($booking_searched_data,'dtBookingConfirmed',true);
        echo all_forwarder_booking_html($booking_searched_data,$t_base,true,false,$page,$limit);
        die;
    }
    else if($szBookingStatusFlag=='NotConfirmed')
    {
            $booking_searched_data = array();
            $t_base="management/operations/confirmation/";
            $booking_searched_data = $kBooking->search_all_forwarder_bookings(false,true);
            echo all_forwarder_booking_html($booking_searched_data,$t_base,false,true,$page,$limit);
            die;
    }
    else if($szBookingStatusFlag=='NotAcknowledge')
    {
        $booking_searched_data = array();
        $t_base="management/operations/confirmation/";
        $booking_searched_data = $kBooking->search_all_forwarder_bookings_non_acknowledge();
        echo all_forwarder_booking_non_acknowledge_html($booking_searched_data,$t_base,true,$page,$limit);
        die;
    }
    else if($szBookingStatusFlag=='CancelBooking')
    {   
        $t_base="management/operations/confirmation/";
        $kBooking = new cBooking;
        $booking_searched_data = $kBooking->search_all_forwarder_bookings(__BOOKING_STATUS_CANCELLED__);
        echo display_cancelled_booking_html($booking_searched_data,$t_base,$page,$limit);
        die();
        
    }
    else if($szBookingStatusFlag=='InsuranceConfirmed')
    {   
        $newInsuredBookingAry = array();
        $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(__BOOKING_INSURANCE_STATUS_CONFIRMED__);
        echo display_sent_insured_booking_listing($newInsuredBookingAry,__BOOKING_INSURANCE_STATUS_CONFIRMED__,$page,$limit);
        die();
    }
    else if($szBookingStatusFlag=='InsuranceCancelled')
    {   
        $newInsuredBookingAry = array();
        $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(__BOOKING_INSURANCE_STATUS_CANCELLED__);
        echo display_sent_insured_booking_listing($newInsuredBookingAry,__BOOKING_INSURANCE_STATUS_CANCELLED__,$page,$limit);
        die();
    }
    else if($szBookingStatusFlag=='forwardertransportecaInvoice')
    {
        $t_base="management/forwarder_transporteca_invoice/";
        $paymentDetails=$kBooking->paymentOfForwardersTransaction($idAdmin,true);
        forwarderTransportecaInvoiceListHtml($paymentDetails,$t_base,$page,$limit);
        die();
    }
    else if($szBookingStatusFlag=='SettledPaymentDetails')
    {
        $billingSettledDetails=$kAdmin->showBookingConfirmedSettledTransaction(); 
        showCustomerAdminBillingSettled($billingSettledDetails,$idAdmin,$page,$limit);
        die();
    }
    else if($szBookingStatusFlag=='paidForwarderInvoice')
    {
        $unpaidMarkupBookingsAry = $kAdmin->showForwarderPendingPaymentDetails(true); 
        displayPayForwarderInvoices($unpaidMarkupBookingsAry,true,$page,$limit);
        die();
    }
    else if($szBookingStatusFlag=='payForwarderInvoice')
    {
        $unpaidMarkupBookingsAry = $kAdmin->showForwarderPendingPaymentDetails(); 
        displayPayForwarderInvoices($unpaidMarkupBookingsAry,false,$page,$limit);
        die();
    }
    else if($szBookingStatusFlag=='InsuranceSent')
    {
        $kInsurance = new cInsurance();
        $newInsuredBookingAry = array();
        $newInsuredBookingAry = $kInsurance->getInsuranceDataForNewBooking(__BOOKING_INSURANCE_STATUS_SENT__,$page); 
        echo display_sent_insured_booking_listing($newInsuredBookingAry,__BOOKING_INSURANCE_STATUS_SENT__,$page,$limit);
        die;
    }
}
else if($_REQUEST['flag']=='CUSTOMER_LABEL_LIST_PAGINATION')
{
    $page=(int)$_POST['page'];
    $pageName=trim($_POST['pageName']);
    $limit=$_POST['limit'];
    $kCourierServices = new cCourierServices();
    if($pageName=='LD')
    {
        $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier(0,__LABEL_DOWNLOAD__);
        echo display_courier_confirmed_booking_listing_send($newCourierBookingAry,__COURIER_BOOKING_LABEL_DOWNLOADED_BY_SHIPPER__,$page,$limit);
    }
    else if($pageName=='LS')
    {
        $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier(0,__LABEL_SENT__);
        echo display_courier_confirmed_booking_listing_send($newCourierBookingAry,'',$page,$limit);
    }
    else
    {
        $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier();
        echo display_courier_confirmed_booking_listing($newCourierBookingAry,$page,$limit);
    }
}
else if($_REQUEST['flag']=='OPEN_SNOOZE_POPUP')
{
    
    $szFlag=trim($_REQUEST['szFlag']);
    $idBooking=(int)$_POST['idBooking'];
    $dtTimingDate = date('d/m/Y');
    $date_picker_argument = "minDate: +1" ;
    $date_picker_argument_defaultDate = "setDate: +1" ;
    
    $remindDateAry=format_reminder_date_time();
    
    $dtTimingDate = $remindDateAry['dtReminderDate'];
    $dtSnoozeTime = $remindDateAry['szReminderTime'];
    ?>
        <style>
            .ui-datepicker-today {
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
            }
        </style>
        <script type="text/javascript" defer="">
        var today = new Date();
        var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
        console.log(tomorrow);
        $("#dtSnooze").datepicker({ minDate: tomorrow,defaultDate: tomorrow,onSelect: function(dateText){           
            var curDate = dateText;
            if(curDate!='')
            {
                $("#cancel_booking_yes").attr('style','opacity:1');
                $("#cancel_booking_yes").unbind('click');
                $("#cancel_booking_yes").attr('onclick','');
                $("#cancel_booking_yes").click(function(){ confirm_snooze_data('<?php echo $szFlag;?>','<?php echo $idBooking;?>') });
            }
        } });
    </script>
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup signin-popup signin-popup-verification" style="width:300px;">
            <form name="cancelBooking" id="cancelBooking" method="post">
                <h5>Snooze</h5>
                <p>When would you like to display the alarm again, if the issue has not cleared?</p> <br />
                <input name="dtSnooze" id="dtSnooze" value="<?php echo date('d/m/Y',strtotime("+1 DAY"));?>" readonly="readonly" style="width:50%">
                <input name="dtSnoozeTime" id="dtSnoozeTime" value="<?php echo $dtSnoozeTime;?>" style="width:30%"><span style="color:#999;font-style: italic;width: 5%;">(hh:mm)</span>
                <br /><br />
                <p align="center"><a href="javascript:void(0)" class="button2" onclick="closePopup('contactPopup');" id="cancel_booking_no"><span>Cancel</span></a> <a href="javascript:void(0)" class="button1" onclick="confirm_snooze_data('<?php echo $szFlag;?>','<?php echo $idBooking;?>');" id="cancel_booking_yes"><span>Confirm</span></a></p>
            </form>
        </div>
    </div>
 <?php   
}
else if($_REQUEST['flag']=='CONFIRM_SNOOZE_POPUP_MSG')
{
    $szFlag=trim($_REQUEST['szFlag']);
    $dtSnooze=trim($_REQUEST['dtSnooze']);
    $dtSnoozeTime=trim($_REQUEST['dtSnoozeTime']);
    $idBooking=(int)$_POST['idBooking'];
    $kAdmin->insertSnoozeData($idBooking,$dtSnooze,$dtSnoozeTime,$szFlag);
    echo  showAdminChecks();
    die();
}
?>