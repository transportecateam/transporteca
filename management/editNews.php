<?php 
/**
  *  Edit News
  */
define('PAGE_PERMISSION','__MESSAGE_EDIT_NEWS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Edit News";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$kRss = new cRSS();

?>
<script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce_edit.js"></script>

	<div id="ajaxLogin"></div>
	<div id="hsbody-2">
	<? require_once(__APP_PATH__ ."/layout/admin_messages_left_nav.php"); ?>
		
	<div class="hsbody-2-right">
		<div id="content_body"></div>
		<div id="content">
		<?=newDetailLists()?>
		</div>
				
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>