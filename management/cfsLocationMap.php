<?php 
/**
  *  LIST OF ALL CFS ( WAREHOUSES ) ON THE MAP
  * 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | CFS Location -  Map";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
$kWHSSearch=new cWHSSearch();
validateManagement();
$warehouseDetails = $kWHSSearch->getAllWareHousesData($_POST['currentHaulage']);
$t_base	= "management/cfsmap";

if(!empty($warehouseDetails))
{
    foreach( $warehouseDetails as $key=>$value)
    {	
        if( $value != array() )
        {
            foreach($value as $key=>$value)
            {
                $var_pipelie_string .=	$value['idForwarder'].'|'.$value['szLatitude'].'|'.$value['szLongitude'].'|'.$value['szWareHouseName'].'|'.$value['szDisplayName']."<br />".$value['szWareHouseName']."<br />".$value['szAddress'].$value['szAddress2'].$value['szAddress3']."<br />".$value['szPostCode'].$value['szCity']."<br />".$value['szCountryName'].'|||||';
            }
        }
    }
    $var_pipelie_string = str_replace('\'','&#39;',substr($var_pipelie_string,0,-5));
}
else
{
    echo "<h3 align='center'>No CFS found as per your search criteria.</h3>";
    die;
}
constantApiKey();
?> 
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_CSS_MANAGEMENT_URL__?>/style.css" />
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/admin.js"></script>

<style type="text/css">
      #map_canvas { height: 100% }     
      .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
      .map-content p{font-size:12px !important;}
      .map_note p{font-size:12px;font-style:italic;display:inline-block;margin:0 0 5px; 0;color:#808080;}
 </style>
<script type="text/javascript"
  src="http://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
</script>
<script type="text/javascript">
    var originIcon = '<?=__BASE_STORE_IMAGE_URL__?>' + "/blue-marker.png";
    var map;
	// deafult value for longs and lats  
    var olat1 = 0 ;
    var olang1 = 0;
    function open_google_map_popup(id)
    {	
    	var pipe_line_string = '';
       	var result_ary = '<?=$var_pipelie_string?>'.split("|||||");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	initialize(result_ary);
    }
    	  
	function initialize(result_ary) 
	{	
	  var length_pipeline = result_ary.length;
	  resultFirst_ary = result_ary[0].split("|");
	  var origin_lat = resultFirst_ary[0];
	  var origin_long = resultFirst_ary[1];
	   var myLatLng = new google.maps.LatLng(origin_lat, origin_long);
	   var myOptions = {
		zoom: 2,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(origin_lat, origin_long)  
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);
	 
	  for(i=0;i<length_pipeline;i++)
	  {
	  	result_arr = result_ary[i].split("|");
	  	var forwarder = result_arr[0];
	  	var szLat = result_arr[1];
		var szLang = result_arr[2];
		var whs_name = result_arr[3];
		var origin_address = result_arr[4];
		var origin = new google.maps.LatLng(szLat, szLang); 
		var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:whs_name
	  	}); 
	     var oaddress = '<div class="map-content"><p>'+origin_address+'</p></div>';
		 var oinfowindow = new google.maps.InfoWindow();
		 oinfowindow.setContent(oaddress);
		 google.maps.event.addListener(
			originmarker, 
			'mouseup', 
			infoCallback(oinfowindow, originmarker)
		  );
	  }
	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
}
	$(document).ready(function(){
		open_google_map_popup();
	});
</script>
    
 <div id="map_popup_div" style="width: 97%;height: 97%;">			   
	  	<div id="map_canvas" style="width:700px;height:700px;border: 1px solid #C4BDA1;"></div>
</div>