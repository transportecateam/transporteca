<?php
ob_start();
session_start();
 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
  
$kExplain= new cExplain();
$t_base="ExplainPage/"; 
$levelExplainDataArr_page = array();


$mainTab=trim($_REQUEST['tabFlag']); 
if($_REQUEST['menuflag']=='agg')
{ 
    $idExplainData = sanitize_all_html_input(trim($_REQUEST['id']));
    $levelExplainDataArr_page = $kExplain->selectExplainId(false,false,false,$idExplainData); 

    if(!empty($levelExplainDataArr_page))
    {
        if(!empty($levelExplainDataArr_page['szMetaTitle']))
        {
                $szMetaTitle= $levelExplainDataArr_page['szMetaTitle'];
        }
        else
        {
                $szMetaTitle= __EXPLAIN_META_TITLE__;
        }
        if(!empty($levelExplainDataArr_page['szMetaKeyWord']))
        {
                $szMetaKeywords= $levelExplainDataArr_page['szMetaKeyWord'];
        }
        else
        {
                $szMetaKeywords= __EXPLAIN_META_KEYWORDS__;
        } 

        if(!empty($levelExplainDataArr_page['szMetaDescription']))
        {
                $szMetaDescription = $levelExplainDataArr_page['szMetaDescription'];
        }
        else
        {
                $szMetaDescription = __EXPLAIN_META_DESCRIPTION__;
        }  

        $szAggregatePageUrl=__MAIN_SITE_HOME_PAGE_SECURE_URL__.$_SERVER['REQUEST_URI']; 
        $szOpenGraphAuthor = $levelExplainDataArr_page['szOpenGraphAuthor'];
        $szOpenGraphImagePath = __MAIN_SITE_HOME_PAGE_URL__."/image.php?img=".$levelExplainDataArr_page['szOpenGraphImage']."&w=300&h=300&temp=explainLevel2";
    }
} 

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/preview_header_new.php" ); 
$t_base="ExplainPage/";
 
$showFlag=true;
   
if($_REQUEST['menuflag']=='agg')
{  
    $idExplainData=sanitize_all_html_input(trim($_REQUEST['id']));

    $levelExplainDataArr = array();
    $levelExplainDataArr = $levelExplainDataArr_page ;

    $toatlArr=$kExplain->selectPageDetailsUserEnd(false,false,$idExplainData);
    $totalCount=count($toatlArr); 
    
    if(!empty($toatlArr))
    {
        $i=0;
        foreach($toatlArr as $toatlArrs)
        {
            ++$i;
            if($levelExplainDataArr['id']==$toatlArrs['id'])
            {
                break;
            } 
        }
    }
    $toatlArr1=$kExplain->selectPageDetailsUserEnd($iLanguage,$mainTab);
	 
    if($i==1)
    {
        $totalCount1=$totalCount-1;
        $prevUrlText=$toatlArr1[$totalCount1]['szPageHeading'];
        $nextUrlText=$toatlArr1[1]['szPageHeading'];
        $prevUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$totalCount1]['szLink'];		
        $nextUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[1]['szLink'];

        $szPreviousLinkTitle = $toatlArr1[$totalCount1]['szLinkTitle'] ;
        $szNextLinkTitle = $toatlArr1[1]['szLinkTitle'] ;

    }
    else if($i==$totalCount)
    {
        $totalCount1=$totalCount-2;
        $prevUrlText=$toatlArr1[$totalCount1]['szPageHeading'];
        $nextUrlText=$toatlArr1[0]['szPageHeading'];
        $prevUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$totalCount1]['szLink'];
        $nextUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[0]['szLink'];

        $szPreviousLinkTitle = $toatlArr1[$totalCount1]['szLinkTitle'] ;
        $szNextLinkTitle = $toatlArr1[0]['szLinkTitle'] ;
    }
    else
    {
        $t=$i-2;
        $prevUrlText=$toatlArr1[$t]['szPageHeading'];
        $nextUrlText=$toatlArr1[$i]['szPageHeading'];
        $prevUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$t]['szLink'];
        $nextUrl=__BASE_URL__."/".$subUrl."/".$toatlArr1[$i]['szLink'];	

        $szPreviousLinkTitle = $toatlArr1[$t]['szLinkTitle'] ;
        $szNextLinkTitle = $toatlArr1[$t]['szLinkTitle'] ;
    }
	
    if(empty($levelExplainDataArr))
    {
        $showFlag=false;
    }
}   
		if($_REQUEST['menuflag']=='agg')
{			
        $currentOrder=$i;
        $prev=$currentOrder;
        if($currentOrder==1)
        {
                $prev=$totalCount;
        }
        else
        {
                $prev=$currentOrder-1;
        }
        $explainContentArr=array();
        $explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($levelExplainDataArr['id']);

        $next=$currentOrder+1;
        if($totalCount==$currentOrder)
        {
            $next=1;
        }
        ?>
        
          <div class="social-section-head clearfix">
                        <div class="text">
                                        <h1><?php echo $levelExplainDataArr['szPageHeading']?></h1>
                                        <h4><?php echo $levelExplainDataArr['szSubTitle']?></h4>
                                        <div class="link-container">
                                        <?php 
                                                if(!empty($explainContentArr))
                                                {
                                                        $strReplace = $_SERVER['REQUEST_URI'];
                                                        $szLinkLastChar = substr($strReplace, -1);  

                                                        $szTagUrl='';
                                                        if($szLinkLastChar=='/')
                                                        {
                                                                $stringLen = strlen($strReplace);
                                                                $szTagUrl = substr($strReplace,0,($stringLen-1)); 
                                                        } 

                                                        //$szTagUrl='';
                                                        $count_1 = count($explainContentArr);
                                                        foreach($explainContentArr as $explainContentArrs)
                                                        { 
                                                                $ctr++;  
                                                                if(!empty($explainContentArrs['szHeading']))
                                                                {
                                                                        ?> 
                                                                        <a href="<?php echo $szTagUrl."#".trim($explainContentArrs['szHeading']) ?>" name="<?php echo trim($explainContentArrs['szHeading']); ?>" title="<?php echo $explainContentArrs['szLinkTitle']?>"><?php echo $explainContentArrs['szHeading']?></a>
                                                                        <?php
                                                                        if($count_1!=$ctr)
                                                                        {
                                                                                echo " | ";
                                                                        }
                                                                }
                                                         }
                                                 }		
                                        ?>
                                        </div>
                                </div>	
                                <div class="image"><img src="<?php echo __BASE_EXPLAIN_LEVEL2_IMAGES_URL__."/".$levelExplainDataArr['szHeaderImage']; ?>" alt="<?php echo $levelExplainDataArr['szPictureDescription']?>" title="<?php echo $levelExplainDataArr['szPictureTitle']?>" ></div>
        </div>
</div>
<?php	

        if(!empty($explainContentArr))
        {
                foreach($explainContentArr as $explainContentArrs)
                {
                        $bgColor='';
                        $bgColor="background-color:".$explainContentArrs['szBackgroundColor'].""; 
                        ?> 
                        <div class="body-color <?php if($explainContentArrs['szBackgroundColor']=='#fff' || $explainContentArrs['szBackgroundColor']=='' || $explainContentArrs['szBackgroundColor']=='#ffffff'){?> white<?php }?>" style="<?php echo $bgColor;?>" id="<?php echo trim($explainContentArrs['szHeading']); ?>" name="<?php echo trim($explainContentArrs['szHeading']); ?>">
                            <div id="hsbody">
                            <?php if(!empty($explainContentArrs['szHeading'])){ ?>
                                    <h2 align="center" class="heading"><?php echo $explainContentArrs['szHeading']?></h2>
                            <?php }?>
                                <?php echo str_replace("&#39;","'",$explainContentArrs['szDescription']);?>
                            </div>
                        </div>
                        <?php					
                }
        }	 			
} 
 
?>
<div id="all_available_service" style="display:none;"></div>
<?php 
require_once( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>