<?php
define('PAGE_PERMISSION','__INSURANCE_RATES_BOOKING__');
ob_start();
session_start();
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$szMetaTitle='Transporteca | Insurance buy/sell rates';
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
$t_base = "management/uploadService/";
$t_base_bulk="BulkUpload/";
validateManagement();

$kInsurance = new cInsurance();
$insuranceBuyRateAry = array();
$insuranceBuyRateAry = $kInsurance->getAllInsuranceBuyRates(); 
$idDefaultBuyRate = (int)$insuranceBuyRateAry[0]['id'];

$insuranceSellRateAry = array();
$insuranceSellRateAry = $kInsurance->getInsuranceRates(__INSURANCE_SELL_RATE_TYPE__,$idDefaultBuyRate);

?>
<div id="insurance_rate_popup" style="display:none"></div> 
<div id="hsbody-2"> 
    <?php require_once(__APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php"); ?> 
    <div class="hsbody-2-right"> 
        <div style="float:left;width:66%;height:200px;"> 
            <?php
                echo display_insurance_buyer_listings(__INSURANCE_BUY_RATE_TYPE__,$insuranceBuyRateAry,$idDefaultBuyRate);
            ?> 
        </div>
        <div style="float:right;width:33%;height:200px;"> 
            <?php 
                echo display_insurance_buyer_listings(__INSURANCE_SELL_RATE_TYPE__,$insuranceSellRateAry); 
            ?>
        </div> 
        <div class="clear-all"></div>
    </div>
</div> 
<?php
    include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>