<?php
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kVatApplication = new cVatApplication();
$kCourierServices = new cCourierServices();
$mode=sanitize_all_html_input(trim($_POST['mode']));
$t_base="management/vatApplication/";


if($mode=='CHECK_FORWARDER_COUNTRY_EXISTS')
{
	if($kVatApplication->validateForwarderCountry($_POST['addForwarderCountryAry']))
	{
		echo "SUCCESS||||";
		echo addEditForwarderCountryHtml($kVatApplication);
		die;
	}
	else
	{
		echo "ERROR||||";
		echo addEditForwarderCountryHtml($kVatApplication);
		die;
	}

}
else if($mode=='ADD_FORWARDER_COUNTRY')
{
	if($kVatApplication->addForwarderCountryData($_POST['addForwarderCountryAry']))
	{
		unset($_POST['addForwarderCountryAry']);
		$_POST['addForwarderCountryAry']=array();
		echo "SUCCESS||||";
		echo forwarderCountriesHTML($kVatApplication);
		die;
	}
	else
	{
		echo "ERROR||||";
		echo addEditForwarderCountryHtml($kVatApplication);
		die;
	}
}
else if($mode=='DELETE_FORWARDER_COUNTRY_CONFIRM')
{
	$deleteIdstr=trim($_POST['deleteCountryForwardersId']);
        $deleteForwarderCountryIdsArr=explode(";",$deleteIdstr);
        $kConfig = new cConfig();
        if(!empty($deleteForwarderCountryIdsArr))
        {
            foreach($deleteForwarderCountryIdsArr as $values)
            {
                $countryNameArr[]=$kConfig->getCountryName($values);
            }
        }
        if(!empty($countryNameArr))
        {
            $countryStr=implode(", ",$countryNameArr);
            
            
        }    
        $str="country";
        if(count($countryNameArr)>1)
        {
            $str="countries";
        }
        
	?>
	<div id="popup-bg"></div>
		<div id="popup-container" >
		<div class="popup">
		<h3><?=t($t_base.'fields/delete_forwarder_country')." ".$str;?> </h3>
		<p><?=str_replace("szCountryfrom",$countryStr,t($t_base.'messages/delete_confirm_msg'));?></p>
		<br/>
		<p align="center">
			<a href="javascript:void(0)" onclick="delete_forwarder_country_data('<?php echo $deleteIdstr; ?>')" class="button1" ><span><?=t($t_base.'fields/yes');?></span></a> 
			<a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/no');?></span></a>
		</p>
	</div>
</div>
	<?php
}
else if($mode=='DELETE_FORWARDER_COUNTRY')
{
	$countryDeleteStr=$_POST['id'];
	 $deleteForwarderCountryIdsArr=  str_replace(";","','",$countryDeleteStr);
	$kVatApplication->deleteForwarderCountryData($deleteForwarderCountryIdsArr);
	//echo $deleteForwarderCountryIdsArr;
	
	echo forwarderCountriesHTML($kVatApplication);
	echo "||||";
	echo forwarderVatTradesHTML($kVatApplication);
	
}
else if($mode=='SHOW_TRADE_DATA')
{
	$id=(int)$_POST['id'];
	
	echo forwarderVatTradesHTML($kVatApplication,$id);
}
else if($mode=='COPY_FORWARDER_COUNTRY_CONFIRM')
{
	$id=(int)$_POST['id'];
	$szCountryName=sanitize_all_html_input(trim($_POST['szCountryName']));
	
	copyVatApplicationData($id,$szCountryName,$kVatApplication);
}
else if($mode=='COPY_FORWARDER_COUNTRY_DATA')
{
	$id=(int)$_POST['id'];
	$szCountryName=sanitize_all_html_input(trim($_POST['szCountryName']));
	$idCountry=(int)$_POST['idCountry'];
	
	$kVatApplication->deleteForwarderCountryData($id);
        
        $getForwarderCountryArr=$kVatApplication->getForwarderCountryData(false,$idCountry);
	//print_r($getForwarderCountryArr);
        if(!empty($getForwarderCountryArr))
        {
            foreach($getForwarderCountryArr as $getForwarderCountryArrs)
            {
                $kVatApplication->copyForwarderCountryData($id,$getForwarderCountryArrs['idCountryFrom'],$getForwarderCountryArrs['idCountryTo']);
            }
        }
	//$kVatApplication->copyForwarderCountryData($id,$idCountry);
	
	
	echo forwarderCountriesHTML($kVatApplication,$id);
	echo "||||";
	echo forwarderVatTradesHTML($kVatApplication,$id);
	echo "||||";
	echo showAdminChecks();
	die();
}
else if($mode=='ADD_FORWARDER_COUNTRY_TRADE')
{ 
    if($kVatApplication->addVatTradeData($_POST['addForwarderCountryTradeAry']))
    {
        unset($_POST['addForwarderCountryTradeAry']);
        $_POST['addForwarderCountryTradeAry']=array();
        echo "SUCCESS||||"; 
        echo forwarderVatTradesHTML($kVatApplication,$idCountryForwarder);
        echo "||||";
        echo showAdminChecks();
        die();
    }
    else
    {
        echo "ERROR||||";
        echo addEditForwarderTradeCountryHtml($kVatApplication,$idCountryForwarder);
        die();
    } 
}
else if($mode=='DELETE_VAT_TRADE_DATA')
{
    $id = $_POST['id'];  
    $deleteForwarderCountryIdsArr =  str_replace(";","','",$id);
    $kVatApplication->deleteForwarderCountryTradeData($deleteForwarderCountryIdsArr,true);	
 
    echo "SUCCESS||||";
    echo forwarderVatTradesHTML($kVatApplication,$idCountryForwarder);
    echo "||||";
    echo showAdminChecks();
    die();
}
else if($mode=='BOOKING_LABELS_SENT')
{
    $kCourierServices = new cCourierServices();
    $idBooking=(int)$_POST['idBooking'];
    labelSentHtml($idBooking,$kCourierServices);
}
else if($mode=='BOOKING_LABELS_SENT_CONFIRM')
{
    $kCourierServices = new cCourierServices();
    $kBooking = new cBooking();
    $kTracking = new cTracking();
    $idBooking=(int)$_POST['idBooking'];
    $szTrackingNumber=sanitize_all_html_input(trim($_POST['szTrackingNumber']));
    
    $page=(int)$_POST['page'];
    $limit=(int)$_POST['limit'];
    
    $bookingDetailAry = $kBooking->getExtendedBookingDetails($idBooking); 
    
    $iSendTrackingUpdates = sanitize_all_html_input(trim($_POST['iSendTrackingUpdates']));
     
    $bookingDetail=$kCourierServices->getAllNewBookingWithCourier($idBooking);
    $redirectUrl=__MANAGEMENT_OPRERATION_COUIER_LABEL_NOT_SEND__;
    if($bookingDetail[0]['idBookingStatus']==__COURIER_BOOKING_LABEL_SENT_TO_SHIPPER__)
    {
        $redirectUrl=__MANAGEMENT_OPRERATION_COUIER_LABEL_SEND__;
    } 
    if($_POST['modeHidden']=='sent')
    {
        //courierLabelSend
        if($kCourierServices->courierLabelSend($szTrackingNumber,$idBooking,$iSendTrackingUpdates))
        {
            echo "REDIRECT||||";
            echo $redirectUrl;
            die();
        }
        else
        {
            echo "ERROR||||";
            echo labelSentHtml($idBooking,$kCourierServices);
            die();
        }
    }
    else 
    {
        $bUpdateTrackingFlag = true;
        $trackingUpdate = false;
        if(!empty($bookingDetailAry['szTrackingNumber']))
        {
            if(trim($bookingDetailAry['szTrackingNumber']) != trim($szTrackingNumber))
            {
                $bUpdateTrackingFlag = false;
                if($kTracking->updateOldAddNewAfterShipTracking($szTrackingNumber,$idBooking,$bookingDetailAry['szTrackingNumber']))
                {
                    $bUpdateTrackingFlag = true;
                    $trackingUpdated = true;
                }
            }
        } 
        
        if($bUpdateTrackingFlag)
        {
            if($kCourierServices->updateMasterTrackingNumber($szTrackingNumber,$idBooking,$iSendTrackingUpdates,$trackingUpdated))
            {
                echo "SUCCESS||||";
                echo $redirectUrl;
                die();
            }
            else
            {
                echo "ERROR||||";
                echo labelSentHtml($idBooking,$kCourierServices);
                die();
            } 
        } 
        else
        {
            if($kTracking->tracking_error==true)
            {
                $kCourierServices->arErrorMessages['szAfterShipTrackingNotification'] = $kTracking->szAfterShipTrackingNotification;
            } 
            echo "ERROR||||";
            echo labelSentHtml($idBooking,$kCourierServices);
            die();
        }
    }
}
else if($mode=='BOOKING_LABELS_NEW_SENT')
{
    $t_base="SERVICEOFFERING/";
    $kCourierServices = new cCourierServices();
    $idBooking=(int)$_POST['idBooking'];
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup">
            <h3><?=t($t_base.'fields/new_labels');?> </h3>
            <p><?=t($t_base.'messages/new_label_msg');?>?</p>
            <br/>
            <p align="center">

                    <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/no');?></span></a>
                    <a href="javascript:void(0)" onclick="sendNewLabelForBooking('<?php echo $idBooking; ?>')" class="button1" ><span><?=t($t_base.'fields/yes');?></span></a>
            </p>
        </div>
    </div>
	<?php
}
else if($mode=='BOOKING_LABELS_SENT_NEW')
{
	$idBooking=(int)$_POST['idBooking'];
	$kCourierServices = new cCourierServices();
	 $pageName=trim($_POST['pageName']);
         
         $kBooking = new cBooking();
         $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
	
	$kCourierServices->updateNewLabelForBooking($idBooking,$bookingDataArr['idQuotePricingDetails'],$bookingDataArr['isManualCourierBooking']);
	
	
	$_SESSION['idBookingFileLabel']=$bookingDataArr['idFile'];
	echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;
        die();

}
else if($mode=='COURIER_BOOKING_LABELS_RESEND')
{
    $idBooking=(int)$_POST['idBooking'];
    $kCourierServices = new cCourierServices();
    
     $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);
     $_POST['szEmail']=$courierBookingArr[0]['szShipperEmail'];
     resendCourierBookingLabelEmail($kCourierServices);
}
else if($mode=='CONFIRM_COURIER_BOOKING_LABELS_RESEND')
{
    $idBooking=(int)$_POST['idBooking'];
    $szEmail=trim($_POST['szEmail']);
    $pageName=trim($_POST['pageName']);
    $kCourierServices = new cCourierServices();
    
     
    if($kCourierServices->resendLabelEmail($idBooking,$szEmail))
    {
         echo "SUCCESS||||";
         if($pageName=='LS')
            echo __MANAGEMENT_OPRERATION_COUIER_LABEL_SEND__;
         else
             echo __MANAGEMENT_OPRERATION_COUIER_LABEL_DOWNLOAD__;
         die();
    }   
    else
    {
        echo "ERROR||||";
        echo resendCourierBookingLabelEmail($kCourierServices);
        die();
    }
}
else if($mode=='COURIER_BOOKING_LABELS_DOWNLOAD')
{
    $kCourierServices = new cCourierServices();
    $idBooking=(int)$_POST['idBooking'];
    $kBooking = new cBooking();
    $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
    //print_r($bookingDataArr);
    $_POST['szShipperFirstName']=$bookingDataArr['szShipperFirstName'];
    $_POST['szShipperLastName']=$bookingDataArr['szShipperLastName'];
    $_POST['szShipperEmail']=$bookingDataArr['szShipperEmail'];
    downloadLabelHtml($kCourierServices);
}
else if($mode=='CONFIRM_DOWNLOAD_LABEL')
{
    if($kCourierServices->courierLabelDownloadedByAdmin($_POST))
    {    
        echo "SUCCESS||||";
        echo __MANAGEMENT_OPRERATION_COUIER_LABEL_SEND__;
        die();
    }
    else
    {
        echo "ERROR||||";
        echo downloadLabelHtml($kCourierServices);
        die();
    }
}
else if($mode=='SORT_VAT_APPLICATION')
{
    $szSortField = sanitize_all_html_input(trim($_POST['szSortField']));
    $szSortOrder = sanitize_all_html_input(trim($_POST['szSortOrder']));
    $kVatApplication = new cVatApplication();
    
    echo "SUCCESS||||";
    echo forwarderVatTradesHTML($kVatApplication,$szSortField,$szSortOrder);
    die;
}
?>