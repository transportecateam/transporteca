<?php
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kCourierServices = new cCourierServices();
$mode=sanitize_all_html_input(trim($_REQUEST['mode']));

if($mode=='SORT_DUMMY_SERVICES')
{
    echo displayDummyServiceListAddEditForm();
    die();
}
else if($mode=='SORT_DUMMY_SERVICES_EXCEPTION')
{
    echo displayDummyServiceExceptionDetails();
    die();
}
else if($mode=='SORT_RAIL_TRANSPORT_SERVICES')
{
    $kServices = new cServices();
    echo display_lcl_rail_icon_listing($kServices);
    die();
}
else if($mode=='CLEAR_DUMMY_SERVICES_FORM' ||  $mode=='CLEAR_DUMMY_SERVICES_FORM')
{
    $_POST['dummyServiceAry'] = array();
    echo "SUCCESS||||";
    echo display_dummy_service_form($kService); 
    die;
}
else if($mode=='DELETE_DAMMY_SERVICE')
{
    $kService = new cServices();
    $kService->deleteDummyService($_POST['deleteDummyServiceId']);
    echo displayDummyServiceListAddEditForm(); 
    die;
}
else if($mode=='DELETE_DUMMY_SERVICE_EXCEPTION')
{
    $kService = new cServices();
    $kService->deleteDummyServiceException($_POST['deleteDummyServiceExceptionId']);
    echo displayDummyServiceExceptionDetails(); 
    die;
}
else if($mode=='EDIT_DAMMY_SERVICE')
{
    $idDummyService=(int)$_POST['idDummyService'];
    $kService = new cServices();
    $dummyServiceArr = $kService->getAllDummyServices('','',$idDummyService); 
    
    echo display_dummy_service_form($kService,$dummyServiceArr[0]);
    die;
}
else if(!empty($_POST['dummyServiceAry']))
{
    $kService = new cServices();
    if($kService->addUpdateDummyServices($_POST['dummyServiceAry']))
    {
        $_POST['dummyServiceAry'] = array();
        echo "SUCCESS||||";
        echo displayDummyServiceListAddEditForm();
        die;
    } 
    else
    {
        echo "ERROR||||";
        echo display_dummy_service_form($kService); 
        die;
    }
}
else if(!empty($_POST['dummyServiceExceptionAry']))
{
    $kService = new cServices();  
    if($kService->addUpdateDummyServicesException($_POST['dummyServiceExceptionAry']))
    {
        $_POST['dummyServiceExceptionAry'] = array();
        echo "SUCCESS||||";
        echo displayDummyServiceExceptionDetails();
        die;
    } 
    else
    {
        echo "ERROR||||";
        echo display_dummy_service_exception_form($kService); 
        die;
    }
}

?>