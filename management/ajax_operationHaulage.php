<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$t_base="management/haualgeListing/";
if(isset($_POST))
{	$kHaulagePricing = new cHaulagePricing;
	$action = sanitize_all_html_input($_POST['mode']);
	SWITCH($action)
	{
	
		CASE 'ACTIVATE_FORWARDER_HAULAGE_CONFIRM':
		{
			$id = (int)$_POST['id'];
			$kHaulagePricing->changeHaulageStatus($id);
			
			/**
			*@param $id id of haulagecfsmodel
			* FETCHING DATA OF HAULAGES AND RETURNING BACK 
			* AFTER ACTIVATE OR REACTIVATE THE FIELD
			*/
			//$status = sanitize_all_html_input();
			$content = $kHaulagePricing->currentHaulages($_POST['currentHaulage']);
			haulageListing($content);
			BREAK;
		}
		CASE 'SHOW_HAULAGE_LISTING':
		{
			if(isset($_POST['currentHaulage']))
			{
				$content = $kHaulagePricing->currentHaulages($_POST['currentHaulage']);
			}
			//$status = sanitize_all_html_input($_POST['status']);
			//
			haulageListing($content);
			BREAK;
		}
		CASE 'SHOW_CHANGED_LISTING':
		{
			$status = sanitize_all_html_input($_POST['status']);
			$content = $kHaulagePricing->currentHaulages($status);
			haulageListing($content);
			BREAK;
		}
		CASE 'ACTIVATE_FORWARDER_HAULAGE':
		{
                    $id = (int)$_POST['id'];
                    $row = $kHaulagePricing->listHaulageDetails($id);
                    $status 	= $row['iActive'];
                    $szHaulageModel = $kHaulagePricing->findHaulageModelName($row['idHaulageModel']);
                    $szWarehouseName = $kHaulagePricing->findWarehouseName($row['idWarehouse']);

                    if($status == true)
                    {
                        ?>
                        <div id="popup-bg"></div>
                        <div id="popup-container">	
                            <div class="compare-popup popup">
                                <h5><strong><?=t($t_base.'fields/delete_pricing_model')?></strong></h5>
                                <p><?=t($t_base.'fields/are_u_sure_that_delete') .' '.$szHaulageModel.' '.t($t_base.'fields/for').' '.$szWarehouseName?></p>
                                <br>
                                <p align="center">
                                    <a href="javascript:void(0)" class="button1" onclick="javscript:deleteHaulage(<?=$id;?>);"><span><?=t($t_base.'fields/inactivate');?></span></a>&nbsp;
                                    <a href="javascript:void(0)" class="button2" onclick="showHide('ajaxLogin')"><span><?=t($t_base.'fields/cancel');?></span></a>
                                </p>
                            </div>
                        </div>	 
                        <?php
                    }
                    else
                    { 
                        ?>
                        <div id="popup-bg"></div>
                        <div id="popup-container">	
                            <div class="compare-popup popup">
                                <h5><strong><?=t($t_base.'fields/reactivate_pricing_model')?></strong></h5>
                                <p><?=t($t_base.'fields/are_u_sure_that') .' '.$szHaulageModel.' '.t($t_base.'fields/for').' '.$szWarehouseName?></p>
                                <br>
                                <p align="center">
                                    <a href="javascript:void(0)" class="button1" onclick="javscript:deleteHaulage(<?=$id;?>);"><span><?=t($t_base.'fields/confirm');?></span></a>&nbsp;
                                    <a href="javascript:void(0)" class="button2" onclick="showHide('ajaxLogin')"><span><?=t($t_base.'fields/cancel');?></span></a>
                                </p>
                            </div>
                        </div> 
                        <?php
                            //$id = (int)$_POST['id'];
                            //$kHaulagePricing->changeHaulageStatus($id);	
                            //$status = sanitize_all_html_input($_POST['status']);
                            //$content = $kHaulagePricing->currentHaulages($status);
                            //haulageListing($content);
                    }
                    /**
                    *@param $id id of haulagecfsmodel
                    * FETCHING DATA OF HAULAGES AND RETURNING BACK 
                    * AFTER ACTIVATE OR REACTIVATE THE FIELD
                    */ 
                    BREAK;
		}
		CASE 'CHANGE_PRIORITY':
		{
                    $id = (int)$_POST['id'];
                    $flag = (boolean)$_POST['flag'];
                    /**
                    *@param $id of haulagecfsmodel
                    *@param $flag  for changing its priority
                    */

                    $kHaulagePricing->changeHaulageLevel($id,$flag);

                    /**
                    * FETCHING DATA OF HAULAGES AND RETURNING BACK 
                    * AFTER CHANGING THE PRIORITY OF HAULAGE MODEL
                    */

                    //$status = sanitize_all_html_input($_POST['status']);
                    //$content = $kHaulagePricing->currentHaulages($status);
                    //haulageListing($content);
                    BREAK;
		}
		CASE 'EDIT_HAULAGE_DETAILS' :
		{
                    $id = (int)$_POST['id']; 
                    //RETURNING ARRAY CONTAINING DETAILS OF THAT HAULAGE

                    $haulagePricingZoneDataArr = $kHaulagePricing->changeHaulageDetails($id);
                    $row = $kHaulagePricing->listHaulageDetails($id);
                    if($row != array())
                    {	
                        $idMapping 	= $row['id'];
                        $idWarehouse = $row['idWarehouse'];
                        $idDirection = $row['iDirection'];
                        $idHaulageModel = $row['idHaulageModel'];
                    }
                    ?><div id="haulage_model_<?=$idHaulageModel?>"><?php		
                    if($idHaulageModel == 1)
                    {
                            pricingHaulageZoneListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
                    }
                    if($idHaulageModel == 2)
                    {
                            pricingHaulageCityListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
                    }
                    if($idHaulageModel == 3)
                    {
                            pricingHaulagePostCodeListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
                    }
                    if($idHaulageModel == 4)
                    {
                            pricingHaulageDistanceListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
                    }
                    ?> </div><?php
                    echo "|||||";
                    echo try_it_out_haulage_admin($idWarehouse,$idDirection);
			
		}
	}
}
?>
