<?php 
define('PAGE_PERMISSION','__CURRENT_WTW__');
/**
  *BILLING DETAILS
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Pricing - Current WTW";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/currentWTW/";
$kExport_Import  = new cExport_Import;
$iWarehouseType = __WAREHOUSE_TYPE_CFS__;
//$bottomContent  = $kExport_Import->getDataForLCLExpiredLastThreeMonth();
//$warehouse = $kExport_Import->getAllwarehouseDetails($_REQUEST['currentWtw']);
//$bottomContent  = $kExport_Import->getDataForLCLExpiredLastThreeMonth(null,$warehouse);
validateManagement();
?>
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="hsbody-2">
    <?php require_once(__APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php"); ?>
    <div class="hsbody-2-right">
        <?php	
            showCurrentWtwTopForm($t_base,$iWarehouseType);
        ?>
        <div id = "bottomForm" >
            <?php	
                //currentWTWBottomForm(array()); 
            ?>
        </div>
        <div id="bottomFormEdit"></div>
        <div id="canvas">
            <?php $t_base = "OBODataExport/";?>
            <h4><strong><?=t($t_base.'title/illustration_of_lcl')?></strong></h4>
            <img src="<?=__BASE_STORE_IMAGE_URL__?>/CargoFlowLCL.png"> 
            <?php
                $t_base = "OBODataExport/";
                //display_obo_lcl_bottom_admin_html($t_base,true);
            ?>
        </div>								
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	