<?php 
/**
  *  admin--- E-Mail log management
  */
  
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || E-mail log management";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/e_mail_log_mgmt/";
$count= $kAdmin->selectMaxEmail();
$emailLog=$kAdmin->emailLog(1);
require_once(__APP_PATH_CLASSES__."/pagination.class.php" );

?>
<script type="text/javascript">
$(document).ready(function(){
	$("#1").css({'background-color' : '#006699'});
});
</script>
<script type="text/javascript">
$().ready(function() {	
    $("#datepicker1").datepicker();
});
</script>
<div id="ajaxLogins" style="display: none;max-height:90%;">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup" style="width:40%;">
	<h5>
	<?=t($t_base.'fields/message');?></h5>
	<p id="view_message"><?=t($t_base.'messages/would_you_like');?> </p>
	<br/>
	<p align="center"><a href="javascript:void(0)" class="button1" onclick="hideTemplates(0);"><span><?=t($t_base.'fields/close');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/admin_messages_left_nav.php" ); ?> 
	<div class="hsbody-2-right">
		<table cellspacing="0" cellpadding="0" border="0" class="format-1" width="99%"  >
			<tr style="border-bottom: none;">
				<td valign="top" width="30%"><?=t($t_base.'fields/search_by_email')?></td>
				<td valign="top" width="40%" >&nbsp;</td>
				<td valign="top" width="15%"><?=t($t_base.'fields/search_by_date')?></td>
				<td valign="top" width="15%">&nbsp;</td>
			</tr>
			<tr style="border-bottom: none;">
				<td valign="top" width="30%"><input type="text" size="35" id="emailSearch" onkeyup="searchingData();"></td>
				<td valign="top" width="40%" ></td>
				<td valign="top" width="15%"> 
				<input id="datepicker1" type="text" onchange="searchingData();" value="" placeholder="<?=t($t_base.'fields/date_format')?>" onblur="show_me(this.id,'<?=t($t_base.'fields/date_format')?>')" onfocus="blank_me(this.id,'<?=t($t_base.'fields/date_format')?>')">
				<? /*<input type="text" size="15" id="dateSearch" onkeyup="searchingData();" placeholder="<?=t($t_base.'fields/date_format')?>" ></td> */ ?>
				<td valign="top" width="15%"></td>
			</tr>
		</table>
		<div id="showdetails">
			<?=emailLogTable($emailLog,$count);?>
		</div>	
	</div>	
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );

?>