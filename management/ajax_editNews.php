<?php 
/**
  *  Edit News
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
$kRss = new cRSS();
validateManagement_ajax();

$t_base="management/AdminMessages/";

//print_r($_REQUEST);
$idNewFeed=$_REQUEST['idNewFeed'];
$flag=$_REQUEST['flag'];

if($flag=='deleteNewFeed')
{
	$kRss->loadNews($idNewFeed);
	if($kRss->dtCreatedOn!='' && $kRss->dtCreatedOn!='0000-00-00 00:00:00')
	{
		$szDate=date('j. F Y',strtotime($kRss->dtCreatedOn));
	}
?>
<div id="send_confirm_popup">
<div id="popup-bg"></div>
<div id="popup-container">
	<div class="popup signin-popup signin-popup-verification">
	<h5><?=t($t_base.'fields/delete_news_flash');?></h5>
	<p><?=t($t_base.'fields/delete_news_flash_text');?> <?=$szDate?></p>
	<br>
	 <p align="center">
	  <input type="hidden" name="idNewFeed" id="idNewFeed" value="<?=$idNewFeed?>">
	 	<a href="javascript:void(0);" class="button1" id="yes" onclick="confirm_delete_news_feed('<?=$idNewFeed?>');"><span><?=t($t_base.'fields/yes');?></span></a>
		<a href="javascript:void(0);" class="button2"  id="no" onclick="cancelPreviewMsg('send_confirm_popup');"><span><?=t($t_base.'fields/no');?></span></a>
	</p>
	</div>
</div>
</div>
<?php
}
else if($flag=='confirmDelete')
{
	$kRss->deleteNewFeeds($idNewFeed);
	newDetailLists();

}
else if($flag=='edit_news_form')
{
	updateNewFeedForm($idNewFeed);
}
else if($flag=='SAVE_NEWS_FEED')
{
	
	if(!empty($_REQUEST['newFeedArr']))
	{
		
		$_REQUEST['newFeedArr']['szDescription']=urldecode(base64_decode($_REQUEST['tinyContent']));
		
		if($kRss->saveNewsFeed($_REQUEST['newFeedArr'],$idNewFeed))
		{
			newDetailLists();
		}
		else
		{
	
			if(!empty($kRss->arErrorMessages)){
			$t_base_error = "Error";
			?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base_error.'/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
				foreach($kRss->arErrorMessages as $key=>$values)
				{
				?><li><?=$values?></li>
				<?php	
				}
			?>
			</ul>
			</div>
			</div>
			<? }
			updateNewFeedForm($idNewFeed,$_REQUEST['newFeedArr']);
		}
	}
}
?>