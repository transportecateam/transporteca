<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();

 header('Content-type: application/pdf');
 readfile(__APP_PATH__."/forwarders/guide/Forwarders_Guide_to_Transporteca.pdf");
 ?>