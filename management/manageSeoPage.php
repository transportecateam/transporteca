<?php 
/**
  *  Explain page List All Explain Pages
  * 
  */
define('PAGE_PERMISSION','__Articles__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
$szMetaTitle = "Transporteca | Setup SEO Pages";
require_once (__APP_PATH__ ."/layout/admin_header.php");

validateManagement();

$t_base="management/landingPageData/";
$kSEO = new cSEO();
$iSessionLanguage = false;
if($_SESSION['session_admin_language']>0)
{
	//$iSessionLanguage = $_SESSION['session_admin_language'] ;
} 

$seoPageDataAry = array();
$seoPageDataAry = $kSEO->getAllSEOPageData(false,$iSessionLanguage);
$iMaxOrder = $kSEO->getMaxOrderSEOPageData();
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	
	<div class="hsbody-2-right">
		<div id="manage_landing_page_content">
			<?	echo viewSEOPageContent($seoPageDataAry,$iMaxOrder);?>
		</div>
		
	</div><br /><br /> 
		<?php //echo previewLandingPage_admin();?>
	
<?php include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); ?>	