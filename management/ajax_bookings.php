<?php
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$kBooking = new cBooking;
$t_base = "ForwarderBookings/";
if(!empty($_REQUEST['mode']))
{
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
}

if($operation_mode=='UPDATE_YOUR_REFERENCE')
{
	$idBooking = sanitize_all_html_input(trim($_REQUEST['id']));
	$szBookingRef = sanitize_all_html_input(trim($_REQUEST['ref']));
         $idBillingFlag=sanitize_all_html_input(trim($_REQUEST['idBillingFlag']));
	$kBooking=new cBooking();
	$kBooking->load($idBooking);
	?>
    <div id="popup-bg"></div>
    <div id="popup-container">	
            <div class="compare-popup popup" >
            
                    <h5><strong><?=t($t_base.'title/booking_reference');?> <?=$szBookingRef?></strong></h5>
                    <p><?=t($t_base.'title/type_reference');?>:</p>
                    <p>
                            <input type="text"  name="updateInvoiceAry[szForwarderReference]" onkeypress="return submitForm(event)"  id="fwdReference"  value="<?=$kBooking->szInvoiceFwdRef?>" size="43" AUTOCOMPLETE ="off">
                    </p>
                    <br>
                    <p align="center">
                            <input type="hidden" id="idBillingFlag" name="updateInvoiceAry[idBillingFlag]" value="<?=$idBillingFlag?>">
                            <input type="hidden" id="idBooking" name="updateInvoiceAry[idBooking]" value="<?=$idBooking?>">
                            <input type="hidden" id="szRef" name="updateInvoiceAry[szBookingRef]" value="<?=$szBookingRef?>">
                            <a href="javascript:void(0)" class="button1" onclick="update_invoice_comment_confirm()"><span><?=t($t_base.'fields/save');?></span></a>
                            <a href="javascript:void(0)" class="button2" onclick="showHide('invoice_comment_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
                    </p>
            </div>
    </div>
	<?php
}
if(!empty($_REQUEST['updateInvoiceAry']))
{	
    $kBooking->set_id(sanitize_all_html_input(trim($_REQUEST['idBooking'])));
    $kBooking->set_szInvoiceForwarderReference(sanitize_all_html_input(trim($_REQUEST['szForwarderReference'])),false);
    
    if($kBooking->id>0)
    {
        $update_query = " szForwarderReference = '".mysql_escape_custom(trim($_REQUEST['szForwarderReference']))."'" ;
        $idBooking = $kBooking->id ;
        if($kBooking->updateDraftBooking($update_query,$kBooking->id));
        {	
            $kBooking->updateBookingReferences($_REQUEST['szForwarderReference'],$idBooking);
            echo "SUCCESS||||".$kBooking->id."||||".createSubString($_REQUEST['szForwarderReference'],0,12);
            die;
        }
    }
    else
    {
        echo "ERROR||||";
    }
?> 
<div id="popup-bg"></div>
<div id="popup-container">	
    
    <form name="update_forwarder_invoice_comment_form" id="update_forwarder_invoice_comment_form">
        <div class="compare-popup popup">
        <?php if(!empty($kBooking->arErrorMessages)){ 	?>
            <div id="regError" class="errorBox ">
                <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
                <div id="regErrorList">
                    <ul>
                    <?php
                        foreach($kBooking->arErrorMessages as $key=>$values)
                        {
                            ?><li><?=$values?></li>
                    <?php } ?>
                    </ul>
                </div>
            </div>
	 <?php } ?>
            <h5><strong><?=t($t_base.'title/booking_reference');?> <?=$_REQUEST['updateInvoiceAry']?></strong></h5>
            <p><?=t($t_base.'title/type_reference');?>:</p>
            <p>
                <textarea rows="1" name="updateInvoiceAry[szInvoiceComment]" cols="40" maxlength="80" id="fwdReference"><?=$_REQUEST['szInvoiceComment']?></textarea>
            </p>
            <br>
            <p align="center">
                <input type="hidden" id="idBillingFlag" name="updateInvoiceAry[idBillingFlag]" value="<?=$_REQUEST['idBillingFlag']?>">
                <input type="hidden" name="updateInvoiceAry[idBooking]" id="idBooking" value="<?=$_REQUEST['idBooking']?>">
                <input type="hidden" name="updateInvoiceAry[szBookingRef]" value="<?=$_REQUEST['updateInvoiceAry']?>" id="szRef">
                <a href="javascript:void(0)" class="button1" onclick="update_invoice_comment_confirm()" ><span><?=t($t_base.'fields/save');?></span></a>
                <a href="javascript:void(0)" class="button2" onclick="showHide('invoice_comment_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
            </p>
	</div>
    </form>
</div>
	<?php
}?>