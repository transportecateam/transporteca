<?php 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Tasks - Pending Tray";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/pendingTray/"; 
validateManagement();   

$number = 1001;
if($_GET['file_id']>0)
{
    $idBookingFile = $_GET['file_id'] ;
}
else
{
    $idBookingFile = 14 ;
}

$kBooking = new cBooking();
$kBooking->loadFile($idBookingFile);  
if(!empty($_POST['quotePricingAry']))
{
    $idBooking = $_POST['quotePricingAry']['idBooking'] ;   
}
else
{
    $idBooking = $kBooking->idBooking ;   
}  
$szMainContainerClass="overview-form afq";
$szFirstSpanClass="field-name";
$szSecondSpanClass="field-container";

$kConfig = new cConfig();
$kForwarder = new cForwarder();
$kAdmin = new cAdmin(); 

$kBooking_new = new cBooking();

$iSectionEdited = 0;
if($edit_flag)
{
    $iSectionEdited =1;
}
$bookingDataArr =  $kBooking_new->getBookingDetails($idBooking);

$iBookingLanguage = $bookingDataArr['iBookingLanguage'];

$fValueOfGoods = $bookingDataArr['fValueOfGoods'] ;
$fValueOfGoodsUSD = (float)$bookingDataArr['fValueOfGoodsUSD'] ; 
$idGoodsInsuranceCurrency = $bookingDataArr['idGoodsInsuranceCurrency'] ;
$szGoodsInsuranceCurrency = $bookingDataArr['szGoodsInsuranceCurrency'] ;
$fGoodsInsuranceExchangeRate = $bookingDataArr['fGoodsInsuranceExchangeRate'] ;
$fTotalBookingPriceUsd = $bookingDataArr['fTotalPriceUSD'] ;
 
$insuranceDataAry = array();
$insuranceDataAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
$insuranceDataAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
$insuranceDataAry['idTransportMode'] = 1 ;
$insuranceDataAry['iPrivate'] = (int)$bookingDataArr['isMoving'] ; 
  
$kInsurance = new cInsurance();
$buyRateListAry = array();
$buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,true);

if(!empty($buyRateListAry))
{
    foreach($buyRateListAry as $idInsuranceBuyRate=>$buyRateListArys)
    {   
        $insuranceSellRateAry = array();
        $insuranceSellRateAry = $kInsurance->getInsuranceRates(__INSURANCE_SELL_RATE_TYPE__,$buyRateListArys['id'],true); 
        $buyRateListAry[$idInsuranceBuyRate]['sellRateAry'] = $insuranceSellRateAry ; 
    }  
} 
        $szOfferText = t($t_base.'fields/szOfferText_pt') ;
        $szFromText =  t($t_base.'fields/szFromText_pt')  ;
        $szToText =  t($t_base.'fields/szToText_pt')  ;
        $szCargoText =  t($t_base.'fields/szCargoText_pt') ;
        $szModeText =  t($t_base.'fields/szModeText_pt') ;
        $szTransitTimeText =  t($t_base.'fields/szTransitTimeText_pt') ;
        $szPriceText =  t($t_base.'fields/szPriceText_pt') ;
        $OfferValidUntillText =  t($t_base.'fields/OfferValidUntillText_pt') ;
        $szInsuranceText =  t($t_base.'fields/szInsuranceText_pt') ;
        $szDaysText =  " ".t($t_base.'fields/szDaysText_pt') ;
        $szNoVatOnBookingText =  t($t_base.'fields/szNoVatOnBookingText_pt') ;
        $szVatExcludingText = t($t_base.'fields/szVatExcludingText_pt') ;        
        $szOfferHeading =  t($t_base.'fields/szOfferHeading_pt') .":";
        $szMultipleOfferHeading = t($t_base.'fields/szMultipleOfferHeading_pt') .":";
        $szMultipleOfferHeading2 = t($t_base.'fields/szMultipleOfferHeading2_pt') .":";
        $szInsuranceValueUptoText = " ".t($t_base.'fields/szInsuranceValueUptoText_pt');        
        $szVatTextExcluingTextWhenOneQuote = t($t_base.'fields/szVatTextExcluingTextWhenOneQuote_pt');
 
//if($iBookingLanguage==__LANGUAGE_ID_DANISH__) //danish
//{
//    $szOfferText = 'TILBUD' ;
//    $szFromText = 'Fra' ;
//    $szToText = 'Til' ;
//    $szCargoText = 'Gods' ;
//    $szModeText = 'Transportform' ;
//    $szTransitTimeText = 'Transittid' ;
//    $szPriceText = 'Pris';
//    $OfferValidUntillText = 'Dette tilbud er gældende til den';
//    $szInsuranceText = 'Forsikring';
//    $szDaysText = 'dage';
//    $szNoVatOnBookingText = 'Der er ikke moms på denne afskibning';
//    $szVatExcludingText = 'eksklusive moms af';
//
//    $szOfferHeading = "Tak for din forespørgsel. Vi fremsender hermed vores tilbud:";
//    $szMultipleOfferHeading = 'Tak for din forespørgsel:';
//    $szMultipleOfferHeading2 = 'Det glæder os at kunne give følgende tilbud:';
//    $szInsuranceValueUptoText = " for en vareværdi op til";
//}
//else
//{
//    $szOfferText = 'OFFER' ;
//    $szFromText = 'From' ;
//    $szToText = 'To' ;
//    $szCargoText = 'Cargo' ;
//    $szModeText = 'Mode' ;
//    $szTransitTimeText = 'Transit time' ;
//    $szPriceText = 'Price';
//    $OfferValidUntillText = 'This offer is valid until';
//    $szInsuranceText = 'Insurance';
//    $szInsuranceValueUptoText = " for an insured value of";
//    $szDaysText = 'days';
//    $szNoVatOnBookingText = 'There is no VAT on this shipment.';
//    $szVatExcludingText = 'excluding VAT of';
//
//    $szOfferHeading = "Thank you for your request. We are hoppy to respond as follows:";
//    $szMultipleOfferHeading = 'Thank you for your request for a price for the following shipment:';
//    $szMultipleOfferHeading2 = 'In response to your request we are happy to offer the following services:';
//}
$add_js_hour = false;
if(!empty($_POST['quotePricingAry']))
{
    $quoteReceivedAry = $_POST['quotePricingAry']; 
    $szEmailSubject = $quoteReceivedAry['szEmailSubject'];
    $szEmailBody = $quoteReceivedAry['szEmailBody'];
    $szQuotationBody = $quoteReceivedAry['szQuotationBody'];
    $dtReminderDate = $quoteReceivedAry['dtReminderDate'];
    $szReminderTime = $quoteReceivedAry['szReminderTime'];
    $idBookingFile = $quoteReceivedAry['idBookingFile'];  
    $idBooking = $quoteReceivedAry['idBooking'];  
    $idCustomerCurrency = $quoteReceivedAry['idCustomerCurrency'];   
    $fCustomerExchangeRateGlobal = $quoteReceivedAry['fCustomerExchangeRateGlobal'];    
    $fPriceMarkUpGlobal = $quoteReceivedAry['fPriceMarkUpGlobal'];     

    $szOriginCountryStr = $bookingDataArr['szOriginCountry'];
    $szDestinationCountryStr = $bookingDataArr['szDestinationCountry']; 
    $szCustomerCurrency = $bookingDataArr['szCurrency'];    
    $fCustomerExchangeRate = $bookingDataArr['fExchangeRate']; 
}
else
{  
    $szOriginCountryStr = $bookingDataArr['szOriginCountry'];
    $szDestinationCountryStr = $bookingDataArr['szDestinationCountry']; 
    $szCustomerCurrency = $bookingDataArr['szCurrency'];   
    $idCustomerCurrency = $bookingDataArr['idCurrency'];   
    $fCustomerExchangeRate = $bookingDataArr['fExchangeRate'];  
    //$fCustomerExchangeRateGlobal = $bookingDataArr['fExchangeRate'];   
    
    if($idCustomerCurrency=='1') //USD
    {
        $fCustomerExchangeRateGlobal = 1;   
    }
    else
    {
        $kWarehouseSearch = new cWHSSearch();
        $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency);		 
        $fCustomerExchangeRateGlobal = $resultAry['fUsdValue'];
    } 

    $kWhsSearch = new cWHSSearch();  
    $fPriceMarkUpGlobal = $kWhsSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__'); 

    $kAdmin->getAdminDetails($_SESSION['admin_id']);

    if($bookingDataArr['idTransportMode']==4) //courier 
    {
        $szVolWeight = format_volume($bookingDataArr['fCargoVolume'])."cbm, ".number_format((float)$bookingDataArr['fCargoWeight'])."kg,  ".number_format((int)$bookingDataArr['iNumColli'])."col";
    }
    else
    {
        $szVolWeight = format_volume($bookingDataArr['fCargoVolume'])."cbm,  ".number_format((float)$bookingDataArr['fCargoWeight'])."kg ";
    }
    $kConfig = new cConfig();
    $kConfig->loadCountry($kAdmin->idInternationalDialCode);
    $iInternationDialCode = $kConfig->iInternationDialCode;

    $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry']);
    $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry']) ; 
    $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
    $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	 
    $fCargoVolume = format_volume($bookingDataArr['fCargoVolume']);
    $fCargoWeight = number_format((float)$bookingDataArr['fCargoWeight']);

    $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone;

    $szBaseUrl_str = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);	
    $szBaseUrl = addhttp(__MAIN_SITE_HOME_PAGE_URL__);

    $szSiteLink = "<a href='".$szBaseUrl."' target='_blank'>".$szBaseUrl_str."</a>";

 
    $kWHSSearch=new cWHSSearch();
//    if($bookingDataArr['iBookingLanguage'] ==__LANGUAGE_ID_DANISH__)
//    {
//        $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//    }
//    else
//    {
        $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$bookingDataArr['iBookingLanguage']);
    //}

    $replace_ary = array(); 
    $replace_ary['szOriginCity'] = $bookingDataArr['szOriginCity'];
    $replace_ary['szOriginCountry'] = $szOriginCountry;
    $replace_ary['szDestinationCity'] = $bookingDataArr['szDestinationCity'] ;
    $replace_ary['szDestinationCountry'] = $szDestinationCountry ;
    $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'] ;
    $replace_ary['fCargoVolume'] = $fCargoVolume;
    $replace_ary['fCargoWeight'] = $fCargoWeight; 
    $replace_ary['szQuoteUrl'] = $szQuoteUrl;  
    $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
    $replace_ary['szTitle'] = $kAdmin->szTitle ;
    $replace_ary['szOfficePhone'] = $szCustomerCareNumer ;
    $replace_ary['szMobile'] = $szMobile ;
    $replace_ary['zCargoVolWeidghtColli'] = $szVolWeight ;
    //$replace_ary['szSiteUrl'] =  $szSiteLink;

    $emailMessageAry = array(); 
    $emailMessageAry = createEmailMessage('__CUSTOMER_SEND_BOOKING_QUOTES__', $replace_ary);

    $breaksAry = array("<br />","<br>","<br/>");  
    $szEmailBody = str_replace($breaksAry, "\r\n", $emailMessageAry['szEmailBody']); 

    $szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
    $szEmailSubject = $emailMessageAry['szEmailSubject'];  

    $idBookingFile = $kBooking->idBookingFile ;
    $dtReminderDateTime = $kBooking->dtCustomerRemindDate ;

    if(!empty($dtReminderDateTime) && $dtReminderDateTime!='0000-00-00 00:00:00')
    {
        $dtReminderDate = date('d/m/Y',strtotime($dtReminderDateTime));
        $szReminderTime = date('H:i',strtotime($dtReminderDateTime)); 
        $add_js_hour = false;
    }
    else
    {
        $kWHSSearch=new cWHSSearch();
        $iForwarderResponseTime = $kWHSSearch->getManageMentVariableByDescription('__CUSTOMER_RESPONSE_TIME__');

        //$dtResponseDateTime = strtotime("+".$iForwarderResponseTime." HOUR"); 
       //$dtReminderDate = date('d/m/Y',strtotime("+".$iForwarderResponseTime." HOUR"));
      //$szReminderTime = date('H:i',strtotime("+".$iForwarderResponseTime." HOUR"));  
        $add_js_hour = true;
    } 

    $searchDataAry = array();
    $searchDataAry['iClosed']=1; 
    $quotesAry = array();  
    $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile,$searchDataAry);

    $szQuotationBody = '';
    if(!empty($quotesAry))
    {
        $iQuoteCount = count($quotesAry); 
        if($iQuoteCount==1)
        { 
            $szQuotationBody = $szOfferHeading."<br><br>" ; 
        }
        else
        { 
            $szQuotationBody = $szMultipleOfferHeading."<br><br>" ; 
            $szQuotationBody .= "&#13;&#10;&#13;&#10;  ".$szFromText.': '.$bookingDataArr['szOriginCountry'].' <br> &#13;&#10; '
                        . ' '.$szToText.': '.$bookingDataArr['szDestinationCountry'].' <br> &#13;&#10;'
                        . '  '.$szCargoText.': '.$szVolWeight.'  <br> &#13;&#10; ';

            $szQuotationBody .= "<br><br>&#13;&#10;&#13;&#10; ".$szMultipleOfferHeading2." <br><br>" ; 
        }

        $counter = 1;
        foreach($quotesAry as $quotesArys)
        {
            if($quotesArys['iClosed']==1)
            {  
                if($quotesArys['fForwarderCurrencyExchangeRate']>0)
                {
                    $fTotalPriceForwarderCurrency_usd = $quotesArys['fTotalPriceForwarderCurrency']*$quotesArys['fForwarderCurrencyExchangeRate'] ;
                    $fTotalVat_usd = $quotesArys['fTotalVat']*$quotesArys['fForwarderCurrencyExchangeRate'];
                }   
                if($fCustomerExchangeRate>0)
                {
                    $fTotalPriceCustomerCurrency = round((float)($fTotalPriceForwarderCurrency_usd / $fCustomerExchangeRate));
                    $fTotalVat = round((float)($fTotalVat_usd / $fCustomerExchangeRate)) ;
                } 
                $szQuotePriceStr =  $szCustomerCurrency." ".number_format((float)$fTotalPriceCustomerCurrency)." all-in" ;
                if($fTotalVat>0)
                {
                    $szQuotePriceStr .=", excluding VAT of ".$szCustomerCurrency." ".number_format((float)$fTotalVat) ;
                }
                $dtOfferValid = date('d F',strtotime($quotesArys['dtQuoteValidTo']));

                if($iQuoteCount==1)
                {
                    $szQuotationBody .= "&#13;&#10;&#13;&#10;  ".$szFromText.': '.$bookingDataArr['szOriginCountry'].' <br> &#13;&#10; '
                        . ' '.$szToText.': '.$bookingDataArr['szDestinationCountry'].' <br> &#13;&#10;'
                        . '  '.$szCargoText.': '.$szVolWeight.'  <br> &#13;&#10; '
                        . ' '.$szModeText.': '.$quotesArys['szTransportMode'].' <br>&#13;&#10; '
                        . ' '.$szTransitTimeText.': '.$quotesArys['iTransitHours'].' '.$szDaysText.' <br>&#13;&#10; '
                        . ' '.$szPriceText.': '.$szQuotePriceStr.' <br>&#13;&#10; '
                        . ' '.$OfferValidUntillText.' '.$dtOfferValid.' <br><br>  &#13;&#10;&#13;&#10; szBookingQuoteLink <br><br>&#13;&#10;&#13;&#10;
                    '; 
                }
                else
                {
                    $szQuotationBody .= "<strong><u>".$counter.'. '.$szOfferText.' </u></strong><br> '. ' '.$szModeText.': '.$quotesArys['szTransportMode'].' <br>&#13;&#10; '
                        . ' '.$szTransitTimeText.': '.$quotesArys['iTransitHours'].' '.$szDaysText.' <br>&#13;&#10; '
                        . ' '.$szPriceText.': '.$szQuotePriceStr.' <br>&#13;&#10; '
                        . ' '.$OfferValidUntillText.' '.$dtOfferValid.' <br><br>  &#13;&#10;&#13;&#10; szBookingQuoteLink <br><br>&#13;&#10;&#13;&#10;
                    ';
                } 
                $counter++;
            }
        }
    } 
    $quotesAry = array();  
    $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile); 
}
$allCurrencyArr = array();
$allCurrencyArr=$kConfig->getBookingCurrency(false,true);
 
$iBookingLanguage = 1;
$transportModeListAry = array();
$transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true,true);

$forwarderListAry = array();
$forwarderListAry = $kForwarder->getForwarderArr(true); 
$kWhsSearch = new cWHSSearch();  
             
$fInsuranceSellCurrencyExchangeRate = 0.1451 ;
?> 
<div id="hsbody">   
<div id="task_pending_tray_main_container">
    <div id="pending_task_list_main_container"> 
        <div class="clear-all"></div>  
    <script type="text/javascript">
        var profitTypeAry = new Array();
        var referalPercentageAry = new Array();
        var currencyListAry = new Array();
        var exchangeRateListAry = new Array();
        var forwarderAccountCurrencyAry = new Array();
        <?php if($add_js_hour){ ?> 
            setDefaultReminderDate('<?php echo $iForwarderResponseTime; ?>');    
        <?php } 
            if(!empty($forwarderListAry))
            {
                foreach($forwarderListAry as $forwarderListArys)
                {
                    ?>
                    profitTypeAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $forwarderListArys['iProfitType'];  ?>';
                    referalPercentageAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $forwarderListArys['fReferalFee'];  ?>';
                    forwarderAccountCurrencyAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $forwarderListArys['szCurrency'];  ?>';
                    <?php
                }
            } 
            if(!empty($allCurrencyArr))
            {
                $currencyInputAry = array();
                foreach($allCurrencyArr as $allCurrencyArrs)
                {
                    $currencyInputAry[] = $allCurrencyArrs['id'];
                    ?>
                    currencyListAry['<?php echo $allCurrencyArrs['id']; ?>'] = '<?php echo $allCurrencyArrs['szCurrency'] ?>' 
                    <?php
                }
            }
            $currencyExchangeListAry = array();
            $currencyExchangeListAry = $kWhsSearch->getCurrencyDetails(false,$currencyInputAry);
            if(!empty($currencyExchangeListAry))
            {
                $alreadyCurrency[0] = 1;
                $ctr=1;
                foreach($currencyExchangeListAry as $currencyExchangeListArys)
                {
                    if($currencyExchangeListArys['fUsdValue']>0 && !in_array($currencyExchangeListArys['idCurrency'],$alreadyCurrency))
                    {
                        $fCurrencyExhang = round((float)$currencyExchangeListArys['fUsdValue'],4); 
                        $alreadyCurrency[$ctr] = $currencyExchangeListArys['idCurrency'];
                        $ctr++; 
                    ?>
                    exchangeRateListAry['<?php echo $currencyExchangeListArys['idCurrency']?>'] = '<?php echo $fCurrencyExhang; ?>';
                    <?php }
                }
            }
        ?> 
        exchangeRateListAry[1] = 1 ; //USD 
        function autofill_profit_type(forwarder_id,number)
        { 
            var iProfitType = profitTypeAry[forwarder_id] ;
            var fReferralPercentage = referalPercentageAry[forwarder_id]; 
            var idForwarderAccountCurrency = forwarderAccountCurrencyAry[forwarder_id]; 
            if(iProfitType==2)
            {
                $("#gross_profit_type_span_"+number).html('Markup');
            }
            else
            {
                $("#gross_profit_type_span_"+number).html('Referral');
            } 
            $("#idForwarderAccountCurrency_"+number).val(idForwarderAccountCurrency);  
            $("#fReferalPercentageStandarad_"+number).val(fReferralPercentage); 
            $("#fReferalPercentage_"+number).val(fReferralPercentage); 
            $("#iProfitType_"+number).val(iProfitType); 
            
            calculate_gross_profit(number,'PERCENTAGE'); 
            updateEmailBody(number);
        }
        
        function autofill_vat_currency(currency_id,number)
        {
            var szCurrency = currencyListAry[currency_id] ; 
            $("#vat_currency_span_"+number).html(szCurrency); 
        }
        function autofill_currency_exhchange_rate(currency_id,number)
        {
            var fExchangeRate = exchangeRateListAry[currency_id] ; 
            $("#fForwarderExchangeRate_"+number).val(fExchangeRate); 
        } 
    </script>
     
    <form id="pending_task_tray_form" name="pending_task_tray_form" method="post" action="">
        <input type="hidden" name="szCustomerCurrency_email" id="szCustomerCurrency_email" value="<?php echo $szCustomerCurrency; ?>">
        <input type="hidden" name="szOriginCountry_email" id="szOriginCountry_email" value="<?php echo $szOriginCountryStr; ?>">
        <input type="hidden" name="szDestinationCountry_email" id="szDestinationCountry_email" value="<?php echo $szDestinationCountryStr; ?>">
        <input type="hidden" name="szCargoText_email" id="szCargoText_email" value="<?php echo $szVolWeight; ?>">   
        <input type="hidden" name="quotePricingAry[idGoodsInsuranceCurrency]" id="idGoodsInsuranceCurrency" value="<?php echo $idGoodsInsuranceCurrency; ?>">
        <input type="hidden" name="quotePricingAry[szGoodsInsuranceCurrency]" id="szGoodsInsuranceCurrency" value="<?php echo $szGoodsInsuranceCurrency; ?>">
        <input type="hidden" name="quotePricingAry[fGoodsInsuranceExchangeRate]" id="fGoodsInsuranceExchangeRate" value="<?php echo $fGoodsInsuranceExchangeRate; ?>">
        <input type="hidden" name="quotePricingAry[fValueOfGoodsUSD]" id="fValueOfGoodsUSD" value="<?php echo $fValueOfGoodsUSD; ?>"> 
        <input type="hidden" name="quotePricingAry[hiddenPosition]" id="hiddenPosition" value="<?php echo $counter+1 ;?>" />
        <input type="hidden" name="quotePricingAry[hiddenPosition1]" id="hiddenPosition1" value="<?php echo $counter+1 ;?>" />
        <input type="hidden" name="quotePricingAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>" /> 
        <input type="hidden" name="quotePricingAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>" />  
        <input type="hidden" name="quotePricingAry[iSectionEdited]" id="iSectionEdited" value="<?php echo $iSectionEdited; ?>" />  
        <input type="hidden" name="quotePricingAry[idCustomerCurrency]" id="idCustomerCurrency" value="<?php echo $idCustomerCurrency; ?>" />  
        <input type="hidden" name="quotePricingAry[fCustomerExchangeRateGlobal]" id="fCustomerExchangeRateGlobal" value="<?php echo $fCustomerExchangeRateGlobal; ?>" />  
        <input type="hidden" name="quotePricingAry[fPriceMarkUpGlobal]" id="fPriceMarkUpGlobal" value="<?php echo $fPriceMarkUpGlobal; ?>" />  
        <div id="booking_quote_main_container_table" class="clearfix">  
            <div id="add_booking_quote_container_0" class="clearfix"> 
                 <?php 
                    echo display_booking_quote_pricing_details($kBooking,$number,$quotesAry[1],true,false,true);
                 ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div id="insurance_details_text" class="clearfix">&nbsp;</div>
        <div class="clearfix">&nbsp;</div>
        <div id="price_details_text" class="clearfix"> &nbsp;</div>  
    </form>
    <script type="text/javascript">
        $("#dtReminderDate").datepicker();
        var transportModeAry = new Array();
        <?php
            if(!empty($transportModeListAry))
            {
                foreach($transportModeListAry as $transportModeListArys)
                {
                    ?>
                    transportModeAry[<?php echo $transportModeListArys['id'] ?>] = '<?php echo $transportModeListArys['szShortName']?>'
                    <?php
                }
            }
        ?> 
        enableQuoteSendButton();    
        function updateEmailBody(number)
        {
            var hiddenPosition = $("#hiddenPosition").val();
            var hiddenPosition1 = $("#hiddenPosition1").val();  
            
            var szCustomerCurrency = $("#szCustomerCurrency_email").val();
            var szOriginCountry = $("#szOriginCountry_email").val();
            var szDestinationCountry = $("#szDestinationCountry_email").val();
            var szCargoText = $("#szCargoText_email").val();
            var szEmailBodyText = '';
            var counter = 1;
            var active_counter = 0;
              
            var active_counter=$('input.quoteActiveCb:checked').length
               
            if(active_counter>0)
            {    
                $("#szEmailSubject").removeAttr('disabled','disabled');
                $("#szQuotationBody").removeAttr('disabled','disabled');
                $("#szEmailBody").removeAttr('disabled','disabled');
                
                if(active_counter==1)
                { 
                    szEmailBodyText = '<?php echo $szOfferHeading; ?>' + "<br><br>" ; 
                }
                else
                { 
                    szEmailBodyText = '<?php echo $szMultipleOfferHeading ?>' + "<br><br>" ; 
                    szEmailBodyText += " \n\n  " + '<?php echo $szFromText; ?>' + ': ' + szOriginCountry + ' <br> \n\n '
                                + ' ' + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + ' <br> \n'
                                + '  ' + '<?php echo $szCargoText; ?>' + ': '+ szCargoText + '  <br> \n ';

                    szEmailBodyText += "<br><br>\n\n " + '<?php echo $szMultipleOfferHeading2; ?>' + " <br><br>\n\n" ; 
                }
                
                var number = 0;
                for(number =0; number<=hiddenPosition;number++)
                { 
                    var cb_flag = false;
                    if($("#iActive_"+number).length)
                    {
                        cb_flag = $("#iActive_"+number).prop("checked");  
                    } 
                    if(cb_flag)
                    {
                        var szTransportMode = '';
                        var idTransportMode = $("#idTransportMode_"+number).val();
                        if(idTransportMode>0)
                        {
                            szTransportMode = transportModeAry[idTransportMode] ;
                        }

                        var iTransitHours = $("#iTransitHours_"+number).val();
                        var fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrency_"+number).val(); 
                        var fTotalVat = parseFloat($("#fTotalVat_"+number).val());
                        var szQuotePriceStr =  szCustomerCurrency+" "+fTotalPriceCustomerCurrency+" all-in" ;
                        var dtOfferValid = $("#dtQuoteValidTo_"+number).val();
                        var fTotalInsuranceCostForBookingCustomerCurrency = $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).val();
                        var fInsuranceValueUptoPrice = $("#fInsuranceValueUpto_"+number).val();
                        
                        fInsuranceValueUptoPrice = parseFloat(fInsuranceValueUptoPrice);
                        var iInsuranceComments = $("#iInsuranceComments_"+number).prop("checked"); 
                        var szForwarderComment = $("#szForwarderComment_"+number).val(); 
                        var szForwarderCommentStr = '';
                        if(iInsuranceComments)
                        {
                            szForwarderCommentStr = ' ' +szForwarderComment + ' <br> \n ' ;
                        }
                        if(fTotalVat>0)
                        {
                            szQuotePriceStr += ", "+'<?php echo $szVatExcludingText; ?> ' + szCustomerCurrency+" "+fTotalVat ;
                        }
                        else
                        {
                            szQuotePriceStr += ", "+'<?php echo $szNoVatOnBookingText; ?>';
                        }
                        
                        var szInsuranceText = '';
                        if(fTotalInsuranceCostForBookingCustomerCurrency>0)
                        {
                            if(fInsuranceValueUptoPrice>0)
                            {
                                szInsuranceText = '<?php echo $szInsuranceText; ?>' + ': ' + " " + szCustomerCurrency+" "+fTotalInsuranceCostForBookingCustomerCurrency + '<?php echo $szInsuranceValueUptoText; ?>'+ " " + szCustomerCurrency + " " + fInsuranceValueUptoPrice + ' <br> \n ' ;
                            }
                            else
                            {
                                szInsuranceText = ' <?php echo $szInsuranceText; ?>' + ': ' + " " + szCustomerCurrency+" "+fTotalInsuranceCostForBookingCustomerCurrency + ' <br> \n ' ;
                            } 
                        } 
                        if(active_counter==1)
                        {
                            szEmailBodyText += '<?php echo $szFromText; ?>' + ': '+szOriginCountry + ' <br> \n '
                                + ' ' + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + '<br> \n'
                                + '  ' + '<?php echo $szCargoText; ?>' + ': ' + szCargoText + '<br> \n '
                                + ' '+'<?php echo $szModeText; ?>'+': ' + szTransportMode + ' <br>\n '
                                + ' ' + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + ' <br>\n '
                                + ' ' + '<?php echo $szPriceText; ?>'+': '+szQuotePriceStr+' <br>\n '
                                + szInsuranceText + szForwarderCommentStr + ''
                                + ' ' + '<?php echo $OfferValidUntillText; ?>' + ' ' + dtOfferValid+ ' <br><br>  \n\n szBookingQuoteLink <br><br>\n\n';
                        }
                        else
                        {
                            szEmailBodyText += "<strong><u>" + counter + '. ' + '<?php echo $szOfferText; ?>' + '  '
                                + ' ' + '<?php echo $szModeText; ?>' + ': ' + szTransportMode + ' <br>\n '
                                + ' ' + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + ' <br>\n '
                                + ' ' + '<?php echo $szPriceText; ?>' + ': ' + szQuotePriceStr + ' <br>\n '
                                + szInsuranceText + szForwarderCommentStr + ''
                                + ' ' + '<?php echo $OfferValidUntillText; ?>' + ' ' + dtOfferValid + ' <br><br>  \n\n szBookingQuoteLink <br><br>\n\n';
                        } 
                        counter++;
                    }  
                } 
                $("#szQuotationBody").val(szEmailBodyText);
            }
            else
            {
                $("#szQuotationBody").val(' ');
                $("#szEmailSubject").attr('disabled','disabled');
                $("#szQuotationBody").attr('disabled','disabled');
                $("#szEmailBody").attr('disabled','disabled');
                
                $("#request_quote_send_button").attr("style","opacity:0.4");
                $("#request_quote_send_button").unbind("click");  
            } 
        } 
        updateEmailBody(1);
        
        function calculate_gross_profit(number,change_field)
        {
            var fPrice = parseFloat($("#fTotalPriceCustomerCurrencyHidden_"+number).val());
            var fReferalAmount = parseFloat($("#fReferalAmountHidden_"+number).val());
            var fReferalPercentage = parseFloat($("#fReferalPercentageHidden_"+number).val());
            
            var fReferalAmountHidden2 = parseFloat($("#fReferalAmountHidden2_"+number).val());
            var fReferalPercentageHidden2 = parseFloat($("#fReferalPercentageHidden2_"+number).val());

            var iProfitType = parseInt($("#iProfitType_"+number).val());
            var fCustomerExchangeRate = parseFloat($("#fCustomerExchangeRate_"+number).val());
            var fForwarderExchangeRate = parseFloat($("#fForwarderExchangeRate_"+number).val());
            var fQuotePriceCustomerCurrency = parseFloat($("#fQuotePriceCustomerCurrency_"+number).val());
            var fTotalVatCustomerCurrency = parseFloat($("#fTotalVatCustomerCurrency_"+number).val());
            var fPriceMarkUp = parseFloat($("#fPriceMarkUp_"+number).val());
            
            var fTotalPriceForwarderCurrency = parseFloat($("#fTotalPriceForwarderCurrency_"+number).val());
            var fTotalVatForwarder = parseFloat($("#fTotalVatForwarder_"+number).val());


            var fTotalPriceForwarderCurrency_usd = parseFloat($("#fTotalPriceForwarderCurrency_usd_"+number).val());
            var fTotalVat_usd = parseFloat($("#fTotalVat_usd_"+number).val()); 

            var idForwarderAccountCurrency = parseFloat($("#idForwarderAccountCurrency_"+number).val()); 
            var idCustomerCurrency = parseFloat($("#idCustomerCurrency").val());  
            var iAddingNewQuote = parseInt($("#iAddingNewQuote_"+number).val()); 
  
            var fCustomerExchangeRate = parseFloat($("#fCustomerExchangeRateGlobal").val()); 
            var fPriceMarkUp = parseFloat($("#fPriceMarkUpGlobal").val());  
            var szPriceLogText = '';
            var szPriceLogFormula = '<h3>Formula</h3><hr>';
            
            if(isNaN(fPrice))
            {
                fPrice = 0;
            }
            if(isNaN(fReferalAmount))
            {
                fReferalAmount = 0;
            }
            if(isNaN(fReferalPercentage))
            {
                fReferalPercentage = 0;
            }
            if(isNaN(fTotalVatForwarder))
            {
                fTotalVatForwarder = 0;
            }
            if(isNaN(fTotalVat_usd))
            {
                fTotalVat_usd = 0;
            }
            if(isNaN(fTotalPriceForwarderCurrency_usd))
            {
                fTotalPriceForwarderCurrency_usd = 0;
            }
            var iTakeDataFromHidden = 0;
            if(change_field=='PRICE')
            {
                var fTotalPriceCustomerCurrency_1 = $("#fTotalPriceCustomerCurrency_"+number).val();
                var fTotalPriceCustomerCurrency_2 = $("#fTotalPriceCustomerCurrencyHidden2_"+number).val();
                
                console.log("price "+fTotalPriceCustomerCurrency_1+"  price2: "+fTotalPriceCustomerCurrency_2+" actual: "+fPrice);
                if(fTotalPriceCustomerCurrency_1!=fTotalPriceCustomerCurrency_2)
                {
                    fPrice = fTotalPriceCustomerCurrency_1 ;
                    iTakeDataFromHidden = 1;
                }
                else
                {
                    iTakeDataFromHidden = 2;
                }
            }
            else if(change_field=='PERCENTAGE')
            { 
                var fReferalPercentage_1 = $("#fReferalPercentage_"+number).val();
                var fReferalPercentage_2 = $("#fReferalPercentageHidden2_"+number).val();
                
                console.log("perc1 "+fReferalPercentage_1+"  perc2: "+fReferalPercentage_2+" actual: "+fReferalPercentage);
                if(fReferalPercentage_1!=fReferalPercentage_2)
                {
                    fReferalPercentage = fReferalPercentage_1;
                    iTakeDataFromHidden = 1;
                }
                else
                {
                    iTakeDataFromHidden = 2;
                }
            }
            else if(change_field=='REFERAL_AMOUNT')
            {
                var fReferalAmount_1 = $("#fReferalAmount_"+number).val(); 
                var fReferalAmount_2 = $("#fReferalAmountHidden2_"+number).val(); 
                
                console.log("ref1 "+fReferalAmount_1+"  ref2: "+fReferalAmount_2+" actual: "+fReferalAmount);
                if(fReferalAmount_1!=fReferalAmount_2)
                {
                    fReferalAmount = fReferalAmount_1;
                    iTakeDataFromHidden = 1;
                }
                else
                {
                    iTakeDataFromHidden = 2;
                }
            } 
            console.log("Values Working on");
            szPriceLogText = '<p>Customer Currency Exchange Rate: '+fCustomerExchangeRate+"</p>" ;
            szPriceLogText += '<p>Forwarder Currency Exchange Rate: '+fForwarderExchangeRate+"</p>" ;
            szPriceLogText += '<p>Forwarder Account Currency: '+idForwarderAccountCurrency+"</p>" ;
            szPriceLogText += '<p>Currency Mark-up: '+fPriceMarkUp+"</p><br><br>" ;
            
            if(fForwarderExchangeRate>0)
            {  
                szPriceLogFormula += '<p>Calculating Prices in to USD </p>'; 
                fTotalPriceForwarderCurrency_usd = (fTotalPriceForwarderCurrency * fForwarderExchangeRate);
                fTotalVat_usd = (fTotalVatForwarder * fForwarderExchangeRate);  
                
                szPriceLogFormula += "<p>Quote Price (USD) = (Forwarder Currency Price * Forwarder Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p>Quote Price (USD) = (" + fTotalPriceForwarderCurrency + "*"+fForwarderExchangeRate + ") = " + fTotalPriceForwarderCurrency_usd + "</p>";
                
                szPriceLogFormula += "<p> VAT Price (USD) = (VAT Price * Forwarder Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p>VAT Price (USD) = (" + fTotalVatForwarder + "*" + fForwarderExchangeRate + ") = " + fTotalVat_usd+"</p>" ;
            }  
            
            szPriceLogText += '<p>Quote Price (USD): '+fTotalPriceForwarderCurrency_usd + "</p>" ;
            szPriceLogText += '<p>Vat Price (USD): ' + fTotalVat_usd +  "</p><br><br>" ;
            
            if(fCustomerExchangeRate>0)
            {
                fTotalPriceCustomerCurrency = (fTotalPriceForwarderCurrency_usd / fCustomerExchangeRate);
                fTotalVat = (fTotalVat_usd / fCustomerExchangeRate) ;
                
                szPriceLogFormula += '<br> <p> Calculating Prices in to Customer currency </p>';  
                szPriceLogFormula += "<p> Quote Price Customer currency = (Quote Price (USD) / Customer Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p>Quote Price Customer currency = (" + fTotalPriceForwarderCurrency_usd + "/" + fCustomerExchangeRate + ") = " + fTotalPriceCustomerCurrency+"</p>" ;
                szPriceLogFormula += "<p> VAT Price Customer currency = (VAT Price (USD) / Customer Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p> VAT Price Customer currency = (" + fTotalVat_usd + "/" + fCustomerExchangeRate + ") = " + fTotalVat+"</p>" ;
            }   
            szPriceLogText += '<p>Quote Price Customer currency : '+fTotalPriceCustomerCurrency+"</p>" ;
            szPriceLogText += '<p>Vat Price Customer currency: '+fTotalVat+"</p><br><br>" ; 
            var fTotalPriceCustomerCurrency_display = fTotalPriceCustomerCurrency ;
            if(idForwarderAccountCurrency!=idCustomerCurrency)
            { 
//                var fTotalMarkupPrice = Math.round(((fTotalPriceCustomerCurrency * fPriceMarkUp)/100)); 
//                fTotalPriceCustomerCurrency = (fTotalPriceCustomerCurrency + fTotalMarkupPrice); 
//
//                var fTotalMarkupPriceOnVat = Math.round(((fTotalVat * fPriceMarkUp)/100),2) ; 
//                fTotalVat = (fTotalVat + fTotalMarkupPriceOnVat);  

                fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency * ( 1+ fPriceMarkUp/100) ;
                fTotalVat = fTotalVat * ( 1+ fPriceMarkUp/100) ;
                
                szPriceLogText += '<p>Currency Mark-up Applied: Yes</p>'  ; 
                szPriceLogText += '<p>Prices After currency Mark-up: </p>'  ; 
                szPriceLogText += '<p>Quote: '+fTotalPriceCustomerCurrency+'</p>'  ; 
                szPriceLogText += '<p>VAT: '+fTotalVat+'</p> <br><br>' ;  
            } 
            else
            {
                szPriceLogText += '<p>Currency Mark-up Applied: No</p><br><br>'  ; 
                fPriceMarkUp = 0;
            } 
            
//            if(change_field=='REFERAL_AMOUNT')
//            {
//                fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrencyHidden_"+number).val();
//                fTotalVat = $("#fTotalVatHidden_"+number).val(); 
//                
//                fTotalPriceForwarderCurrency_usd = fTotalPriceCustomerCurrency * fCustomerExchangeRate;
//                fTotalVat_usd =  fTotalVat * fCustomerExchangeRate ;
//                        
//                szPriceLogText += '<p>Adjusted Price (USD) : '+fTotalPriceForwarderCurrency_usd+"</p>" ;
//                szPriceLogText += '<p>Adjusted Vat (USD): '+fTotalVat_usd+"</p><br><br>" ;
//                
//                szPriceLogText += '<p>Adjusted Price Customer currency : '+fTotalPriceCustomerCurrency+"</p>" ;
//                szPriceLogText += '<p>Adjusted Vat Customer currency: '+fTotalVat+"</p><br><br>" ;
//            }
            
            console.log("Price: "+fPrice+" Precent: "+fReferalPercentage+" GP: "+fReferalAmount+" VAT: "+fTotalVat);
            if(change_field=='PRICE')
            { 
                var fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency;
                var fTempPrice = fTotalPriceCustomerCurrency+fTotalVat; 
                 
                var fAdjustedPriceUSD = fPrice * fCustomerExchangeRate ;
                if(iProfitType==2)
                {
                    var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd ; 
                    fReferalAmount = fPrice - fTotalPriceCustomerCurrency ;   
                    var fReferalAmountUSD = fReferalAmount * fCustomerExchangeRate;  
                    //var fReferalAmountUSD = fAdjustedPriceUSD - fTotalAmountUsd;
                            
                    //fReferalAmount = 0;
                    if(fCustomerExchangeRate>0)
                    {
                        //fReferalAmount = (fReferalAmountUSD / fCustomerExchangeRate);
                    }
                    if(fTotalAmountUsd!=0)
                    { 
                        fReferalPercentage = ((fReferalAmountUSD/(1+fPriceMarkUp/100)) / fTotalAmountUsd) * 100 ;
                    } 
                    
                    szPriceLogFormula += "<hr><p>Adjusted Price: "+fPrice+" Adjusted Price(USD): "+fAdjustedPriceUSD+" Profit Type: Mark-up </p>";
                    szPriceLogFormula += "<p> Gross Profit(USD) = (Adjusted Price(USD) - Quote Price(USD)) </p>";
                    szPriceLogFormula += "<p> Gross Profit = ("+ fAdjustedPriceUSD + " - " +fTotalPriceForwarderCurrency_usd+" ) = " + fReferalAmount + " </p>";
                    
                    szPriceLogFormula += "<p> Gross Profit (%) = (GrossProfitUSD *100) / (QuotePriceUSD + VATPriceUSD)</p>";
                    szPriceLogFormula += "<p> Gross Profit (%) = ("+fReferalAmountUSD+" * 100) / ("+fTotalPriceForwarderCurrency_usd+" + " + fTotalVat_usd +") = "+fReferalPercentage+" </p>";
                    
                    if(fReferalPercentage>0)
                    {
                        var fReferalAmount_vat = (fTotalVat * fReferalPercentage)/100;
                        //fTotalVat = fTotalVat + fReferalAmount_vat ; 
                    }
                    
                    var fTotalVatRounded = Math.round(fTotalVat) ;
                    var fTotalPriceCustomerCurrency_rounded = Math.round(fTotalPriceCustomerCurrency_display) ;
                    var fTotalDiplayedPriceRounded = Math.round(fPrice) ;
                    
                    fTotalVat = (fTotalVatRounded / fTotalPriceCustomerCurrency_rounded ) * fTotalDiplayedPriceRounded ;
                    
                    //fTotalVat = (fTotalVat / fTotalPriceCustomerCurrency_display ) * fPrice ;
                    szPriceLogFormula += "<p>Calculating New VAT </p>";
                    szPriceLogFormula += "<p> NEW VAT Price = Vat Price Customer currency + ((Vat Price Customer currency * Gross Profit (%))/100)  = "+ fTotalVat +"</p>";
                    
                    szPriceLogText += '<p>GP Type: Referral </p>'  ; 
                    szPriceLogText += '<p>Referral Percentage: '+fReferalPercentage+' </p>'  ; 
                    szPriceLogText += '<p>Referral Amount: '+fReferalAmount+' </p>'  ; 
                    szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                }
                else
                { 
                    szPriceLogFormula += "<hr><p>Adjusted Price: "+fPrice+" Profit Type: Referral </p>";
                    szPriceLogFormula += "<p>GP(USD): Price * Customer Currency Exchange rate / (1+CurrencyMarkup(ifApplicable)/100) – QuotePriceUSD*(1-CustomerStandardReferralFee/100) +  VATPriceUSD*CustomerStandardReferralFee/100</p>";
                    var CustomerStandardReferralFee = $("#fReferalPercentageStandarad_"+number).val() ;
                     
                    var fReferalAmountUSD = (fPrice * fCustomerExchangeRate / (1+fPriceMarkUp/100)) - (fTotalPriceForwarderCurrency_usd * (1 - CustomerStandardReferralFee/100)) + (fTotalVat_usd*CustomerStandardReferralFee/100) ;
                    
                    szPriceLogFormula += "<br> <p> GP(USD): "+fPrice+" * "+fCustomerExchangeRate+" / (1+"+fPriceMarkUp+"/100) – "+fTotalPriceForwarderCurrency_usd+"*(1-14.5/100) +  "+fTotalVat_usd+"*"+CustomerStandardReferralFee+"/100</p>";
                    szPriceLogFormula += "<br> <p> GP(USD)= "+fReferalAmountUSD ; 
                    
                    if(fCustomerExchangeRate>0)
                    {
                        fReferalAmount = fReferalAmountUSD/fCustomerExchangeRate * (1+fPriceMarkUp/100) ;
                    }
                    
                    szPriceLogFormula += "<br><br> <p>GP(Customer Currency): GrossProfitUSD / CustomerCurrencyExchangeRate * (1+CurrencyMarkup(ifApplicable)/100) </p>"; 
                    szPriceLogFormula += "<p>GP(Customer Currency): "+fReferalAmountUSD+" / "+fCustomerExchangeRate+" * (1+"+fPriceMarkUp+"/100) </p>";
                    szPriceLogFormula += "<p>GP(Customer Currency) = " + fReferalAmount ;
                    
                    var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd ;
                    if(fTotalAmountUsd>0)
                    {
                        fReferalPercentage = (fReferalAmountUSD / fTotalAmountUsd) * 100 ;
                    }
                    szPriceLogFormula += "<p>GP(%) = " + fReferalPercentage ;
                    
                    console.log("Referral Feed USD "+fReferalAmountUSD); 
                    szPriceLogText += '<p>GP Type: Mark-up </p>'  ; 
                    szPriceLogText += '<p>Referral Percentage: '+fReferalPercentage+' </p>'  ; 
                    szPriceLogText += '<p>Referral Amount: '+fReferalAmount+' </p>'  ; 
                    szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                } 
                
                $("#fReferalAmountHidden_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden_"+number).val(fReferalPercentage);
                $("#fTotalVatHidden_"+number).val(fTotalVat);
                $("#fTotalPriceCustomerCurrencyHidden_"+number).val(fPrice);
                
                var fTotalInvoiceAmount = fPrice + fTotalVat ;
                
                var fTotalVat = Math.round(fTotalVat);
                var fReferalAmount = Math.round(fReferalAmount);
                var fReferalPercentage = fReferalPercentage.toFixed(2);
                
                if(isNaN(fReferalAmount))
                {
                    fReferalAmount = 0;
                }
                if(isNaN(fTotalVat))
                {
                    fTotalVat = 0;
                }
                if(isNaN(fReferalPercentage))
                {
                    fReferalPercentage = 0;
                }
                
                $("#fReferalAmountHidden2_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage); 
                $("#fReferalAmount_"+number).val(fReferalAmount);
                $("#fReferalPercentage_"+number).val(fReferalPercentage); 
                
                if($("#vat_text_container_"+number).length)
                { 
                    $("#vat_text_container_"+number).html(number_format(fTotalVat));  
                }
                $("#fTotalVat_"+number).val(fTotalVat);   
                
                if(iTakeDataFromHidden===1)
                {
                    $("#fTotalPriceCustomerCurrencyHidden2_"+number).val(fPrice); 
                }
                $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage);  
                $("#fReferalAmountHidden2_"+number).val(fReferalAmount); 
                
                calculateActualGP(fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fTotalInvoiceAmount,fCustomerExchangeRate,iProfitType,fPriceMarkUp);
            }
            else if(change_field=='PERCENTAGE')
            { 
                fReferalAmount = 0;  
                var fReferalAmount_usd = 0;
                var fReferalAmount_vat_usd = 0; 
                
                if(fReferalPercentage>0)
                {
                    fReferalAmount_usd = (((fTotalPriceForwarderCurrency_usd + fTotalVat_usd) * fReferalPercentage)/100);
                    fReferalAmount_vat_usd = ((fTotalVat_usd * fReferalPercentage)/100);
                }  
                if(fCustomerExchangeRate>0)
                {
                    fReferalAmount = fReferalAmount_usd / fCustomerExchangeRate * (1+fPriceMarkUp/100);
                    fReferalAmount_vat = fReferalAmount_vat_usd / fCustomerExchangeRate * (1+fPriceMarkUp/100) ;
                }
                szPriceLogFormula += "<hr><p>Gross profit (%): "+fReferalPercentage+"</p>";
                szPriceLogFormula += "<p>Gross profit (USD) = ((Quote Price USD + Vat Price USD) * Gross profit (%) ) / 100  = "+fReferalAmount_usd+"</p>";
                szPriceLogFormula += "<p>Gross profit  = Gross profit (USD) / USD Exchange rate * (1+CurrencyMarkup/100) = "+fReferalAmount+"</p>";
                szPriceLogFormula += "<hr><p>Gross profit on VAT (USD) = ((Vat Price Customer currency) * Gross profit (%) ) / 100  = "+fReferalAmount_vat_usd+"</p>";
                szPriceLogFormula += "<p>Gross profit on VAT = Gross profit on VAT (USD) / USD Exchange rate * (1+CurrencyMarkup/100) = "+fReferalAmount_vat+"</p>";
                
                fTotalVatCustomerCurrency = fTotalVat ;  
                if(iProfitType===2)
                {
                    fTotalPriceCustomerCurrency = (fTotalPriceCustomerCurrency + fReferalAmount);
                    //fTotalVat = (fTotalVat + fReferalAmount_vat); 
                    var fTotalVat_1 = fTotalVat;
                    
                    var fTotalVatRounded = Math.round(fTotalVat) ;
                    var fTotalPriceCustomerCurrency_rounded = Math.round(fTotalPriceCustomerCurrency_display) ;
                    var fTotalDiplayedPriceRounded = Math.round(fTotalPriceCustomerCurrency) ;
                    
                    fTotalVat = (fTotalVatRounded / fTotalPriceCustomerCurrency_rounded ) * fTotalDiplayedPriceRounded ;
                    //fTotalVat = (fTotalVat / fTotalPriceCustomerCurrency_display ) * fTotalPriceCustomerCurrency ;
                    
                    szPriceLogText += '<p>GP Type: Mark-up </p>'; 
                    szPriceLogFormula += "<p>Profit Type: Mark-up</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency = (Quote Price Customer currency + Gross profit)  = "+fTotalPriceCustomerCurrency+"</p>";
                    //szPriceLogFormula += "<p>VAT Price Customer currency = (VAT Price Customer currency + Gross profit on VAT)  = "+fTotalVat+"</p>"; 
                    szPriceLogFormula += "<p>VAT Price Customer currency = (Total VAT / Quote Price) * Price Customer Currency  = ("+fTotalVat_1+"/"+fTotalPriceCustomerCurrency_display+") * "+fTotalPriceCustomerCurrency+" = "+fTotalVat+" </p>"; 
                }
                else
                {
                    fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency;
                    fTotalVat = fTotalVat;
                    
                    szPriceLogText += '<p>GP Type: Referral </p>'; 
                    
                    szPriceLogFormula += "<p>Profit Type: Referral</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency  = "+fTotalPriceCustomerCurrency+"</p>";
                    szPriceLogFormula += "<p>VAT Price Customer currency = "+fTotalVat+"</p>"; 
                }  
                
                szPriceLogText += '<p>Referral Amount: '+fReferalAmount+' </p>'  ; 
                szPriceLogText += '<p>Price: '+fTotalPriceCustomerCurrency+' </p>'  ; 
                szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                     
                $("#fReferalAmountHidden_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden_"+number).val(fReferalPercentage);
                $("#fTotalVatHidden_"+number).val(fTotalVat);
                $("#fTotalPriceCustomerCurrencyHidden_"+number).val(fTotalPriceCustomerCurrency);
                 
                var fTotalInvoiceAmount = fTotalPriceCustomerCurrency + fTotalVat ;
                 
                fTotalVat = Math.round(fTotalVat);
                fReferalAmount = Math.round(fReferalAmount); 
                fTotalPriceCustomerCurrency = Math.round(fTotalPriceCustomerCurrency);
                
                if(isNaN(fReferalAmount))
                {
                    fReferalAmount = 0;
                }
                if(isNaN(fTotalPriceCustomerCurrency))
                {
                    fTotalPriceCustomerCurrency = 0;
                }
                if(isNaN(fTotalVat))
                {
                    fTotalVat = 0;
                }  
                $("#fTotalPriceCustomerCurrency_"+number).val(fTotalPriceCustomerCurrency);
                
                if($("#customer_price_text_container_"+number).length)
                { 
                    $("#customer_price_text_container_"+number).html(number_format(fTotalPriceCustomerCurrency));  
                }
                if($("#vat_text_container_"+number).length)
                { 
                    $("#vat_text_container_"+number).html(number_format(fTotalVat));  
                }
                $("#fTotalVat_"+number).val(fTotalVat);  
                
                if(iTakeDataFromHidden===1)
                {
                    $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage); 
                }
                $("#fReferalAmountHidden2_"+number).val(fReferalAmount); 
                $("#fTotalPriceCustomerCurrencyHidden2_"+number).val(fTotalPriceCustomerCurrency);
                
                $("#fReferalAmount_"+number).val(fReferalAmount); 
                if($("#vat_text_container_"+number).length)
                { 
                    $("#vat_text_container_"+number).html(number_format(fTotalVat));  
                }
                $("#fTotalVat_"+number).val(fTotalVat);    
                 
                calculateActualGP(fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fTotalInvoiceAmount,fCustomerExchangeRate,iProfitType,fPriceMarkUp)
            }
            else if(change_field=='REFERAL_AMOUNT')
            {   
                szPriceLogFormula += "<hr><p>Gross profit: "+fReferalAmount+"</p>";
                var fTempPrice = fTotalPriceCustomerCurrency+fTotalVat; 
                
                var fReferalAmountUSD = fReferalAmount * fCustomerExchangeRate ;
                var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd ;

                if(fTotalAmountUsd>0)
                {
                    fReferalPercentage = ((fReferalAmountUSD/(1+fPriceMarkUp/100)) / fTotalAmountUsd) * 100 ;
                }   
                szPriceLogFormula += "<p> Gross Profit (%) = ((GrossProfitUSD/(1+CurrencyMarkup/100) / (QuotePriceUSD + VATPriceUSD)) * 100</p>";
                szPriceLogFormula += "<p> Gross Profit (%) = (("+fReferalAmountUSD+"/(1+ "+fPriceMarkUp+"/100 ) / ("+fTotalPriceForwarderCurrency_usd+" + " + fTotalVat_usd +")) * 100  = "+fReferalPercentage+" </p>";
                 
                fReferalPercentage = parseFloat(fReferalPercentage); 
                if(fReferalPercentage>0)
                {
                    fReferalAmount = ((fTempPrice * fReferalPercentage)/100);
                    fReferalAmount_vat = ((fTotalVat * fReferalPercentage)/100);
                } 
                szPriceLogFormula += "<hr><p>Gross profit on VAT = ((Vat Price Customer currency * Gross profit (%) ) / 100  = "+fReferalAmount_vat+"</p>";
                fTotalVatCustomerCurrency = fTotalVat ;  
                if(iProfitType==2)
                {
                    var fTotalPriceCustomerCurrency_display = fTotalPriceCustomerCurrency ;
                    var fTotalVat_display = fTotalVat ;
                    
                    fTotalPriceCustomerCurrency = (fTotalPriceCustomerCurrency + fReferalAmount);
                    //fTotalVat = (fTotalVat + fReferalAmount_vat);
                    
                    var fTotalVatRounded = Math.round(fTotalVat) ;
                    var fTotalPriceCustomerCurrency_rounded = Math.round(fTotalPriceCustomerCurrency_display) ;
                    var fTotalDiplayedPriceRounded = Math.round(fTotalPriceCustomerCurrency) ;
                    fTotalVat = (fTotalVatRounded / fTotalPriceCustomerCurrency_rounded ) * fTotalDiplayedPriceRounded ;
                    
                    //fTotalVat = (fTotalVat / fTotalPriceCustomerCurrency_display ) * fTotalPriceCustomerCurrency ;
                    szPriceLogText += '<p>GP Type: Mark-up </p>'  ;
                    
                    szPriceLogFormula += "<p>Profit Type: Mark-up</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency = (Quote Price Customer currency + Gross profit) </p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency = ("+fTotalPriceCustomerCurrency_display+"  + "+fReferalAmount+" ) = "+fTotalPriceCustomerCurrency+"</p>";
                    szPriceLogFormula += "<p>VAT Price Customer currency = (VAT Price Customer currency + Gross profit on VAT)  = "+fTotalVat+"</p>"; 
                    szPriceLogFormula += "<p>VAT Price Customer currency = ("+fTotalVat_display+"  + "+fReferalAmount_vat+" )  = "+fTotalVat+"</p>"; 
                }
                else
                {
                    fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency ;
                    fTotalVat = fTotalVat;
                    
                    szPriceLogFormula += "<p>Profit Type: Referral</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency  = "+fTotalPriceCustomerCurrency+"</p>";
                    szPriceLogFormula += "<p>VAT Price Customer currency = "+fTotalVat+"</p>"; 
                    
                    szPriceLogText += '<p>GP Type: Referral </p>'  ;
                } 
                
                szPriceLogText += '<p>Referral Percentage: '+fReferalPercentage+' </p>'  ; 
                szPriceLogText += '<p>Price: '+fTotalPriceCustomerCurrency+' </p>'  ; 
                szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                
                $("#fReferalAmountHidden_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden_"+number).val(fReferalPercentage);
                $("#fTotalVatHidden_"+number).val(fTotalVat);
                $("#fTotalPriceCustomerCurrencyHidden_"+number).val(fTotalPriceCustomerCurrency);
                
                var fTotalInvoiceAmount = fTotalPriceCustomerCurrency + fTotalVat ;
                
                var fReferalPercentage = fReferalPercentage.toFixed(2);
                fTotalVat = Math.round(fTotalVat);  
                fTotalPriceCustomerCurrency = Math.round(fTotalPriceCustomerCurrency);
                if(isNaN(fReferalPercentage))
                {
                    fReferalPercentage = 0;
                }
                if(isNaN(fTotalPriceCustomerCurrency))
                {
                    fTotalPriceCustomerCurrency = 0;
                }
                if(isNaN(fTotalVat))
                {
                    fTotalVat = 0;
                } 
                $("#fReferalPercentage_"+number).val(fReferalPercentage);
                $("#fTotalPriceCustomerCurrency_"+number).val(fTotalPriceCustomerCurrency);
                if($("#customer_price_text_container_"+number).length)
                { 
                    $("#customer_price_text_container_"+number).html(number_format(fTotalPriceCustomerCurrency));  
                }
                if($("#vat_text_container_"+number).length)
                { 
                    $("#vat_text_container_"+number).html(number_format(fTotalVat));  
                }
                $("#fTotalVat_"+number).val(fTotalVat); 
                
                if(iTakeDataFromHidden===1)
                {
                    $("#fReferalAmountHidden2_"+number).val(fReferalAmount);
                }
                
                $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage); 
                $("#fTotalPriceCustomerCurrencyHidden2_"+number).val(fTotalPriceCustomerCurrency);
                
                console.log("Price: "+fTotalInvoiceAmount+" vat: "+fTotalVat+" referal: "+fReferalAmount+" percentage: "+fReferalPercentage);
                calculateActualGP(fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fTotalInvoiceAmount,fCustomerExchangeRate,iProfitType,fPriceMarkUp);
            }
            
            if(number==1001)
            {
                var szPriceLogHeading = "<hr> <h3> Price Calculations </h3> <hr> ";
                szPriceLogText = szPriceLogHeading + szPriceLogText + "<br /><br />"+ szPriceLogFormula ;
                $("#price_details_text").html(szPriceLogText);
            }
            calculate_quote_insurance_price(number);
        } 
        function calculateActualGP(fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fPrice,fCustomerExchangeRate,iProfitType,fPriceMarkUp)
        {  
            var number = '1001';  
            var fAdjustedPriceUSD = fPrice * fCustomerExchangeRate ;
            console.log("Adjusted Price(USD): "+fAdjustedPriceUSD+" price(DKK): "+fPrice+" Exchange: "+fCustomerExchangeRate);
            
            if(iProfitType==2)
            {
                var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd ;  
                var fReferalAmountUSD = fAdjustedPriceUSD - fTotalAmountUsd;

                var fReferalAmount = 0;
                if(fCustomerExchangeRate>0)
                {
                    fReferalAmount = (fReferalAmountUSD / fCustomerExchangeRate);
                }
                console.log("Ref USD: "+fReferalAmountUSD+" Mark: "+fPriceMarkUp+" USD: "+fAdjustedPriceUSD);
                if(fTotalAmountUsd!=0)
                { 
                    var fReferalPercentage = ((fReferalAmountUSD/(1+fPriceMarkUp/100)) / fAdjustedPriceUSD )* 100 ;
                }    
                var fReferalPercentage = fReferalPercentage.toFixed('6');
                
                $("#actual_gp_percentage_span_"+number).html(fReferalPercentage); 
                $("#actual_gp_value_span_"+number).html(fReferalAmount);  
            }
            else
            {
                $("#actual_gp_percentage_span_"+number).html(" "); 
                $("#actual_gp_value_span_"+number).html(" ");  
            } 
        }
        
        
        function update_quote_price(number)
        { 
            var fPriceForwarderCurrency = $("#fTotalPriceForwarderCurrency_"+number).val();
            var fTotalVatForwarder = $("#fTotalVatForwarder_"+number).val();
            var idForwarderCurrency = $("#idForwarderCurrency_"+number).val();
            var fForwarderExchangeRate = $("#fForwarderExchangeRate_"+number).val();
            var fCustomerExchangeRate = $("#fCustomerExchangeRate_"+number).val();
            var idForwarderAccountCurrency = parseFloat($("#idForwarderAccountCurrency_"+number).val()); 
            var idCustomerCurrency = parseFloat($("#idCustomerCurrency").val()); 
            var fPriceMarkUp = parseFloat($("#fPriceMarkUpGlobal").val());  
            var iAddingNewQuote = parseInt($("#iAddingNewQuote_"+number).val());  
            var iProfitType = parseInt($("#iProfitType_"+number).val());
            
            var fCustomerExchangeRate = parseFloat($("#fCustomerExchangeRateGlobal").val()); 
            fTotalVatForwarder = parseFloat(fTotalVatForwarder);
             
            if(idForwarderCurrency==idCustomerCurrency)
            {
                var fQuotePriceCustomerCurrency = Math.round(fPriceForwarderCurrency);  
                var fVATCustomerCurrency = Math.round(fTotalVatForwarder);  
            }
            else
            { 
                if(fForwarderExchangeRate>0)
                {
                    //converting forwarder currency price to USD
                    var fQuotePriceCustomerCurrency = fPriceForwarderCurrency * fForwarderExchangeRate; 
                    var fVATCustomerCurrency = fTotalVatForwarder * fForwarderExchangeRate; 
                }
                else
                {
                    var fQuotePriceCustomerCurrency = 0;
                    var fVATCustomerCurrency = 0;
                } 
                if(fCustomerExchangeRate>0)
                {
                    //converting USD price to customer currency
                    var fQuotePriceCustomerCurrency = fQuotePriceCustomerCurrency/fCustomerExchangeRate; 
                    var fVATCustomerCurrency = fTotalVatForwarder/fCustomerExchangeRate; 
                }
                else
                {
                    var fQuotePriceCustomerCurrency = 0;
                } 
                var fQuotePriceCustomerCurrency = fQuotePriceCustomerCurrency
            } 
            
            if(idCustomerCurrency!=idForwarderAccountCurrency)
            {
                var fQuotePriceCustomerCurrency = fQuotePriceCustomerCurrency * (1+fPriceMarkUp/100);
                fQuotePriceCustomerCurrency = Math.round(fQuotePriceCustomerCurrency); 
            }
            
            console.log(" out "+fQuotePriceCustomerCurrency);
            $("#fQuotePriceCustomerCurrency_"+number).val(fQuotePriceCustomerCurrency);
            calculate_gross_profit(number,'PERCENTAGE');
             
            updateEmailBody(number);
        }
        
        
        
        
var insuranceCalculationAry = new Array();
insuranceCalculationAry['fLargetSellPrice'] = '<?php echo $fLargetSellPrice; ?>';
insuranceCalculationAry['fTotalBookingPriceUSD'] = '<?php echo $fTotalBookingPriceUsd; ?>';
insuranceCalculationAry['fBookingExchangeRate'] = '<?php echo $fCustomerExchangeRate; ?>'; 
insuranceCalculationAry['idGoodsInsuranceCurrency'] = '<?php echo $idGoodsInsuranceCurrency; ?>';
insuranceCalculationAry['szGoodsInsuranceCurrency'] = '<?php echo $szGoodsInsuranceCurrency; ?>';
insuranceCalculationAry['fGoodsInsuranceExchangeRate'] = '<?php echo $fGoodsInsuranceExchangeRate; ?>';
var fGoodsInsurancePriceUSD = '<?php echo $fValueOfGoodsUSD ; ?>'; 
var fTotalBookingPriceCustomerCurrency = '<?php echo $fTotalBookingPriceCustomerCurrency ?>'; 

var insuranceCalculationAry = new Array();
insuranceCalculationAry['fLargetSellPrice'] = '<?php echo $fLargetSellPrice; ?>';
insuranceCalculationAry['fTotalBookingPriceUSD'] = '<?php echo $fTotalBookingPriceUsd; ?>';
insuranceCalculationAry['fBookingExchangeRate'] = '<?php echo $fCustomerExchangeRate; ?>'; 
insuranceCalculationAry['idGoodsInsuranceCurrency'] = '<?php echo $idGoodsInsuranceCurrency; ?>';
insuranceCalculationAry['szGoodsInsuranceCurrency'] = '<?php echo $szGoodsInsuranceCurrency; ?>';
insuranceCalculationAry['fGoodsInsuranceExchangeRate'] = '<?php echo $fGoodsInsuranceExchangeRate; ?>';
var fGoodsInsurancePriceUSD = '<?php echo $fValueOfGoodsUSD ; ?>'; 
var fTotalBookingPriceCustomerCurrency = '<?php echo $fTotalBookingPriceCustomerCurrency ?>'; 

var idAllTransportMode = '<?php echo __ALL_WORLD_TRANSPORT_MODE_ID__ ?>';
var idRestTransportMode = '<?php echo __REST_TRANSPORT_MODE_ID__ ?>';
var fInsuranceSellCurrencyExchangeRate = '<?php echo $fInsuranceSellCurrencyExchangeRate ?>';  

var insuranceBuySellRateAry  = new Array();  

<?php 

    if(!empty($transportModeListAry))
    {
        foreach($transportModeListAry as $transportModeListArys)
        {
            ?>
            insuranceBuySellRateAry[<?php echo $transportModeListArys['id'] ?>] = new Array();
            <?php
        }
    } 
    if(!empty($transportModeListAry))
    { 
        if(!empty($buyRateListAry))
        {
            foreach($buyRateListAry as $key=>$buyRateListArys)
            { 
                $insuranceSellRateAry = array(); 
                $insuranceSellRateAry = $buyRateListArys['sellRateAry'];  

                if(!empty($insuranceSellRateAry))
                {
                    $ctr=0;
                    foreach($insuranceSellRateAry as $insuranceDetailsArys)
                    { 
                        ?> 
                        var insuranceRateAry = [];
                        insuranceBuySellRateAry['<?php echo $key; ?>']['<?php echo $ctr ?>'] = new Array();

                        insuranceRateAry['fInsuranceUptoPrice'] = '<?php echo $insuranceDetailsArys['fInsuranceUptoPrice']; ?>';
                        insuranceRateAry['szInsuranceCurrency'] = '<?php echo $insuranceDetailsArys['szInsuranceCurrency']; ?>';
                        insuranceRateAry['fInsuranceExchangeRate'] = fInsuranceSellCurrencyExchangeRate;
                        insuranceRateAry['fInsuranceRate'] = '<?php echo $insuranceDetailsArys['fInsuranceRate']; ?>';
                        insuranceRateAry['fInsuranceMinPrice'] = '<?php echo $insuranceDetailsArys['fInsuranceMinPrice']; ?>';
                        insuranceRateAry['szInsuranceMinCurrency'] = '<?php echo $insuranceDetailsArys['szInsuranceMinCurrency']; ?>';
                        insuranceRateAry['fInsuranceMinExchangeRate'] = fInsuranceSellCurrencyExchangeRate;
                        insuranceRateAry['fInsurancePriceUSD'] = '<?php echo $insuranceDetailsArys['fInsurancePriceUSD']; ?>';
                        insuranceRateAry['fInsuranceMinPriceUSD'] = '<?php echo $insuranceDetailsArys['fInsuranceMinPriceUSD']; ?>'; 
                        insuranceBuySellRateAry['<?php echo $key; ?>']['<?php echo $ctr ?>'].push(insuranceRateAry);    
                        <?php 
                        $ctr++;
                    }
                }
            }
        }
    }
?>  
    function calculate_quote_insurance_price(number)
    {    
        console.log("Insurance Pricing called ");
        var idTransportMode = parseInt($("#idTransportMode_"+number).val());
        var insuranceSellRateAry = new Array();
        var arrayKeys = Object.keys(insuranceBuySellRateAry); 
        console.log(insuranceBuySellRateAry);
        
        if(jQuery.inArray(idTransportMode,arrayKeys))
        {
            if(idTransportMode>0)
            { 
                if(insuranceBuySellRateAry[idTransportMode].length>0)
                {
                    //All ok no need to do any thing
                    insuranceSellRateAry = insuranceBuySellRateAry[idTransportMode] ;
                }
                else if(insuranceBuySellRateAry[idAllTransportMode].length>0)
                {
                    insuranceSellRateAry = insuranceBuySellRateAry[idAllTransportMode] ;
                }
                else if(insuranceBuySellRateAry[idRestTransportMode].length>0)
                {
                    insuranceSellRateAry = insuranceBuySellRateAry[idRestTransportMode] ;
                }
            }
        }  
        var iArrayLength = insuranceSellRateAry.length ; 
        var fTotalInsuranceCostForBookingCustomerCurrency = 0;
        var fInsuranceUptoPrice = 0;
        
        if(iArrayLength>0)
        {
            var szInsuranceLogString = " <hr> <h3> Insurance Details </h3> <hr> <br>";
            var szInsuranceFormulaString = " <hr> <h3> Insurance Calculation Formula </h3> <hr> <br>";
            
            var fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrency_"+number).val();
            fTotalPriceCustomerCurrency = parseInt(fTotalPriceCustomerCurrency);

            if(isNaN(fTotalPriceCustomerCurrency))
            {
                console.log("unrecognized character"); 
            }
            else
            { 
                var fTotalBookingPriceUSD = fTotalPriceCustomerCurrency * insuranceCalculationAry['fBookingExchangeRate'];  
                
                szInsuranceLogString += "<br> Booking Price Customer Currency: "+fTotalPriceCustomerCurrency +"<br> Booking Exchange Rate: "+insuranceCalculationAry['fBookingExchangeRate']+"<br> Booking Price(USD): "+fTotalBookingPriceUSD ;
                
                szInsuranceFormulaString += "<br> Booking Price (USD)= Booking Price Customer Currency * Booking Exchange Rate ";                
                szInsuranceFormulaString += " <br> Booking Price (USD)= "+fTotalPriceCustomerCurrency+" * "+insuranceCalculationAry['fBookingExchangeRate']+" = "+fTotalBookingPriceUSD;
                
                var fTotalBookingAmountToBeInsurancedUsd = parseInt(fTotalBookingPriceUSD) + parseInt(fGoodsInsurancePriceUSD);  
                szInsuranceLogString += "<br> Value of Goods(USD): "+fGoodsInsurancePriceUSD + " <br> Amount to be Insured (USD): "+fTotalBookingAmountToBeInsurancedUsd ;
                
                szInsuranceFormulaString += " <br><br> Amount to be Insured (USD) = Booking Price (USD) + Value of Goods(USD) ";
                szInsuranceFormulaString += "<br> Amount to be Insured (USD) = "+fTotalBookingPriceUSD+"  + "+fGoodsInsurancePriceUSD + " = "+fTotalBookingAmountToBeInsurancedUsd;
                
                var insuranceDetailsAry = new Array();
                insuranceDetailsAry = getNearestRecord(fTotalBookingAmountToBeInsurancedUsd,number); 
                var iArrayLength = insuranceDetailsAry.length ;
                
                if (insuranceDetailsAry['fInsuranceUptoPrice'] != null) 
                {
                    szInsuranceLogString += " <br><br> Insurance Found: Yes  ";
                    
                    szInsuranceLogString += "<br><br> Value upto: "+insuranceDetailsAry['fInsuranceUptoPrice'];
                    szInsuranceLogString += "<br> Value upto Currency : "+insuranceDetailsAry['szInsuranceCurrency'];
                    szInsuranceLogString += "<br> Value upto Currency Exchange Rate: "+insuranceDetailsAry['fInsuranceExchangeRate']; 
                    szInsuranceLogString += "<br> Insurance Rate : "+insuranceDetailsAry['fInsuranceRate']; 
                    
                    szInsuranceLogString += "<br><br> Min Price : "+insuranceDetailsAry['fInsuranceMinPrice'];
                    szInsuranceLogString += "<br> Min Price Currency : "+insuranceDetailsAry['szInsuranceMinCurrency'];
                    szInsuranceLogString += "<br> Min Price Currency Exchange Rate: "+insuranceDetailsAry['fInsuranceMinExchangeRate'];
                    
                    szInsuranceFormulaString += "<br><br> Insurance Found: Yes ";
                    var fTotalInsuranceCostUsd = (fTotalBookingAmountToBeInsurancedUsd * parseFloat(insuranceDetailsAry['fInsuranceRate']))/100;  
                    
                    szInsuranceFormulaString += " <br><br> Insurance Cost(USD) = Amount to be Insured (USD) * Insurance Rate ";
                    szInsuranceFormulaString += " <br> Insurance Cost(USD) = "+fTotalBookingAmountToBeInsurancedUsd+" * "+insuranceDetailsAry['fInsuranceRate']+" = "+fTotalInsuranceCostUsd;
                    
                    insuranceDetailsAry['fInsuranceExchangeRate'] = parseFloat(insuranceDetailsAry['fInsuranceExchangeRate']);
                    if(insuranceDetailsAry['fInsuranceExchangeRate']>0)
                    {
                        var  fTotalInsuranceCost = parseFloat(fTotalInsuranceCost)/parseFloat(insuranceDetailsAry['fInsuranceExchangeRate']);
                    }
                    else
                    {
                        var  fTotalInsuranceCost = 0;
                    }  
                    
                    var fTotalInsuranceMinCost = parseFloat(insuranceDetailsAry['fInsuranceMinPrice']); 
                    var fTotalInsuranceMinCostUsd = parseFloat(insuranceDetailsAry['fInsuranceMinPrice']) * parseFloat(insuranceDetailsAry['fInsuranceMinExchangeRate']) ;
                    var fInsuranceUptoPrice = insuranceDetailsAry['fInsuranceUptoPrice'];
                    
                    szInsuranceFormulaString += " <br><br> Insurance Min Cost(USD) = Min Price * Min Price Currency Exchange Rate ";
                    szInsuranceFormulaString += " <br> Insurance Min Cost(USD) = "+insuranceDetailsAry['fInsuranceMinPrice']+" * "+insuranceDetailsAry['fInsuranceMinExchangeRate']+" = "+fTotalInsuranceMinCostUsd;
                    
                    szInsuranceFormulaString += "<br> <br> IF(Insurance Min Cost(USD) > Insurance Cost(USD)) THEN <br> &nbsp;&nbsp;&nbsp; Final Insurance Cost(USD) = Insurance Min Cost(USD) <br> ELSE <br> &nbsp;&nbsp;&nbsp; Final Insurance Cost(USD) = Insurance Cost(USD)";
                     
                    if(fTotalInsuranceMinCostUsd>fTotalInsuranceCostUsd)
                    {
                        fTotalInsuranceCost = fTotalInsuranceMinCost ;
                        fTotalInsuranceCostUsd = fTotalInsuranceMinCostUsd ;
                    }

                    szInsuranceFormulaString += "<br><br> Final Insurance Cost(USD) = "+fTotalInsuranceCostUsd ;
                    
                    if(insuranceCalculationAry['fBookingExchangeRate']>0)
                    {
                        var fTotalInsuranceCostForBookingCustomerCurrency = Math.ceil(parseFloat(fTotalInsuranceCostUsd / parseFloat(insuranceCalculationAry['fBookingExchangeRate'])));
                    }
                    else
                    {
                        var fTotalInsuranceCostForBookingCustomerCurrency = "No match" ;
                    }  
                    
                    szInsuranceFormulaString += "<br><br> Insurance Sell Rate Customer Currency = Final Insurance Cost(USD) / Booking Exchange Rate " ;
                    szInsuranceFormulaString += "<br><br> Insurance Sell Rate Customer Currency = "+fTotalInsuranceCostUsd+" / "+insuranceCalculationAry['fBookingExchangeRate']+" = "+fTotalInsuranceCostForBookingCustomerCurrency ;
                    
                    $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).val(fTotalInsuranceCostForBookingCustomerCurrency);
                    $("#fInsuranceValueUpto_"+number).val(fInsuranceUptoPrice); 
                    
                    var szPriceLogText = szInsuranceLogString + "<br /><br />"+ szInsuranceFormulaString;
                    //$("#insurance_details_text").html(szPriceLogText);
                }
                else
                {
                    $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).val(fTotalInsuranceCostForBookingCustomerCurrency);
                    $("#fInsuranceValueUpto_"+number).val(fInsuranceUptoPrice);
                }
            }
        } 
    }
 
    function getNearestRecord(fMaxBookingAmountToBeInsuranced,number)
    {
        var idTransportMode = parseInt($("#idTransportMode_"+number).val());
        var insuranceSellRateAry = new Array();
        
        var idTransportMode = parseInt($("#idTransportMode_"+number).val());
        var insuranceSellRateAry = new Array();

        if(idTransportMode>0 && insuranceBuySellRateAry.length)
        { 
            if(insuranceBuySellRateAry[idTransportMode].length>0)
            {
                //All ok no need to do any thing
                insuranceSellRateAry = insuranceBuySellRateAry[idTransportMode] ;
            }
            else if(insuranceBuySellRateAry[idAllTransportMode].length>0)
            {
                insuranceSellRateAry = insuranceBuySellRateAry[idAllTransportMode] ;
            }
            else if(insuranceBuySellRateAry[idRestTransportMode].length>0)
            {
                insuranceSellRateAry = insuranceBuySellRateAry[idRestTransportMode] ;
            }
        } 
         
        var length = insuranceSellRateAry.length ; 
        var ret_ary = new Array();
        for (var i = 0; i < length; i++) 
        {  
            console.log(insuranceSellRateAry[i][0]); 
            console.log("arr "+insuranceSellRateAry[i][0]['fInsurancePriceUSD'] + " form"+fMaxBookingAmountToBeInsuranced);
            if(fMaxBookingAmountToBeInsuranced <= insuranceSellRateAry[i][0]['fInsurancePriceUSD'])
            {     
                console.log("found record ");
                console.log(insuranceSellRateAry[i][0]);
                ret_ary = insuranceSellRateAry[i][0] ;  
                break;
            } 
        }
        return ret_ary; 
    }
    
    $(document).ready(function(){
        update_quote_price('1001');
    });
</script>
    </div>
    <br><br> 
</div> 
</div>	
<?php  
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>