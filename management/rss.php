<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__MESSAGE_RSS_FEED__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Send Messages - Rss Feed";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/rss/";
validateManagement();
//$crownJobs=$kAdmin->selectCrownJobs(2);

/*
 * We have discontinued using RSS feeds from 17th March 2017 - @Ajay
 */
ob_end_clean();
header("Location: ".__BASE_URL__);
die;
?>
<script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?=__BASE_URL__?>/js/tiny_mce/tiny_mce_edit.js"></script>
<script type="text/javascript">
	setup();
</script>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/admin_messages_left_nav.php" ); ?> 	
	<div class="hsbody-2-right">
		<div id="Error"></div>
			<div id ="table" style="padding-bottom:20px;">
			<label class="ui-full-fields">
				<span class="field-name"><?=t($t_base.'fields/heading');?></span>
				<span class="field-container">
					<input type="text" id="szHeading" style="width: 600px;">
				</span>
			</label>
			<label class="ui-full-fields">
				<span class="field-name"><?=t($t_base.'fields/url');?></span>
				<span class="field-container">
					<input type="text" id="szUrl" style="width: 600px;">
				</span>
			</label>						
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/content');?></span>
			</label>
				<textarea rows="20" cols="94" name="szDescription" id="szDescription" class="myTextEditor"></textarea> 
				<br/><br/>
                                <p style="float: right;">
                                    <a class="button1" onclick="preview_rss_feed();"><span><?=t($t_base.'fields/preview');?></span></a>
                                    <a class="button1" onclick="save_rss_feed();"><span><?=t($t_base.'fields/publish');?></span></a>
                                </p>
			</div>
			<br/>
			<div id="previewText">
			</div>		
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>