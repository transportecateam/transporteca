<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset($_SESSION )) 
{
  session_start();
} 
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();  
$kQuote = new cQuote();
$kBooking = new cBooking();
$kWhsSearch = new cWHSSearch();
$kServices = new cServices();
$mode = sanitize_all_html_input(trim($_REQUEST['mode'])); 

if($mode=='DISPLAY_WAREHOUSE_DROPDOWN')
{
    $idForwarder = sanitize_all_html_input(trim($_REQUEST['forwarder_id'])); 
    if($idForwarder>0)
    {
        $warehouseListAry = $kWhsSearch->getAllWareHouses('forwarder_warehouse',$idForwarder); 
    }
    echo "SUCCESS||||";
    echo display_warehouse_dropdown($warehouseListAry,'idFromWarehouse',$idFromWarehouse);
    echo "||||";
    echo display_warehouse_dropdown($warehouseListAry,'idToWarehouse',$idToWarehouse);
    die;
}
else if($mode=='CLEAR_RAIL_TRANSPORT_FORM')
{
    $_POST['addRailTransportAry'] = array();
    echo "SUCCESS||||";
    echo display_lcl_rail_transport_addedit_form($kServices);
    die;
}
else if($mode=='EDIT_RAIL_TRANSPORT')
{
    $idRailTransport = sanitize_all_html_input(trim($_POST['idRailTransport']));
    if($idRailTransport>0)
    {
        $_POST['addRailTransportAry'] = array();
        $railTransportListAry = $kServices->getAllRailTransport($idRailTransport); 
        echo "SUCCESS||||";
        echo display_lcl_rail_transport_addedit_form($kServices,$railTransportListAry[0]);
    } 
    die;
}
else if($mode=='DELETE_RAIL_TRANSPORT')
{
    $idRailTransport = sanitize_all_html_input(trim($_POST['idRailTransport']));
    if($kServices->deleteRailTransport($idRailTransport))
    {
        $_POST['addRailTransportAry'] = array();
        echo "SUCCESS_DELETE||||";
        echo display_lcl_rail_icon_listing($kServices);
    } 
    die;
}
else if(!empty($_POST['addRailTransportAry']))
{
    if($kServices->addEditRailTransport($_POST['addRailTransportAry']))
    {
        $_POST['addRailTransportAry'] = array();
        echo "SUCCESS||||";
        echo display_lcl_rail_icon_listing($kServices,1);
    }
    else
    {
        echo "ERROR||||";
        echo display_lcl_rail_transport_addedit_form($kServices);
    }
    die;
}