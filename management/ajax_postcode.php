<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
$t_base_error="management/Error/";
$t_base = "management/postcode/";
validateManagement();
if(ISSET($_REQUEST))
{	
	$kConfig	= 	new cConfig;
	$action		=	trim(sanitize_all_html_input($_REQUEST['action']));
	if($action=='POST_CODE')
	{
		$countryId		=	(int)sanitize_all_html_input($_REQUEST['idCountry']);
		$szCityPostcode	=	trim((string)sanitize_all_html_input($_REQUEST['q']));
		$limit			=	(int)sanitize_all_html_input($_REQUEST['limit']);	
		$city			=   trim((string)sanitize_all_html_input($_REQUEST['szCity'])); 
		$postcode		=	$kConfig->selectPostcode($szCityPostcode,$city,$countryId,$limit);	
		if($postcode)
		{
			foreach($postcode as $key=>$value)
			{
				echo $value['szPostCode']."\n";
			}
		}	
	}
	if($action=='CITY')
	{	
		$countryId		=	(int)sanitize_all_html_input($_REQUEST['idCountry']);
		$szCityPostcode	=	trim((string)sanitize_all_html_input($_REQUEST['q']));
		$limit			=	(int)sanitize_all_html_input($_REQUEST['limit']);	
		$cityPostCodeAry		=	$kConfig->selectCity($szCityPostcode,$countryId,$limit);		
		
		if(!empty($cityPostCodeAry))
		{
			$str_length = strlen($szCityPostcode);
			$final_ary = array();
			$countr = 0;
			foreach($cityPostCodeAry as $cityPostCodeArys)
			{
				if(!empty($cityPostCodeArys['szCity']) && (substr(mb_strtolower($cityPostCodeArys['szCity'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity1']) && (substr(mb_strtolower($cityPostCodeArys['szCity1'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity1'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity2']) && (substr(mb_strtolower($cityPostCodeArys['szCity2'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity2'] ;
				}
				else if(!empty($cityPostCodeArys['szCity3']) && (substr(mb_strtolower($cityPostCodeArys['szCity3'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity3'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity4']) && (substr(mb_strtolower($cityPostCodeArys['szCity4'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity4'] ;
				}
				else if(!empty($cityPostCodeArys['szCity5']) && (substr(mb_strtolower($cityPostCodeArys['szCity5'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity5'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity6']) && (substr(mb_strtolower($cityPostCodeArys['szCity6'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity6'] ;
				}
				else if(!empty($cityPostCodeArys['szCity7']) && (substr(mb_strtolower($cityPostCodeArys['szCity7'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity7'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity8']) && (substr(mb_strtolower($cityPostCodeArys['szCity8'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity8'] ;
				}
				else if(!empty($cityPostCodeArys['szCity9']) && (substr(mb_strtolower($cityPostCodeArys['szCity9'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity9'] ;
				}	
				else if(!empty($cityPostCodeArys['szCity10']) && (substr(mb_strtolower($cityPostCodeArys['szCity10'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szCity10'] ;
				}
				else if(!empty($cityPostCodeArys['szRegion1']) && (substr(mb_strtolower($cityPostCodeArys['szRegion1'],'UTF-8'),0,$str_length) == mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion1'] ;
				}				
				else if(!empty($cityPostCodeArys['szRegion2']) && (substr(mb_strtolower($cityPostCodeArys['szRegion2'],'UTF-8'),0,$str_length)== mb_strtolower($szCityPostcode,'UTF-8')))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion2'] ;
				}				
				else if(!empty($cityPostCodeArys['szRegion3']) && (substr(mb_strtolower($cityPostCodeArys['szRegion3'],'UTF-8'),0,$str_length)==$szCityPostcode))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion3'] ;
				}
				if(!empty($cityPostCodeArys['szRegion4']) && (substr(mb_strtolower($cityPostCodeArys['szRegion4'],'UTF-8'),0,$str_length)==$szCityPostcode))
				{
					$final_ary[$countr] = $cityPostCodeArys['szRegion4'] ;
				}
				$countr++;
			}
			$resultingAry = array_unique ($final_ary , SORT_STRING) ;
			asort($resultingAry);
		}
		if(!empty($resultingAry))
		{
			foreach($resultingAry as $resultingArys)
			{
				echo $resultingArys."\n";
			}
		}	
	}
	iF($action=='SHOW_EMPTY_POST_CODE')
	{
			showPostcodeDetails();
	}
	if($action=='SUBMIT_SEARCH_FORM')
	{
		$from = 0;
		$perPage = __CONTENT_PER_PAGE_POSTCODE__;
		$postcode		=	$kConfig->selectPostCodeDetails($_POST['searchAry'],$from,$perPage);
		if(!empty($kConfig->arErrorMessages))
		{
			echo "ERROR|||||";
		?>
		<div id="regError" class="errorBox" style="display:block;">
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kConfig->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?
		}
		else
		{	echo 'SUCCESS|||||';
			if($postcode!=array())
			{	
				$total = $kConfig->countPostCodeDetails($_POST['searchAry']);
				postcodeDet($postcode,$total);
			}
			else
			{
				echo 'ERROR|||||';
			}
		}
	}
	if($action=='POSTCODE_CONTENT')
	{	
		$searchAry = array();
		$searchAry['szOriginCountry'] = $_POST['country'];
		$searchAry['szOriginCity'] = $_POST['city'];
		$searchAry['szOriginPostCode'] = $_POST['postcode'];
		$postcode		=	$kConfig->selectPostCodeDetails($searchAry);
		if($postcode!=array())
		{	echo 'SUCCESS|||||';
			$count	=1;
			foreach($postcode as $post)
			{
			echo '
				<tr id=\'postcode_details_'.$count.'\' onclick="showPostCodeTable(this.id,\''.$post['id'].'\')">
					<td>'.$post['szPostCode'].'</td>
					<td>'.$post['szLat'].'</td>
					<td>'.$post['szLng'].'</td>
					<td>'.$post['szRegion1'].'</td>
					<td>'.$post['szRegion2'].'</td>
					<td>'.$post['szRegion3'].'</td>
					<td>'.$post['szRegion4'].'</td>
					<td>'.$post['szCity'].'</td>
				</tr>
			';
			$count++;
			}
		}
	}
	if($action=='POSTCODE_CONTENT_NEXT_PAGE')
	{	
		$searchAry = array();
		$searchAry['szOriginCountry'] = $_POST['country'];
		$searchAry['szOriginCity'] = $_POST['city'];
		$searchAry['szOriginPostCode'] = $_POST['postcode'];
		$page = (int)$_POST['page'];
		$from = ($page-1)*__CONTENT_PER_PAGE_POSTCODE__;
		$postcode	=	$kConfig->selectPostCodeDetails($searchAry,$from);
		$total = $kConfig->countPostCodeDetails($searchAry);
		
		if($postcode!=array())
		{	
			postcodeDet($postcode,$total);
		}
	}
	if($action	==	'SHOW_EACH_POST_CODE')
	{
		$idPostcode				=	(int)sanitize_all_html_input($_POST['id']);
		if($idPostcode>0)
		{
			$showCurrentPostcode	=	$kConfig->selectEachPostcode($idPostcode);
			if($showCurrentPostcode!=array())
			{
				showPostcodeDetails($showCurrentPostcode);
			}
		}
	}
	if($action == 'SHOW_DEFAULT_POST_CODE')
	{	
		$t_base="management/postcode/";
		showPostcode($t_base);
	}
	if($action == 'UPDATE_TABLE_POSTCODE')
	{
		$showCurrentPostcode	=	$kConfig->updateTablePostcode($_POST['postcodeArr']);
		if($showCurrentPostcode==false)
		{	echo "ERROR|||||";
			if(!empty($kConfig->arErrorMessages))
			{
			?>
			<div id="regError" class="errorBox" style="display:block;">
			<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kConfig->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
			<?
			}
		}
		else
		{
		echo "SUCCESS|||||";
		}
	}
	if($action == 'ADD_TABLE_POSTCODE')
	{
		$showCurrentPostcode	=	$kConfig->addTablePostcode($_POST['postcodeArr']);
		if($showCurrentPostcode==false)
		{	echo "ERROR|||||";
			if(!empty($kConfig->arErrorMessages))
			{
			?>
			<div id="regError" class="errorBox" style="display:block;">
			<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kConfig->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
			<?
			}
		}
		else
		{
		echo "SUCCESS|||||";
		}
	}
	if($action == 'DELETE_POST_CODE')
	{
		$id = sanitize_all_html_input($_POST['id']);
		$szCity = sanitize_all_html_input($_POST['szCity']);
		echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/remove_postcode').'</b></h5>
							<p>'.t($t_base.'title/are_you_sure').'</p>
							<br />
							<p align="center">
							<a class="button1" onclick="removepostcodeCity(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
							</p>
							</div>
					</div>';
	
	}
	if($action == 'SHOW_ME_POST_CODE')
	{
	
	showPostcodeDetails();
	}
	if($action =='DELETE_POST_CODE_CONFIRM')
	{	
		$id = sanitize_all_html_input($_POST['id']);
		if($kConfig->deletePostcode($id))
		{
			echo 	'SUCCESS|||||';
		}
	}
	if($action == 'CLEAR_CITY_NOT_FOUND')
	{
		echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/clear_table').'</b></h5>
							<p>'.t($t_base.'title/are_u_sure_to_clear_the_table').'</p>
							<br />
							<div align="center">
								<a class="button1" onclick="removecity()"><span>'.t($t_base.'fields/yes').'</span></a>
								<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
							</div>
						</div>
					</div>';
	
	}
	if($action =='CONFIRM_CLEAR_CITY_NOT_FOUND')
	{
		$kWHSSearch = new cWHSSearch;
		if($countryDetails=$kWHSSearch->deleteCityNotFoundDetails())
		{
			echo 'SUCCESS|||||';
		}
		
	}
	if($action =='PRIORITY')
	{
		$cityPostCodeAry = $kAdmin->showPostcodePriority($idAdmin);
		if(!empty($cityPostCodeAry))
		{
			$final_ary = findcityPriroity($cityPostCodeAry);
			showPriorityPostcode($final_ary);
		}
	}
	if($action == 'SORT_PRIORITY')
	{
		$sortBy = trim(sanitize_all_html_input($_POST['by']));
		$sortOrder = trim(sanitize_all_html_input($_POST['mode']));
		$cityPostCodeAry = $kAdmin->showPostcodePriority($idAdmin,$sortBy,$sortOrder);
		if(!empty($cityPostCodeAry))
		{
			$final_ary = findcityPriroity($cityPostCodeAry);
			showPriorityPostcode($final_ary);
		}
	}
	if($action == 'DELETE_PRIORITY')
	{	
		$city  = sanitize_all_html_input($_POST['city']);
		$country = 	sanitize_all_html_input($_POST['country']);
		$id = (int)$_POST['value'];
		$divID = sanitize_all_html_input($_POST['div_id']);
		
		echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/delete_priority').'</b></h5>
							<p>'.t($t_base.'title/are_u_sure_to_delete_priority').' '.$city.' '.t($t_base.'title/city_in').' '.$country.' '.t($t_base.'title/country').' ? </p>
							<br />
							<div align="center">
								<a class="button1" onclick="removePriority(\''.$id.'\',\''.$divID.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
								<a class="button1" onclick="cancelDeletePriority()"><span>'.t($t_base.'fields/no').'</span></a>
							</div>
						</div>
					</div>';
	}
	if($action == 'DELETE_PRIORITY_CONFIRM')
	{
		$id = (int)$_POST['id'];	
		if($kAdmin->updatePriority($idAdmin,$id))
		{
			echo "SUCCESS";
		}
		else
		{
			echo "ERROR";
		}
	}
}
?>