<?php
/**
 * View Invoice
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$kBooking = new cBooking();
$idBooking=(int)$_REQUEST['idBooking']; 
if((int)$idBooking>0)
{  
    $postSearchAry = array();
    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
  
    $kLabel = new cLabels();
    $kLabel->createLabelFromTNTApi($postSearchAry);
      
    if(!empty($kLabel->szLabelPdfFileName))
    {
        $szPdfFilePath = __APP_PATH_TNT_LABEL__."/".$kLabel->szLabelPdfFileName; 
        download_booking_pdf_file($szPdfFilePath);
        die;
    }
}
else
{
    header('Location:'.__BASE_URL__.'/');
    exit(); 
}

?>