<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
$kHaulagePricing = new cHaulagePricing();
$idHaulageModel = (int)$_REQUEST['idHaulagePricingModel'];
$verticesAry = array();
$verticesAry = $kHaulagePricing->getAllVerticesOfZone($idHaulageModel);

if(!empty($verticesAry))
{
	$szLatitude=0;
	$szLongitude=0;
	$ctr=0;
    foreach($verticesAry as $verticesArys)
    {
    		++$ctr;
        	$szLatitude=$szLatitude+$verticesArys['szLatitude'];
        	$szLongitude=$szLongitude+$verticesArys['szLongitude'];		
    }
    
    if($ctr>0)
    {
    	$midLat=($szLatitude/$ctr);
    	$midLong=($szLongitude/$ctr);
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Transporteca|| Google Map Services</title>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
	  var Polygon;
	  
      function initialize() {
        var mapDiv = document.getElementById('map-canvas');
       
        var map = new google.maps.Map(mapDiv, {
          center: new google.maps.LatLng('<?=$midLat?>','<?=$midLong?>'),
          zoom: 4,
          disableDefaultUI: false,
          mapTypeControl:false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          streetViewControl: false
        });
         //var markerBounds = new GLatLngBounds();
         var paths = [
         <?
        	if(!empty($verticesAry))
        	{
        		foreach($verticesAry as $verticesArys)
        		{
        			?>
        			new google.maps.LatLng('<?=$verticesArys['szLatitude']?>','<?=$verticesArys['szLongitude']?>'),
        			<?
        		}
        	}
        ?>
         ];
          var bounds = new google.maps.LatLngBounds();
         <?
        	if(!empty($verticesAry))
        	{
        		foreach($verticesAry as $verticesArys)
        		{
        			?>
        			var randomPoint = new google.maps.LatLng('<?=$verticesArys['szLatitude']?>','<?=$verticesArys['szLongitude']?>');
        			bounds.extend(randomPoint);
        			<?
        		}
        	}
        ?>
      	map.fitBounds(bounds);
      	
        var shape = new google.maps.Polygon({
          paths: paths,
          strokeColor: '#0101DF',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#0101DF',
          fillOpacity: 0.35
        });
      	    
        shape.setMap(map);
       // fitPolygon();
      }
      function fitPolygon()
	  {
		    bounds = Polygon.getBounds();
		    alert(bounds);
		    map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
	  }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
  <body style="font-family: Arial; border: 0 none;margin:0;padding:0;">
    <div id="map-canvas" style="width: 276px; height: 228px"></div>
  </body>
</html>