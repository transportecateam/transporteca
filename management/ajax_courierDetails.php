<?php
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kCourierServices = new cCourierServices();
$mode=sanitize_all_html_input(trim($_REQUEST['mode']));
$t_base="management/providerProduct/";
$kCourierServices=new cCourierServices();
if($mode=='CHECKING_COURIER_PROVIDER_PRODUCT')
{
	if($kCourierServices->validateProviderProduct($_POST['addProviderAry']))
	{
	
	}
	else
	{
		if(!empty($kCourierServices->arErrorMessages))
	    {
	        if($kCourierServices->arErrorMessages['szName']!='' && $kCourierServices->arErrorMessages['fPrice']!='')
	        {
	        	echo "Both";
	        }
	        else if($kCourierServices->arErrorMessages['szName']!='')
	        {
	        	echo "Name";
	        }else if($kCourierServices->arErrorMessages['fPrice']!='')
	        {
	        	echo "Price";
	        }
	        
		}
	}
}
else if($mode=='ADD_EDIT_COURIER_PROVIDER_PRODUCT')
{
    if($kCourierServices->addEditCourierProvider($_POST['addProviderAry']))
    {
        $_POST['addProviderAry']=array();
        unset($_POST['addProviderAry']);
        echo "SUCCESS||||";
        echo addDisplayCouierProviderName($kCourierServices,$kCourierServices->idProvider);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_courier_provider_form($kCourierServices);
        die();
    } 
}
else if($mode=='SELECT_PROVIDER_DATA_FROM_LIST')
{
    $idProvider=(int)$_POST['id'];
    echo addDisplayCouierProviderName($kCourierServices,$idProvider)."||||";
    echo addDisplayCouierProviderProductName($kCourierServices,$idProvider)."||||";
    echo courierProductGroupList($kCourierServices,$idProvider)."||||";
    echo courierCargoLimitationList($kCourierServices)."||||";
    echo display_courier_product_terms_instruction_form($kCourierServices);
}
else if($mode=='EDIT_PROVIDER_DATA_FROM')
{
    $idProvider=(int)$_POST['id'];
    $providerProductArr=$kCourierServices->getCourierProviderList($idProvider);
    $_POST['addProviderAry']=$providerProductArr[0];
    display_courier_provider_form($kCourierServices); 
}
else if($mode=='DELETE_PROVIDER_DATA')
{
    $idProvider=(int)$_POST['id'];
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup">
            <h3><?=t($t_base.'title/delete_courier_service_provider');?></h3>
            <p><?=t($t_base.'messages/delete_msg_provider');?>?</p>
            <br/>
            <p align="center">
                <a href="javascript:void(0)" onclick="delete_courier_provider('<?php echo $idProvider; ?>')" class="button1" ><span><?=t($t_base.'fields/delete');?></span></a> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
            </p>
	</div>
    </div>
    <?php
}
else if($mode=='CONFIRM_DELETE_PROVIDER_DATA')
{
	$idProvider=(int)$_POST['id'];
	if((int)$idProvider>0)
	{
		if($kCourierServices->deleteCourierProvider($idProvider))
		{
			
		}
		
	}
	$providerProductArr=$kCourierServices->getCourierProviderList();
	$idCourierProvider=$providerProductArr[0]['id'];
	echo addDisplayCouierProviderName($kCourierServices,$idCourierProvider)."||||";
	echo addDisplayCouierProviderProductName($kCourierServices,$idCourierProvider);
}
else if($mode=='CHECKING_COURIER_PROVIDER_PRODUCT_DATA')
{
    if($kCourierServices->validateProviderProductData($_POST['addProviderProductAry']))
    {
        echo "SUCCESS||||";
        echo display_courier_provider_product_form($kCourierServices);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_courier_provider_product_form($kCourierServices);
        die;
    }
}
else if($mode=='ADD_EDIT_COURIER_PROVIDER_PRODUCT_DATA')
{
	if($kCourierServices->addEditProviderProductData($_POST['addProviderProductAry']))
	{
		$idCourierProvider=$_POST['addProviderProductAry']['idCourierProvider'];
		$_POST['addProviderProductAry']=array();
		unset($_POST['addProviderProductAry']);
		echo "SUCCESS||||";
		echo addDisplayCouierProviderName($kCourierServices,$idCourierProvider)."||||";
		echo addDisplayCouierProviderProductName($kCourierServices,$idCourierProvider);
		die;
	}
	else
	{
		echo "ERROR||||";
		echo display_courier_provider_product_form($kCourierServices);
		die;
	}
}
else if($mode=='SELECT_COURIER_PRODUCT_DATA')
{
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $id=(int)$_POST['id'];
    $_POST['addCourierCargoLimitAry']['idCourierProviderProduct']=$id;
    
    if((int)$id>0)
    { 
        $productTermsInstructionArr=$kCourierServices->getProductTermsInstructionData($id);
        if(count($productTermsInstructionArr)>0)
        {
            if(count($productTermsInstructionArr)==2)
            {
                $_POST['addEditInsTermsArr']=$productTermsInstructionArr[0];
            }
            else
            {
                $_POST['addEditInsTermsArr']=$productTermsInstructionArr[0];
            }
        }
    }	
    $counter=count($providerProductListArr);
   // if((int)$idGroup>0)
    //{
        $idCourierProviderProduct=$id;
        $_POST['addEditInsTermsArr']['idCourierProviderProduct']=$idCourierProviderProduct;
    //} 
    
    echo addDisplayCouierProviderProductName($kCourierServices,$idCourierProvider,$id)."||||";
    //echo courierCargoLimitationList($kCourierServices,$id);
    //echo "||||";
    echo display_courier_product_terms_instruction_form($kCourierServices);
}
else if($mode=='EDIT_PROVIDER_PRODUCT_DATA_FROM')
{
	$idCourierProvider=(int)$_POST['idCourierProvider'];
	$id=(int)$_POST['id'];
	
	$providerProductListArr=$kCourierServices->getCourierProviderProductList($idCourierProvider,$id);
	
	$_POST['addProviderProductAry']=$providerProductListArr[0];
	$_POST['addProviderProductAry']['szProductName']=$providerProductListArr[0]['szName'];
	echo display_courier_provider_product_form($kCourierServices,$idCourierProvider,$id);

}
else if($mode=='DELETE_PROVIDER_DATA_DATA')
{
    $idProviderProduct=(int)$_POST['id'];
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    ?>
    <div id="popup-bg"></div>
        <div id="popup-container" >
        <div class="popup">
            <h3><?=t($t_base.'title/delete_courier_product');?></h3>
            <p><?=t($t_base.'messages/delete_msg_provider_product');?>?</p>
            <br/>
            <p align="center">
                <a href="javascript:void(0)" onclick="confirm_delete_courier_provider_product('<?php echo $idProviderProduct; ?>','<?php echo $idCourierProvider;?>')" class="button1" ><span><?=t($t_base.'fields/delete');?></span></a> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
            </p>
        </div>
    </div>
    <?php
}
else if($mode=='CONFIRM_DELETE_PROVIDER_PRODUCT_DATA')
{
    $idProviderProduct=(int)$_POST['id'];
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $kCourierServices->deleteCourierProviderProduct($idProviderProduct);
    echo addDisplayCouierProviderProductName($kCourierServices,$idCourierProvider);
}
else if($mode=='CHECKING_COURIER_CARGO_LIMITATION_DATA')
{
    if($kCourierServices->validateCourierCargoLimitationData($_POST['addCourierCargoLimitAry']))
    {
        echo "SUCCESS||||";
        echo display_courier_cargo_limitation_addedit_form($kCourierServices);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_courier_cargo_limitation_addedit_form($kCourierServices);
        die;
    }
}
else if($mode=='ADD_EDIT_COURIER_CARGO_LIMIT_DATA')
{
	if($kCourierServices->addEditCargoLimitationData($_POST['addCourierCargoLimitAry']))
	{
		$idGroup=$_POST['addCourierCargoLimitAry']['idGroup'];
		$_POST['addCourierCargoLimitAry']=array();
		unset($_POST['addCourierCargoLimitAry']);
		echo "SUCCESS||||";
		echo courierCargoLimitationList($kCourierServices,$idGroup);
		die;
	}
	else
	{
		echo "ERROR||||";
		echo display_courier_cargo_limitation_addedit_form($kCourierServices);
		die();
	}
	
}
else if($mode=='SELECT_COURIER_CARGO_LIMITATION_DATA')
{
    $idCargoLimit=(int)$_POST['id'];
    $idGroup=(int)$_POST['idGroup'];

    echo courierCargoLimitationList($kCourierServices,$idGroup,$idCargoLimit);
}
else if($mode=='EDIT_CARGO_LIMITATION_DATA_FROM')
{
    $idCargoLimit=(int)$_POST['id'];
    $idGroup=(int)$_POST['idGroup'];

    $cargoLimitArr=$kCourierServices->getCourierCargoLimitationData($idGroup,$idCargoLimit);
    $_POST['addCourierCargoLimitAry']=$cargoLimitArr[0];
     
    if($_POST['addCourierCargoLimitAry']['idCargoLimitationType']==7) //Total volume for shipment 
    {
        $_POST['addCourierCargoLimitAry']['szLimit'] = $_POST['addCourierCargoLimitAry']['szLimit'];
    }
    else
    {
        $_POST['addCourierCargoLimitAry']['szLimit'] = round($_POST['addCourierCargoLimitAry']['szLimit']);
    } 
    display_courier_cargo_limitation_addedit_form($kCourierServices); 
}
else if($mode=='DELETE_COURIER_CARGO_LIMITATION_DATA')
{

	$idCargoLimit=(int)$_POST['id'];
	$idGroup=(int)$_POST['idGroup'];
	
	$kCourierServices->deleteCourierCargoLimitation($idCargoLimit);
	
	echo courierCargoLimitationList($kCourierServices,$idGroup);
}
else if($mode=='CHECKING_COURIER_PRODUCT_INS_TERM_DATA')
{
	if($kCourierServices->validateTermsInstructionData($_POST['addEditInsTermsArr']))
	{
		echo "SUCCESS||||";
		echo display_courier_product_terms_instruction_form($kCourierServices);
		die;
	}
	else
	{
		echo "ERROR||||";
		echo display_courier_product_terms_instruction_form($kCourierServices);
		die;
	}

}
else if($mode=='ADD_EDIT_COURIER_PRODUCT_INS_TERM_DATA')
{
	if($kCourierServices->addEditTermsInstructionData($_POST['addEditInsTermsArr']))
	{
		echo "SUCCESS||||";
		echo display_courier_product_terms_instruction_form($kCourierServices);
		die;
	}
	else
	{
		echo "ERROR||||";
		echo display_courier_product_terms_instruction_form($kCourierServices);
		die;
	}
}
else if($mode=='GET_INSTRUCTION_TERM_BY_LANGUAGE')
{
    $idLanguage=(int)$_POST['idLanguage'];
    $idCourierProviderProduct=(int)$_POST['idCourierProviderProduct'];

    $productTermsInstructionArr=$kCourierServices->getProductTermsInstructionData($idCourierProviderProduct,'',$idLanguage);

    if(count($productTermsInstructionArr)>0)
    {
        $_POST['addEditInsTermsArr']=$productTermsInstructionArr[0];
    }
    else
    {
        $_POST['addEditInsTermsArr']['idLanguage']=$idLanguage;
        $_POST['addEditInsTermsArr']['idCourierProviderProduct']=$idCourierProviderProduct;
    }
    echo display_courier_product_terms_instruction_form($kCourierServices);
}
else if($mode=='CHECKING_COURIER_MODE_TRANSPORT_DATA')
{ 
    if($kCourierServices->validateModeTransportData($_POST['addModeTransportAry']))
    {
        echo "SUCCESS||||";
        echo display_courier_country_region_addedit_form($kCourierServices);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_courier_country_region_addedit_form($kCourierServices);
        die;
    }
}
else if($mode=='ADD_EDIT_COURIER_MODE_TRANSPORT_DATA')
{ 
    $iDTDTrades = sanitize_all_html_input($_POST['addModeTransportAry']['iDTDTrades']); 
    if($kCourierServices->addEditFromToCountryForModeTransport($_POST['addModeTransportAry']))
    {
        unset($_POST['addModeTransportAry']);
        $_POST['addModeTransportAry']=array();	
        echo "SUCCESS||||";
        echo addDisplayCouierModeTransport($kCourierServices,$iDTDTrades);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_courier_country_region_addedit_form($kCourierServices);
        die;
    }
}
else if($mode=='DELETE_TRCK_ICON_DATA')
{
    $iDTDTrades = sanitize_all_html_input($_POST['iDTDFlag']);  
    $deleteTruckArr=trim($_POST['deleteIdArr']);
    $deleteTruckArr=explode(";",$deleteTruckArr);
    $kCourierServices->deleteTruckIconData($deleteTruckArr);
    addDisplayCouierModeTransport($kCourierServices,$iDTDTrades); 
}
else if($mode=='SORT_MODE_TRANSPORT')
{
    $iDTDTrades = sanitize_all_html_input($_POST['iDTDFlag']);  
    addDisplayCouierModeTransport($kCourierServices,$iDTDTrades);
}
else if($mode=='SELECT_COURIER_PRODUCT_GROUP_DATA')
{
    $kCourierServices=new cCourierServices();
    $idCourierProvider=(int)$_POST['idCourierProvider'];
	$id=(int)$_POST['id'];
       $_POST['groupArr']['idCourierProvider']=$idCourierProvider;
       //$_POST['groupArr']['id']=$id;
       echo courierProductGroupList($kCourierServices,$idCourierProvider,$id)."||||";
       echo courierCargoLimitationList($kCourierServices,$id);
}
else if($mode=='EDIT_COURIER_GROUP_DATA_FROM')
{
    $kCourierProvider=new cCourierServices();
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $id=(int)$_POST['id'];

    $courierProductGroup=$kCourierProvider->getCourierProductGroupByidProvider($idCourierProvider,$id);
   // print_r($courierProductGroup);
   $_POST['groupArr']['idCourierProvider']=$idCourierProvider;
   $_POST['groupArr']['id']=$id;
   $_POST['groupArr']['szName']=$courierProductGroup[0]['szName'];
    display_courier_product_group_addedit_form($kCourierProvider);
}
else if($mode=='ADD_EDIT_COURIER_PRODUCT_GROUP_DATA')
{
    $kCourierProvider=new cCourierServices();
    $idCourierProvider=(int)$_POST['groupArr']['idCourierProvider'];
   // print_r($_POST);
   //$id=(int)$_POST['groupArr']['id'];
    if($kCourierProvider->addUpdateProductGroupData($_POST['groupArr']))
    {
        $_POST['groupArr'] = array();
        echo "SUCCESS||||";
        echo courierProductGroupList($kCourierProvider,$idCourierProvider)."||||";
        echo courierCargoLimitationList($kCourierServices,$id);
    }
    else
    {
        echo "ERROR||||";
        display_courier_product_group_addedit_form($kCourierProvider);
    }
}
else if($mode=='DELETE_COURIER_PRODUCT_GROUP_DATA')
{
    $kCourierProvider=new cCourierServices();
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $id=(int)$_POST['id'];
    $kCourierProvider->deleteCourierProductGroup($id);
    courierProductGroupList($kCourierProvider,$idCourierProvider);
}
else if($mode=='EDIT_EXCLUDED_TRADES')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id']));
    $kCourierServices=new cCourierServices();
    if($idExcludedTrades>0)
    {
        $courierExcludedTradesAry = array(); 
        $courierExcludedTradesAry = $kCourierServices->getAllExcludedTrades($idExcludedTrades);
        if(!empty($courierExcludedTradesAry))
        {
            echo "SUCCESS_EDIT||||";
            echo display_courier_excluded_trades_add_edit_form($kCourierServices,$courierExcludedTradesAry[0]);
            die;
        }
    } 
}
else if($mode=='CANCEL_EXCLUDED_TRADES')
{
    echo "SUCCESS_LIST||||"; 
    $_POST['addCourierExcludedTradesAry'] = array();
    echo courierExcludedTradesList();
    die;
} 
else if($mode=='DELETE_EXCLUDED_TRADES')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id'])); 
    echo "SUCCESS_POPUP||||";
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup">
            <h3><?php echo t($t_base.'title/delete_excluded_trades'); ?></h3>
            <p><?php echo t($t_base.'messages/delete_msg_excluded_courier'); ?>?</p>
            <br/>
            <p align="center">
                <a href="javascript:void(0)" onclick="update_courier_excluded_trades('<?php echo $idExcludedTrades; ?>','DELETE_EXCLUDED_TRADES_CONFIRM');" class="button1" ><span><?=t($t_base.'fields/delete');?></span></a> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
            </p>
        </div>
    </div>
    <?php
}
else if($mode=='DELETE_MULTIPLE_EXCLUDED_TRADES_CONFIRM')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id'])); 
    
    $kCourierServices=new cCourierServices();
    if($kCourierServices->deleteExludedTrades($idExcludedTrades))
    {
        echo "SUCCESS_LIST||||"; 
        $_POST['addCourierExcludedTradesAry'] = array();
        echo courierExcludedTradesList();
        die;
    }
    else
    {
        $szTitle = "System Error";
        $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        echo "SUCCESS_POPUP||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='DELETE_EXCLUDED_TRADES_CONFIRM')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id'])); 
    $kCourierServices=new cCourierServices();
    if($kCourierServices->deleteExludedTrades($idExcludedTrades))
    {
        echo "SUCCESS_LIST||||"; 
        $_POST['addCourierExcludedTradesAry'] = array();
        echo courierExcludedTradesList();
        die;
    }
    else
    {
        $szTitle = "System Error";
        $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        echo "SUCCESS_POPUP||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='LOAD_FORWARDER_LIST')
{
    $idCourierProvider = sanitize_all_html_input(trim($_POST['excluded_trade_id']));
    $data = array();
    $data['idCourierProvider'] = $idCourierProvider;
    $forwarderListAry = $kCourierServices->getAllCourierAgreement($data,true);
    echo "SUCCESS_DROP_DOWN||||";
    ?>
    <select id="idForwarder" name="addCourierExcludedTradesAry[idForwarder]" onchange="checkCourierExludedTradesData();">
        <option value=''>Select</option>
        <?php 
            if(!empty($forwarderListAry))
            {
                foreach($forwarderListAry as $forwarderListArys)
                {
                    ?>
                    <option  value='<?php echo $forwarderListArys['idForwarder']?>' <?php if($idForwarder==$forwarderListArys['id']){?> selected <?php }?>><?php echo $forwarderListArys['szForwarderDisplayName']." (".$forwarderListArys['szForwarderAlias'].")"; ?></option>
                    <?php
                }
            }
        ?>
    </select>
    <?php
}
else if(!empty($_POST['addCourierExcludedTradesAry']) && $mode=='CHECK_DUPLICATE_EXCLUDED_TRADES')
{ 
    $excludedTradeSearch = array();
    $excludedTradeSearch['idCourierProvider'] = $_POST['addCourierExcludedTradesAry']['idCourierProvider'];
    $excludedTradeSearch['idForwarder'] = $_POST['addCourierExcludedTradesAry']['idForwarder'];
    $excludedTradeSearch['idOriginCountry'] = $_POST['addCourierExcludedTradesAry']['idOriginCountry'];
    $excludedTradeSearch['idDestinationCountry'] = $_POST['addCourierExcludedTradesAry']['idDestinationCountry'];
    $excludedTradeSearch['iCustomerType'] = $_POST['addCourierExcludedTradesAry']['iCustomerType'];
    
    $idExludedTrades = false;
    if($_POST['addCourierExcludedTradesAry']['idDestinationCountry']>0)
    {
        $idExludedTrades = $_POST['addCourierExcludedTradesAry']['idExludedTrades'];
    }
     
    if($kCourierServices->isTradeAddedToExcludedList($excludedTradeSearch,$idExludedTrades))
    {
        echo "SUCCESS||||";
    }
    else
    {
        echo "ERROR||||";
    } 
}
else if(!empty($_POST['addCourierExcludedTradesAry']))
{ 
    if($kCourierServices->addEditCourierExludedTrades($_POST['addCourierExcludedTradesAry']))
    {
        echo "SUCCESS||||"; 
        $_POST['addCourierExcludedTradesAry'] = array();
        echo courierExcludedTradesList();
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_courier_excluded_trades_add_edit_form($kCourierServices);
        die;
    }
}
else if(!empty($_POST['addLCLExcludedTradesAry']) && $mode=='CHECK_DUPLICATE_EXCLUDED_TRADES_LCL')
{ 
    $excludedTradeSearch = array();
    
    $excludedTradeSearch['idForwarder'] = $_POST['addLCLExcludedTradesAry']['idForwarder'];
    $excludedTradeSearch['idOriginCountry'] = $_POST['addLCLExcludedTradesAry']['idOriginCountry'];
    $excludedTradeSearch['idDestinationCountry'] = $_POST['addLCLExcludedTradesAry']['idDestinationCountry'];
    $excludedTradeSearch['iCustomerType'] = $_POST['addLCLExcludedTradesAry']['iCustomerType'];
    
    $idExludedTrades = false;
    if($_POST['addLCLExcludedTradesAry']['idDestinationCountry']>0)
    {
        $idExludedTrades = $_POST['addLCLExcludedTradesAry']['idExludedTrades'];
    }
     
    if($kCourierServices->isTradeAddedToExcludedLCLList($excludedTradeSearch,$idExludedTrades))
    {
        echo "SUCCESS||||";
    }
    else
    {
        echo "ERROR||||";
    } 
}
else if(!empty($_POST['addLCLExcludedTradesAry']))
{ 
    if($kCourierServices->addEditLCLExludedTrades($_POST['addLCLExcludedTradesAry']))
    {
       // print_r($kCourierServices);
        echo "SUCCESS||||"; 
        $_POST['addLCLExcludedTradesAry'] = array();
        echo LCLExcludedTradesList();
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_lcl_excluded_trades_add_edit_form($kCourierServices);
        die;
    }
}
else if($mode=='EDIT_EXCLUDED_LCL_TRADES')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id']));
    $kCourierServices=new cCourierServices();
    if($idExcludedTrades>0)
    {
        $courierExcludedTradesAry = array(); 
        $courierExcludedTradesAry = $kCourierServices->getAllExcludedLCLTrades($idExcludedTrades);
        if(!empty($courierExcludedTradesAry))
        {
            echo "SUCCESS_EDIT||||";
            echo display_lcl_excluded_trades_add_edit_form($kCourierServices,$courierExcludedTradesAry[0]);
            die;
        }
    } 
}
else if($mode=='CANCEL_EXCLUDED_TRADES_LCL')
{
    echo "SUCCESS_LIST||||"; 
    $_POST['addCourierExcludedTradesAry'] = array();
    echo LCLExcludedTradesList();
    die;
}
else if($mode=='DELETE_EXCLUDED_LCL_TRADES')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id'])); 
    echo "SUCCESS_POPUP||||";
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup">
            <h3><?php echo t($t_base.'title/delete_excluded_trades'); ?></h3>
            <p><?php echo t($t_base.'messages/delete_msg_excluded_lcl'); ?>?</p>
            <br/>
            <p align="center">
                <a href="javascript:void(0)" onclick="update_lcl_excluded_trades('<?php echo $idExcludedTrades; ?>','DELETE_EXCLUDED_LCL_TRADES_CONFIRM');" class="button1" ><span><?=t($t_base.'fields/delete');?></span></a> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
            </p>
        </div>
    </div>
    <?php
}
else if($mode=='DELETE_MULTIPLE_EXCLUDED_LCL_TRADES_CONFIRM')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id'])); 
    
    $kCourierServices=new cCourierServices();
    if($kCourierServices->deleteExludedLCLTrades($idExcludedTrades))
    {
        echo "SUCCESS_LIST||||"; 
        $_POST['addCourierExcludedTradesAry'] = array();
        echo LCLExcludedTradesList();
        die;
    }
    else
    {
        $szTitle = "System Error";
        $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        echo "SUCCESS_POPUP||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='DELETE_EXCLUDED_LCL_TRADES_CONFIRM')
{
    $idExcludedTrades = sanitize_all_html_input(trim($_POST['excluded_trade_id'])); 
    $kCourierServices=new cCourierServices();
    if($kCourierServices->deleteExludedLCLTrades($idExcludedTrades))
    {
        echo "SUCCESS_LIST||||"; 
        $_POST['addCourierExcludedTradesAry'] = array();
        echo LCLExcludedTradesList();
        die;
    }
    else
    {
        $szTitle = "System Error";
        $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        echo "SUCCESS_POPUP||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='SORT_CUSTOMER_CLEARANCE')
{
    $iDTDTrades = sanitize_all_html_input($_POST['iDTDFlag']);  
    addDisplayCustomerClearance($kCourierServices,$iDTDTrades);
}
else if($mode=='DELETE_CUSTOMER_CLEARANCE_DATA')
{
    $iDTDTrades = sanitize_all_html_input($_POST['iDTDFlag']);  
    $deleteTruckArr=trim($_POST['deleteIdArr']);
    $deleteTruckArr=explode(";",$deleteTruckArr);
    $kCourierServices->deleteCustomerClearanceData($deleteTruckArr);
    addDisplayCustomerClearance($kCourierServices,$iDTDTrades); 
}
else if($mode=='CHECKING_CC_DATA')
{ 
    if($kCourierServices->validateCCData($_POST['addModeTransportAry']))
    {
        echo "SUCCESS||||";
        echo display_customer_clearance_addedit_form($kCourierServices);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_customer_clearance_addedit_form($kCourierServices);
        die;
    }
}
else if($mode=='ADD_EDIT_CUSTOMER_CLEARANCE_DATA')
{ 
    if($kCourierServices->addEditFromToCountryForCustomerClearance($_POST['addModeTransportAry']))
    {
        unset($_POST['addModeTransportAry']);
        $_POST['addModeTransportAry']=array();	
        echo "SUCCESS||||";
        echo addDisplayCustomerClearance($kCourierServices);
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_customer_clearance_addedit_form($kCourierServices);
        die;
    }
}
?>