<?php 
/**
  *  Explain page List All Explain Pages
  * 
  */
define('PAGE_PERMISSION','__CONTENT__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
$szMetaTitle = "Transporteca | Section header";
require_once (__APP_PATH__ ."/layout/admin_header.php");
validateManagement();
$t_base="management/explaindata/";
$kExplain = new cExplain();
 
$iType = sanitize_all_html_input($_REQUEST['type']);
$iSessionLanguage = false;
if($_SESSION['session_admin_language']>0)
{
	$iSessionLanguage = $_SESSION['session_admin_language'] ;
}

$explainLinks = $kExplain->selectPageDetails(false,$iType,$iSessionLanguage);
$maxOrder = $kExplain->getMaxOrderExplainData($iType,$iSessionLanguage);

$explainPageFlag=checkManagementPermission($kAdmin,"__MANAGE_EXPLAIN_PAGES__",2);
?>
<div id="hsbody-2">
    <?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?>  
    <div class="hsbody-2-right">
        <?php if($explainPageFlag){?>
        <div id="content">
            <?php echo viewExplainPage($t_base,$explainLinks,$maxOrder,$iType);	?>
        </div> 
        <?php }?>
    </div>
</div>
	
<?php include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); ?>	