<?php
define('PAGE_PERMISSION','__SYSTEM_LOCATION_DESCRIPTION__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Variables - Location Description";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/system/locationDescription/";
validateManagement();

ob_end_clean();
header("Location:".__BASE_URL__);
die;
$szLanguage = sanitize_all_html_input($_REQUEST['language']);

$iLanguage = getLanguageId($szLanguage);
//echo $iLanguage."iLanguage";
$kConfig =new cConfig();
$langArr=$kConfig->getLanguageDetails();
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
		<?=t($t_base.'fields/description')?>
		<br /><br />
		<div class="row-container" style="text-align:right;">
			<div style="width:90%;"><?=t($t_base.'fields/language');?>: </div>
			<div class="wd-15">
				<select name="language" id="language" onchange="redirect_url('<?php echo __MANAGEMENT_LOCATION_DESCRIPTION_URL__?>'+this.value+'/')">
                                        <?php
                                        if(!empty($langArr))
                                        {
                                            foreach($langArr as $langArrs)
                                            {
                                              ?>
                                              <option value = '<?php echo strtolower($langArrs['szName'])?>' <?php if($szLanguage ==strtolower($langArrs['szName'])){ echo "selected='selected'"; } ?>><?php echo ucfirst(strtolower($langArrs['szName']));?></option>
                                              <?php  
                                            }
                                        }
					?>
					<!--<option value = '<?php echo __LANGUAGE_TEXT_DANISH__?>' <?php if($szLanguage ==__LANGUAGE_TEXT_DANISH__){ echo "selected='selected'"; } ?> ><?php echo ucfirst(__LANGUAGE_TEXT_DANISH__);?></option>-->	
				</select>				
			</div>
		</div>
		<table cellpadding="0"  cellspacing="0" border="0" class="format-4" width="100%" style="margin-bottom:20px;" id="booking_table">
			<tr id = "booking_table_location">
				<th width="41%"  valign="top"><strong><?=t($t_base.'fields/country')?></strong></th>
				<th width="27%"  valign="top"><strong><?=t($t_base.'fields/description_when_loc_required')?></strong></th>
				<th width="27%"  valign="top"><strong><?=t($t_base.'fields/description_when_loc_not_required')?></strong></th>
				<th width="5%"></th>
			</tr>
			<?php	
				showLocationDescription($t_base,$iLanguage);
			?>
			</table>
			<div id="showAvailableSelections">
			<?php
				showAvailableSelection($t_base,$iLanguage);
				
			?>
			</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	