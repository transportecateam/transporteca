<?php
/**
 * admin index page
 */

 ob_start();
session_start();
$szMetaTitle="Transporteca | Admin";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );

$t_base = "management/home/";
if(!empty($_POST['loginArr']))
{
    if($kAdmin->adminLogin($_POST['loginArr']))
    {
        $id = $_SESSION['admin_id'];
        $redirect_url=__MANANAGEMENT_DASHBOARD_URL__;
        header("Location:".$redirect_url);
        exit(); 
    }
}

if(__ENVIRONMENT__=='ANILDEV')
{
    //$_SESSION['admin_id'] = 1; 
}
if((int)$_SESSION['admin_id']>0)
{
    $details=$kAdmin->getAdminDetails($_SESSION['admin_id']);
    $redirect_url=__MANANAGEMENT_DASHBOARD_URL__;
    header("Location:".$redirect_url);
    exit(); 
}

?>
<div id='ajaxLogin' style="display: none;"></div>
<div id="hsbody" style="padding-bottom: 100px;">
<form id="admin_login_form"  method="post">
	<div align="center">
		<br/>
		<br/>
		<h3><?=t($t_base.'title/welcome_to_transporteca')?> ! </h3><br/>
		<div align="center" style="width:300px;border: 1px solid #BFBFBF;padding:10px 10px 10px 10px;">		
		<?php
			if(!empty($kAdmin->arErrorMessages)){
			?>
			<div align="left" id="regError" class="errorBox ">
			<div class="header"><?=t($t_base.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
				foreach($kAdmin->arErrorMessages as $key=>$values)
				{
				?><li><?=$values?></li>
				<?php	
				}
			?>
			</ul>
			</div>
			</div>
			<?php }	?>
			<br/>
			<br/>
			<div class="oh">
                            <p class="fl-30 metrics-label" style="text-align:left;"><?=t($t_base.'fields/signin_email')?></p>
                            <p class="fl-70 metrics-text"><input type="text" id="szEmail" name="loginArr[szEmail]" value="<?=$_POST['loginArr']['szEmail']?>" tabindex="1"/></p>
			</div>
			<div class="oh">
                            <p class="fl-30 metrics-label" style="text-align:left;"><?=t($t_base.'fields/password')?></p>
                            <p class="fl-70 metrics-text"><input type="password" id="szPassword" name="loginArr[szPassword]" tabindex="2" onkeyup="on_enter_key_press(event,'admin_login_form');"/></p>
			</div>
			<p align="right"><a href="javascript:void(0)" onclick="open_forgot_passward_popup('ajaxLogin')" class="f-size-12"><?=t($t_base.'links/forgot_pass')?></a></p>
			<br style="line-height: 16px;" />
			<p align="center">
				<a href="javascript:void(0)" class="button1" onclick="$('#admin_login_form').submit()" tabindex="7"><span><?=t($t_base.'fields/sign_in')?></span></a>
			</p><br/>
			
			<br/>
			</div>			
		</div>	
		<input type="submit" style="visibility: hidden;" />
</form>	
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>