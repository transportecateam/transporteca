<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$t_base="management/forwarderpayment/";
validateManagement();
$id=sanitize_all_html_input($_REQUEST['id']);
$idBatchNumber=sanitize_all_html_input($_REQUEST['batchno']);
$idForwarder=sanitize_all_html_input($_REQUEST['idForwarder']);
$mode=sanitize_all_html_input($_REQUEST['mode']);
 
if($mode=='confirm')
{
    $kFarwarder= new cForwarder();
    $kForwarder= new cForwarder();
    $kBooking = new cBooking();
    $kWarehouse = new cWHSSearch();
    $kFarwarder->load($idForwarder);
	
    if($kAdmin->updateForwarderPayment($id))
    { 
        $uploadServiceStr='';
        $totalPaidAmountArr=$kAdmin->totalAmountConfirm($idBatchNumber);
        $totalPaidAmountUploadServiceArr=$kAdmin->totalAmountConfirmUploadService($idBatchNumber);
        $totalPaidAmountCancelBookingArr=$kAdmin->totalAmountConfirmCancelBooking($idBatchNumber);
        
        $CreateLabelBatch = $idBatchNumber + 1;
        $szInvoice_formatted = $kAdmin->getFormattedString($CreateLabelBatch,6);
        $customer_code = $szInvoice_formatted ;
        
        $totalCourierLabelArr=$kAdmin->totalAmountCourierLabelFee($idBatchNumber); 
        $totalHandlingFeeArr=$kAdmin->totalAmountCourierLabelFee($idBatchNumber,12); 
             
        
        $fPaidToForwarder=0;
        $referralfee=0;
        $totalCurrencyValue=0;
        if(!empty($totalPaidAmountCancelBookingArr))
        {
            $totalCurrencyValue=$totalPaidAmountArr[0]['totalCurrencyValue']-$totalPaidAmountCancelBookingArr[0]['totalCurrencyValue'];
            $referralfee=$totalPaidAmountArr[0]['referralfee']-$totalPaidAmountCancelBookingArr[0]['referralfee'];
            $fPaidToForwarder=$totalPaidAmountArr[0]['fPaidToForwarder']-$totalPaidAmountCancelBookingArr[0]['fPaidToForwarder'];
        }
        else
        {
            $totalCurrencyValue=$totalPaidAmountArr[0]['totalCurrencyValue'];
            $referralfee=$totalPaidAmountArr[0]['referralfee'];
            $fPaidToForwarder=$totalPaidAmountArr[0]['fPaidToForwarder'];
        }
        if($totalCreateLabelAmount>0)
        {
            $fPaidToForwarder = $fPaidToForwarder - $totalCreateLabelAmount;
        }
        if($totalHandlingFeeAmount>0)
        {
            $fPaidToForwarder = $fPaidToForwarder - $totalHandlingFeeAmount;
        } 
        if((float)$totalUploadServiceAmount>0)
        {
            $fPaidToForwarder=$fPaidToForwarder-$totalUploadServiceAmount;
        }
        
        if($totalPaidAmountArr[0]['szForwarderCurrency']=='USD')
        {
            $fExchangeRate = 1 ;
        }
        else 
        {
            $fExchangeRate = 1 ;
        }
        
        $addWorkingCapitalAry = array();
        $addWorkingCapitalAry['fPriceUSD'] = $fPaidToForwarder * $fExchangeRate;
        $addWorkingCapitalAry['szAmountType'] = 'Automatic Transfer' ;
        $addWorkingCapitalAry['iDebitCredit'] = 2;
        $addWorkingCapitalAry['szReference'] = $idBatchNumber;
                
        $kDashBoardAdmin = new cDashboardAdmin();
        $kDashBoardAdmin->addGraphWorkingCapitalCurrent($addWorkingCapitalAry); 
        ?>
        <script type="text/javascript">
            $(location).attr('href','<?=__MANAGEMENT_FORWARDER_TRANSFER_URL__?>');
        </script>
        <?php
        exit();
    }
}
else
{
    $kAdmin = new cAdmin();
    $billingTransferDetail = array();
    $billingTransferDetail = $kAdmin->showForwarderTransferPayment($id);
     
    if(!empty($billingTransferDetail['dtPaymentScheduled']) && $billingTransferDetail['dtPaymentScheduled']!='0000-00-00 00:00:00')
    {
        $dtTransferDueDate = date('Y-m-d',strtotime($billingTransferDetail['dtPaymentScheduled']));
    }
    else
    {
        $dtTransferDueDate = date('Y-m-d',strtotime($billingTransferDetail['dtCreatedOn']));
    }
    
    /*
    * If Transer date is less then today then we consider todays date as Transfer date.
    */
    $dtToday = date('Y-m-d');
    if(strtotime($dtTransferDueDate)<strtotime($dtToday))
    {
        $dtTransferDueDate = date('d/m/Y');
    } 
    else
    {
        $dtTransferDueDate = date('d/m/Y',strtotime($dtTransferDueDate));
    }
    $szHeadingText = "Have you scheduled the transfer on ".$dtTransferDueDate." to ".sanitize_all_html_input($_REQUEST['forwardername']);
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup">
            <h5><?=t($t_base.'fields/confirm');?></h5>
            <p><?php echo $szHeadingText; ?>?</p>
            <br/>
            <p align="center">
                <a href="javascript:void(0)" class="button2" onclick="cancelEditMAnageVar(0);" id="cancel_payment_transfer"><span><?=t($t_base.'fields/no');?></span></a>
                <a href="javascript:void(0)" class="button1" onclick="confirm_forwarder_payment_transfer('<?=$id?>','<?=$idBatchNumber?>','<?=$idForwarder?>');" id="confirm_payment_transfer"><span><?=t($t_base.'fields/yes');?></span></a>
            </p>
        </div>
    </div>
<?php	
}
?>