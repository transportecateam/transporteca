<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__SYSTEM_DNS_AND_APACHE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | DNS and Apache ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/crownJob/";
validateManagement();
$crownJobs=$kAdmin->selectCrownJobs(3);
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
		<div id ="table" style="padding-bottom:20px;">
			<textarea rows="20" cols="94" id="crown_jobs"><?=$crownJobs['szDescription'];?></textarea> 
			<br /><br />
			<a class="button1" style="float: right;" onclick="save_crown_jobs(3);"><span><?=t($t_base.'fields/save');?></span></a>
		</div>		
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>