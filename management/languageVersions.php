<?php 
/**
  *  admin--- maintanance -- lanuage vesrions
  */
define('PAGE_PERMISSION','__SYSTEM_LANGUAGE_VERSIONS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Language - Versions ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/languageConfig/";

$kConfig = new cConfig();
$languagesAry=$kConfig->getLanguageDetails('','','','','',false,'',false,true);
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
            <h4><strong><?=t($t_base.'fields/language_versions');?></strong></h4><br>
            <div id="language_version_list_container_div">
                <?php echo languageVersionsListingView($languagesAry); ?>		
            </div>		
            <div style="padding-top: 10px;"> 
                <div style="clear: both;padding-top: 10px;"></div> 
                <div id="language_version_form_container_div">
                    <?php echo editLanguageVersionsForm($kConfig); ?>
                </div> 
            </div>
        </div>
</div>    

<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
