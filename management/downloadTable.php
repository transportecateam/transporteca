<?php
/**
 * Edit User Information
 */
 
  ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$kBooking = new cBooking();
$kBilling= new cBilling();
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
require_once( __APP_PATH_CLASSES__ . "/PHPExcel.php");
validateManagement();

if(!empty($_REQUEST['id']))
{ 	
	$idForwd=(int)sanitize_all_html_input($_REQUEST['forwarder']);		
	if($idForwd>0)
	{
	$idForwarder=$idForwd;
	}		
	$mode=sanitize_all_html_input($_REQUEST['id']);
	$format = 'Y-m-d'; 
	if($mode=='1')
	{
	$toDate=date($format);
	$fromDate=date($format,strtotime('-1 week'.$toDate));
	}
	if($mode=='2')
	{
	$toDate=date($format);
	$days=date('d')-1;
	//echo $days;
	$fromDate=date($format,strtotime('-'.$days.' days'.$toDate));
	}
	if($mode=='3')
	{
	$days=date('d')-1;
	$toDate=date($format,strtotime('-'.$days.' days '.date($format)));
	$fromDate=date($format,strtotime('-1 month'.$toDate));
	}
	if($mode=='4')
	{
	$days=date('d')-1;
	$toDate=date($format);
	$fromDate=date($format,strtotime('-'.$days.' days -2 month'.$toDate));
	}
	if($mode=='5')
	{
	$toDate=date("Y-m-d H:i:s");
	$fromDate=date($format,strtotime('-3 months'.$toDate));
	}
	if($mode=='6')
	{
		$toDate=date("Y-m-1 H:i:s");
		$toDate=date($format,strtotime('-2 months'.$toDate));
		$fromDate=date($format,strtotime('-3 months'.$toDate));
	}
	if($mode=='7')
	{
	$days=date('d')-1;
	$month=date('m')-1;
	$toDate=date($format);
	$fromDate=date($format,strtotime('-'.$days.' days -'.$month.' month'.$toDate));
	}
	if($mode=='8')
	{
	$days=date('d')-1;
	$month=date('m')-1;
	$toDate=date($format,strtotime('-'.$days.' days -'.$month.' month'.date($format)));
	$fromDate=date($format,strtotime('-1 year'.$toDate));
	}
	if($mode=='9')
	{
	$fromDate='';
	$toDate='';
	}
	if($fromDate!='')
	{
	$date = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
	$date="  BETWEEN '".$fromDate."' and '".$date."'";
	}
	else
	{
	$date='';
	}
	if($fromDate)
	{
	$dateRange= 'Date range: '.date('d/m/Y',strtotime($fromDate)).' - '.date('d/m/Y',strtotime($toDate));
	}
	$date ="dtPaymentConfirmed ".$date."";
	
	
	$path='management';
	$toDate=date_default_timezone_get();
	$sum			 = $kBooking->sumInvoicesBilling($idForwarder,6,'',$toDate);
	echo $file_name = $kBooking->download_billing_table($date,$dateRange,$idForwarder,$path,$sum,$date);
	if(!empty($file_name))
	{
		//header("Location:".__FORWARDER_HOME_PAGE_URL__.'/forwarderBulkExportImport/forwarderDetails/'.$file_name.'.xlsx');
		//$file_path = __APP_PATH_ROOT__."/forwarders/forwarderBulkExportImport/forwarderDetails/".$mode.".xlsx" ;
		
		if(file_exists($file_path))
		{
			//@unlink($file_path);
		}	
		exit;
	}
}
?>