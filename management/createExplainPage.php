<?php 
/**
  *  Expalin page Adding new subcategories
  * 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Setup pages for Explain Section";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/layout/admin_header.php");
validateManagement();
$t_base="management/explaindata/";

?>

<div id="hsbody-2">
    <?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
    <div class="hsbody-2-right">
        <?php echo createNewExplanationPage($t_base); ?>	
    </div>
</div>
	
<?php include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); ?>	