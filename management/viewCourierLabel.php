<?php
/**
 * View Courier Label
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH__ . "/inc/functions.php" );
//include( __APP_PATH_LAYOUT__ . "/ajax_header.php" ); 
require_once( __APP_PATH__ . "/pdfLib/PDFMerger.php" ); 
//require_once(__APP_PATH__.'/invoice/fpdf.php');
$kBooking = new cBooking();
$kCourierServices = new cCourierServices();
//validateManagement();
 
$idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));  
//$idBooking = $kBooking->getBookingIdByBookingRef($szBookingRandomNum);
if((int)$idBooking==0)
{
    // header('Location: '.__BASE_URL__);
     //die(); 
}
$pdfFileArr=$kCourierServices->getCourierLabelUploadedPdf($idBooking,true);
 
$pdf = new PDFMerger;

if(count($pdfFileArr)>0)
{
    $strPdf='';
     $courierLabelPath=__UPLOAD_COURIER_LABEL_PDF__."/courierLabel.pdf";
    foreach($pdfFileArr as $pdfFileArrs)
    {
        $filePath=__UPLOAD_COURIER_LABEL_PDF__."/".$pdfFileArrs;
        if($strPdf=='')
        {
            $strPdf =$pdf->addPDF($filePath,"all");
        }
        else
        {
            $strPdf =$pdf->addPDF($filePath,"all");
        }
    }
     $strPdf =$pdf->merge('browser', $courierLabelPath);
   // echo $pdf."".$strPdf;
}
?>
