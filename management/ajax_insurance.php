<?php 
/**
  *  Admin---> System --> Insurance
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
require_once( __APP_PATH_CLASSES__.'/insurance.class.php' );
require_once( __APP_PATH_CLASSES__.'/warehouseSearch.class.php' );

$t_base="management/insurance/";
$t_base_error="management/Error/";
validateManagement();
  
$kBooking = new cBooking();
$kInsurance = new cInsurance();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));

if($mode=='EDIT_INSURANCE')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType']));
    $idInsurance = sanitize_all_html_input(trim($_REQUEST['idInsurateRate']));
     
    if($idInsurance>0 && $iType>0)
    {
        $kInsurance->getInsuranceDetails($idInsurance,true,$iType); 
        if($kInsurance->idInsurance>0)
        {
            $_POST['addInsuranceAry'][$iType] = array();
            echo "SUCCESS||||";
            echo display_insurance_sell_buy_rate_addedit_form($iType,$kInsurance);
            die;
        }
        else
        {
            $bDisplayError = true;
        } 
    }
    else
    {
        $bDisplayError = true;
    }
    
    if($bDisplayError)
    {
        echo "ERROR||||";
        $szTitle =  "";
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die; 
    } 
}
else if($mode=='EDIT_INSURANCE_RATE')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType']));
    $idInsurateRate = sanitize_all_html_input(trim($_REQUEST['idInsurateRate']));

    $kInsurance->loadInsuranceRates($iType,$idInsurateRate);
    if($kInsurance->id>0)
    { 
        echo "SUCCESS||||";
        echo display_insurance_buy_rate_form($kInsurance,$iType,$insuranceAry,$kInsurance->idInsurance);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  "";
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die; 
    }
}
else if($mode=='SORT_INSURANCE_RATE_LISTING')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType'])); 
    $szSortValue = sanitize_all_html_input(trim($_REQUEST['szSortValue'])); 
    $idPrimarySortKey = sanitize_all_html_input(trim($_REQUEST['idPrimarySortKey'])); 
    
    if($iType>0)
    {
        $sortingAry = array();
        $sortingAry['idPrimarySortKey'] = $idPrimarySortKey;
        $sortingAry['szSortValue'] = $szSortValue;
        
        echo "SUCCESS||||";
        echo display_insurance_rates_listing($iType,$kInsurance,$sortingAry);
    }
    die;
}
else if($mode=='CLEAR_ISURANCE_FORM_DATA')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType'])); 
    if($iType>0)
    {
        echo "SUCCESS||||";
        echo display_insurance_rates_listing($iType,$kInsurance);
    }
    die;
}
else if($mode=='DELETE_INSURANCE_RATE')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType']));
    $idInsurateRate = sanitize_all_html_input(trim($_REQUEST['idInsurateRate']));

    if($iType==__INSURANCE_BUY_RATE_TYPE__) //Buy rate
    {
        $kInsurance->getInsuranceDetails($idInsurateRate,true); 
    }
    else
    {
        $kInsurance->loadInsuranceRates($iType,$idInsurateRate);
    }
    
    if($kInsurance->id>0)
    {
        echo "SUCCESS||||";
        echo display_insurance_rate_delete_confirmation($kInsurance,$iType);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/delete_insurance_rate');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
}
else if($mode=='CANCEL_NEW_INSURED_BOOKING')
{ 
    $idBooking = sanitize_all_html_input(trim($_REQUEST['booking_id'])); 
    $szPageName = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    if($postSearchAry['iInsuranceStatus']==3)
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/insurance_booking_already_cancelled');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
    else
    {
        if(!empty($postSearchAry) && $postSearchAry['iInsuranceIncluded']==1)
        {
            echo "SUCCESS||||";
            echo display_insuredbooking_cancel_confirmation($postSearchAry,$szPageName);
            die;
        }
        else
        {
            echo "ERROR||||";
            $szTitle =  t($t_base.'fields/insurance_notification');
            $szMessage = t($t_base.'fields/internal_server_error');
            $div_id = 'insurance_rate_popup';
            echo success_message_popup_admin($szTitle,$szMessage,$div_id);
            die;
        }
    }
} 
else if($mode=='CONFIRM_INSURED_BOOKING')
{ 
    $idBooking = sanitize_all_html_input(trim($_REQUEST['booking_id'])); 
    $szPageName = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $postSearchAry = $kBooking->getBookingDetails($idBooking);

    if(!empty($postSearchAry) && $postSearchAry['iInsuranceIncluded']==1)
    {
        echo "SUCCESS||||";
        echo display_insuredbooking_cancel_confirmation($postSearchAry,$szPageName);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
}
else if($mode=='CANCEL_NEW_INSURED_BOOKING_CONFIRM')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['booking_id']));
    $iSendEmailToCustomer = sanitize_all_html_input(trim($_REQUEST['iSendCreditNote'])); 
    $szPageName = sanitize_all_html_input(trim($_REQUEST['from_page'])); 
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    $page=(int)$_POST['page'];
    $limit=$_POST['limit'];
    
    if(!empty($postSearchAry) && $postSearchAry['iInsuranceIncluded']==1)
    {
        if($kBooking->calcaleBookingInsurance($idBooking,$iSendEmailToCustomer))
        {
            echo "SUCCESS||||"; 
            if($szPageName=='NEW_INSURED_BOOKING')
            {
                $newInsuredBookingAry = array();
                $newInsuredBookingAry = $kInsurance->getInsuranceDataForNewBooking();
                echo display_new_booking_listing_insurance($newInsuredBookingAry);
            }
            else if($szPageName=='SENT_INSURED_BOOKING')
            {
                $newInsuredBookingAry = array();
                $newInsuredBookingAry = $kInsurance->getInsuranceDataForNewBooking(__BOOKING_INSURANCE_STATUS_SENT__);
                echo display_sent_insured_booking_listing($newInsuredBookingAry);
            }
            else if($szPageName=='CONFIRMED_INSURED_BOOKING')
            {
                    $newInsuredBookingAry = array();
                    $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(__BOOKING_INSURANCE_STATUS_CONFIRMED__); 
                    echo display_sent_insured_booking_listing($newInsuredBookingAry,__BOOKING_INSURANCE_STATUS_CONFIRMED__,$page,$limit);
            }
            else if($szPageName=='CANCELLED_INSURED_BOOKING')
            {
                    $newInsuredBookingAry = array();
                    $newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(__BOOKING_INSURANCE_STATUS_CANCELLED__);  
                    echo display_sent_insured_booking_listing($newInsuredBookingAry,__BOOKING_INSURANCE_STATUS_CANCELLED__,$page,$limit);
            }
            die;
        } 
        else
        {
            echo "ERROR||||";
            $szTitle =  t($t_base.'fields/insurance_notification');
            $szMessage = t($t_base.'fields/internal_server_error');
            $div_id = 'insurance_rate_popup';
            echo success_message_popup_admin($szTitle,$szMessage,$div_id);
            die;
        }
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
}
else if($mode=='DELETE_INSURANCE_RATE_CONFIRM')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType']));
    $idInsurateRate = sanitize_all_html_input(trim($_REQUEST['idInsurateRate']));
    
    if(!empty($idInsurateRate))
    {
        $idInsurateRate = explode(";",$idInsurateRate);
    } 
    $bDisplayError = false; 
    if($kInsurance->deleteInsurance($iType,$idInsurateRate))
    { 
        echo "SUCCESS||||";
        echo display_insurance_rates_listing($iType,$kInsurance);
        if($iType==__INSURANCE_BUY_RATE_TYPE__)
        {
            if(!empty($kInsurance->buyRateAvailableAryStr) || !empty($kInsurance->buyRateNotAvailableAryStr))
            echo "||||";
            echo $kInsurance->buyRateAvailableAryStr;
            echo "||||";
            echo $kInsurance->buyRateNotAvailableAryStr;
        }
        die;
    }
    else
    {
        $bDisplayError = true;
    } 
    
    if($bDisplayError)
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/delete_insurance_rate');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
} 
else if($mode=='REUSE_INSURANCE_RATE')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType']));
    $idInsurance = sanitize_all_html_input(trim($_REQUEST['idInsurateRate']));

    $insuranceAry = array(); 
    $insuranceAry = $kInsurance->getInsuranceDetails($idInsurance); 
    
    if(!empty($insuranceAry))
    {  
        echo "SUCCESS||||";
        echo display_insurance_rate_reuse_confirmation($kInsurance,$insuranceAry);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/delete_insurance_rate');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
} 
else if($mode=='DISPLAY_SELL_RATE_LISTING')
{
    $iType = sanitize_all_html_input(trim($_REQUEST['iType']));
    $idInsurance = sanitize_all_html_input(trim($_REQUEST['idInsurance']));
    $iAllDeleted = sanitize_all_html_input(trim($_REQUEST['deleted_all']));
    
    if($idInsurance>0)
    {
        $insuranceSellRateAry = array();
        $insuranceSellRateAry = $kInsurance->getInsuranceRates(__INSURANCE_SELL_RATE_TYPE__,$idInsurance);
        echo "SUCCESS||||";
        echo display_insurance_buyer_listings(__INSURANCE_SELL_RATE_TYPE__,$insuranceSellRateAry,$idInsurance);
        die;
    } 
    else if($iAllDeleted==1)
    {
        $insuranceSellRateAry = array();
        echo "SUCCESS||||";
        echo display_insurance_buyer_listings(__INSURANCE_SELL_RATE_TYPE__,$insuranceSellRateAry,$idInsurance);
        die;
    }
}
else if($mode=='PREVIEW_INSURANCE_SHEET')
{
    if($kInsurance->sendInsuranceEmailToVendor($_POST['sendInsuranceEmail'],"PREVIEW"))
    {   
        if(!empty($kInsurance->szInsuranceSheetFileName))
        {
            echo "SUCCESS||||";
            echo __MAIN_SITE_HOME_PAGE_URL__."/insuranceSheets/".$kInsurance->szInsuranceSheetFileName;
        } 
        else
        {
            echo "No file found...";
        }
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
    die;
}
else if(!empty($_POST['reuseInsuranceRateAry']))
{
    if($kInsurance->reuseInsuranceProduct($_POST['reuseInsuranceRateAry']))
    {    
        $_POST['addInsuranceAry'] = array();  
        $idLastAddedInsurance = $kInsurance->idLastAddedInsurance ;

        $insuranceBuyRateAry = array();
        $insuranceBuyRateAry = $kInsurance->getAllInsuranceBuyRates();

        $insuranceSellRateAry = array();
        echo "SUCCESS_BUYRATE||||";
        echo display_insurance_buyer_listings(__INSURANCE_BUY_RATE_TYPE__,$insuranceBuyRateAry,$idLastAddedInsurance); 
        die;
    }
    else
    {
        echo "ERROR||||"; 
        echo display_insurance_rate_reuse_confirmation($kInsurance,$insuranceAry);
        die;
    }
}
else if(!empty($_POST['confirmBookingAry']))
{
    $iNumBookingSelected = count($_POST['confirmBookingAry']['iConfirmBooking']);
    if($iNumBookingSelected>0)
    { 
        $confirmedBookingAry = array();
        $confirmedBookingAry = $_POST['confirmBookingAry']['iConfirmBooking'] ;
        $iCheckValueFlagAry = $_POST['confirmBookingAry']['iCheckValueFlag'] ;

        echo "SUCCESS||||";
        echo display_confirm_insured_booking_confirmation($confirmedBookingAry,$iCheckValueFlagAry);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/select_at_least_one');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
    die;
}
else if(!empty($_POST['confirmInsuranceBooking']))
{ 
    $confirmInsuranceBooking = $_POST['confirmInsuranceBooking'] ;
    $pipelineAry = array();
    $pipelineAry = explode("XXXX",$confirmInsuranceBooking['idBookingPipeLine']); 
    $iCheckValueFlagAry = explode("XXXX",$confirmInsuranceBooking['iCheckValueFlagStr']); 

    if($kInsurance->updateInsuranceStatus($pipelineAry,__BOOKING_INSURANCE_STATUS_CONFIRMED__,$iCheckValueFlagAry))
    {  
        $confirmedInsuredBookingAry = array();
        $confirmedInsuredBookingAry = $kInsurance->getInsuranceDataForNewBooking(__BOOKING_INSURANCE_STATUS_SENT__);

        echo "SUCCESS||||";
        echo display_sent_insured_booking_listing($confirmedInsuredBookingAry);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
}
else if(!empty($_POST['insuranceTryitOut']))
{
    $insuranceTryResultAry = array();
    $insuranceTryResultAry = $kInsurance->processInsuranceTryitout($_POST['insuranceTryitOut']);
    
    if(!empty($kInsurance->arErrorMessages) && $kInsurance->iInsuranceOtherErrors<=0)
    {
        echo "ERROR||||";
        echo display_try_it_out_search_form($kInsurance); 
    }
    else
    {
        echo "SUCCESS||||";
        echo display_try_it_out_result($insuranceTryResultAry,$kInsurance,$_POST['insuranceTryitOut']['idBookingCurrency']);
    }
    die;
}
else if(!empty($_POST['sendBookingAry']))
{
    $iNumBookingSelected = count($_POST['sendBookingAry']['iSendBooking']);
    if($iNumBookingSelected>0)
    { 
        $sendBookingAry = $_POST['sendBookingAry']['iSendBooking'] ; 

        echo "SUCCESS||||";
        echo display_insured_booking_send_confirmation($sendBookingAry);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/select_at_least_one');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
    die;
}
else if(!empty($_POST['sendInsuranceEmail']))
{   
    if($kInsurance->sendInsuranceEmailToVendor($_POST['sendInsuranceEmail']))
    {  
        $newInsuredBookingAry = array();
        $newInsuredBookingAry = $kInsurance->getInsuranceDataForNewBooking();

        echo "SUCCESS||||";
        echo display_new_booking_listing_insurance($newInsuredBookingAry);
        die;
    }
    else
    {
        echo "ERROR||||";
        $szTitle =  t($t_base.'fields/insurance_notification');
        $szMessage = t($t_base.'fields/internal_server_error');
        $div_id = 'insurance_rate_popup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id);
        die;
    }
    die;
}
else if(!empty($_POST['addInsuranceAry']))
{
    $iType = sanitize_all_html_input(trim($_POST['addInsuranceAry']['iType'])); 
    $idInsuranceParent = sanitize_all_html_input(trim($_POST['addInsuranceAry'][$iType]['idInsuranceParent'])); 
    $idInsuranceRates = sanitize_all_html_input(trim($_POST['addInsuranceAry'][$iType]['idInsuranceRates'])); 
        
    if($kInsurance->addInsurance($_POST['addInsuranceAry'][$iType]))
    {  
        echo "SUCCESS||||";
        echo display_insurance_rates_listing($iType,$kInsurance);  
        if($iType==__INSURANCE_BUY_RATE_TYPE__)
        {
            if(!empty($kInsurance->buyRateAvailableAry) || !empty($kInsurance->buyRateNotAvailableAry))
            echo "||||";
            echo implode(";",$kInsurance->buyRateAvailableAry);
            echo "||||";
            echo implode(";",$kInsurance->buyRateNotAvailableAry);
        }
        die; 
    }
    else
    {
        echo "ERROR||||";
        echo display_insurance_sell_buy_rate_addedit_form($iType,$kInsurance); 
        die;
    } 
}