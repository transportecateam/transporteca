<?php 
/**
  *  Send Bulk Email to forwarders
  */
define('PAGE_PERMISSION','__MESSAGE_TO_FORWARDERS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Send Messages - To Forwarders ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

$kForwarder	= new 	cForwarder();
?>
	<div id="ajaxLogin"></div>
	<div id="hsbody-2">
	<? require_once(__APP_PATH__ ."/layout/admin_messages_left_nav.php"); ?>
		
	<div id="content_body" class="hsbody-2-right">
		<?=forwarderBulkMsgView()?>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>