<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base = "management/Error/";
if(isset($_POST['mode']))
{
	$mode = sanitize_all_html_input($_POST['mode']);
	if($mode == 'SHOW_EXPIRY_WTW_DATE')
	{
		$id = (int)$_POST['id'];
		$validityExpiring = $kAdmin->selectDtValidExpiry($id);
		echo DATE('d/m/Y',strtotime($validityExpiring['dtValidFrom']));
		echo "|||||";
		echo DATE('d/m/Y',strtotime($validityExpiring['dtExpiry']));
	}
	if($mode == 'SAVE_DATE_VALIDATION')
	{
		$id = (int)$_POST['id'];
		$dtValidFrom = sanitize_all_html_input($_POST['value1']);
		$dtValidTo = sanitize_all_html_input($_POST['value2']);
		if($kAdmin->setvaildateExpiry($id,$dtValidFrom,$dtValidTo))
		{
			echo 'SUCCESS|||||';
			$content = $kAdmin->expiringWTWAdmin();
			expiringWTW($content);
		}
		else if(!empty($kAdmin->arErrorMessages))
			{ echo "ERROR|||||";
			?>	<br />
				<div id="regError" class="errorBox ">
				<div class="header"><?=t($t_base.'fields/please_following');?></div>
				<div id="regErrorList">
				<ul>
				<?php
					foreach($kAdmin->arErrorMessages as $key=>$values)
					{
					?><li><?=$values?></li>
					<?php	
					}
				?>
				</ul>
				</div>
				</div>
				<?php  
			}
	}
	if($mode == 'SORT_TABLE')
	{
		$sortBy = sanitize_all_html_input($_POST['sortby']);
		$order = sanitize_all_html_input($_POST['sort']);
		$content = $kAdmin->expiringWTWAdmin($sortBy,$order);
		$t_base = "management/operations/expirywtw/";
		expiringWTW($content);
	}
}

?>
