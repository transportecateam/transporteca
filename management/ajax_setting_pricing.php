<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base="management/AllProfiles/";
$t_base_error="management/Error/";
if(ISSET($_POST))
{
	$kForwarderContact = new cForwarderContact();	
	$idforwarder = sanitize_all_html_input($_POST['id']);
	if(isset($_POST['updatePrefControlArr']))
	{	
		if($_POST['flag']=='EDIT_SETTING')
		{
                    $kForwarderContact->updateSettingForwarder($_POST['updatePrefControlArr'],$idforwarder);
		}	
		if($_POST['flag']=='EDIT_PRICING')
		{
                    $kForwarderContact->updatePricingForwarder($_POST['updatePrefControlArr'],$idforwarder);
		}
		if($_POST['flag']=='EDIT_COMMENTS')
		{
                    $kForwarderContact->updateCommentsForwarder($_POST['updatePrefControlArr'],$idforwarder);
		}
                if(!empty($kForwarderContact->arErrorMessages))
                {	
                        ?>ERROR|||||
                        <div id="regError" class="errorBox" style="display:block;">
                            <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
                            <div id="regErrorList">
                                <ul>
                                    <?php foreach($kForwarderContact->arErrorMessages as $key=>$values){ ?> 
                                        <li><?php echo $values; ?>
                                            <script type="text/javascript">
                                                if($('#'+'<?php echo $key; ?>').length)
                                                {
                                                    $('#'+'<?php echo $key; ?>').addClass('red_border'); 
                                                }  
                                            </script> 
                                        </li> 
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <?php
                }
                else
                {
                    echo "SUCCESS|||||";
                }
                die;
	}
	$mode = sanitize_all_html_input($_POST['mode']);
	
	echo "<div id='error_log'></div><form id='pricingAndSetting'>";
	if($mode == 'EDIT_SETTING')
	{	
            $forwarderCP2=$kForwarderContact->getSystemPanelforwarderSettingDet($idforwarder);
            $controlPanel='';
            if(isset($forwarderCP2['szControlPanelUrl']))
            $controlPanel = $kForwarderContact->findSubDomainName($forwarderCP2['szControlPanelUrl']);	

            $iAvailableForManualQuotes = $forwarderCP2['iAvailableForManualQuotes'] ;
            $iOffLineQuotes = $forwarderCP2['iOffLineQuotes'] ;
            $iDisplayQuickQuote = $forwarderCP2['iDisplayQuickQuote'] ; 
	
	?>
		<h4><strong><?=t($t_base.'title/settings');?></strong></h4>						
		<div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/codeForBooking');?></span>
                    <span class="field-container"><input style="width:60px;" type="text" name="updatePrefControlArr[BookingRef]" value='<?=$forwarderCP2['szForwarderAlias']?>' > <span style="font-style: italic">  <?=t($t_base.'fields/two_cap_letter');?></span></span>
		</div>
		<div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/urlDns');?></span>
                    <span class="field-container" style="width:50%;" ><input style="width:181px;" type="text" name="updatePrefControlArr[dnsURL]" value=<?=$controlPanel;?>><span style="font-style: italic"> <?=t($t_base.'fields/tenasporetca_com');?></span></span>
		</div>
		<div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/available_formmanual_quotes');?></span>
                    <span class="field-container" style="width:50%;" >
                        <select name="updatePrefControlArr[iAvailableForManualQuotes]" id="iAvailableForManualQuotes"> 
                            <option value="0" <?php echo (((int)$iAvailableForManualQuotes==0)?'selected':''); ?>><?=t($t_base.'fields/no');?></option>
                            <option value="1" <?php echo (((int)$iAvailableForManualQuotes==1)?'selected':''); ?>><?=t($t_base.'fields/yes');?></option>
                        </select>
                    </span>
		</div> 
                <div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/rfq_response');?></span>
                    <span class="field-container" style="width:50%;" >
                        <select name="updatePrefControlArr[iOffLineQuotes]" id="iOffLineQuotes"> 
                            <option value="0" <?php echo (((int)$iOffLineQuotes==0)?'selected':''); ?>><?php echo __RFQ_RESPONSE_IN_SYSTEM__;?></option>
                            <option value="1" <?php echo (((int)$iOffLineQuotes==1)?'selected':''); ?>><?php echo __RFQ_RESPONSE_BY_EMAIL__;?></option>
                        </select>
                    </span>
		</div> 
                <div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/quick_quote_available');?></span>
                    <span class="field-container" style="width:50%;" >
                        <select name="updatePrefControlArr[iDisplayQuickQuote]" id="iDisplayQuickQuote"> 
                            <option value="0" <?php echo (((int)$iDisplayQuickQuote==0)?'selected':''); ?>><?=t($t_base.'fields/no');?></option>
                            <option value="1" <?php echo (((int)$iDisplayQuickQuote==1)?'selected':''); ?>><?=t($t_base.'fields/yes');?></option>
                        </select>
                    </span>
		</div> 
	<?php
		
	}
	if($mode == 'EDIT_PRICING')
	{	
            $kForwarderContact = new cForwarderContact();	
            $forwarderCP2=$kForwarderContact->getSystemPanelforwarderPricingAdmin($idforwarder);
	
	?>	
		<script type="text/javascript">
                    $().ready(function() {
			$("#datepicker1").datepicker();
                    });
		</script>
		<h4><strong><?=t($t_base.'title/transprteca_pricing');?></strong></h4>						
		<div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/gross_profit');?></span>
                    <span class="field-container">
                        <select name="updatePrefControlArr[iProfitType]" id="iProfitType">
                            <option value="<?php echo __FORWARDER_PROFIT_TYPE_REFERRAL_FEE__; ?>" <?php echo (($forwarderCP2['iProfitType']==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__)?'selected':''); ?>><?php echo __FORWARDER_PROFIT_TYPE_REFERRAL_FEE_TEXT__ ; ?></option>
                            <option value="<?php echo __FORWARDER_PROFIT_TYPE_MARK_UP__; ?>" <?php echo (($forwarderCP2['iProfitType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)?'selected':''); ?>><?php echo __FORWARDER_PROFIT_TYPE_MARK_UP_TEXT__ ; ?></option>
                        </select>
                    </span>
		</div>
                <?php
                    echo displayForwarderReferralFeeDetails($idforwarder,true);
                ?> 
                <div class="profile-fields">
                    <span class="field-name"><?=t($t_base.'fields/credit_offered_transporteca');?></span>
                    <span class="field-container"><input type="text" style="width: 100px;" name="updatePrefControlArr[iCreditDays]" value='<?=$forwarderCP2['iCreditDays'];?>'>&nbsp; days</span>
		</div> 
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/validto');?></span>
			<span class="field-container"><input type="text" style="width: 100px;" id="datepicker1" onblur="show_me(this.id,'dd/mm/yyyy')" onfocus="blank_me(this.id,'dd/mm/yyyy')" name="updatePrefControlArr[dtValidTo]" value='<?=(($forwarderCP2['dtReferealFeeValid']!='0000-00-00 00:00:00' && $forwarderCP2['dtReferealFeeValid']!=''  )?date('d/m/Y',strtotime($forwarderCP2['dtReferealFeeValid'])):date('d/m/Y'))?>'></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/automatic_pay');?></span>
			<span class="field-container">
			<select name="updatePrefControlArr[iAutomaticPaymentInvoicing]">
			<option value="1" <? echo ($forwarderCP2['iAutomaticPaymentInvoicing']?"selected=selected":'')?>><?=t($t_base.'fields/yes');?></option>
			<option value="0" <? echo ($forwarderCP2['iAutomaticPaymentInvoicing']?'':"selected=selected")?>><?=t($t_base.'fields/no');?></option>
			</select>
		</div>
	<?php
		
	}
	if($mode == 'EDIT_COMMENTS')
	{	
	$kForwarderContact = new cForwarderContact();	
	$forwarderCP2=$kForwarderContact->getSystemPanelforwarderCommentsAdmin($idforwarder);
	
	?>	
	<script type="text/javascript">
		$(function(){
		checkLimit('szCommentText');
		});
	</script>
			
		
	<h4><strong><?=t($t_base.'fields/comments_for_pricing');?> </strong></h4>							
	<div>
		<textarea style="width:100%;" name="updatePrefControlArr[szComment]" rows="10" cols="85" id="szCommentText" maxlength="400" onkeyup="checkLimit(this.id)"><?=str_replace('<br />',"",$forwarderCP2['szReferalFeeComment'])?></textarea>
	</div>
	<p align="right">You have <span id="char_left">400</span> <?=t($t_base.'fields/char_left');?></p>
		
	<?
		
	}
	?>
			<input type="hidden" name="flag" value="<?=$mode?>">							
			<input type="hidden" name="id" value="<?=$idforwarder;?>">
			</form>	
			<br />
			<div style="clear: both;"></div>
                        <div class="oh">
                            <p class="fl-30" align="right">
                                <a href="javascript:void(0);" onclick="submit_setting_pricing('<?=$idforwarder?>')"  class="button1"><span><?=t($t_base.'fields/save');?></span></a>	
                            </p>
                            <p class="field-container">	
                                &nbsp;<a href="javascript:void(0);" onclick="viewSettingPricing('<?=$idforwarder?>')"  class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>
                            </p>				
                        </div>
	<?
}
?>