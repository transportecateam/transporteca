<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
$t_base="management/AllProfiles/";
$t_base_error="management/Error/";
validateManagement();
if(isset($_POST))
{
$id = (int)sanitize_all_html_input($_POST['id']);
$historyDetails=$kAdmin->historyDetails($id);
echo ($historyDetails['szComment']?$historyDetails['szComment']:t($t_base.'title/no_content_to_display')).'|||||'.($historyDetails['szVersionURL']?$historyDetails['szVersionURL']:'');
}
?>