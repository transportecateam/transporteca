<?php 
/**
  *  LIST OF ALL CFS ( WAREHOUSES ) ON THE MAP
  * 
  */
define('PAGE_PERMISSION','__CFS_LOCATION_MAP__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | CFS Location -  Map";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$kWHSSearch=new cWHSSearch();
validateManagement();

$t_base	= "management/cfsLoctation/";

?>
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>

<div id="hsbody-2">
    <?php require_once( __APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php" ); ?>
    <div class="hsbody-2-right">
    <?php echo showCurrentCFSMapTopForm($t_base);?>
    <div id="popup_container_google_map" style="display:none;">
        <iframe id="google_map_target_1" class="google_map_select"  style="width:720px;" scrolling="no" name="google_map_target_1" src=""></iframe>
    </div>
</div>
</div>
<?php 
	include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	