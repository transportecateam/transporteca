<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
if(isset($_POST))
{
	$action			=	sanitize_all_html_input($_POST['action']);
	$idForwarder 	= 	(int)sanitize_all_html_input($_POST['id']);
	if($action == 'SELECT_FORWARDER_DETAILS' && ($idForwarder>0))
	{
		operationFeedbackDetails($idForwarder);
	}
}


?>
