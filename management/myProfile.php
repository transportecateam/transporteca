<?php 
/**
  *admin Profile 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Settings - My Account";

	if( !defined("__APP_PATH__") )
		define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
	require_once (__APP_PATH__ ."/inc/constants.php");
	require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/myprofile/";
validateManagement();

$kConfig = new cConfig();
$kConfig->loadCountry($kAdmin->idInternationalDialCode);
$iInternationDialCode = $kConfig->iInternationDialCode;

?>

<div id="hsbody-2">
	<div class="hsbody-2-left account-links">
            <p>My Account</p>
            <ul>
                <li class="active"><a href="<?=__MANAGEMENT_MY_ACCOUNT_URL__;?>"> <?=t($t_base.'title/settings');?></a></li>
            </ul>
	</div>
	<div class="hsbody-2-right">
		<div id="myprofile_info">
			<div id="user_information">
				<h5><strong><?=t($t_base.'fields/forwarder_info');?></strong> <a href="javascript:void(0)" onclick="editForwarderUserInformation();"><?=t($t_base.'fields/edit');?></a></h5>	
	
                                <div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/title');?></span>
					<span class="field-container"><?=$kAdmin->szTitle?></span>
					</div>
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
					<span class="field-container"><?=$kAdmin->szFirstName?></span>
					</div>
					
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
					<span class="field-container"><?=$kAdmin->szLastName?></span>
					</div>
					
					
			</div>
			</br>
			</br>
		  <div id="contact_information">
				<h5><strong><?=t($t_base.'fields/contact');?></strong> <a href="javascript:void(0)" onclick="editForwarderContactInformation();"><?=t($t_base.'fields/edit');?></a></h5>	
		
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/email');?></span>
					<span class="field-container"><?=$kAdmin->szEmail?><?php if($kAdmin->iActive!='1')
{?><span style='color:red;font-size:13px;'>(<?=t($t_base.'messages/not_verified');?>)</span> <? }?></span>		</span>
					</div>
                                <div class="ui-fields">
                                    <span class="field-name"><?=t($t_base.'fields/direct_phone');?> <span></span></span>
                                    <span class="field-container"><?php echo "+".$iInternationDialCode." ".$kAdmin->szPhone; ?></span>
                                </div>
					
					
			</div>
			</br>
			</br>
			<div id="password_information">
				<h5 style="width:30%;float:left;"><strong><?=t($t_base.'fields/password');?></strong> <a href="javascript:void(0);" onclick="change_forwarder_password();"><?=t($t_base.'fields/edit');?></a></h5>
						<span class="field-container" >*************</span>
			</div>
			<div style="clear: both"></div>
				</div>
		</div>
		
	</div>

<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>