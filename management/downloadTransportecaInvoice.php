<?php 
/**
  *pay forwarder
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Accounts Payable - Transporteca Invoices";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/vat_functions.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/forwarder_transporteca_invoice/";
validateManagement();
$kBooking= new cBooking();
$t_base="management/billings/";
$checkPostFlag=true;
if(empty($_POST['bookingSearchInvoiceAry'])){
$_POST['bookingSearchInvoiceAry']['dtToDate']="30/06/2016";
    $_POST['bookingSearchInvoiceAry']['dtFromDate']="01/07/2015";
    $checkPostFlag=false;
}
$paymentDetails=$kAdmin->paymentOfForwardersTransactionDownload($idAdmin,true,$_POST['bookingSearchInvoiceAry']);

 $pdf=new HTML2FPDFBOOKING();
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>
<div id="checkDetails" class="hsbody-2-right">
<form  id="transporteca_all_invoice_seach_form" method="post" name="transporteca_all_invoice_seach_form">
	
		<div class="oh">
				
			
		<p class="fl-30">
			<span class="f-size-12"><?=t($t_base.'fields/from_date');?></span><br />
			<span id="origin_warehouse_span">
                            <input readonly="" type="text" id="datepicker1" name="bookingSearchInvoiceAry[dtFromDate]" value="<?=$_POST['bookingSearchInvoiceAry']['dtFromDate']?>">
			</span>
		</p>	
		
		<p class="fl-30">
			<span class="f-size-12"><?=t($t_base.'fields/to_date');?></span><br />
			<span id="origin_warehouse_span">
				<input readonly="" type="text" id="datepicker2" name="bookingSearchInvoiceAry[dtToDate]" value="<?=$_POST['bookingSearchInvoiceAry']['dtToDate']?>">
			</span>
		</p>	
				
			
		
		<p class="fr-40" align="right"><br />
			<a href="javascript:void(0)" onclick="serch_transporteca_invoice_booking()" class="button1"><span><?=t($t_base.'fields/search');?></span></a>
			
		</p>
	</div>	
</form>
<script type="text/javascript">
	function initDatePicker() 
	{
		$("#datepicker1").datepicker();
		$("#datepicker2").datepicker();
	}
	initDatePicker();
</script>    
	<?php
        $flag='PDF';
        //print_r($paymentDetails);
        if(!empty($paymentDetails) && $checkPostFlag)
        {	
            $count=1; 
            for($i=0;$i<count($paymentDetails);++$i)
            { 
                $idForwarder=$paymentDetails[$i]['idForwarder'];
                $batchNumber=$paymentDetails[$i]['iBatchNo'];
                $idBooking=$paymentDetails[$i]['idBooking'];
                //echo $paymentDetails[$i]['iDebitCredit']."dd<br /><br />";
                if($paymentDetails[$i]['iDebitCredit']=='12')
                {
                    $filename = getBookingHandlingFeeInvoice($idForwarder,$batchNumber,$flag,true);
                }
                else if($paymentDetails[$i]['iDebitCredit']=='3')
                {
                    $filename=getForwarderInvoice($idForwarder,$batchNumber,$flag,true);
                }
                else if($paymentDetails[$i]['iDebitCredit']=='8')
                {
                    $filename=getForwarderCourierInvoice($idForwarder,$batchNumber,$flag,true);
                }
                else if($paymentDetails[$i]['iDebitCredit']=='5')
                {
                    $filename=getForwarderInvoiceUploadService($idForwarder,$batchNumber,$flag,true);
                }
                else if($paymentDetails[$i]['iDebitCredit']=='7')
                {
                    if($paymentDetails[$i]['iInsuranceStatus']==__BOOKING_INSURANCE_STATUS_CANCELLED__)
                    {
                        $filename = getInsuranceCreditNotePdf($idBooking,false,'',true);
                    }
                    else
                    {
                        $filename = getBookingInsuranceInvoicePdf($idBooking,false,'',true);
                    }
                }
                if($paymentDetails[$i]['iDebitCredit']=='12' || $paymentDetails[$i]['iDebitCredit']=='7' || $paymentDetails[$i]['iDebitCredit']=='5' || $paymentDetails[$i]['iDebitCredit']=='3' || $paymentDetails[$i]['iDebitCredit']=='8')
                {
                    $pdf->AddPage('',true);
                    $pdf->WriteHTML($filename,true);
                }
                
            }
            
            $file_name = __APP_PATH_ROOT__."/invoice/Transporteca-ALL-Invoice.pdf"; 
            if(file_exists($file_name))
            {
                    @unlink($file_name);
            }

            $pdf->Output($file_name,'F');

            ob_clean();
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename=Transporteca-ALL-Invoice.pdf');
            ob_clean();
            flush();
            readfile($file_name);
        }
        
        if($checkPostFlag && empty($billingSettledDetails))
        {
            echo "No Record Found";
        }
        ?>
</div>
<input type="hidden" id="szBookingStatusFlag" name="szBookingStatusFlag" value="forwardertransportecaInvoice">
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
