<?php 
/**
  *  admin--- maintanance -- lanuage configuration
  */
define('PAGE_PERMISSION','__SYSTEM_LANGUAGE_SETTING__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Variables - Countries";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/languageConfig/";

$kConfig = new cConfig();
        $languageArr=$kConfig->getLanguageDetails();
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
            <h4><strong><?=t($t_base.'fields/language_configuration');?></strong></h4>
            <div style="padding-bottom: 20px;">
		<div id="error"></div>
			<form style="width:520px;">
                            <span>
                                <select style="width:40%;" id="idLanguage" name="arrSelect[idLanguage]" onchange="selectLanguageConfigurationData();"> 
                                    <?php 
                                    if(!empty($languageArr))
                                    {
                                        foreach($languageArr as $languageArrs)
                                        {?>
                                            <option value="<?php echo $languageArrs['id'];?>" <?php if($languageArrs['id']==__LANGUAGE_ID_ENGLISH__){?> selected <?php }?>><?php echo $languageArrs['szName'];?></option>
                                        <?php }
                                    }?>
                                </select>
                            </span>
                            <span >
                                <select style="width:60%;float:right;" name='arrSelect[idLangConfigType]' id="idLangConfigType" onchange="selectLanguageConfigurationData();">
                                    <option value="" selected="selected"><?=t($t_base.'fields/select_text_for_configuration');?></option>
                                    <option value="__TABLE_TRANSPORT_MODE__">Transport Mode</option>
                                    <option value="__TABLE_WEIGHT_MEASURE__">Weight Measure</option>
                                    <option value="__TABLE_CARGO_MEASURE__">Cargo Measure</option>
                                    <option value="__TABLE_PALLET_TYPE__">Pallet Type</option>
                                    <option value="__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__">Courier Provider Product Packing</option>
                                    <option value="__TABLE_MONTH_NAME__">Month Name </option>					
                                    <option value="__TABLE_TIMING_TYPE__">Timing Type</option> 
                                    <option value="__TABLE_ORIGIN_DESTINATION__">Origin Destination Text</option>
                                    <option value="__TABLE_SHIP_CON_GROUP__">Ship Con Group</option> 
                                    <option value="__TABLE_SERVICE_TYPE__">Service Type</option>
                                    <option value="__TABLE_STANDARD_CARGO_CATEGORY__">Standard Cargo Category</option>
                                    <option value="__SITE_STANDARD_TEXT__">Text Messages</option>
                                </select>
                            </span>
			</form>	
			<div style="float: right;margin-top: -21px;" id="">
				<a class="button2" id="cancel" style="opacity:0.4;"><span><?=t($t_base.'fields/cancel');?></span></a>
				<a class="button1" id="save" style="opacity:0.4;"><span><?=t($t_base.'fields/edit');?></span></a>
			</div>
		</div>
                <div id="previewContent" class="preview_box" style="display:none;">
                </div>
        </div>
</div>    

<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
