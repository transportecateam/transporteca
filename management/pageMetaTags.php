<?php 
define('PAGE_PERMISSION','__FEEDBACK_DETAILS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Page Meta Tags";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/textEditor/";


$kConfig = new cConfig();
$languageArr=$kConfig->getLanguageDetails();

$kExplain =new cExplain();
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
            <h4><strong>Manage Page Meta Tags</strong></h4><br>
            <div style="padding-bottom: 20px;">
		<div id="error"></div>
			<form style="width:520px;">
                            <span>
                                <select style="width:40%;" id="idLanguage" name="arrSelect[idLanguage]" onchange="selectLanguageConfigurationMetaData();"> 
                                    <?php 
                                    if(!empty($languageArr))
                                    {
                                        foreach($languageArr as $languageArrs)
                                        {?>
                                            <option value="<?php echo $languageArrs['id'];?>" <?php if($languageArrs['id']==__LANGUAGE_ID_ENGLISH__){?> selected <?php }?>><?php echo $languageArrs['szName'];?></option>
                                        <?php }
                                    }?>
                                </select>
                            </span>
                            <span >
                                <select style="width:60%;float:right;" name='arrSelect[idLangConfigType]' id="idLangConfigType" onchange="selectLanguageConfigurationMetaData();">
                                    <option value="" selected="selected">Select Page Name</option>
                                    <option value="__REQUIREMENT_PAGE_META_TITLE__">Requirement Page</option>
                                    <option value="__SEARCH_PAGE_META_TITLE__">Search Page</option>
                                    <option value="__CREATE_ACCOUNT_PAGE_META_TITLE__">Create Account Page</option>
                                    <option value="__FORWARDER_SIGNUP_PAGE_META_TITLE__">Forwarder Signup Page</option>
                                    <option value="__PARTNER_PROGRAM_PAGE_META_TITLE__">Partner Program Page</option>
                                    <option value="__RSS_FEED_PAGE_META_TITLE__">RSS Feed Page</option> 
                                    <option value="__TERMS_CONDITION_PAGE_META_TITLE__">Terms & Conditons Page</option>
                                    <option value="__PRIVACY_NOTICE_PAGE_META_TITLE__">Private Notice Page</option>
                                    <option value="__BOOKING_CONFIRMATION_META_TITLE__">Booking Confirmation Page</option>
                                    <option value="__BOOKING_RECEIPT_META_TITLE__">Booking Receipt Page</option>
                                    <option value="__BOOKING_DETAILS_META_TITLE__">Booking Details Page</option>
                                    <option value="__CARGO_READYNESS_META_TITLE__">Cargo Readyness Page</option>
                                    <option value="__DELETE_ACCOUNT_META_TITLE__">Delete Account Page</option>
                                    <option value="__MY_ACCOUNT_META_TITLE__">My Account Page</option>
                                    <option value="__MY_BOOKING_META_TITLE__">My Booking Page</option>
                                    <option value="__PRIVACY_META_TITLE__">Privacy Page For Logged In User</option>
                                    <option value="__REGISTER_SHIPPER_CONSIGNEE_META_TITLE__">Register Shipper Consignee Page</option>
                                    <option value="__SERVICES_PAGE_META_TITLE__">Services Listing Page</option>
                                    <option value="__SITE_MAP_META_TITLE__">Sitemap Page</option>
                                    <option value="__LANDING_PAGE_META_TITLE__">Landing Page</option>
                                    <option value="__BOOKING_GET_QUOTE_META_TITLE__">Booking Get Quote Page</option>
                                    <option value="__BOOKING_GET_RATE_META_TITLE__">Booking Get Rate Page</option>
                                    <option value="__BOOKING_QUOTE_THANKS_PAGE_META_TITLE__">Booking Quote Thanks Page</option>
                                    <option value="__COOKIES_META_TITLE__">Cookies Page</option> 
                                    <option value="__USER_MULTI_ACCESS_META_TITLE__">Multi User Access Page</option>
                                    <option value="__BOOKING_QUOTE_SERVICE_PAGE_META_TITLE__">Booking Quote Service Page</option>
                                    <option value="__EXPLAIN_META_TITLE__">Search Word Page</option>
                                    
                                </select>
                            </span>
			</form>	
			<div style="float: right;margin-top: -21px;" id="">
				<a class="button2" id="cancel" style="opacity:0.4;"><span><?=t($t_base.'fields/cancel');?></span></a>
				<a class="button1" id="save" style="opacity:0.4;"><span><?=t($t_base.'fields/edit');?></span></a>
			</div>
		</div>
                <div id="previewContent" class="preview_box" style="display:none;">
                </div>
</div>    

<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>

