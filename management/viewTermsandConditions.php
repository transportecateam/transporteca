<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__FORWARDER_TC__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Site Text - Forwarder T&C ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/textEditor/";
$kWHSSearch= new cWHSSearch();
$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_FORWARDER__);
$publish=$kWHSSearch->selectPublishmentForwarder();
$style='';
if(!$publish)
{
	$style="style='opacity:0.4;'";
}
?>
<div id="loader" class="loader_popup_bg" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container" >
<div class="popup" style="height: 50px;width: 100px;">
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" style="margin:0px 0 10px 20px;position:static;">
</div>
</div>
</div>

<div id="ajaxLogins" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container" >

<div class="popup">
	<p><?=t($t_base."fields/are_you_sure")?></p>
	<br/>
	<p align="center"><a href="javascript:void(0)" class="button2" onclick="cancelDelete();" ><span><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="deleteTextEditor" class="button1" onclick="deleteTextEditor(mode,id);"><span><?=t($t_base.'fields/delete');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
		<div style="padding-bottom: 20px;">
		<div id="error"></div>
		<div id="showDiv">
			<? //print_r($textEditor); ?>
			<div style="clear:both;"></div>
		
		<div id="showUpdate">
			<?=viewTnc($textEditor,'FORWARDER',__TEXT_EDIT_TERMS_FORWARDER__);?>					
		</div>				
		</div>	
		<div id="text_t_n_C"></div>	
	</div>
	
	<div id="preview_Publish">
	<a id="add" class="cursor-pointer" onclick="editTextEditor('FORWARDERTNC','0');" style="float: left;"><?=t($t_base.'fields/add');?></a>
	<div style="float: right;">
			<a id="preview" class="button1" onclick="preview_system('TNC_FORWARDER','preview');" ><span><?=t($t_base.'fields/preview');?></span></a>
			<a id="publish" class="button1" <?if($publish){?>onclick="preview_system('TNC_FORWARDER','publish');" <? }else{echo $style;} ?> ><span><?=t($t_base.'fields/publish');?></span></a>
			<a id="cancel" class="button2" <?if($publish){?>onclick="preview_system('TNC_FORWARDER','cancel');" <? }else{echo $style;} ?>><span><?=t($t_base.'fields/cancel');?></span></a>
		</div>
		<div style="clear: both;"></div>
		<div >
		<br />
			<div id="preview_data" style="display: none;">
			</div>
		</div>
	</div>		
</div>
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>