<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$t_base="management/Error/";
validateManagement();
if(isset($_POST))
{	
	$id=sanitize_all_html_input($_POST['id']);
	if($_POST['mode']=='VIEW_DETAIL')
	{	
            $detail=$kAdmin->excangeRates($idAdmin,$id); 
            exchangeDetails($detail,true);
	}
	if($_POST['mode']=='SAVE_DETAIL')
	{	 
            $id=sanitize_all_html_input($_POST['arrExchange']['id']);
            $checkUpdateFlag=false;
            $exchange=$_POST['arrExchange'];

            if($_POST['arrExchange'] && $id>0)
            {				
                $updateFlag=false;
                if($_POST['arrExchange']['pricing']=='0' || $_POST['arrExchange']['settling']=='0')
                {  
                    if($kAdmin->saveRatesChangeCurrency($idAdmin,$exchange,$id))
                    {
                        if($_POST['arrExchange']['pricing']=='0')
                        {
                            $pricingHaulage = $kAdmin->checkExchangeRatesHaulageServicePricing($id);
                            $pricingNotExpiredLCL = $kAdmin->checkExchangeRatesLCLServicePricing($id);
                            $pricingCustomClearance=$kAdmin->checkExchangeRatesCustmClearanceoServicePricing($id);
                            //echo $pricingNotExpiredLCL."hi";
                            //echo $pricingCustomClearance."hell";
                            //echo $pricingHaulage."dsa";
                            if($pricingHaulage>0 || $pricingNotExpiredLCL>0 || $pricingCustomClearance>0)
                            {
                                showForwarderPricingCurrencyUpdatePopup($id);
                                $checkUpdateFlag=true;
                            }
                            else
                            {
                                $kAdmin->saveRatesForwarderPricing($id);
                                //$updateFlag=true;
                            }
                        }

                        if($_POST['arrExchange']['settling']=='0' && !$checkUpdateFlag)
                        {
                            $updateFlag=false;
                            $forwarderUsingCurrencyArr=$kAdmin->checkForwarderCurrency($id,true); 
                            if($forwarderUsingCurrencyArr>0)
                            {
                                showForwarderCurrencyUpdatePopup($id);
                                $checkUpdateFlag=true;
                            }
                            else
                            {
                                $kAdmin->saveRatesForwarderSettling($id);
                                //$updateFlag=true;
                            }
                        }
                    }
                }
                else
                { 
                    $data=$kAdmin->saveRates($idAdmin,$exchange,$id);
                }
            }
            if($_POST['arrExchange'] && $id==0)
            {	
                $exchange=$_POST['arrExchange'];
                $data=$kAdmin->addRates($idAdmin,$exchange);
            } 
	}
	if($_POST['mode']=='DELETE_RATES')
	{
            $deleteStatus=$kAdmin->deleteExchangeRates($idAdmin,$id);
            if(!$deleteStatus)
            {
                showErrorCurrencyMessage();
            }
	}
	if($_POST['mode']=='DOWNLOAD_DETAILS')
	{
            $kAdmin = new cAdmin();
            $idAdmin = (int)$_POST['idAdmin'];
            $resultAry =  $kAdmin->downloadExchangeRates($idAdmin);
            
            if(!empty($resultAry))
            {
                echo "SUCCESS||||";
                echo $resultAry;
            }
            else
            {
                echo "ERROR||||";
            }
	}
	if($_POST['mode']=='INACTIVATE_SERVICES')
	{
            $exchange=$_POST['arrExchange'];

            $id=sanitize_all_html_input($_POST['inactivateServiceArr']['idCurrency']);
            $forwarderServiceStr=$_REQUEST['inactivateServiceArr']['idForwarderStr'];
            $szText=$_REQUEST['inactivateServiceArr']['szText'];
            $kAdmin->saveRatesForwarderPricing($id);
            $settling=$_REQUEST['settling'];

            $kAdmin->sendServiceInactivationEmailToForwarder($forwarderServiceStr,$szText,$id);

            if((int)$settling=='0')
            {			
                $updateFlag=false;
                $forwarderUsingCurrencyArr=$kAdmin->checkForwarderCurrency($id,true); 

                if($forwarderUsingCurrencyArr>0)
                {?>
                    <script>
                            $("#loader").attr('style','display:none;');
                    </script>
                <?php	
                    showForwarderCurrencyUpdatePopup($id,true);
                    $checkUpdateFlag=true;
                }
                else
                {
                    $kAdmin->saveRatesForwarderSettling($id);
                ?>
                <script>
                cancel_inactivate_forwarder();
                </script>
                <?php
                    }
		}
		else
		{
                    $kAdmin->saveRatesForwarderSettling($id);
                    ?>
                    <script>
                    cancel_inactivate_forwarder();
                    </script>
                    <?php		
		}
	}
	
	if($_REQUEST['mode']=='UPDATE_FORWARDER')
	{
		$id=$_REQUEST['idCurrency'];
		$forwarderUsingCurrencyArr=$kAdmin->checkForwarderCurrency($id);
		$kAdmin->sendEmailForUpdateForwarderCurrency($forwarderUsingCurrencyArr,$id);
		$kAdmin->saveRatesForwarderSettling($id);
	}
	if(isset($_POST['flag']))
	{
		$flag=sanitize_all_html_input($_POST['flag']);
		if($flag=='DELETE')
		{$mode=sanitize_all_html_input($_POST['mode']);
		$link=__APP_PATH_ROOT__."/management/exchangeRate/".$mode.".xlsx";
		@unlink($link);
		}
	}
}
	//print_r($exchange);
	if(!empty($kAdmin->arErrorMessages)){ 
		?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
				foreach($kAdmin->arErrorMessages as $key=>$values)
				{
				?><li><?=$values?></li>
				<?php	
				}
			?>
			</ul>
			</div>
			</div>
			<?php  }
			if((!empty($kAdmin->arErrorMessages) && ($_POST['mode']=='SAVE_DETAIL')) || $checkUpdateFlag || $_POST['mode']=='INACTIVATE_SERVICES')
			{	//$detail=$kAdmin->excangeRates($idAdmin,$id);
				echo "|||||";
				exchangeDetails($exchange);
			}
			if(empty($kAdmin->arErrorMessages) && (($_POST['mode']=='SAVE_DETAIL') ||($_POST['mode']=='DELETE_RATES') ) )
			{	
				echo "|||||";
				exchangeView();
				echo "|||||";
				$exchangeRates=$kAdmin->excangeRates($idAdmin);
				exchangeRatesView($exchangeRates);
			}
			if($_POST['mode']=='CANCEL_DETAILS')
			{
				exchangeView();
				echo "|||||";
				$exchangeRates=$kAdmin->excangeRates($idAdmin);
				exchangeRatesView($exchangeRates);
			}
			
			/*if($_POST['mode']=='INACTIVATE_SERVICES')
			{
				//$detail=$kAdmin->excangeRates($idAdmin,$id);
				echo "|||||";
				exchangeDetails($exchange);
			}*/
?>