<?php 
define('PAGE_PERMISSION','__BOOKING_CANCELLED__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca - Bookings - Cancelled";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/operations/confirmation/";
validateManagement();
$kBooking = new cBooking;
$booking_searched_data = $kBooking->search_all_forwarder_bookings(__BOOKING_STATUS_CANCELLED__);
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
	
	<div class="hsbody-2-right">
	<div id="cancel_booking_popup">
	</div>
	<div id="cance_booking_html">
	<?php
		echo display_cancelled_booking_html($booking_searched_data,$t_base);
	?>
	</div>
	</div>
</div>
<input type="hidden" id="szBookingStatusFlag" name="szBookingStatusFlag" value="CancelBooking">
<?
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	