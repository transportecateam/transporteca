<?php 
/**
  *  GRAPH Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
define('PAGE_PERMISSION','__USER__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$addFile=1;
$szMetaTitle="Transporteca | Users - Details and Access";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/AllProfiles/";
$customerImage = $kAdmin ->selectTblGraphDetails(1);
$activeCustomer = $kAdmin ->selectTblGraphDetails(2);
$activeForwarder = $kAdmin ->selectTblGraphDetails(3);
$ctiveForwarderProfile = $kAdmin ->selectTblGraphDetails(4);

?>
    <div id="hsbody-2"> 
        <div id="content_body"> 
            <?php echo selectPageFormat();?>
            <div style="direction: ltr;" class="graph-oh">
                <div style="float: left;"> 	
                    <h4 style="padding-left:12px;"><b><?=t($t_base.'title/number_of_new_customer');?></b></h4>		
                </div>
                <div style="float: right;"> 	
                    <h4 style="padding-left:12px;"><b><?=t($t_base.'title/total_number_of_registered_customers');?></b></h4>		
                </div>
            </div>
            <div style="direction: ltr;" class="graph-oh">
                <div style="float: left;"> 	
                    <img src="<?=__MANAGEMENT_URL__."/".$customerImage."?id=".time();?>" width="360px">
                </div>
                <div style="float: right;"> 	
                    <img src="<?=__MANAGEMENT_URL__."/".$activeCustomer."?id=".time();?>" width="360px">
                </div>
            </div>
            <div style="clear: both;"></div>
            <br />
            <div style="direction: ltr;" class="graph-oh">
                <div style="float: left;">
                    <h4 style="padding-left:12px;"><b><?=t($t_base.'title/no_of_online');?></b></h4>		
                </div>
                <div style="float: right;"> 	
                    <h4 style="padding-left:12px;"><b><?=t($t_base.'title/no_of_registered_forwarder_profile');?></b></h4>		
                </div>
            </div>
            <div style="direction: ltr;" class="graph-oh">
                <div style="float: left;">
                    <img src="<?=__MANAGEMENT_URL__."/".$activeForwarder."?id=".time();?>" width="360px">
                </div>
                <div style="float: right;"> 	
                    <img src="<?=__MANAGEMENT_URL__."/".$ctiveForwarderProfile."?id=".time();?>" width="360px">
                </div>
            </div>
        </div>
    </div>
</div>
    <div id="confirmation_popup" style="display:none;"></div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>