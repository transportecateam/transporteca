<?php 
define('PAGE_PERMISSION','__FEEDBACK_DETAILS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Feedback - Details";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/operations/confirmation/";
validateManagement();
$kBooking = new cBooking;
$forwarderIdName=$kBooking->forwarderIdName($idAdmin);
$idForwarder=$forwarderIdName[0]['id'];

?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
	
	<div class="hsbody-2-right">
	<div style="text-align: center;padding-bottom: 20px;">
	<div style="float: left;width:45%;text-align: left;padding-left: 2px;">
            <h4><strong><?=t($t_base.'fields/select_forwarder')?></strong></h4>
	</div>
	<div style="float: left;">
		<select style="max-width: none;width: 275px;" onchange="viewNextOperationManagementDetail(this.value)">
				<?php
				if($forwarderIdName!=array())
				{
					foreach($forwarderIdName as $fwdName)
					{
						echo "<option value='".$fwdName['id']."'>".$fwdName['szDisplayName']."</option>";
					}
				}
				?>
		</select>
	</div>	
	<div style="clear: both;"></div>
	</div>	
		<div id ="showForwarderFeedback">
		<?
			operationFeedbackDetails($idForwarder);
		?>
		</div>
	</div>
</div>
</div>
<?
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	