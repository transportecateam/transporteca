<?php 
/**
  *  Admin--- createExplainPage
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kExplain = new cExplain();
$t_base="management/blog/";
$t_base_error="management/Error/";
if(isset($_POST['mode']) && $_POST['mode']=='CREATE_NEW_PAGE' && isset($_POST['id']))
{
	$_POST['blogArr'] = $_POST;
	//print_r($_POST['blogArr']['szContent']);
	if($kExplain->editBlogPage($_POST['blogArr']))
	{
		return true;	
	//header('location:'.__MANAGEMENT_EXPLAIN_URL__);
	}
	else if(!empty($kExplain->arErrorMessages))
	{
		?>
		<div id="regError" class="errorBox" style="display:block;">
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kExplain->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?
	}
}
else if(isset($_POST['mode']) && $_POST['mode']=='CREATE_NEW_PAGE')
{
	$_POST['blogArr'] = $_POST;
	//print_R($_POST['blogArr']['szContent']);DIE;
	if($kExplain->createBlogPage($_POST['blogArr']))
	{
		return true;	
	//header('location:'.__MANAGEMENT_EXPLAIN_URL__);
	}
	else if(!empty($kExplain->arErrorMessages))
	{
		?>
		<div id="regError" class="errorBox" style="display:block;">
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kExplain->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?
	}
}
if(isset($_POST['mode']))
{
	$mode = sanitize_all_html_input($_POST['mode']);
	SWITCH ($mode)
	{
		CASE 'CHANGE_ORDER_BLOG':
		{
			$id = (int)$_POST['id'];
			$order = sanitize_all_html_input($_POST['move']);
			if($order == 'UP')
			{
				$iOrderBy = __ORDER_MOVE_DOWN__;	
			}
			else if($order == 'DOWN')
			{
				$iOrderBy = __ORDER_MOVE_UP__;
			}
			$kExplain->moveUpDownBlogPagesLinks($id,$iOrderBy);
			$explainLinks = $kExplain->selectBlogDetails();
			$maxOrder = $kExplain->getMaxOrderBlogData();
			viewBlogPage($t_base,$explainLinks,$maxOrder);	
			BREAK;
		}
		CASE 'EDIT_BLOG_PAGES':
		{
			$id = (int)$_POST['id'];
			$data = $kExplain->selectBlogDetails($id);
			createNewBlogPage($t_base,$data[0]);
			BREAK;
		}
		CASE 'DELETE_BLOG_PAGE':
		{
			$id = (int)$_POST['id'];
			echo '<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
						'.t($t_base.'title/are_you_sure_to_delete').'<br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="deleteBlogPageConfirm(\''.$id.'\')" class="button1"><span>'.t($t_base.'fields/ok').'</span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span>'.t($t_base.'fields/cancel').'</span></a>			
						</p>
					</div>
			 	</div>
			</div> 	';
			BREAK;
		}
		CASE 'DELETE_BLOG_PAGE_CONFIRM':
		{	
			$id = (int)$_POST['id'];
			$kExplain->deleteBlogPage($id);
		}
	}

}
