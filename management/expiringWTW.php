<?php 
/**
  *EXPIRING WTW LIST
  */
define('PAGE_PERMISSION','__LCL_SERVICES_EXPIRING__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Pricing - Expiring";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$content = $kAdmin->expiringWTWAdmin();
$t_base = "management/operations/expirywtw/";
?>
<script type="text/javascript">
	$().ready(function() {	
	 	$("#datepicker1").datepicker();
	 	$("#datepicker2").datepicker();
	 });
	</script>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
	<div id="topTable">
		<? expiringWTW($content) ?>
	</div>
	<br /><br />
	<div id="Error-log"></div>
		<div class="oh">
			<p class="fl-40 gap-text-field" style="margin-top:20px;"><?=t($t_base.'fields/validity')?></p>
			<p class="fl-15">
				<span class="f-size-12" style="font-style:normal;"><?=t($t_base.'fields/from')?></span>
				<br>
				<input type="text" style="width: 100px;"  id="datepicker1" name = "expiringWTW[dtValidFrom]"  value="" size="10">
			</p>
			<p class="fl-15">
				<span class="f-size-12" style="font-style:normal;"><?=t($t_base.'fields/to')?></span>
				<br>
				<input type="text" style="width: 100px;" id="datepicker2"  name = "expiringWTW[dtValidTo]" value="" size="10">
			</p>
			<a id="editButton" class="button1" style="opacity:0.4;float: right; margin-top:10px;"><span><?=t($t_base.'fields/edit')?></span></a>
		</div>
	</div>
</div>

<?php  include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); ?>
