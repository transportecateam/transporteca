<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Setup Landing Pages ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement_ajax();
$kConfig = new cConfig();

$idLandingPage = sanitize_all_html_input(trim($_REQUEST['id']));
$kExplain = new cExplain();
$landingPageDataAry = array();

if($idLandingPage>0)
{
	$landingPageDataAry = $kExplain->getAllLandingPageData($idLandingPage);
	$form_id='addLandingPage';
	$mode = 'EDIT';
	$data = $landingPageDataAry[0];
}
else
{
	$mode = 'ADD';
	$form_id='addLandingPage';
}

//$site_url = remove_http_from_url(__MAIN_SITE_HOME_PAGE_SECURE_URL__);
$site_url = "www.transporteca.com";

 $kConfig =new cConfig();
 $langArr=$kConfig->getLanguageDetails('',$terms['iLanguage']);
?>
	<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<? $t_base="management/LandingPage/"; ?>	
	<div class="hsbody-2-right">
		<h4><b><?=t($t_base.'title/landing_page_setup');?></b></h4>
		<div id ="Error-log" class="errorBox" style="display:none;"></div>
		<script type="text/javascript">
			$(function(){
				checkDescriptionLength('szMetaDescription');
			});
			setup();
		</script>
		<form name="addLandingPage" id="<?=$form_id?>" action="<?=__BASE_URL__."/landingPagePreview/"?>" target="_blank" method="post" >
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/optimized_for');?></span>
				<span class="field-container"><input type="text" name="landingPageArr[szPageHeading]" value="<? if(isset($data['szSeoKeywords'])){echo $data['szSeoKeywords']; }?>"/></span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/url');?> (<?=$site_url?>/..)</span>
				<span class="field-container"><input type="text" name="landingPageArr[szLink]" value="<? if(isset($data['szUrl'])){echo $data['szUrl']; }?>"/></span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/page_title');?></span>
				<span class="field-container"><input type="text" name="landingPageArr[szMetaTitle]" value="<? if(isset($data['szMetaTitle'])){echo $data['szMetaTitle']; }?>"/></span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_keyword');?></span>
				<span class="field-container"><textarea rows="3" cols="25" name="landingPageArr[szMetaKeyword]"><? if(isset($data['szMetaKeywords'])){echo $data['szMetaKeywords']; }?></textarea></span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_description');?></span>
				
				<span class="field-container"><textarea rows="3" cols="25" onkeyup="checkDescriptionLength(this.id);" name="landingPageArr[szMetaDescription]" id="szMetaDescription"><? if(isset($data['szMetaDescription'])){echo $data['szMetaDescription']; }?></textarea></span>
				<span style="color: #7F7F7F;float: left;font-size: 12px;font-style: italic;margin: 63px 0 0 37px;text-align: right;width: 30px;">(<span id="char_left">160</span>)</span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/active_status');?></span>
				<span class="field-container">
					<select name="landingPageArr[iActive]">
						<option value = '0' <? if(isset($data['iPublished']) && $data['iPublished'] ==0){echo "selected='selected'"; } ?>>No</option>
						<option value = '1' <? if(isset($data['iPublished']) && $data['iPublished'] ==1){echo "selected='selected'"; } ?>>Yes</option>	
					</select>
				</span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/language');?></span>
				<span class="field-container">
					<select name="landingPageArr[iLanguage]" id="iLanguage">
                                            <?php
                                            if(!empty($langArr))
                                            {
                                                foreach($langArr as $langArrs)
                                                {?>
                                                    <option value = '<?php echo $langArrs['id']?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==$langArrs['id']){echo "selected='selected'"; } ?>><?php echo ucfirst($langArrs['szName']);?></option>
                                                   <?php 
                                                }
                                            }
                                            ?>
<!--						<option value = '<?php echo __LANGUAGE_ID_ENGLISH__?>' <? if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_ENGLISH__){echo "selected='selected'"; } ?>><?php echo ucfirst(__LANGUAGE_TEXT_ENGLISH__);?></option>
						<option value = '<?php echo __LANGUAGE_ID_DANISH__?>' <? if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_DANISH__){echo "selected='selected'"; } ?>><?php echo ucfirst(__LANGUAGE_TEXT_DANISH__);?></option>	-->
					</select>
				</span>
			</p>
			
			<!-- This div contains middle part of form -->
			 <div id="first_div" class="clearfix">
			 	<div class="left-fields">
					<p class="profile-fields">
					 <span class="field-name">A</span>
					 <span class="field-container"><textarea rows="1" style="height:21px;width:221px;" cols="25" name="landingPageArr[szTextA]" ><? if(isset($data['szTextA'])){echo $data['szTextA']; }?></textarea></span>
					</p>
					<p class="profile-fields">
						 <span class="field-name">B</span>
					 	<span class="field-container"><textarea rows="1" style="height:21px;width:221px;" cols="25" name="landingPageArr[szTextB]"><? if(isset($data['szTextB'])){echo $data['szTextB']; }?></textarea></span>
					</p>
					<p class="profile-fields">
						<span class="field-name">C</span>
					 	<span class="field-container"><textarea rows="1" style="height:21px;width:221px;" cols="25" name="landingPageArr[szTextC]" ><? if(isset($data['szTextC'])){echo $data['szTextC']; }?></textarea></span>
					</p>
				</div>
				<div class="right-fields">
					<p class="profile-fields" >
					 <span class="field-name">E</span>
					 <span class="field-container"><textarea cols="25" rows="1" name="landingPageArr[szTextE]" ><? if(isset($data['szTextE'])){echo $data['szTextE']; }?></textarea></span>
					</p>
					<p class="profile-fields">
						<span class="field-name">F</span>
					 	<span class="field-container"><textarea rows="2" cols="25" name="landingPageArr[szTextF]"><? if(isset($data['szTextF'])){echo $data['szTextF']; }?></textarea></span>
					</p>
				</div>
			</div>
			
			<!-- This div contains image on left and text element on right-->
			<div id="middle_div" class="clearfix">
				<div style="float: left;width:62%;">
					<!-- preview will be shown in this div -->
					<div id="preview_landing_page">
						<img alt="Landing Page Image" src="<?php echo __BASE_STORE_IMAGE_URL__."/LandingPage.png"?>">
					</div>	
				</div>
				<div style="float: right;width:33%">
					<p class="profile-fields">
						 <span class="field-name">G</span>
					 	<span class="field-container"><textarea style="height:21px;width:215px;" name="landingPageArr[szTextG]" ><? if(isset($data['szTextG'])){echo $data['szTextG']; }?></textarea></span>
					</p>
					<p class="profile-fields">
						 <span class="field-name">H</span>
					 	<span class="field-container"><textarea style="height:21px;width:215px;" name="landingPageArr[szTextH]" ><? if(isset($data['szTextH'])){echo $data['szTextH']; }?></textarea></span>
					</p>
					<p class="profile-fields">
						 <span class="field-name">I</span>
					 	<span class="field-container">
					 	<textarea rows="2" cols="20" name="landingPageArr[szTextI]"><? if(isset($data['szTextI'])){echo $data['szTextI']; }?></textarea>
					</p>
					<p class="profile-fields">
						<span class="field-name">J</span>
					 	<span class="field-container">
					 	<textarea name="landingPageArr[szTextJ]" style="height:21px;width:215px;" ><? if(isset($data['szTextJ'])){echo $data['szTextJ']; }?></textarea></span>
					</p>
					<p class="profile-fields">
						<span class="field-name">K</span>
					 	<span class="field-container">
					 		<textarea rows="2" cols="25" name="landingPageArr[szTextK]"><? if(isset($data['szTextK'])){echo $data['szTextK']; }?></textarea>
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name">L</span>
					 	<span class="field-container">
					 	<textarea name="landingPageArr[szTextL]" style="height:21px;width:215px;" ><? if(isset($data['szTextL'])){echo $data['szTextL']; }?></textarea></span>
					</p>
					<p class="profile-fields">
						<span class="field-name">M</span>
					 	<span class="field-container"><textarea rows="3" cols="25" name="landingPageArr[szTextM]"><? if(isset($data['szTextM'])){echo $data['szTextM']; }?></textarea></span>
					</p>
				</div>		
			</div>
		
			<!-- This div contains tiny on left and buttons on right-->
			<div id="last_div" class="clearfix">
				<div style="float:left;width:631px;">
					<textarea name="landingPageArr[szTextX]" id="content" class="myTextEditor" style="width:30%;height:60%;"><?php echo $data['szTextD'];?></textarea>
				</div>
				<div style="text-align: center;float:right;width:92px;margin-top:303px;">
					<input type="hidden" name="mode" id="mode" value="<?=$mode?>">
					<input type="hidden" name="landingPageArr[idLandingPage]" id="idLandingPage" value="<?=$idLandingPage?>">
					<a class="button1" id="cancelAddingPage" onclick="previewLandingPage('<?=$form_id?>')"><span><?=t($t_base.'fields/preview');?></span></a><br />
					<a class="button1" id="addNewPage" onclick="addNewLandingPage('<?=$form_id?>');"><span><? if(isset($data['id'])){echo t($t_base.'fields/save');}else{echo t($t_base.'fields/add_1');}?></span></a><br />
					<a class="button1" id="cancelAddingPage" onclick="redirect_url('<?=__MANAGEMENT_LANDING_PAGE_EDITOR_URL__?>')"><span><?=t($t_base.'fields/cancel');?></span></a><br />
				</div>
			</div>
		</form>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>