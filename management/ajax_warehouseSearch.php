<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base="management/cfsLoctation/";
$t_base_error="management/Error/";
if(isset($_POST))
{
	$kWHSSearch			= new cWHSSearch;
	
	if(isset($_POST['cfsAddEditArr']))
	{
		//print_r($_POST['cfsAddEditArr']);
		$warehouseDetailArr = $kWHSSearch->saveCFS($_POST['cfsAddEditArr']);
		
		if(!empty($_POST['cfsAddEditArr']['szPhoneNumberUpdate']))
		{
                    $_POST['cfsAddEditArr']['szPhoneNumber'] = urldecode(base64_decode($_POST['cfsAddEditArr']['szPhoneNumberUpdate']));
		}
		if(!empty($kWHSSearch->arErrorMessages))
		{
		?>
		<div id="regError" class="errorBox" style="display:block;">
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kWHSSearch->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?php
		}
	}
	if(isset($_POST['mode']) && $_POST['mode'] == 'SHOW_CFS_LISTING')
	{
            $kWHSSearch = new cWHSSearch;
            $iWarehouseType = $_POST['currentHaulage']['iWarehouseType'];
            $warehouseDetailArr = $kWHSSearch->getWareHouseDetails(false,$_POST['currentHaulage']);
            showCfsListing($t_base,$warehouseDetailArr,$idAdmin,$iWarehouseType);
            echo "|||||";
            addEditWarehouse($t_base,$idAdmin,$iWarehouseType); 
	}
	if(isset($_POST['flag']))
	{	$flag	=	sanitize_all_html_input($_POST['flag']);
		if($flag == 'EDIT_fORWARDER_CFS')
		{
			$id	=	(int)sanitize_all_html_input($_POST['id']);
			$warehouseDetailOBO	=	$kWHSSearch->editSearchCFS($id);
			print_r($warehouseDetailOBO);
			//echo "|||||";
			?>
			
			 <?
		}
		if($flag =='DELETE_fORWARDER_CFS' )
		{
			$id	=	(int)sanitize_all_html_input($_POST['id']);
			echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'fields/remove_CFS').'</b></h5>
							<p>'.t($t_base.'fields/are_you_sure').'</p>
							<a class="button1" onclick="removeCFS(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
						</div>
					</div>';
		}
		if($flag == 'DELETE_fORWARDER_CFS_CONFIRM')
		{
			$id	=	(int)sanitize_all_html_input($_POST['id']);
			$kWHSSearch->deleteCFS($id);
		}
	}
	if(isset($_POST['mode']) &&  $_POST['mode'] == 'SHOW_MULTIPLE_COUNTRIES_SELECT')
	{
		$kConfig = new cConfig();
		$kWHSSearch = new cWHSSearch;
		$allCountriesArr=$kConfig->getAllCountries(true);
		$idCountry = (int)sanitize_all_html_input($_POST['idCountry']);
		$kWHSSearch->set_idCountry($idCountry);
		$countryList = $kWHSSearch->findWarehouseCountryList($kWHSSearch->idCountry,'','');?>
	    	<div id="selectForCountrySelection">
	    	 <? //$selectListCFS = array(1,2,3);
	    	 	if($kWHSSearch->idCountry!= NULL)
	    	 	{
	    	 		$selectListCFS = array($kWHSSearch->idCountry);
	    	 	}
	    	 ?>
	    	 <br />
	    	  <h4><b><?=t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_local_market');?></p>
	    	 <br />
	    	<div style="direction: ltr;" class="profile-fields" >
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td valign="top" width="215"><?=t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			<?php
			if(!empty($allCountriesArr))
			{
				foreach($allCountriesArr as $allCountriesArrs)
				{
					if(in_array($allCountriesArrs['id'],$selectListCFS))
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			}
		?>	
	    	 </select>
	    	  <br />
	    	   <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	  </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?=t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>	
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 <?
	    	 	if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."' >".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	    	 ?>
	    	 </select>
	    	 <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	  	   		<div style="margin-top: 14px;float:left">
	    	 	<div class="field-alert" id="example" style="display:none;">
	    	 		<div>
	    	 		<p><?=t($t_base.'messages/for_your_rotterdam');?></p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'messages/for_your_hong_kong');?></p>
	    	 		</div>
	    	 	</div>
	    	 	</div>
	    	 </div>
		
	    
		
		<input id="CFSListing" type="hidden" value="<? if($id !=NULL){echo implode(',',$selectListCFS);}?>" name="cfsAddEditArr[selectNearCFS]">
	
	    	 <?php //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>
<?php
	}
	if(isset($_POST['flag']) &&  $_POST['flag'] == 'SHOW_COUNTRIES_DETAILS')
	{
		$latitude = (float)sanitize_all_html_input($_POST['latitude']);
		$longitude = (float)sanitize_all_html_input($_POST['longitude']);
		$id = (int)sanitize_all_html_input($_POST['id']);
		$idCountry = (int)sanitize_all_html_input($_POST['idCountry']);
		$kConfig = new cConfig;
		$kWHSSearch = new cWHSSearch;
		$allCountriesArr=$kConfig->getAllCountries(true);
		//$idCountry = ;
		//$kWHSSearch->set_idCountry($idCountry);
		$countryList = $kWHSSearch->findWarehouseCountryList($idCountry,$latitude,$longitude);?>
	    	<div id="selectForCountrySelection">
	    	 <? //$selectListCFS = array(1,2,3);
				if($id)
				{
					$selectListCFS  = $kWHSSearch->selectWarehouseCountries($id);
					 
				}
	    	 	elseif($idCountry!= NULL)
	    	 	{
	    	 		$selectListCFS = array($idCountry);
	    	 	}
	    	 ?>
	    	 <br/>
	    	 <h4><b><?=t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_local_market');?></p>
	    	 <br/>
	    	<div style="direction: ltr;clear: both;" class="profile-fields" >
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td valign="top" width="215" style="padding-top:5px;"><?=t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			<?php
			if(!empty($allCountriesArr))
			{
				foreach($allCountriesArr as $allCountriesArrs)
				{
					if(in_array($allCountriesArrs['id'],$selectListCFS))
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			}
		?>	
	    	 </select>
	    	  <br />
	    	   <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$idCountry?>','<?=$latitude?>','<?=$longitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	  </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?=t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>		    	
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 <?
	    	 	if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."' >".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	    	 ?>
	    	 </select>
	    	  <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	  	   <div style="margin-top: 14px;float:left">
	    	 	<div class="field-alert" id="example" style="display:none;">
	    	 		<div>
	    	 		<p><?=t($t_base.'messages/for_your_rotterdam');?></p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'messages/for_your_hong_kong');?></p>
	    	 		</div>
	    	 	</div>
	    	 </div>
		   </div>
	    
	
	<input id="CFSListing" type="hidden" value="<? if($id !=NULL){echo implode(',',$selectListCFS);}?>" name="cfsAddEditArr[selectNearCFS]">
		
	    	 <?php //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>
	    <?php
	}
	if(isset($_POST['mode']) &&  $_POST['mode'] == 'SELECT_COUNTRIES_FORM_LIST')
	{
		$selectListCFS = array();
		$selectListCFS = sanitize_all_html_input($_POST['content']);
		$selectListCFS = explode(',',$selectListCFS);
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries(true);
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								if(in_array($allCountriesArrs['id'],$selectListCFS))
								{
									?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						}
						
	
	}
	if(isset($_POST['mode']) &&  $_POST['mode'] == 'REMOVE_COUNTRIES_FROM_LIST')
	{
		$kConfig = new cConfig;
		$kWHSSearch = new cWHSSearch;
		$selectListCFS = array();
		$selectListCFS = sanitize_all_html_input($_POST['content']);
		$selectListCFS = explode(',',$selectListCFS);
		$country   = (int)$_POST['country'];
		$latitude  = (int)$_POST['latitude'];
		$longitude = (int)$_POST['longitude'];
		$countryList = $kWHSSearch->findWarehouseCountryList($country,$latitude,$longitude);
		if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."'>".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	}
}
?>