<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base = "ForwardersCompany/Preferences/";
//checkAuthForwarder_ajax();
$kForwarderContact = new cForwarderContact();
//constants for hardcoded data for preferences
	//$bookingReports=array('Daily','Weekly','Monthy','Never');
	//$paymentReports=array('Weekly','Monthly');
	//$pricngUpdates=array('No','Yes');
	$arrBook=BookingReportsArr();
	$bookingReports	= $arrBook[0];
	$paymentReports	= $arrBook[1];
	$pricngUpdates	= $arrBook[2];
	$forwarderControlPanelArr=$kForwarderContact->getSystemPanelDetails($idForwarder);	
	$t_base_error = "Error";
	$forwarderCompanyAry = array();
	$operation_mode = 'EDIT_SYSTEM_CONTROL';
	
if(!empty($_REQUEST['mode']))
{
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
	$idForwarder    = sanitize_all_html_input(trim($_REQUEST['id']));
}

if(!empty($_REQUEST['status'])=='REMOVE_EMAIL_ADDRESS' &&!empty($_REQUEST['idContact']))
{   
	$idContact = sanitize_all_html_input(trim($_REQUEST['idContact']));		
}
if(!empty($_POST['updatePrefEmailAry']) && !empty($idForwarder))
{ 	
	$updateMailer=$kForwarderContact->updateMailingAddress($_POST['updatePrefEmailAry'],$idForwarder);
	
	if(isset($_POST['removeEmails']) && $updateMailer)
	{
		$id=explode('_',$_POST['removeEmails']);
		
		if(count($id)>1)
		{
			foreach($id as $key=>$value) 
			$kForwarderContact->removeMailingAddresses($value);
		}	
	}
	if($updateMailer)
	{
		$operation_mode='DISPLAY_EMAIL_ADDRESS';
	}
}
if(!empty($_POST['updatePrefControlArr']) && !empty($idForwarder) && $operation_mode == 'EDIT_SYSTEM_CONTROL')
{	 
    $idOldCurrency = (int)sanitize_all_html_input(trim($_POST['updatePrefControlArr']['szCurrency_hidden']));
    $idNewCurrency = (int)sanitize_all_html_input(trim($_POST['updatePrefControlArr']['szCurrency']));
    $iConfirmFlag = (int)sanitize_all_html_input(trim($_REQUEST['confirm_flag']));
    
    //echo "new: ".$idNewCurrency." old: ".$idOldCurrency ;
    if(($idOldCurrency==0) || $idNewCurrency==$idOldCurrency || $iConfirmFlag==1)
    { 
        $updateController=$kForwarderContact->updatePreferencesCotrol($_POST['updatePrefControlArr'],$idForwarder);
        if($updateController)
        {	
            echo "SUCCESS|||||";
            $operation_mode='VIEW_SYSTEM_CONTROL';
        }
    }
    else
    { 
        $kWhsSearch = new cWHSSearch(); 
        if($idOldCurrency =='1')
        {
            $oldCurrency ='USD';
        }
        else
        {
            $oldCurrency = $kWhsSearch->getCurrencyDetails($idOldCurrency);
            $oldCurrency = $oldCurrency['szCurrency'];
        }
        if($idNewCurrency =='1')
        {
            $newCurrency ='USD';
        }
        else
        {
            $newCurrency = $kWhsSearch->getCurrencyDetails($idNewCurrency);
            $newCurrency = $newCurrency['szCurrency'];
        }
        echo "CONFIRM|||||";
        echo forwarder_currency_change_notifivcation($idForwarder,$t_base,$newCurrency,$oldCurrency);
        die;
    }
}
if($operation_mode=== 'SAVE_SYSTEM_CONTROL')
{
$updatePrefControlArr =array();
$updatePrefControlArr['szCurrency'] = sanitize_all_html_input($_POST['szCurrency']);
$updatePrefControlArr['szPayment'] = sanitize_all_html_input($_POST['szPayment']);
$updatePrefControlArr['szRss'] = sanitize_all_html_input($_POST['szRss']);
$szOldCurrency = sanitize_all_html_input($_POST['szOldCurrency']);
$updateController=$kForwarderContact->updatePreferencesCotrol($updatePrefControlArr,$idForwarder);
$kForwarder->addCurrencyChangeLogs($szOldCurrency,$updatePrefControlArr['szCurrency'],$idForwarder);
if($updateController)
{
	$operation_mode='VIEW_SYSTEM_CONTROL';
}
}
forwarderCompaniesEdit($idForwarder,'preferences');
echo "<div class='hsbody-2-right'>";
if($operation_mode=='EDIT_EMAIL_ADDRESS')
{ 	
    $forwarderContactAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder);
    $szBookingEmailAry = $forwarderContactAry['szBookingEmail'];
    $szPaymentEmailAry = $forwarderContactAry['szPaymentEmail'];
    $szCustomerServiceEmailAry = $forwarderContactAry['szCustomerServiceEmail'];

    $szAirBookingEmailAry = $forwarderContactAry['szAirBookingEmail'] ;
    $szRoadBookingEmailAry = $forwarderContactAry['szRoadBookingEmail'] ;
    $szCourierBookingEmailAry = $forwarderContactAry['szCourierBookingEmail'];
	?>
	
	<div id="ajax_edit_mail_control">
	<?php
	if(!empty($kForwarderContact->arErrorMessages)){
		?>
		<div id="regError" class="errorBox">
		<div class="header"><?=t($t_base_error.'/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kForwarderContact->arErrorMessages as $key=>$values)
			{
			?><li><?=$key." ".$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<?php } ?>
	<h4><strong><?=t($t_base.'titles/email_address');?></strong></h4>
	
	<form id="ajax_preferences_email" method="post">
        <?php	
	if(!empty($szBookingEmailAry) || !empty($_POST['updatePrefEmailAry']))
	{ 
		$i=1;
	?>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'titles/bookings');?></span>
			<span class="field-container" id="forwarder_preferences_edit_booking" >
		<div id="forwarder_preferences_booking">
	<?php 	
		if(isset($_POST['updatePrefEmailAry']['szEmailBooking']))
		{
                    $ij=$_POST['ajax_preferences_booking']-$_POST['validateBooking']-1; 
		}
		else
		{
                    $ij=count($szBookingEmailAry);
		}
		for($count=0;$ij>$count;$count++)
		{ 
			?>
				<span class="profile-fields" id="bookings<?=$i;?>">
					
					<input type="text" name="updatePrefEmailAry[szEmailBooking][<?=$i;?>]" id="szEmailBooking_<?=$i;?>" value="<?=isset($_POST['updatePrefEmailAry']['szEmailBooking'][$i])?$_POST['updatePrefEmailAry']['szEmailBooking'][$i]:$szBookingEmailAry[$count]['szEmail'];?>" onblur="closeTip('book<?=$i?>');" onfocus="openTip('book<?=$i?>');">
					<input type="hidden" name="updatePrefEmailAry[bookingId][<?=$i;?>]" value="<?=isset($szBookingEmailAry)?$szBookingEmailAry[$count]['id']:'';?>"> 	
					<div class="field-alert"><div id="book<?=$i?>" style="display:none;"><?=t($t_base.'messages/booking');?></div></div>
				</span>
				
					
			<?php  
			$i++;
		}	 
                $count=0;
		if(isset($_POST['validateBooking']))
		{	$book=(int)$_POST['validateBooking'];
			if($book>0)
			{ 
				for($count;$book>$count;$count++)
				{	?>
					<div id="bookings<?=$i+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailBooking][<?=$i+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailBooking'][$i+$count]?>" onblur="closeTip('book<?=$i+$count?>')" onfocus="openTip('book<?=$i+$count?>')">
					<div class='field-alert'><div id='book<?=$i+$count?>' style='display:none;'><?=t($t_base.'messages/booking')?></div></div></div>
			<?php	}
				
			}
		} $display=displayStyle($i+$count);
		?>	
		<input type="hidden" name="validateBooking" id="validateBooking" value="<?=isset($_POST['validateBooking'])?$_POST['validateBooking']:0 ?>"> 
					
		</div><input type="hidden" name="ajax_preferences_booking" id="ajax_preferences_booking" value="<?=isset($_POST['validateBooking'])?($_POST['validateBooking']+$i):$i?>">
		<span class="oh" style="float: right; margin-right: -3px; text-align: right; width: 100px;">
		<?php /* <a class="fl-50"  href="javascript:void(0);" id="remove_forwarder_profile_booking" style="display:<?=$display;?>;" onclick="remove_forwarder_profile('ajax_preferences_booking','bookings','remove_forwarder_profile_booking','<?=$szBookingEmailArys['id']?>','<?=$idForwarder?>','validateBooking');"><?=t($t_base.'fields/remove_this_email');?></a>&nbsp;
			*/ ?>
		<a href="javascript:void(0);" onclick="add_forwarder_profile('booking','Booking','validateBooking');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
				</span></div>
		<?php
	} 
	if(empty($szBookingEmailAry) && empty($_POST['updatePrefEmailAry']))
	{ 
		$i=1;
	?>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'titles/bookings');?></span>
			
		
		<div class="field-container" id="forwarder_preferences_edit_booking" >
		<div id="forwarder_preferences_booking">
		<span class="profile-fields" id="bookings<?=$i;?>">			
					<input type="text" name="updatePrefEmailAry[szEmailBooking][<?=$i;?>]" id="szEmailBooking_<?=$i;?>" value="<?=isset($_POST['updatePrefEmailAry']['szEmailBooking'][$i])?$_POST['updatePrefEmailAry']['szEmailBooking'][$i]:'';?>" onblur="closeTip('book<?=$i?>');" onfocus="openTip('book<?=$i?>');">
					<input type="hidden" name="updatePrefEmailAry[bookingId][<?=$i;?>]" value="<?=isset($szBookingEmailAry)?$szBookingEmailAry[$count]['id']:'';?>"> 	
					<div class="field-alert"><div id="book<?=$i?>" style="display:none;"><?=t($t_base.'messages/booking');?></div></div>
				</span>
				
					
			<?  
			$i++;
		
		?>
	<?php 	$count=0;
		if(isset($_POST['validateBooking'])) 
		{	$book=(int)$_POST['validateBooking'];
			if($book>0)
			{ //echo "hello";
				for($count;$book>$count;$count++)
				{	?>
					<div id="bookings<?=$i+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailBooking][<?=$i+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailBooking'][$i+$count]?>" onblur="closeTip('book<?=$i+$count?>')" onfocus="openTip('book<?=$i+$count?>')">
					<div class='field-alert'><div id='book<?=$i+$count?>' style='display:none;'><?=t($t_base.'messages/booking')?></div></div></div>
			<?	}
				
			}
		} $display=displayStyle($i+$count);
		?>	
		<input type="hidden" name="validateBooking" id="validateBooking" value="<?=isset($_POST['validateBooking'])?$_POST['validateBooking']:0 ?>"> 
					
		</span>
		
				</div>
				<input type="hidden" name="ajax_preferences_booking" id="ajax_preferences_booking" value="<?=isset($_POST['validateBooking'])?($_POST['validateBooking']+$i):$i?>">
		<span class="oh" style="float: right; margin-right: -3px; text-align: right; width: 100px;">
		<?php /*<a class="fl-50"  href="javascript:void(0);" id="remove_forwarder_profile_booking" style="display:<?=$display;?>;" onclick="remove_forwarder_profile('ajax_preferences_booking','bookings','remove_forwarder_profile_booking','<?=$szBookingEmailArys['id']?>','<?=$idForwarder?>','validateBooking');"><?=t($t_base.'fields/remove_this_email');?></a>&nbsp;
		*/ ?>
         <a href="javascript:void(0);" onclick="add_forwarder_profile('booking','Booking','validateBooking');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
				</div></div>
		<?
	} 
?>

    <div style="clear: both;"></div>
    <div class="profile-fields">
        <span class="field-name"><?php echo t($t_base.'titles/air')." ".t($t_base.'titles/bookings');?></span>
        <span class="field-container" id="forwarder_preferences_edit_air_booking" >
        <div id="forwarder_preferences_air_booking">
     <?php
        if(!empty($szAirBookingEmailAry) || !empty($_POST['updatePrefEmailAry']))
        {
            $i=1;
            if(isset($_POST['updatePrefEmailAry']['szEmailAirBooking']))
            {
                $loop_counter = $_POST['ajax_preferences_air_booking'] - $_POST['validateAirBooking']; 
            }
            else
            {
                $loop_counter = count($szAirBookingEmailAry);
            }
            if($loop_counter<=0)
            {
                $loop_counter = 1;
            }
            for($count=0;$count<$loop_counter;$count++)
            {  
                $i = $count+1;
                echo display_air_booking_preferences($i,$szAirBookingEmailAry[$count]);
            } 
        } 
        else
        {
            $i = 1; 
            echo display_air_booking_preferences($i);
        }  
        $count=1;
        if(isset($_POST['validateAirBooking']))
        {	
            $book=(int)$_POST['validateAirBooking'];
            if($book>0)
            { 
                for($count=1;$book>$count;$count++)
                {	 
            ?>
                    <div id="air_bookings<?=$i+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailAirBooking][<?=$i+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailAirBooking'][$i+$count]?>" onblur="closeTip('air_book<?=$i+$count?>')" onfocus="openTip('air_book<?=$i+$count?>')">
                    <div class='field-alert'><div id='air_book<?=$i+$count?>' style='display:none;'><?=t($t_base.'messages/air_booking')?></div></div></div>
    <?php       }
            }
        } 
    ?>
        <input type="hidden" name="validateAirBooking" id="validateAirBooking" value="<?=isset($_POST['validateAirBooking'])?$_POST['validateAirBooking']:0 ?>"> 
        </div>
            <input type="hidden" name="ajax_preferences_air_booking" id="ajax_preferences_air_booking" value="<?=isset($_POST['validateAirBooking'])?($_POST['validateAirBooking']+$i):($i+1)?>">
            <span class="oh"> 
                <a class="fr-70" href="javascript:void(0);" onclick="add_forwarder_profile('air_booking','AirBooking','validateAirBooking');"><?=t($t_base.'fields/add_one_more');?></a>
            </span>
        </span>
    </div>
    <div style="clear: both;"></div>
    <div class="profile-fields">
        <span class="field-name"><?php echo t($t_base.'titles/road')." ".t($t_base.'titles/bookings');?></span>
        <span class="field-container" id="forwarder_preferences_edit_road_booking" >
        <div id="forwarder_preferences_road_booking">
     <?php
        if(!empty($szRoadBookingEmailAry) || !empty($_POST['updatePrefEmailAry']))
        {
            $i=1;
            if(isset($_POST['updatePrefEmailAry']['szEmailRoadBooking']))
            {
                $loop_counter = $_POST['ajax_preferences_road_booking'] - $_POST['validateRoadBooking']; 
            }
            else
            {
                $loop_counter = count($szRoadBookingEmailAry);
            }
            if($loop_counter<=0)
            {
                $loop_counter = 1;
            }
            for($count=0;$count<$loop_counter;$count++)
            {  
                $i = $count+1;
                echo display_road_booking_preferences($i,$szRoadBookingEmailAry[$count]);
            } 
        } 
        else
        {
            $i = 1; 
            echo display_road_booking_preferences($i);
        }  
        $count=0;
        if(isset($_POST['validateRoadBooking']))
        {	
            $book=(int)$_POST['validateRoadBooking'];
            if($book>0)
            { 
                for($count;$book>$count;$count++)
                {	 
            ?>
                    <div id="air_bookings<?=$i+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailRoadBooking][<?=$i+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailRoadBooking'][$i+$count]?>" onblur="closeTip('road_book<?=$i+$count?>')" onfocus="openTip('road_book<?=$i+$count?>')">
                    <div class='field-alert'><div id='road_book<?=$i+$count?>' style='display:none;'><?=t($t_base.'messages/road_booking')?></div></div></div>
    <?php       }
            }
        } 
    ?>
        <input type="hidden" name="validateRoadBooking" id="validateAirBooking" value="<?=isset($_POST['validateRoadBooking'])?$_POST['validateRoadBooking']:0 ?>"> 
        </div>
            <input type="hidden" name="ajax_preferences_road_booking" id="ajax_preferences_road_booking" value="<?=isset($_POST['validateRoadBooking'])?($_POST['validateRoadBooking']+$i):($i+1)?>">
            <span class="oh"> 
                <a class="fr-70" href="javascript:void(0);" onclick="add_forwarder_profile('road_booking','RoadBooking','validateRoadBooking');"><?=t($t_base.'fields/add_one_more');?></a>
            </span>
        </span>
    </div> 
<div style="clear: both;"></div>
<div class="profile-fields">
        <span class="field-name"><?php echo t($t_base.'titles/courier')." ".t($t_base.'titles/bookings');?></span>
        <span class="field-container" id="forwarder_preferences_edit_courier_booking" >
        <div id="forwarder_preferences_courier_booking">
     <?php
        if(!empty($szCourierBookingEmailAry) || !empty($_POST['updatePrefEmailAry']))
        {
            $i=1;
            if(isset($_POST['updatePrefEmailAry']['szEmailCourierBooking']))
            {
                $loop_counter = $_POST['ajax_preferences_road_booking'] - $_POST['validateCourierBooking']; 
            }
            else
            {
                $loop_counter = count($szCourierBookingEmailAry);
            }
            if($loop_counter<=0)
            {
                $loop_counter = 1;
            }
            for($count=0;$count<$loop_counter;$count++)
            {  
                $i = $count+1;
                echo display_courier_booking_preferences($i,$szCourierBookingEmailAry[$count]);
            } 
        } 
        else
        {
            $i = 1; 
            echo display_courier_booking_preferences($i);
        }  
        $count=0;
        if(isset($_POST['validateCourierBooking']))
        {	
            $book=(int)$_POST['validateCourierBooking'];
            if($book>0)
            { 
                for($count;$book>$count;$count++)
                {	 
            ?>
                    <div id="air_bookings<?=$i+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailCourierBooking][<?=$i+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailCourierBooking'][$i+$count]?>" onblur="closeTip('courier_book<?=$i+$count?>')" onfocus="openTip('courier_book<?=$i+$count?>')">
                    <div class='field-alert'><div id='courier_book<?=$i+$count?>' style='display:none;'><?=t($t_base.'messages/courier_booking')?></div></div></div>
    <?php       }
            }
        } 
    ?>
        <input type="hidden" name="validateCourierBooking" id="validateAirBooking" value="<?=isset($_POST['validateCourierBooking'])?$_POST['validateCourierBooking']:0 ?>"> 
        </div>
            <input type="hidden" name="ajax_preferences_courier_booking" id="ajax_preferences_courier_booking" value="<?=isset($_POST['validateCourierBooking'])?($_POST['validateCourierBooking']+$i):($i+1); ?>">
            <span class="oh"> 
                <a class="fr-70" href="javascript:void(0);" onclick="add_forwarder_profile('courier_booking','CourierBooking','validateCourierBooking');"><?=t($t_base.'fields/add_one_more');?></a>
            </span>
        </span>
    </div>
<div style="clear: both;"></div>
<div style="clear: both;"></div>
<?php
	if(!empty($szPaymentEmailAry) || !empty($_POST['updatePrefEmailAry']))
	{
		$j=1;
	?>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'titles/Payments_billing');?></span>
			
		
		<div class="field-container" id="forwarder_preferences_edit_payment">
		<div id="forwarder_preferences_payment">
	<?	if(isset($_POST['updatePrefEmailAry']['szEmailPayment']))
		{
		$ij=$_POST['ajax_preferences_payment']-$_POST['validatePayment']-1;
		
		}
		else
		{
		$ij=count($szPaymentEmailAry);
		}
	
		for($count=0;$ij>$count;$count++)
	
		{
			?>
				<div class="profile-fields" id="payments<?=$j;?>">
					<input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailPayment'][$j])?$_POST['updatePrefEmailAry']['szEmailPayment'][$j]:$szPaymentEmailAry[$count]['szEmail']?>" onblur="closeTip('pay<?=$j?>');" onfocus="openTip('pay<?=$j?>');">
					<input type="hidden" name="updatePrefEmailAry[paymentId][<?=$j;?>]" value="<?=$szPaymentEmailAry[$count]['id']?>"> 
					<div class="field-alert"><div id="pay<?=$j?>" style="display:none;"><?=t($t_base.'messages/payment');?></div></div>
			
				</div>
				<? 
			$j++;
		} 
			
		?>
		<?  $count=0;
		if(isset($_POST['validatePayment']))
		{	$pay=(int)$_POST['validatePayment'];
			if($pay>0)
			{ 
				for($count;$pay>$count;$count++)
				{

				?>
					<div id="payments<?=$j+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailPayment'][$j+$count]?>" onblur="closeTip('pay<?=$j+$count?>')" onfocus="openTip('pay<?=$j+$count?>')">
					<div class='field-alert'><div id='pay<?=$j+$count?>' style='display:none;'><?=t($t_base.'messages/payment')?></div></div></div>
			<?	
				
				
				}
				
			}
		}$displayPayment=displayStyle($j+$count);
		?>
		<input type="hidden" name="validatePayment" id="validatePayment" value="<?=isset($_POST['validatePayment'])?$_POST['validatePayment']:0 ?>">
		</div>
		<input type="hidden" name="ajax_preferences_payment" id="ajax_preferences_payment" value="<?=isset($_POST['validatePayment'])?($_POST['validatePayment']+$j):$j?>">
		<span class="oh" style="float: right; margin-right: -3px; text-align: right; width: 100px;">
		<?php /*<a  class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_payment" style="display:<?=$displayPayment;?>;" onclick="remove_forwarder_profile('ajax_preferences_payment','payments','remove_forwarder_profile_payment','<?=$szPaymentEmailArys['id']?>','<?=$idForwarder?>','validatePayment');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a href="javascript:void(0);" onclick="add_forwarder_profile('payment','Payment','validatePayment');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
				</div></div>
		<?
	}
?>
		<? 
		if(empty($szPaymentEmailAry) && empty($_POST['updatePrefEmailAry']))
		{
		$j=1;
	?>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'titles/Payments_billing');?></span>
			
		
		<div class="field-container" id="forwarder_preferences_edit_payment">
		<div id="forwarder_preferences_payment">
	
				<div class="profile-fields" id="payments<?=$j;?>">
					<input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailPayment'][$j])?$_POST['updatePrefEmailAry']['szEmailPayment'][$j]:''?>" onblur="closeTip('pay<?=$j?>');" onfocus="openTip('pay<?=$j?>');">
					<input type="hidden" name="updatePrefEmailAry[paymentId][<?=$j;?>]" value="<?=$szPaymentEmailAry[$count]['id']?>"> 
					<div class="field-alert"><div id="pay<?=$j?>" style="display:none;"><?=t($t_base.'messages/payment');?></div></div>
			
				</div>
				<? 
			$j++;
	
			
		?>
		<?  $count=0;
		if(isset($_POST['validatePayment']))
		{	$pay=(int)$_POST['validatePayment'];
			if($pay>0)
			{ 
				for($count;$pay>$count;$count++)
				{

				?>
					<div id="payments<?=$j+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailPayment][<?=$j+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailPayment'][$j+$count]?>" onblur="closeTip('pay<?=$j+$count?>')" onfocus="openTip('pay<?=$j+$count?>')">
					<div class='field-alert'><div id='pay<?=$j+$count?>' style='display:none;'><?=t($t_base.'messages/payment')?></div></div></div>
			<?	
				
				
				}
				
			}
		}$displayPayment=displayStyle($j+$count);
		?>
		<input type="hidden" name="validatePayment" id="validatePayment" value="<?=isset($_POST['validatePayment'])?$_POST['validatePayment']:0 ?>">
		</div>
		<input type="hidden" name="ajax_preferences_payment" id="ajax_preferences_payment" value="<?=isset($_POST['validatePayment'])?($_POST['validatePayment']+$j):$j?>">
		<span class="oh" style="float: right; margin-right: -3px; text-align: right; width: 100px;">
		<?php /*<a  class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_payment" style="display:<?=$displayPayment;?>;" onclick="remove_forwarder_profile('ajax_preferences_payment','payments','remove_forwarder_profile_payment','<?=$szPaymentEmailArys['id']?>','<?=$idForwarder?>','validatePayment');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a href="javascript:void(0);" onclick="add_forwarder_profile('payment','Payment','validatePayment');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
				</div></div>
		<?
	}
		?>
<div style="clear: both;"></div>
<?
	if(!empty($szCustomerServiceEmailAry) || !empty($_POST['updatePrefEmailAry']))
	{ 
		$k=1;
	?>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'titles/Customer_service');?></span>
			
		
		<div class="field-container" id="forwarder_preferences_edit_customer">
		<div id="forwarder_preferences_customer">
	<?	
		if(isset($_POST['updatePrefEmailAry']['szEmailCustomer']))
		{
		$ij=$_POST['ajax_preferences_customer']-$_POST['validateCustomer']-1;
		
		}
		else
		{
		$ij=count($szCustomerServiceEmailAry);
		}
		
		for($count=0;$ij>$count;$count++)
		
		{
			?>
				<div class="profile-fields" id="customers<?=$k;?>">
					<input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailCustomer'][$k])?$_POST['updatePrefEmailAry']['szEmailCustomer'][$k]:$szCustomerServiceEmailAry[$count]['szEmail']?>"onblur="closeTip('cust<?=$k?>');" onfocus="openTip('cust<?=$k?>');">
					<input type="hidden" name="updatePrefEmailAry[customerId][<?=$k;?>]" value="<?=$szCustomerServiceEmailAry[$count]['id']?>"> 
					 <div class="field-alert"><div id="cust<?=$k?>" style="display:none;"><?=t($t_base.'messages/customer');?></div></div>
		
				</div>
					<? 
			$k++;
		} 
			
		?>
		<? $count=0;
		if(isset($_POST['validateCustomer']))
		{	$custo=(int)$_POST['validateCustomer'];
			if($custo>0)
			{ 
				for($count;$custo>$count;$count++)
				{	
				
				

				?>
					<div id="customers<?=$k+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailCustomer'][$k+$count]?>" onblur="closeTip('cust<?=$k+$count?>')" onfocus="openTip('cust<?=$k+$count?>')">
					<div class='field-alert'><div id='cust<?=$k+$count?>' style='display:none;'><?=t($t_base.'messages/customer')?></div></div></div>
			<?	
				
				}
				
			}
		}$displayCustomer=displayStyle($k+$count);
		?>
		<input type="hidden" name="validateCustomer" id="validateCustomer" value="<?=isset($_POST['validateCustomer'])?$_POST['validateCustomer']:0 ?>">
		</div><input type="hidden" name="ajax_preferences_customer" id="ajax_preferences_customer" value="<?=isset($_POST['validateCustomer'])?($_POST['validateCustomer']+$k):$k;?>">
		<span class="oh" style="float: right; margin-right: -3px; text-align: right; width: 100px;">
		<?php /*<a class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_customer" style="display:<?=$displayCustomer;?>;" onclick="remove_forwarder_profile('ajax_preferences_customer','customers','remove_forwarder_profile_customer','<?=$szCustomerServiceEmailArys['id']?>','<?=$idForwarder?>','validateCustomer');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a href="javascript:void(0);" onclick="add_forwarder_profile('customer','Customer','validateCustomer');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
				</div></div>
<?		
	}
	?>
 	<? if(empty($szCustomerServiceEmailAry) && empty($_POST['updatePrefEmailAry']) ){
		$k=1;
		
	?>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'titles/Customer_service');?></span>
			
		
		<div class="field-container" id="forwarder_preferences_edit_customer">
		<div id="forwarder_preferences_customer">
	
		<div class="profile-fields" id="customers<?=$k;?>">
		<input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k;?>]" value="<?=isset($_POST['updatePrefEmailAry']['szEmailCustomer'][$k])?$_POST['updatePrefEmailAry']['szEmailCustomer'][$k]:''?>"onblur="closeTip('cust<?=$k?>');" onfocus="openTip('cust<?=$k?>');">
		<input type="hidden" name="updatePrefEmailAry[customerId][<?=$k;?>]" value="<?=$szCustomerServiceEmailAry[$count]['id']?>"> 
		 <div class="field-alert"><div id="cust<?=$k?>" style="display:none;"><?=t($t_base.'messages/customer');?></div></div>

				</div>
		<?  	$k++;$count=0;
		if(isset($_POST['validateCustomer']))
		{	$custo=(int)$_POST['validateCustomer'];
			if($custo>0)
			{ 
				for($count;$custo>$count;$count++)
				{	
				
				

				?>
					<div id="customers<?=$k+$count?>" class="profile-fields" style="padding-top:0px;"><input type="text" name="updatePrefEmailAry[szEmailCustomer][<?=$k+$count?>]" value="<?=$_POST['updatePrefEmailAry']['szEmailCustomer'][$k+$count]?>" onblur="closeTip('cust<?=$k+$count?>')" onfocus="openTip('cust<?=$k+$count?>')">
					<div class='field-alert'><div id='cust<?=$k+$count?>' style='display:none;'><?=t($t_base.'messages/customer')?></div></div></div>
			<?	
				
	 			}
				
			}
		}$displayCustomer=displayStyle($k+$count);
	
		?>
		<input type="hidden" name="validateCustomer" id="validateCustomer" value="<?=isset($_POST['validateCustomer'])?$_POST['validateCustomer']:0 ?>">
		</div><input type="hidden" name="ajax_preferences_customer" id="ajax_preferences_customer" value="<?=isset($_POST['validateCustomer'])?($_POST['validateCustomer']+$k):$k;?>">
		<span class="oh" style="float: right; margin-right: -3px; text-align: right; width: 100px;">
		<?php /*<a class="fl-50" href="javascript:void(0);" id="remove_forwarder_profile_customer" style="display:<?=$displayCustomer;?>;" onclick="remove_forwarder_profile('ajax_preferences_customer','customers','remove_forwarder_profile_customer','<?=$szCustomerServiceEmailArys['id']?>','<?=$idForwarder?>','validateCustomer');"><?=t($t_base.'fields/remove_this_email');?></a>
			*/ ?>
		<a href="javascript:void(0);" onclick="add_forwarder_profile('customer','Customer','validateCustomer');"><?=t($t_base.'fields/add_one_more');?></a>
		</span>
				</div></div>
<?		
	}
	?>	
	<div style="clear: both;"></div>
	<div class="oh">
		<p class="fl-20">&nbsp;</p>
		<p class="fl-60"><br>
			<a href="javascript:void(0);" onclick="submit_preference_emails_details()"  class="button1"><span><?=t($t_base.'fields/save');?></span></a>
			<a href="javascript:void(0);" onclick="show_preference_emailss('<?=$idForwarder?>')"  class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
			<input type="hidden" name="mode" value="EDIT_EMAIL_ADDRESS">
			<input type="hidden" name="removeEmails" value="0" id="removeMails">
			<input type="hidden" name="id" value="<?=$idForwarder;?>">
		</p>
	</div></form>
	<?php
}
else
{	
	?>	<h4><strong><?=t($t_base.'titles/email_address');?></strong> <a href="javascript:void(0);"  onclick="edit_preferences_emails('<?=$idForwarder?>')" ><?=t($t_base.'titles/edit');?></a></h4>
<?php 
    //$idForwarder=$_SESSION['forwarder_id'];
    $forwarderContactAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder);
	
$szBookingEmailAry = (!empty($forwarderContactAry['szBookingEmail'])) ? $forwarderContactAry['szBookingEmail']:'';
$szPaymentEmailAry = (!empty($forwarderContactAry['szPaymentEmail'])) ? $forwarderContactAry['szPaymentEmail']:'';
$szCustomerServiceEmailAry = (!empty($forwarderContactAry['szCustomerServiceEmail'])) ? $forwarderContactAry['szCustomerServiceEmail']: '';
$szAirBookingEmailAry = $forwarderContactAry['szAirBookingEmail'] ;
    $szRoadBookingEmailAry = $forwarderContactAry['szRoadBookingEmail'] ;
    $szCourierBookingEmailAry = $forwarderContactAry['szCourierBookingEmail'];

				?>
					<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'titles/bookings');?></span>	
					<div class="field-container">		
					<?php
					if(!empty($szBookingEmailAry))
					{
						foreach($szBookingEmailAry as $szBookingEmailArys)
						{
							?>
								<span class="field-container"><?=$szBookingEmailArys['szEmail']?></span>

							<?php
						}
					}
					echo "</div></div>";
?>

<?php
					
					?>
                                                                
                <div class="ui-fields">
                    <span class="field-name"><?php echo t($t_base.'titles/air')." ".t($t_base.'titles/bookings');?></span>	
                    <div class="field-container">	
                    <?php
                        if(!empty($szAirBookingEmailAry))
                        {
                            foreach($szAirBookingEmailAry as $szAirBookingEmailArys)
                            {
                    ?>
                            <span class="field-container"><?php echo $szAirBookingEmailArys['szEmail']?></span>
                    <?php
                            }
                        } 
                    ?>
                    </div>
                </div>
                <div class="ui-fields">
                    <span class="field-name"><?php echo t($t_base.'titles/road')." ".t($t_base.'titles/bookings');?></span>	
                    <div class="field-container">	
                    <?php
                        if(!empty($szRoadBookingEmailAry))
                        {
                            foreach($szRoadBookingEmailAry as $szRoadBookingEmailArys)
                            {
                    ?>
                            <span class="field-container"><?php echo $szRoadBookingEmailArys['szEmail']?></span>
                    <?php
                            }
                        } 
                    ?>
                    </div>
                </div>
                <div class="ui-fields">
                    <span class="field-name"><?php echo t($t_base.'titles/courier')." ".t($t_base.'titles/bookings');?></span>	
                    <div class="field-container">	
                    <?php
                        if(!empty($szCourierBookingEmailAry))
                        {
                            foreach($szCourierBookingEmailAry as $szCourierBookingEmailArys)
                            {
                    ?>
                            <span class="field-container"><?php echo $szCourierBookingEmailArys['szEmail']?></span>
                    <?php
                            }
                        } 
                    ?>
                    </div>
                </div>
						<div class="ui-fields">
							<span class="field-name"><?=t($t_base.'titles/Payments_billing');?></span>
							<div class="field-container">	
					<?php
					if(!empty($szPaymentEmailAry))
					{
						foreach($szPaymentEmailAry as $szPaymentEmailArys)
						{
							?>
								<span class="field-container"><?=$szPaymentEmailArys['szEmail']?></span>
							<?
						}
					}
					echo "</div></div>";
				?>

						<div class="ui-fields">
							<span class="field-name"><?=t($t_base.'titles/Customer_service');?></span>
							<div class="field-container">	
					<?
					if(!empty($szCustomerServiceEmailAry))
					{
						foreach($szCustomerServiceEmailAry as $szCustomerServiceEmailArys)
						{
							?>
								<span class="field-container"><?=$szCustomerServiceEmailArys['szEmail']?></span>
							<?
						}
					}
					echo "</div></div>";
}?>
<br /><br />
<?php
if($operation_mode=='EDIT_SYSTEM_CONTROL')
{
//$idForwarder=$_SESSION['forwarder_id'];
$forwarderControlPanelArr=$kForwarderContact->getSystemPanelDetails($idForwarder);	
$allCurrencyArr = array();
$kConfig = new cConfig();	
$allCurrencyArr=$kConfig->getBookingCurrency(false,false,true);
?>	
<div id="update_preference_control">
	<div id="ajax_edit_mode">
	<?php
	if(!empty($kForwarderContact->arErrorMessages)){
		?>
		<div id="regError" class="errorBox">
		<div class="header"><?=t($t_base_error.'/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kForwarderContact->arErrorMessages as $key=>$values)
			{
			?><li><?=$key." ".$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<?php } ?>
	<form id="ajax_prefrence_control" method="post">
	<h4><strong><?=t($t_base.'titles/system_config');?></strong></h4>					

					<!--  <div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
					
						
						<?
							if(isset($kForwarderContact->controlBooking))
							{
								$book=$kForwarderContact->controlBooking;
							}
							else
							{
								$book=$forwarderControlPanelArr['BookingFrequency'];
							}
						?>
						<select name="updatePrefControlArr[szBooking]" onblur="closeTip('booking_reports');" onfocus="openTip('booking_reports');">
						<?
							foreach($bookingReports as $key=>$value)
							{
							echo '<option value="'.($key+1) .'"';
							if($key+1==$book)
							{
								echo "selected=selected";
							}
							echo '>'.$value.'</option>';
								
							}
						?>
						</select>
						<div class="field-alert"><div id="booking_reports" style="display:none;"><?=t($t_base.'messages/booking_reports');?></div></div>
					</div>					
					
					-->
					
					<div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/payment_frequency');?></span>
						
						<?php
							if(isset($kForwarderContact->szPaymentFrequency))
							{
								$pay=$kForwarderContact->szPaymentFrequency;
							}
							else
							{
								$pay=$forwarderControlPanelArr['szPaymentFrequency'];
							}
						?>
						<select id="szPayment" name="updatePrefControlArr[szPayment]" onblur="closeTip('payment_reports');" onfocus="openTip('payment_reports');">
						<?php
							foreach($paymentReports as $key=>$value)
							{
							//echo $pay."hi";
							echo '<option value="'.($key+1) .'"';
							if($pay!='' && $pay!='0')
							{
								if($key+1==$pay)
								{
									echo "selected=selected";
								}
							}
							else
							{
								if($key==1)
								{
									echo "selected=selected";
								}
							}
							echo '>'.$value.'</option>';
								
							}
							?>
						</select>
						<div class="field-alert"><div id="payment_reports" style="display:none;"><?=t($t_base.'messages/payment_reports');?></div></div>
					</div>
					<div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/payment_currency');?></span>
						
							<select size="1"  name="updatePrefControlArr[szCurrency]" id="szCurrency" onblur="closeTip('exchange');" onfocus="openTip('exchange');">
								<?php
								//print_r($forwarderControlPanelArr);
									if(!empty($allCurrencyArr))
									{
										foreach($allCurrencyArr as $allCurrencyArrs)
										{
											?><option value="<?=$allCurrencyArrs['id']?>" <?=(($forwarderControlPanelArr['idCurrency']==$allCurrencyArrs['id']) ? "selected":"")?>><?=$allCurrencyArrs['szCurrency']?></option>
											<?php
										}
									}
								?>
							</select>
					
						<div class="field-alert"><div id="exchange" style="display:none;"><?=t($t_base.'messages/exchange');?></div></div>
						
					</div>	
						
					
					<div class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/pricing_updates_on_rss');?></span>
						
						<?php
							if(isset($kForwarderContact->iUpdateRss))
							{
								$rss=$kForwarderContact->iUpdateRss;
							}
							else
							{
								$rss=$forwarderControlPanelArr['iUpdateRss'];
							}
						?>
						<select name="updatePrefControlArr[szRss]" id="szRss" onblur="closeTip('rss_update');" onfocus="openTip('rss_update');">
						<?php
							foreach($pricngUpdates as $key=>$value)
							{
							echo '<option value="'.($key+1).'"';
							if($rss!='' && $rss!='0')
							{
								if($key+1==$rss)
								{
									echo "selected=selected";
								}
							}
							else
							{
								if($key==1)
								{
									echo "selected=selected";
								}
							}
							echo '>'.$value.'</option>';
								
							}
						?>
						</select>
						<div class="field-alert"><div id="rss_update" style="display:none;"><?=t($t_base.'messages/rss_update');?></div></div>
					</div>						
					
					<div class="oh">
		<p class="fl-20">&nbsp;</p>
		<p class="fl-60"><br>
			<a href="javascript:void(0);" onclick="submit_preference_control()"  class="button1"><span><?=t($t_base.'fields/save');?></span></a>
			<a href="javascript:void(0);" onclick="show_preference_control('<?=$idForwarder?>')"  class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
			<input type="hidden" name="mode" value="<?=$operation_mode?>">
			<input type="hidden" name="id" value="<?=$idForwarder;?>">
			
			<input type="hidden" name="updatePrefControlArr[szCurrency_hidden]" id="szCurrency_hidden" value="<?=$forwarderControlPanelArr['idCurrency']?>">
		</p>
	</div>
</form>	
</div>
<?php										
}
else // if($operation_mode=='VIEW_SYSTEM_CONTROL')
{ //$idForwarder=$_SESSION['forwarder_id'];
$forwarderControlPanelArr=$kForwarderContact->getSystemPanelDetails($idForwarder);	
?>
<div id="forwarder_system_control">
					<h4><strong><?=t($t_base.'titles/system_config');?></strong> <a href="javascript:void(0);" onclick="edit_preferences_systems('<?=$idForwarder?>')"><?=t($t_base.'titles/edit');?></a></h4>	
					<!-- <div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
						<span class="field-container">
						<?
							if(isset($kForwarderContact->controlBooking))
							{
								$book=$kForwarderContact->controlBooking;
							}
							else
							{
								$book=$forwarderControlPanelArr['BookingFrequency'];
							}
						?>
						<? 
						foreach ($bookingReports as $key=>$value)
						{	
							if($key+1==$book)
							{	
								echo $value;
							}
						}	
							?></span>
					</div>-->
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/payment_frequency');?></span>
						<span class="field-container">
						<?
							if(isset($kForwarderContact->szPaymentFrequency))
							{
								$pay=$kForwarderContact->szPaymentFrequency;
							}
							else
							{
								$pay=$forwarderControlPanelArr['szPaymentFrequency'];
							}
						?>
						<? 
						foreach ($paymentReports as $key=>$value)
						{
							if($key+1==$pay)
							{
								echo $value;
							}
						}	
							?>
						</span>
					</div>
					
				<div class="ui-fields">
					<span class="field-name"><?=t($t_base.'fields/payment_currency');?></span>
					<span class="field-container">
						<?=$forwarderControlPanelArr['szCurrency']?>
					</span>
					<div class="field-alert"><div id="exchange" style="display:none;"><?=t($t_base.'messages/exchange');?></div></div>
				</div>
				
					<div class="ui-fields">
						<span class="field-name"><?=t($t_base.'fields/pricing_updates_on_rss');?></span>
						<span class="field-container">
						<?
							if(isset($kForwarderContact->iUpdateRss))
							{
								$rss=$kForwarderContact->iUpdateRss;
							}
							else
							{
								$rss=$forwarderControlPanelArr['iUpdateRss'];
							}
						?>
						<? 
						foreach ($pricngUpdates as $key=>$value)
						{
							if($key+1==$rss)
							{
								echo $value;
							}
						}	
							?>
							
						</span>
					</div>
			
			</div>
			
<?
}
?>