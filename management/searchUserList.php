<?php 
/**
  *  admin--- Search Notification management
  */
define('PAGE_PERMISSION','__SEARCH_USER_LIST__'); 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Search User List";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base = "management/uploadService/";
$kConfig = new cConfig();
$allCountriesArr = array();
$allCountriesArr=$kConfig->getAllCountries(true);
    
if(!empty($_POST['searchUserAry']))
{
    $file_name=$kConfig->searchUserListData($_POST['searchUserAry']);
    ob_clean();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //$f = fopen($file_name,'rb');
    //$fsize=filesize($file_name);
    header('Content-Disposition: attachment; filename=Transporteca_Search_User_sheet_'.date('Ymd').'.xlsx');
    ob_clean();
    flush();
    readfile($file_name);
    exit;
} 
if(empty($_POST['searchUserAry']))
{
    //$dtSearch=date("d/m/Y",strtotime("-1 YEAR"));
    $_POST['searchUserAry']['dtSearch'] = 15;
}
   
?>
<div id="hsbody-2">
    <div id="search_noftification_popup" style="display: none;"></div>
    <?php require_once(__APP_PATH_LAYOUT__ ."/admin_messages_left_nav.php"); ?> 
    <div class="hsbody-2-right">
        <h4><strong><?php echo t($t_base.'title/search_user_list'); ?></strong></h4>
        
        <form id="searchUserList" name="searchUserList">
        <div class="clearfix email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-25">
               <span class="input-title">From country<br></span>
               <span class="input-fields">  
                   <select id="idFromCountry" name="searchUserAry[idFromCountry]"  onchange="enableSearchUserListButton(this.form);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?><option value="<?php echo $allCountriesArrs['id'];?>" <?php echo (($allCountriesArrs['id']==$idCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName'];?></option><?php 
                                }
                            }
                        ?>           
                   </select>
               </span>
            </span>
            <span class="quote-field-container wds-25">
              <span class="input-title">To country<br></span>
              <span class="input-fields">  
                    <select id="idToCountry" name="searchUserAry[idToCountry]" onchange="enableSearchUserListButton(this.form);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?><option value="<?php echo $allCountriesArrs['id'];?>" <?php echo (($allCountriesArrs['id']==$idCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName'];?></option><?php 
                                }
                            }
                        ?>           
                   </select>
              </span>
            </span>
            <span class="quote-field-container wds-20">
              <span class="input-title">Days history<br></span>
              <span class="input-fields">  
                  <input type="text" onkeypress="return isNumberKey(event);" onkeyup="enableSearchUserListButton(this.form);" onblur="enableSearchUserListButton(this.form);" id="dtSearch" name="searchUserAry[dtSearch]" value="<?=$_POST['searchUserAry']['dtSearch']?>">
              </span>
            </span>
            
            <span class="quote-field-container wd-30" style="text-align:right;">  
                <span class="input-title">&nbsp;<br></span>
                <a id="search_user_list_download" class="button1" href="javascript:void(0);" style="opacity: 0.4">
                    <span id="search_user_list_download_text"><?php echo 'Download';?></span>
                </a> 
           </span>
        </div>  
    </form>
    </div>
</div> 
<script type="text/javascript">
	function initDatePicker() 
	{
		$("#datepicker1").datepicker();
	}
	initDatePicker();
</script>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); 
?>