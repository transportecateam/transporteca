<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$kAdmin = new cAdmin();
$t_base="management/users_url/";
if(isset($_POST))
{
	$check=sanitize_all_html_input(trim($_POST['check']));
	$status=sanitize_all_html_input(trim($_POST['status']));
	if($check>0)
	{
		if((int)$status==1)
		{
			$kAdmin->viewCustomerDetails();
		}	
		
	}
	if($check>0)
	{
		if((int)$status==4)
		{
			$transportecaManagement=$kAdmin->viewManagementDetails();
			adminProfileDetailsManagement($t_base,$transportecaManagement);
		}	
		
	}
	
}
?>