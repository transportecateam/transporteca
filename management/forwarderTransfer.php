<?php 
/**
  *pay forwarder
  */
define('PAGE_PERMISSION','__TRANSFER_DUE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Accounts Payable - Transfer Due";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/forwarderpayment/";
validateManagement();
$billingTransferDetails = $kAdmin->showForwarderTransferPayment(); 
 
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
    <?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?> 
    <div id="checkDetails" class="hsbody-2-right">
        <?php showForwarderTransferPayment($billingTransferDetails,$idAdmin); ?>
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>

