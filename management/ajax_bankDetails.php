<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base = "ForwardersCompany/BankDetails/";
$forwarderCompanyAry = array();
$kForwardersContact = new cForwarderContact();
if(!empty($_REQUEST['mode']))
{
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['mode']));
	$idForwarder = sanitize_all_html_input(trim($_REQUEST['id']));
}
elseif(!empty($_REQUEST['updateRegistBankAry']))
{
	$updateRegistBankAry = array();
	$updateRegistBankAry = $_REQUEST['updateRegistBankAry'];
	$operation_mode = sanitize_all_html_input(trim($_REQUEST['updateRegistBankAry']['szMode']));
	$idForwarder = sanitize_all_html_input(trim($_REQUEST['updateRegistBankAry']['idForwarder']));
}
if($operation_mode=='DISPLAY_FORWARWARDER_BANK_DETAILS_ACCESS')
{
	$kForwarder = new cForwarder();
	$kForwarder->getForwarderBankDetails($idForwarder);
	forwarderCompaniesEdit($idForwarder,'bankDetails');
?>
 <div class="hsbody-2-right">
		<div id="forwarder_company">
			<h4 style="margin-bottom: 5px;"><strong><?=t($t_base.'titles/registered_bank_details');?></strong>&nbsp;&nbsp;<span id="upper_edit_link"><a href="javascript:void(0);" onclick="edit_forwarder_bank_details('<?=$idForwarder?>')"><?=t($t_base.'feilds/edit');?></a></span></h4>	
			<div id="forwarder_bank_details">
<?	
	echo display_forwarder_bank_details($kForwarder,$t_base);
?>
</div>
		</div>
	</div>
</div>
<?
}
if($operation_mode=='DISPLAY_FORWARWARDER_BANK_DETAILS')
{
	$kForwarder = new cForwarder();
	$kForwarder->getForwarderBankDetails($idForwarder);
	echo display_forwarder_bank_details($kForwarder,$t_base);
}
if($operation_mode=='EDIT_FORWARWARDER_BANK_DETAILS')
{	
	$kForwarder = new cForwarder();
	$kForwarder->load($idForwarder);
	if(!empty($updateRegistBankAry))
	{
		if($kForwarder->updateForwarderBankDetails($updateRegistBankAry))
		{
			echo "SUCCESS";
			die;
		}
	}
?>
<?
	if(!empty($kForwarder->arErrorMessages))
	{
	?>
	<div id="regError" class="errorBox">
	<div class="header"><?=t($t_base.'feilds/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kForwarder->arErrorMessages as $key=>$values)
	      {
		      ?><li><?=$values?></li>
		      <?php 
	      }
	?>
	</ul>
	</div>
</div>
	<?
	}	
	$allCountriesArr =array();
	$allCurrencyArr = array();
	$kConfig = new cConfig();	
	$allCountriesArr=$kConfig->getAllCountries(true);
	$allCurrencyArr=$kConfig->getBookingCurrency(false,false,true);
	
	?>
<form name="updateRegistBankForm" id="updateRegistBankForm" method="post" enctype="multipart/form-data">	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/bank_name');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szBankName]" value="<?=!empty($_POST['updateRegistBankAry']['szBankName'])?$_POST['updateRegistBankAry']['szBankName']:$kForwarder->szBankName?>" onblur="closeTip('bank_name');" onfocus="openTip('bank_name');"></p>
		<div class="field-alert"><div id="bank_name" style="display:none;"><?=t($t_base.'messages/bank_name');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/swift_code');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szSwiftCode]" value="<?=!empty($_POST['updateRegistBankAry']['szSwiftCode'])?$_POST['updateRegistBankAry']['szSwiftCode']:$kForwarder->szSwiftCode?>" onblur="closeTip('swift_code');" onfocus="openTip('swift_code');"></p>
		<div class="field-alert"><div id="swift_code" style="display:none;"><?=t($t_base.'messages/swift_code');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/country');?></p>
		<p class="field-name">
			<select size="1" style="width:210px;" name="updateRegistBankAry[idBankCountry]" id="idBankCountry" onblur="closeTip('bank_country');" onfocus="openTip('bank_country');">
				<option value=''>Select Country</option>
				<?php
					if(!empty($allCountriesArr))
					{
						foreach($allCountriesArr as $allCountriesArrs)
						{
							?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['updateRegistBankAry']['idBankCountry'])?$_POST['updateRegistBankAry']['idBankCountry']:$kForwarder->idBankCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
		<div class="field-alert"><div id="bank_country" style="display:none;"><?=t($t_base.'messages/bank_country');?></div></div>
		
	</div>
	<br/><br/>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/account_name');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szNameOnAccount]" value="<?=!empty($_POST['updateRegistBankAry']['szNameOnAccount'])?$_POST['updateRegistBankAry']['szNameOnAccount']:$kForwarder->szNameOnAccount?>" onblur="closeTip('name_on_account');" onfocus="openTip('name_on_account');"></p>
		<div class="field-alert"><div id="name_on_account" style="display:none;"><?=t($t_base.'messages/name_on_account');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/sort_code');?>&nbsp;<span class="optional">(<?=t($t_base.'titles/optional');?>)</span> </p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[iSortCode]" value="<?=!empty($_POST['updateRegistBankAry']['iSortCode'])?$_POST['updateRegistBankAry']['iSortCode']:(($kForwarder->iSortCode >0)?$kForwarder->iSortCode:'')?>" onblur="closeTip('sort_code');" onfocus="openTip('sort_code');"></p>
		<div class="field-alert"><div id="sort_code" style="display:none;"><?=t($t_base.'messages/sort_code');?></div></div>
		
	</div>
	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/account_number');?></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[iAccountNumber]" value="<?=!empty($_POST['updateRegistBankAry']['iAccountNumber'])?$_POST['updateRegistBankAry']['iAccountNumber']:(($kForwarder->iAccountNumber >0)?$kForwarder->iAccountNumber:'')?>" onblur="closeTip('account_number');" onfocus="openTip('account_number');"></p>
		<div class="field-alert"><div id="account_number" style="display:none;"><?=t($t_base.'messages/account_number');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/IBAN_account_number');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistBankAry[szIBANAccountNumber]" value="<?=!empty($_POST['updateRegistBankAry']['szIBANAccountNumber'])?$_POST['updateRegistBankAry']['szIBANAccountNumber']:$kForwarder->szIBANAccountNumber?>" onblur="closeTip('ibn_acc_number');" onfocus="openTip('ibn_acc_number');"></p>
		<div class="field-alert"><div id="ibn_acc_number" style="display:none;"><?=t($t_base.'messages/ibn_acc_number');?></div></div>
		
	</div>
	<br><br>
	<div class="oh">
		<p align="right" class="fl-30"><a href="javascript:void(0);" onclick="submit_forwarder_bank_details_pre_check('<?=$idForwarder?>')"  class="button1"><span><?=t($t_base.'feilds/save');?></span></a></p>
		<p class="field-container"><a href="javascript:void(0);" onclick="show_forwarder_bank_details('<?=$idForwarder?>')"  class="button2"><span><?=t($t_base.'feilds/cancel');?></span></a>
			<input type="hidden" name="updateRegistBankAry[szMode]" value="<?=$operation_mode?>">
			<input type="hidden" name="updateRegistBankAry[idForwarder]" value="<?=$idForwarder?>">
		</p>
	</div>
</form>
	<?
}
?>