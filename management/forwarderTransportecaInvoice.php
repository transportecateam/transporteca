<?php 
/**
  *pay forwarder
  */
define('PAGE_PERMISSION','__TRANSPORTECA_INVOICES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Accounts Payable - Transporteca Invoices";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/forwarder_transporteca_invoice/";
validateManagement();
$kBooking= new cBooking();
$paymentDetails=$kBooking->paymentOfForwardersTransaction($idAdmin,true);
 
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>
		
	<div class="hsbody-2-right" id="cance_booking_html">
		<?php forwarderTransportecaInvoiceListHtml($paymentDetails,$t_base);?>	
	</div>
</div>
<input type="hidden" id="szBookingStatusFlag" name="szBookingStatusFlag" value="forwardertransportecaInvoice">
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
