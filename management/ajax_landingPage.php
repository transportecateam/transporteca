<?php
ob_start();
if(!isset($_SESSION))
{
	session_start();
}
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL); 
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
$kConfig=new cConfig();
$kBooking=new cBooking();
$kWHSSearch = new cWHSSearch();
$kForwarder = new cForwarder();

$mode = '';
if(isset($_REQUEST['mode']))
{
    $mode = $_REQUEST['mode'];
}
if($mode == 'SHOW_PRICE_DETAILS')
{
    $idWTW = sanitize_all_html_input(trim($_REQUEST['idWTW']));
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
 
    if($idWTW>0)
    {  
        if(!empty($_SESSION['temp_searched_result_data'.'_'.$szBookingRandomNum]))
        { 
            $searchResultAry = $_SESSION['temp_searched_result_data'.'_'.$szBookingRandomNum];  
        }
        else
        {  
            $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,$_SESSION['management_temp_search_id']);
            $searchResultAry = unserialize($tempResultAry['szSerializeData']); 
        }
 
        if(!empty($searchResultAry))
        {
            $updateBookingAry = array();
            foreach($searchResultAry as $searchResultArys)
            { 
                if(trim($searchResultArys['unique_id']) == $idWTW )
                { 
                    $updateBookingAry = $searchResultArys ;
                    break;
                }
            }
        } 
        $postSearchAry = array();
        if(!empty($szBookingRandomNum))
        {
            $kBooking = new cBooking();
            $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum); 
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
        }
        $t_base = "TryItOut/"; 
        echo display_dtd_forwarder_price_details($updateBookingAry,$t_base,$postSearchAry); 
        die;
    }	
}
else if(!empty($_POST['multiregionCityAry']))
{
	$t_base = "SelectService/";
	$res_ary=array();

	if(((int)$_POST['multiregionCityAry']['iRegionFrom']>0) && ((int)$_POST['multiregionCityAry']['szRegionFrom']<=0))
	{
		$error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['multiregionCityAry']['iRegionTo']>0) && ((int)$_POST['multiregionCityAry']['szRegionTo']<=0))
	{
		$error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
		echo "ERROR||||".$error_message;
		die;
	}
	else
	{
		$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
		
		if((int)$_POST['multiregionCityAry']['szRegionFrom']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionFrom'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idOriginPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szOriginPostCode']= $kConfig->szPostCode;
			$postSearchAry['szOriginCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fOriginLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fOriginLongitude'] = $kConfig->szPostcodeLongitude;		
		}
		if((int)$_POST['multiregionCityAry']['szRegionTo']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionTo'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idDestinationPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szDestinationPostCode']= $kConfig->szPostCode;
			$postSearchAry['szDestinationCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;		
		}
	
		$kConfig = $_SESSION['config_object'] ;
		// converting cargo details into cubic meter 			
		$counter_cargo = count($kConfig->iLength);
		for($i=1;$i<=$counter_cargo;$i++)
		{	
			$fLength = $kConfig->iLength[$i] ;
			$fWidth = $kConfig->iWidth[$i] ;
			$fHeight = $kConfig->iHeight[$i] ;
			$iQuantity = $kConfig->iQuantity[$i] ;
			if($kConfig->idCargoMeasure[$i]==1)  // cm
			{
				$fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
			}
			else
			{
				$fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
				if($fCmFactor>0)
				{
					$fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
				}	
			}
		}
	
		$weight_cargo = count($kConfig->iWeight);
		for($i=1;$i<=$weight_cargo;$i++)
		{
			// converting cargo details into cubic meter 
			if($kConfig->idWeightMeasure[$i]==1)  // kg
			{
				$fTotalWeight += ($kConfig->iWeight[$i]);
			}
			else
			{
				$fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
				if($fKgFactor>0)
				{
					$fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
				}	
			}
		}
		
		//$idForwarder =$_SESSION['forwarder_id'];
		$kForwarder = new cForwarder();
		//$kForwarder->load($idForwarder);
		
		$postSearchAry['fCargoVolume'] = $fTotalVolume ;
		$postSearchAry['fCargoWeight'] = $fTotalWeight ;
		$postSearchAry['forwarder_id'] = $idForwarder ;
		$postSearchAry['dtTimingDate'] = $kConfig->dtTiming ;
		$postSearchAry['idOriginCountry'] = $kConfig->szOriginCountry ;
		$postSearchAry['idDestinationCountry'] = $kConfig->szDestinationCountry ;
		$postSearchAry['iOriginCC'] = $kConfig->iOrigingCC ;
		$postSearchAry['iDestinationCC'] = $kConfig->iDestinationCC ;
		
		//$kForwarder->szCurrency = 1 ;
		//echo $kForwarder->szCurrency;
		
		if( $kConfig->szCurrency==1 || $kConfig->szCurrency == 0 )
		{
			$postSearchAry['idCurrency'] = 1 ;
			$postSearchAry['szCurrency'] = 'USD' ;
			$postSearchAry['fExchangeRate'] = 1 ;
		}
		else
		{
			$kWhsSearch = new cWHSSearch();
			$currencySearchAry = $kWhsSearch->getCurrencyDetails($kConfig->szCurrency);
								
			//$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
			//$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
			//$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
				
			$postSearchAry['idCurrency'] = $currencySearchAry['idCurrency'];
			$postSearchAry['szCurrency'] = $currencySearchAry['szCurrency']; ;
			$postSearchAry['fExchangeRate'] = $currencySearchAry['fUsdValue'];
		}
		$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'MANAGEMENT_TRY_IT_OUT');
		$_SESSION['try_it_out_searched_data'] = $postSearchAry;
		echo "SUCCESS ||||";
		?>
		<div style="border:1px solid #D0D0D0;">
		<?php
		//echo count($searchResultAry);
		//echo "service type ".$postSearchAry['idServiceType'] ;
		if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
		{
			display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
		{
			display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
		{
			display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
		{
			display_wtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		echo '</div>';
	//	unset($_SESSION['config_object'],$_SESSION['try_it_out_searched_data'],$_SESSION['forwarder_temp_search_id']);
		die;
	}
}
else if(!empty($_POST['multiEmptyPostcodeCityAry']))
{
	$t_base = "SelectService/";
	$res_ary=array();
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionFrom']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']<=0))
	{
		$error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionTo']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']<=0))
	{
		$error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
		echo "ERROR||||".$error_message;
	}
	else
	{
		$postSearchAry = $_SESSION['try_it_out_searched_data'] ;
	
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionFrom'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idOriginPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szOriginPostCode']= $kConfig->szPostCode;
			$postSearchAry['szOriginCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fOriginLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fOriginLongitude'] = $kConfig->szPostcodeLongitude;
		}
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']>0)
		{
			$idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionTo'])) ;
			$kConfig->loadPostCode($idFromPostcode);
			
			$postSearchAry['idDestinationPostCode'] = $kConfig->idPostCode;
			$postSearchAry['szDestinationPostCode']= $kConfig->szPostCode;
			$postSearchAry['szDestinationCity'] = $kConfig->szPostcodeCity;
			$postSearchAry['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
			$postSearchAry['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
		}

		$kConfig = $_SESSION['config_object'] ;
		// converting cargo details into cubic meter 			
		$counter_cargo = count($kConfig->iLength);
		for($i=1;$i<=$counter_cargo;$i++)
		{
			$fLength = $kConfig->iLength[$i] ;
			$fWidth = $kConfig->iWidth[$i] ;
			$fHeight = $kConfig->iHeight[$i] ;
			$iQuantity = $kConfig->iQuantity[$i] ;
			if($kConfig->idCargoMeasure[$i]==1)  // cm
			{
				$fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
			}
			else
			{
				$fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
				if($fCmFactor>0)
				{
					$fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
				}	
			}
		}
	
		$weight_cargo = count($kConfig->iWeight);
		for($i=1;$i<=$weight_cargo;$i++)
		{
			// converting cargo details into cubic meter 
			if($kConfig->idWeightMeasure[$i]==1)  // kg
			{
				$fTotalWeight += ($kConfig->iWeight[$i]);
			}
			else
			{
				$fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
				if($fKgFactor>0)
				{
					$fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
				}	
			}
		}
		
		//$idForwarder =$_SESSION['forwarder_id'];
		//$kForwarder = new cForwarder();
		//$kForwarder->load($idForwarder);
		
		$postSearchAry['fCargoVolume'] = $fTotalVolume ;
		$postSearchAry['fCargoWeight'] = $fTotalWeight ;
		$postSearchAry['forwarder_id'] = $idForwarder ;
		$postSearchAry['dtTimingDate'] = $kConfig->dtTiming ;
		$postSearchAry['idOriginCountry'] = $kConfig->szOriginCountry ;
		$postSearchAry['idDestinationCountry'] = $kConfig->szDestinationCountry ;
		$postSearchAry['iOriginCC'] = $kConfig->iOrigingCC ;
		$postSearchAry['iDestinationCC'] = $kConfig->iDestinationCC ;
		
		//$kForwarder->szCurrency = 1 ;
		//echo $kForwarder->szCurrency;
		if($kConfig->szCurrency==1)
		{
			$postSearchAry['idCurrency'] = 1 ;
			$postSearchAry['szCurrency'] = 'USD' ;
			$postSearchAry['fExchangeRate'] = 1 ;
		}
		else
		{
			$kWhsSearch = new cWHSSearch();
			$currencySearchAry = $kWhsSearch->getCurrencyDetails($kConfig->szCurrency);
								
			//$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
			//$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
			//$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];
				
			$postSearchAry['idCurrency'] = $currencySearchAry['idCurrency'];
			$postSearchAry['szCurrency'] = $currencySearchAry['szCurrency']; ;
			$postSearchAry['fExchangeRate'] = $currencySearchAry['fUsdValue'];
		}
		$searchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'MANAGEMENT_TRY_IT_OUT');
		$_SESSION['try_it_out_searched_data'] = $postSearchAry;
		echo "SUCCESS ||||";
		?>
		<div style="border:1px solid #D0D0D0;">
		<?php
		//echo count($searchResultAry);
		//echo "service type ".$postSearchAry['idServiceType'] ;
		if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
		{
			display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
		{
			display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
		{
			display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
		{
			display_wtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
		}
		echo '</div>';
		die;
	}
}
else
{	
	$postSearchAry=$_REQUEST['searchAry'];
	
	$t_base = "home/homepage/";
	//for showing message at top for forwarder as admin and change its TRITOUT status
	// START
	//$idForwarderUser =  (int)$_SESSION['forwarder_user_id'];
	//if(($kForwarder->adminEachForwarder($idForwarderUser) == __ADMINISTRATOR_PROFILE_ID__) && ((int)$_SESSION['forwarder_admin_id']==0))
	//{
			$kConfig = new cConfig;
			$kConfig->updateForwarderTryItOutStauts($idForwarderUser);
			$kConfig->szCurrency = (int)$postSearchAry['szCurrency'];
	//}
	// END
	 
	if($postSearchAry['szPageName']=='SIMPLEPAGE' || $postSearchAry['szPageName']=='NEW_LANDING_PAGE')
	{
		$kConfig=new cConfig();
		if($postSearchAry['szOriginCountryStr']!='')
		{
			$szOriginCountryArr=reverse_geocode($postSearchAry['szOriginCountryStr'],true);
			$postSearchAry['szOriginCountry']=$szOriginCountryArr['szCountryName'];
			$idOriginCountry=$kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);
			$postSearchAry['szOriginCountry']=$idOriginCountry;
			$postSearchAry['szOLat']=$szOriginCountryArr['szLat'];
			$postSearchAry['szOLng']=$szOriginCountryArr['szLng'];
			$postSearchAry['szOriginCity']=$szOriginCountryArr['szCityName'];
			
			$postcodeCheckAry=array();
			$postcodeResultAry = array();
			
			$postcodeCheckAry['idCountry'] = $idOriginCountry ;
			$postcodeCheckAry['szLatitute'] = $szOriginCountryArr['szLat'] ;
			$postcodeCheckAry['szLongitute'] = $szOriginCountryArr['szLng'] ;
			$postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);
						
			if(!empty($postcodeResultAry))
			{
				$postSearchAry['szOriginPostCode'] = $postcodeResultAry['szPostCode'];
			}
		}
		
		if($postSearchAry['szDestinationCountryStr']!='')
		{
			$szDesCountryArr=reverse_geocode($postSearchAry['szDestinationCountryStr'],true);
			$postSearchAry['szDestinationCountry']=$szDesCountryArr['szCountryName'];
			$idDesCountry=$kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);
			$postSearchAry['szDestinationCountry']=$idDesCountry;
			$postSearchAry['szDLat']=$szDesCountryArr['szLat'];
			$postSearchAry['szDLng']=$szDesCountryArr['szLng'];
			$postSearchAry['szDestinationCity']=$szDesCountryArr['szCityName'];
			
			$postcodeCheckAry=array();
			$postcodeResultAry = array();
			
			$postcodeCheckAry['idCountry'] = $idDesCountry ;
			$postcodeCheckAry['szLatitute'] = $szDesCountryArr['szLat'] ;
			$postcodeCheckAry['szLongitute'] = $szDesCountryArr['szLng'] ;
			$postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);
									
			if(!empty($postcodeResultAry))
			{
				$postSearchAry['szDestinationPostCode'] = $postcodeResultAry['szPostCode'];
			}
		}
		
		$postSearchAry['iBookingStep']=1;
		$postSearchAry['iWeight']=$postSearchAry['iWeight'];
		$postSearchAry['iQuantity'][1]='1';
		$postSearchAry['idWeightMeasure'][1]='1';
		
		if($postSearchAry['szPageName']=='SIMPLEPAGE')
		{
			$postSearchAry['iFromRequirementPage']=2;
		}
		else
		{
			$postSearchAry['iFromRequirementPage']=2;
		}		
		if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
		{
			$postSearchAry['idCC'][0]='1';
		}
		$postSearchAry['idCC'][1]='2';
	}
	
	$searchFormAry = array();
	$searchFormAry = $postSearchAry ;
	
	if($postSearchAry['szPageName']=='NEW_LANDING_PAGE')
	{
		$searchFormAry['szPageName'] = 'SIMPLEPAGE';
	}
	 
	$kConfig->validateLandingPage($searchFormAry);
	
	if(!empty($kConfig->arErrorMessages))
	{
		if($postSearchAry['szPageName']=='NEW_LANDING_PAGE')
		{
			echo "ERROR||||";			
			$hidden_search_form=false;	
			$id_suffix = "";		
			if($postSearchAry['iHiddenValue']==1)
			{
				$id_suffix = "_hiden";
			}
			$szErrorMessageStr = '';
			foreach($kConfig->arErrorMessages as $errorKey=>$errorValue)
			{
				if($errorKey=='szOriginCountry')
				{
					$errorKey = 'szOriginCountryStr'.$id_suffix.'_container+++++'.$errorValue.'+++++szOriginCountryStr'.$id_suffix;
				} 
				if($errorKey=='szDestinationCountry')
				{
					$errorKey = 'szDestinationCountryStr'.$id_suffix.'_container+++++'.$errorValue;
				} 
				if($errorKey=='szDestinationCountry')
				{
					$errorKey = 'szDestinationCountryStr'.$id_suffix.'_container+++++'.$errorValue;
				}
				if($errorKey=='fCargoWeight_landing_page')
				{
					$errorKey = 'iWeight'.$id_suffix.'_container+++++'.$errorValue;
				}
				if($errorKey=='iVolume')
				{
					$errorKey = 'iVolume'.$id_suffix.'_container+++++'.$errorValue;
				}
				if($errorKey=='dtTiming')
				{
					$errorKey = 'dtTiming'.$id_suffix.'_container+++++'.$errorValue;
				}				
				$szErrorMessageStr .= $errorKey."$$$$";
			}
			
			echo $szErrorMessageStr ; 
			die;
		}
		else
		{
                    echo "ERROR ||||" ;
                    ?>
                    <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
                    <div id="regErrorList">
                    <ul>
                    <?php
                        foreach($kConfig->arErrorMessages as $key=>$values)
                        {
                          ?><li><?=$values?></li> <?php 
                        }
                    ?>
                    </ul>
                    </div>
                    <?php
		}
	} 
	else if($postSearchAry['szPageName']=='SIMPLEPAGE' || $postSearchAry['szPageName']=='NEW_LANDING_PAGE')
	{
            $kBooking=new cBooking();
            $szBookingRandomNum = $kConfig->szBookingRandomNum;
            $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

            $szReuquestPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$postSearchAry['szReuquestPageUrl'];

            if($idBooking>0)
            {
                $kBooking->save($kConfig,$idBooking,$szReuquestPageUrl);
            }
            else
            {
                $kBooking->add($kConfig,__BOOKING_STATUS_ZERO_LEVEL__,$szReuquestPageUrl);
                $idBooking = $kBooking->idBooking;
            } 

            $kConfig=new cConfig();
            $kWHSSearch=new cWHSSearch();

            $iLanguage = getLanguageId();
            $searchResult=array();
            $countryKeyValueAry=$kConfig->getAllCountryInKeyValuePair(true,false,false,$iLanguage);
            $kBooking = new cBooking();
            $searchResultAry=array();
            $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

            if($idBooking>0)
            { 
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);

                $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);
                $postSearchAry['iNumRecordFound'] = count($searchResultAry);
                $kBooking->addSelectServiceData($postSearchAry);

                // initialising arrays
                $countryAry=array();
                $serviceTypeAry=array();
                $weightMeasureAry=array();
                $cargoMeasureAry = array();
                $timingTypeAry=array();
                $forwarderAry=array();
            }
            else
            {
                $postSearchAry = array();
                $cargoAry = array();
            } 
            $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,true);
 
            $kBooking->addLinkBuilder($postSearchAry);
            $szLinkUrl =  __MAIN_SITE_HOME_PAGE_SECURE_URL__."/service/".$kBooking->szRandomNumber."/" ;
            
            if(!empty($searchResultAry))
            {
                $t_base = "SelectService/";
                echo "LINK_BUILDER||||"; 
                echo display_link_builder($szLinkUrl) ;
                echo "||||";
                echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true,false,true);
                die;
            }
            else
            {
                $t_base = "SelectService/";
                echo "LINK_BUILDER_NO_RECORD||||";
                echo display_link_builder($szLinkUrl) ;
                echo "||||";
                echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,false,true,true);
                die;
            } 
            die;
	}
	else if(!empty($kConfig->multiEmaptyPostcodeFromCityAry) || !empty($kConfig->multiEmaptyPostcodeToCityAry))
	{
		echo "MULTI_EMPTY_POSTCODE||||" ;
		
		$kBooking=new cBooking();	
		
		$_SESSION['config_object'] = $kConfig ;
		$_SESSION['try_it_out_searched_data'] = $postSearchAry ;
		
		?>
		<div id="popup-bg"></div>
		<form name="multi_empty_postcode_from_form" id="multi_empty_postcode_from_form" action="" method="post">
		<div id="popup-container">
		<div class="compare-popup popup">
		<?php
		if(!empty($kConfig->multiEmaptyPostcodeFromCityAry))
		{	
			$counterFrom = count($kConfig->multiEmaptyPostcodeFromCityAry);
			if($counterFrom>5)
			{
				//$div_class = 'class="compare-popup popup" style="margin-top:-30px;"';
			}
			else
			{
				//$div_class = 'class="compare-popup popup"';
			}
			?>
			<div <?=$div_class?>>				
				<H5><?=t($t_base.'fields/empty_postcode_origin');?></H5>
				<p><?=t($t_base.'messages/which_part_of');?> <?=$kConfig->szOriginCity?> <?=t($t_base.'messages/need_transportation');?></p>
				<?php
					foreach($kConfig->multiEmaptyPostcodeFromCityAry as $key=>$fromRegions)
					{
				?>
					<p><input type="radio" name="multiEmptyPostcodeCityAry[szRegionFrom]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
				<?php
					}
				?>
				<input type="hidden" name="multiEmptyPostcodeCityAry[iRegionFrom]" value="1">
				</div>
			<?php
		}			
		
		if(!empty($kConfig->multiEmaptyPostcodeToCityAry))
		{		
			$counterFrom = count($kConfig->multiEmaptyPostcodeToCityAry);
			if($counterFrom>5)
			{
				//$div_class = 'class="compare-popup popup" style="margin-top:-30px;"';
			}
			else
			{
				//$div_class = 'class="compare-popup popup"';
			}
			?>			
			<div <?=$div_class?>>
				<H5><?=t($t_base.'fields/empty_postcode_destination');?></H5>
				<p><?=t($t_base.'messages/which_part_of');?> <?=$kConfig->szDestinationCity?> <?=t($t_base.'messages/need_transportation_to');?></p>
				<?php
					foreach($kConfig->multiEmaptyPostcodeToCityAry as $key=>$fromRegions)
					{
				?>
					<p><input type="radio" name="multiEmptyPostcodeCityAry[szRegionTo]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
				<?php
					}
					?>
					<input type="hidden" name="multiEmptyPostcodeCityAry[iRegionTo]" value="1">
			</div>
			<?
		  }
		?>
		<br/>
		<p align="center">
			<input type="hidden" name="multiEmptyPostcodeCityAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$kConfig->szBookingRandomNum?>">
				<a href="javascript:void(0);" onclick="updateEmptyPostcodeCity_forwarder('multi_empty_postcode_from_form')" class="button1">
				<span><?=t($t_base.'fields/select');?></span></a>
				<a href="javascript:void(0);" onclick="showHide('Transportation_pop')" class="button2">
				<span><?=t($t_base.'fields/cancel');?></span></a>
		</p>
		</div>
		</div>
	</form>	
		<?php
	}
	else if(!empty($kConfig->multiRegionToCityAry) || !empty($kConfig->multiRegionFromCityAry))
	{
		echo "MULTIREGIO||||";
		$kBooking=new cBooking();	
		$_SESSION['config_object'] = $kConfig ;
		$_SESSION['try_it_out_searched_data'] = $postSearchAry ;
			
		?>
		<div id="popup-bg"></div>
		<form name="multiregion_select_form" id="multiregion_select_form" action="" method="post">
		<div id="popup-container">
		<div class="compare-popup popup" style="margin-top:50px;">
			
		<?php
			if(!empty($kConfig->multiRegionFromCityAry))
			{
				?>			
				<H5><?=t($t_base.'fields/from_regions');?></H5>
				<span id="popupError" class="errorBox" style="display:none;"></span>
				<p><?=t($t_base.'messages/from_regions_popup_header');?></p>
				<?php
					foreach($kConfig->multiRegionFromCityAry as $key=>$fromRegions)
					{
				?>
					<p> <input type="radio" name="multiregionCityAry[szRegionFrom]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
				<?php
					}
				?>
				<input type="hidden" name="multiregionCityAry[iRegionFrom]" value="1">
				<?php
			}
		?>
		<?php
			if(!empty($kConfig->multiRegionToCityAry))
			{
				?>		
				<br>	
				<H5><?=t($t_base.'fields/to_regions');?></H5>
				<?php
                                    if(empty($kConfig->multiRegionFromCityAry))
                                    {
                                        ?>
                                        <span id="popupError" class="errorBox" style="display:none;"></span>
                                        <?php
                                    }
				?>
				<p><?=t($t_base.'messages/to_regions_popup_header');?></p>
				<?php
                                    foreach($kConfig->multiRegionToCityAry as $key=>$fromRegions)
                                    {
				?>
					<p> <input type="radio" name="multiregionCityAry[szRegionTo]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
				<?php
                                    }
				?>
				<input type="hidden" name="multiregionCityAry[iRegionTo]" value="1">
				<?php
			}
		?>
		<br/>
		<p align="center">
			<input type="hidden" name="multiregionCityAry[szBookingRandomNum]" value="<?=$kConfig->szBookingRandomNum?>">
			<a href="javascript:void(0);" onclick="updatePostcodeRegion_tryitnow();" class="button1">
				<span><?=t($t_base.'fields/continue');?></span></a></p>
		</div>
		</div>
	</form>	
		<?php
	}
	else
	{
            $t_base = "SelectService/"; 
            // converting cargo details into cubic meter 			
            $counter_cargo = count($searchFormAry['iLength']);
            
            for($i=1;$i<=$counter_cargo;$i++)
            {
                $fLength = $searchFormAry['iLength'][$i] ;
                $fWidth = $searchFormAry['iWidth'][$i] ;
                $fHeight = $searchFormAry['iHeight'][$i] ;
                $iQuantity = $searchFormAry['iQuantity'][$i] ;
                if($searchFormAry['idCargoMeasure'][$i]==1)  // cm
                {
                    $fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
                }
                else
                {
                    $fCmFactor = $kWHSSearch->getCargoFactor($searchFormAry['idCargoMeasure'][$i]);
                    if($fCmFactor>0)
                    {
                        $fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
                    }	
                }
            }

            $weight_cargo = count($searchFormAry['iWeight']);
            for($i=1;$i<=$weight_cargo;$i++)
            {
                // converting cargo details into cubic meter 
                if($searchFormAry['idWeightMeasure'][$i]==1)  // kg
                {
                    $fTotalWeight += ($searchFormAry['iWeight'][$i]);
                }
                else
                {
                    $fKgFactor = $kWHSSearch->getWeightFactor($searchFormAry['idWeightMeasure'][$i]);
                    if($fKgFactor>0)
                    {
                        $fTotalWeight += (($searchFormAry['iWeight'][$i]/$fKgFactor ));
                    }	
                }
            }

            $idForwarder =$_SESSION['forwarder_id'];
            $kForwarder = new cForwarder();
            $kForwarder->load($idForwarder);
            //echo $idForwarder." volume ".$fTotalVolume." total weight: ".$fTotalWeight;
            $postSearchAry['fCargoVolume'] = $fTotalVolume ;
            $postSearchAry['fCargoWeight'] = $fTotalWeight ;
            $postSearchAry['forwarder_id'] = $idForwarder ;
            $postSearchAry['dtTimingDate'] = $kConfig->dtTiming ;
            $postSearchAry['idOriginCountry'] = $kConfig->szOriginCountry ;
            $postSearchAry['idDestinationCountry'] = $kConfig->szDestinationCountry ;
            $postSearchAry['iOriginCC'] = $kConfig->iOrigingCC ;
            $postSearchAry['iDestinationCC'] = $kConfig->iDestinationCC ;

            //$kForwarder->szCurrency = 1 ;
            //echo $kForwarder->szCurrency;
            if($kConfig->szCurrency==1)
            {
                $postSearchAry['idCurrency'] = 1 ;
                $postSearchAry['szCurrency'] = 'USD' ;
                $postSearchAry['fExchangeRate'] = 1 ;
            }
            else
            {
                $kWhsSearch = new cWHSSearch();
                $currencySearchAry = $kWhsSearch->getCurrencyDetails($kConfig->szCurrency);

                //$currencyExchangeRateAry['idCurrency']=$resultAry['idCurrency'];
                //$currencyExchangeRateAry['szCurrency'] = $resultAry['szCurrency'];
                //$currencyExchangeRateAry['fExchangeRate'] = $resultAry['fUsdValue'];

                $postSearchAry['idCurrency'] = $currencySearchAry['idCurrency'] ;
                $postSearchAry['szCurrency'] = $currencySearchAry['szCurrency']; ;
                $postSearchAry['fExchangeRate'] = $currencySearchAry['fUsdValue'];
            }

            $originPostCodeAry=array();
            $originPostCodeAry = $kConfig->getPostCodeDetails($kConfig->szOriginCountry,$kConfig->szOriginPostCode,$kConfig->szOriginCity);

            $destinationPostCodeAry=array();
            $destinationPostCodeAry = $kConfig->getPostCodeDetails($kConfig->szDestinationCountry,$kConfig->szDestinationPostCode,$kConfig->szDestinationCity);

            $postSearchAry['fOriginLatitude'] = round((float)$originPostCodeAry['szLatitude'],8); 
            $postSearchAry['fOriginLongitude'] = round((float)$originPostCodeAry['szLongitude'],8);

            $postSearchAry['fDestinationLatitude'] = round((float)$destinationPostCodeAry['szLatitude'],8);
            $postSearchAry['fDestinationLongitude'] = round((float)$destinationPostCodeAry['szLongitude'],8);

            cWHSSearch::$szServiceCalculationMode = "MANAGEMENT_TRY_IT_OUT";
            $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,false,'MANAGEMENT_TRY_IT_OUT');
            $_SESSION['try_it_out_searched_data'] = $postSearchAry; 
            $szPriceCalculationLogs = $kWHSSearch->szTestCalculationLogs;
            $szLCLCalculationLogs = $kWHSSearch->szLCLCalculationLogs;
            
            echo "SUCCESS ||||";
            ?>
            <div style="border:1px solid #D0D0D0;">
            <?php
                //echo count($searchResultAry);
                if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__)
                {
                    display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                }
                else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
                {
                    display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                }
                else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
                {
                    display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                }
                else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
                {
                    display_wtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry,true);
                }
                echo '</div>';
            ?>
            <div class="" style="width:100%">
                <div style="width:50%;text-align: left;">
                    <?php if(!empty($szLCLCalculationLogs)){ ?>
                        <a href='javascript:void(0)' onclick="showHideCourierCal('courier_service_calculation_lcl')">.</a>
                    <?php }?>
                </div>
                <div style='text-align: right;'>
                    <?php if(!empty($szPriceCalculationLogs)){ ?>
                        <a href='javascript:void(0)' onclick="showHideCourierCal('courier_service_calculation')">Warehouse Logs</a>
                    <?php }?> 
                </div>
            </div>
            <?php
            if(!empty($szPriceCalculationLogs))
            {
                echo display_calculation_logs($szPriceCalculationLogs);
            } 
            if(!empty($szLCLCalculationLogs))
            {
                echo display_calculation_logs($szLCLCalculationLogs,'courier_service_calculation_lcl');
            }
	}
}	
?>