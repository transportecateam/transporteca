<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
define('PAGE_PERMISSION','__LANDING_PAGES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Setup Landing Pages ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement_ajax();
$kConfig = new cConfig();

$idLandingPage = sanitize_all_html_input(trim($_REQUEST['id']));
$kExplain = new cExplain();
$landingPageDataAry = array();
$testimonialAry = array();
$partnerLogoAry = array();


$dialUpCodeAry = array();
$dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);


$allCountriesArr = array();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);

$standardCargoListArr=$kConfig->getDistinctStandardCargoList();
    
if($idLandingPage>0)
{
    $landingPageDataAry = $kExplain->getAllNewLandingPageData($idLandingPage);
    $testimonialAry = $kExplain->getAllTestimonials($idLandingPage);
    $partnerLogoAry = $kExplain->getAllPartnerLogo($idLandingPage);
     
    $form_id='addLandingPage';
    $mode = 'EDIT_NEW_LANDING_PAGE';
    $data = $landingPageDataAry[0];
}
else
{
    $mode = 'ADD_NEW_LANDING_PAGE';
    $form_id='addLandingPage';
} 
//$site_url = remove_http_from_url(__MAIN_SITE_HOME_PAGE_SECURE_URL__);
$site_url = "www.transporteca.com";

$kConfig =new cConfig();
$kVatApplication = new cVatApplication();
$langArr=$kConfig->getLanguageDetails('','','','','',false);

$manualFeeCargoTypeArr = array();
$manualFeeCargoTypeArr = $kVatApplication->getManualFeeCaroTypeList();

$iPageSearchType = (int)$data['iPageSearchType'];

$szVogaFieldVisibilityClass = ' display-none';
$szVogaAutomaticVisibilityClass = ' display-none';
$szCommonVogaFieldVisibilityClass = ' display-none';

$szFromPrefillClass = ' display-block';
if($iPageSearchType ==__SEARCH_TYPE_VOGUE__)
{ 
    $szVogaFieldVisibilityClass = ' display-block';
} 
else if($iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_ || $iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_)
{
    $szVogaAutomaticVisibilityClass = ' display-block';
    $szFromPrefillClass = ' display-none';
}

if($iPageSearchType ==__SEARCH_TYPE_VOGUE__ || $iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_ || $iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_)
{
    $szCommonVogaFieldVisibilityClass = ' display-block';
}

$currencyAry = array();
$currencyAry = $kConfig->getBookingCurrency(false,true);
?>
<style>
.field-title {
    color: grey;
    font-size: 13px;
}
</style>
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/images/loader.gif" alt="" />				
</div>

    <div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<?php $t_base="management/LandingPage/"; ?>	
	<div class="hsbody-2-right">
		<h4><b><?=t($t_base.'title/landing_page_setup');?></b></h4>
		<div id ="Error-log" class="errorBox" style="display:none;"></div>
		<script type="text/javascript">
                    $(function(){
                        checkDescriptionLength('szMetaDescription');
                        form_field_length(); 
                        <?php if($data['iPageSearchType']>0) { ?>
                            toggle_weight_field('<?php echo $data['iPageSearchType']; ?>');
                        <?php } ?> 
                    });
                    setup();
                    function toggle_weight_field(search_type)
                    {
                        var search_type = parseInt(search_type);
                        console.log("Search Type: "+search_type);
                        if(search_type==5 || search_type==6)
                        {
                            $(".voga-automatic-fields").removeClass('display-none');
                            $(".voga-automatic-fields").addClass('display-block'); 
                            
                            
                            $(".voga-automatic-fields-insurance").removeClass('display-none');
                            $(".voga-automatic-fields-insurance").addClass('display-block');
                            
                            $(".from-prefill-field").removeClass('display-block');
                            $(".from-prefill-field").addClass('display-none'); 
                            
                        }
                        else
                        {
                            $(".voga-automatic-fields").removeClass('display-block');
                            $(".voga-automatic-fields").addClass('display-none'); 
                            $(".voga-automatic-fields input[type=text]").attr('value','');
                            
                            $(".voga-automatic-fields-insurance").addClass('display-none');
                            $(".voga-automatic-fields-insurance").removeClass('display-block');
                            
                            $(".from-prefill-field").removeClass('display-none');
                            $(".from-prefill-field").addClass('display-block'); 
                        } 
                        if(search_type==4)
                        {
                            $(".hideClass").removeClass('display-none');
                            $(".hideClass").addClass('display-block'); 
                            
                            $(".voga-automatic-fields-insurance").removeClass('display-none');
                            $(".voga-automatic-fields-insurance").addClass('display-block');
                        }
                        else
                        {
                            $(".hideClass").removeClass('display-block');
                            $(".hideClass").addClass('display-none'); 
                            $(".hideClass input[type=text]").attr('value','');
                            
                            //$(".voga-automatic-fields-insurance").addClass('display-none');
                            //$(".voga-automatic-fields-insurance").removeClass('display-block');
                        }
                        
                        if(search_type==4 || search_type==5 || search_type==6)
                        { 
                            $(".common-voga-field").removeClass('display-none');
                            $(".common-voga-field").addClass('display-block'); 
                        }
                        else
                        {
                            $(".common-voga-field").removeClass('display-block');
                            $(".common-voga-field").addClass('display-none'); 
                            $(".common-voga-field input[type=text]").attr('value','');
                        } 
                        if(search_type==1 || search_type==3)
                        {
                            $("#default-weight-container").attr('style','display:block;');
                        }
                        else
                        {
                            $("#default-weight-container").attr('style','display:none;');
                        }
                    }
		</script>
		<form name="addLandingPage" id="<?=$form_id?>" action="<?=__BASE_URL__."/newLandingPagePreview/"?>" target="_blank" method="post" >
                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/optimized_for');?></span>
                        <span class="field-container"><input type="text" name="landingPageArr[szPageHeading]" value="<?php if(isset($data['szSeoKeywords'])){echo $data['szSeoKeywords']; }?>"/></span>
                    </p>
                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/url');?> (<?=$site_url?>/..)</span>
                        <span class="field-container"><input type="text" name="landingPageArr[szLink]" value="<?php if(isset($data['szUrl'])){echo $data['szUrl']; }?>"/></span>
                    </p>
                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/page_title');?></span>
                        <span class="field-container"><input type="text" name="landingPageArr[szMetaTitle]" value="<?php if(isset($data['szMetaTitle'])){echo $data['szMetaTitle']; }?>"/></span>
                    </p>
                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/meta_keyword');?></span>
                        <span class="field-container"><textarea rows="3" cols="25" name="landingPageArr[szMetaKeyword]"><?php if(isset($data['szMetaKeywords'])){echo $data['szMetaKeywords']; }?></textarea></span>
                    </p>
                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/meta_description');?></span> 
                        <span class="field-container"><textarea rows="3" cols="25" onkeyup="checkDescriptionLength(this.id);" name="landingPageArr[szMetaDescription]" id="szMetaDescription"><?php if(isset($data['szMetaDescription'])){echo $data['szMetaDescription']; }?></textarea></span>
                        <span style="color: #7F7F7F;float: left;font-size: 12px;font-style: italic;margin: 63px 0 0 37px;text-align: right;width: 30px;">(<span id="char_left">160</span>)</span>
                    </p>
                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/active_status');?></span>
                        <span class="field-container">
                            <select name="landingPageArr[iActive]">
                                <option value = '0' <?php if(isset($data['iPublished']) && $data['iPublished'] ==0){echo "selected='selected'"; } ?>>No</option>
                                <option value = '1' <?php if(isset($data['iPublished']) && $data['iPublished'] ==1){echo "selected='selected'"; } ?>>Yes</option>	
                            </select>
                        </span>
                    </p>

                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/language');?></span>
                        <span class="field-container">
                            <select name="landingPageArr[iLanguage]" id="iLanguage">
                                <?php
                                if(!empty($langArr))
                                {
                                    foreach($langArr as $langArrs)
                                    {?>
                                       <option value = '<?php echo $langArrs['id']?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==$langArrs['id']){echo "selected='selected'"; } ?>><?php echo ucfirst($langArrs['szName']);?></option>
                                    <?php
                                    }
                                }?>
                            </select>
                        </span>
                    </p>
                    <p class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/use_holding_page');?></span>
                        <span class="field-container">
                            <select name="landingPageArr[iHoldingPage]" id="iHoldingPage" onchange="showHideHoldingSection(this.value);">
                                <option value = '0' <?php if(isset($data['iHoldingPage']) && $data['iHoldingPage'] ==0){echo "selected='selected'"; } ?>>No</option>
                                <option value = '1' <?php if(isset($data['iHoldingPage']) && $data['iHoldingPage'] ==1){echo "selected='selected'"; } ?>>Yes</option>	
                            </select>
                        </span>
                    </p>
                    <div class="landing-page-holding-fields-container" id="custom_holding_page_container" <?php if(isset($data['iHoldingPage']) && $data['iHoldingPage'] ==1){?> style="display:block;" <?php } else {?> style="display:none;" <?php } ?>>
                        <p class="clearfix email-fields-container" style="width:100%;"><?=t($t_base.'fields/heading')." 1";?><br />
                            <input type="text" id="szTextB2" name="landingPageArr[szHoldingHeading1]"  value="<?php if(isset($data['szHoldingHeading1'])){echo $data['szHoldingHeading1']; }?>"> 
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?=t($t_base.'fields/heading')." 2";?><br />
                            <input type="text" id="szHoldingHeading2" name="landingPageArr[szHoldingHeading2]"  value="<?php if(isset($data['szHoldingHeading2'])){echo $data['szHoldingHeading2']; }?>"> 
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?php echo t($t_base.'fields/text')." 1 ".t($t_base.'fields/bold');?><br />
                            <input type="text" id="szHoldingTextBold1" name="landingPageArr[szHoldingTextBold1]"  value="<?php if(isset($data['szHoldingTextBold1'])){echo $data['szHoldingTextBold1']; }?>"> 
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?php echo t($t_base.'fields/text')." 1 ".t($t_base.'fields/plain');?><br />
                            <input type="text" id="szHoldingTextPlain1" name="landingPageArr[szHoldingTextPlain1]"  value="<?php if(isset($data['szHoldingTextPlain1'])){echo $data['szHoldingTextPlain1']; }?>"> 
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?php echo t($t_base.'fields/text')." 2 ".t($t_base.'fields/bold');?><br />
                            <input type="text" id="szHoldingTextBold2" name="landingPageArr[szHoldingTextBold2]"  value="<?php if(isset($data['szHoldingTextBold2'])){echo $data['szHoldingTextBold2']; }?>"> 
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?php echo t($t_base.'fields/text')." 2 ".t($t_base.'fields/plain');?><br />
                            <input type="text" id="szHoldingTextPlain2" name="landingPageArr[szHoldingTextPlain2]"  value="<?php if(isset($data['szHoldingTextPlain2'])){echo $data['szHoldingTextPlain2']; }?>"> 
                        </p> 
                        <p class="clearfix email-fields-container" style="width:100%;">
                            <span class="quote-field-container wd-20"><?php echo t($t_base.'fields/email_heading'); ?><br>
                                <input type="text" id="szHoldingEmailHeading" name="landingPageArr[szHoldingEmailHeading]"  value="<?php if(isset($data['szHoldingEmailHeading'])){echo $data['szHoldingEmailHeading']; }?>"> 
                            </span>
                            <span class="quote-field-container wds-20"><?php echo t($t_base.'fields/from_heading'); ?><br>
                                <input type="text" id="szHoldingFromHeading" name="landingPageArr[szHoldingFromHeading]"  value="<?php if(isset($data['szHoldingFromHeading'])){echo $data['szHoldingFromHeading']; }?>"> 
                            </span>
                            <span class="quote-field-container wds-20"><?php echo t($t_base.'fields/to_heading'); ?><br>
                                <input type="text" id="szHoldingToHeading" name="landingPageArr[szHoldingToHeading]"  value="<?php if(isset($data['szHoldingToHeading'])){echo $data['szHoldingToHeading']; }?>"> 
                            </span>
                            <span class="quote-field-container wds-20"><?php echo t($t_base.'fields/submit_button_text'); ?><br>
                                <input type="text" id="szHoldingSubmitButtonText" name="landingPageArr[szHoldingSubmitButtonText]"  value="<?php if(isset($data['szHoldingSubmitButtonText'])){echo $data['szHoldingSubmitButtonText']; }?>"> 
                            </span>
                            <span class="quote-field-container wds-20"><?php echo t($t_base.'fields/thanks_button_text'); ?><br>
                                <input type="text" id="szHoldingThanksButtonText" name="landingPageArr[szHoldingThanksButtonText]"  value="<?php if(isset($data['szHoldingThanksButtonText'])){echo $data['szHoldingThanksButtonText']; }?>"> 
                            </span>
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?php echo t($t_base.'fields/text')." 3 ".t($t_base.'fields/bold');?><br />
                            <input type="text" id="szHoldingTextBold3" name="landingPageArr[szHoldingTextBold3]"  value="<?php if(isset($data['szHoldingTextBold3'])){echo $data['szHoldingTextBold3']; }?>"> 
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?php echo t($t_base.'fields/text')." 3 ".t($t_base.'fields/plain');?><br />
                            <input type="text" id="szHoldingTextPlain3" name="landingPageArr[szHoldingTextPlain3]"  value="<?php if(isset($data['szHoldingTextPlain3'])){echo $data['szHoldingTextPlain3']; }?>"> 
                        </p>
                        <p class="clearfix email-fields-container" style="width:100%;"><?php echo t($t_base.'fields/youtube_video_url');?><br />
                            <input type="text" id="szYoutubeUrl" name="landingPageArr[szYoutubeUrl]"  value="<?php if(isset($data['szYoutubeUrl'])){echo $data['szYoutubeUrl']; }?>"> 
                        </p>
                    </div>
			
			<!-- This div contains middle part of form -->
			 <div id="first_div" class="clearfix">
			 	<div class="left-fields">
					<p class="profile-fields">
					 <span class="field-name">A</span>
					 <span class="field-container">					 	
					 	<span class="field-title">Image URL<br></span>
					 	<input type="text" name="landingPageArr[szTextA1]" id="szTextA1" value="<?php if(isset($data['szTextA1'])){echo $data['szTextA1']; }?>">
					 </span>
					</p>
					<p class="profile-fields">
					 <span class="field-name"></span>
					 <span class="field-container">
					 	<span class="field-title">Image description (150-160 characters)<span style="float:right;" id="szTextA2_span">(0)</span><br></span> 
					 	<textarea rows="3" cols="25" onkeypress="limit_characters(this.id,160);" maxlength="160" id="szTextA2" name="landingPageArr[szTextA2]" ><?php if(isset($data['szTextA2'])){echo $data['szTextA2']; }?></textarea>
					 </span>
					</p>
					<p class="profile-fields">
                                            <span class="field-name"></span>
                                            <span class="field-container">					 			
                                               <span class="field-title">Image title (48 characters)<span style="float:right;" id="szTextA3_span">(0)</span><br></span>		 	
                                               <input type="text" name="landingPageArr[szTextA3]" onkeypress="limit_characters(this.id,48);" maxlength="48" id="szTextA3" value="<?php if(isset($data['szTextA3'])){echo $data['szTextA3']; }?>">
                                            </span>
					</p>
                                        <div id="nonholdingPageVariable" <?php if($data['iHoldingPage'] ==1){?> style="display:none;" <?php } ?>>
					<p class="profile-fields">
                                            <span class="field-name">B</span>
                                            <span class="field-container"> 
                                                <span class="field-title">Section 1 heading<br></span>			 		
                                                <input type="text" id="szTextB" name="landingPageArr[szTextB]"  value="<?php if(isset($data['szTextB'])){echo $data['szTextB']; }?>">
                                            </span>
					</p>
					<p class="profile-fields from-prefill-field<?php echo $szFromPrefillClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">From pre-fill<br></span>			 		
                                                <input type="text" id="szTextB2" name="landingPageArr[szTextB2]"  value="<?php if(isset($data['szTextB2'])){echo $data['szTextB2']; }?>">
                                            </span>
					</p> 
					<p class="profile-fields">
                                            <span class="field-name"></span>
                                            <span class="field-container">					 		
                                                <span class="field-title">Description<br></span>
                                                <textarea rows="3" style="width:221px;" cols="25" name="landingPageArr[szTextB3]" ><?php if(isset($data['szTextB3'])){echo $data['szTextB3']; }?></textarea>
                                            </span>
					</p>
                                        <p class="profile-fields">
                                            <span class="field-name">&nbsp;</span>
                                           <span class="field-container"> 
                                                <span class="field-title">Search type</span>			 		
                                                <select name="landingPageArr[iPageSearchType]" id="iPageSearchType" onchange="toggle_weight_field(this.value);">
                                                    <option value = '<?php echo __SEARCH_TYPE_BREAK_BULK__?>' <?php if(isset($data['iPageSearchType']) && $data['iPageSearchType'] ==__SEARCH_TYPE_BREAK_BULK__){ echo "selected='selected'"; } ?>>Break bulk</option>
                                                    <option value = '<?php echo __SEARCH_TYPE_PARCELS__?>' <?php if(isset($data['iPageSearchType']) && $data['iPageSearchType'] ==__SEARCH_TYPE_PARCELS__){ echo "selected='selected'"; } ?>>Parcels</option>
                                                    <option value = '<?php echo __SEARCH_TYPE_PALLETS__?>' <?php if(isset($data['iPageSearchType']) && $data['iPageSearchType'] ==__SEARCH_TYPE_PALLETS__){ echo "selected='selected'"; } ?>>Pallets</option>
                                                    <option value = '<?php echo __SEARCH_TYPE_VOGUE__?>' <?php if(isset($data['iPageSearchType']) && $data['iPageSearchType'] ==__SEARCH_TYPE_VOGUE__){ echo "selected='selected'"; } ?>>Standard Cargo - Delivery Address</option>
                                                    <option value = '<?php echo __SEARCH_TYPE_VOGUE_AUTOMATIC_?>' <?php if(isset($data['iPageSearchType']) && $data['iPageSearchType'] ==__SEARCH_TYPE_VOGUE_AUTOMATIC_){ echo "selected='selected'"; } ?>>Standard Cargo - From and To</option>
                                                    <option value = '<?php echo __SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_?>' <?php if(isset($data['iPageSearchType']) && $data['iPageSearchType'] ==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_){ echo "selected='selected'"; } ?>>Standard Cargo - From and To - simple</option>
                                                </select>
                                           </span>
					</p> 
                                        <p class="profile-fields common-voga-field<?php echo $szCommonVogaFieldVisibilityClass; ?>">
                                            <span class="field-name">&nbsp;</span>
                                           <span class="field-container"> 
                                                <span class="field-title">Standard cargo type</span>			 		
                                                <select name="landingPageArr[idSearchMini]" id="idSearchMini">
                                                    <option value="">Select</option>
                                                   <?php 
                                                   if(!empty($standardCargoListArr))
                                                   {
                                                       foreach($standardCargoListArr as $standardCargoListArrs)
                                                       {?>
                                                            <option value="<?php echo $standardCargoListArrs['idSearchMini']?>" <?php if(isset($data['idSearchMini']) && $data['idSearchMini'] ==$standardCargoListArrs['idSearchMini']){ echo "selected='selected'"; } ?>><?php echo $standardCargoListArrs['idSearchMini'];?></option>
                                                          <?php 
                                                       }
                                                   }?>
                                                </select>
                                           </span>
					</p>   
                                        <p class="profile-fields voga-automatic-fields<?php echo $szVogaAutomaticVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">From instruction<br></span>			 		
                                                <input type="text" id="szFromInstruction" name="landingPageArr[szFromInstruction]"  value="<?php if(isset($data['szFromInstruction'])){echo $data['szFromInstruction']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields voga-automatic-fields<?php echo $szVogaAutomaticVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">To instruction<br></span>			 		
                                                <input type="text" id="szToInstruction" name="landingPageArr[szToInstruction]"  value="<?php if(isset($data['szToInstruction'])){echo $data['szToInstruction']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields common-voga-field<?php echo $szCommonVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Middle text<br></span>			 		
                                                <input type="text" id="szMiddleText" name="landingPageArr[szMiddleText]"  value="<?php if(isset($data['szMiddleText'])){echo $data['szMiddleText']; }?>">
                                            </span>
					</p> 
                                        <p class="profile-fields common-voga-field<?php echo $szCommonVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Contact me<br></span>			 		
                                                <input type="text" id="szContactMe" name="landingPageArr[szContactMe]"  value="<?php if(isset($data['szContactMe'])){echo $data['szContactMe']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields voga-automatic-fields<?php echo $szVogaAutomaticVisibilityClass; ?>" >
                                            <span class="field-name">&nbsp;</span>
                                           <span class="field-container"> 
                                                <span class="field-title">Commodity</span>			 		
                                                <select name="landingPageArr[szCargoType]" id="szCargoType">
                                                    <option value="">Select</option>
                                                    <?php
                                                        if(!empty($manualFeeCargoTypeArr))
                                                        {
                                                            foreach($manualFeeCargoTypeArr as $manualFeeCargoTypeArrs)
                                                            {
                                                               ?>
                                                               <option value="<?php echo $manualFeeCargoTypeArrs['szCode']; ?>" <?php echo (($manualFeeCargoTypeArrs['szCode']==$data['szCargoType'])?'selected':''); ?>><?php echo $manualFeeCargoTypeArrs['szName']; ?></option>
                                                               <?php
                                                            }
                                                        }
                                                    ?> 
                                                </select>
                                           </span>
					</p>
                                        
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Shipper Company Name<br></span>			 		
                                                <input type="text" id="szShipperCompanyName" name="landingPageArr[szShipperCompanyName]"  value="<?php if(isset($data['szShipperCompanyName'])){echo $data['szShipperCompanyName']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
						 <span class="field-name"></span>
					 	<span class="field-container"> 
					 		<span class="field-title">Shipper First Name<br></span>			 		
					 		<input type="text" id="szShipperFirstName" name="landingPageArr[szShipperFirstName]"  value="<?php if(isset($data['szShipperFirstName'])){echo $data['szShipperFirstName']; }?>">
					 	</span>
					</p>
                                         <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
                                                <span class="field-name"></span>
                                               <span class="field-container"> 
                                                       <span class="field-title">Shipper Last Name<br></span>			 		
                                                       <input type="text" id="szShipperLastName" name="landingPageArr[szShipperLastName]"  value="<?php if(isset($data['szShipperLastName'])){echo $data['szShipperLastName']; }?>">
                                               </span>
					</p>
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Shipper Email Address<br></span>			 		
                                                <input type="text" id="szShipperEmail" name="landingPageArr[szShipperEmail]"  value="<?php if(isset($data['szShipperEmail'])){echo $data['szShipperEmail']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Shipper Phone Number<br></span>
                                                <select size="1" name="landingPageArr[idShipperDialCode]" style="max-width:25%;width:25%;min-width:10%">
                                                    <?php
                                                       if(!empty($dialUpCodeAry))
                                                       {
                                                           $usedDialCode = array();
                                                           foreach($dialUpCodeAry as $dialUpCodeArys)
                                                           {
                                                                ?><option value="<?=$dialUpCodeArys['id']?>" <?php if(isset($data['idShipperDialCode']) && $data['idShipperDialCode'] ==$dialUpCodeArys['id']){ echo "selected='selected'"; } ?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                                                
                                                           }
                                                       }
                                                ?>
                                                </select> 
                                                <input style="max-width:73%;width:73%;" type="text" id="szShipperEmail" name="landingPageArr[szShipperPhone]"  value="<?php if(isset($data['szShipperPhone'])){echo $data['szShipperPhone']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Shipper Address<br></span>			 		
                                                <input type="text" id="szShipperAddress" name="landingPageArr[szShipperAddress]"  value="<?php if(isset($data['szShipperAddress'])){echo $data['szShipperAddress']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Shipper Postcode<br></span>			 		
                                                <input type="text" id="szShipperPostcode" name="landingPageArr[szShipperPostcode]"  value="<?php if(isset($data['szShipperPostcode'])){echo $data['szShipperPostcode']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>">
                                            <span class="field-name"></span>
                                            <span class="field-container"> 
                                                <span class="field-title">Shipper City<br></span>			 		
                                                <input type="text" id="szShipperCity" name="landingPageArr[szShipperCity]"  value="<?php if(isset($data['szShipperCity'])){echo $data['szShipperCity']; }?>">
                                            </span>
					</p>
                                        <p class="profile-fields hideClass<?php echo $szVogaFieldVisibilityClass; ?>" >
                                            <span class="field-name">&nbsp;</span>
                                           <span class="field-container"> 
                                                <span class="field-title">Shipper Country</span>			 		
                                                <select name="landingPageArr[idShipperCountry]" id="idShipperCountry">
                                                    <option value="">Select</option>
                                                   <?php 
                                                   if(!empty($allCountriesArr))
                                                   {
                                                       foreach($allCountriesArr as $allCountriesArrs)
                                                       {?>
                                                            <option value="<?php echo $allCountriesArrs['id']?>" <?php if(isset($data['idShipperCountry']) && $data['idShipperCountry'] ==$allCountriesArrs['id']){ echo "selected='selected'"; } ?>><?php echo $allCountriesArrs['szCountryName'];?></option>
                                                          <?php 
                                                       }
                                                   }?>
                                                </select>
                                           </span>
					</p>  
                                        <p class="profile-fields" id="default-weight-container">
                                            <span class="field-name">&nbsp;</span>
                                            <span class="field-container"> 
                                                <span class="field-title">Default weight</span>			 		
                                                <input type="text" id="szTextB2" name="landingPageArr[fDefaultWeight]"  value="<?php if(isset($data['fDefaultWeight'])){echo $data['fDefaultWeight']; }?>">
                                            </span>
					</p> 
                                        
                                        <p class="profile-fields voga-automatic-fields-insurance<?php echo $szVogaAutomaticVisibilityClass; ?>" >
                                            <span class="field-name">&nbsp;</span>
                                           <span class="field-container"> 
                                                <span class="field-title">Default insurance</span>			 		
                                                <select name="landingPageArr[iDefaultInsurance]" id="iDefaultInsurance">
                                                   <option value="1" <?php if($data['iDefaultInsurance']==1){ ?> selected <?php }?>>Yes</option> 
                                                   <option value="2" <?php if($data['iDefaultInsurance']==2){ ?> selected <?php }?>>No</option> 
                                                   <option value="3" <?php if($data['iDefaultInsurance']==3 || $data['iDefaultInsurance']=='0'){ ?> selected <?php }?>>Optional</option>
                                                </select>
                                           </span>
					</p>
                                        <p class="profile-fields voga-automatic-fields-insurance<?php echo $szVogaAutomaticVisibilityClass; ?>" >
                                            <span class="field-name">&nbsp;</span>
                                           <span class="field-container"> 
                                                <span class="field-title">Cargo value currency</span>			 		
                                                <select name="landingPageArr[idInsuranceCurrency]" id="idInsuranceCurrency">
                                                    <option value="">Select</option>
                                                    <?php
                                                        if($data['idInsuranceCurrency']==0)
                                                        {
                                                            $data['idInsuranceCurrency']=2;
                                                        }
                                                        if(!empty($currencyAry))
                                                        {
                                                           foreach($currencyAry as $currencyArys)
                                                           {
                                                            ?>
                                                            <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$data['idInsuranceCurrency'])?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                                            <?php
                                                           }
                                                        }
                                                    ?>  
                                                </select>
                                           </span>
					</p>
                                        <p class="profile-fields">
                                            <span class="field-name">C</span>
                                            <span class="field-container">					 		
                                                <span class="field-title">Section 2 heading<br></span>
                                                <textarea rows="1" style="height:21px;width:221px;" cols="25" name="landingPageArr[szTextC]" ><?php if(isset($data['szTextC'])){echo $data['szTextC']; }?></textarea>
                                            </span>
                                        </p>
                                        <p class="profile-fields">
                                            <span class="field-name">D</span>
                                            <span class="field-container">					 	
                                                <span class="field-title">Left image URL<br></span>
                                                <input type="text" id="szTextD1" name="landingPageArr[szTextD1]" value="<?php if(isset($data['szTextD1'])){echo $data['szTextD1']; }?>">
                                            </span>
                                        </p>
                                        <p class="profile-fields">
                                            <span class="field-name"></span>
                                            <span class="field-container">					 		
                                                <span class="field-title">Left image description (150-160 characters)<span style="float:right;" id="szTextD2_span">(0)</span><br></span>
                                                <textarea rows="3" cols="25" name="landingPageArr[szTextD2]" id="szTextD2" onkeypress="limit_characters(this.id,160);" maxlength="160"><?php if(isset($data['szTextD2'])){echo $data['szTextD2']; }?></textarea>
                                            </span>
                                        </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Left image title (48 characters)<span style="float:right;" id="szTextD3_span">(0)</span><br></span>
                                                            <input type="text" id="szTextD3" name="landingPageArr[szTextD3]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextD3'])){echo $data['szTextD3']; }?>">
                                                    </span>
                                            </p> 
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Centre image URL<br></span>
                                                            <input type="text" id="szTextD4" name="landingPageArr[szTextD4]" value="<?php if(isset($data['szTextD4'])){echo $data['szTextD4']; }?>">
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 		
                                                            <span class="field-title">Centre image description (150-160 characters)<span style="float:right;" id="szTextD5_span">(0)</span><br></span>
                                                            <textarea rows="3" cols="25" name="landingPageArr[szTextD5]" id="szTextD5" onkeypress="limit_characters(this.id,160);" maxlength="160"><?php if(isset($data['szTextD5'])){echo $data['szTextD5']; }?></textarea>
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Centre image title (48 characters)<span style="float:right;" id="szTextD6_span">(0)</span><br></span>
                                                            <input type="text" id="szTextD6" name="landingPageArr[szTextD6]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextD6'])){echo $data['szTextD6']; }?>">
                                                    </span>
                                            </p>					
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Right image URL<br></span>
                                                            <input type="text" id="szTextD1" name="landingPageArr[szTextD7]" value="<?php if(isset($data['szTextD7'])){echo $data['szTextD7']; }?>">
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 		
                                                            <span class="field-title">Right image description (150-160 characters)<span style="float:right;" id="szTextD8_span">(0)</span><br></span>
                                                            <textarea rows="3" cols="25" name="landingPageArr[szTextD8]" id="szTextD8" onkeypress="limit_characters(this.id,160);" maxlength="160" ><?php if(isset($data['szTextD8'])){echo $data['szTextD8']; }?></textarea>
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Right image title (48 characters)<span style="float:right;" id="szTextD9_span">(0)</span><br></span>
                                                            <input type="text" id="szTextD9" name="landingPageArr[szTextD9]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextD9'])){echo $data['szTextD9']; }?>">
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name">E</span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Left heading<br></span>
                                                            <textarea id="szTextE1" name="landingPageArr[szTextE1]"><?php if(isset($data['szTextE1'])){ echo $data['szTextE1']; } ?></textarea>
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Left text<br></span>
                                                            <textarea id="szTextE2" name="landingPageArr[szTextE2]" ><?php if(isset($data['szTextE2'])){echo $data['szTextE2']; }?> </textarea>
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Center heading<br></span>
                                                            <textarea id="szTextE3" name="landingPageArr[szTextE3]" ><?php if(isset($data['szTextE3'])){echo $data['szTextE3']; }?></textarea>
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Center text<br></span>
                                                            <textarea id="szTextE4" name="landingPageArr[szTextE4]" ><?php if(isset($data['szTextE4'])){echo $data['szTextE4']; }?></textarea>
                                                    </span>
                                            </p>		
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Right heading<br></span>
                                                            <textarea id="szTextE5" name="landingPageArr[szTextE5]" ><?php if(isset($data['szTextE5'])){echo $data['szTextE5']; }?></textarea>
                                                    </span>
                                            </p>
                                            <p class="profile-fields">
                                                    <span class="field-name"></span>
                                                    <span class="field-container">					 	
                                                            <span class="field-title">Right text<br></span>
                                                            <textarea id="szTextE6" name="landingPageArr[szTextE6]" ><?php if(isset($data['szTextE6'])){echo $data['szTextE6']; }?></textarea>
                                                    </span>
                                            </p>
                                        </div>
					<p class="profile-fields">
						<span class="field-name">F</span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Section 3 heading<br></span>
					 		<input type="text" id="szTextF1" name="landingPageArr[szTextF1]" value="<?php if(isset($data['szTextF1'])){echo $data['szTextF1']; }?>">
					 	</span>
					</p>			
					<p class="profile-fields">
						<span class="field-name">G</span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Left image URL<br></span>
					 		<input type="text" id="szTextG1" name="landingPageArr[szTextG1]" value="<?php if(isset($data['szTextG1'])){echo $data['szTextG1']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Left image description (150-160 characters)<span style="float:right;" id="szTextG2_span">(0)</span><br></span>
					 		<textarea id="szTextG2" name="landingPageArr[szTextG2]" onkeypress="limit_characters(this.id,160);" maxlength="160"><?php if(isset($data['szTextG2'])){echo $data['szTextG2']; }?></textarea>
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Left image title (48 characters)<span style="float:right;" id="szTextG3_span">(0)</span><br></span>
					 		<input type="text" id="szTextG3" name="landingPageArr[szTextG3]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextG3'])){echo $data['szTextG3']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Center image URL<br></span>
					 		<input type="text" id="szTextG4" name="landingPageArr[szTextG4]" value="<?php if(isset($data['szTextG4'])){echo $data['szTextG4']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Center image description (150-160 characters)<span style="float:right;" id="szTextG5_span">(0)</span><br></span>
					 		<textarea id="szTextG5" name="landingPageArr[szTextG5]" onkeypress="limit_characters(this.id,160);" maxlength="160"><?php if(isset($data['szTextG5'])){echo $data['szTextG5']; }?></textarea>
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Center image title (48 characters)<span style="float:right;" id="szTextG6_span">(0)</span><br></span>
					 		<input type="text" id="szTextG6" name="landingPageArr[szTextG6]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextG6'])){echo $data['szTextG6']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Right image URL<br></span>
					 		<input type="text" id="szTextG7" name="landingPageArr[szTextG7]" value="<? if(isset($data['szTextG7'])){echo $data['szTextG7']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Right image description (150-160 characters)<span style="float:right;" id="szTextG8_span">(0)</span><br></span>
					 		<textarea id="szTextG8" name="landingPageArr[szTextG8]" onkeypress="limit_characters(this.id,160);" maxlength="160"><?php if(isset($data['szTextG8'])){echo $data['szTextG8']; }?></textarea>
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Right image title (48 characters)<span style="float:right;" id="szTextG9_span">(0)</span><br></span>
					 		<input type="text" id="szTextG9" name="landingPageArr[szTextG9]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextG9'])){echo $data['szTextG9']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name">H</span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Left text<br></span>
					 		<input type="text" id="szTextH1" name="landingPageArr[szTextH1]" value="<?php if(isset($data['szTextH1'])){echo $data['szTextH1']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Center text<br></span>
					 		<input type="text" id="szTextH2" name="landingPageArr[szTextH2]" value="<?php if(isset($data['szTextH2'])){echo $data['szTextH2']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Right text<br></span>
					 		<input type="text" id="szTextH3" name="landingPageArr[szTextH3]" value="<?php if(isset($data['szTextH3'])){echo $data['szTextH3']; }?>">
					 	</span>
					</p>
					
					<p class="profile-fields">
						<span class="field-name">I</span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Section 4 heading<br></span>
					 		<input type="text" id="szTextI4" name="landingPageArr[szTextI4]" value="<?php if(isset($data['szTextI4'])){echo $data['szTextI4']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
					 	<span class="field-container">					 	
					 		<span class="field-title">Section 4 Sub heading 1<br></span>
					 		<input type="text" id="szTextI4" name="landingPageArr[szTextI5]" value="<?php if(isset($data['szTextI5'])){echo $data['szTextI5']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
					 	<span class="field-container">					 	
					 		<span class="field-title">Section 4 Sub heading 2<br></span>
					 		<input type="text" id="szTextI4" name="landingPageArr[szTextI6]" value="<?php if(isset($data['szTextI6'])){echo $data['szTextI6']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
					 	<span class="field-container">					 	
					 		<span class="field-title">Section 4 Sub heading 3<br></span>
					 		<input type="text" id="szTextI4" name="landingPageArr[szTextI7]" value="<?php if(isset($data['szTextI7'])){echo $data['szTextI7']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-container">					 	
					 		<span class="field-title">Image URL<br></span>
					 		<input type="text" id="szTextI1" name="landingPageArr[szTextI1]" value="<?php if(isset($data['szTextI1'])){echo $data['szTextI1']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Image description (150-160 characters)<span style="float:right;" id="szTextI2_span">(0)</span><br></span>
					 		<textarea id="szTextI2" name="landingPageArr[szTextI2]" onkeypress="limit_characters(this.id,160);" maxlength="160"><?php if(isset($data['szTextI2'])){echo $data['szTextI2']; }?></textarea>
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Image title (48 characters)<span style="float:right;" id="szTextI3_span">(0)</span><br></span>
					 		<input type="text" id="szTextI3" name="landingPageArr[szTextI3]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextI3'])){echo $data['szTextI3']; }?>">
					 	</span>
					</p>
					
					<p class="profile-fields">
						<span class="field-name">J</span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Section 5 heading<br></span>
					 		<input type="text" id="szTextI1" name="landingPageArr[szTextJ1]" value="<?php if(isset($data['szTextJ1'])){echo $data['szTextJ1']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name">L</span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Image URL<br></span>
					 		<input type="text" id="szTextL1" name="landingPageArr[szTextL1]" value="<?php if(isset($data['szTextL1'])){echo $data['szTextL1']; }?>">
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Image description (150-160 characters)<span style="float:right;" id="szTextL2_span">(0)</span><br></span>
					 		<textarea id="szTextL2" name="landingPageArr[szTextL2]" onkeypress="limit_characters(this.id,160);" maxlength="160"><?php if(isset($data['szTextL2'])){echo $data['szTextL2']; }?></textarea>
					 	</span>
					</p>
					<p class="profile-fields">
						<span class="field-name"></span>
					 	<span class="field-container">					 	
					 		<span class="field-title">Image title (48 characters)<span style="float:right;" id="szTextL3_span">(0)</span><br></span>
					 		<input type="text" id="szTextL3" name="landingPageArr[szTextL3]" onkeypress="limit_characters(this.id,48);" maxlength="48" value="<?php if(isset($data['szTextL3'])){echo $data['szTextL3']; }?>">
					 	</span>
					</p>
				</div>
				<div class="right-fields">
					<div id="preview_landing_page">
                                            <img alt="Landing Page Image" src="<?php echo __BASE_STORE_IMAGE_URL__."/landing_page.png"?>">
					</div>
                                           
                                        <div id="testimonial_main_container_div"> 
                                            <?php  
                                                $testimonial_counter = 1;
                                                if(!empty($_POST['landingPageArr']['szImageURL']) || !empty($testimonialAry))
                                                {
                                                    if(!empty($_POST['landingPageArr']['szImageURL']))
                                                    {
                                                        $loop_counter = count($_POST['landingPageArr']['szImageURL']);
                                                        $loop_counter = $loop_counter;
                                                    }
                                                    else
                                                    {
                                                        $loop_counter = count($testimonialAry);
                                                    }
                                                    if($loop_counter>1)
                                                    {
                                                        $remove_testimonial_link_style = " style='display:block;'";
                                                    }
                                                    else
                                                    {
                                                        $remove_testimonial_link_style = " style='display:none;'";
                                                    } 
                                                    for($j=0;$j<$loop_counter;$j++)
                                                    {
                                                        $testimonial_counter = $j+1;
                                                        ?>
                                                        <div id="testimonial_container_div_<?php echo $j; ?>" > 
                                                            <?php
                                                                echo display_testimonial_form($kExplain,$testimonial_counter,$testimonialAry);
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                else
                                                {
                                                    $remove_testimonial_link_style = " style='display:none;'";
                                                    ?>
                                                    <div id="testimonial_container_div_0"> 
                                                        <?php
                                                            echo display_testimonial_form($kExplain,$testimonial_counter);
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                            ?>
                                            <div id="testimonial_container_div_<?php echo $testimonial_counter; ?>" style="display:none;">
                                            </div>
                                        </div> 
                                    <p class="fl" align="left" style="margin-right:30px"><a href="javascript:void(0);" onclick="add_more_partner_logo('ADD_MORE_TESTIMONIAL')">+ Add more</a></p>
                                    <p class="fr" align="right" style="margin-right:30px"><a href="javascript:void(0);" id="remove_k" <?=$remove_testimonial_link_style?> onclick="remove_partner_logo('ADD_MORE_TESTIMONIAL');">Remove Testimonial</a></p>
                                    <br><br>
				<h4><strong>Partner Logos</strong></h4>
				<div id="partner_logo_container_main_div">
					<?php  
						$references_counter = 1;
						if(!empty($_POST['landingPageArr']['szLogoImageUrl']) || !empty($partnerLogoAry))
						{
							if(!empty($_POST['landingPageArr']['szLogoImageUrl']))
							{
								$loop_counter = count($_POST['landingPageArr']['szLogoImageUrl']);
								$loop_counter = $loop_counter;
							}
							else
							{
								$loop_counter = count($partnerLogoAry);
							}
							if($loop_counter>1)
							{
								$remove_cargo_link_style = " style='display:block;'";
							}
							else
							{
								$remove_cargo_link_style = " style='display:none;'";
							}
							for($j=0;$j<$loop_counter;$j++)
							{
								$references_counter = $j+1;
								?>
								<div id="partner_logo_container_div_<?php echo $j; ?>" > 
									<?php
										echo display_partner_logo_form($kExplain,$references_counter,$partnerLogoAry[$references_counter]);
									?>
								</div>
								<?php
							}
						}
						else
						{
							$remove_cargo_link_style = " style='display:none;'";
							?>
							<div id="partner_logo_container_div_0"> 
								<?php
									echo display_partner_logo_form($kExplain,$references_counter);
								?>
							</div>
							<?php
						}
					?>
					<div id="partner_logo_container_div_<?php echo $references_counter; ?>" style="display:none;">
					</div>
				</div>
				<p class="fl" align="left" style="margin-right:30px"><a href="javascript:void(0);" onclick="add_more_partner_logo('ADD_MORE_PARTNER_LOGO')">+ Add more</a></p>
				<p class="fr" align="right" style="margin-right:30px"><a href="javascript:void(0);" id="remove" <?=$remove_cargo_link_style?> onclick="remove_partner_logo('ADD_MORE_PARTNER_LOGO');">Remove Logo</a></p>
			</div>		
			</div>
			<!-- This div contains tiny on left and buttons on right-->
			<div id="last_div" class="clearfix">			
				<div style="float:left;width:97%;margin-left:15px;margin-top:10px;">
                                    <span class="field-title">Video Script<br></span>
                                    <textarea id="szTextL4" style="width:100%;" name="landingPageArr[szTextL4]"><? if(isset($data['szTextL4'])){echo $data['szTextL4']; }?></textarea>
				</div>		 
                                <div style="float:left;width:97%;margin-left:15px;margin-top:10px;">
                                    <h4><strong>SEO text</strong></h4>
                                    <p class="profile-fields">
                                        <span class="field-name"><?=t($t_base.'fields/display_seo_text');?></span>
                                        <span class="field-container">
                                            <select name="landingPageArr[iDisplaySeoText]">
                                                <option value = '0' <?php if(isset($data['iDisplaySeoText']) && $data['iDisplaySeoText'] ==0){echo "selected='selected'"; } ?>>No</option>
                                                <option value = '1' <?php if(isset($data['iDisplaySeoText']) && $data['iDisplaySeoText'] ==1){echo "selected='selected'"; } ?>>Yes</option>	
                                            </select>
                                        </span>
                                    </p>
                                    <p class="profile-fields">
                                        <span class="field-name">Background Color(Hex)</span>
                                        <span class="field-container"> 
                                            <input type="text" id="szBackgroundColor" name="landingPageArr[szBackgroundColor]" onblur="change_editor_background(this.value,'szSeoDescription');" value="<?php if(isset($data['szBackgroundColor'])){echo $data['szBackgroundColor']; }?>">
                                        </span>
                                    </p>
                                    <p class="profile-fields">
                                        <span class="field-name">Heading</span>
                                        <span class="field-container"> 
                                            <input type="text" id="szSeoHeading" name="landingPageArr[szSeoHeading]" value="<?php if(isset($data['szHeading'])){echo $data['szHeading']; }?>">
                                        </span>
                                    </p>
                                    <span class="field-title">Content: <br></span>
                                    <textarea id="szSeoDescription" class="myTextEditor" style="width:100%;" name="landingPageArr[szSeoDescription]"><?php if(isset($data['szSeoDescription'])){echo $data['szSeoDescription']; }?></textarea>
				</div>
				<div style="text-align:center;float:left;margin-top:15px;">
					<input type="hidden" name="mode" id="mode" value="<?=$mode?>">
                                        <input type="hidden" name="landingPageArr[iDefault]" id="iDefault" value="<?php if(isset($data['iDefault'])){echo $data['iDefault']; }?>">
					<input type="hidden" name="landingPageArr[idLandingPage]" id="idLandingPage" value="<?=$idLandingPage?>">
					<input type="hidden" name="landingPageArr[hiddenPosition]" id="hiddenPosition" value="<?=$references_counter?>" />
					<input type="hidden" name="landingPageArr[hiddenPosition1]" id="hiddenPosition1" value="<?=$references_counter?>" />
                                        
                                        <input type="hidden" name="landingPageArr[hiddenPosition_k]" id="hiddenPosition_k" value="<?=$testimonial_counter?>" />
					<input type="hidden" name="landingPageArr[hiddenPosition1_k]" id="hiddenPosition1_k" value="<?=$testimonial_counter ?>" />
                                        
					<a class="button1" id="cancelAddingPage" onclick="previewLandingPage('<?=$form_id?>','1')"><span><?=t($t_base.'fields/preview');?></span></a>
					<a class="button1" id="addNewPage" onclick="addNewLandingPage('<?=$form_id?>',1);"><span><? if(isset($data['id'])){echo t($t_base.'fields/save');}else{echo t($t_base.'fields/add_1');}?></span></a>
					<a class="button1" id="cancelAddingPage" onclick="redirect_url('<?=__MANAGEMENT_NEW_LANDING_PAGE_EDITOR_URL__?>')"><span><?=t($t_base.'fields/cancel');?></span></a>
				</div>
			</div>
		</form>
                <script type="text/javascript">
                    $(document).ready(function()
		    {    
                    <?php	
                        if($data['szBackgroundColor']!='')
                        {
                            $bgColor=$data['szBackgroundColor'];
                            $bgColor = substr($bgColor, 1);
                            ?>
                            setTimeout(function(){change_editor_background('<?php echo $data['szBackgroundColor'];?>','szSeoDescription')},1200);						
                            <?php
                        }
                ?>
		    });   
		</script>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>