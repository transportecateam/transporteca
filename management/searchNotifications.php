<?php 
/**
  *  admin--- Search Notification management
  */
define('PAGE_PERMISSION','__SEARCH_NOTIFICATION__'); 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Search Notifications";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base = "management/uploadService/";
$kExplain = new cExplain();

?>
<div id="hsbody-2">
    <div id="search_noftification_popup" style="display: none;"></div>
    <?php require_once(__APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php"); ?> 
    <div class="hsbody-2-right">
        <div id="search_noftification_listing_main_container">
            <h4><strong><?php echo t($t_base.'title/search_notification'); ?></strong></h4>
            <p><?php echo t($t_base.'messages/search_notification_description'); ?></p>
            <br>
            <div class="clearfix"></div>
            <div id="search_noftification_listing">
               <?php echo searchNotificationListingHtml();?>
            </div>
        </div>
        <div id="search_noftification_listing_private_customer_main_container">
            <h4><strong><?php echo t($t_base.'title/search_notification_private_customer'); ?></strong></h4>
            <p><?php echo t($t_base.'messages/search_notification_description_private_customer'); ?></p>
            <br>
            <div class="clearfix"></div> 
            <div id="search_noftification_listing_private_customer">
               <?php echo searchNotificationListingPrivateCustomerHtml();?>
            </div>
        </div>
    </div>
</div>    
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );

?>