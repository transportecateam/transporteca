<?php 
/**
  * Customer Feedback DETAILS
  */
define('PAGE_PERMISSION','__NPS_SUGGESTIONS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Feedback - NPS & Suggestions";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

validateManagement();
$t_base="management/NPS/";
$t_base_error="Error";
$kNPS = new cNPS();
$kUser = new cUser();
require( __APP_PATH_CLASSES__ . "/PHPExcel.php" );
$objPHPExcel=new PHPExcel();
$getLastThreeMonthCommentArr=$kNPS->getLastThreeMonthComment(); 

$currntMonthDataArr=$kNPS->storeCurrMonthCustomerData(); 
 
if(!empty($_POST['downloadFeedbackData']))
{
    $feedbackDataGivenDateRange=$kNPS->getLastThreeMonthComment($_POST['downloadFeedbackData']);

    $filename=createFeedBackExcelFile($feedbackDataGivenDateRange,$objPHPExcel,$kUser,$kNPS);

    ob_clean();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //$f = fopen($file_name,'rb');
    //$fsize=filesize($file_name);
    header('Content-Disposition: attachment; filename=Customer_Feedback_Report.xlsx');
    ob_clean();
    flush();
    readfile($filename);
    exit;
}
?>
<script type="text/javascript">
function initDatePicker() 
{
	$("#datepicker1").datepicker();
	$("#datepicker2").datepicker();
}
$().ready(function() {	
initDatePicker();
});
</script>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
	
	<div class="hsbody-2-right">
	<h4><strong><?=t($t_base.'title/net_promoter_score')?></strong></h4>
	<div id="show_graph" class="clearfix" style="width:720px;">
	<div id="left_div"  style="width:530px;float:left;">
	<div style="position: relative; height: 295px; width: 482px;background: url('<?=__BASE_STORE_IMAGE_URL__?>/graph-bg-line.png') no-repeat 0px 29px;padding: 0 0 0 38px;">
	<?php 
        if(!empty($currntMonthDataArr))
	{
            foreach($currntMonthDataArr as $key=>$values)
            {
                $redTop="top:0px;";
                $redHeight="height:0px;";
                $red_height=0; 
                $redPerCount=round($values['redCount']);
                $yellowPerCount=round($values['yellowCount']);
                $greenPerCount=round($values['greenCount']);


                $red_top=0;
                if($redPerCount>0)
                {
                    $red_top=35;
                    $redTop="top:35px;";
                    $red_height=round((($redPerCount*2.55)));
                    $redHeight="height:".$red_height."px;";
                }

                $yellow_top=0;
                $yellowHeight="0px;";
                $yellow_height=0;

                if($yellowPerCount>0)
                {
                    if((int)$red_height<=0)
                    {
                        $yellow_top=35;
                        $yellowTop="top:35px;";
                    }
                    else
                    {
                        $yellow_top=35+$red_height;
                        $yellowTop="top:".$yellow_top."px;";
                    }
                    $yellow_height=round((($yellowPerCount*2.55)));
                    $yellowHeight="height:".$yellow_height."px;";
                }

                $green_top=0;
                if((int)$red_height<=0 && (int)$yellow_height<=0)
                {	
                    $green_top=35;
                    $greenTop="top:35px;";
                }
                else if((int)$red_height<=0 && (int)$yellow_height>0)
                {
                    $green_top=35+$yellow_height;
                    $greenTop="top:".$green_top."px;";
                }
                else if((int)$red_height>0 && (int)$yellow_height<=0)
                {
                    $green_top=35+$red_height;
                    $greenTop="top:".$green_top."px;";
                }
                else if((int)$red_height>0 && (int)$yellow_height>0)
                {
                    $green_top=35+$yellow_height+$red_height;
                    $greenTop="top:".$green_top."px;";
                }

                $greenHeight="0px;";
                $green_height=0;
                //echo $greenPerCount;
                if($greenPerCount>0)
                {
                        $green_height=round((($greenPerCount*2.55)));
                        $greenHeight="height:".$green_height."px;";
                }

                $totalCount=$green_height+$yellow_height+$red_height;

                $totalTop=$yellow_top+$red_top+$green_top;

                if($totalCount>255)
                {
                    //echo $totalCount;
                    $value=max(array($green_height, $yellow_height, $red_height));
                    $sub_value=$totalCount-255;
                    if($value==$green_height)
                    {
                        $green_height=$green_height-$sub_value;
                        $greenHeight="height:".$green_height."px;";

                        if($yellow_height>0)
                        {
                            if($yellow_top>35)
                            {
                                    $yellow_top=$yellow_top-$sub_value;
                                    $yellowTop="top:".$yellow_top."px;";
                            }
                        }

                        if($red_height>0)
                        {
                            if($red_top>35)
                            {
                                $red_top=35-$sub_value;
                                $redTop="top:".$red_top."px;";
                            }
                        }
                    }
                    else if($value==$yellow_height)
                    {
                        $yellow_height=$yellow_height-$sub_value;
                        $yellowHeight="height:".$yellow_height."px;";

                        if($green_height>0 && $green_top>35)
                        {
                            $green_top=$green_top-$sub_value;
                            $greenTop="top:".$green_top."px;";
                        }


                            if($red_height>0 &&$red_top>35){
                                    $red_top=35-$sub_value;
                                    $redTop="top:".$red_top."px;";
                            }

                    }
                    else if($value==$red_height)
                    {
                        $red_height=$red_height-$sub_value;
                        $redHeight="height:".$red_height."px;";


                        if($yellow_height>0)
                        {
                            if($yellow_top>35)
                            {
                                $yellow_top=$yellow_top-$sub_value;
                                $yellowTop="top:".$yellow_top."px;";
                            }
                        }

                        if($green_height>0 && $green_top>35)
                        {
                            $green_top=$green_top-$sub_value;
                            $greenTop="top:".$green_top."px;";
                        }
                    }
                }
                else if($totalCount<255)
                {

                    $value=max(array($green_height, $yellow_height, $red_height));
                    $sub_value=255-$totalCount;
                    if($value==$green_height)
                    {
                        $green_height=$green_height+$sub_value;
                        $greenHeight="height:".$green_height."px;";

                    }
                    else if($value==$yellow_height)
                    {
                        $yellow_height=$yellow_height+$sub_value;
                        $yellowHeight="height:".$yellow_height."px;";

                        if($green_height>0)
                        {
                            $green_top=$green_top+$sub_value;
                            $greenTop="top:".$green_top."px;";
                        }
                    }
                    else if($value==$red_height)
                    {
                        $red_height=$red_height+$sub_value;
                        $redHeight="height:".$red_height."px;";

                        if($yellow_height>0)
                        {
                            $yellow_top=$yellow_top+$sub_value;
                            $yellowTop="top:".$yellow_top."px;";
                        }

                        if($green_height>0)
                        {
                            $green_top=$green_top+$sub_value;
                            $greenTop="top:".$green_top."px;";
                        }
                    }
                } 
            ?>
	<div class="C" style="height:295px;">
	<div class="DW1"><i><?=$values['totalCount']?></i></div>
	<?php if($greenPerCount>0){?> 
	<img class="G" style="<?=$greenTop?><?=$greenHeight?>" src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/sumBarGreen.png" title="<?=$greenPerCount?>%">
	<?php }if($yellowPerCount>0){?>
	<img class="G" style="<?=$yellowTop?><?=$yellowHeight?>" src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/sumBarYellow.png" title="<?=$yellowPerCount?>%">
	<?php } if($redPerCount>0){?>
	<img class="G" style="<?=$redTop?><?=$redHeight?>" src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/sumBarOrange.png" title="<?=$redPerCount?>%">
	<?php }?>
	<div class="DW"><?=strtoupper($key)?></div>
	</div>
	<?php }}?>
	</div>
	</div>
	<div id="right_div" style="width: 132px; float: right;margin: 40px 0 0;">
	<div style="margin-left:10px;">
	<p class="oh">
	<img style="width:15px;height:15px;float: left; margin: 3px 5px 0px 0px;" src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/sumBarOrange.png" /> 
	<span class="fl-80"><?=t($t_base.'fields/Detractors')?></span>
	</p>
	<p class="oh">
	<img style="width:15px;height:15px;float: left; margin: 3px 5px 0px 0px;" src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/sumBarYellow.png" /> 
	<span class="fl-80"><?=t($t_base.'fields/Passives')?></span>
	</p>
	<p class="oh">
	<img style="width:15px;height:15px;float: left; margin: 3px 5px 0px 0px;" src="<?=__MAIN_SITE_HOME_PAGE_URL__?>/images/sumBarGreen.png" /> 
	<span class="fl-80"><?=t($t_base.'fields/Promoters')?></span>
	</p>
	</div>
	
	<div style="padding: 4px 10px 8px;border:solid 1px #BFBFBF;margin-top:10px;">
	<form name="downloadFeedBackData" id="downloadFeedBackData" method="post">
	<?php
		if(!empty($kNPS->arErrorMessages)){
		?>
		<div id="download_data_id">
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup remove-shippers-popup">
		<div id="regError" class="errorBox ">
		<div class="header"><?=t($t_base_error.'/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kNPS->arErrorMessages as $key=>$values)
			{
			?><li><?=$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<p align="center"><a href="javascript:void(0)" onclick="cancel_remove_user_popup('download_data_id','hsbody-2');" class="button1"><span><?=t($t_base.'fields/close')?></span></a></p>
		</div>
		</div>
		</div>
		<?php }?>
	<h4><?=t($t_base.'title/download_data')?></h4>
	<p><?=t($t_base.'fields/from_date')?><br/><input size="10" id="datepicker1" name="downloadFeedbackData[dtFromDownload]" type="text" value="" onBlur="open_download_button();"> </p>
	<p><?=t($t_base.'fields/to_date')?><br/><input size="10"   id="datepicker2" name="downloadFeedbackData[dtToDownload]" type="text" value="" onBlur="open_download_button();"></p><br/>
	<p align="center"><a href="javascript:void(0)" class="button1" id="download_button" style="opacity:0.4;"><span style="min-width:72px"><?=t($t_base.'fields/download')?></span></a></p>
	</form>
	</div>
	
	</div>
	</div> 
        <div style="clear: both;"></div>
        <br/><br/><br/>
        <h4><strong><?=t($t_base.'title/last_three_months_comment')?></strong></h4>
	<div id="show_copy_data" class="tableScroll" style="height:436px;overflow: auto;padding: 7px;"> 
            <?php 
                if(!empty($getLastThreeMonthCommentArr))
                {
                    foreach($getLastThreeMonthCommentArr as $getLastThreeMonthCommentArrs)
                    {
                        /*$starText='';
                        if((int)$getLastThreeMonthCommentArrs['iScore']>0)
                        {
                                $starText=$getLastThreeMonthCommentArrs['iScore']." ".t($t_base.'fields/stars'); 
                        }
                        else
                        {
                                $starText=$getLastThreeMonthCommentArrs['iScore']." ".t($t_base.'fields/star');
                        }*/
                        echo '<p style="font-size: 14px;margin: 0 0 10px 7px;"><em>'.date('d/m/Y H:i',strtotime($getLastThreeMonthCommentArrs["dtComment"])).' - '.$getLastThreeMonthCommentArrs["szComment"].'</em></p>';
                    }
                }
            ?>
	</div> 
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>