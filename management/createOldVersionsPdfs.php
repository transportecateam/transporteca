<?
function saveOldVersion()
{
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require(__APP_PATH__.'/forwarders/html2pdf/html2fpdf.php');
validateManagement();
$pdf=new HTML2FPDF();
$pdf->AddPage();
$kForwarder = new cForwarder;
$version=$kForwarder->selectVersion();
$version=date('dFY',strtotime($version['dtVersionUpdated']));
$order=$kForwarder->getMaxTncOrder();

$version="Version-".$version."_".time();
$fp = fopen(__APP_PATH__."/forwarders/html2pdf/terms_condition_pdf.html","r");
$strContent = fread($fp, filesize(__APP_PATH__."/forwarders/html2pdf/terms_condition_pdf.html"));

fclose($fp);
$strContent=$strContent."".$str;
$pdf->WriteHTML($strContent);
$pdf->Output(__APP_PATH__."/management/versions/".$version.".pdf");
return 	__BASE_URL__."/versions/".$version.".pdf";
}
?>