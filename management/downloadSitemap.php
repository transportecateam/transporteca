<?php 
/**
  *  admin--- maintanance -- contries
  */
define('PAGE_PERMISSION','__SYSTEM_DOWNLOAD__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Sitemap Video Script";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");

$fileurl = __APP_PATH_ROOT__."/sitemap.xml";

if(file_exists($fileurl))
{
	$f = fopen($fileurl,'rb');
	$fsize=filesize($fileurl);
	$filename = "sitemap.xml";
	header('Content-Disposition: attachment; filename='.$filename);
	header('Content-Length:'.$fsize);
	header('Content-type: "text/xml"; charset="utf8"');
	fpassthru($f);
	fclose($f);
	exit;
}
else
{
	echo "<font color=red>llInvalid filename.</font>";	
	exit;
}	

include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	