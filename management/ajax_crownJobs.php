<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$t_base="management/Error/";
validateManagement();
if(isset($_POST))
{

	$mode= trim(sanitize_all_html_input($_POST['mode']));
	if($mode == 'UPDATE_CR_JOBS')
	{	
		$id			=	trim(sanitize_all_html_input($_POST['id']));
		$crownJobs	=	trim(sanitize_all_html_input($_POST['crownJobs']));
		$kAdmin->updateCrownJobs($crownJobs,$id);
		
	}
}