<?php 
/**
  *BILLING DETAILS
  */
define('PAGE_PERMISSION','__FINANCIAL__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$addFile=1;
$szMetaTitle="Transporteca | Accounts Receivable - Outstanding";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/billings/";
validateManagement();
$billingDetails=$kAdmin->showBookingConfirmedTransaction();
//print_r($billingDetails);

if(empty($_SESSION['szCustomerTimeZoneGlobal']))
{
    $ret_ary = array();
    $ret_ary = getCountryCodeByIPAddress();
    if(empty($ret_ary))
    {
        $ret_ary['szCountryCode'] = 'DK';
    } 
    $szCustomerTimeZone = get_time_zone($ret_ary['szCountryCode'],$ret_ary['szRegion']); 
    $_SESSION['szCustomerTimeZoneGlobal'] = $szCustomerTimeZone ; 
} 
$billFlag=checkManagementPermission($kAdmin,"__OUTSTANDING__",2);
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
    <?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>

    <div id="checkDetails" class="hsbody-2-right">
        
        <?php if($billFlag){ showCustomerAdminBilling($billingDetails,$idAdmin); } ?>
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
