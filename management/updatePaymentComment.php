<?php
/**
 * Edit User Information
 */

  ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$t_base="management/billings/";
$idPayment = sanitize_all_html_input(trim($_REQUEST['id']));
	$szBookingRef = sanitize_all_html_input(trim($_REQUEST['ref']));
	$szComment=sanitize_all_html_input(trim($_REQUEST['szComment']));

if(!empty($_REQUEST['updateInvoiceAry']))
{	
	$idPayment=sanitize_all_html_input(trim($_REQUEST['idPayment']));
	$szNewComment=sanitize_all_html_input(trim($_REQUEST['szComment']));
	//$kBooking->set_szInvoiceForwarderReference(sanitize_all_html_input(trim($_REQUEST['szForwarderReference'])),true);
	
	if($idPayment>0)
	{
		$idPayment = $idPayment ;
		if($kAdmin->updatePaymentComment($szNewComment,$idPayment));
		{	
			echo "SUCCESS||||".$idPayment."||||".createSubString($szNewComment,0,25);
			die;
		}
	}
}
?><div id="popup-bg"></div>
<div id="popup-container">	
	<div class="compare-popup popup" >
		<h5><strong><?=t($t_base.'title/booking_reference');?> <?=$szBookingRef?></strong></h5>
		<p><?=t($t_base.'title/type_reference');?>:</p>
		<p>
			<input type="text"  name="updateInvoiceAry[szForwarderReference]" onkeypress="return submitForm(event)"  id="fwdReference"  value="<?=$szComment?>" size="43" AUTOCOMPLETE ="off">
		</p>
		<br>
		<p align="center">
			<input type="hidden" id="idPayment" name="updateInvoiceAry[idPayment]" value="<?=$idPayment?>">
			<input type="hidden" id="szRef" name="updateInvoiceAry[szBookingRef]" value="<?=$szBookingRef?>">
			<a href="javascript:void(0)" class="button1" onclick="update_payment_comment_confirm()"><span><?=t($t_base.'fields/save');?></span></a>
			<a href="javascript:void(0)" class="button2" onclick="showHide('invoice_comment_div')"><span><?=t($t_base.'fields/cancel');?></span></a>&nbsp;
		</p>
	</div>
</div>