<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$t_base = "management/myprofile/";
$t_base_error = "Error";
$showflag=$_REQUEST['showflag'];

if(!empty($_POST['editUserInfoArr']) && $showflag=="user_info")
{
    if($kAdmin->updateAdmininfo($_POST['editUserInfoArr'],$_SESSION['admin_id']))
    {			
        $successFlag=true;	
        echo "<script>$('#admin_name').html('".$kAdmin->szFirstName." ".$kAdmin->szLastName."')</script>";	
    }
}
if(!empty($_POST['editUserInfoArr']) && $showflag=="contact_info")
{
    if($kAdmin->updateAdminContact_info($_POST['editUserInfoArr'],$_SESSION['admin_id']))
    {
        $successContactUpdatedFlag=true;
    }
 }
 
if(!empty($_POST['editUserInfoArr']) && $showflag=="change_pass")
{
    if($kAdmin->updateAdminPassword_info($_POST['editUserInfoArr'],$_SESSION['admin_id']))
    {
        $successPasswordUpdatedFlag=true;
    }
} 

$kAdmin->getAdminDetails($_SESSION['admin_id']);

if($showflag=='user_info' && !$successFlag)
{
	if(!empty($kAdmin->arErrorMessages))
{
?>
	<div id="regError" class="errorBox">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kAdmin->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
?>
	</ul>
	</div>
	</div>
<?php 
}
?>	
<div id="user_information">
<h5><strong><?=t($t_base.'fields/forwarder_info');?></strong></h5>
<form name="updateUserInfo" id="updateUserInfo" method="post">
    <label class="profile-fields">
        <span class="field-name"><?=t($t_base.'fields/title');?></span>
        <span class="field-container"><input type="text" name="editUserInfoArr[szTitle]" id="szTitle" value="<?=!empty($_POST['editUserInfoArr']['szTitle']) ? $_POST['editUserInfoArr']['szTitle'] : $kAdmin->szTitle ?>"/></span>
    </label>
    <label class="profile-fields">
        <span class="field-name">  <?=t($t_base.'fields/f_name');?>  </span>
        <span class="field-container"><input type="text" name="editUserInfoArr[szFirstName]" id="szFirstName" value="<?=!empty($_POST['editUserInfoArr']['szFirstName']) ? $_POST['editUserInfoArr']['szFirstName'] : $kAdmin->szFirstName?>"/> </span>
    </label>
    <label class="profile-fields">
        <span class="field-name"><?=t($t_base.'fields/l_name');?></span>
        <span class="field-container"><input type="text" name="editUserInfoArr[szLastName]" id="szLastName" value="<?=!empty($_POST['editUserInfoArr']['szLastName']) ? $_POST['editUserInfoArr']['szLastName'] : $kAdmin->szLastName ?>"/></span>
    </label> 
    <input type='hidden' name='showflag' value="<?=$showflag;?>">
    <br/>
    <p align="center" style="width:60%"><a href="javascript:void(0)" class="button1" onclick="javscript:update_forwarder_user_info();"><span><?=t($t_base.'fields/save');?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_forwarder_info();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
</form>	
</div><br/><br/>
<?php }else{?>
    <div id="user_information">
        <h5><strong><?=t($t_base.'fields/forwarder_info');?></strong> <a href="javascript:void(0)" onclick="editForwarderUserInformation();"><?=t($t_base.'fields/edit');?></a></h5>	
        <div class="ui-fields">
            <span class="field-name"><?=t($t_base.'fields/title');?></span>
            <span class="field-container"><?=$kAdmin->szTitle?></span>
        </div>
        <div class="ui-fields">
            <span class="field-name"><?=t($t_base.'fields/f_name');?></span>
            <span class="field-container"><?=$kAdmin->szFirstName?></span>
        </div> 
        <div class="ui-fields">
            <span class="field-name"><?=t($t_base.'fields/l_name');?></span>
            <span class="field-container"><?=$kAdmin->szLastName?></span>
        </div> 
    </div>
    <br/><br/>
			
<?php } if($showflag=='contact_info' && !$successContactUpdatedFlag){
    
    $kConfig = new cConfig();
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
    
    if(isset($_POST['editUserInfoArr']['idInternationalDialCode']))
    {        
        $idInternationalDialCode = $_POST['editUserInfoArr']['idInternationalDialCode'] ;
    }
    else
    {
        $idInternationalDialCode = $kAdmin->idInternationalDialCode ;
    }
    
    if(!empty($kAdmin->arErrorMessages)){
?>
<div id="regError" class="errorBox">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kAdmin->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php } ?>
    <div id="contact_information">
        <h5><strong><?=t($t_base.'fields/contact');?></strong></h5>
        <form name="updateContactInfo" id="updateContactInfo" method="post">
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/email');?></span>
                <span class="field-container"><input type="text" name="editUserInfoArr[szEmail]" id="szEmail" value="<?=$_POST['editUserInfoArr']['szEmail'] ? $_POST['editUserInfoArr']['szEmail'] : $kAdmin->szEmail?>" /></span>
            </label> 
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/p_number');?><span></span></span>
                <span class="field-container">
                    <select size="1"  name="editUserInfoArr[idInternationalDialCode]" id="idInternationalDialCode">
                       <?php
                          if(!empty($dialUpCodeAry))
                          {
                               $usedDialCode = array();
                              foreach($dialUpCodeAry as $dialUpCodeArys)
                              {
                                   if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                   {
                                       $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                   ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idInternationalDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                   <?php
                                   }
                              }
                          }
                      ?>
                    </select>
                    <input type="text" style="width:107px;" name="editUserInfoArr[szPhoneNumber]" id="szPhoneNumber" value="<?=$_POST['editUserInfoArr']['szPhoneNumber'] ? $_POST['editUserInfoArr']['szPhoneNumber'] : $kAdmin->szPhone ?>" />
                   </span>
            </label>
            <input type="hidden" name="editUserInfoArr[szOldEmail]" value="<?=$kAdmin->szEmail;?>"> 
            <input type='hidden' name='showflag' value="<?=$showflag;?>">
            <br/>
            <p align="center" style="width:60%"><a href="javascript:void(0)" class="button1" onclick="javscript:update_forwarder_contact_info_details();"><span><?=t($t_base.'fields/save');?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_forwarder_info();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
        </form>	
    </div>
    <br/><br/>
<?php }else{ 
    
    $kConfig = new cConfig();
    $kConfig->loadCountry($kAdmin->idInternationalDialCode);
    $iInternationDialCode = $kConfig->iInternationDialCode;
    
    ?>
    <div id="contact_information">
        <h5><strong><?=t($t_base.'fields/contact');?></strong> <a href="javascript:void(0)" onclick="editForwarderContactInformation();"><?=t($t_base.'fields/edit');?></a></h5>	
	
					<div class="ui-fields" style="height:auto;min-height:26px;">
					<span class="field-name"><?=t($t_base.'fields/email');?></span>
					<span class="field-container"><?=$kAdmin->szEmail?>
					<?php if($kAdmin->iActive!='1')
			 {?><span style='color:red;font-size:13px;'>(<?=t($t_base.'messages/not_verified');?>)</span>
					<? if($successContactUpdatedFlag) {?>
					
					<div style="background: #b6dde8;clear: both;margin:0 0 0 216px;width: 486px;padding:5px 10px 8px;">
					<div id="activationkey" style="display:none";></div>
					<p><?=t($t_base.'messages/click_the_link');?> <?=$_POST['editUserInfoArr']['szEmail']?> <?=t($t_base.'messages/complete_your_account');?></p>
					
					<p><a href="javascript:void(0)" onclick="resendActivationCode('<?=$_SERVER['HTTP_REFERER']?>');"><?=t($t_base.'messages/resend_email');?></a>
					<br /></p>
					</div>
		
			<?php }}?>
					</span>
					</div>
                <div class="ui-fields">
			<span class="field-name"><?=t($t_base.'fields/p_number');?> <span></span></span>
			<span class="field-container"><?php echo "+".$iInternationDialCode." ".$kAdmin->szPhone; ?></span>
		</div>
					
			
			</div><br/><br/>
			
			<? }if($showflag=='change_pass' && !$successPasswordUpdatedFlag){ 
			if(!empty($kAdmin->arErrorMessages)){
?>
<div id="regError" class="errorBox">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kAdmin->arErrorMessages as $key=>$values)
	{
		?><li><?=$values?></li>
		<?php	
	}
?>
</ul>
</div>
</div>
<?php }
			?>
			<div id="password_information">
		
		<form name="changePassword" id="changePassword" method="post">
		<h5><strong><?=t($t_base.'fields/password');?></strong>	</h5>	
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/current_password')?></span>
			<span class="field-container" ><input type="password" name="editUserInfoArr[szOldPassword]" id="szOldPassword"/></span>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/password');?></span>
			<span class="field-container" ><input type="password" name="editUserInfoArr[szNewPassword]" id="szNewPassword" /></span>
			
		</label>
		
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/con_new_password')?></span>
			<span class="field-container" ><input type="password" name="editUserInfoArr[szConPassword]" id="szConPassword"/></span>
		</label>
		<br />
		<input type='hidden' name='showflag' value="<?=$showflag;?>">
		<p align="center" style="width:60%"><a href="javascript:void(0);" class="button1" onclick="changeForwarderPassword();"><span>Save</span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_forwarder_password();"><span><?=t($t_base.'fields/cancel');?></span></a> </p>
	</form>
			</div><br/><br/>
			<? }else{ ?>
			<div id="password_information">
						<h5 style="width:30%;float:left;"><strong><?=t($t_base.'fields/password');?></strong> <a href="javascript:void(0);" onclick="change_forwarder_password();"><?=t($t_base.'fields/edit');?></a></h5>
						<span class="field-container">*************</span>
						<?php
		if($successPasswordUpdatedFlag)
		{
			echo "<div style='clear: both;'></div><p style=color:green>".t($t_base.'messages/password_change_successfully')."</p>";
		}
	?>
			</div><br/><br/>
			<? } ?>
			<div style="clear: both"></div>
	