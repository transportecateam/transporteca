<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );	
if(isset($_POST['FLAG']))
{
	if($_POST['value'] =='Show less')
	{
		$_SESSION['message']='hide';
	}
	if($_POST['value'] =='Show more')
	{
		unset($_SESSION['message']);
	}
}
?>
