<?php
ob_start();
session_start(); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );

$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$kExplain = new cExplain();
$t_base = "management/uploadService/";
if($mode=='OPEN_SEARCH_NOTIFICATION_DELETE_CONFIRMATION_POPUP')
{
    $idSearchNoti = (int)$_POST['idSearchNoti'];
    $iPrivate = (int)$_POST['iPrivate'];
    if($iPrivate==1)
    {
        $szOperationType = 'DELETE_PRIVATE_CUSTOMER_SEARCH_NOTIFICATION';
    }
    else
    {
        $szOperationType = 'DELETE_SEARCH_NOTIFICATION';
    }
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup">
            <h3><?=t($t_base.'title/delete_search_notification');?></h3><br/>
            <p><?php echo t($t_base.'messages/delete_search_notification_msg');?></p>
            <div class="btn-container">
                <p align="center">
                    <a href="javascript:void(0)" id="delete-button" onclick="deleteSearchNotification('<?=$idSearchNoti?>','<?php echo $szOperationType; ?>')" class="button1" ><span><?=t($t_base.'fields/delete');?></span></a>
                    <a href="javascript:void(0)" id="cancel-button" class="button1" onclick="closeTip('search_noftification_popup')"><span><?=t($t_base.'fields/cancel');?></span></a>
                </p>
            </div>
        </div>
    </div>
<?php
}
else if($mode=='DELETE_SEARCH_NOTIFICATION')
{
   $idSearchNoti = (int)$_POST['idSearchNoti']; 
   if((int)$idSearchNoti>0)
   {
       $kExplain->deleteSearchNotification($idSearchNoti);
   } 
   echo searchNotificationListingHtml(); 
}
else if($mode=='DELETE_PRIVATE_CUSTOMER_SEARCH_NOTIFICATION')
{
   $idSearchNoti = (int)$_POST['idSearchNoti']; 
   if((int)$idSearchNoti>0)
   {
       $kExplain->deletePrivateCustomerSearchNotification($idSearchNoti);
   } 
   echo searchNotificationListingPrivateCustomerHtml(); 
}
else if($mode=='OPEN_ADD_SEARCH_NOTIFICTION_FORM')
{
   $idSearchNoti = (int)$_POST['idSearchNoti']; 
   if((int)$idSearchNoti>0)
   {
        $kExplain = new cExplain();
        $searchNotificationArr=$kExplain->getSearchNotificationListing($idSearchNoti);
        $_POST['addEditSearchNotificationAry']=$searchNotificationArr[0];
        $buttonText=$searchNotificationArr[0]['idButtonType'];
        $_POST['addEditSearchNotificationAry']['idButtonType']=  explode(";", $buttonText);
        $_POST['addEditSearchNotificationAry']['idSearchNoti']=$idSearchNoti;
        
        if($_POST['addEditSearchNotificationAry']['szFromCountry']=='Any')
        {
            $_POST['addEditSearchNotificationAry']['idFromCountry']='All';
        }
        
        if($_POST['addEditSearchNotificationAry']['szToCountry']=='Any')
        {
            $_POST['addEditSearchNotificationAry']['idToCountry']='All';
        }
   } 
   searchNotificationAddEditForm();
   die;
}
else if($mode=='OPEN_ADD_SEARCH_NOTIFICTION_FORM_PRIVATE_CUSTOMER')
{
    $idSearchNoti = (int)$_POST['idSearchNoti']; 
    $searchNotificationArrs = array();
    if((int)$idSearchNoti>0)
    {
        $kExplain = new cExplain();
        $searchNotificationArr = $kExplain->getSearchNotificationListingPrivateCustomer($idSearchNoti); 
        $searchNotificationArrs = $searchNotificationArr[0];
    }
    echo searchNotificationAddEditPrivateCustomerForm($kExplain,$searchNotificationArrs);
    die;
}
else if($mode=='ADD_UPDATE_DATA_SUBMIT')
{ 
    if($kExplain->addUpdateSearchNotificationData($_POST['addEditSearchNotificationAry']))
    {
        echo "SUCCESS||||";
        echo searchNotificationListingHtml();
        die();
    }
    else
    {
        echo "ERROR||||";
        echo searchNotificationAddEditForm();
        die();
    }
}
else if($mode=='ADD_UPDATE_PRIVATE_CUSTOMER_DATA_SUBMIT')
{ 
    if($kExplain->addUpdateSearchNotificationDataPrivateCustomer($_POST['addEditSearchNotificationAry']))
    {
        echo "SUCCESS||||";
        echo searchNotificationListingPrivateCustomerHtml();
        die();
    }
    else
    {
        echo "ERROR||||";
        echo searchNotificationAddEditPrivateCustomerForm($kExplain);
        die();
    }
}

?>
