<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$t_base="management/Error/";
validateManagement();
?>

<?
if(isset($_POST))
{
	if(isset($_POST['mode']))
	{
		$mode=sanitize_all_html_input($_POST['mode']);
		
		if($mode=='VIEW_EMAIL_CONTENT')
		{
			$id=sanitize_All_html_input($_POST['id']);
			$text=$kAdmin->viewBodyContent($idAdmin,$id);
			if($text!='')
			{		
				$content=html_entity_decode($text['szEmailBody']);
				$len=strlen($content);
				if($len>1000)
				{	
					echo substr($content,0,1000)." <br /><b>...more content</b>";
				
				}
				else
				{
					echo $content;
				}
			}
			else
			{
				echo "<b>empty email sent</b>";
			}
		}
		if($mode=='SEARCHING')
		{	
			$email = sanitize_all_html_input($_POST['email']);
			$date  = sanitize_all_html_input($_POST['date']);
			$count= $kAdmin->selectMaxEmail($email,$date);
			if($email || $date)
			{	
				$data=$kAdmin->searchEmailLogContent($idAdmin,$email,$date,1);
				emailLogTable($data,$count);
			}
			else if(!($email && $date))
			{
				$emailLog=$kAdmin->emailLog(1);
				emailLogTable($emailLog,$count);
			}
		}
		
		if($mode=='PAGINATION')
		{	
			$email = sanitize_all_html_input($_POST['email']);
			$date  = sanitize_all_html_input($_POST['date']);
			$page  = sanitize_all_html_input($_POST['Page']);
			$count= $kAdmin->selectMaxEmail($email,$date);
			if(isset($_POST['Page']) && $_POST['Page']>0 && $email=='' && $date=='')
			{	
				$emailLog=$kAdmin->emailLog($page);	
				emailLogTable($emailLog,$count,$page);
			}	
			if($email || $date)
			{	
				$data=$kAdmin->searchEmailLogContent($idAdmin,$email,$date,$page);
				emailLogTable($data,$count,$page);
			}
		}
		
		
	}
}
	?>
	