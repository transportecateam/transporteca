<?php 
/**
  *  admin--- Search Notification management
  */
define('PAGE_PERMISSION','__SEARCH_USER_LIST__'); 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Search User List";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
 validateManagement();
$t_base = "management/uploadService/";
$kConfig = new cConfig();
$allCountriesArr = array();
$allCountriesArr=$kConfig->getAllCountries(true);
  
$dtFromDate = '01/11/2016';
$dtToDate = '30/11/2016'; 

?>
<div id="hsbody-2">
    <div id="search_noftification_popup" style="display: none;"></div>
    <?php require_once(__APP_PATH_LAYOUT__ ."/admin_messages_left_nav.php"); ?> 
    <div class="hsbody-2-right"> 
        <h4><strong>Voga Email Listing</strong></h4> 
        <form id="vogaQuoteEmailForm" name="vogaQuoteEmailForm">
            <div class="clearfix email-fields-container" style="width:100%;">
                <span class="quote-field-container wd-25">
                   <span class="input-title">From<br></span>
                   <span class="input-fields">  
                        <input type="text" name="vogaQuoteEmailLogsAry[dtFromDate]" id="dtFromDate" value="<?php echo $dtFromDate?>">
                   </span>
                </span>
                <span class="quote-field-container wds-25">
                  <span class="input-title">To country<br></span>
                  <span class="input-fields">  
                      <input type="text" name="vogaQuoteEmailLogsAry[dtToDate]" id="dtToDate" value="<?php echo $dtToDate; ?>">
                  </span>
                </span> 
                <input type="hidden" name="vogaQuoteEmailLogsAry[idLandingPage]" value="16">
                <span class="quote-field-container wd-30" style="text-align:right;">  
                    <span class="input-title">&nbsp;<br></span>
                    <a id="search_user_list_download" class="button1" href="javascript:void(0);" onclick="display_voga_email_data();">
                        <span id="search_user_list_download_text"><?php echo 'Search'; ?></span>
                    </a> 
               </span>
            </div>  
        </form>
        <div class="clearfix">&nbsp;</div><br>
        <div id="display_voga_email_contents">
            
        </div>
    </div>
</div>  

<script type="text/javascript">
    function initDatePicker() 
    {
        $("#dtFromDate").datepicker(); 
        $("#dtToDate").datepicker(); 
    }
    initDatePicker();
</script>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); 
?>