<?php 
/**
  *  admin--- CfsList
  */
define('PAGE_PERMISSION','__CFS_LOCATION_LIST__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | CFS Location - List";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base	= "management/cfsLoctation/";

?>
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php" ); ?>
	<div class="hsbody-2-right">
		<?php showCurrentCFSListTopForm($t_base);?>
		<div id="lisingTop">
			<?php //showCfsListing($t_base,$warehouseDetailArr,$idAdmin);?>
		</div>
		
		<div style="clear: both;"></div>
		<br/>
		<div id="detailsBottom">
			<?php //addEditWarehouse($t_base);?>
		</div>
</div>
</div>
<?php 
	include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	