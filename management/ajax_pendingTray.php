<?php 
/**
  *  Admin---> System --> Insurance
  */ 
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");

require_once( __APP_PATH_CLASSES__.'/booking.class.php' );
require_once( __APP_PATH_CLASSES__.'/warehouseSearch.class.php' );
require_once( __APP_PATH_CLASSES__.'/vatApplication.class.php' ); 
require_once( __APP_PATH_CLASSES__.'/tracking.class.php' );
require_once( __APP_PATH_CLASSES__.'/chat.class.php' );
require_once( __APP_PATH_CLASSES__.'/pendingTray.class.php' );

$t_base="management/pendingTray/";
$t_base_error="management/Error/";
validateManagement();
 
$kBooking = new cBooking(); 
$kConfig = new cConfig();
$kCrm = new cCRM();
$kSendEmail = new cSendEmail();
$kPendingTray = new cPendingTray();
$mode = sanitize_all_html_input(trim($_REQUEST['mode'])); 
$action = sanitize_all_html_input(trim($_REQUEST['action'])); 
//$szOperationType = sanitize_all_html_input(trim($_REQUEST['type'])); 

if($mode=='DISPLAY_PENDING_TASK_DETAILS')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $kBooking->loadFile($idBookingFile);
    
    if($kBooking->idBookingFile>0)
    {
        /*
        * Adding file visited logs
        */
        $kPendingTray->addBookingFileVisitedLogs($idBookingFile);
        
        echo "SUCCESS||||";
         /*
        if($kBooking->szTaskStatus!='T140' && $kBooking->szTaskStatus!='T150')
        {
            echo display_pending_tray_overview($kBooking);
        } 
        * 
        */
        echo display_pending_tray_overview($kBooking);
        echo "||||";
        echo display_pending_task_tray_container($kBooking);
        die;
    }
    die;
} 
else if($mode=='REMOVE_CRM_EMAIL_MESSAGE')
{
    $idCrmEmail = sanitize_all_html_input(trim($_REQUEST['idEmail'])); 
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page'])); 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['booking_file'])); 
    
    $emailInputAry = array();
    $emailInputAry['idCrmEmail'] = $idCrmEmail;
    $emailInputAry['szFromPage'] = $szFromPage;
    $emailInputAry['idBookingFile'] = $idBookingFile;
    
    echo "SUCCESS||||";
    echo display_remove_crm_popup($emailInputAry);
    die;
} 
else if(!empty($_POST['removeCrmEmailAry']))
{
    $idBookingFile = sanitize_all_html_input(trim($_POST['removeCrmEmailAry']['idBookingFile'])); 
    $szFromPage = sanitize_all_html_input(trim($_POST['removeCrmEmailAry']['szFromPage'])); 
    
    $kSendEmail = new cSendEmail();
    if($kSendEmail->deleteCrmEmail($_POST['removeCrmEmailAry']))
    {
        if($kSendEmail->iRefreshPendingTrays==1)
        {
            $searchTaskAry = array();  
            if(!empty($_SESSION['sessTransportecaTeamAry']))
            {
                $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
            }
            else
            {
                $searchTaskAry['idFileOwnerAry'][0] = 0;
                $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
            }
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
            
            echo "REFRESH_PENDING_TRAYS||||";
            echo display_all_peding_tasks($incompleteTaskAry);  
            die;
        }
        else
        {
            $kBooking->loadFile($idBookingFile);
            echo "SUCCESS||||";
            echo display_pending_tray_email_logs($kBooking,$szFromPage);
        } 
    }
    die;
}
else if(!empty($_POST['addCrmSnoozeAry']))
{
    $idBookingFile = sanitize_all_html_input(trim($_POST['addCrmSnoozeAry']['idBookingFile'])); 
    $szFromPage = sanitize_all_html_input(trim($_POST['addCrmSnoozeAry']['szFromPage'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['addCrmSnoozeAry']['idCrmEmailLogs']));  
    $szAction = sanitize_all_html_input(trim($_REQUEST['action'])); 
     
    $kSendEmail = new cSendEmail();
    $addCrmSnoozeAry = array();
    $addCrmSnoozeAry = $_POST['addCrmSnoozeAry'];
    $addCrmSnoozeAry['iCrmSnoozeClose'] = 1;
    
    if($szAction=='CRM_SNOOZE' || $szAction=='CRM_CLOSE')
    {  
        if($szAction=='CRM_SNOOZE')
        {   
            $addCrmSnoozeAry['szOperationType'] = "SNOOZE"; 
        }
        else if($szAction=='CRM_CLOSE')
        {
            $addCrmSnoozeAry['szOperationType'] = "CLOSE_TASK"; 
            $addCrmSnoozeAry['szActionType'] = "CRM_CLOSE"; 
        }
        
        //$kCrm->addCrmEmailSnooze($addCrmSnoozeAry) 
        if(is_base64_encoded($addCrmSnoozeAry['szReminderEmailBody']))
        {
            $addCrmSnoozeAry['szReminderEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($addCrmSnoozeAry['szReminderEmailBody']))); 
            $_POST['addCrmSnoozeAry']['szReminderEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['addCrmSnoozeAry']['szReminderEmailBody']))); 
        }
        if($kBooking->addBookingFileSnooze($addCrmSnoozeAry))
        {
            $kBooking->loadFile($idBookingFile); 
            $searchTaskAry = array();  
            if(!empty($_SESSION['sessTransportecaTeamAry']))
            {
                $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
            }
            else
            {
                $searchTaskAry['idFileOwnerAry'][0] = 0;
                $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
            }
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
             
            echo "SUCCESS||||";
            echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true);  
            echo "||||"; 
            echo display_pending_tray_email_logs($kBooking,$szFromPage);
            if($kBooking->iReassignCrmLogs==1 && $szAction=='CRM_CLOSE')
            {
                echo "||||CLOSE_OVERVIEW_PANE";
            }
            die;  
        }
        else
        {
            $logEmailsArys = array();
            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
            echo "ERROR||||";
            echo display_crm_email_buttons($logEmailsArys,$idBookingFile,$szFromPage,$kBooking);
            die;
        }
    } 
         
       /* 
    if($szAction=='CRM_CLOSE')
    {
        if($kCrm->closeCrmEmail($_POST['addCrmSnoozeAry']))
        {
            $kBooking->loadFile($idBookingFile);
            echo "SUCCESS||||"; 
            echo display_pending_tray_email_logs($kBooking,$szFromPage);
            die;
        }
        else
        {
            $logEmailsArys = array();
            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
            echo "ERROR||||";
            echo display_crm_email_buttons($logEmailsArys,$idBookingFile,$szFromPage,$kCrm);
            die;
        }
    }
    * 
    */
    die;
}
else if($mode=='CRM_EMAIL_MARK_READ')
{
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['email_id'])); 
    $idBookingFile = sanitize_all_html_input(trim($_POST['file_id'])); 
    if($kCrm->markCrmMailSeen($idCrmEmailLogs,$idBookingFile))
    {
        echo "SUCCESS||||";
    }
    die;
}
else if($mode=='UPDATE_TASK_OWNER')
{ 
    $idBookingFile = sanitize_all_html_input(trim($_POST['file_id']));  
    $idFileOwner = sanitize_all_html_input(trim($_POST['admin_id']));  
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['email_id'])); 
     
    if($idBookingFile>0)
    {
        $fileLogsAry=array();
        $fileLogsAry['idFileOwner'] = $idFileOwner;
        $fileLogsAry['idAdminUpdatedBy'] = $_SESSION['admin_id'];
 
        if(!empty($fileLogsAry))
        {
            foreach($fileLogsAry as $key=>$value)
            {
                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }  
        $file_log_query = rtrim($file_log_query,",");  
        if($kBooking->updateBookingFileDetails($file_log_query,$idBookingFile))
        {
            if(!empty($_SESSION['sessTransportecaTeamAry']))
            {
                $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
            }
            else
            {
                $searchTaskAry['idFileOwnerAry'][0] = 0;
                $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
            }
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
            echo "SUCCESS||||";
            echo display_all_peding_tasks($incompleteTaskAry);  
        }
    }
    die;
}
else if($mode=='CRM_BUTTON_OPERATION_DETAILS')
{
    $szAction = sanitize_all_html_input(trim($_POST['action'])); 
    $idBookingFile = sanitize_all_html_input(trim($_POST['booking_file'])); 
    $szFromPage = sanitize_all_html_input(trim($_POST['from_page'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['idEmail'])); 
    $szCrmEmail = sanitize_all_html_input(trim($_POST['szCrmEmail'])); 
    
    $logEmailsArys = array();
    $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
    
    if($logEmailsArys['szDraftEmailBody']!='')
    {
        $logEmailsArys['szEmailBody']=$logEmailsArys['szDraftEmailBody'];
    }
    
    if($logEmailsArys['szDraftEmailSubject']!='')
    {
        $logEmailsArys['szEmailSubject']=$logEmailsArys['szDraftEmailSubject'];
    }
    
    echo "SUCCESS||||";
    echo display_pending_tray_crm_emails_operation($kCrm,$szAction,$logEmailsArys,$idBookingFile,$szFromPage,false,false,$szCrmEmail);
    die;
}
else if($mode=='DISPLAY_ADD_TRANSPORTECA_TASK_POPUP')
{
    $idBookingFile = sanitize_all_html_input(trim($_POST['file_id'])); 
    $kBooking->loadFile($idBookingFile);
    
    echo "SUCCESS_POPUP||||";
    echo display_transporteca_new_task_popup($kBooking);
    die;
}
else if(!empty($_POST['addTransportecaTask']))
{
    $idAdmin = sanitize_all_html_input(trim($_REQUEST['admin_id'])); 
    $_POST['addTransportecaTask']['idTaskOwner'] = $idAdmin;
    $idBookingFile = (int)$_POST['addTransportecaTask']['idBookingFile'];
    
    if($kCrm->addTransportecaTask($_POST['addTransportecaTask']))
    {
        $kBooking->loadFile($idBookingFile);
        $searchTaskAry = array();   

        $_POST['addCrmOperationAry']['szReminderNotes'] = '';
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true);  
        echo "||||";
        echo display_task_logs_container($kBooking);
        echo "||||".$kCrm->idBookingFileReminderNotes;
        die; 
    }
    else
    {
        echo "ERROR||||";
        echo display_transporteca_new_task_popup($kCrm);
        die;
    }
}
else if(!empty($_POST['addCrmOperationAry']) && $mode!='SAVE_EMAIL_AS_DRAFT')
{
    $szAction = sanitize_all_html_input(trim($_POST['addCrmOperationAry']['szAction'])); 
    $idBookingFile = sanitize_all_html_input(trim($_POST['addCrmOperationAry']['idBookingFile'])); 
    $szFromPage = sanitize_all_html_input(trim($_POST['addCrmOperationAry']['szFromPage'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['addCrmOperationAry']['idCrmEmailLogs'])); 
    $idCustomer = sanitize_all_html_input(trim($_POST['addCrmOperationAry']['idCustomer']));
    $szOperation = sanitize_all_html_input(trim($_REQUEST['operation_type'])); 
          
    if($szAction=='CRM_NOTE')
    {
        $szOperationType = sanitize_all_html_input(trim($_REQUEST['operation_type']));    
        $_POST['addCrmOperationAry']['szOperationType'] = $szOperationType;
        if($szOperationType=='CLOSE_TASK' || $szOperationType=='SEND_CLOSE_TASK')
        {
            $_POST['addCrmOperationAry']['szActionType'] = 'CRM_CLOSE';
        } 
        if(is_base64_encoded($_POST['addCrmOperationAry']['szReminderEmailBody']))
        {
            $_POST['addCrmOperationAry']['szReminderEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['addCrmOperationAry']['szReminderEmailBody']))); 
        }
        
        if($kBooking->addBookingFileSnooze($_POST['addCrmOperationAry']))
        {  
            $searchTaskAry = array();  
            $kBooking->loadFile($idBookingFile);
            
            $_POST['addCrmOperationAry']['szReminderNotes'] = '';
            if(!empty($_SESSION['sessTransportecaTeamAry']))
            {
                $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
            }
            else
            {
                $searchTaskAry['idFileOwnerAry'][0] = 0;
                $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
            }
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
            echo "SUCCESS||||";
            echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true);  
            echo "||||"; 
            echo display_pending_tray_email_logs($kBooking,$szFromPage);
            die; 
        }
        else
        {
            $logEmailsArys = array();
            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
            echo "ERROR||||".$idCrmEmailLogs."||||";
            echo display_crm_operations_notes_fields($kBooking,$logEmailsArys); 
            die;
        }
    }
    else if($szAction=='CRM_LINK')
    {
        if($szOperation=='REASSIGN_CRM_EMAIL_TO_FILE')
        { 
            $idNewBookingFile = sanitize_all_html_input(trim($_REQUEST['new_file_id'])); 
            $_POST['addCrmOperationAry']['idNewBookingFile'] = $idNewBookingFile;
            if($kCrm->reassignCrmEmailFile($_POST['addCrmOperationAry']))
            { 
                $searchTaskAry = array();
                if(!empty($_SESSION['sessTransportecaTeamAry']))
                {
                    $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
                }
                else
                {
                    $searchTaskAry['idFileOwnerAry'][0] = 0;
                    $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
                }
                $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
                
                $kBooking->loadFile($idNewBookingFile);
                
                echo "SUCCESS_FILE_REASSIGNED||||"; 
                echo display_all_peding_tasks($incompleteTaskAry,$idNewBookingFile,true); 
                echo "||||";
                echo display_pending_tray_overview($kBooking);
                echo "||||";
                echo display_pending_tray_email_logs($kBooking,$szFromPage);
                echo "||||";
                echo display_pending_task_header($kBooking,$idNewBookingFile);
                die;  
            }
        }
        else if($szOperation=='CRM_EMAIL_LINK_CUSTOMER')
        {
            $popupAry = array();
            $popupAry['idCrmEmail'] = $idCrmEmailLogs;
            $popupAry['idBookingFile'] = $idBookingFile;
            $popupAry['szFromPage'] = $szFromPage;
            
            echo "SUCCESS_POPUP||||"; 
            echo display_link_customer_popup($popupAry);
            die;
        }
        else
        {
            $iPageNumber = sanitize_all_html_input(trim($_REQUEST['page']));
            $mode = sanitize_all_html_input(trim($_REQUEST['mode']));
            $szSearchTerms = sanitize_all_html_input($_POST['addCrmOperationAry']['szSearchField']);

            if($iPageNumber==0)
            {
                $iPageNumber = 1;
            }
            $searchTaskAry = array();   
            $searchTaskAry['iFromPopup'] = 1 ; 
            $searchTaskAry['szFreeText'] = $szSearchTerms;
            $searchTaskAry['idCRMEmailLogs'] = $idCrmEmailLogs;

            if($mode=='SHOW_MORE_FILES_SEARCH_POPUP')
            { 
                if($idCustomer>0)
                {
                    //$searchTaskAry['idCustomer'] = $idCustomer;
                } 
                $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTaskSearchPopup($searchTaskAry,false,false,$iPageNumber);
                echo "SUCCESS||||";
                echo showSearchBookingTables($incompleteTaskAry,$iPageNumber,'CRM_FILE_LINKS',$idCrmEmailLogs);
                die; 
            }
            else
            {
                echo "SUUCESS_LINK||||";
                echo display_all_recent_task_by_operator($searchTaskAry,'CRM_FILE_LINKS',$kCrm,1);
                die; 
            }
        } 
    } 
    else if($szAction=='CRM_CREATE_USER')
    {
        if($kCrm->createUpdateCrmUserDetails($_POST['addCrmOperationAry']))
        {             
            $_POST['addCrmOperationAry'] = array();
            if(!empty($_SESSION['sessTransportecaTeamAry']))
            {
                $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
            }
            else
            {
                $searchTaskAry['idFileOwnerAry'][0] = 0;
                $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
            }
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
            
            $kBooking->loadFile($idBookingFile);
            
            echo "SUCCESS_UPDATE_USER||||";
            echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true); 
            echo "||||";
            echo display_pending_tray_overview($kBooking);
            echo "||||";
            echo display_pending_tray_email_logs($kBooking,$szFromPage);
            die; 
        }
        else
        { 
            $logEmailsArys = array();
            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs); 
            $kBooking->loadFile($idBookingFile);
            
            echo "ERROR||||".$idCrmEmailLogs."||||";
            echo display_crm_operations_create_crm_user_fields($kCrm,$kBooking,$logEmailsArys);
            die;
        }
    }
    else if($szAction=='CRM_REPLY' || $szAction=='CRM_FORWARD' || $szAction=='CRM_REPLY_ALL' || $szAction=='CRM_NEW_EMAIL')
    {
        $szOperationType = sanitize_all_html_input(trim($_REQUEST['operation_type']));    
        $_POST['addCrmOperationAry']['szOperationType'] = $szOperationType;
        if($szOperationType=='CLOSE_TASK' || $szOperationType=='SEND_CLOSE_TASK')
        {
            $_POST['addCrmOperationAry']['szActionType'] = 'CRM_CLOSE';
        }
        if(is_base64_encoded($_POST['addCrmOperationAry']['szReminderEmailBody']))
        {
            $_POST['addCrmOperationAry']['szReminderEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['addCrmOperationAry']['szReminderEmailBody']))); 
        }
        if($kBooking->addBookingFileSnooze($_POST['addCrmOperationAry']))
        {  
            $searchTaskAry = array();  
            $kBooking->loadFile($idBookingFile);
            
            $_POST['addCrmOperationAry']['szReminderNotes'] = '';
            if(!empty($_SESSION['sessTransportecaTeamAry']))
            {
                $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
            }
            else
            {
                $searchTaskAry['idFileOwnerAry'][0] = 0;
                $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
            }
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
            echo "SUCCESS||||";
            echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true);  
            echo "||||"; 
            echo display_pending_tray_email_logs($kBooking,$szFromPage);
            die; 
        }
        else
        {
            $logEmailsArys = array();
            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
            echo "ERROR||||".$idCrmEmailLogs."||||";
            echo display_crm_operations_reply_email($kBooking,$kBooking,$logEmailsArys);
            die;
        }
    }
    die;
}
else if(!empty($_POST['linkCustomerPopupAry']))
{
    $idBookingFile = sanitize_all_html_input(trim($_POST['linkCustomerPopupAry']['idBookingFile'])); 
    $szFromPage = sanitize_all_html_input(trim($_POST['linkCustomerPopupAry']['szFromPage'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['linkCustomerPopupAry']['idCrmEmail']));   
    $szSearchString = sanitize_all_html_input(trim($_POST['linkCustomerPopupAry']['szSearchString']));   
    
    $linkCustomerAry = array();
    //$linkCustomerAry = $kCrm->getLinkedCustomerSearchResult($szSearchString);
    
    $searchTaskAry = array();   
    $searchTaskAry['iFromPopup'] = 1 ; 
    $searchTaskAry['szFreeText'] = $szSearchString; 
    $linkCustomerAry = $kBooking->getAllBookingWithOpenedTaskSearchPopup($searchTaskAry,false,false,1);
    
    echo "SUCCESS||||";
    echo display_link_customer_listing($linkCustomerAry,$idCrmEmailLogs,$szFromPage,$idBookingFile);
    die;
}
else if(!empty($_POST['searchCrmEmailAry']))
{
    $idBookingFile = sanitize_all_html_input(trim($_POST['searchCrmEmailAry']['idBookingFile'])); 
    $szFromPage = sanitize_all_html_input(trim($_POST['searchCrmEmailAry']['szFromPage'])); 
    $szOperationType = sanitize_all_html_input(trim($_POST['searchCrmEmailAry']['szOperationType'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['searchCrmEmailAry']['idCrmEmail']));   
    $szSearchString = sanitize_all_html_input(trim($_POST['searchCrmEmailAry']['szSearchString']));  
    $szFlag = sanitize_all_html_input(trim($_POST['searchCrmEmailAry']['szFlag']));
    
    $popupAry = array();
    $popupAry['idCrmEmail'] = $idCrmEmailLogs;
    $popupAry['idBookingFile'] = $idBookingFile;
    $popupAry['szFromPage'] = $szFromPage;
    $popupAry['szOperationType'] = $szOperationType;
    
    //$linkCustomerAry = array();
    //$linkCustomerAry = $kCrm->getLinkedCustomerSearchResult($szSearchString);
    
    /*$searchTaskAry = array();   
    $searchTaskAry['iFromPopup'] = 1 ; 
    $searchTaskAry['szFreeText'] = $szSearchString; 
    $searchTaskAry['iNoPagination'] = 1 ;
    $linkCustomerAry = $kBooking->getAllBookingWithOpenedTaskSearchPopup($searchTaskAry,false,false,1);*/
    
    $fileSearchedAry = array();
    $ctr = 0; 
    $kForwarderContact = new cForwarderContact();
    $forwarderContactSearch = array();
    $forwarderContactSearch = $kForwarderContact->searchForwarderContacts($szSearchString);
    
    $finalSearchedAry = array();
    /*if(!empty($forwarderContactSearch) && !empty($linkCustomerAry))
    {
        $finalSearchedAry = array_merge($linkCustomerAry,$forwarderContactSearch);
    }
    else if(!empty($forwarderContactSearch))
    {*/
        $finalSearchedAry = $forwarderContactSearch;
    /*}
    else 
    {
        $finalSearchedAry = $linkCustomerAry;
    }*/
        
    /*$kAdmin = new cAdmin();
    $adminSearchAry = array();
    $adminSearchAry = $kAdmin->searchAdminDetails($szSearchString);  
     
    if(!empty($adminSearchAry) && !empty($finalSearchedAry))
    {
        $finalSearchedAry = array_merge($finalSearchedAry,$adminSearchAry);
    }
    else if(!empty($adminSearchAry))
    {
        $finalSearchedAry = $adminSearchAry;
    }*/  
    echo "SUCCESS||||";
    echo display_replay_customer_email_listing($finalSearchedAry,$popupAry,$szFlag);
    die;
}
else if($mode=='DISPLAY_EMAIL_ATTACHMENTS_POPUP')
{
    $szFromPage = sanitize_all_html_input(trim($_POST['from_page'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['email_id'])); 
    $idTemplate = sanitize_all_html_input(trim($_POST['template_id']));
    
    $popupAry = array();
    $popupAry['idCrmEmail'] = $idCrmEmailLogs;
    $popupAry['idTemplate'] = $idTemplate;
    $popupAry['szFromPage'] = $szFromPage; 
    
    echo "SUCCESS_POPUP||||";
    echo display_email_template_listings($popupAry);
    die;
}
else if($mode=='DISPLAY_REPLY_ADD_EMAIL_POPUP')
{
    $szFromPage = sanitize_all_html_input(trim($_POST['from_page'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['email_id'])); 
    $idBookingFile = sanitize_all_html_input(trim($_POST['file_id']));
    $szOperationType = sanitize_all_html_input(trim($_POST['type']));
    $szFlag = sanitize_all_html_input(trim($_POST['szFlag']));
    
    $popupAry = array();
    $popupAry['idCrmEmail'] = $idCrmEmailLogs;
    $popupAry['idBookingFile'] = $idBookingFile;
    $popupAry['szFromPage'] = $szFromPage;
    $popupAry['szOperationType'] = $szOperationType;
    $popupAry['szFlag'] = $szFlag;

    echo "SUCCESS_POPUP||||"; 
    echo display_cms_operation_replay_add_email_popup($popupAry);
    die;
}
else if($mode=='LINK_CUSTOMER_CRM_EMAILS')
{
    $szFromPage = sanitize_all_html_input(trim($_POST['szFromPage'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['email_id']));
    $idCustomer = sanitize_all_html_input(trim($_POST['customer_id']));
    $idBookingFile = sanitize_all_html_input(trim($_POST['file_id']));
    
    $reAssignCustomerAry = array();
    $reAssignCustomerAry['idCustomer'] = $idCustomer;
    $reAssignCustomerAry['idCrmEmailLogs'] = $idCrmEmailLogs;
    $reAssignCustomerAry['idBookingFile'] = $idBookingFile;
     
    if($kCrm->reassignCrmEmailFile($reAssignCustomerAry,'REASSIGN_CUSTOMER'))
    {
        $kBooking->loadFile($idBookingFile);
        echo "SUCCESS||||"; 
        echo display_pending_tray_email_logs($kBooking,$szFromPage);
        echo "||||";
        echo display_pending_task_header($kBooking,$idBookingFile);
        die;
    }
    
}
else if($mode=='AUTO_LOAD_PENDING_TRAY_TASK_LIST')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $iTotalNumTasksDisplayed = sanitize_all_html_input(trim($_REQUEST['total_num_task']));  
    
    $searchTaskAry = array();  
    if(!empty($_SESSION['sessTransportecaTeamAry']))
    {
        $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
    }
    else
    {
        $searchTaskAry['idFileOwnerAry'][0] = 0;
        $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
    }
    $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);    
    
    echo "SUCCESS||||";
    echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true);
    die; 
}
else if($mode=='DISPLAY_PENDING_TASK_COURIER_LABEL_UPLOAD_FORM')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $kBooking->loadFile($idBookingFile);
    
    if($kBooking->idBookingFile>0)
    {
        /*
        * Adding file visited logs
        */
        $kPendingTray->addBookingFileVisitedLogs($idBookingFile);
        
        echo "SUCCESS||||";
        echo "";
        echo "||||";
        echo display_pending_task_tray_container($kBooking,false,true);
        die;
    }
}
else if($action=='FETCH_FIRST_NAME' || $action=='FETCH_LAST_NAME' || $action=='FETCH_COMPANY_NAME')
{
    $szNameString = sanitize_all_html_input(trim($_REQUEST['q'])); 
    $customerNameAry = array();
    $customerNameAry = $kBooking->getDistinctCompanyName('CUSTOMER_NAME',$szNameString);

    if(!empty($customerNameAry))
    {
        foreach($customerNameAry as $customerNameArys)
        {
            echo $customerNameArys['szCompanyName']."\n"; 
        }
    }
    die;
}
else if($action=='FETCH_LAST_NAME')
{
    $szNameString = sanitize_all_html_input(trim($_REQUEST['q']));
    
    $customerNameAry = array();
    $customerNameAry = $kBooking->getDistinctCompanyName('LAST_NAME',$szNameString);

    if(!empty($customerNameAry))
    {
        foreach($customerNameAry as $customerNameArys)
        {
            echo $customerNameArys['szCompanyName']."\n"; 
        }
    }
    die;
}
else if($action=='FETCH_COMPANY_NAME')
{
    $szNameString = sanitize_all_html_input(trim($_REQUEST['q']));
    
    $customerNameAry = array();
    $customerNameAry = $kBooking->getDistinctCompanyName('COMPANY_NAME',$szNameString);

    if(!empty($customerNameAry))
    {
        foreach($customerNameAry as $customerNameArys)
        {
            echo $customerNameArys['szCompanyName']."\n"; 
        }
    }
    die;
}
else if($action=='FETCH_EMAIL')
{
    $szNameString = sanitize_all_html_input(trim($_REQUEST['q']));
    
    $customerNameAry = array();
    $customerNameAry = $kBooking->getDistinctCompanyName('FETCH_EMAIL',$szNameString);

    if(!empty($customerNameAry))
    {
        foreach($customerNameAry as $customerNameArys)
        {
            echo $customerNameArys['szCompanyName']."\n"; 
        }
    }
    die;
} 
else if($mode=='AUTOFILL_USER_DATA')
{ 
    $szNameString = sanitize_all_html_input(trim($_REQUEST['selected_str']));
    $szValueType = sanitize_all_html_input(trim($_REQUEST['type']));
    $iQuickQuote = sanitize_all_html_input(trim($_REQUEST['quick_quote']));
    $szFieldName = sanitize_all_html_input(trim($_REQUEST['field_name']));
    
    if($szValueType=='EMAIL')
    {
        $szType = 'FETCH_USER_ID_BY_EMAIL';
    }
    else
    {
        $szType = 'FETCH_USER_ID';
    }
    $customerNameAry = array();
    $customerNameAry = $kBooking->getDistinctCompanyName($szType,false,$szNameString);
    
    if(!empty($customerNameAry))
    {
        $idUser = $customerNameAry[0]['idUser'] ; 
        if($idUser>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($idUser);
            echo "SUCCESS||||";
            ?>
            <script type="text/javascript">
                var userDetailsAry = new Array();
                userDetailsAry[0] = addslashes('<?php echo $kUser->szFirstName; ?>') ;
                userDetailsAry[1] = addslashes('<?php echo $kUser->szLastName; ?>') ;
                userDetailsAry[2] = addslashes("<?php echo $kUser->szCompanyName; ?>") ;
                userDetailsAry[3] = addslashes('<?php echo $kUser->idInternationalDialCode; ?>') ;
                userDetailsAry[4] = addslashes('<?php echo $kUser->szPhoneNumber; ?>') ;
                userDetailsAry[5] = addslashes('<?php echo $kUser->szEmail; ?>') ;
                userDetailsAry[6] = addslashes('<?php echo $kUser->szCurrency; ?>') ;
                userDetailsAry[7] = addslashes('<?php echo $kUser->iConfirmed; ?>') ;
                userDetailsAry[8] = addslashes('<?php echo $kUser->iAcceptNewsUpdate; ?>') ;
                userDetailsAry[9] = addslashes('<?php echo $kUser->iPrivate; ?>') ;
                userDetailsAry[10] = addslashes('<?php echo $kUser->idCustomerOwner; ?>') ;
                userDetailsAry[11] = addslashes('<?php echo $kUser->szCountry; ?>') ;
                userDetailsAry[12] = addslashes('<?php echo $kUser->iLanguage; ?>') ;
                
                userDetailsAry[13] = addslashes("<?php echo $kUser->szAddress1; ?>") ;
                userDetailsAry[14] = addslashes("<?php echo $kUser->szPostCode; ?>") ;
                userDetailsAry[15] = addslashes("<?php echo $kUser->szCity; ?>") ;
                userDetailsAry[16] = addslashes("<?php echo $kUser->szCountry; ?>") ;
                userDetailsAry[17] = addslashes("<?php echo $kUser->szCompanyRegNo; ?>");
                
                autofill_customer_data(userDetailsAry,'<?php echo $idUser; ?>','<?php echo $iQuickQuote; ?>','<?php echo $szFieldName; ?>');
            </script>
            <?php
        }
    }
} 
else if($mode=='DISPLAY_FILE_OWNER_CONFIRM_POPUP')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $iCourierBooking = sanitize_all_html_input(trim($_REQUEST['iCourierBooking']));  
    $kBooking->loadFile($idBookingFile);
    if(($kBooking->idBookingFile>0) && (($kBooking->idFileOwner<=0) || ($_SESSION['admin_id']!=$kBooking->idFileOwner)))
    {
        echo "SUCCESS_POPUP||||";
        echo display_file_owner_confimation($kBooking,$iCourierBooking);
        die;
    }
}
else if($mode=='REOPEN_BOOKING_FILE')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));   
    
    if($kBooking->reOpenTask($idBookingFile))
    {
        $searchTaskAry = array(); 
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
        
        $kBooking->loadFile($idBookingFile); 
        
        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true); 
        echo "||||";
        echo display_pending_tray_overview($kBooking);
        echo "||||";
        echo display_pending_task_tray_container($kBooking);
        die; 
    }
}
else if($szOperationType=='CONVERT_TO_RFQ')
{ 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['updatedTaskAry']['idBookingFile']));   
    
    if($kBooking->convertBookingToRFQ($idBookingFile))
    {
        $searchTaskAry = array(); 
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
        
        $kBooking->loadFile($idBookingFile); 
        
        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true); 
        echo "||||";
        echo display_pending_tray_overview($kBooking);
        echo "||||";
        echo display_pending_task_tray_container($kBooking);
        echo "||||";
        echo display_pending_task_header($kBooking,$idBookingFile); 
        die; 
    }
    die;
}
else if($mode=='ADD_BOOKING_FILE_OWNER')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $iCourierBooking = sanitize_all_html_input(trim($_REQUEST['iCourierBooking']));  
    $kBooking->loadFile($idBookingFile);
    
    if($kBooking->idBookingFile>0)
    { 
        /*
        if($kBooking->idFileOwner>0)
        { 
            $kAdmin = new cAdmin();
            $kAdmin->getAdminDetails($kBooking->idFileOwner);
            $szTitle = "File Error";
            $szMessage = "This file is already assigned to ".$kAdmin->szFirstName." ".$kAdmin->szLastName; 
            
            echo "ERROR||||";			
            echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
            die;
        }
        else
        { */
            $fileLogsAry=array();
            $fileLogsAry['idFileOwner'] = $_SESSION['admin_id'];
            $fileLogsAry['idAdminUpdatedBy'] = $_SESSION['admin_id'];
            if((int)$kBooking->idCustomerOwner==0)
            {
                $fileLogsAry['idCustomerOwner'] = $_SESSION['admin_id'];
            }
            
            if(!empty($fileLogsAry))
            {
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }  
            $file_log_query = rtrim($file_log_query,",");  
            if($kBooking->updateBookingFileDetails($file_log_query,$idBookingFile))
            { 
                /*
                * Adding file visited logs
                */
                $kPendingTray->addBookingFileVisitedLogs($idBookingFile);
        
                $kBooking->loadFile($idBookingFile);
                echo "SUCCESS||||"; 
                if($iCourierBooking==1)
                { 
                    echo display_pending_task_tray_container($kBooking,false,true);
                    echo "||||"; 
                    die; 
                }
                else
                {
                    echo display_pending_tray_overview($kBooking);
                    echo "||||";
                    echo display_pending_task_tray_container($kBooking);
                    die; 
                } 
            }
            else
            {
                $szTitle = "System Error";
                $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
                echo "ERROR||||";			
                echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
                die;
            }
       // } 
    }
    else
    {
        $szTitle = "System Error";
        $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        echo "ERROR||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='DISPLAY_PENDING_TASK_OVERVIEW')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $kBooking->loadFile($idBookingFile);
    
    if($kBooking->idBookingFile>0)
    {
        echo "SUCCESS||||";
        echo display_pending_tray_overview($kBooking); 
        die;
    }
} 
else if($mode=='DISPLAY_CUSTOMER_LOG_PAGINATION')
{	 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $iPageNumber = sanitize_all_html_input(trim($_REQUEST['page']));  
    
    $kBooking->loadFile($idBookingFile); 
    if($kBooking->idBookingFile>0)
    {
        echo "SUCCESS||||";
        echo display_task_customer_logs_container($kBooking,$iPageNumber);
    } 
    die; 
}
else if($mode=='CLOSE_REMINDER_TASK')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
    $szOperationType = sanitize_all_html_input(trim($_REQUEST['operation_type']));  
    $idReminderTask = sanitize_all_html_input(trim($_REQUEST['reminder_task_id']));  
    
    $dataInputAry = array();
    $dataInputAry['idBookingFile'] = $idBookingFile;
    $dataInputAry['szOperationType'] = $szOperationType;
    $dataInputAry['idReminderTask'] = $idReminderTask;
     
    if($kCrm->updateReminderTask($dataInputAry))
    {
        $searchTaskAry = array(); 
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
        $kBooking->loadFile($idBookingFile);
        
        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true); 
        echo "||||";
        echo display_pending_tray_email_logs($kBooking,$szFromPage);
    }
    die; 
}
else if($mode=='CHECK_INSURANCE_AVAILABILITY')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['booking_id']));  
    $idOriginCountry = sanitize_all_html_input(trim($_REQUEST['origin_country']));  
    $idDestinationCountry = sanitize_all_html_input(trim($_REQUEST['destination_country']));  
    $idTransportMode = sanitize_all_html_input(trim($_REQUEST['transport_mode']));  
    $iMoving = sanitize_all_html_input(trim($_REQUEST['moving']));  
    $iInsuranceIncluded = sanitize_all_html_input(trim($_REQUEST['insurance_included']));  
    $szCargoType = sanitize_all_html_input(trim($_REQUEST['cargo_type']));  
     
    $insuranceInputAry = array();
    $insuranceInputAry['idOriginCountry'] = $idOriginCountry;
    $insuranceInputAry['idDestinationCountry'] = $idDestinationCountry;
    $insuranceInputAry['idTransportMode'] = $idTransportMode;
    $insuranceInputAry['isMoving'] = $iMoving;
    $insuranceInputAry['szCargoType'] = $szCargoType;
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
    if($insuranceInputAry['idOriginCountry']>0 && $insuranceInputAry['idDestinationCountry']>0 && $insuranceInputAry['idTransportMode']>0)
    {
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        if($postSearchAry['idBookingStatus']==3 || $postSearchAry['idBookingStatus']==4 || $postSearchAry['idBookingStatus']==7)
        {
            //For paid booking we don't update Insurance fields
            die;
        }
        else
        {
            if(!empty($postSearchAry))
            {
                $insuranceInputAry['fTotalBookingPriceUsd'] = $postSearchAry['fTotalPriceUSD'] + $postSearchAry['fValueOfGoodsUSD'];
                if($insuranceInputAry['fTotalBookingPriceUsd']<=0)
                {
                    $insuranceInputAry['fTotalBookingPriceUsd'] = 1;
                }
            }
            else
            {
                $insuranceInputAry['fTotalBookingPriceUsd'] = 1; 
            }

            $kInsurance = new cInsurance();
            if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry))
            {
                echo "SUCCESS||||";
                echo display_insurance_options(1,$iInsuranceIncluded);
                die;
            }
            else
            {
                echo "NOT_AVAILABLE||||";
                echo display_insurance_options(2,$iInsuranceIncluded);
                die;
            } 
        } 
    } 
}
else if($mode=='DISPLAY_TASK_FORM_MENU')
{ 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szEmailClicked = sanitize_all_html_input(trim($_REQUEST['email_id']));  
    $idRemondTemplate = sanitize_all_html_input(trim($_REQUEST['template_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));
     
    $kBooking->loadFile($idBookingFile); 
    if($kBooking->idBookingFile>0)
    {
        if($szTaskStatus=='VALIDATE')
        {
            echo "SUCCESS||||";
            echo display_pending_task_validate_form($kBooking);
            die;
        }
        else if($szTaskStatus=='ASK_FOR_QUOTE')
        {
            echo "SUCCESS||||";
            echo display_task_ask_for_quote_container($kBooking);
            die;
        }
        else if($szTaskStatus=='ESTIMATE')
        {
            echo "SUCCESS||||";
            echo display_task_estimate_container($kBooking);
            die;
        }
        else if($szTaskStatus=='QUOTE')
        {
            echo "SUCCESS||||";
            echo display_task_quote_container($kBooking);
            die;
        }
        else if($szTaskStatus=='BOOK')
        {
            echo "SUCCESS||||";
            echo display_task_book_container($kBooking);
            die;
        }
        else if($szTaskStatus=='REMIND')
        {
            echo "SUCCESS||||";
            echo display_task_reminder_container($kBooking,false,$idRemondTemplate,false,$szEmailClicked);
            die;
        }
        else if($szTaskStatus=='LOG')
        {
            echo "SUCCESS||||";
            echo display_task_logs_container($kBooking);
            die;
        }
        else if($szTaskStatus=='CUSTOMER_LOG')
        {
            echo "SUCCESS||||";
            echo display_task_customer_logs_container($kBooking);
            die;
        }
    }
}
else if($mode=='DISPLAY_CHAT_TRANSCRIPT')
{
    $idZopimChat = sanitize_all_html_input($_REQUEST['chat_id']);
    $kChat = new cChat();
    if($idZopimChat>0)
    {
        $zopimChatTranscriptAry = array();
        $zopimChatTranscriptAry = $kChat->getAllZopimChatMessageLogs($idZopimChat);
        
        $zopimChatAry = array();
        $zopimChatAry = $kChat->getAllZopimChatLogs(false,$idZopimChat);
        
        echo "SUCCESS||||";
        echo display_zopim_chat_transcript($zopimChatTranscriptAry,$zopimChatAry[0]); 
    }
    die;
}
else if($mode=='ADD_MORE_BOOKING_QUOTES')
{  
    $number = sanitize_all_html_input($_REQUEST['number'])+1; 
    $kConfig = new cConfig();
    $kForwarder = new cForwarder();
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(true); 
    
    echo display_forwarder_quote($kBooking,$number,$transportModeListAry,$forwarderListAry,true);
    die;
}
else if($mode=='ADD_MORE_BOOKING_QUOTES_PRICING')
{
    $number = sanitize_all_html_input($_REQUEST['number'])+1; 
    $idBookingFile = sanitize_all_html_input($_REQUEST['file_id']) ;
    $kConfig = new cConfig();
    $kForwarder = new cForwarder(); 
    
    $kBooking->loadFile($idBookingFile);
    $quoteAry = array();
    echo display_booking_quote_pricing_details($kBooking,$number,$quoteAry,true,true);
    die;
}
else if($mode=='DISPLAY_TASK_QUOTE_MANUAL_EDIT_FORM')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));  
    
    $kBooking->loadFile($idBookingFile);  
    if($kBooking->idBookingFile>0)
    {
        echo "SUCCESS||||";
        echo display_task_quote_container($kBooking,true,false,true);
        die;
    }
}
else if($mode=='DISPLAY_RECENT_FILES_SEEN_BY_OPERATOR')
{
    $iToggleType = sanitize_all_html_input(trim($_REQUEST['iToggleType']));  
    $searchTaskAry = array(); 
    $searchTaskAry['idFileOwnerAry'][0] = $_SESSION['admin_id']; 
    $searchTaskAry['iFromPopup'] = 1 ;
    $searchTaskAry['iRecentPopup'] = 1 ;
    $searchTaskAry['iToggleType'] = $iToggleType;
    $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTaskSearchPopup($searchTaskAry);
     
    echo "SUCCESS||||";
    echo display_pending_task_popup($incompleteTaskAry,'RECENT_TASK',$kBooking,array(),$iToggleType);
    die;
}
else if($mode=='DISPLAY_SEARCH_PENDING_TASK_POPUP')
{
    $szOperationType = sanitize_all_html_input(trim($_REQUEST['opration_type']));  
    $searchTaskAry = array();   
    if($szOperationType=='CLEAR_ALL')
    {
        $_SESSION['searchPendingTaskAry'] = array();
        unset($_SESSION['searchPendingTaskAry']);
    }
    else if(!empty($_SESSION['searchPendingTaskAry']))
    {
        $searchTaskAry = $_SESSION['searchPendingTaskAry'];
    } 
    $searchTaskAry['iFromPopup'] = 1; 
    echo "SUCCESS||||";
    echo display_pending_task_popup($searchTaskAry,'SEARCH_TASK',$kBooking);
    die;
}
else if($mode=='DELETE_PENDING_TASK')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
     
    if(empty($szFromPage))
    {
        $szFromPage = 'RECENT_TASK' ;
    }
    
    $kBooking->loadFile($idBookingFile);
    if($kBooking->idBookingFile>0)
    {
        echo "SUCCESS_POPUP||||";
        echo display_delete_booking_file_confirmation($kBooking,$szFromPage);
        die;
    }
} 
else if($mode=='PREFILL_FORWARDER_CONTACT_EMAIL')
{ 
    $idBooking = sanitize_all_html_input(trim($_REQUEST['booking_id']));  
    $idForwarder = sanitize_all_html_input(trim($_REQUEST['forwarder_id']));  
    $idTransportMode = sanitize_all_html_input(trim($_REQUEST['transport_mode_id']));  
    $number = sanitize_all_html_input(trim($_REQUEST['index_num']));   
    $szOperationType = sanitize_all_html_input(trim($_REQUEST['type']));  
      
    if($szOperationType=='CHANGED_MODE')
    {
        $bookingDataArr = array();
        $bookingDataArr =  $kBooking->getBookingDetails($idBooking);
        $data = array(); 
        $data['idTransportMode'] = $idTransportMode;
        $data['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
        $data['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];

        $forwarderContactAry = array();
        $forwarderContactAry = $kConfig->getServiceProviderCompanies($data);
         
        $forwarderListAry = array();
        if(!empty($forwarderContactAry))
        { 
            $kForwarder = new cForwarder();
            $forwarderListAry = $kForwarder->getForwarderArr(true,$forwarderContactAry); 
        } 
        echo "SUCCESS_MODE||||";
        ?> 
        <select name="askForQuoteAry[idForwarder][<?php echo $number; ?>]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableSendButton();" id="idForwarder_<?php echo $number; ?>" onchange="prefill_forwarder_contact('<?php echo $number; ?>');" >
            <option value="">Select</option>
            <?php
                if(!empty($forwarderListAry))
                {
                   foreach($forwarderListAry as $forwarderListArys)
                   {
                    ?>
                    <option value="<?php echo $forwarderListArys['id']; ?>" <?php echo (($forwarderListArys['id']==$idForwarder)?'selected':''); ?>><?php echo $forwarderListArys['szForwarderAlias']; ?></option>
                    <?php
                   }
                }
            ?> 
        </select>
        <?php
        die;
    } 
    else if($idBooking>0 && $idForwarder>0 && $idTransportMode>0)
    {  
        $bookingDataArr = array();
        $bookingDataArr =  $kBooking->getBookingDetails($idBooking);
        $data = array();
        $data['idForwarder'] = $idForwarder;
        $data['idTransportMode'] = $idTransportMode;
        $data['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
        $data['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
        
        
        $kForwarder = new cForwarder();
        $kForwarder->load($idForwarder);
        
        if($kForwarder->iOffLineQuotes==1)
        {
            $idCountryForwarder=$kForwarder->idCountry;
            $kConfig = new cConfig();
            $kConfig->loadCountry($idCountryForwarder);
            //echo $kConfig->iDefaultLanguage."iDefaultLanguage";
            $emailMessageAry=createOffEmailData($idBooking,$kConfig->iDefaultLanguage);
            
            $breaksAry = array("<br />","<br>","<br/>");  
            $szEmailBody = $emailMessageAry['szEmailMessage']; 

            //$szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
            $szEmailSubject = $emailMessageAry['szEmailSubject'];
        }    
        $forwarderContactAry = array();
        $forwarderContactAry = $kConfig->getForwarderContact($data);
         
        if(!empty($forwarderContactAry))
        {
            $szEmailStr = implode(", ",$forwarderContactAry);
        } 
        
        echo "SUCCESS||||";
        echo $szEmailStr;
        echo "||||";
        echo $kForwarder->iOffLineQuotes;
        if($kForwarder->iOffLineQuotes==1)
        {
            echo "||||";
            echo $szEmailSubject;
            echo "||||";
            echo $szEmailBody;
            echo "||||";
            echo $kConfig->iDefaultLanguage;
        }
        die;
    }
}
else if($mode=='DISPLAY_FORWARDER_CONTACT_DROPDOWN')
{
    $idForwarder = sanitize_all_html_input(trim($_REQUEST['forwarder_id']));   
    $kForwarderCont = new cForwarderContact();
    
    //if($idForwarder>0)
    //{
        $forwarderContactAry = array();
        $forwarderContactAry = $kForwarderCont->getAllForwardersContact($idForwarder,false);
        echo "SUCCESS||||"; 
        ?>
        <select name="searchPendingTaskAry[idForwarderContact]" class="search-form-element" id="idForwarderContact" >
            <option value="">All</option>
        <?php
            if(!empty($forwarderContactAry))
            {
                foreach($forwarderContactAry as $forwarderContactArys)
                {
                    $szForwarderContactName = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
                    $szForwarderContactName = trim($szForwarderContactName);
                    if(!empty($szForwarderContactName))
                    {
                        ?>
                        <option value="<?php echo $forwarderContactArys['id']; ?>" <?php echo (($forwarderContactArys['id']==$idForwarderContact)?'selected':''); ?>><?php echo $szForwarderContactName; ?></option>
                        <?php
                    } 
                }
            }
        ?> 
        </select>
        <?php
    //} 
}
else if($mode=='DELETE_PENDING_TASK_CONFIRM')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));   
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
    
    if($kBooking->deleteBookingFile($idBookingFile))
    { 
        $incompleteTaskAry = array();

        $searchTaskAry = array(); 
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
        
        echo "SUCCESS_TRAY||||";
        echo display_all_peding_tasks($incompleteTaskAry);
        echo "||||";
        if($szFromPage=='RECENT_TASK')
        {
            $searchTaskAry = array(); 
            $searchTaskAry['idFileOwnerAry'][0] = $_SESSION['admin_id']; 
            $searchTaskAry['iFromPopup'] = 1 ;
            $searchTaskAry['iRecentPopup'] = 1 ;
            $searchTaskAry['iDonotAddNotServiceAdmin'] = 1 ;
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
 
            echo display_pending_task_popup($incompleteTaskAry,'RECENT_TASK',$kBooking);
            die;
        }
        else
        {
            $searchTaskAry = array();  
            $incompleteTaskAry = array();
            $searchTaskAry['iFromPopup'] = 1 ;
            //$incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
            
            echo display_pending_task_popup($searchTaskAry,'SEARCH_TASK',$kBooking);
            die;
        }
        die;
    }
}
else if($mode == "COPY_PENDING_TASK_POP_UP")
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));   
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
     
    echo "SUCCESS_POPUP||||";
    ?>
     <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup" > 
            <h2>Copy Booking </h2>
            <div class="clearfix email-fields-container">
                <h3>Is this new request from the same or another customer?</h3>
            </div>
            <div class="clearfix" style="width:100%;">
                <div class="btn-container"> 
                    <p align="center">
                        <a href="javascript:void(0);" class="button1" id="recent_task_same_button" onclick="display_pending_task_overview('COPY_PENDING_TASK','<?php echo $idBookingFile;?>','<?php echo $szFromPage;?>');"><span>SAME</span></a>
                        <a href="javascript:void(0);" class="button1" id="recent_task_another_button" onclick="display_pending_task_overview('COPY_PENDING_TASK_ANOTHER','<?php echo $idBookingFile;?>','<?php echo $szFromPage;?>');"><span>ANOTHER</span></a>
                        <a href="javascript:void(0);" class="button1" id="recent_task_another_button" onclick="display_pending_task_overview();"><span>CANCEL</span></a>
                    </p>
                </div> 
            </div>
        </div>
    </div>
            <?php
}
else if($mode=='COPY_QUICK_QUOTE_BOOKING')
{ 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));   
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
     
    if($kBooking->copyBookingFile($idBookingFile,false,false,true))
    { 
        $idBookingFile = $kBooking->iNewFileID;
        $kBooking->loadFile($idBookingFile);  
        $idBooking = $kBooking->idBooking;
        
        if($idBooking>0)
        {
            $postSearchAry = array();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
            
            if(!empty($szBookingRandomNum))
            {
                echo "SUCCESS_REDIRECT||||";
                echo __BASE_URL__."/quickQuote/".$szBookingRandomNum."/";
                die; 
            }
        } 
        
        
        /*
         *  From now onwards we are already updating following values while copying the booking so following codes are no longer in use @Ajay(2016-07-20) 
        $idBookingFile = $kBooking->iNewFileID;
        $kBooking->loadFile($idBookingFile);  
        $idBooking = $kBooking->idBooking;
        
        if($idBooking>0)
        {
            $iCargoPackingType = 0;
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            if($postSearchAry['iCargoPackingType']<=0)
            {
                if($postSearchAry['iSearchMiniVersion']>0)
                {
                    if($postSearchAry['iSearchMiniVersion']==3 || $postSearchAry['iSearchMiniVersion']==4)
                    {
                        $iCargoPackingType = 1; //Parcel booking
                    }
                    else if($postSearchAry['iSearchMiniVersion']==5 || $postSearchAry['iSearchMiniVersion']==6)
                    {
                        $iCargoPackingType = 2; //Pallet booking
                    }
                    else
                    {
                        $iCargoPackingType = 3; //Break bulk
                    }
                }
                else
                {
                    $iCargoPackingType = 3;
                }
            } 
            
            $res_ary = array();
            $res_ary['iQuickQuote'] = __QUICK_QUOTE_CONVERT_RFQ__; 
            
            if($iCargoPackingType>0)
            {
                $res_ary['iCargoPackingType'] = $iCargoPackingType; 
            }
            /*
            * Updating following values just to make important flag are set-up correctly.
             
            $res_ary['iBookingQuotes'] = 1;
            $res_ary['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_NEW_BOOKING__ ;  
            $res_ary['iBookingType'] = __BOOKING_TYPE_RFQ__;  
            $res_ary['iCourierBooking'] = 0;
            $res_ary['dtTimingDate'] = date('Y-m-d');
            
            $update_query = "";
            if(!empty($res_ary))
            {
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");   

            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                //@TO DO
            }
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
        }
         * 
         */ 
    } 
    die;
}
else if($mode=='COPY_PENDING_TASK')
{ 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));   
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
     
    if($kBooking->copyBookingFile($idBookingFile))
    { 
        $idBookingFile = $kBooking->iNewFileID ;
        $searchTaskAry = array();  
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 

        echo "SUCCESS_NEW||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,false,false,'copy');
        die;
        
        /*
        if($szFromPage=='RECENT_TASK')
        {
            $searchTaskAry = array(); 
            $searchTaskAry['idFileOwnerAry'][0] = $_SESSION['admin_id']; 
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);

            echo "SUCCESS_POPUP||||";
            echo display_pending_task_popup($incompleteTaskAry,'RECENT_TASK',$kBooking);
            die;
        }
        else
        {
            $searchTaskAry = array();  
            $incompleteTaskAry = array();
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);

            echo "SUCCESS||||";
            echo display_pending_task_popup($incompleteTaskAry,'SEARCH_TASK',$kBooking);
            die;
        }
        die; */
    }
    else 
    {
        if(!empty($kBooking->szCopyBookingError))
        {
            $szTitle = "System Error";
            $szMessage = $kBooking->szCopyBookingError; 
        }
        else
        {
            $szTitle = "System Error";
            $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        } 
        echo "ERROR||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='COPY_PENDING_TASK_ANOTHER')
{ 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));   
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
     
    if($kBooking->copyBookingFile($idBookingFile,false,true))
    { 
        $idBookingFile = $kBooking->iNewFileID ;
        $searchTaskAry = array();  
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 

        echo "SUCCESS_NEW||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,false,false,'copy');
        die;
        
        /*
        if($szFromPage=='RECENT_TASK')
        {
            $searchTaskAry = array(); 
            $searchTaskAry['idFileOwnerAry'][0] = $_SESSION['admin_id']; 
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);

            echo "SUCCESS_POPUP||||";
            echo display_pending_task_popup($incompleteTaskAry,'RECENT_TASK',$kBooking);
            die;
        }
        else
        {
            $searchTaskAry = array();  
            $incompleteTaskAry = array();
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);

            echo "SUCCESS||||";
            echo display_pending_task_popup($incompleteTaskAry,'SEARCH_TASK',$kBooking);
            die;
        }
        die; */
    }
    else 
    {
        if(!empty($kBooking->szCopyBookingError))
        {
            $szTitle = "System Error";
            $szMessage = $kBooking->szCopyBookingError; 
        }
        else
        {
            $szTitle = "System Error";
            $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        } 
        echo "ERROR||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='RESET_TASK_QUOTE_PANE')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));  
    
    $kBooking->loadFile($idBookingFile);  
    if($kBooking->idBookingFile>0)
    { 
        echo "SUCCESS_POPUP||||";
        echo display_reset_quote_confirmation_popup($kBooking);
        die; 
    }
}
else if($mode=='CONFIRM_RESET_TASK_QUOTE_PANE')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));  
    
    $kBooking->loadFile($idBookingFile);  
    if($kBooking->idBookingFile>0)
    {
        echo "SUCCESS||||";
        echo display_task_quote_container($kBooking,false,2);
        die;
    }
}
else if($mode=='CLOSE_BOOKING_FILE')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));    
    $kBooking->loadFile($idBookingFile);  
    if($kBooking->idBookingFile>0)
    { 
        echo "SUCCESS_POPUP||||";
        echo display_close_task_confirmation_popup($kBooking);
        die; 
    }
}
else if($mode=='CONFIRM_CLOSE_BOOKING_FILE')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    if($kBooking->closeTask($idBookingFile))
    { 
        $searchTaskAry = array();  
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 

        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry);
        die;
    }
    else
    {
        echo "ERROR||||";
        die;
    }
}
else if($mode=='ADD_NEW_BOOKING_FILE')
{
    if($kBooking->addNewBookingFile())
    {
        $idBookingFile = $kBooking->idBookingFile;   
        $kBooking->loadFile($idBookingFile);

        if($kBooking->idBookingFile>0)
        {
            $searchTaskAry = array();  
            if(!empty($_SESSION['sessTransportecaTeamAry']))
            {
                $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
            }
            else
            {
                $searchTaskAry['idFileOwnerAry'][0] = 0;
                $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
            }
            $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
            
            echo "SUCCESS_NEW||||";
            echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile);
            die;
        }
    }
    else
    { 
        $szTitle = "System Error";
        $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        echo "ERROR||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if($mode=='DELETE_REQUESTED_QUOTE')
{
    $idBookingQuote = sanitize_all_html_input(trim($_REQUEST['booking_quote_id']));    
    if($kBooking->deleteBookingQuotes($idBookingQuote))
    {
        echo "SUCCESS||||";
        die;
    }
    die(); 
}
else if($mode=='DISPLAY_DELETE_BOOKING_QUOTE_POPUP')
{  
    $idBookingQuote = sanitize_all_html_input(trim($_REQUEST['quote_id']));    
    $idBookingQuotePricing = sanitize_all_html_input(trim($_REQUEST['quote_pricing_id']));    
    $iRecordNumber = sanitize_all_html_input(trim($_REQUEST['record_number']));    
     
    echo "SUCCESS||||";
    echo display_delete_quote_pricing_confirmation($idBookingQuote,$idBookingQuotePricing,$iRecordNumber);
    die; 
}
else if($mode=='CONFIRM_DELETE_QUOTE_PRICING')
{
    $idBookingQuote = sanitize_all_html_input(trim($_REQUEST['quote_id']));    
    $idBookingQuotePricing = sanitize_all_html_input(trim($_REQUEST['quote_pricing_id']));    
    $iRecordNumber = sanitize_all_html_input(trim($_REQUEST['record_number']));
    if($idBookingQuote<=0)
    {
        //When newly added quote gets deleted then we do not update anything to db as this booking is not yet saved in databse.
        echo "SUCCESS||||"; 
        die;
    }
    else
    {
        if($kBooking->deleteBookingQuotePricing($idBookingQuote,$idBookingQuotePricing))
        {
            echo "SUCCESS||||"; 
            die;
        } 
        else
        {
            echo "ERROR||||";
            die;
        }
    } 
}
else if($mode=='UNDO_TASK_DISQUALIFICATION')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));    
    if($kBooking->undoDisqaualifyTask($idBookingFile))
    { 
        $searchTaskAry = array(); 
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
        
        $kBooking->loadFile($idBookingFile); 
        echo "SUCCESS_LIST||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true); 
        echo "||||";
        echo display_pending_tray_overview($kBooking);
        echo "||||";
        echo display_pending_task_tray_container($kBooking);
        die;
    }
    else
    { 
        $szTitle = "System Error";
        $szMessage = "Due to some internal server error we can't process your request this time. Please try again later."; 
        echo "ERROR||||";			
        echo success_message_popup_admin($szTitle,$szMessage,'contactPopup');
        die;
    }
}
else if(!empty($_POST['addSnoozeAry']) && $mode!='SAVE_EMAIL_REMIND_AS_DRAFT')
{
    $szOperationType = sanitize_all_html_input(trim($_REQUEST['operation_type']));    
    $_POST['addSnoozeAry']['szOperationType'] = $szOperationType;   
    $idBookingFile = $_POST['addSnoozeAry']['idBookingFile'];
    
    if(is_base64_encoded($_POST['addSnoozeAry']['szReminderEmailBody']))
    {
        $_POST['addSnoozeAry']['szReminderEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['addSnoozeAry']['szReminderEmailBody'])));
    }
    
    if(trim($szOperationType) == 'PREVIEW_REMINDER_EMAIL')
    {
        $szResponseText = $kBooking->preview_reminder_email($_POST['addSnoozeAry']);
        echo "SUCCESS_PREVIEW||||";
        echo $szResponseText ;
        die;
    }
    else if($kBooking->addBookingFileSnooze($_POST['addSnoozeAry'],false))
    {
        $searchTaskAry = array(); 
        $_POST['addSnoozeAry']['szReminderNotes'] = '';
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true); 
        
        if($szOperationType!='CLOSE_TASK' && $szOperationType!='SEND_CLOSE_TASK')
        {
            
            if($szOperationType=='SEND_SNOOZE' || $szOperationType=='SEND')
            {
                $iSuccess = 2;
            }
            else
            {
                $iSuccess = 1;
            }
            $kBooking->loadFile($idBookingFile); 
            
            echo "||||";
            echo display_task_reminder_container($kBooking,$iSuccess);
        }       
        $kCRM = new cCRM();
        $kCRM->deleteDraftEmailForRemind($idBookingFile);
            
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_task_reminder_container($kBooking);
        die;
    }
}
else if(!empty($_POST['addReminderNotesAry']))
{
    if($kBooking->addAddReminderNotes($_POST['addReminderNotesAry']))
    { 
        $_POST['addReminderNotesAry'] = array();
        echo "SUCCESS||||"; 
        echo displayAddRFQReminderNotes($kBooking,true);
        die;
    }
    else
    {
        echo "ERROR||||"; 
        echo displayAddRFQReminderNotes($kBooking);
        die;
    }
}
else if(!empty($_POST['quotePricingAry']))
{
    $idBookingFile = (int)$_POST['quotePricingAry']['idBookingFile']; 
    $szButtonClicked = (int)$_POST['quotePricingAry']['szButtonClicked']; 
    $iSectionEdited = (int)$_POST['quotePricingAry']['iSectionEdited']; 
    $szOperationType = $_REQUEST['type'];  
    $_POST['quotePricingAry']['szOperationType'] = $szOperationType ;
      
    $_POST['quotePricingAry'] = format_post_array($_POST['quotePricingAry']);
    
    if(is_base64_encoded($_POST['quotePricingAry']['szEmailBody']))
    {
        $_POST['quotePricingAry']['szEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['quotePricingAry']['szEmailBody'])));
    }
     
    if($szOperationType=='PREVIEW_REQUEST_QUOTES')
    {
        $szResponseText = $kBooking->preview_quote_email($_POST['quotePricingAry']);
        echo "SUCCESS_PREVIEW||||";
        echo $szResponseText ;
        die;
    }
    else
    { 
        if($kBooking->addUpdateQuotePricingDetailsByOperator($_POST['quotePricingAry']))
        {
            $kBooking->loadFile($idBookingFile);
            $_POST['quotePricingAry'] = array();
            if($szOperationType=='SEND_CUSTOMER_QUOTES') 
            {
                $iSuccessFlagType = 3;
            }
            else
            {
                $iSuccessFlagType = 1;
            }
            echo "SUCCESS_QUOTE||||";
            echo display_task_quote_container($kBooking,false,$iSuccessFlagType);
            if($szOperationType=='SEND_CUSTOMER_QUOTES') 
            {
                $searchTaskAry = array(); 
                if(!empty($_SESSION['sessTransportecaTeamAry']))
                {
                    $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
                }
                else
                {
                    $searchTaskAry['idFileOwnerAry'][0] = 0;
                    $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
                }
                $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
        
                 echo "||||";
                 echo "S5. Quote sent";
                 echo "||||";
                 echo display_all_peding_tasks($incompleteTaskAry);
            }
            die;
        }
        else
        { 
            $edit_flag=false;
            if($iSectionEdited==1)
            {
                $edit_flag = true;
            }  
            else
            {
               // $kBooking->loadFile($idBookingFile);
            }  
            echo "ERROR||||";
            echo display_task_quote_container($kBooking,$edit_flag);
            die;
        }
    }
}
else if(!empty($_POST['askForQuoteAry']))
{
    $askForQuoteAry = array();
    $askForQuoteAry = $_POST['askForQuoteAry'] ;
    $idBookingFile = (int)$_POST['askForQuoteAry']['idBookingFile'];  
    $szTaskType = sanitize_all_html_input(trim($_REQUEST['type']));
    $askForQuoteAry['szType'] = $szTaskType ;
    
    if(is_base64_encoded($askForQuoteAry['szEmailBody']))
    {
        $askForQuoteAry['szEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($askForQuoteAry['szEmailBody'])));
        $_POST['askForQuoteAry']['szEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['askForQuoteAry']['szEmailBody'])));
    }
      
    if($kBooking->sendRequestQuotes($askForQuoteAry))
    { 
        $_POST['askForQuoteAry'] = array();
        $iNumQuoteSent = $kBooking->iNumQuoteSent;
        $searchTaskAry = array(); 
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
        //print_r($_POST['askForQuoteAry']['iOffLine']);
        
        if($szTaskType=='SEND_REQUEST_QUOTES')
        {
            $iSuccessFlag = 1;
        }
        else
        {
            $iSuccessFlag = 2;
        }
        
        $kBooking->loadFile($idBookingFile); 
        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true);
        echo "||||";
        echo display_pending_tray_overview($kBooking);
        echo "||||";
        echo display_task_ask_for_quote_container($kBooking,$iSuccessFlag,$iNumQuoteSent);
        $_POST['askForQuoteAry'] = array();
        die;
    }
    else
    {
        echo "ERROR||||";
        echo display_task_ask_for_quote_container($kBooking);
        die;
    }
}
else if(!empty($_POST['searchPendingTaskAry']))
{ 
    $iPageNumber = sanitize_all_html_input(trim($_REQUEST['page']));
    $mode = sanitize_all_html_input(trim($_REQUEST['mode']));
    
    if($iPageNumber==0)
    {
        $iPageNumber = 1;
    }
    $searchTaskAry = array();  
    $searchTaskAry = $_POST['searchPendingTaskAry'] ;
    $searchTaskAry['iFromPopup'] = 1 ; 
    
    $_SESSION['searchPendingTaskAry'] = array();
    unset($_SESSION['searchPendingTaskAry']);
    $_SESSION['searchPendingTaskAry'] = $searchTaskAry;
    
    if($mode=='SHOW_MORE_FILES_SEARCH_POPUP')
    {
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTaskSearchPopup($searchTaskAry,false,false,$iPageNumber);
        echo "SUCCESS||||";
        echo showSearchBookingTables($incompleteTaskAry,$iPageNumber);
        die;
    }
    else
    { 
        echo "SUCCESS||||";
        echo display_all_recent_task_by_operator($searchTaskAry,'SEARCH_TASK',$kBooking,$iPageNumber);
        die;
    } 
}
else if(!empty($_POST['transportecaTeamAry']))
{ 
    $searchTaskAry = array(); 
    $searchTaskAry['idFileOwnerAry'] = $_POST['transportecaTeamAry']['idAdmin']; 
    $_SESSION['sessTransportecaTeamAry'] = $_POST['transportecaTeamAry']['idAdmin'] ;
    $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
    
    echo "SUCCESS||||";
    echo display_all_peding_tasks($incompleteTaskAry);
    die;
}
else if(!empty($_POST['updatedTaskAry']))
{ 
    $updatedTaskAry = $_POST['updatedTaskAry'] ;
    $szFromPage = sanitize_all_html_input(trim($updatedTaskAry['szFromPage']));
    $idBookingFile = sanitize_all_html_input(trim($updatedTaskAry['idBookingFile']));
    $szTaskType = sanitize_all_html_input(trim($_REQUEST['type']));
    $iAlreadyPaidBooking = sanitize_all_html_input(trim($updatedTaskAry['iAlreadyPaidBooking']));
    
    $updatedTaskAry['type'] = $szTaskType ;
    
    $iSingleOption = sanitize_all_html_input(trim($updatedTaskAry['iSingleOption']));
    if($iSingleOption==1)
    {
        $_POST['updatedTaskAry']['idServiceTerms'] = $updatedTaskAry['idServiceTerms_hidden'] ;
        $updatedTaskAry['idServiceTerms'] = $updatedTaskAry['idServiceTerms_hidden'];
    }
    
    if($kBooking->updatePendingTask($updatedTaskAry))
    { 
        $searchTaskAry = array(); 
        if(!empty($_SESSION['sessTransportecaTeamAry']))
        {
            $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
        }
        else
        {
            $searchTaskAry['idFileOwnerAry'][0] = 0;
            $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
        }
        $incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry); 
        if($iAlreadyPaidBooking==1)
        {
            $_POST['updatedTaskAry'] = array();
        }
        
        $kBooking->loadFile($idBookingFile); 
        echo "SUCCESS||||";
        echo display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true);
        echo "||||"; 
        echo display_pending_tray_overview($kBooking);
        echo "||||"; 
        if($szTaskType=='ASK_FOR_QUOTE')
        {
            echo display_task_ask_for_quote_container($kBooking);
        } 
        else if($szTaskType=='ESTIMATE')
        { 
            echo display_task_estimate_container($kBooking);
        }
        else if($szTaskType=='DISQUALIFY')
        {
            echo display_pending_task_validate_form($kBooking,2);
        }
        else if($szTaskType=='SAVE' || $szTaskType=='CONVERT_TO_RFQ')
        {
            echo display_pending_task_validate_form($kBooking,1);
        } 
        echo "||||";
        echo display_pending_task_header($kBooking,$idBookingFile);  
        if($szTaskType=='CONVERT_TO_RFQ')
        {
            echo "||||";
            echo display_pending_task_header($kBooking,$idBookingFile); 
        } 
        die;
    } 
    else
    {
        echo "ERROR||||"; 
        echo display_pending_task_validate_form($kBooking);
        die; 
    }
}
else if($mode=='UPLOAD_SELECTED_BOOKING_LABEL')
{
	$kCourierServices = new cCourierServices();
	$idBooking=(int)$_POST['idBooking'];
	$idFile=$kCourierServices->getIDFileBuBookingId($idBooking);
	$_SESSION['idBookingFileLabel']=$idFile;
	echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;
	die();

}
else if($_REQUEST['flag']=='delete_uploaded_file')
{
	$allFilename=$_REQUEST['allFilename'];
	$tempfilename=$_REQUEST['tempfilename'];
	$filename=$_REQUEST['filename'];
	$originFile=__UPLOAD_COURIER_LABEL_PDF__."/".$tempfilename;
	@unlink($originFile);
        $newAllFileStr='';
	if(!empty($allFilename))
	{
		$allUploadedFileArr=explode(";",$allFilename);
		if(!empty($allUploadedFileArr))
		{
			for($i=0;$i<count($allUploadedFileArr);$i++)
			{
				$newFileStr='';
				$uploadFileArr=explode("#####",$allUploadedFileArr[$i]);
				if($filename!=$uploadFileArr[0])
				{
					echo "<span class='uploadfilelist'><span>".$uploadFileArr[0]." (".round($uploadFileArr[2])." kB)</span><a href='javascript:void(0)' onclick='delete_uploaded_courier_label(\"".$uploadFileArr[0]."\",\"".$uploadFileArr[1]."\");'><img src='".__BASE_STORE_IMAGE_URL__."/delete-icon.png'></a></span>";
					
					$newFileStr=$allUploadedFileArr[$i];
					
					if($newAllFileStr!='')
					{
						$newAllFileStr= $newAllFileStr.";".$newFileStr;
					}
					else
					{
						$newAllFileStr=$newFileStr;
					}
				}
			}
		}
	}
        ?>
		<script>
			var count=$("#filecount").attr('value');
			var newcount=parseInt(count)-1;
			$("#filecount").attr('value',newcount);
			$("#file_name").attr('value','<?=$newAllFileStr?>');
		</script>
	<?php

}
else if(!empty($_POST['addBookingCourierLabelAry']) && $mode!='DELETE_COURIER_LABEL_PDF')
{ 
    $kCourierServices = new cCourierServices();  

    if($_REQUEST['mode']=='SUBMIT_DATA')
    {
        $_POST['addBookingCourierLabelAry']['iSendTrackingUpdates'] = (int)$_REQUEST['iSendTrackingUpdates']; 
        if($kCourierServices->createBookingCourierLabel($_POST['addBookingCourierLabelAry'],true))
        {
            $kTracking = new cTracking();
            
            if($kTracking->tracking_error == true)
            {
                echo "TRACKING_API_ERROR||||";
                echo sendShipperCourierLabel($_POST['addBookingCourierLabelAry']['idBooking'],$kCourierServices);
                die();
            }
            else
            { 
                $quotesDetailsAry = array();
                $quotesDetailsAry[1] = $_POST['addBookingCourierLabelAry'];
                $quotesDetailsAry[1]['id']=$_POST['addBookingCourierLabelAry']['idBookingQuote'];
                echo "SUCCESS||||";
                echo display_pending_quotes_overview_pane_courier_admin($quotesDetailsAry,$kBooking,false,true); 
                die;
            }
        } 
    }
    else 
    { 
        if($_REQUEST['saveFlag']==1)
        {
            if($kCourierServices->createBookingCourierLabel($_POST['addBookingCourierLabelAry'],false))
            {        
                $quotesDetailsAry = array();
                $quotesDetailsAry[1] = $_POST['addBookingCourierLabelAry'];
                $quotesDetailsAry[1]['id']=$_POST['addBookingCourierLabelAry']['idBookingQuote'];
                $kBooking->id=$_POST['addBookingCourierLabelAry']['idBooking'];
               // print_r($kBooking);
                echo "ERROR||||"; 
                echo display_pending_quotes_overview_pane_courier_admin($quotesDetailsAry,$kBooking,false,false,$kCourierServices); 
                die;
            } 
        }
        else 
        {
           if($kCourierServices->createBookingCourierLabelValidation($_POST['addBookingCourierLabelAry'],true))
           {        
               echo "SUCCESS||||";
               echo sendShipperCourierLabel($_POST['addBookingCourierLabelAry']['idBooking'],$kCourierServices);
               die();
           }
           else 
           {
              $quotesDetailsAry = array();
              $quotesDetailsAry[1] = $_POST['addBookingCourierLabelAry'];
              $quotesDetailsAry[1]['id']=$_POST['addBookingCourierLabelAry']['idBookingQuote'];
              echo "ERROR||||"; 
              echo display_pending_quotes_overview_pane_courier_admin($quotesDetailsAry,$kBooking,false,false,$kCourierServices); 
              die; 
           }
        }
    }
}
else if($mode=='OPEN_CHANGE_COURIER_LABEL_POPUP')
{
    $idBookingQuote = sanitize_all_html_input(trim($_REQUEST['idBookingQuote'])); 

    $quotesDetailsAry = array();
    $quotesDetailsAry = $kBooking->getAllBookingQuotesByForwarder($idForwarderSession,$idBookingQuote,false);
    $idLabelCreatedBy=$quotesDetailsAry[1]['idStatusUpdatedBy'];

    $kForwarderContact=new cForwarderContact();
    if($idLabelCreatedBy>0 && $quotesDetailsAry[1]['iUpdatedByAdmin']==0)
    {
        $kForwarderContact->load($idLabelCreatedBy); 

        $szName=$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
    }
    else
    {
            $szName="Transporteca";
    }
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup abandon-popup">
            <p class="close-icon" align="right">
                <a href="javascript:void(0);" onclick="closePopupUpdatedService('contactPopup');">
                <img src="<?php echo __BASE_STORE_IMAGE_URL__?>/close1.png" alt="close">
                </a>
            </p>
            <h5><?=t($t_base.'title/labels_already_uploaded')?></h5><br/>
            <p><?php echo $szName;?> <?=t($t_base.'messages/msg_13');?>?</p>
            <br class="clear-all" /> 
            <div class="btn-container" align="center">
                    <a href="javascript:void(0);"  onclick="changeCourierLabelStatus('<?php echo $idBookingQuote;?>');" id="cancel_button" class="button1" title="Yes"><span><?=t($t_base.'fields/yes');?></span></a>
                   <a href="javascript:void(0);" onclick="closePopupUpdatedService('contactPopup');" id="confirm_button" class="button2" title="No" ><span><?=t($t_base.'fields/no');?></span></a>

            </div>
        </div>
    </div>   
            
<?php    
}
else if($mode=='CHANGE_COURIER_LABEL_POPUP')
{
    $idBookingQuote = sanitize_all_html_input(trim($_REQUEST['idBookingQuote'])); 
         
    $quotesDetailsAry = array();
    $quotesDetailsAry = $kBooking->getAllBookingQuotesByForwarder($idForwarderSession,$idBookingQuote,false);
    
    $kBooking = new cBooking();
    $dtResponseTime = $kBooking->getRealNow();
    
    $kBooking = new cBooking();
    $fileLogsAry = array();
    $fileLogsAry['szTransportecaStatus'] = "S7";
    $fileLogsAry['iLabelStatus'] = __LABEL_NOT_SEND_FLAG__;
    $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
    $fileLogsAry['szForwarderBookingStatus'] = '';                                    
    $fileLogsAry['szForwarderBookingTask'] = 'T140';  

    if(!empty($fileLogsAry))
    {
        foreach($fileLogsAry as $key=>$value)
        {
            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }  
    $file_log_query = rtrim($file_log_query,",");   
    $kBooking->updateBookingFileDetails($file_log_query,$quotesDetailsAry[1]['idFile'],true); 
    
    $kCourierServices= new cCourierServices();
    $kCourierServices->reOpenBookingQuote($idBookingQuote,$quotesDetailsAry[1]['idBooking']); 
}
else if($mode=='SORT_ESTIMATE_BOOKING_LIST' || $mode=='SORT_ESTIMATE_QUOTE_LIST') 
{ 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szSortField = sanitize_all_html_input(trim($_POST['sort_field'])); 
    $szSortType = sanitize_all_html_input(trim($_POST['sort_by']));  
    
    $sorterAry = array();
    $sorterAry['szServiceSortField'] = $szSortField ;
    $sorterAry['szServiceSortType'] = $szSortType ; 
    $kBooking->loadFile($idBookingFile); 
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;  
    $bookingDataArr = $kBooking->getBookingDetails($idBooking); 
    
    $fCustomerExchangeRate = $bookingDataArr['fExchangeRate'];
    $szCustomerCurrency = $bookingDataArr['szCurrency']; 
    $szBookingRandomKey = $bookingDataArr['szBookingRandomNum'];
    
    $estimateSearchAry = array();
    $estimateSearchAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
    $estimateSearchAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
    if(!empty($sorterAry))
    {
        $estimateSearchAry['szSortField'] = $sorterAry['szServiceSortField'];
        $estimateSearchAry['szSortType'] = $sorterAry['szServiceSortType'];
    }
    
    if($mode=='SORT_ESTIMATE_QUOTE_LIST')
    {
        $quoteEstimateAry = array();
        $quoteEstimateAry = $kBooking->getAllQuoteForEstimatePane($estimateSearchAry);  

        echo "SUCCESS||||";
        echo estimate_booking_data_container($quoteEstimateAry,$idBookingFile,'SORT_ESTIMATE_QUOTE_LIST');
        die;
    }
    else
    {
        $bookingEstimateAry = array();
        $bookingEstimateAry = $kBooking->getAllBookingForEstimatePane($estimateSearchAry); 

        echo "SUCCESS||||";
        echo estimate_booking_data_container($bookingEstimateAry,$idBookingFile,'SORT_ESTIMATE_BOOKING_LIST');
        die;
    } 
} 
else if($mode=='CREATE_SHIPPING_LABEL')
{
    if($_POST['idCourierProduct']!='')
    {
        $idCourierProductArr=  explode("-", $_POST['idCourierProduct']);
        $idCourierProvider=(int)$idCourierProductArr[0];
        $idServiceAgreement=(int)$idCourierProductArr[1]; 
    }
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
    $apiInputRequestAry = $kBooking->recalculateCourierPricing($idBooking,false,true,false,true,$idCourierProvider,$idServiceAgreement);
    $kEasyPost = new cEasyPost();
    $trackingCodeAry = $kEasyPost->createShipmentLabelForTNT($apiInputRequestAry);
    
    echo "Hm to mar gye yaar ... ";
    die;
}
else if($mode=='CREATE_SHIPPING_LABEL_EASY_POST')
{
    if($_POST['idCourierProduct']!='')
    {
        $idCourierProductArr=  explode("-", $_POST['idCourierProduct']);
        $idCourierProvider=(int)$idCourierProductArr[0];
        $idServiceAgreement=(int)$idCourierProductArr[1];
        
    }
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
    $apiInputRequestAry = $kBooking->recalculateCourierPricing($idBooking,false,true,false,true,$idCourierProvider,$idServiceAgreement);
       
    $kEasyPost = new cEasyPost();
    $trackingCodeAry = $kEasyPost->getCourierDataEasypostApi($apiInputRequestAry,false,true); 
    /*
     * 1. $kEasyPost->iSuccessMessage==1 Means everything works correctly display success response
     * 2. $kEasyPost->iSuccessMessage==2 Means partially works correctly display success response in popup with API Error
     * 3. $kEasyPost->iSuccessMessage==3 Means Nothing works, display error response
    */
    if($kEasyPost->iSuccessMessage==1)
    {
        echo "SUCCESS||||";
        echo $trackingCodeAry['szLabelPdfName'];
        echo "||||";
        echo $trackingCodeAry['szTrackingNumber'];
        die;
    } 
    else if($kEasyPost->iSuccessMessage==2)
    {
        echo "SUCCESS_POPUP||||"; 
        $szTitle = "Error";
        $szMessage = "We could not produce labels for this shipment automatically. Please upload the labels manually.";
        $div_id = 'contactPopup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id,false,false,true);
        die;
    }
    else
    {
        echo "ERROR||||";
        //TO DO
        die;
    } 
}
else if($mode=='DELETE_COURIER_LABEL_PDF')
{
    $idFile=$_POST['addBookingCourierLabelAry']['idFile'];
    $idBookingFile=$_POST['addBookingCourierLabelAry']['idBookingFile'];
    $idBooking=$_POST['addBookingCourierLabelAry']['idBooking'];
    $file_name=$_POST['file_name'];
    $counter=(int)$_POST['counter'];
    $counter=$counter-1;
    $fileArr=explode(";",$file_name);
    
    if(!empty($fileArr))
    {
        for($i=0;$i<count($fileArr);++$i)
        {
            if($i!=$counter)
            {
                $newArr[]=$fileArr[$i];
            }
        } 
        $newFileStr=  implode(";", $newArr);
    }
    
    $kCourierServices= new cCourierServices();
    if((int)$idFile>0)
    { 
         $kCourierServices->deleteCourierLabelPdfFile($idFile,$idBooking,$idBookingFile); 
    }
    $totalCount=count($newArr);
    $_POST['addBookingCourierLabelAry']['file_name']=$newFileStr;
    $_POST['addBookingCourierLabelAry']['filecount']=$totalCount;
    
    $textpages="page";
    if($totalCount>1)
    {
        $textpages="pages";
    }
    
    $pageText=$totalCount." ".$textpages;
    $_POST['addBookingCourierLabelAry']['filecount']=$totalCount;
    $quotesDetailsAry = array();
    $quotesDetailsAry[1] = $_POST['addBookingCourierLabelAry'];
    $quotesDetailsAry[1]['id']=$_POST['addBookingCourierLabelAry']['idBookingQuote'];
    //print_r($quotesDetailsAry);
    echo display_pending_quotes_overview_pane_courier_admin($quotesDetailsAry,$kBooking,false,false,$kCourierServices); 
    die;
}
else if($mode=='TRACKING_NUMBER_STATUS')
{ 
    $idBooking=(int)$_POST['idBooking'];
    $szMasterTrackingNumber=trim($_POST['szMasterTrackingNumber']);
    $idServiceProvider=(int)$_POST['idServiceProvider'];
    $kTracking = new cTracking(); 
    $trackingARY = $kTracking->createAfterShipTracking($idBooking,$szMasterTrackingNumber,$idServiceProvider);  
    
    if($kTracking->tracking_error==true && __ENVIRONMENT__ == "LIVE")
    {
        echo "ERROR||||"; 
    }
    else
    {
        echo "SUCCESS||||";
    }
    echo $kTracking->szAfterShipTrackingNotification;
    die;
    
    /*
    * if((int)$_POST['iType']==1)
        {    
            $resArr = fedexTrackingNumberApi($idBooking,$szMasterTrackingNumber);
        }
        else if((int)$_POST['iType']==2)
        {
            $resArr = upsTrackingNumberApi($idBooking,$szMasterTrackingNumber);
        } 
        else if((int)$_POST['iType']==3)
        {
            $resArr = upsTrackingNumberApi($idBooking,$szMasterTrackingNumber);
        } 
        echo $resArr;
    */
    
}
else if($mode=='OPEN_PRIVATE_BOOKING_WINDOW')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));   
    $idAdmin = $_SESSION['admin_id'];  
//  $kBooking->createAnonymoumsBooking($idBookingFile); 
    
    $kBooking->loadFile($idBookingFile);
    $idBooking = $kBooking->idBooking;
    if($idBooking>0)
    { 
        $postSearchAry = $kBooking->getBookingDetails($idBooking);  
        $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
       
        $_SESSION['admin_test_search_'.$szBookingRandomNum] = 1;
        $_SESSION['anonymous_user_logged_in'] = 1;
         
        $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
        $kConfig = new cConfig();
        $kConfig->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig->szCountryISO ;
        $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
        $szCountryStrUrl .= $kConfig->szCountryISO ;
        
        $kConfig = new cConfig();
        $languageArr=$kConfig->getLanguageDetails('',$postSearchAry['iBookingLanguage']);
        
           
        echo "SUCCESS||||";
        if(!empty($languageArr) && $postSearchAry['iBookingLanguage']!=__LANGUAGE_ID_ENGLISH__)
        {
            $szLanguageCode=  strtolower($languageArr[0]['szLanguageCode']);
            //echo __MAIN_SITE_HOME_PAGE_URL__."/da/services/".$szBookingRandomNum."/best-".$szCountryStrUrl."/" ; 
            echo __MAIN_SITE_HOME_PAGE_URL__."/".$szLanguageCode."/adminSearch/".$szBookingRandomNum."/" ; 
            die; 
        }
        else
        {
            //echo __MAIN_SITE_HOME_PAGE_URL__."/services/".$szBookingRandomNum."/best-".$szCountryStrUrl."/" ; 
            echo __MAIN_SITE_HOME_PAGE_URL__."/adminSearch/".$szBookingRandomNum."/" ; 
            die; 
        } 
    }
} 
else if($mode=='CHANGE_REMIND_EMAIL_TEMPLATE_BODY')
{ 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $idTemplate = (int)$_REQUEST['idTemplate'];
    $iTemplateLanguage = sanitize_all_html_input(trim($_REQUEST['iLanguage']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $idCrmEmailLogs = sanitize_all_html_input(trim($_REQUEST['email_id']));
    $szOperationType = sanitize_all_html_input(trim($_REQUEST['operation_type']));
    
    $logEmailsArys = array();
    if($idCrmEmailLogs>0)
    { 
        $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs); 
    }
    
    $kBooking->loadFile($idBookingFile); 
    if($kBooking->idBookingFile>0)
    {
        echo "SUCCESS||||";
        echo display_email_template_body($kBooking,$idTemplate,$iTemplateLanguage,false,$szFromPage,$logEmailsArys,$szOperationType);
        die;
    }
}
else if($mode == "ADD_EDIT_REMINDER_EMAIL_TEMPLATE")
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $idCrmEmailLogs = sanitize_all_html_input(trim($_REQUEST['email_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $szAction = sanitize_all_html_input(trim($_REQUEST['action'])); 
    
    $idTemplate = (int)$_REQUEST['idTemplate'];
    $iTemplateLanguage = (int)$_REQUEST['iLanguage'];
     
    echo "SUCCESS_POPUP||||";
    echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,$idTemplate,$szTaskStatus,$iTemplateLanguage,$idCrmEmailLogs,$szFromPage,$szAction);
    die();
}
else if($mode == "UPDATE_REMIND_EMAIL_TEMPLATE")
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));
    $idTemplate = sanitize_all_html_input(trim($_REQUEST['idTemplate']));
    $iTemplateLanguage = sanitize_all_html_input(trim($_REQUEST['iLanguage']));
    $closeFlag = sanitize_all_html_input(trim($_REQUEST['closeFlag']));
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['addEditRemindEmailTemplate']['idCrmEmailLogs']));
    $szFromPage = sanitize_all_html_input(trim($_POST['addEditRemindEmailTemplate']['szFromPage']));
    $szAction = sanitize_all_html_input(trim($_POST['addEditRemindEmailTemplate']['szAction']));
       
    $kCrm = new cCRM();
    $kSendEmail = new cSendEmail();
                        
    if(is_base64_encoded($_POST['addEditRemindEmailTemplate']['szReminderEmailBody']))
    {
        $_POST['addEditRemindEmailTemplate']['szReminderEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['addEditRemindEmailTemplate']['szReminderEmailBody'])));
    }
    if(!empty($_POST['addEditRemindEmailTemplate']))
    {
        if($kConfig->updateRemindEmailTemplate($_POST['addEditRemindEmailTemplate'],$idTemplate))
        {
            if(trim($closeFlag) == "CLOSE_POPUP_TRUE")
            {
                $kBooking->loadFile($idBookingFile); 
                if($kBooking->idBookingFile>0)
                { 
                    if($szFromPage=='PENDING_TRAY_CUSTOMER_LOG' || $szFromPage=='PENDING_TRAY_LOG')
                    {
                        $logEmailsArys = array(); 
                        $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);  
                        
                        echo "SUCCESS_CRM||||";
                        echo $idCrmEmailLogs."||||";
                        echo display_pending_tray_crm_emails_operation($kCrm,$szAction,$logEmailsArys,$idBookingFile,$szFromPage,$idTemplate,$iTemplateLanguage);
                        //echo display_pending_tray_email_logs($kBooking,$szFromPage);
                    }
                    else
                    {
                        echo "SUCCESS||||";
                        echo display_task_reminder_container($kBooking,false,$idTemplate,$iTemplateLanguage);
                    }
                    die;
                }
            }
            else if(trim($closeFlag) == "CLOSE_POPUP_FALSE")
            {
                $kBooking->loadFile($idBookingFile); 
                if($kBooking->idBookingFile>0)
                {
                    if($szFromPage=='PENDING_TRAY_CUSTOMER_LOG' || $szFromPage=='PENDING_TRAY_LOG')
                    {
                        $logEmailsArys = array(); 
                        $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);  
                        
                        echo "SUCCESS_CRM_POPUP||||";  
                        echo $idCrmEmailLogs."||||";
                        echo display_pending_tray_crm_emails_operation($kCrm,$szAction,$logEmailsArys,$idBookingFile,$szFromPage,$idTemplate,$iTemplateLanguage);
                        //echo display_crm_operations_reply_email($kCrm,$kBooking,$logEmailsArys,$szAction,$szFromPage);
                    }
                    else
                    {
                        echo "SUCCESS_POPUP||||";
                        echo display_task_reminder_container($kBooking,false,$idTemplate,$iTemplateLanguage);
                    } 
                    echo "||||";
                    //echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,$idTemplate,$szTaskStatus,$iTemplateLanguage);
                    echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,$idTemplate,$szTaskStatus,$iTemplateLanguage,$idCrmEmailLogs,$szFromPage,$szAction);
                }
            }
        }
    }
    else
    {
         echo "ERROR||||";
         //echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,$idTemplate,$szTaskStatus,$iTemplateLanguage);
         echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,$idTemplate,$szTaskStatus,$iTemplateLanguage,$idCrmEmailLogs,$szFromPage,$szAction);
         die();
    } 
}
else if($mode == "SAVE_NEW_REMIND_EMAIL_TEMPLATE")
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));
    $idTemplate = sanitize_all_html_input(trim($_REQUEST['idTemplate']));
    $iTemplateLanguage = sanitize_all_html_input(trim($_REQUEST['iLanguage']));
    $closeFlag = sanitize_all_html_input(trim($_REQUEST['closeFlag']));
    
    $idCrmEmailLogs = sanitize_all_html_input(trim($_POST['addEditRemindEmailTemplate']['idCrmEmailLogs']));
    $szFromPage = sanitize_all_html_input(trim($_POST['addEditRemindEmailTemplate']['szFromPage']));
    $szAction = sanitize_all_html_input(trim($_POST['addEditRemindEmailTemplate']['szAction']));
    
    if(is_base64_encoded($_POST['addEditRemindEmailTemplate']['szReminderEmailBody']))
    {
        $_POST['addEditRemindEmailTemplate']['szReminderEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['addEditRemindEmailTemplate']['szReminderEmailBody'])));
    }
    if(!empty($_POST['addEditRemindEmailTemplate']))
    {
        if($idNewTemplate=$kConfig->checkRemindCMSTemplateExists($_POST['addEditRemindEmailTemplate']['szFriendlyName'],$iTemplateLanguage))
        {
            echo "SUCCESS_OVERWRITE_POPUP||||";
            echo showoverwritepopup($kConfig,$idBookingFile,$idNewTemplate,$szTaskStatus,$iTemplateLanguage,$closeFlag);
            die();
        }
        else
        {
            $idNewOldTemplate= $kConfig->checkRemindCMSTemplateExists($_POST['addEditRemindEmailTemplate']['szFriendlyName']);
            $_POST['addEditRemindEmailTemplate']['idNewOldTemplate'] =$idNewOldTemplate; 
            $idNewTemplate = $kConfig->saveNewRemindEmailTemplate($_POST['addEditRemindEmailTemplate']);

            if(!empty($idNewTemplate) || $idNewTemplate>0)
            {
                if(trim($closeFlag) == "CLOSE_POPUP_TRUE")
                {
                    $kBooking->loadFile($idBookingFile); 
                    if($kBooking->idBookingFile>0)
                    {
                        if($szFromPage=='PENDING_TRAY_CUSTOMER_LOG' || $szFromPage=='PENDING_TRAY_LOG')
                        {
                            $logEmailsArys = array(); 
                            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);  

                            echo "SUCCESS_CRM||||";
                            echo $idCrmEmailLogs."||||";
                            echo display_pending_tray_crm_emails_operation($kCrm,$szAction,$logEmailsArys,$idBookingFile,$szFromPage,$idNewTemplate,$iTemplateLanguage);
                            //echo display_pending_tray_email_logs($kBooking,$szFromPage);
                        }
                        else
                        {
                            echo "SUCCESS||||";
                            echo display_task_reminder_container($kBooking,false,$idNewTemplate,$iTemplateLanguage);
                        }
                    }
                }
                elseif(trim($closeFlag) == "CLOSE_POPUP_FALSE")
                {
                    $kBooking->loadFile($idBookingFile); 
                    if($kBooking->idBookingFile>0)
                    {
                        if($szFromPage=='PENDING_TRAY_CUSTOMER_LOG' || $szFromPage=='PENDING_TRAY_LOG')
                        {
                            $logEmailsArys = array(); 
                            $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);  

                            echo "SUCCESS_CRM_POPUP||||";  
                            echo $idCrmEmailLogs."||||";
                            echo display_pending_tray_crm_emails_operation($kCrm,$szAction,$logEmailsArys,$idBookingFile,$szFromPage,$idNewTemplate,$iTemplateLanguage);
                            //echo display_crm_operations_reply_email($kCrm,$kBooking,$logEmailsArys,$szAction,$szFromPage);
                        }
                        else
                        {
                            echo "SUCCESS_POPUP||||";
                            echo display_task_reminder_container($kBooking,false,$idNewTemplate,$iTemplateLanguage);
                            echo "||||";
                            echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,$idNewTemplate,$szTaskStatus,$iTemplateLanguage);
                        }
                    }
                }
            }
            else
            {
                 echo "ERROR||||";
                 echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,0,$szTaskStatus,$iTemplateLanguage);
                 die();
            }
        }
    }
    else
    {
         echo "ERROR||||";
         echo add_edit_Reminder_Email_Template($kConfig,$idBookingFile,0,$szTaskStatus,$iTemplateLanguage);
         die();
    }
}
elseif($mode == "DELETE_REMIND_EMAIL_POP_UP")
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));
    $idTemplate = sanitize_all_html_input(trim($_REQUEST['idTemplate']));
    echo "SUCCESS_POPUP||||";
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup" > 
            <h2>Delete Email-Template </h2>
            <div class="clearfix email-fields-container">
                <h3>Are you sure to delete selected email template?</h3>
            </div>
            <div class="clearfix" style="width:100%;">
                <div class="btn-container"> 
                    <p align="center">
                        <a href="javascript:void(0)" class="button1" onclick="confirmDeleteRemindEmailTemplate('<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>')">
                            <span>DELETE</span>
                        </a>
                        <a href="javascript:void(0)"  class="button2" onclick="closeTip('delete-msg');">
                            <span>CLOSE</span>
                        </a>
                    </p>
                </div> 
            </div>
        </div>
    </div>
    <?php
}
elseif($mode == "CONFIRM_DELETE_REMIND_EMAIL_POP_UP")
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $szTaskStatus = sanitize_all_html_input(trim($_REQUEST['type']));
    $idTemplate = sanitize_all_html_input(trim($_REQUEST['idTemplate']));
    
    if($kConfig->deleteRemindEmailTemplate($idTemplate))
    {
        $kBooking->loadFile($idBookingFile); 
        echo "SUCCESS||||";
        echo display_task_reminder_container($kBooking);
        die();
    }
    else
    {
        echo "ERROR||||";
        ?>
        <div id="popup-bg"></div>
        <div id="popup-container" >
            <div class="popup" > 
                <h2>Delete Email-Template </h2>
                <div class="clearfix email-fields-container">
                    <h3>Email template not be deleted.</h3>
                </div>
                <div class="clearfix" style="width:100%;">
                    <div class="btn-container"> 
                        <p align="center">
                            <a href="javascript:void(0)"  class="button2" onclick="closeTip('delete-msg');">
                                <span>CLOSE</span>
                            </a>
                        </p>
                    </div> 
                </div>
            </div>
        </div>
        <?php
    }
    
}
else if($mode=='OPEN_PENDING_TRAY_UPDATE_CARGO_POPUP')
{
    $idBooking=(int)$_POST['idBooking'];
    $iSearchMiniVersion=(int)$_POST['iSearchMiniVersion'];
    $iAlreadyPaidBooking=(int)$_POST['iAlreadyPaidBooking'];
    $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,false);
    //print_r($cargoDetailArr);
    updateCargoDetailsPendingTray($cargoDetailArr,'',$iAlreadyPaidBooking);
}
else if($mode=='REMOVE_PENDING_TRAY_UPDATE_CARGO_POPUP')
{
    $idBooking=(int)$_POST['idBooking'];
    //print_r($cargoDetailArr);
    updateCargoDetailsPendingTray($cargoDetailArr);
}
else if($mode=='ADD_MORE_PENDING_TRAY_UPDATE_CARGO_POPUP')
{
    $iValue=(int)$_POST['iValue'];
    $iSearchMiniVersion=(int)$_POST['iSearchMiniVersion'];
    pendingTrayCargoLineHtml($iValue,$iSearchMiniVersion);
}
else if($mode=='UPDATE_CARGO_LINE_FROM_PENDING_TRAY_UPDATE_CARGO_POPUP')
{
   if($kBooking->updateCargoLineFromPendingTray($_POST))
   {
       $kWHSSearch=new cWHSSearch();
       $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($_POST['idBooking'],false);
       $totalCargoLines=count($cargoDetailArr);
        $totalCargoWeight=0;
        $totalCargoColli=0;
        $fTotalCargoVolume=0;
        $checkWeightFlagForStandartFromTo=false;
        $checkVolumeFlagForStandartFromTo=false;
        if(!empty($cargoDetailArr))
        {
            foreach($cargoDetailArr as $cargoDetailArrs)
            {
                $fWeight=0;
                $fWeight=$cargoDetailArrs['fWeight']*$cargoDetailArrs['iQuantity'];
                if($cargoDetailArrs['fWeight']==0)
                {
                    $checkWeightFlagForStandartFromTo=true;
                }        
                //$totalCargoWeight=$totalCargoWeight+$fWeight;
                $totalCargoColli=$totalCargoColli+$cargoDetailArrs['iColli'];

                if($cargoDetailArrs['idWeightMeasure']==1)  // kg
                {
                    $totalCargoWeight += ($fWeight);
                }
                else
                {
                    $fKgFactor = $kWHSSearch->getWeightFactor($cargoDetailArrs['idWeightMeasure']);
                    if($fKgFactor>0)
                    {
                        $totalCargoWeight += ((($fWeight)/$fKgFactor ));
                    }	
                }
                
                //if($_POST['iSearchMiniVersion']=='8')
                //{
                    if($cargoDetailArrs['fVolume']==0)
                    {
                        $checkVolumeFlagForStandartFromTo=true;
                    }
                    if((float)$cargoDetailArrs['fVolume']>0)
                    {
                        $fTotalCargoVolume += $cargoDetailArrs['fVolume']* $cargoDetailArrs['iQuantity'];
                    }
                    else
                    {
                        if($cargoDetailArrs['idCargoMeasure']==1)  // cm
                        {
                            $fTotalCargoVolume += ($cargoDetailArrs['fLength']/100) * ($cargoDetailArrs['fWidth']/100) * ($cargoDetailArrs['fHeight']/100) * $cargoDetailArrs['iQuantity'];
                        }
                        else
                        {
                            $fCmFactor = $kWHSSearch->getCargoFactor($cargoDetailArrs['idCargoMeasure']);
                            if($fCmFactor>0)
                            {
                                $fTotalCargoVolume += (($cargoDetailArrs['fLength']/$fCmFactor )/100) * (($cargoDetailArrs['fWidth']/$fCmFactor)/100) * (($cargoDetailArrs['fHeight']/$fCmFactor)/100) * $cargoDetailArrs['iQuantity'];
                            }	
                        }
                    }
                /*}
                else
                {
                    if($cargoDetailArrs['idCargoMeasure']==1)  // cm
                    {
                        $fTotalCargoVolume += ($cargoDetailArrs['fLength']/100) * ($cargoDetailArrs['fWidth']/100) * ($cargoDetailArrs['fHeight']/100) * $cargoDetailArrs['iQuantity'];
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($cargoDetailArrs['idCargoMeasure']);
                        if($fCmFactor>0)
                        {
                            $fTotalCargoVolume += (($cargoDetailArrs['fLength']/$fCmFactor )/100) * (($cargoDetailArrs['fWidth']/$fCmFactor)/100) * (($cargoDetailArrs['fHeight']/$fCmFactor)/100) * $cargoDetailArrs['iQuantity'];
                        }	
                    }
                }*/
                

            }
        }
        
        $totalCargoWeight=round_up((float)$totalCargoWeight,2);
        $fTotalCargoVolume = format_volume($fTotalCargoVolume);
        if($checkWeightFlagForStandartFromTo)
        {
            $totalCargoWeight="-";
        }
        if($checkVolumeFlagForStandartFromTo)
        {
            $fTotalCargoVolume="-";
        }
         
        $iNumColliText=t($t_base.'fields/cargo_details_line'); if((int)$totalCargoLines>1){ $iNumColliText=t($t_base.'fields/cargo_details_lines'); }
     
        echo "SUCCESS||||";
        echo t($t_base.'fields/cargo_details').": ".$totalCargoLines." ".$iNumColliText.", ".$fTotalCargoVolume." cbm, ".$totalCargoWeight." kg, ".(int)$totalCargoColli." colli";
        die();
   }
   else
   {
       echo "ERROR||||";
       echo updateCargoDetailsPendingTray(array(),$kBooking);
        die();
   }
}
else if($mode=='UPDATE_CARGO_LINE_CONFIRMATION')
{
    $idBooking=(int)$_POST['idBooking'];
    ?>
        <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup" > 
            <h2>Update Cargo Line Total </h2><br/>
            <p>Are you sure you want to update the cargo details in the booking?</p>
            <div class="clearfix" style="width:100%;">
                <div class="btn-container"> 
                    <p align="center">
                        <a href="javascript:void(0)"  class="button2" onclick="showHide('contactPopup');">
                            <span>Cancel</span>
                        </a>
                        <a href="javascript:void(0)"  id="update_cargo_line" onclick="updateCargoLineConfirmed('<?php echo $idBooking;?>');" class="button1">
                            <span>SAVE</span>
                        </a>
                    </p>
                </div> 
            </div>
        </div>
    </div>
    <?php
}
else if($mode=='UPDATE_CARGO_LINE')
{
    $idBooking=(int)$_POST['idBooking'];
    $iSearchMiniVersion=(int)$_POST['iSearchMiniVersion'];
    
    $kWHSSearch=new cWHSSearch();
       $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($_POST['idBooking'],false);
       $totalCargoLines=count($cargoDetailArr);
        $totalCargoWeight=0;
        $totalCargoColli=0;
        $fTotalCargoVolume=0;
        $checkWeightFlagForStandartFromTo=false;
        $checkVolumeFlagForStandartFromTo=false;
        if(!empty($cargoDetailArr))
        {
            foreach($cargoDetailArr as $cargoDetailArrs)
            {
                $fWeight=0;
                $fWeight=$cargoDetailArrs['fWeight']*$cargoDetailArrs['iQuantity'];
                if($cargoDetailArrs['fWeight']==0)
                {
                    $checkWeightFlagForStandartFromTo=true;
                }
                        
                        
                //$totalCargoWeight=$totalCargoWeight+$fWeight;
                $totalCargoColli=$totalCargoColli+$cargoDetailArrs['iColli'];
                $iTotalQuantity=$iTotalQuantity+$cargoDetailArrs['iQuantity'];
                if($cargoDetailArrs['idWeightMeasure']==1)  // kg
                {
                    $totalCargoWeight += ($fWeight);
                }
                else
                {
                    $fKgFactor = $kWHSSearch->getWeightFactor($cargoDetailArrs['idWeightMeasure']);
                    if($fKgFactor>0)
                    {
                        $totalCargoWeight += ((($fWeight)/$fKgFactor ));
                    }	
                }
                
                
                /*if($iSearchMiniVersion==8)
                {*/
                    if($cargoDetailArrs['fVolume']==0)
                    {
                        $checkVolumeFlagForStandartFromTo=true;
                    }
                    if((float)$cargoDetailArrs['fVolume']>0)
                    {
                        $fTotalCargoVolume += $cargoDetailArrs['fVolume']* $cargoDetailArrs['iQuantity'];
                    }
                    else
                    {
                        if($cargoDetailArrs['idCargoMeasure']==1)  // cm
                        {
                            $fTotalCargoVolume += ($cargoDetailArrs['fLength']/100) * ($cargoDetailArrs['fWidth']/100) * ($cargoDetailArrs['fHeight']/100) * $cargoDetailArrs['iQuantity'];
                        }
                        else
                        {
                            $fCmFactor = $kWHSSearch->getCargoFactor($cargoDetailArrs['idCargoMeasure']);
                            if($fCmFactor>0)
                            {
                                $fTotalCargoVolume += (($cargoDetailArrs['fLength']/$fCmFactor )/100) * (($cargoDetailArrs['fWidth']/$fCmFactor)/100) * (($cargoDetailArrs['fHeight']/$fCmFactor)/100) * $cargoDetailArrs['iQuantity'];
                            }	
                        }
                    }
                /*}
                else
                {    
                    if($cargoDetailArrs['idCargoMeasure']==1)  // cm
                    {
                        $fTotalCargoVolume += ($cargoDetailArrs['fLength']/100) * ($cargoDetailArrs['fWidth']/100) * ($cargoDetailArrs['fHeight']/100) * $cargoDetailArrs['iQuantity'];
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($cargoDetailArrs['idCargoMeasure']);
                        if($fCmFactor>0)
                        {
                            $fTotalCargoVolume += (($cargoDetailArrs['fLength']/$fCmFactor )/100) * (($cargoDetailArrs['fWidth']/$fCmFactor)/100) * (($cargoDetailArrs['fHeight']/$fCmFactor)/100) * $cargoDetailArrs['iQuantity'];
                        }	
                    }
                }*/

            }
        }
        
        $totalCargoWeight=round_up((float)$totalCargoWeight,2);
        $fTotalCargoVolume = format_volume($fTotalCargoVolume); 
        if($checkWeightFlagForStandartFromTo)
        {
            $totalCargoWeight='';            
        }
        if($checkVolumeFlagForStandartFromTo)
        {
            $fTotalCargoVolume='';
        }
        
        
        $szVolWeight = $fTotalCargoVolume." cbm / ".number_format((float)$totalCargoWeight)." kg / ".number_format((int)$totalCargoColli)." colli";
        $kBooking->updateCargoLineTotalInBookingFromPendingTray($idBooking,$fTotalCargoVolume,$totalCargoWeight,$totalCargoColli,$iTotalQuantity);
        echo $totalCargoWeight."||||";
        echo $fTotalCargoVolume."||||";
        echo $totalCargoColli."||||";
        echo $szVolWeight;
        die();
}
else if($mode=='OPEN_MULTIPLE_CLOSE_BOOKING_FILE')
{
    $totalCount=(int)$_POST['totalCount'];
    if($totalCount==1)
    {
        $text="file";
    }else
    {
        $text="files";
    }
    ?>
        <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup" > 
            <h3>Do you want to close <?php echo number_format($totalCount);?> <?php echo $text;?>?</h3>
            <div class="clearfix" style="width:100%;">
                    <div class="btn-container"> 
                        <p align="center">
                            <a href="javascript:void(0)"  id="yes_button" class="button1" onclick="closeMultipleBookingFiles();">
                                <span>Yes</span>
                            </a>
                            <a href="javascript:void(0)" id="no_button"  class="button2" onclick="closeConactPopUp('0');">
                                <span>No</span>
                            </a>
                        </p>
                    </div> 
                </div>
        </div>
    </div>
    <?php
}
else if($mode=='CONFIRM_MULTIPLE_CLOSE_BOOKING_FILE')
{
    $idFileForClose=trim($_POST['idFileForClose']);
    $idFileForCloseArr=explode(";",$idFileForClose);
    if(!empty($idFileForCloseArr))
    {
        foreach($idFileForCloseArr as $idFileForCloseArrs)
        {
            $kBooking->closeTask($idFileForCloseArrs);
        }
    }
    //echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;
    echo "SUCCESS||||";
    die();
}
else if($mode=='SAVE_EMAIL_AS_DRAFT')
{
    ignore_user_abort(true);
    //set_time_limit(0);
    $kCRM = new cCRM();
    $idCrm=(int)$_POST['idCrm'];
    
    $szReminderSubject=  sanitize_all_html_input(trim($_POST['addCrmOperationAry']['szReminderSubject']));
    $szReminderEmailBody=  htmlspecialchars_decode(urldecode(base64_decode(trim($_POST['addCrmOperationAry']['szReminderEmailBody']))));
    
    $kCRM->updateDraftEmailData($szReminderSubject,$szReminderEmailBody,$idCrm); 
    
    echo "SUCCESS||||";
    
}
else if($mode=='SAVE_EMAIL_REMIND_AS_DRAFT')
{
    $idTemplate = (int)$_POST['idTemplate'];
    $idBookingOverView = (int)$_POST['idBookingOverView'];
   
    $kCRM = new cCRM();
    $kCRM->updateDraftEmailRemindData($_POST['addSnoozeAry'],$idTemplate,$idBookingOverView); 
    
    echo "SUCCESS||||";
}
else if($mode=='OPEN_SEARCH_POPUP_CLICK_ON_HISTORY')
{
     $searchTaskAry['iFlag']=$_POST['iFlag'];
     $searchTaskAry['idCustomer']=$_POST['idUser'];
    $iPageNumber='1';
    $searchTaskAry['iFromHistory'] = 1 ;
    $searchTaskAry['iFromPopup']=1;
    $incompleteTaskArySearch = $kBooking->getAllBookingWithOpenedTaskSearchPopup($searchTaskAry,false,false,$iPageNumber);                    
    $_POST['searchPendingTaskAry']['idCustomer']=$_POST['idUser'];
    $_POST['searchPendingTaskAry']['iFlag']=$_POST['iFlag'];
    echo "SUCCESS||||";
    echo display_pending_task_popup($searchTaskAry,'SEARCH_TASK',$kBooking,$incompleteTaskArySearch);
    die();
}    
else if($mode=='OPEN_MAILING_ADDESS_DELIVERY_INSTRUCTION')
{
    $idBooking =(int)$_POST['idBooking'];
    $dtCollectionStartTime =  sanitize_all_html_input(trim($_POST['dtCollectionStartTime']));
    $dtCollectionEndTime =  sanitize_all_html_input(trim($_POST['dtCollectionEndTime']));
    $dtCollection =  sanitize_all_html_input(trim($_POST['dtCollection']));
    $iCollection =  sanitize_all_html_input(trim($_POST['iCollection']));
    
    $additionalParamsAry = array();
    $additionalParamsAry['dtCollectionStartTime'] = $dtCollectionStartTime;
    $additionalParamsAry['dtCollectionEndTime'] = $dtCollectionEndTime;
    $additionalParamsAry['dtCollection'] = $dtCollection;
    $additionalParamsAry['iCollection'] = $iCollection;
    
    createLabelMailAddressInstructionsPopup($idBooking,$additionalParamsAry);
}
else if($mode=='ADD_MAILING_ADDESS_DELIVERY_INSTRUCTION')
{
    ini_set ('max_execution_time',360000);
    ini_set('pcre.backtrack_limit', 10000000);
    
    $addMailAddressInstructionArr = $_POST['addMailAddressInstructionArr']; 
    $labelErrorFieldsArr = $_POST['labelErrorFields']; 
    $labelShipConFieldsArr = $_POST['labelShipConFields']; 
       
    $idBooking =(int)$addMailAddressInstructionArr['idBooking'];
    $szMailAddress =  sanitize_all_html_input(trim($addMailAddressInstructionArr['szMailAddress']));
    $szDeliveryInstructions =  sanitize_all_html_input(trim($addMailAddressInstructionArr['szDeliveryInstructions']));
    $dtCollectionStartTime=  sanitize_all_html_input(trim($addMailAddressInstructionArr['dtCollectionStartTime']));
    $dtCollectionEndTime=  sanitize_all_html_input(trim($addMailAddressInstructionArr['dtCollectionEndTime']));
    $dtCollection=  sanitize_all_html_input(trim($addMailAddressInstructionArr['dtCollection']));
    $iCollection=  sanitize_all_html_input(trim($addMailAddressInstructionArr['iCollection']));
    $idCourierProduct = sanitize_all_html_input(trim($addMailAddressInstructionArr['idCourierProduct']));
    $iDisplayCargoDescField = sanitize_all_html_input(trim($addMailAddressInstructionArr['iDisplayCargoDescField']));
    $szCargoDescription = sanitize_all_html_input(trim($addMailAddressInstructionArr['szCargoDescription']));
    $iDisplayIndividualLineCargoDescField = sanitize_all_html_input(trim($addMailAddressInstructionArr['iDisplayIndividualLineCargoDescField']));
    $iDisplayIndividualLinePieceDescField = sanitize_all_html_input(trim($addMailAddressInstructionArr['iDisplayIndividualLinePieceDescField']));
    
    $iCheckShipperCityFlag = sanitize_all_html_input(trim($addMailAddressInstructionArr['iCheckShipperCityFlag']));
    $iCheckConsigneeCityFlag = sanitize_all_html_input(trim($addMailAddressInstructionArr['iCheckConsigneeCityFlag']));
    $szShipperCity = sanitize_all_html_input(trim($addMailAddressInstructionArr['szShipperCity']));
    $szConsigneeCity = sanitize_all_html_input(trim($addMailAddressInstructionArr['szConsigneeCity'])); 
     
    $szCargoDescription = sanitize_all_html_input(trim($addMailAddressInstructionArr['szCargoDescription']));
    
    $cargoLineDescriptionAry = array();
    $cargoLineDescriptionAry = $addMailAddressInstructionArr['szCargoLineDescription'];
     
    $pieceDescriptionAry = array();
    $pieceDescriptionAry = $addMailAddressInstructionArr['szSellerReference'];
     
    $res_ary=array();
    $res_ary['szMessageToSender'] = $szMailAddress ;  
    $res_ary['szDeliveryInstructions'] = $szDeliveryInstructions;  
    $update_query = "";
    
    if(!empty($res_ary))
    {
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    } 
    $update_query = rtrim($update_query,",");    
    $pdfLabelFileAry = array();
    if($kBooking->updateDraftBooking($update_query,$idBooking))
    { 
        $additionalOptionsAry = array();
        $additionalOptionsAry['dtCollection'] = $dtCollection;
        $additionalOptionsAry['dtCollectionStartTime'] = $dtCollectionStartTime;
        $additionalOptionsAry['dtCollectionEndTime'] = $dtCollectionEndTime;
        $additionalOptionsAry['iCollection'] = $iCollection;
        $additionalOptionsAry['idCourierProduct'] = $idCourierProduct;
        $additionalOptionsAry['iDisplayCargoDescField'] = $iDisplayCargoDescField;
        $additionalOptionsAry['szCargoDescription'] = $szCargoDescription; 
        $additionalOptionsAry['iDisplayIndividualLineCargoDescField'] = $iDisplayIndividualLineCargoDescField; 
        $additionalOptionsAry['iCheckShipperCityFlag'] = $iCheckShipperCityFlag;
        $additionalOptionsAry['szShipperCity'] = $szShipperCity;
        $additionalOptionsAry['iCheckConsigneeCityFlag'] = $iCheckConsigneeCityFlag;
        $additionalOptionsAry['szConsigneeCity'] = $szConsigneeCity; 
        $additionalOptionsAry['cargoLineDescriptionAry'] = $cargoLineDescriptionAry; 
        $additionalOptionsAry['labelErrorFieldsAry'] = $labelErrorFieldsArr;
        $additionalOptionsAry['labelShipConFieldsAry'] = $labelShipConFieldsArr;  
        $additionalOptionsAry['iDisplayIndividualLinePieceDescField'] = $iDisplayIndividualLinePieceDescField;
        $additionalOptionsAry['pieceDescriptionAry'] = $pieceDescriptionAry; 
         
        $kLabel = new cLabels(); 
        $pdfLabelFileAry = $kLabel->downloadLabelFromTNTApi($idBooking,$additionalOptionsAry);   
         
        if(!empty($pdfLabelFileAry['documents']))
        {
            $szPdfFileSerializeStr = serialize($pdfLabelFileAry['documents']);
        }
    } 
    if(!empty($pdfLabelFileAry))
    {
        //To display in File editor we have converted this to Json
        //$jsonAry[0]['name'] = $pdfLabelFileAry['szPdfFileName'];
        ob_end_clean(); 
        echo "SUCCESS||||";
        echo $szPdfFileSerializeStr;
        echo "||||";
        echo $pdfLabelFileAry['szTrackingNumber'];
        die;
    }
    else
    {
        echo "ERROR||||"; 
        $szTitle = "Labels can not be generated";
        if(!empty($kLabel->szLabelApiError))
        {
            $szMessage = "";
            if(!empty($kLabel->szLabelApiErrorHeading))
            {
                $szMessage .= $kLabel->szLabelApiErrorHeading;
            }
            $szMessage .= "Due to following errors, Label can not be generated. Please correct the following <br><br>";
            $szMessage .= "<ul>".$kLabel->szLabelApiError."</ul>";
        }
        else
        {
            $szMessage = "We could not produce labels for this shipment automatically. Please upload the labels manually.";
        }
        
        /*
        if(!empty($kLabel->szLabelLogStr) || !empty($kLabel->szRequestXmlFileName))
        {
            $szMessage .= "<br><br> Developer Notes:<br><strong> Processing Steps: </strong><br> ".$kLabel->szLabelLogStr;
            if(!empty($kLabel->szRequestXmlFileName))
            {
                $szFilePath = __APP_PATH_TNT_XMLS__."/Request/".$kLabel->szRequestXmlFileName;
                if(file_exists($szFilePath))
                {
                    $szMessage .= "<br><br> TNT API Input XML: <br> ".__BASE_URL_TNT__."/xmls/Request/".$kLabel->szRequestXmlFileName;
                }  
            }
        } 
         * 
         */
        $div_id = 'contactPopup';
        echo success_message_popup_admin($szTitle,$szMessage,$div_id,false,false,true);
        die; 
    }
}
else if($mode=='SPLIT_PDF_LABEL_FILES')
{
    ini_set ('max_execution_time',360000);
    ini_set('pcre.backtrack_limit', 10000000);
    
    $szLabelPdfFileName =  sanitize_all_html_input(trim($_POST['file_name']));
    $szTrackingNumber =  sanitize_all_html_input(trim($_POST['tracking_code']));
    
    $kLabel = new cLabels(); 
    $splitedPdfLabelAry = array();
    //$splitedPdfLabelAry = $kLabel->downloadSplitedPdfLabelFrom($szLabelPdfFileName,$szTrackingNumber);
    
    if(!empty($splitedPdfLabelAry))
    {
        echo "SUCCESS||||"; 
        echo $splitedPdfLabelAry['szPdfFileNameJson'];
        die;
    }
    else
    {
        $iSuccessFlag = false;
        if(!empty($szLabelPdfFileName))
        {
            /*
             * $szPdfFilePath: This is temporary directory where pdf file is currency stored
             * $szLabelPdfFilePath: Final directory where all pdf labels shits
             */
            $szPdfFilePath = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/".$szLabelPdfFileName; 
            $szLabelPdfFilePath = __UPLOAD_COURIER_LABEL_PDF__."/".$szLabelPdfFileName; 
            
            if(file_exists($szPdfFilePath))
            { 
                /*
                * If file is available at temporarary directory then we move that file Temporary directory to final directory
                */
                if(file_exists($szLabelPdfFilePath))
                {
                    @unlink($szLabelPdfFilePath);
                }
                @copy($szPdfFilePath, $szLabelPdfFilePath);
                $iSuccessFlag = true;
                
                $jsonAry = array();
                $jsonAry[0]['name'] = $szLabelPdfFileName;
                $szPdfLabelJsonString = json_encode($jsonAry,true);
            } 
        }
        
        if($iSuccessFlag)
        {
            $div_id = 'contactPopup';
            $szTitle = "Notification";
            $szMessage = "We have successfully created pdf label but had some trouble in slited this in pages.";
            echo "SUCCESS_NOTIFICATION||||".$szPdfLabelJsonString;  
            echo success_message_popup_admin($szTitle,$szMessage,$div_id,false,false,true);
            die;  
        }
        else
        {
            $div_id = 'contactPopup';
            $szTitle = "Notification";
            $szMessage = "We have successfully downloaded labels from TNT API but could not split labels for this shipment automatically. Please download label from following url and then try uploading manually.<br><br>".$szTNTPdfLabelLink;
            $szTNTPdfLabelLink = __BASE_URL_TNT__."/labels/".$szLabelPdfFileName;
            echo "ERROR||||"; 
            echo success_message_popup_admin($szTitle,$szMessage,$div_id,false,false,true);
            die;
            die;
        } 
    }
} 
else if($mode=='SHOW_EMAIL_BODY_SUBJECT_OFF_LINE')
{
    $idForwarder=(int)$_POST['idForwarder'];
    $idBooking=(int)$_POST['idBooking'];
    
    if((int)$idForwarder>0)
    {
        $kConfig =new cConfig();
        $kForwarder = new cForwarder();
        $kForwarder->load($idForwarder);
        $idCountryForwarder=$kForwarder->idCountry;
        $kConfig = new cConfig();
        $kConfig->loadCountry($idCountryForwarder);
        $iDefaultLanguage=$kConfig->iDefaultLanguage;
        //echo $kConfig->iDefaultLanguage."iDefaultLanguage";
        $emailMessageAry=createOffEmailData($idBooking,$kConfig->iDefaultLanguage);

        $breaksAry = array("<br />","<br>","<br/>");  
        $szEmailBody = $emailMessageAry['szEmailMessage']; 

        //$szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
        $szEmailSubject = $emailMessageAry['szEmailSubject'];
        
        echo "SUCCESS||||";
        echo $szEmailSubject;
        echo '||||';
        echo $szEmailBody;
        echo "||||";
        echo $iDefaultLanguage;
        die();
    }
}
else if($mode=='SHOW_EMAIL_BODY_SUBJECT_OFF_LINE_BY_LANGUAGE')
{
    $idLanguage=(int)$_POST['idLanguage'];
    $idBooking=(int)$_POST['idBooking'];
    
    
    $emailMessageAry=createOffEmailData($idBooking,$idLanguage);

    $breaksAry = array("<br />","<br>","<br/>");  
    $szEmailBody = $emailMessageAry['szEmailMessage']; 

    //$szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
    $szEmailSubject = $emailMessageAry['szEmailSubject'];

    echo "SUCCESS||||";
    echo $szEmailSubject;
    echo '||||';
    echo $szEmailBody;
    die(); 
}
else if($mode=='SHOW_MORE_CUSTOMER_LOGS_ROWS')
{
    $iPageNumber = sanitize_all_html_input(trim($_REQUEST['page']));
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id'])); 
    $iHistorical = sanitize_all_html_input(trim($_REQUEST['iHistorical'])); 
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
     
    if($idBookingFile>0)
    {
        $kBooking->loadFile($idBookingFile); 
        echo "SUCCESS||||";
        if($iHistorical==1)
        {
            if($szFromPage=='PENDING_TRAY_LOG')
            {
                echo display_historical_data_for_file_logs($kBooking,$iPageNumber);
            }
            else
            {
                echo display_historical_data_for_customer_logs($kBooking,$iPageNumber);
            } 
        }
        else
        {
            if($szFromPage=='PENDING_TRAY_LOG')
            {
                echo display_file_logs_details($kBooking,$iPageNumber);
            }
            else
            {
                echo display_customer_logs_details($kBooking,$iPageNumber);
            }
        }
    }
    die;
}
else if($mode=='DISPLAY_CRM_EMAIL_DETAILS')
{
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id'])); 
    $idCrmEmailLogs = sanitize_all_html_input(trim($_REQUEST['email_id'])); 
    
    $kSendEmail = new cSendEmail();
    $logEmailsArys = array();
    $logEmailsArys = $kSendEmail->getEmailLogDetails($idCrmEmailLogs);
    echo "SUCCESS||||";
    echo display_crm_email_details($logEmailsArys,$idBookingFile,'PENDING_TRAY_CUSTOMER_LOG');
    die; 
}

?>