<?php
define('PAGE_PERMISSION','__INSURANCE_CONFIRMED_BOOKING__');
ob_start();
session_start();
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$szMetaTitle='Transporteca | New Insured Bookings';
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
$t_base="management/insurance/";
$t_base_bulk="BulkUpload/";
validateManagement(); 
$kInsurance = new cInsurance();
$kBooking = new cBooking();

$newInsuredBookingAry = array();
$newInsuredBookingAry = $kBooking->getAllNewBookingWithInsurance(__BOOKING_INSURANCE_STATUS_CONFIRMED__); 

?> 
<div id="hsbody-2">
<div id="insurance_rate_popup" style="display:none"></div> 
    <?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
    <div class="hsbody-2-right">
        <div id="insured_booking_list_container">
            <?php
                echo display_sent_insured_booking_listing($newInsuredBookingAry,__BOOKING_INSURANCE_STATUS_CONFIRMED__);
            ?>
        </div>
    </div>
    <input type="hidden" id="szBookingStatusFlag" name="szBookingStatusFlag" value="InsuranceConfirmed">
</div>
	
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>