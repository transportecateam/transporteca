<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base="management/AllProfiles/";
$t_base_error="management/Error/";
$flag=sanitize_all_html_input($_REQUEST['flag']);
$kForwarderContacts		=	new	cForwarderContact;
$kBooking   = new cBooking();
$kForwarder	= new cForwarder();
$kWHSSearch	= new cWHSSearch();
$kExportImport = new cExport_Import();
if($idAdmin>0)
{
	SWITCH($flag)
	{
		CASE 'cust':
		{
                    $searchAry = array();
                    if($_REQUEST['sortby']!="")
                    { 
			$sortby = sanitize_all_html_input($_REQUEST['sortby']);
			$sortOrder = sanitize_all_html_input($_REQUEST['sort']); 
                        
                        $searchAry['szSortBy'] = $sortby;
                        $searchAry['szSortOrder'] = $sortOrder;
                    }  
                    $iPageNumber = sanitize_all_html_input($_REQUEST['page']);
                    if($iPageNumber<=0)
                    {
                        $iPageNumber = 1;
                    }
                    $searchAry['iPage'] = $iPageNumber;
                    
                    echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Customer');</script>";
                    echo adminProfileDetailsManagement($t_base,$searchAry,true);
                    BREAK;
		}
                CASE 'CREATE_NEW_ADMIN_ROLE':
		{                                       
                    echo addNewAdminRoleType($t_base);
                    BREAK;
                }
                CASE 'CHECK_TO_DELETE_ROLE_TYPE':
		{                          
                    $idRole=(int)$_POST['idRole'];
                    
                    
                        echo "ERROR||||";
                        break;
                    
                }
                CASE 'DELETE_ROLE_TYPE':
                {
                    $idRole=(int)$_POST['idRole'];
                    if($kAdmin->checkRoleTypeAssign($idRole))
                    {
                         echo "SUCCESS||||";  
                         echo '<div id="popup-bg"></div><div id="popup-container"><div class="popup" style="width:20%;"><h5>Users are assigned to this profile, so it cannot be deleted.</h5><div class="oh"></div>'
                         . '<p align="center"><a class="button1" onclick="cancelEditMAnageVar(0)" href="javascript:void(0);">
                                <span>Ok</span>
                </a>        </p></div></div>';
                         break;
                    }else{
                    
                    $kAdmin->deleteRoleType($idRole);
                    echo "ERROR||||";
                    echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Assign Role Permission');</script>";
                    echo getRoleListing($t_base);
                    BREAK;
                    }
                }
                CASE 'ADD_NEW_ROLE_SUBMIT':
                {
                    if($kAdmin->addNewAdminRole($_POST))
                    {
                        echo "SUCCESS||||";    
                        echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Assign Role Permission');</script>";
                        echo getRoleListing($t_base);
                        BREAK;
                
                    }
                    else
                    {
                        echo "ERROR||||";
                        echo addNewAdminRoleType($t_base);
                        BREAK;
                    }
                }
                CASE 'OPEN_CHANGE_PASSWORD_POPUP':
                {
                    changePasswordHtml($kAdmin);
                    break;
                }
                CASE 'CHANGE_PASSWORD_SUBMIT':
                {
                    if($kAdmin->updateAdminPassword_info($_POST['editUserInfoArr'],$_SESSION['admin_id']))
                    {
                        $successPasswordUpdatedFlag=true;
                        changePasswordHtml($kAdmin,true);
                    }
                    else
                    {
                        changePasswordHtml($kAdmin);
                    }
                    break;
                }
                CASE 'RolePermission':
		{                                       
                    echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Assign Role Permission');</script>";
                    echo getRoleListing($t_base);
                    BREAK;
                }
                CASE 'ADD_UPDATE_ROLE_PERMISSION':
		{                                       
                    $idRole=(int)$_POST['idRole'];
                    $kAdmin->insertUpdatedRolePermission($idRole,$_POST['permissionArr']);
                    BREAK;
                }
                CASE 'EDIT_ROLE_PERMISSION':
		{                                       
                    $szRoleType=trim($_POST['szRoleType']);
                    $idRole=(int)$_POST['idRole'];
                    echo getRolePermissionListing($t_base,$idRole,$szRoleType);
                    BREAK;
                }
		CASE 'forwarderComp':
		{	
                    $iPageNumber = sanitize_all_html_input($_REQUEST['page']);
                    if($iPageNumber<=0)
                    {
                        $iPageNumber = 1;
                    }
                    $searchAry['iPage'] = $iPageNumber;
                
                    echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Freight Forwarders');</script>";
                    echo adminForwarderDetails($t_base,$searchAry,true);
                    BREAK;
		}
		CASE 'forwarderProife':
		{	
                    if(isset($_POST['mode']))
                    {	
                        $sort = (string)sanitize_all_html_input($_REQUEST['sort']);
                        $mode = (int)sanitize_all_html_input($_REQUEST['mode']);
                        $allForwarderContacts = $kAdmin->getAllForwarderContacts($idAdmin,$mode,$sort);
                        echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Forwarder Profiles');</script>";
                        adminForwarderProfileDetails($t_base,$allForwarderContacts);
                    }
                    else
                    {
                        $allForwarderContacts = $kAdmin->getAllForwarderContacts($idAdmin);
                        echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Forwarder Profiles');</script>";
                        adminForwarderProfileDetails($t_base,$allForwarderContacts);
                    }
                    BREAK;
		}
		CASE 'Transporteca':
		{
                    $transportecaManagement=$kAdmin->viewManagementDetails();
                    echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Transporteca');</script>";
                    adminDetails($t_base,$transportecaManagement,$idAdmin);
                    BREAK;
		}
                CASE 'Investors':
		{
                    $transportecaManagement=$kAdmin->viewInvestorsDetails();
                    echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Investors');</script>";
                    transportecaInvestorsDetails($t_base,$transportecaManagement,$idAdmin);
                    BREAK;
		}
		CASE 'CONFIRM_TRANSPORTECA_STATUS':
		{
                    $id=(int)sanitize_all_html_input($_POST['id']);
                    $status=(int)sanitize_all_html_input($_POST['status']);
                    if($id>0)
                    {	
                        changeAdminProfileStatus($id,$t_base,$status);
                    }
                    BREAK;
		}
		CASE 'CHANGE_TRANSPORTECA_STATUS':
		{
                    $id=(int)sanitize_all_html_input($_POST['id']);
                    if($id>0)
                    {	
                        $status=(int)sanitize_all_html_input($_POST['status']);
                        $kAdmin->changeAdminStatus($id,$status);
                        $transportecaManagement=$kAdmin->viewManagementDetails();
                        adminDetails($t_base,$transportecaManagement,$idAdmin);
                    }
                    BREAK;
		}
		CASE 'CREATE_NEW_ADMIN_PROFILE':
		{
                    createNewProfileAdmin($idAdmin,$t_base);
                    BREAK;	
		}
                CASE 'CREATE_NEW_INVESTOR_PROFILE':
		{
                    createNewInvestorProfile($idAdmin,$t_base);
                    BREAK;	
		}
		CASE 'CREATE_NEW_PROFILE_ADMIN_CONFIRM':
		{	
                    $ProfileStatus=$kAdmin->createNewAdminProfile($_POST['createProfile'],$idAdmin);
                    if($ProfileStatus==false)
                    {	
                        echo "ERROR|||||";
                        if(!empty($kAdmin->arErrorMessages))
                        {
                            ?>
                            <div id="regError" class="errorBox" style="display:block;">
                            <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
                            <div id="regErrorList">
                            <ul>
                            <?php
                                  foreach($kAdmin->arErrorMessages as $key=>$values)
                                  {
                                  ?><li><?=$values?></li>
                                  <?php 
                                  }
                            ?>
                            </ul>
                            </div>
                            </div>
                            <?php
                            }
                    }
                    if($ProfileStatus==true)
                    {
                        echo "SUCCESS|||||";
                        $transportecaManagement=$kAdmin->viewManagementDetails(0);
                        adminDetails($t_base,$transportecaManagement,$idAdmin);
                    }
                    BREAK;
		}
                CASE 'CREATE_NEW_INVESTORS_PROFILE_CONFIRM':
                {
                    $ProfileStatus=$kAdmin->createNewInvestorProfile($_POST['createProfile'],$idAdmin);
                    if($ProfileStatus==false)
                    {	
                        echo "ERROR|||||";
                        if(!empty($kAdmin->arErrorMessages))
                        {
                            ?>
                            <div id="regError" class="errorBox" style="display:block;">
                                <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
                                <div id="regErrorList">
                                    <ul>
                                        <?php foreach($kAdmin->arErrorMessages as $key=>$values){ ?>
                                            <li><?=$values?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    if($ProfileStatus==true)
                    {
                        echo "SUCCESS|||||";
                        $transportecaManagement=$kAdmin->viewInvestorsDetails();
                        transportecaInvestorsDetails($t_base,$transportecaManagement,$idAdmin);
                    }
                    BREAK;
                }
                CASE 'CONFIRM_TRANSPORTECA_INVESTOR_STATUS':
		{
                    $id=(int)sanitize_all_html_input($_POST[id]);
                    if($id>0)
                    {	
                        changeInvestorProfileStatus($id,$t_base);
                    }
                    BREAK;
		}
		CASE 'CHANGE_TRANSPORTECA_INVESTOR_STATUS':
		{
                    $id=(int)sanitize_all_html_input($_POST[id]);
                    if($id>0)
                    {	
                        $status=(int)sanitize_all_html_input($_POST[status]);
                        $kAdmin->changeInvestorStatus($id,$status);
                        $transportecaManagement=$kAdmin->viewInvestorsDetails();
                        transportecaInvestorsDetails($t_base,$transportecaManagement,$idAdmin);
                    }
                    BREAK;
		}
		CASE 'EDIT_TRANSPORTECA_ADMIN_DETAILS':
		{
                    $id = (int)$_POST['id']; 
                    $transportecaManagement=$kAdmin->viewManagementDetails($id);
                    editProfileAdmin($transportecaManagement[0],$t_base);
                    BREAK;	
		}
		CASE 'EDIT_TRANSPORTECA_ADMIN_DETAILS_CONFIRM':
		{	
                    $ProfileStatus=$kAdmin->editAdminProfile($_POST['createProfile'],$idAdmin);
                    if($ProfileStatus==false)
                    {	
                        echo "ERROR|||||";
                        if(!empty($kAdmin->arErrorMessages))
                        {
                            ?>
                            <div id="regError" class="errorBox" style="display:block;">
                            <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
                            <div id="regErrorList">
                            <ul>
                            <?php  foreach($kAdmin->arErrorMessages as $key=>$values){ ?>
                                <li><?=$values?></li>
                            <?php } ?>
                            </ul>
                            </div>
                            </div>
                            <?php
                        }
                    }
                    if($ProfileStatus==true)
                    {
                        echo "SUCCESS|||||";
                        $transportecaManagement=$kAdmin->viewManagementDetails(0);
                        adminDetails($t_base,$transportecaManagement,$idAdmin);
                    }                    
                    BREAK;
		}
                CASE 'EDIT_TRANSPORTECA_INVESTOR_DETAILS':
		{
                    $id = (int)$_POST['id']; 
                    $transportecaManagement=$kAdmin->viewInvestorsDetails($id);
                    editInvestorProfile($transportecaManagement[0],$t_base);
                    BREAK;	
		}
		CASE 'EDIT_TRANSPORTECA_INVESTOR_DETAILS_CONFIRM':
		{	
                    $ProfileStatus=$kAdmin->editInvestorProfile($_POST['createProfile'],$idAdmin);
                    if($ProfileStatus==false)
                    {	
                        echo "ERROR|||||";
                        if(!empty($kAdmin->arErrorMessages))
                        {
                            ?>
                            <div id="regError" class="errorBox" style="display:block;">
                            <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
                            <div id="regErrorList">
                            <ul>
                            <?php
                                  foreach($kAdmin->arErrorMessages as $key=>$values)
                                  {
                                  ?><li><?=$values?></li>
                                  <?php 
                                  }
                            ?>
                            </ul>
                            </div>
                            </div>
                            <?php
                        }
                    }
                    if($ProfileStatus==true)
                    {
                        echo "SUCCESS|||||";
                        $transportecaManagement=$kAdmin->viewInvestorsDetails();
                        transportecaInvestorsDetails($t_base,$transportecaManagement,$idAdmin);
                    } 
                    BREAK;
		}
		CASE 'ADD_NEW_FORWARDER':
		{
			addNewForwarder($t_base);
			BREAK;
		}
		CASE 'ADD_NEW_FORWARDER_CONFIRMATION':
		{
                    if(isset($_POST['createArr']))
                    {
                        $kForwarder = new cForwarder();
                        $newForwarderConfirmation  = $kForwarder->createNewForwarder($_POST['createArr']);
                        if($newForwarderConfirmation==false)
                        {	
                            echo "ERROR|||||";
                            if(!empty($kForwarder->arErrorMessages))
                            {
                                ?>
                                <div id="regError" class="errorBox" style="display:block;">
                                    <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
                                    <div id="regErrorList">
                                        <ul>
                                            <?php foreach($kForwarder->arErrorMessages as $key=>$values)  {?>
                                                <li><?=$values?></li>
                                                <script type="text/javascript">
                                                    if($('#'+'<?php echo $key; ?>').length)
                                                    {
                                                        $('#'+'<?php echo $key; ?>').addClass('red_border'); 
                                                    }  
                                                </script>
                                            <?php  } ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        else
                        {
                            echo "SUCCESS|||||";
                            show_success_forwarder_creation_popup($t_base,$_POST['createArr']['szEmail']);
                            //$kForwarder= new cForwarder();
                            //$kForwarderDetails=$kForwarder->getAllForwarderDetailsAdmin();
                            //adminForwarderDetails($t_base,$kForwarderDetails);
                        }	
                    }
                    BREAK;
		}
		CASE 'DELETE_FORWARDER_PROFILE':
				$idForwarder = (int)$_POST['id'];
				showForwarderDeleteConfirmation($idForwarder);
				BREAK;
		CASE 'DELETE_FORWARDER_PROFILE_CONFIRM':
			$id = (int)$_POST['id'];
			$kForwarder = new cForwarder();
			if($kForwarder->deleteForwarder($id))
			{
				$kForwarderDetails=$kForwarder->getAllForwarderDetailsAdmin();
				echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Freight Forwarders');</script>";
				adminForwarderDetails($t_base,$kForwarderDetails);
			}
			BREAK;
		CASE 'INACTIVATE_FORWARDER_PROFILE_CONFIRM':
			$id = (int)$_POST['id'];
			$kForwarder = new cForwarder();
			if($kForwarder->toggleForwarderStatus($id,0))
			{
				$kForwarderDetails=$kForwarder->getAllForwarderDetailsAdmin();
				echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Freight Forwarders');</script>";
				adminForwarderDetails($t_base,$kForwarderDetails);
			}
			BREAK;
		CASE 'ACTIVATE_FORWARDER_PROFILE_CONFIRM':
			$id = (int)$_POST['id'];
			$kForwarder = new cForwarder();
			if($kForwarder->toggleForwarderStatus($id,1))
			{
				$kForwarderDetails=$kForwarder->getAllForwarderDetailsAdmin();
				echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Freight Forwarders');</script>";
				adminForwarderDetails($t_base,$kForwarderDetails);
			}
			BREAK;
		CASE 'EDIT_FORWARDER_PROFILE':
		CASE 'user_access':
		{
			$id = (int)$_POST['id'];
			$idRoleAry[0]=1 ;
			$forwarderContactAry = $kForwarderContacts->getAllForwardersContact($id,$idRoleAry);
			forwarderCompaniesEdit($id,'user_access');
			showCompanyUserDetails($forwarderContactAry,$kForwarderContacts,$id);
			BREAK;
		}
		CASE 'ADD_USER':
		{
		$t_base = "ForwardersCompany/UserAccess/";
		$idForwarderRole = (int)$_POST['idForwarderRole'];
		$idForwarder = (int)$_POST['idForwarder'];
		$kForwarderContacts->loadContactRole($idForwarderRole);
		$operation_mode = "ADD_USER";
		$szForwardrRole = $kForwarderContact->szRoleName ;
		$success_sent = false;
		$forwarderCompanyAry = $_POST['forwarderCompanyAry'];
		if(!empty($forwarderCompanyAry))
		{
			if($kForwarderContacts->addForwarderEmail($forwarderCompanyAry))
			{
				$success_sent = true;
			}
		}	
		?>
		<div id="popup-bg"></div>
		<div id="popup-container">	
		<div class="compare-popup popup">
			<h5><strong><?=t($t_base.'fields/add_new');?> <?=$szForwardrRole?> <?=t($t_base.'fields/profile');?></strong></h5>
			<?php
				if($success_sent)
				{
					?>
					<p><?=t($t_base.'titles/add_new_profile_success_message');?></p>
					<br>
					<p align="center">
						<a href="javascript:void(0)" class="button2" onclick="redirect_url('<?=__FORWARDER_COMPANY_PAGE_URL__?>')"><span><?=t($t_base.'fields/close');?></span></a>				
					</p>
					<?
					die;
				}
			?>
				<?php
				if(!empty($kForwarderContacts->arErrorMessages))
				{
				?>
				<div id="regError" class="errorBox ">
				<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
				<div id="regErrorList">
				<ul>
				<?php
				      foreach($kForwarderContacts->arErrorMessages as $key=>$values)
				      {
				      ?><li><?=$values?></li>
				      <?php 
				      }
				?>
				</ul>
				</div>
				</div>
				<?
				}
				?>
				<form action="javascript:void(0);" name="addForwarderContactInfo" id="addForwarderContactInfo" method="post">
					<p><?=t($t_base.'titles/add_new_profile_popup_message');?></p>
					<br>
					<div class="oh">
						<p class="fl-30"><?=t($t_base.'fields/email');?> </p>
						<p class="fl-70"><input type="text" size="30" style="width:238px;" name="forwarderCompanyAry[szEmail]" onkeyup="on_enter_key_press(event,'addNewForwaredrDetails')" id="szEmail" value="<?=(isset($forwarderCompanyAry['szEmail']))?$forwarderCompanyAry['szEmail']:'';?>"/></p>
					</div>
					<p align="center">
						<a href="javascript:void(0)" class="button1" onclick="javscript:addNewForwaredrDetails();"><span><?=t($t_base.'fields/add');?></span></a>&nbsp;
						<a href="javascript:void(0)" class="button2" onclick="showHide('ajaxLogin')"><span><?=t($t_base.'fields/cancel');?></span></a>
						<input type="hidden" name="flag" value="<?=$operation_mode?>">
						<input type="hidden" name="forwarderCompanyAry[idForwarderRole]" value="<?=$idForwarderRole?>">
						<input type="hidden" name="forwarderCompanyAry[idForwarder]" value="<?=$idForwarder?>">
					</p>
				</form>
			</div>
		</div>
		<?php			
                
		BREAK;	
		}
		CASE 'preferences':
		{	
			if(isset($_POST['id']))
			{	
				$id	=	(int)sanitize_all_html_input($_POST['id']);
				$preferencesArr=$kForwarderContacts->showPreferencesForwarder($id);	
				forwarderCompaniesEdit($id,'preferences');
				forwarderCompanyPrederences($t_base,$preferencesArr,$id);
			}
			BREAK;
		}
		CASE 'settings_pricing':
		{
                    $id	=	(int)sanitize_all_html_input($_POST['id']);
                    forwarderCompaniesEdit($id,'settings_pricing');
                    $forwarderCP2=$kForwarderContacts->getSystemPanelforwarderAdmin($id);
                    viewSettingAndPricing($t_base,$forwarderCP2,$id);
                    BREAK;
		}
		CASE 'forwarderHistory':
		{
			if(isset($_POST['id']))
			{
				$idForwarder = (int)sanitize_all_html_input($_POST['id']);
				adminForwarderAccessHistory($t_base,$idForwarder);
			}
			BREAK;
		}
		CASE 'EDIT_FORWARDER_PREFERENCES':
		{	
			if(isset($_POST['id']))
			{
				$idForwarderContact		=	(int)sanitize_all_html_input($_POST['id']);
				$eachForwarderDetails	=	$kForwarderContacts->showPreferencesEachForwarder($idForwarderContact);
				viewForwarderEachPreferences($t_base,$eachForwarderDetails);
				//print_r($eachForwarderDetails);
			}
			BREAK;
		}
		CASE 'UPDATE_PREFERENCES_ARRAY':
		{
			if($_POST['updatePreferenceArr']!= array())
			{	
				$forwarderContactUpdate	=	$kForwarderContacts->updatePreferencesEachForwarder($_POST['updatePreferenceArr']);
			
				if($forwarderContactUpdate==false)
				{	
					echo "ERROR|||||";
					if(!empty($kForwarderContacts->arErrorMessages))
					{
						?>
						<div id="regError" class="errorBox" style="display:block;">
						<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
						<div id="regErrorList">
						<ul>
						<?php
						      foreach($kForwarderContacts->arErrorMessages as $key=>$values)
						      {
						      ?><li><?=$values?></li>
						      <?php 
						      }
						?>
						</ul>
						</div>
						</div>
						<?php
					}
				}
				else
				{
					echo "SUCCESS|||||";
					$id=$kForwarderContacts->idForwarder;
					$preferencesArr=$kForwarderContacts->showPreferencesForwarder($id);	
					forwarderCompaniesEdit($id,'preferences');
					forwarderCompanyPrederences($t_base,$preferencesArr,$id);
				}
			}
			BREAK;
		}
		CASE 'ADD_PREFERENCES_ARRAY':
		{
			if($_POST['addPreferenceArr']!= array())
			{	
				$forwarderContactUpdate	=	$kForwarderContacts->addPreferencesEachForwarder($_POST['addPreferenceArr']);
			
				if($forwarderContactUpdate==false)
				{	
					echo "ERROR|||||";
					if(!empty($kForwarderContacts->arErrorMessages))
					{
						?>
						<div id="regError" class="errorBox" style="display:block;">
						<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
						<div id="regErrorList">
						<ul>
						<?php
						      foreach($kForwarderContacts->arErrorMessages as $key=>$values)
						      {
						      ?><li><?=$values?></li>
						      <?php 
						      }
						?>
						</ul>
						</div>
						</div>
						<?php
					}
				}
				else
				{
					echo "SUCCESS|||||";
					$id=$kForwarderContacts->idForwarder;
					$preferencesArr=$kForwarderContacts->showPreferencesForwarder($id);	
					forwarderCompaniesEdit($id,'preferences');
					forwarderCompanyPrederences($t_base,$preferencesArr,$id);
				}
			}
			BREAK;
		}
		CASE 'REMOVE_FORWARDER_PREFERENCES':
		{
			$email 	=	trim(sanitize_all_html_input($_POST['email']));
			$id		=	(int)sanitize_all_html_input($_POST['id']);
			echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/remove_email').'</b></h5>
							<p>'.t($t_base.'title/are_you_sure').' '.$email.'?</p>
							<a class="button1" onclick="removeForwarderPreferences(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
						</div>
					</div>';
			BREAK;
		}
		CASE 'REMOVE_FORWARDER_PREFERNCES_CONFIRM':
		{	
			$idPreferences	=	(int)sanitize_all_html_input($_POST['id']);	
				
			$deactivate=$kForwarderContacts->deactiveForwarderPreferences($idPreferences);	
			if($deactivate==true)
			{
				echo "SUCCESS|||||";
					$id=$kForwarderContacts->findForwarderPreferencesId($idPreferences);
					$id= $id['idForwarder'];
					$preferencesArr=$kForwarderContacts->showPreferencesForwarder($id);	
					forwarderCompaniesEdit($id,'preferences');
					forwarderCompanyPrederences($t_base,$preferencesArr,$id);
			}
		BREAK;		
		}
		CASE 'companyInformation':
		{
			if((int)$_POST['id']>0)
			{
				$idForwarder = (int)sanitize_all_html_input($_POST['id']);
				forwarderCompaniesEdit($idForwarder,'companyInformation');	
				ForwarderCompanyInformation($idForwarder);
			}
			BREAK;	
		}
		CASE 'SAVE_FORWARDER_COMPANY_DETAILS':
		{
			if(!empty($_POST))
			{	$kForwarder = new cForwarder();
				if(isset($_POST['updateForwarderComapnyAry']['szForwarderLogo']))
				{
				$_POST['updateRegistComapnyAry']['szForwarderLogo']=$_POST['updateForwarderComapnyAry']['szForwarderLogo'];
				}
				//print_R($_POST['updateRegistComapnyAry']);
				$forwarder = $kForwarder->saveForwarderComanyDetails($_POST['updateRegistComapnyAry']);
				if($forwarder==false)
				{	
					echo "ERROR|||||";
					if(!empty($kForwarder->arErrorMessages))
					{
						?>
						<div id="regError" class="errorBox" style="display:block;">
						<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
						<div id="regErrorList">
						<ul>
						<?php
						      foreach($kForwarder->arErrorMessages as $key=>$values)
						      {
						      ?><li><?=$values?></li>
						      <?php 
						      }
						?>
						</ul>
						</div>
						</div>
						<?php
					}
				}
				else if($forwarder==true)
				{	echo 'SUCCESS|||||';die();
					//$idForwarder = (int)sanitize_all_html_input($_POST['updateRegistComapnyAry']['idForwarder']);
					//forwarderCompaniesEdit($idForwarder,'companyInformation');	
					//ForwarderCompanyInformation($idForwarder);
				}
			}
			BREAK;
		}
		CASE 'bankDetails':
		{
			$idForwarder	= (int)sanitize_all_html_input($_POST['id']);
			//$kForwarder		= new cForwarder();
			//$kForwarder->getForwarderBankDetails($idForwarder);
			echo displayForwarderBankDetailsAdmin("ForwardersCompany/BankDetails/",$idForwarder);
			BREAK;
		}
		CASE 'UPDATE_FORWARDER_BANK_DETAILS':
		{	
			if(!empty($_REQUEST['updateRegistBankAry']))
			{	
				$updateRegistBankAry = array();
				$updateRegistBankAry = $_POST['updateRegistBankAry'];
				$operation_mode = sanitize_all_html_input(trim($_REQUEST['updateRegistBankAry']['szMode']));
				$idForwarder = sanitize_all_html_input(trim($_REQUEST['updateRegistBankAry']['idForwarder']));
				
				if(!empty($updateRegistBankAry))
				{	
					$kForwarder = new cForwarder();
					if($kForwarder->updateForwarderBankDetails($updateRegistBankAry))
					{
						echo "SUCCESS";
						die;
					}
					else
					{	
						echo "ERROR|||||";
						if(!empty($kForwarder->arErrorMessages))
						{
							?>
							<div id="regError" class="errorBox" style="display:block;">
							<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
							<div id="regErrorList">
							<ul>
							<?php
							      foreach($kForwarder->arErrorMessages as $key=>$values)
							      {
							      ?><li><?=$values?></li>
							      <?php 
							      }
							?>
							</ul>
							</div>
							</div>
							<?php
						}
					}
				}
			}
			BREAK;	
		}
		CASE 'UPDATE_FORWARDER_PREFERENCES':
		{
			$idForwarder = (int)sanitize_all_html_input(trim($_POST['idForwarder']));
			if(!empty($_POST['updatePrefControlArr']) && !empty($idForwarder))
			{	
				$kForwarderContact = new cForwarderContact();
				$updateController=$kForwarderContact->updatePreferencesCotrolPanel($_POST['updatePrefControlArr'],$idForwarder);
				if($updateController)
				{
					echo "SUCCESS|||||";
					die();
				}
				if($updateController == false)
				{ 
					echo "ERROR|||||";
					if(!empty($kForwarderContact->arErrorMessages))
					{
						?>
						<div id="regError" class="errorBox" style="display:block;">
						<div class="header"><?=t($t_base.'fields/please_following');?></div>
						<div id="regErrorList">
						<ul>
						<?php
						      foreach($kForwarderContact->arErrorMessages as $key=>$values)
						      {
						      ?><li><?=$values?></li>
						      <?php 
						      }
						?>
						</ul>
						</div>
						</div>
						<?php
					}
				}
			}
			BREAK;
		}
		CASE 'Inactive':
		{
			$id		=	(int)sanitize_all_html_input($_POST['id']);
			$mode	=	trim(sanitize_all_html_input($_POST['mode']));
			echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h4><b>'.$mode.' '.t($t_base.'fields/forwarder').'</b></h4>
							<br/>
							<p>'.t($t_base.'title/are_u_sure_that').' '. $mode .' '. t($t_base.'title/this_forwarder').' <br/>';
			if($mode=='Inactivate')
			{
				echo ' <br />'.t($t_base.'title/the_forwarder_will_not');
			}
			echo '	<br/><br/></p>
							<p align="center">
							<a class="button1" onclick="removeForwarder(\''.$id.'\')"><span>'.t($t_base.'fields/yes').'</span></a>
							<a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/no').'</span></a>
							</p>
							</div>
					</div>';
			BREAK;
		}
		CASE 'INACTIVE_FORWARDER_PROFILER':
		{
			$id		=	(int)sanitize_all_html_input($_POST['id']);
			$kForwarderContact = new cForwarderContact();
			$kForwarderContact->deleteForwarderProfile($id);
			BREAK;
		}		
	}	
}
else
{
	header("location:".__MANAGEMENT_URL__);
}
	?>