<?php 
/**
  *  Admin---> System --> Insurance
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");

require_once( __APP_PATH_CLASSES__.'/insurance.class.php' );
require_once( __APP_PATH_CLASSES__.'/warehouseSearch.class.php' );

$t_base="management/insurance/";
$t_base_error="management/Error/";
validateManagement();
 
$kBooking = new cBooking();
$kInsurance = new cInsurance();
$kConfig = new cConfig();
$kForwarder = new cForwarder();
$kRegisterShipCon = new cRegisterShipCon();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));

if($mode=='DISPLAY_MANUAL_QUOTE_FORM')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    
    echo "SUCCESS||||";
    echo display_manual_quotes_form($kRegisterShipCon,$idBooking,$szFromPage);
    die; 
} 
else if($mode=='DISPLAY_MANUAL_QUEOTS_LISTING')
{
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    if(trim($szFromPage)=='SEND_INSURED_BOOKING')
    {
        $newInsuredBookingAry = array();
        $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_SENT__);

        echo "SUCCESS||||";
        echo display_sent_booking_quotes_listing($newBookingQuotesAry); 
        die;
    }
    else
    {
        $newInsuredBookingAry = array();
        $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_NEW_BOOKING__);
        echo "SUCCESS||||";
        echo display_new_booking_quotes_listing($newBookingQuotesAry);
        die;
    }  
}
else if($mode=='DISPLAY_SEND_QUEOTS_POPUP')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
      
    echo "POPUP||||";
    echo display_send_quotes_popup($idBooking,$szFromPage);
    die;
}
else if($mode=='VERIFY_USER_EMAIL')
{
    $idUser = sanitize_all_html_input(trim($_REQUEST['user_id']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));
    
    echo "SUCCESS||||";
    echo display_verify_user_confirmation($idUser,$szFromPage,$idBookingFile);
    die;
}
else if($mode=='VERIFY_USER_EMAIL_CONFIRM')
{
    $idUser = sanitize_all_html_input(trim($_REQUEST['user_id']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id'])); 
    
    if($idUser>0)
    {
        $updateUserDetailsAry = array();  
        $updateUserDetailsAry['iConfirmed'] = 1 ; 
 
        if(!empty($updateUserDetailsAry))
        {
            $update_user_query = '';
            foreach($updateUserDetailsAry as $key=>$value)
            {
                $update_user_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        } 
        
        $update_user_query = rtrim($update_user_query,",");  
        $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idUser);
        
        //$kRegisterShipCon = new cRegisterShipCon(); 
       // $szCustomerHistoryNotes = "E-mail verified on ".date('d/m/Y H:i:s');
       // $kRegisterShipCon->updateCapsuleHistoryNotes($szCustomerHistoryNotes,$idUser);
        
        if($szFromPage=='PENDING_TASK')
        {
            echo "SUCCESS_TASK||||";
            $kBooking->loadFile($idBookingFile); 
            if($kBooking->idBookingFile>0)
            { 
                echo display_pending_tray_overview($kBooking); 
                die;
            }
        }
        else
        {
            echo "SUCCESS_CONFIRM||||";
        }
        die;
    } 
    else if(!empty($_POST['updatedTaskAry']))
    {
        $kUser = new cUser();
        if($kUser->createAndVerifyUserAccount($_POST['updatedTaskAry']))
        {
            echo "SUCCESS||||";
            echo $kUser->idNewAddedUser;
            die;
        }
    } 
}
else if($mode=='DELETE_MANUAL_QUEOTS_POPUP')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    
    $bookingIdAry = array();
    $bookingIdAry[0] = $idBooking ;
    $newInsuredBookingAry = array();
    $newBookingQuotesAry = $kBooking->getAllBookingQuotes(false,$bookingIdAry);
    $szEmail = $newBookingQuotesAry[0]['szEmail'];
    
    echo "POPUP||||";
    echo display_delete_quotes_popup($idBooking,$szEmail,$szFromPage);
    die;
} 
else if($mode=='DELETE_MANUAL_QUEOTS_CONFIRM')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page'])); 
    
    if($kRegisterShipCon->deleteManualQuotes($idBooking))
    {
        if(trim($szFromPage)=='SEND_INSURED_BOOKING')
        {
            $newInsuredBookingAry = array();
            $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_SENT__);

            echo "SUCCESS||||";
            echo display_sent_booking_quotes_listing($newBookingQuotesAry); 
            die; 
        }
        else
        {
            $newInsuredBookingAry = array();
            $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_NEW_BOOKING__);
            echo "SUCCESS||||";
            echo display_new_booking_quotes_listing($newBookingQuotesAry);
            die; 
        }  
    } 
} 
else if($mode=='DISPLAY_EXPIRE_QUEOTS_POPUP')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
      
    
    $bookingIdAry = array();
    $bookingIdAry[0] = $idBooking ;
    $newInsuredBookingAry = array();
    $newBookingQuotesAry = $kBooking->getAllBookingQuotes(false,$bookingIdAry); 
    
    echo "POPUP||||";
    echo display_expire_quotes_popup($idBooking,$newBookingQuotesAry[0]);
    die;
}  
else if($mode=='EXPIRE_MANUAL_QUEOTS_CONFIRM')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
    if($kRegisterShipCon->expireManualQuotes($idBooking))
    {
        $newInsuredBookingAry = array();
        $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_SENT__);
        
        echo "SUCCESS||||";
        echo display_sent_booking_quotes_listing($newBookingQuotesAry); 
        die;
    } 
} 
else if($mode=='COPY_MANUAL_QUEOTS')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking'])); 
    if($kRegisterShipCon->copyBookingQuote($idBooking))
    { 
        $idBooking = $kRegisterShipCon->iNewBookingID ; 
        echo "SUCCESS||||"; 
        echo display_manual_quotes_form($kRegisterShipCon,$idBooking);
        die; 
    } 
}
else if($mode=='VIEW_MANUAL_QUEOTS')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
        
    echo "SUCCESS||||"; 
    echo display_manual_quotes_form($kRegisterShipCon,$idBooking,$szFromPage,1); 
    die;  
}
else if($mode=='ADD_NEW_MANUAL_QUEOTS' && !empty($_POST['addManulQuotesAry']))
{  
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $dtTimingDate = sanitize_all_html_input(trim($_POST['addManulQuotesAry']['dtTimingDate']));
    
    $dtTimingDate = format_date($dtTimingDate);
    
    if(strtotime($dtTimingDate)>=strtotime(date('Y-m-d')))
    { 
        $idBooking = $kRegisterShipCon->addNewManualQuote($dtTimingDate);
        if($idBooking>0)
        {
            echo "SUCCESS||||";
            echo display_manual_quotes_form($kRegisterShipCon,$idBooking,$szFromPage,false,1);
            die;
        }  
    }
    else
    {
        $kRegisterShipCon->addError('dtTimingDate','Invalid Date');
        echo "ERROR||||";
        echo display_shipping_date_popup($kRegisterShipCon,$szFromPage);
        die;
    }
    die;
     
}
else if($mode=='DISAPLAY_SHIPPING_DATE_POPUP')
{ 
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));  
    echo "POPUP||||";
    echo display_shipping_date_popup($kRegisterShipCon,$szFromPage);
    die; 
}
else if($mode=='BACK_TO_MANUAL_QUEOTS_LISTING')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    $iPageNumber = sanitize_all_html_input(trim($_REQUEST['page']));
        
    if($iPageNumber<=0)
    {
        $iPageNumber = 1;
    }
    
    if(trim($szFromPage)=='EXPIRED_QUOTE_PAGE')
    { 
        echo "SUCCESS||||"; 
        echo display_expired_booking_quotes_listing(__BOOKING_QUOTES_STATUS_EXPIRED__,$iPageNumber); 
        die; 
    }
    else
    {  
        echo "SUCCESS||||"; 
        echo display_expired_booking_quotes_listing(__BOOKING_QUOTES_STATUS_WON__,$iPageNumber); 
        die; 
    } 
} 
else if(!empty($_POST['shipperConsigneeAry']))
{  
    $idBooking = sanitize_all_html_input(trim($_POST['shipperConsigneeAry']['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_POST['shipperConsigneeAry']['szFromPage']));
    
    if($kRegisterShipCon->updateManualQuotes($_POST['shipperConsigneeAry'],$idBooking))
    {
        if(trim($szFromPage)=='SEND_INSURED_BOOKING')
        {
            $newInsuredBookingAry = array();
            $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_SENT__);

            echo "SUCCESS||||";
            echo display_sent_booking_quotes_listing($newBookingQuotesAry); 
            die;
        }
        else
        {
            $newInsuredBookingAry = array();
            $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_NEW_BOOKING__);
            echo "SUCCESS||||";
            echo display_new_booking_quotes_listing($newBookingQuotesAry);
            die;
        } 
    }
    else
    {
        echo "ERROR||||";
        echo display_manual_quotes_form($kRegisterShipCon,$idBooking);
        die; 
    }
}
else if(!empty($_POST['sendQuotesAry']))
{  
    $idBooking = sanitize_all_html_input(trim($_POST['sendQuotesAry']['idBooking']));
    $szFromPage = sanitize_all_html_input(trim($_REQUEST['from_page']));
    if($kRegisterShipCon->sendManualQuotes($_POST['sendQuotesAry'],$idBooking))
    { 
        if(trim($szFromPage)=='SEND_INSURED_BOOKING')
        {
            $newInsuredBookingAry = array();
            $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_SENT__);

            echo "SUCCESS||||";
            echo display_sent_booking_quotes_listing($newBookingQuotesAry); 
            die;
        }
        else
        {
            $newInsuredBookingAry = array();
            $newBookingQuotesAry = $kBooking->getAllBookingQuotes(__BOOKING_QUOTES_STATUS_NEW_BOOKING__);
            echo "SUCCESS||||";
            echo display_new_booking_quotes_listing($newBookingQuotesAry);
            die;
        }
    }
    else
    {
        echo "POPUP||||";
        echo display_send_quotes_popup($idBooking);
        die; 
    }
}