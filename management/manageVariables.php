<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__SYSTEM_SETTINGS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Variables - Settings";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/textEditor/";
?>
 <STYLE type="text/css">
   .ui-full-fields .field-container input[type="text"] {float: right;width: auto;}
 </STYLE>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
	<div id="content" class="transporteca-settings">
		<div id="searchPage">
                    <h4><b><?=t($t_base.'fields/search_limitations')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('searchPage');"><?=t($t_base.'fields/editHeading');?></a></span></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(1);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />			
		<div id="selectSevices">
                    <h4><b><?=t($t_base.'fields/select_services')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('selectSevices');"><?=t($t_base.'fields/editHeading');?></a></span></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(2);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />	
		<div id="lclserviceDisplay">
                    <h4><b><?=t($t_base.'fields/lcl_services_displayed')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('lclserviceDisplay');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(11);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />
		<div id="courierserviceDisplay">
                    <h4><b><?=t($t_base.'fields/courier_services_displayed')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('courierserviceDisplay');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(12);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />
		<div id="myBookings">
                    <h4><b><?=t($t_base.'fields/my_bookings')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('myBookings');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(3);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />			
		<div id="bulkUpload">
                    <h4><b><?=t($t_base.'fields/bulk_upload')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('bulkUpload');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(4);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />			
		<div id="zonePricing">
                    <h4><b><?=t($t_base.'fields/lcl_pricing')?></b><span class="edit-links"> <a href="javascript:void(0)" onclick="editManagementVar('zonePricing');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(5);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />		
		<div id="general">
                    <h4><b><?=t($t_base.'fields/general')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('general');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(6);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />	
		<div id="courier_pricing">
                    <h4><b><?=t($t_base.'fields/courier_pricing')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('courier_pricing');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(13);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />
		<div id="communication">
                    <h4><b><?=t($t_base.'fields/communication')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('communication');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(7);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />	
		<!--  <div id="links_to_explain_page">
		<h4><b><?=t($t_base.'fields/links_to_explain_page')?></b> <a href="javascript:void(0)" onclick="editManagementVar('links_to_explain_page');"><?=t($t_base.'fields/editHeading');?></a></h4>
			<?
				$manageVariable=$kAdmin->manageVariables(8);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />-->	
		<div id="links_to_api_key">
                    <h4><b><?=t($t_base.'title/api_key')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('links_to_api_key');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(9);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />	
                <div id="links_to_videos">
                    <h4><b><?=t($t_base.'title/videos')?></b> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('links_to_videos');"><?=t($t_base.'fields/editHeading');?></a></SPAN></h4>
			<?php
				$manageVariable=$kAdmin->manageVariables(14);	
			 	manageVAriables($manageVariable);
			?>
		</div>
		<br />
		<!-- <div id="trusted_partner">
		<h4><b><?=t($t_base.'title/partner')?></b> <a href="javascript:void(0)" onclick="editManagementVar('trusted_partner');"><?=t($t_base.'fields/editHeading');?></a></h4>
			<?
				$manageVariable=$kAdmin->manageVariables(10);	
			 	manageVAriables($manageVariable);
			?>
		</div> -->
	</div>	
</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>