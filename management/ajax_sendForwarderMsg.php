<?php 
/**
  *  Send Message to forwarder
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");


$kAdmin = new cAdmin();
$kForwarderContact = new cForwarderContact();
$kForwarder = new cForwarder();
$t_base="management/AdminMessages/";
if($_REQUEST['sendflag']=='ConfirmFlag')
{
	$szForwarderIdArr=$_REQUEST['szForwarderIdArr'];
	//print_r($szForwarderIdArr);
	if(!empty($szForwarderIdArr))
	{
		$forwarderIdArr=explode(',',$szForwarderIdArr);
		$idBatch=$kAdmin->getMaxBatchId();
		for($k=0;$k<count($forwarderIdArr);++$k)
		{
			//echo $customerIdArr[$k]."hello";
			$message='';
			$forwarderArrId=array();
			$forwarderArrId=explode("_",$forwarderIdArr[$k]);
			$kForwarderContact->getUserDetails($forwarderArrId[0]);
			$idForwarderContact=$forwarderArrId[0];
			
			
			if($forwarderArrId[1]>1)
			{
				$idForwarderContact=$kAdmin->getActiveForwarderDetails($kForwarderContact->szEmail);
				if((int)$idForwarderContact>0)
				{
					$kForwarderContact->getUserDetails($idForwarderContact);
				}
				else
				{
					$idForwarderContact=$forwarderArrId[0];
					$kForwarderContact->getUserDetails($idForwarderContact);
				}
			}
			
			$replaceAry=array();
			$kForwarder->load($kForwarderContact->idForwarder);
			$replaceAry['szFirstName']=$kForwarderContact->szFirstName;
			$replaceAry['szLastName']=$kForwarderContact->szLastName;
			$replaceAry['szEmail']=$kForwarderContact->szEmail;
			$replaceAry['szDisplayName']=$kForwarder->szDisplayName;
			$szSubject=$_REQUEST['szSubject'];
			$szBody=nl2br(urldecode(base64_decode($_REQUEST['szMessage'])));
			
   			$message .=$szBody;
			if (count($replaceAry) > 0)
		    {
		        foreach ($replaceAry as $replace_key => $replace_value)
		        {
		            $message = str_replace(trim($replace_key), $replace_value,$message);
		        }
		    }
		    
			if (count($replaceAry) > 0)
		    {
		        foreach ($replaceAry as $replace_key => $replace_value)
		        {
		            $szSubject = str_replace(trim($replace_key),$replace_value,$szSubject);
		        }
		    }
		    
		    $kAdmin->saveAdminMessages($kForwarderContact->szEmail,$idForwarderContact,$message,$szSubject,2,$idBatch);
		}
		?>
		<script>
		$(location).attr('href','<?=__MANAGEMENT_MESSAGE_FORWARDER_URL__?>');
		</script>
		<?
		exit();
	}
}
else
{
	if($_REQUEST['showflag']=='Preview')
	{
	
	?>
	
	<div>
	<?
			 ob_start();  
    		 require_once(__APP_PATH_ROOT__.'/layout/email_header.php');
    		 ?>
    		 <p><?=nl2br(urldecode(base64_decode($_REQUEST['szMessage'])));?></p>
    		<?
    		 include(__APP_PATH_ROOT__.'/layout/email_footer.php');
		?>
		
	</div>	
	<?
	}
	else if($_REQUEST['showflag']=='HtmlVar')
	{
		getAllHtmlVariable('forwarder');
	}
	else
	{
	
		$total=0;
		$iForwarderStatusFlag=$_REQUEST['iForwarderStatusFlag'];
		$iForwarderUserStatusFlag=$_REQUEST['iForwarderUserStatusFlag'];
		$data['szSubject']=$_REQUEST['szSubject'];
		$data['szMessage']=$_REQUEST['szMessage'];
		
		$idForwarderArr='';
		if($_REQUEST['szForwarderArr']=='All')
		{
			$idForwarderArr='All';
		}
		else if($_REQUEST['szForwarderArr']!='null' && $_REQUEST['szForwarderArr']!='')
		{
			$szForwarderSelArr=$_REQUEST['szForwarderArr'];
			$idForwarderArr=implode(',',$_REQUEST['szForwarderArr']);
		}
		
		$idForwarderProfileArr='';
		if($_REQUEST['szProfileArr']=='All')
		{
			$idForwarderProfileArr='All';
		}
		else if($_REQUEST['szProfileArr']!='null' && $_REQUEST['szProfileArr']!='')
		{
			$szForwarderProfileSelArr=$_REQUEST['szProfileArr'];
			$idForwarderProfileArr=implode(',',$_REQUEST['szProfileArr']);
		}
		
		$iForwarderStatusStr='';
		if($iForwarderStatusFlag!='')
		{
			$iForwarderStatusArr=explode("_",$iForwarderStatusFlag);
			if(!empty($iForwarderStatusArr))
			{
				if(count($iForwarderStatusArr)>1)
					$iForwarderStatusStr=implode("','",$iForwarderStatusArr);
				else
					$iForwarderStatusStr=$iForwarderStatusArr[0];	
					
				$iForwarderStatusStr="'".$iForwarderStatusStr."'";
			}
		}
		
		$iForwarderUserStatusStr='';
		if($iForwarderUserStatusFlag!='')
		{
			$iForwarderUserStatusFlagArr=explode("_",$iForwarderUserStatusFlag);
			if(!empty($iForwarderUserStatusFlagArr))
			{
				if(count($iForwarderUserStatusFlagArr)>1)
					$iForwarderUserStatusStr=implode("','",$iForwarderUserStatusFlagArr);
				else
					$iForwarderUserStatusStr=$iForwarderUserStatusFlagArr[0];	
					
				$iForwarderUserStatusStr="'".$iForwarderUserStatusStr."'";
			}
		}
		if($iForwarderStatusStr!='' && $iForwarderUserStatusStr!='')
		{
			$forwarderUserIdArr=$kAdmin->getAllForwarderUserProfile($idForwarderArr,$idForwarderProfileArr,$iForwarderStatusStr,$iForwarderUserStatusStr,$_REQUEST['flag'],$data);
		}
			$total=count($forwarderUserIdArr);
	
	
	if($_REQUEST['szProfileArr']=='All')
	{	
	?>
	<script>
	$('#szProfile option').attr('selected', 'selected');
	</script>
	<? }?>
	<?
	if($_REQUEST['szForwarderArr']=='All')
	{	
	?>
	<script>
	$('#szForwarder option').attr('selected', 'selected');
	</script>
	
	<? }?>
	<script>
	$("#user_count").html('<?=$total?>');
	</script>
	<?
	if(!empty($kAdmin->arErrorMessages)){
	$t_base_error = "Error";
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kAdmin->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<? }else if(empty($kAdmin->arErrorMessages) && $_REQUEST['flag']=='sendMail'){	
	if(!empty($forwarderUserIdArr))
		$forwarderIdStr=implode(",",$forwarderUserIdArr);
	?>
	<script>
	addPopupScrollClass();
	</script>
	<div id="send_confirm_popup">
	<div id="popup-bg"></div>
	<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification">
		<h5><?=t($t_base.'fields/send');?></h5>
		<p><?=t($t_base.'fields/are_you_sure');?> <?=$total?> <?=t($t_base.'fields/recipients');?>?</p>
		<br>
		 <p align="center">
		  <input type="hidden" name="szForwarderIdArr" id="szForwarderIdArr" value="<?=$forwarderIdStr?>">
		 	<a href="javascript:void(0);" class="button1" id="yes" onclick="confirm_send_msg_to_forwarder();"><span><?=t($t_base.'fields/yes');?></span></a>
			<a href="javascript:void(0);" class="button2"  id="no" onclick="cancelPreviewMsg('send_confirm_popup');"><span><?=t($t_base.'fields/no');?></span></a>
		</p>
		</div>
	</div>
	</div>
	<? }
		else if(empty($kAdmin->arErrorMessages) && $_REQUEST['flag']=='FORWARDER_EMAIL_LIST')
		{?>
			<script>
			addPopupScrollClass();
			</script>
			<div id="customer_email_list">
			<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification" style="width:350px;">
				<h5><?=t($t_base.'fields/receivers_total');?> <?=$total?></h5>
				<div class="multi-payment-transfer-popup" style="height:200px;">
				<?
					if(!empty($forwarderUserIdArr))
					{
						for($k=0;$k<count($forwarderUserIdArr);++$k)
						{
							$forwarderArrId=array();
							$forwarderArrId=explode("_",$forwarderUserIdArr[$k]);
							$kForwarderContact->getUserDetails($forwarderArrId[0]);
							echo "<p>".$kForwarderContact->szEmail."</p>";
						}
					}
				?>
				</div>
				<br/>
				<p align="center"><a href="javascript:void(0);" class="button1"  id="no" onclick="cancelPreviewMsg('customer_email_list');"><span><?=t($t_base.'fields/close');?></span></a></p>
				</div>
			</div>
			</div>	
		<?		
		}
	if($_REQUEST['flag']!='FORWARDER_EMAIL_LIST')
	{	
	?>
	<?php
		forwarderBulkMsgView();
		}
	}
}
?>