<?php 
define('PAGE_PERMISSION','__LCL_SERVICES_LINK_BUILDER__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

$szMetaTitle="Transporteca | Link Builder";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");


$t_base="management/operations/statistics/";
validateManagement(); 
$redirect_url=__MANANAGEMENT_DASHBOARD_URL__;
header("Location:".$redirect_url);
exit(); 
$kWHSSearch = new cWHSSearch();
$szGoogleMapV3Key = $kWHSSearch->getManageMentVariableByDescription('__GOOGLE_MAP_V3_API_KEY__');
 
?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=en" type="text/javascript"></script>
<div id="customs_clearance_pop" class="tooltip-pop"></div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
	<h1><?php echo $landingPageDataAry['szTextB']; ?></h1>
	
	<section id="search-result">
		<div id="service-search-form-container">
			<?php 
				echo display_new_search_form_admin($kBooking,$postSearchAry,false,false,false,$szDefaultFromField);
			?>
		</div> 
		<div id="link-builder-url-container"> </div>
		<div id="service-listing-container"></div>
		<div id="calculations_details_div"></div>
	</section> 
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>