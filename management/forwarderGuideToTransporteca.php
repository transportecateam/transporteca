<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__FORWARDER_GUIDE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Site Text - Forwarder's Guide ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/guideForwarder/";
if(isset($_POST))
{	if(isset($_FILES["file"]))
	{
		$extension 		= explode(".",$_FILES["file"]["name"]);
		$fileExtension	= (end($extension));
		if ( ($fileExtension == "pdf") && ($_FILES["file"]["size"] < 2000000) )
		{ 
		  if ($_FILES["file"]["error"] <= 0)
		    {
		    	@unlink(__APP_PATH_ROOT__."/forwarders/guide/Forwarders_Guide_to_Transporteca.pdf");
		    	$upload = move_uploaded_file($_FILES["file"]["tmp_name"],__APP_PATH_ROOT__."/forwarders/guide/Forwarders_Guide_to_Transporteca.pdf");
				if($upload)
				{
					$kAdmin->ForwarderGuideVersion();
					echo"<script type=\"text/javascript\">
						$().ready(function(){
							$('#ajaxLogins').css('display','block');
						});		
					</script>";
				}
		    }
		}
		else
		{
			echo"<script type=\"text/javascript\">
				$().ready(function(){
					$('#Error-guide').css('display','block');
				});		
			</script>";
		}
		unset($_FILES);
	}
	unset($_POST);
}
?>
<div id="ajaxLogins" style="display: none;">
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup contact-popup"> 
				<h5><b><?=t($t_base.'fields/upload_success');?></b></h5>
				<p><?=t($t_base.'fields/file_uploaded_successfully');?></p>
				<br />
				<p align="center"><a class="button2" onclick="closed('ajaxLogins');"><span><?=t($t_base.'fields/close');?> </span></a></p>
		</div>
	</div>
</div>
<div id="Error-guide" style="display: none;">
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup contact-popup"> 
				<h5><b><?=t($t_base.'fields/error');?> </b></h5>
				<p><?=t($t_base.'fields/error_upload');?></p>
				<ul>
					<li><?=t($t_base.'fields/Unsupported');?></li>
					<li><?=t($t_base.'fields/larger');?></li>
				</ul>
				<br />
				<p><?=t($t_base.'fields/try_again');?></p>
				<p align="center"><a class="button2" onclick="closed('Error-guide');"><span><?=t($t_base.'fields/close');?></span></a></p>
		</div>
	</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?>
	<? $description = $kAdmin->findForwarderGuideVersion(); ?> 
	<div class="hsbody-2-right">
		<div id ="table" style="padding-bottom:20px;">
			<h4><strong><?=t($t_base.'title/guide');?></strong></h4>
                        <br>
			<p><?=t($t_base.'title/current_version');?> <?=($description['dtCreatedOn']?date('d/m/Y',strtotime($description['dtCreatedOn'])):'N/A');?> by <?=($description['szFirstName']?$description['szFirstName']." ".$description['szLastName']:'N/A');?>	</p>
			<br />
			<a href="<?=__MANAGEMENT_FORWARDER_GUIDE_URL__;?>" target="_blank" style="font-size:14px;font-style:normal;"><?=t($t_base.'title/click_here');?></a>
			<br /><br /><br />
			<p><?=t($t_base.'title/select_pdf');?></p>
			<form method="post" enctype="multipart/form-data" name="fileSubmit" id="submitData" action="">
			<div class="file">
			<input type="file" name="file" style="width: 100px;cursor: pointer;">	
			<span class="button"><?=t($t_base.'title/no_file_selected');?></span>
			</div>
			<br />
			<p><input class="button1" type="submit" value="UPLOAD" style="border: 0px;"></p>
			</form>
		</div>		
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>