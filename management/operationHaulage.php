<?php 
define('PAGE_PERMISSION','__CURRENT_HAULAGE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Pricing - Current Haulage";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/haualgeListing/";
validateManagement();
//$content = $kAdmin->currentHaulages(0);
constantApiKey();
?>
<script type="text/javascript">
$("#loader").attr('style','display:none;');
</script>
<script type="text/javascript">
$(document).ready(function(){
	//draw_canvas_image_obo(' ',' ','<?=__BASE_STORE_IMAGE_URL__.'/haulagePricingBulk-1.png'?>','HAULAGE_PRICING');
});
</script>

</script><style type="text/css">
      #map_canvas { height: 100% }     
      .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
      .map-content p{font-size:12px !important;}
      .map_note p{font-size:12px;font-style:italic;display:inline-block;margin:1px 0 18px;}
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
    </script>

<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>
<script type="text/javascript">
$().ready(function() {
/*
$("#Transportation_pop").attr("style","display:block;");
$.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",function(result){		
	//setTimeout(function(){
	    $("#Transportation_pop").attr('style','display:none;');		
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#hsbody-2").html(result_ary[1]);
		}
		if(result_ary[0]=='NO_RECORD_FOUND')
		{
			$("#customs_clearance_pop1").html(result_ary[1]);
		}	
		document.title = 'Transporteca | International Shipping Search Results';		
  	//},1000);		
   });	
   */
});
</script>
<script type="text/javascript">
	  var destinationIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/red-marker.png";
      var originIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/blue-marker.png";
	  var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
    var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(pipe_line_string)
    {
    	addPopupScrollClass();
    	var result_ary = pipe_line_string.split("|");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	initialize(result_ary);
    }
    	  
	function initialize(result_ary) {
	  var olat = result_ary[0];
	  var olang = result_ary[1];
	  var dlat = result_ary[2];
	  var dlang = result_ary[3];
	  var distance = result_ary[4];
	  var origin_address = result_ary[5];
	  var destination_address = result_ary[6];
	  var iDirection = result_ary[7];
	  var origin = new google.maps.LatLng(olat, olang);      
      var destination = new google.maps.LatLng(dlat, dlang);
	  var myLatLng = new google.maps.LatLng(olat, olang);
	  var myOptions = {
		zoom: 6,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  
	  if(iDirection==1)
	  {
		 $("#distance_div").html('<?=t($t_base.'fields/distance_to_warehouse');?> ' + distance + ' ' +'Km');
	  }
	  else if(iDirection==2)
	  {
	  	 $("#distance_div").html('<?=t($t_base.'fields/distance_from_warehouse');?> ' + distance + ' ' +'Km');
	  }
	  
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(olat, olang),
		new google.maps.LatLng(dlat, dlang)   
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
	  
	  var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"Origin"
	  }); 

	  var oaddress = '<div class="map-content"><p>Your cargo:<br>'+origin_address+'</p></div>';
	  var daddress = '<div class="map-content"><p>Forwarder\'s warehouse:<br>'+destination_address+'</p></div>';

	  var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker, 
		'click', 
		infoCallback(oinfowindow, originmarker)
	  );
      //oinfowindow.open(map,originmarker);        

	  var destinationmarker = new google.maps.Marker({
		  position: destination,
		  map: map,
		  icon: destinationIcon,
		  visible: true,
		  title:"Destination"
	  });    
	  
	  var dinfowindow = new google.maps.InfoWindow();
	  dinfowindow.setContent(daddress);
	  google.maps.event.addListener(
		destinationmarker, 
		'click', 
		infoCallback(dinfowindow, destinationmarker)
	  );
	  //dinfowindow.open(map,destinationmarker);        
}
</script>
 
<div id="customs_clearance_pop" class="help-pop"></div>
<div id="customs_clearance_pop_right" class="help-pop right"></div>
<div id="map_popup_div" style="display:none;">		
	<div id="popup-bg"></div>
	 <div id="popup-container"  class="popup-scroller">			
	  <div class="popup map-popup">
	    <div id="distance_div" style="font-weight:bold;margin-bottom:5px;"></div>
	  	<div id="map_canvas" style="width:500px;height:500px;border: 1px solid #C4BDA1;"></div>
	  	<div class="map_note">
		<p><?=t($t_base.'fields/notes');?>: <?=t($t_base.'fields/google_map_notes');?></p>
		</div>
	  	<p align="center"><a href="javascript:void(0);" onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
	  </div>
   </div>
</div>
<div id="hsbody-2">
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>
	<div id="popup-container" style="padding-top: 100px;">
	
	
	<p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
	
	</div>			
</div>
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
		<?php showCurrentHaulageTopForm($t_base);?>
		<div id="frameHaulage">
			<?php //haulageListing($content)?>
		</div>		
		<div id= "bottomForm"></div>
		<style type="text/css">
			h1,h2,h3,h4,h5{font: normal;}
		</style>
		
			<div id="model_list">		
				<?=pricingHaulage();?>
			</div>
			<div id="try_it_out"></div>
				<div id="showHaulageImage">
					<hr>
					<!--<canvas style="margin-left:-5px;" id="myCanvas" width="750" height="186"></canvas>-->
                                        <div id="canvas"> 
                                            <h4><strong><?=t($t_base.'title/illustration_of_what_haulage_services_cover')?></strong></h4>
                                            <img src="<?=__BASE_STORE_IMAGE_URL__?>/haulagePricingBulk-1.png">  
                                        </div>
				</div>
				<form method="post" id="obo_haulage_tryit_out_hidden_form" action="<?=__MANAGEMENT_URL__?>/oboHaulageHelpMap.php" target="google_map_target_1">
				<input type="hidden" name="oboHaulageHiddenAry[szPostCode]" id="szPostCode_hidden" value="">
				<input type="hidden" name="oboHaulageHiddenAry[szLatitude]" id="szLatitude_hidden" value="">
				<input type="hidden" name="oboHaulageHiddenAry[szLongitude]" id="szLongitude_hidden" value="">
				<input type="hidden" name="oboHaulageHiddenAry[szWhsLatitude]" id="szWhsLatitude_hidden" value="">
				<input type="hidden" name="oboHaulageHiddenAry[szWhsLongitude]" id="szWhsLongitude_hidden" value="">
				<input type="hidden" name="oboHaulageHiddenAry[idWarehouse]" id="idWarehouse_hidden" value="">
				<input type="hidden" name="oboHaulageHiddenAry[idDirection]" id="idDirection_hidden" value="">
		 </form>
			
		<div id="popup_container_google_map" style="display:none;">
			<div id="popup-bg"></div>
			<div id="popup-container">
				<iframe id="google_map_target_1" class="google_map_select popup"  name="google_map_target_1" src="#" >
				</iframe>
			</div>
		</div>
			<input type="hidden" value="" name="openHaulageModel" id="openHaulageModel">
			<input type="hidden" value="" name="openHaulageModelMapped" id="openHaulageModelMapped">
			<input type="hidden" value="0" name="callvalue" id="callvalue">
			
			<input type="hidden" value="" id="haulageCountry">
			<input type="hidden" value="" id="szCity">
			<input type="hidden" value="" id="haulageWarehouse">
			<input type="hidden" value="" id="idDirection">
	</div>
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	