<?php 
/**
  *  ADMIN -- EXPLAIN DATA -- ALL EXPLAIN DATA PAGE
  *  THIS IS USED TO UPDATE ALL EXPALIN PAGES FOR CUSTOMER EXPLAIN PAGE 
  *  @param id is id of particular page
  * 
  * ADDING NEW PAGE PROCESS 
  * 	DEFINE 
  * 		CONSTANT PAGE URL
  * 		HTACCESS
  * 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/layout/admin_header.php");
$kExplain = new cExplain();
 
validateManagement();
$t_base="management/explaindata/";
$iType = sanitize_all_html_input($_REQUEST['type']);

$ExplainContent = $kExplain->text_editor($idAdmin,$content['id']);
$publish=$kExplain->findPubishStatus($content['id']);
$style='';

if(!$publish)
{
    $style="style='opacity:0.4;'";
}
?>
<div id="ajaxLogins" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container" >

<div class="popup">
	<p><?=t($t_base."fields/are_you_sure")?></p>
	<br />
	<p align="center"><a href="javascript:void(0)" class="button2" onclick="deleteExchangeRates(2);" ><span><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="deleteTextEditor" class="button1" onclick="deleteTextEditor(mode,id);"><span><?=t($t_base.'fields/delete');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
			<div style="padding-bottom: 20px;">
			<div id="error"></div>
			<div id="showDiv">
				<? //print_r($textEditor); ?>
				<div style="clear:both;"></div>
		
			<div id="showUpdate">
				<?php viewExplainData($ExplainContent,$content['id']);?>		
					
			</div>	
					
			</div>	
			<div id="text_t_n_C"></div>	
		</div>
		<div id="preview_Publish">
			<a id="add" onclick="editExplainationDet('<?=$content['id']?>','0');" style="float: left;cursor: pointer;"><span><?=t($t_base.'fields/add');?></span></a>
			<div style="float: right;">
                            <!-- <a id="preview" class="button1" onclick="preview_explain_data('<?=$content['id']?>','preview');" ><span><?=t($t_base.'fields/preview');?></span></a>-->
                            <a id="preview" href="<?php echo __BASE_URL__."/aggregatePagePreview/".$content['id']."/"?>" target="_blank" class="button1" ><span><?=t($t_base.'fields/preview');?></span></a>
                            <a id="publish" class="button1" <?php if($publish){?>onclick="preview_explain_data('<?=$content['id']?>','publish');" <? }else{echo $style;} ?>><span><?=t($t_base.'fields/publishe');?></span></a>

                            <a id="cancel" class="button2" <?php if($publish){?>onclick="preview_explain_data('<?=$content['id']?>','cancel');" <? }else{echo $style;} ?>><span><?=t($t_base.'fields/cancel');?></span></a>
			</div>
			<div style="clear: both;"></div>
			<br />
			<div id="preview_data" class="preview_box" style="display: none;">
			</div>
	</div>
</div>
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>