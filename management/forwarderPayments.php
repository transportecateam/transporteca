<?php 
/**
  *pay forwarder
  */
define('PAGE_PERMISSION','__PAY_FORWARDER__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
$szMetaTitle="Transporteca | Accounts Payable - Pay Forwarder";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/forwarderpayment/";
validateManagement();
$kBooking = new cBooking();
$billingDetails=$kAdmin->showForwarderPendingPayment(true);
//print_r($billingDetails);

?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
    <?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>

    <div id="checkDetails" class="hsbody-2-right">
        <?php showForwarderPendingPayment($billingDetails,$idAdmin); ?>
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
