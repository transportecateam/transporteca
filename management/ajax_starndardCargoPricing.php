<?php
ob_start();
session_start(); 
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );

$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$kExplain = new cExplain();

if($mode=='DIAPLAY_QUOTE_PRICING_HANDLER')
{
    $idLandingPage = sanitize_all_html_input(trim($_REQUEST['page_id']));
    if($idLandingPage>0)
    {
        echo "SUCCESS||||";
        echo display_standard_cargo_quote_pricing_handler($idLandingPage);
        die;
    }
}
else if($mode=='DIAPLAY_ADD_QUOTE_PRICING_POPUP')
{
    $idLandingPage = sanitize_all_html_input(trim($_REQUEST['page_id']));
    $idStandardQuotePricing = sanitize_all_html_input(trim($_REQUEST['quote_pricing_id']));
    $_POST['addStandardQuotePricingAry']['idHandlingCurrency']=__USD_CURRENCY_ID__;
    $_POST['addStandardQuotePricingAry']['idHandlingMinMarkupCurrency']=__USD_CURRENCY_ID__;
    $_POST['addStandardQuotePricingAry']['fHandlingMarkupPercentage']=0;
    $_POST['addStandardQuotePricingAry']['fHandlingFeePerBooking']=0;
    $_POST['addStandardQuotePricingAry']['fHandlingMinMarkupPrice']=0;
    if($idLandingPage>0)
    {
        echo "SUCCESS||||"; 
        echo display_add_standard_cargo_pricing($idLandingPage);
        die;
    }
}
else if($mode=='RESET_STANDARD_TRY_IT_OUT')
{
    $idLandingPage = sanitize_all_html_input(trim($_REQUEST['page_id']));
    echo "SUCCESS||||"; 
    echo display_add_standard_cargo_pricing_try_it_out($idLandingPage);
    die;
}
else if(!empty($_POST['addStandardQuotePricingAry']))
{ 
    $idLandingPage = (int)$_POST['addStandardQuotePricingAry']['idLandingPage'];
    if($kExplain->addStandardQuotePricing($_POST['addStandardQuotePricingAry']))
    {
        $standardQuotePricingAry = array();
        $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idLandingPage,false,0,false,true);
    
        echo "SUCCESS||||";
        echo display_standard_cargo_quotes_listing($idLandingPage,$standardQuotePricingAry);
        die;
    }
    else
    {
        echo "ERROR||||"; 
        echo display_add_standard_cargo_pricing($idLandingPage,array(),$kExplain);
        die;
    }
}
else if(!empty($_POST['searchAry']))
{
    $idLandingPage = (int)$_POST['searchAry']['idLandingPage'];
    $iVogaPageType = (int)$_POST['searchAry']['iVogaPageType'];
    $iNewAutoRFQ = (int)$_POST['iNewAutoRFQ'];
    $searchFormAry = $_POST['searchAry'];
    $searchFormAry['idDefaultLandingPage'] = $idLandingPage;
    $cargodetailAry = $_POST['cargodetailAry'];
    $iCalculateVogaAutomaticService = false;
    
    if($iNewAutoRFQ==1)
    {
        $kConfig=new cConfig(); 
        $postSearchAry = $searchFormAry;
        if($postSearchAry['szOriginCountryStr']!='')
        {
            $szOriginCountryArr = reverse_geocode($postSearchAry['szOriginCountryStr'],true);  
            $replaceAry = array(" ",",");
            $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szOriginCountryStr']); 
            $szCountryStrSerialized = serialize($szOriginCountryArr);   

            if(__ENVIRONMENT__ == "LIVE")
            {
                setcookie('FROM_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/',$_SERVER['HTTP_HOST'],true);
            }
            else
            {
                setcookie('FROM_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/');  
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/');
            } 
            $postSearchAry['szOriginCountry']=$szOriginCountryArr['szCountryName'];
            $idOriginCountry=$kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);

            if($idOriginCountry<=0 && !empty($szOriginCountryArr['szCountryCode']))
            {
                $kConfig->loadCountry(false,$szOriginCountryArr['szCountryCode']);
                $idOriginCountry = $kConfig->idCountry ; 
            }

            $postSearchAry['szOriginCountry']=$idOriginCountry;
            $postSearchAry['szOLat']=$szOriginCountryArr['szLat'];
            $postSearchAry['szOLng']=$szOriginCountryArr['szLng'];
            $postSearchAry['szOriginCity']=$szOriginCountryArr['szCityName']; 
            $postSearchAry['szOriginPostCode'] = $szOriginCountryArr['szPostCode']; 
            $postSearchAry['szOriginPostCodeTemp'] = $szOriginCountryArr['szPostcodeTemp']; 

            $postSearchAry['iOriginPostcodeFoundAtStep'] = 1;

            if(empty($postSearchAry['szOriginPostCode']))
            {
                $postcodeCheckAry=array();
                $postcodeResultAry = array();

                $postcodeCheckAry['idCountry'] = $idOriginCountry ;
                $postcodeCheckAry['szLatitute'] = $szOriginCountryArr['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $szOriginCountryArr['szLng'] ;

                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                if(!empty($postcodeResultAry))
                {
                    $postSearchAry['szOriginPostCode'] = $postcodeResultAry['szPostCode'];
                    $postSearchAry['iOriginPostcodeFoundAtStep'] = 2;
                    $postSearchAry['szOriginPostCodeTemp'] = $postcodeResultAry['szPostCode']; 
                }
            } 
        }  
        if($postSearchAry['szDestinationCountryStr']!='')
        {
            $szDesCountryArr=reverse_geocode($postSearchAry['szDestinationCountryStr'],true); 

            $replaceAry = array(" ",",");
            $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szDestinationCountryStr']); 
            $szCountryStrSerialized = serialize($szDesCountryArr);   

            if(__ENVIRONMENT__ == "LIVE")
            {
                setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/',$_SERVER['HTTP_HOST'],true);
            }
            else
            {
                setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/');  
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/');
            }
            $postSearchAry['szDestinationCountry']=$szDesCountryArr['szCountryName'];
            $idDesCountry=$kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);

            if($idDesCountry<=0 && !empty($szDesCountryArr['szCountryCode']))
            {
                $kConfig->loadCountry(false,$szDesCountryArr['szCountryCode']);
                $idDesCountry = $kConfig->idCountry ; 
            }

            $postSearchAry['szDestinationCountry']=$idDesCountry;
            $postSearchAry['szDLat']=$szDesCountryArr['szLat'];
            $postSearchAry['szDLng']=$szDesCountryArr['szLng'];
            $postSearchAry['szDestinationCity']=$szDesCountryArr['szCityName'];
            $postSearchAry['szDestinationPostCode'] = $szDesCountryArr['szPostCode']; 
            $postSearchAry['iDestinationPostcodeFoundAtStep'] = 1;
            $postSearchAry['szDestinationPostCodeTemp'] = $szDesCountryArr['szPostcodeTemp'];

            if(empty($postSearchAry['szDestinationPostCode']))
            {
                $postcodeCheckAry = array(); 
                $postcodeResultAry = array(); 
                $postcodeCheckAry['idCountry'] = $idDesCountry ;
                $postcodeCheckAry['szLatitute'] = $szDesCountryArr['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $szDesCountryArr['szLng'] ;
                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                if(!empty($postcodeResultAry))
                {
                    $postSearchAry['szDestinationPostCode'] = $postcodeResultAry['szPostCode'];
                    $postSearchAry['szDestinationPostCodeTemp'] = $postcodeResultAry['szPostCode'];
                    $postSearchAry['iDestinationPostcodeFoundAtStep'] = 2;
                }
            } 
        }   
        $searchFormAry = $postSearchAry;
        
        $kConfig = new cConfig();
        $kConfig->validateAutomaticeRFQPage($searchFormAry,$cargodetailAry,$iSearchMiniPageVersion,true);
    }
    else
    {
        

        if($iVogaPageType==__SEARCH_TYPE_VOGUE_AUTOMATIC_ || $iVogaPageType==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_)
        {
            if($iVogaPageType==__SEARCH_TYPE_VOGUE_AUTOMATIC_)
                $iSearchMiniPageVersion = 8; 
            else if($iVogaPageType==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_)
                $iSearchMiniPageVersion = 9;

            $kConfig=new cConfig(); 

            $postSearchAry = $searchFormAry;
            if($postSearchAry['szOriginCountryStr']!='')
            {
                $szOriginCountryArr = reverse_geocode($postSearchAry['szOriginCountryStr'],true);  
                $replaceAry = array(" ",",");
                $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szOriginCountryStr']); 
                $szCountryStrSerialized = serialize($szOriginCountryArr);   

                if(__ENVIRONMENT__ == "LIVE")
                {
                    setcookie('FROM_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/',$_SERVER['HTTP_HOST'],true); 
                    setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/',$_SERVER['HTTP_HOST'],true);
                }
                else
                {
                    setcookie('FROM_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/');  
                    setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/');
                } 
                $postSearchAry['szOriginCountry']=$szOriginCountryArr['szCountryName'];
                $idOriginCountry=$kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);

                if($idOriginCountry<=0 && !empty($szOriginCountryArr['szCountryCode']))
                {
                    $kConfig->loadCountry(false,$szOriginCountryArr['szCountryCode']);
                    $idOriginCountry = $kConfig->idCountry ; 
                }

                $postSearchAry['szOriginCountry']=$idOriginCountry;
                $postSearchAry['szOLat']=$szOriginCountryArr['szLat'];
                $postSearchAry['szOLng']=$szOriginCountryArr['szLng'];
                $postSearchAry['szOriginCity']=$szOriginCountryArr['szCityName']; 
                $postSearchAry['szOriginPostCode'] = $szOriginCountryArr['szPostCode']; 
                $postSearchAry['szOriginPostCodeTemp'] = $szOriginCountryArr['szPostcodeTemp']; 

                $postSearchAry['iOriginPostcodeFoundAtStep'] = 1;

                if(empty($postSearchAry['szOriginPostCode']))
                {
                    $postcodeCheckAry=array();
                    $postcodeResultAry = array();

                    $postcodeCheckAry['idCountry'] = $idOriginCountry ;
                    $postcodeCheckAry['szLatitute'] = $szOriginCountryArr['szLat'] ;
                    $postcodeCheckAry['szLongitute'] = $szOriginCountryArr['szLng'] ;

                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                    if(!empty($postcodeResultAry))
                    {
                        $postSearchAry['szOriginPostCode'] = $postcodeResultAry['szPostCode'];
                        $postSearchAry['iOriginPostcodeFoundAtStep'] = 2;
                        $postSearchAry['szOriginPostCodeTemp'] = $postcodeResultAry['szPostCode']; 
                    }
                } 
            }  
            if($postSearchAry['szDestinationCountryStr']!='')
            {
                $szDesCountryArr=reverse_geocode($postSearchAry['szDestinationCountryStr'],true); 

                $replaceAry = array(" ",",");
                $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szDestinationCountryStr']); 
                $szCountryStrSerialized = serialize($szDesCountryArr);   

                if(__ENVIRONMENT__ == "LIVE")
                {
                    setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/',$_SERVER['HTTP_HOST'],true); 
                    setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/',$_SERVER['HTTP_HOST'],true);
                }
                else
                {
                    setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/');  
                    setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/');
                }
                $postSearchAry['szDestinationCountry']=$szDesCountryArr['szCountryName'];
                $idDesCountry=$kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);

                if($idDesCountry<=0 && !empty($szDesCountryArr['szCountryCode']))
                {
                    $kConfig->loadCountry(false,$szDesCountryArr['szCountryCode']);
                    $idDesCountry = $kConfig->idCountry ; 
                }

                $postSearchAry['szDestinationCountry']=$idDesCountry;
                $postSearchAry['szDLat']=$szDesCountryArr['szLat'];
                $postSearchAry['szDLng']=$szDesCountryArr['szLng'];
                $postSearchAry['szDestinationCity']=$szDesCountryArr['szCityName'];
                $postSearchAry['szDestinationPostCode'] = $szDesCountryArr['szPostCode']; 
                $postSearchAry['iDestinationPostcodeFoundAtStep'] = 1;
                $postSearchAry['szDestinationPostCodeTemp'] = $szDesCountryArr['szPostcodeTemp'];

                if(empty($postSearchAry['szDestinationPostCode']))
                {
                    $postcodeCheckAry = array(); 
                    $postcodeResultAry = array(); 
                    $postcodeCheckAry['idCountry'] = $idDesCountry ;
                    $postcodeCheckAry['szLatitute'] = $szDesCountryArr['szLat'] ;
                    $postcodeCheckAry['szLongitute'] = $szDesCountryArr['szLng'] ;
                    $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                    if(!empty($postcodeResultAry))
                    {
                        $postSearchAry['szDestinationPostCode'] = $postcodeResultAry['szPostCode'];
                        $postSearchAry['szDestinationPostCodeTemp'] = $postcodeResultAry['szPostCode'];
                        $postSearchAry['iDestinationPostcodeFoundAtStep'] = 2;
                    }
                } 
            }   
            $searchFormAry = $postSearchAry;
        }
        else
        {
            $iSearchMiniPageVersion = 7;
        } 
        $searchFormAry['idCC'][0]='1';
        $searchFormAry['idCC'][1]='2';
        $searchFormAry['idServiceType'] = __SERVICE_TYPE_DTD__;

        $kConfig = new cConfig();
        $kConfig->validateVogueLandingPage($searchFormAry,$cargodetailAry,$iSearchMiniPageVersion,true);
    }
      
    if(!empty($kConfig->arErrorMessages))
    {
        echo "ERROR||||";
        if($iNewAutoRFQ==1)
        {
            echo display_add_standard_cargo_pricing_try_it_out_new($idLandingPage,$kConfig);
        }
        else
        {
            echo display_add_standard_cargo_pricing_try_it_out($idLandingPage,$kConfig);
        }
        die;
    }
    else
    {
        if($kConfig->iVogaSearchAutomatic)
        { 
      
            $kExplain = new cExplain();
            $vogaPageDetailsAry = $kExplain->getAllNewLandingPageData($idLandingPage);
            $landingPageDataAry = $vogaPageDetailsAry[0]; 
            $iBookingLanguage = $landingPageDataAry['iLanguage'];
    
            $kWHSSearch = new cWHSSearch();
            $postSearchAry = array();
            $postSearchAry['szOriginPostCode'] = $kConfig->szOriginPostCode;
            $postSearchAry['szOriginCity'] = $kConfig->szOriginCity;
            $postSearchAry['idOriginCountry'] = $kConfig->szOriginCountry;
            $postSearchAry['szOriginCountry'] = $kConfig->szOriginCountryName;
            $postSearchAry['fOriginLatitude'] = $kConfig->fOriginLatitude;
            $postSearchAry['fOriginLongitude'] = $kConfig->fOriginLongitude;  
            $postSearchAry['idDestinationCountry'] = $kConfig->szDestinationCountry; 
            $postSearchAry['szDestinationCountry'] = $kConfig->szDestinationCountryName; 
            $postSearchAry['szDestinationCity'] = $kConfig->szDestinationCity;
            $postSearchAry['szDestinationPostCode'] = $kConfig->szDestinationPostCode;
            $postSearchAry['szDestinationAddress'] = $kConfig->szDestinationAddress;
            $postSearchAry['szDestinationCountry'] = $kConfig->szDestinationCountryName;
            $postSearchAry['fDestinationLatitude'] = $kConfig->fDestinationLatitude;
            $postSearchAry['fDestinationLongitude'] = $kConfig->fDestinationLongitude;
            $postSearchAry['iOriginCC'] = '1';
            $postSearchAry['iDestinationCC'] = '2';
            $postSearchAry['idTimingType'] = 1; 
            $postSearchAry['idCurrency'] = 1 ;
            $postSearchAry['szCurrency'] = 'USD' ;
            $postSearchAry['fExchangeRate'] = 1 ;
            $postSearchAry['iFromRequirementPage'] = 2;
            
            $postSearchAry['idCustomerCurrency'] = 1;
            $postSearchAry['szCustomerCurrency'] = 'USD';
            
            // converting cargo details into cubic meter 			
            $counter_cargo = count($kConfig->iLength);
            $showCourierPriceFlag=0;
            for($i=1;$i<=$counter_cargo;$i++)
            {
                $fLength = $kConfig->iLength[$i] ;
                $fWidth = $kConfig->iWidth[$i] ;
                $fHeight = $kConfig->iHeight[$i] ;
                $iQuantity = $kConfig->iQuantity[$i] ;
                $iVolumePerColli = $kConfig->iVolumePerColli[$i];
                
                $customizedCargodetailAry[$i]['fLength'] = $fLength;
                $customizedCargodetailAry[$i]['fWidth'] = $fWidth;
                $customizedCargodetailAry[$i]['fHeight'] = $fHeight;
                $customizedCargodetailAry[$i]['fWeight'] = $kConfig->iWeight[$i];
                $customizedCargodetailAry[$i]['iQuantity'] = $iQuantity;
                $customizedCargodetailAry[$i]['iColli'] = $iQuantity;
                $customizedCargodetailAry[$i]['idWeightMeasure'] = $kConfig->idWeightMeasure[$i];
                $customizedCargodetailAry[$i]['idCargoMeasure'] = $kConfig->idCargoMeasure[$i];
                
                if($fLength==0.00 && $fWidth==0.00 && $fHeight==0.00)
                {
                    $fTotalVolume += $iVolumePerColli * $iQuantity;
                    $showCourierPriceFlag=1;
                }
                else
                {
                    if($kConfig->idCargoMeasure[$i]==1)  // cm
                    {
                        $fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
                    }
                    else
                    {
                        $fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
                        if($fCmFactor>0)
                        {
                            $fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
                        }	
                    }
                }
            }
 
            $weight_cargo = count($kConfig->iWeight);
            for($i=1;$i<=$weight_cargo;$i++)
            {
                // converting cargo details into cubic meter 
                if($kConfig->idWeightMeasure[$i]==1) // kg
                {
                    $fTotalWeight += ($kConfig->iWeight[$i]);
                }
                else
                {
                    $fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
                    if($fKgFactor>0)
                    {
                        $fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
                    }	
                }
            } 
            $postSearchAry['fCargoVolume'] = $fTotalVolume ;
            $postSearchAry['fCargoWeight'] = $fTotalWeight ; 
            $postSearchAry['forwarder_id'] = $idForwarder ;
            $postSearchAry['dtTimingDate'] = date('Y-m-d') ;
            $postSearchAry['idServiceType'] = __SERVICE_TYPE_DTD__; 
            $postSearchAry['szSearchMode'] = 'FORWARDER_COURIER_TRY_IT_OUT';
            $postSearchAry['idLandingPage'] = $idLandingPage;
            $postSearchAry['iSearchMiniVersion'] = 7;
            $postSearchAry['szCargoDescription'] = $kConfig->szCargoDescription;
            $postSearchAry['iBookingLanguage'] = $iBookingLanguage;
            $postSearchAry['iShowCourierPriceFlag'] = $showCourierPriceFlag;
  
            cWHSSearch::$cForwarderTryitoutCargoDetails = $customizedCargodetailAry; 
              
//            $_SESSION['searchResult'] = array();
//            if(!empty($_SESSION['searchResult']))
//            {
//                $searchResultAry = $_SESSION['searchResult'];
//            }
//            else
//            {
//                $searchResultAry = array();
//                $searchResultAry = $kWHSSearch->get_search_result($postSearchAry);  
//
//                $_SESSION['searchResult'] = $searchResultAry;
//            } 
            if($iNewAutoRFQ==1)
            {
                $postSearchAry['iCheckHandlingFeeForAutomaticQuote']=1;
                $postSearchAry['iCreateAutomaticQuote']=1;
            } 
            $searchResultAry = array();
            $searchResultAry = $kWHSSearch->get_search_result($postSearchAry);  
             
            $szCalculationLogString = $kWHSSearch->szCalculationLogString;
            $iNumRecordFound = count($searchResultAry);  
             
            /*
            *  Creating Automatic quotes for Voga pages and added this to Mgt >  Pending Tray > Quote
            */  
            $szQuoteEmailAry = $kConfig->createQuoteEmailForTryitout($postSearchAry,$searchResultAry,$idLandingPage);  
            if(!empty($kConfig->szCalculationLogString))
            {
                $szCalculationLogString .= "<br><br>".$kConfig->szCalculationLogString;
            } 
            if(!empty($szQuoteEmailAry))
            {
                $szLandingPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__;
                echo "SUCCESS||||";
                echo '<hr><p><strong> Subject:</strong> '.$szQuoteEmailAry['szQuoteEmailSubject']."</p><br>";

                $message = '  
                    <p align="right">
                        <a href="'.$szLandingPageUrl.'">
                            <img src="'.$szLandingPageUrl.'/images/TransportecaMail.png" alt="Transporteca" title="Transporteca" border="0" />
                         </a> 
                    </p>';
                $message .= $szQuoteEmailAry['szQuoteEmailBody'];

                echo $message;
                echo "<br><br><a href='javascript:void(0)' class='button1' onclick=showHideCourierCal('courier_service_calculation')>View Courier Calculation</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                    </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$szCalculationLogString." </div></div> </div></div>";
                die; 
            }
            else
            {
                echo "SUCCESS||||";
                echo "<hr><p><strong>No pricing, request will be created in pending tray</strong></p>";
                echo "<br><br><a href='javascript:void(0)' class='button1' onclick=showHideCourierCal('courier_service_calculation')>View Courier Calculation</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                    </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$szCalculationLogString." </div></div> </div></div>";
                die;
            } 
        }
        else
        {
            echo "SUCCESS||||";
            echo "<hr><p><strong>No pricing, either quote or handler is not defined for this landing page. Request will be created in pending tray</strong></p>";
            die;
        }
    }
    die;
}
else if(!empty($_POST['addStandardCargoHandler']))
{ 
    $idLandingPage = (int)$_POST['addStandardCargoHandler']['idLandingPage'];
    if(is_base64_encoded($_POST['addStandardCargoHandler']['szQuoteEmailBody']))
    {    
        $_POST['addStandardCargoHandler']['szQuoteEmailBody'] = htmlspecialchars_decode(urldecode(base64_decode($_POST['addStandardCargoHandler']['szQuoteEmailBody']))); 
    }
    if($kExplain->addStandardCargoHandling($_POST['addStandardCargoHandler']))
    {
        $_POST['addStandardCargoHandler']=array();
        unset($_POST['addStandardCargoHandler']);
        $standardHandlerAry = array();
        $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($idLandingPage,false,true);
        echo "SUCCESS||||";
        echo display_standard_cargo_handler($idLandingPage,$kExplain,$standardHandlerAry,1);
        die;
    }
    else
    {
        echo "ERROR||||"; 
        echo display_standard_cargo_handler($idLandingPage,$kExplain,false,2);
        die;
    }
}
else if($mode=='ADD_COURIER_PROVIDER_DROPDOWN')
{
    $idForwarder=(int)$_POST['idForwarder'];
    if((int)$idForwarder>0)
    {
        $kCourierServices = new cCourierServices();
        $data['idForwarder'] = $idForwarder;
        $forwarderListAry = $kCourierServices->getAllCourierAgreement($data,'',true);
        echo "SUCCESS_PROVIDER_DROP_DOWN||||";
        ?>
        <select id="idCourierProvider" name="addStandardQuotePricingAry[idCourierProvider]" onchange="enableProviderProducOptions(this.value);enableQuotePricingSubmitButton();"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
            <option value=''>Select</option>
            <?php 
                if(!empty($forwarderListAry))
                {
                    foreach($forwarderListAry as $forwarderListArys)
                    {
                        ?>
                        <option  value='<?php echo $forwarderListArys['idCourierProvider']?>'><?php echo $forwarderListArys['szCourierProviderName']; ?></option>
                        <?php
                    }
                }
            ?>
        </select>
        <?php
    }
    else 
    {
        echo "ERROR||||";
        die();
    }
}
else if($mode=='ADD_COURIER_PROVIDER_PRODUCT_DROPDOWN')
{
    $idForwarder=(int)$_POST['idForwarder'];
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    if((int)$idForwarder>0)
    {
        $kCourierServices = new cCourierServices();
        $data['idForwarder'] = $idForwarder;
        $forwarderListAry = $kCourierServices->getCourierProductByIdForwarderIdCourierProvider($idCourierProvider,$idForwarder);
        echo "SUCCESS_PROVIDER_PRODUCT_DROP_DOWN||||";
        ?>
        <select id="idCourierProviderProduct" name="addStandardQuotePricingAry[idCourierProviderProduct]" onchange="enableQuotePricingFileds(this.value);"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
            <option value=''>Select</option>
            <?php 
                if(!empty($forwarderListAry))
                {
                    foreach($forwarderListAry as $forwarderListArys)
                    {
                        ?>
                        <option  value='<?php echo $forwarderListArys['idProviderProduct']?>'><?php echo $forwarderListArys['szName']; ?></option>
                        <?php
                    }
                }
            ?>
        </select>
        <?php
    }
    else 
    {
        echo "ERROR||||";
        die();
    }
}
else if($mode=='MOVE_UP_DOWN_STANDARD_PRICING')
{
    $idStandardPricing=(int)$_POST['idStandardPricing'];
    $iSequence=(int)$_POST['iSequence'];
    $szFlag=trim($_POST['szFlag']);
    $idCustomLandingPage=(int)$_POST['idCustomLandingPage'];
    
    $kExplain->moveUpDownStandardPricingData($idStandardPricing,$iSequence,$szFlag,$idCustomLandingPage);
    
    $standardQuotePricingAry = array();
    $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idCustomLandingPage,false,0,false,true);
    
    echo display_standard_cargo_quotes_listing($idCustomLandingPage,$standardQuotePricingAry);
}
else if($mode=='DELETE_STANDARD_PRICING')
{
     $idStandardPricing=(int)$_POST['idStandardPricing'];
    $iSequence=(int)$_POST['iSequence'];
    
    $idCustomLandingPage=(int)$_POST['idCustomLandingPage'];
    
    $kExplain->deleteStandardPricingData($idStandardPricing,$iSequence,$idCustomLandingPage);
    
    $standardQuotePricingAry = array();
    $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idCustomLandingPage,false,0,false,true);
    
    echo display_standard_cargo_quotes_listing($idCustomLandingPage,$standardQuotePricingAry);
}
else if($mode=='OPEN_EDIT_STANDARD_PRICING')
{
    $idLandingPage=(int)$_POST['idCustomLandingPage'];
    $idStandardPricing=(int)$_POST['idStandardPricing'];
    
    $standardPriceArr=$kExplain->getStandardQuotePriceByPageID($idLandingPage,'',$idStandardPricing,false,true);
   // print_r($standardPriceArr);
    $_POST['addStandardQuotePricingAry']=$standardPriceArr[0];
    $_POST['addStandardQuotePricingAry']['idStandardQuotePricing']=$idStandardPricing;
    display_add_standard_cargo_pricing($idLandingPage);
}
else if($mode=='ADD_NEW_CARGO_DETAIL_LINE_FOR_VOGA_PAGES')
{
    $idLandingPage=(int)$_POST['idCustomLandingPage'];
    $iSearchMiniPageVersion = 7;
    $newCounter=(int)$_POST['newCounter'];
    
    $kExplain = new cExplain();
    $vogaPageDetailsAry = $kExplain->getAllNewLandingPageData($idLandingPage);
    $landingPageDataAry = $vogaPageDetailsAry[0]; 
    ?>
    <div id="add_booking_quote_container_<?php echo $newCounter.'_'.$iSearchMiniPageVersion; ?>">
        <?php echo display_furniture_order_form_management($newCounter,$iSearchMiniPageVersion,array(),$landingPageDataAry['idSearchMini']); ?>
    </div>
    <?php 
    die();
}
else if($mode=='OPEN_AUTOMATED_RFQ_RESPONSE_POP')
{
    $idAutomatedRfq = (int)$_POST['idAutomatedRfq'];
    if($idAutomatedRfq>0)
    {
        $automatedRfqResponseAry =$kExplain->getAutomatedRfqResponseList($idAutomatedRfq);
        $_POST['automatedRfqArr']['szComment']=$automatedRfqResponseAry[0]['szResponse'];
        $_POST['automatedRfqArr']['id']=$idAutomatedRfq;
    }
    automateRfqResponseHtmlForm($kExplain);
}
else if($mode=='ADD_UPDATE_AUTOMATED_RFQ_RESPONSE')
{
    $t_base = "management/uploadService/";
    $szCommentText=(urldecode(base64_decode($_REQUEST['szCommentText'])));
    $idAutomatedRfq=(int)$_REQUEST['idAutomatedRfq'];
    if($kExplain->addUpdateAutomatedRfqResponse($szCommentText,$idAutomatedRfq))
    {
        $automatedRfqResponseAry=array();
        $automatedRfqResponseAry =$kExplain->getAutomatedRfqResponseList();
        echo "SUCCESS||||";
        ?>
            <option value=""><?php echo t($t_base.'fields/select')?></option>
            <?php
            if(!empty($automatedRfqResponseAry))
            { 
                foreach($automatedRfqResponseAry as $automatedRfqResponseArys)
                {?>                           
                    <option value=<?php echo $automatedRfqResponseArys['id'];?> <?php if($idAutomatedRfq==$automatedRfqResponseArys['id']){?> selected <?php }?>><?php echo $automatedRfqResponseArys['szResponse']?></option>;
                    <?php
                }
            }
      
    }
}
else if($mode=='CONFIRM_DELETE_AUTOMATED_RFQ_RESPONSE_POP')
{
     $idAutomatedRfq=(int)$_REQUEST['idAutomatedRfq'];
     confirmationPopupForDeletingAutomatedRfqResponse($idAutomatedRfq);
}
else if($mode=='CONFIRMED_DELETE_AUTOMATED_RFQ_RESPONSE')
{
    $t_base = "management/uploadService/";
    $idAutomatedRfq=(int)$_REQUEST['idAutomatedRfq'];
    if($kExplain->deleteAutomatedRfqResponse($idAutomatedRfq))
    {
        $automatedRfqResponseAry=array();
        $automatedRfqResponseAry =$kExplain->getAutomatedRfqResponseList();
        echo "SUCCESS||||";
        ?>
            <option value=""><?php echo t($t_base.'fields/select')?></option>
            <?php
            if(!empty($automatedRfqResponseAry))
            { 
                foreach($automatedRfqResponseAry as $automatedRfqResponseArys)
                {?>                           
                    <option value=<?php echo $automatedRfqResponseArys['id'];?>><?php echo $automatedRfqResponseArys['szResponse']?></option>;
                    <?php
                }
            }
      
    }
}
else if($mode=='ADD_NEW_CARGO_DETAIL_LINE_FOR_VOGA_PAGES_NEW')
{
    $idLandingPage=(int)$_POST['idCustomLandingPage'];
    $iSearchMiniPageVersion = 7;
    $newCounter=(int)$_POST['newCounter'];
    
    
    ?>
    <div id="add_booking_quote_container_<?php echo $newCounter.'_'.$iSearchMiniPageVersion; ?>">
        <?php echo display_furniture_order_form_management_new($newCounter,$iSearchMiniPageVersion,array(),$landingPageDataAry['idSearchMini']); ?>
    </div>
    <?php 
    die();
}
else if($mode=='RESET_STANDARD_TRY_IT_OUT_NEW')
{
    $idLandingPage = sanitize_all_html_input(trim($_REQUEST['page_id']));
    echo "SUCCESS||||"; 
    echo display_add_standard_cargo_pricing_try_it_out_new($idLandingPage);
    die;
}
else if($mode=='SHOW_CATEGORY_CARGO_TYPE')
{
    $idCategory = sanitize_all_html_input(trim($_REQUEST['idCategory']));
    $iCounter = sanitize_all_html_input(trim($_REQUEST['iCounter']));
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__); 
    $kConfig = new cConfig();
    $idSuffix = "_V_7" ;
    if($idCategory>0)
    { 
        $standardCargoAry = $kConfig->getStandardCargoList($idCategory,$iSelectedLanguage); 
    }    
    echo display_cargo_type_dropdown_management_new($standardCargoAry,$iCounter,$idSuffix);
    die();
}
else if($mode=='SHOW_CATEGORY_TYPE')
{
    $idSearchMini = sanitize_all_html_input(trim($_REQUEST['idSearchMini']));
    $iCounter = sanitize_all_html_input(trim($_REQUEST['iCounter']));
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__); 
    $kConfig = new cConfig();
    $idSuffix = "_V_7" ;
    if($idSearchMini>0)
    { 
        $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,'',$idSearchMini);
    }   
    echo display_category_type_dropdown_management_new($standardCargoCategoryAry,$iCounter,$idSuffix);
    echo "||||";
    echo display_cargo_type_dropdown_management_new($standardCargoAry,$iCounter,$idSuffix);
    die();
}