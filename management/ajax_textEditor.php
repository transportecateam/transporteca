<?php
/**
 * Edit Forwarder profile
 */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$kExplain = new cExplain;
validateManagement();
$t_base="management/Error/";
//define constant
$zero=0;
if(isset($_POST))
{
	if(isset($_POST['mode']))
	{	
		$mode=sanitize_all_html_input($_POST['mode']);
		$modeManagement=sanitize_all_html_input($_POST['value']);
                $id=sanitize_all_html_input($_POST['id']);
                $idLanguage=sanitize_all_html_input($_POST['idLanguage']);
                
		if($mode=="VIEW_DETAILS")
		{	
                        //if($id==6)
//			{
//				$textEditor=$kAdmin->textEditor($idAdmin,$zero,__TEXT_EDIT_TERMS_FORWARDER__);
//				showTnCData($textEditor,'forwarderTnC');
//			}
			if($modeManagement!="")
			{	
				//$modeManagement='__BENEFITS_OF_SIGN_IN__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement,$idLanguage);	
			}
			
			if(isset($data) && !empty($data))
			{
				viewGeneralTextEdit($data,'__SAVE_TEXT_EDIT_DATA__');
			}
		}
		if($mode=="PREVIEW_DETAILS")
		{	
			if($id==1)
			{	
				$modeManagement='__BENEFITS_OF_SIGN_IN__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
			if($id==2)
			{	
				$modeManagement='__OUR_COMPANY__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
			if($id==3)
			{	
				$modeManagement='__STARTING_NEW_TRADES__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
			if($id==4)
			{	
				$modeManagement='__WORK_FOR_FORWARDER__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
			if($id==5)
			{	
				$modeManagement='__PRIVACY_POLICY__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
			if($id==7)
			{	
				$modeManagement='__FORWARDER_PRIVACY_POLICY__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
			if($id==10)
			{	
				$modeManagement='__EXPLAIN_INTRODUCTION__';
				$data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
                        if($id==23)
			{	
                            $modeManagement='__LOCAL_SEARCH_POPUP_ENGLISH__';
                            $data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
                        if($id==24)
			{	
                            $modeManagement='__LOCAL_SEARCH_POPUP_DANISH__';
                            $data=$kAdmin->gerenalSelectTextEditData($idAdmin,$modeManagement);	
			}
			echo htmlspecialchars_decode($data['szValue']);
		}
		if($mode=="__SAVE_TEXT_EDIT_DATA__")
		{	 
			$kAdmin->gerenalTextEditData($idAdmin,$id,$_POST['content']);
		}
		if($mode=='SAVE_EDIT_DATA')
		{	
			$heading	 = sanitize_all_html_input($_POST['heading']);
			$description = sanitize_specific_tinymce_html_script_input($_POST['description']);
			$kAdmin->saveTextEditData($idAdmin,$heading,$description,$id,__TEXT_EDIT_TERMS_FORWARDER__);
		}
		if($mode=='SAVE_EDIT_CUSTOMER_DATA')
		{	
			$heading	 = sanitize_all_html_input($_POST['heading']);
			$description = sanitize_specific_tinymce_html_script_input($_POST['description']);
			$iLanguage = sanitize_specific_tinymce_html_input($_POST['iLanguage']);
			$kAdmin->saveTextEditData($idAdmin,$heading,$description,$id,__TEXT_EDIT_TERMS_CUSTOMER__,$iLanguage);
		}
		if($mode=='FORWARDER_FAQ_EDIT_DATA')
		{	
			$heading	 = sanitize_all_html_input($_POST['heading']);
			$description = sanitize_specific_tinymce_html_input($_POST['description']);
			
			$kAdmin->saveTextEditData($idAdmin,$heading,$description,$id,__TEXT_EDIT_FAQ_FORWARDER__);
		}
		if($mode=='CUSTOMER_FAQ_EDIT_DATA')
		{	
			$heading	 = sanitize_all_html_input($_POST['heading']);
			$description = sanitize_specific_tinymce_html_input($_POST['description']);
			$iLanguage = sanitize_specific_tinymce_html_input($_POST['iLanguage']);
			$kAdmin->saveTextEditData($idAdmin,$heading,$description,$id,__TEXT_EDIT_FAQ_CUSTOMER__,$iLanguage);
		}
		if($mode=="DELETE_TERMS_FORWARDER")
		{
			$kAdmin->deleteTermsForwarder($idAdmin,$id,__TEXT_EDIT_TERMS_FORWARDER__);
		}
		if($mode=="DELETE_TERMS_CUSTOMER")
		{
			$kAdmin->deleteTermsForwarder($idAdmin,$id,__TEXT_EDIT_TERMS_CUSTOMER__);
		}
		if($mode=="DELETE_TERMS_FAQ")
		{
			$kAdmin->deleteTermsForwarder($idAdmin,$id,__TEXT_EDIT_FAQ_FORWARDER__);
		}
		if($mode=="DELETE_TERMS_FAQ_CUSTOMER_")
		{
			$kAdmin->deleteTermsForwarder($idAdmin,$id,__TEXT_EDIT_FAQ_CUSTOMER__);
		}
		if($mode=="DELETE_EXPLAIN_CONTENT")
		{	
			$kExplain->deleteTermsExplain($id);
		}
		if($mode=="CHANGE_ORDER_FORWARDER")
		{	
			$order	 = sanitize_all_html_input($_POST['mode_value']);
			$kAdmin->moveUpDown($id,$order,__TEXT_EDIT_TERMS_FORWARDER__);
			$textEditor=$kAdmin->textEditor($idAdmin,$zero,__TEXT_EDIT_TERMS_FORWARDER__);
			echo viewTnc($textEditor,'FORWARDER',__TEXT_EDIT_TERMS_FORWARDER__);
		}
		if($mode=="CHANGE_ORDER_CUSTOMER")
		{	
			$order	 = sanitize_all_html_input($_POST['mode_value']);
			$kAdmin->moveUpDown($id,$order,__TEXT_EDIT_TERMS_CUSTOMER__);
			$textEditor=$kAdmin->textEditor($idAdmin,$zero,__TEXT_EDIT_TERMS_CUSTOMER__);
			echo viewTnc($textEditor,'CUSTOMER',__TEXT_EDIT_TERMS_CUSTOMER__);
		}
		if($mode=="CHANGE_ORDER_FAQ")
		{	
			$order	 = sanitize_all_html_input($_POST['mode_value']);
			$kAdmin->moveUpDown($id,$order,__TEXT_EDIT_FAQ_FORWARDER__);
			$textEditor=$kAdmin->textEditor($idAdmin,$zero,__TEXT_EDIT_FAQ_FORWARDER__);
			echo viewTnc($textEditor,'FAQ',__TEXT_EDIT_FAQ_FORWARDER__);
		}
		if($mode=="CHANGE_ORDER_FAQ_CUSTOMER_")
		{	
			$order	 = sanitize_all_html_input($_POST['mode_value']);
			$kAdmin->moveUpDown($id,$order,__TEXT_EDIT_FAQ_CUSTOMER__);
			$textEditor=$kAdmin->textEditor($idAdmin,$zero,__TEXT_EDIT_FAQ_CUSTOMER__);
			echo viewTnc($textEditor,'FAQ_CUSTOMER_',__TEXT_EDIT_FAQ_CUSTOMER__);
		}
		if($mode=="CHANGE_EXPLAIN_ORDER")
		{	
			$id =  sanitize_all_html_input($_POST['id']);
			$order	 = sanitize_all_html_input($_POST['order']);
			$modeValue	 = sanitize_all_html_input($_POST['mode_value']);
			$kExplain->moveUpDownExplain($id,$order);
			$textEditor=$kExplain->text_editor($idAdmin,$modeValue);
			echo viewExplainData($textEditor,$modeValue);
		}
	}
	if(!empty($kAdmin->arErrorMessages))
	{ 
	?>
		<div id="regError" class="errorBox ">
		<div class="header"><?=t($t_base.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
			foreach($kAdmin->arErrorMessages as $key=>$values)
			{
			?><li><?=$values?></li>
			<?php	
			}
		?>
		</ul>
		</div>
		</div>
		<?php  
	}		
}
//print_r($_POST);
		if($mode=="FORWARDERTNC")
		{
			$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_FORWARDER__);
			mainHeadingGetCOntent($textEditor,'SAVE_EDIT_DATA'); 
		 }
		 
		if($mode=="CUSTOMERTNC")
		{
			$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_CUSTOMER__);	
			mainHeadingGetCOntent($textEditor,'SAVE_EDIT_CUSTOMER_DATA'); 
		}
		if($mode=="FAQTNC")
		{
			$textEditor=$kAdmin->textEditorFAQ($idAdmin,$id,__TEXT_EDIT_FAQ_FORWARDER__);
			mainHeadingGetCOntent($textEditor,'FORWARDER_FAQ_EDIT_DATA');
		}
		if($mode=="FAQ_CUSTOMER_TNC")
		{
			$textEditor=$kAdmin->textEditorFAQ($idAdmin,$id,__TEXT_EDIT_FAQ_CUSTOMER__);
			mainHeadingGetCOntent($textEditor,'CUSTOMER_FAQ_EDIT_DATA');
		}
		if($mode == 'ADD_NEW_EXPLAIN_SECTION' )
		{
			$idData  = sanitize_all_html_input($_POST['idData']);
			$textEditor=$kExplain->text_editor($idAdmin,$idData,$id);
			mainExplainContentDetails($textEditor,$idData,'ADD_INTL'); 
		}
		if($mode == "EDIT_EXPLAIN_SUBSECTION")
		{
			$id  = sanitize_all_html_input($_POST['id']);
			$idExplain = (int)$_POST['idExplain'];
			$textEditor=$kExplain->text_editor($idAdmin,$idExplain,$id);
			mainExplainContentDetails($textEditor,$idExplain,'ADD_INTL'); 
		}
		if($_POST['mode']=='PREVIEW_DATA_LEVEL3')
		{
			$_SESSION['LEVELDATA']=$_POST;
			//$_SESSION['LEVELDATA']['szDescription']=$description;
			die;
		}
		if(isset($_POST['flag']))
		{
			$id = (int)$_POST['id'];
			$idExplain = (int)$_POST['flag'];
			$heading	 = sanitize_all_html_input($_POST['heading']);
			$description = urldecode(base64_decode($_POST['description']));
			$kExplain->saveExplainData($_POST['textEditArr'],$description,(int)$id,$idExplain);
			
			if(!empty($kExplain->arErrorMessages))
			{ 
			?>
				<div id="regError" class="errorBox ">
				<div class="header"><?=t($t_base.'fields/please_following');?></div>
				<div id="regErrorList">
				<ul>
				<?php
					foreach($kExplain->arErrorMessages as $key=>$values)
					{
					?><li><?=$values?></li>
					<?php	
					}
				?>
				</ul>
				</div>
				</div>
				<?php  
			}
		}
		