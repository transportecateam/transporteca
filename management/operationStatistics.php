<?php 
define('PAGE_PERMISSION','__OPERATIONS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Bookings - Statistics";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/operations/statistics/";
validateManagement();
$kReport = new cReport();

$operationStatsAry = array();
$operationStatsAry = $kReport->getAllOperationStatisticsSnapshot();

//$operationStatsAry = array();
//$operationStatsAry = $kReport->updateOperationStatisticsScreen(true);
 
$dailySummeryAry = array() ;
$weeklySummeryAry = array();
$monthlySummeryAry = array();
$forwarderBookingSummery = array();
$customerLocationSummeryAry = array();

if(!empty($operationStatsAry['szDailySummery']))
{
    $dailySummeryAry = unserialize($operationStatsAry['szDailySummery']);
} 
if(!empty($operationStatsAry['szWeeklySummery']))
{
    $weeklySummeryAry = unserialize($operationStatsAry['szWeeklySummery']);
} 
if(!empty($operationStatsAry['szMonthlySummery']))
{
    $monthlySummeryAry = unserialize($operationStatsAry['szMonthlySummery']);
}
if(!empty($operationStatsAry['szForwarderBookingSummery']))
{
    $forwarderBookingSummery = unserialize($operationStatsAry['szForwarderBookingSummery']);
}
if(!empty($operationStatsAry['szCustomerLocationSummery']))
{
    $customerLocationSummeryAry = unserialize($operationStatsAry['szCustomerLocationSummery']);
}
if(!empty($operationStatsAry['szTopTradeSummery']))
{
    $topTradeSummeryAry = unserialize($operationStatsAry['szTopTradeSummery']);
}  
$kQuote = new cQuote();
$dtCronjobLastUpdated = $kQuote->getCronjobLastUpdatedDate(2);

if(date('I')==1)
{  
    //Our server's My sql time is 1 hours behind so that's why added 2 hours to sent date 
    $dtCronjobLastUpdated = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($dtCronjobLastUpdated)));  
}  

$todaysDynamicDataAry = array();
$todaysDynamicDataAry = $kReport->getAllCurrentDynamicData(); 
if(!empty($todaysDynamicDataAry))
{  
    $tempDailySummeryAry = $todaysDynamicDataAry['szDailySummery'];
    $tempWeeklySummeryAry = $todaysDynamicDataAry['szWeeklySummery'];
    $tempMonthlySummeryAry = $todaysDynamicDataAry['szMonthlySummery']; 
    
    $key = date('Ymd'); 
    $dailySummeryAry[$key] = $tempDailySummeryAry[$key]; 
    krsort($dailySummeryAry);
}
$operationPageFlag=checkManagementPermission($kAdmin,"__OPERATION_STATISTICS__",2);
?>
<div id="hsbody-2">
    <?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?>  
    <div class="hsbody-2-right">   
    <?php
    if($operationPageFlag){?>
<!--        <div class="cookie-blue-box clearfix" id="cookie-blue-box-container" style="display:block;">
            Data of this screen was last updated on <?php echo date('d M, Y H:i:s',strtotime($dtCronjobLastUpdated)); ?>. Please click on 'Update' on bottom right of the page to see latest data.
       </div>  -->
        <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
            <tr>
                <td width="28%" align="center" valign="top"></td>
                <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/searches')?></strong></td>
                <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
                <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/volume')?></strong></td>
                <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/weight')?></strong></td>
                <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
                <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/users')?></strong></td>
            </tr>
        <?php 
            if(!empty($dailySummeryAry))
            {
                foreach($dailySummeryAry as $key=>$dailySummeryArys)
                { 
                    if(date("d",strtotime($key))==date("d"))
                    {
                        $dailySummeryArys = $tempDailySummeryAry[$key]; 
                        $szDateStr = 'Today';
                    }
                    else
                    {
                        $date = date("l",strtotime($key)); 
                        $szDateStr = $date;
                    } 
        ?>	
            <tr>
                <td align="left" valign="top"><?php echo $szDateStr;?></td>
                <td align="right" valign="top"><?php echo (int)$dailySummeryArys['szSearchBookingCount'];?></td>
                <td align="right" valign="top"><?php echo (int)$dailySummeryArys['szBookingCount'];?></td>
                <td align="right" valign="top"><?php echo number_format($dailySummeryArys['szVolume'],0);?></td>
                <td align="right" valign="top"><?php echo number_format($dailySummeryArys['szWeight'],0);?></td>
                <td align="right" valign="top"><?php echo number_format((float)($dailySummeryArys['szUSD']+$dailySummeryArys['fTotalInsuranceRevenue']));?></td>
                <td align="right" valign="top"><?php echo (int)$dailySummeryArys['iUser'];?></td>
            </tr>
        <?php
            } }
        ?>
            <tr>
                <td align="left" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
            </tr>
            <?php  
                    if(!empty($weeklySummeryAry))
                    {
                        foreach($weeklySummeryAry as $weeklySummeryArys)
                        {
                            if($weeklySummeryArys['szHeading']=='This week')
                            {
                                $weeklySummeryArys = $tempWeeklySummeryAry[0];
                            }
                ?>
                    <tr>
                        <td align="left" valign="top"><?php echo $weeklySummeryArys['szHeading'];?></td>
                        <td align="right" valign="top"><?php echo (int)$weeklySummeryArys['szSearchBookingCount'];?></td>
                        <td align="right" valign="top"><?php echo (int)$weeklySummeryArys['szBookingCount'];?></td>
                        <td align="right" valign="top"><?php echo number_format($weeklySummeryArys['szVolume'],0);?></td>
                        <td align="right" valign="top"><?php echo number_format($weeklySummeryArys['szWeight'],0);?></td>
                        <td align="right" valign="top"><?php echo number_format((float)($weeklySummeryArys['szUSD']+$weeklySummeryArys['fTotalInsuranceRevenue']));?></td>
                        <td align="right" valign="top"><?php echo (int)$weeklySummeryArys['iUser'];?></td>
                    </tr>	
                    <?php
                } }
            ?>
            <tr>
                <td align="left" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
                <td align="right" valign="top">&nbsp;</td>
            </tr>
            <?php 
			if(!empty($monthlySummeryAry))
                        {
                            foreach($monthlySummeryAry as $key=>$monthlySummeryArys)
                            {
                                if(date("F-Y",strtotime($key))==date("F-Y"))
                                {
                                    $szMonthStr = 'This month';
                                    $monthlySummeryArys = $tempMonthlySummeryAry[0];
                                }
                                else
                                {
                                    $date = date("F",strtotime($key)); 
                                    $szMonthStr = $date;
                                }
			?>	
			<tr>
				<td align="left" valign="top"><?php echo $szMonthStr; ?></td>
				<td align="right" valign="top"><?php echo (int)$monthlySummeryArys['szSearchBookingCount'];?></td>
				<td align="right" valign="top"><?php echo (int)$monthlySummeryArys['szBookingCount'];?></td>
				<td align="right" valign="top"><?php echo number_format($monthlySummeryArys['szVolume'],0);?></td>
				<td align="right" valign="top"><?php echo number_format($monthlySummeryArys['szWeight'],0);?></td>
				<td align="right" valign="top"><?php echo number_format((float)($monthlySummeryArys['szUSD']+$monthlySummeryArys['fTotalInsuranceRevenue']));?></td>
				<td align="right" valign="top"><?php echo (int)$monthlySummeryArys['iUser'];?></td>
			</tr>
			<?php
                        } } 
		?>
		</table> 
		<div id="tableForwarder">
		<?php
                    //shows Table 1
                    operationStat1($forwarderBookingSummery,$kBooking,$t_base);
		?>
		</div>
		<div id="tableCoustomerTable">
		<?php 
                    //TAble 2
                    operationStat2($customerLocationSummeryAry,$kBooking,$t_base);
		?>
		</div>
		<div id="tableTraders">	
                <?php
                    //TAble 3
                    operationStat3($topTradeSummeryAry,$kBooking,$t_base);
                ?>
		</div>	
            <div class="map_note">
            <p>* <?=t($t_base.'fields/data_range_used_for_reporting')?></p>
            </div>
            <div class="btn-container">
                <a href="javascript:void(0);" class="button1" style="float:right;" onclick="update_operation_statistics_script();"><span>Update</span></a>
            </div>
    <?php }?>
	</div>
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>