<?php
/**
  *BILLING DETAILS
  */
define('PAGE_PERMISSION','__RAIL_TRANSPORT__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | LCL - Rail Transports";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$kServices = new cServices();
$t_base="management/railTransport/";
validateManagement();

?>
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script>   
<div id="customs_clearance_pop" class="help-pop"></div>
<div id="hsbody-2">
    <?php require_once(__APP_PATH__ ."/layout/contentMaintainanceLeftNav.php"); ?>
    <div class="hsbody-2-right">
        <p style="padding-bottom: 5px;"><?php echo t($t_base.'title/rail_tranport_text');?></p>
        <h4><strong><?php echo t($t_base.'title/lcl_trades_with_rail_icon');?></strong></h4>
        <div style="min-height:300px;" id="rail_transport_main_listing_container_div"> 
            <?php display_lcl_rail_icon_listing($kServices);?>
        </div>
        
        <input type="hidden" name="railTransportIds" id="railTransportIds" value="">
        <input type="hidden" name="idPrimarySortKey" id="idPrimarySortKey_exception" value="">
        <input type="hidden" name="idPrimarySortValue" id="idPrimarySortValue_exception" value="ASC">
        <input type="hidden" name="idOtherSortKey" id="idOtherSortKey" value=""> 
    
    </div>  
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>