<?php
/**
 * Edit User Information
 */
ob_start();
if( !isset( $_SESSION ) )
{
    session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
//$idForwarder=$_SESSION['forwarder_id'];
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/courier_functions.php" );
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
$kCourierServices = new cCourierServices();
$kWebServices = new cWebServices();
$mode=sanitize_all_html_input(trim($_POST['mode']));

if($mode=='ADD_COURIER_PROVIDER_AGREEMENT')
{
    addCourierProviderAgreement($kCourierServices); 
}
else if($mode=='ADD_COURIER_AGREEMENT_DATA')
{
    if($kCourierServices->addCourierAgreementData($_POST['courierProviderAgreementArr']))
    {
        $idCourierProviderAgree=(int)$kCourierServices->idCourierProviderAgree;
        $idCourierProviderAgree=0;
        $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
        $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

        $courierAgreementArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree);	

        $idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
        echo "SUCCESS||||";
        echo courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree)."||||";
        echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider)."||||";
        echo addDisplayCouierAgreementTrades($kCourierServices)."||||";
        echo addDisplayCouierAgreementPricing($kCourierServices,true);
        die;
    }
    else
    {
        echo "ERROR||||";
        addCourierProviderAgreement($kCourierServices);
    }
}
else if($mode=='ADD_EDIT_COURIER_SERVICE_COVERED_DATA')
{
    if($kCourierServices->addCourierAgreementServiceData($_POST['agreeServiceCoverArr']))
    {
        $idCourierProvider=$_POST['agreeServiceCoverArr']['idCourierProvider'];
        $idCourierAgreeProvider=$_POST['agreeServiceCoverArr']['idCourierAgreeProvider'];
        unset($_POST['agreeServiceCoverArr']);
        $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
        $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);

        $data=$courierAgreeArr[0];
        $response=$kCourierServices->isCourierAgreementDataComplete($data);
 
        echo "SUCCESS||||";
        addDisplayCouierAgreementService($kCourierServices,$idCourierProvider);
        echo "||||".$response;
        die();
    }
    else
    {
        echo "ERROR||||";
        addAgreementServiceCoverdFormHtml($kCourierServices,$_POST['agreeServiceCoverArr']['idCourierProvider']);
        die();
    } 
}
else if($mode=='SELECT_AGREE_SERVICE_COVERED')
{
    $idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
    $idCourierProvider=(int)$_POST['idCourierProvider'];
    $id=(int)$_POST['id'];

     $serviceCoveredArr=$kCourierServices->getCourierAgreeServiceData($idCourierAgreeProvider,$id);	

    $_POST['pricingArr']=$serviceCoveredArr[0];
    $_POST['pricingArr']['idCourierServiceOffered']=$id;
    $_POST['pricingArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;

    $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
    echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider,$id)."||||";
    echo addDisplayCouierAgreementPricing($kCourierServices);
}
else if($mode=="EDIT_AGREE_SERVICE_COVERED")
{
	$idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
	$idCourierProvider=(int)$_POST['idCourierProvider'];
	$id=(int)$_POST['id'];
	
	$serviceCoveredArr=$kCourierServices->getCourierAgreeServiceData($idCourierAgreeProvider,$id);
	$_POST['agreeServiceCoverArr']=$serviceCoveredArr[0];
	$_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
	$_POST['agreeServiceCoverArr']['idCourierProvider']=$idCourierProvider;
	addAgreementServiceCoverdFormHtml($kCourierServices,$idCourierProvider);
}
else if($mode=='DELETE_AGREE_SERVICE_COVERED')
{
	$idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
	$idCourierProvider=(int)$_POST['idCourierProvider'];
	$id=(int)$_POST['id'];
	
	$kCourierServices->deleteServiceData($id);
	
	$_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
	$_POST['agreeServiceCoverArr']['idCourierProvider']=$idCourierProvider;
	
	$courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);
        
        $data=$courierAgreeArr[0];
        $response=$kCourierServices->isCourierAgreementDataComplete($data);

        echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider);
        echo "||||".$response."||||";
        echo addDisplayCouierAgreementPricing($kCourierServices,true);
        die();
}
else if($mode=='ADD_EDIT_COURIER_TRADES_OFFERED_DATA')
{
	if($kCourierServices->addCourierAgreementTradeOffered($_POST['agreeTradesOfferArr']))
	{
		$idCourierAgreeProvider=$_POST['agreeTradesOfferArr']['idCourierAgreeProvider'];
		unset($_POST['agreeTradesOfferArr']);
		$_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
                 $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);
        
                $data=$courierAgreeArr[0];
                $response=$kCourierServices->isCourierAgreementDataComplete($data);

                echo "SUCCESS||||";
                addDisplayCouierAgreementTrades($kCourierServices);
                echo "||||".$response;
                die();
	}
	else
	{
		echo "ERROR||||";
		addAgreementTradesOfferedFormHtml($kCourierServices);
		die();
	}

}
else if($mode=='SELECT_AGREE_TRADE_OFFERED')
{
	
	$idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
	$id=(int)$_POST['id'];
	
	$_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
	addDisplayCouierAgreementTrades($kCourierServices,$id);
}
else if($mode=='EDIT_AGREE_TRADE_OFFERED')
{
	$idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
	$id=(int)$_POST['id'];
	
	
	$tardeOfferedArr=$kCourierServices->getCourierAgreeTradeData($idCourierAgreeProvider,$id);
	$_POST['agreeTradesOfferArr']=$tardeOfferedArr[0];
	$_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
	addAgreementTradesOfferedFormHtml($kCourierServices);
}
else if($mode=='DELETE_TRADE_OFFERED')
{
	$idCourierAgreeProvider=(int)$_POST['idCourierAgreement'];
	$id=(int)$_POST['id'];
	
	$kCourierServices->deleteTradeOfferedData($id);
	
	$_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierAgreeProvider;
	
	$courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierAgreeProvider);
        
        $data=$courierAgreeArr[0];
        $response=$kCourierServices->isCourierAgreementDataComplete($data);
        
	echo addDisplayCouierAgreementTrades($kCourierServices);
        echo "||||".$response;
        die();
}
else if($mode=='SHOW_COURIER_AGREE_PRICING_DATA')
{ 
    if($kCourierServices->updateCourierAgreementPricingData($_POST['pricingArr']))
    { 
        $idCourierProviderAgree=$_POST['pricingArr']['idCourierAgreeProvider']; 
        $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree); 
        $data=$courierAgreeArr[0];
        $response=$kCourierServices->isCourierAgreementDataComplete($data);
                
                
                $_POST['pricingArr']['idForwarder']=$courierAgreeArr[0]['idForwarder'];

                //$serviceCoveredArr=$kCourierServices->getCourierAgreeServiceData($idCourierAgreeProvider,$_POST['pricingArr']['idCourierServiceOffered']);	//
                //print_r($serviceCoveredArr);
                //$_POST['pricingArr']=$serviceCoveredArr[0];
                //$_POST['pricingArr']['idCourierServiceOffered']=$id;
		$idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
                
                $courierAgreeArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree);
        
                //$data=$courierAgreeArr[0];
                //$response=$kCourierServices->isCourierAgreementDataComplete($data);
	
		echo "SUCCESS||||";
		echo addDisplayCouierAgreementPricing($kCourierServices)."||||";
                echo $response;
		die;
	}
	else
	{
		echo "ERROR||||";
		addDisplayCouierAgreementPricing($kCourierServices);
		die();
	}
}
else if($mode=='SHOW_AGREEMENT_SERVICE_TRADES_PRICING')
{
    $idCourierProviderAgree=(int)$_POST['id'];
    if((int)$idCourierProviderAgree>0)
    {
        $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
        $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

        $courierAgreementArr=$kCourierServices->getCourierAgreementData($idCourierProviderAgree);	

        //$_POST['pricingArr']=$courierAgreementArr[0];
        //$_POST['pricingArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
        $idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
    }
    echo courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree)."||||";
    echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider)."||||";
    echo addDisplayCouierAgreementTrades($kCourierServices)."||||";
    echo addDisplayCouierAgreementPricing($kCourierServices,true);
    die;
}
else if($mode=='CHECK_COURIER_AGREEMENT_DATA')
{
    $courierProviderAgreementArr = $_POST['courierProviderAgreementArr'];
    if((int)$_SESSION['admin_id']>0)
    {
        $idForwarder = $courierProviderAgreementArr['idForwarder'];
    }
    else
    {
        $idForwarder = $_SESSION['forwarder_id'] ;
    }
    
    if($kCourierServices->isAccountAlreadyExists($courierProviderAgreementArr['szAccountNumber'],$courierProviderAgreementArr['idCourierProvider'],$idForwarder,$courierProviderAgreementArr['iServiceType'],$courierProviderAgreementArr['idEdit']))
    {
         $kCourierServices->szSpecialErrorMesage = "This account number has already been added" ;
        echo "ERROR||||";
        echo addCourierProviderAgreement($kCourierServices,true); 
        die;
    }
    else
    {
        $courierProviderAgreementArr = $_POST['courierProviderAgreementArr'];
        $kWebservices = new cWebServices();
        if($kWebservices->checkCourierApiCredentials($courierProviderAgreementArr))
        {
            echo "SUCCESS||||";
            echo addCourierProviderAgreement($kCourierServices);
        } 
        else
        {
            if(!empty($kWebservices->szSpecialErrorMesage))
            { 
                $kCourierServices->szSpecialErrorMesage = $kWebservices->szSpecialErrorMesage;
            }
            echo "ERROR||||";
            echo addCourierProviderAgreement($kCourierServices,true);	
        } 
    }
} 
else if($mode=='DELETE_AGREEMENT_SERVICE_TRADES_PRICING')
{
    $id=(int)$_POST['id'];
 
    $kCourierServices->deleteCourierProviderAgreementData($id);
    $courierAgreementArr=$kCourierServices->getCourierAgreementData();
    $idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
    $idCourierProviderAgree=$courierAgreementArr[0]['id'];
    $idCourierProviderAgree=0;
    $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
    $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

    //$_POST['pricingArr']=$courierAgreementArr[0];
    //$_POST['pricingArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

    echo courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree)."||||";
    echo addDisplayCouierAgreementService($kCourierServices,$idCourierProvider)."||||";
    echo addDisplayCouierAgreementTrades($kCourierServices)."||||";
    echo addDisplayCouierAgreementPricing($kCourierServices,true);
    die;
}
else if($mode=='EDIT_COURIER_PROVIDER_AGREEMENT')
{
    $id=(int)$_POST['id'];

    $courierAgreementArr=$kCourierServices->getCourierAgreementData($id); 

    $_POST['courierProviderAgreementArr']['idEdit']=$courierAgreementArr[0]['id'];
    $_POST['courierProviderAgreementArr']['idForwarder']=$courierAgreementArr[0]['idForwarder'];
    $_POST['courierProviderAgreementArr']['idCourierProvider']=$courierAgreementArr[0]['idCourierProvider'];
    $_POST['courierProviderAgreementArr']['szAccountNumber']=decrypt($courierAgreementArr[0]['szAccountNumber'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['szUsername']=decrypt($courierAgreementArr[0]['szUsername'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['szPassword']=decrypt($courierAgreementArr[0]['szPassword'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['szMeterNumber']=decrypt($courierAgreementArr[0]['szMeterNumber'],ENCRYPT_KEY);
    $_POST['courierProviderAgreementArr']['iServiceType']= $courierAgreementArr[0]['iServiceType']; 
    $_POST['courierProviderAgreementArr']['szCarrierAccountID']= $courierAgreementArr[0]['szCarrierAccountID'];   
    addCourierProviderAgreement($kCourierServices); 
}
else if($mode=='SHOW_COURIER_PROVIDER_DROPDOWN')
{
	$t_base = "SERVICEOFFERING/";
	$id=(int)$_POST['id'];
	$providerArr=$kCourierServices->getCourierProviderList();
?>
<select name="courierProviderAgreementArr[idCourierProvider]" id="idCourierProvider" onchange="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);activateCheckLoginButton('<?=t($t_base.'fields/add');?>','<?=t($t_base.'fields/check_login');?>');">
		<option value=''><?php echo t($t_base.'fields/select');?></option>
		<?php
		if(!empty($providerArr))
		{
			foreach($providerArr as $providerArrs)
			{
				$showFlag=true;
				//echo $providerArrs['id'];
				$resArr=$courierAgreementArr=$kCourierServices->getCourierAgreementData('',$providerArrs['id'],$id);
				//echo count($resArr);
				if(count($resArr)>0 && $_POST['courierProviderAgreementArr']['idCourierProvider']!=$providerArrs['id'])
				{
					$showFlag=false;
				}
				
				if($showFlag)
				{
			?>
			
				<option value="<?php echo $providerArrs['id']?>" <?php if($_POST['courierProviderAgreementArr']['idCourierProvider']==$providerArrs['id']){?> selected <?php }?>><?php echo $providerArrs['szName']?></option>
			<?php }}
		
		}
		?>
	
	</select>
<?php
}
else if($mode=='CHECKTRADE_ALREADY_EXISTS')
{
    if($kCourierServices->checkTradeAlreadyExists($_POST['agreeTradesOfferArr']))
    {
        echo "SUCCESS||||";
    }
    else
    {
        echo "ERROR||||";
    }
}
?>