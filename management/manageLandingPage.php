<?php 
/**
  *  Explain page List All Explain Pages
  * 
  */
define('PAGE_PERMISSION','__LANDING_PAGES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
$szMetaTitle = "Transporteca | Setup Landing Pages";
require_once (__APP_PATH__ ."/layout/admin_header.php");

validateManagement();

$t_base="management/landingPageData/";
$kExplain = new cExplain();
$iSessionLanguage = false;
if($_SESSION['session_admin_language']>0)
{
	//$iSessionLanguage = $_SESSION['session_admin_language'] ;
}

$landingPageDataAry = array();
$landingPageDataAry = $kExplain->getAllLandingPageData(false,$iSessionLanguage);
$iMaxOrder = $kExplain->getMaxOrderLandingPageData();

?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	
	<div class="hsbody-2-right">
		<div id="manage_landing_page_content">
			<?php echo viewLandingPageContent($t_base,$landingPageDataAry,$iMaxOrder);?>
		</div>
		
	</div><br /><br /> 
		<?php //echo previewLandingPage_admin();?>
	
<?php include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); ?>	