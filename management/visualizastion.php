<?php 
/**
  *Sales Funnel Visualization. 
  */
define('PAGE_PERMISSION','__SALES_FUNNEL_VISUALIZATION__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Sales Visualization";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

$t_base="management/visualization/";

$kReport = new cReport();
validateManagement();

?>

<div id="hsbody-2">
	<? require_once(__APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php"); ?>
	<div class="hsbody-2-right">
		<?	
			showVisualizationTopForm('VISUALIZATION');
		?>
		<div id = "bottom_div" style="display:block;">	</div>
	</div>
</div>

<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	