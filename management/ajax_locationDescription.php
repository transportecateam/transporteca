<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base="management/system/locationDescription/";
$t_base_error="management/Error/";
if(isset($_POST))
{	
	$id		=	(int)sanitize_all_html_input($_POST['id']);
	$mode	= 	(string)sanitize_all_html_input($_POST['mode']);
	$iLanguage	= 	(string)sanitize_all_html_input($_POST['iLanguage']);
	
	if($mode=='SHOW_LOCATION')
	{
		if($id>0)
		{
			$kWHSSearch = new cWHSSearch;
			$countryDetails=$kWHSSearch->LocationDescription($id,$iLanguage);
			$kWHSSearch				=	new cWHSSearch;
			$availableSelections	=	$kWHSSearch->getAvailableSelection(false,$iLanguage); 
			$countryDetails = $countryDetails[0];
			?>
				<td width="41%"><?=$countryDetails['szCountryName']?></td>
				<td width="27%">
				<select id="exactLoc" >
				<?
					foreach($availableSelections as $avali)
					{
						$select = (($avali['id']==$countryDetails['LocationDescriptionExact'])?'selected=selected':'');
						echo "<option value='".$avali['id']."' ".$select." >".$avali['szAvailableSelection']."</option>";
					
					}
				?>
				</select>
				</td>
				<td width="27%">
				<select id="nonExactLoc" >
				<?
					foreach($availableSelections as $avali)
					{
						$select = (($avali['id']==$countryDetails['LocationDescriptionNotExact'])?'selected=selected':'');
						echo "<option value='".$avali['id']."' ".$select." >".$avali['szAvailableSelection']."</option>";
					
					}
				?>
				</select>
				</td width="5%">
				<td onclick = "saveLocationDescription('<?=$countryDetails['id']?>','<?php echo $iLanguage; ?>')" style="cursor: pointer;"><a><?=t($t_base.'fields/save')?></a></td>
			<?
			
		}
	}
	if($mode=='SAVE_LOCATION')
	{	
		if($id>0)
		{
			$exactLoc		= 	(string)sanitize_all_html_input($_POST['exactLoc']);
			$nonExactLoc	= 	(string)sanitize_all_html_input($_POST['nonExactLoc']);
			$iLanguage	= 	(string)sanitize_all_html_input($_POST['iLanguage']);
			
			$kWHSSearch = new cWHSSearch;
			$countryDetails=$kWHSSearch->saveLocationDescription($id,$exactLoc,$nonExactLoc,$iLanguage);
			showLocationDescription($t_base,$iLanguage);
		}
	}
	if($mode == "LOCATION_DESCRIPTION")
	{
		$szSelectionAvailable = 	(string)sanitize_all_html_input($_POST['szSelectionAvailable']);
		$iSelection		= 	(int)sanitize_all_html_input($_POST['iSelection']);
		$iLanguage	= 	(int)sanitize_all_html_input($_POST['iLanguage']);
		$kWHSSearch = new cWHSSearch;
		$countryDetails=$kWHSSearch->saveAvailableLocation($szSelectionAvailable,$iSelection,$iLanguage);
		if(!empty($kWHSSearch->arErrorMessages))
		{
		echo "ERROR|||||";
		?>	
			<div id="regError" class="errorBox" style="display:block;">
			<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kWHSSearch->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
		<?
		}
		else
		{
			echo "SUCCESS|||||";
			showAvailableSelection('management/system/locationDescription/',$iLanguage);
		}
	}
	if($mode =="SELECT_LOCATION_DESCRIPTION")
	{
		$iSelection				= 	(int)sanitize_all_html_input($_POST['iSelection']);
		$kWHSSearch				=	new cWHSSearch;
		$availableSelections	=	$kWHSSearch->getAvailableSelection($iSelection);
		echo $availableSelections[0]['id']."|||||".$availableSelections[0]['szAvailableSelection'];
	}
}

?>