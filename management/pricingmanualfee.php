<?php 
/**
  *  admin--- Search Notification management
  */
define('PAGE_PERMISSION','__MANUAL_FEE__'); 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Pricing Manual Fee";
define('PAGE_PERMISSION','__PRICING_MANUAL_FEE__');
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base = "management/uploadService/";
$kExplain = new cExplain();
$kVatApplication = new cVatApplication();

$defaultManualFeeAry = array();
$defaultManualFeeAry = $kVatApplication->getDefaultManualFee(); 

?>
<div id="hsbody-2">
    <div id="search_noftification_popup" style="display: none;"></div>
    <?php require_once(__APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php"); ?> 
    <div class="hsbody-2-right">
        <h4><strong><?php echo t($t_base.'title/manual_fee'); ?></strong></h4>
        <p><?php echo t($t_base.'messages/define_manual_fee_for_quick_quote'); ?></p>
        <br>
        <div class="clearfix">
            <div id="manual_princing_listing">
               <?php echo manualFeePricingHtml();?>
            </div>
            <div id="default_manual_fee_container">
                <?php echo display_default_manual_fee_form($kVatApplication,$defaultManualFeeAry);?>
            </div>
        </div>
    </div>
</div>    
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); 
?>