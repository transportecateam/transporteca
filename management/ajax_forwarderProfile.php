<?php
/**
 * Edit Forwarder Information
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$t_base = "management/AllProfiles/";
$t_base_error ="management/Error/";
$statusCofirmation = true;
$kForwarderContact = new cForwarderContact();
if(isset($_POST['editUserInfoArr']))
{	
	if(isset($_GET['flag']))
	{	
		$saveFlag = trim(sanitize_all_html_input($_GET['flag']));
		if(is_string($saveFlag) && ($saveFlag =='SAVE_FORWARDER_PROFILE'))
		{	
			$id = (int)sanitize_all_html_input($_POST['idForwarder']);
			$statusCofirmation = $kForwarderContact->setForwarderProfileDetails($_POST['idForwarder']);
		}
	}
	else
	{
		$statusCofirmation = $kForwarderContact->updateForwarderFromAdmin($_POST['editUserInfoArr']);
		if($statusCofirmation == true)
		{	
			echo "SUCCESS|||||";
			$allForwarderContacts = $kAdmin->getAllForwarderContacts($idAdmin);
			adminForwarderProfileDetails($t_base,$allForwarderContacts);
		}
	}
        $szFlag = sanitize_all_html_input($_POST['editUserInfoArr']['szFlag']);
        $szSearchStr = sanitize_all_html_input($_POST['editUserInfoArr']['szSearchStr']);
}

if(isset($_POST['flag']))
{
	$showflag = sanitize_all_html_input($_POST['flag']);
	$idForwarder = (int)sanitize_all_html_input($_POST['idForwarder']);
        $szFlag = sanitize_all_html_input($_POST['szFlag']);
        $szSearchStr = sanitize_all_html_input($_POST['szSearchStr']);
}
if($showflag == 'DELETE_FORWARDER_PROFILE' && $idForwarder>0)
{
	deleteForwarderByAdmin($t_base,$idForwarder,$szFlag,$szSearchStr);
}
if($showflag == 'DELETE_FORWARDER_PROFILE_DONE' && $idForwarder>0)
{
	$deleteProfile = $kForwarderContact->deactivateForwarderAccount($idForwarder);
	if($deleteProfile)
	{
		echo "SUCCESS|||||";
		$allForwarderContacts = $kAdmin->getAllForwarderContacts($idAdmin);
		adminForwarderProfileDetails($t_base,$allForwarderContacts);
	}
}
if(isset($_POST['newflag']))
{
	$newflag = trim(sanitize_all_html_input($_POST['newflag']));
	if($newflag=='CREATE_NEW_FORWARDER_PROFILE')
	{	$kBooking = new cBooking();
		$forwarderIdName=$kBooking->forwarderIdName($idAdmin);
		createNewForwarderProfileByAdmin($t_base,$forwarderIdName);
	}
	if($newflag=='CREATE_NEW_FORWARDER_PROFILE_CONFIRM')
	{
		$newProfile = $kForwarderContact->newForwarderProfile($_POST['createProfile']);
		if($newProfile == true)
		{
			//admin $kForwarderContact->preload($idForwarder);
			$replace_ary['Admin']  		 = $kForwarderContact->getForwarderName($kForwarderContact->idForwarder);
			$replace_ary['szFirstName']  = $kAdmin->szFirstName;
			$replace_ary['szLastName']   = $kAdmin->szLastName;
			$replace_ary['szForwarderCompany'] = __STORE_SUPPORT_NAME__;
			$replace_ary['szNewUserEmail'] = $kForwarderContact->szEmail;
			$kForwarderContact->getMainAdmin($kForwarderContact->idForwarder);
			createEmail(__ADD_NEW_FORWARDER_PROFILE__,$replace_ary,$kForwarderContact->mainAdminEmail, $subject, $reply_to,$kForwarderContact->idAdmin, __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
			echo "SEND|||||";
			$allForwarderContacts = $kAdmin->getAllForwarderContacts($idAdmin);
			adminForwarderProfileDetails($t_base,$allForwarderContacts);
		}
		else if($newProfile == false)
		{	
			echo "ERROR|||||";
			if(!empty($kForwarderContact->arErrorMessages))
			{
			?>	
				<div id="regError" class="errorBox" style="display:block;">
				<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
				<div id="regErrorList">
				<ul>
				<?php
				      foreach($kForwarderContact->arErrorMessages as $key=>$values)
				      {
				      ?>
				      	<li><?=$values?></li>
				      <?php 
				      }
				?>
				</ul>
				</div>
				</div>
			<?php
			}
		}
	}

}
if($showflag == 'SEND_CHANGE_PASSWORD_EMAIL' && $idForwarder>0)
{
	$passwordNew = $kForwarderContact->changeszPasswordForwarder($idForwarder);
	if($passwordNew[0] == true)
	{	
		$kForwarderContact->preload($idForwarder);
		$replace_ary['szFirstName']  = $kForwarderContact->szFirstName;
		$replace_ary['szLastName']   = $kForwarderContact->szLastName;
		$replace_ary['szPassword']   = $passwordNew[1];
		//createEmail(__PASSWORD_CHANGED_BY_ADMINSTRATOR__, $replace_ary, $kForwarderContact->szEmail, $subject, $reply_to);
		createEmail(__FORGOT_PASSWORD__, $replace_ary, $kForwarderContact->szEmail, $subject, $reply_to,$kForwarderContact->id,__STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__);
		echo "SEND";
	}
}

if(($showflag == 'EDIT_FORWARDER_PROFILE' && $idForwarder>0  && (int)$idAdmin>0) || ($statusCofirmation == false) || ($saveFlag == 'SAVE_FORWARDER_PROFILE'))
{	
	if($statusCofirmation == false)
	{
		echo  "ERROR|||||";
	}
	$kConfig = new cConfig();
	$arrBook=BookingReportsArr();
	$bookingReports	= $arrBook[0];
	$allCountriesArr = $kConfig->getAllCountries(true);
	$kForwarderContact->preload($idForwarder);
				switch(!empty($_POST['editUserInfoArr']['idForwarderContactRole'])?$_POST['editUserInfoArr']['idForwarderContactRole']:$kForwarderContact->idForwarderContactRole)
				{
					CASE 1:
					{
					$szContactRole1 ="selected='selected'";
                                        $szContactRoleName='Administrator';
                                        $_POST['editUserInfoArr']['idForwarderContactRole']=1;
					BREAK;
					}
					CASE 3:
					{
					$szContactRole3 ="selected='selected'";
                                        $szContactRoleName='Pricing & Services';
                                        $_POST['editUserInfoArr']['idForwarderContactRole']=3;
					BREAK;
					}
					CASE 5:
					{
					$szContactRole5 ="selected='selected'";
                                        $szContactRoleName='Bookings & Billing';
                                        $_POST['editUserInfoArr']['idForwarderContactRole']=5;
					BREAK;
					}
				}
?>
	<style>
	#updateUserInfo .profile-fields .field-name{width:43%}
	#updateUserInfo .profile-fields .field-container{width:43%}
	#updateUserInfo .profile-fields .field-container select { min-width: 218px;}
	
	</style>
		 <div id="delete-profile"></div>
			
			<?php 
				//forwarderProfileHeader('user',$kForwarderContact->id?$kForwarderContact->id:$idForwarder);
			 ?>
			<div id="popup-bg"></div>
		<div id="popup-container">	
		<div class="compare-popup popup" style="width: 500px;margin-top:0px;">
			<?php 
				if(!empty($_POST['editUserInfoArr']['szPhoneUpdate']))
				{
					$_POST['editUserInfoArr']['szPhone']=urldecode(base64_decode($_POST['editUserInfoArr']['szPhoneUpdate']));
				}
				
				if(!empty($_POST['editUserInfoArr']['szMobileUpdate']))
				{
					$_POST['editUserInfoArr']['szMobile']=urldecode(base64_decode($_POST['editUserInfoArr']['szMobileUpdate']));
				}
				if($statusCofirmation==false)
				{
				if(!empty($kForwarderContact->arErrorMessages))
				{
				?>
					<div id="regError" class="errorBox" style="display:block;">
					<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
					<div id="regErrorList">
					<ul>
					<?php
					      foreach($kForwarderContact->arErrorMessages as $key=>$values)
					      {
					      ?>
					      	<li><?=$values?></li>
					      <?php 
					      }
					?>
					</ul>
					</div>
					</div>
				<?
				}
			
			}?>
				<form name="updateUserInfo" id="updateUserInfo" method="post">
					<h4><strong><?=t($t_base.'title/user_info');?></strong></h4>	
							<label class="profile-fields">
								<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
								<span class="field-container"><input type="text" name="editUserInfoArr[szFirstName]" id="szFirstName" value="<?=!empty($_POST['editUserInfoArr']['szFirstName']) ? $_POST['editUserInfoArr']['szFirstName'] : $kForwarderContact->szFirstName?>"/></span>
								
							</label>
							
							
							<label class="profile-fields">
								<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
								<span class="field-container"><input type="text" name="editUserInfoArr[szLastName]" id="szLastName" value="<?=!empty($_POST['editUserInfoArr']['szLastName']) ? $_POST['editUserInfoArr']['szLastName'] : $kForwarderContact->szLastName ?>" /></span>
								
							</label>
							<label class="profile-fields">
								<span class="field-name"><?=t($t_base.'fields/responsibility');?></span>
								<span class="field-container"><input type="text" name="editUserInfoArr[szResponsibility]" id="szResponsibility" value="<?=!empty($_POST['editUserInfoArr']['szResponsibility']) ? $_POST['editUserInfoArr']['szResponsibility'] : $kForwarderContact->szResponsibility ?>" /></span>
								
							</label>
							<label class="profile-fields">
								<input type="hidden" name="editUserInfoArr[szOldCountry]" id="szOldCountry" value="<?=isset($_POST['editUserInfoArr']['szOldCountry']) ? $_POST['editUserInfoArr']['szOldCountry'] : $kForwarderContact->idCountry ?>" />
								<span class="field-name"><?=t($t_base.'fields/country');?></span>
								<span class="field-container">
									<select size="1" name="editUserInfoArr[szCountry]" id="szCountry">
									<option value=""><?=t($t_base.'fields/select_country');?></option>
										<?php
											if(!empty($allCountriesArr))
											{
												foreach($allCountriesArr as $allCountriesArrs)
												{
													?><option value="<?=$allCountriesArrs['id']?>" <?=(((isset($_POST['editUserInfoArr']['szCountry']))?$_POST['editUserInfoArr']['szCountry']:$kForwarderContact->idCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
													<?php
												}
											}
										?>
									</select>
								</span>
							</label>	
					
							<label class="profile-fields">
								<span class="field-name"><?=t($t_base.'fields/email');?></span>
								<span class="field-container"><input type="text" name="editUserInfoArr[szEmail]" id="szEmail" value="<?=isset($_POST['editUserInfoArr']['szEmail']) ? $_POST['editUserInfoArr']['szEmail'] : $kForwarderContact->szEmail?>"/></span>
								
							</label>
							
					<label class="profile-fields">
						<span class="field-name"><?=t($t_base.'fields/booking_panel');?></span>
					
						
						<?php
						//print_r($kForwarderContact->controlBooking);
							if(isset($_POST['editUserInfoArr']))
							{
								$book=$_POST['editUserInfoArr']['szBooking'];
							}
							else
							{
								$book=$kForwarderContact->controlBooking;
							}
							//echo $book;
						?>
						<span class="field-container">
						<select name="editUserInfoArr[szBooking]" >
						<?php
							foreach($bookingReports as $key=>$value)
							{
							echo '<option value="'.($key+1) .'"';
							if($key+1==$book)
							{
								echo "selected=selected";
							}
							echo '>'.$value.'</option>';
								
							}
						?>
						</select>
						</span>
					</label>			
		
							<label class="profile-fields">
								<span class="field-name"><?=t($t_base.'fields/phone');?></span>
								<span class="field-container"><input type="text" name="editUserInfoArr[szPhone]" id="szPhone" value="<?=isset($_POST['editUserInfoArr']['szPhone']) ? $_POST['editUserInfoArr']['szPhone'] : $kForwarderContact->szPhone ?>" /></span>
								
							</label>
							
							<label class="profile-fields">
								<span class="field-name"><?=t($t_base.'fields/mobile');?> <span class="optional">(<?=t($t_base.'fields/optional');?>)</span></span>
								<span class="field-container"><input type="text" name="editUserInfoArr[szMobile]" id="szMobile" value="<?=isset($_POST['editUserInfoArr']['szMobile']) ? $_POST['editUserInfoArr']['szMobile'] : $kForwarderContact->szMobile ?>" /></span>
							</label>
							<label class="profile-fields">
								<span class="field-name"><strong><?=t($t_base.'fields/password');?></strong></span>
								<span class="field-container"><a id="forgotPassword" onclick="changePasswordForwarderContact('<?=($idForwarder?$idForwarder:$kForwarderContact->id)?>')"><?=t($t_base.'title/send_new_password');?></a></span>
							</label>
							<label class="profile-fields">
								<span class="field-name"><strong><?=t($t_base.'fields/access_rights');?></strong></span>
								<span class="field-container">
                                                                    <?php if($szFlag=='FROM_HEADER'){
                                                                        echo $szContactRoleName;?>
                                                                        <input type="hidden" value="<?php echo $_POST['editUserInfoArr']['idForwarderContactRole'];?>" name="editUserInfoArr[idForwarderContactRole]" id="idForwarderContactRole">
                                                                    <?php }else{?>
									<select size="1" name="editUserInfoArr[idForwarderContactRole]" id="idForwarderContactRole">
										<option value="">Select <?=t($t_base.'fields/access_rights');?></option>
										<option value="1" <?=$szContactRole1?>>Administrator</option>
										<option value="3" <?=$szContactRole3?>>Pricing & Services</option>
										<option value="5" <?=$szContactRole5?>>Bookings & Billing</option>	
									</select>
                                                                        <?php }?>
								</span>
							</label>
							<br/><br/>							
							<p align="center">
                                                                <input type="hidden" name="editUserInfoArr[szFlag]" id="szFlag" value="<?=$szFlag?>">
                                                                <input type="hidden" name="editUserInfoArr[szSearchStr]" id="szSearchStr" value="<?=$szSearchStr?>">
								<input type="hidden" name="editUserInfoArr[szOldEmail]" value="<?=$kForwarderContact->szEmail?>">
								<input type="hidden" id="forwarderId" name="editUserInfoArr[idForwarder]" value="<?=($idForwarder?$idForwarder:$kForwarderContact->id)?>">
                                                                <?php if($szFlag=='FROM_HEADER'){?>
								<a href="javascript:void(0);" onclick="display_remind_pane_add_popup('','','','','FROM_HEADER','<?=$szSearchStr?>');" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
                                                                <?php }else{?>
                                                                <a href="javascript:void(0);" onclick="hideEditForwarderProfile();" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
                                                                <?php }?>
                                                                <a href="javascript:void(0);" onclick="encode_string('szPhone','szPhoneUpdate','szMobile','szMobileUpdate');ConfirmFormSubmit()" class="button1"><span><?=t($t_base.'fields/save');?></span></a>
								<input type="hidden" name="editUserInfoArr[szPhoneUpdate]" id="szPhoneUpdate" value="">
								<input type="hidden" name="editUserInfoArr[szMobileUpdate]" id="szMobileUpdate" value="">			
							</p>							
						</form>	
			</div></div>			
			
	<?php  }

		if(($showflag == 'FORWARDER_PROFILE_ACCESS_HISTORY' && $idForwarder>0 ))
		{
			adminForwarderProfileAccessHistory($t_base,$idForwarder);
		}
	?>		