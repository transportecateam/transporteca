<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$t_base="management/Error/";
$t_base_manage="management/textEditor/";
validateManagement();
if(isset($_POST))
{
	
	$mode=sanitize_all_html_input($_POST['mode']);
	if(isset($_POST['flag']))
	{
		//$flag = sanitize_all_html_input($_POST['flag']);
		//if($flag == 'searchPage')
		//{
			
			if($kAdmin->updateManageVar($_POST['id'],$_POST['szValue']))
			{
				//echo "<script type='text/javascript'>window.location.href = window.location.href;</script>";
				$mode ='';
			}
		//}
		
	}
	?>
	<div id="searchPage">
		<h4><b><?=t($t_base_manage.'fields/cargo_acceptance')?></b><?php if($mode != 'searchPage'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('searchPage');"><?=t($t_base.'fields/editHeading');?></a></span><? } ?></h4>
			<?php
				if($mode == 'searchPage')
				{	
					$manageVariable=$kAdmin->manageVariables(1);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(1);	
				 	manageVAriables($manageVariable);
				}
			?>
		</div>
		<br />			
		<div id="selectSevices">
		<h4><b><?=t($t_base_manage.'fields/select_services')?></b><?php if($mode != 'selectSevices'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('selectSevices');"><?=t($t_base.'fields/editHeading');?></a></span><? } ?></h4>
			<?php
				if($mode == 'selectSevices')
				{	
					$manageVariable=$kAdmin->manageVariables(2);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(2);	
				 	manageVAriables($manageVariable);
				}
			?>
		</div>
		<br />	
		<div id="lclserviceDisplay">
                    <h4><b><?=t($t_base_manage.'fields/lcl_services_displayed')?></b><?php if($mode != 'lclserviceDisplay'){?><span class="edit-links"> <a href="javascript:void(0)" onclick="editManagementVar('lclserviceDisplay');"><?=t($t_base.'fields/editHeading');?></a></span> <?php } ?></h4>
			<?php
			
				if($mode == 'lclserviceDisplay')
				{	
					$manageVariable=$kAdmin->manageVariables(11);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(11);	
			 	   manageVAriables($manageVariable);
				}
				
			?>
		</div><br/>
		<div id="courierserviceDisplay">
		<h4><b><?=t($t_base_manage.'fields/courier_services_displayed')?></b> <?php if($mode != 'courierserviceDisplay'){?><span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('courierserviceDisplay');"><?=t($t_base.'fields/editHeading');?></a></span> <?php } ?></h4>
			<?php
			
				if($mode == 'courierserviceDisplay')
				{	
					$manageVariable=$kAdmin->manageVariables(12);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(12);	
			 	   manageVAriables($manageVariable);
				}
				
			?>
		</div><br/>
		<div id="myBookings">
		<h4><b><?=t($t_base_manage.'fields/my_bookings')?></b><?php if($mode != 'myBookings'){?><span class="edit-links"> <a href="javascript:void(0)" onclick="editManagementVar('myBookings');"><?=t($t_base.'fields/editHeading');?></a></span><?php } ?></h4>
			<?php
				if($mode == 'myBookings')
				{	
					$manageVariable=$kAdmin->manageVariables(3);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(3);	
				 	manageVAriables($manageVariable);
				}
						?>
		</div>
		<br />			
		<div id="bulkUpload">
		<h4><b><?=t($t_base_manage.'fields/bulk_upload')?></b><?php if($mode != 'bulkUpload'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('bulkUpload');"><?=t($t_base.'fields/editHeading');?></a></span><?php } ?></h4>
			<?php
				if($mode == 'bulkUpload')
				{	
					$manageVariable=$kAdmin->manageVariables(4);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(4);	
				 	manageVAriables($manageVariable);
				}
						?>
		</div>
		<br />			
		<div id="zonePricing">
		<h4><b><?=t($t_base_manage.'fields/lcl_pricing')?></b><?php if($mode != 'zonePricing'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('zonePricing');"><?=t($t_base.'fields/editHeading');?></a></span><? } ?></h4>
			<?php
				if($mode == 'zonePricing')
				{	
					$manageVariable=$kAdmin->manageVariables(5);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(5);	
				 	manageVAriables($manageVariable);
				}
						?>
		</div>
		<br />		
		<div id="general">
		<h4><b><?=t($t_base_manage.'fields/general')?></b><?php if($mode != 'general'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('general');"><?=t($t_base.'fields/editHeading');?></a></span><? } ?></h4>
			<?php
				if($mode == 'general')
				{	
                                    $manageVariable=$kAdmin->manageVariables(6);	
                                    manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
                                    $manageVariable=$kAdmin->manageVariables(6);	
                                    manageVAriables($manageVariable);
				}
					?>
		</div>
                <br />
		<div id="courier_pricing">
		<h4><b><?=t($t_base_manage.'fields/courier_pricing')?></b><?php if($mode != 'courier_pricing'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('courier_pricing');"><?=t($t_base.'fields/editHeading');?></a></span><? } ?></h4>
			<?php
				if($mode == 'courier_pricing')
				{	
					$manageVariable=$kAdmin->manageVariables(13);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(13);	
				 	manageVAriables($manageVariable);
				}
						?>
		</div>
		<br />	
		<div id="communication">
		<h4><b><?=t($t_base_manage.'fields/communication')?></b><?php if($mode != 'communication'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('communication');"><?=t($t_base.'fields/editHeading');?></a></span><? } ?></h4>
			<?php
                            if($mode == 'communication')
                            {	
                                $manageVariable=$kAdmin->manageVariables(7);	
                                manageVAriablesEdit($manageVariable,$mode,$kAdmin);

                            }
                            else
                            {
                                $manageVariable=$kAdmin->manageVariables(7);	
                                manageVAriables($manageVariable);
                            }
                        ?>
		</div>
		<br />	
		<!-- <div id="links_to_explain_page">
		<h4><b><?=t($t_base_manage.'fields/links_to_explain_page')?></b><? if($mode != 'links_to_explain_page'){?> <a href="javascript:void(0)" onclick="editManagementVar('links_to_explain_page');"><?=t($t_base.'fields/editHeading');?></a><? } ?></h4>
			<?
				if($mode == 'links_to_explain_page')
				{	
					$manageVariable=$kAdmin->manageVariables(8);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(8);	
				 	manageVAriables($manageVariable);
				}
		?>
		</div>
		<br />-->	
		<div id="links_to_api_key">
		<h4><b><?=t($t_base_manage.'title/api_key')?></b><?php if($mode != 'links_to_api_key'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('links_to_api_key');"><?=t($t_base.'fields/editHeading');?></a></span><? }?></h4>
				<?php
				if($mode == 'links_to_api_key')
				{	
					$manageVariable=$kAdmin->manageVariables(9);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(9);	
				 	manageVAriables($manageVariable);
				}
		?>
		</div>
                <div id="links_to_videos">
		<h4><b><?=t($t_base_manage.'title/videos')?></b><?php if($mode != 'links_to_videos'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('links_to_videos');"><?=t($t_base.'fields/editHeading');?></a></span><? }?></h4>
				<?php
				if($mode == 'links_to_videos')
				{	
					$manageVariable=$kAdmin->manageVariables(14);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(14);	
				 	manageVAriables($manageVariable);
				}
		?>
		</div>
<!--		<div id="trusted_partner">
		<h4><b><?=t($t_base_manage.'title/partner')?></b><? if($mode != 'trusted_partner'){?> <span class="edit-links"><a href="javascript:void(0)" onclick="editManagementVar('trusted_partner');"><?=t($t_base.'fields/editHeading');?></a></span><? }?></h4>
				<?php
				if($mode == 'trusted_partner')
				{	
					$manageVariable=$kAdmin->manageVariables(10);	
					manageVAriablesEdit($manageVariable,$mode,$kAdmin);
				
				}
				else
				{
					$manageVariable=$kAdmin->manageVariables(10);	
				 	manageVAriables($manageVariable);
				}
		?>
		</div>-->
<?php } ?>