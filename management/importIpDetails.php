<?php
/**
 * Importing Forwarders Data
 */
ob_start();
session_start(); 

if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/"));
}
require_once( __APP_PATH__ . "/inc/constants.php");
require_once( __APP_PATH__ . "/inc/functions.php");

ini_set ('max_execution_time',360000); 
ini_set('memory_limit','3048M');
ini_set('error_reporting',E_ALL);
ini_set('display_error','1');

$kUser = new cUser();

$filename = __APP_PATH__."/maxmind/GeoLiteCity-Blocks.csv";
$row = 1;
$forwardersDatas = array();
if (($handle = fopen("$filename", "r")) !== FALSE) 
{
    $main_counter = 0;
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
    {
        $num = count($data); 
        for ($c=0; $c < $num; $c++) 
        { 
            if($c==0)
            {
                $szIndex = 'iStartIpLocation';
            }
            else if($c==1)
            {
                $szIndex = 'iEndIpLocation';
            }
            else if($c==2)
            {
                $szIndex = 'idLocationDetails';
            } 
            if(!empty($szIndex))
            {
                $forwardersDatas[$main_counter][$row][$szIndex]= trim($data[$c]); 
            } 
        } 
        if($row==2000)
        {
            
            $main_counter++; 
        }
        $row++;
    }
    fclose($handle);
     
    if($kUser->addIPlocationBlack($forwardersDatas))
    {
        echo "<h1> ".$kUser->iRowCounter." rows added successfully.<h1>";
    }
} 
?>