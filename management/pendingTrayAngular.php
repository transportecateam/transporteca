<?php 
define('PAGE_PERMISSION','__FILIES__');
ob_start();
if (!isset( $_SESSION )) 
{
    session_start();
} 
$addFile=1;
$szMetaTitle="Transporteca | Tasks - Pending Tray";
define('PAGE_NAME','PENDING_TRAY');

if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php"); 
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
    
$kBooking = new cBooking();  
$kPendingTray = new cPendingTray();
$t_base="management/pendingTray/"; 
validateManagement();   
  
$_SESSION['szCustomerTimeZoneGlobal'] = array();
if(empty($_SESSION['szCustomerTimeZoneGlobal']))
{
    $ret_ary = array();
    $ret_ary = getCountryCodeByIPAddress();
    if(empty($ret_ary))
    {
        $ret_ary['szCountryCode'] = 'DK';
    } 
    $ret_ary['szCountryCode'] = 'DK';
    $szCustomerTimeZone = get_time_zone($ret_ary['szCountryCode'],$ret_ary['szRegion']); 
    $_SESSION['szCustomerTimeZoneGlobal'] = $szCustomerTimeZone ; 
}      

$incompleteTaskAry = array(); 
 
/*
* updating Task status T110: Remind forwarder
*/
$kBooking->updateRemindForwarder(1); 
/*
* updating Task status T120: Remind Customer
*/
$kBooking->updateRemindForwarder(2);
/*
* updating snoozed booking file
*/
$kBooking->updateRemindForwarder(3); 
/*
* updating Task status: 'Solve Task' to booking file
*/
$kBooking->updateSolveTaskStaus();
  
$searchTaskAry = array(); 

if(!empty($_SESSION['sessTransportecaTeamAry']))
{
    $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
}
else
{
    $searchTaskAry['idFileOwnerAry'][0] = 0;
    $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
}
$incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
 
$szOperatorName = $kAdmin->szFirstName." ".$kAdmin->szLastName ; 
$activeAdminAry = array();
$activeAdminAry = $kAdmin->getAllActiveStoreAdmin($_SESSION['admin_id'],true);  

$dtRealDateTime = $kBooking->getRealNow();
//echo "Current Time: ".date('Y-m-d H:i:s',strtotime("+1 HOUR"));
 
$bNotAllocatedFlag = false;
$iNotAllocatedValue = 0;
if(empty($_SESSION['sessTransportecaTeamAry']))
{
    $bNotAllocatedFlag = true;
}
else if(!empty($_SESSION['sessTransportecaTeamAry']) && in_array($iNotAllocatedValue,$_SESSION['sessTransportecaTeamAry'])) 
{
    $bNotAllocatedFlag = true;
} 

$finalPendingTrayListAry = array();
$finalPendingTrayListAry = display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true,false,false,true); 
$jSonString = ''; 
if(!empty($finalPendingTrayListAry))
{  
    $kPendingTray = new cPendingTray();
    $jSonString = $kPendingTray->jsonEncode($finalPendingTrayListAry);  
    //$jSonString = addslashes($jSonString); 
}  
    
?>  
<?php
    if(trim($_REQUEST['idBookingNum'])!='' && (int)$_REQUEST['openFlag']==1)
    { 
        $kBookingNew = new cBooking();
        $szBookingRandomNum = trim($_REQUEST['idBookingNum']);

        $idBooking=$kBookingNew->getBookingIdByRandomNum($szBookingRandomNum);
        $postSearchAry = $kBookingNew->getBookingDetails($idBooking);
        $idBookingFile = $postSearchAry['idFile'];  
        $kBookingNew->loadFile($idBookingFile);
    } 
    ?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<div id="hsbody"> 
    <div id="content_body" style="display:none;">
        
    </div>
    <div id="cancel_booking_popup" style="display:none;"></div>
    <style type="text/css">
        #map_canvas { height: 100% }     
        .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
        .map-content p{font-size:12px !important;}
     </style>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=<?=__GOOGLE_MAP_V3_API_KEY__?>"></script> 
  
<input type="hidden" name="iScrollValue" id="iScrollValue" value="0">
<div id="task_pending_tray_main_container"> 
    <div ng-app="pendingTrayApp">  
        <div id="pending_task_list_main_container">
            <div class="clearfix accordion-container">
                <span class="accordian"><?php echo t($t_base.'title/pending_tray')?>&nbsp;<a href="javascript:void(0);"  id="pending_task_list_open_close_link" <?php if((int)$kBookingNew->idBookingFile>0){?> class="open-icon" onclick="close_open_div('pending_task_main_list_container_div','pending_task_list_open_close_link','open');" <?php }else {?> onclick="close_open_div('pending_task_main_list_container_div','pending_task_list_open_close_link','close');" class="close-icon" <?php }?>></a></span>
                <span class="accordion-btn">
                    <a href="javascript:void(0);" id="button_close_file" style="opacity:0.4"  class="button2"><span><?php echo t($t_base.'title/close')?></span></a>
                    <a href="javascript:void(0);" id="button_copy_file_header" style="opacity:0.4"  class="button1"><span><?php echo t($t_base.'fields/copy')?></span></a>
                    <a href="javascript:void(0);" id="button_quick_quote_file_header" style="opacity:0.4"  class="button1"><span>Quick Quote</span></a>
                    <a href="javascript:void(0);" onclick="display_recent_files_by_owner('DISPLAY_RECENT_FILES_SEEN_BY_OPERATOR','<?php echo __FILE_RECENT_MODIFIED__; ?>');"  class="button1"><span><?php echo t($t_base.'title/recent')?></span></a>
                    <a href="javascript:void(0);" onclick="display_recent_files_by_owner('DISPLAY_SEARCH_PENDING_TASK_POPUP')"  class="button1"><span><?php echo t($t_base.'title/search')?></span></a>
                    <a href="javascript:void(0);" onclick="display_pending_task_overview('ADD_NEW_BOOKING_FILE');" class="button1"><span><?php echo t($t_base.'title/new')?></span></a> 
                    <span class="x" onmouseover="showHideReAssignDrodown('calender_more_popup');" onmouseout="showHideReAssignDrodown('calender_more_popup');">
                        <?php if($kAdmin->szProfileType!='' && $kAdmin->szProfileType!='__FINANCE__') {?>
                        <a href="javascript:void(0);" id="" class="button1"><span>User</span></a>
                        <?php }?>
                        <div style="display:none;" class="calender_more_popup" id="calender_more_popup">  
                            <input type="hidden" name="idFileForClose" id="idFileForClose" value=""> 
                            <input type="hidden" name="idFileForCloseCount" id="idFileForCloseCount" value="0">
                            <input type="hidden" name="idFileForCloseCtr" id="idFileForCloseCtr" value="">
                            <img class="calender_more_popup_arrow" alt="arrow" src="<?php echo __BASE_STORE_IMAGE_URL__?>/calender_more_popup-arrow.png"> 
                            <form action="" name="transporteca_team_form" id="transporteca_team_form" method="post">
                                <input type="hidden" name="transportecaTeamAry[idAdmin][0]" id="idAdmin_0" value="<?php echo $_SESSION['admin_id']; ?>"> 
                                <ul> 
    <!--                        <li><img alt="arrow" src="<?php echo __BASE_STORE_IMAGE_URL__."/yes-tick.jpg"; ?>"> <?php echo $szOperatorName; ?></li>-->
                                    <li class="user-tray-popup-li"><input type="checkbox" checked disabled="disabled" ><?php echo $szOperatorName; ?></li>
                                    <li><input type="checkbox" onclick="display_pending_task_list_byteam();" <?php if($bNotAllocatedFlag){?>checked<?php }?> name="transportecaTeamAry[idAdmin][1]" id="idAdmin_1" value="0"> &nbsp;Not Allocated</li> 
                                    <?php
                                        if(!empty($activeAdminAry))
                                        {
                                            $counter = 2;
                                            $transportecaTeamAry = $_SESSION['sessTransportecaTeamAry'];
                                            foreach($activeAdminAry as $activeAdminArys)
                                            {
                                                $szCheckedString = '';
                                                if(!empty($transportecaTeamAry) && in_array($activeAdminArys['id'],$transportecaTeamAry))
                                                {
                                                    $szCheckedString = "checked='checked' ";
                                                }
                                                ?>
                                                <li><input type="checkbox" <?php echo $szCheckedString; ?> onclick="display_pending_task_list_byteam();" name="transportecaTeamAry[idAdmin][<?php echo $counter; ?>]" id="idAdmin_<?php echo $counter; ?>" value="<?php echo $activeAdminArys['id'];?>"> &nbsp;<?php echo $activeAdminArys['szFirstName']." ".$activeAdminArys['szLastName']; ?></li>
                                                <?php
                                                $counter++;
                                            }
                                        }
                                    ?>
                                </ul> 
                            </form>
                       </div>
                    </span>
                </span>
            </div> 
            <div class="clear-all"></div> 
            <div id="pending_task_listing_container" ng-controller="tasksController"> 
                <table cellpadding="5" cellspacing="0" class="table-scroll-head <?php echo $szExtraClass; ?>" width="100%">
                    <tr>
                        <th style="width:10%;" valign="top"><?=t($t_base.'fields/time')?></th> 
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/task');?></th>
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/customer');?></th>
                        <th style="width:10%;" valign="top"><?=t($t_base.'fields/mode');?></th> 
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/from');?></th>
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/to');?></th>
                        <th style="width:18%;" valign="top"><?=t($t_base.'fields/cargo_text');?></th>
                        <th style="width: 2%">&nbsp;</th>
                    </tr>
                </table> 
                <div id="pending_task_list_container_div" class="pending-tray-scrollable-div"> 
                    <div class="scrolling-div pending-tray-lists">
                        <table cellpadding="2" cellspacing="0" class="format-3" width="100%" id="insuranced_booking_table"> 
                            <tr id="{{pendingTask.szTrId}}" class="pending_tray_listing_row_{{pendingTask.idBookingFile}}" ng-repeat="pendingTask in pendingTasks">
                                <td class="wd-10 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="display_pending_task_details(pendingTask.szTrId)">{{pendingTask.szTaskUpdatedOn}}</td>
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}">{{pendingTask.szTask}}</td>
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}">{{pendingTask.szCustomerName}}</td>
                                <td class="wd-10 pending-tray-td-{{pendingTask.idBookingFile}}">{{pendingTask.szTransportMode}}</td> 
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}">{{pendingTask.szOriginCity}}</td>
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}">{{pendingTask.szDestinationCity}}</td>
                                <td class="wd-18 pending-tray-td-{{pendingTask.idBookingFile}}">{{pendingTask.szVolWeight}}</td>
                                <td class="wd-2"><input type="checkbox" class="pendingTrayList" name="pendingTrayList" id="pendingTrayList_{{pendingTask.idBookingFile}}" value="{{pendingTask.idBookingFile}}" onclick="selectLineForClosingTheFile(pendingTask.idBookingFile,this.id,pendingTask.iCounter)"/></td>
                            </tr>
                        </table>   
                    </div>
                </div>
            </div>
        </div>  
    </div> 
</div> 
</div>	  
<script type="text/javascript"> 
    var openDiv; 
    function toggleDiv(divID) {
        $("#" + divID).fadeToggle(200, function() {
            openDiv = $(this).is(':visible') ? divID : null;
        });
    } 
    $(document).click(function(e) {
         if (!$(e.target).closest('#'+openDiv).length) {
             toggleDiv(openDiv);
         }
    });  
 </script>
<form method="post" id="google_map_hidden_form" action="<?=__BASE_MANAGEMENT_URL__?>/googleMapPendingTray.php" target="google_map_target_1">
    <input type="hidden" name="cfsHiddenAry[szAddressLine]" id="szAddressLine_hidden" value="">  
</form>
<span id="timer_span_id"></span>
<span id="watch_span_id"></span>
<?php  if((int)$_SESSION['idBookingFileLabel']>0){ ?>
    <script type="text/javascript">
        display_pending_task_overview('DISPLAY_PENDING_TASK_COURIER_LABEL_UPLOAD_FORM','<?php echo $_SESSION['idBookingFileLabel'];?>','SEARCH_TASK'); 
    </script>
    <?php  
    unset($_SESSION['idBookingFileLabel']);
    $_SESSION['idBookingFileLabel']=0;
}
//|| __ENVIRONMENT__=='DEV_LIVE'
?> 
<script>  
    var app = angular.module('pendingTrayApp', []);
    
    var myFactory = {}; 
     app.factory('myFactory', ['$http', function($http) { 
        var myFactory = {}; 
        myFactory.getData = function () {
            return $http.get('http://crossorigin.me/http://www.mocky.io/v2/558b11f45f3dcb421106715d');//'my_json_data.php');
        }; 
        return myFactory;

    }]);
    app.controller('tasksController', function($scope, $http) {  
        $scope.pendingTasks = <?php echo $jSonString; ?>;  
         
        var iNumCounter = 0;
        window.setInterval(function(){
            var iPendingTrayAutoRefreshCalled = $("#iPendingTrayAutoRefreshCalled").val();
            iPendingTrayAutoRefreshCalled = parseInt(iPendingTrayAutoRefreshCalled);
            iNumCounter = parseInt(iNumCounter);
            if(iPendingTrayAutoRefreshCalled==1)
            {
                auto_load_pending_tray_listing(); 
                $("#delayed_message_container_div").css('display','none');
                iNumCounter = 0;
            } 
            else if(iNumCounter>1)
            {
                //$("#delayed_message_container_div").css('display','block');
                iNumCounter = 0;
            }
            else
            {
                iNumCounter++;
            }  
        }, 10000); 
        
        function auto_load_pending_tray_listing()
        {
            $("#iPendingTrayAutoRefreshCalled").val(2);
            var file_id = $("#hidden_selected_file_id").val();
            var disp = $("#pending_task_main_list_container_div").css("display");
            $("#iStatusChanged").val('1'); 
            var ajaxUrl = __JS_ONLY_SITE_BASE__ + "/ajax_reloadPendingTray.php?mode=AUTO_LOAD_PENDING_TRAY_TASK_LIST&file_id="+file_id;
            
            $http.post(ajaxUrl,$("#transporteca_team_form").serialize()).success(function(response){ 
                console.log("In sucess...");
                var iStatusChanged = $("#iStatusChanged").val();
                iStatusChanged = parseInt(iStatusChanged); 
                if(iStatusChanged==1)
                {  
                    //printJsonResponse(response); 
                    $scope.pendingTasks = response; 
                    console.log("Printing from Angular...");  

                    var idFileForCloseCtrValue=$("#idFileForCloseCtr").val(); 
                    var idFileForCloseCtrValueArr=idFileForCloseCtrValue.split(";");
                    var len=idFileForCloseCtrValueArr.length;
                    if(parseInt(len)>0)
                    {
                        for(i=0;i<len;++i)
                        {
                            var divValue = "pendingTrayList_"+idFileForCloseCtrValueArr[i];  
                            $("#"+divValue).prop("checked","checked");
                        }
                    } 
                } 
                else
                { 
                    console.log("Already updated by Team selection...");
                    //display_pending_task_list_byteam();
                }
                $("#iPendingTrayAutoRefreshCalled").val(1); 
            }).error(function(error){
                'Unable to load data: ' + error.message;
            }); 
            /*
            $http({
                type: "POST",
                url: ajaxUrl,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $("#transporteca_team_form").serialize(), 
                success: function(response)
                {              
                    console.log("In sucess...");
                    var iStatusChanged = $("#iStatusChanged").val();
                    iStatusChanged = parseInt(iStatusChanged); 
                    if(iStatusChanged==1)
                    {  
                        //printJsonResponse(response); 
                        $scope.pendingTasks = response; 
                        console.log("Printing from Angular...");  

                        var idFileForCloseCtrValue=$("#idFileForCloseCtr").val(); 
                        var idFileForCloseCtrValueArr=idFileForCloseCtrValue.split(";");
                        var len=idFileForCloseCtrValueArr.length;
                        if(parseInt(len)>0)
                        {
                            for(i=0;i<len;++i)
                            {
                                var divValue = "pendingTrayList_"+idFileForCloseCtrValueArr[i];  
                                $("#"+divValue).prop("checked","checked");
                            }
                        } 
                    } 
                    else
                    { 
                        console.log("Already updated by Team selection...");
                        //display_pending_task_list_byteam();
                    }
                    $("#iPendingTrayAutoRefreshCalled").val(1); 
                },
                error: function(response){
                  alert("Error...");  
                }
            }); 
            */
        }
        
        /*
         * function getData()
        {  
            $http.post(ajaxUrl).success(function(data){
                $scope.pendingTasks = data; 
            });   
        }
         */
    }); 
</script>

<script type="text/javascript"> 
    <?php  if($idBookingFile>0){ ?> 
        enableQuickQuotebutton('<?php echo $idBookingFile; ?>'); 
    <?php } ?> 
   
   <?php  //if(__ENVIRONMENT__ == "LIVE"){ ?> 
        /*
        * FOllowing function will called automatically to load pending tray task list in every 10 mins
        */ 
        var d = new Date(); // for now 
        var iSecond = d.getSeconds();
        var iMinute = d.getMinutes();
        var iHour = d.getHours();
        var szDateTime = '';
        var Interval = 0;
        window.setInterval(function(){
            if(iSecond==60)
            {
                iMinute++;
                iSecond = 0;
            }
            if(iMinute==60)
            {
                iHour++;
                iMinute = 0;
            } 
            if(iHour==24)
            {
                iHour = 0;
            }
            szDateTime = formatDateTime(iHour) + ":" + formatDateTime(iMinute) + ":" + formatDateTime(iSecond);
            Interval++;
            iSecond++;
            
            //$("#timer_span_id").html("Stop Watch: "+Interval);
            //$("#watch_span_id").html("<br>"+szDateTime);
        }, 1000);
         
        function formatDateTime(iNumber)
        {
            if(iNumber<10)
            {
                return "0"+iNumber;
            }
            else
            {
                return iNumber;
            }
        }
   <?php //} ?>
</script>
<input id="iPendingTrayAutoRefreshCalled" name="iPendingTrayAutoRefreshCalled" value="1" type="hidden">
<input id="iStatusChanged" name="iStatusChanged" value="0" type="hidden">
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>