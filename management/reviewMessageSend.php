<?php 
/**
  *  Send Bulk Email to forwarders
  */
define('PAGE_PERMISSION','__MESSAGE_SEND_MESSAGES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Send Messages - Review Sent Messages ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

$kForwarder	= new 	cForwarder();

$allSendMessageArr=$kAdmin->getAllMessageSendCount();
$total=count($allSendMessageArr);
?>
	<div id="ajaxLogin"></div>
	<div id="hsbody-2">
	<? require_once(__APP_PATH__ ."/layout/admin_messages_left_nav.php"); ?>
		
	<div class="hsbody-2-right">
		<div id="content_body"></div>
		<div id="content">
		<?=sendMessageLists($total)?>
		</div>
		<div id="show_message_preview"></div>		
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>