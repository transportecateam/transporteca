<?php 
/**
  *  Send Message to customer
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");

$t_base="management/AdminMessages/";
$idMessage=$_REQUEST['idMessage'];
$sendflag= $_REQUEST['sendflag'];
if((int)$idMessage>0)
{
	if($sendflag=='QueuePrivew')
	{
		$messageDetailArr=$kAdmin->getQueueMessage($idMessage);
		?>
		<div>
		<p><?=t($t_base.'fields/subject')?>: <?=$messageDetailArr[0]['szSubject']?></p>
		<?
			 ob_start();  
    		 require_once(__APP_PATH_ROOT__.'/layout/email_header.php');
    		 ?>
			
			<p><?=$messageDetailArr[0]['szMessage'];?></p>
			<?
    		 include(__APP_PATH_ROOT__.'/layout/email_footer.php');
		?>
		</div>
	<?
	}
	else if($sendflag=='DeleteMsg')
	{?>
		<div id="send_confirm_popup">
		<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<h5><?=t($t_base.'fields/delete_queued_message');?></h5>
			<p><?=t($t_base.'fields/delete_msg_text');?></p>
			<br>
			 <p align="center">
			  <input type="hidden" name="idMessage" id="idMessage" value="<?=$idMessage?>">
			 	<a href="javascript:void(0);" class="button1" id="yes" onclick="confirm_delete_queue_msg('<?=$idMessage?>');"><span><?=t($t_base.'fields/yes');?></span></a>
				<a href="javascript:void(0);" class="button2"  id="no" onclick="cancelPreviewMsg('send_confirm_popup');"><span><?=t($t_base.'fields/no');?></span></a>
			</p>
			</div>
		</div>
	</div>
	
	<?			
	}
	else if($sendflag=='ConfirmDeleteMsg')
	{
		$kAdmin->deletQueuemessage($idMessage);
		sendMessageQueueLists();
	}
	else
	{
	
		$messageDetailArr=$kAdmin->getAllMessageSend($idMessage);
		if($sendflag=='Privew')
		{
			?>
				<div>
					<p><?=t($t_base.'fields/to')?>: <?=$messageDetailArr[0]['szEmail']?></p>
					<p><?=t($t_base.'fields/subject')?>: <?=$messageDetailArr[0]['szSubject']?></p>
					<p><?=$messageDetailArr[0]['szBody']?></p>
				</div>
			<?		
		}
		else if($sendflag=='Resend' || $sendflag=='Confirm')
		{
			if($sendflag=='Confirm')
			{
				$data['szEmail']=$_REQUEST['szEmail'];
				$data['szBody']=$messageDetailArr[0]['szBody'];
				$data['szSubject']=$messageDetailArr[0]['szSubject'];
				$data['idUser']=$messageDetailArr[0]['idUser'];
				$data['iUserType']=$messageDetailArr[0]['iUserType'];
				$kAdmin->resendMessages($data);
				?>
				<script>
					$(location).attr('href','<?=__MANAGEMENT_MESSAGE_REVIEW_URL__?>');
				</script>
				<?
			}
		
		?>
			<div id="popup-bg"></div>
				<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
					<?
						if(!empty($kAdmin->arErrorMessages)){
						$t_base_error = "Error";
						?>
						<div id="regError" class="errorBox ">
						<div class="header"><?=t($t_base_error.'/please_following');?></div>
						<div id="regErrorList">
						<ul>
						<?php
							foreach($kAdmin->arErrorMessages as $key=>$values)
							{
							?><li><?=$values?></li>
							<?php	
							}
						?>
						</ul>
						</div>
						</div>
						<? }?>
					<h5><?=t($t_base.'fields/resend');?></h5>
					<p><?=t($t_base.'fields/which_address');?> ?</p>
					<p><input type="text" id="szEmail" name="szEmail" value="<?=$messageDetailArr[0]['szEmail']?>" style="width:277px;"></p>
					 <br/>
					 <p align="center">
					 <input type="hidden" id="idMessage" name="idMessage" value="<?=$idMessage?>">	     		
					 <a href="javascript:void(0);" class="button1"  onclick="resend_message_to_user();" id="send_button"><span><?=t($t_base.'fields/yes');?></span></a>
					<a href="javascript:void(0);" class="button2"  onclick="cancelPreviewMsg('content_body');" id="cancel_button"><span><?=t($t_base.'fields/no');?></span></a>
					</p>
				</div>
				</div>
		<?php		
		}
	}
}
else
{
	if($sendflag=='MESSAGES_CONTENT_NEXT_PAGE')
	{
		$allSendMessageArr=$kAdmin->getAllMessageSendCount();
		$total=count($allSendMessageArr);
		$page=$_REQUEST['page'];
		sendMessageLists($total,$page);
	}
	else if($sendflag=='SYSTEM_MESSAGES_CONTENT_NEXT_PAGE')
	{
		$page=$_REQUEST['page'];
		systemMessageLists($page);
	}
	else if($sendflag=='SystemMessagePrivew')
	{
		$idEmail=$_REQUEST['idEmail'];
		$emailDetail=$kAdmin->getEmailLogDetailById($idEmail);
		//print_r($emailDetail[0]);
		?>
		<table>
                    <tr>
                        <td><?=t($t_base.'fields/to')?>: <?=$emailDetail[0]['szToAddress']?></td>
                    </tr>
                    <tr>
                        <td><?=t($t_base.'fields/subject')?>: <?php echo utf8_decode($emailDetail[0]['szEmailSubject']); ?></td>
                    </tr>
                    <tr>
                        <td><?php if($emailDetail[0]['iAlreadyUtf8Encoded']=='1'){ echo $emailDetail[0]['szEmailBody']; }else{ echo utf8_decode($emailDetail[0]['szEmailBody']);} ?></td>
                    </tr>
		</table>	
		<?php
	}
	else if($sendflag=='ResendSystemMessage' || $sendflag=='ConfirmSystem')
	{
		$idEmail=$_REQUEST['idEmail'];
                $szFromPage = $_REQUEST['from_page'];
                $idBookingFile = $_REQUEST['booking_file'] ; 
		$emailDetail=$kAdmin->getEmailLogDetailById($idEmail);
		$kBooking = new cBooking();
                if($sendflag=='ConfirmSystem')
                {
                    $to=$_REQUEST['szEmail'];
                    if($emailDetail[0]['iAlreadyUtf8Encoded']=='1'){
                    $message = $emailDetail[0]['szEmailBody'];
                	}else{
                		$message = utf8_decode($emailDetail[0]['szEmailBody']);
                	}
                    $subject = utf8_decode($emailDetail[0]['szEmailSubject']);
                    $idUser=$emailDetail[0]['idUser'];
                    $iUserType=$emailDetail[0]['iMode']; 
                    $iSuccess = $emailDetail[0]['iSuccess'];
                    $idBooking = 0;
                    
                    if($idBookingFile>0)
                    {
                        $kBooking->loadFile($idBookingFile);
                        $idBooking = $kBooking->idBooking;
                        
                        if($iSuccess==4)
                        {
                            /*
                            * If Mgt uses 'Resend' link for bounced email then we close task for the file
                            */
                            
                            $dtResponseTime = $kBooking->getRealNow(); 
                            $fileLogsAry = array(); 
                            $fileLogsAry['szTransportecaTask'] = ""; 
                            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;   

                            if(!empty($fileLogsAry))
                            {
                                foreach($fileLogsAry as $key=>$value)
                                {
                                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                                }
                            }  
                            $file_log_query = rtrim($file_log_query,",");   
                            $kBooking->updateBookingFileDetails($file_log_query,$idBookingFile); 
                        } 
                    }
                    
                    //echo $to."test";
                    $kWHSSearch = new cWHSSearch();
                    $fromEmail=$kWHSSearch->getManageMentVariableByDescription('__SEND_UPDATES_CUSTOMER_EMAIL__'); 
                    
                    /*
                    * building block for send Transporteca E-mail
                    */
                    $kSendEmail = new cSendEmail();
                    $message = $kSendEmail->removeReferenceText($message);
                    $mailBasicDetailsAry = array();
                    $mailBasicDetailsAry['szEmailTo'] = $to;
                    $mailBasicDetailsAry['szEmailFrom'] = $fromEmail; 
                    $mailBasicDetailsAry['szEmailSubject'] = $subject;
                    $mailBasicDetailsAry['szEmailMessage'] = $message;
                    $mailBasicDetailsAry['szReplyTo'] = $fromEmail;
                    $mailBasicDetailsAry['idUser'] = $idUser; 
                    $mailBasicDetailsAry['idBooking'] = $idBooking; 
                    $mailBasicDetailsAry['szAttachmentFileName'] = unserialize($emailDetail[0]['szAttachmentFileName']);
                    $mailBasicDetailsAry['szAttachmentFiles'] = unserialize($emailDetail[0]['szAttachmentFiles']);
                   
                    $kSendEmail = new cSendEmail();
                    $kSendEmail->sendEmail($mailBasicDetailsAry);
                    
                    //sendEmail($to,$fromEmail,$subject,$message,$fromEmail, $idUser,false,false,$idBooking,'',$iUserType);
                    
                    if($szFromPage=='PENDING_TRAY_LOG' || $szFromPage=='PENDING_TRAY_CUSTOMER_LOG')
                    {
                        echo "SUCCESS||||";
                    }
                    else
                    {
                        echo "REDIRECT||||".__MANAGEMENT_MESSAGE_REVIEW_EMAIL_LOG_URL__."||||";  
                    } 
                    die;
                } 
		?>
                <div id="popup-bg"></div>
                    <div id="popup-container">
                        <div class="popup signin-popup signin-popup-verification">
					<?php
						if(!empty($kAdmin->arErrorMessages)){
						$t_base_error = "Error";
						?>
						<div id="regError" class="errorBox ">
						<div class="header"><?=t($t_base_error.'/please_following');?></div>
						<div id="regErrorList">
                                                    <ul>
                                                        <?php
                                                            foreach($kAdmin->arErrorMessages as $key=>$values)
                                                            {
                                                            ?><li><?=$values?></li>
                                                            <?php	
                                                            }
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
						<? }?>
					<h5><?=t($t_base.'fields/resend');?></h5>
					<p><?=t($t_base.'fields/which_address');?> ?</p>
					<p><input type="text" id="szToAddress" name="szToAddress" value="<?=$_REQUEST['szEmail']?$_REQUEST['szEmail']:$emailDetail[0]['szToAddress']?>" style="width:277px;"></p>
					 <br/>
					 <p align="center">
					 <input type="hidden" id="idEmail" name="idEmail" value="<?=$idEmail?>">
                                         <input type="hidden" id="from_page" name="from_page" value="<?php echo $szFromPage; ?>"> 
                                         <input type="hidden" id="booking_file_id_popup" name="booking_file_id_popup" value="<?php echo $idBookingFile; ?>"> 
                                        
					 <a href="javascript:void(0);" class="button1"  onclick="resend_system_message_to_user();" id="send_button"><span><?=t($t_base.'fields/yes');?></span></a>
					<a href="javascript:void(0);" class="button2"  onclick="cancelPreviewMsg('content_body');" id="cancel_button"><span><?=t($t_base.'fields/no');?></span></a>
					</p>
				</div>
				</div>
		<?	
	}
}
?>