<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base="management/AllProfiles/";

$flag=sanitize_all_html_input($_REQUEST['flag']);
$idUser=(int)$_REQUEST['idUser'];
$divId = (string)$_REQUEST['idDiv'];
$kUser= new cUser();
$successFlag=false;
if($flag == 'changePassword')
{
	$EmailCustomer=$kAdmin->findCustomerEmail($idUser);
	if($EmailCustomer!='' || $EmailCustomer!=NULL )
	{
		if($kUser->forgotPassword($EmailCustomer))
		{
			return true;
		}
	}
	
}
if($flag =='editPassword')
{

?>
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
					<h4><b><?=t($t_base.'title/new_password');?></b></h4>
					<br/>
					<p><?=t($t_base.'title/do_you_want_to_send_new_password');?><br /><br /> 
					</p>
					
					<p align="center">
					<a href="javascript:void(0)" class="button1" onclick="send_new_password_customer_confirm('<?=$idUser?>');" tabindex="6"><span><?=t($t_base.'fields/yes')?></span></a> 
					<a href="javascript:void(0)" class="button1" tabindex="7" onclick="cancelEditMAnageVar(0)"><span><?=t($t_base.'fields/no')?></span></a></p>
					
					</form>
				</div>
		</div>
<?php

}
if($flag=='setting')
{
    if(!empty($_POST['editUserInfoArr']))
    {
        //print_r($_POST);
        if($kUser->updateUserInfoByAdmin($_POST['editUserInfoArr'],$idUser))
        {	
            $successFlag=true;
            $allCustomerArr=$kAdmin->getAllCustomers($sortby,$sortOrder);

            adminProfileDetailsManagement($t_base,$allCustomerArr);
            exit();
        }
        if(!empty($_POST['editUserInfoArr']['szPhoneNumber']))
        {
            $szPhoneNumber=urlencode($_POST['editUserInfoArr']['szPhoneNumber']);
            $str = substr($szPhoneNumber, 0, 1); 
            //echo $str;
            $substr=substr($szPhoneNumber,1);
            $phone=str_replace("+"," ",$substr);
            if(!empty($phone))
            {
                $_POST['editUserInfoArr']['szPhoneNumber']=$str."".urldecode($phone);
            }
            else
            {
                $_POST['editUserInfoArr']['szPhoneNumber']=urldecode($_POST['editUserInfoArr']['szPhoneNumber']);
            }
        }
    }
?>
	<?php
	if(!empty($kUser->arErrorMessages))
        {
            $error=$kUser->arErrorMessages;
	}
	if(!$successFlag)
	{
            echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Edit Customer - Settings');</script>";
            echo userProfilePage($t_base,$idUser,$error,$divId);		
	}
	?>
<?php }
else if($flag=='shipperConsign')
{
	$kRegisterShipCon = new cRegisterShipCon();
?>
	<?php
	echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Edit Customer - Registered Shippers and Consignees');</script>";
	userShipperConsignees($t_base,$idUser,$divId);
	shipperConsignEditForm($t_base);
	?>
<?php }else if($flag=='multiUser'){
	echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Edit Customer - Multi-user Access');</script>";
	?>
		<?=adminCustomerMultiUserAccess($t_base,$idUser,$divId);?>
		
<?php }else if($flag=='accountHis'){
	echo "<script type = 'text/javascript'>$('#metatitle').html('Transporteca | Edit Customer - History');</script>";
?>
		<?=adminAccountHistoryDetails($t_base,$idUser,$divId);?>
<?php }else if($flag=='deleteAcc'){
	$kUser->getUserDetails($idUser);
	if($_REQUEST['actionFlag']=='delete')
	{
		if($kUser->deleteUserByAdmin($idUser,$_REQUEST['actionFlag']))
		{
			?>
			<script type="text/javascript">
				showDifferentProfile('cust');
			</script>
			<?
			exit();
		}
	}
	?>
		<div id="delete_reactivate">
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
					<form name="deleteUser" id="deleteUser" method="post">
					<h4><b><?=t($t_base.'messages/delete_account');?></b></h4>
					<br/>
					<p><?=t($t_base.'messages/inactive_the_account_1');?> <?=$kUser->szEmail?>.<br /><br /> <?=t($t_base.'messages/inactive_the_account_2');?></p>
					<br/>
					<p align="center"><a href="javascript:void(0)" class="button1" onclick="confirm_delete_user('<?=$idUser?>','delete');" tabindex="6"><span><?=t($t_base.'fields/yes')?></span></a> 
					<a href="javascript:void(0)" class="button1" tabindex="7" onclick="cancel_remove_popup_admin('deleteAcc')"><span><?=t($t_base.'fields/no')?></span></a></p>
						<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
						<input type="hidden" name="flag" id="flag" value="<?=$flag?>" />
					</form>
				</div>
		</div>
		</div>
	<?php }else if($flag=='sendCustomerPassword'){
	
		if($_REQUEST['confirm']=='confirm')
		{
			$kAdmin->sendCustomerPassword($idUser);
		}
	
	?>
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
					<form name="sendPassword" id="sendPassword" method="post">
					<h5><?=t($t_base.'messages/new_password');?></h5>
					<br/>
					<p><?=t($t_base.'messages/send_new_password');?></p>
					<br/>
					<p align="center"><a href="javascript:void(0)" class="button1" onclick="send_change_password('<?=$idUser?>');" tabindex="6"><span><?=t($t_base.'fields/yes')?></span></a> 
					<a href="javascript:void(0)" class="button1" tabindex="7" onclick="cancel_sending_password()"><span><?=t($t_base.'fields/no')?></span></a></p>
						<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
						<input type="hidden" name="flag" id="flag" value="<?=$flag?>" />
					</form>
				</div>
			</div>
	<?php } else if($flag=='deleteShipperCongign'){
			if($_REQUEST['remove']=='1')
			{
				remove_shipper_consignees($t_base,$idUser);
					
			}
			else if(!empty($_POST['removeArr']))
			{
				$kRegisterShipCon = new cRegisterShipCon();
				if($kRegisterShipCon->removeRegShipperConsigneess($_POST['removeArr']))
				{?>
					<?=userShipperConsignees($t_base,$idUser);?>
					<?=shipperConsignEditForm($t_base);?>	
				<?php
				}
			}
		}
		else if($flag=='editShipperCongign')
		{
			$kRegisterShipCon = new cRegisterShipCon();
			if(!empty($_POST['registerShipperCongArr']))
			{
				//print_r($_POST['registerShipperCongArr']);
				if($kRegisterShipCon->updateRegisterShipperConsigness($_POST['registerShipperCongArr']))
				{
					$idRegisterShipperConsignees=$_POST['registerShipperCongArr']['idShipperConsigness'];
					$successUpdatedflag=true;
					$successflag=true;
					?>
					<script type="text/javascript">
					showDifferentProfile('cust');
					</script>
					<?php
					exit();
				}
			}
			
				//print_r($_POST['registerShipperCongArr']);
			if(!empty($_POST['registerShipperCongArr']['szPhoneNumberUpdate']))
			{
				$_POST['registerShipperCongArr']['szPhoneNumber']=urldecode(base64_decode($_POST['registerShipperCongArr']['szPhoneNumberUpdate']));
			}
			if(!empty($kRegisterShipCon->arErrorMessages)){
			$error=$kRegisterShipCon->arErrorMessages;
			}
		?>	
		<?=shipperConsignEditForm($t_base,$idUser,$_REQUEST['idShipperConsignees'],$error);?>	
		<?php }else if($flag=='deleteMultiAccess'){
				if($_POST['confirmFlag']=='Confirm')
				{
					if($kUser->removeUserByAdmin($_POST['idRemoveUser'],$_POST['accessflag']))
					{?>
						<script type="text/javascript">	
							showEditCustomerPages('multiUser','<?=$idUser?>');	
						</script>
					<?php
					exit();
					}
				}
		?>
			<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
					<form name="removeMultiAccess" id="removeMultiAccess" method="post">
					<h4><b><?=t($t_base.'messages/remove_from_group')?></b></h4>
					<br/>
					<p><?=t($t_base.'messages/are_you_sure_current_group')?></p>
					<br/>
					<p align="center"><a href="javascript:void(0)" class="button1" onclick="confirm_remove_user('<?=$idUser?>');" tabindex="6"><span><?=t($t_base.'fields/yes')?></span></a> 
					<a href="javascript:void(0)" class="button1" tabindex="7" onclick="cancel_remove_popup_admin('multiUser')"><span><?=t($t_base.'fields/no')?></span></a></p>
						<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
						<input type="hidden" name="confirmFlag" id="confirmFlag" value="Confirm" />
						<input type="hidden" name="idRemoveUser" id="idRemoveUser" value="<?=$_REQUEST['idRemoveUser']?>" />
						<input type="hidden" name="accessflag" id="accessflag" value="<?=$_REQUEST['accessflag']?>" />
						<input type="hidden" name="flag" id="flag" value="<?=$flag?>" />
					</form>
				</div>
			</div>
		<?php }else if($flag=='addMultiAccess'){
					if(!empty($_REQUEST['multiUserArr']))
					{
						$userDetailArr=$kUser->checkEmailAddressByAdmin($_REQUEST['multiUserArr']);
						$kUser->getUserDetails($idUser);
						if(!empty($userDetailArr))
						{
							$showpop=true;
							$successflag=true;	
						}
						else
						{
							$showpop=true;
						}
					}
					$flag = 0;
					//print_r($_REQUEST['mappedArr']);
					if(!empty($_REQUEST['mappedArr']))
					{
						if($kUser->updateInvitationStatus($_REQUEST['mappedArr']))
						{
							$flag=1;	
							?>
							<script type="text/javascript">	
							showEditCustomerPages('multiUser','<?=$idUser?>');	
							</script>
							<?
						}
					}if($showpop){
					?>
					<script type="text/javascript">
					//disableForm('hsbody-2');
					</script>
					<div id="mapped_user_id">
					<div id="popup-bg"></div>
					<div id="popup-container">
					<div class="popup remove-shippers-popup">
					<form name="linkedUser" id="linkedUser" method="post">
						<?php
							if(!empty($kUser->arErrorMessages)){
							$t_base_error = "Error";
							?>
							<div id="regError" class="errorBox ">
							<div class="header"><?=t($t_base_error.'/please_following');?></div>
							<div id="regErrorList">
							<ul>
							<?php
								foreach($kUser->arErrorMessages as $key=>$values)
								{
								?><li><?=$values?></li>
								<?php	
								}
							?>
							</ul>
							</div>
							</div>
							<?php }
							
							if($kUser->iAlreadyMapperUserError){
							?>
							<div id="AlreadyMapperUserError">
							<p><strong><?=t($t_base.'messages/already_exist_user_error_message_header');?></strong></p><br>
							<p><?=t($t_base.'messages/already_exist_user_error_message');?></p>
							</div>
							<?php }
							
							if($successflag){
							?>
							<h5><?=t($t_base.'messages/add_linked_user');?></h5>
							<p><?=t($t_base.'messages/do_you_wish_to_link');?> <?=ucwords($userDetailArr[0]['szFirstName'])?> <?=ucwords($userDetailArr[0]['szLastName'])?> <?=t($t_base.'messages/to');?> <?=ucwords($kUser->szFirstName)?> <?=ucwords($kUser->szLastName)?>?</p>
							<?php }?>
							<br />
						<p align="center">
						<?php
						if($successflag){
							?>
							<a href="javascript:void(0)" class="button1" onclick="confirm_mapped_user_by_admin();"><span><?=t($t_base.'fields/confirm');?></span></a>
							<?php }?>
							<input type="hidden" name="flag" id="flag" value="addMultiAccess" />
							<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
							<a href="javascript:void(0)" class="button1" onclick="cancel_remove_user_popup('mapped_user_id','hsbody-2');"><span><?=t($t_base.'fields/cancel');?></span></a>
							<input type="hidden" name="mappedArr[idUser]" id="idUser" value="<?=$idUser?>">
							<input type="hidden" name="mappedArr[IdInvitedUser]" id="IdInvitedUser" value="<?=$userDetailArr[0]['id']?>">
						</p>
						</form>
					</div>
					</div>
					</div>
					<?php	
					}
				?>
		<?php }else if($flag=='reActive'){
		$exitFlag=false;
		$kUser->getUserDetails($idUser);
		if($kUser->isUserEmailExist($kUser->szEmail))
		{
			$messages=t($t_base.'messages/another_active_account_exists_with_same_email');
			$exitFlag=true;
		}
		else
		{
			$messages=t($t_base.'messages/reactive_the_account_1')." ".$kUser->szEmail.".<br/><br/> ". t($t_base.'messages/reactive_the_account_2');
		}
		if($_REQUEST['actionFlag']=='reactive')
		{
			if($kUser->deleteUserByAdmin($idUser,$_REQUEST['actionFlag']))
			{
				?>
				<script type="text/javascript">
					showDifferentProfile('cust');
				</script>
				<?
				exit();
			}
		}
		?>
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
					<form name="deleteUser" id="deleteUser" method="post" >
					<h4><b><?=t($t_base.'messages/reactive_account')?></b></h4>
					<br />
					<p><?=$messages?></p>
					<br/>
					<? if($exitFlag){?>
					<p align="center">
					<a href="javascript:void(0)"  class="button1" tabindex="7" onclick="cancel_remove_popup_admin('deleteAcc')"><span><?=t($t_base.'fields/close')?></span></a></p>
					</p>
					<?
					}else{?>
					<p align="center"><a href="javascript:void(0)" class="button1" onclick="confirm_reactive_user('<?=$idUser?>','reactive');" tabindex="6"><span><?=t($t_base.'fields/yes')?></span></a> 
					<a href="javascript:void(0)" class="button1" tabindex="7" onclick="cancel_remove_popup_admin('deleteAcc')"><span><?=t($t_base.'fields/no')?></span></a></p>
						<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
						<input type="hidden" name="flag" id="flag" value="<?=$flag?>" />
					<? }?>	
					</form>
				</div>
		</div>
	<?php }else if($flag=='sendMail'){ 
			
			$idEmailLog=$_REQUEST['idEmailLog'];
			$emailAddress = $kAdmin->findCustomerEmail((int)$idUser);
			$emailDetail=$kAdmin->getEmailLogDetailById($idEmailLog);
			if($_REQUEST['confirmFlag']=='confirm')
			{
                            $kUser->getUserDetails($idUser);
                            $szSubject=($emailDetail[0]['szEmailSubject']);
                            $message=($emailDetail[0]['szEmailBody']);

                            /*
                            * building block for send Transporteca E-mail
                            */
                            $mailBasicDetailsAry = array();
                            $mailBasicDetailsAry['szEmailTo'] = $kUser->szEmail;
                            $mailBasicDetailsAry['szEmailFrom'] = __STORE_SUPPORT_EMAIL__; 
                            $mailBasicDetailsAry['szEmailSubject'] = $szSubject;
                            $mailBasicDetailsAry['szEmailMessage'] = $message;
                            $mailBasicDetailsAry['szReplyTo'] = __STORE_SUPPORT_EMAIL__;
                            $mailBasicDetailsAry['idUser'] = $idUser;   
                            
                            $kSendEmail = new cSendEmail();
                            $kSendEmail->sendEmail($mailBasicDetailsAry);

                            //sendEmail($kUser->szEmail,__STORE_SUPPORT_EMAIL__,$szSubject,$message,__STORE_SUPPORT_EMAIL__, $idUser);
                            ?>
                            <script type="text/javascript">	
                                showEditCustomerPages('accountHis','<?=$idUser?>');	
                            </script>
                            <?php
                            exit();
			}
	?>
			<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification" style="width:550px;margin-top:-10px;">
					<form name="sendEmail" id="sendEmail" method="post">
						<table>
							<tr>
								<td><?=t($t_base.'fields/to')?>: <?=$emailAddress?></td>
							</tr>
							<tr>
								<td><?=t($t_base.'fields/subject')?>: <?=$emailDetail[0]['szEmailSubject']?></td>
							</tr>
							<tr>
								<td><?=$emailDetail[0]['szEmailBody']?></td>
							</tr>
							<tr>
								<td>
								<input type="hidden" name="confirmFlag" id="confirmFlag" value="confirm" />	
								<input type="hidden" name="idEmailLog" id="idEmailLog" value="<?=$idEmailLog?>" />
								<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
								<input type="hidden" name="flag" id="flag" value="<?=$flag?>" />
								<br />
								<p align="center">
									<a href="javascript:void(0)" class="button1" tabindex="7" onclick="email_send_user()"><span><?=t($t_base.'fields/send')?></span></a>
									<a href="javascript:void(0)" class="button2" tabindex="7" onclick="cancel_remove_popup_admin('accountHis')"><span><?=t($t_base.'fields/cancel')?></span></a>
								</p>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>	
	<? }else if($flag=='userSendEmail'){
	
		$kUser->getUserDetails($idUser);
		if(!empty($_POST['arrUserEmail']))
		{
			$kAdmin->sendEmail($_POST['arrUserEmail'],$idUser);
			if(empty($kAdmin->arErrorMessages)){
			?>
			<script type="text/javascript">	
					showEditCustomerPages('setting','<?=$idUser?>');	
					</script>
					<?
					exit();
			}
		}
	
	?>
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification" style="width:480px;margin-top:-10px;">
					<? if(!empty($kAdmin->arErrorMessages)){
						$t_base_error = "Error";
						?>
						<div id="regError" class="errorBox">
						<div class="header"><?=t($t_base_error.'/please_following');?></div>
						<div id="regErrorList">
						<ul>
						<?php
							foreach($kAdmin->arErrorMessages as $key=>$values)
							{
							?><li><?=$values?></li>
							<?php	
							}
						?>
						</ul>
						</div>
						</div>
						<script type="text/javascript">
						$("#confirm_email").attr("style","opacity:1;");
						$("#confirm_email").attr("onclick","confirm_send_email_user()");
						
						$("#cancel_email").attr("style","opacity:1;");
						$("#cancel_email").attr("onclick","cancel_remove_popup_admin('accountHis')");
						</script>
						<?php }?>
					<form name="sendUserEmail" id="sendUserEmail" method="post">
						<table>
							<tr>
								<td><?=t($t_base.'fields/email')?></td><td> <?=$kUser->szEmail?></td>
							</tr>
							<tr>
								<td><?=t($t_base.'fields/subject')?></td><td> <input type="text" name="arrUserEmail[szSubject]" id="szSubject" value="<?=$_POST['arrUserEmail']['szSubject']?>" style="width: 391px;"/></td>
							</tr>
							<tr>
								<td valign="top"><?=t($t_base.'fields/message')?></td><td> <textarea cols="50" rows="15" name="arrUserEmail[szMessage]" id="szMessage"></textarea></td>
							</tr>
							<tr>
								<td colspan="2" align="center">
								<input type="hidden" name="arrUserEmail[szEmail]" id="szEmail" value="<?=$kUser->szEmail?>"/>		
								<input type="hidden" name="confirmFlag" id="confirmFlag" value="confirm" />	
								<input type="hidden" name="idUser" id="idUser" value="<?=$idUser?>" />
								<input type="hidden" name="flag" id="flag" value="<?=$flag?>" />
								<a href="javascript:void(0)" class="button1" tabindex="7" onclick="confirm_send_email_user()" id="confirm_email"><span><?=t($t_base.'fields/send')?></span></a>
								<a href="javascript:void(0)" class="button2" tabindex="7" onclick="cancel_remove_popup_admin('accountHis')" id="cancel_email"><span><?=t($t_base.'fields/cancel')?></span></a>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>	
	<? }?>
<input type="hidden" name="currentflag" value="<?=$flag?>" id="currentflag"/>