<?php
/**
 * View Booking Confirmation
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();

$kBooking = new cBooking();
$kWHSSearch = new cWHSSearch();
$idBooking=sanitize_all_html_input($_REQUEST['idBooking']);
if((int)$idBooking>0)
{
	$idBooking=$_REQUEST['idBooking'];
        $kBooking->load($idBooking);
	$flag=sanitize_all_html_input($_REQUEST['flag']);
	$pdfhtmlflag=true;
        $flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
	if($flag=='pdf')
	{
		$pdfhtmlflag=false;
	}
        if($kBooking->iFinancialVersion==2 && $kBooking->iForwarderGPType==1)
        {
            $bookingConfirmationPdf=getForwarderBookingConfirmationPdfFile_v2($idBooking,$pdfhtmlflag);
        }
        else
        {
            $bookingConfirmationPdf=getForwarderBookingConfirmationPdfFile($idBooking,$pdfhtmlflag);
        }
		if($flag=='pdf')
		{
			 download_booking_pdf_file($bookingConfirmationPdf);
        	die;
		}
}
else
{
	header('Location:'.__BASE_URL__.'/');
	exit();
}
?>