<?php
/**
 * View Booking Confirmation
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$kBooking = new cBooking();
$idBooking=(int)sanitize_all_html_input($_REQUEST['idBooking']);
 
if($idBooking>0)
{
    $flag=$_REQUEST['flag'];
    $pdfhtmlflag=true;
    $flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
    if($flag=='pdf')
    {
        $pdfhtmlflag=false;
    }
    
    $bookingConfirmationPdf=getBookingConfirmationPdfFileHTML($idBooking,$pdfhtmlflag);
    if($flag=='pdf')
    { 
        download_booking_pdf_file($bookingConfirmationPdf);
        die;
    }
}
else
{
    ob_end_clean();
    header('location:'.__MANAGEMENT_URL__);
    die;
}	

?>