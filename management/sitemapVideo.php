<?php 
/**
  *  admin--- maintanance -- contries
  */
define('PAGE_PERMISSION','__SYSTEM_VIDEO_SCRIPT__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Sitemap Video Script";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/country/";
$t_base_error = "Error/";

$kSEO = new cSEO();

if(!empty($_POST['sitemapVideoAry']))
{
	if($kSEO->updateSitemapVideo($_POST['sitemapVideoAry']))
	{
		$notify = t($t_base.'messages/video_script_success_message')."."; 
	}
	
	$szVideCode = $_POST['sitemapVideoAry']['szVideoCode'];
	$idVideCode = $_POST['sitemapVideoAry']['idVideoCode'];
}
else 
{
	$sitemapVideoAry = array();
	$sitemapVideoAry = $kSEO->getSitemapVideo();
	
	$szVideCode = $sitemapVideoAry['szVideoCode'];
	$idVideCode = $sitemapVideoAry['id'];
}
?>

<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
	<?php 
		if(!empty($kSEO->arErrorMessages))
		{
			?>
			<div id="regError" class="errorBox ">
				<div class="header"><?=t($t_base_error.'please_following');?></div>
				<div id="regErrorList">
					<ul>
					<?php
						foreach($kSEO->arErrorMessages as $key=>$values)
						{
							?>
							<li><?=$values?></li>
							<?php	
						}
					?>
					</ul>
				</div>
			</div>
	<?php  }
	?>
	<form action="" method="post" id="sitemapVideoScriptForm" name="sitemapVideoScriptForm">
		<textarea name="sitemapVideoAry[szVideoCode]" rows="15" id="szVideoCode" cols="93"><?php echo $szVideCode; ?></textarea>
		<input type="hidden" name="sitemapVideoAry[idVideoCode]" value="<?php echo $idVideCode;?>" id="idVideoCode">
		
		<p><br />
			<?php 
			if(!empty($notify))
			{
				echo '<span style="float:left;color:green;">'.$notify."</span>";
			}
			?> 
			<span style="float:right"><a class="button1" onclick="$('#sitemapVideoScriptForm').submit();" ><span>Save</span></a></span>
		</p>
	</form>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>