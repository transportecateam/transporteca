<?php 
/**
  *  Expalin page Adding new subcategories
  * 
  */
define('PAGE_PERMISSION','__SEARCH_PHRASE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Explain Page - Search Phrase";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/layout/admin_header.php");
validateManagement();
$kExplain = new cExplain;
$content = $kExplain->selectSearchFields();
$t_base="management/explaindata/";
?>

<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
			<h5><b><?=t($t_base.'title/search_phrase_six_month');?></b></h5>
			 <table cellpadding="0" cellspacing="0" class="format-4" width="100%">
			<tr>
				<th width="4%" align="center" valign="top"></th>
				<th width="38%" align="left" valign="top"><?=t($t_base.'fields/parase')?></th>
				<th width="15%" align="left" valign="top" ><?=t($t_base.'fields/count')?></th>
			</tr>
	 <?
	 	if(!empty($content))
	 	{	
	 		$count=1;
		 	foreach($content as $terms)
		 	{ 
		 ?>
			 	<tr>
				 	<td valign="top"><?=$count;?></td>
				 	<td valign="top"><?=$terms['szPhrase'];?></td>
				 	<td valign="top"><?=$terms['total'];?></td> 	
				 </tr>
				 
		 <? $count++; }
		}
		elseif(empty($content))
		{
			echo "<tr align='center'><td colspan='5'><strong>NO DATA TO PREVIEW</strong></td></tr>";
		}
	?>
	</table>
	</div>
</div>
	
<?php include( __APP_PATH_LAYOUT__ . "/admin_footer.php" ); ?>	