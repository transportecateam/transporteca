<?php 
/**
  *BILLING DETAILS
  */
define('PAGE_PERMISSION','__PROVIDER_PRODUCTS__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Courier - Providers & Products";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$kCourierServices = new cCourierServices();
$t_base="management/providerProduct/";
validateManagement();

$providerProductArr=$kCourierServices->getCourierProviderList();  
?>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script>  

<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="customs_clearance_pop" class="help-pop">&nbsp;</div>
<div id="hsbody-2"> 
<?php require_once(__APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php"); ?>
<div class="hsbody-2-right">
    <div class="clearfix">
        <div style="float:left;width:45%;min-height:200px;" id="courier_provider_listing"> 
            <?php addDisplayCouierProviderName($kCourierServices,$idCourierProvider);?>
        </div>
        <div style="float:right;width:54%;min-height:200px;" id="courier_provider_product_listing"> 
       <?php addDisplayCouierProviderProductName($kCourierServices,$idCourierProvider);?>     
        </div> 
    </div>

    <div class="clear-all"></div>
    <br> 
    <div id="courier_cargo_limitation_terms_instruction_data">
        <?php
            echo display_courier_product_terms_instruction_form($kCourierServices);
        ?>
    </div> 
    <hr class="cargo-top-line">   
    <div class="clearfix">
        <div id="courier_product_group" style="float:left;width:45%;min-height:200px;">
            <?php
                echo courierProductGroupList($kCourierServices);
            ?>	
        </div>
        <div id="cargo_limitation" style="float:right;width:54%;min-height:200px;">
            <?php
                echo courierCargoLimitationList($kCourierServices);
            ?>	
        </div>
    </div>
    <hr class="cargo-top-line"> 
    <div id="courier_excluded_trade_list" class="clearfix">
        <?php
           echo courierExcludedTradesList($kCourierServices);
        ?>	
    </div> 
</div>
</div>

<input type="hidden" name="idExcludedServiceType" id="idExcludedServiceType" value="">  

<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>