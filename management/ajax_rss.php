<?php
/**
 * Edit Forwarder profile
 */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$t_base_error="management/Error/";

if(isset($_POST))
{
	$arr['szHeading']  =	$_POST['szHeading'];
	$arr['szUrl']      = $_POST['szUrl'];
	$arr['szContent']  = $_POST['content'];
	$arr['id']	= $idAdmin;
	if($kAdmin->addRssFeed($arr))
	{
		echo "SUCCESS|||||";
	}
	else if(!empty($kAdmin->arErrorMessages))
	{
	echo "ERROR|||||";
	?>
	<div id="regError" class="errorBox" style="display:block;">
	<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kAdmin->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	</div>
	<?
	}
}
