<?php 
define('PAGE_PERMISSION','__INSURANCE_RATES_BOOKING__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle='Transporteca | Insurance buy/sell rates';
$display_profile_not_completed_message = true;
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

$kInsurance = new cInsurance();
$kWhsSearch = new cWHSSearch();   
$arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      
  
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];
$szLanguage = "en"; 

$t_base = "management/uploadService/";
$t_base_bulk="BulkUpload/";
validateManagement();

?>
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script>   
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>

<div id="customs_clearance_pop" class="help-pop"></div>
<div id="hsbody-2">
    <?php require_once(__APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php"); ?> 
    <div class="hsbody-2-right"> 
        <div style="min-height:200px;" id="insurance_sell_rates_main_container_div"> 
            <?php echo display_insurance_rates_listing(__INSURANCE_SELL_RATE_TYPE__,$kInsurance); ?>
        </div>
        <div class="clear-all"></div>
        <br>
        <div style="min-height:200px;" id="insurance_buy_rates_main_container_div"> 
            <?php echo display_insurance_rates_listing(__INSURANCE_BUY_RATE_TYPE__,$kInsurance); ?>
        </div> 
        <div class="grey-line">&nbsp;</div><br>
        <div id="insurance_try_it_out_main_container_div"> 
            <?php echo display_insurance_try_it_out($kInsurance); ?>
        </div>
        <input type="hidden" name="insuranceRateIds_<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>" id="insuranceRateIds_<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>" value="">
        <input type="hidden" name="insuranceRateIds_<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>" id="insuranceRateIds_<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>" value=""> 
        
        <input type="hidden" name="idPrimarySortKey[<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>]" id="idPrimarySortKey_<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>" value="">
        <input type="hidden" name="idPrimarySortValue[<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>]" id="idPrimarySortValue_<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>" value="ASC">
        <input type="hidden" name="idOtherSortKey[<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>]" id="idOtherSortKey_<?php echo __INSURANCE_SELL_RATE_TYPE__; ?>" value=""> 
        
        <input type="hidden" name="idPrimarySortKey[<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>]" id="idPrimarySortKey_<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>" value="">
        <input type="hidden" name="idPrimarySortValue[<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>]" id="idPrimarySortValue_<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>" value="ASC">
        <input type="hidden" name="idOtherSortKey[<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>]" id="idOtherSortKey_<?php echo __INSURANCE_BUY_RATE_TYPE__; ?>" value="">  
    </div>  
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>