<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__MESSAGE_TIMING__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Message Settings - Timing";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/crownJob/";
validateManagement();
$crownJobs=$kAdmin->selectCrownJobs(2);
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/admin_messages_left_nav.php" ); ?> 
	<div class="hsbody-2-right">
		<div id ="table" style="padding-bottom:20px;">
		<?=t($t_base.'message/log')?>
		<br /><br />
			<p><textarea rows="20" cols="93" id="crown_jobs"><?=$crownJobs['szDescription'];?></textarea></p> 
			<br/><br/>
			<a class="button1" onclick="save_crown_jobs(2);"><span><?=t($t_base.'fields/save');?></span></a>
		</div>		
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>