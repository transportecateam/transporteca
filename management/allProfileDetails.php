<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/AllProfiles/";
$allCustomerArr=$kAdmin->getAllCustomers();
?>
    <div id="hsbody-2"> 
        <div id="content_body"> 
            <?=adminProfileDetailsManagement($t_base,$allCustomerArr);?>
        </div>
    </div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>