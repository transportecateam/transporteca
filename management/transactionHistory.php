<?php
define('PAGE_PERMISSION','__TRANSACTION_HISTORY__');
ob_start(); 	
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca | Forwarder View - Transaction History "; 
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
$kBooking = new cBooking();
$kForwarder = new cForwarder();
$kBilling= new cBilling();
$kConfig= new cConfig();
validateManagement();

/*
 * From 31st Jan 2017, we are no longer using this screen -  @Ajay
 */
ob_end_clean();
header("Location: ".__MANAGEMENT_URL__);
die;

$forwarderIdName=$kBooking->forwarderIdName($idAdmin);
$idForwarder=$forwarderIdName[0]['id'];
$detailsBankAcc=$kBooking->detailsForwarderBankAcc($idForwarder);

$t_base = "ForwarderBillings/";
$t_base_error="Error";


//dates are for searching in a range
$format = 'Y-m-d';
$date = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
$days = date('d')-1;
$toDate = date($format);
$fromDate = date($format,strtotime('- 3 MONTH'));
//passing an date array for searching
$dateArr=array($fromDate,$date);
//$date="  BETWEEN '".$fromDate."' AND '".$date."'";
	
$invoiceBillPending  = $kBooking->invoicesBilling($idForwarder,0,'');//for waiting from payment gateway
$invoiceBillConfirmedPendingAdmin = $kBooking->invoicesBilling($idForwarder,1,$dateArr);//for pending settlements
$invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,$dateArr);//paid to forwarder

$sumPendingBalance  = $kBooking->sumInvoicesBilling($idForwarder,0,'','');//pending from gateway
$sumCurrentBalancePendingAdmin = $kBooking->sumInvoicesBilling($idForwarder,4,'',$dateArr);//waiting to be approved by admin
$sumCurrentBalance = $kBooking->sumInvoicesBilling($idForwarder,2,'',$dateArr);//paid
$sumPaidReferalAndTransfer = $kBooking->sumInvoicesBilling($idForwarder,6,'',$fromDate);
 
//for showing date
$dateView = 'Date range: '.date('d/m/Y',strtotime($fromDate)).' - '.date('d/m/Y',strtotime($toDate));

$dateFormSearchResult= 	strtotime($fromDate);

$totalAmountAry = $kBooking->getForwarderTotalInMultipleCurrency($idForwarder);
$currentAvailableAmountAry = $kBooking->getForwarderTotalInMultipleCurrency($idForwarder,$dateArr,true);

$totalAmountOfUploadServiceAry = $kBooking->getTransportecaUploadServicePayment($idForwarder);
$dateFormSearchResult= 	strtotime($fromDate);

?> 
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="customs_clearance_pop_right" class="help-pop right">
</div>
<div id="hsbody" style="padding-top: 5px;">
    <?php //require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>
    <div>	
        <div class="date-range-container clearfix"> 
            <span id="date_change"><?=$dateView;?></span> 
            <div style="float: left;"><h4><strong><?=t($t_base.'title/transaction_history');?> </strong></h4></div> 
            <div id="forwarder_billing_top_form"> 
                <?php
                    echo transporteca_billing_search_form($forwarderIdName,$data,$t_base);
                ?>
            </div>	
        </div>
        <div style="clear: both"></div> 
        <div id="content">
            <?=showBillingDetails_new($t_base,$idForwarder,$dateArr);?>
        </div>	
    </div>
</div>
</div>
<?php require_once( __APP_PATH_LAYOUT__ . "/admin_footer.php" );?>	