<?php 
/**
  *  Send Bulk Email to forwarders
  */
define('PAGE_PERMISSION','__MESSAGE_QUEUE_MESSAGES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Send Messages - Review queue Messages ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$kForwarder	= new 	cForwarder();

?>
	<div id="ajaxLogin"></div>
	<div id="hsbody-2">
	<? require_once(__APP_PATH__ ."/layout/admin_messages_left_nav.php"); ?>
		
	<div class="hsbody-2-right">
		<div id="content_body"></div>
		<div id="content">
		<?=sendMessageQueueLists()?>
		</div>
				
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>