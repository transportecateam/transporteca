<?php 
/**
  *Sales Funnel Visualization. 
  */
define('PAGE_PERMISSION','__SALES_MONTHLY__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Monthly Reports";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

$t_base="management/monthlyReport/";

$kReport = new cReport();
validateManagement();

$monthlyReportAry = $kReport->getMonthlyReport();

?>

<div id="hsbody-2">
	<? require_once(__APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php"); ?>
	<div class="hsbody-2-right">
		<div id="top_div">
		<?	
			showMonthlyReportTopTable($monthlyReportAry);
		?>
		</div>
		<br />
		<div id = "bottom_div">
		<?	
			showMonthlyReportBottomTable($monthlyReportAry);
		?>
		</div>
	</div>
</div>

<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	