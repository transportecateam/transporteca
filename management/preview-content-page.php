<?php
define('PAGE_PERMISSION','__CONTENT__');
ob_start();
session_start();


if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH__ . "/inc/functions.php" );
include( __APP_PATH__ . "/inc/content_functions.php" );
if(!empty($_SERVER['HTTP_HOST']))
{
    define ('__BASE_URL__', __BASE_MANAGEMENT_URL__);
    define ('__BASE_URL_SECURE__', __BASE_SECURE_MANAGEMENT_URL__);
    define_management_constants(); 
}
$kAdmin=new cAdmin();
 
if((int)$_SESSION['admin_id']>0)
{
    $idAdmin = $_SESSION['admin_id'];
}
if((int)$idAdmin>0)
{
    $kAdmin->getAdminDetails($idAdmin);
    $checkPerissionDashboardFlag=checkManagementPermission($kAdmin,"__DASHBOARD__",1);
    $checkPerissionFilesFlag=checkManagementPermission($kAdmin,"__FILIES__",1);
    $checkPerissionContentFlag=checkManagementPermission($kAdmin,"__CONTENT__",1);
    $checkPerissionOperationsFlag=checkManagementPermission($kAdmin,"__OPERATIONS__",1);
    $checkPerissionFinancialFlag=checkManagementPermission($kAdmin,"__FINANCIAL__",1);
    $checkPerissionSystemFlag=checkManagementPermission($kAdmin,"__SYSTEM__",1);
    $checkPerissionMessageFlag=checkManagementPermission($kAdmin,"__MESSAGE__",1);
    $checkPerissionUserFlag=checkManagementPermission($kAdmin,"__USER__",1);
    //echo $_REQUEST['menuflag']."menuflag";
    if($_REQUEST['menuflag']!='')
    {
        if(!$checkPerissionUserFlag && !$checkPerissionMessageFlag && !$checkPerissionSystemFlag && !$checkPerissionFinancialFlag && !$checkPerissionOperationsFlag && !$checkPerissionFilesFlag && !$checkPerissionContentFlag && !$checkPerissionDashboardFlag)
        {
            $redirect_url=__MANAGEMENT_BLANK_PAGE_FEE__;
            header("Location:".$redirect_url);
            exit();
        }
        else
        {
            if(!$checkPerissionDashboardFlag && $_REQUEST['menuflag']=='dash')
            {
                $redirect_url=__MANAGEMENT_BLANK_PAGE_FEE__;
                header("Location:".$redirect_url);
                exit();
            }
            else
            {
                redirectPagePermission($_REQUEST['menuflag'],$checkPerissionUserFlag,$checkPerissionMessageFlag,$checkPerissionSystemFlag,$checkPerissionFinancialFlag,$checkPerissionOperationsFlag,$checkPerissionFilesFlag,$checkPerissionContentFlag,$checkPerissionDashboardFlag,PAGE_PERMISSION,$kAdmin);
            }
        }
     }
}
if($_REQUEST['menuflag']=='individualPagePreview')
{ 
    if(!empty($_SESSION['LEVELDATA']))
    {
        $explaingPageDataAry = $_SESSION['LEVELDATA'];  
    }

    //print_r($explaingPageDataAry);
    //print_r($explainLevel2Arr);
    //$explaingPageDataAry['szUploadFileName']
    if(!empty($explaingPageDataAry['textEditArr']['szMetaTitle']))
    {
            $szMetaTitle = $explaingPageDataAry['textEditArr']['szMetaTitle'] ;
    }

    if(!empty($explaingPageDataAry['textEditArr']['szMetaKeywords']))
    {
            $szMetaKeywords = $explaingPageDataAry['textEditArr']['szMetaKeywords'] ;
    }
    if(!empty($explaingPageDataAry['textEditArr']['szMetaDescription']))
    {
            $szMetaDescription = $explaingPageDataAry['textEditArr']['szMetaDescription'];
    }
    $explaingPageDataAry['content'] = urldecode(base64_decode($_SESSION['LEVELDATA']['description']));
    $bgColor="background-color:".$explaingPageDataAry['textEditArr']['szBackgroundColor']."";
    $szLinKColor="color:".$explaingPageDataAry['textEditArr']['szLinkColor']."";
	
	
}
else
{
    if(!empty($_SESSION['explainArr']))
    {
        $explaingPageDataAry = $_SESSION['explainArr'];  
    } 
    $iLanguage = $explaingPageDataAry['iLanguage'];  
}


$headerSearchStyle="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once(__APP_PATH_LAYOUT__."/preview_header_new.php");
$t_base = "LandingPage/"; 
  
if($_REQUEST['menuflag']=='individualPagePreview')
{

?>
		<style>
			#hsbody a {
				color: <?=$explaingPageDataAry['textEditArr']['szLinkColor'];?>
			}
		</style>
	<?php
$kExplain = new cExplain();
//print_R($_SESSION['LEVELDATA']['idExplain']);

$explainLevel2Arr=$kExplain->selectPageDetails($_SESSION['LEVELDATA']['idExplain']);

$explaingPageDataAry['szPageHeading']=$explainLevel2Arr[0]['szPageHeading'];
$explaingPageDataAry['szUploadFileName']=$explainLevel2Arr[0]['szUploadFileName'];
$explaingPageDataAry['szSubTitle']=$explainLevel2Arr[0]['szSubTitle'];
$szLink=$explainLevel2Arr[0]['szLink'];

	$mainTab=trim($explainLevel2Arr[0]['iExplainPageType']);
	if($mainTab==1)
	{
		$subUrl=__HOW_IT_WORKS_URL__;
	}
	else if($mainTab==2)
	{
		$subUrl=__COMPANY_URL__;
	}
	else if($mainTab==3)
	{
		$subUrl=__TRANSTPORTPEDIA_URL__;
	}
}
?>
<section class="content-pages-container">    
	<div class="content-header">	
		
			<?php
			if($_REQUEST['menuflag']!='individualPagePreview')
			{
				//print_r($explaingPageDataAry);
			?>
			<div id="hsbody">
			<div class="social-section-head">
			<table style="width:100%;">
			<tr>
				<td style="width:50%"><h1><?php echo $explaingPageDataAry['szPageHeading']; ?></h1>
					<h4><?php echo $explaingPageDataAry['szSubTitle']; ?></h4>				
				</td>
				<td style="width:50%;text-align:right;" valign="top"> 
					<img src="<?php echo __BASE_EXPLAIN_LEVEL2_IMAGES_URL__."/".$explaingPageDataAry['szUploadFileName']; ?>" alt="<?php echo $explaingPageDataAry['szPictureDescription']?>" title="<?=$explaingPageDataAry['szPictureTitle']?>">
				</td>
			</tr>
			<tr>
				<td style="width:100%;text-align:left;"></td>
			</tr>
			</table>
			</div>
			</div>
			<?php	
				$explainContentArr=array();
				$explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($explaingPageDataAry['id'],true);
				if(!empty($explainContentArr))
				{
					foreach($explainContentArr as $explainContentArrs)
					{
						$bgColor='';
						$bgColor="background-color:".$explainContentArrs['szBackgroundColor']."";
						?>
							<div class="body-color <?php if($explainContentArrs['szBackgroundColor']=='#fff' || $explainContentArrs['szBackgroundColor']=='' || $explainContentArrs['szBackgroundColor']=='#ffffff'){?> white<?php }?>" style="<?php echo $bgColor;?>">
								<div id="hsbody">
									<h1 align="center" class="heading"><?php echo $explainContentArrs['szDraftHeading']?></h1>
									<?php echo $explainContentArrs['szDraftDescription']?>
								</div>
							</div>
						<?php					
					}
				}	
			
			}
			if($_REQUEST['menuflag']=='individualPagePreview')
			{?>
			<table style="width:100%;">
			<tr>
			<td colspan="2" class="body-color <?php if($explaingPageDataAry['textEditArr']['szBackgroundColor']=='#fff' || $explaingPageDataAry['textEditArr']['szBackgroundColor']=='' || $explaingPageDataAry['textEditArr']['szBackgroundColor']=='#ffffff'){?> white<?php }?>" style="<?=$bgColor?>;">
			<div id="hsbody">
			<h1 align="center" class="heading"><?php echo $explaingPageDataAry['textEditArr']['heading']?></h1>
			<?php echo $explaingPageDataAry['content']?>
			<?php
					$explainContentArr=array();
					$explainContentArr=$kExplain->getAllExplainContainDataByIdExplain($_SESSION['LEVELDATA']['idExplain']);
					//print_r($explainContentArr);
					if(count($explainContentArr)>1)
					{?>
						<p class="para" style="<?=$szLinKColor?>"><strong><?php echo t($t_base.'fields/related_topics');?></strong></p>
					<?php	
					}
					if(!empty($explainContentArr))
					{?>
					<ul class="ulclass" style="<?=$szLinKColor?>">
					<?php
						foreach($explainContentArr as $explainContentArrs)
						{
							if($explainContentArrs['id']!=$level3ExplainDataArr[0]['id'])
							{?>
								<li ><a style="<?=$szLinKColor?>" href="<?php echo __BASE_URL__."/".$subUrl."/".$szLink."/".$explainContentArrs['szUrl'].'/'?>"><?php echo $explainContentArrs['szPageName']?></a></li>
							<?php
							}
						}
						?>
						</ul>	
						<?php	
					}		
				?>
				</div>
			</td>
			</tr>
			</table>
			<?php }?>
		
		
	</div>
</section>  
<?php
//unset($_SESSION['explainArr']);
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>	