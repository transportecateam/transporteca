<?php 
/**
  *pay forwarder
  */
define('PAGE_PERMISSION','__PAYMENT_HISTORY__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Accounts Payable - Payment History";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/forwarderpaymenthistory/";
validateManagement();
$kBooking= new cBooking();
$paymentDetails=$kBooking->paymentHistoryOfForwarders($idAdmin);
//print_r($paymentDetails);
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
	<?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>
		
	<div class="hsbody-2-right">
		<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
		<tr>
			<td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/transfer_date')?></strong></td>
			<td width="25%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/forwarder')?></strong></td>
			<td width="20%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/bank')?></strong></td>
			<td width="20%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/account')?></strong></td>
			<td width="20%" style='text-align:right;' style="text-align: right;" valign="top"><strong><?=t($t_base.'fields/amount')?></strong></td>
		</tr>
		<?php	if($paymentDetails!=array())
			{	
                            $count=1;
                            foreach($paymentDetails as $payment)
                            {
                                if(!empty($payment['dtPaymentScheduled']) && $payment['dtPaymentScheduled']!='0000-00-00 00:00:00')
                                {
                                    $dtTransferDueDate = date('d/m/Y',strtotime($payment['dtPaymentScheduled']));
                                }
                                else
                                {
                                    $dtTransferDueDate = date('d/m/Y',strtotime($payment['dtPaymentConfirmed']));
                                }
                                ?>
                                <tr id="booking_data_<?=$count;?>" onclick="viewNoticeTransactionDetails('booking_data_<?=$count;?>','<?=$payment[iBatchNo]?>','<?=$payment[idForwarder]?>','1')">
                                    <td style='text-align:left;'><?=$dtTransferDueDate?></td>
                                    <td style='text-align:left;'><?=returnLimitData($payment['szDisplayName'],20)." (".$payment['szForwarderAlias'].") ";?></td>
                                    <td style='text-align:left;'><?=returnLimitData($payment['szBankName'],20)?></td>
                                    <td style='text-align:left;'><?=$payment['iAccountNumber']?></td>
                                    <td style="text-align: right;"><?=$payment['szCurrency']." ".number_format($payment['fTotalPriceForwarderCurrency'],2)?></td>
                                </tr>
                                <?php
                                $count++;
                            }
			}
			else
			{
				echo "<tr><td colspan='6'><CENTER><b>".t($t_base.'fields/no_record_found')."</b><CENTER></td></tr>";
			}
		?>
		</table>
		<a class="button1" id='view_notice' style="opacity:0.4;float: right;"><span><?=t($t_base."fields/view_notice")?></span></a>	
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
