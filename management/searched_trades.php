<?php 
/**
  *Sales Funnel Visualization. 
  */
define('PAGE_PERMISSION','__SALES_SEARCHED_TRADES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Searched Trades";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");

$t_base="management/visualization/";

$kReport = new cReport();
validateManagement();

$searchedTradeAry = $kReport->getAllSearchedTradeSnapshot(); 

?>

<div id="hsbody-2">
	<?php require_once(__APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php"); ?>
	<div class="hsbody-2-right" id="searched_trade_div">
		<?php echo displaySearchedTradesData($searchedTradeAry,'iSearched_30_days');?>
		<br />		
	</div>
    <input type="hidden" name="szSortField" id="szSortField" value='iSearched_30_days'/>
    <input type="hidden" name="szSortBy" id="szSortBy"  value='DESC'/>
</div>

<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>	