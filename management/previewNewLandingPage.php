<?php
define('PAGE_PERMISSION','__LANDING_PAGES__');
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH__ . "/inc/functions.php" );
include( __APP_PATH__ . "/inc/content_functions.php" );
if(!empty($_SERVER['HTTP_HOST']))
{
    define ('__BASE_URL__', __BASE_MANAGEMENT_URL__);
    define ('__BASE_URL_SECURE__', __BASE_SECURE_MANAGEMENT_URL__);
    define_management_constants(); 
}
$kAdmin=new cAdmin();
 
if((int)$_SESSION['admin_id']>0)
{
    $idAdmin = $_SESSION['admin_id'];
}
if((int)$idAdmin>0)
{
    $kAdmin->getAdminDetails($idAdmin);
    $checkPerissionDashboardFlag=checkManagementPermission($kAdmin,"__DASHBOARD__",1);
    $checkPerissionFilesFlag=checkManagementPermission($kAdmin,"__FILIES__",1);
    $checkPerissionContentFlag=checkManagementPermission($kAdmin,"__CONTENT__",1);
    $checkPerissionOperationsFlag=checkManagementPermission($kAdmin,"__OPERATIONS__",1);
    $checkPerissionFinancialFlag=checkManagementPermission($kAdmin,"__FINANCIAL__",1);
    $checkPerissionSystemFlag=checkManagementPermission($kAdmin,"__SYSTEM__",1);
    $checkPerissionMessageFlag=checkManagementPermission($kAdmin,"__MESSAGE__",1);
    $checkPerissionUserFlag=checkManagementPermission($kAdmin,"__USER__",1);
    //echo $_REQUEST['menuflag']."menuflag";
    if($_REQUEST['menuflag']!='')
    {
        if(!$checkPerissionUserFlag && !$checkPerissionMessageFlag && !$checkPerissionSystemFlag && !$checkPerissionFinancialFlag && !$checkPerissionOperationsFlag && !$checkPerissionFilesFlag && !$checkPerissionContentFlag && !$checkPerissionDashboardFlag)
        {
            $redirect_url=__MANAGEMENT_BLANK_PAGE_FEE__;
            header("Location:".$redirect_url);
            exit();
        }
        else
        {
            if(!$checkPerissionDashboardFlag && $_REQUEST['menuflag']=='dash')
            {
                $redirect_url=__MANAGEMENT_BLANK_PAGE_FEE__;
                header("Location:".$redirect_url);
                exit();
            }
            else
            {
                redirectPagePermission($_REQUEST['menuflag'],$checkPerissionUserFlag,$checkPerissionMessageFlag,$checkPerissionSystemFlag,$checkPerissionFinancialFlag,$checkPerissionOperationsFlag,$checkPerissionFilesFlag,$checkPerissionContentFlag,$checkPerissionDashboardFlag,PAGE_PERMISSION,$kAdmin);
            }
        }
     }
}
//require_once( __APP_PATH__ . "/inc/courier_functions.php" );
if(!empty($_POST['landingPageArr']))
{
	$landingPageDataAry = $_POST['landingPageArr'] ;
	$_SESSION['landing_page_date'] = $landingPageDataAry ;
}
else
{
	$landingPageDataAry = $_SESSION['landing_page_date'] ;
}  
$iLanguage = $landingPageDataAry['iLanguage']; 
 
$landingPageDataAry['szTextD'] = $landingPageDataAry['szTextX'];
if(!empty($landingPageDataAry['szMetaTitle']))
{
	$szMetaTitle = $landingPageDataAry['szMetaTitle'] ;
}

if(!empty($landingPageDataAry['szMetaKeywords']))
{
	$szMetaKeywords = $landingPageDataAry['szMetaKeywords'] ;
}
if(!empty($landingPageDataAry['szMetaDescription']))
{
	$szMetaDescription = $landingPageDataAry['szMetaDescription'];
}

require_once(__APP_PATH_LAYOUT__."/preview_header_new.php");
$t_base = "LandingPage/"; 
?>
<script type="text/javascript">
jQuery().ready(function(){	 
	$('#video').click(function() {
		jQuery.fancybox(
	    { 
		    'href' : "#video_container_div"
	    });
	    console.log("clicked");
	}); 
}); 
</script>
<section id="search-freight" data-type="background" data-speed="10" class="pages" style="background-image:url('<?php echo $landingPageDataAry['szTextA1']; ?>');">    
	<article>
		<h1><?php echo $landingPageDataAry['szTextB']; ?></h1>
                <?php if($landingPageDataAry['iDefault']==2){?>
                 <?php if(!empty($landingPageDataAry['szTextB3'])){ ?>
                    <div class="search-description-box">
                        <?php echo $textDescription; ?>
                    </div>
                <?php } ?>
                <div id="transporteca_search_form_container">
                    <?php 
                        echo download_courier_label_html($kCourierServices);
                    ?>
                </div>
                <?php }else {?>
		<div id="transporteca_search_form_container">
			<?php 
				echo display_new_search_form($kBooking,$postSearchAry);
			?>
		</div>
                <?php }?>
                
	</article>
</section>   
<section class="what-is-in pages">
	<h1><?php echo $landingPageDataAry['szTextC']; ?></h1>
	<ul class="clearfix">
		<li class="quick">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD1']; ?>');">&nbsp;</div>
			<h3><?php echo $landingPageDataAry['szTextE1']; ?></h3>
			<p><?php echo $landingPageDataAry['szTextE2']; ?></p>
		</li>
		<li class="all-price">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD4']; ?>');">&nbsp;</div>
			<h3><?php echo $landingPageDataAry['szTextE3']; ?></h3>
			<p><?php echo $landingPageDataAry['szTextE4']; ?></p>
		</li>
		<li class="on-time">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextD7']; ?>');">&nbsp;</div>
			<h3><?php echo $landingPageDataAry['szTextE5']; ?></h3>
			<p><?php echo $landingPageDataAry['szTextE6']; ?></p>
		</li>
	</ul>
</section>
<section class="how-does-work pages">
	<h1><?php echo $landingPageDataAry['szTextF1']; ?></h1>
	<ul class="clearfix">
		<li class="quick">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG1']; ?>');">&nbsp;</div>
			<p><?php echo $landingPageDataAry['szTextH1']; ?></p>
		</li>
		<li class="all-price">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG4']; ?>');">&nbsp;</div>
			<p><?php echo $landingPageDataAry['szTextH2']; ?></p>
		</li>
		<li class="on-time">
			<div class="icon" style="background-image:url('<?php echo $landingPageDataAry['szTextG7']; ?>');">&nbsp;</div>
			<p><?php echo $landingPageDataAry['szTextH3']; ?></p>
		</li>
	</ul>
</section>
<section id="partners" data-type="background" data-speed="15" class="pages" style='background-image:url("<?php echo $landingPageDataAry['szTextI1']; ?>");'>
	<article>
		<h3><?php echo $landingPageDataAry['szTextI4']; ?></h3>
		<div class="partners-number clearfix">
			<div class="professionals"><span><?php echo number_format((float)$compareButtonDataAry['iTotalUniqueIP']);?></span><br><?php echo $landingPageDataAry['szTextI5']; ?></div>
			<div class="companies"><span><?php echo number_format((float)$compareButtonDataAry['iTotalNumRows']);?></span><br><?php echo $landingPageDataAry['szTextI6']; ?></div>
			<div class="routes"><span><?php echo number_format((float)$iAvailableLclService);?></span><br><?php echo $landingPageDataAry['szTextI7']; ?></div>
		</div>
		<ul class="clearfix">
			<?php
				$logo_counter = count($landingPageDataAry['szLogoImageURL']);
				
				for($i=1;$i<=$logo_counter;$i++)
				{
				
					$idImage="img_".$i;
			?>
				<style type="text/css"> 
				<?="#".$idImage?> img:hover {  background: url('<?php echo $landingPageDataAry['szWhiteLogoImageURL'][$i]; ?>'); } </style>
				<li id="<?=$idImage?>">
					
					 <img src="<?php echo $landingPageDataAry['szLogoImageURL'][$i]; ?>" alt="<?php echo $landingPageDataAry['szLogoImageDesc'][$i]; ?>">
					 <div><div><?php echo $landingPageDataAry['szToolTipText'][$i]; ?></div></div>
				</li>
			<?php
			}
			?>
		</ul>
	</article>
</section>
<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.ba-cond.min.js"></script>
<script type="text/javascript" src="<?=__BASE_STORE_SECURE_JS_URL__?>/jquery.slitslider.js"></script>
<section id="testimonials" class="pages">
	<article>
		<h1><?php echo $landingPageDataAry['szTextJ1']; ?></h1>
		<div class="testimonials-container">
			<div id="slider" class="sl-slider-wrapper">
				<div class="sl-slider">	
					<?php 
						for($i=1;$i<4;$i++)
						{
							?>
							<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="0" data-slice2-rotation="0" data-slice1-scale="2" data-slice2-scale="2">
								<div class="sl-slide-inner">
									<div class="deco"><img src="<?php echo $landingPageDataAry['szImageURL'][$i]; ?>" alt="<?php echo $landingPageDataAry['szImageTitle'][$i]?>" title="<?php echo $landingPageDataAry['szImageDesc'][$i]; ?>"></div>
									<h2><?php echo $landingPageDataAry['szHeading'][$i]; ?></h2>
									<blockquote><p><?php echo nl2br($landingPageDataAry['szTextDesc'][$i]); ?></p><cite><?php echo $landingPageDataAry['szTitleName'][$i]; ?></cite></blockquote>
								</div>
							</div>	
							<?php
						}
					?>
				</div>
				
				<nav id="nav-arrows" class="nav-arrows">
					<span class="nav-arrow-prev">Previous</span>
					<span class="nav-arrow-next">Next</span>
				</nav>
				<nav id="nav-dots" class="nav-dots">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
				</nav> 
			</div>  
		</div>
	</article>
</section>
<section id="video" class="pages" style="cursor:pointer;background-image:url('<?php echo $landingPageDataAry['szTextL1']; ?>');">
	<article>
		
	</article>
</section>

<script type="text/javascript">	
	$(function() {
	
		var Page = (function() {

			var $navArrows = $( '#nav-arrows' ),
				$nav = $( '#nav-dots > span' ),
				slitslider = $( '#slider' ).slitslider( {
					onBeforeChange : function( slide, pos ) {

						$nav.removeClass( 'nav-dot-current' );
						$nav.eq( pos ).addClass( 'nav-dot-current' );

					}
				} ),

				init = function() {
					initEvents();					
				},
				initEvents = function() {
					// add navigation events
					$navArrows.children( ':last' ).on( 'click', function() {
						slitslider.next();
						return false;
					});

					$navArrows.children( ':first' ).on('click', function() {
						slitslider.previous();
						return false;
					} );

					$nav.each( function( i ) {							
						$( this ).on( 'click', function( event ) {									
							var $dot = $( this );									
							if( !slitslider.isActive() ) {
								$nav.removeClass( 'nav-dot-current' );
								$dot.addClass( 'nav-dot-current' );									
							}									
							slitslider.jump( i + 1 );
							return false;								
						});								
					});
				};
				return { init : init };
		})();
		Page.init();
	});
</script>
<div style="display:none;">
	<div id="video_container_div" style="width:auto;height:auto;overflow:auto;">
		<?php echo $landingPageDataAry['szTextL4']; ?>	
	</div>
</div> 
<?php

require_once(__APP_PATH_LAYOUT__."/footer_new.php");
?>	