<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$szMetaTitle="Transporteca | CFS Locations";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
$t_base_error="Error/";
$t_base = "BulkUpload/";
$kWHSSearch=new cWHSSearch();
if(isset($_POST['cfsAddEditArr']))
{
	if($_POST['cfsAddEditArr']['idEditWarehouse']=='')
	{
		//echo "hi";
		$kWHSSearch->addWarehouse($_POST['cfsAddEditArr']);
	}
	elseif(isset($_POST['cfsAddEditArr']['idEditWarehouse']))
	{	//echo "hi2";
		$kWHSSearch->saveWarehouse($_POST['cfsAddEditArr']);
	}
	if(!empty($kWHSSearch->arErrorMessages))
	{
	?>
	Error|||||
	<div id="regError" class="errorBox">
	<div class="header"><?=t($t_base_error.'please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kWHSSearch->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	</div>
	
	<script type="text/javascript">
	$(window).scrollTop($('#regError').offset().top);
	//$('#regError').focus();
	</script>
	<?php
	unset($kWHSSearch->arErrorMessages);
	
	}
	else
	{
		//$_SESSION['eachCfs'] = $_SESSION['eachCfs']+1;
	}
}
if(isset($_POST['mode']))
{
	$mode = trim(sanitize_all_html_input($_POST['mode']));
	
	if($mode == 'DELETE_WAREHOUSE')
	{
            $id	= (int)$_POST['id'];
            $batch = (int)$_POST['batch'];
            if($id>0)
            {
                $kWHSSearch->dleteWarehouse($id,$batch,'1');
            }
	}
	if($mode == 'LEAVE_PAGE')
	{
		$path = trim(sanitize_all_html_input($_POST['path']));
		$idBatch = (int)$_SESSION['idBatch'];
		echo '
			<div id="popup-bg"></div>
					<div id="popup-container" style="padding-top:100px;">
						<div class="popup" style="width:400px;"> 
							<h5><b>'.t($t_base.'messages/leaving_cfs_location').'</b></h5>
							<p>'.t($t_base.'messages/you_are_trying_to_navigate_away').' '.(int)($_SESSION['CFS']-$_SESSION['eachCfs']+1).' '.t($t_base.'messages/cfs_location_from_file').' </p>
							<br />
							<p>'.t($t_base.'messages/we_can_ignore').'</p>
							<br />
							<a class="button2" onclick="showHidePopUp();"><span>cancel</span></a>
							<a class="button1" onclick="deleteCFS(\''.$idBatch.'\',\''.$path.'\');"><span>ignore</span></a>
							<a class="button1" onclick="transfer(\''.$path.'\');"><span>transfer</span></a>
						</div>
					</div>
		';
	}
	if($mode == 'DELETE_BATCH')
	{
		$batch = trim(sanitize_all_html_input($_POST['batch']));
		$kWHSSearch->batchDelete($batch);
	}
	if($mode == 'SELECT_COUNTRIES_FORM_LIST')
	{
		$selectListCFS = array();
		$selectListCFS = sanitize_all_html_input($_POST['content']);
		$selectListCFS = explode(',',$selectListCFS);
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries(true);
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								if(in_array($allCountriesArrs['id'],$selectListCFS))
								{
									?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						}
						
	
	}
	if($mode == 'REMOVE_COUNTRIES_FROM_LIST')
	{
		$selectListCFS = array();
		$selectListCFS = sanitize_all_html_input($_POST['content']);
		$selectListCFS = explode(',',$selectListCFS);
		$country   = (int)$_POST['country'];
		$latitude  = (int)$_POST['latitude'];
		$longitude = (int)$_POST['longitude'];
		$countryList = $kWHSSearch->findWarehouseCountryList($country,$latitude,$longitude);
		if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."'>".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	}
	if($mode == 'SHOW_POP_UP')
	{
		echo '
			<div id="popup-bg"></div>
					<div id="popup-container" style="padding-top:100px;">
						<div class="popup" style="width:400px;"> 
							<h4><b>'.t($t_base.'messages/countries_serviced').'</b></h4>
							<br />
								<p>'.t($t_base.'messages/you_have_no_any').'</p>
								<br />
								<p align="center">
								<a class="button1" onclick="submitCFSlisting();"><span>confirm</span></a>
								<a class="button2" onclick="showHidePopUp();"><span>back</span></a>
								</p>
						</div>
					</div>
			</div>						
		';
	}
	if($mode == 'RESET_SESSION_DETAILS')
	{
		unsetContent();
		echo  true;
	}
	if($mode == 'SHOW_MULTIPLE_COUNTRIES_SELECT')
	{
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries(true);
		$idCountry = (int)sanitize_all_html_input($_POST['idCountry']);
		$kWHSSearch->set_idCountry($idCountry);
		$countryList = $kWHSSearch->findWarehouseCountryList($kWHSSearch->idCountry,'','');?>
	    	<div id="selectForCountrySelection">
	    	 <? //$selectListCFS = array(1,2,3);
	    	 	if($kWHSSearch->idCountry!= NULL)
	    	 	{
	    	 		$selectListCFS = array($kWHSSearch->idCountry);
	    	 	}
	    	 ?>
	    	  <h4><b><?=t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_local_market');?></p>
	    	<div style="direction: ltr;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td valign="top" width="215"><?=t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			<?php
			if(!empty($allCountriesArr))
			{
				foreach($allCountriesArr as $allCountriesArrs)
				{
					if(in_array($allCountriesArrs['id'],$selectListCFS))
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			}
		?>	
	    	 </select>
	    	  <br />
	    	   <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	  </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?=t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>	
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 <?
	    	 	if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."' >".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	    	 ?>
	    	 </select>
	    	 <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	  	   		<div class="profile-fields" style="margin-top: 14px;float:left">
	    	 	<div class="field-alert" id="example" style="display:none;">
	    	 		<div>
	    	 		<p><?=t($t_base.'messages/for_your_rotterdam');?></p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'messages/for_your_hong_kong');?></p>
	    	 		</div>
	    	 	</div>
	    	 	</div>
	    	 </div>
		
	    
		<br /><br />
	
	    	 <? //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>
<?php
	}
	if( $_POST['mode'] == 'mapCountry' )
	{	
		$id = sanitize_all_html_input($_POST['id']);
		$kWHSSearch = new cWHSSearch();
		$latLongs = $kWHSSearch->getLatitudeLongitudeForCountry($id);
		echo $latLongs['szLatitude'].",".$latLongs['szLongitude'];
	}
}
?>