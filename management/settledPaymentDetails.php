<?php 
/**
  *BILLING DETAILS
  */
define('PAGE_PERMISSION','__SETTLED__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Accounts Receivable - Settled";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/billings/";
$billingSettledDetails=$kAdmin->showBookingConfirmedSettledTransaction(); 
?>
<div id="hsbody-2"> 
    <?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>    
    <div id="checkDetails" class="hsbody-2-right">
        <?php showCustomerAdminBillingSettled($billingSettledDetails,$idAdmin); ?> 
    </div>
</div>
<input type="hidden" id="szBookingStatusFlag" name="szBookingStatusFlag" value="SettledPaymentDetails">
<div id="ajaxLogin"></div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>