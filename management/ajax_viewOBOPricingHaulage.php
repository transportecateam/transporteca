<?php
/**
 * view obo pricing haulage
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base = "HaulaugePricing/";
$kHaulagePricing = new cHaulagePricing();
$Excel_export_import=new cExport_Import();
$kWHSSearch = new cWHSSearch();
$kConfig = new cConfig();

	//print_r($_REQUEST);
	if((int)$_REQUEST['idForwarder']>0)
	{
		$data['idForwarder']=$_REQUEST['idForwarder'];
	}
	if((int)$_REQUEST['idDirection']>0)
	{
		$data['idDirection']=$_REQUEST['idDirection'];
	}
	if((int)$_REQUEST['haulageWarehouse']>0)
	{
		$data['idWarehouse']=$_REQUEST['haulageWarehouse'];
	}
	

if($_REQUEST['flag']=='ChangeStatusPopup')
{
	$idMapping=$_REQUEST['idMapping'];
	$idDirection = $_REQUEST['idDirection'];
	$kHaulagePricing->updateStatusCFSHaulageModel($idMapping,$_REQUEST['changeStatusFlag'],$_REQUEST['haulageWarehouse'],$idDirection);
	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	cfs_pricing_model_list($haulagePricingModelArr);
	//pricingHaulage($haulagePricingModelArr,$_REQUEST['idForwarder']);	
	//linkpricingHaulageModel($haulagePricingModelArr,$_REQUEST['idForwarder'],$_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
	//try_it_out_haulage($_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
}
else if($_REQUEST['flag']=='moveupdown')
{
	$idMapping=$_REQUEST['idMapping'];
	$idDirection=$_REQUEST['idDirection'];
	$kHaulagePricing->updatePriorityCFSHaulageModel($idMapping,$_REQUEST['moveFlag'],$_REQUEST['haulageWarehouse'],$idDirection);
	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	cfs_pricing_model_list($haulagePricingModelArr);
	//pricingHaulage($haulagePricingModelArr,$_REQUEST['idForwarder']);
	//linkpricingHaulageModel($haulagePricingModelArr,$_REQUEST['idForwarder'],$_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
	//try_it_out_haulage($_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
}
else if($_REQUEST['flag']=='pricing_zone_detail')
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection);
	pricingHaulageZoneListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
}
else if($_REQUEST['flag']=='pricing_city_detail')
{
	///city
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$haulagePricingCityDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'city');
	pricingHaulageCityListing($haulagePricingCityDataArr,$idHaulageModel,$idDirection,$idWarehouse);
}
else if($_REQUEST['flag']=='pricing_postcode_detail')
{
	///postcode
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$haulagePricingPostCodeDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'postcode');
	pricingHaulagePostCodeListing($haulagePricingPostCodeDataArr,$idHaulageModel,$idDirection,$idWarehouse);
}
else if($_REQUEST['flag']=='pricing_distance_detail')
{
	//distance
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$haulagePricingDistanceDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'distance');
	pricingHaulageDistanceListing($haulagePricingDistanceDataArr,$idHaulageModel,$idDirection,$idWarehouse);
	
}
else if($_REQUEST['flag']=='move_haulage_zone_model')
{	

	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id=$_REQUEST['id'];
	$moveFlag=$_REQUEST['moveflag'];
	$kHaulagePricing->updatePriorityCFSHaulagePricingModel($id,$moveFlag,$idWarehouse,$idDirection,$idHaulageModel);
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection);
	//print_r($haulagePricingZoneDataArr);
	//pricingHaulageZoneListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
	zoneHaulageList($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=="pricing_zone_detail_list")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$fWmFactor=$_REQUEST['fWmFactor'];
	
	$zonename=$kHaulagePricing->getHaulageModelName($id);
	$haulagePricingZoneDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($id);
	//print_r($haulagePricingZoneDetailDataArr);
	showZoneDetail($haulagePricingZoneDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$zonename,$idHaulageModel,$idWarehouse,$fWmFactor);
}
else if($_REQUEST['flag']=="delete_haulage_zone_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$kHaulagePricing->deleteHaulagePricingZoneData($id,$idWarehouse,$idHaulageModel,$idDirection);
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection);	
	pricingHaulageZoneListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
	$data['idDirection']=$_REQUEST['idDirection'];
	$data['idWarehouse']=$_REQUEST['idWarehouse'];
	$data['idHaulageModel']=$_REQUEST['idHaulageModel'];
	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	//print_r($haulagePricingModelArr);
	if(!empty($haulagePricingModelArr))
	{
		foreach($haulagePricingModelArr as $haulagePricingModelArrs)
		{
			if($haulagePricingModelArrs['idHaulageModel']=='1')
			{
				if((int)$haulagePricingModelArrs['iCount']>0)
				{
					if((int)$haulagePricingModelArrs['iCount']==1)
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/zone_defined');
					else
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/zones_defined');	
				}
				else
					$text=t($t_base.'fields/no_zones_defined');
			}
		}
	}
	else
	{
		$text=t($t_base.'fields/no_zones_defined');
	}
	?>
		<script>
		var divid="change_data_value_<?=$_REQUEST['idHaulageModel']?>";
		$("#"+divid).html('<?=$text?>');
		</script>
	<?php
}
else if($_REQUEST['flag']=="delete_haulage_zone")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	$zonename=$kHaulagePricing->getHaulageModelName($id);
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/do_you_want_to_delete');?> <?=$zonename?> <?=t($t_base.'messages/zone');?>?</b></p>
			<br/>
			<p><?=t($t_base.'messages/confirm_msg_delete_zone');?>.</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_zone_confirm" class="button1" onclick="delete_haulage_zone_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=="delete_haulage_zone_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$iUpToKg=number_format((float)$_REQUEST['iUpToKg']);
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/delete_haulage_line');?></b></p>
			<br/>
			<p><?=t($t_base.'messages/are_you_sure');?> <?=$iUpToKg?> <?=t($t_base.'fields/kg');?>?</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_zone_line_confirm" class="button1" onclick="delete_haulage_pricing_line_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$idHaulagePricingModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=="delete_haulage_zone_line_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	
	$kHaulagePricing->deleteHaulageZonePricingData($id);
	$zonename=$kHaulagePricing->getHaulageModelName($idHaulagePricingModel);
	$haulagePricingZoneDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($idHaulagePricingModel);
	
	$arrZone[]=$kHaulagePricing->loadZone($idHaulagePricingModel);
	$fWmFactor=$arrZone[0]['fWmFactor'];
	
	//print_r($haulagePricingZoneDetailDataArr);
	showZoneDetail($haulagePricingZoneDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$zonename,$idHaulageModel,$idWarehouse,$fWmFactor);
	
}
else if($_REQUEST['flag']=="edit_haulage_zone_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$wmfactor=$_REQUEST['wmfactor'];
	$editDataArr=$kHaulagePricing->zoneLoadPricing($id);
	//print_r($editDataArr);
	$arrZone[]=$kHaulagePricing->loadZone($idHaulagePricingModel);
	$fWmFactor=$arrZone[0]['fWmFactor'];
	
	zone_pricing_form($editDataArr,$fWmFactor);
}
else if($_REQUEST['flag']=="update_zone_pricing_line")
{
    $id=$_REQUEST['id'];
    $idDirection=$_REQUEST['idDirection'];
    $idWarehouse=$_REQUEST['idWarehouse'];
    $idHaulageModel=$_REQUEST['idHaulageModel'];
    $idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
    $editDataArr=$kHaulagePricing->zoneLoadPricing($id);
    //print_r($editDataArr);
    $arrZone[]=$kHaulagePricing->loadZone($idHaulagePricingModel);
    $fWmFactor=$arrZone[0]['fWmFactor'];

    zone_pricing_form(array(),$fWmFactor);
}
else if($_REQUEST['flag']=="add_zone_pricing_line")
{
    $idDirection=$_REQUEST['idDirection'];
    $idWarehouse=$_REQUEST['idWarehouse'];
    $idHaulageModel=$_REQUEST['idHaulageModel'];
    $idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];

    $arrZone[]=$kHaulagePricing->loadZone($idHaulagePricingModel);
    $fWmFactor=$arrZone[0]['fWmFactor'];
   
    zone_pricing_form(array(),$fWmFactor);
}
else if($_REQUEST['flag']=='AUTO_UPDATE_HAULAGE_ZONE_COUNTRIES')
{
    $kHaulagePricing->updateHaulageZoneCountries();
    if(!empty($kHaulagePricing->szLastHaulageZoneUpdatingLogs))
    {
        echo "SUCCESS_LOG||||";
        echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
            </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$kHaulagePricing->szLastHaulageZoneUpdatingLogs." </div></div> </div></div>";                    
        die; 
    }
    die;
}
else if($_REQUEST['flag']=="show_ploygon_on_google_map")
{
	$idHaulageModel=$_REQUEST['id'];
	?>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		 <td colspan="5"><iframe style="border:0" name="hssiframe" width="276px" height="228px" src="<?=__BASE_URL__?>/map_polygon.php?idHaulagePricingModel=<?=$idHaulageModel?>"></iframe></td>
		</tr>
	</table>
	<?php
}
else if($_REQUEST['flag']=="edit_zone")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	$iPriorityLevel=$_REQUEST['iPriorityLevel'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
	if($idDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/to');
			$tofrom=t($t_base.'fields/from');
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$formto=t($t_base.'fields/from');
			$tofrom=t($t_base.'fields/to');
		}
		$zonename=$kHaulagePricing->getHaulageModelName($id);
		$arrZone[]=$kHaulagePricing->loadZone($id);
		//print_r($arrZone);
	?>
		<div id="popup-bg"></div>
		<div id="popup-container">
                    <div class="popup signin-popup signin-popup-verification" style="width:750px;height:500px;margin-bottom: 10px;overflow-y: auto;padding: 15px; margin-top:20px;"><h4><strong><?=t($t_base.'fields/edit');?> <?=$zonename?> <?=t($t_base.'messages/zone_for');?> <?=$direction?> <?=t($t_base.'title/haulage');?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></strong></h4>
				<br/>
				<div style="overflow: hidden;text-align: left;margin-top: 10px;">
				<div id="update_zone_form_new"></div>
				<div id="update_zone_form" style="float:left;width:240px;">
				<?php
					update_zone_form($arrZone,$idDirection,$idWarehouse,$idHaulageModel,$id,$mode,$iPriorityLevel);
				?>
				</div>
				<div style="float:right;width:480px;">
				<iframe name="draw_polygon_iframe" id="draw_polygon_iframe"  style="border: 1px solid #BFBFBF;" width="478px" height="360px" src="<?=__BASE_URL__?>/draw_polygon.php?idHaulagePricingModel=<?=$id?>&idWareHouse=<?=$idWarehouse?>"></iframe>
                                    <p class="map_note" style="font-size:12px;"><i><?=t($t_base.'messages/note')?>: <?=t($t_base.'messages/note_msg_zone')?></i></p>
                                    <p style="margin-top:6px;text-align:right"><a href="javascript:void(0)" id="clear_zone_map" class="button2" onclick="clear_zone_map('','<?=$idWarehouse?>')"><span><?=t($t_base.'fields/clear_map');?></span></a></p>
				</div>
			</div>	
			</div>
		</div>	
	<?php
}
else if($_REQUEST['flag']=="update_zone")
{ 
    $id=$_REQUEST['zoneArr']['id'];
    $idDirection=$_REQUEST['zoneArr']['idDirection'];
    $idWarehouse=$_REQUEST['zoneArr']['idWarehouse'];
    $idHaulageModel=$_REQUEST['zoneArr']['idHaulageModel'];
    $mode=$_REQUEST['zoneArr']['mode'];

    $arrZone[]=$kHaulagePricing->loadZone($id);
    update_zone_form($arrZone,$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="open_add_zone_popup")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/to');
		$tofrom=t($t_base.'fields/from');
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/from');
		$tofrom=t($t_base.'fields/to');
	}
	?>
		<div id="zone_popup_add_edit">
		<div id="popup-bg"></div>
		<div id="popup-container">
                    <div class="popup signin-popup signin-popup-verification" style="width:750px;height:500px;margin-bottom: 10px;overflow-y: auto;padding: 15px; margin-top:20px;"><h4><strong><?=t($t_base.'messages/add_new_zone_for');?> <?=$direction?> <?=t($t_base.'title/haulage');?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></strong></h4>
				<br/>				
				<div style="overflow: hidden;text-align: left;margin-top: 10px;">
				<div id="update_zone_form_new"></div>
				<div id="update_zone_form" style="float:left;width:240px;">
				<?php
					update_zone_form(array(),$idDirection,$idWarehouse,$idHaulageModel,0,$mode);
				?>
				</div>
				<div style="float:right;width:480px;">
				<iframe name="draw_polygon_iframe" id="draw_polygon_iframe"  style="border: 1px solid #BFBFBF;" width="478px" height="360px" src="<?=__BASE_URL__?>/draw_polygon.php?idWareHouse=<?=$idWarehouse?>"></iframe>
				<p class="map_note" style="font-size:12px;"><i><?=t($t_base.'messages/note')?>: <?=t($t_base.'messages/note_msg_zone')?></i></p>
				<p style="margin-top:6px;text-align:right"><a href="javascript:void(0)" id="clear_zone_map" class="button2" onclick="clear_zone_map('','<?=$idWarehouse?>')"><span><?=t($t_base.'fields/clear_map');?></span></a></p>
				</div>
			</div>	
			</div>
		</div>	
		</div>
	<?php
}
else if($_REQUEST['flag']=="add_zone")
{
	$idDirection=$_REQUEST['zoneArr']['idDirection'];
	$idWarehouse=$_REQUEST['zoneArr']['idWarehouse'];
	$idHaulageModel=$_REQUEST['zoneArr']['idHaulageModel'];
	$mode=$_REQUEST['zoneArr']['mode'];
	
	$arrZone[]=$kHaulagePricing->loadZone($id);
	update_zone_form(array(),$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="open_copy_zone_popup")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id= $_REQUEST['id'];
	?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:547px;">
			<?=copy_zone_popup($idForwarder,$idWarehouse,$idDirection,$idHaulageModel,$id)?>
		</div>
		</div>
	<?php
}
else if($_REQUEST['flag']=="Warehouse_city")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idZoneWarehouse=$_REQUEST['idZoneWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$ret_ary=$kHaulagePricing->getActiveWarehouseZone($idForwarder,$idZoneWarehouse,$idHaulageModel,$idCountry,$szCity);
	?>
		<div class="fl-25">
						<span class="f-size-12"><?=t($t_base.'fields/city');?></span>
						<select name="dataExportZoneArr[szCityZone]" disabled id="szCityZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'city','warehouse_zone','<?=$idHaulageModel?>')">
									<option value=""><?=t($t_base.'fields/all');?></option>
									<?php
										if(!empty($ret_ary))
										{
											foreach($ret_ary as $ret_arys)
											{
												?>
												<option value="<?=$ret_arys['szCity']?>"><?=$ret_arys['szCity']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div class="fl-25" id="warehouse_zone">
						<span class="f-size-12"><?=t($t_base.'fields/cfs_name');?></span>
						<select name="dataExportZoneArr[haulageWarehouseZone]" disabled id="haulageWarehouseZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value="">Select</option>
							<?php
								if(!empty($ret_ary))
								{
									foreach($ret_ary as $ret_arys)
									{
										?>
										<option value="<?=$ret_arys['warehouseId']?>"><?=$ret_arys['szWareHouseName']?></option>
										<?
									}
								}
							?>
						</select>
					</div>
					<div class="fl-25 direction">
						<span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
						<select name="dataExportZoneArr[idDirectionZone]" disabled id="idDirectionZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value='1'><?=t($t_base.'fields/export')?></option>
							<option value='2'><?=t($t_base.'fields/import')?></option>
						</select>
					</div>
	<?php
}
else if($_REQUEST['flag']=="Warehouse")
{

	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idZoneWarehouse=$_REQUEST['idZoneWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$ret_ary=$kHaulagePricing->getActiveWarehouseZone($idForwarder,$idZoneWarehouse,$idHaulageModel,$idCountry,$szCity);
?>
	<span class="f-size-12"><?=t($t_base.'fields/cfs_name');?></span>
		<select name="dataExportZoneArr[haulageWarehouseZone]" id="haulageWarehouseZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
			<option value="">Select</option>
			<?php
				if(!empty($ret_ary))
				{
					foreach($ret_ary as $ret_arys)
					{
						?>
						<option value="<?=$ret_arys['warehouseId']?>"><?=$ret_arys['szWareHouseName']?></option>
						<?
					}
				}
			?>
		</select>
<?php	
}
else if($_REQUEST['flag']=="show_copy_zone_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	$idDirection=$_REQUEST['idDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$haulageWarehouse=$_REQUEST['haulageWarehouse'];
	$haulageDirection=$_REQUEST['haulageDirection'];
	if($haulageDirection!=$idDirection || $haulageWarehouse!=$idWarehouse)
		$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'zone');
	//print_r($haulagePricingZoneDataArr);
	zone_data_list($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=="copy_zone_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,'1',$idDirection,'zone');
	//print_r($haulagePricingZoneDataArr);
	zone_data_list($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=='move_haulage_postcode_model')
{	

	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id=$_REQUEST['id'];
	$moveFlag=$_REQUEST['moveflag'];
	$kHaulagePricing->updatePriorityCFSHaulagePricingModel($id,$moveFlag,$idWarehouse,$idDirection,$idHaulageModel);
	
	$haulagePricingPostCodeDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'postcode');
	//pricingHaulagePostCodeListing($haulagePricingPostCodeDataArr,$idHaulageModel,$idDirection,$idWarehouse);
	postcodeHaulageList($haulagePricingPostCodeDataArr);
}
else if($_REQUEST['flag']=='pricing_postcode_detail_list')
{ 
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$fWmFactor=$_REQUEST['fWmFactor'];
	
	$arrPostCode[]=$kHaulagePricing->loadPostCode($id);
	$fWmFactor=$arrPostCode[0]['fWmFactor'];
	
	$postcodeset=$kHaulagePricing->getHaulageModelName($id);
	$haulagePricingPostCodeDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($id);
	//print_r($haulagePricingZoneDetailDataArr);
	showPostcodeDetail($haulagePricingPostCodeDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$postcodeset,$idHaulageModel,$idWarehouse,$fWmFactor);

}
else if($_REQUEST['flag']=="edit_haulage_postcode_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$editDataArr=$kHaulagePricing->zoneLoadPricing($id);
	//print_r($editDataArr);
	
	$arrPostCode[]=$kHaulagePricing->loadPostCode($idHaulagePricingModel);
	$fWmFactor=$arrPostCode[0]['fWmFactor'];
	
	postcode_pricing_form($editDataArr,$fWmFactor);
}
else if($_REQUEST['flag']=="add_postcode_pricing_line")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	
	$arrPostCode[]=$kHaulagePricing->loadPostCode($idHaulagePricingModel);
	$fWmFactor=$arrPostCode[0]['fWmFactor'];
	//print_r($editDataArr);
	
	postcode_pricing_form(array(),$fWmFactor);
}
else if($_REQUEST['flag']=="delete_haulage_postcode_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$iUpToKg=number_format((float)$_REQUEST['iUpToKg']);
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/delete_haulage_line');?></b></p>
			<br/>
			<p><?=t($t_base.'messages/are_you_sure');?> <?=$iUpToKg?> <?=t($t_base.'fields/kg');?>?</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_zone_line_confirm" class="button1" onclick="delete_haulage_pricing_line_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$idHaulagePricingModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=="delete_haulage_postcode_line_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	
	
	$arrPostCode[]=$kHaulagePricing->loadPostCode($idHaulagePricingModel);
	$fWmFactor=$arrPostCode[0]['fWmFactor'];
	
	$kHaulagePricing->deleteHaulageZonePricingData($id);
	$postcodeset=$kHaulagePricing->getSetOfPostCode($id);
	$haulagePricingZoneDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($idHaulagePricingModel);
	//print_r($haulagePricingZoneDetailDataArr);
	showPostCodeDetail($haulagePricingZoneDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$postcodeset,$idHaulageModel,$idWarehouse,$fWmFactor);
	
}
else if($_REQUEST['flag']=="delete_haulage_postcode_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$kHaulagePricing->deleteHaulagePricingPostCodeData($id,$idWarehouse,$idHaulageModel,$idDirection);
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'postcode');	
	pricingHaulagePostCodeListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
	$data['idDirection']=$_REQUEST['idDirection'];
	$data['idWarehouse']=$_REQUEST['idWarehouse'];
	$data['idHaulageModel']=$_REQUEST['idHaulageModel'];
	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	if(!empty($haulagePricingModelArr))
	{
		foreach($haulagePricingModelArr as $haulagePricingModelArrs)
		{
			if($haulagePricingModelArrs['idHaulageModel']=='3')
			{
				if((int)$haulagePricingModelArrs['iCount']>0)
				{
					if((int)$haulagePricingModelArrs['iCount']==1)
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/postcode_set_defined');
					else
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/postcode_sets_defined');	
				}
				else
					$text=t($t_base.'fields/no_postcode_sets_defined');
			}
		}
	}
	else
	{
		$text=t($t_base.'fields/no_postcode_sets_defined');
	}
	?>
		<script>
		var divid="change_data_value_"+<?=$_REQUEST['idHaulageModel']?>;
		$("#"+divid).html('<?=$text?>');
		</script>
	<?php
}
else if($_REQUEST['flag']=="delete_haulage_postcode")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	$name=$kHaulagePricing->getHaulageModelName($id);
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/do_you_want_to_delete_haulage_for');?> <?=$name?>?</b></p>
			<br/>
			<p><?=t($t_base.'messages/confirm_msg_delete_postcode');?>.</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_zone_confirm" class="button1" onclick="delete_haulage_postcode_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes');?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no');?></span></a></p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=="open_add_postcode_popup")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/to');
		$tofrom=t($t_base.'fields/from');
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/from');
		$tofrom=t($t_base.'fields/to');
	}
	?>
		<div id="update_postcode_form_add_edit">
		<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:808px;height:425px;margin-bottom: 10px;overflow-y: auto;padding: 15px;margin-top:30px;">
                            <h4>
				<strong><?=t($t_base.'messages/add_new_postcode_set_for');?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></strong>
                            </h4>
                            <div id="update_postcode_form" style="padding:10px 0 0;">
				<?php
                                    update_postcode_form(array(),$idDirection,$idWarehouse,$idHaulageModel,0,$mode);
				?>
                            </div>			
			</div>
		</div>	
		</div>
	<?php
}
else if($_REQUEST['flag']=="add_postcode")
{
	$idDirection=$_REQUEST['postcodeArr']['idDirection'];
	$idWarehouse=$_REQUEST['postcodeArr']['idWarehouse'];
	$idHaulageModel=$_REQUEST['postcodeArr']['idHaulageModel'];
	$mode=$_REQUEST['postcodeArr']['mode'];
	
	update_postcode_form(array(),$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="edit_postcode")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	$iPriorityLevel=$_REQUEST['iPriorityLevel'];
	$id=$_REQUEST['id'];
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/to');
		$tofrom=t($t_base.'fields/from');
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/from');
		$tofrom=t($t_base.'fields/to');
	}
	$arrPostCode[]=$kHaulagePricing->loadPostCode($id);
	?>
		<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:808px;height:425px;margin-bottom: 10px;overflow-y: auto;padding: 15px;margin-top:30px;">
				<b><?=t($t_base.'fields/edit')?> <?=$arrPostCode[0]['szName']?> <?=t($t_base.'fields/for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></b>
				<br/>				
				<div id="update_postcode_form" style="padding:10px 0 0;">
				<?
					update_postcode_form($arrPostCode,$idDirection,$idWarehouse,$idHaulageModel,$id,$mode,$iPriorityLevel);
				?>
				</div>				
			</div>
		</div>	
	<?php
}
else if($_REQUEST['flag']=="update_postcode")
{

	$id=$_REQUEST['postcodeArr']['id'];
	$idDirection=$_REQUEST['postcodeArr']['idDirection'];
	$idWarehouse=$_REQUEST['postcodeArr']['idWarehouse'];
	$idHaulageModel=$_REQUEST['postcodeArr']['idHaulageModel'];
	$mode=$_REQUEST['postcodeArr']['mode'];
	
	$arrPostCode[]=$kHaulagePricing->loadPostCode($id);
	update_postcode_form($arrPostCode,$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="show_postcode_str")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id=$_REQUEST['id'];
	$szPostCodeArr=array();
	$szPostCodeArr=$kHaulagePricing->getSetOfPostCode($id,true);
	if(!empty($szPostCodeArr))
	{
		$szPostCode_Str=implode("<br/>",$szPostCodeArr);
	}
	?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
		 <th class="noborder" align="left"><strong><?=t($t_base.'fields/postcodes_included')?></strong></th>
		</tr>
		<tr>
		 <td class="noborder" align="left"><?=$szPostCode_Str?></td>
		</tr>
	</table>
	<?php
}
else if($_REQUEST['flag']=="pricing_city_detail_list")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$fWmFactor=$_REQUEST['fWmFactor'];
	
	$zonename=$kHaulagePricing->getHaulageModelName($id);
	$haulagePricingZoneDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($id);
	//print_r($haulagePricingZoneDetailDataArr);
	showCityDetail($haulagePricingZoneDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$zonename,$idHaulageModel,$idWarehouse,$id,$fWmFactor);
}
else if($_REQUEST['flag']=='move_haulage_city_model')
{	

	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id=$_REQUEST['id'];
	$moveFlag=$_REQUEST['moveflag'];
	$kHaulagePricing->updatePriorityCFSHaulagePricingModel($id,$moveFlag,$idWarehouse,$idDirection,$idHaulageModel);
	
	$haulagePricingCityDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'city');
	cityHaulageList($haulagePricingCityDataArr);
	//pricingHaulageCityListing($haulagePricingCityDataArr,$idHaulageModel,$idDirection,$idWarehouse);
}
else if($_REQUEST['flag']=='add_city_pricing_line')
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	
	//print_r($editDataArr);
	$arrCity[]=$kHaulagePricing->loadCity($idHaulagePricingModel);
	$fWmFactor=$arrCity[0]['fWmFactor'];
	city_pricing_form(array(),$fWmFactor);
}
else if($_REQUEST['flag']=="delete_haulage_city_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$iUpToKg=number_format((float)$_REQUEST['iUpToKg']);
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/delete_haulage_line')?></b></p>
			<br/>
			<p><?=t($t_base.'messages/are_you_sure')?> <?=$iUpToKg?> <?=t($t_base.'fields/kg')?>?</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_zone_line_confirm" class="button1" onclick="delete_haulage_pricing_line_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$idHaulagePricingModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes')?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no')?></span></a></p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=="delete_haulage_city_line_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	
	$kHaulagePricing->deleteHaulageZonePricingData($id);
	$zonename=$kHaulagePricing->getHaulageModelName($id);
	$haulagePricingZoneDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($idHaulagePricingModel);
	
	$arrCity[]=$kHaulagePricing->loadCity($idHaulagePricingModel);
	$fWmFactor=$arrCity[0]['fWmFactor'];
	
	//print_r($haulagePricingZoneDetailDataArr);
	showCityDetail($haulagePricingZoneDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$zonename,$idHaulageModel,$idWarehouse,$idHaulagePricingModel,$fWmFactor);
	
}
else if($_REQUEST['flag']=="edit_haulage_city_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$editDataArr=$kHaulagePricing->zoneLoadPricing($id);
	//print_r($editDataArr);
	$arrCity[]=$kHaulagePricing->loadCity($idHaulagePricingModel);
	$fWmFactor=$arrCity[0]['fWmFactor'];
	city_pricing_form($editDataArr,$fWmFactor);
}
else if($_REQUEST['flag']=="delete_haulage_city_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$kHaulagePricing->deleteHaulagePricingCityData($id,$idWarehouse,$idHaulageModel,$idDirection);
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'city');	
	pricingHaulageCityListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse);
	$data['idDirection']=$_REQUEST['idDirection'];
	$data['idWarehouse']=$_REQUEST['idWarehouse'];
	$data['idHaulageModel']=$_REQUEST['idHaulageModel'];
	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	if(!empty($haulagePricingModelArr))
	{
		foreach($haulagePricingModelArr as $haulagePricingModelArrs)
		{
			if($haulagePricingModelArrs['idHaulageModel']=='2')
			{
				if((int)$haulagePricingModelArrs['iCount']>0)
				{
					if((int)$haulagePricingModelArrs['iCount']==1)
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/city_defined');
					else
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/cities_defined');
				}
				else
					$text=t($t_base.'fields/no_cities_defined');
			}
		}
	}
	else
	{
		$text=t($t_base.'fields/no_cities_defined');
	}
	?>
		<script>
		var divid="change_data_value_<?=$_REQUEST['idHaulageModel']?>";
		$("#"+divid).html('<?=$text?>');
		</script>
	<?php
}
else if($_REQUEST['flag']=="delete_haulage_city")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	$zonename=$kHaulagePricing->getHaulageModelName($id);
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/do_you_want_to_delete_haulage_for')?> <?=$zonename?>?</b></p>
			<br/>
			<p><?=t($t_base.'messages/confirm_msg_delete_city')?>.</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_city_confirm" class="button1" onclick="delete_haulage_city_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes')?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no')?></span></a></p>
			</div>
		</div>	
<?php
}
else if($_REQUEST['flag']=="add_postcode_string_sort")
{
	$postcode_new_arr=array();
	$textvalue=$_REQUEST['textvalue'];
	$mode=$_REQUEST['mode'];
	if($textvalue!='')
	{
		$postcode_new_arr=explode(",",$textvalue);
		sort($postcode_new_arr);
	}
	?>
		<select  style="width: 140px;height: 100px;"  multiple="multiple" id="szPostCodeStr" name="postcodeArr[szPostCodeStr]" onclick="delete_str_postcode('<?=$mode?>')">
		<? if(!empty($postcode_new_arr))
		{
			foreach($postcode_new_arr as $postcode_new_arrs)
			{
				?>
				<option value="<?=$postcode_new_arrs?>"><?=$postcode_new_arrs?></option>
				<?
			}
		}?>
		</select>
	<?php
}
else if($_REQUEST['flag']=="delete_postcode_string_sort")
{
	$postcode_new_arr=array();
	$textvalue=$_REQUEST['textvalue'];
	$postStr=$_REQUEST['postStr'];
	//print_r($textvalue);
	if($textvalue!='')
	{
		$postcode_arr=$textvalue;
	}
	if($postStr!='')
	{
		$postStr_arr=explode(",",$postStr);
	}
	
	if(!empty($postStr_arr))
	{
		foreach($postStr_arr as $postStr_arrs)
		{
			if(!in_array($postStr_arrs,$postcode_arr))
			{
				$postcode_new_arr[]=$postStr_arrs;
			}
		}
		
		sort($postcode_new_arr);
		$postcode_new_str=implode(",",$postcode_new_arr);
	}
	?>
		<select  style="width: 140px;height: 100px;" multiple="multiple" id="szPostCodeStr" name="postcodeArr[szPostCodeStr]" onclick="delete_str_postcode()">
		<?php if(!empty($postcode_new_arr))
		{
			foreach($postcode_new_arr as $postcode_new_arrs)
			{
				?>
				<option value="<?=$postcode_new_arrs?>"><?=$postcode_new_arrs?></option>
				<?
			}
		}?>
		</select>
		<script>
		$("#szPostCodeStr1").attr('value','<?=$postcode_new_str?>');
		</script>
	<?php
}
else if($_REQUEST['flag']=="open_add_city_popup")
{

	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/to');
		$tofrom=t($t_base.'fields/from');
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/from');
		$tofrom=t($t_base.'fields/to');
	}
?>
	<div id="update_city_form_add_edit">
	<div id="popup-bg"></div>
		<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification" style="width:755px;height:500px;margin-bottom: 10px;overflow-y: auto;padding:15px 10px 15px 15px;margin-top:20px;">
                    <h4><strong><?=t($t_base.'messages/add_new_city_for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></strong></h4>
			<br/>
			<div style="overflow: hidden;text-align: left;margin-top: 10px;padding-bottom: 15px; position:relative;min-height:450px;">
			<div id="update_city_form_new"></div>
			<div id="update_city_form" style="float:left;width:254px;">
			<?php
				update_city_form(array(),$idDirection,$idWarehouse,$idHaulageModel,0,$mode);
			?>
			</div>
			<div style="float:right;width:480px;">
			<iframe name="draw_circle_iframe" id="draw_circle_iframe" style="border: 1px solid #BFBFBF;" width="478px" height="360px" src="<?=__BASE_URL__?>/drawCircle.php?szLatitude=<?=$kWHSSearch->szLatitude?>&szLongitude=<?=$kWHSSearch->szLongitude?>&flag=ADD"></iframe>
			<p class="map_note" style="font-size:12px;"><i><?=t($t_base.'messages/note')?>: <?=t($t_base.'messages/note_msg')?></i></p>
			<p style="margin-top:6px;text-align:right"><a href="javascript:void(0)" id="clear_city_map" class="button2" onclick="clear_city_circle_map('<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>','','ADD')"><span><?=t($t_base.'fields/clear_map')?></span></a></p>
			</div>
		</div>	
		</div>
	</div>
	</div>
<?php
}
else if($_REQUEST['flag']=="add_city")
{
	$idDirection=$_REQUEST['cityArr']['idDirection'];
	$idWarehouse=$_REQUEST['cityArr']['idWarehouse'];
	$idHaulageModel=$_REQUEST['cityArr']['idHaulageModel'];
	$mode=$_REQUEST['cityArr']['mode'];
	
	update_city_form(array(),$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="edit_city")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	$iPriorityLevel=$_REQUEST['iPriorityLevel'];
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
		if($idDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/to');
			$tofrom=t($t_base.'fields/from');
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$formto=t($t_base.'fields/from');
			$tofrom=t($t_base.'fields/to');
		}
		$cityname=$kHaulagePricing->getHaulageModelName($id);
		$arrCity[]=$kHaulagePricing->loadCity($id);
		
		$szLatitude=$arrCity[0]['szLatitude'];
		$szLongitude=$arrCity[0]['szLongitude'];
		$radius=$arrCity[0]['iRadiousKM'];
		//print_r($arrCity);
	?>
		<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:755px;height:500px;margin-bottom: 10px;overflow-y: auto;padding:15px 10px 15px 15px;margin-top:20px;">
                            <h4><strong><?=t($t_base.'fields/edit')?> <?=$cityname?> <?=t($t_base.'messages/city_for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></strong></h4>
				<br/>
				<div style="overflow: hidden;text-align: left;margin-top: 10px;padding-bottom: 15px; position:relative;min-height:450px;">
				<div id="update_city_form_new"></div>
				<div id="update_city_form" style="float:left;width:254px;">
				<?php
					update_city_form($arrCity,$idDirection,$idWarehouse,$idHaulageModel,$id,$mode,$iPriorityLevel);
				?>
				</div>
				<div style="float:right;width:480px;">
				<iframe name="draw_circle_iframe" id="draw_circle_iframe" style="border: 1px solid #BFBFBF;" width="478px"  height="360px" src="<?=__BASE_URL__?>/drawCircle.php?szLatitude=<?=$szLatitude?>&szLongitude=<?=$szLongitude?>&radius=<?=$radius?>"></iframe>
				<p class="map_note" style="font-size:12px;"><i><?=t($t_base.'messages/note')?>: <?=t($t_base.'messages/note_msg')?></i></p>
				<p style="margin-top:6px;text-align:right"><a href="javascript:void(0)" id="clear_city_map" class="button2" onclick="clear_city_circle_map('<?=$szLatitude?>','<?=$szLongitude?>','<?=$radius?>')"><span><?=t($t_base.'fields/clear_map')?></span></a></p>
				</div>
			</div>	
			</div>
		</div>	
	<?
}
else if($_REQUEST['flag']=="update_city")
{

	$id=$_REQUEST['cityArr']['id'];
	$idDirection=$_REQUEST['cityArr']['idDirection'];
	$idWarehouse=$_REQUEST['cityArr']['idWarehouse'];
	$idHaulageModel=$_REQUEST['cityArr']['idHaulageModel'];
	$mode=$_REQUEST['cityArr']['mode'];
	
	$arrCity[]=$kHaulagePricing->loadCity($id);
	
	$szLatitude=$arrCity[0]['szLatitude'];
	$szLongitude=$arrCity[0]['szLongitude'];
	$radius=$arrCity[0]['iRadiousKM'];
	update_city_form($arrPostCode,$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="show_warehouse_city_circle")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	
		if($idDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/to');
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$formto=t($t_base.'fields/from');
		}
	?>
		<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:747px;height:450px;margin-bottom: 10px;overflow-y: auto;padding: 15px;margin-top:30px;">
				<p style="margin-bottom:5px;"><b><?=t($t_base.'messages/map_of_current_cities_defined_for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></b></p>
				<iframe name="draw_circle_iframe" style="border: 1px solid #BFBFBF;" id="draw_circle_iframe" width="100%" height="384px" src="<?=__BASE_URL__?>/showAllCityCircle.php?idWarehouse=<?=$idWarehouse?>&idDirection=<?=$idDirection?>&idHaulageModel=<?=$idHaulageModel?>"></iframe>
				<br/><br/>
				<p align="center"><a href="javascript:void(0)" id="cancel_city" class="button1" onclick="cancel_city('<?=$idHaulageModel?>')"><span><?=t($t_base.'fields/close')?></span></a></p>	
			</div>
		</div>
		<?php
}
else if($_REQUEST['flag']=='pricing_distance_detail_list')
{
    $id=$_REQUEST['id'];
    $idDirection=$_REQUEST['idDirection'];
    $idWarehouse=$_REQUEST['idWarehouse'];
    $kWHSSearch = new cWHSSearch();
    $kWHSSearch->load($idWarehouse);
    $idHaulageModel=$_REQUEST['idHaulageModel'];

    $distanceArr[]=$kHaulagePricing->loadDistance($id);

    $fWmFactor=$distanceArr[0]['fWmFactor'];
    //print_r($distanceArr);
    $haulagePricingDistanceDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($id);
    //print_r($haulagePricingZoneDetailDataArr);
    showDistanceDetail($haulagePricingDistanceDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$distanceArr[0]['iDistanceUpToKm'],$idHaulageModel,$idWarehouse,$fWmFactor);
}
else if($_REQUEST['flag']=='delete_haulage_distance')
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	$distanceArr[]=$kHaulagePricing->loadDistance($id);
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/do_you_want_to_delete_haulage_for')?> <?=strtolower(t($t_base.'fields/up_to'));?> <?=number_format($distanceArr[0]['iDistanceUpToKm']);?> <?=t($t_base.'fields/km')?> <?=t($t_base.'messages/distance')?>?</b></p>
			<br/>
			<p><?=t($t_base.'messages/confirm_msg_delete_distance')?>.</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_distance_confirm" class="button1" onclick="delete_haulage_distance_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes')?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no')?></span></a></p>
			</div>
		</div>
		<?
}
else if($_REQUEST['flag']=="delete_haulage_distance_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$kHaulagePricing->deleteHaulagePricingDistanceData($id,$idWarehouse,$idHaulageModel,$idDirection);
	
	$haulagePricingDistanceDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'distance');
	pricingHaulageDistanceListing($haulagePricingDistanceDataArr,$idHaulageModel,$idDirection,$idWarehouse);
	$data['idDirection']=$_REQUEST['idDirection'];
	$data['idWarehouse']=$_REQUEST['idWarehouse'];
	$data['idHaulageModel']=$_REQUEST['idHaulageModel'];
	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	if(!empty($haulagePricingModelArr))
	{
		foreach($haulagePricingModelArr as $haulagePricingModelArrs)
		{
			if($haulagePricingModelArrs['idHaulageModel']=='4')
			{
				if((int)$haulagePricingModelArrs['iCount']>0)
				{
					if((int)$haulagePricingModelArrs['iCount']==1)
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/distance_bracket_defined');
					else
						$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/distance_brackets_defined');
				}
				else
					$text=t($t_base.'fields/no_distance_brackets_defined');
			}
		}
	}
	else
	{
		$text=t($t_base.'fields/no_distance_brackets_defined');
	}
	?>
		<script>
		var divid="change_data_value_"+<?=$_REQUEST['idHaulageModel']?>;
		$("#"+divid).html('<?=$text?>');
		</script>
	<?
}
else if($_REQUEST['flag']=="delete_haulage_distance_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$iUpToKg=number_format((float)$_REQUEST['iUpToKg']);
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
			<p><b><?=t($t_base.'messages/delete_haulage_line')?></b></p>
			<br/>
			<p><?=t($t_base.'messages/are_you_sure')?> <?=$iUpToKg?> <?=t($t_base.'fields/kg')?>?</p>
			<br/>
			<p align="center"><a href="javascript:void(0)" id="delete_zone_line_confirm" class="button1" onclick="delete_haulage_pricing_line_confirm('<?=$id?>','<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$idHaulagePricingModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/yes')?></span></a><a href="javascript:void(0)" id="delete_zone_confirm_no" class="button2" onclick="close_delete_confirm_popup('<?=$idHaulageModel?>')"><span style="min-width:60px;"><?=t($t_base.'fields/no')?></span></a></p>
			</div>
		</div>	
<?
}
else if($_REQUEST['flag']=="delete_haulage_distance_line_confirm")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	
	$kHaulagePricing->deleteHaulageZonePricingData($id);
	$distanceArr[]=$kHaulagePricing->loadDistance($idHaulagePricingModel);
	
	$fWmFactor=$distanceArr[0]['fWmFactor'];
	//print_r($distanceArr);
	$haulagePricingDistanceDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($idHaulagePricingModel);
	//print_r($haulagePricingZoneDetailDataArr);
	showDistanceDetail($haulagePricingDistanceDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$distanceArr[0]['iDistanceUpToKm'],$idHaulageModel,$idWarehouse,$fWmFactor);
	
}
else if($_REQUEST['flag']=="edit_haulage_distance_line")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	$editDataArr=$kHaulagePricing->zoneLoadPricing($id);
	//print_r($editDataArr);
	
	$arrDistance[]=$kHaulagePricing->loadDistance($idHaulagePricingModel);
	$fWmFactor=$arrDistance[0]['fWmFactor'];
	
	distance_pricing_form($editDataArr,$fWmFactor);
}
else if($_REQUEST['flag']=='add_distance_pricing_line')
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['idHaulagePricingModel'];
	
	$arrDistance[]=$kHaulagePricing->loadDistance($idHaulagePricingModel);
	$fWmFactor=$arrDistance[0]['fWmFactor'];
	

	distance_pricing_form(array(),$fWmFactor);
}
else if($_REQUEST['flag']=="pricing_distance_detail_list")
{
	$id=$_REQUEST['id'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	
	$distanceArr[]=$kHaulagePricing->loadDistance($idHaulagePricingModel);
	$fWmFactor=$distanceArr[0]['fWmFactor'];
	//print_r($distanceArr);
	$haulagePricingDistanceDetailDataArr=$kHaulagePricing->getHaulagePricingZoneDetailData($idHaulagePricingModel);
	//print_r($haulagePricingZoneDetailDataArr);
	showDistanceDetail($haulagePricingDistanceDetailDataArr,$idDirection,$kWHSSearch->szWareHouseName,$distanceArr[0]['iDistanceUpToKm'],$idHaulageModel,$idWarehouse,$fWmFactor);
}
else if($_REQUEST['flag']=='show_distance_str')
{

	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$idHaulagePricingModel=$_REQUEST['id'];
	
	$allHaulageDistanceCountriesArr=$kHaulagePricing->getAllCountriesForDistanceHaulageModel($idHaulagePricingModel);
	?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
		 <th class="noborder" align="left"><strong><?=t($t_base.'fields/countries_included')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/countries_include_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
		</tr>
		<tr>
		 <td class="noborder">
		 <? if(!empty($allHaulageDistanceCountriesArr))
		 	{
		 		foreach($allHaulageDistanceCountriesArr as $allHaulageDistanceCountriesArrs)
		 		{
		 		
		 			echo $allHaulageDistanceCountriesArrs['szCountryName']."<br/>";
		 		}
		 	}?>
		 </td>
		</tr>
	</table>
	<?
}
else if($_REQUEST['flag']=="open_add_distance_popup")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/to');
		$tofrom=t($t_base.'fields/from');
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/from');
		$tofrom=t($t_base.'fields/to');
	}
	?>
		<div id="update_distance_form_add_edit">
		<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:808px;height:400px;margin-bottom: 10px;overflow-y: auto;padding: 15px;margin-top:30px;">
				<p style="margin-bottom:10px;"><b><?=t($t_base.'messages/add_distance_pricing_for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$formto?> <?=$kWHSSearch->szWareHouseName?></b></p>
				<div id="update_distance_form">
				<?
					update_distance_form(array(),$idDirection,$idWarehouse,$idHaulageModel,0,$mode);
				?>
				</div>			
			</div>
		</div>	
		</div>
	<?
}
else if($_REQUEST['flag']=="add_counrty_string_sort")
{
	$counttries_arr=array();
	$countriesArrStr=$_REQUEST['countriesArr'];
	$mode=$_REQUEST['mode'];
	if(!empty($countriesArrStr))
	{
		$counttries_arr=explode(",",$countriesArrStr);
		sort($counttries_arr);
	}
	?>
	<select  style="width: 140px;height: 100px;margin-bottom:5px;" multiple="multiple" id="szCourtriesNameStr" name="distanceArr[szCourtriesNameStr]" onclick="delete_str_countries('<?=$mode?>')">
		<? if(!empty($counttries_arr))
		{
			foreach($counttries_arr as $counttries_arrs)
			{
				$szCountryName=$kConfig->getCountryName($counttries_arrs);
				?>
				<option value="<?=$counttries_arrs?>"><?=$szCountryName?></option>
				<?
			}
		}?>
	</select>
	<?
	
}
else if($_REQUEST['flag']=="remove_added_countries_from_dropdown")
{
	$counttries_arr=array();
	$countriesArrStr=$_REQUEST['countriesArr'];
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	if(!empty($countriesArrStr))
	{
		$counttries_arr=explode(",",$countriesArrStr);
	}
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);

	$maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE_FOR_COUNTRY_INCLUDED__');


	$allCountriesArr=$kHaulagePricing->getAllCountriesInDefinedDistance($maxDistance,$kWHSSearch->szLatitude,$kWHSSearch->szLongitude);
	
	//print_r();
	?>
	<select size="1" name="distanceArr[szCountry]" id="szCountry" style="width:140px">
	<?php
		if(!empty($allCountriesArr))
		{
			foreach($allCountriesArr as $allCountriesArrs)
			{
				if((($idDirection=='1' && $allCountriesArrs['iActiveFromDropdown']!='0') || ($idDirection=='2' && $allCountriesArrs['iActiveToDropdown']!='0')) && !in_array($allCountriesArrs['id'],$counttries_arr))
				{
					?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
					<?php
				}
			}
		}
	?>
</select>
	<?
	
}
else if($_REQUEST['flag']=="add_distance")
{
	$idDirection=$_REQUEST['distanceArr']['idDirection'];
	$idWarehouse=$_REQUEST['distanceArr']['idWarehouse'];
	$idHaulageModel=$_REQUEST['distanceArr']['idHaulageModel'];
	$mode=$_REQUEST['distanceArr']['mode'];
	
	update_distance_form(array(),$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="edit_distance")
{

	$id=$_REQUEST['id'];

	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$mode=$_REQUEST['mode'];
	$iPriorityLevel=$_REQUEST['iPriorityLevel'];
	
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/to');
		$tofrom=t($t_base.'fields/from');
		$towith=t($t_base.'fields/with');
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/from');
		$tofrom=t($t_base.'fields/to');
		//$towith=t($t_base.'fields/to');
	}
		
		$arrDistance[]=$kHaulagePricing->loadDistance($id);
		
	?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:808px;height:400px;margin-bottom: 10px;overflow-y: auto;padding: 15px;margin-top:30px;">
				<p style="margin-bottom:10px;"><b><?=t($t_base.'title/edit_pricing_for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$towith?> <?=strtolower(t($t_base.'fields/up_to'))?> <?=number_format((int)$arrDistance[0]['iDistanceUpToKm'])?> <?=t($t_base.'fields/km')?> <?=$formto?>  <?=$kWHSSearch->szWareHouseName?></b></p>
				<div id="update_distance_form">
				<?
					update_distance_form($arrDistance,$idDirection,$idWarehouse,$idHaulageModel,$id,$mode,$iPriorityLevel);
				?>
				</div>			
			</div>
		</div>	
		<?

}
else if($_REQUEST['flag']=="update_distance")
{

	$id=$_REQUEST['distanceArr']['id'];
	$idDirection=$_REQUEST['distanceArr']['idDirection'];
	$idWarehouse=$_REQUEST['distanceArr']['idWarehouse'];
	$idHaulageModel=$_REQUEST['distanceArr']['idHaulageModel'];
	$mode=$_REQUEST['distanceArr']['mode'];
	
	$arrDistance[]=$kHaulagePricing->loadDistance($id);
	update_distance_form(array(),$idDirection,$idWarehouse,$idHaulageModel,$id,$mode);
}
else if($_REQUEST['flag']=="copy_counrty_string_sort")
{
	$iCopyDistanceCountries=$_REQUEST['iCopyDistanceCountries'];
	$szDistanceArr=$kHaulagePricing->getAllCountriesForDistanceHaulageModel($iCopyDistanceCountries);
	$mode=$_REQUEST['mode'];
	if(!empty($szDistanceArr))
	{
		foreach($szDistanceArr as $szDistanceArrs)
		{
			$idCountriesArr[]=$szDistanceArrs['idCountry'];
			$szCountriesNameArr[$szDistanceArrs['idCountry']]=$szDistanceArrs['szCountryName'];
		}
		$idCountriesStr=implode(",",$idCountriesArr);		
	}
	
	?>
	<select  style="width: 140px;height: 100px;margin-bottom:5px;" multiple="multiple" id="szCourtriesNameStr" name="distanceArr[szCourtriesNameStr]" onclick="delete_str_countries('<?=$mode?>')">
		<? if(!empty($szCountriesNameArr))
		{
			foreach($szCountriesNameArr as $key=>$szCountriesNameArrs)
			{
				$szCountryName=$kConfig->getCountryName($idCountriesStrs);
				?>
				<option value="<?=$key?>"><?=$szCountriesNameArrs?></option>
				<?
			}
		}?>
	</select>
	<script>
		$("#szCountriesStr").attr('value','<?=$idCountriesStr?>');
		</script>
	<?
	
}
else if($_REQUEST['flag']=="remove_copy_countries_from_dropdown")
{
	$iCopyDistanceCountries=$_REQUEST['iCopyDistanceCountries'];
	$szDistanceArr=$kHaulagePricing->getAllCountriesForDistanceHaulageModel($iCopyDistanceCountries);
	
	$idWarehouse= $_REQUEST['idWarehouse'];
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);

	$maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE_FOR_COUNTRY_INCLUDED__');

	
	$allCountriesArr=$kHaulagePricing->getAllCountriesInDefinedDistance($maxDistance,$kWHSSearch->szLatitude,$kWHSSearch->szLongitude);
	
	$idDirection=$_REQUEST['idDirection'];
	//print_r($szDistanceArr);
	if(!empty($szDistanceArr))
	{
		foreach($szDistanceArr as $szDistanceArrs)
		{
			$idCountriesArr[]=$szDistanceArrs['idCountry'];
			$szCountriesNameArr[$szDistanceArrs['idCountry']]=$szDistanceArrs['szCountryName'];
		}
		$idCountriesStr=implode(",",$idCountriesArr);		
	}
	
	
	//print_r($allCountriesArr);
	?>
	<select size="1" name="distanceArr[szCountry]" id="szCountry" style="width:140px">
	<?php
		if(!empty($allCountriesArr))
		{
			foreach($allCountriesArr as $allCountriesArrs)
			{
				if((($idDirection=='1' && $allCountriesArrs['iActiveFromDropdown']!='0') || ($idDirection=='2' && $allCountriesArrs['iActiveToDropdown']!='0')) && !in_array($allCountriesArrs['id'],$idCountriesArr))
				{
					?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
					<?php
				}
			}
		}
	?>
</select>
	<?
}
else if($_REQUEST['flag']=="open_copy_distance_popup")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id= $_REQUEST['id'];
	?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:547px;">
			<?=copy_distance_popup(0,$idWarehouse,$idDirection,$idHaulageModel,$id)?>
		</div>
		</div>
	<?
}
else if($_REQUEST['flag']=="show_copy_distance_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	$idDirection=$_REQUEST['idDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$haulageWarehouse=$_REQUEST['haulageWarehouse'];
	$haulageDirection=$_REQUEST['haulageDirection'];
	if($haulageDirection!=$idDirection || $haulageWarehouse!=$idWarehouse)
		$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'distance');
	//print_r($haulagePricingZoneDataArr);
	distance_data_list($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=="copy_distance_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,'4',$idDirection,'distance');
	//print_r($haulagePricingZoneDataArr);
	distance_data_list($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=='ChangeStatusPopup')
{

	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	cfs_pricing_model_list($haulagePricingModelArr);
	//pricingHaulage($haulagePricingModelArr,$_REQUEST['idForwarder']);	
	//linkpricingHaulageModel($haulagePricingModelArr,$_REQUEST['idForwarder'],$_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
	//try_it_out_haulage($_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
}
else if($_REQUEST['flag']=="open_copy_postcode_popup")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id= $_REQUEST['id'];
	?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:547px;">
			<?=copy_postcode_popup($idForwarder,$idWarehouse,$idDirection,$idHaulageModel,$id)?>
		</div>
		</div>
	<?
}
else if($_REQUEST['flag']=="show_copy_postcode_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	$idDirection=$_REQUEST['idDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$haulageWarehouse=$_REQUEST['haulageWarehouse'];
	$haulageDirection=$_REQUEST['haulageDirection'];
	if($haulageDirection!=$idDirection || $haulageWarehouse!=$idWarehouse)
		$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'postcode');
	//print_r($haulagePricingZoneDataArr);
	postcode_data_list($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=="copy_postcode_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,'3',$idDirection,'postcode');
	//print_r($haulagePricingZoneDataArr);
	postcode_data_list($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=="showModelData")
{
	//$status = sanitize_all_html_input($_POST['status']);
	//$content = $kAdmin->currentHaulages(!$status);
	//haulageListing($content);
	//$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	//cfs_pricing_model_list($haulagePricingModelArr);
	$total = 0;
	$active = 0;
	$total = $kAdmin->fetchTotalPricingHaulage($_POST['haulageWarehouse'],$_POST['idDirection'],$_POST['idHaulageModel']);
	$active  = $kAdmin->fetchActivePricingHaulage($_POST['haulageWarehouse'],$_POST['idDirection'],$_POST['idHaulageModel']);
	echo $total."/".$active;
}else if($_REQUEST['flag']=="open_copy_city_popup")
{
	$idDirection=$_REQUEST['idDirection'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$id= $_REQUEST['id'];
	?>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:547px;">	
		<?=copy_city_popup($idForwarder,$idWarehouse,$idDirection,$idHaulageModel,$id)?>
		</div>
		</div>
	<?
}
else if($_REQUEST['flag']=="show_copy_city_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	$idDirection=$_REQUEST['idDirection'];
	$idHaulageModel=$_REQUEST['idHaulageModel'];
	$haulageWarehouse=$_REQUEST['haulageWarehouse'];
	$haulageDirection=$_REQUEST['haulageDirection'];
	if($haulageDirection!=$idDirection || $haulageWarehouse!=$idWarehouse)
		$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'szCity');
	//print_r($haulagePricingZoneDataArr);
	city_data_list($haulagePricingZoneDataArr);
}
else if($_REQUEST['flag']=="copy_city_data")
{
	$idForwarder=$_REQUEST['idForwarder'];
	$idCountry=$_REQUEST['idCountry'];
	$szCity=$_REQUEST['szCity'];
	$idWarehouse=$_REQUEST['idWarehouse'];
	$idDirection=$_REQUEST['idDirection'];
	
	$haulagePricingZoneDataArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,'2',$idDirection,'szCity');
	//print_r($haulagePricingZoneDataArr);
	city_data_list($haulagePricingZoneDataArr);
}
else
{	

	$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
	//print_r($haulagePricingModelArr);
	pricingHaulage($haulagePricingModelArr,$_REQUEST['idForwarder']);
	
	linkpricingHaulageModel($haulagePricingModelArr,$_REQUEST['idForwarder'],$_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
	
	try_it_out_haulage($_REQUEST['haulageWarehouse'],$_REQUEST['idDirection']);
}
?>