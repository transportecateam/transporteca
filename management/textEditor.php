<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__CUSTOMER_SITE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Site Text - Customer Site";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/textEditor/";
validateManagement();


?>
<script type="text/javascript" src="https://www.transporteca.com/evp/framework.php?div_id=evp-4d592f06387a1830b3133580debd5fc7&id=ZXhwbGFpbi1ob3ctdHJhbnNwb3J0ZWNhLXdvcmtzLTEubXA0&v=1352324580&profile=default"></script>
<div id="ajaxLogin" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container" >
<div class="popup">
	<br/>
	<p align="center"><a href="javascript:void(0)" id="removeData" class="button1" onclick="editTextEditor(mode,value)" ><span><?=t($t_base.'fields/edit');?></span></a> <a href="javascript:void(0)" id="deleteTextEditor" class="button1" onclick="deleteTextEditor(mode,id);"><span><?=t($t_base.'fields/delete');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); 
        
        $kConfig = new cConfig();
        $languageArr=$kConfig->getLanguageDetails('','','','','',true);
        ?> 
	<script type="text/javascript">
		setup();
	</script>
	<div class="hsbody-2-right">
		<div style="padding-bottom: 20px;">
		<div id="error"></div>
			<form style="width:520px;">
                            <span>
                                <select style="width:40%;" id="idLanguage" name="arrSelect[idLanguage]" onchange="selectTextEditorNew();"> 
                                    <?php 
                                    if(!empty($languageArr))
                                    {
                                        foreach($languageArr as $languageArrs)
                                        {?>
                                            <option value="<?php echo $languageArrs['id'];?>" <?php if($languageArrs['id']==__LANGUAGE_ID_ENGLISH__){?> selected <?php }?>><?php echo $languageArrs['szName'];?></option>
                                        <?php }
                                    }?>
                                </select>
                            </span>
                            <span >
                                <select style="width:60%;float:right;" name='arrSelect[select]' id="select" onchange="selectTextEditorNew();">
                                    <option selected="selected"><?=t($t_base.'fields/select_text_for_editing_customer');?></option>
                                    <option value="__BENEFITS_OF_SIGN_IN__"><?=t($t_base.'fields/benefits_of_sign_in');?></option>
                                    <option value="__OUR_COMPANY__"><?=t($t_base.'fields/our_company');?></option>
                                    <option value="__STARTING_NEW_TRADES__"><?=t($t_base.'fields/starting_new_trades');?></option>
                                    <!--<option value="__WORK_FOR_FORWARDER__"><?=t($t_base.'fields/work_for_forwarder');?></option>-->
                                    <option value="__PRIVACY_POLICY__"><?=t($t_base.'fields/privacy_notice');?></option>
                                    <option value="__EXPLAIN_INTRODUCTION__"><?=t($t_base.'fields/explain_introduction');?></option>					
                                    <option value="__BOOKING_TERMS__"><?=t($t_base.'fields/booking_tnc_english');?></option> 
                                    <option value="__INSURANCE_TERMS__"><?=t($t_base.'fields/insurance_tnc_english');?></option>
                                    <option value="__TRANSPORTECA_COOKIE__"><?=t($t_base.'fields/cookie_policy_english');?></option> 
                                    <option value="__LOCAL_SEARCH_POPUP__"><?=t($t_base.'fields/local_search_popup_english');?></option>
                                </select>
                            </span>
			</form>	
			<div style="float: right;margin-top: -24px;">
				<a class="button1" id="preview" style="opacity:0.4;"><span><?=t($t_base.'fields/preview');?></span></a>
				<a class="button1" id="save" style="opacity:0.4;"><span><?=t($t_base.'fields/save');?></span></a>
			</div>
		</div>	
		<div id="viewContent" style="width:100%;height:80%;">
        	<textarea name="content" id="content" class="myTextEditor" style="width:100%;height:60%;"></textarea>
		</div>
		<div>
			<div id="previewContent" class="preview_box" style="display:none;">
			</div>
		</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>