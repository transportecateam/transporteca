<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$t_base = "HaulaugePricing/";
$kForwarder = new cForwarder();
$kWhsSearch = new cWHSSearch();
$kHaulagePricing = new cHaulagePricing();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
if(!empty($_REQUEST['tryoutArr']))
{	
	$haulagePricingData = $kHaulagePricing->getHaulagePricingDetais_tryitout($_REQUEST['tryoutArr']);
	$kWhsSearch->load($_REQUEST['tryoutArr']['idWarehouse']);
	$idForwarder=$kWhsSearch->idForwarder;
	if(!empty($kHaulagePricing->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		      {
			      ?>
			      <li><?=$values?></li>
			      <?php 
		      }
		?>
		</ul>
		</div>
		<?
	}
	else
	{
		echo "SUCCESS||||" ;
		echo display_haulage_tryitout_calculation_details_admin($haulagePricingData,$idForwarder);
	}
}
else if($mode=='COUNTRY_NAME')
{
    $szLatitude = sanitize_all_html_input(trim($_REQUEST['szLatitude']));
    $szLongitude = sanitize_all_html_input(trim($_REQUEST['szLongitude']));
    
    $kHaulagePricing = new cHaulagePricing();
    $szCountryCode = $kHaulagePricing->getCountryCodeByHaulageZone($szLongitude,$szLatitude);
    $kConfig = new cConfig();
    /*
    $kWHSSearch = new cWHSSearch();
    $kConfig = new cConfig();
    $url = __YAHOO_GEOCODE_API_URL__.$szLatitude.',+'.$szLongitude.'&gflags=R&flags=J&appid='.__YAHOO_GEOCODE_API_KEY__;
    $geocode = $kWHSSearch->curl_get_file_contents($url);

    $response = json_decode($geocode);

    $szCountryCode = $response->ResultSet->Results[0]->countrycode;
    $szCountryName = $response->ResultSet->Results[0]->countrycode;
     * 
     */
    $idCountry = $kConfig->getCountryName(false,$szCountryCode);

    if($idCountry>0)
    {
        echo "SUCCESS||||".$idCountry;
    }
    else
    {
        echo "ERROR||||";
    }
}
?>