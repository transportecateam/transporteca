<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once(__APP_PATH__.'/inc/functions.php');
require_once(__APP_PATH__.'/inc/vat_functions.php');
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement(); 
$batchNumber = sanitize_all_html_input($_GET['id']);
$idForwarder = sanitize_all_html_input($_GET['user']);
 
if(!empty($batchNumber) && ((int)$idForwarder>0))
{
    if(isset($_GET['flag']))
    {
        $flag=sanitize_all_html_input($_GET['flag']);
    }
    $flag = 'PDF';
    $version="Forwarder - Transporteca Handling fee Invoices".$idForwarder;
    $kForwarder= new cForwarder();
    $kBooking = new cBooking();
 
    $filename = getBookingHandlingFeeInvoice($idForwarder,$batchNumber,$flag);

    if($flag=='PDF')
    {
        download_booking_pdf_file($filename);
        die;
    }
} 

?>