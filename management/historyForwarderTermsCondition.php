<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__HISTORY__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Site Text - History ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/history/";
validateManagement();
$historyLogTable=$kAdmin->historyViewTNC();

?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
		<table cellpadding="0" id="history_table"  cellspacing="0" border="0" class="format-4" width="100%">
		<tr>
			<th width="20%" align="center" valign="top"><?=t($t_base.'fields/change_date')?> </th>
			<th align="center" valign="top"><?=t($t_base.'fields/description')?></th>
			<th width="30%" align="center" valign="top"><?=t($t_base.'fields/user');?> </th>
		</tr>
		<? $count = 1;
		if($historyLogTable!=array())
		{	
			foreach($historyLogTable as $logTable)
			{	
				echo 
				"
				<tr id ='history_tr_".$count."'  onclick=\"select_history('history_tr_".$count."','".$logTable['id']."')\" title='".$logTable['szComment']."'>
				<td>".date('d. F Y',strtotime($logTable['dtCreatedOn']))."</td>
				<td>".($logTable['szVersionURL']?"<a href ='".$logTable['szVersionURL']."'  target='_blank'>".$logTable['szDescription']."</a>":$logTable['szDescription'])."</td>
				<td>".$logTable['szFirstName'].' '.$logTable['szLastName']."</td>			
				</tr>	
				";
				$count++;
			}
		}
		else
		{		
			echo "<tr><td colspan='3' align='center'><b>".t($t_base.'fields/no_data_found')."</b></td></tr>";
		}
		?>
		</table>
		<div><embed id="pdfviewer" src="" width="710" height="375"></div>
		<div id="showMe" style="width: 720px;height: auto;padding-top: 20px;padding-left: 20px;"></div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>