<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );

$t_base = "management/forgotPassword/";
$t_base_error="Error";
$successFlag=false;
if(!empty($_POST))
{
	if($kAdmin->forgotPassword($_POST['szEmail']))
	{
		$successFlag=true;
	}
}
if(!empty($_REQUEST['redirect_url']))
{
	$_POST['szRedirectUrl'] = sanitize_all_html_input(trim($_REQUEST['redirect_url'])); ;
}

?>
<div id="forgot_password">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification" style="width:300px;">
<h5><strong><?=t($t_base.'messages/forgot_password_heading');?></strong></h5>
			<?php
	if($successFlag)
	{?>
		<p><?=t($t_base.'messages/forgot_password_success_msg1');?> <?=$_POST['szEmail']?><?=t($t_base.'messages/forgot_password_success_msg2');?></p>
		<br />
		<p align="center"><a href="javascript:void(0);" class="button1" onclick="showHide('ajaxLogin');"><span><?=t($t_base.'fields/close')?></span></a></p>
	<?			
	}
	else
	{
			if(!empty($kAdmin->arErrorMessages)){
			?>
			<div id="regError" class="errorBox ">
			<div class="header"><?=t($t_base_error.'/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
				foreach($kAdmin->arErrorMessages as $key=>$values)
				{
				?><li><?=$values?></li>
				<?php	
				}
			?>
			</ul>
			</div>
			</div>
			<?php }?>
			<form name="forgotPassword" action="javascript:void(0)" id="forgotPassword" method="post">
			<p><?=t($t_base.'messages/type_your_email_address_user_profile');?></p><br>
			<div class="oh">
				<p class="fl-25"><?=t($t_base.'fields/signin_email');?></p>
				<p class="fl-65"><input type="text" onkeyup="on_enter_key_press(event,'forgot_passward_popup','')" name="szEmail" id="szEmail" value="<?=$_POST['szEmail']?>"/></p>
			</div>
			<br />
			<input type="hidden" name="szRedirectUrl"  value="<?=$_POST['szRedirectUrl']?>">
			<p align="center"><a href="javascript:void(0);" class="button1" onclick="showHide('ajaxLogin');"><span><?=t($t_base.'fields/close')?></span></a> <a href="javascript:void(0);" class="button1" onclick="forgot_passward_popup();"><span><?=t($t_base.'fields/send_me_pass');?></span></a></p>
			</form>
		<?
		}
		?>	
			</div>
		</div>
	</div>