<?php 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Tasks - Pending Tray";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/pendingTray/"; 
validateManagement();   
$kWhsSearch = new cWHSSearch();
 
$idBooking = (int)$_GET['booking_id'];
$tempsearchARy = $kWhsSearch->getSearchedDataFromTempData($idBooking);
$szCourierCalculationLogString = $tempsearchARy['szCourierCalculationLogString'];
?> 
<div id="hsbody">   
 <?php 
 echo $szCourierCalculationLogString;
 ?>
</div>	
<?php  
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>