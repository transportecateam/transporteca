<?php  
ob_start();
if (!isset( $_SESSION )) 
{
    session_start();
}
ini_set('apc.cache_by_default',false); 
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php"); 
 
validateManagement();

$mode = sanitize_all_html_input(trim($_REQUEST['mode'])); 
$kPendingTray = new cPendingTray();
$kBooking = new cBooking();

if($mode=='AUTO_LOAD_PENDING_TRAY_TASK_LIST')
{
    $webhookResponse = file_get_contents('php://input');  
    $jsonPostAry = json_decode($webhookResponse,true);
    $transportecaAdminAry = array();
    if(!empty($jsonPostAry))
    {
        foreach($jsonPostAry as $jsonPostArys)
        { 
            $transportecaAdminAry[] = $jsonPostArys['value'];
        }
    } 
    $idBookingFile = sanitize_all_html_input(trim($_REQUEST['file_id']));  
    $iTotalNumTasksDisplayed = sanitize_all_html_input(trim($_REQUEST['total_num_task']));  
     
    /*
    * Updating different file status on Load of Pending Tray page.
    */
    $kPendingTray->updateBookingTaskStatusOnload();
  
    $searchTaskAry = array(); 
    if(!empty($transportecaAdminAry))
    {
        $searchTaskAry['idFileOwnerAry'] = $transportecaAdminAry; 
        $_SESSION['sessTransportecaTeamAry'] = $transportecaAdminAry ; 
    }
    else
    {
        $searchTaskAry['idFileOwnerAry'][0] = 0;
        $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
    }
    
    $incompleteTaskAry = $kPendingTray->getAllBookingWithOpenedTask($searchTaskAry);    
    $finalPendingTrayListAry = array();
    $finalPendingTrayListAry = display_all_peding_tasks($incompleteTaskAry,$idBookingFile,true,false,false,true); 
    $jSonString = '';
    if(!empty($finalPendingTrayListAry))
    {  
        $kPendingTray = new cPendingTray();
        $jSonString = $kPendingTray->jsonEncode($finalPendingTrayListAry);  
    }  
    ob_end_clean();
    echo $jSonString;
    die; 
} 