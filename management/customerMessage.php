<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
define('PAGE_PERMISSION','__MESSAGE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Send Messages - To Customers ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$kConfig = new cConfig();

$custmsgFlag=checkManagementPermission($kAdmin,"__MESSAGE_TO_CUSTOMERS__",2);
?>
	<div id="ajaxLogin"></div>
	<div id="hsbody-2">
	<? require_once(__APP_PATH__ ."/layout/admin_messages_left_nav.php"); ?>
		
	<div id="content_body" class="hsbody-2-right">
            <?php if($custmsgFlag){?>
		<?=customerMessageView()?>
            <?php }?>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>