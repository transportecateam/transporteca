<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__FORWARDER_SITE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Site Text - Forwarder Site ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/textEditor/";
validateManagement();
?>
<div id="ajaxLogin" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container" >
<div class="popup">
	<br/>
	<p align="center"><a href="javascript:void(0)" id="removeData" class="button1" onclick="editTextEditor(mode,value)" ><span><?=t($t_base.'fields/edit');?></span></a> <a href="javascript:void(0)" id="deleteTextEditor" class="button1" onclick="deleteTextEditor(mode,id);"><span><?=t($t_base.'fields/delete');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<script type="text/javascript">
		setup();
	</script>
	<div class="hsbody-2-right">
		<div style="padding-bottom: 20px;">
		<div id="error"></div>
			<form style="width:300px;">
				<select name='arrSelect[select]' id="select" onchange="selectTextEditorNew(this.value);" style="width: 213px;">
					<option selected="selected"><?=t($t_base.'fields/select_text');?></option>
					<option value="__FORWARDER_PRIVACY_POLICY__"><?=t($t_base.'fields/privacy_notice_forwarder');?></option>
					<option value="__FORWARDER_COMPANY_INTRODUCTION__"><?=t($t_base.'fields/company_page');?></option>
					<option value="__FORWARDER_PRICING_SERVICING_INTRODUCTION__"><?=t($t_base.'fields/pricing_servicing');?></option>
				</select>
                            <input type="hidden" id="idLanguage" name="arrSelect[idLanguage]" value="<?php echo __LANGUAGE_ID_ENGLISH__;?>"> 
			</form>	
			<div style="float: right;margin-top: -24px;">
				<a class="button1" id="preview" style="opacity:0.4;"><span><?=t($t_base.'fields/preview');?></span></a>
				<a class="button1" id="save" style="opacity:0.4;"><span><?=t($t_base.'fields/save');?></span></a>
			</div>
		</div>	
		<div id="viewContent" style="width:100%;height:80%;">
        	<textarea name="content" class="myTextEditor" id="content" style="width:100%;height:60%;"></textarea>
		</div>
		<div>
			<div id="previewContent" style="display:none;" class="preview_box">
			</div>
		</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>