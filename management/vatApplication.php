<?php 
/**
  *  admin--- maintanance -- contries
  */
define('PAGE_PERMISSION','__SYSTEM_VAT_APPLICATION__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Variables - Countries";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/vat_functions.php");

require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/country/";
$kConfig = new cConfig();
//$countryDetails=$kAdmin->countries($idAdmin);
$kVatApplication = new cVatApplication();
 
?>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery.1.8.2.js"></script>
<script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script>  

<div id="ajaxLogins" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container" >
<div class="popup">
	<h5>
	<?=t($t_base.'messages/remove_link');?></h5>
	<p id="view_message"><?=t($t_base.'messages/would_you_like');?> </p>
	<br/>
	<p align="center"><a href="javascript:void(0)" id="removeData" class="button1" ><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancelDelete();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div class="hsbody-2-right"> 
            <div style="min-height:200px;" id="vat_appliaction_right"> 
              <?php echo forwarderVatTradesHTML($kVatApplication);?>
            </div>
	</div>
</div>
    <input type="hidden" name="deleteCountryForwardersId" id="deleteCountryForwardersId" value="">
    <input type="hidden" name="deleteCountryForwardersTradesId" id="deleteCountryForwardersTradesId" value="">
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>