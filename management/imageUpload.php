<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
?>
<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-1.7.1.js"></script>
<?php
$flag=$_REQUEST['flag'];
//print_r($_REQUEST);
$f = fopen("test.log", "a");
fwrite($f, "test");
fwrite($f, print_r(__BASE_STORE_JS_URL__,true));
 
$valid_formats = array("jpg", "png", "gif", "bmp");

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	if($flag == 'explainLevel2')
	{
		$tmp = $_FILES['file_upload']['tmp_name'];		
		$name = $_FILES['file_upload']['name'];
		$size = $_FILES['file_upload']['size'];
		
		$szImagePrefix = "LVL2";		 
		$path=__EXPLAIN_LEVEL2_IMAGES__;
	}
	else if($flag=='explainLevel3')
	{ 
		$tmp = $_FILES['file_upload']['tmp_name'];		
		$name = $_FILES['file_upload']['name'];
		$size = $_FILES['file_upload']['size'];
	
		$szImagePrefix = "LVL3"; 
		$path=__EXPLAIN_LEVEL3_IMAGES__;
	}
	else if($flag=='explainLevel2_og')
	{
		$tmp = $_FILES['og_file_upload']['tmp_name'];		
		$name = $_FILES['og_file_upload']['name'];
		$size = $_FILES['og_file_upload']['size'];
				 
		$szImagePrefix = "LVL2_OG"; 
		$path=__EXPLAIN_LEVEL2_IMAGES__;
	} 
	fwrite($f, strlen($name)."\n");
	fwrite($f, strlen($name));
	
	if(strlen($name))
	{
		$fileParts = pathinfo($name);
		if(in_array($fileParts['extension'],$valid_formats))
		{							
				$actual_image_name = $szImagePrefix."_".str_replace(" ", "_", $fileParts['filename'])."-".time().".".$fileParts['extension'];
				 
				if(move_uploaded_file($tmp, $path."/".$actual_image_name))
				{ 
					if($flag=='explainLevel2')
					{
						echo '<img src="'. __MAIN_SITE_HOME_PAGE_URL__.'/image.php?img='.$actual_image_name.'&w=100&h=75&temp=explainLevel2" alt="'.$actual_image_name.'" ><a href="javascript:void(0);" onclick="delete_explain_level2_image(\''.$actual_image_name.'\',\'temp\');" title="Delete Image"><img src="'.__BASE_STORE_IMAGE_URL__.'/delete-icon.png"></a>';
					
				?>
					<script type="text/javascript" >
						$("#szUploadFileName").attr('value','<?php echo $actual_image_name;?>');
						$("#iFileUpload").attr('value','1');
						$("#file_upload").attr('style','display:none;');
					</script>
				<?php	
					}
					else if($flag=='explainLevel3')
					{
						echo '<img src="'. __MAIN_SITE_HOME_PAGE_URL__.'/image.php?img='.$actual_image_name.'&w=100&h=75&temp=explainLevel3" alt="'.$actual_image_name.'" ><a href="javascript:void(0);" onclick="delete_explain_level3_image(\''.$actual_image_name.'\',\'temp\');" title="Delete Store Image"><img src="'.__BASE_STORE_IMAGE_URL__.'/delete-icon.png"></a>';
					?>								
					<script type="text/javascript" >
						$("#szUploadFileName").attr('value','<?php echo $actual_image_name;?>');
						$("#iFileUpload").attr('value','1');
						$("#file_upload").attr('style','display:none;');
					</script>
				<?php	
					}
					else if($flag=='explainLevel2_og')
					{
						echo '<img src="'. __MAIN_SITE_HOME_PAGE_URL__.'/image.php?img='.$actual_image_name.'&w=100&h=75&temp=explainLevel2" alt="'.$actual_image_name.'" ><a href="javascript:void(0);" onclick="delete_explain_level2_image(\''.$actual_image_name.'\',\'temp\',false,\'OPEN_GRAPH\');" title="Delete Image"><img src="'.__BASE_STORE_IMAGE_URL__.'/delete-icon.png"></a>';
					?>								
					<script type="text/javascript" >
						$("#szUploadOGFileName").attr('value','<?php echo $actual_image_name;?>');
						$("#iOGFileUpload").attr('value','1');
						$("#og_file_upload").attr('style','display:none;');
					</script>
				<?php	
					}
				}
				else
					echo "failed";
			}
			else
			echo "Invalid file format..";	
	}				
	else
	{
		echo "Please select image..!";
	}	
	exit;
}
?>