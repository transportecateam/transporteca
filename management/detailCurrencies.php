<?php 
/**
  *  admin--- maintanance -- currencies 
  */
define('PAGE_PERMISSION','__SYSTEM_CURRENCY__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | System Variables - Currencies";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base="management/currency/";
$exchangeRates=$kAdmin->excangeRates($idAdmin);

?>
<div id="ajaxLogin" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup">
	<h5>
	<?=t($t_base.'messages/remove_link');?></h5>
	<p id="view_message"><?=t($t_base.'messages/would_you_like');?> </p>
	<br/>
	<p align="center"><a href="javascript:void(0)" id="removeData" class="button1" ><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="deleteExchangeRates(0);"><span><?=t($t_base.'fields/cancel');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
	<div id="loader" class="loader_popup_bg" style="display:none;">
		<div class="popup_loader"></div>
		<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification" style="padding:10px;width:290px;">
		<p><?=t($t_base.'messages/plz_wait_while_we_process');?></p>
		<br/><br/>
		<p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
		</div>	</div>			
					
	</div>	
	<div class="hsbody-2-right">
		<div id="table">
			<?=exchangeRatesView($exchangeRates); ?>		
		</div>		
		<div style="padding-top: 10px;">
		<div style="float: right;">
				<a class="button1" id="deleteExchangeRates" style="opacity:0.4;"><span><?=t($t_base.'fields/cancel');?></span></a>	
				<a class="button1" id="editExchangeRates" style="opacity:0.4;"><span><?=t($t_base.'fields/edit');?></span></a>
			</div>
			<div style="clear: both;"></div>
			<br />
		<div id="error"></div>
			<form id="sendExchange">
			<div id="editCurrency">
				<?php echo exchangeView();?>
			</div>
			</form>
			
		</div>		
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>