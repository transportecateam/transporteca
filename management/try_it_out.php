<?php
define('PAGE_PERMISSION','__LCL_SERVICES_TRY_IT_OUT__');
ob_start(); 	
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$szMetaTitle="Transporteca | Pricing - Try it out ";
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
validateManagement();
//$t_base="SelectService/";
$idForwarder =$_SESSION['forwarder_id'];
$kConfig=new cConfig();
$number = 1;
$kBooking = new cBooking();
$_SESSION['try_it_out_searched_data']  = array();
unset($_SESSION['try_it_out_searched_data']);
// initialising arrays
$countryAry=array();
$serviceTypeAry=array();
$weightMeasureAry=array();
$cargoMeasureAry = array();
$timingTypeAry=array();

$countryAry = $kConfig->getAllCountries();
$currency = $kConfig->getBookingCurrency(false,true);
// geting all service type 
$serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');

// geting all Timing type 
$timingTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_TIMING_TYPE__');

// geting all weight measure 
$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

// geting all cargo measure 
$cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');

// geting all custom clearance 
$customClearAry=$kConfig->getConfigurationLanguageData("__TABLE_ORIGIN_DESTINATION__"); 
constantApiKey();

$t_base = "BulkUpload/";
?>

<script type="text/javascript">
$().ready(function() {	
	$("#szOriginPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szDestinationPostCode").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
	
	$("#szOriginCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#szDestinationCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});	
	
    $("#datepicker1").datepicker();
});
</script><style type="text/css">
      #map_canvas { height: 100% }     
      .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
      .map-content p{font-size:12px !important;}
      .map_note p{font-size:12px;font-style:italic;display:inline-block;margin:1px 0 18px;}
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=<?php echo __GOOGLE_MAP_V3_API_KEY__?>&sensor=true">
    </script>

<style type="text/css" media="screen">
 .layout { padding: 50px; font-family: Georgia, serif; }
 .layout-slider { margin-bottom: 20px; width: 50%; }
 .layout-slider-settings { font-size: 12px; padding-bottom: 10px; }
 .layout-slider-settings pre { font-family: Courier; }
</style>
<script type="text/javascript">
$().ready(function() {
/*
$("#Transportation_pop").attr("style","display:block;");
$.post(__JS_ONLY_SITE_BASE__+"/ajax_processing.php",function(result){		
	//setTimeout(function(){
	    $("#Transportation_pop").attr('style','display:none;');		
		result_ary = result.split("||||");
		if(result_ary[0]=='SUCCESS')
		{
			$("#hsbody-2").html(result_ary[1]);
		}
		if(result_ary[0]=='NO_RECORD_FOUND')
		{
			$("#customs_clearance_pop1").html(result_ary[1]);
		}	
		document.title = 'Transporteca | International Shipping Search Results';		
  	//},1000);		
   });	
   */
});
</script>
 <script type="text/javascript">
	  var destinationIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/red-marker.png";
      var originIcon = '<?=__MAIN_SITE_HOME_PAGE_URL__?>' + "/images/blue-marker.png";
	  var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
    var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(pipe_line_string)
    {
    	addPopupScrollClass();
    	var result_ary = pipe_line_string.split("|");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	initialize(result_ary);
    }
    	  
	function initialize(result_ary) {
	  var olat = result_ary[0];
	  var olang = result_ary[1];
	  var dlat = result_ary[2];
	  var dlang = result_ary[3];
	  var distance = result_ary[4];
	  var origin_address = result_ary[5];
	  var destination_address = result_ary[6];
	  
	  var origin = new google.maps.LatLng(olat, olang);      
      var destination = new google.maps.LatLng(dlat, dlang);
	  var myLatLng = new google.maps.LatLng(olat, olang);
	  var myOptions = {
		zoom: 6,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	 $("#distance_div").html('<?=t($t_base.'fields/distance_to_warehouse');?> ' + dollarAmount(distance) + ' ' +'Km');
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(olat, olang),
		new google.maps.LatLng(dlat, dlang)   
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
	  
	  var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"Origin"
	  }); 

	  var oaddress = '<div class="map-content"><p>Your cargo:<br>'+origin_address+'</p></div>';
	  var daddress = '<div class="map-content"><p>Forwarder\'s warehouse:<br>'+destination_address+'</p></div>';

	  var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker, 
		'click', 
		infoCallback(oinfowindow, originmarker)
	  );
      //oinfowindow.open(map,originmarker);        

	  var destinationmarker = new google.maps.Marker({
		  position: destination,
		  map: map,
		  icon: destinationIcon,
		  visible: true,
		  title:"Destination"
	  });    
	  
	  var dinfowindow = new google.maps.InfoWindow();
	  dinfowindow.setContent(daddress);
	  google.maps.event.addListener(
		destinationmarker, 
		'click', 
		infoCallback(dinfowindow, destinationmarker)
	  );
	  //dinfowindow.open(map,destinationmarker);        
}
</script>

<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="Transportation_pop" style="display:none;"></div>
<div id="dang-cargo-pop" class="help-pop right">
</div>
<div id="customs_clearance_pop_right" class="help-pop right">
</div>
<div id="Transportation_pop" style="display:none;"></div>
<div id="hsbody-2">

<div id="map_popup_div" style="display:none;">		
	<div id="popup-bg"></div>
	 <div id="popup-container"  class="popup-scroller">			
	  <div class="popup map-popup">
	    <div id="distance_div" style="font-weight:bold;margin-bottom:5px;"></div>
	  	<div id="map_canvas" style="width:500px;height:500px;border: 1px solid #C4BDA1;"></div>
	  	<div class="map_note">
		<p><?=t($t_base.'fields/notes');?>: <?=t($t_base.'fields/google_map_notes');?></p>
		</div>
	  	<p align="center"><a href="javascript:void(0);" onclick="showHide('map_popup_div');$('body').removeClass('popupscroll');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
	  </div>
   </div>
</div>
  
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>

<div id="customs_clearance_pop" class="help-pop">
</div>
		<?php 
			require_once( __APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php" ); 
			//$t_base = "home/homepage/";
		?>
	<div class="hsbody-2-right">
		<div id="regError" class="errorBox " style="display:none;">
		</div>
<form action="<?=__SELECT_SERVICES_URL__?>" method="post" id="landing_page_form">
	<!--  <h5><?=t($t_base.'fields/pagetitle_try_it_out');?> </h5>-->
	<div class="oh">
		<div class="fl-90 pos-rel">
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/transportation');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/TRANSPORTATION_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/TRANSPORTATION_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-45 pos-rel">
					<select size="1" name="searchAry[idServiceType]" id="szTransportation" class="transportation-select">
						 <?
						 	if(!empty($serviceTypeAry))
						 	{
						 		foreach($serviceTypeAry as $serviceTypeArys)
						 		{
						 			?>
						 				<option value="<?=$serviceTypeArys['id']?>" <? if($postSearchAry['idServiceType']==$serviceTypeArys['id']){?> selected <? }?>><?=$serviceTypeArys['szDescription']?></option>
						 			<?
						 		}
						 	}
						 ?>
					</select><br>
					<div id="szTransportation_div" style="display:none;"></div>
				</p>
				<p class="fl-10 pos-rel">&nbsp;&nbsp;&nbsp;<?=t($t_base.'fields/currency');?></p>
				<p class="fl-10 pos-rel">
					<select size="1" name="searchAry[szCurrency]" class="transportation-select">
						 <?
						 	if(!empty($currency))
						 	{
						 		foreach($currency as $currency)
						 		{
						 			?>
						 				<option value="<?=$currency['id']?>" <? if('USD'==$currency['szCurrency']){?> selected <? }?>><?=$currency['szCurrency']?></option>
						 			<?
						 		}
						 	}
						 ?>
					</select><br>
					<div id="szTransportation_div" style="display:none;"></div>
				</p>
			</div>
			<div class="oh">
                            <p class="fl-33"><?=t($t_base.'fields/customs_clearance');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CUSTOM_CLEARANCE_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CUSTOM_CLEARANCE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p> 
                            <p class="fl-20 pos-rel checkbox-ab">
                                <input type="checkbox" <?php if($postSearchAry['iOriginCC'] == 1) {?>checked<? }?> id="iOriginCC" name="searchAry[idCC][0]" value="1"  /> Origin
                            </p>
                            <p class="fl-20 pos-rel checkbox-ab">
                                <input type="checkbox" <?php if($postSearchAry['iDestinationCC'] == 2) {?>checked<? }?> id="iDestinationCC" name="searchAry[idCC][1]" value="2"  /> Destination
                            </p> 
                            <br>
                            <div id="iCustomClearance_div" style="display:none;"></div>
			</div>
		</div>
	</div>
	
	<div class="oh">
		<div class="tryitnow">
			<div class="oh">				
				<p class="fl-33"><br/><?=t($t_base.'fields/from');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/COUNTRY_FROM_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/COUNTRY_FROM_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-25 f-size-12">	
					<?=t($t_base.'fields/country');?><br/>
					<select name="searchAry[szOriginCountry]" id="szOriginCountry" size="1" onchange="activatePostCodeField(this.value,'szOriginCity','szOriginPostCode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')">
                                            <option value="">Select Country</option>
                                         <?php
                                            if(!empty($countryAry))
                                            {
                                                foreach($countryAry as $countryArys)
                                                {
                                                    ?>
                                                    <option value="<?=$countryArys['id']?>" <? if($postSearchAry['idOriginCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,15)?></option>
                                                    <?
                                                }
                                            }
					 ?>
					</select>
					<br>
					<span id="szOriginCountry_div" style="display:none;"></span>
				</p>
				<p class="fl-20 f-size-12">
				<?=t($t_base.'fields/city');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CITY_POST_CODE_1_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CITY_POST_CODE_1_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a><br/>
				<input <? if(empty($postSearchAry['idOriginCountry'])){?>disabled<? }?> name="searchAry[szOriginCity]" id="szOriginCity" value="<?=$postSearchAry['szOriginCity']?>" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')"  size="18">
				
				
				<p class="fl-20 f-size-12">
				<?=t($t_base.'fields/postcode');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/CITY_POST_CODE_2_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CITY_POST_CODE_2_TOOL_TIP_TEXT');?>','dang-cargo-pop',event);" onmouseout="hide_tool_tip('dang-cargo-pop')">&nbsp;</a><br/>
				<input <? if(empty($postSearchAry['idOriginCountry'])){?>disabled<? }?> name="searchAry[szOriginPostCode]" id="szOriginPostCode" value="<?=$postSearchAry['szOriginPostCode']?>" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  size="18"></p>
			</div>
			<div class="oh">
                            <p class="fl-33"><?=t($t_base.'fields/to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/COUNTRY_TO_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/COUNTRY_TO_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
                            <p class="fl-25">				
                                <select name="searchAry[szDestinationCountry]" id="szDestinationCountry" size="1" onchange="activatePostCodeField(this.value,'szDestinationCity','szDestinationPostCode','<?=t($t_base.'fields/type_name');?>','<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" >
                                    <option value="">Select Country</option>
                                    <?php
                                       if(!empty($countryAry))
                                       {
                                           foreach($countryAry as $countryArys)
                                           {
                                               ?>
                                               <option value="<?=$countryArys['id']?>" <? if($postSearchAry['idDestinationCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,15)?></option>
                                               <?php
                                           }
                                       }
                                    ?>
                                </select>
                            </p>

                            <p class="fl-20">				
                            <input <? if(empty($postSearchAry['idDestinationCountry'])){?> disabled="disabled" <? }?> value="<?=$postSearchAry['szDestinationCity']?>" name="searchAry[szDestinationCity]" id="szDestinationCity" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')"  size="18"></p>

                            <p class="fl-20">				
                            <input <? if(empty($postSearchAry['idDestinationCountry'])){?> disabled="disabled" <? }?> value="<?=$postSearchAry['szDestinationPostCode']?>" name="searchAry[szDestinationPostCode]" id="szDestinationPostCode" type="text" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  size="18"></p>
			</div>
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/timing');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/TIMING_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/TIMING_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<p class="fl-33">
					<select name="searchAry[idTimingType]" id="szTiming" onchange="change_date('<?=date('d/m/Y')?>','<?=t($t_base.'fields/date_format');?>')">
						 <?
						 	if(!empty($timingTypeAry))
						 	{
						 		foreach($timingTypeAry as $timingTypeArys)
						 		{
						 			?>
						 				<option value="<?=$timingTypeArys['id']?>" <? if($postSearchAry['idTimingType']==$timingTypeArys['id']){?> selected <? }?>><?=$timingTypeArys['szDescription']?></option>
						 			<?
						 		}
						 	}
						?>
					</select>
				</p>
				<p class="fl-20"><input id="datepicker1" name="searchAry[dtTiming]" onfocus="blank_me(this.id,'<?=t($t_base.'fields/date_format');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/date_format');?>')" type="text" value="<? if($postSearchAry['dtTimingDate']!='0000-00-00 00:00:00' && !empty($postSearchAry['dtTimingDate'])) {?><?=date('d/m/Y',strtotime($postSearchAry['dtTimingDate']))?><? }else {?><?=date('d/m/Y')?><? }?>">
				</p>
			</div>
		</div>
	</div>
	<div class="oh">
		<div >
			  <div class="oh">
				<p class="fl-33"><br /><?=t($t_base.'fields/dimensions');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<div class="fl-65 dimensions-field">				
						<span class="f-size-12">
							<?=t($t_base.'fields/length');?><br/>
							<input type="hidden" name="searchAry[idCargo][<?=$number?>]" value="<?=$idCargo?>">
							<input type="text" pattern="[0-9]*" name="searchAry[iLength][<?=$number?>]" value="<?=$fLength?>" id= "iLength<?=$number?>" /><br>
							<span id="iLength<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
						<span class="f-size-12">
							<?=t($t_base.'fields/width');?><br/>
							<input type="text" pattern="[0-9]*"  name="searchAry[iWidth][<?=$number?>]" value="<?=$fWidth?>" id= "iWidth<?=$number?>" /><br>
							<span id="iWidth<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-14  cross_seperator">&nbsp;<br/>X</span>
						<span class="f-size-12">
							<?=t($t_base.'fields/height');?><br/>
							<input type="text" pattern="[0-9]*" name="searchAry[iHeight][<?=$number?>]" value="<?=$fHeight?>" id= "iHeight<?=$number?>" />
							<span id="iHeight<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-12">
						&nbsp;<br/>
							<select name="searchAry[idCargoMeasure][<?=$number?>]">
								 <?
								 	if(!empty($cargoMeasureAry))
								 	{
								 		foreach($cargoMeasureAry as $cargoMeasureArys)
								 		{
								 			?>
								 				<option value="<?=$cargoMeasureArys['id']?>" <? if($cargoAry[$number]['idCargoMeasure']==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</span>
						<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
						<span class="last f-size-12">
							<?=t($t_base.'fields/quantity');?><br/>
							<input type="text" pattern="[0-9]*" name="searchAry[iQuantity][<?=$number?>]" value="<?=$cargoAry[$number]['iQuantity']?>" id= "iQuantity<?=$number?>" /><br>
							<span id="iQuantity<?=$number?>_div" style="display:none;"></span>
						</span>
					</div>
			</div>			
			<div class="oh">
				<p class="fl-33" style="padding:2px 0 0;"><?=t($t_base.'fields/weight');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WEIGHT_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WEIGHT_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>&nbsp;</a></p>
				<div class="fl-65 dimensions-field">					
						<span>
							<input type="text" pattern="[0-9]*" name="searchAry[iWeight][<?=$number?>]" onkeyup="on_enter_key_press(event,'validateLandingPageForm_forwarder','<?=$function_params?>');" value="<?=$fWeight?>" id= "iWeight<?=$number?>" />
							<span id="iWeight<?=$number?>_div" style="display:none;"></span>
						</span>
						<span style="width:30.5%">
							<select size="1" name="searchAry[idWeightMeasure][<?=$number?>]" style="margin-left: 13px;width: 78px;">
								 <?
								 	if(!empty($weightMeasureAry))
								 	{
								 		foreach($weightMeasureAry as $weightMeasureArys)
								 		{
								 			?>
								 				<option value="<?=$weightMeasureArys['id']?>" <? if($cargoAry[$number]['idWeightMeasure']==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</span>
					</div>			
			</div>
			<div id="cargo">
				<div id="cargo1"></div>
			</div>
			<input type="hidden" name="searchAry[szBookingMode]" id="szBookingMode" value="<?=$booking_mode?>">
			<input type="hidden" name="searchAry[hiddenPosition]" id="hiddenPosition" value="1">
			<input type="hidden" name="searchAry[hiddenPosition1]" id="hiddenPosition1" value="1">			
			<div class="oh">
			<p class="fl-49"><a href="javascript:void(0);" onclick="add_more_cargo()"><?=t($t_base.'fields/add_more');?> </a></p>
			<div id="remove" align="right" style="display:none;" class="fl-49"><a href="javascript:void(0);" onclick="remove_cargo();"><?=t($t_base.'fields/remove_cargo');?></a></div>
			</div>
		</div>
	</div>
	<div class="oh">
		<p align="center">
		<a href="javascript:void(0);" onclick="clear_try__it_out_form('landing_page_form')" class="button2"><span><?=t($t_base.'fields/clear');?> </span></a>
		<a href="javascipt:void(0);" onclick="return validateLandingPageForm_Management('landing_page_form');" class="button1"><span><?=t($t_base.'fields/test');?> </span></a>
		</p>
	</div>	
	</form>
	<div>
		<p><h3><?=t($t_base.'fields/customer_view');?></h3></p>
		<div  id="bottom_search_result" style="border:1px solid #D0D0D0;height:100px;">
		</div>
		<p><?=t($t_base.'fields/click_on_price');?>
		</p>
		<div id="calculations_details_div">
		<?
			//echo display_dtd_forwarder_price_details($searchedAry,$t_base);
		?>
		</div>
	</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>