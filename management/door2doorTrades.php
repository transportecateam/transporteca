<?php
/**
  *BILLING DETAILS
  */
define('PAGE_PERMISSION','__DOOR_TO_DOOR_TRADES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Courier - Mode & Transports";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$kCourierServices = new cCourierServices();
$t_base="management/modeTransport/";
validateManagement();

?>
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script>  
    
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="hsbody-2">
	<?php require_once(__APP_PATH__ ."/layout/contentMaintainanceLeftNav.php"); ?>
	<div class="hsbody-2-right">
            <p style="padding-bottom: 5px;"><?php echo t($t_base.'title/dtd_trades_heading');?></p>
            <h4><strong><?php echo t($t_base.'title/trades_only_offers_dtd');?></strong></h4>
            <div style="min-height:300px;" id="mode_transport_div"> 
                <?php echo addDisplayCouierModeTransport($kCourierServices,true);?>
            </div>
            <hr class="cargo-top-line">
            <div id="courier_excluded_trade_list">
            <?php 
                echo LCLExcludedTradesList();
            ?>
            </div>
	</div>
	<input type="hidden" name="deleteTruckiconId" id="deleteTruckiconId" value="">
	<input type="hidden" name="idPrimarySortKey" id="idPrimarySortKey" value="">
	<input type="hidden" name="idPrimarySortValue" id="idPrimarySortValue" value="ASC">
	<input type="hidden" name="idOtherSortKey" id="idOtherSortKey" value="">
        <input type="hidden" name="iDoor2DoorTrades" id="iDoor2DoorTrades" value="1">
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>