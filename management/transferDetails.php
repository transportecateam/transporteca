<?php
session_start();
if(isset($_GET))
{
    if( !defined( "__APP_PATH__" ) )
    define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    require_once( __APP_PATH__ . "/inc/constants.php" );
    require_once(__APP_PATH__.'/inc/functions.php');
    require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
    validateManagement();
 
    if($_GET['id'] && $_GET['user'])
    {
        $id=$_GET['id'];
        $user=$_GET['user'];
        $flag=$_GET['flag'];
        $flag = 'PDF';
        $filename=getForwarderTransferPDF($user,$id,$flag,true);
        
        download_booking_pdf_file($filename);
        die;
    }
    /*if(isset($_POST['flag']))
    {
            $flag=sanitize_all_html_input($_POST['flag']);
            $mode=sanitize_all_html_input($_POST['mode']);
            if($flag=='DELETE')
            {
                //@unlink(__APP_PATH__."/forwarders/html2pdf/".$mode);
                echo success;
            }

    }*/
}
