<?php 
define('PAGE_PERMISSION','__PERFORMANCE__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Feedback - Performance";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/operations/performance/";
validateManagement();
$kBooking = new cBooking;
$kWHSSearch = new cWHSSearch;
$manageVar=(int)$kWHSSearch->getManageMentVariableByDescription(__FORWARDER_RATING__);
$forwarderIdName=$kBooking->forwarderIdName($idAdmin);
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
	
	<div class="hsbody-2-right">
		<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
			<tr>
				<td  align="center" valign="top"></td>
				<td align="center" width="5%" valign="top"><b><?=$manageVar?>M</b></td>
			<?
			for( $i = 0; $i <= 12; $i++ )
			{
	    		$year = DATE('Y',strtotime("-$i month"));
				$months[ $i ] = DATE( 'M', mktime( 0, 0, 0, DATE('m')-$i,1, $year ) );
			?>	
			
				<td align="center" width="5%" valign="top"><b><?=strtoupper($months[ $i ]);?></b></td>
			<?
			}
			$i = 0
			?>
			</tr>
			
			
				<? $arrFeed[$i++][0] = t($t_base.'fields/remind')?> 
				<? $arrFeed[$i++][0] = t($t_base.'fields/rated_remind_booking')?>
				<? $arrFeed[$i++][0] = t($t_base.'fields/rated_booking')?>
				
				<? $arrFeed[$i++][0] = t($t_base.'fields/rating_submitted')?>
				<? $arrFeed[$i++][0] = t($t_base.'fields/avg_rating')?>
				<? $arrFeed[$i++][0] = t($t_base.'fields/review_submitted')?>
				<? $arrFeed[$i++][0] = t($t_base.'fields/feedback_submitted')?>
				
				<? $arrFeed[$i++][0] = t($t_base.'fields/on_time_score')?> 
				<? $arrFeed[$i++][0] = t($t_base.'fields/customer_Service_score')?>
				
			<?	
					$showArr  =array(0,1,2,7,8);
					$i = 0;
					$operationPerformances6M=$kBooking->findOperation6mPerformance($manageVar);
					if($operationPerformances6M!= array())
					{
						foreach($operationPerformances6M as $key=>$value)
						{
							$arrFeed[$i][1] = (($i == 4)?($value?number_format((float)$value,2):number_format(0,2)):($value?number_format((int)$value):number_format(0))).(in_array($i,$showArr)?'%':'');
							$i++;
						}
					}	
					//print_R($arrFeed);
			?>
			
			<?
				
				for( $i = 0; $i <= 12; $i++ )
				{
					$j=0;
		    		$year = DATE('Y',strtotime("-$i month"));
					$months[ $i ] = DATE( 'm', mktime( 0, 0, 0, DATE('m')-$i,1, $year ) );
					$operationPerformances=$kBooking->findOperationFeedbackPerformance($months[ $i ],$year);
					
					if($operationPerformances!= array())
					{
						foreach($operationPerformances as $key=>$value)
						{	
							 $arrFeed[$j][$i+2] = (($j == 4)?($value?number_format((float)$value,2):number_format(0,2)):($value?number_format((int)$value):number_format(0))).(in_array($j,$showArr)?'%':'');
							$j++;
						}
					}	
					
				}
				//print_r($arrFeed);
				if($arrFeed != array())
				{
					foreach($arrFeed as $key=>$value)
					{ 
						$newKey = $key;
					?>
					<tr>
					<? foreach($value as $key=>$value)
					   {
						?>
						<td <?=($key==0)?'':'align="right"';?>><?=$value?></td>
					<?
						} ?>	
					</tr>
				<? 
						if($newKey == 2 || $newKey == 6 || $newKey == 8)
						{
							echo "<tr><td style='padding:13px;'></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
						}
					}
				}
				//echo '<tr><td>';
				$showArr  =array();
				if($forwarderIdName!=array())
				{
					$i=0;
					foreach($forwarderIdName as $Forwarder)
					{
						$showArr[$i++][0] = returnLimitData($Forwarder['szDisplayName'],18)." rating";
					}
				}
				?>
				
				
			<?		$i=0;
					foreach($forwarderIdName as $Forwarder)
					{
						
					$rating=$kWHSSearch->getForwarderFeedback6MAvgRating($Forwarder['id'],$manageVar);
					$showArr[$i++][1] =  number_format((float)$rating['rating'],2);
					//echo "</br>";
					}
					//print_r($showArr);
			?>
				
			<?
				for( $i = 0; $i <= 12; $i++ )
				{	$year = DATE('Y',strtotime("-$i month"));
					$months[ $i ] = DATE( 'm', mktime( 0, 0, 0, DATE('m')-$i,1, $year ) );
					$j = 0;
					foreach($forwarderIdName as $Forwarder)
					{
						//echo $j;
						$rating=$kWHSSearch->getForwarderFeedbackAvgRating($Forwarder['id'],$months[ $i ],$year);
						$showArr[$j][$i+2] =  number_format((float)$rating['rating'],2);
						$j++;
					}
				}
				foreach($showArr as $data)
				{
					echo "<tr>";
					foreach($data as $key=>$value)
					{	
						echo "<td ".($key==0?'':'style="text-align:right;"').">".($key==0?$value:((int)$value==0?'':$value))."</td>";
					}
					echo "</tr>";
				
				}
			?>
			
			</tr>
		</table>
		</div>
	</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>			