<?php 
/**
  *  Send Message to customer
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();

$kAdmin = new cAdmin();
$kUser = new cUser();
$t_base="management/AdminMessages/";

if($_REQUEST['sendflag']=='ConfirmFlag')
{
	$customerIdArr=$_REQUEST['szCustomerArr'];
	//print_r($customerIdArr);
	if(!empty($customerIdArr))
	{
		$idBatch=$kAdmin->getMaxBatchId();
		$customerIdArr=explode(',',$customerIdArr);
		for($k=0;$k<count($customerIdArr);++$k)
		{
			$customerArrId=array();
			$customerArrId=explode("_",$customerIdArr[$k]);
			$message='';
			$kUser->getUserDetails($customerArrId[0]);
			if($customerArrId[0]>1)
			{
				$idCustomer=$kAdmin->getActiveCustomerDetails($kUser->szEmail);
				if((int)$idCustomer)
				{
					$kUser->getUserDetails($idCustomer);
				}
				else
				{
					$idCustomer=$customerArrId[0];
					$kUser->getUserDetails($idCustomer);
				}
			}
			else
			{
				$idCustomer=$customerArrId[0];
			}
			if((int)$idCustomer>0)
			{
				$replaceAry=array();
				$replaceAry['szFirstName']=$kUser->szFirstName;
				$replaceAry['szLastName']=$kUser->szLastName;
				$replaceAry['szEmail']=$kUser->szEmail;
				$replaceAry['szCompanyName']=$kUser->szCompanyName;
				$szSubject=$_REQUEST['szSubject'];
				$szBody=nl2br(urldecode(base64_decode($_REQUEST['szMessage'])));
				
				
				$message .=$szBody;
				if (count($replaceAry) > 0)
			    {
			        foreach ($replaceAry as $replace_key => $replace_value)
			        {
			            $message = str_replace(trim($replace_key), $replace_value,$message);
			        }
			    }
			    
				if (count($replaceAry) > 0)
			    {
			        foreach ($replaceAry as $replace_key => $replace_value)
			        {
			            $szSubject = str_replace(trim($replace_key),$replace_value,$szSubject);
			        }
			    }
			    
			    $kAdmin->saveAdminMessages($kUser->szEmail,$idCustomer,$message,$szSubject,1,$idBatch);
			}
			
		}
		?>
		<script>
		$(location).attr('href','<?=__MANAGEMENT_MESSAGE_CUSTOMER_URL__?>');
		</script>
		<?
		exit();
	}
}
else
{
if($_REQUEST['showflag']=='Preview')
{
//print_r(urldecode(base64_decode($_REQUEST['szMessage'])));
?>
	<div>
	<?
			 ob_start();  
    		 require_once(__APP_PATH_ROOT__.'/layout/email_header.php');
    		 ?>
    		 <p><?=nl2br(urldecode(base64_decode($_REQUEST['szMessage'])));?></p>
    		 <?
    		 include(__APP_PATH_ROOT__.'/layout/email_footer.php');
		?>
</div>	
<?
}
else if($_REQUEST['showflag']=='HtmlVar')
{
	getAllHtmlVariable('user');
}
else
{
	//print_r($_REQUEST);
	$total=0;
	$iNewsUpdate=$_REQUEST['iNewsUpdate'];
	$iActiveFlag=$_REQUEST['iActiveFlag'];
	$data['szSubject']=$_REQUEST['szSubject'];
	$data['szMessage']=$_REQUEST['szMessage'];
	
	$idCountryArr='';
	if($_REQUEST['szCountryArr']=='All')
	{
		$idCountryArr='All';
	}
	else if($_REQUEST['szCountryArr']!='null' && $_REQUEST['szCountryArr']!='')
	{
		$szCountrySelArr=$_REQUEST['szCountryArr'];
		$idCountryArr=implode(',',$_REQUEST['szCountryArr']);
	}
	$iNewsUpdateStr='';
	if($iNewsUpdate!='')
	{
		$iNewsUpdateArr=explode("_",$iNewsUpdate);
		if(!empty($iNewsUpdateArr))
		{
			if(count($iNewsUpdateArr)>1)
				$iNewsUpdateStr=implode("','",$iNewsUpdateArr);
			else
				$iNewsUpdateStr=$iNewsUpdateArr[0];	
				
			$iNewsUpdateStr="'".$iNewsUpdateStr."'";
		}
	}
	$iActiveFlagStr='';
	if($iActiveFlag!='')
	{
		$iActiveFlagArr=explode("_",$iActiveFlag);
		if(!empty($iActiveFlagArr))
		{
			if(count($iActiveFlagArr)>1)
				$iActiveFlagStr=implode("','",$iActiveFlagArr);
			else
				$iActiveFlagStr=$iActiveFlagArr[0];	
				
			$iActiveFlagStr="'".$iActiveFlagStr."'";
		}
	}
	
	if($iNewsUpdateStr!='' && $iActiveFlagStr!='')
	{
		$customerIdArr=$kAdmin->getAllActiveCustomer($idCountryArr,$iNewsUpdateStr,$iActiveFlagStr,$_REQUEST['flag'],$data);
	}
	$total=count($customerIdArr);
		
if($_REQUEST['szCountryArr']=='All')
{	
?>
<script>
$('#szCountry option').attr('selected', 'selected');
</script>
<? }?>
<script>
$("#user_count").html('<?=$total?>');
</script>
<?
if(!empty($kAdmin->arErrorMessages)){
	$t_base_error = "Error";
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kAdmin->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<? }else if(empty($kAdmin->arErrorMessages) && $_REQUEST['flag']=='sendMail'){	
	if(!empty($customerIdArr))
		$customerIdStr=implode(",",$customerIdArr);
	?>
	<script>
	addPopupScrollClass();
	</script>
	<div id="send_confirm_popup">
	<div id="popup-bg"></div>
	<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification">
		<h5><?=t($t_base.'fields/send_message');?></h5>
		<p><?=t($t_base.'fields/are_you_sure');?> <?=$total?> <?=t($t_base.'fields/recipients');?> ?</p>
		<br>
		 <p align="center">
		  <input type="hidden" name="szCustomerArr" id="szCustomerArr" value="<?=$customerIdStr?>">
		 	<a href="javascript:void(0);" class="button1" id="yes" onclick="confirm_send_msg_to_user();"><span><?=t($t_base.'fields/yes');?></span></a>
			<a href="javascript:void(0);" class="button2"  id="no" onclick="cancelPreviewMsg('send_confirm_popup');"><span><?=t($t_base.'fields/no');?></span></a>
		</p>
		</div>
	</div>
	</div>
	<? }
		else if(empty($kAdmin->arErrorMessages) && $_REQUEST['flag']=='CUSTOMER_EMAIL_LIST')
		{?>
			<script>
			addPopupScrollClass();
			</script>
			<div id="customer_email_list">
			<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification" style="width:350px;">
				<h5><?=t($t_base.'fields/receivers_total');?> <?=$total?></h5>
				<div class="multi-payment-transfer-popup" style="height:200px;">
				<?
					if(!empty($customerIdArr))
					{
						for($k=0;$k<count($customerIdArr);++$k)
						{
							$customerArrId=array();
							$customerArrId=explode("_",$customerIdArr[$k]);
							$kUser->getUserDetails($customerArrId[0]);
							echo "<p>".$kUser->szEmail."</p>";
						}
					}
				?>
				</div>
				<br/>
				<p align="center"><a href="javascript:void(0);" class="button1"  id="no" onclick="cancelPreviewMsg('customer_email_list');"><span><?=t($t_base.'fields/close');?></span></a></p>
				</div>
			</div>
			</div>	
		<?		
		}
	if($_REQUEST['flag']!='CUSTOMER_EMAIL_LIST')
	{	
	?>
<?=customerMessageView()?>
<? }}}?>