<?php
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" );
  
$ctr_org = 1;
$ctr = 1;
$ctr_size = 1;
$original_name_arr = array();
$images_arr = array();
$file_size_arr = array();
    
$iUploadedByDragDrop = (int)$_REQUEST['iUploadedByDragDrop'];
$iAttachmentCounter = (int)$_REQUEST['iAttachmentCounter'];
 
$szUlExtraClass = "";
$szIdSuffix = "";
$bShowAttachmentLabel = false;
 
if($iUploadedByDragDrop==1)
{
    $idCrmEmail = (int)$_REQUEST['idCrmEmail'];
    $formAryName = trim($_REQUEST['formAryName']);
 
    if($formAryName=='addEditRemindEmailTemplate')
    {
        $szArrayName = 'addEditRemindEmailTemplate';
        $bShowAttachmentLabel = true;
        $szUlExtraClass = " add-edit-popup-ul";
    }
    else if($formAryName=='addCrmOperationAry')
    {
        $szArrayName = 'addCrmOperationAry'; 
        $szIdSuffix = "_".$idCrmEmail;
        $szUlExtraClass = " add-edit-popup-ul_".$idCrmEmail;
    }
    else if($formAryName == 'addSnoozeAry')
    {
        $szArrayName = 'addSnoozeAry'; 
        $szUlExtraClass = " add-edit-popup-ul";
    }
    else
    {
        die;
    }
    if(!empty($_FILES['filesToUpload']))
    {
        $image_name = $_FILES['filesToUpload']['name'];
        $tmp_name = $_FILES['filesToUpload']['tmp_name'];
        $file_size = $_FILES['filesToUpload']['size'];
        $type = $_FILES['filesToUpload']['type'];
        $error = $_FILES['filesToUpload']['error'];

        if (($file_size > 5242880))
        {      
            $message = 'File too large. File must be less than 5MB.';  
            die;
        }
        else
        {
            $iOneMbSize = 1048576;
            if($file_size>$iOneMbSize)
            {
                $szFileSize = round_up((float)($file_size/$iOneMbSize),2)." MB";
            }
            else
            {
                $iSizeInMb = round_up((float)($file_size/$iOneMbSize),3);
                $szFileSize = round_up((float)($iSizeInMb*1000),2)." KB";
            }
        }
        ############ Remove comments if you want to upload and stored images into the "uploads/" folder ############# 
        $target_dir = __APP_PATH_EMAIL_ATTACHMENT__;
        $szOriginalFileName = $_FILES['filesToUpload']['name'];
        $target_file = "Attachment_".time().mt_rand(0,999999)."_".$szOriginalFileName;

        $szTargerFullPath = $target_dir."/".$target_file;   
        if(move_uploaded_file($_FILES['filesToUpload']['tmp_name'],$szTargerFullPath))
        {  
            $success_fullly_upload[$ctr] = $szOriginalFileName;
        }  
        $images_arr[$iAttachmentCounter] = $target_file;
        $original_name_arr[$iAttachmentCounter] = $szOriginalFileName;
        $file_size_arr[$iAttachmentCounter] = $szFileSize; 
    } 
}
else
{  
    /*
    * Same file is called from several places so we have created
    */
    if(!empty($_POST['addEditRemindEmailTemplate']))
    {
        $szArrayName = 'addEditRemindEmailTemplate';
        $bShowAttachmentLabel = true;
        $szUlExtraClass = " add-edit-popup-ul";
    } 
    else if(!empty($_POST['addCrmOperationAry']))
    {
        $szArrayName = 'addCrmOperationAry';
        $idCrmEmail = (int)$_POST['addCrmOperationAry']['idCrmEmailLogs']; 
        $szIdSuffix = "_".$idCrmEmail;
        $szUlExtraClass = " add-edit-popup-ul_".$idCrmEmail;
    }
    else if(!empty($_POST['addSnoozeAry']))
    {
        $szArrayName = 'addSnoozeAry'; 
        $szUlExtraClass = " add-edit-popup-ul";
    }

    if(!empty($_POST[$szArrayName]['uploadedFileName']))
    {
        foreach ($_POST[$szArrayName]['uploadedFileName'] as $key=>$value)
        {
            $value = trim($value);
            if(!empty($value))
            {
                $images_arr[$ctr] = $value; 
                $ctr++;
            } 
        }
    }
    if(!empty($_POST[$szArrayName]['originalFileName']))
    {
        foreach ($_POST[$szArrayName]['originalFileName'] as $key=>$value)
        { 
            $value = trim($value);
            if(!empty($value))
            {
                $original_name_arr[$ctr_org] = $value; 
                $ctr_org++;
            }
        }
    }

    if(!empty($_POST[$szArrayName]['originalFileSize']))
    {
        foreach ($_POST[$szArrayName]['originalFileSize'] as $key=>$value)
        { 
            $value = trim($value);
            if(!empty($value))
            {
                $file_size_arr[$ctr_size] = $value; 
                $ctr_size++;
            }
        }
    } 
    if($_POST['image_form_submit'] == 1 && !empty($_FILES['filesToUpload']['name']))
    { 
        foreach($_FILES['filesToUpload']['name'] as $key=>$val)
        { 
            $image_name = $_FILES['filesToUpload']['name'][$key];
            $tmp_name = $_FILES['filesToUpload']['tmp_name'][$key];
            $file_size = $_FILES['filesToUpload']['size'][$key];
            $type = $_FILES['filesToUpload']['type'][$key];
            $error = $_FILES['filesToUpload']['error'][$key];

            if (($file_size > 5242880))
            {      
                $message = 'File too large. File must be less than 5MB.';  
                die;
            }
            else
            {
                $iOneMbSize = 1048576;
                if($file_size>$iOneMbSize)
                {
                    $szFileSize = round_up((float)($file_size/$iOneMbSize),2)." MB";
                }
                else
                {
                    $iSizeInMb = round_up((float)($file_size/$iOneMbSize),3);
                    $szFileSize = round_up((float)($iSizeInMb*1000),2)." KB";
                }
            }
            ############ Remove comments if you want to upload and stored images into the "uploads/" folder ############# 
            $target_dir = __APP_PATH_EMAIL_ATTACHMENT__;
            $szOriginalFileName = $_FILES['filesToUpload']['name'][$key];
            $target_file = "Attachment_".time().mt_rand(0,999999)."_".$szOriginalFileName;

            $szTargerFullPath = $target_dir."/".$target_file;  
            if(move_uploaded_file($_FILES['filesToUpload']['tmp_name'][$key],$szTargerFullPath))
            {  
                $success_fullly_upload[$ctr] = $szOriginalFileName;
            }  
            $images_arr[$ctr] = $target_file;
            $original_name_arr[$ctr_org] = $szOriginalFileName;
            $file_size_arr[$ctr_size] = $szFileSize;
            $ctr++;
            $ctr_org++;
            $ctr_size++;
        } 
    }
}  
//Generate images view
if(!empty($images_arr))
{ 
    $count=0;
    if($iUploadedByDragDrop==1)
    {
        $count = $iAttachmentCounter-1;
    }
    else {?> 
    <ul class="reorder_ul reorder-photos-list<?php echo $szUlExtraClass; ?>"> 
    <?php 
    }
        foreach($images_arr as $key=>$image_src)
        { 
            $count++; 
            $szBasePath = __BASE_URL_EMAIL_ATTACHMENT__."/".$image_src;
            if($image_src=='BOOKING_INVOICE' || $image_src=='BOOKING_NOTICE' || $image_src=='BOOKING_CONFIRMATION')
            {
                if($image_src==='BOOKING_INVOICE')
                {
                    $szAttachmentLinkText = "Booking Invoice";
                }
                else if($image_src==='BOOKING_NOTICE')
                {
                    $szAttachmentLinkText = "Booking Notice";
                }
                else if($image_src==='BOOKING_CONFIRMATION')
                {
                    $szAttachmentLinkText = "Booking Confirmation";
                }
                $szAttachmentLink = "Attachment ".$count.": ".$szAttachmentLinkText;
                $szBasePath = "javascript:void(0);";
            } 
            if($bShowAttachmentLabel)
            {
                if($image_src=='BOOKING_INVOICE' || $image_src=='BOOKING_NOTICE' || $image_src=='BOOKING_CONFIRMATION')
                {
                    $szAttachmentLink .= " (<i>if applicable</i>)";
                }
                else
                {
                    $szAttachmentLink = "Attachment ".$count.": ".$original_name_arr[$key];
                } 
            }
            else
            {
                if($image_src=='BOOKING_INVOICE' || $image_src=='BOOKING_NOTICE' || $image_src=='BOOKING_CONFIRMATION')
                {
                    //@To Do
                }
                else 
                {
                    $szAttachmentLink = "Attachment ".$count.": ".(!empty($file_size_arr[$key])?($original_name_arr[$key]." (".$file_size_arr[$key].")"):$original_name_arr[$key]);
                } 
            }
            ?>  
            <li id="image_li_<?php echo $count; ?>" class="ui-sortable-handle">   
                <a href="<?php echo $szBasePath; ?>" target="_blank" class="image_link"><?php echo $szAttachmentLink; ?></a>  
                &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="closeFile('<?php echo $count; ?>')" class="red_text" style="text-decoration:none;"><strong>X</strong></a> 
                <input type="hidden" name="<?php echo $szArrayName; ?>[uploadedFileName][]" id="uploadedFileName_<?php echo $count;?>" value="<?php echo $image_src; ?>">
                <input type="hidden" name="<?php echo $szArrayName; ?>[originalFileName][]" id="originalFileName_<?php echo $count;?>" value="<?php echo $original_name_arr[$key]; ?>">
                <input type="hidden" name="<?php echo $szArrayName; ?>[originalFileSize][]" id="originalFileSize_<?php echo $count;?>" value="<?php echo $file_size_arr[$key]; ?>">
            </li> 
            <?php  
        }
    if($iUploadedByDragDrop!=1){ 
    ?>
   </ul>         
    <input type="hidden" name="<?php echo $szArrayName; ?>[iAttachmentCounter]" id="iAttachmentCounter<?php echo $szIdSuffix; ?>" value="<?php echo $count; ?>"> 
    <?php
    }
}


                            
?>