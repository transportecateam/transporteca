<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__CUSTOMER_TC__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Site Text - Customer T&C";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/terms_and_condition/";
validateManagement();
$kWHSSearch= new cWHSSearch();
$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_CUSTOMER__);
$publish=$kWHSSearch->selectPublishmentCustomer();
$style='';

if(!$publish)
{
	$style="style='opacity:0.4;'";
}

?>
<div id="ajaxLogins" style="display: none;">
<div id="popup-bg"></div>
<div id="popup-container" >

<div class="popup">
	<p><?=t($t_base."fields/are_you_sure")?></p>
	<br />
	<p align="center"><a href="javascript:void(0)" class="button2" onclick="deleteExchangeRates(2);" ><span><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="deleteTextEditor" class="button1" onclick="deleteTextEditor(mode,id);"><span><?=t($t_base.'fields/delete');?></span></a></p>
</div>
</div>
</div>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" ); ?> 
	<div class="hsbody-2-right">
		<div style="padding-bottom: 20px;">
		<div id="error"></div>
		<div id="showDiv">
			<? //print_r($textEditor); ?>
			<div style="clear:both;"></div>
	
		<div id="showUpdate">
			<?=viewTnc($textEditor,'CUSTOMER',__TEXT_EDIT_TERMS_CUSTOMER__);?>		
				
		</div>	
				
		</div>	
		<div id="text_t_n_C"></div>	
	</div>
	<div id="preview_Publish">
		<a id="add" onclick="editTextEditor('CUSTOMERTNC','0');" style="float: left;cursor: pointer;"><span><?=t($t_base.'fields/add');?></span></a>
		<div style="float: right;">
			<a id="preview" class="button1" onclick="preview_system('TNC_CUSTOMER','preview');" ><span><?=t($t_base.'fields/preview');?></span></a>
			<a id="publish" class="button1" <?if($publish){?>onclick="preview_system('TNC_CUSTOMER','publish');" <? }else{echo $style;} ?>><span><?=t($t_base.'fields/publish');?></span></a>
			<a id="cancel" class="button2" <?if($publish){?>onclick="preview_system('TNC_CUSTOMER','cancel');" <? }else{echo $style;} ?>><span><?=t($t_base.'fields/cancel');?></span></a>
		</div>
		<div style="clear: both;"></div>
		<br />
	
		<div >
			<div id="preview_data" class="preview_box" style="display:none;">
			</div>
		</div>
	</div>	
</div>
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>