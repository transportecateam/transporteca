<?php 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Credit Notes";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/admin_functions.php");

require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );

$kBooking = new cBooking();
//checkAuth();
$idBooking = $_REQUEST['idBooking'];
if($idBooking>0){
$flag=$_REQUEST['flag'];
$pdfhtmlflag=true;
$flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
if($flag=='pdf')
{
    $pdfhtmlflag=false;
}
$kBooking->load($idBooking);
if($kBooking->iFinancialVersion==2)
{
    $bookingInvoicePdf = getCreditNoteConfirmationPdfFileHTML_v2($idBooking,$pdfhtmlflag);
}
else
{
    $bookingInvoicePdf = getCreditNoteConfirmationPdfFileHTML($idBooking,$pdfhtmlflag);
}
if($flag=='pdf')
{
     download_booking_pdf_file($bookingInvoicePdf);
        die;
}}
?>