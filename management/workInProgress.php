<?php
define('PAGE_PERMISSION','__UPLOAD_SERVICE_WORK_IN_PROGRESS__');
ob_start();
session_start();
$display_profile_not_completed_message = true;
ini_set('max_execution_time',1200);
ini_set('memory_limit', '-1');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$szMetaTitle='Upload Services | Work in Progress';
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
$t_base_bulk="BulkUpload/";
$t_base = "management/uploadService/";
//error_reporting (E_ALL);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
validateManagement();
$kUploadBulkService = new cUploadBulkService();
$Export_import = new cExport_Import();
$topTableData=$kUploadBulkService->getAllDataNotApprovedByAdmin('worktopdata');
$bottomTableData=$kUploadBulkService->getAllDataNotApprovedByAdmin('workbottomdata');
$mode = sanitize_all_html_input($_POST['mode']);
if($mode=='UPLOAD_FILE_SUBMISSION')
{
	$id=sanitize_all_html_input($_POST['id']);
	$msg=$kUploadBulkService->uploadBulkServicesRecords($id,$_FILES);
	//echo $msg;
	
	if(!empty($kUploadBulkService->arErrorMessages))
	{	
			?>
				<div id="Error">			
				<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 	
							<div id="regError" class="errorBox" style="display:block;">
							<div class="header"><?=t($t_base_bulk.'title/please_following');?></div>
							<div id="regErrorList">
							<ul>
							<?php
							      foreach($kUploadBulkService->arErrorMessages as $key=>$values)
							      {
							      ?><li><?=$values?></li>
							      <?php 
							      }
							?>
							</ul>
							</div>
							</div>
						</div>
					</div>
				</div>			
		<?php
	
	}
	if($msg=='1')
	{
	?>
					
	<div id="popup-bg"></div>
	<div id="popup-container">
		<div class="popup">
		<strong><?=t($t_base.'title/file_upload_err_msg')?></strong>
		<br/><br/>
		<p><?=t($t_base.'messages/file_error_file_msg')?></p><br />
		<? if($kUploadBulkService->errorRowCount>0){?>
		<p><?=t($t_base.'messages/we_found');?> <?=$kUploadBulkService->errorRowCount?> 
				<? if($kUploadBulkService->errorRowCount>1){?>
				<?=t($t_base.'messages/lines_error_uploaded');?>
				<? }else{?>
				<?=t($t_base.'messages/line_error_uploaded');?><? }?></p><br/>
				<? }?>
		<p align="center"><a href="<?=__MANAGEMENT_WORK_IN_PROGRESS_URL__?>" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
		</div>
	</div>	 
	<?php	
	}
	else if($msg=='MAX_ROW_EXCEED')
	{
            $maxRowCount=__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__;
	
	?>
		<div id="popup-bg"></div>
	<div id="popup-container">
		<div class="popup">
		<strong><?=t($t_base.'error/file_max_row_count_error_2')?></strong>
		<br/><br/>
		<p><?=t($t_base_bulk.'error/file_max_row_count_error_1')?> <?=number_format((int)__MAX_ROW_COUNT_UPLOAD_FILE_EXCEEDS__)?> <?=t($t_base_bulk.'error/file_max_row_count_error_3')?> </p><br />
		<p align="center"><a href="<?=__MANAGEMENT_WORK_IN_PROGRESS_URL__?>" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
		</div>
	</div>
	<?php }
	else{
	header("location:".__MANAGEMENT_WORK_IN_PROGRESS_URL__);
	}
}
?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?>
	<div id="loader" class="loader_popup_bg" style="display:none;">
		<div class="popup_loader"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="padding:10px;width:290px;">
				<p><?=t($t_base.'messages/plz_wait_while_we_process');?></p>
				<br/><br/>
				<p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
			</div>	
		</div>					
	</div>
	
	<div class="hsbody-2-right">
		<div id="open_processing_popup"></div>
		<p align="left"><b><?=t($t_base.'title/approved_submissions_awaiting_completion')?></b></p>
		<br/>
		<p style="TEXT-ALIGN:JUSTIFY"><?=t($t_base.'title/the_below_submissions_have_been_approved')?></p>				
		<div id="waiting_for_approved_list">
			<?php submission_awaiting_review_and_pricing_waiting($topTableData,'__WTOP__'); ?>
	    </div>
	    <br />
	    	<div style="clear: both;"></div>
	    	
	    	<h4><b><?=t($t_base.'title/completed_submissions_awaiting_final')?></b></h4>
	    	<br />
			<div id="awaiting_for_approved_list">
				<?php submission_awaiting_review_and_pricing_waiting($bottomTableData,'__WBOTTOM__'); ?>
		    </div>
	<iframe style="border:0" name="hssiframe" id="hssiframe" width="0px" height="0px"></iframe>
	<iframe style="border:0" name="hssiframe" id="hiframe" width="0px" height="0px"></iframe>	    
	</div>	
</div>
	<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>