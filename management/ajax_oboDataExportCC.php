<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kExport_Import=new cExport_Import();
$t_base = "OBODataExport/";
$t_base_error="management/customerclearence/";
if(isset($_POST['mode']))
{
	$mode = sanitize_all_html_input($_POST['mode']);
	SWITCH($mode)
	{
		CASE 'SHOW_ACTIVE_CC':
		{
			$flag = 0;
			$idWhFrom = sanitize_all_html_input((int)$_POST['idWhFrom']);
			$idWhTo = sanitize_all_html_input((int)$_POST['idWhTo']);
			$idSelectedField = sanitize_all_html_input($_POST['idSelectedField']);
			$bottomContent  = $kAdmin->oboCCModelAdmin($_POST['currentCC']);
			$count = $kAdmin->oboCCModelAdminCount($_POST['currentCC']);
			$arrData = showCustomClearenceNewArr($bottomContent);
			oboCCModelAdminFunction($arrData,$count);
			echo '|||||';
			echo display_admin_obo_cc_bottom_html($t_base,true);
			if($idWhFrom!=0 || $idWhTo!=0)
			{
				echo "<script type='text/javascript'>showCustomClearence('".$idSelectedField."','".$idWhFrom."','".$idWhTo."');</script>";
			}
			BREAK;
		}
		CASE 'SHOW_ALL_CC':
		{
			$flag = 1;
			$idWhFrom = sanitize_all_html_input((int)$_POST['idWhFrom']);
			$idWhTo = sanitize_all_html_input((int)$_POST['idWhTo']);
			$idSelectedField = sanitize_all_html_input($_POST['idSelectedField']);
			$bottomContent  = $kAdmin->oboCCModelAdmin($idAdmin,$flag);
			//$count = $kAdmin->oboCCModelAdminCount($_POST['currentCC']);
			$arrData = showCustomClearenceNewArr($bottomContent);
			oboCCModelAdminFunction($arrData);
			echo '|||||';
			echo display_admin_obo_cc_bottom_html($t_base,true);
			if($idWhFrom!=0 || $idWhTo!=0)
			{
				echo "<script type='text/javascript'>showCustomClearence('".$idSelectedField."','".$idWhFrom."','".$idWhTo."');</script>";
			}
			BREAK;
		}
		CASE 'UPDATE_CC_DETAILS':
		{
			$exportId  = sanitize_All_html_input($_POST['exportId']);
			$importId  = sanitize_All_html_input($_POST['importId']);
			$div_id = sanitize_All_html_input($_POST['div_id']);
			$searchedDataAry['import'] = $kAdmin->showCCWarehouseByID($exportId,$importId,2);
			$searchedDataAry['export'] = $kAdmin->showCCWarehouseByID($exportId,$importId,1);
			$idWh  = array();
			//$idWh['idDestinationWarehouse'] = $searchedDataAry['import']['idWarehouseTo'];
			//$idWh['idOriginWarehouse'] = $searchedDataAry['import']['idWarehouseFrom'];
			$idWh['idDestinationWarehouse'] = $importId;
			$idWh['idOriginWarehouse'] = $exportId;
			
			display_admin_obo_cc_bottom_html($t_base,'',$searchedDataAry,$idWh,'',$div_id);
			BREAK;
		}
		CASE 'SORT_TABLE_ORDER':
		{
			$flag  = sanitize_All_html_input($_POST['flag']);
			$order  = sanitize_All_html_input($_POST['order']);
			$_POST['currentCC']['idDestinationCountry'] = (int)$_POST['idDestinationCountry'];
			$_POST['currentCC']['idForwarder'] = (int)$_POST['idForwarder'];
			$_POST['currentCC']['idOriginCountry'] = (int)$_POST['idOriginCountry'];
			
			$bottomContent  = $kAdmin->oboCCModelAdmin($_POST['currentCC'],$flag,$order);
			$count = $kAdmin->oboCCModelAdminCount($_POST['currentCC'],$flag,$order);
			$arrData = showCustomClearenceNewArr($bottomContent);
			oboCCModelAdminFunction($arrData,$count);
			echo '|||||';
			echo display_admin_obo_cc_bottom_html($t_base,true);
			BREAK;
		}
		CASE 'PAGE_TABLE_ORDER':
		{
			$flag  = sanitize_All_html_input($_POST['flag']);
			$order  = sanitize_All_html_input($_POST['order']);
			$_POST['currentCC']['idDestinationCountry'] = (int)$_POST['idDestinationCountry'];
			$_POST['currentCC']['idForwarder'] = (int)$_POST['idForwarder'];
			$_POST['currentCC']['idOriginCountry'] = (int)$_POST['idOriginCountry'];
			$page = sanitize_All_html_input($_POST['page']);
			$bottomContent  = $kAdmin->oboCCModelAdmin($_POST['currentCC'],$flag,$order,$page);
			$count = $kAdmin->oboCCModelAdminCount($_POST['currentCC'],$flag,$order);
			$arrData = showCustomClearenceNewArr($bottomContent);
			oboCCModelAdminFunction($arrData,$count);
			echo '|||||';
			echo display_admin_obo_cc_bottom_html($t_base,true);
			BREAK;
		}
	}

}
if(!empty($_REQUEST['editOBOCCData']))
{
	$searchedDataAry=array();
	if($kExport_Import->updateOBOCustomClearanceByAdmin($_REQUEST['editOBOCCData']))
	{
		echo "SUCCESS||||";
		echo display_admin_obo_cc_bottom_html($t_base,true);
	}
	if(!empty($kExport_Import->arErrorMessages))
	{
		echo "ERROR ||||" ;
	?>
	
	<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
	      foreach($kExport_Import->arErrorMessages as $key=>$values)
	      {
	      ?><li><?=$values?></li>
	      <?php 
	      }
	?>
	</ul>
	</div>
	<?
	}
}
?>
