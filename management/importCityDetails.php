<?php
/**
 * Importing Forwarders Data
 */
ob_start();
session_start(); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php");
require_once( __APP_PATH__ . "/inc/functions.php");

ini_set ('max_execution_time',360000); 
ini_set('memory_limit','3048M');
ini_set('error_reporting',E_ALL & ~E_NOTICE );
ini_set('display_error','1');
$kUser = new cUser();
$kConfig = new cConfig();

$filename = __APP_PATH__."/maxmind/GeoLiteCity-Location.csv";
$row = 1;

$countryDetailsAry = array();
$countryDetailsAry = $kConfig->getAllCountryForMaxmindScript();
  
$forwardersDatas = array();
if (($handle = fopen("$filename", "r")) !== FALSE) 
{
    $main_counter = 0;
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
    {
        $num = count($data); 
        for ($c=0; $c < $num; $c++) 
        { 
            $szIndex='';
            if($c==0)
            {
                $szIndex = 'idLocationDetails';
            }
            else if($c==1)
            {
                $szIndex = 'szCountryCode';
            }
            else if($c==2)
            {
                $szIndex = 'szRegion';
            } 
            else if($c==3)
            {
                $szIndex = 'szCity';
            } 
            else if($c==4)
            {
                $szIndex = 'szPostcode';
            } 
            else if($c==5)
            {
                $szIndex = 'szLatitute';
            } 
            else if($c==6)
            {
                $szIndex = 'szLongitute';
            } 
            if(!empty($szIndex))
            {
                $forwardersDatas[$main_counter][$row][$szIndex]= trim($data[$c]); 
            }  
        } 
        $szCountryCode = $forwardersDatas[$main_counter][$row]['szCountryCode'] ;
        if(!empty($countryDetailsAry[$szCountryCode]))
        { 
            $forwardersDatas[$main_counter][$row]['szCountryName'] = $countryDetailsAry[$szCountryCode]['szCountryName'] ;
            $forwardersDatas[$main_counter][$row]['idCountry'] = $countryDetailsAry[$szCountryCode]['id'] ; 
        }
        else
        {
            $forwardersDatas[$main_counter][$row]['szCountryName'] = "" ;
            $forwardersDatas[$main_counter][$row]['idCountry'] = "";
        } 
        if($row==2000)
        {           
            $main_counter++; 
        }
        $row++;
    }
    fclose($handle);
     
    if($kUser->addIPlocationDetails($forwardersDatas))
    {
        echo "<h1> ".$kUser->iRowCounter." rows added successfully.<h1>";
    }
} 
?>