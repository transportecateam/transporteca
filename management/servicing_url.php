<?php
define('PAGE_PERMISSION','__UPLOAD_SERVICE_SUBMITTED__');
ob_start();
session_start();
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$szMetaTitle='Transporteca | Bulk Upload Services Approval List';
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
$t_base = "management/uploadService/";
$t_base_bulk="BulkUpload/";
validateManagement();
$kUploadBulkService = new cUploadBulkService();
$topTableData=$kUploadBulkService->getAllDataNotApprovedByAdmin('topdata');
$bottomTableData=$kUploadBulkService->getAllDataNotApprovedByAdmin('bottomdata');

?>
<div id="hsbody-2">
    <?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?>
    <!--
	<div class="hsbody-2-left account-links">
            <?php require_once( __APP_PATH__ ."/layout/admin_services.php" ); ?>
	</div> -->
	<div id="loader" class="loader_popup_bg" style="display:none;">
		<div class="popup_loader"></div>
		<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification" style="padding:10px;width:290px;">
		<p><?=t($t_base_bulk.'messages/plz_wait_while_we_process');?></p>
		<br/><br/>
		<p align="center"><img style="margin:0 auto;position:static;" src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></p>
		</div>	</div>			
					
	</div>
	<div class="hsbody-2-right">
            <div id="open_processing_popup"></div>
            <p align="left"><b><?=t($t_base.'title/submission_awaiting_review_pricing')?></b></p>
            <br/>
            <p style="TEXT-ALIGN:JUSTIFY"><?=t($t_base.'title/please_review_the_below_submission')?></p>
	    <br/>				
            <div id="waiting_for_approved_list">
                <?php	submission_awaiting_review_and_pricing($topTableData,'__TOP__'); ?>
            </div>
	    <br />
	    <div id="Error"></div>
            <?php echo estimationContent(array()); ?>
            <div style="clear: both;"></div>
            <br/><br/><br/>
            <h4><b><?=t($t_base.'title/submissions_awaiting_forwarder_approval')?></b></h4>
            <br/>
            <div id="awaiting_for_approved_list">
                <?php echo submission_awaiting_review_and_pricing($bottomTableData,'__BOTTOM__'); ?>
            </div>
	<iframe style="border:0" name="hssiframe" id="hssiframe" width="0px" height="0px"></iframe>	    
    </div>
</div>
	<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>