<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
} 
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
require_once( __APP_PATH_CLASSES__ . "/customerLogs.class.php");
validateManagement_ajax();
$t_base="management/LandingPage/";
$t_base_error="management/Error/";
$t_base_allprofile="management/AllProfiles/";
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$kConfig = new cConfig();
$kAdmin = new cAdmin(); 

if($mode=='CHECK_COUNTRY_VALIDITION_OF_SIMPLE_SEARCH')
{
	$t_base = "home/homepage/";
	$szValue=sanitize_all_html_input(trim($_REQUEST['szValue']));
	$flag=sanitize_all_html_input(trim($_REQUEST['flag']));
	
	if($flag=='VOLUME' || $flag=='WEIGHT')
	{
		$kWHSSearch=new cWHSSearch();
		$maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
		$maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
		$maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');
		$maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');
		$maxCargoQty = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_QUANTITY__');
		
		if($flag=='VOLUME')
		{
			$maxVolume=($maxCargoLength*$maxCargoWidth*$maxCargoHeight)/1000000;
			$kConfig->set_iVolume($szValue,$maxVolume);

			if($kConfig->arErrorMessages!='')
			{
				foreach($kConfig->arErrorMessages as $key=>$value)
				{
					echo "ERROR||||".$value;
				}
			}
		}
		else if($flag=='WEIGHT')
		{
			$kConfig->set_iNewWeight($szValue,$maxCargoWeight);
			if($kConfig->arErrorMessages!='')
			{
				foreach($kConfig->arErrorMessages as $key=>$value)
				{
					echo "ERROR||||".$value;
				}
			}
		}
	
	}
	else
	{	
		if(!empty($szValue))
		{			
                    $szCountryArr=reverse_geocode($szValue,true);
		}
		else
		{
                    echo "ERROR||||".t($t_base.'errors/check_location');
                    die;
		}       
		
		$szCountry=$szCountryArr['szCountryName'];
		$idCountry=$kConfig->getCountryIdByCountryName($szCountry,$iLanguage); 
                if($idCountry<=0 && !empty($szCountryArr['szCountryCode']))
                {
                    $kConfig->loadCountry(false,$szCountryArr['szCountryCode']);
                    $idCountry = $kConfig->idCountry ; 
                }
                
		if((int)$idCountry>0)
		{
			if($flag=='FROM_COUNTRY')
			{
				$kConfig->loadCountry($idCountry);
				if($kConfig->iActiveFromDropdown==0)
				{
					echo "ERROR||||".t($t_base.'errors/do_not_service_from_export_country');
				}
				else if($szCountryArr['szCityName']=='')
				{
					echo "ERROR||||".t($t_base.'errors/city_name_is_required');
				}
				else
				{
					echo "SUCCESS||||";
				}
			}
			else if($flag=='TO_COUNTRY')
			{
				$kConfig->loadCountry($idCountry);
				if($kConfig->iActiveToDropdown==0)
				{
					echo "ERROR||||".t($t_base.'errors/do_not_service_from_import_country');
				}
				else if($szCountryArr['szCityName']=='')
				{
					echo "ERROR||||".t($t_base.'errors/city_name_is_required');
				}
				else
				{
					echo "SUCCESS||||";
				}
			}
		}
		else
		{
			if($flag=='FROM_COUNTRY')
			{
				echo "ERROR||||".t($t_base.'errors/do_not_service_from_export_country');
			}
			else if($flag=='TO_COUNTRY')
			{
				echo "ERROR||||".t($t_base.'errors/do_not_service_from_import_country');
			}
		}
	}
}
else if($mode=='PREVIEW_GMAIL_INBOX_DATA')
{
    $idGmailLogs = sanitize_all_html_input(trim($_REQUEST['gmail_id']));
    if($idGmailLogs>0)
    {
        $kGmail = new cGmail();
        $kGmail->markMailSeen($idGmailLogs);
        $emailLogsAry = array();
        $emailLogsAry = $kGmail->getAllEmails($idGmailLogs);
        if(!empty($emailLogsAry))
        {
            echo "SUCCESS||||";
            echo display_gmail_preview($emailLogsAry[0]);
            die;
        } 
    }
}
else if($mode=='AUTO_LOAD_ADMIN_HEADER_NOTIFICATION')
{
    echo "SUCCESS||||";
    echo showAdminChecks();
    die;
}
else if($mode=='MANULLY_SYNC_WITH_GMAIL_INBOX')
{
    $kGmail = new cGmail();
    $dtLastRunDate = date('d F Y H:i',  strtotime("-36 Hours"));   
    $kGmail->fetchInboxDataFromGmail($dtLastRunDate);
    
    $gmailSyncErrorAry = array();
    $gmailSyncErrorAry = $kGmail->getAllGmailSyncMessages();
    
    echo "SUCCESS||||";
    echo displayGmailSyncErrorMessage($gmailSyncErrorAry);
    die;
}
else if($mode=='SYNC_WITH_GMAIL')
{
    ini_set (max_execution_time,240000);
    $kGmail = new cGmail();
    $kGmail->refreshGmailInboxData();
    echo "SUCCESS||||";
    echo gmailInboxEmailsListing();
    die;
}
else if($mode=='CANCEL_REGION')
{ 
    $countryDetails = array();  
    $countryDetails=$kAdmin->getAllRegions();
    $_POST['arrRegion'] = array();
    
    echo "SUCCESS||||";
    echo regionsListingView($countryDetails);
    echo "||||";
    echo addRegionForm($kAdmin);
    die; 
}
else if($mode=='DISPLAY_EDIT_REGION_FORM')
{
    $idRegion = sanitize_all_html_input(trim($_REQUEST['idRegion']));
    if($idRegion>0)
    {
        $searchAry = array();
        $searchAry['idRegion'] = $idRegion ;
        
        $countryDetails = array();  
        $countryDetails=$kAdmin->getAllRegions($searchAry);
        if(!empty($countryDetails[0]))
        {
            echo "SUCCESS||||";
            echo addRegionForm($kAdmin,$countryDetails[0]);
            die; 
        } 
    }
    
}
else if($mode=='DISPLAY_PAY_FORWARDER_INVOICE_FORM')
{
    $idBooking = sanitize_all_html_input(trim($_REQUEST['booking_id'])); 
    
    $kBooking = new cBooking();
    $postSearchAry = array();
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
     
    if(!empty($postSearchAry))
    {
        echo "SUCCESS||||";
        echo display_pay_forwarder_invoice_popup($postSearchAry,$kBooking);
        die;
    } 
}
else if($mode=='__OPEN_PAYPAL_COUNRTY_POPUP__')
{
    paypalCountriesPOpup($kAdmin);
}
else if($mode=='CHECK_COUNTRY_ALREADY_EXISTS')
{
    $idCountry=(int)$_POST['idCountry'];    
    if($kAdmin->getListPaypalCoutries($idCountry))
    {
        echo "SUCCESS";
        die();
    }
    else
    {
        echo "ERROR";
        die();
    }
}
else if($mode=='ADD_TO_PAYPAL_COUNTRY_LIST')
{
    $idCountry=(int)$_POST['idCountry'];    
    $kAdmin->addToPaypalCountryList($idCountry);
    paypalCountriesPOpup($kAdmin);
}
else if($mode=='REMOVE_FROM_PAYPAL_COUNTRY_LIST')
{
    $idCountry=(int)$_POST['idCountry'];    
    $kAdmin->removeFromPaypalCountryList($idCountry);
    paypalCountriesPOpup($kAdmin);
}
else if(!empty($_POST['forwarderTurnoverAry']))
{
    $kBilling = new cBilling(); 
    if($mode=='DOWNLOAD_FORWARDER_TURNOVER_DATA')
    {
        $forwarderTurnoverAry = array();
        $forwarderTurnoverAry = $kBilling->getForwarderTurnoverList($_POST['forwarderTurnoverAry']); 
        $szTurnoverFileName = $kBilling->downloadForwarderTurnoverSheet($forwarderTurnoverAry); 
        
        if(!empty($szTurnoverFileName))
        {
            echo "REDIRECT||||";
            echo __MAIN_SITE_HOME_PAGE_URL__."/insuranceSheets/".$szTurnoverFileName;
            die;
        }
    }
    else
    {
        $forwarderTurnoverAry = array();
        $forwarderTurnoverAry = $kBilling->getForwarderTurnoverList($_POST['forwarderTurnoverAry'],true); 
    
        echo "SUCCESS||||";
        echo displayForwarderTurnover($forwarderTurnoverAry); 
        die;
    } 
}
else if(!empty($_POST['confirmForwarderPayment']))
{
    $kBilling = new cBilling();
    $idBooking = sanitize_all_html_input(trim($_POST['confirmForwarderPayment']['idBooking'])); 
    $iMarkupPaid = sanitize_all_html_input(trim($_POST['confirmForwarderPayment']['iMarkupPaid'])); 
     $page=(int)$_POST['page'];
    
    $limit=$_POST['limit'];
    if($idBooking<=0)
    {
        //If booking ID is NULL then just die the script in normal scenario this will never happened
        die;
    }
    
    if($kBilling->payForwarderInvoice($_POST['confirmForwarderPayment']))
    {
        if($iMarkupPaid==1)
        {
            $unpaidMarkupBookingsAry = array();
            $unpaidMarkupBookingsAry = $kAdmin->showForwarderPendingPaymentDetails(true); 
            
            echo "SUCCESS||||";
            echo displayPayForwarderInvoices($unpaidMarkupBookingsAry,true,$page,$limit);
            die;
        }
        else
        {
            $unpaidMarkupBookingsAry = array();
            $unpaidMarkupBookingsAry = $kAdmin->showForwarderPendingPaymentDetails(); 
            
            echo "SUCCESS||||";
            echo displayPayForwarderInvoices($unpaidMarkupBookingsAry,false,$page,$limit);
            die;
        }   
    }
    else
    {
        $kBooking = new cBooking();
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
    
        echo "ERROR||||";
        echo display_pay_forwarder_invoice_popup($postSearchAry,$kBilling);
        die;
    }
}
else if(!empty($_POST['arrRegion']))
{
    $idRegion = sanitize_all_html_input(trim($_POST['arrRegion']['id']));
    if($kAdmin->updateRegionName($_POST['arrRegion']))
    {
        $countryDetails = array();  
        $countryDetails=$kAdmin->getAllRegions();
        $_POST['arrRegion'] = array();
        
        echo "SUCCESS||||";
        echo addRegionForm($kAdmin); 
        echo "||||";
        echo regionsListingView($countryDetails);
        die; 
    } 
    else
    {
        echo "ERROR||||";
        echo addRegionForm($kAdmin);
        die;
    }
}
else if(!empty($_POST['transportecaTurnoverAry']))
{
    $kBilling = new cBilling();
    $kBooking = new cBooking();
    if($mode=='DOWNLOAD_TRANSPORTECA_TURNOVER_DATA')
    {
        $transportecaTurnoverAry = array();
        $transportecaTurnoverAry = $kBooking->paymentOfForwardersTransaction($_SESSION['admin_id'],true,$_POST['transportecaTurnoverAry']); 
        $szTurnoverFileName = $kBilling->downloadTransportecaTurnoverSheet($transportecaTurnoverAry); 
        
        if(!empty($szTurnoverFileName))
        {
            echo "REDIRECT||||";
            echo __MAIN_SITE_HOME_PAGE_URL__."/insuranceSheets/".$szTurnoverFileName;
            die;
        }
    }
    else
    {
        $transportecaTurnoverAry = array();
        $transportecaTurnoverAry = $kBooking->paymentOfForwardersTransaction($_SESSION['admin_id'],true,$_POST['transportecaTurnoverAry'],true); 
    
        echo "SUCCESS||||";
        echo displayTransportecaTurnover($transportecaTurnoverAry); 
        die;
    } 
}
else if($mode=='ADD_EDIT_CONTACT_ONLY_FORWARDER_PROFILE')
{
    $idForwarderContact =(int)$_POST['idForwarderContact'];
    
    if((int)$idForwarderContact>0)
    {
        $kForwarderContacts = new cForwarderContact();
        $forwarderContactAry = $kForwarderContacts->preload($idForwarderContact);
        $_POST['addContactOnlyForwarderArr']=$forwarderContactAry;
        $_POST['addContactOnlyForwarderArr']['szPhoneUpdate']=urlencode(base64_encode($forwarderContactAry['szPhone']));
        $_POST['addContactOnlyForwarderArr']['szMobileUpdate']=urlencode(base64_encode($forwarderContactAry['szMobile']));
        $_POST['addContactOnlyForwarderArr']['idForwarderContact']=$idForwarderContact;
    }
    
    $_POST['addContactOnlyForwarderArr']['szFlag']=  sanitize_all_html_input(trim($_POST['szFlag']));
    $_POST['addContactOnlyForwarderArr']['idCrmEmail']=  (int)$_POST['idCrmEmail'];
    $_POST['addContactOnlyForwarderArr']['szOperationType']=  sanitize_all_html_input(trim($_POST['szOperationType']));
    $_POST['addContactOnlyForwarderArr']['szSearchStr']=sanitize_all_html_input(trim($_POST['szSearchStr']));
    echo forwarderNewContactForm();
}
else if(!empty($_POST['addContactOnlyForwarderArr']))
{
    if($kAdmin->addUpdateContactProfileData($_POST['addContactOnlyForwarderArr']))
    {
        
        echo "SUCCESS|||||";
        echo $kAdmin->idForwarderContact;
    }
    else
    {
        if(!empty($kAdmin->arErrorMessages))
        {
            $formId = 'addNewForwarderContactForm';
            $szValidationErrorKey = '';
            foreach($kAdmin->arErrorMessages as $errorKey=>$errorValue)
            {
                
                ?>
                <script type="text/javascript">
                    $("#"+'<?php echo $errorKey; ?>').addClass('red_border');
                </script>
                <?php  
                
            }
        }
        echo "ERROR|||||";
        echo forwarderNewContactForm($kAdmin);
    }
}
else if(!empty($_POST['vogaQuoteEmailLogsAry']))
{ 
    $kCustomerLogs = new cCustomerLogs();
    $allSendMessageArr = $kCustomerLogs->getVogaEmailQuotesListing($_POST['vogaQuoteEmailLogsAry']);
    $t_base="management/AdminMessages/";
    echo "SUCCESS||||";
    ?>
    <table cellspacing="0" id="send_message_list" cellpadding="0" border="0" class="format-2" width="100%">
    <tr>
        <td style="text-align:left;width:1%;"></td>
        <td style="text-align:left;width:16%;"><strong><?=t($t_base.'fields/date_time')?></strong></td>
        <td style="text-align:left;width:27%;"><strong><?=t($t_base.'fields/email_address')?></strong></td> 
        <td style="text-align:left;width:45%;"><strong><?=t($t_base.'fields/subject')?></strong></td>
        <td style="text-align:left;width:5%;"></td>
    </tr>
    <?php
        if(!empty($allSendMessageArr))
        {
            $i=1;
            $counter = 1;
            foreach($allSendMessageArr as $allSendMessageArrs)
            {
                if(empty($allSendMessageArrs['szEmailStatus']))
                {
                    $allSendMessageArrs['szEmailStatus'] = 'Sent';
                }
                if(!empty($allSendMessageArrs["dtSents"]) && $allSendMessageArrs["dtSents"]!='0000-00-00 00:00:00')
                {
                    $allSendMessageArrs['dtSents'] = getDateCustom($allSendMessageArrs['dtSents']);
                    /*
                        if(date('I')==1)
                        { 
                            $allSendMessageArrs["dtSents"] =  date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($allSendMessageArrs['dtSents'])));  
                        } 
                        else
                        {
                            $allSendMessageArrs["dtSents"] =  date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($allSendMessageArrs['dtSents'])));  
                        }
                     * 
                     */
                }

                    ?>
                    <tr id="message_data_<?=$i?>" onclick="select_message_system_send_tr('message_data_<?=$i?>','<?=$allSendMessageArrs['id']?>')">
                        <td><?php echo $counter++; ?></td>
                        <td><?=((!empty($allSendMessageArrs["dtSents"]) && $allSendMessageArrs["dtSents"]!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($allSendMessageArrs["dtSents"])):"")?></td>
                        <td><?=returnLimitData($allSendMessageArrs['szToAddress'],25)?></td>
                        <!--<td><?php echo ucfirst($allSendMessageArrs['szEmailStatus']); ?></td>-->
                        <td><?=substrwords(utf8_decode($allSendMessageArrs['szEmailSubject']),58)?></td>
                        <td><a href="javascript:void(0)" onclick="resend_system_messages('<?=$allSendMessageArrs['id']?>')"><?=t($t_base.'fields/resend');?></a></td>
                    </tr>
                    <?php
                    ++$i;
                }
            }
        ?>
    </table>
    <?php 
}
else if($mode=='GET_CATEGORY_LIST_BY_SEARCH_MINI')
{
    $idSearchMini = (int)$_POST['idSearchMini'];
    $iSelectedLanguage=1;
    if($idSearchMini>0)
    {
        $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,false,$idSearchMini);
        if(!empty($standardCargoCategoryAry))
        {
            echo "SUCCESS||||";
            $str ="<option value=''>Select</option>";
            foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
            {
                $str .= '<option value="'.$standardCargoCategoryArys['id'].'">'.$standardCargoCategoryArys['szCategoryName'].'</option>';
            }
            echo $str;
        }
        else
        {
            echo "ERROR||||";
            echo "<option>Select</option>";
           
        }
         die();
    }
    else
    {
        echo "ERROR||||";
        echo "<option>Select</option>";
        die();
    }
}
else if($mode=='GET_CATEGORY_CARGO_LIST_BY_ID_CATEGORY')
{
    $idSearchMini = (int)$_POST['idSearchMini'];
    $idCategory = (int)$_POST['idCategory'];
    $iSelectedLanguage=1;
    if($idSearchMini>0 && $idCategory>0)
    {
        $standardCargoAry = $kConfig->getStandardCargoList($idCategory,$iSelectedLanguage,'',$idSearchMini);
        if(!empty($standardCargoAry))
        {
            echo "SUCCESS||||";
            $str ="<option value=''>Select</option>";
            foreach($standardCargoAry as $standardCargoArys)
            {
                $str .= '<option value="'.$standardCargoArys['id'].'">'.$standardCargoArys['szSellerReference'].'</option>';
            }
            echo $str;
        }
        else
        {
            echo "ERROR||||";
            echo "<option>Select</option>";
           
        }
         die();
    }
    else
    {
        echo "ERROR||||";
        echo "<option>Select</option>";
        die();
    }
}
else if ($mode=='UPDATE_MANAGEMENT_CUSTOMER_SERVICE_AGENT')
{
    $idAdmin=(int)$_POST['idAdmin'];
    if((int)$idAdmin>0)
    {
        $res=$kAdmin->updateCustomerServiceAgentOfManagement($idAdmin); 
        echo "SUCCESS||||";
        echo $res;
        die();
    }
}
else if($mode=='SHOW_REQUEST_CALL_DATA')
{
    $szErrorCode=trim($_POST['szErrorCode']);
    $page=trim((int)$_POST['page']);
    $kApi = new cApi();
    $errorResponseArr=$kApi->getPartnerAPIEventErrorLogs(true);
    $ctr=0;
    if(!empty($errorResponseArr))
    {
        foreach($errorResponseArr as $errorResponseArrs)
        {
            $errorResponseUnserialize=unserialize($errorResponseArrs['szResposeData']);
            $errorRequestUnserialize=unserialize($errorResponseArrs['szRequestParams']);
            $dtCreated=date('d/m/Y H:i:s',strtotime($errorResponseArrs['dtCreated']));
            $errorCodeArr=$errorResponseUnserialize['errors'];
            if(!empty($errorCodeArr))
            {
                foreach($errorCodeArr as $errorCodeArrs)
                {
                    if($errorCodeArrs['code']==$szErrorCode)
                    {
                        $errorCodeCountArr[$ctr]['szMessage']=json_encode($errorRequestUnserialize);
                        $errorCodeCountArr[$ctr]['dtDate']=($dtCreated);
                        ++$ctr;
                    }
                }
            }
        }
    }
    
    ?>
                
    <div id="popup-bg"></div>
        <div id="popup-container"> 
            <div class="popup" style="width:95%;max-height:550px;">  
                 
                <div style="overflow:auto;max-height:450px;min-height:430px;">
                    <table  class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0">  
                        <tr>
                            <td style="font-weight:bold;width:10%;">Sr. No.</td>
                            <td style="font-weight:bold;width:80%;">Request Call</td>
                            <td style="font-weight:bold;width:10%;">Date/Time</td>
                        </tr>
                    <?php
                    
                    if(!empty($errorCodeCountArr))
                    {
                        $gtotal=count($errorCodeCountArr);
                        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
                        $kPagination  = new Pagination($gtotal,__API_REQUEST_PER_PAGE__,15);
                
                        $startFlow=($page-1)*__API_REQUEST_PER_PAGE__;
                        $end=$startFlow+__API_REQUEST_PER_PAGE__-1;
                        
                        if($end>=$gtotal)
                        {
                                $end=$gtotal-1;
                        }

                        for($t=$startFlow;$t<=$end;++$t)
                        {   
                            $srNo=$t+1;
                            ?>
                            <tr>
                                <td><?php echo $srNo; ?></td>
                                <td><textarea col="5" rows="5"><?php echo $errorCodeCountArr[$t]['szMessage'];?></textarea></td>
                                 <td><?php echo $errorCodeCountArr[$t]['dtDate']; ?></td>
                            </tr>
                           <?php 
                        }
                    }
                    ?>
                    </table>
                </div>
                <?php
                if($gtotal>0)
                {
                    ?>
                <div class="page_limit clearfix">
                    <div class="pagination">
                        <?php
                            $funcName='showPageApiRequest';
                            $kPagination->paginate($funcName);
                            if($kPagination->links_per_page>1)
                            {
                                echo $kPagination->renderFullNav();
                            }
                        ?>
                    </div>
                    <div class="limit">
                       
                            <a onclick="closeTip('show-request-call');" href="javascript:void(0);" class="button1">Close</a>
                    </div>
                </div>
                  <?php  
                    
                    
                }
                ?>
            </div>
        </div>
                    <?php
}
?>