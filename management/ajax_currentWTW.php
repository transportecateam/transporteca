<?php 
/**
  *  SHOW WAREHOUSE DETAILS 
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kExport_Import  = new cExport_Import;
$t_base = "OBODataExport/";
$t_base_error="management/Error/";
if(isset($_POST['idWarehouse']) && $_POST['idWarehouse']>0)
{
    $kExport_Import  = new cExport_Import;
    $idWareHouse  = (int)$_POST['idWarehouse'];
    $bottomContent  = $kExport_Import->getDataForLCLExpiredLastThreeMonth($idWareHouse);
    $t_base = "OBODataExport/";
    $formData['idOriginWarehouse']  = $bottomContent[0]['idWarehouseFrom'];
    $formData['idDestinationWarehouse']  = $bottomContent[0]['idWarehouseTo'];	
    display_obo_lcl_bottom_admin_html($t_base,false,$bottomContent[0],$formData);
}

if(isset($_POST['mode']))
{	
	$kAdmin =  new cAdmin;
	$mode = sanitize_all_html_input($_POST['mode']);
        $iWarehouseType = sanitize_all_html_input($_POST['iWarehouseType']);
	$id = (int)$_POST['id'];
	if($mode == 'FORWARDER')
	{	
            $Country = $kAdmin->findSOurceDestinationCountryForwarder($id,$iWarehouseType);
            if($Country != array())
            {
                foreach( $Country as $count)
                {
                    $originCountry[] = $count['idOrignCountry'];
                    $destinationCountry[] = $count['idDestinationCountry'];
                }

                $originCountry = array_unique($originCountry);
                $destinationCountry = array_unique($destinationCountry);
                if(count($originCountry)==1)
                {
                    $strArr = "selected=\"selected\"";
                }
                else
                {
                    $strArr = "";
                } 
                echo "<option value=''>".t($t_base."fields/all_country_from")."</option>"; 
                foreach($originCountry as $id)
                {	
                    $details = $kAdmin->countriesView(1,$id); 
                    echo "<option value='".$id."' ".$strArr.">".$details['szCountryName']."</option>";
                } 
                echo "|||||";
                $strArr = "";

                if(count($destinationCountry)==1)
                {
                    $strArr = "selected=\"selected\"";
                }
                else
                {
                    $strArr = "";
                }

                echo "<option value=''>".t($t_base."fields/all_country_to")."</option>";
                foreach($destinationCountry as $id)
                {	
                    $details = $kAdmin->countriesView(1,$id); 
                    echo "<option value='".$id."' ".$strArr.">".$details['szCountryName']."</option>";
                }
            }
	}
	if($mode == 'ORIGIN')
	{
            $idCountryForwarder = $kAdmin->findForwarderDestinationCountryId($id,$iWarehouseType); 
            if($idCountryForwarder != array())
            {
                foreach( $idCountryForwarder as $count)
                {
                    $idForwarder[] = $count['idForwarder'];
                    $destinationCountry[] = $count['idDestinationCountry'];
                } 
                $idForwarder = array_unique($idForwarder);
                $destinationCountry = array_unique($destinationCountry);
                $strArr = "";

                if(count($destinationCountry)==1)
                {
                    $strArr = "selected=\"selected\"";
                }
                else
                {
                    $strArr = "";
                } 
                echo "<option value=''>".t($t_base."fields/all_forwarder")."</option>"; 
                foreach($idForwarder as $id)
                {
                    $details = $kAdmin->getAllForwarderNameList($id);
                    echo "<option value='".$details[0]['id']."'>".$details[0]['szDisplayName']."</option>";
                } 
                echo '|||||';
                echo "<option value=''>".t($t_base."fields/all_country_to")."</option>"; 
                foreach($destinationCountry as $id)
                {	
                    $details = $kAdmin->countriesView(1,$id); 
                    echo "<option value='".$id."' ".$strArr.">".$details['szCountryName']."</option>";
                }
            }
	}
	if($mode == 'DESTINATION')
	{
            $idCountryForwarder = $kAdmin->findForwarderOriginCountryId($id,$iWarehouseType);
            if($idCountryForwarder != array())
            {	
                foreach( $idCountryForwarder as $count)
                {
                    $idForwarder[] = $count['idForwarder'];
                    $originCountry[] = $count['idOriginCountry'];
                }

                $idForwarder = array_unique($idForwarder);
                $originCountry = array_unique($originCountry);
                echo "<option value=''>".t($t_base."fields/all_forwarder")."</option>";

                foreach($idForwarder as $id)
                {
                    $details = $kAdmin->getAllForwarderNameList($id);
                    echo "<option value='".$details[0]['id']."'>".$details[0]['szDisplayName']."</option>";
                }
			
                echo '|||||';
                echo "<option value=''>".t($t_base."fields/all_country_from")."</option>";
			
                foreach($originCountry as $id)
                {	
                    $details = $kAdmin->countriesView(1,$id); 
                    echo "<option value='".$id."'>".$details['szCountryName']."</option>";
                }
            }
	}
	if($mode == 'VIEW_DEFAULT_BOTTOM')
	{
            display_obo_lcl_bottom_admin_html($t_base,true);
	}
}
if(!empty($_REQUEST['editOBOLCLData']))
{	
    if($kExport_Import->updateOBOLCL($_REQUEST['editOBOLCLData']))
    {
        $searchedDataAry = $kExport_Import->getDataForLCL('',$kExport_Import->id);
        $requestAry = array();

        if(empty($searchedDataAry))
        {
            $idOriginWarehouse = $_REQUEST['editOBOLCLData']['idOriginWarehouse'] ;
            $idDestinationWarehouse = $_REQUEST['editOBOLCLData']['idDestinationWarehouse'];		
            $requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
            $requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
        } 
        echo "SUCCESS||||";
    }
    if(!empty($kExport_Import->arErrorMessages))
    {
        echo "ERROR ||||" ;
        ?>
        <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
        <div id="regErrorList">
            <ul>
                <?php
                      foreach($kExport_Import->arErrorMessages as $key=>$values)
                      {
                      ?><li><?=$values?></li>
                      <?php 
                      }
                ?>
            </ul>
        </div>
        <?php
    }
}
if($operation_mode=='CANCEL_OBO_LCL')
{
	$idPricingWTW = sanitize_all_html_input(trim($_REQUEST['id']));
	
	if($idPricingWTW<=0)
	{
		$idOriginWarehouse = sanitize_all_html_input(trim($_REQUEST['idOriginWarehouse']));
		$idDestinationWarehouse = sanitize_all_html_input(trim($_REQUEST['idDestinationWarehouse']));		
		
		$idPricingWTW = $kExport_Import->getWTWIdByCfs($idOriginWarehouse,$idDestinationWarehouse);
	}
	
	$searchedDataAry = $kExport_Import->getDataForLCL('',$idPricingWTW);
	$requestAry = array();
	
	if(empty($searchedDataAry))
	{
		$idOriginWarehouse = sanitize_all_html_input(trim($_REQUEST['idOriginWarehouse']));
		$idDestinationWarehouse = sanitize_all_html_input(trim($_REQUEST['idDestinationWarehouse']));		
		$requestAry['idOriginWarehouse'] = $idOriginWarehouse ;
		$requestAry['idDestinationWarehouse'] = $idDestinationWarehouse ;
	}
	else
	{
		$idOriginWarehouse = $searchedDataAry[0]['idWarehouseFrom'];
		$idDestinationWarehouse = $searchedDataAry[0]['idWarehouseTo'];
	}
	//$pricingWtwAry['id'] = $idPricingWTW ;
	$formData['idOriginWarehouse'] = $idOriginWarehouse ;
	$formData['idDestinationWarehouse'] = $idDestinationWarehouse ;

	//echo display_obo_lcl_bottom_html($t_base,false,$pricingWtwAry,$formData);
	//echo "||||";
	//echo display_multiple_lcl_services($searchedDataAry,$t_base,$requestAry);
}
if(!empty($_REQUEST['currentWtw']))
{ 
    $iWarehouseType = (int)$_REQUEST['currentWtw']['iWarehouseType'];
    $warehouse = $kExport_Import->getAllwarehouseDetails($_REQUEST['currentWtw']);

    if((int)$warehouse>0)
    {	
        $bottomContent  = $kExport_Import->getDataForLCLExpiredLastThreeMonth(null,$warehouse);
        currentWTWBottomForm($bottomContent);
    }
    else
    {	
        currentWTWBottomForm(array());
    }
}
	
?>