<?php 
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$addFile=1;
$szMetaTitle="Transporteca | Tasks - Pending Tray"; 
define('PAGE_PERMISSION','__QUICK_QUOTE__');
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php"); 
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
    
$kBooking = new cBooking();
$t_base="management/quickQuote/"; 
validateManagement();   

$kWhsSearch = new cWHSSearch();  
$kConfig = new cConfig();
$kBookingNew = new cBooking();
$arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);      
  
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];
$szLanguage = "en"; 

$iLanguage = 1;
$allCountriesArr = array();
$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage); 
 
$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomKey']));
$postSearchAry = array();
$iCopyQuickQuoteBookingFlag=0;
if(!empty($szBookingRandomNum))
{ 
    $idBooking=$kBookingNew->getBookingIdByRandomNum($szBookingRandomNum);
    $postSearchAry = $kBookingNew->getBookingDetails($idBooking); 
    $iCopyQuickQuoteBookingFlag=1;
} 
$languageAry = array();
$languageAry = $kConfig->getLanguageDetails(false,false,false,false,false,true); 

$kCourierServices = new cCourierServices();
    $dtdTradeStr=$kCourierServices->getAllDTDTradesList(true);
 
?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
<script type="text/javascript"> 
var transportecaCountryListAry = new Array(); 
var CountryLanguageListAry = new Array();
var dtdTradeStr = '<?php echo $dtdTradeStr;?>';
<?php
    if(!empty($allCountriesArr))
    {
        foreach($allCountriesArr as $allCountriesArrs)
        {
            ?>
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>'] = new Array();
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>']['id'] = '<?php echo $allCountriesArrs['id'] ?>';
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>']['idCurrency'] = '<?php echo $allCountriesArrs['iDefaultCurrency'] ?>';
            transportecaCountryListAry['<?php echo $allCountriesArrs['szCountryISO'] ?>']['iInternationDialCode'] = '<?php echo $allCountriesArrs['iInternationDialCode'] ?>';
            <?php
        }
    }
    if(!empty($languageAry))
    {
        foreach($languageAry as $languageArys)
        {
            $iLanguage = $languageArys['id'];
            $allCountriesArr = array();
            $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage); 
            ?>
            CountryLanguageListAry['<?php echo $iLanguage ?>'] = new Array();
            <?php
            if(!empty($allCountriesArr))
            {
                foreach($allCountriesArr as $allCountriesArrs)
                {
                    ?> 
                    CountryLanguageListAry['<?php echo $iLanguage ?>']['<?php echo $allCountriesArrs['id'] ?>'] = new Array();
                    CountryLanguageListAry['<?php echo $iLanguage ?>']['<?php echo $allCountriesArrs['id'] ?>']['szCountryName'] = '<?php echo addslashes($allCountriesArrs['szCountryName']) ?>'; 
                    <?php
                }
            }  
        }
    }
?>    
</script>
<div id="hsbody">   
    <div id="quick_quote_booking_popup" style="display:none;"></div>  
    <div id="quick_quote_main_container" class="quick_quote_search_form_container">  
          <?php echo display_quick_quote_contents($postSearchAry); ?> 
    </div> 
    <input type="hidden" id="iCopyQuickQuoteBookingFlag" name="iCopyQuickQuoteBookingFlag" value="<?php echo $iCopyQuickQuoteBookingFlag;?>" />
</div>	    
<?php   
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>