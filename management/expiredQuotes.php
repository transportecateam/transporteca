<?php
ob_start();
session_start();
$display_profile_not_completed_message = true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
$szMetaTitle='Transporteca | Expired Booking Quotes';
require_once( __APP_PATH_LAYOUT__ . "/admin_header.php" );
$t_base="management/insurance/";
$t_base_bulk="BulkUpload/";
validateManagement(); 
$kInsurance = new cInsurance();
$kBooking = new cBooking; 

?> 
<div id="hsbody-2">
    <div id="insurance_rate_popup" style="display:none"></div> 
    <?php require_once( __APP_PATH__ ."/layout/contentManagementOperationsLeftNav.php" ); ?> 
    <div class="hsbody-2-right">
        <div id="booking_quotes_list_container">
        <?php
            echo display_expired_booking_quotes_listing(__BOOKING_QUOTES_STATUS_EXPIRED__); 
        ?>
        </div>
    </div>
</div>  
<?php
    include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>