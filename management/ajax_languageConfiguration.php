<?php
/**
  *  Admin--- System -- System variables ---Language Configuration
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$mode=sanitize_all_html_input(trim($_REQUEST['mode']));
$kConfig = new cConfig();
$kExplain = new cExplain();
if($mode=='OPEN_EDIT_CONFIGURATION_DATA')
{
    $idLangConfigType=trim($_POST['idLangConfigType']);    
    $idLanguage=(int)$_POST['idLanguage'];
    
    $iServiceUpdateFlag=true;
    if($idLangConfigType==__TABLE_SERVICE_TYPE__)
    {
        $iServiceUpdateFlag=true;
    }
    $configLangArr=$kConfig->getConfigurationLanguageData($idLangConfigType,$idLanguage,false,false,false,$iServiceUpdateFlag);
    
    viewLanguageConfiguration($configLangArr,$idLangConfigType);
   
}
else if($mode=='SAVE_LANGUAGE_CONFIGURATION_DATA')
{
    $idLangConfigType=trim($_POST['idLangConfigType']);    
    $idLanguage=(int)$_POST['idLanguage'];
    
    if($kConfig->updateLanguageConfigurationData($_POST['langConfigArr'],$idLanguage,$idLangConfigType))
    {
        
    }
    else
    {
        if(!empty($kConfig->arErrorMessages))
        {
            $formId = 'languageConfigurationForm';
            $szValidationErrorKey = '';
            foreach($kConfig->arErrorMessages as $errorKey=>$errorValue)
            {                
                ?>
                <script type="text/javascript">
                    $("#<?php echo $errorKey; ?>").addClass('red_border');
                </script>
                <?php  
                
            }
        } 
        $configLangArr=$kConfig->getConfigurationLanguageData($idLangConfigType,$idLanguage);
        viewLanguageConfiguration($configLangArr,$idLangConfigType);
    }
    
}
else if($mode=='DISPLAY_EDIT_LANGUAGE_VERSIONS_FORM')
{
   $idLanguage=(int)$_POST['id'];
   $languageAry= array();
   
   $languageAry=$kConfig->getLangugeDetailsById($idLanguage);
   echo 'SUCCESS';
   echo '||||';
   echo editLanguageVersionsForm($kConfig,$languageAry);
   die();
}
else if($mode=='SAVE_LANGUAGE_VESRIONS_DATA')
{  
    $idLanguage=(int)$_POST['id'];
    if($kConfig->updateLanguageVersionsData($_POST['arrVersions'],$idLanguage))
    {
      $_POST['arrVersions']=array();  
      $languagesAry=$kConfig->getLanguageDetails('','','','','',false);
      echo 'SUCCESS';
      echo '||||';
      echo languageVersionsListingView($languagesAry);
      echo '||||';
      echo editLanguageVersionsForm($kConfig);
      die();
    }
    else
    { 
        $arrLangugeVesrion['szNameLocal']= trim($_POST['arrVersions']['szName']) ;
        $arrLangugeVesrion['iDoNotShow']=trim($_POST['arrVersions']['iDoNotShow']) ; 
        $arrLangugeVesrion['szDomain'] = trim($_POST['arrVersions']['szDomain']); 
        $arrLangugeVesrion['szComments'] = trim($_POST['arrVersions']['szComments']); 
        $arrLangugeVesrion['id']=trim($_POST['id']) ;
        
        echo 'ERROR';
        echo '||||';
        echo editLanguageVersionsForm($kConfig,$arrLangugeVesrion);
        die();
    }
}
else if($mode=='CANCEL_LANGUAGE_VERSIONS')
{  
      $languagesAry=$kConfig->getLanguageDetails('','','','','',false);
      echo 'SUCCESS';
      echo '||||';
      echo languageVersionsListingView($languagesAry);
      echo '||||';
      echo editLanguageVersionsForm($kConfig);
      die();
}
if($mode=='OPEN_EDIT_CONFIGURATION_META_DATA')
{
    $idLangConfigType=trim($_POST['idLangConfigType']);    
    $idLanguage=(int)$_POST['idLanguage'];
    
    
    $configLangArr=$kExplain->getMetaTitle($idLangConfigType,$idLanguage);
    
    viewLanguageConfigurationMeta($configLangArr,$idLanguage);
   
}
if($mode=='SAVE_LANGUAGE_CONFIGURATION_META_DATA')
{
    $idLangConfigType=trim($_POST['idLangConfigType']);    
    $idLanguage=(int)$_POST['idLanguage'];
    
    if($kExplain->updateLanguageConfigurationMetaData($_POST['langConfigArr'],$idLanguage,$idLangConfigType))
    {
        
    }
    else
    {
        if(!empty($kExplain->arErrorMessages))
        {
            $formId = 'languageConfigurationForm';
            $szValidationErrorKey = '';
            foreach($kExplain->arErrorMessages as $errorKey=>$errorValue)
            {                
                ?>
                <script type="text/javascript">
                    $("#<?php echo $errorKey; ?>").addClass('red_border');
                </script>
                <?php  
                
            }
        } 
        $configLangArr=$kExplain->getMetaTitle($idLangConfigType,$idLanguage);
        viewLanguageConfigurationMeta($configLangArr,$idLangConfigType);
    }
}
?>
