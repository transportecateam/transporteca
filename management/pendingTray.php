﻿<?php 
define('PAGE_PERMISSION','__FILIES__');
ob_start();
if (!isset( $_SESSION )) 
{
    session_start();
} 
$addFile=1;
$szMetaTitle="Transporteca | Tasks - Pending Tray";
define('PAGE_NAME','PENDING_TRAY');

if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php"); 
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
    
$kBooking = new cBooking();  
$kPendingTray = new cPendingTray();
$t_base="management/pendingTray/"; 
validateManagement();   
  
$_SESSION['szCustomerTimeZoneGlobal'] = array();
if(empty($_SESSION['szCustomerTimeZoneGlobal']))
{
    $ret_ary = array();
    $ret_ary = getCountryCodeByIPAddress();
    if(empty($ret_ary))
    {
        $ret_ary['szCountryCode'] = 'DK';
    } 
    $ret_ary['szCountryCode'] = 'DK';
    $szCustomerTimeZone = get_time_zone($ret_ary['szCountryCode'],$ret_ary['szRegion']); 
    $_SESSION['szCustomerTimeZoneGlobal'] = $szCustomerTimeZone ; 
}      

$incompleteTaskAry = array(); 
  
$searchTaskAry = array(); 
if(!empty($_SESSION['sessTransportecaTeamAry']))
{
    $searchTaskAry['idFileOwnerAry'] = $_SESSION['sessTransportecaTeamAry'] ;
}
else
{
    $searchTaskAry['idFileOwnerAry'][0] = 0;
    $searchTaskAry['idFileOwnerAry'][1] = $_SESSION['admin_id'];
}
$incompleteTaskAry = $kBooking->getAllBookingWithOpenedTask($searchTaskAry);
 
$szOperatorName = $kAdmin->szFirstName." ".$kAdmin->szLastName ; 
$activeAdminAry = array();
$activeAdminAry = $kAdmin->getAllActiveStoreAdmin($_SESSION['admin_id'],true); 




$dtRealDateTime = $kBooking->getRealNow();
//echo "Current Time: ".date('Y-m-d H:i:s',strtotime("+1 HOUR"));
 
$bNotAllocatedFlag = false;
$iNotAllocatedValue = 0;
if(empty($_SESSION['sessTransportecaTeamAry']))
{
    $bNotAllocatedFlag = true;
}
else if(!empty($_SESSION['sessTransportecaTeamAry']) && in_array($iNotAllocatedValue,$_SESSION['sessTransportecaTeamAry'])) 
{
    $bNotAllocatedFlag = true;
} 
$kWhsSearch = new cWHSSearch();
$szGoogleApiKey = $kWhsSearch->getManageMentVariableByDescription('__GOOGLE_MAP_V3_API_KEY__');

$kConfig = new cConfig();
$allCountriesArr = array();
$allCountriesArr=$kConfig->getAllCountries(true,1);

?>  
<?php
    if(trim($_REQUEST['idBookingNum'])!='' && (int)$_REQUEST['openFlag']==1)
    { 
        $kBookingNew = new cBooking();
        $szBookingRandomNum = trim($_REQUEST['idBookingNum']);

        $idBooking=$kBookingNew->getBookingIdByRandomNum($szBookingRandomNum);
        $postSearchAry = $kBookingNew->getBookingDetails($idBooking);
        $idBookingFile = $postSearchAry['idFile'];  
        $kBookingNew->loadFile($idBookingFile);
    }
        //echo "File ID: ".$kBookingNew->idBookingFile;
    ?>
<div id="hsbody"> 
    <div id="content_body" style="display:none;">
        
    </div>
    <div id="cancel_booking_popup" style="display:none;"></div>
    <div id="courier_product_attention_popup" style="display:none;">
        <?php showCourierProductAttentionPopup();?>
    </div>
    <style type="text/css">
        #map_canvas { height: 100% }     
        .map-content h3 { margin: 0; padding: 5px 0 0 0; }    
        .map-content p{font-size:12px !important;}
     </style>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=<?=$szGoogleApiKey?>"></script>
    <script type="text/javascript">
        $().ready(function() {
        
        <?php
       
        if(!empty($allCountriesArr))
        {
            foreach($allCountriesArr as $allCountriesArrs)
            {
                ?>
                
                //countriesArr[<?php echo $allCountriesArrs['id'] ?>]['id'] = "<?php echo $allCountriesArrs['id'];?>";
                countriesArr[<?php echo $allCountriesArrs['id'] ?>] = "<?php echo $allCountriesArrs['szCountryName'];?>";
                <?php
            }
        } ?>
   });
        console.log('<?php echo $szCustomerTimeZone; ?>');
      var originIcon = '<?=__BASE_STORE_IMAGE_URL__?>' + "/blue-marker.png";
	  var map;
	  
	// deafult value for longs and lats  
    var olat1 = 51.144894 ;
    var olang1 = 1.2854;
    var dlat1 = 51.905307 ;
    var dlang1 = 4.465942;
   // var pipe_line_string = olat1 + "|" + olang1 +"|" + dlat1 + "|" + dlang1 ;
    
    function open_google_map_popup(id)
    {	
    	var pipe_line_string = '';
    	/*
    	$.post(__JS_ONLY_SITE_BASE__+"/ajax_googleMap.php",function(result){
			$("#show_on_map_pipe_line").attr("value",result);
		});
		*/
    	pipe_line_string = $("#show_on_map_pipe_line").attr("value");
       	var result_ary = pipe_line_string.split("||||||");
    	$("#map_popup_div").attr("style","display:block;");
    	$("#map_popup_div").focus();
    	addPopupScrollClass('map_popup_div');
    	initialize(result_ary);
    }
    	  
	function initialize(result_ary) 
	{	
	  var length_pipeline = result_ary.length;
	  var origin_lat = $("#average_latitude_pipe_line").attr("value");
	  var origin_long = $("#average_longitute_pipe_line").attr("value");
	   var myLatLng = new google.maps.LatLng(origin_lat, origin_long);
	   var myOptions = {
		zoom: 2,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	 $("#distance_div").html('<?=t($t_base.'fields/your_companies_registered_cfs');?>');
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(origin_lat, origin_long)  
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);
	  
	  for(i=0;i<length_pipeline;i++)
	  {
	  	result_arr = result_ary[i].split("||||");
	  	var szLat = result_arr[0];
		var szLang = result_arr[1];
		var whs_name = result_arr[2];
		var origin_address = result_arr[3];
		 
		var origin = new google.maps.LatLng(szLat, szLang);  
		var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:whs_name
	  	}); 
	  	
	     var oaddress = '<div class="map-content"><p>'+origin_address+'</p></div>';
		 var oinfowindow = new google.maps.InfoWindow();
		 oinfowindow.setContent(oaddress);
		 google.maps.event.addListener(
			originmarker, 
			'click', 
			infoCallback(oinfowindow, originmarker)
		  );
	   	  //oinfowindow.open(map,originmarker);  
   	
	  }

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
}

function help_me_to_select()
{
  var latitude = $("#szLatitude").attr("value");
  var logitude = $("#szLongitude").attr("value");
  if(latitude!='' && logitude!='')
  {
  	  var result_ary=new Array();
	  result_ary[0]= latitude
	  result_ary[1]= logitude
	  result_ary[2]= "address needs to implement ";
	  $("#help_me_to_select_div").attr("style","display:block;");
	  $("#help_me_to_select_div").focus();
	  initialize_hep_me_select(result_ary);
  }
  else
  {
  	alert("latitute and longitude are required fields");
  } 
}
function initialize_hep_me_select()
{
	var olat = result_ary[0];
	var olang = result_ary[1];
	var origin_address = result_ary[2];
	 
	  var origin = new google.maps.LatLng(olat, olang);  
	  var origin1 = new google.maps.LatLng(olat+1, olang+1);  
	  var myLatLng = new google.maps.LatLng(olat, olang);
	  
	  var myOptions = {
		zoom: 1,
		center: myLatLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	 
	  map = new google.maps.Map(document.getElementById("map_canvas"),
		  myOptions);
	  geocoder = new google.maps.Geocoder();
	  var flightPlanCoordinates = [
		new google.maps.LatLng(olat, olang)  
	  ];
	  var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		strokeColor: "#7b6493",
		strokeOpacity: 1.0,		
		strokeWeight: 2
	  });

	  flightPath.setMap(map);

	  function infoCallback(infowindow, marker) { 
          return function() {
            infowindow.open(map, marker);
          };
        }	
	  
	  var originmarker = new google.maps.Marker({
		  position: origin,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"Origin"
	  }); 

	  var oaddress = '<div class="map-content"><p>'+origin_address+'</p></div>';
	  var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker, 
		'click', 
		infoCallback(oinfowindow, originmarker)
	  );
   	oinfowindow.open(map,originmarker);     
   	
   	
   	var originmarker1 = new google.maps.Marker({
		  position: origin1,
		  map: map,
		  icon: originIcon,
		  visible: true,
		  title:"Origin1"
	  }); 

	  var oaddress = '<div class="map-content"><p>'+origin_address+'</p></div>';
	  var oinfowindow2 = new google.maps.InfoWindow();
	  oinfowindow2.setContent(oaddress);
	  google.maps.event.addListener(
		originmarker1, 
		'click', 
		infoCallback(oinfowindow2, originmarker1)
	  );
   	oinfowindow2.open(map,originmarker1); 
}

    function updateValue(latitude, longitude)
    {
        document.getElementById("szLatitude").value = latitude ;
        document.getElementById("szLongitude").value = longitude ;
    }  
</script> 
 <div id="popup_container_google_map" style="display:none;">
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup" style="width:720px;margin-top:30px;"> 
            <iframe id="google_map_target_1" class="google_map_select"  style="width:720px;height: 400px"  scrolling="no" name="google_map_target_1" src="#" > </iframe>
        </div>
    </div>
</div>
<input type="hidden" name="iScrollValue" id="iScrollValue" value="0">
<div id="task_pending_tray_main_container">
    <div id="pending_task_list_main_container">
        <div class="clearfix accordion-container">
            <span class="accordian"><?php echo t($t_base.'title/pending_tray')?>&nbsp;<a href="javascript:void(0);"  id="pending_task_list_open_close_link" <?php if((int)$kBookingNew->idBookingFile>0){?> class="open-icon" onclick="close_open_div('pending_task_main_list_container_div','pending_task_list_open_close_link','open');" <?php }else {?> onclick="close_open_div('pending_task_main_list_container_div','pending_task_list_open_close_link','close');" class="close-icon" <?php }?>></a></span>
            <span class="accordion-btn">
                <a href="javascript:void(0);" id="button_close_file" style="opacity:0.4"  class="button2"><span><?php echo t($t_base.'title/close')?></span></a>
                <a href="javascript:void(0);" id="button_copy_file_header" style="opacity:0.4"  class="button1"><span><?php echo t($t_base.'fields/copy')?></span></a>
                <a href="javascript:void(0);" id="button_quick_quote_file_header" style="opacity:0.4"  class="button1"><span>Quick Quote</span></a>
                <a href="javascript:void(0);" onclick="display_recent_files_by_owner('DISPLAY_RECENT_FILES_SEEN_BY_OPERATOR','<?php echo __FILE_RECENT_MODIFIED__; ?>');"  class="button1"><span><?php echo t($t_base.'title/recent')?></span></a>
                <a href="javascript:void(0);" onclick="display_recent_files_by_owner('DISPLAY_SEARCH_PENDING_TASK_POPUP')"  class="button1"><span><?php echo t($t_base.'title/search')?></span></a>
                <a href="javascript:void(0);" onclick="display_pending_task_overview('ADD_NEW_BOOKING_FILE');" class="button1"><span><?php echo t($t_base.'title/new')?></span></a> 
                <span class="x" onmouseover="showHideReAssignDrodown('calender_more_popup');" onmouseout="showHideReAssignDrodown('calender_more_popup');">
                    <?php if($kAdmin->szProfileType!='' && $kAdmin->szProfileType!='__FINANCE__') {?>
                    <a href="javascript:void(0);" id="" class="button1"><span>User</span></a>
                    <?php }?>
                    <div style="display:none;" class="calender_more_popup" id="calender_more_popup">   
                        <img class="calender_more_popup_arrow" alt="arrow" src="<?php echo __BASE_STORE_IMAGE_URL__?>/calender_more_popup-arrow.png"> 
                        <form action="" name="transporteca_team_form" id="transporteca_team_form" method="post">
                            <input type="hidden" name="transportecaTeamAry[idAdmin][0]" id="idAdmin_0" value="<?php echo $_SESSION['admin_id']; ?>"> 
                            <ul> 
<!--                        <li><img alt="arrow" src="<?php echo __BASE_STORE_IMAGE_URL__."/yes-tick.jpg"; ?>"> <?php echo $szOperatorName; ?></li>-->
                                <li class="user-tray-popup-li"><input type="checkbox" checked disabled="disabled" ><?php echo $szOperatorName; ?></li>
                                <li><input type="checkbox" onclick="display_pending_task_list_byteam();" <?php if($bNotAllocatedFlag){?>checked<?php }?> name="transportecaTeamAry[idAdmin][1]" id="idAdmin_1" value="0"> &nbsp;Not Allocated</li> 
                                <?php
                                    if(!empty($activeAdminAry))
                                    {
                                        $counter = 2;
                                        $transportecaTeamAry = $_SESSION['sessTransportecaTeamAry'];
                                        foreach($activeAdminAry as $activeAdminArys)
                                        {
                                            $szCheckedString = '';
                                            if(!empty($transportecaTeamAry) && in_array($activeAdminArys['id'],$transportecaTeamAry))
                                            {
                                                $szCheckedString = "checked='checked' ";
                                            }
                                            ?>
                                            <li><input type="checkbox" <?php echo $szCheckedString; ?> onclick="display_pending_task_list_byteam();" name="transportecaTeamAry[idAdmin][<?php echo $counter; ?>]" id="idAdmin_<?php echo $counter; ?>" value="<?php echo $activeAdminArys['id'];?>"> &nbsp;<?php echo $activeAdminArys['szFirstName']." ".$activeAdminArys['szLastName']; ?></li>
                                            <?php
                                            $counter++;
                                        }
                                    }
                                ?>
                            </ul> 
                        </form>
                   </div>
                </span>
            </span>
        </div>
        <script type="text/javascript"> 
            var openDiv; 
            function toggleDiv(divID) {
                 $("#" + divID).fadeToggle(200, function() {
                     openDiv = $(this).is(':visible') ? divID : null;
                 });
            } 
            $(document).click(function(e) {
                 if (!$(e.target).closest('#'+openDiv).length) {
                     toggleDiv(openDiv);
                 }
            });  
         </script>
        <div class="clear-all"></div> 
        <div id="pending_task_listing_container" ng-app="pendingTrayApp" ng-controller="tasksController">
            <?php
                $szBookingRefRedirectFlag=false;
                $only_highlight_bc=false;
                if((int)$kBookingNew->idBookingFile>0)
                {
                    $szBookingRefRedirectFlag=true;
                    $only_highlight_bc=true;
                    
                    /*
                    * Adding file visited logs
                    */
                    $kPendingTray->addBookingFileVisitedLogs($idBookingFile);
                }
                echo display_all_peding_tasks($incompleteTaskAry,$kBookingNew->idBookingFile,$only_highlight_bc,$szBookingRefRedirectFlag,false,false,true);
            ?>
        </div>
    </div>  
    
    <div id="pending_task_overview_main_container"  class="pending-tray-scrollable-div-overview" <?php if((int)$kBookingNew->idBookingFile==0){?>style="display:none;"<?php }?>> 
        <?php if((int)$kBookingNew->idBookingFile>0){
        echo display_pending_tray_overview($kBookingNew);
        }
        ?>
    </div> 
    <div id="pending_task_tray_main_container" class="pending-tray-scrollable-div-task-pane" <?php if((int)$kBookingNew->idBookingFile==0){?>style="display:none;"<?php }?>>
        <?php if((int)$kBookingNew->idBookingFile>0){
            echo display_pending_task_tray_container($kBookingNew);
        }
        ?>
    </div>
</div> 
</div>	
<div id="pdfDiv" style="display:none;">                   
<div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup pdf-popup">
            <p class="close-icon" align="right">
                <a href="javascript:void(0);" onclick="closePdfDiv();">
                <img src="<?php echo __BASE_STORE_IMAGE_URL__?>/close1.png" alt="close">
                </a>
            </p>
            <iframe id="showPdf" width="795px" height="500px;">

           </iframe>
        </div> 
    </div>
 </div>
<div id="delayed_message_container_div" style="display:none;">
    <?php echo displayDelayedResponseError(); ?>
</div>
<input type="hidden" name="idFileForClose" id="idFileForClose" value=""> 
<input type="hidden" name="idFileForCloseCount" id="idFileForCloseCount" value="0">
<input type="hidden" name="idFileForCloseCtr" id="idFileForCloseCtr" value="">
                        
<form method="post" id="google_map_hidden_form" action="<?=__BASE_MANAGEMENT_URL__?>/googleMapPendingTray.php" target="google_map_target_1">
    <input type="hidden" name="cfsHiddenAry[szAddressLine]" id="szAddressLine_hidden" value="">  
</form>
<span id="timer_span_id"></span>
<span id="watch_span_id"></span>
<?php  if((int)$_SESSION['idBookingFileLabel']>0){ ?>
    <script type="text/javascript">
        display_pending_task_overview('DISPLAY_PENDING_TASK_COURIER_LABEL_UPLOAD_FORM','<?php echo $_SESSION['idBookingFileLabel'];?>','SEARCH_TASK'); 
    </script>
    <?php  
    unset($_SESSION['idBookingFileLabel']);
    $_SESSION['idBookingFileLabel']=0;
}
//|| __ENVIRONMENT__=='DEV_LIVE'
?>
<script type="text/javascript">
    <?php  if($idBookingFile>0){ ?> 
        enableQuickQuotebutton('<?php echo $idBookingFile; ?>'); 
    <?php } ?> 
   
   <?php  //if(__ENVIRONMENT__ == "LIVE"){ ?> 
        /*
        * FOllowing function will called automatically to load pending tray task list in every 10 mins
        */
        var Interval = 0;  
        var d = new Date(); // for now 
        var iSecond = d.getSeconds();
        var iMinute = d.getMinutes();
        var iHour = d.getHours();
        var szDateTime = '';
        window.setInterval(function(){
            if(iSecond==60)
            {
                iMinute++;
                iSecond = 0;
            }
            if(iMinute==60)
            {
                iHour++;
                iMinute = 0;
            } 
            if(iHour==24)
            {
                iHour = 0;
            }
            szDateTime = formatDateTime(iHour) + ":" + formatDateTime(iMinute) + ":" + formatDateTime(iSecond);
            Interval++;
            iSecond++;
            
            //$("#timer_span_id").html("Stop Watch: "+Interval);
            //$("#watch_span_id").html("<br>"+szDateTime);
        }, 1000);
         
        function formatDateTime(iNumber)
        {
            if(iNumber<10)
            {
                return "0"+iNumber;
            }
            else
            {
                return iNumber;
            }
        }
   <?php //} ?>
</script>
<input id="iPendingTrayAutoRefreshCalled" name="iPendingTrayAutoRefreshCalled" value="1" type="hidden">
<input id="iStatusChanged" name="iStatusChanged" value="0" type="hidden">
<input id="iPendingTrayPage" name="iPendingTrayPage" value="1" type="hidden">
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>