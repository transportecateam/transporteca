<?php
ob_start();
session_start();
$display_profile_not_completed_message=true;
$szMetaTitle="Transporteca | CFS Locations";
ini_set (max_execution_time,180000); 
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL); 
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );

$kCourierService = new cCourierServices();

$iSplitData = true;
// If you want to ignore the uploaded files, 
// set $demo_mode to true;
$output_dir = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/";
$allowed_ext = array('pdf');
  
if(isset($_FILES["myfile"]))
{
    $ret = array(); 
    $error =$_FILES["myfile"]["error"];
    { 
    	if(!is_array($_FILES["myfile"]['name'])) //single file
    	{
            $RandomNum   = time();
            
            $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
            $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
         
            $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
            $ImageExt       = str_replace('.','',$ImageExt);
            $ImageName      = preg_replace_callback("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt; 
              
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $NewImageName); 
             
            if($iSplitData)
            {
                $szNewImagePath = trim($output_dir); 
                $NewImageName = trim($NewImageName); 
                $ret = $kCourierService->splitUploadedLabels($NewImageName,$szNewImagePath);
                
                if(!empty($ret) && is_array($ret))
                { 
                    echo json_encode($ret); 
                }
                else
                { 
                    $output_dir = __UPLOAD_COURIER_LABEL_PDF__."/";
                    $old_file_dir = __UPLOAD_COURIER_LABEL_PDF_MANAGEMENT__."/";
        
                    $szOldPdfFilePath = $old_file_dir."".$NewImageName ;
                    $szNewPdfFilePath = $output_dir."".$NewImageName ;
                    
                    @rename($szOldPdfFilePath, $szNewPdfFilePath);
                    $ret[0]['name']= $NewImageName;  
                    ob_end_clean(); 
                    echo json_encode($ret);
                }
            }
            else
            { 
                $ret[0]['name']= $NewImageName; 
                echo json_encode($ret);
            }
    	}
    	else
    	{
            $fileCount = count($_FILES["myfile"]['name']);
            for($i=0; $i < $fileCount; $i++)
            {
                $RandomNum   = time(); 
                $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name'][$i]));
                $ImageType      = $_FILES['myfile']['type'][$i]; //"image/png", image/jpeg etc. 
                $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
                $ImageExt       = str_replace('.','',$ImageExt);
                $ImageName      = preg_replace_callback("/\.[^.\s]{3,4}$/", "", $ImageName);
                $NewImageName = $ImageName.'-'.$RandomNum.'.'.$ImageExt; 
                
                $ret[$NewImageName]= $NewImageName;
                move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$NewImageName );
            }
    	}
    } 
}
?>