<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code));
//require_once(__APP_PATH_LAYOUT__."/forwarder_header.php");
$t_base = "BulkUpload/";
constantApiKey();
//checkAuthForwarder();
$kConfig = new cConfig();
$request_searched_ary=array();
$request_searched_ary = $_REQUEST['cfsHiddenAry'] ;
$idForwarder =$_SESSION['forwarder_id'];
if($request_searched_ary['idForwarder']!=$idForwarder)
{
	//header("Location:".__FORWARDER_HOME_PAGE_URL__);
	//exit;
}
$szCountryName = $kConfig->getCountryName(sanitize_all_html_input(trim($request_searched_ary['szCountry'])));
$szLatitute = sanitize_all_html_input(trim($request_searched_ary['szLatitude']));
$szLongitude = sanitize_all_html_input(trim($request_searched_ary['szLongitude']));
if((float)$szLatitute == 0)
{
	$szLatitute = 0;
}
if((float)$szLongitude == 0)
{
	$szLongitude = 0;
}
/*
if($request_searched_ary['szPostCode'] == t($t_base.'fields/optional'))
{
	$request_searched_ary['szPostCode'] = '';
}
if($request_searched_ary['szCity'] == t($t_base.'fields/type_name'))
{
	$request_searched_ary['szCity'] = '';
}
*/
$address_line = sanitize_all_html_input(trim($request_searched_ary['szAddressLine1'])).(sanitize_all_html_input(trim($request_searched_ary['szAddressLine1']))?", ":"").sanitize_all_html_input(trim($request_searched_ary['szAddressLine2']));
$address_line = $address_line.(sanitize_all_html_input(trim($request_searched_ary['szAddressLine2']))?", ":"").sanitize_all_html_input(trim($request_searched_ary['szAddressLine3'])).(sanitize_all_html_input(trim($request_searched_ary['szAddressLine3']))?", ":"").sanitize_all_html_input(trim($request_searched_ary['szPostCode'])).(sanitize_all_html_input(trim($request_searched_ary['szPostCode']))?", ":"").sanitize_all_html_input(trim($request_searched_ary['szCity'])).(sanitize_all_html_input(trim($request_searched_ary['szCity']))?", ":"").sanitize_all_html_input(trim($request_searched_ary['szState'])).(sanitize_all_html_input(trim($request_searched_ary['szState']))?", ":"").$szCountryName ;
//echo $szLatitute ;
//$address_line='1600 Amphitheatre Pky, Mountain View, CA';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
        <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
		<link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
		<script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/forwarder.js"></script>
    <script src="<?php echo __GOOGLE_MAP_JSON_URL__;?>" type="text/javascript"></script>
    <!--   <SCRIPT type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/mapctrl.js"></SCRIPT>-->
     
<script type="text/javascript">

// Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html

var map = null;
var geocoder = null;
var latsgn = 1;
var lgsgn = 1;
var zm = 0; 
var marker = null;
var posset = 0;
var markersArray = [];

function xz() {
//if (GBrowserIsCompatible()) {
//addPopupScrollClass();
 var origin_lat = '<?=$szLatitute?>';
  var origin_long = '<?=$szLongitude?>';
   var myLatLng = new google.maps.LatLng(origin_lat, origin_long);
   var myOptions = {
	zoom: 2,
	zoomControl: true,
	center: myLatLng,
	scaleControl: true,
    overviewMapControl: true,
    scrollwheel: true,
    disableDoubleClickZoom: true,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	zoomControlOptions: {
              style: google.maps.ZoomControlStyle.LARGE,
              position: google.maps.ControlPosition.LEFT_CENTER
          }
  };
map = new google.maps.Map(document.getElementById("map"),myOptions);
//map.setCenter(new google.maps.LatLng('<?=$szLatitute?>','<?=$szLongitude?>'), 2);
/*map.setMapType(G_NORMAL_MAP);
map.addControl(new GLargeMapControl());
map.addControl(new MapTypeControl());
map.addControl(new GScaleControl());*/
//map.enableScrollWheelZoom();
//map.disableDoubleClickZoom();
geocoder = new google.maps.Geocoder();
//marker = new GMarker(new google.maps.LatLng('<?=$szLatitute?>','<?=$szLongitude?>'), {draggable: true});

var properties = {
        position: new google.maps.LatLng('<?=$szLatitute?>', '<?=$szLongitude?>'),
        map: map,
        draggable: true
    };

var marker = new google.maps.Marker(properties);
//map.addOverlay(marker);

google.maps.event.addListener(map, 'click', function(event) 
{

//alert('hi2');
posset = 1;
clearOverlays();
fc( event.latLng) ;
//marker.setPoint(point);
computepos(event.latLng);
});


google.maps.event.addListener(marker, "click", function() {
var point = marker.getPosition();
 var oinfowindow = new google.maps.InfoWindow();
	  oinfowindow.setContent(marker.getPosition().toUrlValue(6));
	  google.maps.event.addListener(
		marker, 
		'click'
	  );
   	oinfowindow.open(map,marker);
//marker.openInfoWindowHtml(marker.getPosition().toUrlValue(6));
//alert(marker.getPosition().toUrlValue(6));
computepos (point);

});
markersArray.push(marker);


google.maps.event.addListener(marker, 'dragstart', function(overlay,point_marker)  { //Add drag start event
	     		computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'drag', function(overlay,point_marker)  { //Add drag event
	        	
				computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'dragend', function(overlay,point_marker)  { //Add drag end event
	    	       computepos(marker.getPosition());
	    });
//}
}

function computepos (point)
{
//alert(point.toUrlValue(6));
//var pointPixel = map.getProjection().fromLatLngToPoint(point);
var newPoints=point.toUrlValue(6);
var myarr = newPoints.split(",");
var latA = Math.abs(Math.round(value=myarr[1] * 1000000.));
var lonA = Math.abs(Math.round(value=myarr[0] * 1000000.));

if(value=point.y < 0)
{
	var ls = '-' + Math.floor((latA / 1000000));
}
else
{
	var ls = Math.floor((latA / 1000000));
}

var lm = Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60);
var ld = ( Math.floor(((((latA/1000000) - Math.floor(latA/1000000)) * 60) - Math.floor(((latA/1000000) - Math.floor(latA/1000000)) * 60)) * 100000) *60/100000 );

if(value=point.x < 0)
{
  var lgs = '-' + Math.floor((lonA / 1000000));
}
else
{
	var lgs = Math.floor((lonA / 1000000));
}

var lgm = Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60);
var lgd = ( Math.floor(((((lonA/1000000) - Math.floor(lonA/1000000)) * 60) - Math.floor(((lonA/1000000) - Math.floor(lonA/1000000)) * 60)) * 100000) *60/100000 );
//var pixelpoint = map.fromPointToLatLng(point);
//alert(lgm);
document.getElementById("latbox").value=myarr[0];
document.getElementById("lonbox").value=myarr[1];


}

function showAddress() {
geocoder = new google.maps.Geocoder();
var address = document.getElementById("address").value;

//alert(address);
//alert(markersArray.length);
clearOverlays();
 if (geocoder) {
 geocoder.geocode({ 'address': address }, function (results, status) {
    if (status == google.maps.GeocoderStatus.OK) {    
        
        //marker.setMap(map);       
        var point = results[0].geometry.location;
        fc( point) ;
 		computepos(point);
 		
 		
    }
    else
    {
    	alert(address + " not found");
    }
});
//alert(markersArray);
 }
}

function showLatLong(latitude, longitude) 
{
	if (isNaN(latitude)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(longitude)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	
	latitude1 = Math.abs( Math.round(latitude * 1000000.));
	if(latitude1 > (90 * 1000000)) 
	{ 
		alert(' Latitude must be between -90 to 90. ');  
		document.getElementById("latbox1").value=''; 
		return;
	}
	longitude1 = Math.abs( Math.round(longitude * 1000000.));
	if(longitude1 > (180 * 1000000)) { alert(' Longitude must be between -180 to 180. ');  document.getElementById("lonbox1").value='';  return;}
	
	var point = new GLatLng(latitude,longitude);
	
	posset = 1;
	
	if (zm == 0)
	{
		//map.setMapType(G_HYBRID_MAP);
		map.setMapType(G_NORMAL_MAP);
		map.setCenter(point,16);
		zm = 1;
	}
	else
	{
		map.setCenter(point);
	}
	
	 var html = "";
	 html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
	
	 var baseIcon = new GIcon();
	 baseIcon.iconSize=new GSize(32,32);
	 baseIcon.shadowSize=new GSize(56,32);
	 baseIcon.iconAnchor=new GPoint(16,32);
	 baseIcon.infoWindowAnchor=new GPoint(16,0);
	 var thisicon = new GIcon(baseIcon, "http://itouchmap.com/i/blue-dot.png", null, "http://itouchmap.com/i/msmarker.shadow.png");
	
	 var marker = new GMarker(point,thisicon);
	 var properties = {
        position: point,
        map: map,
        draggable: true
	};
	
	var marker = new google.maps.Marker(properties);
	 google.maps.event.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
	 map.addOverlay(marker);
	
	 google.maps.event.trigger(marker, "click");
}

function showLatLong1(latitude, latitudem,latitudes, longitude,  longitudem,  longitudes) 
{
	if (isNaN(latitude)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(latitudem)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(latitudes)) {alert(' Latitude must be a number. e.g. Use +/- instead of N/S'); return false;}
	if (isNaN(longitude)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	if (isNaN(longitudem)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	if (isNaN(longitudes)) {alert(' Longitude must be a number.  e.g. Use +/- instead of E/W'); return false;}
	
	if(latitude < 0)  { latsgn = -1; }
	alat = Math.abs( Math.round(latitude * 1000000.));
	if(alat > (90 * 1000000)) 
	{ 
		alert(' Degrees Latitude must be between -90 to 90. ');  
		return; 
	 }
	latitudem = Math.abs(Math.round(latitudem * 1000000.)/1000000);
	absmlat = Math.abs(Math.round(latitudem * 1000000.));  //integer
	if(absmlat >= (60 * 1000000)) 
	{  
		alert(' Minutes Latitude must be between 0 to 59. ');
		return;
	}
	latitudes = Math.abs(Math.round(latitudes * 1000000.)/1000000);
	absslat = Math.abs(Math.round(latitudes * 1000000.));
	if(absslat > (59.99999999 * 1000000)) 
	{  
		alert(' Seconds Latitude must be between 0 and 59.99. '); 
		document.getElementById("latbox1ms").value=''; 
		return; 
	}
	
	if(longitude < 0)  
	{
	 lgsgn = -1; 
	}
	alon = Math.abs( Math.round(longitude * 1000000.));
	if(alon > (180 * 1000000)) 
	{
	  alert(' Degrees Longitude must be between -180 to 180. ');
	  return;
	}
	longitudem = Math.abs(Math.round(longitudem * 1000000.)/1000000);
	absmlon = Math.abs(Math.round(longitudem * 1000000));
	if(absmlon >= (60 * 1000000))   
	{  
		alert(' Minutes Longitude must be between 0 to 59. '); 
		return;
	}
	longitudes = Math.abs(Math.round(longitudes * 1000000.)/1000000);
	absslon = Math.abs(Math.round(longitudes * 1000000.));
	if(absslon > (59.99999999 * 1000000)) 
	{  
		alert(' Seconds Longitude must be between 0 and 59.99. '); 
		return;
	}
	
	latitude = Math.round(alat + (absmlat/60.) + (absslat/3600.) ) * latsgn/1000000;
	longitude = Math.round(alon + (absmlon/60) + (absslon/3600) ) * lgsgn/1000000;
	
	var point = new GLatLng(latitude,longitude);
	posset = 1;
	
	if (zm == 0)
	{
		//map.setMapType(G_HYBRID_MAP);
		map.setMapType(G_NORMAL_MAP);
		map.setCenter(point,16);
		zm = 1;
	}
	else
	{
		map.setCenter(point);
	}
	 var html = "";
	 html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
	
	 var baseIcon = new GIcon();
	 baseIcon.iconSize=new GSize(32,32);
	 baseIcon.shadowSize=new GSize(56,32);
	 baseIcon.iconAnchor=new GPoint(16,32);
	 baseIcon.infoWindowAnchor=new GPoint(16,0);
	 var thisicon = new GIcon(baseIcon, "http://itouchmap.com/i/blue-dot.png", null, "http://itouchmap.com/i/msmarker.shadow.png");
	
	 var marker = new GMarker(point,thisicon);
	 google.maps.event.addListener(marker, "click", function() {marker.openInfoWindowHtml(html);});
	 map.addOverlay(marker);
	
	 GEvent.trigger(marker, "click");
}

function streetview()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the street view point.");
		return;
	}
	
	var point = map.getCenter();
	var t1 = String(point);
	t1 = t1.replace(/[() ]+/g,"");
	var str = "http://www.streetviews.co?e=" + t1;
	var popup = window.open(str, "streetview");
	popup.focus();
}

function googleearth()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the Google Earth map point.");
		return;
	}
	var point = map.getCenter();
	var gearth_str = "http://gmap3d.com?r=3dmap&mt=Latitude-Longitude Point&ml=" + point.y+ "&mg=" + point.x;
	var popup = window.open(gearth_str, "googleearth");
	popup.focus();
}

function pictures()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the photograph point.");
		return;
	}
	var point = map.getCenter();
	var pictures_str = "http://ipicture.mobi?r=pictures&mt=Latitude-Longitude Point&ml=" + point.y+ "&mg=" + point.x;
	var popup = window.open(pictures_str, "pictures");
	popup.focus();
}

function lotsize()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the lot size map point.");
		return;
	}
	var point = map.getCenter();
	var t1 = String(point);
	t1 = t1.replace(/[() ]+/g,"");
	var vpike_str = "http://viewofhouse.com?e=" + t1 + "::findlotsize:";
	var popup = window.open(vpike_str, "lotsize");
	popup.focus();
}

function getaddress()
{
	if (posset == 0)
	{
		alert("Position Not Set.  Please click on the spot on the map to set the get address map point.");
		return;
	}
	var point = map.getCenter();
	var t1 = String(point);
	t1 = t1.replace(/[() ]+/g,"");
	var getaddr_str = "http://www.getaddress.net?latlng=" + t1;
	var popup = window.open(getaddr_str, "getaddress");
	popup.focus();
}

function fc( point )
{
	//alert('hi');
	 var html = "";
	 html += html + "Latitude, Longitude<br>" + point.toUrlValue(6);
	//alert(html);
	 var baseIcon = new google.maps.MarkerImage();
	 baseIcon.iconSize=new google.maps.Size(32,32);
	 baseIcon.shadowSize=new google.maps.Size(56,32);
	 baseIcon.iconAnchor=new google.maps.Point(16,32);
	 baseIcon.infoWindowAnchor=new google.maps.Point(16,0);
	 var thisicon = new google.maps.MarkerImage(baseIcon, '<?=__BASE_STORE_IMAGE_URL__.'/red-marker.png'?>', null, "http://itouchmap.com/i/msmarker.shadow.png");
	
	// var marker = new GMarker(point,{draggable: true});
	  var properties = {
        position: point,
        map: map,
        draggable: true
	};
	
	var marker = new google.maps.Marker(properties);
	markersArray.push(marker);
	map.setCenter(point);
	
	map.setZoom(8);
	
	 google.maps.event.addListener(marker, "click", function() {
	 //alert('hi');
		var oinfowindow = new google.maps.InfoWindow();
	  	oinfowindow.setContent(html);
	  	oinfowindow.open(map,marker);
	  });
	  
	 //map.addOverlay(marker);
	 
	 google.maps.event.addListener(marker, 'dragstart', function(overlay,point)  { //Add drag start event
	     		computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'drag', function(overlay,point)  { //Add drag event
	        	
				computepos(marker.getPosition());
	    });
	    google.maps.event.addListener(marker, 'dragend', function(overlay,point)  { //Add drag end event
	    	       computepos(marker.getPosition());
	    });
}


function createMarker(point, html) 
{
	 var marker = new GMarker(point);
	 google.maps.event.addListener(marker, "click", function()
	 {
	 	marker.openInfoWindowHtml(html);
	 });
	 return marker;
}

function reset() 
{
	clearOverlays();
	
	document.getElementById("latbox").value='';
	document.getElementById("lonbox").value='';
	
	
	var properties = {
        position: new google.maps.LatLng('<?=$szLatitute?>', '<?=$szLongitude?>'),
        map: map,
        draggable: true
    };

	var marker = new google.maps.Marker(properties);

	 var point = new google.maps.LatLng('<?=$szLatitute?>', '<?=$szLongitude?>');
    // fc( point) ;
 	 //computepos(point);
 	 map.setCenter(point);
	
	map.setZoom(2);
 	 google.maps.event.addListener(marker, "click", function() {
	//var point = marker.getPosition();
	 var oinfowindow = new google.maps.InfoWindow();
		  oinfowindow.setContent(point);
		  google.maps.event.addListener(
			marker, 
			'click'
		  );
	   	oinfowindow.open(map,marker);
	//marker.openInfoWindowHtml(marker.getPosition().toUrlValue(6));
	//alert(marker.getPosition().toUrlValue(6));
	computepos (point);

});
markersArray.push(marker);
}

function reset1() 
{
	marker.setPoint(map.getCenter());
}


function sendValue()
{
	var latitute = document.getElementById("latbox").value;
	var logitude = document.getElementById("lonbox").value;
    //window.parent.document.getElementById('szLatitude').value = latitute;
    //window.parent.document.getElementById('szLongitude').value = logitude;
    //alert("I m in googlemap.php   latitute "+latitute+" longitute "+logitude);
    window.top.window.insert_value_google_map(latitute,logitude);    
   // checkCfsDetails();
}

function clearOverlays() {
  //alert(markersArray.length);	
  if (markersArray) {
  for (var i = 0; i < markersArray.length; i++) {
  
  	//alert(markersArray[i]);
  	//alert(markersArray[i]);
    markersArray[i].setMap(null);
    //alert(i);
   }
   //markersArray=[];
    
  }
}


function show_address_on_submit(kEvent)
{
	var temp=(window.event)?kEvent.keyCode:kEvent.which;
	if(temp == 13)
	{
		showAddress();
	}
}
/*
function hide_google_map()
{
	window.parent.document.getElementsByTagName('body').class='';	
	window.parent.document.getElementsByTagName('body').style='' ;	
	window.parent.document.getElementById('popup_container_google_map').style.display = 'none';
	//parent.document.getElementById('mainClass').style.overflow = 'none';
	
	$("#mainClass",window.parent.document).removeClass("popupscroll");
	$('#mainClass',window.parent.document).removeAttr('style');
	$('.mainpage',window.parent.document).removeAttr('id');
	
//	$('#mainClass',parent.document).removeId('mainClass');
	//$("parent.document.body").removeClass("popupscroll");
	//$("parent.document.body").attr('style',"");
}
*/
</script>
  </head>

  <body onload="xz()">
  <div >
    <form action="javascript:void();" methos="post" >
      <p align="left"><strong><?=t($t_base.'title/cfs_google_map_page_title');?></strong></p>
      <br>
      <p align="left"><?=t($t_base.'title/cfs_google_map_bottom_text');?></p>
      <br>
      <div class="oh">
      <p class="fl-45" align="left" style="padding-top: 5px;"><?=t($t_base.'title/alternatively_type_address_here');?></p>
      <p class="fl-40" style="padding-top: 4px;">
        <input type="text" onkeyup="show_address_on_submit(event)" size="30" style="width:268px;" id="address" name="address" value="<?=$address_line?>" />
      </p>
      <p class="fl-15" align="right">
        <a href="javascript:void(0)" align="right" onclick="showAddress()" class="button1"><span style="min-width:60px;"><?=t($t_base.'fields/go');?></span></a>
      </p>
      </div>
        <div id="map" align="center" style="width: 700px; height: 300px"></div>
     </form> 
     <div class="map_note" align="left">
     <p>Note: <?=t($t_base.'title/note_map');?></p>
     </div>
     
     <br />
	 <div class="oh">
		 <p class="fl-15" align="right" style="margin-top: 5px;"> <?=t($t_base.'fields/latitude');?></p> 
		 <p class="fl-25" align="center" style="margin-top: 5px;"><input type="text" id="latbox" name="szLatitude" value="<?=$szLatitute?>"></p>
		 <p class="fl-15" align="right" style="margin-top: 5px;"> <?=t($t_base.'fields/longitude');?></p>
		 <p class="fl-25" align="center" style="margin-top: 5px;"><input type="text" id="lonbox" name="szLongitude" value="<?=$szLongitude?>"></p>
	
		 <p class="fl-20" align="right" ><a href="javascript:void(0);" onclick="reset()" class="button2"><span><?=t($t_base.'fields/clear_marker');?></span></a></p>	 
	</div>
	<p align="center">
		  	 <a href="javascript:void(0);" onclick="sendValue();" class="button1"><span><?=t($t_base.'fields/insert');?></span></a>
	    	 <a href="javascript:void(0);" onclick="window.top.window.hide_google_map()" class="button1"><span><?=t($t_base.'fields/close');?></span></a>  	  
	</p> 
	</div>
  </body>
</html>

