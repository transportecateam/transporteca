<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement_ajax();
$t_base="management/LandingPage/";
$t_base_error="management/Error/";

$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$kExplain = new cExplain();
if($mode == 'CHANGE_ORDER_LANDING_PAGE')
{
	$id = (int)$_POST['id'];
	$order = sanitize_all_html_input($_POST['move']);
	$szPageName = sanitize_all_html_input($_POST['page_name']);
	if($order == 'UP')
	{
		$iOrderBy = __ORDER_MOVE_DOWN__;
	}
	else if($order == 'DOWN')
	{
		$iOrderBy = __ORDER_MOVE_UP__;
	}
	if($szPageName=='NEW_LANDING_PAGE')
	{
		$kExplain->moveUpDownLandingPagesLinks($id,$iOrderBy,$szPageName);
		
		$landingPageDataAry = array();
		$landingPageDataAry = $kExplain->getAllNewLandingPageData();
		$iMaxOrder = $kExplain->getMaxOrderNewLandingPageData();
		
		echo "SUCCESS||||";
		echo viewNewLandingPageContent($t_base,$landingPageDataAry,$iMaxOrder);
		die;
	}
	else
	{
		$kExplain->moveUpDownLandingPagesLinks($id,$iOrderBy);
	
		$landingPageDataAry = array();
		$landingPageDataAry = $kExplain->getAllLandingPageData();
		$iMaxOrder = $kExplain->getMaxOrderLandingPageData();
		echo "SUCCESS||||";
		echo viewLandingPageContent($t_base,$landingPageDataAry,$iMaxOrder);
		die;
	}
}
else if($mode=='DELETE_LANDING_PAGE')
{
	$idLandingPage = (int)sanitize_all_html_input($_POST['id']);
	$szPageName = sanitize_all_html_input($_POST['page_name']);
	echo deleteConfirmation($idLandingPage,$szPageName); 
	die;
}
else if($mode=='DELETE_LANDING_PAGE_CONFIRM')
{
	$id = (int)$_POST['id'];
	$szPageName = sanitize_all_html_input($_POST['page_name']);
	if($szPageName=='NEW_LANDING_PAGE')
	{
		$kExplain->deleteLandingPage($id,$szPageName);
			
		$landingPageDataAry = array();
		$landingPageDataAry = $kExplain->getAllNewLandingPageData();
		$iMaxOrder = $kExplain->getMaxOrderNewLandingPageData();
		
		echo "SUCCESS||||";
		echo viewNewLandingPageContent($t_base,$landingPageDataAry,$iMaxOrder);
		die;
	}
	else
	{
		$kExplain->deleteLandingPage($id);	
		$landingPageDataAry = array();
		$landingPageDataAry = $kExplain->getAllLandingPageData();
		$iMaxOrder = $kExplain->getMaxOrderLandingPageData();
		
		echo "SUCCESS||||";
		echo viewLandingPageContent($t_base,$landingPageDataAry,$iMaxOrder);
		die;
	} 
}
else if($mode=='ADD_MORE_PARTNER_LOGO')
{
    $number1 = sanitize_all_html_input($_REQUEST['number']);
    $number = sanitize_all_html_input($_REQUEST['number'])+1;
    $mode = sanitize_all_html_input($_REQUEST['mode']);

    echo display_partner_logo_form($kExplain,$number);
    die;
}
else if($mode=='ADD_MORE_TESTIMONIAL')
{
    $number1 = sanitize_all_html_input($_REQUEST['number']);
    $number = sanitize_all_html_input($_REQUEST['number'])+1;
    $mode = sanitize_all_html_input($_REQUEST['mode']);
 
    echo display_testimonial_form($kExplain,$number);
    die;
} 
else if($mode=='DELETE_SEARCH_AID_WORDS')
{
    $idSearchAdwords = sanitize_all_html_input($_REQUEST['aid_word_id']);  
    if($idSearchAdwords>0)
    { 
        echo "SUCCESS||||" ;
        echo deleteSearchAidWordsConfirmation($idSearchAdwords);
        die;
    } 
}
else if($mode=='DELETE_SEARCH_AID_WORDS_CONFIRM')
{
    $idSearchAdwords = sanitize_all_html_input($_REQUEST['aid_word_id']);  
    if($kExplain->deleteSearchAidWords($idSearchAdwords))
    { 
        $_POST['addSearchAidWordsAry'] = array(); 
        
        echo "SUCCESS||||" ;
        echo add_edit_search_aid_words_lists($kExplain);
        echo "||||";
        echo display_search_aid_words_lists();
        die;
    } 
} 
else if($mode=='EDIT_SEARCH_AID_WORDS')
{
    $idSearchAdwords = sanitize_all_html_input($_REQUEST['aid_word_id']); 
    
    if($idSearchAdwords>0)
    {
        $searchAidwordsAry = array();
        $searchAidwordsAry = $kExplain->getAllsearchAidWords($idSearchAdwords);
        
        echo "SUCCESS||||" ;
        echo add_edit_search_aid_words_lists($kExplain,$searchAidwordsAry[0]);
        die;
    } 
}  
else if(!empty($_POST['addSearchAidWordsAry']))
{
    if($kExplain->addUpdateSearchAidWords($_POST['addSearchAidWordsAry']))
    {
        $_POST['addSearchAidWordsAry'] = array(); 
        
        echo "SUCCESS||||" ;
        echo add_edit_search_aid_words_lists($kExplain);
        echo "||||";
        echo display_search_aid_words_lists();
        die;
    }
    else
    {
        echo "ERROR||||";
        echo add_edit_search_aid_words_lists($kExplain);
        die;
    }
}
else if(!empty($_POST['landingPageArr']) && $mode=='ADD_NEW_LANDING_PAGE')
{
	$kExplain = new cExplain();
	if($kExplain->addNewLandingPage($_POST['landingPageArr']))
	{
            echo "REDIRECT||||" ;
            echo __MANAGEMENT_NEW_LANDING_PAGE_EDITOR_URL__ ;
            echo "||||";
            die;
	}	
	else if(!empty($kExplain->arErrorMessages))
	{
            echo "ERROR||||" ;
            ?>
            <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
            <div id="regErrorList">
                <ul>
                    <?php foreach($kExplain->arErrorMessages as $key=>$values)  {   ?>
                        <li><?=$values?></li>
                    <?php   } ?>
                </ul>
            </div>
            <?php
	}
}
else if(!empty($_POST['landingPageArr']) && $mode=='EDIT_NEW_LANDING_PAGE')
{
	$kExplain = new cExplain();
	if($kExplain->editNewLandingPage($_POST['landingPageArr']))
	{
		echo "REDIRECT||||" ;
		echo __MANAGEMENT_NEW_LANDING_PAGE_EDITOR_URL__ ;
		echo "||||";
		die;
	}	
	else if(!empty($kExplain->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kExplain->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?php
	 }
}
else if(!empty($_POST['landingPageArr']) && $mode=='PREVIEW_NEW_LANDING_PAGE')
{
	$kExplain = new cExplain();
	if($kExplain->validateNewLandingPageData($_POST['landingPageArr']))
	{
		echo "SUCCESS||||" ;
		echo previewLandingPage_admin($_POST['landingPageArr']);
		die;
	}
	else if(!empty($kExplain->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kExplain->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?php
	 }
}
else if(!empty($_POST['landingPageArr']) && $mode=='ADD')
{
	$kExplain = new cExplain();
	if($kExplain->addLandingPage($_POST['landingPageArr']))
	{
		echo "SUCCESS|||| add" ;
		die;
	}	
	else if(!empty($kExplain->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kExplain->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?php
	}
}
else if(!empty($_POST['landingPageArr']) && $mode=='EDIT')
{
	$kExplain = new cExplain();
	if($kExplain->editLandingPage($_POST['landingPageArr']))
	{
		echo "SUCCESS||||" ;
		die;
	}	
	else if(!empty($kExplain->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kExplain->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?php
	 }
}
else if(!empty($_POST['landingPageArr']) && $mode=='PREVIEW')
{
	$kExplain = new cExplain();
	if($kExplain->validateLandingPageData($_POST['landingPageArr']))
	{
		echo "SUCCESS||||" ;
		echo previewLandingPage_admin($_POST['landingPageArr']);
		die;
	}
	else if(!empty($kExplain->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kExplain->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?
	 }
}

?>
