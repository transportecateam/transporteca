<?php 
/**
  *  admin--- E-Mail log management
  */
define('PAGE_PERMISSION','__MESSAGE_RSS_TEMPLATES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | RSS Templates";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement(); 
$t_base="management/temp_email_mgmt/";

/*
 * We have discontinued using RSS feeds from 17th March 2017 - @Ajay
 */
ob_end_clean();
header("Location: ".__BASE_URL__);
die;

$kRss = new cRSS();
$rssFeedAry = array();
$rssFeedAry = $kRss->getAllRssTemplates();

?>
<div id="hsbody-2">
	<?php require_once( __APP_PATH_LAYOUT__ ."/admin_messages_left_nav.php" ); ?> 
	<div class="hsbody-2-right">
		<div id="rss_template_top">
			<?php echo rss_template_top_form($rssFeedAry); ?>
		</div>
		<div id="showdetails">
			<?php 
				echo display_rss_edit_form(true);
			?>
		</div>
		
	</div>	
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );

?>