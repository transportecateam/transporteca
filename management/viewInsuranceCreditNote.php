<?php
/**
 * View Invoice
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );

$kBooking = new cBooking();
validateManagement();
$idBooking=$_REQUEST['idBooking'];
if($idBooking>0)
{
	$flag=$_REQUEST['flag'];
	$pdfhtmlflag=true;
        $flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
	if($flag=='pdf')
	{
		$pdfhtmlflag=false;
	}
	
	$bookingInvoicePdf = getInsuranceCreditNotePdf($idBooking,$pdfhtmlflag);
	if($flag=='pdf')
	{
		download_booking_pdf_file($bookingInvoicePdf);
        	die;
	}
}
else
{
	header('Location:'.__BASE_URL__."/");
	exit(); 
} 
?>