<?php 
/**
  *  ADMIN--- NON ACKNOWLEDGE
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
$t_base="management/operations/statistics/";
validateManagement();
$kBooking = new cBooking;
if(isset($_POST['field']))
{
    $mode = trim(sanitize_all_html_input($_POST['mode']));
    $field = (int)$_POST['field']; 
    
    $kReport = new cReport();

    $operationStatsAry = array();
    $operationStatsAry = $kReport->getAllOperationStatisticsSnapshot();

    $dailySummeryAry = array() ;
    $weeklySummeryAry = array();
    $monthlySummeryAry = array();
    $forwarderBookingSummery = array();
    $customerLocationSummeryAry = array();

    if(!empty($operationStatsAry['szDailySummery']))
    {
        $dailySummeryAry = unserialize($operationStatsAry['szDailySummery']);
    } 
    if(!empty($operationStatsAry['szWeeklySummery']))
    {
        $weeklySummeryAry = unserialize($operationStatsAry['szWeeklySummery']);
    } 
    if(!empty($operationStatsAry['szMonthlySummery']))
    {
        $monthlySummeryAry = unserialize($operationStatsAry['szMonthlySummery']);
    }
    if(!empty($operationStatsAry['szForwarderBookingSummery']))
    {
        $forwarderBookingSummery = unserialize($operationStatsAry['szForwarderBookingSummery']);
    }
    if(!empty($operationStatsAry['szCustomerLocationSummery']))
    {
        $customerLocationSummeryAry = unserialize($operationStatsAry['szCustomerLocationSummery']);
    }
    if(!empty($operationStatsAry['szTopTradeSummery']))
    {
        $topTradeSummeryAry = unserialize($operationStatsAry['szTopTradeSummery']);
    }
    SWITCH($mode)
    {
        CASE 'SHOW_TABLE_ONE_DATA':
        {
            if($field>0)
            { 
                operationStat1($forwarderBookingSummery,$kBooking,$t_base,$field);
            }
            BREAK;
        }
        CASE 'SHOW_TABLE_TWO_DATA':
        {
            if($field>0)
            {
                operationStat2($customerLocationSummeryAry,$kBooking,$t_base,$field);
            }
            BREAK;
        }
        CASE 'SHOW_TABLE_THREE_DATA':
        {
            if($field>0)
            {
                operationStat3($topTradeSummeryAry,$kBooking,$t_base,$field);
            }
            BREAK;
        }
    }
}
?>