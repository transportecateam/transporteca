<?php
/**
 * Forwarder My Company  
 */
define('PAGE_PERMISSION','__LABEL_NOT_SENT__');
 ob_start();
session_start();
$szMetaTitle="Transporteca | Label Not Send";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base = "OBODataExport/";
$t_base_service = "SERVICEOFFERING/";
//$idForwarder =$_SESSION['forwarder_id'];
$kCourierServices= new cCourierServices();
//$kCourierServices->getCourierServices();
?>
<div id="hsbody-2">
    <div id="loader" class="loader_popup_bg" style="display:none;">
        <div class="popup_loader"></div>				
        <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
    </div>
    <div id="customs_clearance_pop" class="help-pop">&nbsp;</div> 
    <?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
    <div class="hsbody-2-right">
        <div id="courier_booking_list_container">
            <?php
            	$newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier();
                echo display_courier_confirmed_booking_listing($newCourierBookingAry);
            ?>
        </div>		
    </div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>