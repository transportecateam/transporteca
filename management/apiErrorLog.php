<?php
/**
  *BILLING DETAILS
  */
define('PAGE_PERMISSION','__API_ERROR_LOG__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Partner Api Error Log";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once (__APP_PATH__ ."/inc/functions.php");
require_once (__APP_PATH__ ."/inc/courier_functions.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/modeTransport/";
validateManagement();

?>
<div id="loader" class="loader_popup_bg" style="display:none;">
    <div class="popup_loader"></div>				
    <img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="show-request-call">
    
</div>    
<div id="customs_clearance_pop" class="help-pop">
</div>
<div id="hsbody-2">
	<?php require_once(__APP_PATH__ ."/layout/contentMaintainanceLeftNav.php"); ?>
	<div class="hsbody-2-right">
            <h4><strong>Error messages - last 30 days</strong></h4>
            <?php
                $kApi = new cApi();
                $errorResponseArr=$kApi->getPartnerAPIEventErrorLogs();
                if(!empty($errorResponseArr))
                {
                    foreach($errorResponseArr as $errorResponseArrs)
                    {
                        $errorResponseUnserialize=unserialize($errorResponseArrs);
                        $errorCodeArr=$errorResponseUnserialize['errors'];
                        if(!empty($errorCodeArr))
                        {
                            foreach($errorCodeArr as $errorCodeArrs)
                            {
                                $errorCodeCountArr[$errorCodeArrs['code']]['szCode']=$errorCodeArrs['code'];
                                $errorCodeCountArr[$errorCodeArrs['code']]['szDescription']=$errorCodeArrs['description'];
                                $errorCodeCountArr[$errorCodeArrs['code']]['iCount']=$errorCodeCountArr[$errorCodeArrs['code']]['iCount'] +1;
                            }
                        }
                    }
                }
                ?>
            
            <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="18%"><strong>Error message code</strong></td>
                    <td width="80%"><strong>Error message description</strong></td>
                    <td><strong>Count</strong></td>
                </tr>
            <?php
                $errorCodeCountArr = sortArray($errorCodeCountArr, 'iCount',true);
                if(!empty($errorCodeCountArr))
                {
                    foreach($errorCodeCountArr as $key=>$errorCodeCountArrs)
                    {
                        
                        ?>
                        <tr>
                            <td width="15%"><?php echo $errorCodeCountArrs['szCode'];?></td>
                            <td width="80%"><?php echo $errorCodeCountArrs['szDescription'];?></td>
                            <td><a href="javascript:void(0);" onclick="openRequestCallForApi('<?php echo $errorCodeCountArrs['szCode'];?>');"><?php echo $errorCodeCountArrs['iCount'];?></a></td>
                        </tr>
                       <?php 
                    }
                }
            ?>
            </table>
        
        </div>
    <intput id="szErrorCode" name="szErrorCode" value="" type="hidden"/>
</div>	
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>