<?php
/**
 * Edit Forwarder profile
 */
 ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );

require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/vat_functions.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
validateManagement();
$t_base="management/forwarderpayment/";
$kBooking = new cBooking();
$kBilling = new cBilling();

$pdf=new HTML2FPDFBOOKING();
if(!empty($_POST['confirmPayment']))
{ 
    $kFarwarder = new cForwarder();
    $kFarwarder->load($_POST['confirmPayment']['idForwarder']);
    $invoiceNumber = $kBilling->generateBatchNumberForPayment();
    $invoiceNumberBatchLabel = $invoiceNumber;
    $idForwarder = (int)$_POST['confirmPayment']['idForwarder'];
    $szForwarderCurrencyName = sanitize_all_html_input($_POST['confirmPayment']['currencyName']);
    $fTotalForwarderAmount = round($_POST['confirmPayment']['forwarderAmount'],2);
    $dtPaymentScheduled = sanitize_all_html_input($_POST['confirmPayment']['dtPaymentScheduled']);
    $dtPaymentScheduledDate = format_date($dtPaymentScheduled);
    
    $dtPaymentScheduledDateTime = strtotime($dtPaymentScheduledDate);
    $dtCurrentDateTime = strtotime(date('Y-m-d')); 
    /*
     * Either date is empty or less then today then returned with a error.
     */
    if(empty($dtPaymentScheduled) || $dtCurrentDateTime>$dtPaymentScheduledDateTime)
    {
        echo "DATE_ERROR||||";
        die;
    } 
    if($kAdmin->updateForwarderPaymentStatus($_POST['confirmPayment']['transferArr'],$invoiceNumber))
    {  
        $dtPaymentScheduledDate = $dtPaymentScheduledDate." ".date('H:i:s');        
        $forwarderTransactionExchangeRate=$kBilling->getExchangeRateBillingPrice($szForwarderCurrencyName,date('Y-m-d'));
        $forwarderAmountArr['idForwarder']= $idForwarder;
        $forwarderAmountArr['invoiceNumber']=$invoiceNumber;
        $forwarderAmountArr['forwarderAmount']= $fTotalForwarderAmount;
        $forwarderAmountArr['currencyName']= $szForwarderCurrencyName;
        $forwarderAmountArr['szDescription']='Automatic transfer';
        $forwarderAmountArr['fExchangeRate']=$forwarderTransactionExchangeRate;
        $forwarderAmountArr['dtPaymentScheduled'] = $dtPaymentScheduledDate;
        $kAdmin->insertForarderPayment($forwarderAmountArr);

        $uploadServiceAmount=$_POST['confirmPayment']['uploadServiceAmount'];
        if((float)$uploadServiceAmount>0)
        {
            $uploadServiceAmountArr['idForwarderCurrency']=$_POST['confirmPayment']['idForwarderCurrency'];
            $uploadServiceAmountArr['idForwarder']=$_POST['confirmPayment']['idForwarder'];
            $uploadServiceAmountArr['invoiceNumber']=$invoiceNumber;
            $uploadServiceAmountArr['fExchangeRate']=$forwarderTransactionExchangeRate;
            $kAdmin->updateForarderUplaodServicePayment($uploadServiceAmountArr);
              
            $uploadServiceStr ="Deduction for settlement of invoice number ".$invoiceNumber." ".$szForwarderCurrencyName." ".number_format((float)$uploadServiceAmount,2)."<br/>"; 
        }
        /*
        $getReferralFeeUSDValue=$kAdmin->getReferralFeeUSDValue($invoiceNumber);
        
        $referalFeeFeeExchangeRate=$kBilling->getExchangeRateBillingPrice($_POST['confirmPayment']['currencyName'],date('Y-m-d'));

        $referaalAmountArr['idForwarder']=$_POST['confirmPayment']['idForwarder'];
        $referaalAmountArr['invoiceNumber']=$invoiceNumber;
        $referaalAmountArr['referralFee']=round($_POST['confirmPayment']['referralfeeAmount'],2);
        $referaalAmountArr['currencyName']=$_POST['confirmPayment']['currencyName'];
        $referaalAmountArr['fExchangeRate']=$referalFeeFeeExchangeRate;
        if($_POST['confirmPayment']['referralfeeAmount']<0.00)
        {
            $referaalAmountArr['szDescription']='Transporteca Referral Fee Credit';
        }
        else{
        $referaalAmountArr['szDescription']='Transporteca referral fee';
        }
        $referaalAmountArr['fUSDReferralFee']=$getReferralFeeUSDValue;
         * 
         */ 
        if(!($kFarwarder->checkForwarderComleteBankDetails($idForwarder)))
        {
            $replace_ary = array();
            $replace_ary['szBank'] = $kFarwarder->szBankName;
            $replace_ary['szAccount'] = $kFarwarder->iAccountNumber;
            $replace_ary['sztotal'] = $szForwarderCurrencyName." ".number_format((float)$fTotalForwarderAmount,2);
            $replace_ary['szTotal'] = $szForwarderCurrencyName." ".number_format((float)$fTotalForwarderAmount,2);
            $replace_ary['szReferralfee'] = "";
            $replace_ary['szTransferamount'] = $szForwarderCurrencyName." ".number_format((float)$fTotalForwarderAmount,2);
            $replace_ary['szinvoce'] = $invoiceNumber;
            $replace_ary['fUploadServiceAmountStr'] = $uploadServiceStr;
            $replace_ary['szDate'] = date('j. F Y',strtotime('-1 second',strtotime(date('m').'/01/'.date('Y'))));
            $replace_ary['szMonth'] = date('F', strtotime(date('Y-m',strtotime($dtPaymentScheduledDate))." -1 month"));

            $idRoleAry=array('3');
            $kForwarder= new cForwarder();
            $getEmailArr=$kForwarder->getForwarderCustomerServiceEmail($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__);

            $attacfilename = getForwarderTransferPDF($idForwarder,$invoiceNumber,'PDF',true);  

            createEmail(__TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$getEmailArr,'', __FINANCE_CONTACT_EMAIL__,$replace_ary['idUser'], __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__,true,0,$attacfilename,false,false,false,false,'TRANSFER_CONFIRMATION'); 
        }
        else
        {
            $ForwarderAdminDetails = $kAdmin->findForwarderAdminsByBookingId($idForwarder);
            $replace_ary['szDate']=findPaymentFreq($ForwarderAdminDetails[0]['szPaymentFrequency']);
            if($ForwarderAdminDetails!= array())
            {
                foreach($ForwarderAdminDetails AS $key=>$value)
                {
                    $paymentFrequency = $value['szPaymentFrequency'];
                    $replace_ary['szFirstName']=$value['szFirstName'];
                    $replace_ary['szDisplayname']=$value['szDisplayName'];

                    $replace_ary['szHere']='<a href ="http://'.$value['szControlPanelUrl'].'/Company/BankDetails/">here</a>';
                    $to =  $value['szEmail'];
                    createEmail(__FORWARDER_TRANSFER_BANK_DETAILS_DUE__, $replace_ary, $to,'' , __FINANCE_CONTACT_EMAIL__,$value['id'] , __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__);
                }
            }
        }
        echo "SUCCESS_REDIRECT||||";
        echo __MANAGEMENT_FORWARDER_PAYMENT_URL__;
        die;  
    }
}
 if(!empty($_POST['transfer_id']))
 {
 	$transferArr=implode(',',$_POST['transfer_id']);
 	//echo $transferArr;
 	$totalAmountPaid=$kAdmin->getReferralFeeForwarderAmount($transferArr);
 	//print_r($totalAmountPaid);
 	$totalAmountCancelBookingPaid=$kAdmin->getReferralFeeForwarderAmountCancelBooking($transferArr);
        
        $totalBookingLabelCancel=0;
        $totalBookingLabelCancel=$kAdmin->getReferralFeeForwarderAmountCancelBookingLabel($transferArr);
 	//print_r($totalAmountCancelBookingPaid[0]['fPaidToForwarder']);
 	
 	$totalAmountCancelBooking=$totalAmountCancelBookingPaid[0]['fPaidToForwarder']-$totalBookingLabelCancel;
 	$totalReferralFeeCancelBooking=$totalAmountCancelBookingPaid[0]['referralfee'];
        
        /*
         * From Financial version 2 we are no longer displaying few lines please changes as follows:
         *  The “Referral fee” line and “Bookings and labels” are no longer relevant, so the only additional line which can be here is the “Upload service” line
         * @Ajay - 27-Feb-2017
         * 
         * $totalcourierLabelFee=$_POST['arrForwarderPayment']['totalcourierLabelFee'];
            $totalBookingHandlingFee = $_POST['arrForwarderPayment']['totalBookingHandlingFee']; 
            $handlingFeeVat = $_POST['arrForwarderPayment']['handlingFeeVat']; 

            if((float)$totalReferralFeeCancelBooking>0)
            {
                $referralfee=$_POST['arrForwarderPayment']['totalReferralFee'];
            }
            else
            {
                $referralfee=$_POST['arrForwarderPayment']['totalReferralFee'];
            } 
         * 
         * if($_POST['arrForwarderPayment']['szCurrencyName']!='USD')
        { 
            $totalcourierLabelUSDFee = $totalcourierLabelFee * $_POST['arrForwarderPayment']['fForwarderExchangeRate'];
            $totalBookingHandlingFeeUSD = $totalBookingHandlingFee * $_POST['arrForwarderPayment']['fForwarderExchangeRate'];
            $totalBookingHandlingFeeVatUSD = $handlingFeeVat * $_POST['arrForwarderPayment']['fForwarderExchangeRate'];
        } 
        */ 
        
        $tAmountPaid = $_POST['arrForwarderPayment']['farwordertotal'];   
 	$uploadServiceAmount = $_POST['arrForwarderPayment']['uploadServiceAmount'];
 	
 	if((float)$uploadServiceAmount>0)
 	{
            $tAmountPaid=$tAmountPaid;
 	}
 	else
 	{
            $tAmountPaid=$tAmountPaid;
 	} 
        if($totalcourierLabelFee>0)
        {
            $tAmountPaid=$tAmountPaid;
        } 
        if($totalBookingHandlingFee>0)
        {
            $tAmountPaid = $tAmountPaid;
        }   
        $idForwarder = $totalAmountPaid[0]['idForwarder'];
        /*
        * Calculating Transfer Date for Forwarder Billing screen.
        */
        $kBilling = new cBilling();
        $dtNextTransferDate = $kBilling->getNextTransferDate($idForwarder);
        
        $kForwarder = new cForwarder();
        $kForwarder->load($idForwarder);
        $iCreditDays = $kForwarder->iCreditDays;
        $szForwarderDisplayName = $kForwarder->szDisplayName;
        
        if($iCreditDays>0)
        {
            $dtPaymentScheduled = date('d/m/Y',strtotime("+".$iCreditDays." days"));
        }
        else
        {
            $dtPaymentScheduled = date('d/m/Y');
        } 
        $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
    ?>
		<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup" style="width:550px;">
                            <script type="text/javascript">
                                $().ready(function() {	
                                    $("#dtPaymentScheduled").datepicker({ <?php echo $date_picker_argument?> }); 
                                });
                            </script>
				<form name="forwarderPayment" id="forwarderPayment"	 method="post">	
				<h5><?=t($t_base.'messages/please_confirm_you_want_to');?>:</h5>
				<p class="oh">
				<span class="fl-60">Value of new transfer record for <?php echo $szForwarderDisplayName; ?></span>
				<span  style="float: right;"><?=$totalAmountPaid[0]['szForwarderCurrency']?> <?=number_format((float)$tAmountPaid,2)?></span>
				</p>
                                <?php
                                   if((float)$referralfee!='0.00'){
                                    
                                    if($referralfee<0.00)
                                    {
                                        $referralFee1=str_replace("-","",$referralfee);
                                        $totalreferralFeeText = "(".$totalAmountPaid[0]['szForwarderCurrency']." ".number_format((float)$referralFee1,2).")";
                                    }
                                    else
                                    {
                                        $totalreferralFeeText = $totalAmountPaid[0]['szForwarderCurrency']." ".number_format((float)$referralfee,2);
                                   }
                                    ?>
				<p class="oh">
				<span class="fl-60"><?=t($t_base.'messages/create_transporteca_referral_fee_record_of');?></span>
				<span  style="float: right;font-style: italic;"><?=$totalreferralFeeText?></span>
				</p>
                                <?php
                                   }
                                   if((float)$totalcourierLabelFee!='0.00'){
                                    
                                    if($totalcourierLabelFee<0.00)
                                    {
                                        $totalcourierLabelFee1=str_replace("-","",$totalcourierLabelFee);
                                        $totalcourierLabelFeeText = "(".$totalAmountPaid[0]['szForwarderCurrency']." ".number_format((float)$totalcourierLabelFee1,2).")";
                                    }
                                    else
                                    {
                                        $totalcourierLabelFeeText = $totalAmountPaid[0]['szForwarderCurrency']." ".number_format((float)$totalcourierLabelFee,2);
                                    }
                                    ?>
                                <p class="oh">
				<span class="fl-60"><?=t($t_base.'messages/courier_booking_and_labels_invoice');?></span>
				<span  style="float: right;font-style: italic;"><?=$totalcourierLabelFeeText?></span>
				</p>
                                <?php }?>
                                <?php
                                   if((float)$totalBookingHandlingFee!='0.00'){
                                    
                                    if($totalBookingHandlingFee<0.00)
                                    {
                                        $totalBookingHandlingFee1 = str_replace("-","",$totalBookingHandlingFee);
                                        $totalBookingHandlingFeeText = "(".$totalAmountPaid[0]['szForwarderCurrency']." ".number_format((float)$totalBookingHandlingFee1,2).")";
                                    }
                                    else
                                    {
                                        $totalBookingHandlingFeeText = $totalAmountPaid[0]['szForwarderCurrency']." ".number_format((float)$totalBookingHandlingFee,2);
                                    }
                                    ?>
                                    <p class="oh">
                                        <span class="fl-60"><?=t($t_base.'messages/handling_fee_invoice');?></span>
                                        <span style="float: right;font-style: italic;"><?=$totalBookingHandlingFeeText?></span>
                                    </p>
                                <?php }?>
				<p class="oh">
                                    <span class="fl-60">Scheduled payment date – will be stated on transfer record</span>
                                    <span style="float: right;">
                                        <input type='text' name="confirmPayment[dtPaymentScheduled]" style="width:60%;float:right;" readonly="true" id='dtPaymentScheduled' value="<?php echo $dtPaymentScheduled; ?>">
                                    </span>
				</p>
				<br/>
				<p align="center"><a href="javascript:void(0)" class="button2" onclick="cancelEditMAnageVar(0);" id="cancel_referral_payment_admin"><span><?=t($t_base.'fields/cancel');?></span></a>
                                    <a href="javascript:void(0)" class="button1" onclick="confirm_referral_payment();" id="confirm_referral_payment_admin"><span><?=t($t_base.'fields/confirm');?></span></a>
				</p>
				<input type="hidden" name="confirmPayment[idForwarder]" id="idForwarder" value="<?=$totalAmountPaid[0]['idForwarder']?>">
				<input type="hidden" name="confirmPayment[currencyName]" id="currencyName" value="<?=$totalAmountPaid[0]['szForwarderCurrency']?>">
				<input type="hidden" name="confirmPayment[idForwarderCurrency]" id="idForwarderCurrency" value="<?=$totalAmountPaid[0]['idForwarderCurrency']?>">
                                <input type="hidden" name="confirmPayment[courierLabelFee]" id="courierLabelFee" value="<?=$totalcourierLabelFee?>">
                                <input type="hidden" name="confirmPayment[courierUSDLabelFee]" id="courierUSDLabelFee" value="<?=$totalcourierLabelUSDFee?>"> 
                                <input type="hidden" name="confirmPayment[totalBookingHandlingFee]" id="totalBookingHandlingFee" value="<?php echo $totalBookingHandlingFee?>">
                                <input type="hidden" name="confirmPayment[totalBookingHandlingFeeUSD]" id="totalBookingHandlingFeeUSD" value="<?php echo $totalBookingHandlingFeeUSD?>">
                                 
                                <input type="hidden" name="confirmPayment[handlingFeeVat]" id="handlingFeeVat" value="<?php echo $handlingFeeVat?>">
                                <input type="hidden" name="confirmPayment[totalBookingHandlingFeeVatUSD]" id="totalBookingHandlingFeeVatUSD" value="<?php echo $totalBookingHandlingFeeVatUSD?>">
                                
				<input type="hidden" name="confirmPayment[transferArr]" id="transferArr" value="<?=$transferArr?>">
				<input type="hidden" name="confirmPayment[forwarderAmount]" id="forwarderAmount" value="<?=round((float)$tAmountPaid,2)?>">
				<input type="hidden" name="confirmPayment[referralfeeAmount]" id="referralfeeAmount" value="<?=round((float)$referralfee,2)?>">
				<input type="hidden" name="confirmPayment[uploadServiceAmount]" id="uploadServiceAmount" value="<?=round((float)$uploadServiceAmount,2)?>">
				</form>
			</div>
		</div>	
 	<?php
 	
 }
if(isset($_POST))
{
	$id		= sanitize_all_html_input($_POST['id']);
	$check	= sanitize_all_html_input($_POST['check']);
	$mode	= sanitize_all_html_input($_POST['mode']);
	$idForwarderCurrency= sanitize_all_html_input($_POST['idForwarderCurrency']);
	$kFarwarder = new cForwarder();
	$kFarwarder->load($check);
        $kAdmin = new cAdmin();
	if($check>0)
	{
            if($mode=="SHOW_CONFIRM_POP_UP")
            {	
                $billingDetails=$kAdmin->showEachForwarderPendingPayment($check,$idForwarderCurrency,true); 
                $serviceUploadPaymentArr=$kAdmin->showForwarderPendingPaymentServiceUpload($check,$idForwarderCurrency);
                
                /*
                 * In Financial version 2 we have deleted the “Referral Fee Invoice” line and “Courier Booking and Label fee…” line and the “Transporteca handling fee…” line @Ajay @date 27-Feb-2017
                 */
                 $szStyle = 'style="display:none;"';

                echo '<div id="popup-bg"></div>
                    <div id="popup-container">
                    <div class="multi-payment-transfer-parent-popup popup" style="margin-top:-10px;width:650px;">		
                    <h5>Pay '.$kFarwarder->szDisplayName.' now</h5>
                    '.t($t_base."messages/plz_select_bookin_wish");
                ?>
					
            <form id="paymentTransactions" method="post" name="paymentTransactions">
                <div class="multi-payment-transfer-popup">		
                <table cellpadding="0"  cellspacing="0" border="0" class="format-1" width="100%" id="booking_table">
                    <tr>
                        <th class="wd-4"></th>
			<th class="wds-24" style='text-align:left;' valign="top"><?=t($t_base.'fields/invoice_date')?> </th>
                        <th class="wds-24" style='text-align:left;' valign="top"><?=t($t_base.'fields/invoive')?> </th>
			<th class="wds-24" style='text-align:left;' valign="top"><?=t($t_base.'fields/booking')?> </th>
			<!--<th width="20%" style='text-align:right;' valign="top"><?=t($t_base.'fields/booking_value')?> </th>-->
			<!--<th width="10%" style='text-align:right;' valign="top"><?=t($t_base.'fields/fee')?> </th>-->
			<!--<th width="15%" style='text-align:right;' valign="top"><?=t($t_base.'fields/amount')?> </th>-->
			<th class="wds-24" style='text-align:right;' valign="top"><?=t($t_base.'fields/transfer')?> </th>
                    </tr>	 
		<?php
                   // print_r($billingDetails);
			$confirm_button_flag=false;
			if($billingDetails!=array())
                        {	
                            $ctr=1;
                            $grandtotal=0;
                            $totalReferralFee=0;
                            $forwardertotal=0;
                            $forwardertotal1=0;
                            $i=1;
                            $j=0;
                            $szHandlingCurrencyStr = "";
                            $handlingFeeVat=0;
                            foreach($billingDetails as $bills)
                            {	
                                //print_r($bills);
                                //echo "<br/><br/>";
                                $ftoatl=0;
                                $currencyValue='';	
                                if($bills['szForwarderCurrency']!='')
                                    $currencyValue = $bills['szForwarderCurrency'];
                                else
                                    $currencyValue = $bills['szCurrency'];

                                if($bills['iDebitCredit']==6 || $bills['iDebitCredit']==7)
                                { 
                                    $bookingLabelFee=0;
                                    $fHandlingFeeForwarderCurrency = 0;
                                    $fBookingLabelFeeRateROE=$bills['fForwarderExchangeRate'];
                                    $fBookingLabelFeeCurrencyId=$bills['idForwarderCurrency'];
                                             
                                    if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                    {
                                        $bookingLabelFee = 0;
                                    }
                                    else if($bills['iCourierBooking']==1 && $bills['iCourierAgreementIncluded']==0)
                                    { 
                                        $kCourierServices = new cCourierServices(); 
                                        $fBookingLabelFeeRate = $bills['fLabelFeeUSD']; 
                                        if($fBookingLabelFeeCurrencyId==1)
                                        {
                                            $bookingLabelFee = $fBookingLabelFeeRate;
                                        }
                                        else
                                        {                                                                
                                           $bookingLabelFee = $fBookingLabelFeeRate/$fBookingLabelFeeRateROE; 
                                        }
                                    }
                                    
                                    if($bills['iHandlingFeeApplicable']==1)
                                    { 
                                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                        {
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                                            $fHandlingFeeForwarderCurrency = 0;
                                            $fHandlingFeeForwarderCurrencyVatValue=0;
                                        }
                                        else
                                        { 
                                            if($bills['idForwarderCurrency']==1)
                                            {
                                                $fHandlingFeeForwarderCurrency = $bills['fTotalHandlingFeeUSD'];
                                            }
                                            else
                                            {
                                                $fHandlingFeeForwarderCurrency = ($bills['fTotalHandlingFeeUSD']/$fBookingLabelFeeRateROE);
                                            }

                                            $fHandlingFeeForwarderCurrencyVatValue=0;
                                            if((float)$bills['fVATPercentage']>0)
                                            {
                                                $fHandlingFeeForwarderCurrencyVatValue=round((float)($fHandlingFeeForwarderCurrency*.01*$bills['fVATPercentage']),2);
                                            } 
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalForwarderPriceWithoutHandlingFee'];
                                        }
                                    }
                                    else
                                    {
                                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                        {
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                                        }
                                        else
                                        {
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalPriceForwarderCurrency'];
                                        }
                                    }
                                           
                                    if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                    {
                                        //For Financial Version 2 booking we don't substract Referal amount from Self invoice amount as this is already substracted while calculation of services for booking.
                                        $bills['fReferalAmount'] = 0;
                                    } 
                                    $ftoatl = round($fTotalPriceForwarderCurrencyWithoutHandlingFee,2) - (round($bills['fReferalAmount'],2)+ $bookingLabelFee);
                                    
                                    $ftoatNewl = round($bills['fTotalPriceForwarderCurrency'],2) - (round($bills['fReferalAmount'],2) + $bookingLabelFee);
                                    $forwardertotal1=$forwardertotal1-($ftoatNewl)+$fHandlingFeeForwarderCurrency;
                                    $ftoatNewTransfer=$ftoatNewl-$fHandlingFeeForwarderCurrency;
                                    $grandtotal = $grandtotal - (round($bills['fTotalPriceForwarderCurrency'],2));

                                    $totalReferralFee = $totalReferralFee - round($bills['fReferalAmount'],2);
                                    $szBookingRef_text = "Cancelled: ".$bills['szBookingRef'];
                                    $bookingLabelFeeTotal = $bookingLabelFeeTotal - round($bookingLabelFee,2);
                                    $fTotalHandlingFeeForwarderCurrency = $fTotalHandlingFeeForwarderCurrency - round($fHandlingFeeForwarderCurrency,2);

                                    $szTotalAmountText = "(".$currencyValue." ".number_format((float)$ftoatl,2).")";

                                    $fTotalPriceForwarderCurrency_text = "(".$currencyValue." ".number_format((float)$fTotalPriceForwarderCurrencyWithoutHandlingFee,2).")";
                                    $fReferalAmount_text = "(".$currencyValue." ".number_format((float)$bills['fReferalAmount'],2).")";
                                    $fReferalAmount = round($bills['fReferalAmount'],2);
                                    
                                    $szHandlingCurrencyStr.= $szBookingRef_text." USD: ".$bills['fTotalHandlingFeeUSD']." XE: ".$bills['fForwarderExchangeRate']." FWD Curr: ".$fHandlingFeeForwarderCurrency." Total: ".$fTotalHandlingFeeForwarderCurrency."\\n";
                                }
                                else
                                { 
                                    $bookingLabelFee=0;
                                    $fHandlingFeeForwarderCurrency = 0;
                                    $fBookingLabelFeeRateROE=$bills['fForwarderExchangeRate'];
                                    $fBookingLabelFeeCurrencyId=$bills['idForwarderCurrency'];
                                     
                                    if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                    {
                                        $bookingLabelFee = 0;
                                        //For Financial Version 2 booking we don't substract Referal amount from Self invoice amount as this is already substracted while calculation of services for booking.
                                        $bills['fReferalAmount'] = 0;
                                    }
                                    else if($bills['iCourierBooking']==1 && $bills['iCourierAgreementIncluded']==0)
                                    {
                                        $kCourierServices = new cCourierServices();
                                        //$courierBookingArr=$kCourierServices->getCourierBookingData($bills['idBooking']);
                                        $fBookingLabelFeeRate=$bills['fLabelFeeUSD']; 
                                        if($fBookingLabelFeeCurrencyId==1)
                                        {
                                            $bookingLabelFee=$fBookingLabelFeeRate;
                                        }
                                        else
                                        {                                                                
                                           $bookingLabelFee=$fBookingLabelFeeRate/$fBookingLabelFeeRateROE;

                                        }
                                        //echo $bookingLabelFee."<br/>";
                                    } 
                                    
                                    
                                    if($bills['iHandlingFeeApplicable']==1)
                                    {  
                                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                        {
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                                            $fHandlingFeeForwarderCurrency = 0;
                                            $fHandlingFeeForwarderCurrencyVatValue=0;
                                        }
                                        else
                                        {
                                            
                                            if($bills['idForwarderCurrency']==1)
                                            {
                                                $fHandlingFeeForwarderCurrency = $bills['fTotalHandlingFeeUSD'];
                                            }
                                            else
                                            {
                                                $fHandlingFeeForwarderCurrency = round((float)($bills['fTotalHandlingFeeUSD']/$fBookingLabelFeeRateROE),2);
                                            }
                                            $fHandlingFeeForwarderCurrencyVatValue=0;
                                            if((float)$bills['fVATPercentage']>0)
                                            {
                                                $fHandlingFeeForwarderCurrencyVatValue=round((float)($fHandlingFeeForwarderCurrency*.01*$bills['fVATPercentage']),2);
                                            } 
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee=$bills['fTotalForwarderPriceWithoutHandlingFee'];
                                        }  
                                    }
                                    else
                                    {
                                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                        {
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                                        }
                                        else
                                        {
                                            $fTotalPriceForwarderCurrencyWithoutHandlingFee=$bills['fTotalPriceForwarderCurrency'];
                                        } 
                                    }
                                    $ftoatl = round($fTotalPriceForwarderCurrencyWithoutHandlingFee,2) - (round($bills['fReferalAmount'],2)+ $bookingLabelFee);
                                    
                                    $ftoatNewl = round($bills['fTotalPriceForwarderCurrency'],2) - (round($bills['fReferalAmount'],2)+ $bookingLabelFee);
                                    $forwardertotal1=$forwardertotal1+$ftoatl;
                                    $ftoatNewTransfer=$ftoatl;
                                            
                                            
                                    $grandtotal = $grandtotal+round($bills['fTotalPriceForwarderCurrency'],2);
                                    
                                    $totalReferralFee = $totalReferralFee+round($bills['fReferalAmount'],2);
                                    $bookingLabelFeeTotal=$bookingLabelFeeTotal+$bookingLabelFee;
                                    $fTotalHandlingFeeForwarderCurrency = $fTotalHandlingFeeForwarderCurrency + round($fHandlingFeeForwarderCurrency,2);

                                    $szBookingRef_text = $bills['szBookingRef'];
                                    $fTotalPriceForwarderCurrency_text = $currencyValue." ".number_format((float)$fTotalPriceForwarderCurrencyWithoutHandlingFee,2);
                                    $fReferalAmount_text = $currencyValue." ".number_format((float)$bills['fReferalAmount'],2);
                                    $szTotalAmountText = $currencyValue." ".number_format((float)$ftoatl,2);

                                    $fReferalAmount = round($bills['fReferalAmount'],2);
                                    $szHandlingCurrencyStr .= $szBookingRef_text." USD: ".$bills['fTotalHandlingFeeUSD']." XE: ".$bills['fForwarderExchangeRate']." FWD Curr: ".$fHandlingFeeForwarderCurrency." Total: ".$fTotalHandlingFeeForwarderCurrency."\\n";
                                } 
                                $handlingFeeVat=$handlingFeeVat+$fHandlingFeeForwarderCurrencyVatValue;
                                
                                if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                                {
                                    $szSelfInvoice = $bills['szSelfInvoice'];
                                }
                                else
                                {
                                    $szSelfInvoice = $bills['szCustomerInvoice'];
                                }
                                ?> 
                                    <tr align='center'>
                                        <td style='text-align:left;'><input type ="checkbox" value="<?=$bills['id']?>" checked="checked" name="transfer_id[]" id="transfer_id_<?=$i?>" onclick="select_unselect_forwarder_payment('<?=$ftoatNewTransfer?>','<?=$fReferalAmount?>','<?=$i?>','<?=$bills['iDebitCredit']?>','<?=round($bookingLabelFee,2)?>','<?php echo round($fHandlingFeeForwarderCurrency,2); ?>','<?php echo $fHandlingFeeForwarderCurrencyVatValue;?>');"></td>
                                        <td style='text-align:left;'><?=date('d-M',strtotime($bills['dtCreditOn']))?></td>
                                        <td style='text-align:left;'><?=$szSelfInvoice?></td>
                                        <td style='text-align:left;'><?=$szBookingRef_text?></td>
                                        <!--<td style='text-align:right;'><?=$fTotalPriceForwarderCurrency_text?></td>-->
                                        <!--<td style='text-align:right;'><?=$bills['fReferalPercentage']?> %</td>-->
                                        <!--<td style='text-align:right;'><?=$fReferalAmount_text?></td>-->
                                        <td style='text-align:right;'><?=$szTotalAmountText?></td>
                                    </tr>
                                <?php
                                ++$i;	
                                ++$j;	
                            }
                            
                            $uploadServiceAmount='0.00';
                            $uploadServiceAmount=round($serviceUploadPaymentArr[$check][$currencyValue],2);
                            //echo $uploadServiceAmount."hello";
                            if((float)$uploadServiceAmount>'0.00')
                            {
                                $grandtotal=$grandtotal-$uploadServiceAmount;
                                $forwardertotal =$forwardertotal1-$uploadServiceAmount;
                                $forwardertotal1 =$forwardertotal1;
                                //echo $forwardertotal."forwarder";
                            }
                            else
                            {
                                $grandtotal=$grandtotal;
                                $forwardertotal=$forwardertotal1;
                                $forwardertotal1 =$forwardertotal1;
                                $uploadServiceAmount='0.00';
                                //echo $forwardertotal."forwarder1";
                            }
                                                
                            if((float)$bookingLabelFeeTotal>0.00)
                            {
                                $text_courierLabelFee=$currencyValue." ".number_format((float)$bookingLabelFeeTotal,2);
                            }else
                            {
                                $bookingLabelFeeTotal1=str_replace("-","",$bookingLabelFeeTotal);
                                $text_courierLabelFee="(".$currencyValue." ".number_format((float)$bookingLabelFeeTotal1,2).")";
                            }
                            
                            if((float)$fTotalHandlingFeeForwarderCurrency>0.00)
                            {
                                $text_fHandlingFeeForwarderCurrency = $currencyValue." ".number_format((float)$fTotalHandlingFeeForwarderCurrency,2);
                            }
                            else
                            {
                                $fTotalHandlingFeeForwarderCurrency = str_replace("-","",$fTotalHandlingFeeForwarderCurrency);
                                $text_fHandlingFeeForwarderCurrency = "(".$currencyValue." ".number_format((float)$fTotalHandlingFeeForwarderCurrency,2).")";
                            } 
                            if((float)$totalReferralFee>='0.00')
                            {
                                    $text_referralfee=$currencyValue." ".number_format((float)$totalReferralFee,2);
                            }
                            else
                            {
                                    $totalReferralFee1=str_replace("-","",$totalReferralFee);
                                    $text_referralfee="(".$currencyValue." ".number_format((float)$totalReferralFee1,2).")";
                            }

                            if((float)$forwardertotal1>='0.00')
                            {
                                $text_forwardertotal1=$currencyValue." ".number_format((float)$forwardertotal1,2);
                            }
                            else
                            {
                                $forwardertotal_1=str_replace("-","",$forwardertotal1);
                                $text_forwardertotal1="(".$currencyValue." ".number_format((float)$forwardertotal_1,2).")";
                            }

                            if((float)$forwardertotal>='0.00')
                            {
                                $confirm_button_flag=true;
                                $text_forwardertotal=$currencyValue." ".number_format((float)$forwardertotal,2);
                            }
                            else
                            {
                                $confirm_button_flag=false;
                                $forwardertotal_2=str_replace("-","",$forwardertotal);
                                $text_forwardertotal="(".$currencyValue." ".number_format((float)$forwardertotal_2,2).")";
                            }
				?>
                                <tr>
                                    <td colspan="7" style="border-bottom: 0px solid #9CA0BD"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i><?=t($t_base.'messages/current_account_balance');?></i></td>
                                    <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><?=$currencyValue?> <?=number_format((float)$grandtotal,2)?></i></td>
                                </tr>
                                <tr <?php echo $szStyle; ?>>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i><?=t($t_base.'messages/referral_fee_invoice');?></i></td>
                                    <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="referral_fee"> <?=$text_referralfee?></span></i></td>
                                </tr>
                                <tr <?php echo $szStyle; ?>>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i><?=t($t_base.'messages/courier_booking_and_labels_invoice');?></i></td>
                                    <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="courier_label_fee"> <?=$text_courierLabelFee?></span></i></td>
                                </tr>
                                <tr <?php echo $szStyle; ?>>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i>Transporteca handling fee invoice, w/o VAT</i></td>
                                    <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="booking_handling_fee"> <?php echo $text_fHandlingFeeForwarderCurrency?></span></i></td>
                                </tr> 
                                <tr>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i> - <?=t($t_base.'messages/transfer_to_forwarder_booking');?></i></td>
                                    <td colspan="2" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="forwarder_total1"><?=$text_forwardertotal1?></span></i></td>
                                </tr>	
                                <tr>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i> - <?=t($t_base.'messages/account_debits_since_last_transfer');?></i></td>
                                    <td colspan="2" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i>(<?=$currencyValue?> <span id="forwarder_upload_total"><?=number_format((float)$uploadServiceAmount,2)?></span>)</i></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i><?=t($t_base.'messages/transfer_to_forwarder');?></i></td>
                                    <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="forwarder_total"> <?=$text_forwardertotal?></span></i></td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i><?=t($t_base.'messages/new_account_balance');?></i></td>
                                    <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="account_balance_cancel"><?=$currencyValue?> <span id="account_balance">0.00</span></i></span></td>
                                </tr>
                            </table>
                            </div>	
                            <table cellpadding="5"  cellspacing="0" border="0" width="100%" id="booking_table">
                                <tr align='center' >
                                    <td align="right" width="50%" > 
                                            <a href="javascript:void(0)" class="button2" id="cancel" onclick="cancelEditMAnageVar(0);"><span><?=t($t_base.'fields/cancel');?></span></a>
                                    </td>
                                    <td align="left">	
                                        <input type="hidden" name="check" value="<?=$check?>">	
                                        <input type="hidden" name="idForwarderCurrency" value="<?=$idForwarderCurrency?>">	
                                        <input type="hidden" name="mode" value="PAY_FORWARDER_CONFORMATION">
                                        <input type="hidden" name="totalcheckbox" id="totalcheckbox" value="<?=$j?>">
                                        <input type="hidden" name="arrForwarderPayment[grandtotal]" id="grandtotal" value="<?=round($grandtotal,2)?>">
                                        <input type="hidden" name="arrForwarderPayment[farwordertotal1]" id="farwordertotal1" value="<?=round($forwardertotal1,2)?>">  
                                        <input type="hidden" name="arrForwarderPayment[farwordertotal]" id="farwordertotal" value="<?=round($forwardertotal,2)?>">	
                                        <input type="hidden" name="arrForwarderPayment[totalReferralFee]" id="totalReferralFee" value="<?=round($totalReferralFee,2)?>">
                                        <input type="hidden" name="arrForwarderPayment[totalcourierLabelFee]" id="totalcourierLabelFee" value="<?=round($bookingLabelFeeTotal,2)?>">
                                        <input type="hidden" name="arrForwarderPayment[totalBookingHandlingFee]" id="totalBookingHandlingFee" value="<?=round($fTotalHandlingFeeForwarderCurrency,2)?>"> 
                                        <input type="hidden" name="arrForwarderPayment[uploadServiceAmount]" id="uploadServiceAmount" value="<?=round($uploadServiceAmount,2)?>">
                                        <input type="hidden" name="arrForwarderPayment[szCurrencyName]" id="szCurrencyName" value="<?=$currencyValue?>">
                                        <input type="hidden" name="arrForwarderPayment[fForwarderExchangeRate]" id="fForwarderExchangeRate" value="<?=$bills['fForwarderExchangeRate']?>">
                                        <input type="hidden" name="arrForwarderPayment[handlingFeeVat]" id="handlingFeeVat" value="<?=$handlingFeeVat?>">
                                        <a href="javascript:void(0)" class="button1" <?php if($confirm_button_flag ) {?> onclick="confirm_forwarder_billing_amount();" <? }else{?> onclick="" style="opacity:0.4" <? }?> id="confirm_bill_payment"><span style="min-width:100px !important"><?=t($t_base.'fields/confirm');?></span></a>
                                    </td>
                                </tr>
                            </table>
                        </form>
			<script type="text/javascript"> 
                            console.log('<?php echo $szHandlingCurrencyStr; ?>');
			<?php
				if($forwardertotal<=0)
				{
					?>
					$('#confirm_bill_payment').removeAttr('onclick');
					$('#confirm_bill_payment').css('opacity','0.4');
					<?
				}
			?>
			</script>
						</p>
						</div>
					</div>
				<?php
			}
		}
		if($mode=="CHANGE_STATUS")
		{
			$bookingId=base64_decode($id);
			$comment = sanitize_all_html_input($_POST['comment']);
			$date    = substr(sanitize_all_html_input($_POST['date']),0,10);
			if($date==date('d/m/Y'))
			{
				$date=date("Y-m-d H:i:s");
			}
			else
			{
			$date=explode('/',$date);
			$date=implode('-',$date);
			$date = date("Y-m-d 12:00:00", strtotime($date));
			}
			if($bookingId>0)
			{
				$status=$kAdmin->changePaymentRecievedStatus($bookingId,$check,$date,$comment);
				if($status==1)
				{	
					$billingDetails=$kAdmin->showBookingConfirmedTransaction();
					showCustomerAdminBilling($billingDetails,$check);
				}
			}
			
		}
	}	
}	