<?php 
/**
  *  admin--- maintanance -- text editor
  */
define('PAGE_PERMISSION','__SYSTEM_API__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Logs - API Logs ";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/crownJob/";
validateManagement();

$apiLogsAry = array();
$apiLogsAry = $kAdmin->getAllApiLogs();

?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
    <?php require_once( __APP_PATH_LAYOUT__ ."/contentMaintainanceLeftNav.php" ); ?> 
    <div class="hsbody-2-right">
        <table cellpadding="0"  cellspacing="0" class="format-4" width="100%">
            <tr>
                <th width="20%" valign="top"><?=t($t_base.'fields/api_url')?></th>
                <th width="10%" valign="top"><?=t($t_base.'fields/last_status')?></th>
                <th width="15%" valign="top"><?=t($t_base.'fields/last_run');?></th> 
                <th width="10%" valign="top"><?=t($t_base.'fields/priority');?></th>
                <th width="40%" valign="top"><?=t($t_base.'fields/notes');?></th>
            </tr>
            <?php 
                if(!empty($apiLogsAry))
                { 
                    foreach($apiLogsAry as $apiLogsArys)
                    {
                        if(!empty($apiLogsArys['dtLastRun']) && $apiLogsArys['dtLastRun']!='0000-00-00 00:00:00')
                        { 
                            $dtLastRunOn = date('Y-m-d H:i',strtotime("+2 HOUR",strtotime($apiLogsArys['dtLastRun'])));  
                        } 
                        else
                        {
                            $dtLastRunOn = "N/A";
                        }
                        ?>
                        <tr>
                            <td valign="top"><?php echo $apiLogsArys['szBaseUrl']; ?></td>
                            <td valign="top"><?php echo $apiLogsArys['szLastStatus']; ?></td>
                            <td valign="top"><?php echo $dtLastRunOn; ?></td> 
                            <td valign="top"><?php echo $apiLogsArys['szPriority']; ?></td>
                            <td valign="top"><?php echo $apiLogsArys['szDeveloperNotes']; ?></td>
                        </tr>
                        <?php
                    }
                } 
            ?>  
        </table>
    </div>
</div>

<?php
    include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>