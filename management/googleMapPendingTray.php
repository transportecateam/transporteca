<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
define("__LANGUAGE__",'english');
$lang_code=__LANGUAGE__;
I18n::add_language(strtolower($lang_code),true);
//require_once(__APP_PATH_LAYOUT__."/forwarder_header.php");
$t_base = "BulkUpload/";
constantApiKey();
//checkAuthForwarder();
$kConfig = new cConfig();
$request_searched_ary=array();
$request_searched_ary = $_REQUEST['cfsHiddenAry'] ;
$idForwarder =$_SESSION['forwarder_id'];
if($request_searched_ary['idForwarder']!=$idForwarder)
{
	//header("Location:".__FORWARDER_HOME_PAGE_URL__);
	//exit;
}
$szCountryName = $kConfig->getCountryName(sanitize_all_html_input(trim($request_searched_ary['szCountry'])));
$szLatitute = sanitize_all_html_input(trim($request_searched_ary['szLatitude']));
$szLongitude = sanitize_all_html_input(trim($request_searched_ary['szLongitude']));
if((float)$szLatitute == 0)
{
	$szLatitute = 0;
}
if((float)$szLongitude == 0)
{
	$szLongitude = 0;
} 

$address_line = sanitize_all_html_input(trim($request_searched_ary['szAddressLine'])); 

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/jquery-latest.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=__BASE_STORE_FORWARDER_CSS_URL__?>/style.css" />
    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/functions.js"></script>
    <script type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/forwarder.js"></script>
    <script src="<?php echo __GOOGLE_MAP_JSON_URL__;?>" type="text/javascript"></script>
    <!--   <SCRIPT type="text/javascript" src="<?=__BASE_STORE_JS_URL__?>/mapctrl.js"></SCRIPT>-->
     
<script type="text/javascript">

// Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html

var map = null;
var geocoder = null;
var latsgn = 1;
var lgsgn = 1;
var zm = 0; 
var marker = null;
var posset = 0;
var markersArray = [];

function xz() 
{ 
    var origin_lat = '<?=$szLatitute?>';
    var origin_long = '<?=$szLongitude?>';
    var myLatLng = new google.maps.LatLng(origin_lat, origin_long);
    var myOptions = {
	zoom: 2,
	zoomControl: true,
	center: myLatLng,
	scaleControl: true,
        overviewMapControl: true,
        scrollwheel: true,
        disableDoubleClickZoom: true,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	zoomControlOptions: {
              style: google.maps.ZoomControlStyle.LARGE,
              position: google.maps.ControlPosition.LEFT_CENTER
          }
    };
    map = new google.maps.Map(document.getElementById("map"),myOptions); 
    geocoder = new google.maps.Geocoder(); 
    
    if(geocoder)
    {
        var address = '<?php echo $address_line; ?>';
        geocoder.geocode({ 'address': address }, function (results, status) 
        {
            if (status == google.maps.GeocoderStatus.OK) 
            {     
                clearOverlays();
                var point = results[0].geometry.location;
                fc( point) ; 
            }
            else
            {
                alert(address + " not found");
            }
        }); 
    } 
} 
function fc( point )
{ 
    var html = "";
    html = '<?php echo $address_line; ?>'; 
     
    var baseIcon = new google.maps.MarkerImage();
    baseIcon.iconSize=new google.maps.Size(32,32);
    baseIcon.shadowSize=new google.maps.Size(56,32);
    baseIcon.iconAnchor=new google.maps.Point(16,32);
    baseIcon.infoWindowAnchor=new google.maps.Point(16,0);
    var thisicon = new google.maps.MarkerImage(baseIcon, '<?=__BASE_STORE_IMAGE_URL__.'/red-marker.png'?>', null, "http://itouchmap.com/i/msmarker.shadow.png");
 
    var properties = {
        position: point,
        map: map,
        draggable: true
    };
	
    var marker = new google.maps.Marker(properties);
    markersArray.push(marker);
    map.setCenter(point);
	
    map.setZoom(8); 
    google.maps.event.addListener(marker, "click", function() 
    { 
        var oinfowindow = new google.maps.InfoWindow();
        oinfowindow.setContent(html);
        oinfowindow.open(map,marker);
    });  
} 
function createMarker(point, html) 
{
    var marker = new GMarker(point);
    google.maps.event.addListener(marker, "click", function()
    {
        marker.openInfoWindowHtml(html);
    });
    return marker;
}   
function clearOverlays() 
{ 
} 
 
</script>

<style type="text/css">
    .button-map{width:auto;border-radius:0;background:#e74118;font-size:14px;min-width: 94px;text-align:center;display:inline-block;cursor: pointer;text-transform:uppercase;padding:7px;font-weight: bold;color:#fff;font-weight:bold;font-style:normal;text-decoration:none;}
    .button-map:hover{color:#000;}
</style>

  </head>

  <body onload="xz()">
    <div>
        <form action="javascript:void();" methos="post" >
          <p align="left"><strong><?=$address_line;?></strong></p> 
          <div id="map" align="center" style="width: 100%; height: 300px"></div>
        </form>   
        <p align="center"> 
            <br>
            <a href="javascript:void(0);" onclick="window.top.window.hide_google_map()" class="button-map"><span>close</span></a>  	  
	</p> 
    </div>
  </body>
</html>

