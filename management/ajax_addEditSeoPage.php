<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement_ajax();
$t_base="management/LandingPage/";
$t_base_error="management/Error/";

$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$kSEO = new cSEO();

if($mode == 'CHANGE_ORDER_SEO_PAGE')
{
	$id = (int)sanitize_all_html_input($_POST['id']);
	$order = sanitize_all_html_input($_POST['move']);
	if($order == 'UP')
	{
		$iOrderBy = __ORDER_MOVE_DOWN__;
	}
	else if($order == 'DOWN')
	{
		$iOrderBy = __ORDER_MOVE_UP__;
	}
	
	$kSEO->moveUpDownSeoPagesLinks($id,$iOrderBy);
	
	$seoPageDataAry = array();
	$seoPageDataAry = $kSEO->getAllSeoPageData();
	$iMaxOrder = $kSEO->getMaxOrderSeoPageData();
	echo "SUCCESS||||";
	echo viewSEOPageContent($seoPageDataAry,$iMaxOrder);
	die;
}
else if($mode=='DELETE_SEO_PAGE')
{
	$idSeoPage = (int)sanitize_all_html_input($_POST['id']);
	echo deleteConfirmation_SEO($idSeoPage); 
	die;
}
else if($mode=='DELETE_SEO_PAGE_CONFIRM')
{
	$id = (int)$_POST['id'];
	$kSEO->deleteSeoPage($id);
	
	$seoPageDataAry = array();
	$seoPageDataAry = $kSEO->getAllSeoPageData();
	$iMaxOrder = $kSEO->getMaxOrderSeoPageData();
	
	echo "SUCCESS||||";
	echo viewSEOPageContent($seoPageDataAry,$iMaxOrder);
	die;
}
else if(!empty($_POST['landingPageArr']) && $mode=='ADD')
{
	if($kSEO->addSeoPageData($_POST['landingPageArr']))
	{
		echo "SUCCESS|||| add" ;
		die;
	}	
	else if(!empty($kSEO->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kSEO->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?php
	}
}
else if(!empty($_POST['landingPageArr']) && $mode=='EDIT')
{
	if($kSEO->editSeoPage($_POST['landingPageArr']))
	{
		echo "SUCCESS||||" ;
		die;
	}	
	else if(!empty($kSEO->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kSEO->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?
	 }
}
else if(!empty($_POST['landingPageArr']) && $mode=='PREVIEW')
{
	if($kSEO->validateSeoPageData($_POST['landingPageArr']))
	{
		echo "SUCCESS||||" ;
		echo "<br /><div>".$_POST['landingPageArr']['szContent']."</div>";
		die;
	}
	else if(!empty($kSEO->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
			<ul>
				<?php
				      foreach($kSEO->arErrorMessages as $key=>$values)
				      {
				      	?>
				      		<li><?=$values?></li>
				      	<?php 
				      }
				?>
			</ul>
		</div>
		<?
	 }
}

?>
