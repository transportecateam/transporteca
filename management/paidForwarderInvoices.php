<?php 
/**
  *pay forwarder
  */
define('PAGE_PERMISSION','__FORWARDER_INVOICES__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Accounts Payable - Pay Forwarder";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
$t_base="management/forwarderpayment/";
validateManagement();
$kBooking = new cBooking();

$unpaidMarkupBookingsAry = array();
$unpaidMarkupBookingsAry = $kAdmin->showForwarderPendingPaymentDetails(true); 
 
?>
<div id="ajaxLogin"></div>
<div id="hsbody-2">
    <?php require_once(__APP_PATH__ ."/layout/admin_financial_left_nav.php"); ?>

    <div id="checkDetails" class="hsbody-2-right">
        <?php echo displayPayForwarderInvoices($unpaidMarkupBookingsAry,true); ?>
    </div>
</div>
<input type="hidden" id="szBookingStatusFlag" name="szBookingStatusFlag" value="paidForwarderInvoice">
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>
