<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
define('PAGE_PERMISSION','__Articles__');
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle = "Transporteca | Setup SEO Pages";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement_ajax();
$kConfig = new cConfig();

$idLandingPage = sanitize_all_html_input(trim($_REQUEST['id']));
$kSEO = new cSEO();
$landingPageDataAry = array();

if($idLandingPage>0)
{
    $landingPageDataAry = $kSEO->getAllSeoPageData($idLandingPage);
    $form_id='addLandingPage';
    $mode = 'EDIT';
    $data = $landingPageDataAry[0];
    $data['fCargoWeight'] = (int)$data['fCargoWeight'];
}
else
{
    $mode = 'ADD';
    $form_id='addLandingPage';
}

//$site_url = remove_http_from_url(__MAIN_SITE_HOME_PAGE_SECURE_URL__);
$site_url = "www.transporteca.com";

 $kConfig =new cConfig();
 $langArr=$kConfig->getLanguageDetails();
?>
	<div id="hsbody-2">
	<?php 
		require_once( __APP_PATH_LAYOUT__ ."/explainLeftNav.php" );
		$t_base="management/SeoPage/";
	?> 
	<div class="hsbody-2-right">
		<h4><b><?=t($t_base.'title/seo_page_setup');?></b></h4>
		<div id ="Error-log" class="errorBox" style="display:none;"></div>
		<script type="text/javascript">
			$(function(){
				checkDescriptionLength('szMetaDescription');
			});
			setup();
		</script>
		<form name="addLandingPage" id="<?=$form_id?>" method="post" >
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/link_title_in_sitemap');?></span>
				<span class="field-container"><input type="text" name="landingPageArr[szPageHeading]" value="<? if(isset($data['szPageHeading'])){echo $data['szPageHeading']; }?>"/></span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/url');?> (<?=$site_url?>/information/..)</span>
				<span class="field-container"><input type="text" name="landingPageArr[szLink]" value="<? if(isset($data['szUrl'])){echo $data['szUrl']; }?>"/></span>
			</p>
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/page_title');?></span>
				<span class="field-container"><input type="text" name="landingPageArr[szMetaTitle]" value="<? if(isset($data['szMetaTitle'])){echo $data['szMetaTitle']; }?>"/></span>
			</p>
			<p class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/link_title');?></span>
                            <span class="field-container"><input type="text" name="landingPageArr[szLinkTitle]" value="<? if(isset($data['szLinkTitle'])){echo $data['szLinkTitle']; }?>"/></span>
			</p>
			<!-- 
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_keyword');?></span>
				<span class="field-container"><textarea rows="3" cols="25" name="landingPageArr[szMetaKeyword]"><? if(isset($data['szMetaKeywords'])){echo $data['szMetaKeywords']; }?></textarea></span>
			</p>
			 -->
			<p class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_description');?></span>
				
				<span class="field-container"><textarea rows="3" cols="25" onkeyup="checkDescriptionLength(this.id);" name="landingPageArr[szMetaDescription]" id="szMetaDescription"><? if(isset($data['szMetaDescription'])){echo $data['szMetaDescription']; }?></textarea></span>
				<span style="color: #7F7F7F;float: left;font-size: 12px;font-style: italic;margin: 63px 0 0 37px;text-align: right;width: 30px;">(<span id="char_left">160</span>)</span>
			</p>
			<p class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/active_status');?></span>
                            <span class="field-container">
                                <select name="landingPageArr[iPublished]">
                                    <option value = '0' <? if(isset($data['iPublished']) && $data['iPublished'] ==0){echo "selected='selected'"; } ?>>No</option>
                                    <option value = '1' <? if(isset($data['iPublished']) && $data['iPublished'] ==1){echo "selected='selected'"; } ?>>Yes</option>							
                                </select>
                            </span>
			</p>
			<p class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/language');?></span>
                            <span class="field-container">
                                <select name="landingPageArr[iLanguage]" id="iLanguage">
                                    <?php
                                    if(!empty($langArr))
                                    {
                                        foreach($langArr as $langArrs)
                                        {?>
                                           <option value = '<?php echo $langArrs['id']?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==$langArrs['id']){echo "selected='selected'"; } ?>><?php echo ucfirst($langArrs['szName']);?></option>
                                        <?php
                                        }
                                        
                                    }?>
<!--                                    <option value = '<?php echo __LANGUAGE_ID_ENGLISH__?>' <? if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_ENGLISH__){echo "selected='selected'"; } ?>><?php echo ucfirst(__LANGUAGE_TEXT_ENGLISH__);?></option>
                                    <option value = '<?php echo __LANGUAGE_ID_DANISH__?>' <? if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_DANISH__){echo "selected='selected'"; } ?>><?php echo ucfirst(__LANGUAGE_TEXT_DANISH__);?></option>	-->
                                </select>
                            </span>
			</p>
                        <p class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/available_in_left_menu');?></span>
                            <span class="field-container">
                                <select name="landingPageArr[iAvailableInLeftMenu]" id="iAvailableInLeftMenu">
                                    <option value = '1' <?php if($data['iAvailableInLeftMenu']==1){echo "selected='selected'"; } ?>>Yes</option>
                                    <option value = '2' <?php if($data['iAvailableInLeftMenu']==2){echo "selected='selected'"; } ?>>No</option>	
                                </select>
                            </span>
			</p>
                        <p class="profile-fields">
                            <span class="field-name"><?php echo t($t_base.'fields/from')." ".t($t_base.'fields/defualt_selection')." (".t($t_base.'fields/defualt_selection').")";?></span>
                            <span class="field-container">
                                <input type="text" style="width:60%;" name="landingPageArr[szFromSelection]" id="szFromSelection" <?php if($data['iFromIPLocation']==1){ echo "disabled"; }?> value="<?php if(isset($data['szFromSelection'])){echo $data['szFromSelection']; }?>"/> 
                                <input type="checkbox" name="landingPageArr[iFromIPLocation]" id="iFromIPLocation" onclick="toggle_text_field(this.id,'szFromSelection')" value="1" <?php if($data['iFromIPLocation']==1){ echo "checked"; }?>/> <?php echo t($t_base.'fields/ip_location'); ?>
                            </span>
			</p>
                        <p class="profile-fields">
                            <span class="field-name"><?php echo t($t_base.'fields/to')." ".t($t_base.'fields/defualt_selection')." (".t($t_base.'fields/defualt_selection').")";?></span>
                            <span class="field-container">
                                <input type="text" name="landingPageArr[szToSelection]" style="width:60%;" id="szToSelection" <?php if($data['iToIPLocation']==1){ echo "disabled"; }?> value="<?php if(isset($data['szToSelection'])){echo $data['szToSelection']; }?>"/>
                                <input type="checkbox" name="landingPageArr[iToIPLocation]" id="iToIPLocation" onclick="toggle_text_field(this.id,'szToSelection')" value="1" <?php if($data['iToIPLocation']==1){ echo "checked"; }?>/> <?php echo t($t_base.'fields/ip_location'); ?>
                            </span>
			</p> 
                        <p class="profile-fields">
                            <span class="field-name"><?php echo t($t_base.'fields/volume')." / ".t($t_base.'fields/weight')." ".t($t_base.'fields/defualt_selection')." (".t($t_base.'fields/will_not_overtake_cookie').")";?></span>
                            <span class="field-container">
                                <input type="text" name="landingPageArr[fCargoVolume]" style="width:35%;" id="fCargoVolume" value="<?php if(isset($data['fCargoVolume'])){echo $data['fCargoVolume']; }?>"/> <?php echo t($t_base.'fields/cbm'); ?>
                                <input type="text" name="landingPageArr[fCargoWeight]" style="width:35%;" id="fCargoWeight" value="<?php if(isset($data['fCargoWeight'])){echo $data['fCargoWeight']; }?>"/> <?php echo t($t_base.'fields/kg'); ?>
                            </span>
			</p>
			<br />
			<div id="content_div" style="width:100%;height:60%;">
                            <textarea name="landingPageArr[szTextX]" id="content" class="myTextEditor" ><?php echo $data['szContent'];?></textarea>
			</div>
			<br />
			<div style="text-align: center;clear: both;">
				<input type="hidden" name="mode" id="mode" value="<?=$mode?>">
				<input type="hidden" name="landingPageArr[idSeoPage]" id="idSeoPage" value="<?=$idLandingPage?>">
				<a class="button1" id="cancelAddingPage" onclick="previewSeoPage('<?=$form_id?>')"><span><?=t($t_base.'fields/preview');?></span></a>
				<a class="button1" id="addNewPage" onclick="addNewSeoPage('<?=$form_id?>');"><span><? if(isset($data['id'])){echo t($t_base.'fields/save');}else{echo t($t_base.'fields/add_1');}?></span></a>
				<a class="button1" id="cancelAddingPage" onclick="redirect_url('<?=__MANAGEMENT_SEO_PAGE_EDITOR_URL__?>')"><span><?=t($t_base.'fields/cancel');?></span></a>
			</div>
			<!-- preview will be shown in this div -->
			<div id="preview_seo_page" style="display:none;" class="preview_box"></div>
		</form>
	</div>
</div>
<script type="text/javscript">

$(".mceLayout").attr('style','width:100%;height:384px;');

</script>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>