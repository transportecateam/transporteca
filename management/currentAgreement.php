<?php
/**
 * Forwarder My Company  
 */
define('PAGE_PERMISSION','__CURRENT_AGREEMENT__');
 ob_start();
session_start();
$szMetaTitle="Transporteca | Courier Service Offering";
$display_profile_not_completed_message=true;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base = "OBODataExport/";
$t_base_service = "SERVICEOFFERING/";
//$idForwarder =$_SESSION['forwarder_id'];
$kCourierServices= new cCourierServices();

$courierAgreementArr=$kCourierServices->getCourierAgreementData();
$idCourierProvider=$courierAgreementArr[0]['idCourierProvider'];
//$idCourierProviderAgree=$courierAgreementArr[0]['id'];

$_POST['agreeServiceCoverArr']['idCourierAgreeProvider']=$idCourierProviderAgree;
$_POST['agreeTradesOfferArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

//$_POST['pricingArr']=$courierAgreementArr[0];
//$_POST['pricingArr']['idCourierAgreeProvider']=$idCourierProviderAgree;

$idCourierProviderAgree=0;
//$kCourierServices->getCourierServices();
?>
<div id="hsbody-2">
<div id="loader" class="loader_popup_bg" style="display:none;">
	<div class="popup_loader"></div>				
		<img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" />				
</div>
<div id="customs_clearance_pop" class="help-pop">
</div>
	 <?php require_once( __APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php" ); ?> 
    <div class="hsbody-2-right">
		<h4><strong><?php echo t($t_base_service.'title/courier_agreements');?></strong></h4>
		<div id="provider_agreement" class="clearfix">
			<?php courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree);?>
		</div> 
                <hr class="cargo-top-line">
		<div id="provider_agreement_service_trades" class="clearfix">
                    <div style="float:left;width:49%;min-height:200px;" id="provider_agreement_service"> 
                        <?php addDisplayCouierAgreementService($kCourierServices,$idCourierProvider);?>
                    </div>
                    <div style="float:right;width:49%;min-height:200px;" id="provider_agreement_trades"> 
                        <?php addDisplayCouierAgreementTrades($kCourierServices);?>
                    </div>
		</div> 
                <br/>
		<div id="provider_agreement_price" style="width:100%;" class="clearfix">
                    <?php addDisplayCouierAgreementPricing($kCourierServices,true);?>
		</div>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );
?>