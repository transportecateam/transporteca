<?php
/**
 * Edit User Information
 */
  ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_admin_header.php" );
validateManagement();
$t_base = "ForwarderBillings/";
$kBooking = new cBooking();
$kForwarder = new cForwarder();
$kBilling= new cBilling();
$kConfig= new cConfig();

if(!empty($_REQUEST['mode']))
{	
    $idForwarder=sanitize_all_html_input($_REQUEST['id']);	
    $mode=sanitize_all_html_input($_REQUEST['mode']);
    $format = 'Y-m-d'; 
    if($mode=='1')
    {
	$toDate=date($format);
	$fromDate=date($format,strtotime('-1 week'.$toDate));
    }
    if($mode=='2')
    {
	$toDate=date($format);
	$days=date('d')-1;
	//echo $days;
	$fromDate=date($format,strtotime('-'.$days.' days'.$toDate));
    }
    if($mode=='3')
    {
	$days=date('d')-1;
	$tdays=date('d');
	$toDate=date($format,strtotime('-'.$tdays.' days '.date($format)));
	$Date=date($format,strtotime('-'.$days.' days '.date($format)));
	$fromDate=date($format,strtotime('-1 month'.$Date));
    }
    if($mode=='4')
    {
	$days=date('d')-1;
	$toDate=date($format);
	$fromDate=date($format,strtotime('-3 MONTH'));
    }
    if($mode=='5')
    {
        $currentArr=get_quarter();
        $toDate=date($format,strtotime($currentArr['end']));
        $fromDate=date($format,strtotime($currentArr['start']));
    }
    if($mode=='6')
    {
        $prevquaArr=get_quarter(1);
        $toDate=date($format,strtotime($prevquaArr['end']));
        $fromDate=date($format,strtotime($prevquaArr['start']));
        //$toDate=date("Y-m-1 H:i:s");
        //$toDate=date($format,strtotime('-2 months'.$toDate));
        //$fromDate=date($format,strtotime('-3 months'.$toDate));
    }
    if($mode=='7')
    {
	$days=date('d')-1;
	$month=date('m')-1;
	$toDate=date($format);
	$fromDate=date($format,strtotime('-'.$days.' days -'.$month.' month'.$toDate));
    }
    if($mode=='8')
    {
        $todays=date('d');	
        $days=date('d')-1;
        $month=date('m')-1;
        $toDate=date($format,strtotime('-'.$todays.' days -'.$month.' month'.date($format)));
        $Dateto=date($format,strtotime('-'.$days.' days -'.$month.' month'.date($format)));
        $fromDate=date($format,strtotime('-1 year'.$Dateto));
    }
    if($mode=='9')
    {
	$fromDate='';
	$toDate='';
    }
    if($fromDate!='')
    {
	$date = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
	//$date="  BETWEEN '".$fromDate."' and '".$date."'";
    }
    else
    {
	$date='';
    }
    //$fromDate = '2014-10-01';
    //echo "<br> From Date: ".$fromDate." To Date: ".$toDate ;
    if($fromDate)
    {
	echo 'Date range: '.date('d/m/Y',strtotime($fromDate)).' - '.date('d/m/Y',strtotime($toDate));
    }
    else
    {
	echo '&nbsp;';
    }
    
    echo "|||||";
    $dateFormSearchResult = strtotime($fromDate);
    $detailsBankAcc=$kBooking->detailsForwarderBankAcc($idForwarder);
    
    if($mode!='9')
    {
        $dateArr=array($fromDate,$date);
    }
    else
    {
        $dateArr= array();
    }
    $invoiceBillPending = $kBooking->invoicesBilling($idForwarder,0,'');
    $invoiceBillConfirmedPendingAdmin = $kBooking->invoicesBilling($idForwarder,1,$dateArr);
    $invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,$dateArr);

    $sumPendingBalance = $kBooking->sumInvoicesBilling($idForwarder,0,'','');
    $sumCurrentBalancePendingAdmin =$kBooking->sumInvoicesBilling($idForwarder,4,'',$dateArr);
    $sumCurrentBalance = $kBooking->sumInvoicesBilling($idForwarder,2,'',$dateArr);
    $sumPaidReferalAndTransfer = $kBooking->sumInvoicesBilling($idForwarder,6,'','');

    $totalAmountAry = $kBooking->getForwarderTotalInMultipleCurrency($idForwarder);
    $currentAvailableAmountAry = $kBooking->getForwarderTotalInMultipleCurrency($idForwarder,$dateArr,true);

    $totalAmountOfUploadServiceAry = $kBooking->getTransportecaUploadServicePayment($idForwarder);
    $dateFormSearchResult =  strtotime($fromDate);
	
    showBillingDetails_new($t_base,$idForwarder,$dateArr);
}