<?php 
/**
  *  system
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$mode	=  sanitize_all_html_input($_POST['mode']);
$option	=  sanitize_all_html_input($_POST['option']);
$kExplain = new cExplain;
$t_base = "management/textEditor/";
$t_base_error="management/Error/";
SWITCH ($mode)
{	
	CASE 'TNC_CUSTOMER':
	{
		SWITCH ($option)
		{
			CASE 'preview':
			{
				$KwarehouseSearch=new cWHSSearch();
				$tnc=$KwarehouseSearch->selectQueryPreview(4);
				$t_base="TermCondition/";
				$replacearray=array(',',' ');
				?>
				<style>
				<!--
				.heading {color: black;font-style:normal;text-decoration: none;font-size: 16px}
				-->
				</style>
				<div>
					<div id="top">	
					<? foreach($tnc as $terms){?>
					<a name="<?=str_replace($replacearray,"_",strtolower($terms['szDraftHeading']))?>" href="javascript:void(0);" ></a>
					<h5><strong><?=ucfirst($terms['szDraftHeading'])?></strong></h5>
					<p align="right" class="terms_condition_top">	
						<a href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
					</p>
					<?=$terms['szDraftDescription']?>
					<br />
					<? } ?>
					<br />
					</div>
				</div>
		<?		BREAK;
			}
			CASE 'publish':
			{	
?>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
					<div id='error-Ver'></div>
						<?=t($t_base.'title/please_add_any_comment');?><br /><br />
						<input type ="text" id="comment" name ="comment" value=''>
						<br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="submitVersionUpdates('CUSTOMER')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?
				BREAK;
			}
			CASE 'cancel':
			{	
?>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
					<div id='error-Ver'></div>
						<?=t($t_base.'title/are_you_sure');?><br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="submitVersionUpdates('CANCEL_CUSTOMER_DRAFTS_TNC')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?
				BREAK;
			}
		}
		BREAK;
	}
	CASE 'TNC_FORWARDER':
	{
		SWITCH ($option)
		{
			CASE 'preview':
			{
				$KwarehouseSearch=new cWHSSearch();
				$tnc=$KwarehouseSearch->selectQueryPreview(1);
				$t_base="TermCondition/";
				$replacearray=array(',',' ');
				?>
				<style>
				<!--
				.heading {color: black;font-style:normal;text-decoration: none;font-size: 16px}
				-->
				</style>
				<div>
					
					<?if($tnc)
					{
						$heading=$tnc[0]['szDraftHeading'];
						$description=$tnc[0]['szDraftDescription'];
					?>
				
					
					<div  id="top">
					<? /*	
					
					<p><h5>
						<a href="javascript:void(0);" name="<?=str_replace($replacearray,"_",strtolower($heading))?>" class="heading"><strong><?=ucfirst($heading)?></strong></a>
					</h5></p>
					<p align="right" class="terms_condition_top">	
						<a href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
					</p>
					
					<p><?=$description?></p>
					<br />
					*/ ?>
					<? foreach($tnc as $terms){?>
					<a name="<?=str_replace($replacearray,"_",strtolower($terms['szDraftHeading']))?>" href="javascript:void(0);" ></a>
					<h5><strong><?=ucfirst($terms['szDraftHeading'])?></strong></h5>
					<p align="right" class="terms_condition_top">	
						<a href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
					</p>
					<?=$terms['szDraftDescription']?>
					<br />
					<? }} ?>
					<br />
					</div>
				</div>
		<?		BREAK;
			}
			CASE 'publish':
			{
?>
			<script type="text/javascript">
				//$('#comment').focus();
			</script>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;width:650px;">
					<div id='error-Ver'></div>
						<H5><?=t($t_base.'title/issuee_new_tnc');?></H5>
						<p><?=t($t_base.'title/you_are_about');?></p>
						<br/>
						<p style="font-style: italic;"><?=t($t_base.'title/dear_admin');?></p>
						<p style="font-style: italic;"><?=t($t_base.'title/transporteca_has');?></p>
						<textarea id="comment" name ="comment" style="font-style: italic" onfocus="blank_text('comment','<?=t($t_base.'title/comment_default_text');?>','1')" onblur="show_text('comment','<?=t($t_base.'title/comment_default_text');?>','1')"><?=t($t_base.'title/comment_default_text');?></textarea>
						<p style="font-style: italic;"><?=t($t_base.'title/click_here');?> <?=date('d. F Y',strtotime(date("Y-m-d").'+30 days'))?>.</p>
						<br /><p style="font-style: italic;"><?=t($t_base.'title/kind_regards');?> ,</p>
						<p style="font-style: italic;"><?=t($t_base.'title/transporteca');?></p><br />
						<p align="center">	
							<a href="javascript:void(0);" id="publishContent" class="button1"><span><?=t($t_base.'fields/confirm');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?			BREAK;
			}
			CASE 'cancel':
			{	
?>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
					<div id='error-Ver'></div>
						<?=t($t_base.'title/are_you_sure_cancel_to_draft');?><br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="submitVersionUpdates('CANCEL_FORWARDER_DRAFTS_TNC')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?
				BREAK;
			}
		}
		BREAK;
	}	
	CASE 'FAQ_CUSTOMER':
	{
		SWITCH ($option)
		{
			CASE 'preview':
			{
				$KwarehouseSearch=new cWHSSearch();
				$faq=$KwarehouseSearch->selectQueryPreview(3);
				$t_base="FAQ/";
				?>
				<div>
					
					<div>	
					<? if(!empty($faq))
					{
						foreach($faq AS $faqs)
						{				
					?>		
					<p><?=t($t_base.'title/q');?>:&nbsp;<?=$faqs['szDraftHeading']?></p>
					<p style="float: left;padding-right: 2px;"><?=t($t_base.'title/a');?>:&nbsp;<?=$faqs['szDraftDescription']?></p>
					 <br/>
					 <? }} ?>
					</div>
				</div>
				<?
				BREAK;
			}
			CASE 'publish':
			{	
?>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
					<div id='error-Ver'></div>
						<?=t($t_base.'title/are_you_sure_to_publish');?><br />
					
						<br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="submitVersionUpdates('CUSTOMER_FAQ')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?
				BREAK;
			}
			CASE 'cancel':
			{	
?>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
					<div id='error-Ver'></div>
						<?=t($t_base.'title/are_you_sure_cancel_to_draft');?><br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="submitVersionUpdates('CANCEL_CUSTOMER_DRAFTS_FAQ')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?
				BREAK;
			}
		}
		BREAK;
	}
	CASE 'FAQ_FORWARDER':
	{
		SWITCH ($option)
		{
			CASE 'preview':
			{
				$KwarehouseSearch=new cWHSSearch();
				$faq=$KwarehouseSearch->selectQueryPreview(2);
				$t_base="FAQ/";
				?>
				<br />
				<div>	
					<? if(!empty($faq))
					{
						foreach($faq AS $faqs)
						{				
					?>		
					<p>Q:&nbsp;<?=$faqs['szDraftHeading']?></p>
					<p style="float: left;padding-right: 5px;">A:<?=$faqs['szDraftDescription']?></p>
					 <br/>
					 <? }} ?>
					</div>
				
				<?
				BREAK;
			}
			CASE 'publish':
			{	
?>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
					<div id='error-Ver'></div>
						<?=t($t_base.'title/are_you_sure_to_publish');?><br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="submitVersionUpdates('FORWARDER_FAQ')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?
				BREAK;
			}
			CASE 'cancel':
			{	
?>
			<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
					<div id='error-Ver'></div>
						<?=t($t_base.'title/are_you_sure_cancel_to_draft');?><br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="submitVersionUpdates('CANCEL_FORWARDER_DRAFTS_FAQ')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
						</p>
					</div>
			 	</div>
			</div> 				    
<?
				BREAK;
			}
		}
		BREAK;
	}
	CASE 'CUSTOMER':
	{	
		$comment = sanitize_all_html_input($_POST['comment']);
		$versionError=$kAdmin->updateVersion($comment,$mode);
			if($versionError==false)
			{	echo "ERROR|||||";
				if(!empty($kAdmin->arErrorMessages))
				{
				?>
				<div id="regError" class="errorBox" style="display:block;">
				<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
				<div id="regErrorList">
				<ul>
				<?php
				      foreach($kAdmin->arErrorMessages as $key=>$values)
				      {
				      ?><li><?=$values?></li>
				      <?php 
				      }
				?>
				</ul>
				</div>
				</div>
				<?
				}
			}
			else
			{	echo "SUCCESS|||||";
				$kAdmin->saveTextEditDataConfirm($idAdmin,__TEXT_EDIT_TERMS_CUSTOMER__);
				$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_CUSTOMER__);
				viewTnc($textEditor,'CUSTOMER',__TEXT_EDIT_TERMS_CUSTOMER__);
			}
		BREAK;	
	}
	CASE 'FORWARDER':
	{
		$comment = sanitize_specific_tinymce_html_input($_POST['comment']);
		$versionError=$kAdmin->updateVersion($comment,$mode);
		
		if($versionError==false)
		{	echo "ERROR|||||";
			if(!empty($kAdmin->arErrorMessages))
			{
			?>
			<div id="regError" class="errorBox" style="display:block;">
			<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
			<div id="regErrorList">
			<ul>
			<?php
			      foreach($kAdmin->arErrorMessages as $key=>$values)
			      {
			      ?><li><?=$values?></li>
			      <?php 
			      }
			?>
			</ul>
			</div>
			</div>
			<?
			}
		}
		else
		{	
			
			echo "SUCCESS|||||";
			$kAdmin->saveTextEditDataConfirm($idAdmin,__TEXT_EDIT_TERMS_FORWARDER__);
			$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_FORWARDER__);
			viewTnc($textEditor,'FORWARDER',__TEXT_EDIT_TERMS_FORWARDER__);
		}
		BREAK;
	}
	CASE 'CANCEL_CUSTOMER_DRAFTS_TNC':
	{
		if($kAdmin->removeDraftData($idAdmin,__TEXT_EDIT_TERMS_CUSTOMER__))
		{
			echo "SUCCESS|||||";
			$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_CUSTOMER__);
			viewTnc($textEditor,'CUSTOMER',__TEXT_EDIT_TERMS_CUSTOMER__);
		}
		BREAK;
	}
	CASE 'CANCEL_FORWARDER_DRAFTS_TNC':
	{
		if($kAdmin->removeDraftData($idAdmin,__TEXT_EDIT_TERMS_FORWARDER__))
		{
			echo "SUCCESS|||||";
			$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_TERMS_FORWARDER__);
			viewTnc($textEditor,'FORWARDER',__TEXT_EDIT_TERMS_FORWARDER__);
		}
		BREAK;
	}
	CASE 'CUSTOMER_FAQ':
	{
		echo "SUCCESS|||||";
		$kAdmin->saveTextEditDataConfirm($idAdmin,__TEXT_EDIT_FAQ_CUSTOMER__);
		$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_FAQ_CUSTOMER__);
		viewTnc($textEditor,'FAQ_CUSTOMER_',__TEXT_EDIT_FAQ_CUSTOMER__);
		BREAK;
	}
	CASE 'CANCEL_CUSTOMER_DRAFTS_FAQ':
	{
		if($kAdmin->removeDraftData($idAdmin,__TEXT_EDIT_FAQ_CUSTOMER__))
		{
			echo "SUCCESS|||||";
			$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_FAQ_CUSTOMER__);
			viewTnc($textEditor,'FAQ_CUSTOMER_',__TEXT_EDIT_FAQ_CUSTOMER__);
		}
		BREAK;
	}
	CASE 'FORWARDER_FAQ':
	{
		echo "SUCCESS|||||";
		$kAdmin->saveTextEditDataConfirm($idAdmin,__TEXT_EDIT_FAQ_FORWARDER__);
		$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_FAQ_FORWARDER__);
		viewTnc($textEditor,'FAQ',__TEXT_EDIT_FAQ_FORWARDER__);
		BREAK;
	}
	CASE 'CANCEL_FORWARDER_DRAFTS_FAQ':
	{
		if($kAdmin->removeDraftData($idAdmin,__TEXT_EDIT_FAQ_FORWARDER__))
		{
			echo "SUCCESS|||||";
			$textEditor=$kAdmin->textEditor($idAdmin,$id,__TEXT_EDIT_FAQ_FORWARDER__);
			viewTnc($textEditor,'FAQ',__TEXT_EDIT_FAQ_FORWARDER__);
		}
		BREAK;
	}
	CASE 'CANCEL_EXPLAIN_DATA':
	{
		$id = sanitize_all_html_input($_POST['id']);
		if($kExplain->removeExplainDraftData($id))
		{
			return true;
		}
		BREAK;
	}
	CASE 'SUBMIT_EXPLAIN_DATA':
	{
		$id = sanitize_all_html_input($_POST['id']);
		if($kExplain->submitExpalinDraftData($id))
		{
			return true;
		}
		BREAK;
	}
			
}

if(isset($option) && !isset($_POST['mode']))
{
	SWITCH($option)
	{
		CASE 'preview':
		{
			$id = sanitize_all_html_input($_POST['id']);
			$previewData=$kExplain->selectQueryPreviewData($id);
			$t_base="management/explaindata/";
			$replacearray=array(',',' ');
			?>
			<style>
			<!--
			.heading {color: black;font-style:normal;text-decoration: none;font-size: 16px}
			-->
			</style>
			<div>
				<div id="top" style="text-align: left;">	
				<? foreach($previewData as $preview){?>
				<a name="<?=str_replace($replacearray,"_",strtolower($preview['szDraftHeading']))?>" href="javascript:void(0);" ></a>
				<h5><strong><?=ucfirst($preview['szDraftHeading'])?></strong></h5>
				<p align="right" class="terms_condition_top">	
					<a href="#top"><strong><?=t($t_base.'title/top')?></strong></a>
				</p>
				<?=refineDescriptionData($preview['szDraftDescription'])?>
				<br />
				<? } ?>
				<br />
				</div>
			</div>
	<?		BREAK;
		}
		CASE 'publish':
		{	
			$id = sanitize_all_html_input($_POST['id']);
	?>
		<div id="popup-bg"></div>
		 	<div id="popup-container">	
				<div class="popup contact-popup" style="height: auto;">
				<div id='error-Ver'></div>
					<?=t($t_base.'title/are_you_sure_to_publish');?>
					<br /><br />
					<p align="center">	
						<a href="javascript:void(0);" onclick="submitExplainVersion('SUBMIT_EXPLAIN_DATA','<?=$id?>')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
						<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
					</p>
				</div>
		 	</div>
		</div> 				    
	<?
		BREAK;
		}
		CASE 'cancel':
		{	
		$id = sanitize_all_html_input($_POST['id']);
	?>
		<div id="popup-bg"></div>
		 	<div id="popup-container">	
				<div class="popup contact-popup" style="height: auto;">
				<div id='error-Ver'></div>
					<?=t($t_base.'title/are_you_sure_cancel_to_draft');?><br /><br />
					<p align="center">	
						<a href="javascript:void(0);" onclick="submitExplainVersion('CANCEL_EXPLAIN_DATA','<?=$id?>')" class="button1"><span><?=t($t_base.'fields/ok');?></span></a>			
						<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
					</p>
				</div>
		 	</div>
		</div> 				    
	<?
		BREAK;
		}
	}
}


?>