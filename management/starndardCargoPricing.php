<?php 
/**
  *  admin--- E-Mail log management
  */
define('PAGE_PERMISSION','__STANDARD_CARGO_PRICING__');  
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca | Standard Cargo Pricing";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/admin_header.php");
validateManagement();
$t_base = "management/uploadService/";
$kExplain = new cExplain();
//$vogaPageAry = array();
//$vogaPageAry = $kExplain->getAllNewLandingPageData(false,false,false,false,false,false,true);
$automatedRfqResponseAry=array();
$automatedRfqResponseAry =$kExplain->getAutomatedRfqResponseList();
 
$kWhsSearch = new cWHSSearch();  
$arrDescriptions = array("'__GOOGLE_MAP_V3_API_KEY__'"); 
$bulkManagemenrVarAry = array();
$bulkManagemenrVarAry = $kWhsSearch->getBulkManageMentVariableByDescription($arrDescriptions);
$szGoogleMapV3Key = $bulkManagemenrVarAry['__GOOGLE_MAP_V3_API_KEY__'];
$szLanguage = "en"; 

?>
<div id="warning_msg" style="display:none;">
    <?php showWarningMessageWhenChangeTheValue();?>
</div>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $szGoogleMapV3Key; ?>&language=<?php echo $szLanguage; ?>" type="text/javascript"></script>
<div id="hsbody-2">
    <?php require_once(__APP_PATH_LAYOUT__ ."/contentManagementOperationsLeftNav.php"); ?> 
    <div class="hsbody-2-right">
        <h4><strong><?php echo t($t_base.'title/standard_cargo_pricing'); ?></strong></h4>
        <p><?php echo t($t_base.'messages/standard_cargo_pricing_description'); ?></p>
        <br>
        <div class="clearfix">
            Landing page
            <select id="idAutomatedRfqResponse" name="idAutomatedRfqResponse" onchange="display_pricing_handler(this.value)" style="max-width:421px;">
                <?php
                    echo "<option ''>".t($t_base.'fields/select')."</option>";
                    if(!empty($automatedRfqResponseAry))
                    { 
                        foreach($automatedRfqResponseAry as $automatedRfqResponseArys)
                        {
                           
                            echo "<option value='".$automatedRfqResponseArys['id']."'>".$automatedRfqResponseArys['szResponse']."</option>";
                        }
                    }
                ?>	
            </select> 
            <span style="float: right;"> 
                <input id="idAutomatedRfqResponseHidden" name="idAutomatedRfqResponseHidden" value="0" type="hidden">
                <a class="button1" id="standard_cargo_select_new_button" onclick="open_automated_rfq_popup();"><span id="standard_cargo_select_new_button_text"><?=t($t_base.'fields/new');?></span></a>
                <a class="button2" style="opacity:0.4;" id="standard_cargo_delete_new_button"><span id="standard_cargo_delete_new_button"><?=t($t_base.'fields/delete');?></span></a>
            </span>
        </div> 
        <div class="clearfix">&nbsp;</div>
        <div id="standard_cargo_quote_pricing_container" style="display: none;">&nbsp;</div>	
    </div>
    <div id="available_variables_popup" style="display: none;">
        <?php echo display_available_variables(); ?>
    </div>
    <div id="add_standard_quote_pricing_form_container">
        <?php
           // echo display_add_standard_cargo_pricing($idLandingPage)
        ?>
    </div> 
</div>
<?php 
include( __APP_PATH_LAYOUT__ . "/admin_footer.php" );

?>