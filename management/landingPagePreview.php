<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );

$landingPageDataAry = $_POST['landingPageArr'] ;
$iLanguage = $landingPageDataAry['iLanguage'];

$landingPageDataAry['szTextD'] = $landingPageDataAry['szTextX'];
if(!empty($landingPageDataAry['szMetaTitle']))
{
	$szMetaTitle = $landingPageDataAry['szMetaTitle'] ;
}

if(!empty($landingPageDataAry['szMetaKeywords']))
{
	$szMetaKeywords = $landingPageDataAry['szMetaKeywords'] ;
}
if(!empty($landingPageDataAry['szMetaDescription']))
{
	$szMetaDescription = $landingPageDataAry['szMetaDescription'];
}

require_once(__APP_PATH_LAYOUT__."/preview_header.php");
$t_base = "LandingPage/";

echo previewLandingPage_admin($landingPageDataAry);

require_once(__APP_PATH_LAYOUT__."/footer.php");
?>	