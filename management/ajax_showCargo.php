<?php
if( !isset( $_SESSION ) )
{
	session_start();
}

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_admin_header.php");

$t_base = "home/homepage/";

$kConfig=new cConfig();

$number1 = $_REQUEST['number'];
$number = $_REQUEST['number']+1;

// geting all weight measure 
$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

// geting all cargo measure 
$cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');

?>
<div class="oh">
<hr>
				<p class="fl-33"><br /><?=t($t_base.'fields/dimensions');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DIMENTIONS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
				<div class="fl-65 dimensions-field">				
						<span class="f-size-12">
							<?=t($t_base.'fields/length');?><br/>
							<input type="text" pattern="[0-9]*" name="searchAry[iLength][<?=$number?>]" value="<?=$postSearchAry['iLength'][$number]?>" id= "iLength<?=$number?>" /><br>
							<span id="iLength<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
						<span class="f-size-12">
							<?=t($t_base.'fields/width');?><br/>
							<input type="text" pattern="[0-9]*"  name="searchAry[iWidth][<?=$number?>]" value="<?=$postSearchAry['iWidth'][$number]?>" id= "iWidth<?=$number?>" /><br>
							<span id="iWidth<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
						<span class="f-size-12">
							<?=t($t_base.'fields/height');?><br/>
							<input type="text" pattern="[0-9]*" name="searchAry[iHeight][<?=$number?>]" value="<?=$postSearchAry['iHeight'][$number]?>" id= "iHeight<?=$number?>" />
							<span id="iHeight<?=$number?>_div" style="display:none;"></span>
						</span>
						<span class="f-size-12">
						&nbsp;<br/>
							<select name="searchAry[idCargoMeasure][<?=$number?>]">
								 <?
								 	if(!empty($cargoMeasureAry))
								 	{
								 		foreach($cargoMeasureAry as $cargoMeasureArys)
								 		{
								 			?>
								 				<option value="<?=$cargoMeasureArys['id']?>" <? if($postSearchAry['idCargoMeasure'][$number]==$cargoMeasureArys['id']){?> selected <? }?>><?=$cargoMeasureArys['szDescription']?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</span>
						<span class="f-size-14 cross_seperator">&nbsp;<br/>X</span>
						<span class="last f-size-12">
							<?=t($t_base.'fields/quantity');?><br/>
							<input type="text" name="searchAry[iQuantity][<?=$number?>]" value="<?=$postSearchAry['iQuantity'][$number]?>" id= "iQuantity<?=$number?>" /><br>
							<span id="iQuantity<?=$number?>_div" style="display:none;"></span>
						</span>
					</div>				
			</div>			
			<div class="oh">
				<p class="fl-33"><?=t($t_base.'fields/weight');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/WEIGHT_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/WEIGHT_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a>&nbsp;</a></p>
				<div class="fl-65 dimensions-field">					
						<span>
							<input type="text" onkeyup="on_enter_key_press(event,'validateLandingPageForm','<?=$function_params?>');" name="searchAry[iWeight][<?=$number?>]" value="<?=$postSearchAry['iWeight'][$number]?>" id= "iWeight<?=$number?>" />
							<span id="iWeight<?=$number?>_div" style="display:none;"></span>
						</span>
						<span style="width:30.5%">
							<select size="1" name="searchAry[idWeightMeasure][<?=$number?>]" style="margin-left: 13px;width: 78px;">
								 <?
								 	if(!empty($weightMeasureAry))
								 	{
								 		foreach($weightMeasureAry as $weightMeasureArys)
								 		{
								 			?>
								 				<option value="<?=$weightMeasureArys['id']?>" <? if($postSearchAry['idWeightMeasure'][$number]==$weightMeasureArys['id']){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</span>
						<!-- 
						<span style="padding:2px 0 0;" class="dangerous_cargo"><?=t($t_base.'fields/dangerous_cargo');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/DANGEROUS_CARGO_TOOL_TIP_TEXT');?>','dang-cargo-pop',event);" onmouseout="hide_tool_tip('dang-cargo-pop')">&nbsp;</a></span>
						<span class="last">
						
							<select size="1" name="searchAry[iDangerCargo][]" style="min-width:65px">
								<option value="No" selected="">No</option>
								<option value="Yes">Yes</option>
							</select>
						</span>
						 -->
					</div>				
			</div>	