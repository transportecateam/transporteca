<?php 
/**
  *  ADMIN--- NON ACKNOWLEDGE
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
$t_base="management/operations/confirmation/";
validateManagement();
$kBooking = new cBooking;
if(isset($_POST['id']))
{
	$mode = trim(sanitize_all_html_input($_POST['mode']));
	$id = (int)$_POST['id'];
	SWITCH($mode)
	{
		CASE 'CHECK_EMAIL_DATE_STATUS':
		{
			if($kBooking->findBookingConfirmedDate($id))
			{
				echo true;
			}
			else
			{
				echo false;
			}
			BREAK;
		}
		CASE 'RESEND_BOOKING_CONFIRMATION_EMAIL':
		{
			$data = $kBooking->bookingConfirmationDetails($id);
			if($data!=array() && confirmBookingAfterPaymentResend($data))
			{
				showConfirmationPopUpMessage($t_base);
			
			}
			BREAK;
		}
	}
}
?>