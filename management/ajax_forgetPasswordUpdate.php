<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kConfig = new cConfig();
$case = 'MANAGEMENT';
$id = $idAdmin;
$data = $_POST['update'];
if($kConfig->updateForgotPassword($id,$data,$case) == true)
{
	echo "SUCCESS";
}
$t_base_error = "management/Error/";
if(!empty($kConfig->arErrorMessages))
{
?>
<div id="regError" class="errorBox" style="display:block;">
<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
      foreach($kConfig->arErrorMessages as $key=>$values)
      {
      ?><li><?=$values?></li>
      <?php 
      }
?>
</ul>
</div>
</div>
<?
	}
?>