<?php 
/**
  *  Admin--- Users -- Customers---Forwarder Companies And Profiles
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
$szMetaTitle="Transporteca || Users";
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$t_base = "Users/AccountPage/";
$t_base_error="management/Error/";
$kConfig = new cConfig();
$kUser = new cUser();
$successFlag=false;
$idCustomer = (int)$_POST['id'];
$kUser->getUserDetails($idCustomer);
if(!empty($_POST['editUserInfoArr']) && $_REQUEST['showflag']=='user_info')
{
    //print_r($_POST['editUserInfoArr']);
    if($kUser->updateUserInfo($_POST['editUserInfoArr'],$idCustomer,true))
    {	
        echo "<script>$('#userInfo').html('".$kUser->szFirstName." ".$kUser->szLastName."')</script>";
        $successFlag=true;
        $kUser->getUserDetails($idCustomer);
    }
}
if(!empty($_POST['editUserInfoArr']) && $_REQUEST['showflag']=='contact_info')
{
    if($kUser->updateContactInfo($_POST['editUserInfoArr'],$idCustomer,true))
    {
        $successContactUpdatedFlag=true;
        $kUser->getUserDetails($idCustomer);
    }
    if(!empty($_POST['editUserInfoArr']['szPhoneNumberUpdate']))
    {
        $_POST['editUserInfoArr']['szPhoneNumber']=urldecode(base64_decode($_POST['editUserInfoArr']['szPhoneNumberUpdate']));
    }
}
if(!empty($_POST['editUserInfoArr']) && $_REQUEST['showflag']=='change_pass')
{
	if($kUser->changePassword($_POST['editUserInfoArr'],$idCustomer))
	{
		$successPasswordUpdatedFlag=true;
	}
}

$allCountriesArr=$kConfig->getAllCountries(true);
$allCurrencyArr=$kConfig->getBookingCurrency(false,true);
$langArr=$kConfig->getLanguageDetails();

$showflag=$_REQUEST['showflag'];
if($showflag=='user_info' && !$successFlag)
{

if(!empty($kUser->arErrorMessages)){
?>
<div id="regError" class="errorBox">
<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kUser->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php }?>	
<div id="user_information">
<h5><strong>User Information</strong></h5>
<form name="updateUserInfo" id="updateUserInfo" method="post">
<div id="userInfo">
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szFirstName]" id="szFirstName" value="<?=$_POST['editUserInfoArr']['szFirstName'] ? $_POST['editUserInfoArr']['szFirstName'] : $kUser->szFirstName ?>" onblur="closeTip('fname');" onfocus="openTip('fname');"/></span>
				<div class="field-alert"><div id="fname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
				<span class="field-container"><input type="text" name="editUserInfoArr[szLastName]" id="szLastName" value="<?=$_POST['editUserInfoArr']['szLastName'] ? $_POST['editUserInfoArr']['szLastName'] : $kUser->szLastName ?>" onblur="closeTip('lname');" onfocus="openTip('lname');"/></span>
				<div class="field-alert"><div id="lname" style="display:none;"><?=t($t_base.'messages/your_name');?></div></div>
			</label>
                        <label class="profile-fields">
                            <span class="field-name">&nbsp;</span>
                            <span class="field-container checkbox-container"><input type="checkbox" name="editUserInfoArr[iPrivate]" onclick="lock_company_field_myaccount();" id="iPrivate" value="1" <?php echo (($_POST['editUserInfoArr'] ? $_POST['editUserInfoArr']['iPrivate'] : $kUser->iPrivate)==1)?'checked':''; ?> /> <span class="private-text"><?=t($t_base.'fields/private');?></span></span> 
			</label>
                        <label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/language');?></span>
                            <span class="field-container">
                                <select size="1" name="editUserInfoArr[iLanguage]" id="iLanguage" style="max-width:221px;">
                                    <option value="">Select</option>
                                    <?php
                                        if(!empty($langArr))
                                        {
                                            foreach($langArr as $langArrs)
                                            {
                                                ?><option value="<?=$langArrs['id']?>" <?=((($_POST['editUserInfoArr']['iLanguage'])?$_POST['editUserInfoArr']['iLanguage']:$kUser->iLanguage) ==  $langArrs['id'] ) ? "selected":""?>><?=ucwords($langArrs['szName']);?></option>                                              <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </span> 
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/c_name');?></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szCompanyName]" id="szCompanyName" value="<?=$_POST['editUserInfoArr']['szCompanyName'] ? $_POST['editUserInfoArr']['szCompanyName'] : $kUser->szCompanyName ?>" onblur="closeTip('cname');" onfocus="openTip('cname');"/></span>
                            <div class="field-alert"><div id="cname" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/c_reg_n');?></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szCompanyRegNo]" id="szCompanyRegNo" value="<?=$_POST['editUserInfoArr']['szCompanyRegNo'] ? $_POST['editUserInfoArr']['szCompanyRegNo'] : $kUser->szCompanyRegNo ?>" onblur="closeTip('c_reg_no');" onfocus="openTip('c_reg_no');"/></span>
                            <div class="field-alert"><div id="c_reg_no" style="display:none;"><?=t($t_base.'messages/comapany_reg_no');?></div></div>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szAddress1]" id="szAddress1" value="<?=$_POST['editUserInfoArr']['szAddress1'] ? $_POST['editUserInfoArr']['szAddress1'] : $kUser->szAddress1 ?>" onblur="closeTip('address1');" onfocus="openTip('address1');"/></span>
                            <div class="field-alert"><div id="address1" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szAddress2]" id="szAddress2" value="<?=$_POST['editUserInfoArr']['szAddress2'] ? $_POST['editUserInfoArr']['szAddress2'] : $kUser->szAddress2 ?>" onblur="closeTip('address2');" onfocus="openTip('address2');"/></span>
                            <div class="field-alert"><div id="address2" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szAddress3]" id="szAddress3" value="<?=$_POST['editUserInfoArr']['szAddress3'] ? $_POST['editUserInfoArr']['szAddress3'] : $kUser->szAddress3 ?>" onblur="closeTip('address3');" onfocus="openTip('address3');"/></span>
                            <div class="field-alert"><div id="address3" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/postcode');?></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szPostCode]" id="szPostCode" value="<?=$_POST['editUserInfoArr']['szPostCode'] ? $_POST['editUserInfoArr']['szPostCode'] : $kUser->szPostcode ?>" onblur="closeTip('postcode');" onfocus="openTip('postcode');"/></span>
                            <div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/city');?></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szCity]" id="szCity" value="<?=$_POST['editUserInfoArr']['szCity'] ? $_POST['editUserInfoArr']['szCity'] : $kUser->szCity ?>" onblur="closeTip('city');" onfocus="openTip('city');"/></span>
                            <div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
                            <span class="field-container"><input type="text" name="editUserInfoArr[szState]" id="szState" value="<?=$_POST['editUserInfoArr']['szState'] ? $_POST['editUserInfoArr']['szState'] : $kUser->szState ?>" onblur="closeTip('state');" onfocus="openTip('state');"/></span>
                            <div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/invoice_address');?></div></div>
			</label>
			<label class="profile-fields">
                            <input type="hidden" name="editUserInfoArr[szOldCountry]" id="szOldCountry" value="<?=$_POST['editUserInfoArr']['szOldCountry'] ? $_POST['editUserInfoArr']['szOldCountry'] : $kUser->szCountry ?>" />
                            <input type="hidden" name="editUserInfoArr[szPhoneNo]" id="szPhoneNo" value="<?=$_POST['editUserInfoArr']['szPhoneNo'] ? $_POST['editUserInfoArr']['szPhoneNo'] : html_entity_decode($kUser->szPhoneNumber) ?>" />
                            <span class="field-name"><?=t($t_base.'fields/country');?></span>
                            <span class="field-container">
                                <select size="1" name="editUserInfoArr[szCountry]" id="szCountry" style="max-width:221px;">
                                    <?php
                                        if(!empty($allCountriesArr))
                                        {
                                            foreach($allCountriesArr as $allCountriesArrs)
                                            {
                                                ?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['editUserInfoArr']['szCountry'])?$_POST['editUserInfoArr']['szCountry']:$kUser->szCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>                                              <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </span>
			</label>
			<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
			<span class="field-container">
                            <select size="1" name="editUserInfoArr[szCurrency]" id="szCurrency" onblur="closeTip('currency');" onfocus="openTip('currency');">
                                <?php
                                    if(!empty($allCurrencyArr))
                                    {
                                        foreach($allCurrencyArr as $allCurrencyArrs)
                                        {
                                            ?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['editUserInfoArr']['szCurrency'])?$_POST['editUserInfoArr']['szCurrency']:$kUser->szCurrency) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                                        }
                                    }
                                ?>
                            </select>
			</span>
			<div class="field-alert"><div id="currency" style="display:none;"><?=t($t_base.'messages/currency');?></div></div>
		</label>	
		<input type='hidden' name='showflag' value="<?=$showflag?>">
		<br/>
		<p align="right" style="width:60%"><a href="javascript:void(0)" class="button1" onclick="javscript:update_customerinfo_details();"><span><?=t($t_base.'fields/save');?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_customer_info(<?=$idCustomer?>);"><span><?=t($t_base.'fields/cancel');?></span></a></p>
<input type="hidden" name="id" value="<?=$idCustomer?>">
</div>
</form>	
<script type="text/javascript">
    lock_company_field_myaccount();
</script>
</div><br/><br/>
<? }else{?>
<div id="user_information">
	<h5><strong><?=t($t_base.'fields/user_info');?></strong> <a href="javascript:void(0)" onclick="editCustomerInformation(<?=$idCustomer?>);"><?=t($t_base.'fields/edit');?></a></h5>
	
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/f_name');?></span>
		<span class="field-container"><?=$kUser->szFirstName?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/l_name');?></span>
		<span class="field-container"><?=$kUser->szLastName?></span>
	</div>
        <div class="ui-fields">
            <span class="field-name">&nbsp;</span>
            <span class="field-container checkbox-container"><input type="checkbox" disabled="disabled" <?php echo ($kUser->iPrivate==1)?'checked="checked"':''; ?> > <span class="private-text"><?=t($t_base.'fields/private');?></span></span>
        </div>
        <div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/language');?></span>
                <span class="field-container"><?php echo ($kUser->szCustomerLanguage!='')?ucwords($kUser->szCustomerLanguage):'N/A'; ?></span>
	</div>
        <?php if($kUser->iPrivate!=1){?>
	<div class="ui-fields">
            <span class="field-name"><?=t($t_base.'fields/c_name');?></span>
            <span class="field-container"><?=$kUser->szCompanyName?></span>
	</div>
	<div class="ui-fields">
            <span class="field-name"><?=t($t_base.'fields/c_reg_n');?></span>
            <span class="field-container"><?=$kUser->szCompanyRegNo?></span>
	</div>
        <?php } ?> 
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/address_line1');?></span>
		<span class="field-container"><?=$kUser->szAddress1?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/address_line2');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szAddress2?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/address_line3');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szAddress3?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/postcode');?></span>
		<span class="field-container"><?=$kUser->szPostcode?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/city');?></span>
		<span class="field-container"><?=$kUser->szCity?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/p_r_s');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
		<span class="field-container"><?=$kUser->szState?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/country');?></span>
		<span class="field-container"><?=$kUser->szCountryName?></span>
	</div>
	<div class="ui-fields">
		<span class="field-name"><?=t($t_base.'fields/prefer_currency');?></span>
		<span class="field-container">
			<?=$kUser->szCurrencyName?>
		</span>
	</div>
	</div>
	<br />
	<br />
<?php }
if($showflag=='contact_info' && !$successContactUpdatedFlag){
    
    $kConfig = new cConfig();
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);

    if(isset($_POST['editUserInfoArr']['idInternationalDialCode']))
    {        
        $idInternationalDialCode = $_POST['editUserInfoArr']['idInternationalDialCode'] ;
    }
    else
    {
        $idInternationalDialCode = $kUser->idInternationalDialCode ;
    }
    
    ?>
<div id="contact_info">
	<?php
	if(!empty($kUser->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kUser->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php }?>
	<h5><strong><?=t($t_base.'messages/contact_info');?></strong></h5>
		<form id="edit_contact_information_data" name="edit_contact_information_data" method="post"> 
		<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/email');?></span>
					<span class="field-container"><input type="text" name="editUserInfoArr[szEmail]" id="szEmail" value="<?=$_POST['editUserInfoArr']['szEmail'] ? $_POST['editUserInfoArr']['szEmail'] : $kUser->szEmail ?>" onblur="closeTip('email');" onfocus="openTip('email');" value="<?=$_POST['createAccountArr']['szEmail']?>" /></span>
					<div class="field-alert1" style="left:60.5%;width: 290px;"><div id="email" style="display:none;"><?=t($t_base.'messages/email');?></div></div>
			</label>
			<div class="ui-fields">
				<span class="field-name"><?=t($t_base.'fields/contact_by_email');?></span>
				<span class="field-container checkbox-ab"><input type="checkbox" name="editUserInfoArr[iSendUpdate]" id="iSendUpdate" <?=((($_POST['editUserInfoArr']['iSendUpdate'])?$_POST['editUserInfoArr']['iSendUpdate']:$kUser->iAcceptNewsUpdate) ==  1 ) ? "checked":""?> value='1'/><?=t($t_base.'messages/please_news_updates');?></span>
			</div>
			
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/p_number');?></span>
				<span class="field-container">
                                <select size="1"  name="editUserInfoArr[idInternationalDialCode]" id="idInternationalDialCode" onfocus="change_dropdown_value(this.id);">
                                    <?php
                                       if(!empty($dialUpCodeAry))
                                       {
                                            $usedDialCode = array();
                                           foreach($dialUpCodeAry as $dialUpCodeArys)
                                           {
                                                if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                                {
                                                    $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idInternationalDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                                <?php
                                                }
                                           }
                                       }
                                   ?>
                                   </select>
                                    <input type="text" style="width:107px;" name="editUserInfoArr[szPhoneNumber]" onblur="closeTip('phone_number');" onfocus="openTip('phone_number');" id="szPhoneNumber" value="<?=$_POST['editUserInfoArr']['szPhoneNumber'] ? $_POST['editUserInfoArr']['szPhoneNumber'] : $kUser->szPhoneNumber ?>" />
                                </span>
				<div class="field-alert" style="left:60.5%;width: 290px;"><div id="phone_number" style="display:none;"><?=t($t_base.'messages/phone_number_tool_tip');?></div></div>
			</label>
			<br />
			<p align="center" style="width:60%">
			<input type='hidden' name='showflag' value="<?=$showflag?>">
			<input type="hidden" name="editUserInfoArr[szOldEmail]" id="szOldEmail" value="<?=$_POST['editUserInfoArr']['szOldEmail'] ? $_POST['editUserInfoArr']['szOldEmail'] : $kUser->szEmail ?>"/>
			<input type="hidden" name="editUserInfoArr[szCountry]" id="szCountry" value="<?=$_POST['editUserInfoArr']['szCountry'] ? $_POST['editUserInfoArr']['szCountry'] : $kUser->szCountry ?>"/>
			<a href="javascript:void(0)" class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');edit_customer_contact_info();"><span><?=t($t_base.'fields/save');?></span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_customer_info(<?=$idCustomer?>);"><span><?=t($t_base.'fields/cancel');?></span></a> </p>
			<input type="hidden" name="id" value="<?=$idCustomer?>">
			<input type="hidden" name="editUserInfoArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="">
		</form>
		</div>
		<br/>
		<br/>
<?php }else {

    $kConfig = new cConfig();
    $kConfig->loadCountry($kUser->idInternationalDialCode);
    $iInternationDialCode = $kConfig->iInternationDialCode;
	?>
<div id="contact_info">
		<h5><strong><?=t($t_base.'messages/contact_info');?></strong> <a href="javascript:void(0)" onclick="edit_customer_contact_information(<?=$idCustomer?>);"><?=t($t_base.'fields/edit');?></a></h5>
		
		<div class="ui-fields" style="height:auto;min-height:26px;">
			<span class="field-name"><?=t($t_base.'fields/email');?></span>
			<span class="field-container" style="margin-bottom:4px;"><?=$kUser->szEmail?>
			 <?php if($kUser->iConfirmed!='1')
			 {?>
                            <span style='color:red;font-size:13px;'>(<?=t($t_base.'messages/not_verified');?>)</span>
                            <a href="javascript:void(0)" onclick="display_verify_popup('<?php echo $kUser->id; ?>','VERIFY_USER_EMAIL');">E-mail verified</a>
			<? } ?>
			</span>		
			 <?php 
			 if($kUser->iConfirmed!='1')
			 { /*
			 	if($successContactUpdatedFlag) {?>
					
					<div style="background: #b6dde8;clear: both;width: 486px;margin:0 0 0 216px;padding:5px 10px 8px;">
					<div id="activationkey" style="display:none";></div>
					<p><?=t($t_base.'messages/click_the_link');?> <?=$_POST['editUserInfoArr']['szEmail']?> <?=t($t_base.'messages/complete_your_account');?></p>
					
					<p><a href="javascript:void(0)" onclick="resend_activation_code('<?=$idCustomer?>');"><?=t($t_base.'messages/resend_email');?></a>
					<br /><!--  <a href="javascript:void(0)" onclick="edit_contact_information();"><?=t($t_base.'messages/change_email');?></a>--></p>
					</div>
			
			<?php } */ }?>
			
		</div>
		<div class="ui-fields">
			<span class="field-name"><?=t($t_base.'fields/contact_by_email');?></span>
			<span class="field-container checkbox-ab"><input type="checkbox" <?php if((int)$kUser->iAcceptNewsUpdate=='1'){?> checked <?php }?> disabled="disabled"/><?=t($t_base.'messages/please_news_updates');?></span>
		</div>
		<div class="ui-fields">
			<span class="field-name"><?=t($t_base.'fields/p_number');?> <span>(<?=t($t_base.'fields/optional');?>)</span></span>
			<span class="field-container"><?php echo  "+".$iInternationDialCode." ".$kUser->szPhoneNumber?></span>
		</div>
	</div>
	<br/>
	<br/>
<?php } if($showflag=='change_pass' && !$successPasswordUpdatedFlag){?>
<div id="user_change_password">
	<?php
	if(!empty($kUser->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kUser->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php }?>
	<form name="changePassword" id="changePassword" method="post">
		<h5><strong><?=t($t_base.'fields/password');?></strong> </h5>		
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/current_password')?></span>
			<span class="field-container"><input type="password" name="editUserInfoArr[szOldPassword]" id="szOldPassword"/></span>
		</label>
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/password');?></span>
			<span class="field-container"><input type="password" name="editUserInfoArr[szNewPassword]" id="szNewPassword" onblur="closeTip('pass');" onfocus="openTip('pass');"/></span>
			<div class="field-alert"><div id="pass" style="display:none;"><?=t($t_base.'messages/password_msg');?></div></div>
		</label>
		
		<label class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/con_new_password')?></span>
			<span class="field-container"><input type="password" name="editUserInfoArr[szConPassword]" id="szConPassword"/></span>
		</label>
		<br />
		<input type='hidden' name='showflag' value="<?=$showflag?>">
		<p align="center" style="width:60%"><a href="javascript:void(0);" class="button1" onclick="changeCustomerPassword();"><span>Save</span></a> <a href="javascript:void(0)" class="button2" onclick="cancel_customer_info(<?=$idCustomer?>);"><span><?=t($t_base.'fields/cancel');?></span></a> </p>
		<input type="hidden" name="id" value="<?=$idCustomer?>">
	</form>
	</div>
<?php }else {?>
<div id="user_change_password">	
	
		<h5 style="width:30%;float:left;"><strong><?=t($t_base.'fields/password');?></strong> </h5>
		<span class="field-container" style="float:left;width:65%;font-style: italic;cursor: pointer;" id="customerSend"><u onclick="send_new_password_customer(<?=$idUser?>)">Send user new password</u></span>					
</div>
<?php } ?> 