<?php 
/**
  *  Admin--- createExplainPage
  */
ob_start();
if (!isset( $_SESSION )) 
{
  session_start();
}
if( !defined("__APP_PATH__") )
define("__APP_PATH__" , realpath ( dirname( __FILE__ ) ."/../"));
require_once (__APP_PATH__ ."/inc/constants.php");
require_once(__APP_PATH_LAYOUT__ ."/ajax_admin_header.php");
validateManagement();
$kExplain = new cExplain();
$t_base="management/explaindata/";
$t_base_error="management/Error/";
$mode = sanitize_all_html_input($_REQUEST['mode']);

if(isset($_POST['explainArr']) && $mode=='PREVIEW_EXPLAIN_PAGE')
{  
    if($kExplain->validate_explain($_POST['explainArr'],'PREVIEW'))
    {		
        echo "SUCCESS";
        $_SESSION['explainArr'] = $_POST['explainArr'] ;
        die;
    }
    else if(!empty($kExplain->arErrorMessages))
    {
        ?>
        <div id="regError" class="errorBox" style="display:block;">
        <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
        <div id="regErrorList">
        <ul>
        <?php
              foreach($kExplain->arErrorMessages as $key=>$values)
              {
              ?><li><?=$values?></li>
              <?php 
              }
        ?>
        </ul>
        </div>
        </div>
        <?php
        die;
    }
}
else if(isset($_POST['explainArr']) && isset($_POST['explainArr']['id']))
{
    if($kExplain->editExplainPage($_POST['explainArr']))
    {
            return true;	 
    }
    else if(!empty($kExplain->arErrorMessages))
    {
        ?>
        <div id="regError" class="errorBox" style="display:block;">
        <div class="header"><?=t($t_base_error.'fields/please_following');?></div>
        <div id="regErrorList">
        <ul>
            <?php  foreach($kExplain->arErrorMessages as $key=>$values) { ?>
                <li><?=$values?></li>
            <?php  } ?>
        </ul>
        </div>
        </div>
        <?php
    }
}
else if(isset($_POST['explainArr']))
{
	if($kExplain->createExplainPage($_POST['explainArr']))
	{
		return true;	
	//header('location:'.__MANAGEMENT_EXPLAIN_URL__);
	}
	else if(!empty($kExplain->arErrorMessages))
	{
		?>
		<div id="regError" class="errorBox" style="display:block;">
		<div class="header"><?=t($t_base_error.'fields/please_following');?></div>
		<div id="regErrorList">
		<ul>
		<?php
		      foreach($kExplain->arErrorMessages as $key=>$values)
		      {
		      ?><li><?=$values?></li>
		      <?php 
		      }
		?>
		</ul>
		</div>
		</div>
		<?php
	}
}
if(isset($_POST['mode']))
{
	$mode = sanitize_all_html_input($_POST['mode']);
	SWITCH ($mode)
	{
		CASE 'CHANGE_ORDER_EXPLAIN':
		{
			$id = (int)$_POST['id'];
			$order = sanitize_all_html_input($_POST['move']);
			$iExplauinType = sanitize_all_html_input($_POST['type']);
			$iSessionLanguage = false;
			if($_SESSION['session_admin_language']>0)
			{
				$iSessionLanguage = $_SESSION['session_admin_language'] ;
			} 
			if($order == 'UP')
			{
				$iOrderBy = __ORDER_MOVE_DOWN__;	
			}
			else if($order == 'DOWN')
			{
				$iOrderBy = __ORDER_MOVE_UP__;
			}
			$kExplain->moveUpDownExplainPagesLinks($id,$iOrderBy,$iExplauinType,$iSessionLanguage);
			$explainLinks = $kExplain->selectPageDetails(false,$iExplauinType,$iSessionLanguage);
			$maxOrder = $kExplain->getMaxOrderExplainData($iExplauinType,$iSessionLanguage);
			viewExplainPage($t_base,$explainLinks,$maxOrder,$iExplauinType);	
			echo "|||||";
			$links = $kExplain->selectPageDetails(false,$iExplauinType,$iSessionLanguage);
			if($links != array())
			{
				foreach($links as $linking)
				{
			?>	
				<li <?php if(isset($_REQUEST['flag']) && ($_REQUEST['flag'] == $linking['szLink'])){?>class="active" <?php }?> ><?=($linking['iActive']==0?'*':'') ?><a href="<?=__MANAGEMENT_EXPLAIN_URL__.$linking['szLink']?>"> <?=$linking['szPageHeading']?></a></li>
			<?php  }
			}
			
			BREAK;
		}
		CASE 'EDIT_EXPLAIN_PAGES':
		{
			$id = (int)$_POST['id'];
			$data = $kExplain->selectPageDetails($id,false,$iSessionLanguage); 
			createNewExplanationPage($t_base,$data[0]);
			BREAK;
		}
		CASE 'DELETE_EXPLAIN_PAGE':
		{
			$id = (int)$_POST['id'];
			echo '<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
						'.t($t_base.'title/are_you_sure_to_delete').'<br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="deleteExplainPageConfirm(\''.$id.'\')" class="button1"><span>'.t($t_base.'fields/ok').'</span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span>'.t($t_base.'fields/cancel').'</span></a>			
						</p>
					</div>
			 	</div>
			</div> 	';
			BREAK;
		}
		CASE 'DELETE_LEVEL2_IMAGE':
		{
			$szFileName= sanitize_all_html_input($_POST['szFileName']);
			$flag= sanitize_all_html_input($_POST['flag']);
			$idExplanPage= sanitize_all_html_input($_POST['idExplanPage']);
			$szImageType = sanitize_all_html_input($_POST['image_type']);
			
			if($idExplanPage>0 && $flag=='main')
			{
				echo '<div id="popup-bg"></div>
				 	<div id="popup-container">	
						<div class="popup contact-popup" style="height: auto;">
							'.t($t_base.'title/are_you_sure_to_delete_image').'<br /><br />
							<p align="center">	
								<a href="javascript:void(0);" onclick="deleteExplainPageImageConfirm(\''.$idExplanPage.'\',\''.$szFileName.'\',\''.$szImageType.'\')" class="button1"><span>'.t($t_base.'fields/ok').'</span></a>			
								<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span>'.t($t_base.'fields/cancel').'</span></a>			
							</p>
						</div>
				 	</div>
				</div> 	';
			}else
			{
				@unlink(__EXPLAIN_LEVEL2_IMAGES__."/".$szFileName);
			}
			BREAK;
		}
		CASE 'DELETE_LEVEL3_IMAGE':
		{
			$szFileName=$_POST['szFileName'];
			$flag=$_POST['flag'];
			$idExplanPage=$_POST['idExplanPage'];
			if($idExplanPage>0 && $flag=='main')
			{
				echo '<div id="popup-bg"></div>
			 	<div id="popup-container">	
					<div class="popup contact-popup" style="height: auto;">
						'.t($t_base.'title/are_you_sure_to_delete_image').'<br /><br />
						<p align="center">	
							<a href="javascript:void(0);" onclick="deleteExplainPageImageLevel3Confirm(\''.$idExplanPage.'\',\''.$szFileName.'\')" class="button1"><span>'.t($t_base.'fields/ok').'</span></a>			
							<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span>'.t($t_base.'fields/cancel').'</span></a>			
						</p>
					</div>
			 	</div>
			</div> 	';
			}else
			{
				@unlink(__EXPLAIN_LEVEL3_IMAGES__."/".$szFileName);
			}
			BREAK;
		}
		CASE 'DELETE_EXPLAIN_PAGE_CONFIRM':
		{
			$id = (int)$_POST['id'];
			$kExplain->deleteExplainPage($id);
		}
		CASE 'CHANGE_EXPLAING_PAGE_LANGUAGE':
		{
			$iLanguage = (int)$_POST['lang_id'] ;
			
			if($iLanguage==1 || $iLanguage==2)
			{
				$_SESSION['session_admin_language'] = $iLanguage ;
			}
			else
			{
				$_SESSION['session_admin_language'] = 1 ;
			}
			echo "SUCCESS||||";
		}
	}

}
