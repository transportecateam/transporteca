<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szMetaTitle = "Transporteca | Page not found";
require_once(__APP_PATH_LAYOUT__."/header_new.php");
pageNotFound();
  require_once(__APP_PATH_LAYOUT__."/footer.php");
?>	