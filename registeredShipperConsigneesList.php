<?php
 ob_start();
session_start();
$page_title="My Account - Registered Shippers";
$szMetaKeywords = "My Account - Registered Shippers ...";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$kRegisterShipCon = new cRegisterShipCon();
$t_base = "Users/ShipperConsigness/";
$deleteflag=false;

if(!empty($_POST['removeArr']))
{
	if($kRegisterShipCon->removeRegShipperConsigneess($_POST['removeArr']))
	{
		$deleteflag=true;	
	}
}
if($_REQUEST['remove']=='1')
{
	
	?>
<div id="remove_ship_con_id">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup remove-shippers-popup">

	<?php
	$heading='';
	if(count($_REQUEST['regShipper'])>0)
	{
		$shipperConsignFlag=false;
		$remove_arr=implode(",",$_REQUEST['regShipper']);
		
		if(!empty($_REQUEST['regShipper']))
		{
			$regShipper=$_REQUEST['regShipper'];
			for($i=0;$i<count($regShipper);$i++)
			{
				$kRegisterShipCon->getShipperConsignessDetail($regShipper[$i]);
				$Type=$kRegisterShipCon->idGroup;
				if($Type==3)
				{
					$shipperConsignFlag=true;
				}
			}
		}
		
		//$remove_msg=t($t_base.'messages/shipper');
		
		if(count($_REQUEST['regShipper'])==1)
		{
			$heading=t($t_base.'messages/remove_shipper');
			$remove_msg=t($t_base.'messages/shipper');
		}
		else
		{
			$total=count($_REQUEST['regShipper']);
			$heading=t($t_base.'messages/remove_shippers');
			$remove_msg=t($t_base.'messages/shippers');
		}
	}
	if(count($_REQUEST['regCon'])>0)
	{
		$shipperConsignFlag=false;
		$remove_arr=implode(",",$_REQUEST['regCon']);
		
		if(!empty($_REQUEST['regCon']))
		{
			$regCon=$_REQUEST['regCon'];
			for($i=0;$i<count($regCon);$i++)
			{
				$kRegisterShipCon->getShipperConsignessDetail($regCon[$i]);
				$Type=$kRegisterShipCon->idGroup;
				if($Type==3)
				{
					$shipperConsignFlag=true;
				}
			}
		}
		//echo $Type;
		if(count($_REQUEST['regCon'])==1)
		{
			$heading=t($t_base.'messages/remove_consignee');
			$remove_msg=t($t_base.'messages/consignee');
		}
		else
		{
			$total=count($_REQUEST['regCon']);
			$heading=t($t_base.'messages/remove_consignees');
			$remove_msg=t($t_base.'messages/consign');
		}
	}
	?>
	<form name="remove_confirm" id="remove_confirm" method="post">
	<h5><?=$heading?></h5>
	<p><?=t($t_base.'messages/would_you_like');?> <?=$total?> <?=t($t_base.'messages/selected');?> <?=$remove_msg?>?
	<?php 
		if($shipperConsignFlag)
		{
			echo "<br/><br>".t($t_base.'messages/reg_shipper_consignees_removed_both');
		}
	?>
	</p>
	<input type="hidden" name="removeArr" id="removeArr" value="<?=$remove_arr?>">
	<br />
	<p align="center"><a href="javascript:void(0)" class="button1" onclick="remove_shipper_con_data();"><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_remove_popup();"><span><?=t($t_base.'fields/cancel');?></span></a></p>
	</form>
</div>
</div>
</div>
<?php	
}
if((int)$kUser->id>0)
{
	$userMutliArr=$kUser->getMultiUserData($kUser->idGroup);
	if(!empty($userMutliArr))
	{
		foreach($userMutliArr as $userMutliArrs)
		{
			$idUserArr[]=$userMutliArrs['id'];
		}
	}
}
if(!empty($idUserArr))
{
	$userStr=implode(",",$idUserArr);
}
else
{
	$userStr=$kUser->id;
}

$regShipperArr=$kRegisterShipCon->getRegisteredShipperConsigness('ship',$userStr);
$regConArr=$kRegisterShipCon->getRegisteredShipperConsigness('con',$userStr);
$idRegShipperConsigness=$_REQUEST['idRegShipperConsigness'];
if($idRegShipperConsigness>0)
{
	$kRegisterShipCon->getShipperConsignessDetail($idRegShipperConsigness);
	$regType=$kRegisterShipCon->idGroup;
	if($regType=='1')
	{
		$msg=t($t_base.'messages/update_success_register_shipper');
	}
	else if($regType=='2')
	{
		$msg=t($t_base.'messages/update_success_register_consigness');
	}
	else if($regType=='3')
	{
		$msg=t($t_base.'messages/update_success_register_shipper_consigness');
	}
}
 /* We are no longer displaying this success message
if($deleteflag)
{ 
   
?>
<div id="regError" class="errorBox" style="background-color: #DCF9C9; border: 1px solid #71A250;">
		<div id="regErrorList">
			<ul>
				<li><?=t($t_base.'messages/remove_success_register_consigness')?></li>
			</ul>
		</div>
	</div>
<?php
 * *
 
}
/*if($_REQUEST['successflag']=='add')
{
	echo "<p style=color:green>".t($t_base.'messages/add_success_register_consigness')."</p>";
}
if($_REQUEST['successflag']=='update')
{
	?>
	<div id="regError" class="errorBox" style="background-color: #DCF9C9; border: 1px solid #71A250;">
		<div id="regErrorList">
			<ul>
				<li><?=$msg?></li>
			</ul>
		</div>
	</div>
	<?
}
  * 
  */
?>
<form name="removeShipperConsigness" id="removeShipperConsigness" method="post">
	<p class="f-size-22"><?=t($t_base.'title/register_shipper_consigness_available_booking');?></p>
	<br />
	<div class="oh registered-textarea">
		<div class="fl-49">
			<h2 class="create-account-heading"><?=t($t_base.'fields/register_shipper');?><a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/shipper_msg');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop')">&nbsp;</a></h2>
			<p>
				<select multiple="multiple" name="regShipper[]" id="regShipper" onDblClick="get_shipper_consigness_detail(this.value);" onClick="remove_value_select_box('shipper');">
				<?php
					if(!empty($regShipperArr)){
						foreach($regShipperArr as $regShipperArrs)
						{
						?>
							<option value="<?=$regShipperArrs['id']?>" ><?=$regShipperArrs['szCompanyName']?> (<?=$regShipperArrs['szCountryName']?>)</option>
						<? }
					}
					?>
				</select>
			</p>
		</div>
		<div class="fl-49 consignee">
			<h2 class="create-account-heading"><?=t($t_base.'fields/register_consignees');?><a href="javascript:void(0)" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/consigness_msg');?>','signin_help_pop',event);" onmouseout="hide_tool_tip('signin_help_pop')">&nbsp;</a></h2>
			<p>
				<select multiple="multiple" name="regCon[]" id="regCon" onDblClick="get_shipper_consigness_detail(this.value);" onClick="remove_value_select_box('consignes');">
				<?php
					if(!empty($regConArr)){
						foreach($regConArr as $regConArrs)
						{?>
							<option value="<?=$regConArrs['id']?>" ><?=$regConArrs['szCompanyName']?> (<?=$regConArrs['szCountryName']?>)</option>
						<? }
					}
					?>
				</select>
			</p>
		</div>
	</div>
	
	<div class="oh">
		<input type="hidden" name="remove" value="1">
		<p class="fl-80 f-size-12"><em>* <?=t($t_base.'messages/registered_another_multi_user_access');?> </em></p>
		<p class="fl-20" align="right"><a href="javascript:void(0)" class="button1" id="remove" style="opacity:0.4;" onclick=""><span><?=t($t_base.'fields/remove');?></span></a></p>
	</div>
</form>