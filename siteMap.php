<?php
ob_start();
session_start();
$szMetaTitle= __SITE_MAP_META_TITLE__;
$szMetaKeywords =  __SITE_MAP_PAGE_META_KEYWORDS__;
$szMetaDescription = __SITE_MAP_PAGE_META_DESCRIPTION__;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
$sitemapFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$kRss = new cRSS();
$rssFeedsAry = array();
$rssFeedsAry = $kRss->getAllRssFeedsHtml();
$t_base="siteMap/";

$iLanguage = getLanguageId();

$kExplain = new cExplain();
$kSEO = new cSEO();

//getting all active and published landingpages
//$landingPageAry = $kExplain->getLandingPageDataByUrl(false,false,true,$iLanguage);
$landingPageAry = $kExplain->getAllNewLandingPageData(false,false,false,$iLanguage,true);

//getting all active and published seopages
//$seoPageAry = $kSEO->getAllPublishedSeoPages($iLanguage);

//getting all active explainpages
//$explainPageAry = $kExplain->selectPageDetailsUserEnd($iLanguage);

//getting all active blog pages
$blogPageAry = $kExplain->selectBlogDetailsActive($iLanguage);

//getting sitemap video
//$sitemapVideoAry = array();
//$sitemapVideoAry = $kSEO->getSitemapVideo();
//echo __HOME_PAGE_URL__; 


$kExplain = new cExplain();

    $iLanguage = getLanguageId();
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('',$iLanguage,'','','','','',false);
    $szLanguageCode=  strtolower($languageArr[0]['szLanguageCode']);
    
    $menuRightFooter=__ENGLISH_RIGHT_FOOTER_MENU__."".strtoupper($szLanguageCode);
    
    $menurightarr=wp_get_nav_menu_items($menuRightFooter);
    //print_r($menuarr);
    $menurightarr=(toArray($menurightarr));
    if(empty($menurightarr))
    {
        $menurightarr=wp_get_nav_menu_items(__ENGLISH_RIGHT_FOOTER_MENU__);
        $menurightarr=(toArray($menurightarr));
    }
    
//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    {
//        $szLanguageCode=__DANISH_CODE_WP__;
//    }else{
//        $szLanguageCode=__ENGLISH_CODE_WP__;
//    }
    $contentFolder='';
    if(__ENVIRONMENT__ =='LIVE'){ 
        $contentFolder="/content";
    }

    $mainMenuArr=$kExplain->getMainMenuParentData($szLanguageCode,true,false);
    
    $seoPageAry=$kExplain->getMainMenuParentData($szLanguageCode,true,true);
    
?>
<div id="hsbody">
	<h1><?php echo t($t_base.'fields/thanks_for_visiting_our_sitemap');?></h1><br/>
	<div class="hsbody-2 clearfix">
		<!--<div class="sitemaps_left">--19-sep-2016-->					
			<!--  <div class="first common">
				<h5><?php echo t($t_base.'fields/search_service');?></h5>
				<p><a href="<?php echo __REQUIREMENT_PAGE_URL__;?>"><?php echo t($t_base.'fields/requirement_wizard');?></a></p>
				<p><a href="<?php echo __LANDING_PAGE_URL__;?>"><?php echo t($t_base.'fields/instant_search');?></a></p>
				<p><a href="<?php echo __CREATE_ACCOUNT_PAGE_URL__;?>"><?php echo t($t_base.'fields/create_account');?></a></p>
				<br />
			</div>
			<div class="common">
				<h5><?php echo t($t_base.'fields/about_us');?></h5>
				<p><a href="<?=__OUR_COMPANY_PAGE_URL__?>/"><?php echo t($t_base.'fields/our_company');?></a></p>
				<p><a href="<?=__STARTING_NEW_TRADE_PAGE_URL__?>/"><?php echo t($t_base.'fields/starting_new_trades');?></a></p>
				<br />
			</div>-->
			<!--<div class="first common">
				<h5><?php echo t($t_base.'fields/for_forwarders');?></h5>--19-sep-2016-->
				<!--<p><a href="<?=__HOW_IT_WORK_PAGE_URL__?>"><?php echo t($t_base.'fields/how_it_works');?></a></p>-->
				<!--<p><a href="<?=__FORWARDER_SIGNUP_PAGE_URL__?>"><?php echo t($t_base.'fields/sing_up');?></a></p>
				<br />
			</div>
			<div class="common">
				<h5><?php echo t($t_base.'fields/for_partners');?></h5>
				<p><a href="<?=__PARTNER_PROGRAM_PAGE_URL__?>"><?php echo t($t_base.'fields/partner_program');?></a></p>
				<br />
			</div>
			<div class="common">
				<h5><?php echo t($t_base.'fields/media');?></h5>--19-sep-2016-->
				<!--  <p><a href="<?php echo __BLOG_PAGE_URL__;?>"><?php echo t($t_base.'fields/blog');?></a></p>-->
				<!--<p><a href="<?php echo __RSS_FEED_PAGE_URL__;?>"><?php echo t($t_base.'fields/news');?></a></p>
				<br />
			</div>--19-sep-2016-->
			<!--<div class="common">
				<h5><?php echo t($t_base.'fields/general');?></h5>
                                <?php
                                    if(!empty($menurightarr))
                                    {
                                        foreach($menurightarr as $menurightarrs)
                                        {?>
                                            <p><a href="<?php echo $menurightarrs['url'];?>" ><?=$menurightarrs['title'];?></a></p>
                                           <?php 
                                        }
                                    }?>--19-sep-2016-->
<!--				<p><a href="<?=__TERMS_CONDITION_PAGE_URL__?>"><?php echo t($t_base.'fields/terms_condition');?></a></p>
				<p><a href="<?=__PRIVACY_NOTICE_PAGE_URL__?>"><?php echo t($t_base.'fields/privacy_notioce');?></a></p>
				<p><a href="<?php echo __FAQ_PAGE_URL__;?>"><?php echo t($t_base.'fields/faq');?></a></p>-->
			<!--</div>
			<br> 
		</div>--19-sep-2016-->
                    <div class="sitemaps_left">
                        <div class="first common">  
                            <h5><?php echo t($t_base.'fields/information');?></h5>
				<?php  
					if(!empty($seoPageAry))
					{
                                            foreach($seoPageAry as $seoPageArys)
                                            {
                                               
                                                    $mainPageUrl='';
                                                    $mainPageUrl=$mainMenuArrs['post_name'];
                                                    $howDoesworksLinks=$kExplain->getWordpressExplainPageDataForMenu($seoPageArys['ID']);
                                                    if(!empty($howDoesworksLinks))
                                                    {
                                                        foreach($howDoesworksLinks as $howDoesworksLinkss)
                                                        {
                                                            if($howDoesworksLinkss['post_title']!=''){
                                                                $redir_da_url='';
                                                                $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'_custom_permalink',true);
                                                                if(empty($redir_da_url))
                                                                {
                                                                   $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'custom_permalink',true); 
                                                                }
                                                                if($redir_da_url!='')
                                                                {
                                                                    $redir_da_url= __MAIN_SITE_HOME_PAGE_URL__.'/'.$redir_da_url;
                                                                }
                                                                else
                                                                {
                                                                    $redir_da_url = get_page_link( $howDoesworksLinkss['id'] );
                                                                }
                        
                                                                if($_REQUEST['menuflag'] == $howDoesworksLinkss['szLink'])
                                                                {  
                                                                    $szClass = 'class="active-triangle"';
                                                                    $szUrl = $redir_da_url;
                                                                    										
                                                                }
                                                                else 
                                                                {
                                                                    $szClass = 'class="triangle" ' ;
                                                                    $szUrl = $redir_da_url;
                                                                    
                                                                }
                                                                ?>
                                                                    <p><strong><a href="<?php echo $szUrl; ?>" title="<?php echo $szUrl; ?>"> <?=$howDoesworksLinkss['post_title']?></a></strong></p>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    
                                            }
					} 
					if(!empty($blogPageAry))
					{
                                            foreach($blogPageAry as $blogPageArys)
                                            {
                                                ?>
                                                <p><a href="<?php echo __BLOG_PAGE_URL__.$blogPageArys['szLink'] ?>" title="<?php echo $blogPageArys['szLinkTitle']; ?>"><?php echo $blogPageArys['szHeading']?></a></p>
                                                <?php 
                                            }
					}
				?>
			</div>
                    </div>
		<div class="sitemaps_right">
			<div class="clearfix">					
			
			
			<div class="first common">
                            <h5><?php echo t($t_base.'fields/welcome_page');?></h5>
                            <?php 
                                if(!empty($landingPageAry))
                                {
                                    foreach($landingPageAry as $landingPageArys)
                                    {
                                        /*
                                        * We have excluded following 2 pages from sitemap.xml 
                                        * 
                                        * 14. /labels/
                                        * 15. /da/labels/
                                        */ 
                                       if($landingPageArys['id']==14 || $landingPageArys['id']==15)
                                       {
                                           continue;
                                       }
                                       else
                                       {
                                            if($landingPageArys['szUrl']=='/')
                                            {
                                                $szLandingPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
                                            }
                                            else if(!empty($landingPageArys['szUrl']))
                                            {
                                                $szLandingPageUrl =__MAIN_SITE_HOME_PAGE_SECURE_URL__.'/'.$landingPageArys['szUrl'];  
                                            }
                                            else
                                            {
                                                $szLandingPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
                                            }
                                            ?>
                                            <p><a href="<?php echo $szLandingPageUrl; ?>" ><?php echo $landingPageArys['szSeoKeywords'] ;?></a></p>
                                            <?php 
                                       }
                                    }
                                }
                            ?>
			</div>
                            <?php
                            $countarr=count($mainMenuArr);
                            if(!empty($mainMenuArr))
                            {?>   

                                <?php
                                require(__WORDPRESS_FOLDER__.'/wp-config.php');
                                $i=0;
                                $t=1;
                                foreach($mainMenuArr as $mainMenuArrs)
                                {
                                    
                                    if($i==0){
                                    ?>
                                    <div class="common">
                                    <?php }?> 
                                        <h5><?=$mainMenuArrs['post_title'];?></h5>
                                    <?php
                                    $mainPageUrl='';
                                    $mainPageUrl=$mainMenuArrs['post_name'];
                                    $howDoesworksLinks=$kExplain->getWordpressExplainPageDataForMenu($mainMenuArrs['ID']);
                                    if(!empty($howDoesworksLinks))
                                    {
                                        foreach($howDoesworksLinks as $howDoesworksLinkss)
                                        {
                                            if($howDoesworksLinkss['post_title']!=''){
                                                $redir_da_url='';
                                                $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'_custom_permalink',true);
                                                if(empty($redir_da_url))
                                                {
                                                   $redir_da_url = get_post_meta($howDoesworksLinkss['id'],'custom_permalink',true); 
                                                }
                                                if($redir_da_url!='')
                                                {
                                                    $redir_da_url= __MAIN_SITE_HOME_PAGE_URL__.'/'.$redir_da_url;
                                                }
                                                else
                                                {
                                                    $redir_da_url = get_page_link( $howDoesworksLinkss['id'] );
                                                }
                                                if($_REQUEST['menuflag'] == $howDoesworksLinkss['szLink'])
                                                {
                                                    $szClass = 'class="active-triangle"';
                                                    $szUrl = $redir_da_url;
                                                    									
                                                }
                                                else 
                                                {
                                                    $szClass = 'class="triangle" ' ;
                                                    $szUrl = $redir_da_url;
                                                }
                                                ?>
                                                    <p><strong><a href="<?php echo $szUrl; ?>" title="<?php echo $szUrl; ?>"> <?=$howDoesworksLinkss['post_title']?></a></strong></p>
                                                <?php
                                            }
                                        }
                                    }
                                    if($i==1 || $t==$countarr){
                                        
                                    ?>
                                        </div>
                                        <?php
                                    } 
                                    ++$i;
                                    if($i==2)
                                    {
                                       
                                        $i=0;
                                    }
                                    else
                                    {
                                         echo "<br />";
                                    }
                                    ++$t;
                                    
                                }
                            }?>
			
			
		</div>
	</div>
</div>
</div>
<?php
	include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>