<?php
/**
 * How Forwarder Sorted PopUp
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );
$t_base = "BookingDetails/";
$serviceType=$_REQUEST['servicetype'];
if($serviceType=='1')
{
	$serviceType=t($t_base.'messages/d_t_d');
	$sorting=t($t_base.'messages/d_t_d_sort_msg');
}
else if($serviceType=='2')
{
	$serviceType=t($t_base.'messages/d_t_w');
	$sorting=t($t_base.'messages/d_t_w_sort_msg');
}
else if($serviceType=='3')
{
	$serviceType=t($t_base.'messages/w_t_d');
	$sorting=t($t_base.'messages/w_t_d_sort_msg');
}
else if($serviceType=='4')
{
	$serviceType=t($t_base.'messages/w_t_w');
	$sorting=t($t_base.'messages/w_t_w_sort_msg')." - ".t($t_base.'messages/w_t_w_sort_msg_2');
}
?>
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification" style="width:400px;">
<p class="close-icon" align="right">
<a onclick="cancel_remove_user_popup('forwarder_sorted_popup');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<h5><?=t($t_base.'fields/how_is_this_sorted')?></h5>
<p><?=t($t_base.'messages/how_we_sort_msg')?>  <?=t($t_base.'messages/how_we_sort_msg_1')?></p><p> <?=t($t_base.'messages/how_we_sort_msg_2')?></p><br>
<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancel_remove_user_popup('forwarder_sorted_popup');"><span><?=t($t_base.'fields/close');?></span></a></p>
</div>
</div>