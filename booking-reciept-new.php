<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/booking_functions.php" );
require_once( __APP_PATH_CLASSES__.'/nps.class.php');
///$display_abandon_message = true;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$szMetaTitle = __BOOKING_RECEIPT_META_TITLE__ ;
$szMetaKeywords = __BOOKING_RECEIPT_META_KEYWORDS__;
$szMetaDescription = __BOOKING_RECEIPT_META_DESCRIPTION__;
$headerSearchFlag=1; 
//$_SESSION['booking_id'] = 5085;
$kBooking = new cBooking();  
$postSearchAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']); 
$fTotalVatUSD='0.0';
$fTotalVat=$postSearchAry['fTotalVat'];
if((float)$fTotalVat>0.00)
{
    $szVatCurrency=$postSearchAry['szCustomerCurrency'];
    $exhangeRateVatValue='1.000'; 
}
if($postSearchAry['idCustomerCurrency']=='USD')
{
    $fTotalVatUSD=$fTotalVat;
}
else
{  
    $fTotalVatUSD=$fTotalVat*$postSearchAry['fCustomerExchangeRate'];
}
$fInsuranceRevenueUSD="0.00";
if($postSearchAry['iInsuranceIncluded']=='1' && $postSearchAry['iInsuranceStatus']!='3' && $postSearchAry['iTransferConfirmed']==0 && ($postSearchAry['idBookingStatus']==3 || $postSearchAry['idBookingStatus']==4))
{
    $fTotalInsuranceCostForBookingUSD=$postSearchAry['fTotalInsuranceCostForBookingUSD'];
    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] * $postSearchAry['fCustomerExchangeRate'];
    
    $fTotalPriceUSD = $fTotalPriceCustomerCurrency+$fTotalInsuranceCostForBookingUSD;
    
   $fInsuranceRevenueUSD=$postSearchAry['fTotalInsuranceCostForBookingUSD']-$postSearchAry['fTotalInsuranceCostForBookingUSD_buyRate'];
}
else 
{
    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] * $postSearchAry['fCustomerExchangeRate']; 
    $fTotalPriceUSD = $fTotalPriceCustomerCurrency;
}
$kCourierService = new cCourierServices();
$exhangeRate=$kCourierService->getExchangeRateCourierPrice(__DANISH_CURRENCY_CODE__);
$fReferalAmountUSD="0.00";
$fInsuranceRevenueDKK="0.00";
$fBookingLabelFeeRateUSD="0.00";
$fBookingLabelFeeRateDKK="0.00";
if((float)$exhangeRate>0.00)
{
    $fReferalAmountUSD=$postSearchAry['fReferalAmount']*$postSearchAry['fForwarderExchangeRate'];
    $fReferalAmountDKK=$fReferalAmountUSD/$exhangeRate;
    $fTotalPriceDKK=$fTotalPriceUSD/$exhangeRate;
    $fTotalVatDKK=$fTotalVatUSD/$exhangeRate;
    $fInsuranceRevenueDKK=$fInsuranceRevenueUSD/$exhangeRate;
    if($postSearchAry['iCourierBooking']==1)
    {
        $courierBookingArr=$kBooking->getCourierBookingDetails($_SESSION['booking_id']);
        if($courierBookingArr['iCourierAgreementIncluded']==0)
        {
            $fBookingLabelFeeRateUSD=round((float)($courierBookingArr['fBookingLabelFeeRate']*$courierBookingArr['fBookingLabelFeeROE']));
            
            $fBookingLabelFeeRateDKK=$fBookingLabelFeeRateUSD/$exhangeRate;
        }
    }
    
    $fHandlingFeeDKK = 0;
    if($postSearchAry['iHandlingFeeApplicable']==1)
    { 
        if($postSearchAry['idHandlingFeeLocalCurrency']==20)//DKK
        {
            $fHandlingFeeDKK = $postSearchAry['fTotalHandlingFeeLocalCurrency'];
        }
        else
        {
            $fHandlingFeeDKK = round((float)($postSearchAry['fTotalHandlingFeeUSD']/$exhangeRate),2);
        }
    }
    $fReferalAmountDKK = $fReferalAmountDKK + $fHandlingFeeDKK +$fInsuranceRevenueDKK+$fBookingLabelFeeRateDKK;
    
    $fReferalAmountUSD =$fReferalAmountDKK * $exhangeRate;
    $fReferalAmountUSD = round((float)$fReferalAmountUSD,2);
}
 $fTotalPriceUSDForFBPixel=$fReferalAmountUSD;
 
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingReceipt/";

$kConfig = new cConfig(); 
//$_SESSION['booking_id'] = 1022;
if((int)$_SESSION['booking_id']<=0)
{
    echo "Opppsss! It seems your session has expired. ";
    //$redirect_url = __HOME_PAGE_URL__ ;
    //header("Location:".$redirect_url);
    //die;
}


 
if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__) 
{ 
    //Only in case of Bank Transfer we send E-mail from this page. 

    $kForwarderContact= new cForwarderContact();
    $kUser = new cUser();
    $kWHSSearch = new cWHSSearch();
    confirmBookingForBankTransfer($postSearchAry); 
}

$postSearchAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']); 
 
if($postSearchAry['iQuotesStatus']==3 || $postSearchAry['iQuotesStatus']==5)
{
    $iBookingQuotes = 1;
}
else
{
    $iBookingQuotes = 0;
}

$kForwarder = new cForwarder();
$customerServiceEmailAry = array();
$idForwarder = $postSearchAry['idForwarder'] ;
$customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder); 
$szWareHouseFromStr = '';
$szWareHouseMapStr = ''; 

if($postSearchAry['szPaymentStatus']=='Completed')
{
    $iLanguage = getLanguageId();
    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
    {
        $szBankTransferDueDateTime = strtotime($postSearchAry['dtCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);

        $iMonth = date('m',$szBankTransferDueDateTime); 
        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);  

        $szMonthName = getMonthName($iLanguage,$iMonth);
        $szBankTransferDueDateTime .=" ".$szMonthName ; 
    }
    else
    {
        $szBankTransferDueDateTime = strtotime($postSearchAry['dtWhsCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);

        $iMonth = date('m',$szBankTransferDueDateTime); 
        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);   

        $szMonthName = getMonthName($iLanguage,$iMonth);
        $szBankTransferDueDateTime .=" ".$szMonthName ;
    }  
	
    $dtAvailableDateTime = strtotime($postSearchAry['dtAvailable']) ;
    $dtAvailableString = date('j.',$dtAvailableDateTime); 

    $iAvailableMonth = date('m',$dtAvailableDateTime); 
    $szAvailableMonthName = getMonthName($iLanguage,$iAvailableMonth);
    $dtAvailableString .=" ".$szAvailableMonthName ;

    $idCurrency = $postSearchAry['idCustomerCurrency'];
    $currencyDetailsAry = array();
    $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency); 

    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $iBookingQuotes==1)
    {
        $idWarehouseFromCountry = $postSearchAry['idShipperCountry'];
    }
    else
    {
        $idWarehouseFromCountry = $postSearchAry['idShipperCountry_pickup'];
    }

    $kConfig->loadCountry($idWarehouseFromCountry);
    $szCountryName = $kConfig->szCountryName ;

    if($postSearchAry['iInsuranceIncluded']==1)
    {
        $fTotalPriceCustomerCurrency = ($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']); 
    }
    else
    {
        $fTotalPriceCustomerCurrency =  $postSearchAry['fTotalPriceCustomerCurrency'];
    }
    
    if($iBookingQuotes==1)
    {
        //$fTotalPriceCustomerCurrency += $postSearchAry['fTotalVat'] ;
    }
    $fTotalPriceCustomerCurrency += $postSearchAry['fTotalVat'] ;
    
//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    { 
//        $szBookingAmountStr =  $postSearchAry['szCurrency']." ". number_format((float)$fTotalPriceCustomerCurrency,0,'.','.');
//    }
//    else
//    { 
//        $szBookingAmountStr = $postSearchAry['szCurrency']." ".number_format((float)$fTotalPriceCustomerCurrency,0,'.',',');
//    }
    
    $szBookingAmountStr = $postSearchAry['szCurrency']." ".  number_format_custom((float)$fTotalPriceCustomerCurrency,$iLanguage,0);
?> 
<div id="hsbody-2"> 
	<div class="hsbody-right">
            <h2 class="thank-you-page-heading"><?=t($t_base.'fields/your_booking_recieved');?></h2>
            <div class="booking-reciept-main-container clearfix" id="booking_details_container">
                <div class="pdf-image-container">		
                <?php if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__){ ?>		
                    <a href="<?php echo __BASE_URL__."/viewInvoice/".$postSearchAry['id']."/"; ?>" target="_blank"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/invoice-icon.png"; ?>"></a>
                <?php } else { ?>
                    <a href="<?php echo __BASE_URL__."/viewInvoice/".$postSearchAry['id']."/"; ?>" target="_blank"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/invoice-icon.png"; ?>"></a>
                    <a href="<?php echo __BASE_URL__."/viewBookingConfirmation/".$postSearchAry['id']."/"; ?>"  target="_blank"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/booking-icon.png"; ?>"></a>
                <?php } ?> 
                </div>
                <div class="oh even first icons">
                    <p><?php echo t($t_base.'fields/booking');?> - <?php echo t($t_base.'fields/your_details_recieved');?></p>
                </div>
                <div class="oh even icons">
                    <p><?php echo t($t_base.'fields/invoice');?> - <?php echo t($t_base.'fields/sent_to')." ".$postSearchAry['szEmail'];?></p>
                </div>
                <?php if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__) { $class="even"; ?>
                        <div class="oh odd">
                            <?php if($postSearchAry['iCourierBooking']==1)
                            {?>
                            <p><?php echo t($t_base.'fields/payment')." - ".t($t_base.'fields/please_make_transfer_courier')." ".$szBookingAmountStr." ".t($t_base.'fields/to_our_account_courier'); ?></p>
                              <?php  
                            }else{?>
                            <?php if($iBookingQuotes==1){ ?>
                                <p><?php echo t($t_base.'fields/payment')." - ".t($t_base.'fields/please_make_transfer')." ".$szBookingAmountStr." ".t($t_base.'fields/to_our_account'); ?></p>
                            <?php } else { ?>
                                <p><?php echo t($t_base.'fields/payment')." - ".t($t_base.'fields/please_make_transfer')." ".$szBookingAmountStr." ".t($t_base.'fields/payment_latest_by')." ".$szBankTransferDueDateTime; ?></p>
                            <?php } }?>
                            <div class="bank-details-box">
                                <p class="clearfix"><span class="left"><?php echo t($t_base.'fields/bank').": "; ?></span><span><?php echo $currencyDetailsAry['szBankName']; ?></span></p>
                                <?php if(!empty($currencyDetailsAry['szSortCode'])){?>
                                    <p class="clearfix"><span class="left"><?php echo t($t_base.'fields/sort_code').": "; ?></span><span><?php echo $currencyDetailsAry['szSortCode']; ?></span></p>
                                <?php } ?>
                                <p class="clearfix"><span class="left"><?php echo t($t_base.'fields/account_number').": "; ?></span><span><?php echo $currencyDetailsAry['szAccountNumber']; ?></span></p>
                                <p class="clearfix"><span class="left"><?php echo t($t_base.'fields/swift').": "; ?></span><span><?php echo $currencyDetailsAry['szSwiftNumber']; ?></span></p>
                                <p class="clearfix"><span class="left"><?php echo t($t_base.'fields/name_on_account').": "; ?></span><span><?php echo $currencyDetailsAry['szNameOnAccount']; ?></span></p>
                                <p class="clearfix"><span class="left"><?php echo t($t_base.'fields/reference_on_transfer').": "; ?></span><span><?php echo $postSearchAry['szBookingRef']; ?></span></p>						
                            </div>
                        </div>
                        <div class="oh even">
                            <?php if($iBookingQuotes==1) {?>
                                <p><?php echo t($t_base.'fields/confirmation');?> - <?php echo t($t_base.'fields/we_send_booking_confirmation')." ".$postSearchAry['szEmail']."".t($t_base.'fields/when_payment_recieved');?></p>
                            <?php } else { ?>
                                <p><?php echo t($t_base.'fields/confirmation');?> - <?php echo t($t_base.'fields/we_send_booking_confirmation')." ".$postSearchAry['szEmail'];?></p>
                            <?php } ?>
                        </div>
                <?php } else { $class = 'odd'; ?>
                        <div class="oh even">
                            <p><?php echo t($t_base.'fields/payment')." - ".t($t_base.'fields/completed'); ?></p>
                        </div>
                <?php } ?>
                <?php 
                    if($postSearchAry['iCourierBooking']==1)
                    {
                        ?>
                        <div class="oh <?php echo $class; ?>">
                            <p><?php echo t($t_base.'fields/departure');?> - <?php echo t($t_base.'fields/courier_service_departure');?></p>
                        </div>
                    <?php
                    }
                    else if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__)
                    {
                        ?>
                        <div class="oh <?php echo $class; ?>">
                            <p><?php echo t($t_base.'fields/departure');?> - <?php echo t($t_base.'fields/forwarder_will_recieve')." ".$szCountryName;?></p>
                        </div>
                        <?php
                    }else if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__){?>			
                        <div class="oh <?php echo $class; ?>">
                            <p><?php echo t($t_base.'fields/pick-up');?> - <?php echo t($t_base.'fields/forwarder_will_collect')." ".$szCountryName; ?></p>
                        </div>
                <?php }else{ ?>
                        <div class="oh <?php echo $class; ?>">
                            <p><?php echo t($t_base.'fields/departure');?> - <?php echo t($t_base.'fields/forwarder_will_recieve')." ".$szCountryName;?></p>
                        </div>
                <?php } ?>
                <div class="oh even last">
                    <?php 
                    if($postSearchAry['iCourierBooking']==1)
                    {
                        ?>
                        <p><?php echo t($t_base.'fields/tracking');?> - <?php echo t($t_base.'fields/we_send_shipment'); ?></p>
                        <?php
                    }
                    else if($iBookingQuotes==1){ ?>
                        <?php if($postSearchAry['idServiceTerms']==__SERVICE_TERMS_EXW_WTD__ && !empty($postSearchAry['dtAvailable']) && $postSearchAry['dtAvailable']!='0000-00-00 00:00:00'){ ?>			
                        <p><?php echo t($t_base.'fields/delivery');?> - <?php echo t($t_base.'fields/receive_your_shipment')." ".$dtAvailableString ; ?></p>
                        <?php } else if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_WTD__){?>			
                            <p><?php echo t($t_base.'fields/delivery');?> - <?php echo t($t_base.'fields/within')." ".$postSearchAry['iTransitHours']." ".t($t_base.'fields/days') ; ?></p>
                        <?php } else { ?>
                            <p><?php echo t($t_base.'fields/arrival');?> - <?php echo t($t_base.'fields/within')." ".$postSearchAry['iTransitHours']." ".t($t_base.'fields/days') ; ?></p>
                        <?php } ?>
                    <?php } else { 
                            if(($postSearchAry['iShipperConsignee']==1 || $postSearchAry['iShipperConsignee']==3) && $postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__){
                            ?>
                            <p><?php echo t($t_base.'fields/delivery');?> - <?php echo t($t_base.'fields/receive_delivery_text')." ".$dtAvailableString ; ?></p>
                    <?php }else 
                        if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__){
                          ?>
                            <p><?php echo t($t_base.'fields/delivery');?> - <?php echo t($t_base.'fields/receive_delivery_text')." ".$dtAvailableString ; ?></p>
                        <?php  
                            
                        }else{
                            ?>
                            <p><?php echo t($t_base.'fields/delivery');?> - <?php echo t($t_base.'fields/receive_your_shipment')." ".$dtAvailableString ; ?></p>
                            <?php
                        }
                    
                    }?>
                </div>
            </div>	 
	</div>
	<?php
	$kNPS = new cNPS();
	if(!$kNPS->getCustomerFeedBackDay($_SESSION['user_id']))
	{
		?>
		<div class="booking-feedback-container" id="booking-feedback-container-div">			
			<?php 
				echo customer_feedback_form();
			?>
		</div>
<?php } else {  ?>
<script type="text/javascript">
	$("#header-search").attr('style','top: 0px; bottom: auto; display: block;'); 
</script>
<?php }
$kUser->getUserDetails($_SESSION['user_id']);
if($kUser->iPrivate=='1')
{
    $affiliation=$kUser->szFirstName." ".$kUser->szLastName;
}
else
{
    $affiliation=$kUser->szCompanyName;
}
$szServiceDescriptionString = display_service_type_description($postSearchAry,$iLanguage);
$kConfig->loadCountry($postSearchAry['idOriginCountry']);
$fromCountry=$kConfig->szCountryISO;

$kConfig->loadCountry($postSearchAry['idDestinationCountry']);
$toCountry=$kConfig->szCountryISO;
$sku=$fromCountry."-".$toCountry;

//$szCurrencyCode='DKK';

$nameECommerce='{"name": "'.__BASE_URL__.'"}';
?>
</div>
<div class="google-analytic-codes" style="position:absolute;right:0;bottom:0;">
		
    <!-- Google Code for purchase Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1001820024;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "omJXCKCouQMQ-J7a3QM";
        var google_conversion_value = 0;
        /* ]]> */
        
    </script>
    <?php 
    if(__ENVIRONMENT__ == "LIVE"){?>
    <script>    
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
        { (i[r].q=i[r].q||[]).push(arguments)} 
        ,i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-33742200-1', 'auto');
        //ga('require', 'GTM-T4BXRGW');
        ga('send', 'pageview');

        
        ga('require', 'ecommerce', 'ecommerce.js');
        ga('require', 'ecommerce');
          
        ga('ecommerce:addTransaction', {
          'id': '<?php echo $postSearchAry['szBookingRef']?>',                     // Transaction ID. Required.
          'affiliation': '<?php echo $affiliation;?>',   // Affiliation or store name.
          'revenue': '<?php echo round($fReferalAmountDKK,2)?>',               // Grand Total.
          'shipping': '<?php echo round($fTotalPriceDKK,2)?>',                  // Shipping.
          'tax': '<?php echo round($fTotalVatDKK,2);?>'                     // Tax.
        });
        ga('ecommerce:addItem', {
          'id': '<?php echo $postSearchAry['szBookingRef']?>',                     // Transaction ID. Required.
          'name': '<?php echo $szServiceDescriptionString;?>',    // Product name. Required.
          'sku': '<?php echo $sku;?>',
          'category': '<?php echo $postSearchAry['szTransportMode']?>' // Quantity.
        });
        ga('ecommerce:send');
    </script>
    <?php }?>
    <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1001820024/?value=0&amp;label=omJXCKCouQMQ-J7a3QM&amp;guid=ON&amp;script=0"/></div>
    </noscript>
</div>		
<?php } else if($postSearchAry['szPaymentStatus']=='Failed') {  unset($_SESSION['amount_mismatch']); ?>
	<div id="hsbody">			
		<p><h5><?=t($t_base.'fields/thank_you_booking');?></h5></p>
		<p> <?=t($t_base.'fields/booking_ref_is');?>: <?=$postSearchAry['szBookingRef']?></p><br>
		<p> <? echo t($t_base.'fields/your_paid_amount_does_not_match_with_booking_amount');?>.</p><br>
	</div>
<?php } else {?>
	<div id="hsbody">			
		<p><h5><?=t($t_base.'fields/thank_you_booking');?></h5></p>
		<p> <?=t($t_base.'fields/booking_ref_is');?>: <?=$postSearchAry['szBookingRef']?></p><br>
		<p> <?php echo t($t_base.'fields/your_payment_status_is_in_pending_state');?></p><br>
	</div>		
<?php } ?> 
<?php
$invoiceUrl=__BASE_URL__."/viewInvoice/".$postSearchAry['id']."/";
$bookingConfirmationUrl=__BASE_URL__."/viewBookingConfirmation/".$postSearchAry['id']."/";

$_SESSION['booking_id'] = '';
unset($_SESSION['booking_id']);
$_SESSION['booking_register_shipper_id'] = '';
$_SESSION['booking_register_consignee_id'] = '';
unset($_SESSION['booking_register_shipper_id']);
unset($_SESSION['booking_register_consignee_id']);

$_SESSION['logged_in_by_booking_process']='';
unset($_SESSION['logged_in_by_booking_process']);
		
$iThankYouPageAdword = 1;
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
$kNPS = new cNPS();
if(!$kNPS->getCustomerFeedBackDay($_SESSION['user_id']))
{
?>
<script type="text/javascript">
       var elements = document.getElementsByTagName('a');
                  //alert(elements[0].href);
                  for(var i = 0, len = elements.length; i < len; i++) 
                  {
                        //alert(elements[28].href);
                        if(elements[i].href!='' && elements[i].href!='javascript:void(0)' && elements[i].href!='javascript:void(0);' && elements[i].href!='<?=$invoiceUrl?>' && elements[i].href!='<?=$bookingConfirmationUrl?>')
                        {
                        	 // alert(elements[i].href);	
                              //$(elements[i]).attr('onClick','checkPageRedirectionBulkService_feedback(\''+elements[i].href+'\');');
                              //$(elements[i]).attr('href','javascript:void(0);');
                  		}
                  }
</script>
<?php  }?>