<?php 
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
//require_once( __APP_PATH_CLASSES__ . "/config.class.php" );
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL); 
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

ini_set (max_execution_time,360000);
$newlink=getTransportecaDB(); 
if(!class_exists('cChat'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php"); 
    require_once(__APP_PATH_CLASSES__."/chat.class.php");
    require_once(__APP_PATH_CLASSES__."/user.class.php");
    require_once(__APP_PATH_CLASSES__."/booking.class.php");
    require_once(__APP_PATH_CLASSES__."/imap.class.php");
    require_once(__APP_PATH_CLASSES__."/gmail.class.php");
    require_once(__APP_PATH_CLASSES__."/sendEmail.class.php");
    require_once(__APP_PATH_CLASSES__."/registeredShippersConsignees.class.php");
} 
/*
* pulling data from Gmail Inbox and updates to crm email logs
*/

$kGmail = new cGmail();
$resultAry = $kGmail->refreshGmailInboxData(); 
closeDB($newlink);
?>