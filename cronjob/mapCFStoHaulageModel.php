<?php
/**
 * All CFS to Pricing Haulage Model
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$newlink=getTransportecaDB();
if(!class_exists('cHaulagePricing'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/haulagePricing.class.php");
}
$kHaulagePricing = new cHaulagePricing();
$kHaulagePricing->mapAllCFSTOHaulageModel();
?>