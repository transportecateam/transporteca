<?php
//session_start();
/**
 * Currency conversion cronjob
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_once('functions.php');

$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
	require_once(__APP_PATH_CLASSES__."/booking.class.php");
}

$kForwarder = new cForwarder();
$kBooking = new cBooking();
$forwarderAry = $kForwarder->getAllForwarderWithCurrencyChanged();

if(!empty($forwarderAry))
{
	$updatedForwarderAry = array();
	foreach($forwarderAry as $forwarderArys)
	{
		$forwarderBookingAry = array();
		$idForwarder = $forwarderArys['idForwarder'] ;
		$forwarderBookingAry = $kBooking->getBookingDetailsByForwarder($idForwarder);
		if(!empty($forwarderBookingAry))
		{
			$forwarderExchangeRate = round((float)1/$forwarderArys['fExchangeRate'],6);
			foreach($forwarderBookingAry as $bookingArr)
			{
				$totalPrice = 0;
				$fOriginHaulagePrice = round((float)($bookingArr['fOriginHaulagePrice'] * $forwarderExchangeRate),2);
				$fOriginCCPrice = round((float)($bookingArr['fOriginCCPrice']*$forwarderExchangeRate),2);
				$fCfstoCfsPrice = round((float)($bookingArr['fCfstoCfsPrice']*$forwarderExchangeRate),2);
				$fDestinationCCPrice = round((float)($bookingArr['fDestinationCCPrice']*$forwarderExchangeRate),2);
				$fDestinationHaulagePrice =round((float)($bookingArr['fDestinationHaulagePrice'] * $forwarderExchangeRate),2);

				$totalPrice = $fOriginHaulagePrice + $fOriginCCPrice + $fCfstoCfsPrice + $fDestinationCCPrice + $fDestinationHaulagePrice ;
			
				$updatePriceAry = array();
				$updatePriceAry['fTotalPrice'] = $totalPrice ;
				$updatePriceAry['idBooking'] = $bookingArr['id'] ;
				$updatePriceAry['idForwarder'] = $bookingArr['idForwarder'] ;
				$kBooking->updateForwarderNewPrice($updatePriceAry);
			}
		}
		
		if(!empty($updatedForwarderAry) && !in_array($idForwarder,$updatedForwarderAry))
		{
			$updatedForwarderAry[$ctr] = $idForwarder;
			$ctr++;
		}
		else if(empty($updatedForwarderAry))
		{
			$updatedForwarderAry[$ctr] = $idForwarder;
			$ctr++;
		}	
	}
	
	if($kForwarder->deleteForwarderCurrencyLogs($updatedForwarderAry))
	{
		//echo "<h3> Script executed successfully</br></h3>";
	}
	else
	{
		//echo "<h3> Script executed successfully</br></h3>";
	}
}
?>