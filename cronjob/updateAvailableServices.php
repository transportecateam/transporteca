<?php
//session_start();
/**
 * Currency conversion cronjob
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    ini_set (max_execution_time,36000);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_once('functions.php');

//error_reporting(E_ALL);
//ini_set('error_reporting','E_ALL');
//ini_set('display_error','on');

$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/haulagePricing.class.php");
	require_once(__APP_PATH_CLASSES__."/config.class.php");
	require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
}

$kWHSSearch = new cWHSSearch();

$cfsServiceAry = array();

/*
 * Fetching all the actice and valid LCL service. 
 * */
$cfsServiceAry = $kWHSSearch->getAllActiveCfsServices();

$globalCFSAry = array();
if(!empty($cfsServiceAry))
{
	$ctr = 0;
	$counter = 0;
	$addCFSCombinatinAry = array();
	$haulageCountryAry = array();
	foreach($cfsServiceAry as $cfsServiceArys)
	{
		if((int)$cfsServiceArys['idWarehouseFrom']>0 && (int)$cfsServiceArys['idWarehouseTo']>0)
		{
			$cfsCountryAry = array();
			$idOriginWarehouse = (int)$cfsServiceArys['idWarehouseFrom'] ;
			$idDestinationWarehouse = (int)$cfsServiceArys['idWarehouseTo'] ;
			
			$cfs_details_ary = array();
			$cfs_country_ary = array();
			$originCFSCountryAry = array();
			$originCFSCountryAry = $kWHSSearch->getWareHouseDetails($idOriginWarehouse);
				
			/*
			 * Fetching all the country from tblwarehousecountry in which this warehouse serving.
			 * 
			 */
			
			$warehouseCountryListAry = array();
			$warehouseCountryListAry = $kWHSSearch->getWarehouseAllCountry($idOriginWarehouse);
			
			//$haulageCountryListAry = array();
			//$haulageCountryListAry = $kWHSSearch->getAllHaulageCountry($idOriginWarehouse,1);
			
			if(!empty($haulageCountryListAry) && !empty($warehouseCountryListAry))
			{
				$globalCFSAry[$idOriginWarehouse]['country']  = array_merge($warehouseCountryListAry,$haulageCountryListAry);
			}
			else if(!empty($warehouseCountryListAry))
			{
				$globalCFSAry[$idOriginWarehouse]['country']  = $warehouseCountryListAry ;
			}
			else if(!empty($haulageCountryListAry))
			{
				$globalCFSAry[$idOriginWarehouse]['country']  = $haulageCountryListAry ;
			}
			
			$globalCFSAry[$idOriginWarehouse]['szCFSDetails'] = $originCFSCountryAry;
			
			$origin_cfs_country_ary = $globalCFSAry[$idOriginWarehouse]['country'] ;
			$cfs_country_ary = $globalCFSAry[$idOriginWarehouse]['country'] ;
			
			$idOriginCountry = $originCFSCountryAry[0]['idCountry'];
			if($cfsServiceArys['iOriginChargesApplicable']==1)
			{				
				$iOriginHaulageExists = $kWHSSearch->isHaulageServiceExist($originCFSCountryAry[0]['idCountry'],$idOriginWarehouse,1);
				//echo "<br /> checking origin haulage for country id ".$originCFSCountryAry[0]['idCountry'] ;
				//echo "<br /> origin whs id ".$idOriginWarehouse ;
				//echo "<br /> haulage exist ".$iOriginHaulageExists ;
			}
			else
			{
				$iOriginHaulageExists = 0;				
			}
			
			$haulageExistAry[$idOriginWarehouse] = $iOriginHaulageExists ;
			$usedCFSAry[] = $idOriginWarehouse ;
			
			$destinationCFSCountryAry = $kWHSSearch->getWarehouseDetails($idDestinationWarehouse);
			$idDestinationCountry = $destinationCFSCountryAry[0]['idCountry'];
			
			$warehouseCountryListAry = array();
			$warehouseCountryListAry = $kWHSSearch->getWarehouseAllCountry($idDestinationWarehouse);
			
			//$haulageCountryListAry = array();
			//$haulageCountryListAry = $kWHSSearch->getAllHaulageCountry($idDestinationWarehouse,2);
			
			if(!empty($haulageCountryListAry) && !empty($warehouseCountryListAry))
			{
				$globalCFSAry[$idDestinationWarehouse]['country'] = array_merge($warehouseCountryListAry,$haulageCountryListAry);
			}
			else if(!empty($warehouseCountryListAry))
			{
				$globalCFSAry[$idDestinationWarehouse]['country'] = $warehouseCountryListAry ;
			}
			else if(!empty($haulageCountryListAry))
			{
				$globalCFSAry[$idDestinationWarehouse]['country'] = $haulageCountryListAry ;
			}
			
			$globalCFSAry[$idDestinationWarehouse]['szCFSDetails'] = $destinationCFSCountryAry;
			
			$cfs_details_ary = $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'] ;
			
			$destination_cfs_country_ary = $globalCFSAry[$idDestinationWarehouse]['country'] ;
			
			if($cfsServiceArys['iDestinationChargesApplicable']==1)
			{
				/*
				 * If this warehouse is serving ?TW service then check for ?TD
				*
				
				$houlageData = array();
				$houlageData['idWareHouse'] = $idDestinationWarehouse ;
				$houlageData['iDirection'] = 2; // destination haulage
					
				$houlageData['szLatitude'] = $destinationCFSCountryAry[0]['szLatitude'] ;
				$houlageData['szLongitude'] = $destinationCFSCountryAry[0]['szLongitude'] ;
				$houlageData['szPostcode'] = $destinationCFSCountryAry[0]['szPostCode'];
				$houlageData['idCountry'] = $destinationCFSCountryAry[0]['idCountry'];
				
				/*
				 * checking haulage at destination side
				* iDirection = 2 means haulage at destination side.
				* $iDestinationHaulageExists = $kWHSSearch->isHaulageExistByWarehouseId($houlageData,2);
				*/
				
				$iDestinationHaulageExists = $kWHSSearch->isHaulageServiceExist($destinationCFSCountryAry[0]['idCountry'],$idDestinationWarehouse,2);
				//echo "<br /> checking dest haulage for country id ".$destinationCFSCountryAry[0]['idCountry'] ;
				//echo "<br /> dest whs id ".$idDestinationWarehouse ;
				//echo "<br /> dest haulage exist ".$iDestinationHaulageExists ;
			}
			else
			{
				$iDestinationHaulageExists = 0;				
			}
			
			$haulageExistAry[$idDestinationWarehouse] = $iDestinationHaulageExists ;
			$usedCFSAry[] = $idDestinationWarehouse ;
			
			/*
			 * Checking if this warehouse is serving custom clearance at origin side?
			*
			*/
			$iOriginCC = $kWHSSearch->getCustomClearanceDetails($idDestinationWarehouse,$idOriginWarehouse,1,true);
			
			/*
			 * Checking if this warehouse is serving custom clearance at destination side?
			*
			*/
			$iDestinationCC = $kWHSSearch->getCustomClearanceDetails($idDestinationWarehouse,$idOriginWarehouse,2,true);
			
			$addCFSCombinatinAry[$ctr]['idWarehouseFrom'] =  $globalCFSAry[$idOriginWarehouse]['szCFSDetails'][0]['id'] ;
			$addCFSCombinatinAry[$ctr]['idWarehouseTo'] =  $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['id'] ;
			$addCFSCombinatinAry[$ctr]['idFrequency'] =  $cfsServiceArys['idFrequency'];
			
			$addCFSCombinatinAry[$ctr]['idForwarder'] =  $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['idForwarder'] ;
			$addCFSCombinatinAry[$ctr]['szDisplayName'] =  $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['szDisplayName'] ;
			
			$addCFSCombinatinAry[$ctr]['szOriginCountryName'] =  $globalCFSAry[$idOriginWarehouse]['szCFSDetails'][0]['szCountryName'] ;
			$addCFSCombinatinAry[$ctr]['szDestinationCountryName'] =  $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['szCountryName'] ;
			
			$addCFSCombinatinAry[$ctr]['idOriginCountry'] =  $globalCFSAry[$idOriginWarehouse]['szCFSDetails'][0]['idCountry'] ;
			$addCFSCombinatinAry[$ctr]['idDestinationCountry'] =  $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['idCountry'] ;
			
			$addCFSCombinatinAry[$ctr]['szOriginCity'] =  $globalCFSAry[$idOriginWarehouse]['szCFSDetails'][0]['szCity'] ;
			$addCFSCombinatinAry[$ctr]['szDestinationCity'] =  $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['szCity'] ;
			$addCFSCombinatinAry[$ctr]['iTransitTime'] =  $cfsServiceArys['iTransitHours'];
			$addCFSCombinatinAry[$ctr]['szFrequency'] =  $cfsServiceArys['szDescription'];
			$addCFSCombinatinAry[$ctr]['dtValidFrom'] =  $cfsServiceArys['dtValidFrom'];
			$addCFSCombinatinAry[$ctr]['dtValidTo'] =  $cfsServiceArys['dtExpiry'];
			$addCFSCombinatinAry[$ctr]['iPTP'] = 1 ;
			
			if($iOriginCC)
			{
				$addCFSCombinatinAry[$ctr]['iOriginCC'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iOriginCC'] = 0 ;
			}
			
			if($iDestinationCC)
			{
				$addCFSCombinatinAry[$ctr]['iDestinationCC'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iDestinationCC'] = 0 ;
			}			
			if($cfsServiceArys['iOriginChargesApplicable']==1)
			{
				$addCFSCombinatinAry[$ctr]['iWTP'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iWTP'] = 0 ;
			}
			
			if($cfsServiceArys['iDestinationChargesApplicable'])
			{
				$addCFSCombinatinAry[$ctr]['iPTW'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iPTW'] = 0 ;
			}
			
			if($cfsServiceArys['iDestinationChargesApplicable']==1 && $cfsServiceArys['iOriginChargesApplicable']==1)
			{
				$addCFSCombinatinAry[$ctr]['iWTW'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iWTW'] = 0 ;
			}
						
			if($iOriginHaulageExists)
			{
				$addCFSCombinatinAry[$ctr]['iDTP'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iDTP'] = 0;
			}					
			if($iDestinationHaulageExists)
			{
				$addCFSCombinatinAry[$ctr]['iPTD'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iPTD'] = 0;
			}			
			if(($iDestinationHaulageExists) && ($cfsServiceArys['iDestinationChargesApplicable']==1) && ($cfsServiceArys['iOriginChargesApplicable']==1))
			{
				$addCFSCombinatinAry[$ctr]['iWTD'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iWTD'] = 0;
			}
			
			if(($iOriginHaulageExists) && ($cfsServiceArys['iDestinationChargesApplicable']==1) && ($cfsServiceArys['iOriginChargesApplicable']==1))
			{
				$addCFSCombinatinAry[$ctr]['iDTW'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iDTW'] = 0;
			}
			
			if(($iOriginHaulageExists) && ($iDestinationHaulageExists) && ($cfsServiceArys['iDestinationChargesApplicable']==1) && ($cfsServiceArys['iOriginChargesApplicable']==1))
			{
				$addCFSCombinatinAry[$ctr]['iDTD'] = 1 ;
			}
			else
			{
				$addCFSCombinatinAry[$ctr]['iDTD'] = 0;
			}
			
			if(empty($destination_cfs_country_ary))
			{
				$destination_cfs_country_ary[0]['szCountryName'] = $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['szCountryName'] ;
				$destination_cfs_country_ary[0]['countryID'] = $globalCFSAry[$idDestinationWarehouse]['szCFSDetails'][0]['idCountry'] ;
			}
			if (empty($origin_cfs_country_ary))
			{
				$origin_cfs_country_ary[0]['szCountryName'] = $globalCFSAry[$idOriginWarehouse]['szCFSDetails'][0]['szCountryName'] ;
				$origin_cfs_country_ary[0]['countryID'] = $globalCFSAry[$idOriginWarehouse]['szCFSDetails'][0]['idCountry'] ;
			}
			
			$haulageCountryAry[$idOriginWarehouse][] = $addCFSCombinatinAry[$ctr]['idOriginCountry'];
			$haulageCountryAry[$idDestinationWarehouse][] = $addCFSCombinatinAry[$ctr]['idDestinationCountry'];	
					
			if(!empty($origin_cfs_country_ary) && !empty($destination_cfs_country_ary))
			{		
				foreach($origin_cfs_country_ary as $origin_cfs_country_arys)
				{
					foreach($destination_cfs_country_ary as $destination_cfs_country_arys)
					{
						//echo " <br> country from ".$origin_cfs_country_arys['szCountryName']." id ".$origin_cfs_country_arys['countryID']." dest ".$destination_cfs_country_arys['szCountryName']." id: ".$destination_cfs_country_arys['countryID']." <br>";
						
						$ctr++;
						$addCFSCombinatinAry[$ctr] = $addCFSCombinatinAry[$ctr-1];
						
						$addCFSCombinatinAry[$ctr]['szOriginCountryName'] = $origin_cfs_country_arys['szCountryName'];
						$addCFSCombinatinAry[$ctr]['idOriginCountry'] = $origin_cfs_country_arys['countryID'];
						
						$addCFSCombinatinAry[$ctr]['szDestinationCountryName'] = $destination_cfs_country_arys['szCountryName'];
						$addCFSCombinatinAry[$ctr]['idDestinationCountry'] = $destination_cfs_country_arys['countryID'];
						
						if($cfsServiceArys['iOriginChargesApplicable']==1)
						{

							/*
							 * If this warehouse is serving WT? service then check for DT?
							
							
							$houlageData = array();
							$houlageData['idWareHouse'] = $idOriginWarehouse ;
							$houlageData['iDirection'] = 1; // origin haulage
							
							$houlageData['szLatitude'] = $origin_cfs_country_arys['szLatitude'] ;
							$houlageData['szLongitude'] = $origin_cfs_country_arys['szLongitude'];
							$houlageData['szPostcode'] = $originCFSCountryAry[0]['szPostCode'];
							$houlageData['idCountry'] = $origin_cfs_country_arys['countryID'];
							
							/*
							 * checking haulage at origin side
							* iDirection = 1 means haulage at origin side.
							
							$iOriginHaulageExists = $kWHSSearch->isHaulageExistByWarehouseId($houlageData,1);
							*/
							$iOriginHaulageExists = $kWHSSearch->isHaulageServiceExist($origin_cfs_country_arys['countryID'],$idOriginWarehouse,1);
							
							//echo "<br /> 2 checking origin haulage for country id ".$origin_cfs_country_arys['countryID'] ;
							//echo "<br /> 2 origin whs id ".$idOriginWarehouse ;
							//echo "<br /> 2 haulage exist ".$iOriginHaulageExists ;
						}
						else
						{
							$iOriginHaulageExists = 0;				
						}						
						if($cfsServiceArys['iDestinationChargesApplicable']==1)
						{
							/*
							 * If this warehouse is serving WT? service then check for DT?
							
								
							$houlageData = array();
							$houlageData['idWareHouse'] = $idDestinationWarehouse ;
							$houlageData['iDirection'] = 2; // origin haulage
								
							$houlageData['szLatitude'] = $destination_cfs_country_arys['szLatitude'] ;
							$houlageData['szLongitude'] = $destination_cfs_country_arys['szLongitude'];
							$houlageData['szPostcode'] = $originCFSCountryAry[0]['szPostCode'];
							$houlageData['idCountry'] = $destination_cfs_country_arys['countryID'];
								
							/*
							 * checking haulage at origin side
							* iDirection = 1 means haulage at origin side.
							
								
							$iDestinationHaulageExists = $kWHSSearch->isHaulageExistByWarehouseId($houlageData,2);
							*/
							
							$iDestinationHaulageExists = $kWHSSearch->isHaulageServiceExist($destination_cfs_country_arys['countryID'],$idDestinationWarehouse,2);
							//echo "<br /> 2 checking dest haulage for country ".$destination_cfs_country_arys['countryID'] ;
							//echo "<br /> 2 dest warehouse ".$idDestinationWarehouse ;
							//echo "<br /> 2 haulage exist ".$iDestinationHaulageExists ;
						}
						else
						{
							$iDestinationHaulageExists = 0;				
						}
						
						if(($iOriginHaulageExists) && ($iDestinationHaulageExists) && ($cfsServiceArys['iDestinationChargesApplicable']==1) && ($cfsServiceArys['iOriginChargesApplicable']==1))
						{
							$addCFSCombinatinAry[$ctr]['iDTD'] = 1 ;
						}
						else
						{
							$addCFSCombinatinAry[$ctr]['iDTD'] = 0;
						}
						
						if($iOriginHaulageExists)
						{
							$addCFSCombinatinAry[$ctr]['iDTP'] = 1 ;
						}
						else
						{
							$addCFSCombinatinAry[$ctr]['iDTP'] = 0;
						}					
						if($iDestinationHaulageExists)
						{
							$addCFSCombinatinAry[$ctr]['iPTD'] = 1 ;
						}
						else
						{
							$addCFSCombinatinAry[$ctr]['iPTD'] = 0;
						}			
						if(($iDestinationHaulageExists) && ($cfsServiceArys['iDestinationChargesApplicable']==1) && ($cfsServiceArys['iOriginChargesApplicable']==1))
						{
							$addCFSCombinatinAry[$ctr]['iWTD'] = 1 ;
						}
						else
						{
							$addCFSCombinatinAry[$ctr]['iWTD'] = 0;
						}
						
						if(($iOriginHaulageExists) && ($cfsServiceArys['iDestinationChargesApplicable']==1) && ($cfsServiceArys['iOriginChargesApplicable']==1))
						{
							$addCFSCombinatinAry[$ctr]['iDTW'] = 1 ;
						}
						else
						{
							$addCFSCombinatinAry[$ctr]['iDTW'] = 0;
						}						
					}
				}
				$ctr++;
			}
			else
			{
				$ctr++;
			}
		}
	}
	
/*	
 * This is was previously used for checking if warehouse has haulage service if yes add a PTP and D service from or to warehouse.
 * please don't delete this code it may be used in future.
 */ 
	if(!empty($addCFSCombinatinAry))
	{	
		$additionalAry = array();
		$countr=0;
		foreach($addCFSCombinatinAry as $addCFSCombinatinArys)
		{
			$originHaulageCFSAry = array();
			$idOriginWarehouse = $addCFSCombinatinArys['idWarehouseFrom'] ;
			$idDestinationWarehouse = $addCFSCombinatinArys['idWarehouseTo'] ;
			
			$origin_cfs_key = $idOriginWarehouse."_1"."_".$addCFSCombinatinArys['idOriginCountry'];
			if(!empty($usedAry) && !array_key_exists($origin_cfs_key,$usedAry))
			{
				//$originHaulageCFSAry = $kWHSSearch->getAllCountryServedByCFS($idOriginWarehouse,$haulageCountryAry[$idOriginWarehouse],1);
				$originHaulageCFSAry = $kWHSSearch->getAllHaulageCountry($idOriginWarehouse,1);
			
				if(!empty($originHaulageCFSAry))
				{
					foreach($originHaulageCFSAry as $originHaulageCFSArys)
					{
						$additionalAry[$countr] = $addCFSCombinatinArys ;
						$kConfig = new cConfig();
						$countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$originHaulageCFSArys['idCountry']);
						$additionalAry[$countr]['idOriginCountry'] = $originHaulageCFSArys['idCountry'];
						$additionalAry[$countr]['szOriginCountryName'] = $countryNameAry[$originHaulageCFSArys['idCountry']]['szCountryName'];
						$additionalAry[$countr]['iPTP'] = 1 ;
						$additionalAry[$countr]['iDTP'] = 1 ;
						
						$additionalAry[$countr]['iDTW'] = 0 ;
						$additionalAry[$countr]['iWTD'] = 0 ;
						$additionalAry[$countr]['iWTW'] = 0 ;
						$additionalAry[$countr]['iPTW'] = 0 ;
						$additionalAry[$countr]['iWTP'] = 1 ;
						
						if($addCFSCombinatinArys['iPTD']==1)
						{
							$additionalAry[$countr]['iDTD'] = 1 ;
						}
						else
						{
							$additionalAry[$countr]['iDTD'] = 0 ;
						}
						$countr++;
					}
				}
				$usedAry[$origin_cfs_key] = $origin_cfs_key ;
			}
			else if(empty($usedAry))
			{
				//$originHaulageCFSAry = $kWHSSearch->getAllCountryServedByCFS($idOriginWarehouse,$haulageCountryAry[$idOriginWarehouse],1);
				$originHaulageCFSAry = $kWHSSearch->getAllHaulageCountry($idOriginWarehouse,1);
				
				if(!empty($originHaulageCFSAry))
				{
					foreach($originHaulageCFSAry as $originHaulageCFSArys)
					{
						$additionalAry[$countr] = $addCFSCombinatinArys ;
						$kConfig = new cConfig();
						$countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$originHaulageCFSArys['idCountry']);
						$additionalAry[$countr]['idOriginCountry'] = $originHaulageCFSArys['idCountry'];
						$additionalAry[$countr]['szOriginCountryName'] = $countryNameAry[$originHaulageCFSArys['idCountry']]['szCountryName'];
						$additionalAry[$countr]['iPTP'] = 1 ;
						$additionalAry[$countr]['iDTP'] = 1 ;
						$additionalAry[$countr]['iDTW'] = 0 ;
						$additionalAry[$countr]['iWTD'] = 0 ;
						$additionalAry[$countr]['iWTW'] = 0 ;
						$additionalAry[$countr]['iPTW'] = 0 ;
						$additionalAry[$countr]['iWTP'] = 1 ;
						if($addCFSCombinatinArys['iPTD']==1)
						{
							$additionalAry[$countr]['iDTD'] = 1 ;
						}
						else
						{
							$additionalAry[$countr]['iDTD'] = 0 ;
						}
						$countr++;
					}
				}
				$usedAry[$origin_cfs_key] = $origin_cfs_key ;
			}
			
			$dest_cfs_key = $idDestinationWarehouse."_2_".$haulageCountryAry[$idDestinationWarehouse];
			if(!empty($usedAry) && !array_key_exists($dest_cfs_key,$usedAry))
			{
				//$destinationHaulageCFSAry = $kWHSSearch->getAllCountryServedByCFS($idDestinationWarehouse,$haulageCountryAry[$idDestinationWarehouse],2);
				$destinationHaulageCFSAry = $kWHSSearch->getAllHaulageCountry($idDestinationWarehouse,2);
				if(!empty($destinationHaulageCFSAry))
				{
					foreach($destinationHaulageCFSAry as $destinationHaulageCFSArys)
					{
						$additionalAry[$countr] = $addCFSCombinatinArys ;
						$kConfig = new cConfig();
						$countryNameAry = array();
						$countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$destinationHaulageCFSArys['idCountry']);
						$additionalAry[$countr]['idDestinationCountry'] = $destinationHaulageCFSArys['idCountry'];
						$additionalAry[$countr]['szDestinationCountryName'] = $countryNameAry[$destinationHaulageCFSArys['idCountry']]['szCountryName'];
						$additionalAry[$countr]['iPTP'] = 0 ;
						$additionalAry[$countr]['iPTD'] = 1 ;						
						$additionalAry[$countr]['iDTD'] = 0 ;
						$additionalAry[$countr]['iDTW'] = 0 ;
						$additionalAry[$countr]['iWTD'] = 0 ;
						$additionalAry[$countr]['iWTW'] = 0 ;
						$additionalAry[$countr]['iPTW'] = 0 ;
						$additionalAry[$countr]['iWTP'] = 0 ;
						
						if($addCFSCombinatinArys['iDTP']==1)
						{
							$additionalAry[$countr]['iDTD'] = 1 ;
						}
						else
						{
							$additionalAry[$countr]['iDTD'] = 0 ;
						}						
						$countr++;
					}
				}
				$usedAry[$dest_cfs_key] = $dest_cfs_key ;
			}
			else if(empty($usedAry))
			{
				//$destinationHaulageCFSAry = $kWHSSearch->getAllCountryServedByCFS($idDestinationWarehouse,$haulageCountryAry[$idDestinationWarehouse],2);
				$destinationHaulageCFSAry = $kWHSSearch->getAllHaulageCountry($idDestinationWarehouse,2);
				if(!empty($destinationHaulageCFSAry))
				{
					foreach($destinationHaulageCFSAry as $destinationHaulageCFSArys)
					{
						$additionalAry[$countr] = $addCFSCombinatinArys ;
						$kConfig = new cConfig();
						$countryNameAry = array();
						$countryNameAry = $kConfig->getAllCountryInKeyValuePair(false,$destinationHaulageCFSArys['idCountry']);
						$additionalAry[$countr]['idDestinationCountry'] = $destinationHaulageCFSArys['idCountry'];
						$additionalAry[$countr]['szDestinationCountryName'] = $countryNameAry[$destinationHaulageCFSArys['idCountry']]['szCountryName'];
						$additionalAry[$countr]['iPTP'] = 0 ;
						$additionalAry[$countr]['iPTD'] = 1 ;							
						$additionalAry[$countr]['iDTD'] = 0 ;
						$additionalAry[$countr]['iDTW'] = 0 ;
						$additionalAry[$countr]['iWTD'] = 0 ;
						$additionalAry[$countr]['iWTW'] = 0 ;
						$additionalAry[$countr]['iPTW'] = 0 ;
						$additionalAry[$countr]['iWTP'] = 0 ;
						$additionalAry[$countr]['iWTW'] = 0 ;
						
						if($addCFSCombinatinArys['iDTP']==1)
						{
							$additionalAry[$countr]['iDTD'] = 1 ;
						}
						else
						{
							$additionalAry[$countr]['iDTD'] = 0 ;
						}
						$countr++;
					}
				}
				$usedAry[$dest_cfs_key] = $dest_cfs_key ;
			}
		}
	}
	
	if(!empty($addCFSCombinatinAry))
	{		
		if(!empty($additionalAry))
		{
			$addCFSCombinatinAry = array_merge($addCFSCombinatinAry,$additionalAry);
		}
		
		/*
		* Delete all old service with status=2
		*/
		$kWHSSearch->deleteOldServices();
		
		/*
		* removing duplicate country combination.
		**/
		$final_ary = array();
		$ret_ary = array();
		$ctr=0;
		foreach($addCFSCombinatinAry as $addCFSCombinatinArys)
		{
			$key = $addCFSCombinatinArys['idWarehouseFrom']."_".$addCFSCombinatinArys['idWarehouseTo']."_".$addCFSCombinatinArys['idOriginCountry']."_".$addCFSCombinatinArys['idDestinationCountry']."_".$addCFSCombinatinArys['idDestinationCountry'] ; ;
			if(!empty($final_ary) && !array_key_exists($key,$final_ary))
			{
				$ret_ary[$ctr] = $addCFSCombinatinArys ;
				$final_ary[$key] = $key ;
				$ctr++;
			}
			else if(empty($final_ary))
			{
				$ret_ary[$ctr] = $addCFSCombinatinArys ;
				$final_ary[$key] = $key ;
				$ctr++;
			}
		}
		
		/*
		* Adding new services with status=3 
		*/
		
		$kWHSSearch->addCFSCombinatin($ret_ary);	
		
		/*
		* Update service's status
		*/
		$kWHSSearch->updateAvailabelServices();
	}
	
	require_once( __APP_PATH__ ."/cronjob/sendServiceNotificationEmails.php" );
}
?>