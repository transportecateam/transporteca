<?php
//session_start();
/**
 * Currency conversion cronjob
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    ini_set (max_execution_time,360000);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_once('functions.php');
  
$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/booking.class.php");
    require_once(__APP_PATH_CLASSES__."/billing.class.php"); 
} 
$kPendingTray = new cPendingTray(); 
$kBooking = new cBooking();
$kBookingNew = new cBooking();
$courierBookingAry = array();
$courierBookingAry = $kPendingTray->getAllBookingstobeupdatetoAftership(); 
  
if(!empty($courierBookingAry))
{
    foreach($courierBookingAry as $courierBookingArys)
    {  
        $idBooking = (int)$courierBookingArys['id'];
        $idBookingFile = (int)$courierBookingArys['idFile'];
        $szMasterTrackingNumber = trim($courierBookingArys['szTrackingNumber']);
        $idServiceProvider = (int)$courierBookingArys['idServiceProvider'];
        
        $kTracking = new cTracking(); 
        $trackingARY = $kTracking->createAfterShipTracking($idBooking,$szMasterTrackingNumber,$idServiceProvider);   
            
        if($kTracking->tracking_error==true)
        {
            $dtCurrentDateTime = $kBooking->getRealNow();      
            $dtReminderDate = getDateCustom($dtCurrentDateTime);      
            
            $szCancelBookingText=  str_replace("szErrorText", $kTracking->szAfterShipTrackingNotification, __AFTER_SHIP_TEXT_SLOVE_TASK__);
            $addTransportecaTask = array();
            $addTransportecaTask['szReminderNotes'] = $szCancelBookingText;
            $addTransportecaTask['idTaskOwner'] = "";
            $addTransportecaTask['idBookingFile'] = $idBookingFile;
            $addTransportecaTask['dtTaskDate'] = $dtReminderDate;
            $addTransportecaTask['dtTaskTime'] = date('H:i:s',  strtotime($dtReminderDate)); 
            $addTransportecaTask['dtRemindDate'] = $dtReminderDate;
            $addTransportecaTask['iOverviewTask'] = 1;   
            
            $kBookingNew->addAddReminderNotes($addTransportecaTask);
            $idBookingFileReminderNotes = $kBookingNew->idBookingFileReminderNotes; 

            $fileLogsAry = array();   
            $fileLogsAry['dtSolveTaskCreated'] = $dtCurrentDateTime;
            $fileLogsAry['iAddSolveTask'] = 0;
            $fileLogsAry['idFileOwner'] = "";  //Unassigned to anybody
            $fileLogsAry['idReminderTask'] = $idBookingFileReminderNotes;
            $fileLogsAry['iClosed'] = 0; 
            $fileLogsAry['szTransportecaTask'] = "T220";  
            $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtCurrentDateTime;  
            
            if(!empty($fileLogsAry))
            {
                $file_log_query = "";
                foreach($fileLogsAry as $key=>$value)
                {
                    $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }  
            $file_log_query = rtrim($file_log_query,",");    
            //$kBooking_new = new cBooking();
            $kBookingNew->updateBookingFileDetails($file_log_query,$idBookingFile,true);

            echo "<br><br>We got error while adding Tracking code to Aftership API.<br>Description: ".$kTracking->szAfterShipTrackingNotification."<br> Tracking: ".$szMasterTrackingNumber;
        }  
        $res_ary = array();
        $res_ary['iUpdateToAftership'] = 0;
        $update_query = ""; 
        if(!empty($res_ary))
        {
            foreach($res_ary as $key=>$value)
            {
                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
            $update_query = rtrim($update_query,",");    
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            { 
                //@TO DO
            }
        }   
    }
}

?>