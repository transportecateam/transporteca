<?php
//session_start();
/**
 * Forwarder Rate Expiry Notification Email
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
	require_once(__APP_PATH_CLASSES__."/config.class.php");
	require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
}
$kForwarder=new cForwarder();
$kWHSSearch=new cWHSSearch();
$kConfig = new cConfig();
$expiryMailArr=array();
$wtwWarehosueExpiryArr=$kForwarder->getWarehouseExpiryRateForWTW(false);
//$haulageWarehosueExpiryArr=$kForwarder->getWarehouseExpiryRateForHaulage(false);

if($wtwWarehosueExpiryArr)
{
	foreach($wtwWarehosueExpiryArr as $key=>$forwarderWarehouseArrs)
	{
		$ctr=0;
		//print_r($forwarderWarehouseArrs);
		$i=0;
		$str='LCL Services <br />';
		foreach($forwarderWarehouseArrs as $forwarderWarehouse)
		{
			$str .=++$ctr.": From ".$forwarderWarehouse[$i]['originWarehouseName'].", ".$forwarderWarehouse[$i]['fromWarehouseCity'].", ".$forwarderWarehouse[$i]['fromWarehouseCountry']." to ".$forwarderWarehouse[$i]['desWarehouseName'].", ".$forwarderWarehouse[$i]['toWarehouseCity'].", ".$forwarderWarehouse[$i]['toWarehouseCountry']." - expire ".date('j F Y',strtotime($forwarderWarehouse[$i]['expiryDate']))." <br />";
			++$i;
		}
		$expiryMailArr[$key][0]=$str;
	}
}
//print_r($expiryMailArr);
//echo "<br/>";
/*if($haulageWarehosueExpiryArr)
{
	foreach($haulageWarehosueExpiryArr as $key=>$haulageWarehosueExpiry)
	{
		$ctr=0;
		$str='Haulage <br />';
		//print_r($forwarderWarehouseArrs);
		$i=0;
		foreach($haulageWarehosueExpiry as $haulageWarehosueExpiries)
		{
			$str .=++$ctr.": ".$haulageWarehosueExpiries[$i]['idDirection']." haulage for ".$haulageWarehosueExpiries[$i]['originWarehouseName'].", ".$haulageWarehosueExpiries[$i]['fromWarehouseCity'].", ".$haulageWarehosueExpiries[$i]['fromWarehouseCountry']." - expire ".date('d/m/Y',strtotime($haulageWarehosueExpiries[$i]['expiryDate']))." <br />";
			++$i;
		}
		if(count($expiryMailArr[$key])>0)
		{
			$counter=count($expiryMailArr[$key]);
			$expiryMailArr[$key][$counter]=$str;
		}
		else
		{
			$expiryMailArr[$key][0]=$str;
		}
	}
}*/

if(!empty($expiryMailArr))
{
	
	foreach($expiryMailArr as $keys=>$expiryMailArrs)
	{
		$kForwarder->load($keys);
		$emailExpirty='';
			
		$replace_ary['szSubject'] = "Rate expiring on Transporteca today - action required Dear ".$kForwarder->szDisplayName." Team";
		$emailExpirty=implode("<br />",$expiryMailArrs);
		$idRoleAry=array('1','3');
		$getEmailArr=$kForwarder->getAllForwardersContactEmail($keys,$idRoleAry);
		//$getEmailArr="ashishsr124@gmail.com";
		$date=date('YmdHis');
		$update_key=md5(strtotime($date));
		
		$szLink="http://".$kForwarder->szControlPanelUrl."/updateServiceExpiry/".$update_key."/";
		//print_r($getEmailArr);
		//echo $replace_ary['szSubject']."<br />";
		$replace_ary['szLink'] = "<a href=".$szLink.">EXTEND RATE VALIDITY BY ONE MONTH</a>";
		$replace_ary['szCopyLink'] = $szLink;
		$replace_ary['szDisplayName'] = $kForwarder->szDisplayName;
		$replace_ary['szemailString'] = $emailExpirty;
		//echo $emailExpirty;Heading: Rate expiring on Transporteca - action required Dear 'ForwarderDisplayName' Team
		createEmail(__EXPIRY_EMAIL_FORWARDER_PRICING_PER_DAY__, $replace_ary,$getEmailArr,$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
		
		$kForwarder->updateExpiryServiceEmailDeail($keys,$update_key,'1');
	}
}?>