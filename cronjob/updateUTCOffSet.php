<?php
//session_start();
/**
 * Update Warehouses UTCOffSetMs Using askgeo.com api
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$newlink=getTransportecaDB();
if(!class_exists('cWHSSearch'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
}
$kWHSSearch = new cWHSSearch();

$f = fopen(__APP_PATH_LOGS__."/updateUTCOffSet.log", "a");
fwrite($f, "\n\n###################Cronjob Started-:".date("d-m-Y h:i:s")."#######################\n");

$timezone_url=__ASK_GEO_API_URL__."/".__ASK_GEO_ACCOUNT_ID__."/".__ASK_GEO_API_KEY__."/query.".__ASK_GEO_FORMAT__."?databases=".__ASK_GEO_DATABATE__."&points=";
$wareHouseArr=$kWHSSearch->getAllWareHouses();
//print_r($wareHouseArr);

$updatedFlag=false;
if(!empty($wareHouseArr))
{
    fwrite($f, "\n========================Start Updating UTCOffset Of Warehouses=========================\n\n");
    foreach($wareHouseArr as $wareHouseArrs)
    {
        fwrite($f, "\n++++++++++++++++++Warehouse Id-:".$wareHouseArrs['id']."+++++++++++++++++++++\n");
        fwrite($f, "\n------------------Warehouse Latitude-:".$wareHouseArrs['szLatitude']."-----------------------\n");
        fwrite($f, "\n------------------Warehouse Longitude-:".$wareHouseArrs['szLongitude']."-----------------------\n");
        fwrite($f, "\n------------------Warehouse UTCOffset-:".$wareHouseArrs['szUTCOffset']."-----------------------\n\n");
        $response='';	
        $posturl=$timezone_url."".$wareHouseArrs['szLatitude'].",".$wareHouseArrs['szLongitude'];
        //echo $posturl."<br/>";
        $response = curl_get_file_contents($posturl);
        $decoded_response = json_decode($response);
        $utcoffsetupdated='';
        $utcoffsetupdated=$decoded_response->data[0]->TimeZone->CurrentOffsetMs;
        //echo $utcoffsetupdated."<br/>";
        if($utcoffsetupdated!='' && $utcoffsetupdated!=$wareHouseArrs['szUTCOffset'])
        {
          $kWHSSearch->updateUTCOffSetMs($wareHouseArrs['id'],$utcoffsetupdated);
          $updatedFlag=true;
        }
    }
    fwrite($f, "\n========================End Updating UTCOffset Of Warehouses=========================\n\n");
}

if($updatedFlag)
{
    fwrite($f, "\n========================Start Updating Transit Time Of Warehouses=========================\n\n");
    $wareHouseUpdatedArr=$kWHSSearch->getAllWareHouses('updateUTCOffSet');
    //print_r($wareHouseUpdatedArr);
    if(!empty($wareHouseUpdatedArr))
    {
        foreach($wareHouseUpdatedArr as $wareHouseUpdatedArrs)
        {		 
            $idForwarder=array();
            $idWareHouse=$kWHSSearch->getWareHouseDataById($wareHouseUpdatedArrs['id']);
            fwrite($f, "\n++++++++++++++++++Warehouse Id-:".print_r($idWareHouse,true)."+++++++++++++++++++++\n");
            if(!empty($idWareHouse))
            {
                foreach($idWareHouse as $idWareHouses)
                {
                    $toUTCOFFSET='';
                    //$fromflag=false;
                    //$toflag=false;
                    $szAvailableLocalTime='';
                    $szCutOffLocalTime='';
                    $cutoffTimeAry = explode(':',$idWareHouses['szCutOffLocalTime']);
                    $szCutOffLocalTime = $cutoffTimeAry[0]+($cutoffTimeAry[1]/60) ;

                    $availabelTimeAry = explode(':',$idWareHouses['szAvailableLocalTime']);
                    $szAvailableLocalTime = $availabelTimeAry[0]+($availabelTimeAry[1]/60) ;

                    $localtime=$szAvailableLocalTime-$szCutOffLocalTime;

                    if($idWareHouses['idWarehouseFrom']==$wareHouseUpdatedArrs['id'])
                    {
                        $idWareHouseTo=$idWareHouses['idWarehouseTo'];
                        //$fromflag=true;
                    }
                    else if($idWareHouses['idWarehouseTo']==$wareHouseUpdatedArrs['id'])
                    {
                        $idWareHouseTo=$idWareHouses['idWarehouseFrom'];
                        //$toflag=true;
                    } 
                    $kWHSSearch->load($idWareHouseTo);

                    $toUTCOFFSET=$kWHSSearch->szUTCOffset;

                    fwrite($f, "\n------------------Warehouse To Id-:".$idWareHouseTo."-----------------------\n");
                    fwrite($f, "\n------------------Warehouse To UTCOffset-:".$kWHSSearch->szUTCOffset."-----------------------\n");
                    fwrite($f, "\n------------------Warehouse From UTCOffset-:".$wareHouseUpdatedArrs['szUTCOffset']."-----------------------\n");

                    $diffUTVOffSet=$wareHouseUpdatedArrs['szUTCOffset']-$toUTCOFFSET;

                    $transit_time=($diffUTVOffSet)/1000/60/60;
                    $transittime=$transit_time+$localtime;
                    //echo $transittime."<br/>";
                    fwrite($f, "\n------------------transittime-:".$transittime."-----------------------\n");
                    $kWHSSearch->updateTransitTime($idWareHouseTo,$wareHouseUpdatedArrs['id'],$transittime); 
                }
            }
        }
        fwrite($f, "\n========================End Updating Transit Time Of Warehouses=========================\n\n");
    }
}
closeDB($newlink);
fwrite($f, "\n\n###################Cronjob End-:".date("d-m-Y h:i:s")."#######################\n");

function curl_get_file_contents($URL) 
{
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
    $contents = curl_exec($c);
    $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
    curl_close($c);
    if ($contents) return $contents;
    else return FALSE;
}

?>