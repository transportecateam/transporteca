<?php
//session_start();
/**
 * CRON JOB TO FIND OUT THE UPLOAD SERVICES 
 * NOT CONFIRMED WITHIN 
 * 80 DAYS AND 90 DAYS 
 * CORRESPONDINGLY
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
ini_set (max_execution_time,360000);
$newlink=getTransportecaDB();
if(!class_exists('cUploadBulkService'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/uploadBulkService.class.php");
}
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
}
$kUploadBulkService = new cUploadBulkService();
$count = $kUploadBulkService->FindOutUploadSevicesNotConfirmed80Days();
if(count($count>0))
{
	foreach($count as $num)
	{	
		$kUploadBulkService->sendEmailsToForwarderAdminAndPricingAfter80Days($num['idForwarder'],$num['dtCompleted']);
	}
}
$count90 = $kUploadBulkService->FindOutUploadSevicesNotConfirmed90Days();
if(count($count90)>0)
{
	foreach($count90 as $num90)
	{
		$kUploadBulkService->updateUploadService($num90['id']);
	}
}