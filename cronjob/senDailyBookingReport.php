<?php
//session_start();
/**
 * Send Booking Report Daily to Forwarders
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
 ini_set (max_execution_time,360000);

$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
}

$kForwarder = new cForwarder();
///daily
$forwarderArr=$kForwarder->getForwarderArr("daily");
//print_r($forwarderArr);
if(!empty($forwarderArr))
{
    $expectedStr='';
    $getExpectedServieces=$kForwarder->getExpectedServieces('daily');
    if($getExpectedServieces)
    {
        foreach($getExpectedServieces as $getExpectedServiece)
        {
            $expect_cargo_volume = format_volume((float)$getExpectedServiece['fCargoVolume']);
            $expect_cargo_weight = get_formated_cargo_measure((float)($getExpectedServiece['fCargoWeight']/1000)) ;
            $expectedStr .= $getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." requests - total ".$expect_cargo_volume." cbm / ".$expect_cargo_weight." mt <br />";	
        }
    }
    foreach($forwarderArr as $forwarderArrs)
    {
        $getAllBookingForForwarder=array();
        $getAllBookingForForwarder=$kForwarder->getAllBookingForForwarder($forwarderArrs['id'],'daily');
        $replace_ary['szSubject']="Daily booking report from Transporteca";
        $replace_ary['szBookingFreq']="24 Hours";
        $idRoleAry=array('1','2');
        $replace_ary['szExpectedServices']=$expectedStr."<br /><br />";
        $getEmailArr=$kForwarder->getAllForwardersContactEmail($forwarderArrs['id'],$idRoleAry);
        if($kForwarder->checkForwarderOnlineStatus($forwarderArrs['id']))
        {
            $replace_ary['szOffline'] ='';
        }
        else
        {
            $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
        }
        if(!empty($getAllBookingForForwarder))
        {
            $str='';
            foreach($getAllBookingForForwarder as $getAllBookingForForwarders)
            {
                $cargo_volume = format_volume((float)$getAllBookingForForwarders['fCargoVolume']);
                $cargo_weight = get_formated_cargo_measure((float)($getAllBookingForForwarders['fCargoWeight']/1000)) ;
                $str .=$getAllBookingForForwarders['szBookingRef'].": ".$cargo_volume." cbm / ".$cargo_weight." mt from ".$getAllBookingForForwarders['szOriginCountry']." to ".$getAllBookingForForwarders['szDestinationCountry']."<br />";
            }
            if($expectedStr!='')
            {
                $message = str_replace('szBookingFreq', $replace_ary['szBookingFreq'], __BOOKING_EXPECTED_SERVICES_MESSAGE__);
                $replace_ary['szMessage']=$message."<br /><br />";
            }
            else
            {
                $replace_ary['szMessage']='';
            }
            $replace_ary['szContralUrl']=$forwarderArrs['szControlPanelUrl']."/booking/";
            $replace_ary['szBookingDetail']=$str;
            createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$getEmailArr,$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
        }
        else
        {
            if($expectedStr!='')
            {
                $replace_ary['szMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE__."<br /><br />";
                $replace_ary['szSecondMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__."<br /><br />";
            }
            else
            {
                $replace_ary['szMessage']='';
                $replace_ary['szSecondMessage']='';
            }
            createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$getEmailArr,$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
        }
    }
}

//weekly
if(date('D')=='Mon')
{
	$forwarderArr=array();
	$forwarderArr=$kForwarder->getForwarderArr("weekly");
	if(!empty($forwarderArr))
	{
		$expectedStr='';
		$getExpectedServieces=$kForwarder->getExpectedServieces('weekly');
		if($getExpectedServieces)
		{
			foreach($getExpectedServieces as $getExpectedServiece)
			{
				$expect_cargo_volume = format_volume((float)$getExpectedServiece['fCargoVolume']);
				$expect_cargo_weight = get_formated_cargo_measure((float)($getExpectedServiece['fCargoWeight']/1000)) ;
				$expectedStr .=	$getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." requests - total ".$expect_cargo_volume." cbm / ".$expect_cargo_weight." mt <br />";	
			}
		}
		foreach($forwarderArr as $forwarderArrs)
		{
			$getAllBookingForForwarder=array();
			$getAllBookingForForwarder=$kForwarder->getAllBookingForForwarder($forwarderArrs['id'],'weekly');
			$replace_ary['szSubject']="Weekly booking report from Transporteca";
			$replace_ary['szBookingFreq']="7 Days";
			$idRoleAry=array('1','2');
			$replace_ary['szExpectedServices']=$expectedStr."<br /><br />";
			$getEmailArr=$kForwarder->getAllForwardersContactEmail($forwarderArrs['id'],$idRoleAry);
			if($kForwarder->checkForwarderOnlineStatus($forwarderArrs['id']))
			{
				$replace_ary['szOffline'] ='';
			}
			else
			{
				$replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
			}
			if(!empty($getAllBookingForForwarder))
			{
                            $str='';
                            foreach($getAllBookingForForwarder as $getAllBookingForForwarders)
                            {
                                $cargo_volume = format_volume((float)$getAllBookingForForwarders['fCargoVolume']);
                                $cargo_weight = get_formated_cargo_measure((float)($getAllBookingForForwarders['fCargoWeight']/1000)) ;
                                $str .=$getAllBookingForForwarders['szBookingRef'].": ".$cargo_volume." cbm / ".$cargo_weight." mt from ".$getAllBookingForForwarders['szOriginCountry']." to ".$getAllBookingForForwarders['szDestinationCountry']."<br />";
                            }
                            if($expectedStr!='')
                            {
                                $message = str_replace('szBookingFreq', $replace_ary['szBookingFreq'], __BOOKING_EXPECTED_SERVICES_MESSAGE__);
                                $replace_ary['szMessage']=$message."<br /><br />";
                            }
                            else
                            {
                                $replace_ary['szMessage']='';
                            }
                            $replace_ary['szContralUrl']=$forwarderArrs['szControlPanelUrl']."/booking/";
                            $replace_ary['szBookingDetail']=$str;
                            createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$getEmailArr,$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
			}
			else
			{
                            if($expectedStr!='')
                            {
                                $replace_ary['szMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE__."<br /><br />";
                                $replace_ary['szSecondMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__."<br /><br />";
                            }
                            else
                            {
                                $replace_ary['szMessage']='';
                                $replace_ary['szSecondMessage']='';
                            }
                            //print_r($replace_ary);

                            createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$getEmailArr,$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
			}
		}
	}
}

////monthly
if(date('d')=='03')
{
    $forwarderArr=array();
    $forwarderArr=$kForwarder->getForwarderArr("monthly");
    if(!empty($forwarderArr))
    {
        $expectedStr='';
        $getExpectedServieces=$kForwarder->getExpectedServieces('monthly');
        if($getExpectedServieces)
        {
            foreach($getExpectedServieces as $getExpectedServiece)
            {
                $expect_cargo_volume = format_volume((float)$getExpectedServiece['fCargoVolume']);
                $expect_cargo_weight = get_formated_cargo_measure((float)($getExpectedServiece['fCargoWeight']/1000)) ;
                $expectedStr .=	$getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." requests - total ".$expect_cargo_volume." cbm / ".$expect_cargo_weight." mt <br />";	
            }
        }
        foreach($forwarderArr as $forwarderArrs)
        {
            $getAllBookingForForwarder=array();
            $getAllBookingForForwarder=$kForwarder->getAllBookingForForwarder($forwarderArrs['id'],'monthly');
            $replace_ary['szSubject']="Monthly booking report from Transporteca";
            $replace_ary['szBookingFreq']="Monthly";
            $idRoleAry=array('1','2');
            $replace_ary['szExpectedServices']=$expectedStr."<br /><br />";
            $getEmailArr=$kForwarder->getAllForwardersContactEmail($forwarderArrs['id'],$idRoleAry);
            if($kForwarder->checkForwarderOnlineStatus($forwarderArrs['id']))
            {
                $replace_ary['szOffline'] ='';
            }
            else
            {
                $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
            }	
            if(!empty($getAllBookingForForwarder))
            {
                $str='';
                foreach($getAllBookingForForwarder as $getAllBookingForForwarders)
                {
                    $cargo_volume = format_volume((float)$getAllBookingForForwarders['fCargoVolume']);
                    $cargo_weight = get_formated_cargo_measure((float)($getAllBookingForForwarders['fCargoWeight']/1000)) ;
                    $str .=$getAllBookingForForwarders['szBookingRef'].": ".$cargo_volume." cbm / ".$cargo_weight." mt from ".$getAllBookingForForwarders['szOriginCountry']." to ".$getAllBookingForForwarders['szDestinationCountry']."<br />";
                }
                if($expectedStr!='')
                {
                    $message = str_replace('szBookingFreq', $replace_ary['szBookingFreq'], __BOOKING_EXPECTED_SERVICES_MESSAGE__);
                    $replace_ary['szMessage']=$message."<br /><br />";
                }
                else
                {
                    $replace_ary['szMessage']='';
                }
                $replace_ary['szContralUrl']=$forwarderArrs['szControlPanelUrl']."/booking/";
                $replace_ary['szBookingDetail']=$str;
                //print_r($replace_ary);
                createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$getEmailArr,$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
            }
            else
            {
                if($expectedStr!='')
                {
                    $replace_ary['szMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE__."<br /><br />";
                    $replace_ary['szSecondMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__."<br /><br />";
                }
                else
                {
                    $replace_ary['szMessage']='';
                    $replace_ary['szSecondMessage']='';
                } 
                createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$getEmailArr,$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
            }
        }
    }
}
?>