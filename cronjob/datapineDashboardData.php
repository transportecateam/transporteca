<?php
//session_start();
/**
 * THIS CRON JOB IS NEED TO RUN ON 
 * MONTHLY BASIS 
 * AT THE STARTING OF EACH MONTH
 * Store The Costermer Feedback Score
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
 ini_set (max_execution_time,360000);

$newlink=getTransportecaDB();
if(!class_exists('cDashboardAdmin'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/admin.class.php");
	require_once(__APP_PATH_CLASSES__."/dashboardAdmin.class.php");
	require_once(__APP_PATH_CLASSES__."/nps.class.php");
}
$kAdmin = new cAdmin();
$kDashboardAdmin = new cDashboardAdmin();

$firstDate = 100; 
$currentDate = (int)date('H');

$kDashboardAdmin->insertDataPineData();


/*
 * Monthly Data Updated Functions
 */
if($currentDate<=$firstDate){
$kDashboardAdmin->insertDataPinePrevDayData();
}

/*
 * Datapine Finacial Data
 */

$kDashboardAdmin->insertDataPineFinacialData();
$kDashboardAdmin->insertDataPineFinacialPrevData(1);
?>
