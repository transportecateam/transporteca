<?php
//session_start();
/**
 * THIS CRON JOB IS NEED TO RUN ON 
 * MONTHLY BASIS 
 * AT THE STARTING OF EACH MONTH
 * Store The Costermer Feedback Score
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
 ini_set (max_execution_time,360000);

$newlink=getTransportecaDB();
if(!class_exists('cDashboardAdmin'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/admin.class.php");
	require_once(__APP_PATH_CLASSES__."/dashboardAdmin.class.php");
	require_once(__APP_PATH_CLASSES__."/nps.class.php");
}
$kAdmin = new cAdmin();
$kDashboardAdmin = new cDashboardAdmin();
$firstDate = 1; 
$currentDate = (int)date('d');

//$kAdmin->updateTblGraphDetails();
//$kDashboardAdmin->bookingTurnoverPerMonthCronJob();
	
$kNPS = new cNPS(); 
$date=date('Y-m',strtotime('-1 MONTH'));	
	

if($firstDate!=$currentDate && $currentDate!=0)
{
    $kAdmin->updateTblGraphDetails();
    $kDashboardAdmin->bookingTurnoverPerMonthCronJob();
    $kDashboardAdmin->updateLast3MonthsDataForDashboard();
    $kNPS->storePrevMonthCustomerData($date);
}
else
{
    //$kAdmin->updateTblGraphDetailsManually();
}