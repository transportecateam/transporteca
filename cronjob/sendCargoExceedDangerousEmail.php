<?php
/**
 * Send email to forwarder and customer when cargo exceed or dangerous
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
ini_set(max_execution_time,360000);

$newlink=getTransportecaDB();
if(!class_exists('cBooking'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/booking.class.php");
	require_once(__APP_PATH_CLASSES__."/config.class.php");
}

$kBooking = new cBooking();
$kConfig= new cConfig();
$cargoExceedDangAry=array();
$cargoExceedDangAry=$kBooking->sendEmailToForwarderAndCustomerCargoDetail();

if(!empty($cargoExceedDangAry))
{
	foreach($cargoExceedDangAry as $cargoExceedDangArrs)
	{
		$forwarderArr=array();	
		$postSearchAry=array();
		$cargoDetailArr=array();
		$postSearchAry['idOriginCountry']=$cargoExceedDangArrs['idOriginCountry'];
		$postSearchAry['idDestinationCountry']=$cargoExceedDangArrs['idDestinationCountry'];
		
		///forwarder detail array////
		$forwarderArr=$kBooking->getAllForwardersBookingEmail($postSearchAry);
		
	
		///Cargo detail array////
		$cargoDetailArr=$kBooking->getAllCargoDetail($cargoExceedDangArrs['id']);
		$cargoDetailsStr='';
		if(!empty($cargoDetailArr))
		{
			$i=1;
			foreach($cargoDetailArr as $cargoDetailArrs)
			{
				$fLength = ceil($cargoDetailArrs['fLength']);
				$fWidth = ceil($cargoDetailArrs['fWidth']);
				$fHeight = ceil($cargoDetailArrs['fHeight']);
				$fWeight = ceil($cargoDetailArrs['fWeight']);
				$iQuantity = ceil($cargoDetailArrs['iQuantity']);
				$szDangerCargo = $cargoDetailArrs['iDG'];
				
				if((int)$szDangerCargo==1)
				{
					$szDangerCargo="Yes";
				}
				else
				{
					$szDangerCargo="No";
				}
				
				$idCargoMeasure = $cargoDetailArrs['idCargoMeasure'];
				$cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
                                $cargoMeasureAry=$cargoMeasureAry[$idCargoMeasure];
				$szCargoMeasure = $cargoMeasureAry[$idCargoMeasure]['szDescription'];
				
				$idWeightMeasure = $cargoDetailArrs['idWeightMeasure'] ;
				$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');
                                $weightMeasureAry=$weightMeasureAry[$idWeightMeasure];
				$szWeightMeasure = $weightMeasureAry[$idWeightMeasure]['szDescription'];
					
				
				if(!empty($cargoDetailsStr))
				{
					$cargoDetailsStr .= "<br />".$i.": ".$iQuantity." x ".number_format((float)$fLength).$szCargoMeasure." x ".number_format((float)$fWidth).$szCargoMeasure." x ".number_format((float)$fHeight).$szCargoMeasure." (LxWxH), ".number_format((float)$fWeight)." ".$szWeightMeasure." - dangerous cargo: ".$szDangerCargo;
				}
				else
				{
					$cargoDetailsStr = ''.$i.": ".$iQuantity." x ".number_format((float)$fLength).$szCargoMeasure." x ".number_format((float)$fWidth).$szCargoMeasure." x ".number_format((float)$fHeight).$szCargoMeasure." (LxWxH), ".number_format((float)$fWeight)." ".$szWeightMeasure." - dangerous cargo: ".$szDangerCargo;
				}
				++$i;
			}
		}
		$cc_string='';
		if((int)$cargoExceedDangArrs['iOriginCC']==1)
		{
			$cc_string = "customs clearance at origin";
		}		
		else if((int)$cargoExceedDangArrs['iDestinationCC']==2)
		{
			if(empty($cc_string))
			{
				$cc_string = "and customs clearance at origin";
			}
			else
			{
				$cc_string.= " and destination";
			}
		}
		else
		{
			$cc_string = 'None';
		}
		
		$szTimingText = '';
			
		// geting all Timing type 
		$timingTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_TIMING_TYPE__');
		if(!empty($timingTypeAry))
		{
			$szTimingText = $timingTypeAry[$cargoExceedDangArrs['idTimingType']]['szDescription'];
		}
		if($cargoExceedDangArrs['dtTiming']!='' && $cargoExceedDangArrs['dtTiming']!='0000-00-00 00:00:00')
			$szTimingText .=" ".date('d. F Y',strtotime($cargoExceedDangArrs['dtTiming']));
		else
			$szTimingText .=" ".date('d. F Y');
		
		///echo $cargoExceedDangArrs['idServiceType']."test";
		$serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
		$szServiceType = $serviceTypeAry[$cargoExceedDangArrs['idServiceType']]['szDescription'];
		
		//echo $szTimingText."<br/>";
		if(!empty($forwarderArr))
		{
		
			$replace_ary['szName'] = $cargoExceedDangArrs['szCustomerName'] ;			
			$replace_ary['szEmail'] = $cargoExceedDangArrs['szCustomerEmail'];
			if($cargoDetailArrs['iShipperConsignee']==1)
			{
				$replace_ary['szShipperConsigneeText'] = "The customer is the shipper";
			}
			else if($cargoDetailArrs['iShipperConsignee']==2)
			{
				$replace_ary['szShipperConsigneeText'] = "The customer is the consignee";
			}
			else
			{
				$replace_ary['szShipperConsigneeText'] = '';
			}
			
			$replace_ary['szCargoDetails'] = $cargoDetailsStr;
			$replace_ary['szServiceType'] = $szServiceType;
			$replace_ary['szAdditionalService'] = ucfirst($cc_string);
			$replace_ary['szTimingText'] = $szTimingText;
			$replace_ary['szOriginCountry'] = $cargoExceedDangArrs['szOriginCountry'];
			$replace_ary['szDestinationCountry'] = $cargoExceedDangArrs['szDestinationCountry'];
			$replace_ary['szOriginCity'] = $cargoExceedDangArrs['szOriginCity'];
			$replace_ary['szDetsinationCity'] = $cargoExceedDangArrs['szDestinationCity'];
			
			if($cargoExceedDangArrs['szType']==1)	
			{			
				$replace_ary['szTopNoticeText'] = "Transporteca has received a shipping enquiry exceeding LCL dimensions or weight in a trade lane you currently offer. The user has requested that we forward the shipping requirements to all our associated freight forwarders who currently have services from ".$cargoExceedDangArrs['szOriginCountry']." to ".$cargoExceedDangArrs['szDestinationCountry'].", and agreed that you can revert directly with a quote if you are interested. Please find below the details submitted by the user on www.transporteca.com: ";
				$replace_ary['szNoticeText'] = "You receive this e-mail because you are registered on your Transporteca Control Panel and have an active LCL service from ".$cargoExceedDangArrs['szOriginCountry']." to ".$cargoExceedDangArrs['szDestinationCountry'].".";
				$replace_ary['szBottomNoticeText'] = " Transporteca has no further details of this quote request and will not be able to answer any questions related to it. You should not keep Transporteca in copy of future correspondence related to this quote request.";
				
			}
			else if($cargoExceedDangArrs['szType']==2)
			{
				$replace_ary['szTopNoticeText'] = "Transporteca has received a shipping enquiry for dangerous goods in a trade lane you currently offer. The user has requested that we forward the shipping requirements to all our associated freight forwarders who currently have services from ".$cargoExceedDangArrs['szOriginCountry']." to ".$cargoExceedDangArrs['szDestinationCountry'].", and agreed that you can revert directly with a quote if you are interested. Please find below the details submitted by the user on www.transporteca.com: ";
				$replace_ary['szNoticeText'] = "You receive this e-mail because you are registered on your Transporteca Control Panel and have an active LCL service from ".$cargoExceedDangArrs['szOriginCountry']." to ".$cargoExceedDangArrs['szDestinationCountry'].".";
				$replace_ary['szBottomNoticeText'] = " Transporteca has no further details of this quote request and will not be able to answer any questions related to it. You should not keep Transporteca in copy of future correspondence related to this quote request.";
					
			}
			
			foreach($forwarderArr as $forwarderArrs)
			{
		   
				$replace_ary['szDisplayName'] = $forwarderArrs['szForwarderName'];
				if($cargoExceedDangArrs['szType']==1)	
				{
					createEmail(__CARGO_EXCEED_EMAIL__, $replace_ary,$forwarderArrs['szEmail'], __CARGO_EXCEED_EMAIL_SUBJECT__, __STORE_SUPPORT_EMAIL__,$forwarderArrs['idForwarder'], 'feedback@transporteca.com',__FLAG_FOR_FORWARDER__);
				}
				else if($cargoExceedDangArrs['szType']==2)
				{
					createEmail(__DANGEROUS_CARGO_EMAIL__, $replace_ary,$forwarderArrs['szEmail'], __DANGEROUS_CARGO_EMAIL_SUBJECT__, __STORE_SUPPORT_EMAIL__,$forwarderArrs['idForwarder'], 'feedback@transporteca.com',__FLAG_FOR_FORWARDER__);
				}
			}
			
			if($cargoExceedDangArrs['szType']==1)	
			{
				$replace_ary['szDisplayName'] = $replace_ary['szName'] ;
				$replace_ary['szTopNoticeText'] = "Thank you for your enquire on Transporteca. We have sent following message to all our associated freight forwarders with services from ".$cargoExceedDangArrs['szOriginCountry']." to ".$cargoExceedDangArrs['szDestinationCountry'].". Note that Transporteca does not guarantee a response: ";
				$replace_ary['szNoticeText'] = '';
				$replace_ary['szBottomNoticeText'] = '';
				createEmail(__CARGO_EXCEED_EMAIL__, $replace_ary,$cargoExceedDangArrs['szCustomerEmail'], __CARGO_EXCEED_EMAIL_SUBJECT__, __STORE_SUPPORT_EMAIL__,'', 'feedback@transporteca.com',__FLAG_FOR_CUSTOMER__);
			}	
			else if($cargoExceedDangArrs['szType']==2)
			{
				$replace_ary['szDisplayName'] = $replace_ary['szName'];
				$replace_ary['szTopNoticeText'] = "Thank you for your enquire on Transporteca. We have sent following message to all our associated freight forwarders with services from ".$cargoExceedDangArrs['szOriginCountry']." to ".$cargoExceedDangArrs['szDestinationCountry'].". Note that Transporteca does not guarantee a response:";
				$replace_ary['szNoticeText'] = '';
				$replace_ary['szBottomNoticeText'] = '';
				createEmail(__DANGEROUS_CARGO_EMAIL__, $replace_ary,$cargoExceedDangArrs['szCustomerEmail'], __DANGEROUS_CARGO_EMAIL_SUBJECT__, __STORE_SUPPORT_EMAIL__,'', 'feedback@transporteca.com',__FLAG_FOR_CUSTOMER__);
				
			}
		}
		
		$kBooking->updateCargoExceedDangerousNotificationSent($cargoExceedDangArrs['id']);
	}
}
//print_r($cargoExceedDangAry);
?>