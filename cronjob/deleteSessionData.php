<?php
//session_start();
/**
 * Deleting session data cronjob
 * It will delete data from the table tblsessions created 7 days before
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$newlink=getTransportecaDB();
if(!class_exists('cConfig'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/config.class.php");
}

$kConfig = new cConfig();

$kConfig->deleteSessionData();
closeDB($newlink);
?>