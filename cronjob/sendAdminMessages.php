<?php
/**
 * Send Admin Messages To Customer And Forwarder
 * Hourly
 * 
 */

 if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_once('functions.php');
 ini_set (max_execution_time,360000);

$newlink=getTransportecaDB();

if(!class_exists('cAdmin'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/admin.class.php");
	require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
}

$kWHSSearch = new cWHSSearch();
$fromEmail=$kWHSSearch->getManageMentVariableByDescription('__SEND_UPDATES_CUSTOMER_EMAIL__');
		
$kAdmin = new cAdmin();

$allDataArr=$kAdmin->getAllDataAdminEmailSend();
//print_r($allDataArr);
if(!empty($allDataArr))
{
	foreach($allDataArr as $allDataArrs)
	{
	
			 ob_start();  
    		include(__APP_PATH_ROOT__.'/layout/email_header.php');
   			$message = ob_get_clean();
   			
   			$message .=$allDataArrs['szMessage'];
			
		     ob_start();
		    include(__APP_PATH_ROOT__.'/layout/email_footer.php');
		    $message .= ob_get_clean();
		    //$allDataArrs['szEmail']="ashish@whiz-solutions.com";
		    $kAdmin->updateEmailSendFlag($allDataArrs['id']);
		    sendBulkEmail($allDataArrs['szEmail'],$fromEmail,$allDataArrs['szSubject'],$message,$fromEmail,$allDataArrs['idUser'],2);
	}
}

?>