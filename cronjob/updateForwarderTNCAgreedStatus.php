<?php
//session_start();
/**
 * Update Forwarder Term And Condition Agreed Status
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
ini_set (max_execution_time,360000);
$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
}
$kForwarder=new cForwarder();

$date=date('Y-m-d',strtotime('- 30 DAY'));

$forwarderTCArr=$kForwarder->getForwarderAgreedTC($date);

	if(!empty($forwarderTCArr))
	{		
		for($k=0;$k<count($forwarderTCArr);++$k)
		{		
			$kForwarder->updateForwarderTNCAgreedFlag($forwarderTCArr[$k]);
		}
	}
?>