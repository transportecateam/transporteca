<?php
//session_start();
/**
 * Currency conversion cronjob
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$newlink=getTransportecaDB();
if(!class_exists('cBooking'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/booking.class.php");
}
$kBooking=new cBooking();
$bookingAry=array();
$bookingAry = $kBooking->getAllBookingByRemindToRate('5');
print_r($bookingAry);



$bookingAry10Days=array();
$bookingAry10Days = $kBooking->getAllBookingByRemindToRate('10');
//print_r($bookingAry10Days);


$f = fopen(__APP_PATH_LOGS__."/sendForwarderRatingEmail.log", "a");
fwrite($f, "\n\n###################Cronjob Started 5 days-:".date("d-m-Y h:i:s")."#######################\n");

if(!empty($bookingAry))
{
    fwrite($f, "\n------------------Booking Id Array-:".print_r($bookingAry,true)."-----------------------\n");
    foreach($bookingAry as $bookingArys)
    {
        fwrite($f, "\n------------------Start Sending Email For Booking Id-:".$bookingArys['id']."-----------------------\n");
        if(!$kBooking->isForwarderAlreadyRatedForBooking($bookingArys['id']))
        {
            $textDateString='';
            if($bookingArys['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingArys['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingArys['idServiceType']==__SERVICE_TYPE_PTD__)
            {
                    $textDateString="delivery";
            }
            else
            {
                    $textDateString="available";
            }
            $replace_ary = array();
            $replace_ary['idUser'] = $bookingArys['idUser'];
            $replace_ary['szFirstName'] = $bookingArys['szFirstName'];
            $replace_ary['szLastName'] = $bookingArys['szLastName'];
            $replace_ary['szEmail'] = $bookingArys['szEmail'];
            $replace_ary['szBookingRef'] = $bookingArys['szBookingRef'];
            $replace_ary['szForwarderDisplayName'] = $bookingArys['szForwarderDispName'];

            $replace_ary['szDateString'] = $textDateString;

            $replace_ary['iAdminFlag'] = 1;
            $replace_ary['idBooking'] = $bookingArys['id'];

            $bookingArys['dtAvailable']=date('Y-m-d',strtotime($bookingArys['dtAvailable']));
            $dtAvailableDateArr = explode("-",$bookingArys['dtAvailable']);
           
            $szAvailableMonthName = getMonthName($bookingArys['iBookingLanguage'],$dtAvailableDateArr[1],true);
            $dtAvailable = $dtAvailableDateArr[2]." ".$szAvailableMonthName;
            
            $bookingArys['dtBookingConfirmed']=date('Y-m-d',strtotime($bookingArys['dtBookingConfirmed']));
            $dtBookingConfirmedArr = explode("-",$bookingArys['dtBookingConfirmed']);
            $szBookingConfirmedMonthName = getMonthName($bookingArys['iBookingLanguage'],$dtBookingConfirmedArr[1],true);
            $dtBookingConfirmed = $dtBookingConfirmedArr[2]." ".$szBookingConfirmedMonthName;

            //$replace_ary['szDateString'] = $textDateString;

            $replace_ary['dtAvailableDate'] = $dtAvailable;
            $replace_ary['dtBookingDate'] = $dtBookingConfirmed;
            $url=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/myBooking/".$bookingArys['id']."__".md5(time())."/";
            //echo $url."<br/><br/>";
            $replace_ary['szFeedBackPageLink'] = "<a href='".$url."'> RATE ".strtoupper($bookingArys['szForwarderDispName'])."</a>";
            $replace_ary['szFeedBackPage_LinkCP']=$url;
            $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
            $replace_ary['iBookingLanguage'] = $bookingArys['iBookingLanguage'] ;
            $bookedArr=implode(";",$replace_ary);
            //$replace_ary['szEmail']="ashish@whiz-solutions.com";
            fwrite($f, "\n------------------Booking Customer Details-:".$bookedArr."-----------------------\n");
            createEmail(__RATE_FORWARDER__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
            fwrite($f, "\n------------------Email Send Successfully-----------------------\n");
        }
        fwrite($f, "\n------------------End Sending Email For Booking Id-:".$bookingArys['id']."-----------------------\n");
    }
}

fwrite($f, "\n\n###################Cronjob End 5 days-:".date("d-m-Y h:i:s")."#######################\n");


$f = fopen(__APP_PATH_LOGS__."/sendForwarderRatingEmail.log", "a");
fwrite($f, "\n\n###################Cronjob Started 10 Days-:".date("d-m-Y h:i:s")."#######################\n");

if(!empty($bookingAry10Days))
{
	fwrite($f, "\n------------------Booking Id Array-:".print_r($bookingAry10Days,true)."-----------------------\n");
	foreach($bookingAry10Days as $bookingAry10Day)
	{
		fwrite($f, "\n------------------Start Sending Email For Booking Id-:".$bookingAry10Day['id']."-----------------------\n");
		if(!$kBooking->isForwarderAlreadyRatedForBooking($bookingAry10Day['id']))
		{
			$textDateString='';
			if($bookingAry10Day['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingAry10Day['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingAry10Day['idServiceType']==__SERVICE_TYPE_PTD__)
			{
				$textDateString="delivery";
			}
			else
			{
				$textDateString="available";
			}
			$replace_ary = array();
			$replace_ary['idUser'] = $bookingAry10Day['idUser'];
			$replace_ary['szFirstName'] = $bookingAry10Day['szFirstName'];
			$replace_ary['szLastName'] = $bookingAry10Day['szLastName'];
			$replace_ary['szEmail'] = $bookingAry10Day['szEmail'];
			$replace_ary['szBookingRef'] = $bookingAry10Day['szBookingRef'];
			$replace_ary['szForwarderDisplayName'] = $bookingAry10Day['szForwarderDispName'];
			
			//$replace_ary['szDateString'] = $textDateString;
			
			$replace_ary['iAdminFlag'] = 1;
            $replace_ary['idBooking'] = $bookingAry10Day['id'];
            
            $bookingAry10Day['dtAvailable']=date('Y-m-d',strtotime($bookingAry10Day['dtAvailable']));
            $dtAvailableDateArr = explode("-",$bookingAry10Day['dtAvailable']);
            $szAvailableMonthName = getMonthName($bookingAry10Day['iBookingLanguage'],$dtAvailableDateArr[1],true);
            $dtAvailable = $dtAvailableDateArr[2]." ".$szAvailableMonthName;
                        
            $bookingAry10Day['dtBookingConfirmed']=date('Y-m-d',strtotime($bookingAry10Day['dtBookingConfirmed']));
            $dtBookingConfirmedArr = explode("-",$bookingAry10Day['dtBookingConfirmed']);
            $szBookingConfirmedMonthName = getMonthName($bookingAry10Day['iBookingLanguage'],$dtBookingConfirmedArr[1],true);
            $dtBookingConfirmed = $dtBookingConfirmedArr[2]." ".$szBookingConfirmedMonthName;
                        
            //$replace_ary['szDateString'] = $textDateString;
            
            $replace_ary['dtAvailableDate'] = $dtAvailable;
            $replace_ary['dtBookingDate'] = $dtBookingConfirmed;
			$replace_ary['iBookingLanguage'] = $bookingAry10Day['iBookingLanguage'] ;
			$url=__MAIN_SITE_HOME_PAGE_SECURE_URL__."/myBooking/".$bookingAry10Day['id']."__".md5(time())."/";
			//echo $url."<br/><br/>";
			$replace_ary['szFeedBackPageLink'] = "<a href='".$url."'> RATE ".strtoupper($bookingAry10Day['szForwarderDispName'])." </a>";
			$replace_ary['szFeedBackPage_LinkCP']=$url;
			
            $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
            $bookedArr=implode(";",$replace_ary);
           // $replace_ary['szEmail']="ashish@whiz-solutions.com";
           // print_r($replace_ary);
            fwrite($f, "\n------------------Booking Customer Details-:".$bookedArr."-----------------------\n");
          	createEmail(__RATE_FORWARDER_10DAYS__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
            fwrite($f, "\n------------------Email Send Successfully-----------------------\n");
		}
		fwrite($f, "\n------------------End Sending Email For Booking Id-:".$bookingAry10Day['id']."-----------------------\n");
	}
}

fwrite($f, "\n\n###################Cronjob End 10 days-:".date("d-m-Y h:i:s")."#######################\n");
closeDB($newlink);
?>