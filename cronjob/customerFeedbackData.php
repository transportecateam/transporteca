<?php
/**
 * THIS CRON JOB IS NEED TO RUN ON
 * Monthly
 * Store The Costermer Feedback Score
 *
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
 ini_set (max_execution_time,360000);

$newlink=getTransportecaDB();
if(!class_exists('cNPS'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/nps.class.php");
}  

$kNPS = new cNPS();

$date=date('Y-m',strtotime('-1 MONTH'));

$firstDate = 1; 
$currentDate = (int)date('d');
if($firstDate==$currentDate && $currentDate!=0)
{
	$kNPS->storePrevMonthCustomerData($date);
}
?>