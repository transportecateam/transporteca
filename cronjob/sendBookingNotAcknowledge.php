<?php
/**
 * for sending mail to all forwarders who have not acknowledged booking after 3 days
 * THIS CRONJOB RUNS EVERYDAY AT 3.30 PM
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once('functions.php');
ini_set (max_execution_time,360000);
$newlink=getTransportecaDB();
require_once(__APP_PATH_CLASSES__."/error.class.php");
require_once(__APP_PATH_CLASSES__."/database.class.php");
if(!class_exists('cForwarder'))
{
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
}
if(!class_exists('cBooking'))
{
    require_once(__APP_PATH_CLASSES__."/booking.class.php");
}

if(!class_exists('cCourierServices'))
{
    require_once(__APP_PATH_CLASSES__."/courier.class.php");
}
$kForwarder = new cForwarder(); 
$kBooking = new cBooking();
$forwarderArr = array();
$forwarderArr = $kForwarder->getForwarderArr();

$kConfig = new cConfig();
$transportModeListAry = array();
$transportModeListAry = $kConfig->getAllTransportMode(false,false,true);
        
if($forwarderArr != array())
{
    foreach($forwarderArr as $forwarderArrs)
    {
        //$kForwarder->load($idForwarder);
        $idForwarder = $forwarderArrs['id'];
        $replace_ary = array();
        $controlPanelURL = $forwarderArrs['szControlPanelUrl'];
        $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($idForwarder);
        $iProfitType = $forwarderArrs['iProfitType'];
        
        $replace_ary['szForwarderDispName'] = $forwarderArrs['szDisplayName'];			
        $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$controlPanelURL.'/T&C/">Terms & Conditions</a>';	

        $replace_ary['szFirstName'] = ($acceptedByUserAry['szFirstName']);
        $replace_ary['szLastName'] = ($acceptedByUserAry['szLastName']);
        $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));
        $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
        
        if($iProfitType==1)
        {
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
            $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
            $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
            $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
            $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
            $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
        }
        else
        {
            $replace_ary['szTermsConditionForReferralForwarderText']='';
        }

        //FETCHING OUT ALL BOOKING DETAILS CORRESPONDING TO THAT FORWARDER
        //$bookingDataArr=$kBooking->bookingNotAcknowledgedAfterThreeDays($idForwarder);
          
        if($bookingDataArr != array())
        {
            foreach($bookingDataArr as $bookingData)
            { 	  
                $bookingId = $bookingData['idBooking'];
                $szControlPanelUrl = "http://".$controlPanelURL."/booking/".$bookingId."__".md5(time())."/";
                $bookingDataArr=$kBooking->getBookingDetailsCountryName($bookingId);

                $kConfig = new cConfig();
                $szOriginCountryAry = array();
                $szDestinationCountryAry = array();

                if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__ || $bookingDataArr['iQuotesStatus']==5)
                {
                    $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry']);
                    $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry']) ;

                    $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
                    $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	

                    $szOriginCountryStr = '';
                    if(!empty($bookingDataArr['szShipperCity']))
                    {
                        $szOriginCountryStr = $bookingDataArr['szShipperCity'].", ";
                    }
                    $szOriginCountryStr .= $szOriginCountry ;

                    $szDestinationCountryStr = '';
                    if(!empty($bookingDataArr['szConsigneeCity']))
                    {
                        $szDestinationCountryStr = $bookingDataArr['szConsigneeCity'].", ";
                    }
                    $szDestinationCountryStr .= $szDestinationCountry ;
                }
                else
                {
                    $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseFromCountry']);
                    $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseToCountry']) ;

                    $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idWarehouseFromCountry']]['szCountryName'] ;
                    $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idWarehouseToCountry']]['szCountryName'] ;	

                    $szOriginCountryStr = '';
                    if(!empty($bookingDataArr['szWarehouseFromCity']))
                    {
                        $szOriginCountryStr = $bookingDataArr['szWarehouseFromCity'].", ";
                    }
                    $szOriginCountryStr .= $szOriginCountry ;


                    $szDestinationCountryStr = '';
                    if(!empty($bookingDataArr['szWarehouseToCity']))
                    {
                        $szDestinationCountryStr = $bookingDataArr['szWarehouseToCity'].", ";
                    }
                    $szDestinationCountryStr .= $szDestinationCountry ;
                }


                $replace_ary['szOriginCountry'] = $szOriginCountryStr;
                $replace_ary['szDestinationCountry'] = $szDestinationCountryStr;
                $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef'];
                $replace_ary['szUrl'] = $szControlPanelUrl;
                $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'">CLICK HERE</a>';
                 
                if($bookingData['idTransportMode']>0)
                {
                    $replace_ary['szBookingType'] = $transportModeListAry[$bookingData['idTransportMode']]['szLongName'] ; 
                }
                else
                {
                    $replace_ary['szBookingType'] = 'Seafreight';
                } 
                
                /*if($bookingData['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
                {
                    $idForwarderContactRole = __AIR_FREIGHT_BOOKING_PROFILE_ID__ ;
                }
                else if($bookingData['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__)
                {
                    $idForwarderContactRole = __ROAD_FREIGHT_BOOKING_PROFILE_ID__ ;
                }
                else if($bookingData['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
                {
                    $idForwarderContactRole = __COURIER_FREIGHT_BOOKING_PROFILE_ID__ ;
                }
                else
                {
                    $idForwarderContactRole = __CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__ ;
                }
                $email_address_forwarders_Arrs=$kForwarder->getForwarderCustomerServiceEmail($idForwarder,$idForwarderContactRole);
                */
                $searchDataAry = array();
                $searchDataAry['idForwarder'] = $idForwarder;
                $searchDataAry['idTransportMode'] = $bookingData['idTransportMode'];
                $searchDataAry['idOriginCountry'] = $bookingData['idOriginCountry'];
                $searchDataAry['idDestinationCountry'] = $bookingData['idDestinationCountry'];
                $searchDataAry['iBookingSentFlag'] = '1';

                $forwarderContactAry = array(); 
                $kConfig = new cConfig();
                $forwarderContactAry = $kConfig->getForwarderContact($searchDataAry);
    
                createEmail('__FORWARDER_BOOKING_CONFIRMATION_REMINDER_EMAIL__', $replace_ary,$forwarderContactAry ,' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,false);
            }
        }
        
        $courierBookingArr=$kBooking->courierBookingNotAcknowledgedAfterOneDays($idForwarder);
        
        if(!empty($courierBookingArr))
        {
            foreach($courierBookingArr as $courierBookingArrs)
            {
                
                if($courierBookingArrs['iCourierBookingType']==1)
                {
                    $szBookingType="Courier";
                }
                else
                {
                    $szBookingType="Courier";
                }    
                 $bookingId = $courierBookingArrs['idBooking'];
                $szControlPanelUrl = "http://".$controlPanelURL."/booking/".$bookingId."__".md5(time())."/";
                 $bookingArr=$kBooking->getExtendedBookingDetails($bookingId);
                
                $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
                $replace_ary['szUrl'] = $szControlPanelUrl;
                    
                 $kCourierServices = new cCourierServices();   
                 $quotesAry =$kCourierServices->getAllBookingQuotesByFileByBookingId($bookingId);

               $szControlPanelLabelUrl='http://'.$controlPanelURL."/pendingQuotes/".$quotesAry[1]['iQuotationId']."__".md5(time())."/";


               $replace_ary['szLabelLink'] = '<a href="'.$szControlPanelLabelUrl.'" target="_blank"> UPLOAD LABLES AND TRACKING NUMBER HERE</a>';
               $replace_ary['szLabelUrl'] = $szControlPanelLabelUrl;

              $replace_ary['szBookingType']=$szBookingType;  
              $replace_ary['szBookingRef']=$bookingArr['szBookingRef'];
              $replace_ary['iBookingLanguage']=$bookingArr['iBookingLanguage'];
              $replace_ary['szOriginCountry']=$bookingArr['szOriginCountry'];
              $replace_ary['szDestinationCountry']=$bookingArr['szDestinationCountry'];
              $replace_ary['szForwarderDispName']=$bookingArr['szForwarderDispName'];
              
              
                $kConfig = new cConfig();
                $data = array();
                $data['idForwarder'] = $bookingArr['idForwarder'];
                $data['idTransportMode'] = $bookingArr['idTransportMode'];
                $data['idOriginCountry'] = $bookingArr['idOriginCountry'];
                $data['idDestinationCountry'] = $bookingArr['idDestinationCountry'];
                $data['iBookingSentFlag'] = '1';

                $kForwarder = new cForwarder();
                $kForwarder->load($bookingArr['idForwarder']);

                $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($bookingArr['idForwarder']);

                $forwarderContactAry = array();
                $forwarderContactAry = $kConfig->getForwarderContact($data);
                if(!empty($forwarderContactAry))
                {
                   foreach($forwarderContactAry as $forwarderContactArys)
                   {
                        $replace_ary['szFirstName'] = utf8_encode($acceptedByUserAry['szFirstName']);
                        $replace_ary['szLastName'] = utf8_encode($acceptedByUserAry['szLastName']);
                        $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));
                        $replace_ary['szEmail']=$forwarderContactArys;   

                        $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>';
                        
                        $kWhsSearch = new cWHSSearch();  
                        $forwarderBookingEmail = $kWhsSearch->getManageMentVariableByDescription('__FORWARDER_BOOKING_CONFIRMATION_EMAIL__');  
                        //$forwarderBookingEmail = __FINANCE_CONTACT_EMAIL__;
                        
                        createEmail(__COURIER_BOOKING_RECEIVED_CREATE_LABEL_REMINDER_FORWARDER_EMAIL__, $replace_ary, $replace_ary['szEmail'],'' ,$forwarderBookingEmail,$bookingArr['idForwarder'] , $forwarderBookingEmail,__FLAG_FOR_FORWARDER__);
                   }
                }

            }
        }
    }
}
	        //return true;        