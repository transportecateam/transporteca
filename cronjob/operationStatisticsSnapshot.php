<?php
//session_start();
/**
 * Currency conversion cronjob
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
ini_set ('max_execution_time',36000);
 
require_once(__APP_PATH_CLASSES__."/error.class.php");
require_once(__APP_PATH_CLASSES__."/database.class.php");
require_once(__APP_PATH_CLASSES__."/report.class.php"); 
require_once(__APP_PATH_CLASSES__."/config.class.php");  
        
ini_set('display_error','1');

include_once('functions.php');
$newlink=getTransportecaDB(); 
$kReport = new cReport(); 

$kReport->updateOperationStatisticsScreen();

closeDB($newlink);
?>