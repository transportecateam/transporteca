<?php 
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
ini_set (max_execution_time,360000);
$newlink=getTransportecaDB();
if(!class_exists('cChat'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php"); 
    require_once(__APP_PATH_CLASSES__."/chat.class.php");
    require_once(__APP_PATH_CLASSES__."/user.class.php");
    require_once(__APP_PATH_CLASSES__."/booking.class.php");
}
$kChat = new cChat();
$resultAry = $kChat->fetchChatTrascript(); 
closeDB($newlink);
?>