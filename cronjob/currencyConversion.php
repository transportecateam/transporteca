<?php
//session_start();
/**
 * Currency conversion cronjob
 * Delete the data where dtDate is greater than 188 days 
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

$newlink=getTransportecaDB();
if(!class_exists('cConfig'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/config.class.php");
}

$kConfig = new cConfig();
$kConfig->currencyConversion();

$date=date('Y-m-d',strtotime('- 180 DAY'));

$kConfig->deleteCurrencyConversionData($date);
//echo 
echo "Currency conversion rate successfully inserted.";
closeDB($newlink);
?>