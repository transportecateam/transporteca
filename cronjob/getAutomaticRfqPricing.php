<?php  
//session_start();
/**
 * Currency conversion cronjob
 */
ini_set ('max_execution_time',360000);
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/content_functions.php");
 
require_once(__APP_PATH_CLASSES__."/error.class.php");
require_once(__APP_PATH_CLASSES__."/database.class.php");
require_once(__APP_PATH_CLASSES__."/user.class.php"); 
require_once(__APP_PATH_CLASSES__."/config.class.php");
require_once(__APP_PATH_CLASSES__."/booking.class.php");
require_once(__APP_PATH_CLASSES__."/admin.class.php");
require_once(__APP_PATH_CLASSES__."/quote.class.php");
require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
    
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL);

include_once('functions.php');
  
$kQuote = new cQuote();
$kBooking = new cBooking();

$bookingDataAry = array();
$bookingDataAry = $kQuote->getAllVogabookingCreatedByAPI();
  
$kWhsSearch=new cWHSSearch();
$kConfig = new cConfig(); 

if(!empty($bookingDataAry))
{
    foreach($bookingDataAry as $bookingDataArys)
    {  
        try
        {
            $idBooking = $bookingDataArys['id'];  
            $postSearchAry = array();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            
            if(!empty($postSearchAry))
            { 
                $postSearchAry['idLandingPage'] = $postSearchAry['idDefaultLandingPage'];
                $searchResultAry = array();
                $searchResultAry = $kWhsSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
                $postSearchAry['iNumRecordFound'] = count($searchResultAry);
                $iNumRecordFound = count($searchResultAry);
                
                $postSearchAry['iNumRecordFound'] = $iNumRecordFound; 
                $kBooking->addSelectServiceData($postSearchAry);

                $bRecalculateFlag=false;
                if($iNumRecordFound>0)
                {
                    $idDefaultLandingPage = $postSearchAry['idDefaultLandingPage'];
                    $kConfig = new cConfig(); 
                    $kConfig->createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage,false,1);

                    $res_ary = array();
                    $res_ary['iCreateAutomaticQuote'] = "0";  
                    
                    $update_query = "";
                    if(!empty($res_ary))
                    {
                        foreach($res_ary as $key=>$value)
                        {
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    }
                    $update_query = rtrim($update_query,",");
                    if($kBooking->updateDraftBooking($update_query,$idBooking))
                    {

                    }  
                }
                else
                {
                    $idBookingFile =$postSearchAry['idFile'];
                    $res_ary = array();
                    $res_ary['iCreateAutomaticQuote'] = "0";  
                    $szInternalComment = $postSearchAry['szInternalComment'].PHP_EOL."No automatic pricing available";
                    $res_ary['szInternalComment'] = $szInternalComment;                    
                    $update_query = "";
                    if(!empty($res_ary))
                    {
                        foreach($res_ary as $key=>$value)
                        {
                            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    }
                    $update_query = rtrim($update_query,",");
                    if($kBooking->updateDraftBooking($update_query,$idBooking))
                    {

                    }
                    
                    $dtResponseTime = $kBooking->getRealNow();

                    $fileLogsAry = array(); 
                    $fileLogsAry['szTransportecaStatus'] = 'S1';
                    $fileLogsAry['szTransportecaTask'] = 'T1';
                    $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
                    $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime;
                    $fileLogsAry['iSearchType'] = 2;
                    if(!empty($fileLogsAry))
                    {
                        $file_log_query = "";
                        foreach($fileLogsAry as $key=>$value)
                        {
                            $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                        }
                    } 
                    $kBooking_new = new cBooking();
                    $file_log_query = rtrim($file_log_query,",");   
                    //echo $file_log_query;
                    $kBooking_new->updateBookingFileDetails($file_log_query,$idBookingFile);
                }
            } 
        } 
        catch (Exception $ex) 
        {
            echo "Exception: <br> ";
            print_R($ex);
        } 
    }
}