<?php
//session_start();
/**
 * Currency conversion cronjob
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    ini_set (max_execution_time,360000);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/pdf_functions.php" );
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" );
include_once('functions.php');
  
$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
    require_once(__APP_PATH_CLASSES__."/billing.class.php"); 
    require_once(__APP_PATH_CLASSES__."/admin.class.php"); 
}   

$kForwarder = new cForwarder();
$kBilling = new cBilling();
$kAdmin = new cAdmin();
/*
 echo "<br> Fetching all uppaid bookings...<br>";
 echo "<br> Processing...<br>";
 
 echo "<br>Due to some Internal server error we could not process your request. Please try again later.<br>";
 echo "<br> Developer Notes: ".md5(time());
 * 
 */
$automaticPayableForwarderAry = array();
$automaticPayableForwarderAry = $kBilling->getAllForwarderForAutomaticPayment();
  
if(!empty($automaticPayableForwarderAry))
{
    foreach($automaticPayableForwarderAry as $automaticPayableForwarderArys)
    { 
        $idForwarder = $automaticPayableForwarderArys['id'];
        $idForwarderCurrency = $automaticPayableForwarderArys['szCurrency'];
        $szDisplayName = $automaticPayableForwarderArys['szDisplayName'];
         
        $billingDetails = array();
        $serviceUploadPaymentArr = array();
        $billingDetails = $kAdmin->showEachForwarderPendingPayment($idForwarder,$idForwarderCurrency,true); 
        $serviceUploadPaymentArr = $kAdmin->showForwarderPendingPaymentServiceUpload($idForwarder,$idForwarderCurrency);

        /*
        * In Financial version 2 we have deleted the “Referral Fee Invoice” line and “Courier Booking and Label fee…” line and the “Transporteca handling fee…” line @Ajay @date 27-Feb-2017
        */
        $szStyle = 'style="display:none;"';
 ?>
    <h5>Pay <?php echo $szDisplayName?> now</h5>    	
    <table cellpadding="0"  cellspacing="0" border="0" class="format-1" width="100%" id="booking_table">
        <tr> 
            <th class="wds-24" style='text-align:left;' valign="top">Invoice date</th>
            <th class="wds-24" style='text-align:left;' valign="top">Invoice number</th>
            <th class="wds-24" style='text-align:left;' valign="top">Booking</th> 
            <th class="wds-24" style='text-align:right;' valign="top">Transfer</th>
        </tr>	 
    <?php 
        $confirm_button_flag=false;
        $ctr=1;
        $grandtotal=0;
        $totalReferralFee=0;
        $forwardertotal=0;
        $forwardertotal1=0;
        $i=1;
        $j=0;
        $szHandlingCurrencyStr = "";
        $handlingFeeVat=0;
        $forwarderTransactionAry = array();
        if(!empty($billingDetails))
        {	 
            foreach($billingDetails as $bills)
            {	 
                $ftoatl=0;
                $currencyValue='';	
                if($bills['szForwarderCurrency']!='')
                    $currencyValue = $bills['szForwarderCurrency'];
                else
                    $currencyValue = $bills['szCurrency'];

                $forwarderTransactionAry[] = $bills['id'];
                if($bills['iDebitCredit']==6 || $bills['iDebitCredit']==7)
                { 
                    $bookingLabelFee=0;
                    $fHandlingFeeForwarderCurrency = 0;
                    $fBookingLabelFeeRateROE=$bills['fForwarderExchangeRate'];
                    $fBookingLabelFeeCurrencyId=$bills['idForwarderCurrency'];

                    if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                    {
                        $bookingLabelFee = 0;
                    }
                    else if($bills['iCourierBooking']==1 && $bills['iCourierAgreementIncluded']==0)
                    { 
                        $kCourierServices = new cCourierServices(); 
                        $fBookingLabelFeeRate = $bills['fLabelFeeUSD']; 
                        if($fBookingLabelFeeCurrencyId==1)
                        {
                            $bookingLabelFee = $fBookingLabelFeeRate;
                        }
                        else
                        {                                                                
                           $bookingLabelFee = $fBookingLabelFeeRate/$fBookingLabelFeeRateROE; 
                        }
                    }

                    if($bills['iHandlingFeeApplicable']==1)
                    { 
                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                        {
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                            $fHandlingFeeForwarderCurrency = 0;
                            $fHandlingFeeForwarderCurrencyVatValue=0;
                        }
                        else
                        { 
                            if($bills['idForwarderCurrency']==1)
                            {
                                $fHandlingFeeForwarderCurrency = $bills['fTotalHandlingFeeUSD'];
                            }
                            else
                            {
                                $fHandlingFeeForwarderCurrency = ($bills['fTotalHandlingFeeUSD']/$fBookingLabelFeeRateROE);
                            }

                            $fHandlingFeeForwarderCurrencyVatValue=0;
                            if((float)$bills['fVATPercentage']>0)
                            {
                                $fHandlingFeeForwarderCurrencyVatValue=round((float)($fHandlingFeeForwarderCurrency*.01*$bills['fVATPercentage']),2);
                            } 
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalForwarderPriceWithoutHandlingFee'];
                        }
                    }
                    else
                    {
                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                        {
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                        }
                        else
                        {
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalPriceForwarderCurrency'];
                        }
                    }

                    if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                    {
                        //For Financial Version 2 booking we don't substract Referal amount from Self invoice amount as this is already substracted while calculation of services for booking.
                        $bills['fReferalAmount'] = 0;
                    } 
                    $ftoatl = round($fTotalPriceForwarderCurrencyWithoutHandlingFee,2) - (round($bills['fReferalAmount'],2)+ $bookingLabelFee);

                    $ftoatNewl = round($bills['fTotalPriceForwarderCurrency'],2) - (round($bills['fReferalAmount'],2) + $bookingLabelFee);
                    $forwardertotal1=$forwardertotal1-($ftoatNewl)+$fHandlingFeeForwarderCurrency;
                    $ftoatNewTransfer=$ftoatNewl-$fHandlingFeeForwarderCurrency;
                    $grandtotal = $grandtotal - (round($bills['fTotalPriceForwarderCurrency'],2));

                    $totalReferralFee = $totalReferralFee - round($bills['fReferalAmount'],2);
                    $szBookingRef_text = "Cancelled: ".$bills['szBookingRef'];
                    $bookingLabelFeeTotal = $bookingLabelFeeTotal - round($bookingLabelFee,2);
                    $fTotalHandlingFeeForwarderCurrency = $fTotalHandlingFeeForwarderCurrency - round($fHandlingFeeForwarderCurrency,2);

                    $szTotalAmountText = "(".$currencyValue." ".number_format((float)$ftoatl,2).")";

                    $fTotalPriceForwarderCurrency_text = "(".$currencyValue." ".number_format((float)$fTotalPriceForwarderCurrencyWithoutHandlingFee,2).")";
                    $fReferalAmount_text = "(".$currencyValue." ".number_format((float)$bills['fReferalAmount'],2).")";
                    $fReferalAmount = round($bills['fReferalAmount'],2);

                    $szHandlingCurrencyStr.= $szBookingRef_text." USD: ".$bills['fTotalHandlingFeeUSD']." XE: ".$bills['fForwarderExchangeRate']." FWD Curr: ".$fHandlingFeeForwarderCurrency." Total: ".$fTotalHandlingFeeForwarderCurrency."\\n";
                }
                else
                { 
                    $bookingLabelFee=0;
                    $fHandlingFeeForwarderCurrency = 0;
                    $fBookingLabelFeeRateROE=$bills['fForwarderExchangeRate'];
                    $fBookingLabelFeeCurrencyId=$bills['idForwarderCurrency'];

                    if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                    {
                        $bookingLabelFee = 0;
                        //For Financial Version 2 booking we don't substract Referal amount from Self invoice amount as this is already substracted while calculation of services for booking.
                        $bills['fReferalAmount'] = 0;
                    }
                    else if($bills['iCourierBooking']==1 && $bills['iCourierAgreementIncluded']==0)
                    {
                        $kCourierServices = new cCourierServices();
                        //$courierBookingArr=$kCourierServices->getCourierBookingData($bills['idBooking']);
                        $fBookingLabelFeeRate=$bills['fLabelFeeUSD']; 
                        if($fBookingLabelFeeCurrencyId==1)
                        {
                            $bookingLabelFee=$fBookingLabelFeeRate;
                        }
                        else
                        {                                                                
                           $bookingLabelFee=$fBookingLabelFeeRate/$fBookingLabelFeeRateROE;

                        }
                        //echo $bookingLabelFee."<br/>";
                    } 


                    if($bills['iHandlingFeeApplicable']==1)
                    {  
                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                        {
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                            $fHandlingFeeForwarderCurrency = 0;
                            $fHandlingFeeForwarderCurrencyVatValue=0;
                        }
                        else
                        {

                            if($bills['idForwarderCurrency']==1)
                            {
                                $fHandlingFeeForwarderCurrency = $bills['fTotalHandlingFeeUSD'];
                            }
                            else
                            {
                                $fHandlingFeeForwarderCurrency = round((float)($bills['fTotalHandlingFeeUSD']/$fBookingLabelFeeRateROE),2);
                            }
                            $fHandlingFeeForwarderCurrencyVatValue=0;
                            if((float)$bills['fVATPercentage']>0)
                            {
                                $fHandlingFeeForwarderCurrencyVatValue=round((float)($fHandlingFeeForwarderCurrency*.01*$bills['fVATPercentage']),2);
                            } 
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee=$bills['fTotalForwarderPriceWithoutHandlingFee'];
                        }  
                    }
                    else
                    {
                        if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                        {
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee = $bills['fTotalSelfInvoiceAmount'];
                        }
                        else
                        {
                            $fTotalPriceForwarderCurrencyWithoutHandlingFee=$bills['fTotalPriceForwarderCurrency'];
                        } 
                    }
                    $ftoatl = round($fTotalPriceForwarderCurrencyWithoutHandlingFee,2) - (round($bills['fReferalAmount'],2)+ $bookingLabelFee);

                    $ftoatNewl = round($bills['fTotalPriceForwarderCurrency'],2) - (round($bills['fReferalAmount'],2)+ $bookingLabelFee);
                    $forwardertotal1=$forwardertotal1+$ftoatl;
                    $ftoatNewTransfer=$ftoatl;


                    $grandtotal = $grandtotal+round($bills['fTotalPriceForwarderCurrency'],2);

                    $totalReferralFee = $totalReferralFee+round($bills['fReferalAmount'],2);
                    $bookingLabelFeeTotal=$bookingLabelFeeTotal+$bookingLabelFee;
                    $fTotalHandlingFeeForwarderCurrency = $fTotalHandlingFeeForwarderCurrency + round($fHandlingFeeForwarderCurrency,2);

                    $szBookingRef_text = $bills['szBookingRef'];
                    $fTotalPriceForwarderCurrency_text = $currencyValue." ".number_format((float)$fTotalPriceForwarderCurrencyWithoutHandlingFee,2);
                    $fReferalAmount_text = $currencyValue." ".number_format((float)$bills['fReferalAmount'],2);
                    $szTotalAmountText = $currencyValue." ".number_format((float)$ftoatl,2);

                    $fReferalAmount = round($bills['fReferalAmount'],2);
                    $szHandlingCurrencyStr .= $szBookingRef_text." USD: ".$bills['fTotalHandlingFeeUSD']." XE: ".$bills['fForwarderExchangeRate']." FWD Curr: ".$fHandlingFeeForwarderCurrency." Total: ".$fTotalHandlingFeeForwarderCurrency."\\n";
                } 
                $handlingFeeVat=$handlingFeeVat+$fHandlingFeeForwarderCurrencyVatValue;

                if($bills['iFinancialVersion']==__TRANSPORTECA_FINANCIAL_VERSION__)
                {
                    $szSelfInvoice = $bills['szSelfInvoice'];
                }
                else
                {
                    $szSelfInvoice = $bills['szCustomerInvoice'];
                }
                ?> 
                <tr align='center'> 
                    <td style='text-align:left;'><?=date('d-M',strtotime($bills['dtCreditOn']))?></td>
                    <td style='text-align:left;'><?=$szSelfInvoice?></td>
                    <td style='text-align:left;'><?=$szBookingRef_text?></td> 
                    <td style='text-align:right;'><?=$szTotalAmountText?></td>
                </tr>
                <?php
                ++$i;	
                ++$j;	
            } 

            $uploadServiceAmount='0.00';
            $uploadServiceAmount=round($serviceUploadPaymentArr[$check][$currencyValue],2);
            //echo $uploadServiceAmount."hello";
            if((float)$uploadServiceAmount>'0.00')
            {
                $grandtotal=$grandtotal-$uploadServiceAmount;
                $forwardertotal =$forwardertotal1-$uploadServiceAmount;
                $forwardertotal1 =$forwardertotal1;
                //echo $forwardertotal."forwarder";
            }
            else
            {
                $grandtotal=$grandtotal;
                $forwardertotal=$forwardertotal1;
                $forwardertotal1 =$forwardertotal1;
                $uploadServiceAmount='0.00';
                //echo $forwardertotal."forwarder1";
            }

            if((float)$bookingLabelFeeTotal>0.00)
            {
                $text_courierLabelFee=$currencyValue." ".number_format((float)$bookingLabelFeeTotal,2);
            }else
            {
                $bookingLabelFeeTotal1=str_replace("-","",$bookingLabelFeeTotal);
                $text_courierLabelFee="(".$currencyValue." ".number_format((float)$bookingLabelFeeTotal1,2).")";
            }

            if((float)$fTotalHandlingFeeForwarderCurrency>0.00)
            {
                $text_fHandlingFeeForwarderCurrency = $currencyValue." ".number_format((float)$fTotalHandlingFeeForwarderCurrency,2);
            }
            else
            {
                $fTotalHandlingFeeForwarderCurrency = str_replace("-","",$fTotalHandlingFeeForwarderCurrency);
                $text_fHandlingFeeForwarderCurrency = "(".$currencyValue." ".number_format((float)$fTotalHandlingFeeForwarderCurrency,2).")";
            } 
            if((float)$totalReferralFee>='0.00')
            {
                $text_referralfee=$currencyValue." ".number_format((float)$totalReferralFee,2);
            }
            else
            {
                $totalReferralFee1=str_replace("-","",$totalReferralFee);
                $text_referralfee="(".$currencyValue." ".number_format((float)$totalReferralFee1,2).")";
            }

            if((float)$forwardertotal1>='0.00')
            {
                $text_forwardertotal1=$currencyValue." ".number_format((float)$forwardertotal1,2);
            }
            else
            {
                $forwardertotal_1=str_replace("-","",$forwardertotal1);
                $text_forwardertotal1="(".$currencyValue." ".number_format((float)$forwardertotal_1,2).")";
            }
 
            if((float)$forwardertotal>='0.00')
            {
                $confirm_button_flag=true;
                $text_forwardertotal=$currencyValue." ".number_format((float)$forwardertotal,2);
            }
            else
            {
                $confirm_button_flag=false;
                $forwardertotal_2=str_replace("-","",$forwardertotal);
                $text_forwardertotal="(".$currencyValue." ".number_format((float)$forwardertotal_2,2).")";
            }
            
            $selfInvoiceAmountAry[$idForwarder]['fTotalSelfInvoice'] = $forwardertotal; 
            $selfInvoiceAmountAry[$idForwarder]['idForwarderCurrency'] = $idForwarderCurrency; 
            $selfInvoiceAmountAry[$idForwarder]['szForwarderCurrency'] = $currencyValue; 
            $selfInvoiceAmountAry[$idForwarder]['forwarderTransactionAry'] = $forwarderTransactionAry;  
            ?>
            <tr>
                <td colspan="7" style="border-bottom: 0px solid #9CA0BD"></td>
            </tr>
            <tr>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i>Current Account Balance</i></td>
                <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><?=$currencyValue?> <?=number_format((float)$grandtotal,2)?></i></td>
            </tr>
            <tr <?php echo $szStyle; ?>>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i><?=t($t_base.'messages/referral_fee_invoice');?></i></td>
                <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="referral_fee"> <?=$text_referralfee?></span></i></td>
            </tr>
            <tr <?php echo $szStyle; ?>>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i><?=t($t_base.'messages/courier_booking_and_labels_invoice');?></i></td>
                <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="courier_label_fee"> <?=$text_courierLabelFee?></span></i></td>
            </tr>
            <tr <?php echo $szStyle; ?>>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i>Transporteca handling fee invoice, w/o VAT</i></td>
                <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="booking_handling_fee"> <?php echo $text_fHandlingFeeForwarderCurrency?></span></i></td>
            </tr> 
            <tr>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i> - Transfer due for Bookings</i></td>
                <td colspan="2" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="forwarder_total1"><?=$text_forwardertotal1?></span></i></td>
            </tr>	
            <tr>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i> - Other account debits since last Transfer</i></td>
                <td colspan="2" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i>(<?=$currencyValue?> <span id="forwarder_upload_total"><?=number_format((float)$uploadServiceAmount,2)?></span>)</i></td>
            </tr>
            <tr>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i>Transfer To Forwarder</i></td>
                <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="forwarder_total"> <?=$text_forwardertotal?></span></i></td>
            </tr>
            <tr>
                <td colspan="4" style="border-bottom: 0px solid #9CA0BD;text-align:left;font-size:13px;"><i>New Account Balance</i></td>
                <td colspan="3" style="border-bottom: 0px solid #9CA0BD;text-align:right;font-size:13px;"><i><span id="account_balance_cancel"><?=$currencyValue?> <span id="account_balance">0.00</span></i></span></td>
            </tr>
        </table> 
        <table cellpadding="5"  cellspacing="0" border="0" width="100%" id="booking_table">
            <tr align='center' >
                <td align="right" width="50%" >&nbsp;</td>
                <td align="left">	
                    <input type="hidden" name="check" value="<?=$check?>">	
                    <input type="hidden" name="idForwarderCurrency" value="<?=$idForwarderCurrency?>">	
                    <input type="hidden" name="mode" value="PAY_FORWARDER_CONFORMATION">
                    <input type="hidden" name="totalcheckbox" id="totalcheckbox" value="<?=$j?>">
                    <input type="hidden" name="arrForwarderPayment[grandtotal]" id="grandtotal" value="<?=round($grandtotal,2)?>">
                    <input type="hidden" name="arrForwarderPayment[farwordertotal1]" id="farwordertotal1" value="<?=round($forwardertotal1,2)?>">  
                    <input type="hidden" name="arrForwarderPayment[farwordertotal]" id="farwordertotal" value="<?=round($forwardertotal,2)?>">	
                    <input type="hidden" name="arrForwarderPayment[totalReferralFee]" id="totalReferralFee" value="<?=round($totalReferralFee,2)?>">
                    <input type="hidden" name="arrForwarderPayment[totalcourierLabelFee]" id="totalcourierLabelFee" value="<?=round($bookingLabelFeeTotal,2)?>">
                    <input type="hidden" name="arrForwarderPayment[totalBookingHandlingFee]" id="totalBookingHandlingFee" value="<?=round($fTotalHandlingFeeForwarderCurrency,2)?>"> 
                    <input type="hidden" name="arrForwarderPayment[uploadServiceAmount]" id="uploadServiceAmount" value="<?=round($uploadServiceAmount,2)?>">
                    <input type="hidden" name="arrForwarderPayment[szCurrencyName]" id="szCurrencyName" value="<?=$currencyValue?>">
                    <input type="hidden" name="arrForwarderPayment[fForwarderExchangeRate]" id="fForwarderExchangeRate" value="<?=$bills['fForwarderExchangeRate']?>">
                    <input type="hidden" name="arrForwarderPayment[handlingFeeVat]" id="handlingFeeVat" value="<?=$handlingFeeVat?>"> 
                </td>
            </tr>
        </table> 
        <?php
        }  
    }  
       
    //Calculating last date of last month
    $dtInvoiceOn = date('Y-m-t 23:59:59', strtotime("-1 month")); //Last day of preveious month 
    
    $szCalculationLogs = "";
    $kFarwarder_new = new cForwarder();
    if(!empty($selfInvoiceAmountAry))
    {
        foreach($selfInvoiceAmountAry as $idForwarder=>$selfInvoiceAmountArys)
        {
            $kFarwarder = new cForwarder();
            $kFarwarder->load($idForwarder); 
            $iCreditDays = $kFarwarder->iCreditDays;

            $szCalculationLogs = ".> Started working for Forwarder ID: ".$idForwarder." Time: ".date('Y-m-d H:i:s').PHP_EOL;
            $szForwarderCurrencyName = $selfInvoiceAmountArys['szForwarderCurrency'];
            $fTotalForwarderAmount = round($selfInvoiceAmountArys['fTotalSelfInvoice'],2); 
            $forwarderTransactionAry = $selfInvoiceAmountArys['forwarderTransactionAry']; 
            $szforwarderTransactionIds = implode(',',$forwarderTransactionAry);
            
            //$dtPaymentScheduledDate = date('Y-m-d H:i:s',strtotime("+".$iCreditDays." day"));
            $dtPaymentScheduledDate = date('Y-m-d',strtotime($dtInvoiceOn."+".$iCreditDays." day"));
  
            $szCalculationLogs .= ".> Payment scheduled date: ".$dtPaymentScheduledDate."".PHP_EOL;
            /*
            * Creating unique batch number for the transfer
            */
            $invoiceNumber = $kBilling->generateBatchNumberForPayment();
            $invoiceNumberBatchLabel = $invoiceNumber;  
            
            $szCalculationLogs .= ".> Batch number: ".$invoiceNumber."".PHP_EOL;
            $szCalculationLogs .= ".> Forwarder's transaction ID: ".$szforwarderTransactionIds."".PHP_EOL;

            if($kAdmin->updateForwarderPaymentStatus($szforwarderTransactionIds,$invoiceNumber))
            {          
                $szCalculationLogs .= ".> Successfully updated batch number ".PHP_EOL;
                $forwarderTransactionExchangeRate = $kBilling->getExchangeRateBillingPrice($szForwarderCurrencyName,date('Y-m-d')); 
                 
                $forwarderAmountArr = array();
                $forwarderAmountArr['idForwarder']= $idForwarder;
                $forwarderAmountArr['invoiceNumber']= $invoiceNumber;
                $forwarderAmountArr['forwarderAmount']= $fTotalForwarderAmount;
                $forwarderAmountArr['currencyName'] = $szForwarderCurrencyName;
                $forwarderAmountArr['szDescription'] = 'Automatic transfer';
                $forwarderAmountArr['fExchangeRate'] = $forwarderTransactionExchangeRate;
                $forwarderAmountArr['dtPaymentScheduled'] = $dtPaymentScheduledDate;
                $forwarderAmountArr['dtInvoiceOn'] = $dtInvoiceOn;
                $forwarderAmountArr['iTransferEmailTobeSent'] = 1;
                $kAdmin->insertForarderPayment($forwarderAmountArr);  
            } 
            else
            {
                $szCalculationLogs .= ".> Batch number could not be updated into above transaction IDs ".PHP_EOL;
                $szCalculationLogs .= print_R($kAdmin,true); 
            }
            $kQuote->__copyBookingLogs($szReferenceID,$szCalculationLogs,true);
        }
    }
}


/*
* Sending payment transfer emails for bookings
*/

$kFarwarder_new = new cForwarder();
$kBilling = new cBilling();
$kQuote = new cQuote(); 
$forwarderTransferEmail = array();
$forwarderTransferEmail = $kBilling->getAllTranserEmailTobeSentTransactions();

if(!empty($forwarderTransferEmail))
{
    foreach($forwarderTransferEmail as $forwarderTransferEmails)
    { 
        $idForwarder = $forwarderTransferEmails['idForwarder'];
        $szForwarderCurrencyName = $forwarderTransferEmails['szForwarderCurrency'];
        $fTotalForwarderAmount = $forwarderTransferEmails['fTotalPriceForwarderCurrency'];
        $invoiceNumber = $forwarderTransferEmails['szInvoice'];
        $dtPaymentScheduledDate = $forwarderTransferEmails['dtPaymentScheduledDate'];
        $idForwarderTransaction = $forwarderTransferEmails['id'];
        
        $szCalculationLogs = ".> Sending Transfer confirmation email process started.".PHP_EOL;
        $szCalculationLogs .= ".> Forwarder: ".$idForwarder.PHP_EOL;
        $szCalculationLogs .= ".> Price: ".$szForwarderCurrencyName." ".$fTotalForwarderAmount.PHP_EOL;
        $szCalculationLogs .= ".> Invoice: ".$invoiceNumber.PHP_EOL;
        $szCalculationLogs .= ".> Scheduled Date: ".$dtPaymentScheduledDate.PHP_EOL;
        
        $kFarwarder = new cForwarder();
        $kFarwarder->load($idForwarder); 
            
        if(!($kFarwarder_new->checkForwarderComleteBankDetails($idForwarder)))
        {
            $szCalculationLogs .= ".> Forwarder Bank details updated: Yes .".PHP_EOL;
            $replace_ary = array();
            $replace_ary['szBank'] = $kFarwarder->szBankName;
            $replace_ary['szAccount'] = $kFarwarder->iAccountNumber;
            $replace_ary['sztotal'] = $szForwarderCurrencyName." ".number_format((float)$fTotalForwarderAmount,2);
            $replace_ary['szTotal'] = $szForwarderCurrencyName." ".number_format((float)$fTotalForwarderAmount,2);
            $replace_ary['szReferralfee'] = "";
            $replace_ary['szTransferamount'] = $szForwarderCurrencyName." ".number_format((float)$fTotalForwarderAmount,2);
            $replace_ary['szinvoce'] = $invoiceNumber;
            $replace_ary['fUploadServiceAmountStr'] = $uploadServiceStr;
            $replace_ary['szDate'] = date('j. F Y',strtotime('-1 second',strtotime(date('m').'/01/'.date('Y'))));
            $replace_ary['szMonth'] = date('F',strtotime($dtPaymentScheduledDate));

            $szCalculationLogs .= ".> Sending E-mail with following data.".PHP_EOL;
            $szCalculationLogs .= print_R($replace_ary,true)."".PHP_EOL;
            $idRoleAry=array('3');
            $kForwarder= new cForwarder();
            $getEmailArr=$kForwarder->getForwarderCustomerServiceEmail($idForwarder,__PAYMENT_AND_BILLING_PROFILE_ID__);

            $kBilling->updateForwarderPaymentStatus($idForwarderTransaction);
            try
            {
                $attacfilename = getForwarderTransferPDF($idForwarder,$invoiceNumber,'PDF',true);   
                $szCalculationLogs .= ".> Attachment Name: ".$attacfilename.PHP_EOL;
                $szCalculationLogs .= ".> To: ".print_R($getEmailArr,true).PHP_EOL;
                createEmail(__TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$getEmailArr,'', __FINANCE_CONTACT_EMAIL__,$replace_ary['idUser'], __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__,true,0,$attacfilename,false,false,false,false,'TRANSFER_CONFIRMATION'); 
            } 
            catch (Exception $ex) 
            {
                $szCalculationLogs .= ">. Exception: ".$ex->getMessage().PHP_EOL; 
            }  
        }
        else
        {
            $szCalculationLogs .= ".> Forwarder Bank details updated: No .".PHP_EOL;
            $ForwarderAdminDetails = array();
            $ForwarderAdminDetails = $kAdmin->findForwarderAdminsByBookingId($idForwarder);
            $replace_ary['szDate']=findPaymentFreq($ForwarderAdminDetails[0]['szPaymentFrequency']);
            
            $kBilling->updateForwarderPaymentStatus($idForwarderTransaction);
            if(!empty($ForwarderAdminDetails))
            {
                foreach($ForwarderAdminDetails AS $key=>$value)
                {
                    $paymentFrequency = $value['szPaymentFrequency'];
                    $replace_ary['szFirstName']=$value['szFirstName'];
                    $replace_ary['szDisplayname']=$value['szDisplayName'];

                    $replace_ary['szHere']='<a href ="http://'.$value['szControlPanelUrl'].'/Company/BankDetails/">here</a>';
                    $to =  $value['szEmail'];
                    $szCalculationLogs .= ".> Sending E-mail with following data.".PHP_EOL;
                    $szCalculationLogs .= print_R($replace_ary,true)."".PHP_EOL;
                    $szCalculationLogs .= ".> To: ".$to;
                    
                    try
                    {
                        $szCalculationLogs .= ".> Sending E-mail to: ".$to;
                        createEmail(__FORWARDER_TRANSFER_BANK_DETAILS_DUE__, $replace_ary, $to,'' , __FINANCE_CONTACT_EMAIL__,$value['id'] , __FINANCE_CONTACT_EMAIL__,__FLAG_FOR_FORWARDER__);
                    } 
                    catch (Exception $ex) 
                    {
                        $szCalculationLogs .= ">. Exception: ".$ex->getMessage().PHP_EOL; 
                    } 
                }
            }
        }
        $kQuote->__copyBookingLogs($szReferenceID,$szCalculationLogs,true);
    }
}
?>