<?php
//session_start();
/**
 * Currency conversion cronjob
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
/*
require_once(__APP_PATH_CLASSES__."/error.class.php");
require_once(__APP_PATH_CLASSES__."/database.class.php");
require_once(__APP_PATH_CLASSES__."/user.class.php"); 
require_once(__APP_PATH_CLASSES__."/config.class.php");
require_once(__APP_PATH_CLASSES__."/capsuleCrm.class.php");
require_once(__APP_PATH_CLASSES__."/admin.class.php");
require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
 
//ini_set ('max_execution_time',360000);

error_reporting(E_ALL);
ini_set('display_error','1');

include_once('functions.php');

$newlink=getTransportecaDB();

if(!class_exists('cDatabase'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/user.class.php"); 
    require_once(__APP_PATH_CLASSES__."/config.class.php");
    require_once(__APP_PATH_CLASSES__."/capsuleCrm.class.php");
    require_once(__APP_PATH_CLASSES__."/registeredShippersConsignees.class.php");
    require_once(__APP_PATH_CLASSES__."/admin.class.php");
    require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php"); 
}  
 
$f = fopen(__APP_PATH_LOGS__."/capsuleCrmCronjob.log", "a");
fwrite($f, "\n\n###################Cronjob Started to Post data to Capsule CRM days-:".date("d-m-Y h:i:s")."#######################\n");
$kUser = new cUser();

$kCapsuleCrm = new cCapsuleCrm();
  
$kRegisterShipCon = new cRegisterShipCon();
$userCapsuleIdAry = array();
$userCapsuleIdAry = $kUser->getAllUsersCapsuleCrmID(); 
 
$capsuleDataAry = $kCapsuleCrm->getAllUnPostedDataForCronjob();  
 
if(!empty($capsuleDataAry))
{
    fwrite($f, "\n------------------CRM Data Array-:".print_r($capsuleDataAry,true)."-----------------------\n");
    
    $postedCapsuleIdAry = array();
    foreach($capsuleDataAry as $capsuleDataArys)
    {
        $idCustomer = $capsuleDataArys['idUser'];
        if($capsuleDataArys['iDataType']==1)
        { 
            //Creating XML for Capsule CRM
            $szCapsuleCrmXml = $kCapsuleCrm->createXmlForCapsuleCRM($idCustomer);
            fwrite($f, "\n------------------CRM Data XML:".$szCapsuleCrmXml."-----------------------\n");
            if(!empty($userCapsuleIdAry[$idCustomer]))
            {
                fwrite($f, "\n------------------ IN IF CRM Party ID:".$userCapsuleIdAry[$idCustomer]."-----------------------\n");
                if(!empty($szCapsuleCrmXml))
                {
                    //Adding User details to CapsuleCRM
                    $kCapsuleCrm->addCustomerDetailsToCapsule($szCapsuleCrmXml,$userCapsuleIdAry[$idCustomer]);  
                    $postedCapsuleIdAry[] = $capsuleDataArys['id'];
                } 
            }
            else
            {  
                if(!empty($szCapsuleCrmXml))
                { 
                    //Adding User details to CapsuleCRM
                    $szCapsulePrtyID = $kCapsuleCrm->addCustomerDetailsToCapsule($szCapsuleCrmXml); 
                    fwrite($f, "\n------------------IN ELSE CRM Party ID:".$szCapsulePrtyID."-----------------------\n");
                    if(!empty($szCapsulePrtyID))
                    {
                        $update_user_query = " szCapsulePrtyID = '".trim($szCapsulePrtyID)."' ";   
                        $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer);  
                        $postedCapsuleIdAry[] = $capsuleDataArys['id'];
                    }
                } 
            }
        }
        else if($capsuleDataArys['iDataType']==2)
        { 
            $szCapsulePrtyID = '';
            if(!empty($userCapsuleIdAry[$idCustomer]))
            {
                $szCapsulePrtyID = $userCapsuleIdAry[$idCustomer] ;
            }
            else
            {
                $kUser = new cUser();
                $kUser->getUserDetails($idCustomer);
                if(!empty($kUser->szCapsulePrtyID))
                {
                    $szCapsulePrtyID = $kUser->szCapsulePrtyID ;
                }
            }
            
            fwrite($f, "\n------------------UPDATING NOTES FOR Party ID :".$szCapsulePrtyID." and Notes is: \n\n ".$capsuleDataArys['szNotes']." \n\n -----------------------\n");
            
            /* 
             * If a user has added a notes but that user not yet added to CRM then we simply add that user to crm
              
            if(empty($szCapsulePrtyID) && !empty($capsuleDataArys['szNotes']))
            {
                $szCapsuleCrmXml = $kCapsuleCrm->createXmlForCapsuleCRM($idCustomer);
                
                if(!empty($szCapsuleCrmXml))
                { 
                    //Adding User details to CapsuleCRM
                    $szCapsulePrtyID = $kCapsuleCrm->addCustomerDetailsToCapsule($szCapsuleCrmXml); 
                    fwrite($f, "\n------------------IN ELSE CRM Party ID:".$szCapsulePrtyID."-----------------------\n");
                    if(!empty($szCapsulePrtyID))
                    {
                        $update_user_query = " szCapsulePrtyID = '".trim($szCapsulePrtyID)."' ";   
                        $kRegisterShipCon->updateUserDetailsByQuotes($update_user_query,$idCustomer);  
                        $postedCapsuleIdAry[] = $capsuleDataArys['id'];
                    }
                }
            }
            if(!empty($szCapsulePrtyID) && !empty($capsuleDataArys['szNotes']))
            {
                $szCapsuleNotes = $capsuleDataArys['szNotes'];
                $szCapsuleNotes = nl2br($szCapsuleNotes);
                $szCapsuleNotes = strip_tags($szCapsuleNotes); 
                
                 //Creating XML for Capsule CRM
                $szCapsuleCrmXml = $kCapsuleCrm->createXMLForAddNote($szCapsuleNotes);

                if(!empty($szCapsuleCrmXml))
                {
                    //Adding Customer History Notes to CapsuleCRM
                   $kCapsuleCrm->addCustomerHistoryNotesToCapsule($szCapsuleCrmXml,$szCapsulePrtyID);   
                   $postedCapsuleIdAry[] = $capsuleDataArys['id'];
                }  
                
                if($capsuleDataArys['iCreateTask']>0)
                { 
                    $crmTaskAry = array();
                    if($capsuleDataArys['iCreateTask']==1)
                    {
                        $crmTaskAry['szTaskDesc'] = 'Check new user';
                    }
                    else if($capsuleDataArys['iCreateTask']==2)
                    {
                        //$crmTaskAry['szTaskDesc'] = 'User profile updated';
                        continue;
                    }
                    else if($capsuleDataArys['iCreateTask']==3)
                    {
                         $crmTaskAry['szTaskDesc'] = 'Automatic Search';
                    }
                        
                    $crmTaskAry['dtTaskDate'] = gmdate("Y-m-d\TH:i:s\Z");
                    $szCapsuleTaskCrmXml = $kCapsuleCrm->createXMLForAddTask($crmTaskAry);
                     
                    //Adding Customer Task to CapsuleCRM
                     $kCapsuleCrm->addCustomerHistoryNotesToCapsule($szCapsuleTaskCrmXml,$szCapsulePrtyID,'TASK');    
                } 
            } 
        }
    }  
    
    fwrite($f, "\n------------------Capsule IDs Posted :".print_R($postedCapsuleIdAry,true)." \n\n -----------------------\n");
    if(!empty($postedCapsuleIdAry))
    {
       $kCapsuleCrm->updatedPostedFlag($postedCapsuleIdAry);
    }
} 

$kCapsuleCrm->deleteOldData();
fwrite($f, "\n\n###################Cronjob End:".date("d-m-Y h:i:s")."#######################\n");


$f = fopen(__APP_PATH_LOGS__."/capsuleCrmCronjob.log", "a"); 
 
closeDB($newlink);
 * *
 */
?>