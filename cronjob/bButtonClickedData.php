<?php
//session_start();
/**
 * B Button Clicked Data
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
 ini_set ('max_execution_time',360000);

$newlink=getTransportecaDB();
if(!class_exists('cExplain'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/explain.class.php"); 
}

$kExplain =  new cExplain();
$landingPageDataAry = array();
$landingPageDataAry = $kExplain->getAllNewLandingPageData();

if(!empty($landingPageDataAry))
{
    foreach($landingPageDataAry as $landingPageDataArys)
    {
        $iNumButtonClicked=0;
        if((int)$landingPageDataArys['iHoldingPage']==0)
        {
            $iNumButtonClicked=$kExplain->getBButtonClickData($landingPageDataArys['id']);
            echo $iNumButtonClicked."iNumButtonClicked---Landing Page<br />";
        }
        else
        {
            $iNumButtonClicked=$kExplain->getBButtonClickDataHoldingPages($landingPageDataArys['id']);
            echo $iNumButtonClicked."iNumButtonClicked---Holding Page<br />";
        }
        
        $kExplain->updateiNumButtonClicked($landingPageDataArys['id'],$iNumButtonClicked);
    }
}