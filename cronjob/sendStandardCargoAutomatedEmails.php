<?php  
//session_start();
/**
 * Currency conversion cronjob
 */
ini_set (max_execution_time,360000);
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/content_functions.php");
 
require_once(__APP_PATH_CLASSES__."/error.class.php");
require_once(__APP_PATH_CLASSES__."/database.class.php");
require_once(__APP_PATH_CLASSES__."/user.class.php"); 
require_once(__APP_PATH_CLASSES__."/config.class.php");
require_once(__APP_PATH_CLASSES__."/capsuleCrm.class.php");
require_once(__APP_PATH_CLASSES__."/admin.class.php");
require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
    
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);

include_once('functions.php');
  
$kQuote = new cQuote();
$bookingDataAry = $kQuote->getAllVogaAutomatedQuote();
 
$kWhsSearch=new cWHSSearch();
$kConfig = new cConfig();
//$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__');
         
if(!empty($bookingDataAry))
{
    foreach($bookingDataAry as $bookingDataArys)
    {  
        try
        {
            $fCargoVolume = $bookingDataArys['fCargoVolume'];
            $fCargoWeight = $bookingDataArys['fCargoWeight'];
            $dtTimingDate = $bookingDataArys['dtTimingDate'];

            echo "<br>Booking ID: ".$bookingDataArys['id'];
            echo "<br>File ID: ".$bookingDataArys['idFile'];
            if((float)$fCargoVolume==0 && (float)$fCargoWeight==0)
            {
                continue;
            }   
            $iBookingLanguage = $bookingDataArys['iBookingLanguage']; 
            $szCustomerEmail = $bookingDataArys['szEmail'];
            $idCustomer = $bookingDataArys['idUser'] ; 
            $idBooking = $bookingDataArys['id'];

            $languageArr=$kConfig->getLanguageDetails('',$iBookingLanguage);

            if(!empty($languageArr))
            {
                $szCbmText = $languageArr[0]['szDimensionUnit'];
            }
            else
            {
                $szCbmText = 'cbm'; 
            } 
            $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iBookingLanguage);

            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($bookingDataArys['idOriginCountry'],false,$iBookingLanguage);
            $szOriginCountryName = $kConfig_new->szCountryName;

            $kConfig_new = new cConfig();
            $kConfig_new->loadCountry($bookingDataArys['idDestinationCountry'],false,$iBookingLanguage);
            $szDestinationCountryName = $kConfig_new->szCountryName;

            $returnAry = array();
            $returnAry['szFromPostcode'] = $bookingDataArys['szOriginPostCode'];
            $returnAry['szFromCity'] = $bookingDataArys['szOriginCity'];
            $returnAry['szFromCountry'] = $szOriginCountryName;
            $returnAry['szToPostcode'] = $bookingDataArys['szDestinationPostCode'];
            $returnAry['szToCity'] = $bookingDataArys['szDestinationCity'];
            $returnAry['szToCountry'] = $szDestinationCountryName;
            $returnAry['szCargoDescription'] = utf8_decode($bookingDataArys['szCargoDescription']);
            $returnAry['szVolume'] = format_volume($bookingDataArys['fCargoVolume'],$iBookingLanguage).$szCbmText;
            $returnAry['szWeight'] = number_format_custom((float)$bookingDataArys['fCargoWeight'],$iBookingLanguage)."kg";
            $returnAry['szFirstName'] = $bookingDataArys['szFirstName'];
            $returnAry['szLastName'] = $bookingDataArys['szLastName'];

            $returnAry['szLastName'] = $bookingDataArys['szLastName'];

            if($bookingDataArys['idTransportMode']==4)//courier 
            { 
                $szVolWeight = format_volume($bookingDataArys['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$bookingDataArys['fCargoWeight'],$iBookingLanguage)."kg,  ".number_format_custom((int)$bookingDataArys['iNumColli'],$iBookingLanguage)."col";
            }
            else
            {
                $szVolWeight = format_volume($bookingDataArys['fCargoVolume'],$iBookingLanguage).$szCbmText.",  ".number_format_custom((float)$bookingDataArys['fCargoWeight'],$iBookingLanguage)."kg ";
            }

            $idBookingFile = $bookingDataArys['idFile'];
            $idLandingPage = $bookingDataArys['idLandingPage'];

            $kBooking_new = new cBooking();
            $kAdmin = new cAdmin();
            if($idBookingFile>0)
            { 
                $kBooking_new->loadFile($idBookingFile);
                $idFileOwner = $kBooking_new->idFileOwner;  
                $kAdmin->getAdminDetails($idFileOwner);

                $kConfig = new cConfig();
                $kConfig->loadCountry($kAdmin->idInternationalDialCode);
                $iInternationDialCode = $kConfig->iInternationDialCode;
                $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone; 
            } 
            $returnAry['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
            $returnAry['szTitle'] = $kAdmin->szTitle ;
            $returnAry['szOfficePhone'] = $szCustomerCareNumer ;
            $returnAry['szMobile'] = $szMobile;
            $returnAry['szWebSiteUrl'] = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);

            $searchDataAry = array();
            $searchDataAry['iClosed']=1; 
            $quotesAry = array();  
            $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile,$searchDataAry);

            $kQuote = new cQuote();
            $quoteDataAry = $kQuote->createQuoteOfferTexts($quotesAry,$bookingDataArys);

            $returnAry['szValidTo'] = $quoteDataAry['szValidTo'];
            $returnAry['szQuote'] = $quoteDataAry['szQuote'];
            $iCheckAutoRFQCode=false;
            if($bookingDataArys['szAutomatedRfqResponseCode']!='')
            {
                $iCheckAutoRFQCode=true;
            }
            $kExplain = new cExplain();
            $quoteHandlerAry = $kExplain->getQuoteHandlerByPageID($idLandingPage,false,$iCheckAutoRFQCode);

            $szQuoteEmailBody = $quoteHandlerAry['szQuoteEmailBody'];
            $szQuoteEmailSubject = $quoteHandlerAry['szQuoteEmailSubject'];

            if(!empty($returnAry))
            {
                foreach ($returnAry as $replace_key => $replace_value)
                { 
                    $szQuoteEmailBody = str_replace($replace_key, $replace_value, $szQuoteEmailBody);
                    $szQuoteEmailSubject= str_replace($replace_key, $replace_value, $szQuoteEmailSubject);
                }
            } 
            $szEmailBody = $szQuoteEmailBody;
            $szEmailSubject = $szQuoteEmailSubject;

            ob_start();  
            include_once(__APP_PATH_ROOT__.'/layout/email_header.php');
            $message = ob_get_clean();

            $szEmailBody = $message."".$szEmailBody;
            //print_R($returnAry);
            echo "<br><br> Subject: ".$szEmailSubject;
            echo "<br> E-mail: ".$szCustomerEmail;
            echo $szEmailBody; 
    //        continue;
    //        die;
            $szFromEmail = $kAdmin->szEmail;
            $szFromUserName = $kAdmin->szFirstName." ".$kAdmin->szLastName ;

            $szCustomerHistoryNotes = "To: ".$szCustomerEmail." <br> From: ".$szFromEmail." <br> Subject: ".$szEmailSubject." <br> <br> ".$szEmailBody ;
  
            $contactEmailAry[0] = $szCustomerEmail ;  

            $kBooking_new->addReminderResponseTracker($idBookingFile,'SEND_VOGA_CUSTOMER_QUOTES');   

            /*
            *  Update iBookingType
            */
            $res_ary = array();
            $res_ary['iBookingType'] = __BOOKING_TYPE_RFQ__;

            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                } 
                $update_query = rtrim($update_query,",");  
                $kBooking_new->updateDraftBooking($update_query,$idBooking);
            } 
            sendQuatationEmail($contactEmailAry,$szFromEmail,$szEmailSubject,$szEmailBody,__STORE_SUPPORT_EMAIL__,$idCustomer,$idBooking,$szFromUserName,1); 

            $dtResponseTime = $kBooking_new->getRealNow();

            $res_ary = array();
            $res_ary['szTransportecaStatus'] = "S5"; 
            $res_ary['szTransportecaTask'] = ''; 
            $res_ary['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
            $res_ary['dtFileStatusUpdatedOn'] = $dtResponseTime;
            $res_ary['iQuoteSent'] = 1 ; 

            if(!empty($res_ary))
            {
                $update_query='';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
                $update_query .= " dtQuoteSend= NOW()," ;
            }  

            $update_query = rtrim($update_query,",");  

            if($kBooking_new->updateBookingFileDetails($update_query,$idBookingFile))
            { 

            }
            echo "<hr>"; 
        } 
        catch (Exception $ex) 
        {
            echo "Exception: <br> ";
            print_R($ex);
        } 
    }
}