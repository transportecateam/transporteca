<?php
//session_start();
/**
 * Forwarder Term And Condition Email Daily
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
ini_set (max_execution_time,360000);
$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
}
$kForwarder=new cForwarder();

$date=date('Y-m-d',strtotime('- 23 DAY'));
$dtExpiry=date('d. M Y',strtotime('+ 7 DAY'));
	$forwarderTCArr=$kForwarder->getForwarderAgreedTC($date);
	
	$forwarderComment=$kForwarder->getForwardrVersionComment();
	
	if(!empty($forwarderTCArr))
	{
		$replaceAry['dtTC']=date('d. M Y',strtotime($date));
		$replaceAry['dtExpiry']=$dtExpiry;
		for($k=0;$k<count($forwarderTCArr);++$k)
		{
			$kForwarder->load($forwarderTCArr[$k]);
			$szUrl="http://".$kForwarder->szControlPanelUrl."/T&C/";
			//echo $szUrl;
			$replaceAry['szTNCURL']="<a href='".$szUrl."'>here</a>";
			$forwarderAdminDetails=array();
			$forwarderAdminDetails=$kForwarder->getForwarderAdminDetails($forwarderTCArr[$k]);
			//echo $forwarderTCArr[$k]."<br/>";
			//print_r($forwarderAdminDetails);
			if(!empty($forwarderAdminDetails) && count($forwarderAdminDetails)>0 )
			{
				foreach($forwarderAdminDetails as $forwarderAdminDetailArr)
				{
					$replaceAry['szFirstName']=utf8_encode($forwarderAdminDetailArr['szFirstName']);
					$replaceAry['szLastName']=utf8_encode($forwarderAdminDetailArr['szLastName']);
					$replaceAry['szEmail']=$forwarderAdminDetailArr['szEmail'];
					$replaceAry['szComment']=$forwarderComment;
					//print_r($replaceAry);
					//echo "<br/>";
					createEmail(__SEND_TNC_EXPIRY_EMAIL_BEFORE_7DAYS__, $replaceAry,$replaceAry['szEmail'],'', __STORE_SUPPORT_EMAIL__,0, __STORE_SUPPORT_EMAIL__);
				}
				
			}
			
		}
	}
?>

