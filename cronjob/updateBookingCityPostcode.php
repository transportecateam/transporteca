<?php
//session_start();
/**
 * Update Forwarder Term And Condition Agreed Status
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');

if(!class_exists('cBooking'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/booking.class.php");
}

ini_set (max_execution_time,360000);

$kBooking = new cBooking();
$bookingAry = $kBooking->getAllBooking();

if(!empty($bookingAry))
{
	foreach($bookingAry as $bookingArys)
	{
		$idBooking = $bookingArys['id'];
		$res_ary = array();
		$res_ary['szOriginCity'] = $bookingArys['szOriginCity'];
		$res_ary['szDestinationCity'] = $bookingArys['szDestinationCity'];
		$res_ary['szOriginPostCode'] = $bookingArys['szOriginPostCode'];
		$res_ary['szDestinationPostCode'] = $bookingArys['szDestinationPostCode'];
		$kBooking->updateBooking($res_ary,$idBooking);
	}
}