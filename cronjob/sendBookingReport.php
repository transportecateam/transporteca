<?php
//session_start();
/**
 * Send Booking Report Daily to Forwarders
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
ini_set(max_execution_time,360000);

$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
}

$kForwarder = new cForwarder();

///daily
$forwarderArr=$kForwarder->getForwarderArr();

$kConfig = new cConfig();
$transportModeListAry = array();
$transportModeListAry = $kConfig->getAllTransportMode(false,false,true);

//print_r($forwarderArr);
if(!empty($forwarderArr))
{
    $expectedStr='';
    $getExpectedServieces=$kForwarder->getExpectedServieces('daily');
  
    foreach($forwarderArr as $forwarderArrs)
    {
        $expectedStr='';
        $expectedServiceAddedStr='';
        if($getExpectedServieces)
        {
            $countrr=0;
            $loop_counter = count($getExpectedServieces);

            foreach($getExpectedServieces as $getExpectedServiece)
            {
                if($getExpectedServiece['total']>1)
                {
                    $request_string="requests";
                }
                else
                {
                    $request_string="request";
                }
                //echo "hello";
                if(!$kForwarder->checkServiceAvailableForForwarder($forwarderArrs['id'],$getExpectedServiece['idOriginCountry'],$getExpectedServiece['idDestinationCountry']))
                {
                    $expectedStr .= $getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." ".$request_string."<br />";	
                }
                else
                {
                    $expectedServiceAddedStr .=	$getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." ".$request_string."<br />";
                }
            }
        }
        $expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]=$expectedServiceAddedStr;
        $expectedStrForForwarderArr[$forwarderArrs['id']]=$expectedStr;  
    }
    foreach($forwarderArr as $forwarderArrs)
    {
        $getAllBookingForForwarder=array();
        $getAllBookingForForwarder=$kForwarder->getAllBookingForForwarder($forwarderArrs['id'],'daily');
        $replace_ary['szSubject']="Daily booking report from Transporteca";
        $replace_ary['szBookingFreqType']="Daily";
        $replace_ary['szBookingFreq']="24 hours";

        if(!empty($forwarderArrs['szDisplayName']))
        {
            $replace_ary['szDisplayName']=$forwarderArrs['szDisplayName'];
        }
        else 
        {
            $replace_ary['szDisplayName']="your company";
        }
        $idRoleAry=array('1','3');
        $createdServiceText='';
        if($expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]!='')
        {
            $createdServiceText=__SERVICE_ONLINE_MESSAGE__."<br/><br/>".$expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]."<br/>";
        }
        $replace_ary['szServicesOnline']=$createdServiceText;
        $replace_ary['szExpectedServices']=$expectedStrForForwarderArr[$forwarderArrs['id']]."<br />";
        $getEmailFirstNameArr=$kForwarder->getAllForwardersDetails($forwarderArrs['id'],$idRoleAry,'daily');
        $urlMyAccount="http://".$forwarderArrs['szControlPanelUrl']."/Profile/";
        $replace_ary['szMyAccount']="<a href=".$urlMyAccount.">here</a>";

        $kForwarder_new = new cForwarder();
        $kForwarder_new->load($forwarderArrs['id']);
        if($kForwarder_new->isOnline==1)
        {
            $replace_ary['szOffline'] ='';
        }
        else
        {
            $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
        }

        /*
        if($kForwarder->checkForwarderOnlineStatus($forwarderArrs['id']))
        {
                $replace_ary['szOffline'] ='';
        }
        else
        {
                $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
        }
        */
        if(!empty($getAllBookingForForwarder))
        {
            $str='';
            foreach($getAllBookingForForwarder as $getAllBookingForForwarders)
            {
                $cargo_volume = format_volume((float)$getAllBookingForForwarders['fCargoVolume']);
                $cargo_weight = get_formated_cargo_measure((float)($getAllBookingForForwarders['fCargoWeight']/1000)) ;
                if($getAllBookingForForwarders['idTransportMode']>0)
                {
                    $szBookingType = $transportModeListAry[$getAllBookingForForwarders['idTransportMode']]['szLongName'] ;
                }
                else
                {
                    $szBookingType = 'Seafreight';
                }
                $str .=$getAllBookingForForwarders['szBookingRef'].": ".$cargo_volume." cbm / ".$cargo_weight." mt - ".$szBookingType." from ".$getAllBookingForForwarders['szOriginCountry']." to ".$getAllBookingForForwarders['szDestinationCountry']."<br />";
            }

            if($expectedStr!='')
            {
                $message = str_replace('szBookingFreq', $replace_ary['szBookingFreq'], __BOOKING_EXPECTED_SERVICES_MESSAGE__);
                $replace_ary['szMessage']=$message."<br />";
            }
            else
            {
                $replace_ary['szMessage']='';
            }
            $replace_ary['szContralUrl']="http://".$forwarderArrs['szControlPanelUrl']."/booking/";

            $replace_ary['szBookingDetail']=$str;
            if(!empty($getEmailFirstNameArr))
            {
                foreach($getEmailFirstNameArr as $getEmailFirstNameArrs)
                {
                    $replace_ary['szFirstName']=$getEmailFirstNameArrs['szFirstName'];
                    $replace_ary['szEmail']=$getEmailFirstNameArrs['szEmail'];					
                    createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$replace_ary['szEmail'],$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);

                    //createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,'ajay@whiz-solutions.com',$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                }
            }
        }
        else
        {
            if($expectedStr!='')
            {
                $replace_ary['szMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE__."<br />";
                $replace_ary['szSecondMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__;
            }
            else
            {
                $replace_ary['szMessage']='';
                $replace_ary['szSecondMessage']='';
            }
            if(!empty($getEmailFirstNameArr))
            {
                foreach($getEmailFirstNameArr as $getEmailFirstNameArrs)
                {
                    $replace_ary['szFirstName']=$getEmailFirstNameArrs['szFirstName'];
                    $replace_ary['szEmail']=$getEmailFirstNameArrs['szEmail'];

                    createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$replace_ary['szEmail'],$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                    //createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,'ajay@whiz-solutions.com',$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);

                }
            }
        }
    }
}
//weekly
if(date('D')=='Mon')
{
	$forwarderArr=array();
	$expectedStrForForwarderArr=array();
	$forwarderArr=$kForwarder->getForwarderArr();
	//PRINT_R($forwarderArr);
	if(!empty($forwarderArr))
	{
		$expectedStr='';
		$expectedServiceAddedStr='';
		$getExpectedServieces=$kForwarder->getExpectedServieces('weekly');
		foreach($forwarderArr as $forwarderArrs)
		{
                    $expectedStr='';
                    if($getExpectedServieces)
                    {
                        $countrr=0;
                        $loop_counter = count($getExpectedServieces);

                        foreach($getExpectedServieces as $getExpectedServiece)
                        {
                            if($getExpectedServiece['total']>1)
                            {
                                $request_string="requests";
                            }
                            else
                            {
                                $request_string="request";
                            }
                            //echo "hello";
                            if(!$kForwarder->checkServiceAvailableForForwarder($forwarderArrs['id'],$getExpectedServiece['idOriginCountry'],$getExpectedServiece['idDestinationCountry']))
                            {
                                $expectedStr .=	$getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." ".$request_string."<br />";	
                            }
                            else
                            {
                                $expectedServiceAddedStr .=	$getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." ".$request_string."<br />";
                            }
                        }
                    }
                    $expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]=$expectedServiceAddedStr;
                    $expectedStrForForwarderArr[$forwarderArrs['id']]=$expectedStr;
		}
		foreach($forwarderArr as $forwarderArrs)
		{
                    $getAllBookingForForwarder=array();
                    $getAllBookingForForwarder=$kForwarder->getAllBookingForForwarder($forwarderArrs['id'],'weekly');
                    
                    $replace_ary['szSubject']="Weekly booking report from Transporteca";
                    $replace_ary['szBookingFreqType']="Weekly";
                    $replace_ary['szBookingFreq']="7 days";
                    if(!empty($forwarderArrs['szDisplayName']))
                    {
                        $replace_ary['szDisplayName']=$forwarderArrs['szDisplayName'];
                    }
                    else 
                    {
                        $replace_ary['szDisplayName']="your company";
                    }
                    $createdServiceText='';
                    if($expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]!='')
                    {
                        $createdServiceText= __SERVICE_ONLINE_MESSAGE__."<br/><br/>".$expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]."<br/>";
                    }
                    $replace_ary['szServicesOnline']=$createdServiceText;
                    $idRoleAry=array('1','3');
                    $replace_ary['szExpectedServices']=$expectedStrForForwarderArr[$forwarderArrs['id']]."<br />";
                    $getEmailFirstNameArr=$kForwarder->getAllForwardersDetails($forwarderArrs['id'],$idRoleAry,'weekly');
                    $urlMyAccount="http://".$forwarderArrs['szControlPanelUrl']."/Profile/";
                    $replace_ary['szMyAccount']="<a href=".$urlMyAccount.">here</a>";

                    $kForwarder_new = new cForwarder();
                    $kForwarder_new->load($forwarderArrs['id']);
                    if($kForwarder_new->isOnline==1)
                    {
                        $replace_ary['szOffline'] ='';
                    }
                    else
                    {
                        $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
                    }

                    /*
                    if($kForwarder->checkForwarderOnlineStatus($forwarderArrs['id']))
                    {
                        $replace_ary['szOffline'] ='';
                    }
                    else
                    {
                        $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
                    }
                    */
                    if(!empty($getAllBookingForForwarder))
                    {
                        $str='';
                        foreach($getAllBookingForForwarder as $getAllBookingForForwarders)
                        {
                            $cargo_volume = format_volume((float)$getAllBookingForForwarders['fCargoVolume']);
                            $cargo_weight = get_formated_cargo_measure((float)($getAllBookingForForwarders['fCargoWeight']/1000)) ;
                            
                            if($getAllBookingForForwarders['idTransportMode']>0)
                            {
                                $szBookingType = $transportModeListAry[$getAllBookingForForwarders['idTransportMode']]['szLongName'] ;
                            }
                            else
                            {
                                $szBookingType = 'Seafreight';
                            } 
                            $str .=$getAllBookingForForwarders['szBookingRef'].": ".$cargo_volume." cbm / ".$cargo_weight." mt - ".$szBookingType." from ".$getAllBookingForForwarders['szOriginCountry']." to ".$getAllBookingForForwarders['szDestinationCountry']."<br />";
                        }
                        if($expectedStr!='')
                        {
                            $message = str_replace('szBookingFreq', $replace_ary['szBookingFreq'], __BOOKING_EXPECTED_SERVICES_MESSAGE__);
                            $replace_ary['szMessage']=$message."<br />";
                        }
                        else
                        {
                            $replace_ary['szMessage']='';
                        }
                        $replace_ary['szContralUrl']="http://".$forwarderArrs['szControlPanelUrl']."/booking/";
                        $replace_ary['szBookingDetail']=$str;
                        if(!empty($getEmailFirstNameArr))
                        {
                            foreach($getEmailFirstNameArr as $getEmailFirstNameArrs)
                            {
                                $replace_ary['szFirstName']=$getEmailFirstNameArrs['szFirstName'];
                                $replace_ary['szEmail']=$getEmailFirstNameArrs['szEmail'];

                                createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$replace_ary['szEmail'],$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                                //createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,'ajay@whiz-solutions.com',$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                            }
                        }
                    }
                    else
                    {
                        if($expectedStr!='')
                        {
                            $replace_ary['szMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE__;
                            $replace_ary['szSecondMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__."<br />";
                        }
                        else
                        {
                            $replace_ary['szMessage']='';
                            $replace_ary['szSecondMessage']='';
                        }
                        //print_r($replace_ary);

                        if(!empty($getEmailFirstNameArr))
                        {
                            foreach($getEmailFirstNameArr as $getEmailFirstNameArrs)
                            {
                                $replace_ary['szFirstName']=$getEmailFirstNameArrs['szFirstName'];
                                $replace_ary['szEmail']=$getEmailFirstNameArrs['szEmail'];

                                createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$replace_ary['szEmail'],$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                                //createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,'ajay@whiz-solutions.com',$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                            }
                        }
                    }
		}
	}
}

////monthly
if(date('d')=='01')
{
	$forwarderArr=array();
	$expectedStrForForwarderArr=array();
	$forwarderArr=$kForwarder->getForwarderArr();
        $expectedStr='';
	if(!empty($forwarderArr))
	{
            $expectedStr='';
            $getExpectedServieces=$kForwarder->getExpectedServieces('monthly');
            foreach($forwarderArr as $forwarderArrs)
            {
                $expectedStr='';
                $expectedServiceAddedStr='';
                if($getExpectedServieces)
                {
                    $countrr=0;
                    $loop_counter = count($getExpectedServieces);

                    foreach($getExpectedServieces as $getExpectedServiece)
                    {
                        if($getExpectedServiece['total']>1)
                        {
                            $request_string="requests";
                        }
                        else
                        {
                            $request_string="request";
                        }
                        //echo "hello";
                        if(!$kForwarder->checkServiceAvailableForForwarder($forwarderArrs['id'],$getExpectedServiece['idOriginCountry'],$getExpectedServiece['idDestinationCountry']))
                        {
                                $expectedStr .=	$getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." ".$request_string."<br />";	
                        }
                        else
                        {
                                $expectedServiceAddedStr .=	$getExpectedServiece['szOriginCountry']." to ".$getExpectedServiece['szDestinationCountry']." ".$getExpectedServiece['total']." ".$request_string."<br />";
                        }
                    }
                }
                $expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]=$expectedServiceAddedStr;
                $expectedStrForForwarderArr[$forwarderArrs['id']]=$expectedStr;
            }
            foreach($forwarderArr as $forwarderArrs)
            {
                $getAllBookingForForwarder=array();
                $getAllBookingForForwarder=$kForwarder->getAllBookingForForwarder($forwarderArrs['id'],'monthly');
                $replace_ary['szSubject']="Monthly booking report from Transporteca";
                $replace_ary['szBookingFreqType']="Monthly";
                $replace_ary['szBookingFreq']="month";
                if(!empty($forwarderArrs['szDisplayName']))
                {
                    $replace_ary['szDisplayName']=$forwarderArrs['szDisplayName'];
                }
                else 
                {
                    $replace_ary['szDisplayName']="your company";
                }

                $idRoleAry=array('1','3');
                $createdServiceText='';
                if($expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]!='')
                {
                    $createdServiceText=__SERVICE_ONLINE_MESSAGE__."<br/><br/>".$expectedStrForForwarderServiceAddedArr[$forwarderArrs['id']]."<br/>";
                }
                $replace_ary['szServicesOnline']=$createdServiceText;
                $replace_ary['szExpectedServices']=$expectedStrForForwarderArr[$forwarderArrs['id']]."<br />";
                $getEmailFirstNameArr=$kForwarder->getAllForwardersDetails($forwarderArrs['id'],$idRoleAry,'monthly');
                $urlMyAccount="http://".$forwarderArrs['szControlPanelUrl']."/Profile/";
                $replace_ary['szMyAccount']="<a href=".$urlMyAccount.">here</a>";

                $kForwarder_new = new cForwarder();
                $kForwarder_new->load($forwarderArrs['id']);
                if($kForwarder_new->isOnline==1)
                {
                    $replace_ary['szOffline'] ='';
                }
                else
                {
                    $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
                }

                /*
                if($kForwarder->checkForwarderOnlineStatus($forwarderArrs['id']))
                {
                        $replace_ary['szOffline'] ='';
                }
                else
                {
                        $replace_ary['szOffline'] = __OFFLINE_FORWARDER_COMMENT__;
                }
                */
                if(!empty($getAllBookingForForwarder))
                {
                    $str='';
                    foreach($getAllBookingForForwarder as $getAllBookingForForwarders)
                    {
                        $cargo_volume = format_volume((float)$getAllBookingForForwarders['fCargoVolume']);
                        $cargo_weight = get_formated_cargo_measure((float)($getAllBookingForForwarders['fCargoWeight']/1000)) ;
                        
                        if($getAllBookingForForwarders['idTransportMode']>0)
                        {
                            $szBookingType = $transportModeListAry[$getAllBookingForForwarders['idTransportMode']]['szLongName'] ;
                        }
                        else
                        {
                            $szBookingType = 'Seafreight';
                        }
                        $str .=$getAllBookingForForwarders['szBookingRef'].": ".$cargo_volume." cbm / ".$cargo_weight." mt - ".$szBookingType." from ".$getAllBookingForForwarders['szOriginCountry']." to ".$getAllBookingForForwarders['szDestinationCountry']."<br />";
                    }
                    if(!empty($expectedStr))
                    {
                        $message = str_replace('szBookingFreq', $replace_ary['szBookingFreq'], __BOOKING_EXPECTED_SERVICES_MESSAGE__);
                        $replace_ary['szMessage']=$message."<br />";
                    }
                    else
                    {
                        $replace_ary['szMessage']='';
                    }
                    $replace_ary['szContralUrl']="http://".$forwarderArrs['szControlPanelUrl']."/booking/";
                    $replace_ary['szBookingDetail']=$str;

                    if(!empty($getEmailFirstNameArr))
                    {
                        foreach($getEmailFirstNameArr as $getEmailFirstNameArrs)
                        {
                            //print_r($replace_ary);
                            $replace_ary['szFirstName']=$getEmailFirstNameArrs['szFirstName'];
                            $replace_ary['szEmail']=$getEmailFirstNameArrs['szEmail'];

                            createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$replace_ary['szEmail'],$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                            //createEmail(__SEND_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,'ajay@whiz-solutions.com',$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                        }
                    }
                }
                else
                {
                    if(!empty($expectedStr))
                    {
                        $replace_ary['szMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE__;
                        $replace_ary['szSecondMessage']=__BOOKING_EXPECTED_SERVICES_NO_BOOKING_MESSAGE_1__."<br />";
                    }
                    else
                    {
                        $replace_ary['szMessage']='';
                        $replace_ary['szSecondMessage']='';
                    }

                    if(!empty($getEmailFirstNameArr))
                    {
                        foreach($getEmailFirstNameArr as $getEmailFirstNameArrs)
                        {
                            $replace_ary['szFirstName']=$getEmailFirstNameArrs['szFirstName'];
                            $replace_ary['szEmail']=$getEmailFirstNameArrs['szEmail'];
                            createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,$replace_ary['szEmail'],$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                            //createEmail(__SEND_NO_BOOKING_REPORT_TO_FORWARDER__, $replace_ary,'ajay@whiz-solutions.com',$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
                        }
                    } 
                }
            }
	}
}
?>