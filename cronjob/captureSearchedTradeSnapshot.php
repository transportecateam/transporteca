<?php
//session_start();
/**
 * Currency conversion cronjob
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
ini_set ('max_execution_time',36000);
 
require_once(__APP_PATH_CLASSES__."/error.class.php");
require_once(__APP_PATH_CLASSES__."/database.class.php");
require_once(__APP_PATH_CLASSES__."/report.class.php"); 
require_once(__APP_PATH_CLASSES__."/config.class.php");  
  
include_once('functions.php');

$newlink=getTransportecaDB(); 
$f = fopen(__APP_PATH_LOGS__."/searchedTradeCronjob.log", "a");
fwrite($f, "\n\n###################Cronjob Started on: ".date("d-m-Y h:i:s")."#######################\n");

$kReport = new cReport();

$searchedTradeAry = array();
$searchedTradeAry = $kReport->getAllSearchedTrade();

fwrite($f, "\n\n###################Total Data Should Be Added: ".count($searchedTradeAry)."#######################\n");
   
if(!empty($searchedTradeAry))
{
    $kReport->updateStatus();
    $ctr = 0; 
    foreach($searchedTradeAry as $searchedTradeArys)
    {   
        $kReport->addSearchedTradeSnapshot($searchedTradeArys);    
    }
    $kReport->deleteOldSearchedTrades();
}
//
fwrite($f, "\n\n###################Cronjob End:".date("d-m-Y h:i:s")."#######################\n");


$f = fopen(__APP_PATH_LOGS__."/searchedTradeCronjob.log", "a"); 
 
closeDB($newlink);
?>