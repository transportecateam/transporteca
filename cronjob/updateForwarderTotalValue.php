<?php
//session_start();
/**
 * Currency conversion cronjob
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    ini_set (max_execution_time,360000);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_once('functions.php');
  
$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
    require_once(__APP_PATH_CLASSES__."/billing.class.php"); 
}   

$kForwarder = new cForwarder();
$kBilling = new cBilling();

$activeForwarderAry = array();
$activeForwarderAry = $kForwarder->getAllForwarder(false,false,true);

if(!empty($activeForwarderAry))
{
    foreach($activeForwarderAry as $activeForwarderArys)
    {  
        $idForwarder = $activeForwarderArys['id'];  
        $forwarderTotalValueAry = array();
        $forwarderTotalValueAry = $kBilling->getTotalBokingValueSnapshotByForwarder($idForwarder);
             
        if(!empty($forwarderTotalValueAry))
        { 
            $currentBalArr = array();
            $szCurrentTotalValStr = "";
            foreach($forwarderTotalValueAry as $szCurrencyName=>$forwarderTotalValueArys)
            {
                if(!empty($szCurrencyName))
                {
                    $addSnapshotAry = array();
                    $addSnapshotAry['idForwarder'] = $idForwarder;
                    $addSnapshotAry['szCurrencyName'] = $szCurrencyName;
                    $addSnapshotAry['fForwarderTotalVolume'] = round((float)$forwarderTotalValueArys['fTotalBalance'],2);
                   
                    $kBilling->addForwarderBookingSnapshot($addSnapshotAry);
                } 
            }    
        }
    }
}

?>