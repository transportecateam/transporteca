<?php
//session_start();
/**
 * Major functinality of this cronscript
 * 1. Fetch all the data from tblnotification where iNotificationSent='0' 
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    ini_set (max_execution_time,36000);
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_once('functions.php');

//error_reporting(E_ALL);
//ini_set('error_reporting','E_ALL');
//ini_set('display_error','on');

$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/haulagePricing.class.php");
	require_once(__APP_PATH_CLASSES__."/config.class.php");
	require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
}

$kWHSSearch = new cWHSSearch();
		
//Send notification email 
$notificationAry = array();
$notificationAry = $kWHSSearch->getAllNotificationTobeSent();
$notificationSentAry = array();
$ctr = 0;
if(!empty($notificationAry))
{
	foreach($notificationAry as $notificationArys)
	{
		$idNotificationType = (int)$notificationArys['idNotificationType'] ;
		if($idNotificationType==__NOTIFICATION_TYPE_NO_SERVICE_COUNTRY_TO_COUNTRY___)
		{
			if($kWHSSearch->isServiceExists($notificationArys['idOriginCountry'],$notificationArys['idDestinationCountry'],'iPTP'))
			{
				// sending emails 
				$kWHSSearch->sendNotificationEmail($notificationArys,$idNotificationType);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
		}
		else if($idNotificationType==__NOTIFICATION_TYPE_NO_DELIVERY_TO_DESTINATION_COUNTRY___)
		{
			if($kWHSSearch->isServiceExists($notificationArys['idOriginCountry'],$notificationArys['idDestinationCountry'],'iPTD'))
			{
				// sending emails 
				$kWHSSearch->sendNotificationEmail($notificationArys,$idNotificationType);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
		}
		else if($idNotificationType==__NOTIFICATION_TYPE_NO_PICKUP_FROM_ORIGIN_COUNTRY___)
		{
			if($kWHSSearch->isServiceExists($notificationArys['idOriginCountry'],$notificationArys['idDestinationCountry'],'iDTP'))
			{
				// sending emails
				$kWHSSearch->sendNotificationEmail($notificationArys,$idNotificationType);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
		}
		else if($idNotificationType==__NOTIFICATION_TYPE_NO_PICKUP_AND_NO_DELIVERY___)
		{
			if($kWHSSearch->isServiceExists($notificationArys['idOriginCountry'],$notificationArys['idDestinationCountry'],'iDTD'))
			{
				// sending emails
				$kWHSSearch->sendNotificationEmail($notificationArys,$idNotificationType);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
		}
		else if($idNotificationType==__NOTIFICATION_TYPE_RP1_RP2___)
		{
			/*
			 * a.Make the sum of the services columns (PTP,PTW,�) for each row with Country A to Country B combination
			 * b.Test for each forwarders with a country A to country B row if the sum for the row with iStatus = 1 is greater than the row with iStatus = 2 (if there is no row with iStatus = 2, the sum considered is 0)
			 * c.If YES � we have new services � then send this mail to user
			 * 
			 * */
			if($kWHSSearch->checkNewServiceAdded($notificationArys))
			{
				// sending emails
				$kWHSSearch->sendNotificationEmail($notificationArys,$idNotificationType);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
		}
		else if($idNotificationType==__NOTIFICATION_TYPE_S1_S2___)
		{
			/* if(dd/mm/yyyy<= today) THEN 'send sorry email'; ELSE 
			 *  IF tblservices has PTP=1 for CountryA to CountryB with ValidFrom < dd/mm/yyyy < ValidTo
			 */
			//echo "<br /> timing date ".$notificationArys['dtTiming'] ;
			//echo "<br /> current date ".date('Y-m-d');
			$dtTimingTime = strtotime($notificationArys['dtTiming']);
			if($dtTimingTime < time())
			{
				//echo "<br /> sending email of type ".__NOTIFICATION_TYPE_SORRY_EMAIL_S1_S2___ ;
				$kWHSSearch->sendNotificationEmail($notificationArys,__NOTIFICATION_TYPE_SORRY_EMAIL_S1_S2___);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}				
			else if($kWHSSearch->isValidServiceExists($notificationArys['idOriginCountry'],$notificationArys['idDestinationCountry'],'iPTP',$notificationArys['dtTiming'],1))
			{
				// sending emails
				$kWHSSearch->sendNotificationEmail($notificationArys,$idNotificationType);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
		}
		else if($idNotificationType==__NOTIFICATION_TYPE_T1_T2___)
		{
			/*
			 *  tblservices has PTP=1 for CountryA to CountryB with (ValidFrom+transitdays) < dd/mm/yyyy < (ValidTo+transitdays)
			 */
			$dtTimingTime = strtotime($notificationArys['dtTiming']);
			//echo "<br /> timing date ".$notificationArys['dtTiming'] ;
			//echo "<br /> current date ".date('Y-m-d');
			
			if($dtTimingTime < time())
			{
				//echo "<br /> sending email of type ".__NOTIFICATION_TYPE_SORRY_EMAIL_T1_T2___ ;
				$kWHSSearch->sendNotificationEmail($notificationArys,__NOTIFICATION_TYPE_SORRY_EMAIL_T1_T2___);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
			else if($kWHSSearch->isValidServiceExists($notificationArys['idOriginCountry'],$notificationArys['idDestinationCountry'],'iPTP',$notificationArys['dtTiming'],2))
			{
				// sending emails
				$kWHSSearch->sendNotificationEmail($notificationArys,$idNotificationType);
				$notificationSentAry[$ctr] = $notificationArys['id'];
				$ctr++;
			}
		}
	}
	
	if(!empty($notificationSentAry))
	{
		$kWHSSearch->updateNotificationStatus($notificationSentAry);
	}
}
?>