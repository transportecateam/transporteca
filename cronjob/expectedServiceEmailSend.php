<?php
//session_start();
/**
 * Send Expected Service Email To Customer
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
  ini_set (max_execution_time,360000);
  
  $newlink=getTransportecaDB();
if(!class_exists('cWHSSearch'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
	require_once(__APP_PATH_CLASSES__."/config.class.php");
}
$kWHSSearch = new cWHSSearch();
$kConfig = new cConfig();
$getExpectedServices=$kWHSSearch->getExpectedServices();
$warehouseEmailFoundArr=array();
if(!empty($getExpectedServices))
{
	foreach($getExpectedServices as $getExpectedService)
	{
		$successflag=false;
		$warehouseto_ary=array();
		$warehousefrom_ary=array();
		if($getExpectedService['idOriginCountry']>0)
		{
			$warehousefrom_ary=$kWHSSearch->getWarehouseFromCountryId($getExpectedService['idOriginCountry']);
			//print_r($warehousefrom_ary);
			if(!empty($warehousefrom_ary))
			{
				$warehouseto_ary=$kWHSSearch->getWarehouseFromCountryId($getExpectedService['idDestinationCountry']);
				//print_r($warehouseto_ary);
				if(!empty($warehouseto_ary))
				{
					foreach($warehousefrom_ary as $warehousefrom_arys)
					{
						$fromWarehouseId=$warehousefrom_arys;
						
						foreach($warehouseto_ary as $warehouseto_arys)
						{
							$toWarehouseId=$warehouseto_arys;
							//echo "hello";
							if($kWHSSearch->getLCLServiceWarehouses($fromWarehouseId,$toWarehouseId))
							{
								$countryfromto='';
								$countryfromto=$getExpectedService['idOriginCountry']."_".$getExpectedService['idDestinationCountry'];
								$warehouseEmailFoundArr[]=$countryfromto;
								$successflag=true;
								break;
							}
						}
						if($successflag)
						{
							break;
						}
					}
				}
			}
		}
	}
		
	if(!empty($warehouseEmailFoundArr))
	{
		foreach($warehouseEmailFoundArr as $warehouseEmailFoundArrs)
		{
			$countriesArr=array();
			$countriesArr=explode("_",$warehouseEmailFoundArrs);
			$expectedDetailData=$kWHSSearch->getExpectedServicesDetails($countriesArr[0],$countriesArr[1]);
			if(!empty($expectedDetailData))
			{
				foreach($expectedDetailData as $expectedDetailDatas)
				{
					$replace_ary['szSubject']="Services from ".htmlentities($expectedDetailDatas['szOriginCountry'],ENT_COMPAT, "UTF-8")." to ".htmlentities($expectedDetailDatas['szDestinationCountry'],ENT_COMPAT, "UTF-8")." now available on Transporteca";
					$replace_ary['szEmail']=$expectedDetailDatas['szEmail'];
					$replace_ary['szfromcountry']=htmlentities($expectedDetailDatas['szOriginCountry'],ENT_COMPAT, "UTF-8");
					$replace_ary['sztocountry']=htmlentities($expectedDetailDatas['szDestinationCountry'],ENT_COMPAT, "UTF-8");
					createEmail(__EXPECTED_SERVICES_EMAIL_SEND__, $replace_ary,$replace_ary['szEmail'],$replace_ary['szSubject'], __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__);
				}
				$kWHSSearch->updateSendNotificationFlag($countriesArr[0],$countriesArr[1]);
			}
		}
	}
}
?>