<?php
//session_start();
/**
 * Remove Not Submitted Quote From Forwarder Pending Tray After 2 Weeks.
 */

if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
 ini_set (max_execution_time,360000);
$newlink=getTransportecaDB();
if(!class_exists('cBooking'))
{
    require_once(__APP_PATH_CLASSES__."/error.class.php");
    require_once(__APP_PATH_CLASSES__."/database.class.php");
    require_once(__APP_PATH_CLASSES__."/booking.class.php"); 
}

$kBooking=new cBooking();

$dtRemoved=date('Y-m-d',strtotime('- 14 DAY'));

echo $dtRemoved."dtRemoved<br />";
$kBooking->removeNotSubmittedQuoteFromForwarderPendingTray($dtRemoved);