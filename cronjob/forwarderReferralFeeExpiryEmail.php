<?php
/**
 * Send email to management thatl referral fee of forwarder will expiry in 30  days.  
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
include_once('functions.php');
ini_set(max_execution_time,360000);

$newlink=getTransportecaDB();
if(!class_exists('cForwarder'))
{
	require_once(__APP_PATH_CLASSES__."/error.class.php");
	require_once(__APP_PATH_CLASSES__."/database.class.php");
	require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
	require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
}

$kForwarder=new cForwarder();

$dtExpiry=date('Y-m-d',strtotime('+ 30 DAY'));
//echo $dtExpiry;
//die;
$getAllForwarderReferralFeeExpiryIN30Days=$kForwarder->getAllForwarderReferralFeeExpiryIn30Days($dtExpiry);

//print_r($getAllForwarderReferralFeeExpiryIN30Days);
if(!empty($getAllForwarderReferralFeeExpiryIN30Days))
{
	$ctr=0;
//	$str .=" The referral fee  will expire in 30 days for following forwarders.Please update the pricing and validity in the Management Module.<br/><br/>";
	foreach($getAllForwarderReferralFeeExpiryIN30Days as $getAllForwarderReferralFeeExpiryIN30Day)
	{
		 $str .=++$ctr.". ".$getAllForwarderReferralFeeExpiryIN30Day['szDisplayName']." (".$getAllForwarderReferralFeeExpiryIN30Day['szForwarderAlias'].")<br/>";
	}
	//$getAllAdminEmailArr=$kForwarder->getAllAdminEmail();
	
	//echo "<br/><br/>";
	//print_r($getAllAdminEmailArr);
	$replace_ary['szString'] = $str;
	$replace_ary['szExpiryDays'] = "in 30 days";
	
	$kWHSSearch = new cWHSSearch();
	$toEmail=$kWHSSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
	createEmail(__REFERRAL_FEE_EXPIRY_EMAIL__, $replace_ary,$toEmail,' ', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,false);
		
	
}

//Referral fee expiry tomorrow/// 
$dtExpiry=date('Y-m-d',strtotime('+ 1 DAY'));
//echo $dtExpiry;
$getAllForwarderReferralFeeExpiryIN1Days=$kForwarder->getAllForwarderReferralFeeExpiryIn30Days($dtExpiry);

//print_r($getAllForwarderReferralFeeExpiryIN30Days);
if(!empty($getAllForwarderReferralFeeExpiryIN1Days))
{
	$ctr=0;
//	$str .=" The referral fee  will expire in 30 days for following forwarders.Please update the pricing and validity in the Management Module.<br/><br/>";
	foreach($getAllForwarderReferralFeeExpiryIN1Days as $getAllForwarderReferralFeeExpiryIN1Day)
	{
		 $str .=++$ctr.". ".$getAllForwarderReferralFeeExpiryIN1Day['szDisplayName']." (".$getAllForwarderReferralFeeExpiryIN30Day['szForwarderAlias'].")<br/>";
	}
	//$getAllAdminEmailArr=$kForwarder->getAllAdminEmail();
	
	//echo "<br/><br/>";
	//print_r($getAllAdminEmailArr);
	$replace_ary['szString'] = $str;
	$replace_ary['szExpiryDays'] = 'tomorrow';
	
	$kWHSSearch = new cWHSSearch();
	$toEmail=$kWHSSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
	createEmail(__REFERRAL_FEE_EXPIRY_EMAIL__, $replace_ary,$toEmail,' ', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__,false);
		
	
}

?>