<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle=__OUR_COMPANY_PAGE_META_TITLE__;
$szMetaKeywords = __OUR_COMPANY_PAGE_META_KEYWORDS__;
$szMetaDescription= __OUR_COMPANY_PAGE_META_DESCRIPTION__;

require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );
$KwarehouseSearch=new cWHSSearch();
$iLanguage = getLanguageId();

//if($iLanguage ==__LANGUAGE_ID_DANISH__)
//{
//	$ourCompany=$KwarehouseSearch->getManageMentVariableByDescription(__OUR_COMPANY_DANISH__);
//}
//else
//{
	$ourCompany=$KwarehouseSearch->getManageMentVariableByDescription(__OUR_COMPANY__,$iLanguage);
//}
$ourCompany = html_entity_decode($ourCompany);

$t_base="Company/"
?>
<div id="hsbody">
	<h5><strong><?=t($t_base.'title/our_company')?></strong></h5>
	<div class="link16">
	<? 
		if($ourCompany)
		{
			echo refineDescriptionData($ourCompany);
		}		
?>
	</div>
	<br />
	<br />
	<p><a href="<?=__REQUIREMENT_PAGE_URL__?>"><?=t($t_base.'title/booking_link')?></a></p>
	<br />
	<br />
</div>


<?php
include( __APP_PATH_LAYOUT__ . "/footer.php" );
?>