<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );

$iGetLanguageFromRequest = 1; 
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
$t_base = "BookingDetails/";
$operation_type = sanitize_all_html_input(trim($_REQUEST['type']));
$kWHSSearch=new cWHSSearch();
$kConfig=new cConfig();
if($operation_type =='SHIPPER')
{
    $kConfig = new cConfig();
    $iLanguage = getLanguageId();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
    $idServiceType = sanitize_all_html_input(trim($_REQUEST['idServiceType']));
    if($idServiceType==__SERVICE_TYPE_DTD__ || $idServiceType==__SERVICE_TYPE_DTW__ || $idServiceType==__SERVICE_TYPE_DTP__) //DTD or DTW
    {
        $display_pickup_checkbox = true;
    }
    if($idServiceType==__SERVICE_TYPE_DTD__ || $idServiceType==__SERVICE_TYPE_WTD__ || $idServiceType==__SERVICE_TYPE_PTD__) //DTD or WTD
    {
        $display_delivery_checkbox = true;
    }
    $visibility = sanitize_all_html_input(trim($_REQUEST['visibility']));
    if($visibility==1)
    {
        $display_pickup_address='none';
    }
    else
    {
        $display_pickup_address='block';
    }
    if($_REQUEST['idShipperConsignee']!='New')
    {
        $kRegisterShipCon = new cRegisterShipCon();	
        $idShipper = sanitize_all_html_input(trim($_REQUEST['idShipperConsignee']));
        if(!empty($idShipper))
        {
            $idShipperAry=explode('_',$idShipper);
        }

        if($idShipperAry[1]=='idUser')
        {
            $kUser=new cUser();
            $kUser->getUserDetails($idShipperAry[0]);

            $shipperConsigneeAry['szShipperCompanyName'] = $kUser->szCompanyName ;
            $shipperConsigneeAry['szShipperFirstName'] = $kUser->szFirstName ;
            $shipperConsigneeAry['szShipperLastName'] = $kUser->szLastName ;
            $shipperConsigneeAry['szShipperEmail'] = $kUser->szEmail ;
            $shipperConsigneeAry['szShipperPhone'] = $kUser->szPhoneNumber ;
            $shipperConsigneeAry['idShipperCountry'] =  $kUser->szCountry ;	
            $shipperConsigneeAry['szShipperCity'] = $kUser->szCity ;
            $shipperConsigneeAry['szShipperPostCode'] = $kUser->szPostcode ;
            $shipperConsigneeAry['szShipperAddress'] = $kUser->szAddress1 ;
            $shipperConsigneeAry['szShipperAddress2'] = $kUser->szAddress2 ;
            $shipperConsigneeAry['szShipperAddress3'] = $kUser->szAddress3 ;
            $shipperConsigneeAry['szShipperState'] =  $kUser->szState ;

            $shipperConsigneeAry['idShipperCountry_pickup'] =  $kUser->szCountry ;	
            $shipperConsigneeAry['szShipperCity_pickup'] = $kUser->szCity ;
            $shipperConsigneeAry['szShipperPostcode_pickup'] = $kUser->szPostcode ;
            $shipperConsigneeAry['szShipperAddress_pickup'] = $kUser->szAddress1 ;
            $shipperConsigneeAry['szShipperAddress2_pickup'] = $kUser->szAddress2 ;
            $shipperConsigneeAry['szShipperAddress3_pickup'] = $kUser->szAddress3 ;
            $shipperConsigneeAry['szShipperState_pickup'] =  $kUser->szState ;
        }
        else
        {
            $kRegisterShipCon->getShipperConsignessDetail($idShipper);

            $shipperConsigneeAry['szShipperCompanyName'] = $kRegisterShipCon->szCompanyName ;
            $shipperConsigneeAry['szShipperFirstName'] = $kRegisterShipCon->szFirstName ;
            $shipperConsigneeAry['szShipperLastName'] = $kRegisterShipCon->szLastName ;
            $shipperConsigneeAry['szShipperEmail'] = $kRegisterShipCon->szEmail ;
            $shipperConsigneeAry['szShipperPhone'] = $kRegisterShipCon->szPhoneNumber ;
            $shipperConsigneeAry['idShipperCountry'] =  $kRegisterShipCon->szCountry ;	
            $shipperConsigneeAry['szShipperCity'] = $kRegisterShipCon->szCity ;
            $shipperConsigneeAry['szShipperPostCode'] = $kRegisterShipCon->szPostcode ;
            $shipperConsigneeAry['szShipperAddress'] = $kRegisterShipCon->szAddress1 ;
            $shipperConsigneeAry['szShipperAddress2'] = $kRegisterShipCon->szAddress2 ;
            $shipperConsigneeAry['szShipperAddress3'] = $kRegisterShipCon->szAddress3 ;
            $shipperConsigneeAry['szShipperState'] =  $kRegisterShipCon->szState ;

            $shipperConsigneeAry['idShipperCountry_pickup'] =  $kRegisterShipCon->szCountry ;	
            $shipperConsigneeAry['szShipperCity_pickup'] = $kRegisterShipCon->szCity ;
            $shipperConsigneeAry['szShipperPostcode_pickup'] = $kRegisterShipCon->szPostcode ;
            $shipperConsigneeAry['szShipperAddress_pickup'] = $kRegisterShipCon->szAddress1 ;
            $shipperConsigneeAry['szShipperAddress2_pickup'] = $kRegisterShipCon->szAddress2 ;
            $shipperConsigneeAry['szShipperAddress3_pickup'] = $kRegisterShipCon->szAddress3 ;
            $shipperConsigneeAry['szShipperState_pickup'] =  $kRegisterShipCon->szState ;
        }
    }
    else
    {
        //$display_pickup_address ='none';		
        $kBooking = new cBooking();
        $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

        $postSearchAry = $kBooking->getBookingDetails($idBooking);

        $shipperConsigneeAry['idShipperCountry'] =  $postSearchAry['idOriginCountry'] ;	
        $shipperConsigneeAry['szShipperCity'] = $postSearchAry['szOriginCity'] ;	
        $shipperConsigneeAry['szShipperPostcode'] = $postSearchAry['szOriginPostCode'] ;	

        $shipperConsigneeAry['idShipperCountry_pickup'] =  $postSearchAry['idOriginCountry'] ;	
        $shipperConsigneeAry['szShipperCity_pickup'] = $postSearchAry['szOriginCity'] ;	
        $shipperConsigneeAry['szShipperPostcode_pickup'] = $postSearchAry['szOriginPostCode'] ;	
    }

    if($display_pickup_address=='none')
    {
        if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__ )  //DTD 
        {		
            $disable_shipper_fields = true ;
            $disable_consignee_fields = true ;
        }
        else if($postSearchAry['idServiceType'] == __SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__) //DTW
        {
            $disable_shipper_fields = true ;
            $disable_consignee_fields = false ;
        }
        else if($postSearchAry['idServiceType'] == __SERVICE_TYPE_WTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__) //DTW
        {
            $disable_shipper_fields = false ;
            $disable_consignee_fields = true ;
        }
        else
        {
            $disable_shipper_fields = false ;
            $disable_consignee_fields = false ;
        }
    }
    //print_r($shipperConsigneeAry);
    //echo " id service ".$idServiceType ;
    display_shipper_information_html($shipperConsigneeAry,$postSearchAry,$t_base,$allCountriesArr,$display_pickup_address,$disable_shipper_fields);
}
else if($operation_type =='CONSIGNEE')
{
    $kConfig = new cConfig();
    $iLanguage = getLanguageId();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
    $idServiceType = sanitize_all_html_input(trim($_REQUEST['idServiceType']));
    $visibility = sanitize_all_html_input(trim($_REQUEST['visibility']));
    if($visibility==1)
    {
        $display_delivery_address='none';
    }
    else
    {
        $display_delivery_address='block';
    }
    if($_REQUEST['idShipperConsignee']!='New')
    {
        $kRegisterShipCon = new cRegisterShipCon();	
        $idShipper = $_REQUEST['idShipperConsignee'];
        if(!empty($idShipper))
        {
            $idShipperAry=explode('_',$idShipper);
        }		
        if($idShipperAry[1]=='idUser')
        {
            $kUser=new cUser();
            $kUser->getUserDetails($idShipperAry[0]);

            $shipperConsigneeAry['szConsigneeCompanyName'] = $kUser->szCompanyName ;
            $shipperConsigneeAry['szConsigneeFirstName'] = $kUser->szFirstName ;
            $shipperConsigneeAry['szConsigneeLastName'] = $kUser->szLastName ;
            $shipperConsigneeAry['szConsigneeEmail'] = $kUser->szEmail ;
            $shipperConsigneeAry['szConsigneePhone'] = $kUser->szPhoneNumber ;
            $shipperConsigneeAry['idConsigneeCountry'] =  $kUser->szCountry ;	
            $shipperConsigneeAry['szConsigneeCity'] = $kUser->szCity ;
            $shipperConsigneeAry['szConsigneePostCode'] = $kUser->szPostcode ;
            $shipperConsigneeAry['szConsigneeAddress'] = $kUser->szAddress1 ;
            $shipperConsigneeAry['szConsigneeAddress2'] = $kUser->szAddress2 ;
            $shipperConsigneeAry['szConsigneeAddress3'] = $kUser->szAddress3 ;
            $shipperConsigneeAry['szConsigneeState'] =  $kUser->szState ;

            $shipperConsigneeAry['idConsigneeCountry_pickup'] =  $kUser->szCountry ;	
            $shipperConsigneeAry['szConsigneeCity_pickup'] = $kUser->szCity ;
            $shipperConsigneeAry['szConsigneePostcode_pickup'] = $kUser->szPostcode ;
            $shipperConsigneeAry['szConsigneeAddress_pickup'] = $kUser->szAddress1 ;
            $shipperConsigneeAry['szConsigneeAddress2_pickup'] = $kUser->szAddress2 ;
            $shipperConsigneeAry['szConsigneeAddress3_pickup'] = $kUser->szAddress3 ;
            $shipperConsigneeAry['szConsigneeState_pickup'] =  $kUser->szState ;
        }
        else
        {
            $kRegisterShipCon->getShipperConsignessDetail($idShipper);

            $shipperConsigneeAry['szConsigneeCompanyName'] = $kRegisterShipCon->szCompanyName ;
            $shipperConsigneeAry['szConsigneeFirstName'] = $kRegisterShipCon->szFirstName ;
            $shipperConsigneeAry['szConsigneeLastName'] = $kRegisterShipCon->szLastName ;
            $shipperConsigneeAry['szConsigneeEmail'] = $kRegisterShipCon->szEmail ;
            $shipperConsigneeAry['szConsigneePhone'] = $kRegisterShipCon->szPhoneNumber ;
            $shipperConsigneeAry['idConsigneeCountry'] =  $kRegisterShipCon->szCountry ;	
            $shipperConsigneeAry['szConsigneeCity'] = $kRegisterShipCon->szCity ;
            $shipperConsigneeAry['szConsigneePostCode'] = $kRegisterShipCon->szPostcode ;
            $shipperConsigneeAry['szConsigneeAddress'] = $kRegisterShipCon->szAddress1 ;
            $shipperConsigneeAry['szConsigneeAddress2'] = $kRegisterShipCon->szAddress2 ;
            $shipperConsigneeAry['szConsigneeAddress3'] = $kRegisterShipCon->szAddress3 ;
            $shipperConsigneeAry['szConsigneeState'] =  $kRegisterShipCon->szState ;

            $shipperConsigneeAry['idConsigneeCountry_pickup'] =  $kRegisterShipCon->szCountry ;	
            $shipperConsigneeAry['szConsigneeCity_pickup'] = $kRegisterShipCon->szCity ;
            $shipperConsigneeAry['szConsigneePostcode_pickup'] = $kRegisterShipCon->szPostcode ;
            $shipperConsigneeAry['szConsigneeAddress_pickup'] = $kRegisterShipCon->szAddress1 ;
            $shipperConsigneeAry['szConsigneeAddress2_pickup'] = $kRegisterShipCon->szAddress2 ;
            $shipperConsigneeAry['szConsigneeAddress3_pickup'] = $kRegisterShipCon->szAddress3 ;
            $shipperConsigneeAry['szConsigneeState_pickup'] =  $kRegisterShipCon->szState ;
        }
    }
    else
    {
        //$display_pickup_address='none';
        $kBooking = new cBooking();
        $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

        $postSearchAry = $kBooking->getBookingDetails($idBooking);

        $shipperConsigneeAry['idConsigneeCountry'] =  $postSearchAry['idDestinationCountry'] ;	
        $shipperConsigneeAry['szConsigneeCity'] = $postSearchAry['szDestinationCity'] ;	
        $shipperConsigneeAry['szConsigneePostcode'] = $postSearchAry['szDestinationPostCode'] ;	

        $shipperConsigneeAry['idConsigneeCountry_pickup'] =  $postSearchAry['idDestinationCountry'] ;	
        $shipperConsigneeAry['szConsigneeCity_pickup'] = $postSearchAry['szDestinationCity'] ;	
        $shipperConsigneeAry['szConsigneePostcode_pickup'] = $postSearchAry['szDestinationPostCode'] ;
        //$display_delivery_address='none';	
    } 
    if($display_delivery_address=='none')
    {
            if($postSearchAry['idServiceType'] == __SERVICE_TYPE_DTD__) //DTD
            {		
                    $disable_shipper_fields = true ;
                    $disable_consignee_fields = true ;
            }
            else if($postSearchAry['idServiceType'] == __SERVICE_TYPE_DTW__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_DTP__) //DTW
            {
                    $disable_shipper_fields = true ;
                    $disable_consignee_fields = false ;
            }
            else if($postSearchAry['idServiceType'] == __SERVICE_TYPE_PTD__ || $postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__) //WTD
            {
                    $disable_shipper_fields = false ;
                    $disable_consignee_fields = true ;
            }
            else
            {
                    $disable_shipper_fields = false ;
                    $disable_consignee_fields = false ;
            }
    }

    echo display_consignee_information_html($shipperConsigneeAry,$postSearchAry,$t_base,$allCountriesArr,$display_delivery_address,$disable_consignee_fields);
}
else if($operation_type =='SHIPPER_DROPDOWN')
{
	$kRegisterShipCon = new cRegisterShipCon();
	$registShippersAry = array();			
	$idCustomer = (int)$_SESSION['user_id'];
	$szPostCode = sanitize_all_html_input(trim($_REQUEST['szPostCode']));	
	$szCity = sanitize_all_html_input(trim($_REQUEST['szCity']));	
	$idCountry = sanitize_all_html_input(trim($_REQUEST['idCountry']));	

	$iShowByPostCode = sanitize_all_html_input(trim($_REQUEST['iShowByPostCode']));
	if($iShowByPostCode==2)
	{
		// getting all registered shippers of the user 
		$registShippersAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('ship',$idCustomer,$szPostCode,$idCountry,$szCity);
	}
	elseif($iShowByPostCode==1)
	{
		// getting all registered shippers of the user 
		$registShippersAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('ship',$idCustomer);	
	}
	?>
	<select size="1" id="idRegistShipper" name="shipperConsigneeAry[idRegistShipper]" onchange="auto_fill_shipper_consignee(this.value,'SHIPPER','shipper_info_div')">
		<option value="New">New</option>
		<?php
			if(!empty($registShippersAry))
			{
				foreach($registShippersAry as $registShippersArys)
				{
					?>										
					 <option value="<?=$registShippersArys['id']?>" <? if($shipperConsigneeAry['idRegistShipper']==$registShippersArys['id']) {?>selected<? }?>><?=$registShippersArys['szCompanyName']?></option>
					<?php
				}
			}
		?>
	</select>	
	<?php
}
else if($operation_type=='CONSIGNEE_DROPDOWN')
{
	$kRegisterShipCon = new cRegisterShipCon();
	$registShippersAry = array();			
	$idCustomer = (int)$_SESSION['user_id'];
	$szPostCode = sanitize_all_html_input(trim($_REQUEST['szPostCode']));	
	$szCity = sanitize_all_html_input(trim($_REQUEST['szCity']));	
	$idCountry = sanitize_all_html_input(trim($_REQUEST['idCountry']));	
	$iShowByPostCode = sanitize_all_html_input(trim($_REQUEST['iShowByPostCode']));
	if($iShowByPostCode==2)
	{
		// getting all registered shippers of the user 
		$registShippersAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('con',$idCustomer,$szPostCode,$idCountry,$szCity);
	}
	elseif($iShowByPostCode==1)
	{
		// getting all registered shippers of the user 
		$registShippersAry = $kRegisterShipCon->getRegisteredShipperConsigness_bookingDetails('con',$idCustomer);	
	}
	?>
	<select size="1" id="idRegistConsignee" name="shipperConsigneeAry[idRegistConsignee]" onchange="auto_fill_shipper_consignee(this.value,'CONSIGNEE','consignee_info_div')">
		<option value="New">New</option>
		<?php
			if(!empty($registShippersAry))
			{
				foreach($registShippersAry as $registShippersArys)
				{
					?>										
					 <option value="<?=$registShippersArys['id']?>" <? if($shipperConsigneeAry['idRegistShipper']==$registShippersArys['id']) {?>selected<? }?>><?=$registShippersArys['szCompanyName']?></option>
					<?php
				}
			}
		?>
	</select>	
	<?php
}
else if($operation_type =='SHIPPER_NEW')
{ 
	$kConfig = new cConfig();
	$iLanguage = getLanguageId();
	
	$dialUpCodeAry = array();
	$dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
	
	$allCountriesArr = array();
	$allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
	
	$idServiceType = sanitize_all_html_input(trim($_REQUEST['idServiceType']));
	$szCompanyName = sanitize_all_html_input(trim($_REQUEST['company_name']));
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum']));
        $iGetShipperPostcodeFromGoogle = sanitize_all_html_input(trim($_REQUEST['iGetShipperPostcodeFromGoogle']));
	$shipperConsigneeAry = array();
	$iNobookingIdExists = 1 ;
	if(!empty($szBookingRandomNum))
	{
		$kBooking = new cBooking();
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		if($idBooking>0)
		{
			$postSearchAry = array();
			$postSearchAry = $kBooking->getBookingDetails($idBooking);
			$iNobookingIdExists = 2;
			$kRegisterShipCon = new cRegisterShipCon(); 
			$kRegisterShipCon->getShipperConsignessDetail(false,$szCompanyName); 
			
			if($kRegisterShipCon->id>0)
			{	
				$shipperConsigneeAry['idRegisteredShipper'] = $kRegisterShipCon->id ;	
				$shipperConsigneeAry['szShipperCompanyName'] = $kRegisterShipCon->szCompanyName ;
				$shipperConsigneeAry['szShipperFirstName'] = $kRegisterShipCon->szFirstName ;
				$shipperConsigneeAry['szShipperLastName'] = $kRegisterShipCon->szLastName ;
				$shipperConsigneeAry['szShipperEmail'] = $kRegisterShipCon->szEmail ;
				$shipperConsigneeAry['szShipperPhone'] = $kRegisterShipCon->szPhoneNumber ;
				$shipperConsigneeAry['idShipperCountry'] =  $kRegisterShipCon->szCountry ;	
				$shipperConsigneeAry['szShipperCity'] = $kRegisterShipCon->szCity ;
				$shipperConsigneeAry['szShipperPostCode'] = $kRegisterShipCon->szPostcode ;
				$shipperConsigneeAry['szShipperAddress'] = $kRegisterShipCon->szAddress1 ; 
				$shipperConsigneeAry['idShipperCountry_pickup'] =  $kRegisterShipCon->szCountry ;	
				$shipperConsigneeAry['idShipperDialCode'] =  $kRegisterShipCon->iInternationalDialCode ; 
				$shipperConsigneeAry['szShipperCity_pickup'] = $kRegisterShipCon->szCity ;
				$shipperConsigneeAry['szShipperPostcode_pickup'] = $kRegisterShipCon->szPostcode ;
				$shipperConsigneeAry['szShipperAddress_pickup'] = $kRegisterShipCon->szAddress1 ; 
                                $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = $iGetShipperPostcodeFromGoogle ; 
			} 
			echo "SUCCESS||||";
			echo display_shipper_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry); 
			die;
		}
	}
	
	if($iNobookingIdExists==1)
	{
		echo "BOOKING_NOTIFICATION||||" ;
		$redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
		echo display_booking_not_found_popup($redirect_url);
		die;
	}
}
else if($operation_type == 'CONSIGNEE_NEW')
{
    $kConfig = new cConfig();
    $iLanguage = getLanguageId(); 

    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);

    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);

    $idServiceType = sanitize_all_html_input(trim($_REQUEST['idServiceType']));
    $szCompanyName = sanitize_all_html_input(trim($_REQUEST['company_name']));
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum']));
    $iGetConsigneePostcodeFromGoogle = sanitize_all_html_input(trim($_REQUEST['iGetConsigneePostcodeFromGoogle']));
    $iNobookingIdExists = 1;
    if(!empty($szBookingRandomNum))
    {
        $kBooking = new cBooking();
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
        if($idBooking>0)
        {
            $postSearchAry = array();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $iNobookingIdExists = 2 ;
            $kRegisterShipCon = new cRegisterShipCon(); 
            $kRegisterShipCon->getShipperConsignessDetail(false,$szCompanyName); 
 
            if($kRegisterShipCon->id>0)
            {
                $shipperConsigneeAry['idRegisteredConsignee'] = $kRegisterShipCon->id ;
                $shipperConsigneeAry['szConsigneeCompanyName'] = $kRegisterShipCon->szCompanyName ;
                $shipperConsigneeAry['szConsigneeFirstName'] = $kRegisterShipCon->szFirstName ;
                $shipperConsigneeAry['szConsigneeLastName'] = $kRegisterShipCon->szLastName ;
                $shipperConsigneeAry['szConsigneeEmail'] = $kRegisterShipCon->szEmail ;
                $shipperConsigneeAry['szConsigneePhone'] = $kRegisterShipCon->szPhoneNumber ;
                $shipperConsigneeAry['idConsigneeCountry'] =  $kRegisterShipCon->szCountry ;
                $shipperConsigneeAry['idConsigneeDialCode'] =  $kRegisterShipCon->iInternationalDialCode ; 
                $shipperConsigneeAry['szConsigneeCity'] = $kRegisterShipCon->szCity ;
                $shipperConsigneeAry['szConsigneePostCode'] = $kRegisterShipCon->szPostcode ;
                $shipperConsigneeAry['szConsigneeAddress'] = $kRegisterShipCon->szAddress1 ;
                $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = $iGetConsigneePostcodeFromGoogle ;
            }
            echo "SUCCESS||||";
            echo display_consignee_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry);
            die;
        } 
    }	
    if($iNobookingIdExists==1)
    {
        echo "BOOKING_NOTIFICATION||||" ;
        $redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
        echo display_booking_not_found_popup($redirect_url);
        die;
    }
}
else if($operation_type=='SHOW_NON_ACCEPTANCE_POPUP')
{
    //displaying non acceptance popup
    $nonAcceptedAry = array();
    $kForwarder = new cForwarder();
    $idForwarder = sanitize_all_html_input(trim($_REQUEST['id']));	
    $idWTW = sanitize_all_html_input(trim($_REQUEST['idWTW']));	
    $iPageType = sanitize_all_html_input(trim($_REQUEST['page_type']));
    $idPackingType = sanitize_all_html_input(trim($_REQUEST['packing_type']));
    $iCourierService = sanitize_all_html_input(trim($_REQUEST['courier_service']));
    $LCDTDQuantityFlag = sanitize_all_html_input(trim($_REQUEST['LCDTDQuantityFlag']));
 
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum']));
    if(!empty($szBookingRandomNum))
    {
        $kBooking = new cBooking();
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
        if($idBooking>0)
        {
            $postSearchAry = array();
            $postSearchAry = $kBooking->getBookingDetails($idBooking);
             
            if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
            {
                echo "SUCCESS||||" ;
                $t_base_booking_details = "BookingDetails/";
                echo display_booking_already_paid($t_base_booking_details,$idBooking);
                die;
            }
            
            $res_ary=array();
            if((int)$LCDTDQuantityFlag>0)
            {
                if($LCDTDQuantityFlag==1)
                {
                    $iLCLCargoQuantity = 1;
                    $iPalletType = __PALLET_ID_HALF__;
                }
                else if($LCDTDQuantityFlag==2)
                {
                   $fCargoVolume= $postSearchAry['fCargoVolume']/__LCL_DTD_VOLUME_CONSTANT__;
                   $fCargoWeight = $postSearchAry['fCargoWeight']/__LCL_DTD_WEIGHT_CONSTANT__;
                  // echo round_up($fCargoWeight)."fCargoWeight-----".round_up($fCargoVolume);
                   $iLCLCargoQuantity = round_up(max($fCargoVolume ,$fCargoWeight));
                   $iPalletType = __PALLET_ID_EURO__;
                } 
                $kBooking->updateBoookingQuantityLCLDTD($iLCLCargoQuantity,$idBooking,$iLCLCargoQuantity,$postSearchAry['fCargoWeight'],$iPalletType,$postSearchAry['iBookingLanguage'],$postSearchAry['fCargoVolume']);
                
                $res_ary['iNumColli'] = $iLCLCargoQuantity;
                $res_ary['iTotalQuantity'] = $iLCLCargoQuantity;
                $res_ary['iPalletType'] = $iPalletType;
            } 
            if((int)$postSearchAry['iBookingStep']<9)
            {
                $res_ary['iBookingStep'] = 9 ;
            }
            if($iCourierService==1)
            {
                $res_ary['iCourierBooking'] = 1 ;
                $res_ary['idCourierPackingType'] = $idPackingType;
            }
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }
            $update_query = rtrim($update_query,","); 
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            { 
            }
        }
    }
    if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
    {
        $szBookingDetails = __NEW_BOOKING_DETAILS_PAGE_URL__ ;
        $iNewPageFlag=1 ;
    }
    else
    {
        $szBookingDetails = __BOOKING_DETAILS_PAGE_URL__ ;
        $iNewPageFlag=0 ;
    }

    $nonAcceptedAry = $kForwarder->getAllNonAcceptedGoodsForForwarder($idForwarder);
    $kForwarder->load($idForwarder);
    $success = false;
    if($_SESSION['user_id']>0)
    {
        $kUser=new cUser();
        $kUser->getUserDetails($_SESSION['user_id']);
        if($kForwarder->checkNonAcceptanceOfForwarder(false,$idForwarder,$kUser->szEmail,'EMAIL'))
        {
            $success = true;
            echo "SUCCESS||||"; 
            ?>
            <div id="popup-bg"></div>
            <div id="popup-container">
                    <div class="compare-popup popup">
                            <h5><?=t($t_base.'title/cargo_limitation');?></h5>
                            <p><?=t($t_base.'messages/can_not_access_with_email');?> <?=$kForwarder->szDisplayName?></p>
                            <br>
                            <p align="center">
                                    <a href="javascript:void(0);" onclick="showHide('customs_clearance_pop1')" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
                            </p>
                    </div>	
            </div>
            <?php
            die;
        }
    } 
    if(!$success)
    {
        echo "NO_RECORD||||".$szBookingDetails; 
    } 
    if($iCourierService==1)
    {
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking); 

        echo "||||";
        echo update_cargo_dimension($kBooking,$postSearchAry);
        die;
    }
}
else if($operation_type =='UPDATE_DEFAULT_CARGO')
{
	$kBooking = new cBooking();
	$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	if($idBooking>0)
	{
		$postSearchAry = array();
		$postSearchAry = $kBooking->getBookingDetails($idBooking);
		if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
		{
			echo "BOOKING_NOTIFICATION||||" ;
			$t_base_booking_details = "BookingDetails/";
			echo display_booking_already_paid($t_base_booking_details,$idBooking);
			die;
		}
		
		/*
		 * deafult cargo is (100 x 100 x 100 x 1 cm );(1000 kg ) (L x W x H x Q );(W)
		 */
		$kConfig = new cConfig();
		$kConfig->iLength[1] = 100;
		$kConfig->iWidth[1] = 100;
		$kConfig->iHeight[1] = 100;
		$kConfig->idCargoMeasure[1] = 1;
		$kConfig->iQuantity[1] = 1;
		$kConfig->iWeight[1] = 1000;
		$kConfig->idWeightMeasure[1] = 1000;
		
		$res_ary=array();
		$res_ary['fCargoVolume'] = 1;
		$res_ary['fCargoWeight'] = 1000 ;
		$res_ary['iDoNotKnowCargo'] = 1;
		
		if(!empty($res_ary))
		{
			$update_query = '';
			foreach($res_ary as $key=>$value)
			{
				$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
			}
		}
		$update_query = rtrim($update_query,",");
		
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			if($kBooking->updateCargoDetails($kConfig,$idBooking))
			{
				echo "SUCCESS";
				die;
			}
		}
	}
	else
	{
		echo "Invalid Booking-id ";
	}
}
else if(!empty($_REQUEST['shipperConsigneeAry']))
{ 
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['shipperConsigneeAry']['szBookingRandomNum'])); 
    $operation_type = sanitize_all_html_input(trim($_REQUEST['shipperConsigneeAry']['szOperationType'])); 

    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $kHaulagePricing = new cHaulagePricing();

    if((int)$idBooking>0)
    {
        $kBooking = new cBooking();
        $postSearchAry = array();
        $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);

        $szBookingRandomNum = $_REQUEST['shipperConsigneeAry']['szBookingRandomNum'] ;

        if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
        {
            echo "BOOKING_NOTIFICATION||||" ;
            $t_base_booking_details = "BookingDetails/";
            echo display_booking_already_paid($t_base_booking_details,$idBooking);
            die;
        }
        else if(($postSearchAry['idForwarder']<=0))
        {
            echo "BOOKING_NOTIFICATION||||" ;
            $redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
            echo display_booking_notification($redirect_url);
            die;
        }
    }
    else if($idBooking <= 0)
    {
        echo "BOOKING_NOTIFICATION||||" ;
        $redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
        echo display_booking_not_found_popup($redirect_url);
        die;
    }  
    $kRegisterShipCon = new cRegisterShipCon();
    if($operation_type =='NEW_BOOKING_PAGE')
    { 
        $iBookingQuotes = $_REQUEST['shipperConsigneeAry']['iBookingQuotesFlag'] ; 
        $iDonotCalculateAgain = $_REQUEST['shipperConsigneeAry']['iDonotCalculateAgain'] ; 

        $szOldShipperPostCode = $postSearchAry['szShipperPostCode'];
        $szOldConsigneePostCode = $postSearchAry['szConsigneePostCode'];
        
        if($_REQUEST['shipperConsigneeAry']['iShipperConsignee']==1 || $_REQUEST['shipperConsigneeAry']['iShipperConsignee']==2)
        {
            $_REQUEST['shipperConsigneeAry']['iThisIsMe'] = 1;
        }

        if($kRegisterShipCon->addShipperConsignee_new($_REQUEST['shipperConsigneeAry'],$idBooking))
        { 
            //If Booking is courier booking then we are recalculating prices on the basis of details user putted in shipper/consignee form
            $postSearchAry = $kBooking->getBookingDetails($idBooking);

            $szNewShipperPostcode = $kRegisterShipCon->szShipperPostcode;
            $szNewConsigneePostcode = $kRegisterShipCon->szConsigneePostcode;

            $redirect_flag=false;
            if($postSearchAry['iBookingType']==3 && $postSearchAry['iCourierBooking']==1 && $postSearchAry['iQuickQuote']!=__QUICK_QUOTE_SEND_QUOTE__)
            {
                if($iDonotCalculateAgain==2)
                {
                    //If user has not changed shipper/consignee postcode and clicks on continue again then we reload service else we just only displays error message.
                    if($szOldShipperPostCode==$szNewShipperPostcode && $szOldConsigneePostCode==$szNewConsigneePostcode)
                    {
                        $iDonotCalculateAgain = 2;
                    }
                    else
                    {
                        $iDonotCalculateAgain = 0;
                    }
                }

                if($kBooking->recalculateCourierPricing($idBooking,false,true))
                {
                    $redirect_flag=true; 
                    /*
                     * If user changes shipper/consignee postcode and if that postcode is in other city then we just update shipper/consignee city for e.g
                     * User searched 'Mumbai, India' and on shipper/consignee form he then enters postcode for Delhi e.f 100001 then we change shipper city from Mumbai to New Delhi.
                     */
                    //$kBooking->changeShipperConsigneeCity($idBooking);
                }
                else
                { 
                    if($iDonotCalculateAgain==2 && $kBooking->iDonotCalculateAgain==2)
                    {
                        $extendedBookingAry = array();
                        $extendedBookingAry = $kBooking->getExtendedBookingDetails($idBooking);

                        $idShipperConsignee = $extendedBookingAry['idShipperConsignee'];
                        $szShipperAddress = $extendedBookingAry['szShipperPostCode']." ".$extendedBookingAry['szShipperCity']." ".$extendedBookingAry['szShipperCountry'];
                        $szConsigneeAddress = $extendedBookingAry['szConsigneePostCode']." ".$extendedBookingAry['szConsigneeCity']." ".$extendedBookingAry['szConsigneeCountry'];

                        $res_ary = array();
                        $res_ary['szOriginCountry'] = $szShipperAddress;
                        $res_ary['szDestinationCountry'] = $szConsigneeAddress ; 

                        if(!empty($res_ary))
                        {
                            foreach($res_ary as $key=>$value)
                            {
                                $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                            }
                        }
                        $update_query = rtrim($update_query,",");

                        $kBooking->updateDraftBooking($update_query,$idBooking); 

                        $postSearchAry = $kBooking->getBookingDetails($idBooking);    
                        $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
                        $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking);   

                        $kConfig_new = new cConfig();
                        $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
                        $szCountryStrUrl = $kConfig_new->szCountryISO ; 
                        $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
                        $szCountryStrUrl .= $kConfig_new->szCountryISO ; 
                        if(!empty($_SESSION['load-booking-details-page-for-booking-id']) && $_SESSION['load-booking-details-page-for-booking-id']==$postSearchAry['szBookingRandomNum'])
                        {
                            $_SESSION['load-booking-details-page-for-booking-id'] = 0;
                            unset($_SESSION['load-booking-details-page-for-booking-id']);
                        }
                        $redirect_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                        echo "SUCCESS||||".$redirect_url;
                        die;
                    }
                    else
                    { 
                        $redirect_flag=false;
                        echo "DISPLAY_FORM_ERROR||||";
                        if($iBookingQuotes==1)
                        {
                            $flag=true;
                        }
                        else
                        {
                            $flag=false;
                        }
                        $kRegisterShipCon->szSpecialErrorMessage = $kBooking->arErrorMessages[szSpecialError];
                        $kRegisterShipCon->iDonotCalculateAgain = $kBooking->iDonotCalculateAgain;  
                        echo display_booking_details($postSearchAry,$kRegisterShipCon,$flag);
                        die;
                    }
                }  
            }
            else
            {
                $redirect_flag=true;
            }

            if($redirect_flag)
            { 
                $kConfig_new = new cConfig();
                $kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
                $szCountryStrUrl = $kConfig_new->szCountryISO ; 
                $kConfig_new->loadCountry($postSearchAry['idDestinationCountry']); 
                $szCountryStrUrl .= $kConfig_new->szCountryISO ; 

                if($iBookingQuotes==1 || $postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
                {
                    $redirect_url = __BOOKING_QUOTE_PAY_PAGE_URL__."/".$szBookingRandomNum."/".__OVERVIEW_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                }
                else
                { 
                    $redirect_url = __NEW_BOOKING_OVERVIEW_PAGE_URL__."/".$szBookingRandomNum."/".__OVERVIEW_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                }  
                echo "SUCCESS||||".$redirect_url;
                die; 
            }
        }
        else
        {
            echo "DISPLAY_FORM_ERROR||||";
            if($iBookingQuotes==1)
            {
                $flag=true;
            }
            else
            {
                $flag=false;
            }
            echo display_booking_details($postSearchAry,$kRegisterShipCon,$flag);
            die; 
        }
    }
    else
    {	
        $kRegisterShipCon->addShipperConsignee($_REQUEST['shipperConsigneeAry'],$idBooking);
        if(!empty($_POST['shipperConsigneeAry']['szShipperPhoneNumberUpdate']))
        {
            $_POST['shipperConsigneeAry']['szShipperPhone'] = urldecode(base64_decode($_POST['shipperConsigneeAry']['szShipperPhoneNumberUpdate']));
        }

        if(!empty($_POST['shipperConsigneeAry']['szConsigneePhoneNumberUpdate']))
        {
            $_POST['editUserInfoArr']['szConsigneePhone'] = urldecode(base64_decode($_POST['shipperConsigneeAry']['szConsigneePhoneNumberUpdate']));
        }
        if(!empty($kRegisterShipCon->arErrorMessages))
        {
            echo "ERROR ||||" ;
            ?>
            <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
            <div id="regErrorList">
                <ul>
                <?php
                    foreach($kRegisterShipCon->arErrorMessages as $key=>$values)
                    {
                      ?><li><?=$values?></li><?php 
                    }
                ?>
                </ul>
            </div>
            <?php
        }
        else
        {
            $redirect_url = __BOOKING_CONFIRMATION_PAGE_URL__ ;
            echo "SUCCESS||||".$redirect_url;
            die;
        }
    }
    die;
}
else if(!empty($_REQUEST['bookingQuotesAry']))
{ 
    $kBooking = new cBooking();
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['bookingQuotesAry']['szBookingRandomNum'])); 
    $operation_type = sanitize_all_html_input(trim($_REQUEST['bookingQuotesAry']['szOperationType']));  
    $mode = sanitize_all_html_input(trim($_REQUEST['mode']));  
    $iPrivateShipping = (int)$_REQUEST['bookingQuotesAry']['iPrivateShipping'];
     
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    $kHaulagePricing = new cHaulagePricing();
    
    if((int)$idBooking>0)
    {
        $kBooking = new cBooking();
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $szBookingRandomNum = $_REQUEST['bookingQuotesAry']['szBookingRandomNum'] ;

        if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
        {
            echo "BOOKING_NOTIFICATION||||" ;
            $t_base_booking_details = "BookingDetails/";
            echo display_booking_already_paid($t_base_booking_details,$idBooking);
            die;
        } 
    }
    else if($idBooking<=0)
    {
        echo "BOOKING_NOTIFICATION||||" ;
        $redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
        echo display_booking_not_found_popup($redirect_url);
        die;
    } 

    $kRegisterShipCon = new cRegisterShipCon();
    $kConfig = new cConfig();
     
    if($kRegisterShipCon->addBookingQuotesContactDetails($_REQUEST['bookingQuotesAry'],$idBooking,$mode))
    { 
        $kConfig->loadCountry($postSearchAry['idOriginCountry']);
        $szCountryStrUrl = $kConfig->szCountryISO ;
        $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
        $szCountryStrUrl .= $kConfig->szCountryISO ; 
        
        if($mode=='GET_RATES')
        { 
            $redirect_url = __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            if($iPrivateShipping==1)
            {
                $kExplain = new cExplain();
                $searchNotificationArr = array();
                $searchNotificationArr = $kExplain->isSearchNotificationExists($postSearchAry,true);
                   
                if(!empty($searchNotificationArr))
                { 
                    echo "SUCCESS_LOCAL_SEARCH||||";
                    echo display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,'','',$iSearchMiniPageVersion);
                    die;
                }
                else
                {
                    echo "SUCCESS||||".$redirect_url; 
                    die; 
                }
            }
            else
            { 
                echo "SUCCESS||||".$redirect_url;
                die; 
            }
        }
        else
        {
            $res_upd_arr=array();
            $res_upd_arr['szInternalComment'] = '';
 
            if(!empty($res_upd_arr))
            {
                $update_query = '';
                foreach($res_upd_arr as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");
            $kBooking->updateDraftBooking($update_query,$idBooking);

            
            
            $redirect_url = __BOOKING_QUOTE_THANK_YOU_PAGE_URL__."/".___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            echo "SUCCESS||||".$redirect_url;
            die; 
        }
        $kExplain = new cExplain();
        $searchNotificationArr = array();
        $searchNotificationArr = $kExplain->isSearchNotificationExists($postSearchAry);

        if(!empty($searchNotificationArr))
        { 
            echo "SUCCESS_LOCAL_SEARCH||||";
            echo display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,'','',$iSearchMiniPageVersion);
            die;
        } 
        if($mode=='GET_RATES')
        { 
            
        }
        else
        {
           
        } 
        
    }
    else
    {
        echo "ERROR||||";
        echo display_booking_quotes_contact_details($postSearchAry,$kRegisterShipCon);
        die; 
    } 
    die;
} 
else if(!empty($_REQUEST['searchAry']))
{
    $kConfig = new cConfig();
    $data = $_REQUEST['searchAry'] ;
    $kConfig->validateCargoDetails($data);
    $kBooking=new cBooking();

    $szBookingRandomNum = sanitize_all_html_input(trim($data['szBookingRandomNum']));
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

    $postSearchAry = $kBooking->getBookingDetails($idBooking);

    if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
    {
            echo "CARGO_EXCEED_POPUP||||" ;
            $t_base_booking_details = "BookingDetails/";
            echo display_booking_already_paid($t_base_booking_details,$idBooking);
            die;
    }

    if(!empty($kConfig->arErrorMessages))
    {
            echo "ERROR ||||" ;
    ?>
    <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
    <div id="regErrorList">
    <ul>
    <?php
          foreach($kConfig->arErrorMessages as $key=>$values)
          {
          ?><li><?=$values?></li>
          <?php 
          }
    ?>
    </ul>
    </div>
    <?php
    die;
    }
    else if($kConfig->iCargoExceed==1)
    {
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $_SESSION['cargo_details_'.$idBooking] = $kConfig->tempCargoAry ;

        echo "CARGO_EXCEED_POPUP||||";
        echo display_cargo_exceed_popup($postSearchAry,'POP_UP');
        die;
    }
    else if($kConfig->iDangerCargoFlag)
    {
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $_SESSION['cargo_details_'.$idBooking] = $kConfig->tempCargoAry ;
        echo "CARGO_EXCEED_POPUP||||";
        echo display_dangerous_cargo_popup($postSearchAry,'POP_UP');
        die;
    }
    //converting cargo details into cubic meter 			
    $counter_cargo = count($kConfig->iLength);
    for($i=1;$i<=$counter_cargo;$i++)
    {
        $fLength = $kConfig->iLength[$i] ;
        $fWidth = $kConfig->iWidth[$i] ;
        $fHeight = $kConfig->iHeight[$i] ;
        $iQuantity = $kConfig->iQuantity[$i] ;

        if($kConfig->idCargoMeasure[$i]==1)  // cm
        {
            $fTotalVolume += ($fLength/100) * ($fWidth/100) * ($fHeight/100) * $iQuantity;
        }
        else
        {
            $fCmFactor = $kWHSSearch->getCargoFactor($kConfig->idCargoMeasure[$i]);
            //echo "<br> cargo factor ".$fCmFactor ;

            if($fCmFactor>0)
            {
                $fTotalVolume += (($fLength/$fCmFactor )/100) * (($fWidth/$fCmFactor)/100) * (($fHeight/$fCmFactor)/100) * $iQuantity;
            }	
                //echo "<br> total volume ".$fTotalVolume ;
        }
    }

    $weight_cargo = count($kConfig->iWeight);
    for($i=1;$i<=$weight_cargo;$i++)
    {
        // converting cargo details into cubic meter 
        if($kConfig->idWeightMeasure[$i]==1)  // kg
        {
                $fTotalWeight += ($kConfig->iWeight[$i]);
        }
        else
        {
            $fKgFactor = $kWHSSearch->getWeightFactor($kConfig->idWeightMeasure[$i]);
            //echo "<br>  factor value ".$fKgFactor;
            if($fKgFactor>0)
            {
                $fTotalWeight += (($kConfig->iWeight[$i]/$fKgFactor ));
            }	
            //echo "<br> total weight ".$fTotalWeight ;
        }
    }

    $res_ary['fCargoVolume'] = round((float)$fTotalVolume,4);
    $res_ary['fCargoWeight'] = ciel((float)$fTotalWeight) ;
    //$res_ary['iDoNotKnowCargo'] = 0;

    if(!empty($res_ary))
    {
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }
    $update_query = rtrim($update_query,",");

    if($kBooking->updateDraftBooking($update_query,$idBooking))
    {
        if($kBooking->updateCargoDetails($kConfig,$idBooking))
        {
            echo "SUCCESS||||".$redirect_url=__BOOKING_DETAILS_PAGE_URL__;
            die;
        }
    }
}
else if($operation_type =='UPDATE_COURIER_BOOKING')
{ 
    $kWHSSearch=new cWHSSearch();
    $kForwarder = new cForwarder();
    $kBooking = new cBooking();

    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

    if((int)$idBooking>0)
    {
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $idWTW = sanitize_all_html_input(trim($_REQUEST['idWTW']));	

        if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
        {
            echo "BOOKING_NOTIFICATION||||" ;
            $t_base_booking_details = "BookingDetails/";
            echo display_booking_already_paid($t_base_booking_details,$idBooking);
            die;
        } 
        if(!empty($idWTW))
        {
            $ret_flag='';
            $ret_flag = $kBooking->updateCourierServiceForwarderDetails($idBooking,$idWTW);
            echo $ret_flag."||||";
            die;
        }
    }
    else
    {
        echo "BOOKING_NOTIFICATION||||" ;
        $redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
        echo display_booking_not_found_popup($redirect_url);
        die;
    }
}
else
{
    $kWHSSearch=new cWHSSearch();
    $kForwarder = new cForwarder();
    $kBooking = new cBooking();

    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['szBookingRandomNum'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

    if((int)$idBooking>0)
    {
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $idWTW = sanitize_all_html_input(trim($_REQUEST['idWTW']));	

        if(($postSearchAry['idBookingStatus']=='3') || ($postSearchAry['idBookingStatus']=='4'))
        {
            echo "BOOKING_NOTIFICATION||||" ;
            $t_base_booking_details = "BookingDetails/";
            echo display_booking_already_paid($t_base_booking_details,$idBooking);
            die;
        } 
        if(!empty($idWTW))
        {
            $ret_flag='';
            $ret_flag = $kBooking->updateForwarderDetails($idBooking,$idWTW);
            echo $ret_flag."||||";
            die;
        }
    }
    else
    {
        echo "BOOKING_NOTIFICATION||||" ;
        $redirect_url = __BOOKING_DETAILS_PAGE_URL__ ;				
        echo display_booking_not_found_popup($redirect_url);
        die;
    }
}	

?>