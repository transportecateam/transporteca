<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = true;
$szLanguage = $_SESSION['transporteca_language_en']; 

if($szLanguage=='danish')
{
    $szMetaTitle = "Transporteca | Ups, siden kan ikke vises";
}
else
{
    $szMetaTitle = "Transporteca | Ooops, we can't show the page";
}

$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
$page_404 = 1;   
require_once(__APP_PATH_LAYOUT__."/header_new.php");
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
echo "Page not found... ";
pageNotFound(); 
require_once(__APP_PATH_LAYOUT__."/footer_new.php");

?>	