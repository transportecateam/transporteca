<?php
header('Content-Type:text/html; charset=UTF-8');
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once('SimpleImage.php');

if ( (isset($_GET['img'])) and (isset($_GET['w'])) and (isset($_GET['h'])) ) {
if($_GET['temp']==1) // from images/forwarder/temp
{
	$tmp_path = __APP_PATH_FORWARDER_IMAGES_TEMP__;
}
else if($_GET['temp']==2) //from images folder
{
	$tmp_path = __APP_PATH_IMAGES__;
}
else if($_GET['temp']=='explainLevel2') //from images folder
{
	$tmp_path = __EXPLAIN_LEVEL2_IMAGES__;
}
else if($_GET['temp']=='explainLevel3') //from images folder
{
	$tmp_path = __EXPLAIN_LEVEL3_IMAGES__;
}
else
{
	$tmp_path = __APP_PATH_FORWARDER_IMAGES__;
}
	$tmp_file = $tmp_path.'/'.$_GET['img'];
			
	$fn = $tmp_file;
	$fn2 = $tmp_path.'/thumb/'.$_GET['w'].'x'.$_GET['h'].'_'.$_GET['img'];
	
	$pathinfo = pathinfo($fn2);
	$mime = '';
	if (strtoupper($pathinfo['extension']) == 'JPG') { $mime = 'image/jpeg'; }
	if (strtoupper($pathinfo['extension']) == 'GIF') { $mime = 'image/gif';  }
	if (strtoupper($pathinfo['extension']) == 'PNG') { $mime = 'image/png';  }
	
	if (!file_exists($fn2)) {

		$image = new SimpleImage();
		
		if (strtoupper($pathinfo['extension']) == 'JPG') { $image->image_type = IMAGETYPE_JPEG; }
		if (strtoupper($pathinfo['extension']) == 'GIF') { $image->image_type = IMAGETYPE_GIF; }
		if (strtoupper($pathinfo['extension']) == 'PNG') { $image->image_type = IMAGETYPE_PNG; }
		
		$image->load($fn);
		$w = ($_GET['w'] / $image->getWidth()) * 100;
		$h = ($_GET['h'] / $image->getHeight()) * 100;
		if ($w < $h) {
			$image->scale($w);
		} else {
			$image->scale($h);
		}
	//	$image->crop(120,60);
		
		if (strtoupper($pathinfo['extension']) == 'JPG') { $image->save($fn2, IMAGETYPE_JPEG); }
		if (strtoupper($pathinfo['extension']) == 'GIF') { $image->save($fn2, IMAGETYPE_GIF); }
		if (strtoupper($pathinfo['extension']) == 'PNG') { $image->save($fn2, IMAGETYPE_PNG); }
	}
	
	$data = file_get_contents($fn2);
	
	$expires = 60*60*24*14; // 14 days
	
	header("Pragma: public");
	header("Cache-Control: max-age=".$expires.", public");
	header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');

	$eTag = md5($data);
	$eTag = '"'.$eTag.'"';
	header("Etag: ".$eTag);
	
	if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
		if ($eTag == $_SERVER['HTTP_IF_NONE_MATCH']) {   
			header("HTTP/1.0 304 Not Modified");
			header('Content-Length: 0');
			exit;
		}
	}
	
	header('Content-Type: '.$mime);
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: '.strlen($data));
	echo $data;
}

?>