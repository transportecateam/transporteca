<?php
/**
 * Affiliate Login
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );



$t_base_error = "Error";
$t_base = "Affiliate/";


?>
<script type="text/javascript">
$("#szEmail").focus();
</script>
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup signin-popup signin-popup-verification">
<p class="close-icon" align="right">
<a onclick="cancel_signin('<?=$cancel_url?>','ajaxLogin');" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
<div id="activationkey" style="display:none";></div>
<form name="loginForm" id="loginForm" method="post">
	<h5><?=t($t_base.'title/signin')?></h5>
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/signin_email')?></p>
		<p class="fl-65"><input type="email" id="szEmail" name="loginArr[szEmail]" value="<?=$_POST['loginArr']['szEmail']?>" tabindex="1"/></p>
	</div>
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/password')?></p>
		<p class="fl-65"><input type="password" id="szPassword" name="loginArr[szPassword]" tabindex="2" onkeyup="check_login_password_submit_key_press(event);"/></p>
	</div>
	<br style="line-height: 16px;" />
	<p align="center"><a href="javascript:void(0)" class="button1" onclick="cancel_signin('<?=$cancel_url?>','ajaxLogin');" tabindex="6"><span><?=t($t_base.'fields/cancel')?></span></a> <a href="javascript:void(0)" class="button1" tabindex="7"><span><?=t($t_base.'fields/sign_in')?></span></a></p>
</form>
</div>
		