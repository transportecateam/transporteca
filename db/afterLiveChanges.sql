//04-04-2017 @Ajay

CREATE TABLE IF NOT EXISTS `tblwarehousetype` (
`id` int(11) NOT NULL,
  `szTypeCode` varchar(255) NOT NULL,
  `szFriendlyName` varchar(255) NOT NULL,
  `iActive` int(11) NOT NULL DEFAULT '1',
  `dtCreatedOn` datetime NOT NULL
);

INSERT INTO `tblwarehousetype` (`id`, `szTypeCode`, `szFriendlyName`, `iActive`, `dtCreatedOn`) VALUES
(1, 'CFS', 'CFS', 1, '2017-04-04 00:00:00'),
(2, 'AIR', 'Airport warehouse', 1, '2017-04-04 00:00:00');

ALTER TABLE `tblwarehousetype` ADD PRIMARY KEY (`id`);
ALTER TABLE `tblwarehousetype` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

ALTER TABLE `tblwarehouses` ADD `iWarehouseType` INT NOT NULL DEFAULT '1' ; 
ALTER TABLE `tblwarehousestempdata` ADD `iWarehouseType` INT NOT NULL DEFAULT '1' ;

ALTER TABLE `tblbookings` ADD `iWarehouseType` INT NOT NULL;
ALTER TABLE `tblclonebookings` ADD `iWarehouseType` INT NOT NULL;
 
ALTER TABLE `tblwarehousetype` CHANGE `szFriendlyName` `szWarehouseType` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
INSERT INTO `tblmanagementvar` ( 
`szDescription` ,
`szValue` ,
`szDeveloperNotes` ,
`iActive` ,
`szFriendlyName` ,
`iLanguage`
)
VALUES (
'__TOTAL_NUM_AIR_FREIGHT_SEARCH_RESULT__', '10', 'Number of best AIR options to show on screen 1', '1', 'Number of best AIR options to show on screen 1', '1'
);

ALTER TABLE `tblbookings` ADD INDEX `forwarder_booking` (`idForwarder`, `iTransferConfirmed`, `idBookingStatus`, `dtBookingConfirmed`);

//19-04-2017 @Ajay
CREATE TABLE IF NOT EXISTS `tblbookingfilestaskqueues` (
`id` int(11) NOT NULL,
  `idBookingFile` int(11) NOT NULL,
  `szTaskStatus` varchar(255) NOT NULL,
  `iPriority` int(11) NOT NULL,
  `dtCreatedOn` int(11) NOT NULL,
  `szDeveloperNotes` varchar(255) NOT NULL,
  `isDeleted` int(11) NOT NULL
);

ALTER TABLE `tblbookingfilestaskqueues` ADD PRIMARY KEY (`id`);
ALTER TABLE `tblbookingfilestaskqueues` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


//28-04-2017 @AJay
ALTER TABLE `tblfedexapilogs` ADD `szResponseSerializedData` LONGTEXT NOT NULL AFTER `szResponseData`; 

ALTER TABLE `tblfedexapilogs` ADD `idBooking` INT NOT NULL AFTER `idUser`, ADD `szReferenceToken` VARCHAR(255) NOT NULL AFTER `idBooking`;

//04-05-2017 @Ajay
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szEventType', 'E1191', 'REQUIRED', '5');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szEventType', 'E1192', 'INVALID', '4');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szPriceToken', 'E1193', 'REQUIRED', '5'), ('szPriceToken', 'E1194', 'INVALID', '4');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szBillingEmail', 'E1195', 'INVALID', '4');

//05-05-2017 @Ajay
ALTER TABLE `tblpartnerapicall` ADD `iDonotKnowShipperPostcode` INT NOT NULL , ADD `iDonotKnowConsigneePostcode` INT NOT NULL ;

ALTER TABLE `tblpartnerapicall` ADD `iBookingLanguage` INT NOT NULL ;
ALTER TABLE `tblpartnerapicall` ADD `iShipmentType` INT NOT NULL ; 

//09-05-2017 @Ajay 
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szInsurancePrice', 'E1201', 'REQUIRED', '5'), ('szInsurancePrice', 'E1202', 'NUMERIC', '1');
ALTER TABLE `tblbookings` ADD `iUpdateInsurancePriceByAPI` INT NOT NULL;
ALTER TABLE `tblclonebookings` ADD `iUpdateInsurancePriceByAPI` INT NOT NULL;

//10-05-2017 @Ajay
ALTER TABLE `tbllanguage` ADD `szDomain` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `idCountry`; 
ALTER TABLE `tblquotepricingdetails` ADD `szQuotePricingKey` VARCHAR(255) NOT NULL AFTER `id`;

//11-05-2017 @Ajay 
UPDATE `tblquotepricingdetails` SET `szQuotePricingKey` = `id`; 
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szQuoteExpired', 'E1203', 'INVALID', '4');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szShipperPostcode', 'E1204', 'INVALID', '4'), ('szConsigneePostcode', 'E1205', 'INVALID', '5');
INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES ('szBookingKey', 'E1206', 'INVALID', '');

INSERT INTO `tbllanguageconfiguration` (`idMapped`, `szFieldName`, `szValue`, `szTableKey`, `idLanguage`) VALUES
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 1),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 2),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 3),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 4),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 5),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 6),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 7),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 8),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 9),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 10),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 11),
(1, 'szLabelPageHeading', 'Please reconfirm your contact details and expected shipping date below  to download szProvider shipping labels for your shipment for szDetial reference szBookingReference.', '__SITE_STANDARD_TEXT__', 12);

INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES
('dtPickUpDay', 'E1207', 'NUMERIC', 1),
('dtPickUpDay', 'E1208', 'INVALID', 4), 
('dtPickUpDay', 'E1209', 'REQUIRED', 5);

INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES
('dtPickUpMonth', 'E1210', 'NUMERIC', 1),
('dtPickUpMonth', 'E1211', 'INVALID', 4), 
('dtPickUpMonth', 'E1212', 'REQUIRED', 5);

INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES
('dtPickUpYear', 'E1213', 'NUMERIC', 1),
('dtPickUpYear', 'E1214', 'INVALID', 4), 
('dtPickUpYear', 'E1215', 'REQUIRED', 5);

INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`) VALUES
('iScore', 'E1216', 'INVALID', 4);


//15-05-2017 @AJay
ALTER TABLE `tblforwardertransactions` ADD `dtPaymentSettled` DATETIME NOT NULL AFTER `dtPaymentScheduled` ;

ALTER TABLE `tblbookings` ADD `iCreateAutomaticQuote` INT NOT NULL;
ALTER TABLE `tblclonebookings` ADD `iCreateAutomaticQuote` INT NOT NULL;

ALTER TABLE `tblbookings` ADD `iCreateVogaAutomaticQuote` INT NOT NULL;
ALTER TABLE `tblclonebookings` ADD `iCreateVogaAutomaticQuote` INT NOT NULL;

ALTER TABLE `tblbookings` ADD `idDefaultLandingPage` INT NOT NULL;
ALTER TABLE `tblclonebookings` ADD `idDefaultLandingPage` INT NOT NULL;


ALTER TABLE `tblerrordetails` ADD `iInternalErrorCode` INT NOT NULL ;

INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`, `iInternalErrorCode`) VALUES ('szShipperCity', 'E1219', 'INVALID', '2', ''), ('szConsigneeCity', 'E1220', 'INVALID', '2', '');


INSERT INTO `tblerrordetails` (`szFieldKey`, `szErrorCode`, `szErrorType`, `iErrorTypeFlag`, `iInternalErrorCode`) VALUES
('iLanguage', 'E1221', 'REQUIRED', 5, 0);

//26-05-2017 @Ajay
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 1, '2017-03-07 15:07:50');

INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 3, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 4, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 5, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 6, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 7, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 8, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 9, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 10, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 11, '2017-03-07 15:07:50');
INSERT INTO `cms_sections_mapping` (`idEmailTemplate`, `subject`, `section_description`, `idLanguage`, `updated`) VALUES
('116', 'Din forsendelse med reference szBookingRef er planlagt til afhentning szPickupDate', '<div align="left">KÃ¦re szFirstName,<br><br>Det glÃ¦der os at kunne fortÃ¦lle dig, at din forsendelse szBookingRef fra szShipperCompany er sat op til afhentning szPickupDate. NÃ¥r den er afhentet, sender vi dig indefor 24 timer en e-mail med bekrÃ¦ftelse, samt et trackinglink, hvor du kan fÃ¸lge din forsendelse og se leveringsdato og tidspunkt.<br><br>Hvis du har spÃ¸rgsmÃ¥l, er du velkommen til at skrive til os pÃ¥ support@transporteca.com, eller ringe pÃ¥ 7199 7299.<br><br>Med venlig hilsen,<br><br>Transporteca<br></div><br>', 12, '2017-03-07 15:07:50');

//29-05-2017 @Ajay
ALTER TABLE `tblquotepricingdetails` ADD `iRemovePriceGuarantee` INT NOT NULL;  
ALTER TABLE `tbltempsearchresult` CHANGE `szCourierCalculationLogString` `szCourierCalculationLogString` LONGBLOB NOT NULL ;
ALTER TABLE `tbltempsearchresult` CHANGE `szCourierCalculationLogString` `szCourierCalculationLogString` LONGTEXT NOT NULL ;

//01-06-2017 @Ajay
ALTER TABLE `tblforwardertransactions` ADD `iTransferEmailTobeSent` INT NOT NULL COMMENT '1. Means we haven''t sent payment transfer conirmation email from cronjob';

//12-06-2017 @Ajay  
CREATE TABLE IF NOT EXISTS `tblforwarderpricing` (
`id` int(11) NOT NULL,
  `idForwarder` int(11) NOT NULL,
  `idTransportMode` int(11) NOT NULL,
  `fReferralFee` float(8,2) NOT NULL,
  `dtCreatedOn` datetime NOT NULL,
  `dtUpdateOn` datetime NOT NULL,
  `iActive` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `idForwarderUpdatedBy` int(11) NOT NULL,
  `iUpdatedByAdmin` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
ALTER TABLE `tblforwarderpricing` ADD PRIMARY KEY (`id`);
ALTER TABLE `tblforwarderpricing` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


//14-06-2017 @Ajay
ALTER TABLE `tblforwarders` DROP `fReferalFee`;

//19-06-2017 @AJay
ALTER TABLE `tblpartnerapieventlog` CHANGE `idPartner` `idPartner` INT(11) NOT NULL DEFAULT '0';