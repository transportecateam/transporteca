<?php
/**
 * Customer Account Confirmation 
 */
 ob_start();
session_start();
$szMetaTitle= __EMAIL_CONFIRMATION_META_TITLE__;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/header_new.php" );

//error_reporting(E_ALL);
//ini_set('display_error','1');
//error_reporting(E_ALL);

$t_base = "Users/AccountPage/";

$kBooking = new cBooking();
$kWHSSearch = new cWHSSearch();
$kVatApplication = new cVatApplication();

$szConfirmationKey = trim($_REQUEST['szRandomKey']); 
$idQuotePricing = $_REQUEST['quote_pricing_id'] ;
 
$data = array();
$data['idBookingQuotePricing'] = $idQuotePricing ;
$quotePricingDetailsAry = array();
$quotePricingDetailsAry = $kBooking->getAllBookingQuotesPricingDetails($data);
              
if(!empty($quotePricingDetailsAry))
{
    foreach($quotePricingDetailsAry as $quotePricingDetailsArys)
    {  
        $fTotalPriceForwarderCurrencyExcludingHandlingFee = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'];
        $fTotalSelfInvoiceAmountQuoteCurrency = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'];
        $idBooking = $quotePricingDetailsArys['idBooking'];
        $idBookingFile = $quotePricingDetailsArys['idFile'];
        $idBookingQuote = $quotePricingDetailsArys['idQuote'];
        $idQuotePricingDetails = $quotePricingDetailsArys['id'];
        $iHandlingFeeApplicable = $quotePricingDetailsArys['iHandlingFeeApplicable'];
        $iManualFeeApplicable = $quotePricingDetailsArys['iManualFeeApplicable'];
        $szServiceID = $quotePricingDetailsArys['szServiceID'];
        $iAddHandlingFee = $quotePricingDetailsArys['iAddHandlingFee']; 
         
        $postSearchAry = array();
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        
        $szReferenceToken = $postSearchAry['szReferenceToken'];
        $iQuickQuote = $postSearchAry['iQuickQuote'];
        $idCustomer = $postSearchAry['idUser'];
              
        if($postSearchAry['idBookingStatus']=='3' || $postSearchAry['idBookingStatus']=='4' || $postSearchAry['idBookingStatus']=='7')
        {
            //if booking is already paid then we do not update anything into booking just redirect user to next screen
            $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
            $kConfig = new cConfig();
            $kConfig->loadCountry($postSearchAry['idOriginCountry']);
            $szCountryStrUrl = $kConfig->szCountryISO ;
            $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
            $szCountryStrUrl .= $kConfig->szCountryISO ;

            ob_end_clean();
            $redirect_url = __BOOKING_QUOTE_SERVICE_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
            header("location:".$redirect_url);
            die;
        } 
        else
        { 
            $bookingQuoteAry = array();
            $bookingQuoteAry = $kBooking->getAllBookingQuotesByFile(false,false,$idBookingQuote); 
            $bookingQuoteArys = $bookingQuoteAry['1']; 
            $ret_ary = array();
            /*
            *  For quick quote bookings we need to update data for both Automatic Booking and RFQ because Quick quote bookings are hybrid mixture of Automatic and Manual bookings
            */
            if($iQuickQuote==__QUICK_QUOTE_SEND_QUOTE__)
            {
                cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__; 
                cPartner::$szGlobalToken = $szReferenceToken;  
                cPartner::$idPartnerCustomer = $idCustomer;
                $idTransportMode = $bookingQuoteArys['idTransportMode'];
                
                if($idTransportMode==__BOOKING_TRANSPORT_MODE_COURIER__)
                {
                    $kBooking->updateCourierServiceForwarderDetails($idBooking,$szServiceID,false,true,$quotePricingDetailsArys); 
                }
                else
                {
                    $kBooking->updateForwarderDetails($idBooking,$szServiceID,false,true,$quotePricingDetailsArys);
                }
                
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                
                if(($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceChoice']==3) && $postSearchAry['fValueOfGoods']>0)
                { 
                    /*
                    * Recalculation Insurance for Quick quotes
                    */
                    
                    $insuranceInputAry = array();
                    $insurancePriceAry = array();
                    $insuranceInputAry['iInsurance'] = 1;
                    $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
                    $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fValueOfGoods'];

                    /*
                    * Recalculating Insurance Prices
                    */
                    $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry);  
                }
                $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
                $_SESSION['load-booking-details-page-for-booking-id'] = $szBookingRandomNum ;
                 
                /*  
                 *  $ret_ary['iBookingType'] = __BOOKING_TYPE_RFQ__ ; //Automatic Booking
                    $ret_ary['iQuotesStatus '] = __BOOKING_QUOTES_STATUS_SENT__;
                    $ret_ary['iBookingQuotes '] = 1;
                 *  
                 */
                
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                $szCountryStrUrl = $kConfig->szCountryISO ;
                $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                $szCountryStrUrl .= $kConfig->szCountryISO ;
                
                ob_end_clean(); 
                $redirect_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                header("location:".$redirect_url);
                die; 
                 
            } 
            else
            {
                if($iManualFeeApplicable==1)
                {
                    $ret_ary['iQuickQuote'] = 1;
                }
                else
                {
                    $ret_ary['iQuickQuote'] = 0;
                } 
                $bRecalculateInsuranceFlag = false;
                
                /*
                * From Management > Pending Tray > Validate Pane
                 * 1. If admin has added Insurance: Yes and Cargo value: null that means $postSearchAry['iIncludeInsuranceWithZeroCargoValue']=1 and $postSearchAry['iInsuranceIncluded']=1. And for this case we don't recalculate Insurance Pricing again.
                 * 2. If admin has added Insurance: Optional and Cargo value: null that means $postSearchAry['iOptionalInsuranceWithZeroCargoValue']=1 and $postSearchAry['iInsuranceChoice']=1. And for this case we recalculate Insurance Pricing again.
                */
                if($postSearchAry['fValueOfGoods']<=0)
                {
                    $bRecalculateInsuranceFlag = false;
                }
                else if($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceChoice']==3)
                {
                    $bRecalculateInsuranceFlag = true;
                }
                else if(($postSearchAry['iIncludeInsuranceWithZeroCargoValue']==1 && $postSearchAry['iInsuranceIncluded']==1) || ($postSearchAry['iOptionalInsuranceWithZeroCargoValue']==1 && $postSearchAry['iInsuranceChoice']==3))
                {
                    $bRecalculateInsuranceFlag = true;
                }
                else
                {
                    /*
                    * For quick quote type: 'Send Quote' we don't update insurance values because we already have these with the booking.
                    */
                    $ret_ary['fTotalInsuranceCostForBooking'] = $quotePricingDetailsArys['fTotalInsuranceCostForBooking']; ;
                    $ret_ary['fTotalInsuranceCostForBookingCustomerCurrency'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingCustomerCurrency']; ;
                    $ret_ary['fTotalInsuranceCostForBookingUSD'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingUSD']; ;
                    $ret_ary['fTotalAmountInsured'] = $quotePricingDetailsArys['fTotalAmountInsured'];
                    $res_ary['iMinrateApplied'] = $quotePricingDetailsArys['iMinrateApplied'];
                    $ret_ary['idInsuranceRate'] = $quotePricingDetailsArys['idInsuranceSellCurrency'];
                    $ret_ary['fInsuranceUptoPrice'] = $quotePricingDetailsArys['fInsuranceUptoPrice'];
                    $ret_ary['idInsuranceUptoCurrency'] = $quotePricingDetailsArys['idInsuranceUptoCurrency'];
                    $ret_ary['szInsuranceUptoCurrency'] = $quotePricingDetailsArys['szInsuranceUptoCurrency'];
                    $ret_ary['fInsuranceUptoExchangeRate'] = $quotePricingDetailsArys['fInsuranceUptoExchangeRate'];
                    $ret_ary['fInsuranceRate'] = $quotePricingDetailsArys['fInsuranceRate'];
                    $ret_ary['fInsuranceMinPrice'] = $quotePricingDetailsArys['fInsuranceMinPrice'];
                    $ret_ary['idInsuranceMinCurrency'] = $quotePricingDetailsArys['idInsuranceMinCurrency']; 
                    $ret_ary['szInsuranceMinCurrency'] = $quotePricingDetailsArys['szInsuranceMinCurrency'];
                    $ret_ary['fInsuranceMinExchangeRate'] = $quotePricingDetailsArys['fInsuranceMinExchangeRate'];
                    $ret_ary['fInsuranceUptoPriceUSD'] = $quotePricingDetailsArys['fInsuranceUptoPriceUSD'];
                    $ret_ary['fInsuranceMinPriceUSD'] = $quotePricingDetailsArys['fInsuranceMinPriceUSD'];

                    $ret_ary['idInsuranceRate_buyRate'] = $quotePricingDetailsArys['idInsuranceRate_buyRate'];
                    $ret_ary['fInsuranceUptoPrice_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoPrice_buyRate'];
                    $ret_ary['idInsuranceUptoCurrency_buyRate'] = $quotePricingDetailsArys['idInsuranceUptoCurrency_buyRate'];
                    $ret_ary['szInsuranceUptoCurrency_buyRate'] = $quotePricingDetailsArys['szInsuranceUptoCurrency_buyRate'];
                    $ret_ary['fInsuranceUptoExchangeRate_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoExchangeRate_buyRate'];
                    $ret_ary['fInsuranceRate_buyRate'] = $quotePricingDetailsArys['fInsuranceRate_buyRate'];
                    $ret_ary['fInsuranceMinPrice_buyRate'] = $quotePricingDetailsArys['fInsuranceMinPrice_buyRate'];
                    $ret_ary['idInsuranceMinCurrency_buyRate'] = $quotePricingDetailsArys['idInsuranceMinCurrency_buyRate'];
                    $ret_ary['szInsuranceMinCurrency_buyRate'] = $quotePricingDetailsArys['szInsuranceMinCurrency_buyRate'];
                    $ret_ary['fInsuranceMinExchangeRate_buyRate'] = $quotePricingDetailsArys['fInsuranceMinExchangeRate_buyRate'];
                    $ret_ary['fInsuranceUptoPriceUSD_buyRate'] = $quotePricingDetailsArys['fInsuranceUptoPriceUSD_buyRate'];
                    $ret_ary['fInsuranceMinPriceUSD_buyRate'] = $quotePricingDetailsArys['fInsuranceMinPriceUSD_buyRate'];

                    $ret_ary['fTotalInsuranceCostForBooking_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBooking_buyRate'];
                    $ret_ary['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                    $ret_ary['fTotalInsuranceCostForBookingUSD_buyRate'] = $quotePricingDetailsArys['fTotalInsuranceCostForBookingUSD_buyRate'];
                } 
                if($iManualFeeApplicable==1)
                {
                    cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__; 
                    cPartner::$szGlobalToken = $szReferenceToken; 
                    $kWHSSearch = new cWHSSearch();
                    $tempResultAry = array();
                    $searchResultAry = array();
                    $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true);
                    //$szSerializeData = stripslashes($tempResultAry['szSerializeData']);

                    $szSerializeData =  $tempResultAry['szSerializeData']; 
                    $searchResultAry = unserialize($szSerializeData); 

                    if(!empty($searchResultAry))
                    {
                        foreach($searchResultAry as $searchResultArys)
                        {
                            if($searchResultArys['unique_id'] == $szServiceID )
                            {
                                $updateBookingAry = $searchResultArys ; 
                                break;
                            }
                        }
                    }  
                    $ret_ary['idUniqueWTW'] = $szServiceID ;
                    if(!empty($updateBookingAry))
                    {
                        $ret_ary['iBookingCutOffHours'] = $updateBookingAry['iBookingCutOffHours'];
                        $ret_ary['dtCutOff']= $updateBookingAry['dtCutOffDate'];
                        $ret_ary['dtAvailable'] = $updateBookingAry['dtAvailableDate'];
                        $ret_ary['dtBookedAvailableDate'] = $updateBookingAry['dtAvailableDate'];
                        $ret_ary['dtWhsCutOff'] = $updateBookingAry['dtWhsCutOff'];
                        $ret_ary['dtWhsAvailabe'] = $updateBookingAry['dtWhsAvailable']; 
                    } 
                }
            }   
            
            $kForwarder = new cForwarder();
            $kForwarder->load($bookingQuoteArys['idForwarder']);

            $ret_ary['szForwarderDispName'] = $kForwarder->szDisplayName;
            $ret_ary['szForwarderRegistName'] = $kForwarder->szCompanyName;
            $ret_ary['szForwarderAddress']=$kForwarder->szAddress;
            $ret_ary['szForwarderAddress2']=$kForwarder->szAddress2;
            $ret_ary['szForwarderAddress3']=$kForwarder->szAddress3;
            $ret_ary['szForwarderPostCode']=$kForwarder->szPostCode;
            $ret_ary['szForwarderCity']=$kForwarder->szCity;
            $ret_ary['szForwarderState']=$kForwarder->szState;
            $ret_ary['idForwarderCountry']=$kForwarder->idCountry;
            $ret_ary['szForwarderLogo']=$kForwarder->szLogo; 
            $ret_ary['idForwarderCurrency'] = $kForwarder->szCurrency ;
            $ret_ary['iForwarderGPType'] = $kForwarder->iProfitType ; 

            $fApplicableVatRate = 0; 
            if(!$kVatApplication->checkForwarderCountryExists($ret_ary['idForwarderCountry'],true))
            {
                $vatInputAry = array();
                $vatInputAry['idForwarderCountry'] = $ret_ary['idForwarderCountry'];
                $vatInputAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
                $vatInputAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];

                /*
                * We are no longer applying VAT on forwarder amount.
                */
               // $fApplicableVatRate = $kVatApplication->getVatRate($vatInputAry);   
            } 
            
            if((int)$postSearchAry['iBookingStep']<9)
            {
                $ret_ary['iBookingStep'] = 9;
            }  
            if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
            {
                $ret_ary['szForwarderCurrency'] = $quotePricingDetailsArys['szForwarderAccountCurrency'];
                $ret_ary['fForwarderExchangeRate'] = $quotePricingDetailsArys['fForwarderAccountCurrencyExchangeRate'];
            }
            else
            {
                if($kForwarder->szCurrency==1)
                { 
                    $ret_ary['szForwarderCurrency'] = 'USD';
                    $ret_ary['fForwarderExchangeRate'] = 1;
                }
                else
                {
                    $resultAry = $kWHSSearch->getCurrencyDetails($kForwarder->szCurrency);		 
                    $ret_ary['szForwarderCurrency'] = $resultAry['szCurrency'];
                    $ret_ary['fForwarderExchangeRate'] = $resultAry['fUsdValue'];
                }
            } 
            if($iManualFeeApplicable==1)
            { 
                if($quotePricingDetailsArys['idHandlingCurrency']==$quotePricingDetailsArys['idForwarderCurrency'])
                {
                    $fManualFeeForwarderCurrency = $quotePricingDetailsArys['fTotalHandlingFee'];
                }
                else if($quotePricingDetailsArys['idForwarderCurrency']==1)
                {
                    $fManualFeeForwarderCurrency = $quotePricingDetailsArys['fTotalHandlingFeeUSD'];
                }
                else if($quotePricingDetailsArys['fForwarderCurrencyExchangeRate']>0)
                {
                    $fManualFeeForwarderCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $quotePricingDetailsArys['fForwarderCurrencyExchangeRate']),2);
                }      
                $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] + $fManualFeeForwarderCurrency ; 

                if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
                {
                    $fManualFeeForwarderAccountCurrency = $fManualFeeForwarderCurrency;
                }
                else if($kForwarder->szCurrency==$quotePricingDetailsArys['idHandlingCurrency'])
                {
                    $fManualFeeForwarderAccountCurrency = $quotePricingDetailsArys['fTotalHandlingFee'];
                }
                else if($ret_ary['fForwarderExchangeRate']>0)
                {
                    $fManualFeeForwarderAccountCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderCurrencyExchangeRate']),2);
                } 
                $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] + $fManualFeeForwarderAccountCurrency;
                $quotePricingDetailsArys['fTotalVatForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] * $fApplicableVatRate * 0.01; 
            } 
            else if($iAddHandlingFee==1)
            { 
                $fHandlingFeeForwarderCurrency = 0;
                if($searchResults['idForwarderCurrency']==1) //USD
                {
                    $fHandlingFeeForwarderCurrency = $quotePricingDetailsArys['fTotalHandlingFeeUSD'];
                }
                else if($quotePricingDetailsArys['fForwarderCurrencyExchangeRate']>0)
                {
                    $fHandlingFeeForwarderCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD']/$quotePricingDetailsArys['fForwarderCurrencyExchangeRate']),4);
                }    
                $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] + $fHandlingFeeForwarderCurrency ; 

                if($searchResults['idForwarderCurrency']==1) //USD
                {
                    $fHandlingFeeForwarderAccountCurrency = $quotePricingDetailsArys['fTotalHandlingFeeUSD'];
                }
                else if($ret_ary['fForwarderExchangeRate']>0)
                {
                    $fHandlingFeeForwarderAccountCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD']/$ret_ary['fForwarderExchangeRate']),4);
                }  
                $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] + $fHandlingFeeForwarderAccountCurrency;
                $quotePricingDetailsArys['fTotalVatForwarderAccountCurrency'] = $quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency'] * $fApplicableVatRate * 0.01 ;  
            } 
            /*
            *  If forwarder Quote currency and Customer currency are same then we have rounded off both the values to make sure we always have matching values under Transaction History
            */
            if($quotePricingDetailsArys['idForwarderCurrency']==$postSearchAry['idCustomerCurrency'])
            {
                $quotePricingDetailsArys['fTotalPriceForwarderCurrency'] = round((float)$quotePricingDetailsArys['fTotalPriceForwarderCurrency']); 
            } 
            else
            {
                /*
                 * We always have customer currency value rounded to 0 decimal places
                 */
                $quotePricingDetailsArys['fTotalPriceCustomerCurrency'] = round((float)$quotePricingDetailsArys['fTotalPriceCustomerCurrency']); 
            }

            $ret_ary['idQuotePricingDetails'] = $idQuotePricingDetails ;
            $ret_ary['idFile'] = $idBookingFile ;
            $ret_ary['idTransportMode'] = $bookingQuoteArys['idTransportMode'];
            $ret_ary['szTransportMode'] = $bookingQuoteArys['szTransportMode'];
            $ret_ary['idForwarder'] = $bookingQuoteArys['idForwarder'];  

            if($ret_ary['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__) //Courier
            { 
                if($quotePricingDetailsArys['idCourierProviderProduct']>0)
                {
                    $kCourierService = new cCourierServices();
                    $courierAgreementInputAry = array();
                    $courierAgreementInputAry['idForwarder'] = $bookingQuoteArys['idForwarder'];
                    $courierAgreementAry = $kCourierService->getCourierProductProviderPricingData($courierAgreementInputAry,false,$quotePricingDetailsArys['idCourierProviderProduct']);
                    $idCourierAgreement = $courierAgreementAry[0]['idCourierAgreement'];
                }
                 
                $ret_ary['isManualCourierBooking'] = 1;
                $ret_ary['dtCutOff'] = $postSearchAry['dtTimingDate']; 
                $ret_ary['idServiceProvider'] = $idCourierAgreement;
                $ret_ary['idServiceProviderProduct'] = $quotePricingDetailsArys['idCourierProviderProduct'];
            }
            else
            {
                $ret_ary['isManualCourierBooking'] = 0;
            } 

            $fTotalPriceIncludingVat = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'] + $quotePricingDetailsArys['fTotalVatCustomerCurrency'];
            $fTotalPriceIncludingVatUsd = ($fTotalPriceIncludingVat * $postSearchAry['fExchangeRate']) ;

            /*
            * If forwarder account currency and customer currency then there is no currency markup will applied
            * @Ajay
            */
            if($kForwarder->szCurrency==$postSearchAry['idCurrency'])
            {
                $fCurrencyMarkupPrice = 0;
                $fMarkupPercentage = 0;

                $fTotalPriceNoMarkup = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'];
            }
            else
            {
                $fPriceMarkUp = $kWHSSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__'); 

                /*
                 * We are calculating no currency markup price from price with mark-up for e.g
                 * fTotalPriceCustomerCurrency = 400
                 * fMarkupRate = 2.5
                 * fPriceNoMarkup = (400 * 100 )/(100+2.5) = (40000/102.5) = 390.2439
                 * @Ajay
                 */
                $fTotalPriceNoMarkup = ($quotePricingDetailsArys['fTotalPriceCustomerCurrency'] * 100 ) / (100+$fPriceMarkUp);
                $fTotalVatNoMarkup = ($quotePricingDetailsArys['fTotalVatCustomerCurrency'] * 100 ) / (100+$fPriceMarkUp);

                $fMarkupPercentage = $fPriceMarkUp ;
                $fCurrencyMarkupPrice = ($quotePricingDetailsArys['fTotalPriceCustomerCurrency'] - $fTotalPriceNoMarkup) + ($quotePricingDetailsArys['fTotalVatCustomerCurrency'] - $fTotalVatNoMarkup) ;
                $fCurrencyMarkupPriceUSD = ($fCurrencyMarkupPrice * $postSearchAry['fExchangeRate']) ;
            } 

            /*
            * IF the forwarder account currency remains the same while quoting and booking then we are picking values from quote otherwise we will calculate forwarder prices in forwarder's account currency
            * e.g if fwd account currency was USD when quote was created and remains same USD when this page got called then we calculate nothing and pick the values what we have in quote tables
            * AND IF it changes to some other currency e.g DKK then we calculate these prices in DKK
            * @Ajay
            */
            if($kForwarder->szCurrency==$quotePricingDetailsArys['idForwarderAccountCurrency'])
            { 
                $fTotalPriceForwarderAccountCurrency=$quotePricingDetailsArys['fTotalPriceForwarderAccountCurrency']+$quotePricingDetailsArys['fTotalVatForwarderAccountCurrency'];
                $ret_ary['fTotalPriceForwarderCurrency'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
                $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = round((float)$fTotalPriceForwarderAccountCurrency,2);
            }
            else
            {
                if($kForwarder->szCurrency==$postSearchAry['idCurrency'])
                {
                    $ret_ary['fTotalPriceForwarderCurrency'] = round((float)$fTotalPriceIncludingVat,2);
                    $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = round((float)$fTotalPriceIncludingVat,2);
                    $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = round((float)$fTotalPriceIncludingVat,2);
                }
                else
                {  
                    if($ret_ary['fForwarderExchangeRate']>0)
                    {
                        $ret_ary['fTotalPriceForwarderCurrency'] = ($fTotalPriceIncludingVatUsd/$ret_ary['fForwarderExchangeRate']);
                        $ret_ary['fTotalPriceNoMarkupForwarderCurrency'] = ( ($fTotalPriceIncludingVatUsd-$fCurrencyMarkupPriceUSD)/$ret_ary['fForwarderExchangeRate']);
                        $ret_ary['fTotalPriceNoMarkupForwarderCurrency_new'] = (($fTotalPriceIncludingVatUsd-$fCurrencyMarkupPriceUSD)/$ret_ary['fForwarderExchangeRate']);
                    }  
                }
            } 
            $fDkkExchangeRate = 0;
            if($postSearchAry['idCurrency']==20)
            {
                /*
                * If customer currency is DKK(20) then we consider customer's currency exchange rate as fDkkExchangeRate
                */
                $fDkkExchangeRate = $postSearchAry['fExchangeRate'];
            }
            else
            {
                $dkkExchangeAry = array();
                $dkkExchangeAry = $kWHSSearch->getCurrencyDetails(20);	 
                if($dkkExchangeAry['fUsdValue']>0)
                {
                    $fDkkExchangeRate = $dkkExchangeAry['fUsdValue'] ;						 
                }
            }
            
            $fTotalPriceUSD = $postSearchAry['fExchangeRate'] * $quotePricingDetailsArys['fTotalPriceCustomerCurrency']; 
            $fTotalPriceNoMarkupUSD = $fTotalPriceNoMarkup * $postSearchAry['fExchangeRate'] ;

            $ret_ary['iIncludeComments'] = $quotePricingDetailsArys['iInsuranceComments'];
            $ret_ary['szForwarderComment'] = $quotePricingDetailsArys['szForwarderComment']; 

            if($quotePricingDetailsArys['iInsuranceComments']==1)
            {
                $ret_ary['szOtherComments'] = $quotePricingDetailsArys['szForwarderComment']; 
            }  
            
            $kConfig = new cConfig();
            $transportModeListAry = array();
            //$transportModeListAry = $kConfig->getAllTransportMode($bookingQuoteArys['idTransportMode']); 
            $iBookingLanguage = getLanguageId();
            $transportModeListAry=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__',$iBookingLanguage);
              
            $ret_ary['szFrequency'] =$transportModeListAry[$bookingQuoteArys['idTransportMode']]['szFrequency']; 
 
            $ret_ary['idForwarderQuoteCurrency'] = $quotePricingDetailsArys['idForwarderCurrency'];
            $ret_ary['szForwarderQuoteCurrency'] = $quotePricingDetailsArys['szForwarderCurrency'];
            $ret_ary['fForwarderQuoteCurrencyExchangeRate'] = $quotePricingDetailsArys['fForwarderCurrencyExchangeRate']; 
            $ret_ary['fForwarderTotalQuotePrice'] = $quotePricingDetailsArys['fTotalPriceForwarderCurrency']; 

            $ret_ary['fPriceMarkupCustomerCurrency'] = $fCurrencyMarkupPrice;
            $ret_ary['fMarkupPercentage'] = $fMarkupPercentage;
            $ret_ary['fTotalPriceUSD'] = $fTotalPriceUSD ;
            $ret_ary['fTotalPriceNoMarkupUSD'] = $fTotalPriceNoMarkupUSD ; 
            $ret_ary['iTransitHours'] = $quotePricingDetailsArys['iTransitDays']; 
            $ret_ary['fReferalPercentage'] = $quotePricingDetailsArys['fReferalPercentage'];
            
            //$ret_ary['fReferalAmount'] = round((float)(($ret_ary['fTotalPriceForwarderCurrency'] * $quotePricingDetailsArys['fReferalPercentage'])/100),2); 

            /*
            * Rounding the value when forwarder and customer currency are same. 
            */
            if($quotePricingDetailsArys['idForwarderCurrency']==$postSearchAry['idCustomerCurrency'])
            {
                $fTotalPriceForwarderCurrencyExcludingHandlingFee = round((float)$fTotalPriceForwarderCurrencyExcludingHandlingFee); 
                $fTotalSelfInvoiceAmountQuoteCurrency = round((float)$fTotalSelfInvoiceAmountQuoteCurrency);
            } 
            if($ret_ary['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
            {
                if($postSearchAry['fExchangeRate']>0)
                {
                    $ret_ary['fReferalAmountUSD'] = ($quotePricingDetailsArys['fReferalAmount'] * $postSearchAry['fExchangeRate']) ; 
                }
                if($ret_ary['fForwarderExchangeRate']>0)
                {
                    $ret_ary['fReferalAmount'] = ($ret_ary['fReferalAmountUSD'] / $ret_ary['fForwarderExchangeRate']) ; 
                }
                //$ret_ary['fTotalVatForwarderCurrency'] = $quotePricingDetailsArys['fTotalVat'];
                /*
                * From Version 2 we always have Forwarder VAT as 0
                */
                $ret_ary['fTotalVatForwarderCurrency'] = 0;
                $ret_ary['fTotalForwarderPriceWithoutHandlingFee'] = $fTotalSelfInvoiceAmountQuoteCurrency; //This field always have fTotalSelfInvoiceAmount + fReferalAmount
                //Deducting Referal fee from Self invoiced amount
                $fTotalSelfInvoiceAmountQuoteCurrency = ($fTotalSelfInvoiceAmountQuoteCurrency - $quotePricingDetailsArys['fReferalAmount']);
                
                if($ret_ary['idForwarderCurrency']==$ret_ary['idForwarderQuoteCurrency'])
                {
                    $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmountQuoteCurrency;
                }
                else
                {
                    $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountQuoteCurrency * $ret_ary['fForwarderQuoteCurrencyExchangeRate'];  
                    if($ret_ary['fForwarderExchangeRate']>0)
                    { 
                        $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmountUSD / $ret_ary['fForwarderExchangeRate'];
                        $ret_ary['fTotalForwarderPriceWithoutHandlingFee'] = $fTotalSelfInvoiceAmount; //This field always have fTotalSelfInvoiceAmount + fReferalAmount
                        //deducting Referral Fee from self invoice amount
                        $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmount - $ret_ary['fReferalAmount'];
                        $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmount;
                    } 
                }
            }
            else
            {
                /*
                *  If forwarder Quote currency and Customer currency are same then then VAT should also be same in case of Referal Bookings
                */
                if($quotePricingDetailsArys['idForwarderCurrency']==$postSearchAry['idCustomerCurrency'])
                {
                    $fTotalVatForwarderCurrency = $quotePricingDetailsArys['fTotalVatCustomerCurrency'];
                }
                else
                {
                    $fTotalCustomerVatUsd = $quotePricingDetailsArys['fTotalVatCustomerCurrency']* $postSearchAry['fExchangeRate']; 
                    if($ret_ary['fForwarderQuoteCurrencyExchangeRate']>0)
                    {
                        $fTotalVatForwarderCurrency = round((float)($fTotalCustomerVatUsd/$ret_ary['fForwarderQuoteCurrencyExchangeRate']),2);
                        $fTotalVatForwarderCurrency = $fTotalVatForwarderCurrency/1.025; // removing currency mark-up
                    }
                } 
                /*
                * From Version 2 we always have Forwarder VAT as 0
                */
                $fTotalVatForwarderCurrency = 0;
                $ret_ary['fTotalVatForwarderCurrency'] = $fTotalVatForwarderCurrency;
                
                $fManualFeeForwarderAccountCurrency = 0;
                if($ret_ary['fForwarderCurrencyExchangeRate']>0)
                {
                    $fManualFeeForwarderAccountCurrency = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderCurrencyExchangeRate']),2);
                }    
                /*
                * Referal fee is calculated based on (Total Price + VAT - Handling Fee (if applicable))
                */   
                $fTotalVatForwarderCurrencyExcludingHandlingFee = round((float)($fTotalPriceForwarderCurrencyExcludingHandlingFee * $fApplicableVatRate * 0.01),2);  
                      
                $fTotalForwarderPriceWithoutHandlingFee = ($fTotalPriceForwarderCurrencyExcludingHandlingFee + $fTotalVatForwarderCurrencyExcludingHandlingFee);
                 
                $fReferalAmount = round((float)($fTotalSelfInvoiceAmountQuoteCurrency * $ret_ary['fReferalPercentage'] * 0.01),2); 
                if($ret_ary['idForwarderCurrency']==$ret_ary['idForwarderQuoteCurrency'])
                {
                    $ret_ary['fReferalAmount'] = $fReferalAmount;
                    $ret_ary['fReferalAmountUSD'] = ($fReferalAmount * $ret_ary['fForwarderQuoteCurrencyExchangeRate']) ;
                    
                    //Deducting Referal fee from Self invoiced amount
                    $fTotalSelfInvoiceAmountQuoteCurrency = ($fTotalSelfInvoiceAmountQuoteCurrency - $fReferalAmount);
                    $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmountQuoteCurrency;
                }
                else 
                {
                    $ret_ary['fReferalAmountUSD'] = ($fReferalAmount * $ret_ary['fForwarderQuoteCurrencyExchangeRate']) ;
                    
                    $fTotalForwarderPriceWithoutHandlingFeeUSD = $fTotalForwarderPriceWithoutHandlingFee * $ret_ary['fForwarderQuoteCurrencyExchangeRate'];
                    $fTotalSelfInvoiceAmountUSD = $fTotalSelfInvoiceAmountQuoteCurrency * $ret_ary['fForwarderQuoteCurrencyExchangeRate'];
                    
                    if($ret_ary['fForwarderExchangeRate']>0)
                    {
                        $ret_ary['fReferalAmount'] = ($ret_ary['fReferalAmountUSD'] / $ret_ary['fForwarderExchangeRate']) ;  
                        $fTotalForwarderPriceWithoutHandlingFee = $fTotalForwarderPriceWithoutHandlingFeeUSD / $ret_ary['fForwarderExchangeRate'];
                        $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmountUSD / $ret_ary['fForwarderExchangeRate'];
                        
                        //deducting Referral Fee from self invoice amount
                        $fTotalSelfInvoiceAmount = $fTotalSelfInvoiceAmount - $ret_ary['fReferalAmount'];
                        $ret_ary['fTotalSelfInvoiceAmount'] = $fTotalSelfInvoiceAmount;
                    }
                } 
                $ret_ary['fTotalForwarderPriceWithoutHandlingFee'] = $fTotalForwarderPriceWithoutHandlingFee; 
            } 
            if((float)$quotePricingDetailsArys['fTotalVatCustomerCurrency']>0)
            {
                $kConfig_new1 = new cConfig();
                $kConfig_new1->loadCountry($postSearchAry['szCustomerCountry']);
                $szVATRegistration = $kConfig_new1->szVATRegistration;                
                $ret_ary['szVATRegistration'] = $szVATRegistration;
            }
            
            $ret_ary['fTotalPriceCustomerCurrency'] = $quotePricingDetailsArys['fTotalPriceCustomerCurrency'];
            $ret_ary['fTotalVat'] = $quotePricingDetailsArys['fTotalVatCustomerCurrency']; 
            $ret_ary['dtQuoteValidTo'] = $quotePricingDetailsArys['dtQuoteValidTo'] ; 
            $ret_ary['iHandlingFeeApplicable'] = $iHandlingFeeApplicable;
            $ret_ary['fTotalHandlingFeeUSD'] = $quotePricingDetailsArys['fTotalHandlingFeeUSD']; 
            $ret_ary['iFinancialVersion'] = __TRANSPORTECA_FINANCIAL_VERSION__;
 
            if($quotePricingDetailsArys['fHandlingCurrencyExchangeRate']>0)
            {
                $ret_ary['fTotalHandlingFeeLocalCurrency'] = $quotePricingDetailsArys['fTotalHandlingFeeUSD']/$quotePricingDetailsArys['fHandlingCurrencyExchangeRate']; 
                $ret_ary['idHandlingFeeLocalCurrency'] = $quotePricingDetailsArys['idHandlingCurrency']; 
                $ret_ary['szHandlingFeeLocalCurrency'] = $quotePricingDetailsArys['szHandlingCurrency']; 
                $ret_ary['szHandlingFeeLocalCurrencyExchangeRate']=$quotePricingDetailsArys['fHandlingCurrencyExchangeRate'];
            }
            if($postSearchAry['fExchangeRate']>0)
            {
                $ret_ary['fTotalHandlingFeeCustomerCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $postSearchAry['fExchangeRate']),2);
            }
            if($ret_ary['fForwarderQuoteCurrencyExchangeRate']>0)
            {
                $ret_ary['fTotalHandlingFeeQuoteCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderQuoteCurrencyExchangeRate']),2);
            }
            if($ret_ary['fForwarderExchangeRate']>0)
            {
                $ret_ary['fTotalHandlingFeeForwarderCurrency'] = round((float)($quotePricingDetailsArys['fTotalHandlingFeeUSD'] / $ret_ary['fForwarderExchangeRate']),2);
            }     
            $ret_ary['fVATPercentage'] = $quotePricingDetailsArys['fVATPercentage']; 
            $ret_ary['fDkkExchangeRate'] = $fDkkExchangeRate;
             
            if(!empty($ret_ary))
            {
                $update_query = '';
                foreach($ret_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }   
            $update_query = rtrim($update_query,","); 
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {   
                $postSearchAry = array();
                $postSearchAry = $kBooking->getBookingDetails($idBooking);
                $isManualCourierBooking = $postSearchAry['isManualCourierBooking'];

                if($bRecalculateInsuranceFlag)
                {
                    if($postSearchAry['iInsuranceIncluded']==1 || $postSearchAry['iInsuranceChoice']==3) //1. Yes 3. Optional
                    {  
                        $insuranceInputAry = array();
                        $insurancePriceAry = array();
                        $insuranceInputAry['iInsurance'] = 1;
                        $insuranceInputAry['idInsuranceCurrency'] = $postSearchAry['idGoodsInsuranceCurrency'];
                        $insuranceInputAry['fValueOfGoods'] = $postSearchAry['fValueOfGoods'];

                        /*
                        * Adding Insurance prices to the booking
                        */
                        $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry);
                        
                        if($postSearchAry['iInsuranceChoice']==3)
                        {
                            $insuranceInputAry = array();
                            $insurancePriceAry = array();
                            $insuranceInputAry['iInsurance'] = 0; 

                            /*
                             * This looks bit funny but as per process we are calling same function again to achive following point
                             * If insurance choice is added as 'Optional' then we by deault mark Insurance option on /pay/ screen as 'No' so do this i am removing Insurance flag
                            */
                            $kBooking->updateInsuranceDetails($insuranceInputAry,$postSearchAry,'REMOVE_INSURANCE_ONLY'); 
                        }
                    }
                }
                
                $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
                $kConfig = new cConfig();
                $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                $szCountryStrUrl = $kConfig->szCountryISO ;
                $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                $szCountryStrUrl .= $kConfig->szCountryISO ;

                if($iHandlingFeeApplicable==1)
                {
                    $addHandlingFeeDetailsAry = array();
                    $addHandlingFeeDetailsAry['idBooking'] = $idBooking;
                    $addHandlingFeeDetailsAry['idForwarder'] = $bookingQuoteArys['idForwarder'];
                    if($iManualFeeApplicable==1)
                    {
                        $addHandlingFeeDetailsAry['iProductType'] = __AUTOMATIC_MANUAL_FEE__; 
                    }
                    else
                    {
                        $addHandlingFeeDetailsAry['iProductType'] = $quotePricingDetailsArys['iProductType']; 
                    } 
                    $addHandlingFeeDetailsAry['idCourierProvider'] = $quotePricingDetailsArys['idCourierProvider'];
                    $addHandlingFeeDetailsAry['idCourierProviderProduct'] = $quotePricingDetailsArys['idCourierProviderProduct'];
                    $addHandlingFeeDetailsAry['idHandlingCurrency'] = $quotePricingDetailsArys['idHandlingCurrency'];
                    $addHandlingFeeDetailsAry['szHandlingCurrency'] = $quotePricingDetailsArys['szHandlingCurrency'];
                    $addHandlingFeeDetailsAry['fHandlingCurrencyExchangeRate'] = $quotePricingDetailsArys['fHandlingCurrencyExchangeRate'];
                    $addHandlingFeeDetailsAry['fHandlingFeePerBooking'] = $quotePricingDetailsArys['fHandlingFeePerBooking'];
                    $addHandlingFeeDetailsAry['fHandlingFeePerBookingLocalCurrency'] = $quotePricingDetailsArys['fHandlingFeePerBooking']/$quotePricingDetailsArys['fHandlingCurrencyExchangeRate'];
                    $addHandlingFeeDetailsAry['fHandlingMarkupPercentage'] = $quotePricingDetailsArys['fHandlingMarkupPercentage']; 
                    $addHandlingFeeDetailsAry['idHandlingMinMarkupCurrency'] = $quotePricingDetailsArys['idHandlingMinMarkupCurrency'];
                    $addHandlingFeeDetailsAry['szHandlingMinMarkupCurrency'] = $quotePricingDetailsArys['szHandlingMinMarkupCurrency'];
                    $addHandlingFeeDetailsAry['fHandlingMinMarkupCurrencyExchangeRate'] = $quotePricingDetailsArys['fHandlingMinMarkupCurrencyExchangeRate'];
                    $addHandlingFeeDetailsAry['fHandlingMinMarkupPrice'] = $quotePricingDetailsArys['fHandlingMinMarkupPrice']; 
                    $addHandlingFeeDetailsAry['fHandlingMinMarkupPriceLocalCurrency'] = $quotePricingDetailsArys['fHandlingMinMarkupPrice']/$quotePricingDetailsArys['fHandlingMinMarkupCurrencyExchangeRate'];

                    $kBooking->updateBookingsHandlingFeeDetails($addHandlingFeeDetailsAry,$idBooking);
                }

                /*
                * IF $iQuickQuote==__QUICK_QUOTE_SEND_QUOTE__ then we already have added courier service data data like as per automatic courier booking
                 * ELSE for $isManualCourierBooking==1 we add a dummy line in table tblcourierbooking
                */
                if($isManualCourierBooking==1 && $iQuickQuote!=__QUICK_QUOTE_SEND_QUOTE__)
                {
                    $kCourierService = new cCourierServices();
                    $iLabelCreatedByForwarder = $kCourierService->checkAllCourierProductPricing($postSearchAry); 
                    if($iLabelCreatedByForwarder)
                    {
                        $iBookingIncluded = 1;
                    }
                    else
                    {
                        $iBookingIncluded = 0;
                    }
                    $updateBookingAry = array();
                    $updateBookingAry['szCurrencyCode'] = $postSearchAry['szCurrency'];
                    $updateBookingAry['iBookingIncluded'] = $iBookingIncluded;
                    $kBooking->updateCourierServiceDetails($updateBookingAry,$idBooking); 	
                }  
                if($iQuickQuote==__QUICK_QUOTE_SEND_QUOTE__)
                {
                    ob_end_clean(); 
                    $redirect_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    header("location:".$redirect_url);
                    die;
                }
                else
                {
                    ob_end_clean();
                    $redirect_url = __BOOKING_QUOTE_SERVICE_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    header("location:".$redirect_url);
                    die;
                } 
            } 
        }
        break;
    }
} 
die;   

?>