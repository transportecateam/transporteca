<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );

ini_set('max_execution_time',360000); 
ini_set('memory_limit','3048M');
ini_set('error_reporting',E_ALL & ~E_NOTICE );
ini_set('display_error','1');

$kDatabase = new cDatabase();
$allpostAry=array();
$szLanguageCode='de';
$query="
            SELECT
                wpp.ID,
                wpp.post_type
            FROM
                ".__DBC_SCHEMATA_WP_POSTS__." AS wpp
            INNER JOIN
                ".__DBC_SCHEMATA_WP_TERMS_RELATIONSHIPS__." AS wptr
            ON
                wptr.object_id=wpp.ID
            INNER JOIN
                ".__DBC_SCHEMATA_WP_TERMS__." AS wpt
            ON
                wptr.term_taxonomy_id=wpt.term_id
            WHERE
                wpt.slug='".  mysql_escape_custom($szLanguageCode)."'
            ORDER BY
                wpp.ID ASC 
            ";

if ($result = $kDatabase->exeSQL($query))
{
    if ($kDatabase->iNumRows > 0)
    {
       while($row=$kDatabase->getAssoc($result))
       {
            $allpostAry[]=$row;
       }
        
    }
}

if(!empty($allpostAry))
{
  $i=0;
  foreach($allpostAry as $post)
  {
      $szpostType='post_'.$post['post_type'];
      $queryupdate="
                    UPDATE
                        ".__DBC_SCHEMATA_WP_ICL_TRANSLATIONS__."
                    SET
                        language_code='".mysql_escape_custom($szLanguageCode)."'
                    WHERE
                        element_id='".(int)$post['ID']."'
                    AND
                        element_type='".mysql_escape_custom($szpostType)."'        
                ";
      
      if($result = $kDatabase->exeSQL( $queryupdate ))
      {
         $i++; 
      }
  }
  echo $i.' records updated successfully';
}
?>				