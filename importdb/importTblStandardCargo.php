<?php
/**
 * Importing Standard Cargo Table Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes();
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();

$filename = __APP_PATH_DATA_CSV__."/tblstandardcargo_25-jan.csv";
//echo $filename ;
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $cargoAry[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	
	
	if(!empty($cargoAry))
	{
		$j=2;
		for($i=2;$i<=count($cargoAry);$i++)
		{
			$cargoData[$j]['idSearchMini']=$cargoAry[$i][1];
			$cargoData[$j]['idCategory']=$cargoAry[$i][2];
			$cargoData[$j]['szSellerReference']=$cargoAry[$i][3];
			$cargoData[$j]['szShortName']=$cargoAry[$i][4];
			$cargoData[$j]['szLongName']=$cargoAry[$i][5];
			$cargoData[$j]['szShortNameDanish']=$cargoAry[$i][6];
			$cargoData[$j]['szLongNameDanish']=$cargoAry[$i][7];
			$cargoData[$j]['fVolume']=$cargoAry[$i][8];
			$cargoData[$j]['fWeight']=$cargoAry[$i][9];
			$cargoData[$j]['fLength']=$cargoAry[$i][10];
			$cargoData[$j]['fWidth']=$cargoAry[$i][11];
			$cargoData[$j]['fHeight']=$cargoAry[$i][12];
			$cargoData[$j]['iColli']=$cargoAry[$i][13];
			$cargoData[$j]['dtCreatedOn']=$cargoAry[$i][14];
			$cargoData[$j]['iActive']=$cargoAry[$i][15];
			//break;
			$j++;
		}
		//print_r($countriesArr);
		$kImportData->importStandardCargoData($cargoData);
		
		echo "Data is successfully imported to tblstandardcargo table.";
	}
	
	//print_r($countriesArr);
?>