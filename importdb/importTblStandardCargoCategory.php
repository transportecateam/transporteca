<?php
/**
 * Importing Standard Cargo Table Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
include_classes();
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();

$filename = __APP_PATH_DATA_CSV__."/tblstandardcargocategory_25-jan.csv";
//echo $filename ;
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $cargoCateogoryAry[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	
	
	if(!empty($cargoCateogoryAry))
	{
		$j=2;
		for($i=2;$i<=count($cargoCateogoryAry);$i++)
		{
			$cargoCateogoryData[$j]['szCategoryName']=$cargoCateogoryAry[$i][1];
			$cargoCateogoryData[$j]['szCategoryNameDanish']=$cargoCateogoryAry[$i][2];
			$cargoCateogoryData[$j]['iActive']=$cargoCateogoryAry[$i][3];
			$cargoCateogoryData[$j]['dtCreatedOn']=$cargoCateogoryAry[$i][4];
			$cargoCateogoryData[$j]['dtUpdatedOn']=$cargoCateogoryAry[$i][5];
			$cargoCateogoryData[$j]['isDeleted']=$cargoCateogoryAry[$i][6];
			$j++;
		}
		//print_r($countriesArr);
		$kImportData->importStandardCargoCategoryData($cargoCateogoryData);
		
		echo "Data is successfully imported to tblstandardcargocategory table.";
	}
	
	//print_r($countriesArr);
?>