<?
/**
 * Importing Country Table Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/tbl_county.csv";
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $countriesdatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	
	
	if(!empty($countriesdatas))
	{
		$j=0;
		for($i=2;$i<=count($countriesdatas);$i++)
		{
			$countriesArr[$j]['szCountryName']=$countriesdatas[$i][1];
			$countriesArr[$j]['szCountryISO']=$countriesdatas[$i][2];
			$countriesArr[$j]['iInternationDialCode']=$countriesdatas[$i][3];
			$countriesArr[$j]['dtRevised']=$countriesdatas[$i][4];
			$countriesArr[$j]['iActive']=$countriesdatas[$i][5];
			$countriesArr[$j]['iActiveFromDropdown']=$countriesdatas[$i][6];
			$countriesArr[$j]['iActiveToDropdown']=$countriesdatas[$i][7];
			$countriesArr[$j]['fStandardTruckRate']=$countriesdatas[$i][8];
			
			$countriesArr[$j]['dtStrUpdated']=$countriesdatas[$i][9];
			$countriesArr[$j]['szLatitude']=$countriesdatas[$i][10];
			$countriesArr[$j]['szLongitude']=$countriesdatas[$i][11];
			
			$j++;
		}
		//print_r($countriesArr);
		$kImportData->importCountriesData($countriesArr);
		
		echo "Data is successfully imported to tblcountries table.";
	}
	
	//print_r($countriesArr);
?>