<?
/**
 * Importing Warehouses Pricing Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/pricingwtow.csv";
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $warehousespricingdatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	//print_r($warehousespricingdatas);
	
	if(!empty($warehousespricingdatas))
	{
		$j=0;
		for($i=2;$i<=count($warehousespricingdatas);$i++)
		{
			$wareHousePricingArr[$j]['FromWarehouseID']=$warehousespricingdatas[$i][0];
			$wareHousePricingArr[$j]['ToWarehouseID']=$warehousespricingdatas[$i][1];
			$wareHousePricingArr[$j]['BookingCutoffHours']=$warehousespricingdatas[$i][2];
			$wareHousePricingArr[$j]['CutOffDayID']=$warehousespricingdatas[$i][3];
			$wareHousePricingArr[$j]['CutOffLocalTime']=$warehousespricingdatas[$i][4];
			$wareHousePricingArr[$j]['AvailableDayID']=$warehousespricingdatas[$i][5];
			$wareHousePricingArr[$j]['AvailableLocalTime']=$warehousespricingdatas[$i][6];
			$wareHousePricingArr[$j]['TransitDays']=$warehousespricingdatas[$i][7];
			$wareHousePricingArr[$j]['FrequencyID']=$warehousespricingdatas[$i][8];
			$wareHousePricingArr[$j]['RateWM']=$warehousespricingdatas[$i][9];
			$wareHousePricingArr[$j]['MinimumRateWM']=$warehousespricingdatas[$i][10];
			$wareHousePricingArr[$j]['RatePerBooking']=$warehousespricingdatas[$i][11];
			$wareHousePricingArr[$j]['FreightCurrency']=$warehousespricingdatas[$i][12];
			$wareHousePricingArr[$j]['ValidFromDate']=$warehousespricingdatas[$i][13];
			$wareHousePricingArr[$j]['ExpiryDate']=$warehousespricingdatas[$i][14];
			$j++;
		}
		
		$kImportData->importWareHousePricingData($wareHousePricingArr);
		
		echo "Data is successfully imported to tblpricingwtw table.";
		//print_r($wareHousePricingArr);
	}
	
?>