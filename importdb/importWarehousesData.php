<?
/**
 * Importing Warehouses Table Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/warehouses.csv";
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $warehousesdatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	//print_r($warehousesdatas);
	
	if(!empty($warehousesdatas))
	{
		$j=0;
		for($i=2;$i<=count($warehousesdatas);$i++)
		{
			$wareHouseArr[$j]['ForwarderID']=$warehousesdatas[$i][1];
			$wareHouseArr[$j]['WarehouseName']=$warehousesdatas[$i][2];
			$wareHouseArr[$j]['AddressLine1']=$warehousesdatas[$i][3];
			$wareHouseArr[$j]['AddressLine2']=$warehousesdatas[$i][4];
			$wareHouseArr[$j]['AddressLine3']=$warehousesdatas[$i][5];
			$wareHouseArr[$j]['Postcode']=$warehousesdatas[$i][6];
			$wareHouseArr[$j]['City']=$warehousesdatas[$i][7];
			$wareHouseArr[$j]['State']=$warehousesdatas[$i][8];
			$wareHouseArr[$j]['CountryID']=$warehousesdatas[$i][9];
			$wareHouseArr[$j]['PhoneNumber']=$warehousesdatas[$i][10];
			$wareHouseArr[$j]['Latitude']=$warehousesdatas[$i][11];
			$wareHouseArr[$j]['Longitude']=$warehousesdatas[$i][12];
			$wareHouseArr[$j]['UTCOffsetMs']=$warehousesdatas[$i][13];
			$wareHouseArr[$j]['Active']=$warehousesdatas[$i][14];
			$j++;
		}
		
		$kImportData->importWareHouseData($wareHouseArr);
		
		echo "Data is successfully imported to tblwarehouses table.";
	}
?>