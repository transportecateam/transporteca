<?
/**
 * Importing Forwarders Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/forwarders.csv";
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $forwardersDatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	if(!empty($forwardersDatas))
	{
		$j=0;
		for($i=2;$i<=count($forwardersDatas);$i++)
		{
			$forwardersArr[$j]['displayName']=$forwardersDatas[$i][1];
			$forwardersArr[$j]['Logo']=$forwardersDatas[$i][2];
			$forwardersArr[$j]['companyName']=$forwardersDatas[$i][3];
			$forwardersArr[$j]['AddressLine1']=$forwardersDatas[$i][4];
			$forwardersArr[$j]['AddressLine2']=$forwardersDatas[$i][5];
			$forwardersArr[$j]['AddressLine3']=$forwardersDatas[$i][6];
			$forwardersArr[$j]['Postcode']=$forwardersDatas[$i][7];
			$forwardersArr[$j]['City']=$forwardersDatas[$i][8];
			$forwardersArr[$j]['State']=$forwardersDatas[$i][9];
			$forwardersArr[$j]['idCountry']=$forwardersDatas[$i][10];
			$forwardersArr[$j]['PhoneNumber']=$forwardersDatas[$i][11];
			$forwardersArr[$j]['Extension']=$forwardersDatas[$i][12];
			$forwardersArr[$j]['VATNumber']=$forwardersDatas[$i][13];
			$forwardersArr[$j]['CompanyNumber']=$forwardersDatas[$i][14];
			$forwardersArr[$j]['StandardTerms']=$forwardersDatas[$i][15];
			$forwardersArr[$j]['BankName']=$forwardersDatas[$i][16];
			$forwardersArr[$j]['BankCountry']=$forwardersDatas[$i][17];
			$forwardersArr[$j]['NameOnAccount']=$forwardersDatas[$i][18];
			$forwardersArr[$j]['AccountNumber']=$forwardersDatas[$i][19];
			$forwardersArr[$j]['SortCode']=$forwardersDatas[$i][20];
			$forwardersArr[$j]['IBANAccountNumber']=$forwardersDatas[$i][21];
			$forwardersArr[$j]['CurrencyID']=$forwardersDatas[$i][22];
			$forwardersArr[$j]['Active']=$forwardersDatas[$i][23];
			$j++;
		}
		
		$kImportData->importForwardersData($forwardersArr);
		
		echo "Data is successfully imported to tblforwarders table.";
	}
?>