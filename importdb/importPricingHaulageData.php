<?
/**
 * Importing Pricing Haulage Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/pricingHaulage.csv";
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $pricinghaulagedatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	//print_r($pricinghaulagedatas);
	
	if(!empty($pricinghaulagedatas))
	{
		$j=0;
		for($i=2;$i<=count($pricinghaulagedatas);$i++)
		{
			$pricinghaulageArr[$j]['WarehouseID']=$pricinghaulagedatas[$i][0];
			$pricinghaulageArr[$j]['DirectionID']=$pricinghaulagedatas[$i][1];
			$pricinghaulageArr[$j]['UpToKm']=$pricinghaulagedatas[$i][2];
			$pricinghaulageArr[$j]['HaulageTransitTimeID']=$pricinghaulagedatas[$i][3];
			$pricinghaulageArr[$j]['RateWM_Km']=$pricinghaulagedatas[$i][4];
			$pricinghaulageArr[$j]['MinimumRateWM_Km']=$pricinghaulagedatas[$i][5];
			$pricinghaulageArr[$j]['RatePerBooking']=$pricinghaulagedatas[$i][6];
			$pricinghaulageArr[$j]['HaulageCurrency']=$pricinghaulagedatas[$i][7];
			$pricinghaulageArr[$j]['CrossBorder']=$pricinghaulagedatas[$i][8];
			$pricinghaulageArr[$j]['Updated']=$pricinghaulagedatas[$i][9];
			$pricinghaulageArr[$j]['ExpiryDate']=$pricinghaulagedatas[$i][10];
			$j++;
		}
		
		$kImportData->importPricingHaulageData($pricinghaulageArr);
		//print_r($pricinghaulageArr);
	}
?>