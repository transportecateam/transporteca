<?
/**
 * Importing Forwarders Contacts Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/forwarderContact.csv";
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $forwardersContactDatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	
	//print_r($forwardersContactDatas);
	if(!empty($forwardersContactDatas))
	{
		$j=0;
		for($i=2;$i<=count($forwardersContactDatas);$i++)
		{
			$forwardersContactArr[$j]['Fname']=$forwardersContactDatas[$i][2];
			$forwardersContactArr[$j]['Lname']=$forwardersContactDatas[$i][3];
			$forwardersContactArr[$j]['CountryID']=$forwardersContactDatas[$i][4];
			$forwardersContactArr[$j]['LPN']=$forwardersContactDatas[$i][5];
			$forwardersContactArr[$j]['MPN']=$forwardersContactDatas[$i][6];
			$forwardersContactArr[$j]['Email']=$forwardersContactDatas[$i][7];
			$forwardersContactArr[$j]['RoleID']=$forwardersContactDatas[$i][8];
			$forwardersContactArr[$j]['Password']=$forwardersContactDatas[$i][9];
			$forwardersContactArr[$j]['ForwarderId']=$forwardersContactDatas[$i][1];
			$j++;
		}
		//print_r($forwardersContactArr);
		$kImportData->importForwardersContactData($forwardersContactArr);
		
		echo "Data is successfully imported to tblforwardercontacts table.";
	}
	
	
	//print_r($forwardersContactArr);
?>