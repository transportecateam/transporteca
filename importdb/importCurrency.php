<?
/**
 * Importing Country Table Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/tblcurrency.csv";
//echo $filename ;
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $currencydatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	
	
	if(!empty($currencydatas))
	{
		$j=0;
		for($i=1;$i<=count($currencydatas);$i++)
		{
			$currencyArr[$j]['szCurrency']=$currencydatas[$i][1];
			$currencyArr[$j]['iPricing']=$currencydatas[$i][2];
			$currencyArr[$j]['iBooking']=$currencydatas[$i][3];
			$currencyArr[$j]['szFeed']=$currencydatas[$i][4];
			//break;
			$j++;
		}
		//print_r($countriesArr);
		$kImportData->importCurrencyData($currencyArr);
		
		echo "Data is successfully imported to tblcurrency table.";
	}
	
	//print_r($countriesArr);
?>