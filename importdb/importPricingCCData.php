<?
/**
 * Importing Pricing CC Data
 */
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include_once(__APP_PATH_CLASSES__."/dbimport.class.php");
ini_set (max_execution_time,360000);

$kImportData = new cDBImport();


$filename = __APP_PATH_DATA_CSV__."/pricingcc.csv";
$row = 1;
if (($handle = fopen("$filename", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	        $num = count($data);
	        //echo "<p> $num fields in line $row: <br /></p>\n";

	        for ($c=0; $c < $num; $c++) {
	            $pricingccdatas[$row][]= trim($data[$c]);
	        }
	         $row++;
	    }
	    fclose($handle);
	}
	
	if(!empty($pricingccdatas))
	{
		$j=0;
		for($i=2;$i<=count($pricingccdatas);$i++)
		{
			$pricingccdatasArr[$j]['FromWarehouseID']=$pricingccdatas[$i][0];
			$pricingccdatasArr[$j]['ToWarehouseID']=$pricingccdatas[$i][1];
			$pricingccdatasArr[$j]['OriginDestinationID']=$pricingccdatas[$i][2];
			$pricingccdatasArr[$j]['Price']=$pricingccdatas[$i][3];
			$pricingccdatasArr[$j]['Currency']=$pricingccdatas[$i][4];
			$pricingccdatasArr[$j]['Updated']=$pricingccdatas[$i][5];
			$pricingccdatasArr[$j]['Active']=$pricingccdatas[$i][6];
			$j++;
		}
		
		$kImportData->importPricingCCData($pricingccdatasArr);
		//print_r($pricingccdatasArr);
	}
	
?>