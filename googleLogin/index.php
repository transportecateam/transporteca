<?php
session_start(); //session start
if( !defined( "__APP_PATH__" ) )
    define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
    require( __APP_PATH__ . "/inc/constants.php" );
    require_once (__APP_PATH__.'/googleLogin/libraries/Google/autoload.php');

    $client = new Google_Client();

    //$client->setApplicationName('API Project');
    $client->setScopes(implode(' ', array(Google_Service_Gmail::GMAIL_READONLY)));
    //Web Applicaion (json)
    $client->setAuthConfigFile('client_id.json');
    $client->setRedirectUri("http://www.a.transport-eca.com/googleLogin/");
    $client->setAccessType('offline');        

    // Redirect the URL after OAuth
    if (isset($_GET['code'])) {
        $client->authenticate($_GET['code']);
        $_SESSION['access_token'] = $client->getAccessToken();

        $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
    }
    $refreshToken = $client->getRefreshToken();
    print_r($refreshToken);
    echo "<br /><br />";

    // If Access Toket is not set, show the OAuth URL
    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
        $client->setAccessToken($_SESSION['access_token']);
    } else {
        $authUrl = $client->createAuthUrl();
    }

    if ($client->getAccessToken()) {

        $_SESSION['access_token'] = $client->getAccessToken();

        // Prepare the message in message/rfc822
        try {

            // The message needs to be encoded in Base64URL

             $service = new Google_Service_Gmail($client);

            $optParams = array();
           // $optParams['maxResults'] = 10; // Return Only 5 Messages
            $optParams['labelIds'] = 'INBOX'; // Only show messages in Inbox
            $optParams['q']="in:inbox after:2016/01/01 before:2016/01/30";
            $messages = $service->users_messages->listUsersMessages('me',$optParams);
            $list = $messages->getMessages();
           // print_r($list);
            if(!empty($list))
            {
                for($i=0;$i<count($list);++$i)
                {
                    $messageId = $list[$i]->getId(); // Grab first Message

                    $parts=array();
                    $message=array();
                    $optParamsGet = array();
                    $optParamsGet['format'] = 'full'; // Display message in payload
                    $message = $service->users_messages->get('me',$messageId,$optParamsGet);
                    $messagePayload = $message->getPayload();
                    $headers = $message->getPayload()->getHeaders();
                    $parts = $message->getPayload()->getParts();

                    if(!empty($headers))
                    {
                        foreach($headers as $headerss)
                        {
                           if($headerss['name']=='Subject')
                           {
                               echo $headerss['value'];
                           }
                        }
                    }

                    $partArr=toArray($parts[0]);

                    print_r($partArr);
                    echo "<br /><br /><br /><br />";
                    $body = $parts[0]['body'];
                    $rawData = $body->data;
                    $sanitizedData = strtr($rawData,'-_', '+/');
                    $decodedMessage = base64_decode($sanitizedData);
                    //echo $decodedMessage."<br /><br /><br /><br />";
                  // die();

                }
            } 
        } catch (Exception $e) {
            print($e->getMessage());
            unset($_SESSION['access_token']);
        } 
    }

    // If there is no access token, there will show url
    if ( isset ( $authUrl ) ) { 
           echo $authUrl;
     }
?> 