#Forwarder Rating myBooking
RewriteRule myBooking/(.*)/$ /myBooking.php?flag=mb&myBookingkey=$1 [L,NC] customer section

//following htaccess id added after live of 07/03/2013

 //Management section
 
#manageSEOPages
RewriteRule ^manageSEOPages$ /manageSEOPages/ [L,NC,R=301]
RewriteRule ^manageSEOPages/$ /manageSeoPage.php?flag=seo_page&menuflag=content [L,NC]

#manageSEOPages
RewriteRule ^addSEOPage$ /addSEOPage/ [L,NC,R=301]
RewriteRule ^addSEOPage/$ /addEditSeoPage.php?flag=seo_page&menuflag=content [L,NC]

#manageSEOPages
RewriteRule ^editSEOPage$ /editSEOPage/ [L,NC,R=301]
RewriteRule ^editSEOPage/([0-9]+)/$ /addEditSeoPage.php?flag=seo_page&menuflag=content&id=$1 [L,NC]

#Countries
RewriteRule ^sitemapVideo$ /sitemapVideo/ [L,NC,R=301]
RewriteRule ^sitemapVideo/$ /sitemapVideo.php?flag=vs&menuflag=tncfwd [L,NC]

//customer modules

#blog
RewriteRule ^information$ /information/ [L,NC,R=301]
RewriteRule ^information/(.*)$ /information.php?flag=$1&menuflag=info [L,NC]

#Sitemap rewrites
RewriteRule ^sitemap$ /sitemap/ [L,NC,R=301]
RewriteRule ^sitemap/$ /siteMap.php?dtDate=$1 [L,NC]



#home rewrites
change the file name only
RewriteRule ^invitation_confirmed/$ /index.php?flag=cb&err=2&use_session=1 [L,NC]



20/03/2013
#reviewQueueMessage
RewriteRule ^reviewQueueMessage$ /reviewQueueMessage/ [L,NC,R=301]
RewriteRule ^reviewQueueMessage/$ /queueMessageList.php?menuflag=msg&leftmenuflag=reviewquemsg [L,NC]

#editNews
RewriteRule ^editNews$ /editNews/ [L,NC,R=301]
RewriteRule ^editNews/$ /editNews.php?menuflag=msg&leftmenuflag=editNews [L,NC]

#systemMessages
RewriteRule ^systemMessages$ /systemMessages/ [L,NC,R=301]
RewriteRule ^systemMessages/$ /systemMessages.php?menuflag=msg&leftmenuflag=reviewemaillog [L,NC]

#updateServiceExpiry
RewriteRule ^updateServiceExpiry$ /updateServiceExpiry/ [L,NC,R=301]
RewriteRule ^updateServiceExpiry/(.*)/$ /PricingServicesIntroduction.php?updatekey=$1&flag=pric [L,NC]