<?php
 ob_start();
session_start();
$page_title="My Account";
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/MultiUserAccess/";

$deleteflag=false;
if(!empty($_POST['removeUserArr']))
{
	if($kUser->removeUser($_POST['removeUserArr']))
	{
		$deleteflag=true;	
	}
}

if((int)$kUser->id>0)
{
	$userMutliArr=$kUser->getMultiUserData($kUser->idGroup);
	$pendingUserAry = $kUser->getPendingInvitedUser($kUser->id,$kUser->idGroup);
}
if(count($_REQUEST['userIdArr'])>0)
{
	
	?>

<div id="remove_user_id">
<div id="popup-bg"></div>
<div id="popup-container">
<div class="popup remove-shippers-popup">
	<?php
	if(count($_REQUEST['userIdArr'])>0)
	{
		$total=count($_REQUEST['userIdArr']);
		$remove_arr=implode(",",$_REQUEST['userIdArr']);
		
		$remove_msg=t($t_base.'messages/users');
	}
	?>
	<form name="remove_confirm" id="remove_confirm" method="post">
	<h5>
	<?=t($t_base.'messages/remove_link');?></h5>
	<p><?=t($t_base.'messages/would_you_like');?> <?=$total?> <?=$remove_msg?></p>
	<br/>
	<input type="hidden" name="removeUserArr" id="removeUserArr" value="<?=$remove_arr?>">
	<p align="center"><a href="javascript:void(0)" class="button1" onclick="remove_user_data();"><span><?=t($t_base.'fields/confirm');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancel_remove_user_popup('remove_user_id','hsbody-2');"><span><?=t($t_base.'fields/cancel');?></span></a></p>
	</form>
</div>
</div>
</div>
<script type="text/javascript">
//disableForm('hsbody-2');
</script>
<?php	
}

/*if($deleteflag)
{
	echo "<p style=color:green>".t($t_base.'messages/remove_success_user')."</p>";
}
if($_REQUEST['successflag']=='1')
{
	echo "<p style=color:green>".t($t_base.'messages/mapped_success')."</p>";
}*/
?>
<form name="removeUser" id="removeUser" method="post">
<p class="f-size-22"><?=t($t_base.'title/user_linked_your_account');?></p>
	<br />
	<div class="oh">
		<p class="fl-40"><?=t($t_base.'fields/user_current_linkd');?></p>
		<p class="fl-40">
			<select multiple="multiple" name="userIdArr[]" id="userIdArr" onclick="get_remove_user_id();">
				<?php
				   if(!empty($userMutliArr))
				   { 
				       foreach($userMutliArr as $userMutliArrs)
				   	   {
				   	   		if($userMutliArrs['id']!=$_SESSION['user_id']){
				?>
				<option value="<?=$userMutliArrs['id']?>"><?=$userMutliArrs['szFirstName']?> <?=$userMutliArrs['szLastName']?> <?php if($userMutliArrs['iInvited']!='1') { echo "(".t($t_base.'messages/not_confirmed').")";  }?></option>
				<?php }}}?>
				
				<?php
				   if(!empty($pendingUserAry))
				   { 
				       foreach($pendingUserAry as $pendingUserArys)
				   	   {
				   	   		if($pendingUserArys['id']!=$_SESSION['user_id']){
				?>
						<option value="<?=$pendingUserArys['id']?>"><?=$pendingUserArys['szFirstName']?> <?=$pendingUserArys['szLastName']?> (Pending)</option>
				<?php }}}?>
				
			</select>
		</p>
		<p class="fl-20" align="right"><a href="javascript:void(0)" class="button1" style="opacity:0.4;margin-top: 121px;" onclick="" id="remove"><span><?=t($t_base.'fields/remove');?></span></a></p>
	</div>
	</form>