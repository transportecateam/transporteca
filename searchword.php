<?php
ob_start();
session_start();

$szMetaTitle= __EXPLAIN_META_TITLE__;
$szMetaKeywords = __EXPLAIN_META_KEYWORDS__;
$szMetaDescription = __EXPLAIN_META_DESCRIPTION__;

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;

require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

$t_base="ExplainPage/";

$kExplain= new cExplain();
$szKeyWordLeftValue=sanitize_all_html_input(trim($_POST['szKeyWordLeftValue']));
$iLanguage=  getLanguageId();


$kConfig=  new cConfig();
$langArr=$kConfig->getLanguageDetails('',$iLanguage);
if(!empty($langArr))
    $szLanguageszName=$langArr[0]['szName'];
?>
<div id="hsbody">
	
	<form name="wordSearchForm" id="wordSearchForm" method="post">
		<div class="clearfix">
			<input type="text" size="60" name="szKeyWordValue" autocomplete="off" onkeyup="submitenterKeyValue(event,'wordSearchForm');" id="szKeyWordValue" value="<?php echo $szKeyWordLeftValue;?>" placeholder="<?php echo t($t_base.'fields/search');?>">	
			<input type="text" style="display: none;" />
                        <input type="hidden" value="<?php echo strtolower($szLanguageszName);?>" id="idLanguage"  name="idLanguage"/>
			<img align="absmiddle" id="searchButtonId" src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/images/magnifying-icon.jpg">
		</div>
	</form>
	<div id="level3SerachResult">
		<?php
			if($szKeyWordLeftValue!='')
			{
				$kExplain->submitSearchFields($szKeyWordLeftValue);
				$level3DataArr=$kExplain->getExplainContentSearchByKeyword($szKeyWordLeftValue,$iLanguage);

				showSearchResultLevel3($kExplain,$level3DataArr,$szKeyWordLeftValue,$iLanguage);
			}
		?>
	</div>
</div><br/><br/>
<div id="all_available_service" style="display:none;"></div>
		<?php 
require_once( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>