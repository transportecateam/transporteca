<?php
session_start();
if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/../"));
}
require_once(__APP_PATH__ . "/inc/constants.php");  
require_once( __APP_PATH__ . "/inc/functions.php" );
 
$requestInputAry = array();   
$requestInputAry = $_REQUEST['transporteca'];  
 
$kSeo = new cSEO();
$szLanguage = $kSeo->getLanguageName($requestInputAry);
require_once(__APP_PATH_LAYOUT__."/ajax_partnerHeader.php");  

$kPartner = new cPartner();   
$t_base = "Partner/"; 
static $bAuthentication = false;
$szMethod=trim($_REQUEST['transporteca']['szRequest']);


if($szMethod=='getShippers')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_REGISTERED_SHIPPER');
}
else if($szMethod=='getConsignees')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_REGISTERED_CONSIGNEES');
}

else if($szMethod=='getShipperDetails')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_REGISTERED_SHIPPER_DETAIL');
}
else if($szMethod=='getConsigneeDetails')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_REGISTERED_CONSIGNEE_DETAIL');
}
else if($szMethod=='updateShipperDetails')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_UPDATE_SHIPPER_DETAIL');
}
else if($szMethod=='updateConsigneeDetails')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_UPDATE_CONSIGNEE_DETAIL');
}
else if($szMethod=='addShipperDetails')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_ADD_SHIPPER_DETAIL');
}
else if($szMethod=='addConsigneeDetails')
{
    $clientApiHeaderAry = cPartner::request_headers();     
    $kTransportecaApi = new cTransportecaApi();
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CUSTOMER_ADD_CONSIGNEE_DETAIL');
}
?>
