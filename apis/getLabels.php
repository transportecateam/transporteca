<?php
session_start();
if (!defined("__APP_PATH__")) {
    define("__APP_PATH__", realpath(dirname(__FILE__) . "/../"));
}
require_once(__APP_PATH__ . "/inc/constants.php");  
require_once( __APP_PATH__ . "/inc/functions.php" );
 //ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
$requestInputAry = array();   
$requestInputAry = $_REQUEST['transporteca'];  

$szTaskType = $_REQUEST['task_type'];
 
$kSeo = new cSEO();
$szLanguage = $kSeo->getLanguageName($requestInputAry);
require_once(__APP_PATH_LAYOUT__."/ajax_partnerHeader.php");  

$kPartner = new cPartner();   
$t_base = "Partner/"; 
static $bAuthentication = false;

$clientApiHeaderAry = cPartner::request_headers();     
$kTransportecaApi = new cTransportecaApi();
if($szTaskType=='GET_SHIPPER_DETAILS')
{
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'GET_LABEL_DETAILS');
}
else if($szTaskType=='CONFIRM_LABEL_DOWNLOAD')
{
    $kTransportecaApi->callTransportecaAPI($clientApiHeaderAry,$requestInputAry,'CONFIRM_LABEL_DOWNLOAD');
}

   
?>
