<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );

$szMetaTitle= __FORWARDER_SIGNUP_PAGE_META_TITLE__;
$szMetaKeywords = __FORWARDER_SIGNUP_PAGE_META_KEYWORDS__;
$szMetaDescription = __FORWARDER_SIGNUP_PAGE_META_DESCRIPTION__;
$style="style='top: 0px; bottom: auto; display: block;'";
$headerSearchFlag=1;
require_once( __APP_PATH_LAYOUT__ . "/header_new.php" );

?>
<div id="hsbody">
	<div id="forwardersignup">
	<?
		require_once( __APP_PATH__ . "/ajax_forwarderSignup.php" ); ?>
	</div>
</div>
<?php
include( __APP_PATH_LAYOUT__ . "/footer_new.php" );
?>