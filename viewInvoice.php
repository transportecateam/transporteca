<?php
/**
 * View Invoice
 */
 ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
include( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$kBooking = new cBooking();
checkAuth();
$idBooking=$_REQUEST['idBooking']; 
if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
{
    $flag=$_REQUEST['flag']; 
    $pdfhtmlflag=true;
    $flag = 'pdf'; //We have removed to show HTML so this flag will always be pdf now.
    
    if($flag=='pdf')
    {
        $pdfhtmlflag=false;
    } 
    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    if($postSearchAry['iInsuranceIncluded']==1)
    {
    	$szInvoiceUrl = __BASE_URL__."/viewInsuranceInvoice/".$idBooking."/";
    	$bookingInvoicePdf=getCombinedInvoicesPdf($idBooking,$pdfhtmlflag); 
    }
    else
    { 
        if($postSearchAry['iFinancialVersion']==2)
        {
            $bookingInvoicePdf=getInvoiceConfirmationPdfFileHTML_v2($idBooking,$pdfhtmlflag);
        }
        else
        {
            $bookingInvoicePdf=getInvoiceConfirmationPdfFileHTML($idBooking,$pdfhtmlflag);
        }
    }
    if($flag=='pdf')
    {
        download_booking_pdf_file($bookingInvoicePdf);
        die;
    }
}
else
{
    header('Location:'.__LANDING_PAGE_URL__);
    exit();
}
/*
$t_base = "BookingDetails/";
$kBooking = new cBooking();
$kConfig = new cConfig();

checkAuth();
$idBooking=$_REQUEST['idBooking'];
if($kBooking->isBookingBelongsToCustomer($idBooking,$_SESSION['user_id']))
{
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);

$shipperCountry=$kConfig->getCountryName($bookingDataArr['idShipperCountry']);

$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
?>
<html>
	<head>
		<title><?=t($t_base.'fields/invoice');?></title>
	</head>
	<script>
	function PrintDiv()
	{    
                var divToPrint = document.getElementById('viewInvoice');
                var popupWin = window.open('', '_blank', 'width=900,height=700');
                popupWin.document.open();
                popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
                popupWin.document.close();
	}
	</script>
	<body>
		<p style="text-align:right;margin-right:300px;"><a href="javascript:void(0)" onclick="PrintDiv();"><?=t($t_base.'fields/print')?></a></p>
		<div id="viewInvoice">
		<div style="margin:0px;padding:0px;font-family:calibri,arial;font-size:14px;color:#000;text-align:center;">
		<div style="margin:0 auto;width:600px;text-align:left;">
			<h1 style="font-size:20px;text-align:center;"><?=t($t_base.'fields/invoice');?></h1>
			<div style="overflow:hidden;margin:0;">
				<p style="margin:0;float:left;width:295px;"><?=$bookingDataArr['szShipperCompanyName']?><br/>
				<?=$bookingDataArr['szShipperAddress']?> <?=$bookingDataArr['szShipperAddress2']?><br />
				<?=$bookingDataArr['szShipperPostCode']." ".$bookingDataArr['szShipperCity']?><br />
				<?=$shipperCountry?><br />
				<?=t($t_base.'fields/att');?>: <?=$bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']?><br />
				</p>
				<p style="margin:0;float:right;width:295px;">
					<?=t($t_base.'fields/invoice_number')?>: <?=$bookingDataArr['szInvoice']?><br />
					<?=t($t_base.'fields/invoice_date')?>: <?=date('d F Y H:i',strtotime($bookingDataArr['dtBookingConfirmed']))?><br />
					<?=t($t_base.'fields/booking_date')?>: <?=date('d F Y H:i',strtotime($bookingDataArr['dtBookingConfirmed']))?><br />
					<?=t($t_base.'fields/booing_reference')?>: <?=$bookingDataArr['szBookingRef']?>
				</p>
			</div>
			
			<br />
			<div style="overflow:hidden;margin:0;">
				<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'fields/booking_width');?>:</p>
				<p style="margin:0;float:right;width:295px;"><?=$bookingDataArr['szForwarderDispName']?><br />
					<?=$bookingDataArr['szForwarderAddress']." ".$bookingDataArr['szForwarderAddress2']?><br />
					<?=$bookingDataArr['szForwarderPostCode']." ".$bookingDataArr['szForwarderCity']?><br />
					<?=$forwardCountry?></p>
			</div>
			<p><strong><?=t($t_base.'fields/service');?>:</strong> <?=$bookingDataArr['szServiceName']?></p>
			<div style="overflow:hidden;margin:0;">
				<p style="margin:0;font-weight:bold;float:left;width:295px;"><?=t($t_base.'fields/cut_off');?>:</p>
				<p style="margin:0;font-weight:bold;float:right;width:295px;"><?=t($t_base.'fields/avial');?>:</p>
			</div>
			<div style="overflow:hidden;margin:0;">
				<p style="margin:0;float:left;width:295px;"><?=date('d F Y H:i',strtotime($bookingDataArr['dtCutOff']))?></p>
				<p style="margin:0;float:right;width:295px;"><?=date('d F Y H:i',strtotime($bookingDataArr['dtAvailable']))?></p>
			</div>
			<p><strong><?=t($t_base.'fields/cargo');?>:</strong><br />
				<?
				$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
				if(!empty($cargoDetailArr))
				{
					$ctr=0;
					foreach($cargoDetailArr as $cargoDetailArrs)
					{
						$str='';
						$str=++$ctr.". ".$cargoDetailArrs['fWeight']."".strtoupper($cargoDetailArrs['wmdes']).",  ".$cargoDetailArrs['fLength']."x".$cargoDetailArrs['fWidth']."x".$cargoDetailArrs['fHeight']."".$cargoDetailArrs['cmdes'].",".$cargoDetailArrs['szCommodity'];	
						echo $str."<br/>";
						
					}
				}
				?>
				</p>
			
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td width="80%" style="border-bottom:solid 1px #a0a0a0;font-size:14px;"><?=t($t_base.'fields/freight_and_custons_clearance');?></td>
					<td width="10%" style="border-bottom:solid 1px #a0a0a0;font-size:14px;"><?=$bookingDataArr['szCurrency']?></td>
					<td width="10%" align="right" style="border-bottom:solid 1px #a0a0a0;font-size:14px;"><?=$bookingDataArr['fTotalPriceCustomerCurrency'];?></td>
				</tr>
				<tr>
					<td width="80%" style="font-size:14px;"><?=t($t_base.'fields/total')?></td>
					<td width="10%" style="font-size:14px;"><?=$bookingDataArr['szCurrency']?></td>
					<td width="10%" align="right" style="font-size:14px;"><?=$bookingDataArr['fTotalPriceCustomerCurrency'];?></td>
				</tr>
			</table>
			<br />
			<p><?=t($t_base.'fields/your_comment')?>:<br /><?=$bookingDataArr['szInvoiceComment']?></p>
			<p><?=t($t_base.'messages/for_booking_related_ques')?> <strong><?=$bookingDataArr['szForwarderDispName']?></strong> <?=t($t_base.'messages/customer_service_on')?> customerservice@<?=$bookingDataArr['szForwarderDispName']?>.com.</p>
			<p><?=t($t_base.'messages/invoice_paid_via_paypal')?> <?=date('d F Y H:i',strtotime($bookingDataArr['dtBookingConfirmed']))?></p>
			<br />
			<p><?=t($t_base.'fields/thank_you')?>!</p>
			<br />
			<p align="center"></p>
		</div>
		</div>
		</div>
	</body>
</html>
<?php
}
else
{
	header('Location:'.__BASE_URL__.'/home/');
	exit();

}*/
?>