<?php
if( !isset( $_SESSION ) )
{
    session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/admin_functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
include_classes();
 
function display_quick_quote_contents($postSearchAry=array())
{ 
    $kQuote = new cQuote();
    $kBooking = new cBooking();
    $t_base="management/quickQuote/";  
    $idBooking = $postSearchAry['id'];
    if($idBooking>0)
    {
        $bookingDataAry = array();
        $bookingDataAry = $kBooking->getExtendedBookingDetails($idBooking);
    } 
    if($bookingDataAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
    {
        $mode = 'SEND_QUOTE';
    } 
    //$mode = 'SEND_QUOTE'; 
    ?>
    <div class="clearfix">
        <span class="accordian"><?php echo t($t_base.'title/quick_quote')?></span>
    </div> 
    <!-- Booking Search Form -->
    <div id="quick_quote_search_form_container">
        <?php echo display_quick_quote_search_form($kQuote,$postSearchAry); ?>
    </div>
         
    <!-- Search result listing's -->
    <div id="quick_quote_search_result_container" style="display:none;">
        <?php //echo display_quick_quote_search_result($searchResultAry);?>
    </div>
    
    <!-- Booking Information Section--> 
    <div id="quick_quote_booking_infomation_main_container">
        <div class="clearfix accordion-container">
            <span class="accordian" style="text-transform:none;"> 
                <a id="pending_task_overview_open_close_link" class="open-icon" onclick="close_open_div('booking_information_container','pending_task_overview_open_close_link','open',1);" href="javascript:void(0);"></a>
                Booking Information
            </span> 
        </div>
        <div id="quick_quote_booking_infomation_container">
            <?php echo display_quick_quote_shipper_consignee($bookingDataAry,$mode); ?>
        </div> 
    </div>
    <?php
}  
function display_quick_quote_search_form($kQuote,$postSearchAry=array())
{
    $t_base="management/quickQuote/";
    $iUpdatedTermsDropDown=0;
    if(!empty($_POST['quickQuoteAry']))
    {
        $postQuickQuoteAry = $_POST['quickQuoteAry'];
        $szOriginCountryStr = $postQuickQuoteAry['szOriginCountryStr'];
        $szDestinationCountryStr = $postQuickQuoteAry['szDestinationCountryStr'];
        $idServiceTerms = $postQuickQuoteAry['idServiceTerms'];
        $dtShipmentDate = $postQuickQuoteAry['dtShipmentDate'];
        $szCargoType = $postQuickQuoteAry['szCargoType'];
        $szCargoType = $postQuickQuoteAry['szCargoType'];
        $szCustomerType = $postQuickQuoteAry['szCustomerType'];
        $idCustomerCurrency = $postQuickQuoteAry['idCustomerCurrency'];
        $idCustomerCountry = $postQuickQuoteAry['idCustomerCountry'];
        
        if(!empty($postQuickQuoteAry['szShipmentType']))
        {
            if(in_array('BREAK_BULK',$postQuickQuoteAry['szShipmentType']))
            {
                $iBreakBulk = 1;
            }
            else if(in_array('PARCEL',$postQuickQuoteAry['szShipmentType']))
            {
                $iParcel = 1;
            }
            else if(in_array('PALLET',$postQuickQuoteAry['szShipmentType']))
            {
                $iPallets = 1;
            }
        }
    }
    else if(!empty($postSearchAry))
    { 
        $iUpdatedTermsDropDown=1;
        $szOriginCountryStr = $postSearchAry['szOriginCountry'];
        $szDestinationCountryStr = $postSearchAry['szDestinationCountry'];
        $idServiceTerms = $postSearchAry['idServiceTerms'];
        $szCargoType = $postSearchAry['szCargoType'];
        $dtShipmentDate = date("d/m/Y",strtotime($postSearchAry['dtTimingDate']));
        
        if(empty($szCargoType))
        {
            if($postSearchAry['isMoving']==1)
            {
                $szCargoType = "__PERSONAL_EFFECTS__"; 
            }
            else
            {
                $szCargoType = "__GENERAL_CODE__"; 
            } 
        }
        
        $szCustomerType = $postSearchAry['iPrivateShipping'];
        $idCustomerCurrency = $postSearchAry['idCurrency'];
        $idCustomerCountry = $postSearchAry['szCustomerCountry']; 
        
        if($postSearchAry['iCargoPackingType']==1) //Parcel
        {
            $iParcel = 1;
        }
        else if($postSearchAry['iCargoPackingType']==2) //Pallet
        {
            $iPallets = 1;
        }
        else if($postSearchAry['iCargoPackingType']==3) //Break Bulk
        {
            $iBreakBulk = 1;
        }
    }
    else
    {
        $dtShipmentDate = date('d/m/Y');
        $idServiceTerms = __SERVICE_TERMS_FOB__;
        $szCargoType = "__GENERAL_CODE__"; 
    } 
    $iLanguage = 1; //ENGLISH
    $kConfig = new cConfig();
    $kVatApplication = new cVatApplication();
    
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);
    
    $countryAry = array();
    $countryAry = $kConfig->getAllCountries();
    
    $serviceTermsAry = array();
    $serviceTermsAry = $kConfig->getAllServiceTerms(false,false,true);
     
    $manualFeeCargoTypeArr = array();
    $manualFeeCargoTypeArr = $kVatApplication->getManualFeeCaroTypeList();
    
    $szMainContainerClass="overview-form";
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container"; 
    $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
    
    ?>
    <script type="text/javascript" defer="">
        $().ready(function() {	 
            var autocomplete1,place ; 
            function initialize1() 
            {
                var input1 = document.getElementById('szOriginCountryStr');
                var options = {types: ['regions']};

                var autocomplete1 = new google.maps.places.Autocomplete(input1); 
                $(".pac-container:last").attr("id", 'szOriginCountryStrPacContainer');
                google.maps.event.addListener(autocomplete1, 'place_changed', function() 
                {    
                    var szOriginAddress_js = $("#szOriginCountryStr").val();   
                    checkFromAddressAdmin(szOriginAddress_js,'FROM_COUNTRY');
                }); 
                var input2 = document.getElementById('szDestinationCountryStr');
                var autocomplete2 = new google.maps.places.Autocomplete(input2); 
                $(".pac-container:last").attr("id", 'szDestinationCountryStrPacContainer');
                google.maps.event.addListener(autocomplete2, 'place_changed', function() 
                {
                    var szDestinationAddress_js = $("#szDestinationCountryStr").val();  
                    checkFromAddressAdmin(szDestinationAddress_js,'TO_COUNTRY');
                }); 

                $('#szOriginCountryStr').on('paste focus keypress keyup', function (e){  
                    $('#szOriginCountryStr').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szOriginCountryStrPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks
                        }
                        else
                        {
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display'); 
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);
                                }
                            });
                        }
                        var key_code = e.keyCode || e.which;
                        //console.log("ORG Code: "+key_code);
                        if (key_code == 9 || key_code == 13) {
                            prefillAddressOnTabPress('szOriginCountryStr',pac_container_id);
                        }  
                    }); 
                });

                $('#szDestinationCountryStr').on('paste focus', function (e){  
                    $('#szDestinationCountryStr').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szDestinationCountryStrPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks 
                            //console.log("div exist called");
                        }
                        else
                        {
                            var iFoundDiv = 1;
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display');  
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);  
                                    iFoundDiv = 2;
                                } 
                            });

                            if(iFoundDiv==1)
                            {
                                $(".pac-container").each(function(pac)
                                {
                                    var container_id = $(this).attr('id');  
                                    if(container_id=='' || container_id === undefined)
                                    {
                                        $(this).attr('id',pac_container_id); 
                                        return true;
                                    }
                                });
                            }
                        } 
                        console.log("Dest Code: "+e.keyCode);
                        if (e.keyCode == 9 || e.keyCode == 13) {
                            prefillAddressOnTabPress('szDestinationCountryStr',pac_container_id);
                        }  
                    });
                }); 
            } 
            function prefillAddressOnTabPress(input_field_id,pac_container_id)
            {  
                $(".pac-container").each(function(pac){
                   var disp = $(this).css('display'); 
                   var contaier_id = $(this).attr('id');  

                   //console.log("Container: "+contaier_id + " pac id: "+pac_container_id); 
                    if(contaier_id==pac_container_id)
                    {
                        var spanObj = $(this).children(".pac-item:first" );
                        var firstResult = ""; 
                        spanObj.children("span").each(function(){
                            var spanText = $(this).text(); 
                            spanText = jQuery.trim(spanText);
                            //console.log("Span Text: "+spanText);
                            if(spanText!='')
                            {
                                if(firstResult=='')
                                {
                                    firstResult += spanText;
                                }
                                else
                                {
                                    firstResult += ", "+ spanText;
                                }
                                //$(this).children( ".pac-item:first" ).addClass("pac-selected");
                                //$(this).css("display","none"); 
                                if(jQuery.trim(input_field_id)=='szOriginCountryStr')
                                {
                                    console.log("Mathed: "+firstResult);
                                    $("#szOriginCountryStr").val(firstResult);
                                    $("#szOriginCountryStr").select();
                                }
                                else if(jQuery.trim(input_field_id)=='szDestinationCountryStr')
                                { 
                                    console.log("Not Mathed");
                                    $("#szDestinationCountryStr").val(firstResult); 
                                    $("#szDestinationCountryStr").select();
                                } 
                            }
                        });  
                    }
                }); 
            } 
            function clean_pac_container()
            { 
                $("div").each(function(index){
                    if($( this ).hasClass("pac-container"))
                    {
                        $( this ).remove();
                    }
                }); 
            } 
            
        enableQQGetRatesButton('',1);
        $("#szOriginCountryStr").focus();
        initialize1();  
        
        //$("#szOriginCountryStr").focus();

        $("#dtShipmentDate").bind('keydown', function (event) {
            if(event.which == 13){
                var e = jQuery.Event("keydown");
                e.which = 9;//tab 
                e.keyCode = 9;
                $(this).trigger(e);
                return false;
            }
        }).datepicker({ <?=$date_picker_argument?> }); 
          
    });  
    </script> 
    <?php 
        if(!empty($kQuote->arErrorMessages['szSpecialError']))
        {
            echo '<div class="red_text">'.$kQuote->arErrorMessages['szSpecialError']." </div> ";
        }
    ?>
    <form id="quick_quote_search_form" name="quick_quote_search_form" method="post" action="javascript:void(0);"> 
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <span class="quote-field-container wd-19">
                From<br>
                <input type="text" name="quickQuoteAry[szOriginCountryStr]" placeholder="Start typing" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableQQGetRatesButton(event);$('#iCopyQuickQuoteBookingFlag').attr('value','0');" onblur="checkFromAddressAdmin(this.value,'FROM_COUNTRY',1)" id="szOriginCountryStr" value="<?php echo $szOriginCountryStr; ?>">
            </span> 
            <span class="quote-field-container wds-19">
                To<br>
                <input type="text" name="quickQuoteAry[szDestinationCountryStr]" placeholder="Start typing" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableQQGetRatesButton(event);$('#iCopyQuickQuoteBookingFlag').attr('value','0');" onblur="checkFromAddressAdmin(this.value,'TO_COUNTRY',1)" id="szDestinationCountryStr" value="<?php echo $szDestinationCountryStr; ?>">
            </span>
            <span class="quote-field-container wds-11">
                Terms<br> 
                <select name="quickQuoteAry[idServiceTerms]" id="idServiceTerms" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="update_ship_con(this.value);enableQQGetRatesButton(event);">
                   <option value="">Select</option>
                   <?php
                       if(!empty($serviceTermsAry))
                       {
                          foreach($serviceTermsAry as $serviceTermsArys)
                          {
                           ?>
                           <option value="<?php echo $serviceTermsArys['id']; ?>" <?php echo (($serviceTermsArys['id']==$idServiceTerms)?'selected':''); ?>><?php echo $serviceTermsArys['szDisplayName']; ?></option>
                           <?php
                          }
                       }
                   ?> 
                </select>
            </span>
            <span class="quote-field-container wds-11">
                Ready date<br>
                <input type="text" name="quickQuoteAry[dtShipmentDate]" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableQQGetRatesButton(event);" id="dtShipmentDate" value="<?php echo $dtShipmentDate; ?>">
            </span>
            <span class="quote-field-container wds-11">
                Cargo<br> 
                <select name="quickQuoteAry[szCargoType]" onchange="enableQQGetRatesButton(event);" id="szCargoType" onfocus="check_form_field_not_required(this.form.id,this.id);"> 
                   <?php
                        if(!empty($manualFeeCargoTypeArr))
                        {
                            foreach($manualFeeCargoTypeArr as $manualFeeCargoTypeArrs)
                            {
                               ?>
                               <option value="<?php echo $manualFeeCargoTypeArrs['szCode']; ?>" <?php echo (($manualFeeCargoTypeArrs['szCode']==$szCargoType)?'selected':''); ?>><?php echo $manualFeeCargoTypeArrs['szName']; ?></option>
                               <?php
                            }
                        }
                   ?> 
                </select> 
            </span>
            <span class="quote-field-container wds-23">
                Packing (one or more options)<br>
                <span class="checkbox-container quote-packing"><label id="label_szShipmentType_BREAK_BULK"><input type="checkbox" name="quickQuoteAry[szShipmentType][0]" <?php if($iBreakBulk==1){ echo 'checked'; }?> onclick="toggleCargoFields(this.id,'BREAK_BULK');enableQQGetRatesButton(event);" id="szShipmentType_BREAK_BULK" value="BREAK_BULK"></label>&nbsp;Break bulk</span>
                <span class="checkbox-container quote-packing"><label id="label_szShipmentType_PARCEL"><input type="checkbox" name="quickQuoteAry[szShipmentType][1]" <?php if($iParcel==1){ echo 'checked'; }?> onclick="toggleCargoFields(this.id,'PARCEL');enableQQGetRatesButton(event);" id="szShipmentType_PARCEL" value="PARCEL"></label>&nbsp;Packages</span>
                <span class="checkbox-container quote-packing"><label id="label_szShipmentType_PALLET"><input type="checkbox" name="quickQuoteAry[szShipmentType][2]" <?php if($iPallets==1){ echo 'checked'; }?> onclick="toggleCargoFields(this.id,'PALLET');enableQQGetRatesButton(event);" id="szShipmentType_PALLET" value="PALLET"></label>&nbsp;Pallets</span>
            </span>
            <span class="quote-field-container wds-6">
                Customer<br>
                <span class="checkbox-container"><input type="checkbox" name="quickQuoteAry[szCustomerType]" <?php if($szCustomerType==1){ echo 'checked'; }?> onclick="prefillCompanyField(this.id);enableQQGetRatesButton(event);" id="szCustomerType" value="1">&nbsp;Private</span>
            </span>
        </div>  
        <!-- Cargo description's container -->
        <div id="quick_quote_cargo_container">
            <div id="quick_quote_cargo_container_BREAK_BULK" <?php if($iBreakBulk==1){ echo "style='display:block;'"; } else { echo "style='display:none;'"; }?>>
                <?php echo display_quick_quote_cargo_fields('BREAK_BULK',$postSearchAry); ?>
            </div>
            <div id="quick_quote_cargo_container_PARCEL" <?php if($iParcel==1){ echo "style='display:block;'"; } else { echo "style='display:none;'"; }?>>
                <?php echo display_quick_quote_cargo_fields('PARCEL',$postSearchAry); ?>
            </div>
            <div id="quick_quote_cargo_container_PALLET" <?php if($iPallets==1){ echo "style='display:block;'"; }else{ echo "style='display:none;'"; }?>>
                <?php echo display_quick_quote_cargo_fields('PALLET',$postSearchAry); ?>
            </div>
        </div>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div class="clearfix email-fields-container quick-quote-button-container" style="width:100%;">  
            <span class="quick-quote-currency">
                Customer country<br>
                <select name="quickQuoteAry[idCustomerCountry]" id="idCustomerCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="update_ship_con_by_country_selection(this.value);enableQQGetRatesButton(event);">
                    <option value="">Select</option>
                    <?php
                        if(!empty($countryAry))
                        {
                           foreach($countryAry as $countryArys)
                           {
                            ?>
                            <option value="<?php echo $countryArys['id']; ?>" <?php echo (($countryArys['id']==$idCustomerCountry)?'selected':''); ?>><?php echo $countryArys['szCountryName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select> 
            </span>
            <span class="quick-quote-currency">
                Currency<br>
                <select name="quickQuoteAry[idCustomerCurrency]" id="idCustomerCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableQQGetRatesButton(event);">
                    <option value="">Select</option>
                    <?php
                        if(!empty($currencyAry))
                        {
                           foreach($currencyAry as $currencyArys)
                           {
                            ?>
                            <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idCustomerCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select> 
            </span>
            <span>
                &nbsp;<br>
                <input type="hidden" name="quickQuoteAry[iUpdatedTermsDropDown]" id="iUpdatedTermsDropDown" value="<?php echo $iUpdatedTermsDropDown;?>">
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="quick_quote_get_price_button"><span>GET RATES</span></a>
            </span>
            <span>
                &nbsp;<br> 
                <a href="javascript:void(0)" class="button2" onclick="clearQuickQuotesForm();"><span>Clear</span></a>
            </span> 
        </div>
    </form>
    <input type="hidden" name="quickQuoteAry[idOriginCountryHidden]" id="idOriginCountryHidden" value="">
    <input type="hidden" name="quickQuoteAry[idDestinationCountryHidden]" id="idDestinationCountryHidden" value="">
    <?php
    if(!empty($kQuote->arErrorMessages))
    {  
        $formId = 'quick_quote_search_form'; 
        display_form_validation_error_message($formId,$kQuote->arErrorMessages);
    }
} 

function display_quick_quote_shipper_consignee($bookingDataAry=array(),$szBookingType=false,$kQuote=false,$iSuccessFlag=false,$iQuickQuoteTryItOut=false)
{ 
    $t_base="management/quickQuote/";  
    $kConfig = new cConfig(); 
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
       
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
    
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);
    
    $languageAry = array();
    $languageAry = $kConfig->getLanguageDetails(false,false,false,false,false,true);
    
    $paymentTypeAry = array();
    $paymentTypeAry = $kConfig->getAllPaymentTypes();
      
    $kQuoteNew = new cQuote();
     
    $iInsuranceNotAvailable = false;
    if($kQuote->iInsuranceNotAvailable==1)
    {
        $iInsuranceNotAvailable = 1; 
    }
    if(!empty($_POST['quickQuoteShipperConsigneeAry']))
    {
        $quickQuoteShipperConsigneeAry = $_POST['quickQuoteShipperConsigneeAry'];
        $idBooking = $quickQuoteShipperConsigneeAry['idBooking'];
        $idBookingFile = $quickQuoteShipperConsigneeAry['idBookingFile'];
        $iShipperConsignee = $quickQuoteShipperConsigneeAry['iShipperConsignee'];
        $idUser = $quickQuoteShipperConsigneeAry['idUser'];
        $szFirstName = $quickQuoteShipperConsigneeAry['szFirstName'];
        $szLastName = $quickQuoteShipperConsigneeAry['szLastName'];
        $szEmail = $quickQuoteShipperConsigneeAry['szEmail'];
        $szCustomerCompanyName = $quickQuoteShipperConsigneeAry['szCustomerCompanyName'];
        $idCustomerDialCode = $quickQuoteShipperConsigneeAry['idCustomerDialCode'];
        $szCustomerPhoneNumber = $quickQuoteShipperConsigneeAry['szCustomerPhoneNumber'];
        $szCustomerAddress1 = $quickQuoteShipperConsigneeAry['szCustomerAddress1']; 
        $szCustomerPostCode = $quickQuoteShipperConsigneeAry['szCustomerPostCode'];
        $szCustomerCity = $quickQuoteShipperConsigneeAry['szCustomerCity'];
        $szCustomerCountry = $quickQuoteShipperConsigneeAry['szCustomerCountry'];
        
        $szShipperCompanyName = $quickQuoteShipperConsigneeAry['szShipperCompanyName'];
        $szShipperFirstName = $quickQuoteShipperConsigneeAry['szShipperFirstName'];
        $szShipperLastName = $quickQuoteShipperConsigneeAry['szShipperLastName'];
        $idShipperDialCode = $quickQuoteShipperConsigneeAry['idShipperDialCode'];
        $szShipperPhoneNumber = $quickQuoteShipperConsigneeAry['szShipperPhoneNumber'];
        $szShipperEmail = $quickQuoteShipperConsigneeAry['szShipperEmail'];
        $szOriginAddress = $quickQuoteShipperConsigneeAry['szOriginAddress'];        
        $szOriginPostcode = $quickQuoteShipperConsigneeAry['szOriginPostcode'];
        $szOriginCity = $quickQuoteShipperConsigneeAry['szOriginCity'];
        $idOriginCountry = $quickQuoteShipperConsigneeAry['idOriginCountry'];
        
        $szConsigneeCompanyName = $quickQuoteShipperConsigneeAry['szConsigneeCompanyName'];
        $szConsigneeFirstName = $quickQuoteShipperConsigneeAry['szConsigneeFirstName'];
        $szConsigneeLastName = $quickQuoteShipperConsigneeAry['szConsigneeLastName'];
        $idConsigneeDialCode = $quickQuoteShipperConsigneeAry['idConsigneeDialCode'];
        $szConsigneePhoneNumber = $quickQuoteShipperConsigneeAry['szConsigneePhoneNumber']; 
        $szConsigneeEmail = $quickQuoteShipperConsigneeAry['szConsigneeEmail'];
        $szDestinationAddress = $quickQuoteShipperConsigneeAry['szDestinationAddress'];
        $szDestinationPostcode = $quickQuoteShipperConsigneeAry['szDestinationPostcode'];
        $szDestinationCity = $quickQuoteShipperConsigneeAry['szDestinationCity'];
        $idDestinationCountry = $quickQuoteShipperConsigneeAry['idDestinationCountry'];
        
        $iInsuranceIncluded = $quickQuoteShipperConsigneeAry[$szBookingType]['iInsuranceIncluded'];
        $idGoodsInsuranceCurrency = $quickQuoteShipperConsigneeAry['idGoodsInsuranceCurrency'];
        $fCargoValue = $quickQuoteShipperConsigneeAry['fCargoValue'];
        $szCargoDescription = $quickQuoteShipperConsigneeAry['szCargoDescription'];
         
        $idDestinationCountry = $quickQuoteShipperConsigneeAry['idDestinationCountry'];
        $idDestinationCountry = $quickQuoteShipperConsigneeAry['idDestinationCountry']; 
        $iPrivateCustomer = $quickQuoteShipperConsigneeAry['iPrivateCustomer'];  
    }
    else if(!empty($bookingDataAry))
    {
        $iShipperConsignee = $bookingDataAry['iShipperConsignee'];
        $idBooking = $bookingDataAry['id'];
        $idBookingFile = $bookingDataAry['idFile'];
        $idUser = $bookingDataAry['idUser'];
        $szFirstName = $bookingDataAry['szFirstName'];
        $szLastName = $bookingDataAry['szLastName'];
        $szEmail = $bookingDataAry['szEmail']; 
        $szCustomerCompanyName = $bookingDataAry['szCustomerCompanyName'];  
        $idCustomerDialCode = $bookingDataAry['idCustomerDialCode'];  
        $szCustomerPhoneNumber = $bookingDataAry['szCustomerPhoneNumber'];  
        $szCustomerAddress1 = $bookingDataAry['szCustomerAddress1']; 
        $szCustomerPostCode = $bookingDataAry['szCustomerPostCode']; 
        $szCustomerCity = $bookingDataAry['szCustomerCity']; 
        $szCustomerCountry = $bookingDataAry['szCustomerCountry'];  
        
        $szShipperCompanyName = $bookingDataAry['szShipperCompanyName']; 
        $szShipperFirstName = $bookingDataAry['szShipperFirstName']; 
        $szShipperLastName = $bookingDataAry['szShipperLastName']; 
        $idShipperDialCode = $bookingDataAry['idShipperDialCode']; 
        $szShipperPhoneNumber = $bookingDataAry['szShipperPhoneNumber']; 
        $szShipperEmail = $bookingDataAry['szShipperEmail'];   
        $szOriginAddress = $bookingDataAry['szShipperAddress'];  
        $szOriginPostcode = $bookingDataAry['szShipperPostCode'];  
        $szOriginCity = $bookingDataAry['szShipperCity'];  
        $idOriginCountry = $bookingDataAry['idShipperCountry'];   
        $szConsigneeCompanyName = $bookingDataAry['szConsigneeCompanyName']; 
        $szConsigneeFirstName = $bookingDataAry['szConsigneeFirstName']; 
        $szConsigneeLastName = $bookingDataAry['szConsigneeLastName']; 
        $idConsigneeDialCode = $bookingDataAry['idConsigneeDialCode']; 
        $szConsigneePhoneNumber = $bookingDataAry['szConsigneePhoneNumber']; 
        $szConsigneeEmail = $bookingDataAry['szConsigneeEmail'];  
        $szDestinationAddress = $bookingDataAry['szConsigneeAddress']; 
        $szDestinationPostcode = $bookingDataAry['szConsigneePostCode']; 
        $szDestinationCity = $bookingDataAry['szConsigneeCity']; 
        $idDestinationCountry = $bookingDataAry['idConsigneeCountry'];  
        
        $iInsuranceIncluded = $bookingDataAry['iInsuranceChoice']; 
        $idGoodsInsuranceCurrency = $bookingDataAry['idGoodsInsuranceCurrency']; 
        
        if($bookingDataAry['fValueOfGoods']>0)
        {
            $fCargoValue = round((float)$bookingDataAry['fValueOfGoods']); 
        } 
        $szCargoDescription = utf8_decode($bookingDataAry['szCargoDescription']);  
        $iPrivateCustomer = $bookingDataAry['iPrivateCustomer'];
    }
    else
    {
        $iShipperConsignee = 2;
        $iInsuranceIncluded = 3;
    }  
    if(!$iQuickQuoteTryItOut){
    ?> 
    <script type="text/javascript">
        $().ready(function() {	
           $("#szFirstName").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false,
                select: function( event, ui ) 
                { 
                    cosole.log("Selected");
                } 
            });
            
            $("#szCustomerCompanyName").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false,
                select: function( event, ui ) 
                { 
                    cosole.log("Selected");
                } 
            });
            
            $("#szLastName").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false
            });  
            $("#szEmail").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false
            });    
            prefillCompanyField('szCustomerType');
        });
    </script>
    <?php }?>
    <form id="quick_quote_shipper_consignee_form" name="quick_quote_shipper_consignee_form" method="post"> 
        <div class="clearfix" id="booking_information_container" style="display:none;">
            <div class="clearfix email-fields-container" style="width:100%;">
                <div class="quote-field-container quick-quote-billing"> 
                    
                    <div class="clearfix email-fields-container" style="width:100%;">
                         <?php                           
                              $wd_26="wd-26";  
                              $wd_21="wd-21";
                              $wd_25="wd-25";
                            ?>
                        <span class="quote-field-container wd-25 quick-quote-title"><?php echo t($t_base.'fields/customer_role')?></span>
                        
                        <span class="quote-field-container checkbox-container <?php echo $wd_21;?>">
                            <input type="radio" name="quickQuoteShipperConsigneeAry[iShipperConsignee]" onclick="autofill_billing_address__new_rfq(1);" id="iShipperConsignee_1" <?php if($iShipperConsignee==1){ echo "checked"; }?> value="1"> <?php echo t($t_base.'fields/shipper')?>
                        </span> 
                        <span class="quote-field-container checkbox-container <?php echo $wd_26;?>">
                            <input type="radio" name="quickQuoteShipperConsigneeAry[iShipperConsignee]" onclick="autofill_billing_address__new_rfq(1);" id="iShipperConsignee_2" <?php if($iShipperConsignee==2){ echo "checked"; }?> value="2"> <?php echo t($t_base.'fields/consignee')?>
                        </span> 
                        <span class="quote-field-container checkbox-container <?php echo $wd_25;?> quick-quote-cb-title"> 
                            <input type="radio" name="quickQuoteShipperConsigneeAry[iShipperConsignee]" onclick="autofill_billing_address__new_rfq(1);" id="iShipperConsignee_3" <?php if($iShipperConsignee==3){ echo "checked"; }?> value="3"> <?php echo t($t_base.'fields/billing')?>
                        </span> 
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;">
                        <span class="quote-field-container wd-50">
                            <input type="text" name="quickQuoteShipperConsigneeAry[szFirstName]" onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);prefillCompanyField('szCustomerType');validateQQBookingInformation();buildQuickQuoteEmail('<?php echo $szBookingType; ?>');" id="szFirstName" placeholder="First Name" value="<?php echo $szFirstName; ?>">                        
                        </span>
                        <span class="quote-field-container wd-50"> 
                            <input type="text" name="quickQuoteShipperConsigneeAry[szLastName]"  onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);prefillCompanyField('szCustomerType');validateQQBookingInformation();buildQuickQuoteEmail('<?php echo $szBookingType; ?>');" id="szLastName" placeholder="Last Name" value="<?php echo $szLastName; ?>">
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field">
                            <input type="text" name="quickQuoteShipperConsigneeAry[szCustomerCompanyName]" onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="szCustomerCompanyName" placeholder="Company Name" value="<?php echo $szCustomerCompanyName; ?>">
                        </span> 
                    </div>
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="quote-field-container wd-35">
                            <select size="1" name="quickQuoteShipperConsigneeAry[idCustomerDialCode]" onchange="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" id="idCustomerDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()">
                                <?php
                                   if(!empty($dialUpCodeAry))
                                   {
                                        $usedDialCode = array();
                                        foreach($dialUpCodeAry as $dialUpCodeArys)
                                        {
                                            if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                            {
                                                $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idCustomerDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                            }
                                        }
                                   }
                            ?>
                            </select> 
                        </span>
                        <span class="quote-field-container wd-65">
                            <input type="text" name="quickQuoteShipperConsigneeAry[szCustomerPhoneNumber]" onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" id="szCustomerPhoneNumber" placeholder="Phone" value="<?php echo $szCustomerPhoneNumber; ?>" />
                        </span> 
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field"> 
                            <input type="text" name="quickQuoteShipperConsigneeAry[szEmail]" placeholder="Email" onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" id="szEmail" value="<?php echo $szEmail; ?>" >
                        </span>
                    </div>
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field"> 
                           <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szCustomerAddress1]" id="szCustomerAddress1" placeholder="Address" value="<?php echo $szCustomerAddress1; ?>">
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;">  
                        <span class="quote-field-container wd-33">  
                            <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szCustomerPostCode]" id="szCustomerPostCode" placeholder="Postcode" value="<?php echo $szCustomerPostCode; ?>">
                        </span>
                        <span class="quote-field-container wd-33">  
                            <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szCustomerCity]" id="szCustomerCity" placeholder="City" value="<?php echo $szCustomerCity; ?>">
                        </span>
                        <span class="quote-field-container wd-34">  
                            <select name="quickQuoteShipperConsigneeAry[szCustomerCountry]" id="szCustomerCountry" onchange = "<?php if($iQuickQuoteTryItOut){?>update_dail_code(this.value,'cust');<?php }?>autofill_billing_address__new_rfq(1);validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($allCountriesArr))
                                    {
                                       foreach($allCountriesArr as $allCountriesArrs)
                                       {
                                        ?>
                                        <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$szCustomerCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                        <?php
                                       }
                                    }
                                ?> 
                            </select>
                        </span>
                    </div>
                </div>
                <div class="quote-field-container quick-quote-shipping">
                    <div class="clearfix email-fields-container quick-quote-title" style="width:100%;"><?php echo t($t_base.'fields/shipper')?></div> 
                    
                    <div class="clearfix email-fields-container" style="width:100%;">
                        <span class="quote-field-container wd-50">
                            <input type="text" class="shipper-fields" name="quickQuoteShipperConsigneeAry[szShipperFirstName]" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" id="szShipperFirstName" placeholder="Shipper First Name" value="<?php echo $szShipperFirstName; ?>">
                        </span>
                        <span class="quote-field-container wd-50"> 
                            <input type="text" class="shipper-fields" name="quickQuoteShipperConsigneeAry[szShipperLastName]" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" id="szShipperLastName" placeholder="Shipper Last Name" value="<?php echo $szShipperLastName; ?>">
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field">
                            <input type="text" class="shipper-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="validateQQBookingInformation()" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szShipperCompanyName]" id="szShipperCompanyName" placeholder="Shipper Company" value="<?php echo $szShipperCompanyName; ?>">
                        </span> 
                    </div>
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="quote-field-container wd-35">
                            <select size="1" name="quickQuoteShipperConsigneeAry[idShipperDialCode]" class="shipper-fields" onchange="validateQQBookingInformation()" id="idShipperDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);">
                                <?php
                                   if(!empty($dialUpCodeAry))
                                   {
                                       $usedDialCode = array();
                                       foreach($dialUpCodeAry as $dialUpCodeArys)
                                       {
                                            if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                            {
                                                $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idShipperDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                            }
                                       }
                                   }
                            ?>
                            </select> 
                        </span>
                        <span class="quote-field-container wd-65">
                            <input type="text" class="shipper-fields" name="quickQuoteShipperConsigneeAry[szShipperPhoneNumber]" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" placeholder="Shipper Phone" id="szShipperPhoneNumber" value="<?php echo $szShipperPhoneNumber; ?>" />
                        </span> 
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field"> 
                            <input type="text" class="shipper-fields" name="quickQuoteShipperConsigneeAry[szShipperEmail]" onkeyup="validateQQBookingInformation()" placeholder="Shipper Email" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" id="szShipperEmail" value="<?php echo $szShipperEmail; ?>" > 
                        </span>
                    </div>
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field"> 
                            <input type="text" class="shipper-fields" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szOriginAddress]" id="szOriginAddress" placeholder="Shipper Street Address" value="<?php echo $szOriginAddress; ?>">
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;">  
                        <span class="quote-field-container wd-33">  
                            <input type="text" class="shipper-fields" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szOriginPostcode]" id="szOriginPostcode" placeholder="Shipper Postcode" value="<?php echo $szOriginPostcode; ?>">
                        </span>
                        <span class="quote-field-container wd-33">  
                            <input type="text" class="shipper-fields" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szOriginCity]" id="szOriginCity" placeholder="Shipper City" value="<?php echo $szOriginCity; ?>">
                        </span>
                        <span class="quote-field-container wd-34">  
                            <select name="quickQuoteShipperConsigneeAry[idOriginCountry]" class="shipper-fields" id="idOriginCountry" <?php if($iQuickQuoteTryItOut){?>onchange="update_dail_code(this.value,'ship');"<?php }?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($allCountriesArr))
                                    {
                                       foreach($allCountriesArr as $allCountriesArrs)
                                       {
                                        ?>
                                        <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$idOriginCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                        <?php
                                       }
                                    }
                                ?> 
                            </select>
                        </span>
                    </div>
                </div>
                <div class="quote-field-container quick-quote-consignee">
                    <div class="clearfix email-fields-container quick-quote-title" style="width:100%;"><?php echo t($t_base.'fields/consignee')?></div> 
                     
                    <div class="clearfix email-fields-container" style="width:100%;">
                        <span class="quote-field-container wd-50">
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szConsigneeFirstName]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" id="szConsigneeFirstName" placeholder="Consignee First Name" value="<?php echo $szConsigneeFirstName; ?>">
                        </span>
                        <span class="quote-field-container wd-50"> 
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szConsigneeLastName]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" id="szConsigneeLastName" placeholder="Consignee Last Name" value="<?php echo $szConsigneeLastName; ?>">
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field">
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szConsigneeCompanyName]" id="szConsigneeCompanyName" placeholder="Consignee Company" value="<?php echo $szConsigneeCompanyName; ?>">
                        </span> 
                    </div>
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="quote-field-container wd-35">
                            <select size="1" class="consignee-fields" name="quickQuoteShipperConsigneeAry[idConsigneeDialCode]" id="idConsigneeDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" onchange="validateQQBookingInformation()">
                                <?php
                                   if(!empty($dialUpCodeAry))
                                   {
                                       $usedDialCode = array();
                                       foreach($dialUpCodeAry as $dialUpCodeArys)
                                       {
                                            if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                            {
                                                $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idConsigneeDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                            }
                                       }
                                   }
                            ?>
                            </select> 
                        </span>
                        <span class="quote-field-container wd-65">
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szConsigneePhoneNumber]" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" placeholder="Consignee Phone" id="szConsigneePhoneNumber" value="<?php echo $szConsigneePhoneNumber; ?>" />
                        </span> 
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field"> 
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szConsigneeEmail]" placeholder="Consignee Email" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="Consignee Email" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);" id="szConsigneeEmail" value="<?php echo $szConsigneeEmail; ?>" >
                        </span>
                    </div>
                    <div class="clearfix email-fields-container" style="width:100%;"> 
                        <span class="single-field"> 
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szDestinationAddress]" id="szDestinationAddress" placeholder="Consignee Street Address" value="<?php echo $szDestinationAddress; ?>">
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container" style="width:100%;">  
                        <span class="quote-field-container wd-33">  
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szDestinationPostcode]" id="szDestinationPostcode" placeholder="Consignee Postcode" value="<?php echo $szDestinationPostcode; ?>">
                        </span>
                        <span class="quote-field-container wd-33">  
                            <input type="text" class="consignee-fields" onkeyup="validateQQBookingInformation()" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" name="quickQuoteShipperConsigneeAry[szDestinationCity]" id="szDestinationCity" placeholder="Consignee City" value="<?php echo $szDestinationCity; ?>">
                        </span>
                        <span class="quote-field-container wd-34">  
                            <select name="quickQuoteShipperConsigneeAry[idDestinationCountry]" class="consignee-fields" id="idDestinationCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation()" <?php if($iQuickQuoteTryItOut){?> onchange="update_dail_code(this.value,'con');validateQQBookingInformation();"<?php }else{?>onchange="validateQQBookingInformation()"<?php }?>>
                                <option value="">Select</option>
                                <?php
                                    if(!empty($allCountriesArr))
                                    {
                                       foreach($allCountriesArr as $allCountriesArrs)
                                       {
                                        ?>
                                        <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$idDestinationCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                        <?php
                                       }
                                    }
                                ?> 
                            </select>
                        </span>
                    </div>
                </div>
            </div>
            <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
            <div class="clearfix email-fields-container" style="width:100%;">
                <span class="quote-field-container wd-68">
                    Cargo description<br>
                    <input type="text" name="quickQuoteShipperConsigneeAry[szCargoDescription]" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="Cargo Description" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation();buildQuickQuoteEmail('<?php echo $szBookingType; ?>');" onkeyup="validateQQBookingInformation();" id="szCargoDescription" value="<?php echo $szCargoDescription; ?>" >
                </span>
                <span class="quote-field-container wd-11">
                    <?php echo t($t_base.'fields/insurance')?><br> 
                    <span id="insurance_dropdoen_CREATE_BOOKING" <?php if($szBookingType=='CREATE_BOOKING'){ ?>style="display:block;" <?php } else {?> style="display:none;"<?php }?>>
                        <select name="quickQuoteShipperConsigneeAry[CREATE_BOOKING][iInsuranceIncluded]" id="iInsuranceIncluded" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation();" onchange="toggleInsuranceFields(this.value,1);validateQQBookingInformation();">
                            <option value="">Select</option>
                            <option value="1" <?php echo (($iInsuranceIncluded==1)?'selected':''); ?>>Yes</option>
                            <option value="2" <?php echo (($iInsuranceIncluded==2)?'selected':''); ?>>No</option> 
                        </select>
                    </span>  
                    <span id="insurance_dropdoen_SEND_QUOTE" <?php if($szBookingType!='CREATE_BOOKING'){ ?>style="display:block;" <?php } else {?> style="display:none;"<?php }?>>     
                        <select name="quickQuoteShipperConsigneeAry[SEND_QUOTE][iInsuranceIncluded]" id="iInsuranceIncluded" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_quick_quote(this.form.id,this.id);validateQQBookingInformation();" onchange="toggleInsuranceFields(this.value,1);validateQQBookingInformation();">
                            <option value="1" <?php echo (($iInsuranceIncluded==1)?'selected':''); ?>>Yes</option>
                            <option value="2" <?php echo (($iInsuranceIncluded==2)?'selected':''); ?>>No</option> 
                            <option value="3" <?php echo (($iInsuranceIncluded==3)?'selected':''); ?>>Optional</option> 
                        </select>
                    </span>
                </span>
                <span class="quote-field-container wds-11">
                    Cargo value<br>
                    <select name="quickQuoteShipperConsigneeAry[idGoodsInsuranceCurrency]"  id="idGoodsInsuranceCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();">
                        <option value="">Select</option>
                        <?php
                            if(!empty($currencyAry))
                            {
                               foreach($currencyAry as $currencyArys)
                               {
                                ?>
                                <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idGoodsInsuranceCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                <?php
                               }
                            }
                        ?> 
                    </select> 
                </span> 
                <span class="quote-field-container wds-10">
                    &nbsp;<br>
                    <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();" onkeyup="validateQQBookingInformation();" name="quickQuoteShipperConsigneeAry[fCargoValue]" placeholder="Value" id="fCargoValue" value="<?php echo $fCargoValue; ?>"/>  
                </span> 
            </div>
            <div id="quick-quote-email-box-container">
                <?php 
                    $iQuickQuoteTryItOutFlag=false;
                    if($iQuickQuoteTryItOut)
                    {
                        $iQuickQuoteTryItOutFlag=true;
                    }
                    echo display_quick_quote_email_templates($szBookingType,$bookingDataAry,false,false,$iSuccessFlag,$iInsuranceNotAvailable,$iQuickQuoteTryItOut,$iQuickQuoteTryItOutFlag);
                ?>
            </div>  
        </div> 
        <input type="hidden" name="quickQuoteShipperConsigneeAry[idPackingType]" value="<?php echo $szPackingType; ?>" id="idPackingType_BI"> 
        <input type="hidden" name="quickQuoteShipperConsigneeAry[iPrivateCustomer]" value="<?php echo $iPrivateCustomer; ?>" id="iPrivateCustomer"> 
        <input type="hidden" name="quickQuoteShipperConsigneeAry[szInternalComment]" value="<?php echo $szInternalComment; ?>" id="szInternalComment_BI">
        <input id="szAllServieIDS_BI" type="hidden" value="<?php echo $szAllServieIDS; ?>" name="quickQuoteShipperConsigneeAry[szAllServieIDS]">
    </form>
    <script type="text/javascript"> 
    function updateEmailBody(idLanguage)
    { 
        var szCustomerCurrency = '<?php echo $szCustomerCurrency; ?>';
        var szOriginCountry = '<?php echo $szOriginCountryStr; ?>'; 
        var szDestinationCountry = '<?php echo $szDestinationCountryStr; ?>';
        var szCargoText = $("#szCargoText_"+idLanguage).val();
        var szGoodsInsuranceCurrency = $("#szGoodsInsuranceCurrency").val();
        var fValueOfGoods = $("#fValueOfGoods").val();

        var szEmailBodyText = '';
        var counter = 1;
        var active_counter = 0;
        var iLanguage = '<?php echo $iBookingLanguage; ?>';  
        var active_counter=$('input.quoteActiveCb:checked').length
         console.log("Lang: "+iLanguage);  
        if(active_counter>0)
        {    
            $("#szEmailSubject").removeAttr('disabled','disabled');
            $("#szQuotationBody").removeAttr('disabled','disabled');
            $("#szEmailBody").removeAttr('disabled','disabled');

            if(iLanguage==2)
            {
                var decimal_seprator = ",";
                var thousand_seprator = ".";
            }
            else
            {
                var decimal_seprator = ".";
                var thousand_seprator = ",";
            }
            //number_format(67000, 0, ',', '.'); 
            if(active_counter==1)
            { 
                szEmailBodyText = '<?php echo $szOfferHeading; ?>' + "<br><br>\n\n" ; 
            }
            else
            { 
                szEmailBodyText = '<?php echo $szMultipleOfferHeading ?>' + "<br><br>" ; 
                szEmailBodyText += "\n\n" + '<?php echo $szFromText; ?>' + ': ' + szOriginCountry + ' <br>\n'
                            + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + ' <br>\n'
                            + '<?php echo $szCargoText; ?>' + ': '+ szCargoText + '  <br> \n ';

                szEmailBodyText += "<br><br>\n\n" + '<?php echo $szMultipleOfferHeading2; ?>' + " <br><br>\n\n" ; 
            }

            var number = 0;
            var num_counter = 1;
            for(number =0; number<=hiddenPosition;number++)
            { 
                var cb_flag = false;
                if($("#iActive_"+number).length)
                {
                    cb_flag = $("#iActive_"+number).prop("checked");  
                }
                console.log("checked: "+cb_flag); 
                if(cb_flag)
                {
                    var szTransportMode = '';
                    var idTransportMode = $("#idTransportMode_"+number).val();
                    if(idTransportMode>0)
                    {
                        szTransportMode = transportModeAry[idTransportMode] ;
                    }

                    var iTransitHours = $("#iTransitHours_"+number).val();
                    var fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrency_"+number).val(); 
                    var fTotalVat = parseFloat($("#fTotalVat_"+number).val());
                    var szQuotePriceStr =  szCustomerCurrency+" "+number_format(fTotalPriceCustomerCurrency,'0',decimal_seprator,thousand_seprator)+" all-in" ;
                    var dtOfferValid = $("#dtQuoteValidTo_"+number).val();
                    var fTotalInsuranceCostForBookingCustomerCurrency = $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).val();
                    var fInsuranceValueUptoPrice = $("#fInsuranceValueUpto_"+number).val();

                    fInsuranceValueUptoPrice = parseFloat(fInsuranceValueUptoPrice);
                    var iInsuranceComments = $("#iInsuranceComments_"+number).prop("checked"); 
                    var szForwarderComment = $("#szForwarderComment_"+number).val(); 
                    var szForwarderCommentStr = '';
                    if(iInsuranceComments)
                    {
                        szForwarderCommentStr = szForwarderComment + ' <br>\n' ;
                    }
                    if(fTotalVat>0)
                    {
                        szQuotePriceStr += ", "+'<?php echo $szVatExcludingText; ?> ' + szCustomerCurrency+" "+number_format(fTotalVat,'0',decimal_seprator,thousand_seprator) ;
                    }
                    else
                    {
                        szQuotePriceStr += ". "+'<?php echo $szNoVatOnBookingText; ?>';
                    }

                    var szInsuranceText = '';
                    if(fTotalInsuranceCostForBookingCustomerCurrency>0)
                    {
                        if(fValueOfGoods>0)
                        {
                            szInsuranceText = '<?php echo $szInsuranceText; ?>' + ': ' + " " + szCustomerCurrency+" "+number_format(fTotalInsuranceCostForBookingCustomerCurrency,'0',decimal_seprator,thousand_seprator) + '<?php echo $szInsuranceValueUptoText; ?>'+ " " + szGoodsInsuranceCurrency + " " + number_format(fValueOfGoods,'0',decimal_seprator,thousand_seprator) + ' <br>\n' ;
                        }
                        else
                        {
                            szInsuranceText = ' <?php echo $szInsuranceText; ?>' + ': ' + " " + szCustomerCurrency+" "+number_format(fTotalInsuranceCostForBookingCustomerCurrency,'0',decimal_seprator,thousand_seprator) + ' <br>\n' ;
                        } 
                    } 
                    if(active_counter==1)
                    {
                        szEmailBodyText += '<?php echo $szFromText; ?>' + ': '+szOriginCountry + ' <br>\n'
                            + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + '<br>\n'
                            + '<?php echo $szCargoText; ?>' + ': ' + szCargoText + '<br>\n'
                            +'<?php echo $szModeText; ?>'+': ' + szTransportMode + ' <br>\n'
                            + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + '<?php echo $szDaysText; ?>' + ' <br>\n'
                            + '<?php echo $szPriceText; ?>'+': '+szQuotePriceStr+' <br>\n'
                            + szInsuranceText + szForwarderCommentStr + ''
                            + '<?php echo $OfferValidUntillText; ?>' + ' ' + dtOfferValid+ ' <br><br>\n\nszBookingQuoteLink_'+num_counter+' \n\n';
                    }
                    else
                    {
                        if(counter>1)
                        {
                            szEmailBodyText +="<br><br>";
                        }

                        szEmailBodyText += "<strong><u>" + counter + '. ' + '<?php echo $szOfferText; ?>' + '</u> </strong> <br>\n'
                            + '<?php echo $szModeText; ?>' + ': ' + szTransportMode + ' <br>\n'
                            + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + '<?php echo $szDaysText; ?>' + ' <br>\n'
                            + '<?php echo $szPriceText; ?>' + ': ' + szQuotePriceStr + ' <br>\n'
                            + szInsuranceText + szForwarderCommentStr + ''
                            + '<?php echo $OfferValidUntillText; ?>' + ' ' + dtOfferValid + ' <br><br>  \n\nszBookingQuoteLink_'+num_counter+' \n\n';
                    } 
                    counter++;
                    num_counter++;
                }  
            } 
            var iStandardPricing=$("#iStandardPricing").val();
            if(parseInt(iStandardPricing)==0)
            {
                //szEmailBodyText += "<br><br>";
                $("#szQuotationBody").val(szEmailBodyText);
            }
        }
        else
        {
            $("#szQuotationBody").val(' ');
            $("#szEmailSubject").attr('disabled','disabled');
            $("#szQuotationBody").attr('disabled','disabled');
            $("#szEmailBody").attr('disabled','disabled');

            $("#request_quote_send_button").attr("style","opacity:0.4");
            $("#request_quote_send_button").unbind("click");  
        } 
    }
    </script>
    <?php  
    if(!empty($kQuote->arErrorMessages))
    {  
        $formId = 'quick_quote_shipper_consignee_form'; 
        display_form_validation_error_message($formId,$kQuote->arErrorMessages);
    }
}
function display_quick_quote_email_templates($szBookingType,$bookingDataAry,$iBookingLanguage=false,$szPaymentType=false,$iSuccessFlag=false,$iInsuranceNotAvailable=false,$iQuickQuoteTryItOut=false,$iQuickQuoteTryItOutFlag=false)
{
    $kQuoteNew = new cQuote();
    $kConfig = new cConfig();
    $languageAry = array();
    $languageAry = $kConfig->getLanguageDetails(false,false,false,false,false,true); 
    $paymentTypeAry = array();
    $paymentTypeAry = $kConfig->getAllPaymentTypes();
    if(!empty($_POST['quickQuoteShipperConsigneeAry']))
    { 
        $quickQuoteShipperConsigneeAry = $_POST['quickQuoteShipperConsigneeAry'];
        if($szBookingType=='SEND_QUOTE')
        {  
            $quoteEmailsAry = $_POST['dummyAry'];   
            
            $szEmailSubject = $quickQuoteShipperConsigneeAry['SEND_QUOTE']['szEmailSubject'];
            $szEmailBody = $quickQuoteShipperConsigneeAry['SEND_QUOTE']['szEmailBody'];  
            $iBookingLanguage = $quickQuoteShipperConsigneeAry['SEND_QUOTE']['iBookingLanguage'];  
        }
        else if($szBookingType=='CREATE_BOOKING')
        {
            $quoteEmailsAry = $_POST['dummyAry'];   
            
            $szEmailSubject = $quickQuoteShipperConsigneeAry['CREATE_BOOKING']['szEmailSubject'];
            $szEmailBody = $quickQuoteShipperConsigneeAry['CREATE_BOOKING']['szEmailBody'];  
            $iBookingLanguage = $quickQuoteShipperConsigneeAry['CREATE_BOOKING']['iBookingLanguage'];  
            $szPaymentType = $quickQuoteShipperConsigneeAry['CREATE_BOOKING']['szPaymentType']; 
        }
        $idBooking = $_POST['quickQuoteShipperConsigneeAry']['idBooking'];
        $idUser = $_POST['quickQuoteShipperConsigneeAry']['idUser'];
    }
    else if(!empty($bookingDataAry))
    { 
        $szCustomerCountry = $bookingDataAry['szCustomerCountry'];  
        $idOriginCountry = $bookingDataAry['idShipperCountry'];   
        $idDestinationCountry = $bookingDataAry['idConsigneeCountry'];  
        $iShipperConsignee = $bookingDataAry['iShipperConsignee'];   
        $idBooking = $bookingDataAry['id']; 
        $idUser = $bookingDataAry['idUser'];
        $idBookingFile = $bookingDataAry['idBookingFile'];
       // echo $iBookingLanguage."iBookingLanguage";
        
        if(!$iQuickQuoteTryItOut)
        {
            if($iBookingLanguage<=0)
            {
                if($szCustomerCountry>0)
                {
                    $idLanguageCountry = $szCustomerCountry;
                }
                else
                {
                    if($iShipperConsignee==1)
                    {
                        $idLanguageCountry = $idOriginCountry;
                    }
                    else if($iShipperConsignee==2)
                    {
                        $idLanguageCountry = $idDestinationCountry;
                    }
                }

                $kConfig_new = new cConfig();
                $kConfig_new->loadCountry($idLanguageCountry);
                if($kConfig_new->iDefaultLanguage>0) 
                {
                    $iBookingLanguage = $kConfig_new->iDefaultLanguage;
                }
                else
                {
                    $iBookingLanguage = __LANGUAGE_ID_ENGLISH__;
                } 
            }
        }
        if($szBookingType=='SEND_QUOTE')
        {
            $quoteEmailsAry = array();
            $quoteEmailsAry = $kQuoteNew->createSendQuoteEmail($bookingDataAry,$szBookingType,'','',true,$iQuickQuoteTryItOut);
              
            $szEmailBody = $quoteEmailsAry['szEmailTemplateBody'][$iBookingLanguage];
            $szEmailSubject = $quoteEmailsAry['szEmailTemplateSubject'][$iBookingLanguage];   
        } 
        else if($szBookingType=='CREATE_BOOKING')
        {
            $kWhsSearch = new cWHSSearch();
            
            $ShowPaypalOption=false;
            /*
                $iPaypalStatus = $kWhsSearch->getManageMentVariableByDescription('__PAYPAL_STATUS__');  
                if($iPaypalStatus=='ENABLED') 
             * 
             */
            if($bookingDataAry['szCustomerCountry']>0)
            {
                $idCustomerCountry = $bookingDataAry['szCustomerCountry'];
            }
            else if($bookingDataAry['iShipperConsignee']>0)
            {
                if($bookingDataAry['iShipperConsignee']==1)
                {
                    $idCustomerCountry = $bookingDataAry['idOriginCountry'];
                }
                else
                {
                    $idCustomerCountry = $bookingDataAry['idDestinationCountry'];
                }
            }      
            $ShowPaypalOption = false; 
            if($kQuoteNew->isPaypalAvailableForCountry($idCustomerCountry))
            {  
                $ShowPaypalOption = true; 
            }   
            if(!$iQuickQuoteTryItOut && empty($szPaymentType))
            {
               $szPaymentType = 'UNKNOWN';
            }  
            //@To do
            $quoteEmailsAry = array();
            $quoteEmailsAry = $kQuoteNew->createSendQuoteEmail($bookingDataAry,$szBookingType,$iBookingLanguage,$szPaymentType,false,$iQuickQuoteTryItOut);
               
            $szEmailBody = $quoteEmailsAry['szEmailTemplateBody'][$szPaymentType][$iBookingLanguage];
            $szEmailSubject = $quoteEmailsAry['szEmailTemplateSubject'][$szPaymentType][$iBookingLanguage];  
        }
    }
    $szSpanStyle = 'color:green;';
    if($iInsuranceNotAvailable==1)
    {
        $szSpanStyle = 'color:red;width:180%;';
        $szSuccessMessage = 'Not sent, insurance not available';
    }
    else if($iSuccessFlag==1)
    {
       $szSuccessMessage = 'Sent';
    } 
    $wds_10="wds-10";
    $wds_65="wds-65";
    $wds_15="wds-15";
    $wds_qq_75="wds-65";
    $wds_qq_15="wds-15";
    if($iQuickQuoteTryItOut)
    {
        $wds_10="wds-15";
        $wds_65="wds-55";
        $wds_15="wds-20";
        
        $wds_qq_75="wds-70";
        $wds_qq_15="wds-20";
    }
    
    if($iQuickQuoteTryItOutFlag)
    {
        $iBookingLanguage='';
        $szEmailSubject='';
        $szEmailBody='';
    }
    ?>
    <script type="text/javascript">
        var szEmailBodyId = "szEmailBody_"+'<?php echo $szBookingType; ?>';
        if($("#"+szEmailBodyId).length)
        {
            NiceEditorInstance = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance(szEmailBodyId,{hasPanel : true});
        
            NiceEditorInstance.addEvent('blur', function() {
                
                //check_insurance_required(this.form.id,this.id);
                //validateQQBookingInformation();
            });
            
            $(".nicEdit-main").attr('style','width: 98.8%; margin: 4px; min-height: 300px; overflow: hidden;');
            
        } 
         
    </script>
    <div id="quick_quote_send_quote_container" <?php if($szBookingType=='SEND_QUOTE'){ echo "style='display:block;'"; }else{ echo "style='display:none;'"; } ?>>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div> 
        <div class="clearfix" >
            <span class="accordian" style="text-transform:none; padding: 0px;">Send Quote</span> 
        </div>
        <div class="clearfix email-fields-container wd-100">
            <span class="quote-field-container wd-10">
                Language<br>
                <select name="quickQuoteShipperConsigneeAry[SEND_QUOTE][iBookingLanguage]"  id="iBookingLanguage_SEND_QUOTE" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();" onchange="<?php if($iQuickQuoteTryItOutFlag){?> updateEmailData(this.value,'SEND_QUOTE');<?php }else {?>updateEmailData(this.value,'SEND_QUOTE'); <?php }?>validateQQBookingInformation();">                         
                   <?php if($iQuickQuoteTryItOut){?>
                    <option value="">Select</option>
                    <?php
                   }
                        if(!empty($languageAry))
                        {
                           foreach($languageAry as $languageArys)
                           {
                            ?>
                            <option value="<?php echo $languageArys['id']; ?>" <?php echo (($languageArys['id']==$iBookingLanguage)?'selected':''); ?>><?php echo $languageArys['szName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select>
            </span>
            <span class="quote-field-container <?php echo $wds_qq_75;?>">
                Subject<br>
                <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();" onkeyup="validateQQBookingInformation();" name="quickQuoteShipperConsigneeAry[SEND_QUOTE][szEmailSubject]" placeholder="Subject" id="szEmailSubject_SEND_QUOTE" value="<?php echo $szEmailSubject; ?>"/>  
            </span>
            <span class="quote-field-container <?php echo $wds_qq_15;?>">
                &nbsp;<br>
                <a href="javascript:void(0);" onclick="reload_quick_quote_email_template('<?php echo $szBookingType; ?>','<?php echo $idBooking; ?>')">Reload e-mail template</a>
            </span>
        </div>
        <div class="clearfix email-fields-container wd-100">
            Body<br>
            <textarea name="quickQuoteShipperConsigneeAry[SEND_QUOTE][szEmailBody]" rows="20" id="szEmailBody_SEND_QUOTE" placeholder="Message" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();" onkeyup="validateQQBookingInformation();"><?php echo $szEmailBody; ?></textarea>
        </div>
        <div class="clearfix email-fields-container wd-100" style="text-align: center;">
            <div class="quick-quote-send-button-container">
                <a href="javascript:void(0);" class="button1" id="quick_quote_send_quote_button" style="opacity: 0.4;"><span>Send</span></a> 
                <span style="<?php echo $szSpanStyle; ?>" id="success_message_span">
                    <?php echo $szSuccessMessage; ?> 
                </span> 
            </div> 
        </div>
    </div>  
    <div id="quick_quote_make_booking_container" <?php if($szBookingType=='CREATE_BOOKING'){ echo "style='display:block;'"; }else{ echo "style='display:none;'"; } ?>>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div> 
        <div class="clearfix" >
            <span class="accordian" style="text-transform:none; padding: 10px 0 0;">Send Payment Request</span> 
        </div>
        <div class="clearfix email-fields-container wd-100">
            <span class="quote-field-container wd-10">
                Language<br>
                <select name="quickQuoteShipperConsigneeAry[CREATE_BOOKING][iBookingLanguage]" onchange="reload_quick_quote_email_template('<?php echo $szBookingType; ?>','<?php echo $idBooking; ?>');validateQQBookingInformation();" id="iBookingLanguage_CREATE_BOOKING" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();">                         
                    <?php if($iQuickQuoteTryItOut) {?>
                    <option value="">Select</option>
                    <?php 
                    }
                        if(!empty($languageAry))
                        {
                           foreach($languageAry as $languageArys)
                           {
                            ?>
                            <option value="<?php echo $languageArys['id']; ?>" <?php echo (($languageArys['id']==$iBookingLanguage)?'selected':''); ?>><?php echo $languageArys['szName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select>
            </span>
            <span class="quote-field-container <?=$wds_10;?>">
                Payment type<br>
                <select name="quickQuoteShipperConsigneeAry[CREATE_BOOKING][szPaymentType]" onchange="reload_quick_quote_email_template('<?php echo $szBookingType; ?>','<?php echo $idBooking; ?>');validateQQBookingInformation();" id="szPaymentType_CREATE_BOOKING" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();">                          
                   <?php if($iQuickQuoteTryItOut){?>
                    <option value="">Select</option>
                    <?php 
                   }
                        if(!empty($paymentTypeAry))
                        {
                            foreach($paymentTypeAry as $paymentTypeArys)
                            {
                                if($ShowPaypalOption)
                                {
                                    ?>
                                    <option value="<?php echo $paymentTypeArys['szPaymentCode']; ?>" <?php echo (($szPaymentType==$paymentTypeArys['szPaymentCode'])?'selected':''); ?>><?php echo $paymentTypeArys['szFriendlyName']; ?></option>
                                    <?php 
                                }
                                else
                                {
                                    //If paypal is enabled in customer country then we only allow paypal option to select
                                    if($paymentTypeArys['szPaymentCode']!='PAYPAL')
                                    {
                                        ?>
                                        <option value="<?php echo $paymentTypeArys['szPaymentCode']; ?>" <?php echo (($szPaymentType==$paymentTypeArys['szPaymentCode'])?'selected':''); ?>><?php echo $paymentTypeArys['szFriendlyName']; ?></option>
                                        <?php
                                    }
                                } 
                            }
                        }
                    ?>
                </select>
            </span>
            <span class="quote-field-container <?=$wds_65;?>">
                Subject<br>
                <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();" onkeyup="validateQQBookingInformation();" name="quickQuoteShipperConsigneeAry[CREATE_BOOKING][szEmailSubject]" placeholder="Subject" id="szEmailSubject_CREATE_BOOKING" value="<?php echo $szEmailSubject; ?>"/>  
            </span> 
            <span class="quote-field-container <?=$wds_15;?>">
                &nbsp;<br>
                <a href="javascript:void(0);" onclick="reload_quick_quote_email_template('<?php echo $szBookingType; ?>','<?php echo $idBooking; ?>')">Reload e-mail template</a>
            </span>
        </div>
        <div class="clearfix email-fields-container wd-100">
            Body<br>
            <textarea name="quickQuoteShipperConsigneeAry[CREATE_BOOKING][szEmailBody]" rows="20" id="szEmailBody_CREATE_BOOKING" placeholder="Message" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_insurance_required(this.form.id,this.id);validateQQBookingInformation();"><?php echo $szEmailBody; ?></textarea>
        </div>
        <div class="clearfix email-fields-container wd-100" style="text-align: center;">
            <div class="quick-quote-send-button-container">
                <a href="javascript:void(0);" class="button1" id="quick_quote_create_booking_button" style="opacity: 0.4;"><span>Book & Send</span></a> 
                <span style="<?php echo $szSpanStyle; ?>" id="success_message_span_create_booking">
                    <?php echo $szSuccessMessage; ?> 
                </span>
            </div> 
        </div>
    </div>  
    <input type="hidden" name="quickQuoteShipperConsigneeAry[szBookingType]" value="<?php echo $szBookingType; ?>" id="szBookingType"> 
    <input type="hidden" name="quickQuoteShipperConsigneeAry[idUser]" value="<?php echo $idUser; ?>" id="idUser"> 
    <input type="hidden" name="quickQuoteShipperConsigneeAry[idBooking]" value="<?php echo $idBooking; ?>" id="idBooking">  
    <input type="hidden" name="quickQuoteShipperConsigneeAry[idBookingFile]" value="<?php echo $idBookingFile; ?>" id="idBookingFile">  
    
     
    <span id="" style="display:none;"> 
            <?php  
                if(!empty($languageAry))
                {
                    foreach($languageAry as $languageArys)
                    {
                        $iLanguage = $languageArys['id'];
                        if($szBookingType=='SEND_QUOTE')
                        {
                           ?>
                            <input type="text" name="dummyAry[szEmailTemplateSubject][<?php echo $iLanguage; ?>]" id="szEmailTemplateSubject_SEND_QUOTE_<?php echo $iLanguage ?>" value="<?php echo $quoteEmailsAry['szEmailTemplateSubject'][$iLanguage]; ?>"/>  
                            <textarea name="dummyAry[szEmailTemplateBody][<?php echo $iLanguage; ?>]" rows="1" id="szEmailTemplateBody_SEND_QUOTE_<?php echo $iLanguage ?>"><?php echo $quoteEmailsAry['szEmailTemplateBody'][$iLanguage]; ?></textarea> 
                           <?php
                        } 
                        else
                        {
                            if(!empty($paymentTypeAry))
                            {
                                foreach($paymentTypeAry as $paymentTypeArys)
                                {
                                    $szPaymentCode = $paymentTypeArys['szPaymentCode'];  
                                    ?>
                                    <input type="text" name="dummyAry[szEmailTemplateSubject][<?php echo $szPaymentCode; ?>][<?php echo $iLanguage; ?>]" id="szEmailTemplateSubject_CREATE_BOOKING_<?php echo $szPaymentCode."_".$iLanguage ?>" value="<?php echo $quoteEmailsAry['szEmailTemplateSubject'][$szPaymentCode][$iLanguage]; ?>"/>  
                                    <textarea name="dummyAry[szEmailTemplateBody][<?php echo $szPaymentCode; ?>][<?php echo $iLanguage; ?>]" rows="1" id="szEmailTemplateBody_CREATE_BOOKING_<?php echo $szPaymentCode."_".$iLanguage ?>"><?php echo $quoteEmailsAry['szEmailTemplateBody'][$szPaymentCode][$iLanguage]; ?></textarea> 
                                   <?php
                                }
                            } 
                        }
                    }
                } 
            ?> 
        </span>
    <?php
}

function display_quick_quote_cargo_fields($szShipmentType,$postSearchAry=array())
{  
    if($szShipmentType=='BREAK_BULK')
    {
        if(!empty($_POST['quickQuoteAry'][$szShipmentType]))
        {
            $cargoontainerAry = $_POST['quickQuoteAry'][$szShipmentType];
            if($szShipmentType=='BREAK_BULK')
            {
                $fTotalVolume = $cargoontainerAry['fTotalVolume'];
                $fTotalWeight = $cargoontainerAry['fTotalWeight'];
            }
        } 
        else
        {
            $fTotalVolume = $postSearchAry['fCargoVolume'];
            $fTotalWeight = $postSearchAry['fCargoWeight'];
            
            if($fTotalVolume>0)
            {
                if(__float($fTotalVolume))
                {
                    $fTotalVolume = round_up($fTotalVolume,2); 
                }
                else
                {
                    $fTotalVolume = round($fTotalVolume);
                }
            } 
            if($fTotalWeight>0)
            {
                if(__float($fTotalWeight))
                {
                    $fTotalWeight = round_up($fTotalWeight,2); 
                }
                else
                {
                    $fTotalWeight = round($fTotalWeight);
                }
            } 
        }
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div class="email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-40">
                &nbsp;<br>
                <strong>Break Bulk</strong> 
            </span>
            <span class="quote-field-container wd-10"> 
                Volume<br>
                <input type="text" onkeyup="enableQQGetRatesButton(event);" name="quickQuoteAry[<?php echo $szShipmentType; ?>][fTotalVolume]" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="fTotalVolume_<?php echo $szShipmentType; ?>" value="<?php echo $fTotalVolume; ?>">&nbsp;
            </span> 
            <span class="quote-field-container wds-6">&nbsp;<br>cbm</span>
            <span class="quote-field-container wd-10"> 
                Weight<br>
                <input type="text" onkeyup="enableQQGetRatesButton(event);" name="quickQuoteAry[<?php echo $szShipmentType; ?>][fTotalWeight]" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="fTotalWeight_<?php echo $szShipmentType; ?>" value="<?php echo $fTotalWeight; ?>">
            </span> 
            <span class="quote-field-container wds-5">&nbsp;<br>kg</span>
        </div>
        <?php
        ?>
        <script type="text/javascript">
            validateQuickQuoteCargo('<?php echo $szShipmentType; ?>','<?php echo $number; ?>');
        </script>
        <?php
    }
    else if($szShipmentType=='PARCEL')
    { 
        $szDivKey = "_PARCEL";
        $szChilDivClass = 'class="request-quote-fields"';
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div id="complete-box" class="active transporteca-box">    
            <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
                <?php 
                    $counter = 1; 
                    $parcelPostAry = array();
                    
                    if(!empty($_POST['quickQuoteAry']))
                    {
                        $parcelPostAry = $_POST['quickQuoteAry']['PARCEL'];
                    }
                    else 
                    {
                        //if($postSearchAry['iCargoPackingType']==1) //Parcel Services
                        $kBooking = new cBooking();
                        $parcelPostAry = $kBooking->getCargoDeailsByBookingId($postSearchAry['id'],false,true,$szShipmentType);
                    }
                    if(count($parcelPostAry['iLength'])>0)
                    {  
                        $lengthAry = $parcelPostAry['iLength'];
                        $counter_parcel = 0; 
                        $j = 0;
                        foreach($lengthAry as $key=>$lengthArys)
                        {
                            $counter = $j+1;
                            $parcelCargoAry = array();
                            $parcelCargoAry['iLength'] = sanitize_all_html_input($parcelPostAry['iLength'][$key]); 
                            $parcelCargoAry['iWidth'] = sanitize_all_html_input($parcelPostAry['iWidth'][$key]);
                            $parcelCargoAry['iHeight'] = sanitize_all_html_input($parcelPostAry['iHeight'][$key]);
                            $parcelCargoAry['iQuantity'] = sanitize_all_html_input($parcelPostAry['iQuantity'][$key]);
                            $parcelCargoAry['iWeight'] = sanitize_all_html_input($parcelPostAry['iWeight'][$key]);
                            $parcelCargoAry['idCargoMeasure'] = sanitize_all_html_input($parcelPostAry['idCargoMeasure'][$key]);
                            $parcelCargoAry['idWeightMeasure'] = sanitize_all_html_input($parcelPostAry['idWeightMeasure'][$key]);

                            ?>
                            <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" <?php echo $szChilDivClass; ?>> 
                                <?php
                                    echo display_quick_quote_cargo_details_form($szShipmentType,$counter,$parcelCargoAry);
                                ?>
                            </div>
                            <?php 
                            $j++;
                        } 
                    }
                    else
                    {   
                        ?>
                        <div id="add_booking_quote_container_0<?php echo $szDivKey;?>" <?php echo $szChilDivClass; ?> > 
                            <?php
                               echo display_quick_quote_cargo_details_form($szShipmentType,$counter);
                            ?>
                        </div>
                        <?php 
                    } 
                ?>
                <div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>" style="display:none;"></div>
            </div>    
            <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
            <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" /> 
        </div>
        <?php
    }
    else if($szShipmentType=='PALLET')
    {   
        $szDivKey = "_PALLET";
        $szChilDivClass = 'class="request-quote-fields"';
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div id="complete-box" class="active transporteca-box">    
            <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
                <?php 
                    $counter = 1; 
                    $palletPostAry = array(); 
                    if(!empty($_POST['quickQuoteAry']['PALLET']))
                    {
                        $palletPostAry = $_POST['quickQuoteAry']['PALLET'];
                    }
                    else if($postSearchAry['iCargoPackingType']==2) //Pallet Services
                    {
                        $kBooking = new cBooking();
                        $palletPostAry = array();
                        $palletPostAry = $kBooking->getCargoDeailsByBookingId($postSearchAry['id'],false,true,$szShipmentType);
                    }   
                    if(count($palletPostAry['iLength'])>0)
                    {  
                        $lengthAry = $palletPostAry['iLength'];
                        $counter_parcel = 0; 
                        $j = 0;
                        foreach($lengthAry as $key=>$lengthArys)
                        {
                            $counter = $j+1; 
                            $palletPostArys = array();
                            $palletPostArys['iLength'] = sanitize_all_html_input($palletPostAry['iLength'][$key]); 
                            $palletPostArys['iWidth'] = sanitize_all_html_input($palletPostAry['iWidth'][$key]);
                            $palletPostArys['iHeight'] = sanitize_all_html_input($palletPostAry['iHeight'][$key]);
                            $palletPostArys['iQuantity'] = sanitize_all_html_input($palletPostAry['iQuantity'][$key]);
                            $palletPostArys['iWeight'] = sanitize_all_html_input($palletPostAry['iWeight'][$key]);
                            $palletPostArys['idCargoMeasure'] = sanitize_all_html_input($palletPostAry['idCargoMeasure'][$key]);
                            $palletPostArys['idWeightMeasure'] = sanitize_all_html_input($palletPostAry['idWeightMeasure'][$key]);
 
                            ?>
                            <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" <?php echo $szChilDivClass; ?>> 
                                <?php
                                    echo display_quick_quote_cargo_details_form($szShipmentType,$counter,$palletPostArys);
                                ?>
                            </div>
                            <?php 
                            $j++;
                        } 
                    }
                    else
                    {   
                        ?>
                        <div id="add_booking_quote_container_0<?php echo $szDivKey;?>" <?php echo $szChilDivClass; ?> > 
                            <?php
                               echo display_quick_quote_cargo_details_form($szShipmentType,$counter);
                            ?>
                        </div>
                        <?php 
                    } 
                ?>
                <div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>" style="display:none;"></div>
            </div>   
            
            <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
            <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" />  
        </div>
        <?php
    }
} 
function display_quick_quote_cargo_details_form($szShipmentType,$number,$cargoAry=array(),$postSearchAry=array())
{   
    $t_base = "LandingPage/"; 
    $kConfig = new cConfig();  
    // geting all weight measure 
    $weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

    // geting all cargo measure 
    $cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
    
    if(!empty($cargoAry))
    { 
        $llop_counter = $number - 1 ;
        if($szShipmentType=='PALLET')
        { 
            $szPalletType = $cargoAry['szPalletType'];
        } 
        $fLength = $cargoAry['iLength'];
        $fWidth = $cargoAry['iWidth'];
        $fHeight = $cargoAry['iHeight'];
        $fWeight = $cargoAry['iWeight'];
        $iQuantity = $cargoAry['iQuantity']; 
        $idCargoMeasure = $cargoAry['idCargoMeasure']; 
        $idWeightMeasure = $cargoAry['idWeightMeasure'];  
    }
    else
    {
        if($szShipmentType=='PALLET')
        {
            $szPalletType = 1; //Euro Pallet
            $fLength = 120;
            $fWidth = 80;
        }
    }
    
    $szDottedDiv = ''; 
    $iSearchMiniPage = $szShipmentType; 
    $idSuffix = "_V_".$iSearchMiniPage ;   
    $szClassHide = '';
    
    if($number!=1)
    {
        $szClassHide = ' hide-cargo-heading'; 
    }  
    $iLanguage = 1;
    $palletTypesAry = array();
    $palletTypesAry = $kConfig->getAllPalletTypes(false,$iLanguage);
        
    echo $szDottedDiv; 
    if($szShipmentType=='PARCEL')
    {
        ?>
        <div class="clearfix email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-15">
                <?php if($number==1){?>
                    &nbsp;<br>
                <strong>Packages</strong> 
                <?php } else{ echo "&nbsp;"; } ?> 
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/length')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iLength][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fLength,1); }?>" id= "iLength<?php echo $number."".$idSuffix; ?>" class="first-cargo-fields<?php echo $idSuffix; ?>" onkeyup="enableQQGetRatesButton(event);" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/width')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iWidth][<?=$number?>]" value="<?php if((float)$fWidth>0.00){ echo round_up($fWidth,1);}?>" id= "iWidth<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onkeyup="enableQQGetRatesButton(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/height')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iHeight][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeight<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onkeyup="enableQQGetRatesButton(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select name="quickQuoteAry[<?php echo $szShipmentType;  ?>][idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?php echo $number."".$idSuffix; ?>" onchange="enableQQGetRatesButton(event);" onclick="enableQQGetRatesButton(event);">
                        <?php
                            if(!empty($cargoMeasureAry))
                            {
                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                {
                                    ?>
                                    <option value="<?=$cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/count')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" onkeyup="enableQQGetRatesButton(event);" pattern="[0-9]*" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);"/>
                </span>
            </span>
            <span class="quote-field-container wd-2">&nbsp;</span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/weight')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iWeight][<?=$number?>]" value="<?php if((float)$fWeight>0.00){ echo round_up($fWeight,1);}?>" id= "iWeight<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" onkeyup="enableQQGetRatesButton(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select size="1" id= "idWeightMeasure<?php echo $number."".$idSuffix; ?>" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][idWeightMeasure][<?=$number?>]" onchange="enableQQGetRatesButton(event);" onclick="enableQQGetRatesButton(event);">
                        <?php
                        if(!empty($weightMeasureAry))
                        {
                            foreach($weightMeasureAry as $weightMeasureArys)
                            {
                                ?>
                                <option value="<?=$weightMeasureArys['id']?>" <?php if($idWeightMeasure==$weightMeasureArys['id']){?> selected <?php }?>><?=$weightMeasureArys['szDescription']?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </span>
            </span>
            <span class="quote-field-container wds-12">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">
                    <select size="1" id= "szWeightType<?php echo $number."".$idSuffix; ?>" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][szWeightType][<?=$number?>]" onchange="enableQQGetRatesButton(event);" onclick="enableQQGetRatesButton(event);"> 
                        <option value="WEIGHT_PER_PACKAGE" <?php echo (($szWeightType=='WEIGHT_PER_PACKAGE')?'selected':''); ?>>Per package</option>
                        <option value="WEIGHT_IN_TOTAL" <?php echo (($szWeightType=='WEIGHT_IN_TOTAL')?'selected':''); ?>>In total</option> 
                    </select>
                </span>
            </span>
            <span class="quote-field-container wd-15" style="text-align:right;">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <a href="javascript:void(0);" onclick="add_more_cargo_quick_quote('<?php echo $szShipmentType ?>')" class="cargo-add-button"></a>  
                <a href="javascript:void(0);" id="cargo-line-remove-1-v<?php echo $szShipmentType ?>" class="cargo-remove-button" onclick="remove_cargo_quick_quote('add_booking_quote_container_<?php echo ($number-1)."_".$szShipmentType ?>','<?php echo $szShipmentType; ?>','<?php echo $number; ?>');"></a>
            </span>
            <input type="hidden" name="field_name_key_PARCEL[]" class="field_name_key_PARCEL" value="<?php echo $number."".$idSuffix; ?>">
        </div>
        <?php
    } 
    else if($szShipmentType=='PALLET')
    {
        ?>
        <div class="clearfix email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-9">
                <?php if($number==1){?>
                    &nbsp;<br>
                <strong>Pallets</strong> 
                <?php } else{ echo "&nbsp;"; } ?> 
            </span>
            <span class="quote-field-container wd-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/count')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iQuantity][<?=$number?>]" value="<?=$iQuantity?>" onkeyup="enableQQGetRatesButton(event);" id= "iQuantity<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" pattern="[0-9]*" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-14">
                <span class="input-title<?php echo $szClassHide;?>">Type<br></span>
                <span class="input-fields">  
                    <select name="quickQuoteAry[<?php echo $szShipmentType;  ?>][szPalletType][<?=$number?>]" id= "szPalletType<?php echo $number."".$idSuffix; ?>" onchange="prefill_dimensions(this.value,'<?php echo $number."".$idSuffix; ?>');" onclick="enableQQGetRatesButton(event);">
                        <?php
                            if(!empty($palletTypesAry))
                            {
                                foreach($palletTypesAry as $palletTypesArys)
                                {
                                    ?>
                                    <option value="<?=$palletTypesArys['id']?>" <?php if($szPalletType==$palletTypesArys['id']){?> selected <?php }?>><?php echo $palletTypesArys['szLongName']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            </span> 
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/length')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableQQGetRatesButton(event);" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iLength][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fLength,1); }?>" id= "iLength<?php echo $number."".$idSuffix; ?>" class="first-cargo-fields<?php echo $idSuffix; ?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/width')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableQQGetRatesButton(event);" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iWidth][<?=$number?>]" value="<?php if((float)$fWidth>0.00){ echo round_up($fWidth,1);}?>" id= "iWidth<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/height')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableQQGetRatesButton(event);" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iHeight][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeight<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="Optional" />
                    <input type="hidden"  name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iHeightDefault][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeightDefault<?php echo $number."".$idSuffix; ?>"  />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select name="quickQuoteAry[<?php echo $szShipmentType;  ?>][idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?php echo $number."".$idSuffix; ?>" onchange="enableQQGetRatesButton(event);" onclick="enableQQGetRatesButton(event);">
                        <?php
                            if(!empty($cargoMeasureAry))
                            {
                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                {
                                    ?>
                                    <option value="<?=$cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            </span> 
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/weight')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableQQGetRatesButton(event);" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][iWeight][<?=$number?>]" value="<?php if((float)$fWeight>0.00){ echo round_up($fWeight,1);}?>" id= "iWeight<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);"/>
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select size="1" id= "idWeightMeasure<?php echo $number."".$idSuffix; ?>" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][idWeightMeasure][<?=$number?>]" onchange="enableQQGetRatesButton(event);" onclick="enableQQGetRatesButton(event);">
                        <?php
                        if(!empty($weightMeasureAry))
                        {
                            foreach($weightMeasureAry as $weightMeasureArys)
                            {
                                ?>
                                <option value="<?=$weightMeasureArys['id']?>" <?php if($idWeightMeasure==$weightMeasureArys['id']){?> selected <?php }?>><?=$weightMeasureArys['szDescription']?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </span>
            </span>
            <span class="quote-field-container wds-12">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">
                    <select size="1" id= "szWeightType<?php echo $number."".$idSuffix; ?>" name="quickQuoteAry[<?php echo $szShipmentType;  ?>][szWeightType][<?=$number?>]" onchange="enableQQGetRatesButton(event);" onclick="enableQQGetRatesButton(event);"> 
                        <option value="WEIGHT_PER_PACKAGE" <?php echo (($szWeightType=='WEIGHT_PER_PACKAGE')?'selected':''); ?>>Per pallet</option>
                        <option value="WEIGHT_IN_TOTAL" <?php echo (($szWeightType=='WEIGHT_IN_TOTAL')?'selected':''); ?>>In total</option>  
                    </select>
                </span>
            </span>
            <span class="quote-field-container wd-9" style="text-align:right;">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <a href="javascript:void(0);" onclick="add_more_cargo_quick_quote('<?php echo $szShipmentType ?>')" class="cargo-add-button"></a>  
                <a href="javascript:void(0);" id="cargo-line-remove-1-v<?php echo $szShipmentType ?>" class="cargo-remove-button" onclick="remove_cargo_quick_quote('add_booking_quote_container_<?php echo ($number-1)."_".$szShipmentType ?>','<?php echo $szShipmentType; ?>');"></a>
            </span>
        </div>
        <?php
    }
    ?>
    <script type="text/javascript">
        validateQuickQuoteCargo('<?php echo $szShipmentType; ?>','<?php echo $number; ?>');
    </script>
    <?php
}  
function display_quick_quote_search_result($searchResultAry,$kQuote)
{
    if(!empty($searchResultAry))
    {
//        $_SESSION['searchResultAry'] = $searchResultAry;
//        $_SESSION['token'] = $kQuote->szQuickQuoteToken;
//        $_SESSION['partner_id'] = $kQuote->idQuickQuotePartner;
//        $_SESSION['logs'] = $kQuote->szPriceCalculationLogs; 
    }
    else
    {
//        $searchResultAry = $_SESSION['searchResultAry'];
//        $kQuote->szPriceCalculationLogs = $_SESSION['logs'];
//        $kQuote->szQuickQuoteToken = $_SESSION['token'];
//        $kQuote->idQuickQuotePartner = $_SESSION['partner_id'];  
    }      
    
    $szPriceCalculationLogs = $kQuote->szPriceCalculationLogs;
    $szQuickQuoteToken = $kQuote->szQuickQuoteToken;
    $idQuickQuotePartner = $kQuote->idQuickQuotePartner;
    $idOriginCountry = $kQuote->idOriginCountry;
    $idDestinationCountry = $kQuote->idDestinationCountry;
     
    $kConfig = new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true,false,false,false,true);  
    
    $bDTDTradesFlag = false;
    $kCourierServices = new cCourierServices(); 
    if($kCourierServices->checkFromCountryToCountryExists($idOriginCountry,$idDestinationCountry,true))
    { 
        $bDTDTradesFlag = true;
    }
    
    $kPartner = new cPartner(); 
    cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__;
    cPartner::$szGlobalToken = $szQuickQuoteToken; 
    cPartner::$szApiRequestMode = "QUICK_QUOTE";

    $postSearchAry = $kPartner->getQuickQuoteRequestData(); 
    
    ?>
    <form id="quote_search_result_form" name="quote_search_result_form">
        <table class="quick-quote-results" style="width:100%;">
            <tr>
                <th class="wd-11">Forwarder</th>
                <th class="wd-5">Product</th>
                <th class="wd-6" style="text-align: center;">Pick-up</th>
                <th class="wd-6" style="text-align: center;">Cut-off</th>
                <th class="wd-6" style="text-align: center;">Available</th>
                <th class="wd-6" style="text-align: center;">Delivery</th>
                <th class="wd-6" style="text-align: center;">Time</th>
                <th class="wd-9" style="text-align: center;">Portal Price</th>
                <th class="wd-13" style="text-align:center;">Manual Fee</th>
                <th class="wd-9" style="text-align: center;">Price</th>
                <th class="wd-9" style="text-align: center;">VAT</th>
                <th class="wd-13" style="text-align: center;">Price incl. VAT</th>
                <th class="wd-2"></th>
            </tr>
            <?php
                if(!empty($searchResultAry))
                {
                    foreach($searchResultAry as $searchResultArys)
                    {
                        $idCustomerCurrency = $searchResultArys['idCurrency'];
                        $szCustomerCurrency = $searchResultArys['szCurrency'];
                        $idForwarderCurrency = $searchResultArys['idForwarderCurrency'];
                        
                        $dtPickupDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtCutOffDate'])));
                        $dtDeliveryDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtAvailableDate']))); 
                        
                        $szShipperCity = $postSearchAry['szShipperCity'];
                        $szConsigneeCity = $postSearchAry['szConsigneeCity'];
                            
                        if($searchResultArys['idCourierProvider']>0)
                        {
                            $szProductName = 'Courier';
                            $dtCutOffDate = '-';
                            $dtAvailableDate = '-';
                            $szForwarderCompanyName = $searchResultArys['szForwarderCompanyName'];
                            $fCustomerCurrencyExchangeRate = $searchResultArys['fExchangeRate']; 
                            $idTransportMode = __BOOKING_TRANSPORT_MODE_COURIER__;
                             
                            /*
                            * Pickup date from Door (origin)
                            */
                            $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                            $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                            
                            /*
                            * Delivery date at Door(destination)
                            */
                            $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime); 
                            $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity;
                            
                            $szCollection = 'Yes';
                            $szDelivery = 'Yes';
                                    
                        }
                        else
                        {
                            $szForwarderCompanyName = $searchResultArys['szDisplayName']; 
                            $fCustomerCurrencyExchangeRate = $searchResultArys['fUsdValue']; 
                        
                            $szWareHouseFromCity = $searchResultArys['szFromWHSCity'];
                            $szWareHouseToCity = $searchResultArys['szToWHSCity'];
                             
                            if($bDTDTradesFlag)
                            {
                                $szProductName = 'LTL';
                                $dtCutOffDate = '-';
                                $dtAvailableDate = '-'; 
                                $idTransportMode = __BOOKING_TRANSPORT_MODE_ROAD__;
                                
                                /*
                                * Pickup date from Door (origin)
                                */
                                $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                                
                                /*
                                * Delivery date at Door(destination)
                                */
                                $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                
                                

                            }
                            else
                            {
                                $railTransportAry = array();
                                $railTransportAry['idForwarder'] = $searchResultArys['idForwarder'];
                                $railTransportAry['idFromWarehouse'] = $searchResultArys['idWarehouseFrom'];
                                $railTransportAry['idToWarehouse'] = $searchResultArys['idWarehouseTo'];  
                                 
                                $kServices = new cServices();
                                if($kServices->isRailTransportExists($railTransportAry))
                                {
                                    $idTransportMode = __BOOKING_TRANSPORT_MODE_RAIL__;
                                    $szProductName = 'Rail'; 
                                }
                                else if($searchResultArys['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                                {
                                    $idTransportMode = __BOOKING_TRANSPORT_MODE_AIR__;
                                    $szProductName = 'AIR'; 
                                }
                                else
                                {
                                    $idTransportMode = __BOOKING_TRANSPORT_MODE_SEA__;
                                    $szProductName = 'LCL'; 
                                } 
                                $dtCutOffDate = '-';
                                $dtAvailableDate = '-';
                                $dtPickupDate = "-";
                                $dtDeliveryDate = "-";

                                $dtAvailableDay = ""; 
                                $dtCutOffDay = "";
                                $szPickupDay = "";
                                $szDeliveryDay = "";

                                $dtWhsCutOffDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtWhsCutOff'])));
                                $dtWhsAvailableDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtWhsAvailable'])));
                                if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__)
                                {
                                    /*
                                    * Pickup date from Door (origin)
                                    */
                                    $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                    $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                                    
                                    /*
                                    * Pickup from warehouse(origin)
                                    */
                                    $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                    $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;
                                    
                                    /*
                                    * Delivery to warehouse(destination)
                                    */ 
                                    $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                    $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;
                                    
                                    /*
                                    * Delivery date at Door(destination)
                                    */
                                    $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                    $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                }
                                else if($searchResultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_PTD__)
                                { 
                                    /*
                                    * Pickup from warehouse(origin)
                                    */
                                    $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                    $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;
                                    
                                    /*
                                    * Delivery to warehouse(destination)
                                    */ 
                                    $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                    $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;
                                    
                                    /*
                                    * Delivery date at Door(destination)
                                    */
                                    $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                    $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                }
                                else if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTP__)
                                {
                                    /*
                                    * Pickup date from Door (origin)
                                    */
                                    $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                    $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                                    
                                    /*
                                    * Pickup from warehouse(origin)
                                    */
                                    $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                    $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;
                                    
                                    /*
                                    * Delivery to warehouse(destination)
                                    */ 
                                    $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                    $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;
                                }
                            }
                            
                            if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
                            {
                                $szCollection = 'Yes';
                            }
                            else
                            {
                                $szCollection = 'No';
                            } 
                            if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 2.DTD
                            {
                                $szDelivery = 'Yes';
                            }
                            else
                            {
                                $szDelivery = 'No';
                            }
                        }   
                        
                        $szServiceID = $searchResultArys['unique_id'];
                        $idManualFeeCurrency = $searchResultArys['idManualFeeCurrency']; 
                        $fTotalManualFee = round((float)$searchResultArys['fTotalManualFee'],2);
                        $fTotalManualFeeUSD = round((float)$searchResultArys['fTotalManualFeeUSD'],2);
 
                        $fTotalPriceCustomerCurrency = round((float)$searchResultArys['fDisplayPrice']);
                        if($idManualFeeCurrency == $searchResultArys['idCurrency'])
                        {
                            $fTotalManualFeeCustomerCurrency = $fTotalManualFee;
                        } 
                        else if($searchResultArys['idCurrency']==1)
                        {
                            $fTotalManualFeeCustomerCurrency = $fTotalManualFeeUSD;
                        }
                        else if($fCustomerCurrencyExchangeRate>0)
                        {
                            $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeUSD/$fCustomerCurrencyExchangeRate));
                        } 
                        /*
                        * If Forwarder currency is not same as customer currency then currency mark-up should be applied on this 
                        */
                        if($idForwarderCurrency!=$idCustomerCurrency)
                        {
                            /*
                            * Adding currency mark-up on Manual Fee
                            */
                            $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeCustomerCurrency + ($fTotalManualFeeCustomerCurrency*.025))); 
                        } 

                        $fTotalPriceIncludingManualFee = round((float)($fTotalPriceCustomerCurrency + $fTotalManualFeeCustomerCurrency));

                        $fVATPercentage = $searchResultArys['fVATPercentage'];
                        if($fVATPercentage>0)
                        {
                            $fTotalVat = round((float)($fTotalPriceIncludingManualFee * $searchResultArys['fVATPercentage'] * 0.01),2);
                        } 
                        $fTotalPriceIncludingVat = $fTotalPriceIncludingManualFee + $fTotalVat; 
                        $szSericeIDStr .= $szServiceID."||||"; 
                        
                         

                        $kAdmin = new cAdmin();
                        $idShipperCountry=$searchResultArys['idOriginCountry'];
                        $shipperCountryArr=$kAdmin->countriesView($idShipperCountry,$idShipperCountry);

                        if($szCollection=='Yes' && $shipperCountryArr['iUsePostcodes']==1)
                        {
                            $szShipperPostcodeRequired="Yes";
                        }
                        else
                        {
                            $szShipperPostcodeRequired="No";
                        }

                        $idConsigneeCountry==$searchResultArys['idDestinationCountry'];
                        $kAdmin = new cAdmin();
                        $consigneeCountryArr=$kAdmin->countriesView($idConsigneeCountry,$idConsigneeCountry);
                        if($szDelivery=='Yes' && $consigneeCountryArr['iUsePostcodes']==1)
                        {
                            $szConsigneePostcodeRequired="Yes";
                        }
                        else
                        {
                            $szConsigneePostcodeRequired="No";
                        }
                       
                        
                        ?>
                        <tr>
                            <td><?php echo $szForwarderCompanyName." (".$searchResultArys['szForwarderAlias'].")"; ?></td>
                            <td><?php echo $szProductName; ?></td>
                            <td title="<?php echo $szPickupDay; ?>" style="text-align: center;"><?php echo $dtPickupDate; ?></td>
                            <td title="<?php echo $dtCutOffDay; ?>" style="text-align: center;"><?php echo $dtCutOffDate;?></td>
                            <td title="<?php echo $dtAvailableDay; ?>" style="text-align: center;"><?php echo $dtAvailableDate;?></td>
                            <td title="<?php echo $szDeliveryDay; ?>" style="text-align: center;"><?php echo $dtDeliveryDate; ?></td>
                            <td style="text-align: center;"><?php echo $searchResultArys['iDaysBetween']." days"?></td>
                            <td style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$searchResultArys['fDisplayPrice']); ?></td>
                            <td>
                               <div class="clearfix email-fields-container" style="width:100%;">
                                   <span class="quote-field-container wd-50">
                                       <select id="idManualFeeCurrency_<?php echo $szServiceID; ?>" name="quoteResultAry[idManualFeeCurrency][<?php echo $szServiceID; ?>]" onchange="calculate_prices('<?php echo $szServiceID; ?>');prefill_selected_values('<?php echo $szServiceID; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"> 
                                            <?php
                                                if(!empty($currencyAry))
                                                {
                                                    foreach($currencyAry as $currencyArys)
                                                    {?>
                                                        <option value="<?php echo $currencyArys['id'];?>" <?php echo (($currencyArys['id']==$idManualFeeCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency'];?></option>
                                                    <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                   </span>
                                   <span class="quote-field-container wds-50">
                                       <input id="fTotalManualFee_<?php echo $szServiceID; ?>" name="quoteResultAry[fTotalManualFee]" onkeyup="calculate_prices('<?php echo $szServiceID; ?>');prefill_selected_values('<?php echo $szServiceID; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" type="text" value="<?php echo $fTotalManualFee; ?>" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                                   </span>  
                                   <input type="hidden" name="quoteResultAry[fTotalPriceCustomerCurrency][<?php echo $szServiceID; ?>]" id="fTotalPriceCustomerCurrency_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceCustomerCurrency; ?>">                                   
                                   <input type="hidden" name="quoteResultAry[fVATPercentage][<?php echo $szServiceID; ?>]" id="fVATPercentage_<?php echo $szServiceID; ?>" value="<?php echo $fVATPercentage; ?>">
                                   <input type="hidden" name="quoteResultAry[fTotalPriceIncludingManualFee][<?php echo $szServiceID; ?>]" id="fTotalPriceIncludingManualFee_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceIncludingManualFee; ?>">
                                   <input type="hidden" name="quoteResultAry[fTotalPriceIncludingVat][<?php echo $szServiceID; ?>]" id="fTotalPriceIncludingVat_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceIncludingVat; ?>">
                                   <input type="hidden" name="quoteResultAry[fTotalVat][<?php echo $szServiceID; ?>]" id="fTotalVat_<?php echo $szServiceID; ?>" value="<?php echo $fTotalVat; ?>">
                                   <input type="hidden" name="quoteResultAry[idShipmentType][<?php echo $szServiceID; ?>]" id="idShipmentType_<?php echo $szServiceID; ?>" value="<?php echo $searchResultArys['idShipmentType']; ?>"> 
                                   <input type="hidden" name="quoteResultAry[idTransportMode][<?php echo $szServiceID; ?>]" id="idTransportMode_<?php echo $szServiceID; ?>" value="<?php echo $idTransportMode; ?>"> 
                                   <input type="hidden" name="quoteResultAry[idForwarderCurrency][<?php echo $szServiceID; ?>]" id="idForwarderCurrency_<?php echo $szServiceID; ?>" value="<?php echo $idForwarderCurrency; ?>"> 
                                   
                                   <input type="hidden" name="quoteResultAry[szConsigneePostcodeRequiredFlag][<?php echo $szServiceID; ?>]" id="szConsigneePostcodeRequiredFlag_<?php echo $szServiceID; ?>" value="<?php echo $szConsigneePostcodeRequired; ?>"> 
                                   <input type="hidden" name="quoteResultAry[szShipperPostcodeRequiredFlag][<?php echo $szServiceID; ?>]" id="szShipperPostcodeRequiredFlag_<?php echo $szServiceID; ?>" value="<?php echo $szShipperPostcodeRequired; ?>"> 
                                   <input type="hidden" name="quoteResultAry[szCollectionFlag][<?php echo $szServiceID; ?>]" id="szCollectionFlag_<?php echo $szServiceID; ?>" value="<?php echo $szCollection; ?>"> 
                                   <input type="hidden" name="quoteResultAry[szDeliveryFlag][<?php echo $szServiceID; ?>]" id="szDeliveryFlag_<?php echo $szServiceID; ?>" value="<?php echo $szDelivery; ?>"> 
                                   
                               </div>
                            </td>
                            <td id="fTotalPriceIncludingManualFee_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$fTotalPriceIncludingManualFee); ?></td>
                            <td id="fTotalVat_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$fTotalVat,2); ?></td>
                            <td id="fTotalPriceIncludingVat_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$fTotalPriceIncludingVat,2); ?></td>
                            <td>
                                <input type="checkbox" name="quoteResultAry[szServiceIs][]" onclick="enableBookingButtons(this.id,'<?php echo $szServiceID; ?>');" id="szServiceIs_<?php echo $szServiceID; ?>" class="quick-quote-service-cb" value="<?php echo $szServiceID; ?>" />
                                <?php
                                    if(!empty($searchResultArys['szCustomerPriceLogs']) && __ENVIRONMENT__!='LIVE')
                                    {    
                                        echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation_".$szServiceID."')>.</a> <div id='courier_service_calculation_".$szServiceID."' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation_".$szServiceID."') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                                            </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$searchResultArys['szCustomerPriceLogs']." </div></div> </div></div>";                     
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php 
                    }
                }
                else
                {
                    ?>
                    <tr>
                        <td colspan="13" style="text-align:center;"><strong>There are no services online for this search</strong></td>
                    </tr>
                    <?php
                }
            ?>
        </table>    
        <input type="hidden" name="quoteResultAry[idPackingType]" value="<?php echo $szPackingType; ?>" id="idPackingType">
        <input type="hidden" name="quoteResultAry[szAllServieIDS]" value="<?php echo $szSericeIDStr; ?>" id="szAllServieIDS">
        <input type="hidden" name="quoteResultAry[szToken]" value="<?php echo $szQuickQuoteToken; ?>" id="szToken">
        <input type="hidden" name="quoteResultAry[szInternalComment]" value="<?php echo $szInternalComment; ?>" id="szInternalComment"> 
    </form> 
    <div class="clearfix email-fields-container quick-quote-button-container" id="quick_quote_service_button_container" style="width:100%;text-align: center;padding: 10px 0;"> 
        <a id="quote_result_send_quote_button" class="button1 quick-quote-action-button" style="opacity:0.4;" href="javascript:void(0)">
            <span>Send Quote</span>
        </a> 
        <a id="quote_result_make_booking_button" class="button1 quick-quote-action-button" style="opacity:0.4;" href="javascript:void(0)">
            <span>MAKE BOOKING</span>
        </a>
        <a id="quote_result_send_quote_button" class="button1" onclick="submitSearchResultForm('quote_search_result_form','CONVERT_RFQ')" href="javascript:void(0)">
            <span>CONVERT RFQ</span>
        </a>  
    </div>  
    <?php    
        if(!empty($szPriceCalculationLogs))
        {  
            echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$szPriceCalculationLogs." </div></div> </div></div>";                    

        } 
    ?> 
    <script type="text/javascript">
        var idCustomerCurrency = '<?php echo $idCustomerCurrency ?>';
        var fCustomerCurrencyExchangeRate = '<?php echo $fCustomerCurrencyExchangeRate ?>';
        var szCustomerCurrency = '<?php echo $szCustomerCurrency ?>';
        var currencyExchangeAry = new Array();
        <?php
            if(!empty($currencyAry))
            {
                foreach($currencyAry as $currencyArys)
                {
                    ?>
                    currencyExchangeAry[<?php echo $currencyArys['id']; ?>] = '<?php echo $currencyArys['fUsdValue']?>';
                    <?php
                }
            }
        ?> 
        function calculate_prices(szServiceID)
        {
            var idManualCurrency = $("#idManualFeeCurrency_"+szServiceID).val();
            var fTotalManualFee = $("#fTotalManualFee_"+szServiceID).val();
            var fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrency_"+szServiceID).val();
            var fVATPercentage = $("#fVATPercentage_"+szServiceID).val();
            var idForwarderCurrency = $("#idForwarderCurrency_"+szServiceID).val();
            
            var fManualFeeExchangeRate = getExchangeRate(idManualCurrency); 
            var fManualFeeUSD = fTotalManualFee * fManualFeeExchangeRate;
            var fTotalManualFeeCustomerCurrency = 0;
            var fTotalVat = 0;
            var fTotalPriceIncludingVat =0;
             
            if(idManualCurrency == idCustomerCurrency)
            {
                fTotalManualFeeCustomerCurrency = fTotalManualFee;
            } 
            else if(idManualCurrency==1)
            {
                fTotalManualFeeCustomerCurrency = fManualFeeUSD;
            }
            else if(fCustomerCurrencyExchangeRate>0)
            {
                fTotalManualFeeCustomerCurrency = Math.round(fManualFeeUSD/fCustomerCurrencyExchangeRate);
            }     
            if(idCustomerCurrency != idForwarderCurrency)
            {
                var fCurrencyMarkup = Math.round(fTotalManualFeeCustomerCurrency * 0.025);
                fTotalManualFeeCustomerCurrency = parseFloat(fTotalManualFeeCustomerCurrency);
                fTotalManualFeeCustomerCurrency = Math.round(fTotalManualFeeCustomerCurrency + fCurrencyMarkup);
            }
            console.log("Manual Fee: "+fTotalManualFeeCustomerCurrency+ " Exchange: "+fManualFeeExchangeRate+" Fee USD: "+fManualFeeUSD+" Cust Exch: "+fCustomerCurrencyExchangeRate);
            
            fTotalPriceCustomerCurrency = parseInt(fTotalPriceCustomerCurrency);
            fTotalManualFeeCustomerCurrency = parseFloat(fTotalManualFeeCustomerCurrency);
            
            var fTotalPriceIncludingManualFee = Math.round((fTotalPriceCustomerCurrency + fTotalManualFeeCustomerCurrency)); 
            
            if(fVATPercentage>0)
            {
                fTotalVat = (fTotalPriceIncludingManualFee * fVATPercentage * 0.01);
                fTotalVat = fTotalVat.toFixed(2);
                fTotalVat = parseFloat(fTotalVat);
            }  
            fTotalPriceIncludingVat = fTotalPriceIncludingManualFee + fTotalVat;
            
            $("#fTotalPriceIncludingManualFee_"+szServiceID).val(fTotalPriceIncludingManualFee);
            $("#fTotalPriceIncludingVat_"+szServiceID).val(fTotalPriceIncludingVat);
            $("#fTotalVat_"+szServiceID).val(fTotalVat);
            
            fTotalVat = number_format(fTotalVat,'2','.',',');
            fTotalPriceIncludingVat = number_format(fTotalPriceIncludingVat,'2','.',',');
            fTotalPriceIncludingManualFee = number_format(fTotalPriceIncludingManualFee,'','.',',');
            
            $("#fTotalPriceIncludingManualFee_container_"+szServiceID).html(szCustomerCurrency +" " +fTotalPriceIncludingManualFee);
            $("#fTotalVat_container_"+szServiceID).html(szCustomerCurrency +" " +fTotalVat);
            $("#fTotalPriceIncludingVat_container_"+szServiceID).html(szCustomerCurrency +" " +fTotalPriceIncludingVat);
        }
        
        function getExchangeRate(idCurrency)
        {
            if(idCurrency>0)
            {
                if(idCurrency==1)
                {
                    return 1;
                }
                else
                {
                    return currencyExchangeAry[idCurrency];
                } 
            }
        }
    </script>  
    <?php  
}

function display_convert_rfq_confirmation_popup($data,$kQuote)
{
    $shipingTypesAry = array();
    $shipingTypesAry = $kQuote->shipingTypesAry;
    $szInterCommentsCargoStr = $kQuote->szInterCommentsCargoStr;
    
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup"> 
            <h3>Convert to RFQ</h3>
            <p>This will convert this quick quote information to an RFQ.</p><br> 
            <?php
                if(count($shipingTypesAry)>1)
                {
                    ?>
                    <div class="clearfix email-fields-container" style="width:100%;">
                        You have selected service with differentpacking types. An RFQ can maximum have one packing type. Select which one to use (Pricing for all lines will be transferred):
                    </div>
                    <div class="clearfix email-fields-container" style="width:100%;">
                        <select name="convertRfqAry[idPackingType]" id="idPackingType_popup">
                            <?php
                                foreach($shipingTypesAry as $shipingTypesArys)
                                {
                                    if($shipingTypesArys==1)
                                    {
                                        $szShipmentName = 'Package';
                                    }
                                    else if($shipingTypesArys==2)
                                    {
                                        $szShipmentName = 'Pallet';
                                    }
                                    else if($shipingTypesArys==3)
                                    {
                                        $szShipmentName = 'Break Bulk';
                                    }
                                    ?>
                                    <option value="<?php echo $shipingTypesArys; ?>"><?php echo $szShipmentName; ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                    <?php 
                }
                else
                {
                    $idPackingType = $shipingTypesAry[0];
                    ?>
                    <select name="convertRfqAry[idPackingType]" id="idPackingType_popup" style="display:none;">
                        <option value="<?php echo $idPackingType; ?>"><?php echo $idPackingType; ?></option>
                    </select>
                    <?php
                }
            ?>
            <div class="clearfix email-fields-container" style="width:100%;">
                Add internal comments, if relevant.<br>
                <textarea name="convertRfqAry[szInternalComment]" id="szInternalComment_popup" rows="8"><?php echo $szInterCommentsCargoStr; ?></textarea>
            </div>
            <div class="clearfix email-fields-container" style="width:100%;text-align:center;"> 
                <a class="button2" onclick="showHide('quick_quote_booking_popup')" href="javascript:void(0)">
                    <span>Cancel</span>
                </a> 
                <a id="quote_quote_convert_rfq_confirm_button" class="button1" onclick="submitSearchResultForm('quote_search_result_form','CONVERT_RFQ_CONFIRM')" href="javascript:void(0)">
                    <span>Confirm</span>
                </a>  
            </div>
        </div>
    </div>
    <?php
} 

function displayTaskManagerForm()
{
    ?>
    <div class="widget-box" id="recent-box" ng-controller="tasksController">
        <div class="widget-header header-color-blue">
            <div class="row">
                <div class="col-sm-6">
                    <h4 class="bigger lighter" style="float:left;">
                        Task Manager Application
                    </h4>
                </div>
                <div class="col-sm-3">
                    <button ng-click="addNewClicked=!addNewClicked;" class="btn btn-sm btn-danger header-elements-margin"><i class="glyphicon  glyphicon-plus"></i>&nbsp;Add New Task</button>
                </div>
                <div class="col-sm-3"> 
                    <input type="text" ng-model="filterTask" class="form-control search header-elements-margin" placeholder="Filter Tasks"> 
                </div>
            </div>
        </div> 
        <div class="widget-body ">
            <form ng-init="addNewClicked=false;" ng-if="addNewClicked" id="newTaskForm" class="add-task">
                <div class="form-actions">
                    <div class="input-group">
                        <input type="text" class="form-control" name="comment" ng-model="taskInput" placeholder="Add New Task" ng-focus="addNewClicked">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit" ng-click="addTask(taskInput)"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add New Task</button>
                        </div>
                    </div>
                </div>
            </form> 
            <div id="task-manager-listing-container">
                <?php echo displayTaskManagerListing(); ?>
            </div> 
        </div> 
    </div>
    <?php
}

function displayTaskManagerListing()
{
   $kDeveloper = new cDeveloper();
   $taskManagerAry = array();
   $taskManagerAry = $kDeveloper->getAllTaskLists();
           
   print_R($taskManagerAry);
   $json_task_data = json_encode($taskManagerAry);
   echo "<br><br>Json: ".$json_task_data;
    ?> 
    <script>
        
    //Define an angular module for our app
    var app = angular.module('myApp', []);

    app.controller('tasksController', function($scope, $http) {  
        
        function getTaskData()
        { 
            return '<?php echo $json_task_data; ?>';
        }
        function getTask(){   
            $scope.tasks = getTaskData();
        };  
        getTask();
        console.log($scope.tasks);
        $scope.addTask = function (task) {
          $http.post("ajax/addTask.php?task="+task).success(function(data){
              getTask();
              $scope.taskInput = "";
            });
        };
        $scope.deleteTask = function (task) {
          if(confirm("Are you sure to delete this line?")){
          $http.post("ajax/deleteTask.php?taskID="+task).success(function(data){
              getTask();
            });
          }
        };

        $scope.toggleStatus = function(item, status, task) {
          if(status=='2'){status='0';}else{status='2';}
            $http.post("ajax/updateTask.php?taskID="+item+"&status="+status).success(function(data){
              getTask();
            });
        }; 
    }); 

    </script>
    
    <div class="task"> 
        <label class="checkbox" ng-repeat="task in tasks | filter : filterTask">
            <input  type="checkbox" value="1" ng-checked="task.iCompleted==1" ng-click="toggleStatus(task.id,task.iCompleted)"/> 
            <span ng-class="{strike:task.iCompleted==1}">{{task.szTaskName}} [{{task.id}}]</span>
            <a ng-click="deleteTask(task.id)" class="pull-right">delete</a>
        </label>
    </div>
    <?php
}