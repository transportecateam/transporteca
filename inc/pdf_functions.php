<?php
if(!isset($_SESSION))
{
    session_start();
} 
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/booking_functions.php" );
require_once( __APP_PATH__ . "/inc/search_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH_CLASSES__ . "/booking.class.php"); 
require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php');
require_once( __APP_PATH__ . "/inc/pdf_functions_new.php" );
 
function getForwarderBookingConfirmationPdfFile($idBooking,$flag=false,$utf8Flag=false,$iHideHandlingFeeTotal=false)
{	  
    if((int)$idBooking>0)
    {	
        require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');	
        $kConfig = new cConfig();
        $kBooking = new cBooking();
        $kUser = new cUser();
        $kForwarderContact = new cForwarderContact();

        $kWHSSearch = new cWHSSearch();

        $markupPricing=$kWHSSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__');

        $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
        /*
         * 20170129 Financial Revamp – step 1 Removed 31-Jan-2017
         */
        /*if($iHideHandlingFeeTotal && $bookingDataArr['iHandlingFeeApplicable']==0 && (int)$bookingDataArr['fTotalForwarderManualFee']==0.00)
        {
            $iHideHandlingFeeTotal=false;
        }*/
        $iHideHandlingFeeTotal=false;
        if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
        }
        if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
        } 
        if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
        }
        if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
        }
        
        $bDisplayVatWithoutCurrencyMarkup = false;
        /*
        * for all the booking which will be created after 29-08-2016 we are displaying ROE of VAT = ROE * 1.025
        */
        $dtBookingCreatedDate = date('Y-m-d',strtotime($bookingDataArr['dtBookingConfirmed']));
        $dtDateTime = __SHOW_VAT_WITHOUT_MARKUP__; 
        if(strtotime($dtBookingCreatedDate) >= strtotime($dtDateTime))
        { 
            $bDisplayVatWithoutCurrencyMarkup = true;
        } 
        
        $idForwarder = $bookingDataArr['idForwarder'];
        $kUser->getUserDetails($bookingDataArr['idUser']);
        $kForwarderContact->load($_SESSION['forwarder_user_id']);
        //$serviceTypeAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$bookingDataArr['idServiceType']);
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
        $serviceTypeAry = $configLangArr[$bookingDataArr['idServiceType']];
        
        $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
        $szHandlingFeeDetailsString='';
        $szPrivateCustomerFeeDetailsString = '';
        $idRoleAry = array();
        $forwarderContactAry = array();
        $idRoleAry[0]=1 ; //Administrator User
        $forwarderContactAry = $kForwarderContact->getAllForwardersContact($idForwarder,$idRoleAry);
        $forwarderAdminUserAry = array();
        if(!empty($forwarderContactAry))
        {
            foreach($forwarderContactAry as $forwarderContactArys)
            {
                $forwarderAdminUserAry[$ctr_contc] = $forwarderContactArys['szEmail'];
                $ctr_contc++;
            }
        }
        $szForwarderAdministratorEmails = '';
        if(!empty($forwarderAdminUserAry))
        {
            $forwarderRetAry = array(); 
            $forwarderRetAry = format_fowarder_emails($forwarderAdminUserAry); 
            $szForwarderAdministratorEmails = $forwarderRetAry[1]; 
        } 
        $forwarderTransactionDetailAry = $kBooking->getTransionDetailsByBookingId($idBooking);

        $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($bookingDataArr['id'],true);
        $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);

        $cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
        $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',','); 

        /*
        * Building cargo string for different types of bookings
        */
        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__ && (int)$bookingDataArr['idServiceProvider']>0)
        {
            $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,true); 
            $szCargoFullDetails='';
            $total=count($cargoDetailArr);
            if(!empty($cargoDetailArr))
            { 
                $ctr=0;
                $totalQuantity=0;
                $kCourierServices = new cCourierServices(); 
                $packingTypeArr = array();
                $packingTypeArr = $kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1); 
                
                foreach($cargoDetailArr as $cargoDetailArrs)
                { 
                    $totalQuantity = $totalQuantity + $cargoDetailArrs['iColli'];
                    $t=$total-1;
                    if((int)$cargoDetailArrs['iQuantity']>1)
                    {
                        if($cargoDetailArrs['szCommodity']!='')
                        {
                            $szPackingSingle = $cargoDetailArrs['szCommodity'];
                        }
                        else
                        {
                            $szPackingSingle = $packingTypeArr[0]['szPacking'];
                        }
                        $quantityText= number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($szPackingSingle);
                    }
                    else
                    {
                        if($cargoDetailArrs['szCommodity']!='')
                        {
                            $szPackingSingle = $cargoDetailArrs['szCommodity'];
                        }
                        else
                        {
                            if(!empty($packingTypeArr[0]['szSingle']))
                            {
                                $szPackingSingle = $packingTypeArr[0]['szSingle'];
                            }
                            else
                            {
                                $szPackingSingle = $packingTypeArr[0]['szPackingSingle'];
                            }
                        }
                        $quantityText= number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($szPackingSingle);
                    } 
                    if($ctr==0)
                    {
                        $szCargoFullDetails .=$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)." ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                    }
                    else
                    {
                        $szCargoFullDetails .="<br />".$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."  ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                    }
                    if(!empty($cargoDetailArrs['szSellerReference']))
                    {
                        $szCargoFullDetails .= " (".$cargoDetailArrs['szSellerReference'].")";
                    }
                    $szCargoFullDetails .= ", ".$cargoDetailArrs['iColli']." colli "; 
                    ++$ctr;	
                } 
                $textPackingText='';
                if($totalQuantity>1)
                {
                    $textPacking=strtolower($packingTypeArr[0]['szPacking']);
                    
                    $textPackingText=number_format($totalQuantity)." ".strtolower($textPacking).", ";
                } 
                else if($totalQuantity==1)
                {
                    if(!empty($packingTypeArr[0]['szSingle']))
                    {
                        $szPackingSingle = $packingTypeArr[0]['szSingle'];
                    }
                    else
                    {
                        $szPackingSingle = $packingTypeArr[0]['szPackingSingle'];
                    }
                    $textPackingText=number_format($totalQuantity)." ".strtolower($szPackingSingle).", ";   
                } 
                $szCargoFullDetails .= "<br />Total: ".$textPackingText."".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity;
            } 
        }
        else if($bookingDataArr['iSearchMiniVersion']==7 || $bookingDataArr['iSearchMiniVersion']==8 || $bookingDataArr['iSearchMiniVersion']==9) //Voga Landing Page
        {
            
            $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,true); 
            
            
            $szCargoFullDetails='';
            $total=count($cargoDetailArr);
            if(!empty($cargoDetailArr))
            { 
                $ctr=0;
                $totalQuantity=0;
                foreach($cargoDetailArr as $cargoDetailArrs)
                {   
                    $totalQuantity = $totalQuantity+$cargoDetailArrs['iColli'];
                    $szVogaPackingType = ($cargoDetailArrs['szCommodity']);
                    $t=$total-1;  
                    $quantityText = number_format((int)$cargoDetailArrs['iQuantity'])." ".$szVogaPackingType;
                   
                    if($ctr==0)
                    {
                        
                        $szCargoFullDetails .=$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)." ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                        
                    }
                    else
                    {
                        
                        $szCargoFullDetails .="<br />".$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."  ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                        
                    }
                    
                    if(!empty($cargoDetailArrs['szSellerReference']))
                    {
                        $szCargoFullDetails .= " (".$cargoDetailArrs['szSellerReference'].")";
                    }
                    $szCargoFullDetails .= ", ".$cargoDetailArrs['iColli']." colli ";
                    
                    ++$ctr;	
                }   
            } 
            $szVogaPackingType = 'colli';
            $totalQuantityText='';
            $totalQuantity=$bookingDataArr['iNumColli'];
            if((int)$totalQuantity>0)
            {
               $totalQuantityText= number_format($totalQuantity)." ".$szVogaPackingType.", ";
            }
            $szCargoFullDetails .= "<br />Total: ".$totalQuantityText."".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity;
        }
        else
        {
            $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,true); 
            if(!empty($cargoDetailArr) && $bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__)
            { 
                $szCargoFullDetails='';
                $total=count($cargoDetailArr);
                if(!empty($cargoDetailArr))
                { 
                    $ctr=0;
                    $totalQuantity=0;  
                    foreach($cargoDetailArr as $cargoDetailArrs)
                    { 
                        $totalQuantity = $totalQuantity + $cargoDetailArrs['iColli'];
                        $t=$total-1;
                        $szPackingSingle = $cargoDetailArrs['szCommodity'];
                        $quantityText= number_format((int)$cargoDetailArrs['iQuantity'])." ".($szPackingSingle); 
                        if($ctr==0)
                        {
                            $fHeightText='';
                            if((float)$cargoDetailArrs['fHeight']==0.00)
                            {
                                $fHeightText="max ";
                                $cargoDetailArrs['fHeight']=__HEIGHT_PER__PALLET_COLI__;
                            }
                            $szCargoFullDetails .=$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".$fHeightText."".round_up($cargoDetailArrs['fHeight'],1)." ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                        }
                        else
                        {
                            $fHeightText='';
                            if((float)$cargoDetailArrs['fHeight']==0.00)
                            {
                                $fHeightText="max ";
                                $cargoDetailArrs['fHeight']=__HEIGHT_PER__PALLET_COLI__;
                            }
                            $szCargoFullDetails .="<br />".$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".$fHeightText."".round_up($cargoDetailArrs['fHeight'],1)."  ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                        }
                        if(!empty($cargoDetailArrs['szSellerReference']))
                        {
                            $szCargoFullDetails .= " (".$cargoDetailArrs['szSellerReference'].")";
                        }
                        $szCargoFullDetails .= ", ".$cargoDetailArrs['iColli']." colli "; 
                        ++$ctr;	
                    } 
                    $totalQuantityText='';
                    if((int)$totalQuantity>0)
                    {
                       $totalQuantityText= number_format($totalQuantity)." colli, ";
                    }
                    $szCargoFullDetails .= "<br />Total: ".$totalQuantityText."".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity;
                } 
            }
            else
            {
                $showColliTextFlag=false;
                if((int)$bookingDataArr['iNumColli']>0)
                {
                    $showColliTextFlag=true;
                }
                if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__ || $showColliTextFlag)
                {
                    $szCargoFullDetails = $cargo_volume." cbm, ".$cargo_weight." kg, ".$bookingDataArr['iNumColli']." colli ".$szCargoCommodity; 
                }
                else
                {
                    $szCargoFullDetails = $cargo_volume." cbm, ".$cargo_weight." kg, ".$szCargoCommodity;
                } 
            } 
        } 
        if(trim($bookingDataArr['iPaymentType'])==__TRANSPORTECA_PAYMENT_TYPE_1__ || trim($bookingDataArr['iPaymentType'])=="Zooz")
        {
            $iPaymentType="Credit Card";
        }
        else if(trim($bookingDataArr['iPaymentType'])==__TRANSPORTECA_PAYMENT_TYPE_3__)
        {
            $iPaymentType="Bank Transfer";
        }
        else
        {
            $iPaymentType=$bookingDataArr['iPaymentType'];
        } 
        if($flag==true)
        {
            $body_info='style="background:#000;text-align:center;"';
            $image_url=__MAIN_SITE_HOME_PAGE_URL__;
            $align='align="center"';
            $printStringStart='<p style="text-align:right;margin:5px auto 10px;width:760px;"><a href='.__BASE_URL__.'/downloadForwarderBooking/'.$idBooking.'/ target="_blank"
            style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>&nbsp;&nbsp;&nbsp;
            <a href="javascript:void(0)" onclick="PrintDiv();" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a></p>
            <div id="viewForwarderConfirmation" style="background:#fff;margin:auto;padding:10px 20px;width:720px;text-align:left;">';
            $printStringEnd='</div>';
            $script='<script>
            function PrintDiv()
            {    
                  var divToPrint = document.getElementById("viewForwarderConfirmation");
                  var popupWin = window.open("", "_blank", "width=900,height=700");
                  popupWin.document.open();
                  popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                  popupWin.document.close();
            }
            </script>';
            $imagetr='<tr>
                            <td align="right"><img src='.$image_url.'/images/Powered_By.jpg width="170"></td>
                    </tr>';
        }
        else
        {
            $image_url=__APP_PATH__;
            $align="";
            $printStringStart='';
            $printStringEnd='';
            $script='';
            $imagetr='';
        } 
        
        /*
        *  Building block for customer account information
        */
        $ctrBr=0;
        $billcountry=$kConfig->getCountryName($bookingDataArr['szCustomerCountry']);
        $customer_address = '';
        $customer_address_br = '';
        if(!empty($bookingDataArr['szCustomerAddress1']))
        {
            $customer_address .= html_entities_flag($bookingDataArr['szCustomerAddress1'],$utf8Flag)." <br />" ;
            ++$ctrBr; 
        } 
        if(!empty($bookingDataArr['szCustomerAddress2']))
        {
            $customer_address .=html_entities_flag($bookingDataArr['szCustomerAddress2'],$utf8Flag)." <br />" ;
            ++$ctrBr;
        } 
        if(!empty($bookingDataArr['szCustomerAddress3']))
        {
            $customer_address .= html_entities_flag($bookingDataArr['szCustomerAddress3'],$utf8Flag)." <br />" ;
            ++$ctrBr;
        } 
        if(!empty($bookingDataArr['szCustomerPostCode']))
        {
            $customer_address .=html_entities_flag($bookingDataArr['szCustomerPostCode'],$utf8Flag) ;
        }
        if(!empty($bookingDataArr['szCustomerCity']))
        {
            $customer_address .= " ".html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag)." <br />" ;
            ++$ctrBr;
        }
        else
        {  
            $customer_address .= " <br />";
            ++$ctrBr;
        }
        if(!empty($bookingDataArr['szCustomerState']))
        {
            $customer_address .=html_entities_flag($bookingDataArr['szCustomerState'],$utf8Flag)." <br />" ;
            ++$ctrBr;
        } 
        if(!empty($billcountry))
        {
            $customer_address .=html_entities_flag($billcountry,$utf8Flag) ;
            ++$ctrBr;
        }  
        /*
        * Calculation <br> for empty customer account details
        */
        if($ctrBr>0)
        {	
            $strbr='';
            for($i=0;$i<$ctrBr;++$i)
            {
                if($strbr=='')
                {
                    $strbr=" <br />";
                }
                else
                {
                    $strbr =" <br /> ".
                            $strbr;
                }
            }
        } 
        /*
        * Creating building block for Shipper details
        */
        $shipperBr=0;
        $shipperAddress = '';
        if(!empty($bookingDataArr['szShipperCompanyName']))
        {
            $shipperAddress.=html_entities_flag($bookingDataArr['szShipperCompanyName'],$utf8Flag)." <br />";
            ++$shipperBr;
        } 
        if(!empty($bookingDataArr['szShipperAddress_pickup']))
        {
            $shipperAddress.=html_entities_flag($bookingDataArr['szShipperAddress'],$utf8Flag)." <br />";
            ++$shipperBr;
        }
        if(!empty($bookingDataArr['szShipperAddress2']))
        {
            $shipperAddress.=html_entities_flag($bookingDataArr['szShipperAddress2'],$utf8Flag)." <br />";
            ++$shipperBr;
        }
        if(!empty($bookingDataArr['szShipperAddress3']))
        {
            $shipperAddress.=html_entities_flag($bookingDataArr['szShipperAddress3'],$utf8Flag)." <br />";
            ++$shipperBr;
        }
        if(!empty($bookingDataArr['szShipperPostCode']))
        {
            $shipperAddress.=html_entities_flag($bookingDataArr['szShipperPostCode'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szShipperCity']))
        {
            ++$shipperBr;
            if(!empty($bookingDataArr['szShipperPostCode']))	
                    $shipperAddress.=" ".html_entities_flag($bookingDataArr['szShipperCity'],$utf8Flag)." <br />";
            else
                    $shipperAddress.=html_entities_flag($bookingDataArr['szShipperCity'],$utf8Flag)." <br />";
        }
        else
        {
            $shipperAddress.=" <br />";
            ++$shipperBr;
        }
        if(!empty($bookingDataArr['szShipperCountry']))
        {
            $shipperAddress.=html_entities_flag($bookingDataArr['szShipperCountry'],$utf8Flag)." <br />";
            ++$shipperBr;
        } 
        /*
        * Calculation <br> for empty shipper details line, To maintain pdf structure we usually replace empty shipper line with <br>
        */
        if($shipperBr>0)
        {	
            $shipperBrStr='';
            for($i=0;$i<$shipperBr;++$i)
            {
                if($shipperBrStr=='')
                {
                    $shipperBrStr=" <br />";
                }
                else
                {
                    $shipperBrStr =" <br />".$shipperBrStr;
                }
            }
        }
        
        /*
        * Creating building block for Consignee details
        */
        
        $consigneeBr=0;
        $ConsigneeAddress = '';
        if(!empty($bookingDataArr['szConsigneeCompanyName']))
        {
            $ConsigneeAddress.=html_entities_flag($bookingDataArr['szConsigneeCompanyName'],$utf8Flag)." <br />";
            ++$consigneeBr;
        } 
        if(!empty($bookingDataArr['szConsigneeAddress']))
        {
            $ConsigneeAddress.=html_entities_flag($bookingDataArr['szConsigneeAddress'],$utf8Flag)." <br />";
            ++$consigneeBr;
        }
        if( !empty($bookingDataArr['szConsigneeAddress2']))
        {
            $ConsigneeAddress.=html_entities_flag($bookingDataArr['szConsigneeAddress2'],$utf8Flag)." <br />";
            ++$consigneeBr;
        }
        if( !empty($bookingDataArr['szConsigneeAddress3']))
        {
            $ConsigneeAddress.=html_entities_flag($bookingDataArr['szConsigneeAddress3'],$utf8Flag)." <br />";
            ++$consigneeBr;
        }
        if(!empty($bookingDataArr['szConsigneePostCode']))
        {
            $ConsigneeAddress.=html_entities_flag($bookingDataArr['szConsigneePostCode'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szConsigneeCity']))
        {
            ++$consigneeBr;
            if(!empty($bookingDataArr['szConsigneePostCode']))
                $ConsigneeAddress.=" ".html_entities_flag($bookingDataArr['szConsigneeCity'],$utf8Flag)." <br />";
            else
                $ConsigneeAddress.=html_entities_flag($bookingDataArr['szConsigneeCity'],$utf8Flag)." <br />";
        }
        else
        {
            $ConsigneeAddress.=" <br />";
            ++$consigneeBr;
        }
        if(!empty($bookingDataArr['szConsigneeCountry']))
        {
            $ConsigneeAddress.=html_entities_flag($bookingDataArr['szConsigneeCountry'],$utf8Flag)." <br />";
            ++$consigneeBr;
        }

        /*
        * Calculation <br> for empty consignee details line, To maintain pdf structure we usually replace empty consignee line with <br>
        */ 
        if($consigneeBr>0)
        {	
            $consigneeBrStr='';
            for($i=0;$i<$consigneeBr;++$i)
            {
                if($consigneeBrStr=='')
                {
                    $consigneeBrStr=" <br />";
                }
                else
                {
                    $consigneeBrStr = " <br />".
                            $consigneeBrStr;
                }
            }
        }
        
        /*
        * Creating building block for From warehouse
        */
        if(!empty($bookingDataArr['szShipperAddress_pickup']) || !empty($bookingDataArr['szShipperAddress2_pickup']) || !empty($bookingDataArr['szShipperAddress2_pickup']))
        {
            $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperAddress_pickup'],$utf8Flag);

            if(!empty($bookingDataArr['szShipperAddress2_pickup']))
            {
                $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress2_pickup'],$utf8Flag);
            }
            if(!empty($bookingDataArr['szShipperAddress3_pickup']))
            {
                $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress3_pickup'],$utf8Flag). "<br />";
            }
            else
            {
                $szWareHouseFromStr.=" <br />";
            }
        }
        if(!empty($bookingDataArr['szShipperPostCode_pickup']) || !empty($bookingDataArr['szShipperCity_pickup']))
        {
            $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperPostCode_pickup'],$utf8Flag);
            if(!empty($bookingDataArr['szShipperPostCode_pickup']))
            {
                $szWareHouseFromStr.=" ".html_entities_flag($bookingDataArr['szShipperCity_pickup'],$utf8Flag)."<br />";
            }
            else
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperCity_pickup'],$utf8Flag)."<br />";
            }
        }
        if(!empty($bookingDataArr['szShipperCountry_pickup']))
        {
            $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperCountry_pickup'],$utf8Flag). "<br />";
        } 
        
        $szShippingMode = '';
        $szTransportationModeShortName = '';
        $kConfig = new cConfig();
        if($bookingDataArr['idTransportMode']>0)
        { 
            $transportModeListAry = array();
            $transportModeListAry = $kConfig->getAllTransportMode($bookingDataArr['idTransportMode']);
            $szShippingMode = $transportModeListAry[0]['szLongName'];
            $szTransportationModeShortName = $transportModeListAry[0]['szShortName'];  
            $bookingDataArr['szFrequency'] = $transportModeListAry[0]['szFrequency'];  
        } 
        
        if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
        }
        if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
        }
        if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
        }
        if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
        }
        
        $dtBookingCutOff = (!empty($bookingDataArr['dtCutOff']) && ($bookingDataArr['dtCutOff']!='0000-00-00 00:00:00'))?date('d. F Y (H:i e)',strtotime($bookingDataArr['dtCutOff'])):"" ;
        $dtBookingAvailable = (!empty($bookingDataArr['dtAvailable'])  && ($bookingDataArr['dtAvailable']!='0000-00-00 00:00:00'))?date('d. F Y (H:i e)',strtotime($bookingDataArr['dtAvailable'])):" ";

        $dtPickupAfter = (!empty($bookingDataArr['dtCutOff']) && ($bookingDataArr['dtCutOff']!='0000-00-00 00:00:00'))?date('d M Y H:i',strtotime($bookingDataArr['dtCutOff'])):'' ;
        $dtWhsCutOff = ( !empty($bookingDataArr['dtWhsCutOff']) && ($bookingDataArr['dtWhsCutOff']!='0000-00-00 00:00:00'))?date('d M Y H:i',strtotime($bookingDataArr['dtWhsCutOff'])):'' ;
        $dtWhsAvailable = (!empty($bookingDataArr['dtWhsAvailabe']) && ($bookingDataArr['dtWhsAvailabe']!='0000-00-00 00:00:00'))?date('d M Y H:i',strtotime($bookingDataArr['dtWhsAvailabe'])):'' ;
        $dtDeliveryBefore = (!empty($bookingDataArr['dtAvailable']) && ($bookingDataArr['dtAvailable']!='0000-00-00 00:00:00'))?date('d M Y H:i',strtotime($bookingDataArr['dtAvailable'])):'' ;
        $dtBookingConfirmed = (!empty($bookingDataArr['dtBookingConfirmed']) && ($bookingDataArr['dtBookingConfirmed']!='0000-00-00 00:00:00'))?date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])):"" ;
        $dtBookingConfirmedTime = (!empty($bookingDataArr['dtBookingConfirmed']) && ($bookingDataArr['dtBookingConfirmed']!='0000-00-00 00:00:00'))?date('H:i',strtotime($bookingDataArr['dtBookingConfirmed'])):"" ;
  
        $iHandlingFeeApplicable = $bookingDataArr['iHandlingFeeApplicable']; 
        $iPrivateFeeApplicable = $bookingDataArr['iPrivateCustomerFeeApplicable']; 
        
        $bookingHandlingFeeDetailsAry = array();
        
        if($iHandlingFeeApplicable==1)
        { 
            if($bookingDataArr['idHandlingFeeLocalCurrency']==$bookingDataArr['idForwarderCurrency'])
            {
                $szHandlingFeeLocalCurrencyExchangeRate='1';
            }
            else
            {
                $szHandlingFeeLocalCurrencyExchangeRate =$bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['szHandlingFeeLocalCurrencyExchangeRate'];
            }
            if($bookingDataArr['fForwarderExchangeRate'])
            {
               $fTotalHandlingFeeApplicable = $bookingDataArr['fTotalHandlingFeeUSD'] / $bookingDataArr['fForwarderExchangeRate'];
            } 

            if($bookingDataArr['fForwarderQuoteCurrencyExchangeRate'])
            {
                $fTotalHandlingFeeApplicableQuoteCurrency = $bookingDataArr['fTotalHandlingFeeUSD'] / $bookingDataArr['fForwarderQuoteCurrencyExchangeRate'];
            }
            $totalAmountValue=round((float)$fTotalHandlingFeeApplicable,2);
            //echo $totalAmountValue."fTotalHandlingFeeApplicable<br />";
            if((float)$fTotalHandlingFeeApplicable>0.00 && !$iHideHandlingFeeTotal){
                $szHandlingFeeDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'" border-bottom="0">Handling fee for Transporteca<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'"> '.$bookingDataArr['szHandlingFeeLocalCurrency'].' '.number_format((float)$bookingDataArr['fTotalHandlingFeeLocalCurrency'],2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.number_format((float)$szHandlingFeeLocalCurrencyExchangeRate,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fTotalHandlingFeeApplicable,2).'</td>
                    </tr>
                ';   
            } 
            /*$szHandlingFeeDetailsString = "(Including Handling fee ".$bookingDataArr['szForwarderQuoteCurrency']." ".$bookingDataArr['fTotalHandlingFeeQuoteCurrency'].")";*/ 

        } 
        $iForwarderManualFeeFlag=false;
        $szHandlingFeeDetailsForForwarderString='';
        if((int)$bookingDataArr['fTotalForwarderManualFee']>0.00){
            
            if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
            {  
                $customerForwarderManualFeeExchangeRate = round((float)($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate']),6);  
                //$customerVATExchangeRate = $customerVATExchangeRate * 1.025 ; 
            }
            else
            { 
                $customerForwarderManualFeeExchangeRate='1.0000';
            }
        $totalAmountValue=$totalAmountValue+round((float)$bookingDataArr['fTotalForwarderManualFee'],2);
            //echo $totalAmountValue."fTotalForwarderManualFee<br />";
            if((float)$bookingDataArr['fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup']>0.00){
                $iForwarderManualFeeFlag=true;
                if(!$iHideHandlingFeeTotal){
                $szHandlingFeeDetailsForForwarderString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'" border-bottom="0">Handling fee for '.$bookingDataArr['szForwarderDispName'].'<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'"> '.$bookingDataArr['szCustomerCurrency'].' '.number_format((float)$bookingDataArr['fTotalForwarderManualFeeCustomterCurrencyWithoutMarkup'],2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.number_format((float)$customerForwarderManualFeeExchangeRate,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".number_format((float)$bookingDataArr['fTotalForwarderManualFee'],2).'</td>
                    </tr>
                ';   
                }
            }
        }
        
        
        if($iPrivateFeeApplicable==1)
        { 
            if($bookingDataArr['idPrivateCustomerFeeCurrency']==$bookingDataArr['idForwarderCurrency'])
            {
                $fPrivateCustomerFeeLocalCurrencyExchangeRate='1';
                $fTotalPrivateCustomerFeeApplicable = $bookingDataArr['fPrivateCustomerFee'];
            }
            else if($bookingDataArr['fPrivateCustomerFeeExchangeRate']>0 && $bookingDataArr['fForwarderExchangeRate'])
            {
                $fPrivateCustomerFeeLocalCurrencyExchangeRate =$bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fPrivateCustomerFeeExchangeRate'];
                $fTotalPrivateCustomerFeeApplicable = $bookingDataArr['fPrivateCustomerFeeUSD'] / $bookingDataArr['fForwarderExchangeRate'];
            }
               
            $totalAmountValue = $totalAmountValue + round((float)$fTotalPrivateCustomerFeeApplicable,2);
            //echo $totalAmountValue."fTotalPrivateCustomerFeeApplicable<br />"; 
            if((float)$fTotalPrivateCustomerFeeApplicable>0.00){
                $szPrivateCustomerFeeDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'" border-bottom="0">Private customer fee<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'"> '.$bookingDataArr['szPrivateCustomerFeeCurrency'].' '.number_format((float)$bookingDataArr['fPrivateCustomerFee'],2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.number_format((float)$fPrivateCustomerFeeLocalCurrencyExchangeRate,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fTotalPrivateCustomerFeeApplicable,2).'</td>
                    </tr>
                ';   
            } 
            /*$szHandlingFeeDetailsString = "(Including Handling fee ".$bookingDataArr['szForwarderQuoteCurrency']." ".$bookingDataArr['fTotalHandlingFeeQuoteCurrency'].")";*/ 

        } 
        /*
        * Please Put conditional HTMLs here in following if blocks, Otherwise its very difficult to change anything in any perticular case - @Ajay
        */ 
        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__ && (int)$bookingDataArr['idServiceProvider']>0)
        { 
            //Automatic Courier Booking block 
            $kCourierServices= new cCourierServices();
            $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($idBooking);
            if((int)$newCourierBookingAry[0]['iCourierAgreementIncluded']==1)
                $labelBy=$newCourierBookingAry[0]['szDisplayName'];
            else
                $labelBy="Transporteca";

            if(!empty($bookingDataArr['dtCutOff']))
            {
                $requestPickUpDate=date('d M Y',strtotime($bookingDataArr['dtCutOff']));
            }
            if(!empty($bookingDataArr['dtAvailable']))
            {
                $expdeliveryDate=date('d M Y',strtotime($bookingDataArr['dtAvailable']));
            }	 
            $szTransportationService="Door-to-door courier service with ".$newCourierBookingAry[0]['szProviderName']." ".$newCourierBookingAry[0]['szProviderProductName'].", ".$labelBy." will setup the booking and make labels.<br />Requested pick-up: ".$requestPickUpDate."<br />Expected delivery: ".$expdeliveryDate;
             
            $o=0;
            $kCourierServices= new cCourierServices();
            $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);

            $providerApiRate=$courierBookingArr[0]['fProviderRate'];
            $providerApiRateCurrency=$courierBookingArr[0]['szProviderCurrency'];
            $exhangeRateProviderValue='1.000';
            if($courierBookingArr[0]['idProviderCurrency']!=$bookingDataArr['idForwarderCurrency'])
            { 
                if($bookingDataArr['szForwarderCurrency']=='USD')
                {
                    $exhangeRateProviderValue=1/$courierBookingArr[0]['fProviderROE']; 
                }
                else
                {
                    $exhangeRateProviderValue=($bookingDataArr['fForwarderExchangeRate'])/$courierBookingArr[0]['fProviderROE'];
                    $exhangeRateProviderValue=round($exhangeRateProviderValue,4);
                }
                $providerApiRate=$providerApiRate/$exhangeRateProviderValue;
                $providerApiRateCurrency=$bookingDataArr['szForwarderCurrency'];		
            } 
            $markUpRate=$courierBookingArr[0]['fMarkUpRate'];  
            if((float)$markUpRate!=0.00)
            {
                $szMarkUpCurrency=$courierBookingArr[0]['szMarkUpCurrency']; 
                $exhangeRateMarkUpValue='1.000';
                $markUpLocalRate=$markUpRate;
                if((int)$courierBookingArr[0]['idMarkUpCurrency']!=(int)$courierBookingArr[0]['idProviderCurrency'])
                { 
                    if($bookingDataArr['szForwarderCurrency']=='USD')
                    {
                        $exhangeRateMarkUpValue=1/$courierBookingArr[0]['fProviderROE']; 
                    }
                    else
                    { 
                        $exhangeRateMarkUpValue=($bookingDataArr['fForwarderExchangeRate'])/$courierBookingArr[0]['fProviderROE'];
                        $exhangeRateMarkUpValue=round($exhangeRateMarkUpValue,4);
                   }
                    $markUpLocalRate=$markUpRate*$exhangeRateMarkUpValue;
                     
                    $szMarkUpCurrency=$bookingDataArr['szForwarderCurrency'];		
                }
            } 
            
            $markUpAdditionalRate=$courierBookingArr[0]['fMarkupperShipment'];
            if((float)$markUpAdditionalRate!=0.00)
            {
                $szMarkUpAdditionalCurrency=$courierBookingArr[0]['szAdditionalMarkUpCurrency'];
                $exhangeRateAdditionalValue='1.000';
                if($courierBookingArr[0]['idAdditionalMarkUpCurrency']!=$bookingDataArr['idProviderCurrency'])
                {  
                    $exhangeRateAdditionalLocalValue=$courierBookingArr[0]['fProviderROE']/$courierBookingArr[0]['fAdditionalMarkUpREO']; 
                    $exhangeRateAdditionalLocalValue=round($exhangeRateAdditionalLocalValue,4); 
                    $exhangeRateAdditionalValue=$courierBookingArr[0]['fAdditionalMarkUpREO']/$courierBookingArr[0]['fProviderROE']; 
                    $exhangeRateAdditionalValue=round($exhangeRateAdditionalValue,4); 
                    $markUpAdditionalLoaclRate=$markUpAdditionalRate/$exhangeRateAdditionalLocalValue;
                    $szMarkUpAdditionalCurrency=$bookingDataArr['szForwarderCurrency'];		
                }
            }

            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==0)
            {
                $markUpFeeRate=$courierBookingArr[0]['fBookingLabelFeeRate'];
                $szMarkUpFeeCurrency=$courierBookingArr[0]['szBookingLabelFeeCurrency'];
                $exhangeRateFeeValue='1.000';
                if($courierBookingArr[0]['idBookingLabelFeeCurrency']!=$bookingDataArr['idForwarderCurrency'])
                {
                    if($bookingDataArr['szForwarderCurrency']=='USD')
                    {
                            $exhangeRateFeeValue=1/$courierBookingArr[0]['fBookingLabelFeeROE'];
                    }
                    else
                    {
                            $exhangeRateFeeValue=($bookingDataArr['fForwarderExchangeRate'])/($courierBookingArr[0]['fBookingLabelFeeROE']);
                            $exhangeRateFeeValue=round($exhangeRateFeeValue,4);	
                    }					
                    $markUpFeeRate=$markUpFeeRate/$exhangeRateFeeValue;
                    $szMarkUpFeeCurrency=$bookingDataArr['szForwarderCurrency'];		
                }
            }
            
            if($bookingDataArr['iQuickQuote']>0)
            {
                $fTotalVat = $bookingDataArr['fTotalVat']; 
                $szVatCurrency = $bookingDataArr['szCustomerCurrency'];	
                $exhangeRateVatValue = 1;
                $fTotalVatLocalCurrency = $fTotalVat;
            }
            else
            {
                $fTotalVat=$courierBookingArr[0]['fVATAmountCustomerCurrency'];
                $fTotalVatLocalCurrency = $courierBookingArr[0]['fVATAmountCustomerCurrency']; 
                $szVatCurrency=$bookingDataArr['szCustomerCurrency'];
                $exhangeRateVatValue='1.000';
            } 
            
            if((float)$fTotalVat>0.00)
            { 
                if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                { 
                    if($bookingDataArr['idCustomerCurrency']=='USD')
                    {
                        $exhangeRateVatValue=1/$bookingDataArr['fCustomerExchangeRate'];
                        $exhangeRateVatValueCal=$bookingDataArr['fCustomerExchangeRate'];
                    }
                    else
                    { 
                        $exhangeRateVatValue=($bookingDataArr['fForwarderExchangeRate'])/($bookingDataArr['fCustomerExchangeRate']);
                        $exhangeRateVatValue=round($exhangeRateVatValue,4);
                    }	
                    if($bDisplayVatWithoutCurrencyMarkup)
                    {
                        $exhangeRateVatValue = $exhangeRateVatValue * 1.025 ;
                    }  
                    if($exhangeRateVatValue>0)
                    {
                        $fTotalVat=$fTotalVat/$exhangeRateVatValue;
                        $szVatCurrency=$bookingDataArr['szForwarderCurrency'];	
                    } 
                }
            }
            /*
            if($bookingDataArr['iQuickQuote']>0)
            {
                $fTotalVat = $bookingDataArr['fTotalVatForwarderCurrency']; 
                $szVatCurrency = $bookingDataArr['szForwarderCurrency'];	
                $exhangeRateVatValue = 1;
                $fTotalVatLocalCurrency = $fTotalVat;
            }
            else
            {
                $fTotalVat=$courierBookingArr[0]['fVATAmountCustomerCurrency'];
                $fTotalVatLocalCurrency = $courierBookingArr[0]['fVATAmountCustomerCurrency'];
                if((float)$fTotalVat>0.00)
                {
                    $szVatCurrency=$bookingDataArr['szCustomerCurrency'];
                    $exhangeRateVatValue='1.000';
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                    { 
                        if($bookingDataArr['idCustomerCurrency']=='USD')
                        {
                            $exhangeRateVatValue=1/$bookingDataArr['fCustomerExchangeRate'];
                            $exhangeRateVatValueCal=$bookingDataArr['fCustomerExchangeRate'];
                        }
                        else
                        { 
                            $exhangeRateVatValue=($bookingDataArr['fForwarderExchangeRate'])/($bookingDataArr['fCustomerExchangeRate']);
                            $exhangeRateVatValue=round($exhangeRateVatValue,4);
                        }	
                        if($bDisplayVatWithoutCurrencyMarkup)
                        {
                            $exhangeRateVatValue = $exhangeRateVatValue * 1.025 ;
                        }  
                        if($exhangeRateVatValue>0)
                        {
                            $fTotalVat=$fTotalVat/$exhangeRateVatValue;
                            $szVatCurrency=$bookingDataArr['szForwarderCurrency'];	
                        } 
                    }
                }
            }
             * 
             */

            $totalAmountValue = $totalAmountValue + round((float)$providerApiRate,2);
            
            $szPricingDetailsString = '';
            $td_style="border-bottom:0px none;";
            $szPricingDetailsString .='
                <tr>
                    <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">Buy rate from '.$newCourierBookingAry[0]['szProviderName'].'</td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$courierBookingArr[0]['szProviderCurrency'].' '.number_format($courierBookingArr[0]['fProviderRate'],2).'</td>
                    <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.number_format((float)$exhangeRateProviderValue,4).'</td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$providerApiRateCurrency.' '.number_format((float)$providerApiRate,2).'</td>
                </tr>
            ';
            if((float)$markUpRate!=0.00)
            {
                if($markUpRate<0)
                {
                    $szMarkupPriceForwarderCurrencyStr = "(".$szMarkUpCurrency.' '.number_format((float)($markUpRate * (-1)),2).")"; 
                    $szMarkupLocalCurrencyStr = "(".$courierBookingArr[0]['szProviderCurrency'].' '.number_format(($markUpLocalRate* (-1)),2).")";
                }
                else
                {
                    $szMarkupPriceForwarderCurrencyStr = $szMarkUpCurrency.' '.number_format((float)$markUpRate,2);
                    $szMarkupLocalCurrencyStr = $courierBookingArr[0]['szProviderCurrency'].' '.number_format($markUpLocalRate,2);
                } 
                
                $totalAmountValue = $totalAmountValue + round((float)$markUpRate,2);
                
                $szPricingDetailsString .= '
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">Markup ('.number_format((float)$courierBookingArr[0]['fMarkupPercent'],2).'% minimum '.$courierBookingArr[0]['szMarkUpCurrency'].' '.number_format($courierBookingArr[0]['fMiniMarkDefaultAmount'],2).')</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$szMarkupLocalCurrencyStr.'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.number_format((float)$exhangeRateMarkUpValue,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$szMarkupPriceForwarderCurrencyStr.'</td>
                    </tr>
                ';
            }

            if((float)$markUpAdditionalRate!=0.00)
            {
                if($markUpAdditionalRate<0)
                { 
                    $szAdditionalMarkupPriceForwarderCurrencyStr = "(".$szMarkUpAdditionalCurrency.' '.number_format((float)($markUpAdditionalRate * (-1)),2).")";
                    $szAdditionalMarkupLocalCurrencyStr = "(".$courierBookingArr[0]['szProviderCurrency'].' '.number_format(($markUpAdditionalLoaclRate * (-1)),2).")";
                }
                else
                {
                    $szAdditionalMarkupPriceForwarderCurrencyStr = $szMarkUpAdditionalCurrency.' '.number_format((float)$markUpAdditionalRate,2);
                    $szAdditionalMarkupLocalCurrencyStr = $courierBookingArr[0]['szProviderCurrency'].' '.number_format($markUpAdditionalLoaclRate,2);
                } 
                 $totalAmountValue = $totalAmountValue + round((float)$markUpAdditionalRate,2);
                 
                $szPricingDetailsString .='
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">Additional fixed markup ('.$courierBookingArr[0]['szAdditionalMarkUpCurrency'].' '.number_format($courierBookingArr[0]['fMarkupperShipment'],2).')</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$szAdditionalMarkupLocalCurrencyStr.'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.number_format((float)$exhangeRateAdditionalValue,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$szAdditionalMarkupPriceForwarderCurrencyStr.'</td>
                    </tr>
                ';
            }
            if((int)$courierBookingArr[0]['iCourierAgreementIncluded']==0)
            {
                $totalAmountValue = $totalAmountValue + round((float)$markUpFeeRate,2);
                
               $szPricingDetailsString .='
                   <tr>
                       <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">Booking and label fee for Transporteca</td>
                       <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$courierBookingArr[0]['szBookingLabelFeeCurrency'].' '.number_format((float)$courierBookingArr[0]['fBookingLabelFeeRate'],2).'</td>
                       <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.number_format((float)$exhangeRateFeeValue,4).'</td>
                       <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$szMarkUpFeeCurrency.' '.number_format((float)$markUpFeeRate,2).'</td>
                   </tr>
               ';
            }
            $totalAmountValue = $totalAmountValue + round((float)$fTotalVat,2);
            //echo $totalAmountValue."totalAmountValue".round((float)$fTotalVat,2)."<br />";
            
            $fTotalPriceForwarderCurrencyValue = round((float)($forwarderTransactionDetailAry[0]['fTotalPriceForwarderCurrency']),2);
            //$totalAmountValue=
            $totalAmountValue = $totalAmountValue - $fTotalPriceForwarderCurrencyValue;
            $szPricingDetailsString .= $szPrivateCustomerFeeDetailsString."".$szHandlingFeeDetailsString."".$szHandlingFeeDetailsForForwarderString; 
            $fAdjustmentPriceForwarderCurrency=round((float)$totalAmountValue,2);
            if((float)$fAdjustmentPriceForwarderCurrency!=0.00 && !$iHideHandlingFeeTotal)
            {
                if($bookingDataArr['fForwarderExchangeRate']>0)
                {
                    $fRoundAdjustRoe = (($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate'])); 
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'] && $bDisplayVatWithoutCurrencyMarkup)
                    {
                        $fRoundAdjustRoe = $fRoundAdjustRoe * 1.025 ;
                    }

                }
                if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                {
                    $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency/$fRoundAdjustRoe;
                }
                else
                {
                    $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency;
                }
                if((float)$fAdjustmentPriceForwarderCurrency>0)
                {
                    $fAdjustmentPriceForwarderCurrencyStr ="(".$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2).")";
                    $fAdjustmentPriceCustomerCurrencyStr ="(".$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2).")";
                }
                else
                {
                    $fAdjustmentPriceForwarderCurrency= str_replace("-", "", $fAdjustmentPriceForwarderCurrency);
                    $fAdjustmentPriceForwarderCurrencyStr =$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2);

                    $fAdjustmentPriceCustomerCurrency= str_replace("-", "", $fAdjustmentPriceCustomerCurrency);
                    $fAdjustmentPriceCustomerCurrencyStr =$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2);
                }
                $szPricingDetailsString .='	
                <tr>
                    <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">Round off adjustment<br /></td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                    <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$fAdjustmentPriceForwarderCurrencyStr.'</td>
                </tr>
            ';
            }
            
            if((float)$fTotalVat>0.00 && !$iHideHandlingFeeTotal)
            {    
                $szPricingDetailsString .='
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">VAT ('.number_format((float)$courierBookingArr[0]['fVATPercentage'],2).'%)</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$bookingDataArr['szCustomerCurrency'].' '.number_format((float)$fTotalVatLocalCurrency,2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.number_format((float)$exhangeRateVatValue,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$szVatCurrency.' '.number_format((float)$fTotalVat,2).'</td>
                    </tr>
                ';
                $fTotalVatForwarderCurrency = $fTotalVat;
                $fTotalVatForwarderCurrency = 0;
            } 
            
            $shipperDetailsArr=$kCourierServices->getShipperNameById($bookingDataArr['idShipperConsignee'],true);

            $szNextStepString =  ' 
                    <table cellpadding="0" style="border-collapse: collapse;" cellspacing="0" border="0" width="730" '.$align.'>
                        <tr>
                            <td valign="top" style="font-family:Cambria;font-size:15px;text-align:center;" align="center"> 
                                Next steps: ';
            if($courierBookingArr[0]['iCourierAgreementIncluded']==1)
            {
                $kForwarder = new cForwarder();
                $kForwarder->load($idForwarder);

                $quotesAry =$kCourierServices->getAllBookingQuotesByFileByBookingId($idBooking);

                $urlPendingTray='http://'.$kForwarder->szControlPanelUrl."/pendingQuotes/createLabel/".$idBooking."__".md5(time())."/";

                $szNextStepString .= $bookingDataArr['szForwarderDispName']." must create shipping labels and upload them <a href='".$urlPendingTray."' target='__blank'>here</a> so shipment can be picked up from ".$shipperDetailsArr['szShipperCompanyName']." by ".date('d M Y',strtotime($bookingDataArr['dtCutOff']));
            }
            else
            {
                $szNextStepString .= $shipperDetailsArr['szShipperCompanyName']." has received shipping labels and will contact ".$courierBookingArr[0]['szServiceProviderName']." to arrange pick-up, expected by ".date('d M Y',strtotime($bookingDataArr['dtCutOff']));
            } 
        }
        else if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__ && $bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        { 
            if((int)$bookingDataArr['idServiceTerms']<=0)
            {
                $idServiceType = $bookingDataArr['idServiceType']; 
                $iShipperConsignee = $bookingDataArr['iShipperConsignee']; 
                $idServiceTerms = $kBooking->getServiceTerms($idServiceType,$iShipperConsignee);
                $bookingDataArr['idServiceTerms'] = $idServiceTerms;
            }  
            //Manual RFQ Booking block
            //$szTransportationService = display_service_type_description_forwarder($bookingDataArr,1);  
            $szTransportationService = display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true); 

            $szOtherComments = $bookingDataArr['szOtherComments'];

            $szTransportationService .="<br /><br />Mode of transport: ".$szShippingMode."<br />Transit time: ".$bookingDataArr['iTransitHours']." days <br />Frequency: ".$bookingDataArr['szFrequency']."<br/>" ;
            if(!empty($szOtherComments))
            { 
                $breaks = array("\n","\r","\r\n");
                $szOtherComments = str_replace($breaks, "<br />", $szOtherComments);  
                $szOtherComments = convertFromCP1252($szOtherComments);

                $szTransportationService .=" <br />".$szOtherComments.". <br />";
            }
            //echo $totalAmountValue."totalAmountValue";
            if($bookingDataArr['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_MARK_UP__) //This section is called 'Manual Mark-up' booking document
            {
                //This section is called 'Manual Mark-up' booking document 
                if($bookingDataArr['fForwarderQuoteCurrencyExchangeRate']>0)
                {
                    if($bookingDataArr['idForwarderQuoteCurrency']==$bookingDataArr['idForwarderCurrency'])
                    {
                        $forwarderExchangeRate = 1.00;
                    }
                    else
                    {
                        $forwarderExchangeRate = round((float)($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fForwarderQuoteCurrencyExchangeRate']),4); 
                    } 
                }  
                //echo $forwarderExchangeRate."forwarderExchangeRate<br />";
                if($bDisplayVatWithoutCurrencyMarkup)
                {
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                    {  
                        $forwarderVATExchangeRate = round((float)($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fForwarderQuoteCurrencyExchangeRate']),6);  
                        //$forwarderVATExchangeRate = $forwarderVATExchangeRate * 1.025 ;
                        
                        $forwarderVATExchangeRateDisplay = round((float)($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate']),6);  
                        //$forwarderVATExchangeRateDisplay = $forwarderVATExchangeRateDisplay * 1.025 ;
                        
                        $customerVATExchangeRate = round((float)($bookingDataArr['fExchangeRate']/$bookingDataArr['fForwarderQuoteCurrencyExchangeRate']),6);  
                        //$customerVATExchangeRate = $customerVATExchangeRate * 1.025 ; 
                    }
                    else
                    { 
                        $forwarderVATExchangeRate=$forwarderExchangeRate;
                        $customerVATExchangeRate=$forwarderExchangeRate;
                        $forwarderVATExchangeRateDisplay = 1;
                    } 
                   
                    if($forwarderVATExchangeRateDisplay>0)
                    { 
                        $fTotalVat = round((float)($bookingDataArr['fTotalVatForwarderCurrency']/$forwarderVATExchangeRate),2);
                        //$totalAmountValue=$totalAmountValue+ round((float)$fTotalVat,2);
                    } 
                    $fTotalVatForwarderCurrency=$bookingDataArr['fTotalVatForwarderCurrency']/$customerVATExchangeRate;
                   
                    $szDisplayVatString = $bookingDataArr['szCurrency']." ".number_format((float)$fTotalVatForwarderCurrency,2);
                }
                else
                {
                    $forwarderVATExchangeRateDisplay = $forwarderExchangeRate;
                    if($forwarderExchangeRate>0)
                    {
                        $fTotalVat = round((float)($bookingDataArr['fTotalVatForwarderCurrency']/$forwarderExchangeRate),2) ;
                        //$totalAmountValue=$totalAmountValue+ round((float)$fTotalVat,2);
                    }
                    //echo $totalAmountValue."totalAmountValue2<br />";
                    $szDisplayVatString = $bookingDataArr['szForwarderQuoteCurrency']." ".number_format((float)$bookingDataArr['fTotalVatForwarderCurrency'],2);
                } 
                $totalAmountValue=$totalAmountValue+ $fTotalVat;
                //echo $totalAmountValue."totalAmountValue4<br />";
                if($forwarderExchangeRate>0)
                {
                    
                    $fTotalPriceCustomerCurrency = number_format((float)($bookingDataArr['fForwarderTotalQuotePrice']/$forwarderExchangeRate),2) ; 
                    $fInvoiceTotal = ($bookingDataArr['fForwarderTotalQuotePrice']+$bookingDataArr['fTotalVatForwarderCurrency'])/$forwarderExchangeRate; 
                    $totalAmountValue=$totalAmountValue+ round((float)($bookingDataArr['fForwarderTotalQuotePrice']/$forwarderExchangeRate),2);
                    $forwarderExchangeRateDisplay = round((float)($forwarderExchangeRate),4);
                }  
                //echo $totalAmountValue."totalAmountValue5<br />";
                 
                $szPriceTitle = "Price";
                $szPricingDetailsString .= '  
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;">&nbsp;</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Local Currency</strong></td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>ROE</strong></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Your Currency</strong></td>
                        </tr>
                ' ;
                
                $szPricingDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'" border-bottom="0">Customer quote<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderQuoteCurrency']." ".number_format((float)$bookingDataArr['fForwarderTotalQuotePrice'],2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.number_format((float)$forwarderExchangeRateDisplay,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".$fTotalPriceCustomerCurrency.'</td>
                    </tr>
                '; 
                
                
                //$flag=true;
                $totalAmountValue =  $totalAmountValue - $fInvoiceTotal;
                $fAdjustmentPriceForwarderCurrency=round((float)$totalAmountValue,2);
                //echo $fAdjustmentPriceForwarderCurrency."fAdjustmentPriceForwarderCurrency";
                if((float)$fAdjustmentPriceForwarderCurrency!=0.00 && !$iHideHandlingFeeTotal)
                {
                    if($bookingDataArr['fForwarderExchangeRate']>0)
                    {
                        $fRoundAdjustRoe = (($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate'])); 
                        if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'] && $bDisplayVatWithoutCurrencyMarkup)
                        {
                            $fRoundAdjustRoe = $fRoundAdjustRoe * 1.025 ;
                        }
                           
                    }
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency/$fRoundAdjustRoe;
                    }
                    else
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency;
                    }
                    if((float)$fAdjustmentPriceForwarderCurrency>0)
                    {
                        $fAdjustmentPriceForwarderCurrencyStr ="(".$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2).")";
                        $fAdjustmentPriceCustomerCurrencyStr ="(".$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2).")";
                    }
                    else
                    {
                        $fAdjustmentPriceForwarderCurrency= str_replace("-", "", $fAdjustmentPriceForwarderCurrency);
                        $fAdjustmentPriceForwarderCurrencyStr =$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2);

                        $fAdjustmentPriceCustomerCurrency= str_replace("-", "", $fAdjustmentPriceCustomerCurrency);
                        $fAdjustmentPriceCustomerCurrencyStr =$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2);
                    }
                    $szPricingDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">Round off adjustment<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$fAdjustmentPriceForwarderCurrencyStr.'</td>
                    </tr>
                ';
                }
                
                if($fTotalVat>0 && !$iHideHandlingFeeTotal)
                { 
                    $szPricingDetailsString .='	
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">VAT<br /></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$szDisplayVatString.'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.number_format((float)$forwarderVATExchangeRateDisplay,4).'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fTotalVat,2).'</td>
                        </tr>
                    '; 
                }
                if(!$iHideHandlingFeeTotal){
                $szPricingDetailsString .='
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Total</strong></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.$bookingDataArr['szForwarderQuoteCurrency'].' '.number_format((float)($bookingDataArr['fForwarderTotalQuotePrice']+$bookingDataArr['fTotalVatForwarderCurrency']),2).'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.number_format((float)$forwarderExchangeRateDisplay,4).'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.$bookingDataArr['szForwarderCurrency'].' '.number_format((float)$fInvoiceTotal,2).'</td>
                        </tr>';
                }
                $szPricingDetailsString .=' </table>
                <br>' ;
                
            }
            else
            { 
                //This section is called 'Manual Referral' booking document  
                if($bookingDataArr['fForwarderQuoteCurrencyExchangeRate']>0)
                {
                    if($bookingDataArr['idForwarderQuoteCurrency']==$bookingDataArr['idForwarderCurrency'])
                    {
                        $forwarderExchangeRate = 1.00;
                    }
                    else
                    {
                        $forwarderExchangeRate = round((float)($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fForwarderQuoteCurrencyExchangeRate']),4); 
                    } 
                }              
                $fForwarderTotalQuotePriceUSD = $bookingDataArr['fForwarderTotalQuotePrice'] * $bookingDataArr['fForwarderQuoteCurrencyExchangeRate'] ;
                $fTotalVatForwarderCurrencyUSD = $bookingDataArr['fTotalVatForwarderCurrency'] * $bookingDataArr['fForwarderQuoteCurrencyExchangeRate'] ;
                $fTotalQuotePriceIncludingVatUsd = $fForwarderTotalQuotePriceUSD + $fTotalVatForwarderCurrencyUSD; 
  
                if($bDisplayVatWithoutCurrencyMarkup)
                {
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                    {  
                        $forwarderVATExchangeRateDisplay = round((float)($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate']),6);  
                        $forwarderVATExchangeRateDisplay = $forwarderVATExchangeRateDisplay * 1.025 ;
                    }
                    else
                    { 
                        $forwarderVATExchangeRateDisplay = 1;
                    } 
                    
                    if($forwarderVATExchangeRateDisplay>0)
                    {
                       // $fTotalVatCustomerCurrencyUSD = $bookingDataArr['fTotalVat'] * $bookingDataArr['fExchangeRate'];
                        $fTotalVat = round((float)($bookingDataArr['fTotalVat']/$forwarderVATExchangeRateDisplay),2);
                        $totalAmountValue=$totalAmountValue+ round((float)$fTotalVat,2);
                    }  
                    
                    $szDisplayVatString = $bookingDataArr['szCurrency']." ".number_format((float)$bookingDataArr['fTotalVat'],2);
                }
                else
                { 
                    $forwarderVATExchangeRateDisplay = $forwarderExchangeRate;
                    if($forwarderVATExchangeRateDisplay>0)
                    { 
                        $fTotalVat = number_format((float)($bookingDataArr['fTotalVatForwarderCurrency']/$forwarderVATExchangeRateDisplay),2);  
                        $totalAmountValue=$totalAmountValue+ round((float)$fTotalVat,2);
                    }
                    $szDisplayVatString = $bookingDataArr['szForwarderQuoteCurrency']." ".number_format((float)$bookingDataArr['fTotalVatForwarderCurrency'],2);
                }  
                if($forwarderExchangeRate>0)
                {
                    $fTotalPriceCustomerCurrency = round((float)($bookingDataArr['fForwarderTotalQuotePrice']/$forwarderExchangeRate),2) ;  
                }
                if($iHandlingFeeApplicable==1)
                {
                    $fTotalPriceCustomerCurrency=round((float)($fTotalPriceCustomerCurrency-$fTotalHandlingFeeApplicable),2);
                    $bookingDataArr['fForwarderTotalQuotePrice']=$bookingDataArr['fForwarderTotalQuotePrice']-$fTotalHandlingFeeApplicableQuoteCurrency;
                }
                
                //$fTotalPriceCustomerCurrency=round((float)($fTotalPriceCustomerCurrency-$fTotalHandlingFeeApplicable),2);
                $totalAmountValue=$totalAmountValue+ round((float)$fTotalPriceCustomerCurrency,2);
                
                $fTotalPriceCustomerCurrency=  number_format((float)$fTotalPriceCustomerCurrency,2);
                $fInvoiceTotal = $forwarderTransactionDetailAry[0]['fTotalPriceForwarderCurrency'];
                $fAllInclusivePriceCustomer = ($bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalVat']);
                if($fInvoiceTotal)
                { 
                    $fAllInclusiveROE = $forwarderTransactionDetailAry[0]['fExchangeRate'];
                }
                
                $szPricingDetailsString .= ' 
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;">&nbsp;</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Local Currency</strong></td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>ROE</strong></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Your Currency</strong></td>
                        </tr>
                ' ; 
                $szPricingDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'" border-bottom="0">Customer quote <br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderQuoteCurrency']." ".number_format((float)$bookingDataArr['fForwarderTotalQuotePrice'],2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.number_format((float)$forwarderExchangeRate,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".$fTotalPriceCustomerCurrency.'</td>
                    </tr>
                '; 
                $szPricingDetailsString .= $szPrivateCustomerFeeDetailsString."".$szHandlingFeeDetailsString."".$szHandlingFeeDetailsForForwarderString;
                $totalAmountValue=$totalAmountValue;
                //echo $totalAmountValue."totalAmountValue".round((float)$fTotalPriceCustomerCurrency,2)."<br />";
                $totalAmountValue =  $totalAmountValue - $fInvoiceTotal;
                //echo $totalAmountValue."totalAmountValue";
                //$flag=true;
                $fAdjustmentPriceForwarderCurrency=round((float)$totalAmountValue,2);
                if((float)$fAdjustmentPriceForwarderCurrency!=0.00 && !$iHideHandlingFeeTotal)
                {
                    if($bookingDataArr['fForwarderExchangeRate']>0)
                    {
                        $fRoundAdjustRoe = (($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate'])); 
                        if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'] && $bDisplayVatWithoutCurrencyMarkup)
                        {
                            $fRoundAdjustRoe = $fRoundAdjustRoe * 1.025 ;
                        }
                           
                    }
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency/$fRoundAdjustRoe;
                    }
                    else
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency;
                    }
                    if((float)$fAdjustmentPriceForwarderCurrency>0)
                    {
                        $fAdjustmentPriceForwarderCurrencyStr ="(".$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2).")";
                        $fAdjustmentPriceCustomerCurrencyStr ="(".$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2).")";
                    }
                    else
                    {
                        $fAdjustmentPriceForwarderCurrency= str_replace("-", "", $fAdjustmentPriceForwarderCurrency);
                        $fAdjustmentPriceForwarderCurrencyStr =$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2);

                        $fAdjustmentPriceCustomerCurrency= str_replace("-", "", $fAdjustmentPriceCustomerCurrency);
                        $fAdjustmentPriceCustomerCurrencyStr =$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2);
                    }
                    $szPricingDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">Round off adjustment<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$fAdjustmentPriceForwarderCurrencyStr.'</td>
                    </tr>
                ';
                }
                if($bookingDataArr['fTotalVat']>0 && !$iHideHandlingFeeTotal)
                {
                    $szPricingDetailsString .='	
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">VAT<br /></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$szDisplayVatString.'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.number_format((float)$forwarderVATExchangeRateDisplay,4).'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fTotalVat,2).'</td>
                        </tr>
                    ';  
                }
                $rateExchangeText='';
                if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                {
                    $rateExchangeText='
                        <tr>
                            <td colspan="4" valign="top" style="font-family:Cambria;font-size:11px;background:#fff;">*If other currency than your bank currency, ROE includes '.number_format((float)($bookingDataArr['fMarkupPercentage']),2).'% to cover any currency exchange cost, as per bank tariff.</td>
                        </tr>
                    ';
                } 
                if(!$iHideHandlingFeeTotal){
                $szPricingDetailsString .='
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Customer Invoice '.$bookingDataArr['szInvoice'].' (Paid with '.$iPaymentType.')</strong></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.$bookingDataArr['szCurrency'].' '.number_format($fAllInclusivePriceCustomer,2).'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.number_format((float)$fAllInclusiveROE,4).'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.$bookingDataArr['szForwarderCurrency'].' '.number_format((float)$fInvoiceTotal,2).'</td>
                        </tr>';
                }
                 $szPricingDetailsString .= $rateExchangeText.'
                    </table>
                    <br>
                ';
            } 
        }
        else
        {  
            if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
            {
                if((int)$bookingDataArr['idServiceTerms']<=0)
                {
                    $idServiceType = $bookingDataArr['idServiceType']; 
                    $iShipperConsignee = $bookingDataArr['iShipperConsignee']; 
                    $idServiceTerms = $kBooking->getServiceTerms($idServiceType,$iShipperConsignee);
                    $bookingDataArr['idServiceTerms'] = $idServiceTerms;
                } 
                //Automatic LTL bookings, i.e bookings where trade is definde under /dtdTrades/
                //$szTransportationService = display_service_type_description_forwarder($bookingDataArr,1); 
                $szTransportationService = display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true); 
                $dtRequestedPickup = (!empty($bookingDataArr['dtTimingDate']) && ($bookingDataArr['dtTimingDate']!='0000-00-00 00:00:00'))?date('d M Y',strtotime($bookingDataArr['dtTimingDate'])):'' ;
                $dtLatestPickup = (!empty($bookingDataArr['dtCutOff']) && ($bookingDataArr['dtCutOff']!='0000-00-00 00:00:00'))?date('d M Y',strtotime($bookingDataArr['dtCutOff'])):'' ;
                $dtWhsCutOff = ( !empty($bookingDataArr['dtWhsCutOff']) && ($bookingDataArr['dtWhsCutOff']!='0000-00-00 00:00:00'))?date('d M Y',strtotime($bookingDataArr['dtWhsCutOff'])):'' ;
                $dtExpactedDelivery = (!empty($bookingDataArr['dtAvailable']) && ($bookingDataArr['dtAvailable']!='0000-00-00 00:00:00'))?date('d M Y',strtotime($bookingDataArr['dtAvailable'])):'' ;
                
                $szTransportationService .= "<br />Mode of transport: ".$szShippingMode;
                $szTransportationService .= "<br />Cargo ready: ".$dtRequestedPickup;
                $szTransportationService .= "<br />Latest pick-up: ".$dtLatestPickup;
                $szTransportationService .= "<br />Export truck: ".$dtWhsCutOff;
                $szTransportationService .= "<br />Expected delivery: ".$dtExpactedDelivery; 
            }
            else
            {
                //Automatic LCL bookings
                $services_string = $serviceTypeAry['szDescription'] ;
                $cc_string = '';
                if((int)$bookingDataArr['iOriginCC']==1)
                {
                    $cc_string = "Customs clearance at origin";
                }						
                if((int)$bookingDataArr['iDestinationCC']==2)
                {
                    if(empty($cc_string))
                    {
                        $cc_string = "Customs clearance at destination";
                    }
                    else
                    {
                        $cc_string.= " and destination";
                    }
                }

                if($cc_string=='')
                {
                    $cc_string.= " Customs clearance not included.";
                }
                else
                {
                    $cc_string.= " included.";
                }
                $szTransportationService = $services_string.'. '.$cc_string ;
            }  
            
            if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_1__ || trim($bookingDataArr['iPaymentType'])=="Zooz")
            {
                $iPaymentType="Credit Card";
            }
            else if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
            {
                $iPaymentType="Bank Transfer";
            }
            else
            {
                $iPaymentType=$bookingDataArr['iPaymentType'];
            }  
            $szPriceTitle = "Price break down";
            if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
            {
                if($bookingDataArr['fForwarderCurrencyExchangeRate']>0)
                {
                    $forwarderExchangeRate = round((float)(1/$bookingDataArr['fForwarderCurrencyExchangeRate']),6);
                }
                 
                //This type of booking has always idServiceType: DTD
                $fGrandTotalForwarderCurrency = ($bookingDataArr['fOriginHaulagePrice'] * $forwarderExchangeRate)+($bookingDataArr['fOriginCCPrice']*$forwarderExchangeRate)+($bookingDataArr['fCfstoCfsPrice']*$forwarderExchangeRate)+($bookingDataArr['fDestinationCCPrice']*$forwarderExchangeRate)+($bookingDataArr['fDestinationHaulagePrice'] * $forwarderExchangeRate);
                $grandtotal = ($bookingDataArr['fOriginHaulagePrice'] * $forwarderExchangeRate)+($bookingDataArr['fOriginCCPrice']*$forwarderExchangeRate)+($bookingDataArr['fCfstoCfsPrice']*$forwarderExchangeRate)+($bookingDataArr['fDestinationCCPrice']*$forwarderExchangeRate)+($bookingDataArr['fDestinationHaulagePrice'] * $forwarderExchangeRate);
                   
                $fTotalPrivateCustomerFeeApplicableForwarderCurrency = 0;
                $fTotalPrivateCustomerFeeApplicableCustomerCurrency = 0;
                
                if($iPrivateFeeApplicable==1 && $fTotalPrivateCustomerFeeApplicable>0)
                {
                    $fTotalPrivateCustomerFeeApplicableForwarderCurrency = $fTotalPrivateCustomerFeeApplicable; 
                    $fTotalPrivateCustomerFeeApplicableUSD = $fTotalPrivateCustomerFeeApplicableForwarderCurrency * $bookingDataArr['fForwarderExchangeRate'];
                    if($bookingDataArr['fExchangeRate']>0)
                    {
                        $fTotalPrivateCustomerFeeApplicableCustomerCurrency = (float)($fTotalPrivateCustomerFeeApplicableUSD/$bookingDataArr['fExchangeRate']);
                    } 
                }
                
                if($bookingDataArr['fExchangeRate']>0)
                { 
                    $forwarderExchangeRate = round((float)($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate']),4);
                } 
                $forwarderVatExchangeRate = $forwarderExchangeRate;
                if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'] && $bDisplayVatWithoutCurrencyMarkup)
                { 
                    $forwarderVatExchangeRate = round((float)($forwarderVatExchangeRate * 1.025),6) ;
                } 
                if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                { 
                    $fTotalFreightPriceCustomerCurrency = $bookingDataArr['fTotalPriceCustomerCurrency']/1.025 ;
                    $fTotalVatCustomerCurrency = $bookingDataArr['fTotalVat']/1.025;
                }
                else
                {
                    $fTotalFreightPriceCustomerCurrency = $bookingDataArr['fTotalPriceCustomerCurrency'];
                    $fTotalVatCustomerCurrency = $bookingDataArr['fTotalVat'];
                } 
                
                if($iForwarderManualFeeFlag)
                {
                    $fTotalFreightPriceCustomerCurrency=$fTotalFreightPriceCustomerCurrency-$bookingDataArr['fTotalForwarderManualFee'];
                }
                
                if($iHandlingFeeApplicable==1)
                {
                    
                    if($bookingDataArr['fExchangeRate']>0)
                    {
                        $fTotalHandlingFeeApplicableCustomerCurrency = (float)($bookingDataArr['fTotalHandlingFeeUSD']/$bookingDataArr['fExchangeRate']);
                    }        
                    /*
                    * While creating 'Handling fee' was included in total price but as on /bookingNotice/ we are displaying price break-ups.
                    *  So that we are substarcting Handling fee from total price(if applicable) 
                    */
                    $fTotalFreightPriceCustomerCurrency = $fTotalFreightPriceCustomerCurrency - $fTotalHandlingFeeApplicableCustomerCurrency;
                }
                
                if($iPrivateFeeApplicable==1)
                {
                    /*
                    * While creating 'Private customer fee' was included in total price but as on /bookingNotice/ we are displaying price break-ups.
                    *  So that we are substarcting private customer fee from total price(if applicable) 
                    */
                    $fTotalFreightPriceCustomerCurrency = $fTotalFreightPriceCustomerCurrency - $fTotalPrivateCustomerFeeApplicableCustomerCurrency;
                }
                
                $fForwarderTotalQuotePriceUSD = $fTotalFreightPriceCustomerCurrency * $bookingDataArr['fExchangeRate'] ;
                $fTotalVatForwarderCurrencyUSD = $fTotalVatCustomerCurrency * $bookingDataArr['fExchangeRate'];
                $fTotalQuotePriceIncludingVatUsd = $fForwarderTotalQuotePriceUSD + $fTotalVatForwarderCurrencyUSD;  
                if($bookingDataArr['fForwarderExchangeRate']>0)
                {
                    $totalAmountValue = $totalAmountValue + round((float)($fForwarderTotalQuotePriceUSD/$bookingDataArr['fForwarderExchangeRate']),2);
                    $fTotalPriceCustomerCurrency = number_format((float)($fForwarderTotalQuotePriceUSD/$bookingDataArr['fForwarderExchangeRate']),2) ; 
                    //$fTotalVat = number_format((float)($fTotalVatForwarderCurrencyUSD/$bookingDataArr['fForwarderExchangeRate']),2);   
                }  
                //echo $totalAmountValue."totalAmountValue";
                
                if($forwarderVatExchangeRate>0)
                {
                    $totalAmountValue = $totalAmountValue + round((float)($bookingDataArr['fTotalVat']/$forwarderVatExchangeRate),2);
                    $fTotalVat = number_format((float)($bookingDataArr['fTotalVat']/$forwarderVatExchangeRate),2);     
                }
                $fInvoiceTotal = $forwarderTransactionDetailAry[0]['fTotalPriceForwarderCurrency'];
                $fAllInclusivePriceCustomer = ($bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalVat']);
                if($fInvoiceTotal)
                { 
                    $fAllInclusiveROE = $forwarderTransactionDetailAry[0]['fExchangeRate'];
                } 
                $szPriceTitle = "Price";
                
                
                $szPricingDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'" border-bottom="0">Freight<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szCurrency']." ".number_format((float)$fTotalFreightPriceCustomerCurrency,2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.number_format((float)$forwarderExchangeRate,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-bottom:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".$fTotalPriceCustomerCurrency.'</td>
                    </tr>
                ';  
                
                //echo $totalAmountValue."fTotalPriceForwarderCurrencyValueBefore<br />"; 
                $szPricingDetailsString .= $szPrivateCustomerFeeDetailsString."".$szHandlingFeeDetailsString."".$szHandlingFeeDetailsForForwarderString;
                $fTotalPriceForwarderCurrencyValue=round((float)($forwarderTransactionDetailAry[0]['fTotalPriceForwarderCurrency']),2);
                $totalAmountValue = $totalAmountValue-$fTotalPriceForwarderCurrencyValue;
                //echo $totalAmountValue."fTotalPriceForwarderCurrencyValue<br />"; 
                $fAdjustmentPriceForwarderCurrency=round((float)$totalAmountValue,2);
                if((float)$fAdjustmentPriceForwarderCurrency!=0.00 && !$iHideHandlingFeeTotal)
                {
                    if($bookingDataArr['fForwarderExchangeRate']>0)
                    {
                        $fRoundAdjustRoe = (($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate'])); 
                        if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'] && $bDisplayVatWithoutCurrencyMarkup)
                        {
                            $fRoundAdjustRoe = $fRoundAdjustRoe * 1.025 ;
                        }
                           
                    }
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency/$fRoundAdjustRoe;
                    }
                    else
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency;
                    }
                    if((float)$fAdjustmentPriceForwarderCurrency>0)
                    {
                        $fAdjustmentPriceForwarderCurrencyStr ="(".$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2).")";
                        $fAdjustmentPriceCustomerCurrencyStr ="(".$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2).")";
                    }
                    else
                    {
                        $fAdjustmentPriceForwarderCurrency= str_replace("-", "", $fAdjustmentPriceForwarderCurrency);
                        $fAdjustmentPriceForwarderCurrencyStr =$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2);

                        $fAdjustmentPriceCustomerCurrency= str_replace("-", "", $fAdjustmentPriceCustomerCurrency);
                        $fAdjustmentPriceCustomerCurrencyStr =$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2);
                    }
                    $szPricingDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">Round off adjustment<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$fAdjustmentPriceCustomerCurrencyStr.'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.number_format((float)$fRoundAdjustRoe,4).'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$fAdjustmentPriceForwarderCurrencyStr.'</td>
                    </tr>
                ';
                }
                 
                if($bookingDataArr['fTotalVat']>0 && !$iHideHandlingFeeTotal)
                {
                    $szPricingDetailsString .='	
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">VAT<br /></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$bookingDataArr['szCurrency']." ".number_format((float)$bookingDataArr['fTotalVat'],2).'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.number_format((float)$forwarderVatExchangeRate,4).'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".$fTotalVat.'</td>
                        </tr>
                    ';  
                } 
            }
            else
            {
                if($bookingDataArr['fForwarderCurrencyExchangeRate']>0)
                {
                    $forwarderExchangeRate = round((float)(1/$bookingDataArr['fForwarderCurrencyExchangeRate']),6);
                } 
                $extramarkuprate = ($bookingDataArr['fOriginHaulagePrice'] * $forwarderExchangeRate)+($bookingDataArr['fOriginCCPrice']*$forwarderExchangeRate)+
                                            ($bookingDataArr['fCfstoCfsPrice']*$forwarderExchangeRate)+($bookingDataArr['fDestinationCCPrice']*$forwarderExchangeRate)+
                                            ($bookingDataArr['fDestinationHaulagePrice'] * $forwarderExchangeRate);	
                if((float)$extramarkuprate>0)		
                $extramarkuprateroe=round((float)($bookingDataArr['fTotalPriceCustomerCurrency']/$extramarkuprate),6);
                if($bookingDataArr['fOriginHaulageExchangeRate']>0)
                { 
                    $bookingDataArr['fOriginHaulageExchangeRate'] = 1/$bookingDataArr['fOriginHaulageExchangeRate'] ;
                    $fOriginHaulageRoe = round((float)(1/($forwarderExchangeRate*$bookingDataArr['fOriginHaulageExchangeRate'])),6) ;		    	
                    $fOriginHaulageTotalRoe =round((float)(1/$fOriginHaulageRoe),6);
                }
                else
                {
                    $fOriginHaulageTotalRoe=0.0000 ;
                }	

                if($bookingDataArr['szOriginHaulageCurrency']>0)
                {
                    $OriginHaulAry = $kConfig->getBookingCurrency($bookingDataArr['szOriginHaulageCurrency']);
                } 
                if($bookingDataArr['fDestinationHaulageExchangeRate']>0)
                { 
                    $bookingDataArr['fDestinationHaulageExchangeRate'] = 1/$bookingDataArr['fDestinationHaulageExchangeRate'] ;
                    $fDestinationHaulageRoe = round((float)(1/($forwarderExchangeRate * $bookingDataArr['fDestinationHaulageExchangeRate'])),6);		    	
                    $fDestinationHaulageTotalRoe =round((float)(1/$fDestinationHaulageRoe),6);
                }
                else
                {
                    $fDestinationHaulageTotalRoe=0.0000 ;
                }	

                if($bookingDataArr['szDestinationHaulageCurrency']>0)
                {
                    $DestinationHaulAry = $kConfig->getBookingCurrency($bookingDataArr['szDestinationHaulageCurrency']);
                }	 
                if($bookingDataArr['fOriginCCExchangeRate']>0)
                {
                    $originCCROE = round((float)($forwarderExchangeRate*$bookingDataArr['fOriginCCExchangeRate']),8) ; 
                }	
                else
                {
                    $originCCROE = 0.0 ;		
                }
                if($bookingDataArr['fDestinationCCExchangeRate']>0)
                {
                    $DestinationCCROE = round((float)($forwarderExchangeRate*$bookingDataArr['fDestinationCCExchangeRate']),8) ; 
                }	
                else
                {
                    $DestinationCCROE = 0.0 ;				
                }

                if($bookingDataArr['fFreightExchangeRate']>0)
                {
                    $cfs_to_cfs_roe = round((float)($forwarderExchangeRate * $bookingDataArr['fFreightExchangeRate']),6);
                }
                else
                {
                    $cfs_to_cfs_roe = 0.0;
                }

                if($bookingDataArr['fOriginPortExchangeRate']>0)
                {
                    $origin_charges_roe = round((float)($forwarderExchangeRate * (1/$bookingDataArr['fOriginPortExchangeRate'])),6);
                }
                else
                {
                    $origin_charges_roe = 0.0;
                }

                if($bookingDataArr['fDestinationPortExchangeRate']>0)
                {
                    $destination_charge_roe = round((float)($forwarderExchangeRate * (1/$bookingDataArr['fDestinationPortExchangeRate'])),6);
                }
                else
                {
                    $destination_charge_roe = 0.0;
                }

                if($bookingDataArr['fExchangeRate']>0)
                {
                    $customerRoe = round((float)(1/($forwarderExchangeRate * $bookingDataArr['fExchangeRate'])),6);
                }	
                if(empty($bookingDataArr['szOriginHaulageCurrency']))
                {
                    $bookingDataArr['szOriginHaulageCurrency'] = 'USD';
                }
                if(empty($bookingDataArr['szDestinationHaulageCurrency']))
                {
                    $bookingDataArr['szDestinationHaulageCurrency'] = 'USD';
                }
                if(empty($bookingDataArr['szOriginPortCurrency']))
                {
                    $bookingDataArr['szOriginPortCurrency'] = 'USD';
                }
                if(empty($bookingDataArr['szDestinationPortCurrency']))
                {
                    $bookingDataArr['szDestinationPortCurrency'] = 'USD';
                }
                if(empty($bookingDataArr['szOriginCCCurrency']))
                {
                    $bookingDataArr['szOriginCCCurrency'] = 'USD';
                }
                if(empty($bookingDataArr['szDestinationCCCurrency']))
                {
                    $bookingDataArr['szDestinationCCCurrency'] = 'USD';
                }
                if(empty($bookingDataArr['szFreightCurrency']))
                {
                    $bookingDataArr['szFreightCurrency'] = 'USD';
                }
                
                if((float)$cfs_to_cfs_roe>0)
                $cfs_to_cfs_roe_inverse = number_format((float)(1/$cfs_to_cfs_roe),4);	
                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD , 2.DTW
                {
                    $export_haulage_td1 = 'Export Haulage<br /><font size="1" color="gray"><i>(max(ROUNDUP(max('.number_format((float)$bookingDataArr['fCargoWeight']).'kg ; '.format_volume((float)$bookingDataArr['fCargoVolume']).'cbm x '.number_format((float)$bookingDataArr['fOriginHaulageWmFactor']).'kg/cbm)/100kg) x '.$bookingDataArr['szOriginHaulageCurrency'].' '.number_format((float)$bookingDataArr['fOriginHaulageRateWM_KM'],2).') ; '.$bookingDataArr['szOriginHaulageCurrency'].' '.number_format((float)$bookingDataArr['fOriginHaulageMinRateWM_KM'],2).') + '.$bookingDataArr['szOriginHaulageCurrency'].' '.number_format((float)$bookingDataArr['fOriginHaulageBookingRateWM_KM'],2).') x ( 1 + ('.number_format((float)$bookingDataArr['fOriginHaulageFuelPercentage'],1).'%))</i></font>
                            <br />';
                    $export_haulage_td2 = $bookingDataArr['szOriginHaulageCurrency'].' '.number_format((float)($bookingDataArr['fOriginHaulagePrice']/$bookingDataArr['fOriginHaulageExchangeRate']),2).'<br />
                    <br />';
                    if($fOriginHaulageTotalRoe>0)
                    {
                        $fOriginHaulageTotalRoe_inverse = number_format((float)(1/$fOriginHaulageTotalRoe),4);
                    }
                    else
                    {
                        $fOriginHaulageTotalRoe_inverse = 0.0 ;
                    }					
                    $export_haulage_td3 = $fOriginHaulageTotalRoe_inverse.'<br />
                    <br />' ;

                    $export_haulage_td4 = $bookingDataArr['szForwarderCurrency'].' '.number_format((float)($bookingDataArr['fOriginHaulagePrice'] * $forwarderExchangeRate),2).'<br> <br>' ;
                    $totalAmountValue =$totalAmountValue+round((float)($bookingDataArr['fOriginHaulagePrice'] * $forwarderExchangeRate),2);
                    
                    //echo $totalAmountValue."totalAmountValue".round((float)($bookingDataArr['fOriginHaulagePrice'] * $forwarderExchangeRate),2)."<br />";
                }

                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD , 3.WTD
                {
                    $import_haulage_td1 = '
                                    Import Haulage<br /><font size="1" color="gray"><i>(max(ROUNDUP(max('.number_format((float)$bookingDataArr['fCargoWeight']).'kg ; '.format_volume((float)$bookingDataArr['fCargoVolume']).'cbm x '.number_format((float)$bookingDataArr['fDestinationHaulageWmFactor']).'kg/cbm)/100kg) x '.$bookingDataArr['szDestinationHaulageCurrency'].' '.number_format((float)$bookingDataArr['fDestinationHaulageRateWM_KM'],2).') ; '.$bookingDataArr['szDestinationHaulageCurrency'].' '.number_format((float)$bookingDataArr['fDestinationHaulageMinRateWM_KM'],2).') + '.$bookingDataArr['szDestinationHaulageCurrency'].' '.number_format((float)$bookingDataArr['fDestinationHaulageBookingRateWM_KM'],2).') x ( 1 + ('.number_format((float)$bookingDataArr['fDestinationHaulageFuelPercentage'],1).'%))</i></font>
                            <br />';
                    //$import_haulage_td1 = 'Import Haulage<br /><font size="1" color="gray"><i>max('.number_format((float)$bookingDataArr['fDestinationHaulageDistance']).' km x '.$DestinationHaulAry[0]['szCurrency'].' '.number_format($bookingDataArr['fDestinationHaulageRateWM_KM'],2).' x max('.ceil($bookingDataArr['fCargoVolume']).'cbm;'.ceil($bookingDataArr['fCargoWeight']/1000).'mt);'.$DestinationHaulAry[0]['szCurrency'].' '.number_format($bookingDataArr['fDestinationHaulageMinRateWM_KM'],2).') + '.$DestinationHaulAry[0]['szCurrency'].' '.number_format($bookingDataArr['fDestinationHaulageBookingRateWM_KM'],2).'</i></font>';
                    $import_haulage_td2 = $bookingDataArr['szDestinationHaulageCurrency'].' '.number_format((float)($bookingDataArr['fDestinationHaulagePrice']/$bookingDataArr['fDestinationHaulageExchangeRate']),2).'<br />
                    <br />';

                    if($fDestinationHaulageTotalRoe>0)
                    {
                        $fDestinationHaulageTotalRoe_inverse = number_format((float)(1/$fDestinationHaulageTotalRoe),4);
                    }
                    else
                    {
                        $fDestinationHaulageTotalRoe_inverse = 0.0 ;		
                    }
                    $import_haulage_td3 = $fDestinationHaulageTotalRoe_inverse.'<br />
                    <br />';
                    $import_haulage_td4 = $bookingDataArr['szForwarderCurrency'].' '.number_format((float)($bookingDataArr['fDestinationHaulagePrice'] * $forwarderExchangeRate),2).'<br><br>';
                    $totalAmountValue =$totalAmountValue+round((float)($bookingDataArr['fDestinationHaulagePrice'] * $forwarderExchangeRate),2);
                    //echo $totalAmountValue."totalAmountValue".round((float)($bookingDataArr['fDestinationHaulagePrice'] * $forwarderExchangeRate),2)."<br />";
                } 
                
                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD , 3.WTD
                {
                    $origin_charge_td1 = 'Origin Charges<br /><font size="1" color="gray"><i>max('.$bookingDataArr['szOriginPortCurrency'].' '.$bookingDataArr['fOriginRateWM'].' x max ('.ceil($bookingDataArr['fCargoVolume']).'cbm;'.ceil($bookingDataArr['fCargoWeight']/1000).'mt);'.$bookingDataArr['szOriginPortCurrency'].' '.$bookingDataArr['fOriginMinRateWM'].') + '.$bookingDataArr['szOriginPortCurrency'].' '.$bookingDataArr['fOriginBookingRate'].'</i></font><br />';
                    $origin_charge_td2 = $bookingDataArr['szOriginPortCurrency'].' '.number_format((float)($bookingDataArr['fOriginPortPrice'] * $bookingDataArr['fOriginPortExchangeRate']),2).'<br />
                    <br />';

                    if($origin_charges_roe>0)
                    {
                            $origin_charges_roe_inverse = number_format((float)(1/$origin_charges_roe),4);
                    }
                    else
                    {
                            $origin_charges_roe_inverse = 0.0 ;		
                    }
                    $origin_charge_td3 = $origin_charges_roe_inverse.'<br />
                    <br />';
                    $origin_charge_td4 = $bookingDataArr['szForwarderCurrency'].' '.number_format((float)($bookingDataArr['fOriginPortPrice'] * $forwarderExchangeRate),2).'<br />
                    <br />';
                    
                    $totalAmountValue =$totalAmountValue+round((float)($bookingDataArr['fOriginPortPrice'] * $forwarderExchangeRate),2);
                    //echo $totalAmountValue."totalAmountValue".round((float)($bookingDataArr['fOriginPortPrice'] * $forwarderExchangeRate),2)."<br />";
                }

                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD , 3.WTD
                {
                    $destination_charge_td1 = 'Destination Charges<br /><font size="1" color="gray"><i>max('.$bookingDataArr['szDestinationPortCurrency'].' '.$bookingDataArr['fDestinationRateWM'].' x max ('.ceil($bookingDataArr['fCargoVolume']).'cbm;'.ceil($bookingDataArr['fCargoWeight']/1000).'mt);'.$bookingDataArr['szDestinationPortCurrency'].' '.$bookingDataArr['fDestinationMinRateWM'].') + '.$bookingDataArr['szDestinationPortCurrency'].' '.$bookingDataArr['fDestinationBookingRate'].'</i></font><br />';
                    $destination_charge_td2 = $bookingDataArr['szDestinationPortCurrency'].' '.number_format((float)($bookingDataArr['fDestinationPortPrice'] * $bookingDataArr['fDestinationPortExchangeRate']),2).'<br />
                    <br />';

                    if($destination_charge_roe>0)
                    {
                            $destination_charges_roe_inverse = number_format((float)(1/$destination_charge_roe),4);
                    }
                    else
                    {
                            $destination_charges_roe_inverse = 0.0 ;		
                    }
                    $destination_charge_td3 = $destination_charges_roe_inverse.'<br />
                    <br />';
                    $destination_charge_td4 = $bookingDataArr['szForwarderCurrency'].' '.number_format((float)($bookingDataArr['fDestinationPortPrice'] * $forwarderExchangeRate),2).'<br><br>';
                    
                     $totalAmountValue =$totalAmountValue+round((float)($bookingDataArr['fDestinationPortPrice'] * $forwarderExchangeRate),2);
                     //echo  $totalAmountValue."totalAmountValue".round((float)($bookingDataArr['fDestinationPortPrice'] * $forwarderExchangeRate),2)."<br />";
                }  
                
                if($bookingDataArr['iOriginCC']==1)
                {
                    $export_cc_td1 = 'Export Customs Clearance<br />';
                    if((float)$bookingDataArr['fOriginCCExchangeRate']>0)
                        $export_cc_td2 = $bookingDataArr['szOriginCCCurrency'].' '.number_format((float)($bookingDataArr['fOriginCCPrice']/$bookingDataArr['fOriginCCExchangeRate']),2).'<br>' ;
                    else
                        $export_cc_td2 = $bookingDataArr['szOriginCCCurrency'].' '.number_format((float)($bookingDataArr['fOriginCCPrice']),2).'<br>' ;	
                    if($originCCROE>0)
                    {
                        $originCCROE_inverse = number_format((float)(1/$originCCROE),4);
                    }
                    else
                    {
                        $originCCROE_inverse = 0.0 ;
                    } 
                    $export_cc_td3 = $originCCROE_inverse.'<br />';
                    $export_cc_td4 = $bookingDataArr['szForwarderCurrency'].' '.number_format((float)($bookingDataArr['fOriginCCPrice']*$forwarderExchangeRate),2).'<br />';
                    
                    $totalAmountValue =$totalAmountValue+round((float)($bookingDataArr['fOriginCCPrice'] * $forwarderExchangeRate),2);
                    //echo  $totalAmountValue."totalAmountValue".round((float)($bookingDataArr['fOriginCCPrice'] * $forwarderExchangeRate),2)."<br />";
                }		

                if($bookingDataArr['iDestinationCC']==2)
                {
                    $import_cc_td1 = 'Import Customs Clearance
                                                      <br />';
                    $import_cc_td2 = $bookingDataArr['szDestinationCCCurrency'].' '.number_format((float)($bookingDataArr['fDestinationCCPrice']/$bookingDataArr['fDestinationCCExchangeRate']),2).'<br>';
                    if($DestinationCCROE>0)
                    {
                        $DestinationCCROE_inverse = number_format((float)(1/$DestinationCCROE),4);
                    }
                    else
                    {
                        $DestinationCCROE_inverse = '0.00';	
                    }					
                    $import_cc_td3 = $DestinationCCROE_inverse.'<br />';
                    $import_cc_td4 = $bookingDataArr['szForwarderCurrency'].' '.number_format((float)($bookingDataArr['fDestinationCCPrice']*$forwarderExchangeRate),2).'<br />' ;
                    
                    $totalAmountValue =$totalAmountValue+round((float)($bookingDataArr['fDestinationCCPrice'] * $forwarderExchangeRate),2);
                    //echo  $totalAmountValue."totalAmountValue".round((float)($bookingDataArr['fDestinationCCPrice'] * $forwarderExchangeRate),2)."<br />";
                } 
                $grandtotal=($bookingDataArr['fOriginHaulagePrice'] * $forwarderExchangeRate)+($bookingDataArr['fOriginCCPrice']*$forwarderExchangeRate)+($bookingDataArr['fCfstoCfsPrice']*$forwarderExchangeRate)+($bookingDataArr['fDestinationCCPrice']*$forwarderExchangeRate)+($bookingDataArr['fDestinationHaulagePrice'] * $forwarderExchangeRate); 
                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__) 
                {
                    $fPTPPriceUSD = $bookingDataArr['fCfstoCfsPrice'] - ($bookingDataArr['fOriginPortPrice'] + $bookingDataArr['fDestinationPortPrice']);
                }
                else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__) 
                {
                    $fPTPPriceUSD = $bookingDataArr['fCfstoCfsPrice'] - $bookingDataArr['fDestinationPortPrice'];
                }
                else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__) 
                {
                    $fPTPPriceUSD = $bookingDataArr['fCfstoCfsPrice'] - $bookingDataArr['fOriginPortPrice'];
                }
                else
                {
                    $fPTPPriceUSD = $bookingDataArr['fCfstoCfsPrice'] ;
                } 
                $totalAmountValue =$totalAmountValue+round((float)($fPTPPriceUSD*$forwarderExchangeRate),2);
                //echo  $totalAmountValue."totalAmountValue".round((float)($fPTPPriceUSD*$forwarderExchangeRate),2)."<br />";
                $o=0;		
                if($export_haulage_td1!='')
                {	
                    $td_style="border-bottom:0px none;";
                    $szPricingDetailsString .='
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$export_haulage_td1.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$export_haulage_td2.'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$export_haulage_td3.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$export_haulage_td4.'</td>
                        </tr>
                    ';
                    ++$o;
                }
                if($export_cc_td1!='')
                {		 
                    if($o>0)
                    {
                        $td_style="border-top:0px none;border-bottom:0px none;";
                    }
                    else
                    {
                        $td_style="border-bottom:0px none;";
                    }
                    $szPricingDetailsString .= ' 
                            <tr>
                                <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'" border-bottom="0">'.$export_cc_td1.'<br /></td>
                                <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$export_cc_td2.'</td>
                                <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$export_cc_td3.'</td>
                                <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$export_cc_td4.'</td>
                            </tr>
                        ';
                        ++$o;
                }
                if($origin_charge_td1!='')
                { 
                    if($o>0)
                    {
                        $td_style="border-top:0px none;border-bottom:0px none;";
                    }
                    else
                    {
                        $td_style="border-bottom:0px none;";
                    }
                    $szPricingDetailsString .='
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'" border-bottom="0">'.$origin_charge_td1.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$origin_charge_td2.'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$origin_charge_td3.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$origin_charge_td4.'</td>
                        </tr>
                    ';
                    ++$o;
                } 
                if($import_cc_td1!='' || $import_haulage_td1!='' || $destination_charge_td1!='')	
                {
                    $td_style="border-top:0px none;border-bottom:0px none;";
                }
                else
                {
                    $td_style="border-top:0px none;";
                }
                $szPricingDetailsString .='
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">Freight and Surcharges<br /><font size="0.5" color="gray"><i>max('.$bookingDataArr['szFreightCurrency'].' '.$bookingDataArr['fRateWM'].' x max ('.ceil($bookingDataArr['fCargoVolume']).'cbm;'.ceil($bookingDataArr['fCargoWeight']/1000).'mt);'.$bookingDataArr['szFreightCurrency'].' '.$bookingDataArr['fMinRateWM'].') + '.$bookingDataArr['szFreightCurrency'].' '.$bookingDataArr['fBookingRate'].'</i></font></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$bookingDataArr['szFreightCurrency'].' '.number_format((float)($fPTPPriceUSD/$bookingDataArr['fFreightExchangeRate']),2).'</td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$cfs_to_cfs_roe_inverse.'</td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency'].' '.number_format((float)($fPTPPriceUSD*$forwarderExchangeRate),2).'</td>
                    </tr>
                '; 
                if($destination_charge_td1!='')	
                {
                    $szPricingDetailsString .='
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:0px none;'.$td_style.'">'.$destination_charge_td1.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$destination_charge_td2.'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$destination_charge_td3.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$destination_charge_td4.'</td>
                        </tr>
                    ';
                    ++$o; 
                }

                if($import_cc_td1!='')
                {	 
                    if($import_haulage_td1!='')	
                    {
                        $td_style="border-top:0px none;border-bottom:0px none;";
                    }
                    else
                    {
                        $td_style="border-top:0px none;";
                    }

                    $szPricingDetailsString .='
                        <tr>
                            <td width="60%" height="5%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$import_cc_td1.'<br /></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$import_cc_td2.'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$import_cc_td3.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;'.$td_style.'">'.$import_cc_td4.'</td>
                        </tr>
                    ';
                }
                if($import_haulage_td1!='')
                {	
                    $szPricingDetailsString .='
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:0px none;">'.$import_haulage_td1.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:0px none;">'.$import_haulage_td2.'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:0px none;">'.$import_haulage_td3.'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:0px none;">'.$import_haulage_td4.'</td>
                        </tr>
                    ';
                }
                $szPricingDetailsString .= $szPrivateCustomerFeeDetailsString."".$szHandlingFeeDetailsString."".$szHandlingFeeDetailsForForwarderString;
                
                if($bookingDataArr['fTotalVat']>0 && !$iHideHandlingFeeTotal)
                { 
                    $fTotalVatForwarderCurrency = 0;
                    $fTotalVatForwarderCurrencyUSD = $bookingDataArr['fTotalVat'] * $bookingDataArr['fExchangeRate'] ;
                    if($bookingDataArr['fForwarderExchangeRate']>0)
                    {
                        $fVatRoe = (($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate'])); 
                        if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'] && $bDisplayVatWithoutCurrencyMarkup)
                        {
                            $fVatRoe = $fVatRoe * 1.025 ;
                        }
                        
                        $fTotalVatForwarderCurrency = round((float)($bookingDataArr['fTotalVat']/$fVatRoe),2);    
                    } 
                    
                    $totalAmountValue =$totalAmountValue+$fTotalVatForwarderCurrency;
                    //echo $totalAmountValue."totalAmountValue";
                }
                $fTotalPriceForwarderCurrencyValue=round((float)($forwarderTransactionDetailAry[0]['fTotalPriceForwarderCurrency']),2);
                //echo $fTotalPriceForwarderCurrencyValue."fTotalPriceForwarderCurrencyValue";
                $totalAmountValue=$totalAmountValue-$fTotalPriceForwarderCurrencyValue;
                //echo $totalAmountValue."totalAmountValue";
                //$flag=true;
                $fAdjustmentPriceForwarderCurrency=round((float)$totalAmountValue,2);
                if((float)$fAdjustmentPriceForwarderCurrency!=0.00 && !$iHideHandlingFeeTotal)
                {
                    if($bookingDataArr['fForwarderExchangeRate']>0)
                    {
                        $fRoundAdjustRoe = (($bookingDataArr['fForwarderExchangeRate']/$bookingDataArr['fExchangeRate'])); 
                        if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'] && $bDisplayVatWithoutCurrencyMarkup)
                        {
                            $fRoundAdjustRoe = $fRoundAdjustRoe * 1.025 ;
                        }
                           
                    }
                    if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency/$fRoundAdjustRoe;
                    }
                    else
                    {
                        $fAdjustmentPriceCustomerCurrency=$fAdjustmentPriceForwarderCurrency;
                    }
                    if((float)$fAdjustmentPriceForwarderCurrency>0)
                    {
                        $fAdjustmentPriceForwarderCurrencyStr ="(".$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2).")";
                        $fAdjustmentPriceCustomerCurrencyStr ="(".$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2).")";
                    }
                    else
                    {
                        $fAdjustmentPriceForwarderCurrency= str_replace("-", "", $fAdjustmentPriceForwarderCurrency);
                        $fAdjustmentPriceForwarderCurrencyStr =$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fAdjustmentPriceForwarderCurrency,2);

                        $fAdjustmentPriceCustomerCurrency= str_replace("-", "", $fAdjustmentPriceCustomerCurrency);
                        $fAdjustmentPriceCustomerCurrencyStr =$bookingDataArr['szCurrency']." ".number_format((float)$fAdjustmentPriceCustomerCurrency,2);
                    }
                    $szPricingDetailsString .='	
                    <tr>
                        <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">Round off adjustment<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                        <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'"></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$fAdjustmentPriceForwarderCurrencyStr.'</td>
                    </tr>
                ';
                }
                if($bookingDataArr['fTotalVat']>0 && !$iHideHandlingFeeTotal)
                { 
                            
                    $szPricingDetailsString .='	
                        <tr>
                            <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'" border-bottom="0">VAT<br /></td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$bookingDataArr['szCurrency']." ".number_format((float)$bookingDataArr['fTotalVat'],2).'</td>
                            <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.number_format((float)$fVatRoe,4).'</td>
                            <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;border-top:none;'.$td_style.'">'.$bookingDataArr['szForwarderCurrency']." ".number_format((float)$fTotalVatForwarderCurrency,2).'</td>
                        </tr>
                    '; 
                } 
                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD , 2.DTW
                {
                    if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
                    {
                        $td_width = 25 ;
                    }
                    else
                    {
                        $td_width = 33 ;
                    }
						
                    $pickup_address_td1 = '
                        <td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                            <strong>Pick-up</strong> <font size="1" color="gray">('.number_format((float)$bookingDataArr['fOriginHaulageDistance']).' km from CFS)</font>
                        </td>
                    ';
                    $pickup_address_td2 = '   
                        <td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                            '.$szWareHouseFromStr.'<em style="font-size:11px;">Postcode coordinates:<br />('.$bookingDataArr['fOriginLongitude'].', '.$bookingDataArr['fOriginLatitude'].')</em>
                        </td>
                    ' ; 		
                    $pickup_address_td3 = '<td width="25%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">After '.$dtPickupAfter.' (local)</td>';
                }
                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD , 2.DTW
                {
                    if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
                    {
                        $td_width = 25 ;
                    }
                    else
                    {
                        $td_width = 33 ;
                    }

                    $delivery_address_td1 = ' 
                        <td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                            <strong>Delivery</strong> <font size="1" color="gray">('.number_format((float)$bookingDataArr['fDestinationHaulageDistance']).' km from CFS)</font>
                        </td>
                    ';
                    $delivery_address_td2 = '<td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"> '; 
                    $szWareHouseToStr = '';
                    if(!empty($bookingDataArr['szConsigneeAddress_pickup']) || !empty($bookingDataArr['szConsigneeAddress2_pickup']) || !empty($bookingDataArr['szConsigneeAddress2_pickup']))
                    {
                        $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeAddress_pickup'],$utf8Flag);
                        if(!empty($bookingDataArr['szConsigneeAddress2_pickup']))
                        {
                            $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2_pickup'],$utf8Flag);
                        }
                        if(!empty($bookingDataArr['szConsigneeAddress3_pickup']))
                        {
                            $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3_pickup'],$utf8Flag)."";
                        }
                        else
                        {
                            $szWareHouseToStr.="";
                        }
                    }
                    if(!empty($bookingDataArr['szConsigneePostCode_pickup']) || !empty($bookingDataArr['szConsigneeCity_pickup']))
                    {
                        $szWareHouseToStr.="<br />".html_entities_flag($bookingDataArr['szConsigneePostCode_pickup'],$utf8Flag);
                        if(!empty($bookingDataArr['szConsigneePostCode_pickup']))
                        {
                            $szWareHouseToStr.=" ".html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag)."<br />";
                        }
                        else
                        {
                            $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag)."<br />";
                        }
                    }
                    if(!empty($bookingDataArr['szConsigneeCountry_pickup']))
                    {
                        $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeCountry_pickup'],$utf8Flag)."<br>";
                    }
                    $delivery_address_td2 .=$szWareHouseToStr ;
                    $delivery_address_td2 .= '<em style="font-size:11px;">Postcode coordinates:<br />('.$bookingDataArr['fDestinationLongitude'].', '.$bookingDataArr['fDestinationLatitude'].')</em></td>';
                    $delivery_address_td3 = '<td width="25%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">Before '.$dtDeliveryBefore.'  (local)</td>';
                }
					
                if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_PTP__) //WTW
                {
                    $td_width = 50 ;
                }
					
                $szWarehouseString .=' 
                    <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" width="730" '.$align.'>
                        <tr>		
                            '.$pickup_address_td1.'					
                            <td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Origin CFS</strong></td>
                            <td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Destination CFS</strong></td>
                            '.$delivery_address_td1.'
                        </tr>
			<tr>
                            '.$pickup_address_td2.'
                            <td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                ' ;
									
                $szWareHouseFromStr = '';
                if(!empty($bookingDataArr['szWarehouseFromName']))
                {
                    $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromName'],$utf8Flag)."<br>";
                }
                if(!empty($bookingDataArr['szWarehouseFromAddress']) || !empty($bookingDataArr['szWarehouseFromAddress2']) || !empty($bookingDataArr['szWarehouseFromAddress3']))
                {
                    $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromAddress'],$utf8Flag);
                    if(!empty($bookingDataArr['szWarehouseFromAddress2']))
                    {
                        $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress2'],$utf8Flag);
                    }
                    if(!empty($bookingDataArr['szWarehouseFromAddress3']))
                    {
                        $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress3'],$utf8Flag);
                    }
                    else
                    {

                    }
                }
                if(!empty($bookingDataArr['szWarehouseFromPostCode']) || !empty($bookingDataArr['szWarehouseFromCity']))
                {
                    $szWareHouseFromStr.="<br />".html_entities_flag($bookingDataArr['szWarehouseFromPostCode'],$utf8Flag);
                    if($bookingDataArr['szWarehouseFromPostCode']!='')
                    {
                        $szWareHouseFromStr.=" ".html_entities_flag($bookingDataArr['szWarehouseFromCity'],$utf8Flag). "<br />";
                    }
                    else
                    {
                        $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromCity'],$utf8Flag). "<br />";
                    }
                }		

                if(!empty($bookingDataArr['szWarehouseFromCountry']) || !empty($bookingDataArr['szWarehouseFromState']))
                {
                    $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromCountry'],$utf8Flag)."<br>";
                }
                $szWarehouseString .= $szWareHouseFromStr ;
                $szWarehouseString .= '
                    </td>
                    <td width="'.$td_width.'%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">' ;

                $szWareHouseToStr='';
                if(!empty($bookingDataArr['szWarehouseToName']))
                {
                    $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToName'],$utf8Flag)."<br>";
                }
                if(!empty($bookingDataArr['szWarehouseToAddress']) || !empty($bookingDataArr['szWarehouseToAddress2']) || !empty($bookingDataArr['szWarehouseToAddress3']))
                {
                    $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToAddress'],$utf8Flag);
                    if(!empty($bookingDataArr['szWarehouseToAddress2']))
                    {
                        $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress2'],$utf8Flag);
                    }
                    if(!empty($bookingDataArr['szWarehouseToAddress3']))
                    {
                        $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress3'],$utf8Flag);
                    }	
                    else
                    {
                        $szWareHouseToStr.="";
                    }
                }
                if(!empty($bookingDataArr['szWarehouseToPostCode']) || !empty($bookingDataArr['szWarehouseToCity']))
                {
                    $szWareHouseToStr.="<br />".html_entities_flag($bookingDataArr['szWarehouseToPostCode'],$utf8Flag);
                    if($bookingDataArr['szWarehouseToPostCode']!='')
                    {
                        $szWareHouseToStr.=" ".html_entities_flag($bookingDataArr['szWarehouseToCity'],$utf8Flag)."<br />";
                    }
                    else
                    {
                        $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToCity'],$utf8Flag)."<br />";
                    }
                }						
                if(!empty($bookingDataArr['szWarehouseToCountry']) || !empty($bookingDataArr['szWarehouseToState']))
                {
                    $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToCountry'],$utf8Flag)."<br>";
                } 
                $szWarehouseString .= $szWareHouseToStr;
                $szWarehouseString .='</td>'.$delivery_address_td2.'</tr>
                        <tr>	
                                '.$pickup_address_td3.'						
                                <td width="25%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">Cut off on '.$dtWhsCutOff.' (local)</td>
                                <td width="25%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">Available '.$dtWhsAvailable.' (local)</td>
                                '.$delivery_address_td3.'
                        </tr>
                    </table>
                ' ;   
                
                $szNextStepString =  ' 
                    <table cellpadding="0" style="border-collapse: collapse;" cellspacing="0" border="0" width="730" '.$align.'>
                        <tr>
                            <td valign="top" style="font-family:Cambria;font-size:15px;text-align:center;" align="center"> 
                                Next steps: ';
                if((int)$bookingDataArr['idServiceProvider']==0)
                { 
                    if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__) // DTD=1 , DTW=2
                    {
                        $next_step_str =$bookingDataArr['szForwarderDispName']." will need to arrange for pick-up after ".$dtPickupAfter." (local) to meet cut off at ".$bookingDataArr['szWarehouseFromName']." on ".$dtWhsCutOff." (local).";
                    }
                    else
                    {
                        $next_step_str =$bookingDataArr['szShipperCompanyName']." will deliver the cargo at ".$bookingDataArr['szWarehouseFromName']." prior to cut off on ".$dtWhsCutOff;
                    }
                    $szNextStepString .= $next_step_str ;
                    $kForwarder = new cForwarder();
                    $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($bookingDataArr['idForwarder']); 
                    $customerServiceEmailLinkAry = format_fowarder_emails($customerServiceEmailAry) ;
                    $customerServiceEmailLink = $customerServiceEmailLinkAry[0];
                    $customerServiceEmailStr = $customerServiceEmailLinkAry[1];
                    $kForwarderContact->getMainAdmin($bookingDataArr['idForwarder']);
                } 
                else
                {
                    $szNextStepString .= $next_step_str ;
                }
            } 
        }    
        
        $pdf=new HTML2FPDFBOOKING();
        $pdf->AddPage('',true); 
        $bookingDownloadAry=$kBooking->getAcknowledgeDownloadedByUserDetails($idBooking,$bookingDataArr['idForwarder']);
        $download_text='';
        if(!empty($bookingDownloadAry))
        {
            $dtBookingDownload = (!empty($bookingDownloadAry[0]['dtDownloaded']) && ($bookingDownloadAry[0]['dtDownloaded']!='0000-00-00 00:00:00'))?date('d F Y',strtotime($bookingDownloadAry[0]['dtDownloaded'])):"" ;
            $dtBookingDownloadTime = (!empty($bookingDownloadAry[0]['dtDownloaded']) && ($bookingDownloadAry[0]['dtDownloaded']!='0000-00-00 00:00:00'))?date('H:i',strtotime($bookingDownloadAry[0]['dtDownloaded'])):"" ;
            $download_text=$dtBookingDownload." (".$dtBookingDownloadTime." GMT)
                                                            <br />".html_entities_flag($bookingDownloadAry[0]['szFirstName'],$utf8Flag)." ".html_entities_flag($bookingDownloadAry[0]['szLastName'],$utf8Flag)."
                                                            <br />".html_entities_flag($bookingDownloadAry[0]['szEmail'],$utf8Flag); 
        } 
        if($dtBookingConfirmedTime!='')
        {
            $dtBookingConfirmedTime="(".$dtBookingConfirmedTime." GMT)";
        }

        $kConfig_new = new cConfig();
        $kConfig_new->loadCountry($bookingDataArr['idCustomerDialCode']);
        $iInternationDialCode = $kConfig_new->iInternationDialCode;

        if($bookingDataArr['idShipperDialCode']>0)
        {
            $kConfig_new->loadCountry($bookingDataArr['idShipperDialCode']);
            $szShipperDialCodeStr = "+".$kConfig_new->iInternationDialCode." ";
        }

        if($bookingDataArr['idConsigneeDialCode']>0)
        {
            $kConfig_new->loadCountry($bookingDataArr['idConsigneeDialCode']);
            $szConsigneeDialCodeStr = "+".$kConfig_new->iInternationDialCode." ";
        }

        if(!empty($szTransportationModeShortName))
        {
            $szTransportationHeading = strtoupper($szTransportationModeShortName)." BOOKING";
        }
        else
        {
            $szTransportationHeading = "SEA BOOKING";
        } 
//<img src='.$image_url.'/images/Heading-LCL-Booking.jpg width="500" hieght="35">
    //html_entities_flag($bookingDataArr['szForwarderCity'],ENT_COMPAT, "UTF-8")
    $pdf_string .= '
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <title>Forwarder Booking</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        '.$script.'
        </head>	
        <body '.$body_info.'>
        '.$printStringStart.'
        <h1 style="TEXT-ALIGN:CENTER;color:grey;font-size:42px;font-weight:300;"><u>'.$szTransportationHeading.'</u></h1> 
        <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" width="730" '.$align.'>
            <tr>
                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><b>Reference</b>:<br>Received: <br> <br>Downloaded By: <br></td>
                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><b>'.html_entities_flag($bookingDataArr['szBookingRef'],$utf8Flag).'</b> <br>'.$dtBookingConfirmed.' '.$dtBookingConfirmedTime.' <br> <br>'.$download_text.'</td>
                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><b>User:</b><br />Reg. No.:<br>Address:'.$strbr.'Contact:<br>E-mail:<br>Phone:<br></td>
                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">'.html_entities_flag($bookingDataArr['szCustomerCompanyName'],$utf8Flag).'<br>'.html_entities_flag($bookingDataArr['szCustomerCompanyRegNo'],$utf8Flag).'<br>'.$customer_address.''.$customer_address_br.'<br>'.html_entities_flag($bookingDataArr['szFirstName'],$utf8Flag)." ".html_entities_flag($bookingDataArr['szLastName'],$utf8Flag).'<br>'.html_entities_flag($bookingDataArr['szEmail'],$utf8Flag).'<br>+'.$iInternationDialCode." ".$bookingDataArr['szCustomerPhoneNumber'].'</td>
            </tr> 
        </table><br>
        <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" width="730" '.$align.'>
            <tr>
                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Shipper:</strong> '.$shipperBrStr.'Name:<br>E-mail:<br>Phone:</td>
                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">'.$shipperAddress.html_entities_flag($bookingDataArr['szShipperFirstName'],$utf8Flag)." ".html_entities_flag($bookingDataArr['szShipperLastName'],$utf8Flag).'<br>'.html_entities_flag($bookingDataArr['szShipperEmail'],$utf8Flag).'<br>'.$szShipperDialCodeStr."".$bookingDataArr['szShipperPhone'].'</td>
                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><b>Consignee:</b>'.$consigneeBrStr.'Name:<br>E-mail:<br>Phone:</td>
                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">'.$ConsigneeAddress.html_entities_flag($bookingDataArr['szConsigneeFirstName'],$utf8Flag).' '.html_entities_flag($bookingDataArr['szConsigneeLastName'],$utf8Flag).'<br>'.html_entities_flag($bookingDataArr['szConsigneeEmail'],$utf8Flag).'<br>'.$szConsigneeDialCodeStr."".$bookingDataArr['szConsigneePhone'].'</td>
            </tr>
        </table><br>			
        <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" width="730" '.$align.'>
            <tr>
                <td colspan="4" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Transportation Service and Pricing</strong></td>
            </tr>
            <tr>
                <td colspan="4" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;" height="30">Service: '.$szTransportationService.'</td>
            </tr>
        ';           
        if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        {
            $pdf_string .= $szPricingDetailsString; 
            $pdf_string .= '
                <br>
                <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" width="730" '.$align.'>
                    <tr>
                        <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Cargo</strong></td>
                    </tr>
                    <tr>
                        <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"> '.$szCargoFullDetails.' </td>
                    </tr>
                </table>
                <br>
                <br> 
            ';

            $pdf_string .=  ' 
                    <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="0" width="730" '.$align.'>
                        <tr>
                            <td valign="top" style="font-family:Cambria;font-size:15px;text-align:center;" align="center">Transporteca\'s Terms & Conditions apply to this booking, as agreed and available to you on your Transporteca Control Panel. Contact your administrator on '.$szForwarderAdministratorEmails.' if in doubt.</td>
                        </tr>
                        '.$imagetr.'
                    </table>
                    <br>
                    <br> 
                    <br>
                    '.$printStringEnd.'
            ';  
        }
        else
        {
            $pdf_string .= ' 
                <tr>
                    <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>'.$szPriceTitle.'</strong></td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Local Currency</strong></td>
                    <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>ROE</strong></td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Your Currency</strong></td>
                </tr>' ; 
            $pdf_string .= $szPricingDetailsString; 
            $rateExchangeText='';
            if($bookingDataArr['idCustomerCurrency']!=$bookingDataArr['idForwarderCurrency'])
            {
                $rateExchangeText='
                    <tr>
                        <td colspan="4" valign="top" style="font-family:Cambria;font-size:11px;background:#fff;">*If other currency than your bank currency, ROE includes '.number_format((float)($bookingDataArr['fMarkupPercentage']),2).'% to cover any currency exchange cost, as per bank tariff.</td>
                    </tr>';
            } 
            if(!$iHideHandlingFeeTotal){
            $pdf_string .='
                <tr>
                    <td width="60%" valign="top" style="font-family:Cambria;font-size:12px;background:#fff;"><strong>Customer Invoice '.$bookingDataArr['szInvoice'].' (Paid with '.$iPaymentType.')</strong></td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.$bookingDataArr['szCurrency'].' '.number_format((float)($bookingDataArr['fTotalPriceCustomerCurrency']+$bookingDataArr['fTotalVat']),2).'</td>
                    <td width="10%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.number_format((float)$forwarderTransactionDetailAry[0]['fExchangeRate'],4).'</td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:12px;background:#fff;">'.$bookingDataArr['szForwarderCurrency'].' '.number_format((float)($forwarderTransactionDetailAry[0]['fTotalPriceForwarderCurrency']),2).'</td>
                        </tr>';
            }
                $pdf_string .= $rateExchangeText.'
                </table>
                <br>
            ';       
            $pdf_string .= $szWarehouseString;
            $pdf_string .= '
                <br>
                <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" width="730" '.$align.'>
                    <tr>
                        <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Cargo</strong></td>
                    </tr>
                    <tr>
                        <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"> '.$szCargoFullDetails.' </td>
                    </tr>
                </table> 
                <br><br> 
            '; 
            $pdf_string .= $szNextStepString; 
            $pdf_string .=' 
                    <br><br>
                            Transporteca\'s Terms & Conditions apply to this booking, as agreed and available to you on your Transporteca Control Panel. Contact your administrator on '.$kForwarderContact->mainAdminEmail.' if in doubt.
                        </td>
                    </tr>
                    '.$imagetr.'
                </table>
                <br><br><br>
                '.$printStringEnd.'
            ' ;
        }  
        if($flag==true)
        {							
            echo $pdf_string;
        }
        else
        {	
            class forwarderConfirmation extends HTML2FPDFBOOKING
            {
                    function Footer()
                    {
                                   $this->SetY(-10);
                              //Copyright //especial para esta vers
                              $this->SetFont('Arial','B',9);
                                  $this->SetTextColor(0);
                              //Arial italic 9
                              $this->SetFont('Arial','B',9);
                              //Page number
                              $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
                              //Return Font to normal
                              $this->SetFont('Arial','',11);
                    }
            }
            $pdf = new forwarderConfirmation();
            $pdf->AddPage('',true);
            $pdf->WriteHTML($pdf_string,true);

            $file_name = __APP_PATH__."/invoice/Forwarder-Confirmation-".$bookingDataArr['szBookingRef'].".pdf";
            $file = "Forwarder-Confirmation.pdf";

            $pdf->Output($file_name,'F');
            return $file_name;
        }
    }
}
function getOriinDestinationAddress($bookingDataArr)
{
    if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_PTD__) // 1-DTD , 3-WTD
    {					
        if(!empty($bookingDataArr['szConsigneeAddress_pickup']))
        {
             $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeAddress_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szConsigneeAddress2_pickup']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szConsigneeAddress3_pickup']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szConsigneePostCode_pickup']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneePostCode_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szConsigneeCity_pickup']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag);						
        }
        if(!empty($bookingDataArr['szConsigneeCountry_pickup']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeCountry_pickup'],$utf8Flag)."<br />";
        }
    }
    else
    {
        if(!empty($bookingDataArr['szWarehouseToName']))
        {
             $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToName'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseToAddress']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseToAddress2']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress2'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseToAddress3']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress3'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseToPostCode']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToPostCode'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseToCity']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToCity'],$utf8Flag);						
        }
        if(!empty($bookingDataArr['szWarehouseToCountry']))
        {
             $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToCountry'],$utf8Flag)."<br />";
        }	
    }
    
    if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTP__) // 1-DTD , 2-DTW
    {
        if(!empty($bookingDataArr['szShipperAddress_pickup']))
        {
            $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperAddress_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szShipperAddress2_pickup']) )
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress2_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szShipperAddress3_pickup']) )
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress3_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szShipperPostCode_pickup']))
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperPostCode_pickup'],$utf8Flag);
        }						
        if(!empty($bookingDataArr['szShipperCity_pickup']))
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperCity_pickup'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szShipperCountry_pickup']))
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperCountry_pickup'],$utf8Flag)."<br />";
        }
    }
    else
    {
        if(!empty($bookingDataArr['szWarehouseFromName']))
        {
            $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromName'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseFromAddress']))
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseFromAddress2']) )
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress2'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseFromAddress3']) )
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress3'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseFromPostCode']))
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromPostCode'],$utf8Flag);
        }						
        if(!empty($bookingDataArr['szWarehouseFromCity']))
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromCity'],$utf8Flag);
        }
        if(!empty($bookingDataArr['szWarehouseFromCountry']))
        {
            $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromCountry'],$utf8Flag)."<br />";
        }
    }
    
    $retAry = array();
    $retAry['szWareHouseFromStr'] = $szWareHouseFromStr;
    $retAry['szWareHouseToStr'] = $szWareHouseToStr;
    return $retAry;
}

function getCargoTexts($bookingDataArr)
{
    $kBooking = new cBooking();
    $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($bookingDataArr['id'],true);
    $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag) ;
    
    $idBooking = $bookingDataArr['id'];
    $cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
    $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',',');
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__ && $bookingDataArr['idServiceProvider']>0)
    { 
        $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,true); 
        $szCargoFullDetails='Cargo: <br />';
        $total=count($cargoDetailArr);
        if(!empty($cargoDetailArr))
        { 
            $ctr=0;
            $totalQuantity=0;
            $kCourierServices = new cCourierServices(); 
            $packingTypeArr = array();
            $packingTypeArr = $kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1); 

            foreach($cargoDetailArr as $cargoDetailArrs)
            { 
                $totalQuantity = $totalQuantity + $cargoDetailArrs['iColli'];
                $t=$total-1; 
                if((int)$cargoDetailArrs['iQuantity']>1)
                {
                    if($cargoDetailArrs['szCommodity']!='')
                    {
                        $szPackingSingle = $cargoDetailArrs['szCommodity'];
                    }
                    else
                    {
                        $szPackingSingle = $packingTypeArr[0]['szPacking'];
                    }
                    $quantityText= number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($szPackingSingle);
                }
                else
                {
                    if($cargoDetailArrs['szCommodity']!='')
                    {
                        $szPackingSingle = $cargoDetailArrs['szCommodity'];
                    }                    
                    else
                    {
                        if(!empty($packingTypeArr[0]['szSingle']))
                        {
                            $szPackingSingle = $packingTypeArr[0]['szSingle'];
                        }
                        else
                        {
                            $szPackingSingle = $packingTypeArr[0]['szPackingSingle'];
                        }
                    }
                    $quantityText= number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($szPackingSingle);
                } 

                if($ctr==0)
                {
                    $szCargoFullDetails .=$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)." ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                }
                else
                {
                    $szCargoFullDetails .="<br />".$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."  ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                }
                $szCargoFullDetails .= ", ".$cargoDetailArrs['iColli']." colli ";
                ++$ctr;	
            }
            if($totalQuantity>1)
            {
                $textPacking=strtolower($packingTypeArr[0]['szPacking']);
            } 
            else
            {
                if(!empty($packingTypeArr[0]['szSingle']))
                {
                    $szPackingSingle = $packingTypeArr[0]['szSingle'];
                }
                else
                {
                    $szPackingSingle = $packingTypeArr[0]['szPackingSingle'];
                }
                $textPacking=strtolower($szPackingSingle);   
            } 
            $totalQuantityText='';
            if($totalQuantity>0)
            {
                $totalQuantityText=number_format($totalQuantity)." ".$textPacking.", ";
            }
            $szCargoFullDetails .= "<br />Total: ".$totalQuantityText."".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity;
        } 
        $cargoText = $szCargoFullDetails; 
    }
    else if($bookingDataArr['iSearchMiniVersion']==7 || $bookingDataArr['iSearchMiniVersion']==8 || $bookingDataArr['iSearchMiniVersion']==9)//Voga Landing Page
    { 
        
        $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,true); 
        
        $szCargoFullDetails='Cargo: <br />';
        $total=count($cargoDetailArr);
        if(!empty($cargoDetailArr))
        { 
            $ctr=0;
            $totalQuantity=0;
            foreach($cargoDetailArr as $cargoDetailArrs)
            {  
                $totalQuantity = $totalQuantity + $cargoDetailArrs['iColli'];
                $szVogaPackingType = ($cargoDetailArrs['szCommodity']);
                $t=$total-1;  
                $quantityText = number_format((int)$cargoDetailArrs['iQuantity'])." ".$szVogaPackingType;
                $addTextFlag=false;
                if($ctr==0)
                {
                    
                    $szCargoFullDetails .=$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)." ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                    
                }
                else
                {
                    $addTextFlag=true;
                    $szCargoFullDetails .="<br />".$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."  ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                    
                }
               
                $szCargoFullDetails .= ", ".$cargoDetailArrs['iColli']." colli ";
                
                ++$ctr;	
            }  
            
        }
        $totalQuantity=$bookingDataArr['iNumColli'];
        $szVogaPackingType = 'colli';
        $totalQuantityText='';
        if((int)$totalQuantity>0)
        {
           $totalQuantityText= number_format($totalQuantity)." ".$szVogaPackingType.", ";
        }
        $szCargoFullDetails .= "<br />Total: ".$totalQuantityText."".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity;
        $cargoText = $szCargoFullDetails;
    }
    else
    { 
        $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,true); 
        if(!empty($cargoDetailArr) && $bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__)
        { 
            $cargoText='';
            $total=count($cargoDetailArr);
            if(!empty($cargoDetailArr))
            { 
                $ctr=0;
                $totalQuantity=0;  
                foreach($cargoDetailArr as $cargoDetailArrs)
                { 
                    $totalQuantity = $totalQuantity + $cargoDetailArrs['iColli'];
                    $t=$total-1;
                    $szPackingSingle = $cargoDetailArrs['szCommodity'];
                    $quantityText= number_format((int)$cargoDetailArrs['iQuantity'])." ".($szPackingSingle); 
                    if($ctr==0)
                    {
                        $fHeightText='';
                        if((float)$cargoDetailArrs['fHeight']==0.00)
                        {
                            $fHeightText="max ";
                            $cargoDetailArrs['fHeight']=__HEIGHT_PER__PALLET_COLI__;
                        }
                        $cargoText .=$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".$fHeightText."".round_up($cargoDetailArrs['fHeight'],1)." ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                    }
                    else
                    {
                        $fHeightText='';
                        if((float)$cargoDetailArrs['fHeight']==0.00)
                        {
                            $fHeightText="max ";
                            $cargoDetailArrs['fHeight']=__HEIGHT_PER__PALLET_COLI__;
                        }
                        $cargoText .="<br />".$quantityText." of ".round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".$fHeightText."".round_up($cargoDetailArrs['fHeight'],1)."  ".$cargoDetailArrs['cmdes']." (L x W x H), ".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                    }
                    if(!empty($cargoDetailArrs['szSellerReference']))
                    {
                        $cargoText .= " (".$cargoDetailArrs['szSellerReference'].")";
                    }
                    $cargoText .= ", ".$cargoDetailArrs['iColli']." colli "; 
                    ++$ctr;	
                } 
                $totalQuantityText='';
                if($totalQuantity>0)
                {
                    $totalQuantityText=number_format($totalQuantity)." colli, ";
                }
                $cargoText .= "<br />Total: ".$totalQuantityText."".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity;
            } 
        }
        else
        {
            $showColliTextFlag=false;
            if((int)$bookingDataArr['iNumColli']>0)
            {
                $showColliTextFlag=true;
            }
            if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__ || $showColliTextFlag)
            {
                $cargoText = 'Cargo: Maximum '.$cargo_volume." cbm, ".$cargo_weight." kg, ".$bookingDataArr['iNumColli']." colli ".$szCargoCommodity;  
            }
            else
            {
                $cargoText = 'Cargo: Maximum '.$cargo_volume." cbm, ".$cargo_weight." kg, ".$szCargoCommodity;
            } 
        }
    }
    $retAry = array(); 
    $retAry['szCargoText'] = $cargoText;
    return $retAry;
}
function getBookingConfirmationPdfFileHTML($idBooking,$flag=false,$utf8Flag=false,$bDebug=false,$save_pdf_file=false)
{  
    $iTcpdfFlag = 2; 
    if($iTcpdfFlag==1)
    {
        require_once(__APP_PATH__.'/tcpdf/tcpdf.php'); 
        // create new PDF document
        $pdf = new cBookingPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
    }
    else
    {
        require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php'); 
    }
    if(!class_exists('cForwarderContact'))
    { 
        require_once(__APP_PATH_CLASSES__."/config.class.php");
        require_once(__APP_PATH_CLASSES__."/booking.class.php");
        require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
        require_once(__APP_PATH_CLASSES__."/user.class.php");
        require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
        require_once(__APP_PATH_CLASSES__."/forwarderContact.class.php"); 
        require_once(__APP_PATH_CLASSES__."/pdfDocuments.class.php"); 
    }
	
    $kBooking = new cBooking();
    $kForwarder = new cForwarder();
    $kForwarderContact = new cForwarderContact();
    $kConfig = new cConfig(); 

    if($iTcpdfFlag==1)
    { 
        //set page margins left top right
	$pdf->SetMargins(15, 15, 15);
	
	// set auto page breaks and bottom margin
	$pdf->SetAutoPageBreak(FALSE, 2);
        // set page font
        $pdf->SetFont('freeserif', 'NN', 8); 
        
        $pdf->AddPage();
        // add a page
        //$pdf->AddPage();
    } 
    else
    {
        $pdf=new HTML2FPDFBOOKING();
        $pdf->AddPage('',true);
    } 

    $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
    if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
    }
    if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
    }
    if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
    }
    if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
    }
    
    $forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
  
    $kConfig_new = new cConfig();
    if($bookingDataArr['idShipperDialCode']>0)
    {
        $kConfig_new->loadCountry($bookingDataArr['idShipperDialCode']);
        $szShipperDialCodeStr = "+".$kConfig_new->iInternationDialCode." ";
    }

    if($bookingDataArr['idConsigneeDialCode']>0)
    {
        $kConfig_new->loadCountry($bookingDataArr['idConsigneeDialCode']);
        $szConsigneeDialCodeStr = "+".$kConfig_new->iInternationDialCode." ";
    }
    $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).'<br />';
    if($bookingDataArr['szForwarderAddress']!='')
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderAddress'],$utf8Flag).'<br />';
    if($bookingDataArr['szForwarderAddress2']!='')	
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderAddress2'],$utf8Flag).' <br />';
    if($bookingDataArr['szForwarderPostCode']!='')
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderPostCode'],$utf8Flag).' <br />';
    if($bookingDataArr['szForwarderCity']!='')	
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderCity'],$utf8Flag).' <br />';

    $forwarderDetails .=html_entities_flag($forwardCountry,$utf8Flag);
    $strbr=0;
    if($bookingDataArr['szShipperCompanyName']!='')
    {
        $shipperDetails .=html_entities_flag($bookingDataArr['szShipperCompanyName'],$utf8Flag).' <br />';
        ++$strbr;
    }
    if($bookingDataArr['szShipperAddress']!='')
    {
        $shipperDetails .=html_entities_flag($bookingDataArr['szShipperAddress'],$utf8Flag);
    }		
    if($bookingDataArr['szShipperAddress2']!='')
    {	
        $shipperDetails .=', '.html_entities_flag($bookingDataArr['szShipperAddress2'],$utf8Flag);
    }
    if($bookingDataArr['szShipperAddress3']!='')
    {	
        $shipperDetails .=', '.html_entities_flag($bookingDataArr['szShipperAddress3'],$utf8Flag).' <br />';
        ++$strbr;	
    }
    else
    {
        $shipperDetails .=' <br />';			
        ++$strbr;
    }	
    if($bookingDataArr['szShipperPostCode']!='')
    {
        $shipperDetails .=html_entities_flag($bookingDataArr['szShipperPostCode'],$utf8Flag);
    }

    if($bookingDataArr['szShipperCity']!='')
    {	
        ++$strbr;
        if(!empty($bookingDataArr['szShipperPostCode']))
        {
            $shipperDetails .=' '.html_entities_flag($bookingDataArr['szShipperCity'],$utf8Flag).' <br />';
        }
        else
        {
            $shipperDetails .=html_entities_flag($bookingDataArr['szShipperCity'],$utf8Flag).' <br />';
        }
    }
    else
    {
        $shipperDetails .='<br />';
        ++$strbr;
    }	
    if($bookingDataArr['szShipperCountry']!='')
    {	
        $shipperDetails .=html_entities_flag($bookingDataArr['szShipperCountry'],$utf8Flag).'<br />';
        ++$strbr;
    }	 
    
	if($strbr>0)
	{	
            $strshipperbr='';
            for($i=0;$i<$strbr;++$i)
            {
                if($strshipperbr=='')
                {
                    $strshipperbr="<br />";
                }
                else
                {
                    $strshipperbr ="<br /> ".$strshipperbr;
                }
            }
	}
	$ctrcon=0;
	if($bookingDataArr['szConsigneeCompanyName']!='')
	{
		$consigneeDetails .=html_entities_flag($bookingDataArr['szConsigneeCompanyName'],$utf8Flag).'<br />';
		++$ctrcon;
	}
	if($bookingDataArr['szConsigneeAddress']!='')
		$consigneeDetails .=html_entities_flag($bookingDataArr['szConsigneeAddress'],$utf8Flag);
	
	if($bookingDataArr['szConsigneeAddress2']!='')	
		$consigneeDetails .=', '.html_entities_flag($bookingDataArr['szConsigneeAddress2'],$utf8Flag);
				
	if($bookingDataArr['szConsigneeAddress3']!='')	
	{
		$consigneeDetails .=', '.html_entities_flag($bookingDataArr['szConsigneeAddress3'],$utf8Flag).'<br />';
		++$ctrcon;
	}
	else
	{
		$consigneeDetails .='<br />';
		++$ctrcon;
	}
	if($bookingDataArr['szConsigneePostCode']!='')	
		$consigneeDetails .=html_entities_flag($bookingDataArr['szConsigneePostCode'],$utf8Flag);
	 
	if($bookingDataArr['szConsigneeCity']!='')	
	{
		++$ctrcon;
		if($bookingDataArr['szConsigneePostCode']!='')		
			$consigneeDetails .=' '.html_entities_flag($bookingDataArr['szConsigneeCity'],$utf8Flag).'<br />';
		else
			$consigneeDetails .=html_entities_flag($bookingDataArr['szConsigneeCity'],$utf8Flag).'<br />';
	}
	else
	{
		++$ctrcon;
		$consigneeDetails .='<br />';
	}		
	if($bookingDataArr['szConsigneeCountry']!='')	
	{	
            ++$ctrcon;
            $consigneeDetails .=html_entities_flag($bookingDataArr['szConsigneeCountry'],$utf8Flag).'<br />';
	}
	else
	{
            ++$ctrcon;
            $consigneeDetails .='<br />';	
	}  
	if($ctrcon>0)
	{	
            $strconsigneebr='';
            for($i=0;$i<$ctrcon;++$i)
            {
                if($strconsigneebr=='')
                {
                    $strconsigneebr="<BR>";
                }
                else
                {
                    $strconsigneebr ="<BR> ".$strconsigneebr;
                }
            }
	}
	$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
	if(!empty($cargoDetailArr))
	{
		$ctr=0;
		foreach($cargoDetailArr as $cargoDetailArrs)
		{
			//$str .=++$ctr.": ".number_format(ceil($cargoDetailArrs['iQuantity']))."x".number_format(ceil($cargoDetailArrs['fLength']))."x".number_format(ceil($cargoDetailArrs['fWidth']))."x".number_format(ceil($cargoDetailArrs['fHeight']))."".$cargoDetailArrs['cmdes'].", ".number_format(ceil($cargoDetailArrs['fWeight']))." ".ucwords($cargoDetailArrs['wmdes']).", ".html_entities_flag($cargoDetailArrs['szCommodity'],$utf8Flag).'<br />';	
		}
	}	
	
	if(!empty($bookingDataArr['dtCutOff']))
	{
		$pickUpDate=date('d F Y',strtotime($bookingDataArr['dtCutOff']));
	}
	if(!empty($bookingDataArr['dtAvailable']))
	{
		$deliveryDate=date('d F Y',strtotime($bookingDataArr['dtAvailable']));
	}
	
	if(!empty($bookingDataArr['dtWhsCutOff']))
	{
		$dtWhsCutOff=date('d F Y',strtotime($bookingDataArr['dtWhsCutOff']));
	}
	
        $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($bookingDataArr['id'],true);
        $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);
        
	$cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
        $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',','); 
        
        /*
        * Building cargo string for different types of bookings
        */ 
        $cargoTextAry = getCargoTexts($bookingDataArr);  
        $szCargoFullDetails = $cargoTextAry['szCargoText'];
        $cargoText = $cargoTextAry['szCargoText']; 
 
	$fCargoWeight=$bookingDataArr['fCargoWeight']/1000; 
	
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTP__) // 1-DTD , 2-DTW
	{
            if(!empty($bookingDataArr['szShipperAddress_pickup']) || !empty($bookingDataArr['szShipperAddress2_pickup']) || !empty($bookingDataArr['szShipperAddress2_pickup']))
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperAddress_pickup'],$utf8Flag);
                if(!empty($bookingDataArr['szShipperAddress2_pickup']))
                {
                        $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress2_pickup'],$utf8Flag);
                }

                if(!empty($bookingDataArr['szShipperAddress3_pickup']))
                {
                        $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress3_pickup'],$utf8Flag)."<br />";
                }
                else
                {
                        $szWareHouseFromStr.="<br />";
                }
            }
            if(!empty($bookingDataArr['szShipperPostCode_pickup']) || !empty($bookingDataArr['szShipperCity_pickup']))
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperPostCode_pickup'],$utf8Flag);
                if($bookingDataArr['szShipperPostCode_pickup']!='')
                    $szWareHouseFromStr.=" ".html_entities_flag($bookingDataArr['szShipperCity_pickup'],$utf8Flag)."<br />";
                else
                    $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperCity_pickup'],$utf8Flag)."<br />";
            }
            if(!empty($bookingDataArr['szShipperCountry_pickup']))
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperCountry_pickup'],$utf8Flag)."<br />";
            }
	}
	else
	{
            if(!empty($bookingDataArr['szWarehouseFromName']))
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromName'],$utf8Flag)."<br />";
            }
            if(!empty($bookingDataArr['szWarehouseFromAddress']) || !empty($bookingDataArr['szWarehouseFromAddress2']) || !empty($bookingDataArr['szWarehouseFromAddress3']))
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromAddress'],$utf8Flag);
                if(!empty($bookingDataArr['szWarehouseFromAddress2']))
                {
                        $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress2'],$utf8Flag);
                } 
                if(!empty($bookingDataArr['szWarehouseFromAddress3']))
                {
                        $szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress3'],$utf8Flag)."<br/>";
                }
                else
                {
                        $szWareHouseFromStr.="<br />";
                }
            }
            if(!empty($bookingDataArr['szWarehouseFromPostCode']) || !empty($bookingDataArr['szWarehouseFromCity']))
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromPostCode'],$utf8Flag);
                if($bookingDataArr['szWarehouseFromPostCode']!='')
                    $szWareHouseFromStr.=" ".$bookingDataArr['szWarehouseFromCity']."<br />";
                else
                    $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromCity'],$utf8Flag)."<br />";
            }						
            if(!empty($bookingDataArr['szWarehouseFromCountry']) || !empty($bookingDataArr['szWarehouseFromState']))
            {
                $szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromCountry'],$utf8Flag)."<br />";
            }
	} 
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_PTD__) // 1-DTD , 3-WTD
	{					
            if(!empty($bookingDataArr['szConsigneeAddress_pickup']) || !empty($bookingDataArr['szConsigneeAddress2_pickup']) || !empty($bookingDataArr['szConsigneeAddress2_pickup']))
            {
                //$szWareHouseToStr.=$bookingDataArr['szConsigneeAddress_pickup'].", ".$bookingDataArr['szConsigneeAddress2_pickup'].", ".$bookingDataArr['szConsigneeAddress3_pickup']."<br />";
                $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeAddress_pickup'],$utf8Flag);
                if($bookingDataArr['szConsigneeAddress2_pickup']!='')
                    $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2_pickup'],$utf8Flag);
                if($bookingDataArr['szConsigneeAddress3_pickup']!='')
                    $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3_pickup'],$utf8Flag)."<br />";
                else
                    $szWareHouseToStr.="<br />"; 
            }
            if(!empty($bookingDataArr['szConsigneePostCode_pickup']) || !empty($bookingDataArr['szConsigneeCity_pickup']))
            {
                $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneePostCode_pickup'],$utf8Flag);
                if($bookingDataArr['szConsigneePostCode_pickup']!='')
                    $szWareHouseToStr.=" ".html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag)."<br />";
                else
                    $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag)."<br />";
            }
            if(!empty($bookingDataArr['szConsigneeCountry_pickup']))
            {
                $szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeCountry_pickup'],$utf8Flag)."<br />";
            }
	}
	else
	{
            if(!empty($bookingDataArr['szWarehouseToName']))
            {
                $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToName'],$utf8Flag)."<br />";
            }
            if(!empty($bookingDataArr['szWarehouseToAddress']) || !empty($bookingDataArr['szWarehouseToAddress2']) || !empty($bookingDataArr['szWarehouseToAddress3']))
            {
                $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToAddress'],$utf8Flag);
                if($bookingDataArr['szWarehouseToAddress2']!='')
                    $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress2'],$utf8Flag);
                if($bookingDataArr['szWarehouseToAddress3']!='')
                    $szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress3'],$utf8Flag)."<br />";
                else
                    $szWareHouseToStr.="<br />";
            }
            if(!empty($bookingDataArr['szWarehouseToPostCode']) || !empty($bookingDataArr['szWarehouseToCity']))
            {
                $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToPostCode'],$utf8Flag);
                if($bookingDataArr['szWarehouseToPostCode']!='')
                    $szWareHouseToStr.=" ".html_entities_flag($bookingDataArr['szWarehouseToCity'],$utf8Flag)."<br />";
                else
                    $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToCity'],$utf8Flag)."<br />";
            }						
            if(!empty($bookingDataArr['szWarehouseToCountry']) || !empty($bookingDataArr['szWarehouseToState']))
            {
                $szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToCountry'],$utf8Flag)."<br />";
            }	
	}
	
	$kForwarder->load($bookingDataArr['idForwarder']);  
	if($flag==true)
	{
            $image_url=__MAIN_SITE_HOME_PAGE_URL__;
	}
	else
	{
            $image_url=__APP_PATH_ROOT__;
	}

	if($kForwarder->szPdfLogo!='')
	{
		$wh='';
		$logoPath=__APP_PATH__."/images/forwarders/thumb/150x60_".$kForwarder->szPdfLogo;
		if(file_exists($logoPath))
		{
			$wh='width="70" hieght="30"';
			$logoPath=$image_url."/images/forwarders/thumb/150x60_".$kForwarder->szPdfLogo;
		}
		else
		{
			$logoPath=__APP_PATH__."/images/forwarders/".$kForwarder->szPdfLogo;
			if(file_exists($logoPath))
			{
				$wh='width="70" hieght="30"';
				$logoPath=$image_url."/images/forwarders/".$kForwarder->szPdfLogo;
			}
			else
			{
				$logoPath='';
			}
		}
	}
	 
	$kForwarderContact->getMainAdmin($bookingDataArr['idForwarder']);
	$bookingServiceEmailArr=$kForwarder->getForwarderCustomerServiceEmail($bookingDataArr['idForwarder'],__CUSTOMER_PROFILE_ID__);
	if(!empty($bookingServiceEmailArr))
	{
            //$strbookingServiceEmailArr=implode(",",$bookingServiceEmailArr);
            $iTotalCounter = count($bookingServiceEmailArr);
            $ctr=0;
            foreach($bookingServiceEmailArr as $customerServiceEmailArys)
            {
                if(empty($customerServiceEmailStr))
                {
                    $customerServiceEmailStr = html_entities_flag($customerServiceEmailArys,$utf8Flag) ;
                    $customerServiceEmailLink = $customerServiceEmailArys;
                }
                else
                {
                    $customerServiceEmailLink .= ', '.$customerServiceEmailArys;

                    if($ctr>0 && $ctr==($iTotalCounter-1))
                    {
                        $customerServiceEmailStr .= ' and '.html_entities_flag($customerServiceEmailArys,$utf8Flag);
                    }
                    else
                    {
                        $customerServiceEmailStr .= ', '.html_entities_flag($customerServiceEmailArys,$utf8Flag);
                    }
                }							
                $ctr++;
            }
	}
        
	$serviceString="Freight from";
	$str_date='';
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
	{
            $serviceString .=" door at origin to door at destination (EXW)";
            $str_date="Pick-up";
           if($bookingDataArr['iShipperConsignee']==1){
                $pickupline="Expected pick-up ".$pickUpDate." will be coordinated directly with the shipper.";
                $deliveryLine="Expected delivery ".$deliveryDate." will be coordinated directly with the consignee.";
            }else{
            $pickupline="Expected pick-up after ".$pickUpDate." will be coordinated directly with the shipper.";
            $deliveryLine="Expected delivery before ".$deliveryDate." will be coordinated directly with the consignee.";
            }
            $pickAddressText="Pick-up<br />address";
            $deliveryAddressText="Delivery<br />address";
            $nextstepText=$bookingDataArr['szForwarderDispName']." will coordinate pick-up of cargo with ".$bookingDataArr['szShipperCompanyName'].".";
	} 
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__)
	{
            $serviceString .=" port at origin to door at destination (FOB)";
            $str_date="Cut-Off";
            $pickupline="Shipper must deliver cargo at origin warhouse latest ".$pickUpDate." (local).";
            $deliveryLine="Expected delivery before ".$deliveryDate." (local) will be coordinated directly with the consignee.";
            $pickAddressText="Origin<br />Warehouse";
            $deliveryAddressText="Delivery<br />address";
            $nextstepText=$bookingDataArr['szShipperCompanyName']." must hand over cargo to ".$bookingDataArr['szForwarderDispName']." at the origin warehouse latest ".$pickUpDate."."; 
	}
        else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__)
	{
            $serviceString .=" door at origin to door at destination (EXW)";
            $str_date="Pick-up";
             if($bookingDataArr['iShipperConsignee']==1 && $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__){
                $pickupline="Expected pick-up ".$pickUpDate." will be coordinated directly with the shipper.";
               
            }else{
            $pickupline="Expected pick-up after ".$pickUpDate." will be coordinated directly with the shipper.";
            //$deliveryLine="Expected delivery before ".$deliveryDate." will be coordinated directly with the consignee.";
           
            }
            //$deliveryLine="Expected delivery before ".$deliveryDate." will be coordinated directly with the consignee.";
            $deliverytimetext="Expected cargo availability ".$deliveryDate;	
            $pickAddressText="Pick-up<br />address";
            $deliveryAddressText="Destination<br />Warehouse";
            $nextstepText=$bookingDataArr['szForwarderDispName']." will coordinate pick-up of cargo with ".$bookingDataArr['szShipperCompanyName']."."; 
	}
        
        if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
        {
            $pickupline="Expected pick-up on ".$pickUpDate.", or earlier, will be coordinated directly with the shipper.";
            $deliveryLine="Expected delivery on ".$deliveryDate." will be coordinated directly with the consignee.";
        } 
        
	$cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($bookingDataArr['id'],true);
	$szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag) ;
	$kConfig = new cConfig();
        
        if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        {
            if($bookingDataArr['idServiceTerms']>0)
            {
                $serviceTermsAry = array();
                $serviceTermsAry = $kConfig->getAllServiceTerms($bookingDataArr['idServiceTerms']);
                $idServiceType = $serviceTermsAry[0]['idServiceType'];
                $bookingDataArr['szServiceDescription'] = $serviceTermsAry[0]['szLongTerms']; 
            } 
            $szTransportationService = display_service_type_description($bookingDataArr,$iLanguage,true);
            $szServiceDescription = "Service: ".$szTransportationService;  
            
            
            $transportModeListAry = array();
            $transportModeListAry = $kConfig->getAllTransportMode($bookingDataArr['idTransportMode']);
            $szShippingMode = $transportModeListAry[0]['szLongName'];
            
            $kConfig = new cConfig();
            $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_TRANSPORT_MODE__');
            $bookingDataArr['szFrequency'] =$configLangArr[$bookingDataArr['idTransportMode']]['szFrequency'];               
//            if($bookingDataArr['iBookingLanguage']==__LANGUAGE_ID_DANISH__)
//            {
//                $bookingDataArr['szFrequency'] = $transportModeListAry[0]['szFrequencyDanish']; 
//            }
//            else
//            {
//                $bookingDataArr['szFrequency'] = $transportModeListAry[0]['szFrequency']; 
//            }
            
            $szOtherComments = $bookingDataArr['szOtherComments'];
             
            $szServiceDescription .="<br /><br />Mode of transport: ".$szShippingMode."<br />Transit time: ".$bookingDataArr['iTransitHours']." days<br />Frequency: ".$bookingDataArr['szFrequency']."<br />" ;
            if(!empty($szOtherComments))
            {
                $szServiceDescription .= "<br />".$szOtherComments.". <br /> ";
            } 
        }
        else
        {
           $szServiceDescriptionString = display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true); 
           $szServiceDescription =  $szServiceDescriptionString.'<br /><br /><br/>'.$pickupline.'<br />'.$deliveryLine ;
           $width = 'width="720"';
           $szWarehouseDetailsText = '
			<table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" '.$width.' '.$align.'>
                            <tr>
                                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                    <strong>'.$pickAddressText.':</strong>
                                </td>
                                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                    '.$szWareHouseFromStr.'
                                </td>
                                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.$deliveryAddressText.':</strong></td>
                                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                    '.$szWareHouseToStr.'
                                </td>
                            </tr>
			</table>';
            if($flag==true)
            {
                $szNextStepRows = '
                    <tr>
                        <td>
                        <p style="TEXT-ALIGN:CENTER;FONT-SIZE:MEDIUM;" width="600">
                        Next steps: '.$nextstepText.'
                        </p>
                        </td>
                    </tr>';
            }
            else
            {
                $szNextStepRows = '<p style="TEXT-ALIGN:CENTER;FONT-SIZE:MEDIUM;">
				Next steps: '.$nextstepText.'
				</p> ' ;
            }
        }
        
	if($flag==true)
	{
		$body_info='style="background:#000;text-align:center;"';
		$width='width="720"';
		$width1='width="720"';
		$align='align="center"';
		$bodyfontsize='style="font-size:12px;"';
		$printStringStart='<p style="text-align:right;margin:5px auto 10px;width:760px;">
		<a href='.__BASE_URL__.'/downloadBookingConfirmation/'.$idBooking.'/ target="_blank" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>&nbsp;&nbsp;&nbsp;
		<a href="javascript:void(0)" onclick="PrintDiv();" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a></p>
		<div id="viewBookingConfimation" style="background:#fff;margin:auto;padding:10px 20px;width:720px;text-align:left;">';
		$printStringEnd='</div>';
		$script='<script>
		function PrintDiv()
		{    
			      var divToPrint = document.getElementById("viewBookingConfimation");
			      var popupWin = window.open("", "_blank", "width=900,height=700");
			      popupWin.document.open();
			      popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
			      popupWin.document.close();
		}
		</script>';
		$greyText='<font color="gray" size="1">(Line: Number of units x L x W x H, weight, description)</font>';
	}
	else
	{
		$printStringStart='';
		$width='width="720"';
		$width1='width="720"';
		$align='';
		$bodyfontsize='';
		$script='';
		$printStringEnd='';
		$greyText='<font color="#a5a5a5"; size="1"><small><i>(Line: Number of units x L x W x H, weight, description)</i></small></font>';
	}
	
	if(__ENVIRONMENT__ =="LIVE")
	{
		$show_url=substr(__MAIN_SITE_HOME_PAGE_URL__,8);
	}
	else
	{
		$show_url=substr(__MAIN_SITE_HOME_PAGE_URL__,7);
	}
	$kWHSSearch = new cWHSSearch(); 
	
	$iLanguage = getLanguageId();
//	if($iLanguage==__LANGUAGE_ID_DANISH__)
//	{
//		$szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK_DANISH__' ;
//	}
//	else
//	{
//		$szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK__' ;
//	}
        $szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK__' ;
	if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
        {
               $kCourierServices= new cCourierServices();
               $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($idBooking);
               if((int)$newCourierBookingAry[0]['iCourierAgreementIncluded']==1)
                       $labelBy=$newCourierBookingAry[0]['szDisplayName']." will setup the booking and make labels";
               //else
                      // $labelBy="Transporteca";

               if(!empty($bookingDataArr['dtCutOff']))
               {
                       $requestPickUpDate=date('d M Y',strtotime($bookingDataArr['dtCutOff']));
               }
               if(!empty($bookingDataArr['dtAvailable']))
               {
                       $expdeliveryDate=date('d M Y',strtotime($bookingDataArr['dtAvailable']));
               }		
               $szServiceDescription=''; 
               $szServiceDescription="Door-to-door courier service with ".$newCourierBookingAry[0]['szProviderName']." ".$newCourierBookingAry[0]['szProviderProductName'].", ".$labelBy."  including tracking.<br />Requested pick-up: ".$requestPickUpDate."<br />Expected delivery: ".$expdeliveryDate; 
        } 
         
        //$shipperDetails = " A <br> B<br> C <br> D <br> E <br> F";
	$explainPageLink=$kWHSSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
        /*
         * <table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="0" '.$width.' '.$align.'>
                <tr><td width="100%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">This is a confirmation that your booking has been placed with '.$bookingDataArr['szForwarderDispName'].'. You will receive a notification by e-mail when '.$bookingDataArr['szForwarderDispName'].' has acknowledged receipt.</td></tr>
        </table>
         */
        
        $headingUrl=$image_url.'/images/Heading_Booking_Confirmation.jpg'; 
	$string .='
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<title>Booking Confirmation</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			'.$script.'						
			</head>
			<body '.$body_info.'>
			'.$printStringStart;
                        if($flag==true)
                        {
                            $string .='<p style="TEXT-ALIGN:CENTER;"><img src="'.$headingUrl.'" width="590" hieght="35"></p>';
                        }
			$string .='<table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" '.$width.'  '.$align.'>
				<tr nobr="true">
                                    <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                        <b>Reference:</b>
                                        <br />Date (Time):<br />Booked By:
                                    </td>
                                    <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                        <strong>'.$bookingDataArr['szBookingRef'].'</strong>
                                        <br />'.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).' ('.date('H:i',strtotime($bookingDataArr['dtBookingConfirmed'])).' GMT)<br />'.html_entities_flag($bookingDataArr['szFirstName'], ENT_QUOTES,'UTF-8').' '.html_entities_flag($bookingDataArr['szLastName'], ENT_QUOTES,'UTF-8').'<br />'.html_entities_flag($bookingDataArr['szEmail'], ENT_QUOTES,'UTF-8').'
                                    </td>
                                    <td width="15%" height="17%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                        <strong>Freight Forwarder:</strong>
                                        
                                    </td>
                                    <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                        '.$forwarderDetails.'
                                    </td>
				</tr>
			</table>
			<table cellpadding="3" style="border-collapse: collapse;border:" cellspacing="0" border="1" '.$width.'   '.$align.'>
                            <tr nobr="true">
                                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;border-right:solid 1px #000;"><strong>Shipper:</strong></td>
                                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;border-right:solid 1px #000;">'.$shipperDetails.'</td>
                                <td width="15%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;border-right:solid 1px #000;"><strong>Consignee:</strong></td>
                                <td width="35%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">'.$consigneeDetails.'</td>
                            </tr>
                            <tr>
                                <td style="font-family:Cambria;font-size:14px;background:#fff;border-right:solid 1px #000;border-top: none;">Name:<br />E-mail:<br />Phone:</td>
                                <td style="font-family:Cambria;font-size:14px;background:#fff;border-right:solid 1px #000;border-top: none;">'.html_entities_flag($bookingDataArr['szShipperFirstName'],$utf8Flag).' '.html_entities_flag($bookingDataArr['szShipperLastName'],$utf8Flag).'<br />'.html_entities_flag($bookingDataArr['szShipperEmail'],$utf8Flag).'<br />'.$szShipperDialCodeStr."".html_entities_flag($bookingDataArr['szShipperPhone'],$utf8Flag).'</td>
                                <td style="font-family:Cambria;font-size:14px;background:#fff;border-right:solid 1px #000;border-top: none;">Name:<br />E-mail:<br />Phone:</td>
                                <td style="font-family:Cambria;font-size:14px;background:#fff;border-right:solid 1px #000;border-top: none;">'.html_entities_flag($bookingDataArr['szConsigneeFirstName'],$utf8Flag).' '.html_entities_flag($bookingDataArr['szConsigneeLastName'],$utf8Flag).'<br />'.html_entities_flag($bookingDataArr['szConsigneeEmail'],$utf8Flag).'<br />'.$szConsigneeDialCodeStr."".html_entities_flag($bookingDataArr['szConsigneePhone'],$utf8Flag).'</td>
                            </tr>
                        </table>
			<table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" '.$width.' '.$align.'>
                            <tr>
                                <td colspan="4" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                    <strong>Transportation Service Description</strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                    '.$szServiceDescription.'<br /> 				
                                </td>
                            </tr>
			</table>
			<table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="1" '.$width.' '.$align.'>
				<tr>
					<td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
					<b>Cargo</b>
					</td>
				</tr>';
                            if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
                            {
                                $string .='<tr>
					<td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"> 
						<br />'.$szCargoFullDetails.'</td></tr>
                                </table>';
                            }
                            else
                            { 
				$string .='<tr>
					<td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"> 
						<br />'.$szCargoFullDetails.'</td></tr>
			</table>
                        '.$szWarehouseDetailsText.'
			<BR>';	
                            }
		if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
                {
                     $shipperDetailsArr=$kCourierServices->getShipperNameById($bookingDataArr['idShipperConsignee'],true); 
                     $szNextStepRows =$shipperDetailsArr['szShipperCompanyName']." will receive shipping labels and shipping instruction before ".date('d M Y',strtotime($bookingDataArr['dtCutOff']));
                    $string .= '<p style="TEXT-ALIGN:CENTER;FONT-SIZE:MEDIUM;">Next steps: '.$szNextStepRows.'</p>'; 
                }
                else
                {
                    if($flag==true)
                    {			
                        $string .='<table cellpadding="3" style="border-collapse: collapse;" cellspacing="0" border="0"   '.$align.' >
                                '.$szNextStepRows.'
                                <tr>
                                        <td>
                                                <p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;" width="600">For any questions related to this booking, please write to the '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' customer service on '.$customerServiceEmailStr.'. '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' reserves the right to reject the shipment if you have submitted inaccurate information in placing this booking. This booking was placed with the '.html_entities_flag($bookingDataArr['szForwarderDispName'],$utf8Flag).' on www.transporteca.com. The booking is subject the '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' standard terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).'.</p>
                                        </td>
                            </tr>
                                <tr>
                                        <td align="right"><img src="/images/Powered_By.jpg" width="170"></td>
                                </tr>
                            </table>';
                    }
                    else
                    {
                        $string .= $szNextStepRows.' 
                        <p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;">For any questions related to this booking, please write to the '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' customer service on '.$customerServiceEmailStr.'. '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' reserves the right to reject the shipment if you have submitted inaccurate information in placing this booking. This booking was placed with the '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' on www.transporteca.com. The booking is subject the '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' standard terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).'.</p>';

                    }
                }
		$string .= $printStringEnd;
		$string .='	<br /></body>
			</html>
			';
                //$flag=true; 
	if($flag==true)
	{		
            echo $string;
	}
	else
	{
            if($iTcpdfFlag==1)
            { 
                
               // $pdf->AddPage('L');
                // output the HTML content
                $pdf->writeHTML($string, true, false, true, false, '');

                // reset pointer to the last page
                //$pdf->lastPage();
            }
            else
            {
                class bookingConfirmation extends HTML2FPDFBOOKING
                {
                        function Footer()
                        {
                                       $this->SetY(-10);
                                  //Copyright //especial para esta vers
                                  $this->SetFont('Arial','B',9);
                                      $this->SetTextColor(0);
                                  //Arial italic 9
                                  $this->SetFont('Arial','B',9);
                                  //Page number
                                  $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
                                  //Return Font to normal
                                  $this->SetFont('Arial','',11);
                        }
                }
                $pdf = new bookingConfirmation();
                $pdf->AddPage('',true);
                $pdf->Image($headingUrl,15,10,180,10);        
                
                $pdf->WriteHTML($string,true);
            } 

            $file_name = __APP_PATH__."/invoice/Booking-Confirmation-".$bookingDataArr['szBookingRef'].".pdf";
            $szFileName = "Booking-Confirmation-".$bookingDataArr['szBookingRef'].".pdf"; 
            $pdf->Output($file_name,'F');
    
            //$pdf->save($file_name);
            if($save_pdf_file)
            { 
                $ret_ary = array();
                $ret_ary['szFileName'] = $szFileName ;
                $ret_ary['szFilePath'] = $file_name ;
                return $ret_ary ;
            }
            else
            {
                return $file_name;
            } 
	}
}
function getInvoiceConfirmationPdfFileHTML($idBooking,$flag=false,$utf8Flag=false,$view_invoice_pdf=false,$save_invoices=false)
{  
    require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php'); 
    if(!class_exists('cForwarderContact'))
    { 
        require_once(__APP_PATH_CLASSES__."/config.class.php");
        require_once(__APP_PATH_CLASSES__."/booking.class.php");
        require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
        require_once(__APP_PATH_CLASSES__."/user.class.php");
        require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
        require_once(__APP_PATH_CLASSES__."/forwarderContact.class.php"); 
    }

    $kBooking = new cBooking();
    $kForwarder = new cForwarder();
    $kForwarderContact = new cForwarderContact();
    $kConfig = new cConfig();
    $kUser = new cUser();
    $kWHSSearch = new cWHSSearch();
	
	 
    $iLanguage = getLanguageId();
//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    {
//        $szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK_DANISH__' ;
//    }
//    else
//    {
//        $szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK__' ;
//    }
    $szVariableName = '__TRANSPORTECA_EXPLAIN_PAGE_LINK__' ;
    $explainPageLink=$kWHSSearch->getManageMentVariableByDescription($szVariableName,$iLanguage);
	 
    $pdf=new HTML2FPDFBOOKING();
    $pdf->AddPage('',true);

    $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
 
    if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
    }
    if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
    }
    if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
    }
    if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
    {
        $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
    }
    
    $forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);

    $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).'<br />';
    if($bookingDataArr['szForwarderAddress']!='')
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderAddress'],$utf8Flag).'<br />';
    if($bookingDataArr['szForwarderAddress2']!='')	
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderAddress2'],$utf8Flag).'<br />';
    if($bookingDataArr['szForwarderPostCode']!='')
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderPostCode'],$utf8Flag).'<br />';
    if($bookingDataArr['szForwarderCity']!='')	
            $forwarderDetails .= html_entities_flag($bookingDataArr['szForwarderCity'],$utf8Flag).'<br />';


    $kForwarder->load($bookingDataArr['idForwarder']);   
	 
    //$kForwarderContact->getMainAdmin($bookingDataArr['idForwarder']);
    $bookingServiceEmailArr=$kForwarder->getForwarderCustomerServiceEmail($bookingDataArr['idForwarder'],__PAYMENT_AND_BILLING_PROFILE_ID__);
    $iTotalCounter=count($bookingServiceEmailArr);
    if(!empty($bookingServiceEmailArr))
    {
        foreach($bookingServiceEmailArr as $customerServiceEmailArys)
        {
            if(empty($customerServiceEmailStr))
            {
                $customerServiceEmailStr = $customerServiceEmailArys ;
                $customerServiceEmailLink = $customerServiceEmailArys;
            }
            else
            {
                //$customerServiceEmailLink .= ', '.$customerServiceEmailArys;

                if($ctr>0 && $ctr==($iTotalCounter-1))
                {
                    $customerServiceEmailStr .= ' and '.$customerServiceEmailArys;
                }
                else
                {
                    $customerServiceEmailStr .= ', '.$customerServiceEmailArys;
                }
            }							
            $ctr++;
        }
    }
    
    $kWhsSearch = new cWHSSearch();  
    $customerServiceEmailStr = $kWhsSearch->getManageMentVariableByDescription('__FINANCE_CONTACT_EMAIL__'); 
         
    $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($bookingDataArr['id'],true);
    $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag) ;
    
    $cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
    $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',',');
    
    /*
    * Building cargo string for different types of bookings
    */
    $cargoTextAry = getCargoTexts($bookingDataArr);  
    $cargoText = $cargoTextAry['szCargoText']; 

    $warehouseAddressAry = getOriinDestinationAddress($bookingDataArr); 
    $szWareHouseFromStr = $warehouseAddressAry['szWareHouseFromStr'];
    $szWareHouseToStr = $warehouseAddressAry['szWareHouseToStr'];
        
    $serviceString="Freight from";
    $str_date='';
    if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
    {
        $serviceString .=" door at origin to door at destination (DTD)";
        $pickAddressText="Pick-up address";
        $deliveryAddressText="Delivery";
        if($bookingDataArr['iShipperConsignee']==1){
            $picktimetext="Expected pick-up";
        }else{
        $picktimetext="Expected pick-up time";
        }
        $deliverytimetext="Expected delivery";

    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__)
    {
        $serviceString .=" door at origin to warehouse at destination (DTW)";
        $pickAddressText="Pick-up address";
        $deliveryAddressText="Destination Warehouse";	
        $picktimetext="Expected pick-up time";
        $deliverytimetext="Expected cargo availability";	
    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__)
    {
        $serviceString .=" warehouse at origin to door at destination (WTD)";
        $pickAddressText="Origin Warehouse";
        $deliveryAddressText="Delivery";		
        $picktimetext="Cargo cut off";
        $deliverytimetext="Expected delivery";
    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__)
    {
        $serviceString .=" warehouse at origin to warehouse at destination (WTW)";
        $pickAddressText="Origin Warehouse";
        $deliveryAddressText="Destination Warehouse";
        $picktimetext="Cargo cut off";
        $deliverytimetext="Expected cargo availability";
    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__)
    {
        $serviceString .=" door at origin to port at destination (DTP)";
        $pickAddressText="Pick-up address";
        $deliveryAddressText="Destination Warehouse";	
        if($bookingDataArr['iShipperConsignee']==1){
            $picktimetext="Expected pick-up";
        }else{
        $picktimetext="Expected pick-up time";
        } 
        $deliverytimetext="Expected cargo availability";	
    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__)
    {
        $serviceString .=" port at origin to door at destination (PTD)";
        $pickAddressText="Origin Warehouse";
        $deliveryAddressText="Delivery";		
        $picktimetext="Cargo cut off";
        $deliverytimetext="Expected delivery";
    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTP__)
    {
        $serviceString .=" port at origin to port at destination (PTP)";
        $pickAddressText="Origin Warehouse";
        $deliveryAddressText="Destination Warehouse";
        $picktimetext="Cargo cut off";
        $deliverytimetext="Expected cargo availability";
    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__)
    {
        $serviceString .=" port at origin to warehouse at destination (PTW)";
        $pickAddressText="Origin Warehouse";
        $deliveryAddressText="Destination Warehouse";
        $picktimetext="Cargo cut off";
        $deliverytimetext="Expected cargo availability";
    }
    else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__)
    {
        $serviceString .=" warehouse at origin to port at destination (WTP)";
        $pickAddressText="Origin Warehouse";
        $deliveryAddressText="Destination Warehouse";
        $picktimetext="Cargo cut off";
        $deliverytimetext="Expected cargo availability";
    }

    if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
    {
        //$serviceString .=" door at origin to port at destination (DTP)";
        $picktimetext="Expected pick-up";
        //$deliveryAddressText="Destination Warehouse";	
        //$picktimetext="Expected pick-up";
        $deliverytimetext="Expected delivery";
    }

    if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
    {
        $picktimetext="Expected pick-up";
        $deliverytimetext="Expected delivery"; 
        $deliveryAddressText="Delivery address";
    } 
    else
    {
        $fCargoWeight=$bookingDataArr['fCargoWeight']/1000;	 
    }

    if($bookingDataArr['iOriginCC']==1 && $bookingDataArr['iDestinationCC']==0)
    {
        $serviceString .=" and customs clearance at origin";
    }

    if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==0)
    {
            $serviceString .=" and customs clearance at destination";
    }

    if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==1)
    {
            $serviceString .=" and customs clearance at origin and destination";
    }
    if($bookingDataArr['iDestinationCC']==0 && $bookingDataArr['iOriginCC']==0)
    {
            $serviceString .=". Customs clearance not included";
    }
    if($flag==true)
    {
            $image_url=__MAIN_SITE_HOME_PAGE_URL__;
    }
    else
    {
            $image_url=__APP_PATH_ROOT__;
    }
	if($kForwarder->szPdfLogo!='')
	{
		$logoPath=__APP_PATH_ROOT__."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
		if(file_exists($logoPath))
		{
			$logoPath=$image_url."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
		}
		else
		{
			$logoPath=$image_url."/images/forwarders/".$kForwarder->szPdfLogo;
		}
	}
	
	if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
	{
            if(!empty($bookingDataArr['dtCutOff']))
            {
                $pickUpDate=date('j F',strtotime($bookingDataArr['dtCutOff']));
            }
            if(!empty($bookingDataArr['dtAvailable']))
            {
                $deliveryDate=date('j F',strtotime($bookingDataArr['dtAvailable']));
            }
	}
	else
	{
            if(!empty($bookingDataArr['dtCutOff']))
            {
                if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
                {
                    $pickUpDate=date('d F Y',strtotime($bookingDataArr['dtCutOff']));
                }
                else
                {
                    $pickUpDate=date('d F Y',strtotime($bookingDataArr['dtCutOff']));
                }
            }
            if(!empty($bookingDataArr['dtAvailable']))
            {
                if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
                {
                    $deliveryDate=date('d F Y',strtotime($bookingDataArr['dtAvailable']));
                }
                else
                {
                    $deliveryDate=date('d F Y',strtotime($bookingDataArr['dtAvailable']));
                }
            } 	
	}
	$kUser->getUserDetails($bookingDataArr['idUser']); 
	       	
        $billcountry=$kConfig->getCountryName($bookingDataArr['szCustomerCountry']);
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerCompanyName'],$utf8Flag)."<br />";         	
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerAddress1'],$utf8Flag);
	
        $kConfig_new = new cConfig();
        $kConfig_new->loadCountry($bookingDataArr['idCustomerDialCode']);
        $iInternationDialCode = $kConfig_new->iInternationDialCode;

	if(!empty($bookingDataArr['szCustomerAddress2']))
            $billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress2'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerAddress3']))
            $billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress3'],$utf8Flag);			
	if(!empty($bookingDataArr['szCustomerPostCode']))
            $billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerPostCode'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerCity']))
	{
            if(!empty($bookingDataArr['szCustomerPostCode']))
                $billingAddress .=" ".html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
            else
                $billingAddress .=html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
	}
	if(!empty($bookingDataArr['szCustomerState']))
            $billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerState'],$utf8Flag);
	if(!empty($billcountry))
            $billingAddress .=", ".html_entities_flag($billcountry,$utf8Flag);
	else
            $billingAddress .="<br />";
	//It will change zooz api work is finish///		
	///$bookingDataArr['fCustomerPaidAmount']=$bookingDataArr['fTotalPriceCustomerCurrency'];		
	 
	if($bookingDataArr['iInsuranceIncluded']==1)
	{
            $fCustomerPaidAmount = ($bookingDataArr['fTotalPriceCustomerCurrency']) ;
            $totalPaidAmount=number_format((float)($fCustomerPaidAmount),2);
	}
	else
	{
            $totalPaidAmount=number_format((float)($bookingDataArr['fTotalPriceCustomerCurrency']),2);
            $fCustomerPaidAmount = $bookingDataArr['fTotalPriceCustomerCurrency'] ;
	}
	
	$totalUnpaidAmount1 = ($bookingDataArr['fTotalPriceCustomerCurrency'] - $fCustomerPaidAmount);
	if((float)$totalUnpaidAmount1<0)
	{
            $totalUnpaidAmount1=str_replace("-","",$totalUnpaidAmount1);
            $totalUnpaidAmountStr="(".html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.number_format((float)$totalUnpaidAmount1,2).")";
	}
	else
	{
            $totalUnpaidAmountStr=html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.number_format((float)$totalUnpaidAmount1,2);
	}
	
	$fTotalPriceCustomerCurrency=number_format((float)$bookingDataArr['fTotalPriceCustomerCurrency'],2);
        $fCustomerCurrencyPrice = $bookingDataArr['fTotalPriceCustomerCurrency'];
        
	$fCargoWeight=$bookingDataArr['fCargoWeight']/1000;	
	 
        if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        {
            if($bookingDataArr['idServiceTerms']>0)
            {
                $serviceTermsAry = array();
                $serviceTermsAry = $kConfig->getAllServiceTerms($bookingDataArr['idServiceTerms']);
                $idServiceType = $serviceTermsAry[0]['idServiceType'];
                
                if($idServiceType>0)
                {
                    $serviceTypeAry = array();
                    $serviceTypeAry = $kConfig->getAllServiceTypes($idServiceType,$bookingDataArr['iBookingLanguage']); 
                    
                    /*if($bookingDataArr['iBookingLanguage']==2) //danish
                    {
                        $bookingDataArr['szServiceDescription'] = $serviceTypeAry[0]['szDescriptionDanish'];
                    } 
                    else
                    {*/
                        $bookingDataArr['szServiceDescription'] = $serviceTypeAry[0]['szDescription'];
                    //}
                } 
            } 
            $serviceString = $bookingDataArr['szServiceDescription'] ;
            $serviceString = display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true);
            
            $shipperAddress = '';
            if(!empty($bookingDataArr['szShipperCompanyName']))
            {
                $shipperAddress.= html_entities_flag($bookingDataArr['szShipperCompanyName']);
            }  
            if(!empty($bookingDataArr['szShipperAddress_pickup']))
            {
                $shipperAddress.= ", ".html_entities_flag($bookingDataArr['szShipperAddress_pickup']);
            }
            if(!empty($bookingDataArr['szShipperAddress2']))
            {
                $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress2']) ;
            }
            if(!empty($bookingDataArr['szShipperAddress3']))
            {
                $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress3']);
            }
            if(!empty($bookingDataArr['szShipperPostCode']) || !empty($bookingDataArr['szShipperCity']))
            {
                if(!empty($bookingDataArr['szShipperPostCode']) && !empty($bookingDataArr['szShipperCity']))
                {
                    $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperPostCode'])." ".html_entities_flag($bookingDataArr['szShipperCity'])."";
                }
                else if(!empty($bookingDataArr['szShipperPostCode']))
                {
                    $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperPostCode']);
                }
                else if(!empty($bookingDataArr['szShipperCity']))
                {
                    $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperCity']);
                }
            }
            
            $ConsigneeAddress = '';
            if(!empty($bookingDataArr['szConsigneeCompanyName']))
            {
                $ConsigneeAddress.= html_entities_flag($bookingDataArr['szConsigneeCompanyName']).", ";
            }  
            if(!empty($bookingDataArr['szConsigneeAddress']))
            {
                $ConsigneeAddress.= html_entities_flag($bookingDataArr['szConsigneeAddress']);
            }
            if(!empty($bookingDataArr['szConsigneeAddress2']))
            {
                $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2']);
            }
            if(!empty($bookingDataArr['szConsigneeAddress3']))
            {
                $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3']);
            }
            if(!empty($bookingDataArr['szConsigneePostCode']) || !empty($bookingDataArr['szConsigneeCity']))
            {
                $ConsigneeAddress.=", ".html_entities_flag($bookingDataArr['szConsigneePostCode'])." ".html_entities_flag($bookingDataArr['szConsigneeCity'])."";
            } 
            
            $kConfig = new cConfig();
            $transportModeListAry = array();
            $transportModeListAry = $kConfig->getAllTransportMode($bookingDataArr['idTransportMode'],1);
            $szShippingMode = $transportModeListAry[0]['szLongName'];
                            
            $szOtherComments = $bookingDataArr['szOtherComments'];
            
            $szWarehouseDetails = "<br /><br />Shipper: ".$shipperAddress." <br />Consignee: ".$ConsigneeAddress ;
            $szWarehouseDetails .="<br />Mode of transport: ".$szShippingMode ;
            if(!empty($szOtherComments))
            {
                $szWarehouseDetails .= "<br /><p>".$szOtherComments."</p><br />" ;
            } 
            else
            {
                $szWarehouseDetails .= "<br />";
            }
            
            if($bookingDataArr['fTotalVat']>0)
            {
                $szVatString = '
                    <tr>
                        <td valign="top" style="border-top:0px;font-size:14px;font-family:Cambria;background:#fff;">
                            VAT 
                        </td>
                        <td  valign="top" align="right" style="border-top:0px;font-size:14px;font-family:Cambria;background:#fff;" >'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag(number_format((float)$bookingDataArr['fTotalVat'],2),$utf8Flag).'</td>
                    </tr> 
		'; 
                $fTotalPriceCustomerCurrency = number_format((float)($fCustomerCurrencyPrice + $bookingDataArr['fTotalVat']),2);
            }  
        }
        else
        {
            if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
            {
                $szWarehouseDetails = '<br />'.$pickAddressText.': '.$szWareHouseFromStr.'<br />
                <br />'.$deliveryAddressText.': '.$szWareHouseToStr.'<br />';
            }
            else
            {
                $orearlierText='';    
                if($bookingDataArr['idTransportMode']!=__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
                {
                    $orearlierText=", or earlier";
                }
                if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
                {
                    $szWarehouseDetails = '<br />'.$pickAddressText.': '.$szWareHouseFromStr.''.$picktimetext.': '.$pickUpDate.''.$orearlierText.' (will be coordinated with shipper)<br />
            	<br />'.$deliveryAddressText.': '.$szWareHouseToStr.''.$deliverytimetext.': '.$deliveryDate.' <br />';			
                }else{
                    if($bookingDataArr['iShipperConsignee']==1 && ($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__)){
                        $szWarehouseDetails = '<br />'.$pickAddressText.': '.$szWareHouseFromStr.''.$picktimetext.': '.$pickUpDate.'<br />
                <br />'.$deliveryAddressText.': '.$szWareHouseToStr.''.$deliverytimetext.': '.$deliveryDate.'<br />';           
                                            
                    }else{
                    $szWarehouseDetails = '<br />'.$pickAddressText.': '.$szWareHouseFromStr.''.$picktimetext.': Latest '.$pickUpDate.'<br />
                <br />'.$deliveryAddressText.': '.$szWareHouseToStr.''.$deliverytimetext.': Before '.$deliveryDate.'<br />';            
                    }			
                } 
            }
            /*
            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
            {					
                $serviceString = "Transportation from the suppliers door in ".$bookingDataArr['szShipperCity']." to your door in ".$bookingDataArr['szConsigneeCity'].", all inclusive. This is also known as EXW terms.";
            }
            else
            {
                $serviceString = "Transportation from the port in ".$bookingDataArr['szWarehouseFromCity']." to your door in ".$bookingDataArr['szConsigneeCity'].", all inclusive. This is also known as FOB terms.";
            }
             * 
             */
            $serviceString = display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true);
            
            if($bookingDataArr['fTotalVat']>0)
            {
                $szVatString = '
                    <tr>
                        <td valign="top" style="border-top:0px;font-size:14px;font-family:Cambria;background:#fff;">
                            VAT 
                        </td>
                        <td  valign="top" align="right" style="border-top:0px;font-size:14px;font-family:Cambria;background:#fff;" >'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag(number_format((float)$bookingDataArr['fTotalVat'],2),$utf8Flag).'</td>
                    </tr> 
		'; 
                $fTotalPriceCustomerCurrency = number_format((float)($fCustomerCurrencyPrice + $bookingDataArr['fTotalVat']),2);
            }
        } 
        if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
        {
            $cargo_volume = format_volume($bookingDataArr['fCargoVolume'],true); 
            $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',',');

            $kCourierServices= new cCourierServices();
            $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($idBooking);

            $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);

            if((int)$newCourierBookingAry[0]['iCourierAgreementIncluded']==1)
                $labelBy=$newCourierBookingAry[0]['szDisplayName'];
            else
                $labelBy="Transporteca";

            if(!empty($bookingDataArr['dtCutOff']))
            {
                $requestPickUpDate=date('d M Y',strtotime($bookingDataArr['dtCutOff']));
            }
            if(!empty($bookingDataArr['dtAvailable']))
            {
                $expdeliveryDate=date('d M Y',strtotime($bookingDataArr['dtAvailable']));
            } 
            $serviceString="Door-to-door courier service with ".$newCourierBookingAry[0]['szProviderName']." ".$newCourierBookingAry[0]['szProviderProductName'].", ".$labelBy." will setup the booking and make labels.<br /> <br />Requested pick-up: ".$requestPickUpDate."<br />Expected delivery: ".$expdeliveryDate; 
             
    } 
    if($flag==true)
    {
        $body_info='style="background:#000;text-align:center;"';
        $width='width="650"';

        $align='align="center"';
        $bodyfontsize='style="font-size:12px;"';
        $printStringStart='<p style="text-align:right;margin:5px auto 10px;width:690px;">
        <a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href='.__BASE_URL__.'/downloadBookingInvoice/'.$idBooking.'/ target="_blank">Download PDF</a>&nbsp;&nbsp;&nbsp;
        <a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href="javascript:void(0)" onclick="PrintDiv();">Print</a></p>
        <div id="viewInvoice" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;">';
        $printStringEnd='</div>';
        $script='<script>
        function PrintDiv()
        {    
              var divToPrint = document.getElementById("viewInvoice");
              var popupWin = window.open("", "_blank", "width=900,height=900");
              popupWin.document.open();
              popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
              popupWin.document.close();
        }
        </script>';
    }
    else
    {
        $width='width="650"';

        $align='';
        $bodyfontsize='';
        $script='';
        $printStringEnd='';
    }
	if(trim($bookingDataArr['iPaymentType'])==__TRANSPORTECA_PAYMENT_TYPE_1__ || trim($bookingDataArr['iPaymentType'])=="Zooz")
	{
            $iPaymentType="Credit Card";
	}
        else if(trim($bookingDataArr['iPaymentType'])==__TRANSPORTECA_PAYMENT_TYPE_3__)
        {
            $iPaymentType="Bank Transfer";
        }
	else
	{
            $iPaymentType=$bookingDataArr['iPaymentType'];
	}
	
	$szTransferHeading = '';
	$szBankDetailsBox = '';		
	$firstTdWidth = "";
	if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
	{
            $idCurrency = $bookingDataArr['idCustomerCurrency'];
            $currencyDetailsAry = array();
            $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);

            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
            {
                $szBankTransferDueDateTime = strtotime($bookingDataArr['dtCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60);
                $szBankTransferDueDateTime = date('d. F',$szBankTransferDueDateTime);
            }
            else
            {
                $szBankTransferDueDateTime = strtotime($bookingDataArr['dtWhsCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60);
                $szBankTransferDueDateTime = date('d. F',$szBankTransferDueDateTime);
            }  
            if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
            { 
                $iNumDaysPayBeforePickup = $bookingDataArr['iNumDaysPayBeforePickup'];
                if($iNumDaysPayBeforePickup<=0)
                {
                    $iNumDaysPayBeforePickup = 2;
                }
                $szBankTransferDueDateTime=date('Y-m-d',strtotime($bookingDataArr['dtCutOff']));
                $szBankTransferDueDateTime=getBusinessDaysForCourier($szBankTransferDueDateTime,$iNumDaysPayBeforePickup.' DAY');
                $szBankTransferDueDateTime = date('d. F',strtotime($szBankTransferDueDateTime));
            }
            
            $szTransferHeading = '<br /><strong>Method of Payment: Bank Transfer</strong>';
            $szBankDetailsBox = '	
                    <td valign="top" width="40%" style="font-family:Cambria;font-size:14px;border:solid 1px #000;padding:3px;" border="1">
                        <strong>Bank: '.$currencyDetailsAry['szBankName'].'</strong>
                        <br /><strong>Sort code: '.$currencyDetailsAry['szSortCode'].'</strong>
                        <br /><strong>Account number: '.$currencyDetailsAry['szAccountNumber'].'</strong> 
                        <br /><strong>IBAN: '.$currencyDetailsAry['szIBANNumber'].'</strong> 
                        <br /><strong>SWIFT: '.$currencyDetailsAry['szSwiftNumber'].'</strong> 
                        <br /><strong>Name on account: '.$currencyDetailsAry['szNameOnAccount'].'</strong> 
                        <br /><strong>Reference on transfer: '.$bookingDataArr['szBookingRef'].'</strong> 
                    </td>
            ';
            $firstTdWidth = "width='60%'";
            
            
            if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
            {
                $szPaidByString = '<tr> <td style="width:99%;padding:3px;" colspan="2"> To confirm your booking please transfer the full invoice amount to our account. If we have not received your payment by '.$szBankTransferDueDateTime.', the pick-up of your shipment may be delayed.<br></td></tr>';
            }
            else
            {    
                $ltlText='';    
                if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
                {
                    $ltlText=" before noon";
                }

                if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
                {
                    $szPaidByString = '<tr> <td style="width:99%;padding:3px;" colspan="2">To confirm your booking, please transfer the full invoice amount to our account<br></td></tr>';
                }
                else
                {
                    $szPaidByString = '<tr> <td style="width:99%;padding:3px;" colspan="2"> To confirm your booking, please transfer the full invoice amount to our account so we receive payment latest by '.$szBankTransferDueDateTime.''.$ltlText.'<br></td></tr>';
                } 
            }
	}
	else
	{ 
            $szPaidByString = '
                <tr>
                    <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                            <strong>Paid by '.$iPaymentType.' on '.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).'</strong>
                    </td>
                    <td valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fTotalPriceCustomerCurrency,$utf8Flag).'</strong></td>
                </tr>
                <tr>
                    <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                            <strong>Balance</strong>
                    </td>
                    <td  valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.$totalUnpaidAmountStr.'</strong></td>
                </tr>
            ';
	}
        
        if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        {
            $szRateAndReviewText = '';
        }
        else
        {
            $szRateAndReviewText = '<br />Please rate and review our service on www.transporteca.com after '.date('d F Y',strtotime($bookingDataArr['dtAvailable'])).'.<br /><br />';
        }
	
	$HeadingInvoice=$image_url."/images/HeadingInvoice.jpg";
	$string .= '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<title>Booking Invoice</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			'.$script.'
			</head>
			<body '.$body_info.'>	
			'.$printStringStart;
                        if($flag==true)
                        {
                            $string .= '<table cellpadding="3" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                                    <tr>
                                            <td align="left" valign="bottom" width="70%"><img src="'.$logoPath.'" width="150" hieght="60"></td>
                                            <td align="right" valign="bottom" width="30%"><img src="'.$HeadingInvoice.'" width="200" hieght="50"></td>
                                    </tr>
                            </table>';
                        }
                        else
                        {
                            $string .= '<table cellpadding="3" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                                    <tr>
                                        <td align="left" valign="bottom" width="70%">&nbsp;</td>
                                        <td align="right" valign="bottom" width="30%">&nbsp;</td>
                                    </tr>
                            </table>';
                        }
			$string .= '<table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
                                    <td valign="top" width="60%" style="font-family:Cambria;font-size:13px;background:#fff;">
                                        '.$forwarderDetails.'E-mail: '.html_entities_flag($kForwarder->szGeneralEmailAddress,$utf8Flag).'<br />Phone: '.$kForwarder->szPhone.'
                                    </td>	
                                    <td valign="top" width="40%" style="font-family:Cambria;font-size:14px;border:solid 1px #000;padding:1px;" border="1">
                                        <strong>Invoice Number: '.$bookingDataArr['szInvoice'].'</strong>
                                        <br /><strong>Invoice Date: '.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).'</strong>
                                        <br /><strong>Booking Reference: '.$bookingDataArr['szBookingRef'].'</strong>
                                        '.$szTransferHeading.'						
                                    </td>	
				</tr> 
			 </table>
			 <table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
					<td '.$firstTdWidth.' valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
						<strong style="font-size:14px;">Billing Address:</strong>
						<br />'.$billingAddress.'<br />Contact: '.html_entities_flag($bookingDataArr['szFirstName'],$utf8Flag).' '.html_entities_flag($bookingDataArr['szLastName'],$utf8Flag).'<br />E-mail: '.html_entities_flag($bookingDataArr['szEmail'],$utf8Flag).'<br />Phone: +'.$iInternationDialCode.' '.$bookingDataArr['szCustomerPhoneNumber'].'<br />
						<br />
					</td>
					'.$szBankDetailsBox.'
				</tr>
			</table><br />';
		if($bookingDataArr['szInvoiceComment']!='')
		{	
			$string .='<table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
					<td valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
					<strong style="font-size:14px;">Invoice Comments:</strong>
					<br />'.html_entities_flag($bookingDataArr['szInvoiceComment'],$utf8Flag).'
					</td>
				</tr>
			</table>
			<br />
			';
		}
		
		if(__ENVIRONMENT__ =="LIVE")
		{
			$show_url=substr(__MAIN_SITE_HOME_PAGE_URL__,8);
		}
		else
		{
			$show_url=substr(__MAIN_SITE_HOME_PAGE_URL__,7);
		}
                $string .='<table cellpadding="3" cellspacing="0" border="1" '.$width.' '.$align.'>
                        <tr>
                            <td width="80%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                <strong>Transportation Service Description</strong>
                            </td>
                            <td width="20%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Total</strong></td>
                        </tr>
                        <tr>
                            <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;border-bottom:none;">
                                '.$serviceString.'<br />
                                '.$szWarehouseDetails.'
                                 <br />'.$cargoText.'</td> '
                            . '<td valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;border-bottom:none; ">'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag(number_format((float)$fCustomerCurrencyPrice,2),$utf8Flag).'</td></tr>
                            '.$szVatString.' <tr>
                                <td valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                    <strong>Invoice Total</strong>
                                </td>
                                <td valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fTotalPriceCustomerCurrency,$utf8Flag).'</strong></td>
                        </tr>
                        '.$szPaidByString.'
                    </table>';
                    if($flag==true)
                    {
                        $string .='<br /><table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
                        <tr>
                                <td style="TEXT-ALIGN:CENTER";>Thank you for your business.<br />
                                '.$szRateAndReviewText.'
                                </td>
                        <tr>
                        <tr>
                                <td style="TEXT-ALIGN:CENTER";>This invoice is available in soft copy on www.transporteca.com under My Bookings. The
                                                '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' standard trading terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).' apply to this booking.
                                        If you have any questions or concerns about this invoice, please contact '.$customerServiceEmailStr.'.
                                </td>
                        <tr>
                        <tr>
                                <td align="right"><img src='.$image_url.'/images/Powered_By.jpg width="170"></td>
                        </tr>
                        </table>';
                    }
                    else
                    {
                        $string .='<p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;">Thank you for your business.<br />
                        '.$szRateAndReviewText.'
                        </p>
                        <p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;">
                                                This invoice is available in soft copy on www.transporteca.com under My Bookings. The
                                                '.html_entities_flag($bookingDataArr['szForwarderRegistName'],$utf8Flag).' standard trading terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).' apply to this booking.
                                        If you have any questions or concerns about this invoice, please contact '.$customerServiceEmailStr.'.
                        </p>';
                    }	
		$string .=$printStringEnd;			
		$string .='</body>
		</html>';
	if($flag==true)
	{		
		echo $string;
	}
	else if($view_invoice_pdf)
	{
            return $string ;
	}
	else
	{
            $pdf->WriteHTML($string,true); 
            if($save_invoices)
            {
                $file_name = __APP_PATH_ROOT__."/transportecaInvoice/Invoice-".$bookingDataArr['szBookingRef']."-".$bookingDataArr['szInvoice'].".pdf"; 
                if(file_exists($file_name))
                {
                    @unlink($file_name);
                } 
                $pdf->Output($file_name,'F'); 
            }
            else
            {
                $file_name = __APP_PATH_ROOT__."/invoice/Invoice-".$bookingDataArr['szBookingRef']."-".$bookingDataArr['szInvoice'].".pdf"; 
                if(file_exists($file_name))
                {
                    @unlink($file_name);
                } 
                $pdf->Output($file_name,'F'); 
            } 
            return $file_name;
	}
} 
function getCombinedInvoicesPdf($idBooking,$pdfhtmlflag)
{
    $kBooking = new cBooking();
    $kBooking->load($idBooking);
    
        if($kBooking->iFinancialVersion==2)
        {
            $bookingInvoiceHtml = getInvoiceConfirmationPdfFileHTML_v2($idBooking,$pdfhtmlflag,false,true);
        }else{
            $bookingInvoiceHtml = getInvoiceConfirmationPdfFileHTML($idBooking,$pdfhtmlflag,false,true);
            $bookingInsuranceInvoicePdf = getBookingInsuranceInvoicePdf($idBooking,$pdfhtmlflag,false,true);
        }
	
	//$bookingInsuranceInvoicePdf = getBookingInsuranceInvoicePdf($idBooking,$pdfhtmlflag,false,true);
	
	if($pdfhtmlflag==true)
	{
		echo $bookingInvoiceHtml;
                if($kBooking->iFinancialVersion!=2)
                {
                    echo "<br>";
                    echo $bookingInsuranceInvoicePdf ;
                }
	}
	else
	{
		$pdf=new HTML2FPDFBOOKING();
		$pdf->AddPage('',true);
		$pdf->Image($logoPath,10,10,30,10);
                $pdf->Image($HeadingInvoice,170,10,30,10);
		$pdf->WriteHTML($bookingInvoiceHtml,true);
                if($kBooking->iFinancialVersion!=2)
                {
                   $pdf->AddPage('',true);
                   $pdf->WriteHTML($bookingInsuranceInvoicePdf,true);
                }
		
		$file_name = __APP_PATH_ROOT__."/invoice/Booking-Invoice-".$bookingDataArr['szInvoice'].".pdf"; 
		if(file_exists($file_name))
		{
			@unlink($file_name);
		}
	   	
	   	$pdf->Output($file_name,'F'); 
	}
   	return $file_name;
	
} 

function getBookingInsuranceInvoicePdf($idBooking,$flag=false,$utf8Flag=false,$ret_html=false)
{ 
    require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php');

    if(!class_exists('cForwarderContact'))
    { 
        require_once(__APP_PATH_CLASSES__."/config.class.php");
        require_once(__APP_PATH_CLASSES__."/booking.class.php");
        require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
        require_once(__APP_PATH_CLASSES__."/user.class.php");
        require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
        require_once(__APP_PATH_CLASSES__."/forwarderContact.class.php"); 
        require_once(__APP_PATH_CLASSES__."/pdfDocuments.class.php"); 
    }
	
    $kBooking = new cBooking();
    $kForwarder = new cForwarder();
    $kForwarderContact = new cForwarderContact();
    $kConfig = new cConfig();
    $kUser = new cUser();
    $kWHSSearch = new cWHSSearch();

    $iLanguage = getLanguageId(); 

//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    { 
//        $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//    } 
//    else 
//    {
//        $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__'); 
//    }
	$szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage); 	
	$pdf=new HTML2FPDFBOOKING();
	$pdf->AddPage('',true);
	
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking,__LANGUAGE_ID_ENGLISH__,true);
         
        $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($idBooking,true);
        $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag) ;
  
	$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
	$total=count($cargoDetailArr);
	if(!empty($cargoDetailArr))
	{
            $str.="";
            $ctr=0;
            foreach($cargoDetailArr as $cargoDetailArrs)
            {
                $t=$total-1;
                if($ctr==0)
                {
                    $str .= utf8_decode($cargoDetailArrs['szCommodity']);
                }
                else if($ctr>0 && $ctr==($total-1))
                {
                    $str .= ' and '.utf8_decode($cargoDetailArrs['szCommodity']);
                }
                else
                {
                    $str .= ', '.utf8_decode($cargoDetailArrs['szCommodity']);
                }
                ++$ctr;	
            }
            $str.="";
	}  
 	if($flag==true)
	{
            $image_url=__MAIN_SITE_HOME_PAGE_URL__;
	}
	else
	{
            $image_url=__APP_PATH_ROOT__;
	} 
	
	$logoPath=$image_url."/images/transporteca-logo.jpg";
 
	$kUser->getUserDetails($bookingDataArr['idUser']);
	
	       	
	$billcountry=$kConfig->getCountryName($bookingDataArr['szCustomerCountry']);
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerCompanyName'],$utf8Flag)."<br />";         	
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerAddress1'],$utf8Flag);
	
	if(!empty($bookingDataArr['szCustomerAddress2']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress2'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerAddress3']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress3'],$utf8Flag);			
	if(!empty($bookingDataArr['szCustomerPostCode']))
			$billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerPostCode'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerCity']))
	{
		if(!empty($bookingDataArr['szCustomerPostCode']))
			$billingAddress .=" ".html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
		else
			$billingAddress .=html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
	}
	if(!empty($bookingDataArr['szCustomerState']))
			$billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerState'],$utf8Flag);
	if(!empty($billcountry))
			$billingAddress .=", ".html_entities_flag($billcountry,$utf8Flag);
	else
			$billingAddress .="<br />";
			  
	$totalPaidAmount = number_format((float)$bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'],2); 
	$fTotalPriceCustomerCurrency = number_format((float)$bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'], 2);  
	$totalUnpaidAmount = ($totalPaidAmount - $fTotalPriceCustomerCurrency) ; 
	$totalUnpaidAmountStr = html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.number_format((float)$totalUnpaidAmount,2); 
	$fCargoWeight = $bookingDataArr['fCargoWeight']/1000;	
	
	if($flag==true)
	{
            $body_info='style="background:#000;text-align:center;"';
            $width='width="650"'; 
            $align='align="center"';
            $bodyfontsize='style="font-size:12px;"';

            if($ret_html)
            {
                $printStringStart='<p style="text-align:right;margin:5px auto 10px;width:690px;">&nbsp;</p><div id="viewInvoice" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;">';
            }
            else
            {
                $printStringStart='<p style="text-align:right;margin:5px auto 10px;width:690px;">
                <a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href='.__BASE_URL__.'/downloadInsuranceInvoice/'.$idBooking.'/ target="_blank">Download PDF</a>&nbsp;&nbsp;&nbsp;
                <a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href="javascript:void(0)" onclick="PrintDiv();">Print</a></p>
                <div id="viewInvoice" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;">';
            }

            $printStringEnd='</div>';
            $script='<script>
            function PrintDiv()
            {    
                  var divToPrint = document.getElementById("viewInvoice");
                  var popupWin = window.open("", "_blank", "width=900,height=700");
                  popupWin.document.open();
                  popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                  popupWin.document.close();
            }
            </script>';
	}
	else
	{
            $width='width="650"';

            $align='';
            $bodyfontsize='';
            $script='';
            $printStringEnd='';
	}
        
	if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_1__ || trim($bookingDataArr['iPaymentType'])=="Zooz")
	{
            $iPaymentType="Credit Card";
	}
	else
	{
            $iPaymentType=$bookingDataArr['iPaymentType'];
	}
	
	$szTransferHeading = '';
	$szBankDetailsBox = '';		
	$firstTdWidth = "";
	if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
	{
            $idCurrency = $bookingDataArr['idCustomerCurrency'];
            $currencyDetailsAry = array();
            $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);

            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
            {
                $szBankTransferDueDateTime = strtotime($bookingDataArr['dtCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60);
                $szBankTransferDueDateTime = date('d. F',$szBankTransferDueDateTime);
            }
            else
            {
                $szBankTransferDueDateTime = strtotime($bookingDataArr['dtWhsCutOff']) - ($bookingDataArr['iBookingCutOffHours']*60*60);
                $szBankTransferDueDateTime = date('d. F',$szBankTransferDueDateTime);
            }
            
            if((int)$bookingDataArr['idServiceProvider']>0 && $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
            {
                $iNumDaysPayBeforePickup = $bookingDataArr['iNumDaysPayBeforePickup']; 
                if($iNumDaysPayBeforePickup<=0)
                {
                    $iNumDaysPayBeforePickup = 2;
                }
                $szBankTransferDueDateTime=date('Y-m-d',strtotime($bookingDataArr['dtCutOff']));
                $szBankTransferDueDateTime=getBusinessDaysForCourier($szBankTransferDueDateTime,$iNumDaysPayBeforePickup.' DAY');
                $szBankTransferDueDateTime = date('d. F',strtotime($szBankTransferDueDateTime));
            }
            
            $szTableBorder = 'border="1" ';
            $szMethodOfPayment = '<strong>Method of Payment: Bank Transfer</strong><br />';
            $szBankDetailsBox = '	
                    <p style="font-family:Cambria;font-size:14px;border:solid 1px #000;padding:3px;" border="1"><strong>Bank: '.$currencyDetailsAry['szBankName'].'</strong>
                        <br /><strong>Sort code: '.$currencyDetailsAry['szSortCode'].'</strong>
                        <br /><strong>Account number: '.$currencyDetailsAry['szAccountNumber'].'</strong> 
                        <br /><strong>IBAN: '.$currencyDetailsAry['szIBANNumber'].'</strong> 
                        <br /><strong>SWIFT: '.$currencyDetailsAry['szSwiftNumber'].'</strong> 
                        <br /><strong>Name on account: '.$currencyDetailsAry['szNameOnAccount'].'</strong> 
                        <br /><strong>Reference on transfer: '.$bookingDataArr['szBookingRef'].'</strong> 
                    </p>
            '; 
            $szTransferHeading = '<br /><strong>Method of Payment: Bank Transfer</strong> <br />'; 
            $firstTdWidth = "width='60%'"; 

            $ltlText='';
            if($bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__) //LTL
            {
                $ltlText=" before noon";
            }
            
            if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
            {
                $szPaidByString = '<tr> <td style="width:99%;padding:3px;" colspan="2"> For this insurance to cover your shipment, please transfer the full invoice amount to our account <br></td></tr>';
            }
            else
            {
                $szPaidByString = '<tr> <td style="width:99%;padding:3px;" colspan="2"> For this insurance to cover your shipment, please transfer the full invoice amount to our account so we receive payment latest by '.$szBankTransferDueDateTime.''.$ltlText.'<br></td></tr>';
            }
	}
	else
	{
            $szPaidByString = '
                <tr>
                    <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                            <strong>Paid by '.$iPaymentType.' on '.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).'</strong>
                    </td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($totalPaidAmount,$utf8Flag).'</strong></td>
                </tr>
                <tr>
                    <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                            <strong>Balance</strong>
                    </td>
                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.$totalUnpaidAmountStr.'</strong></td>
                </tr>
            ';
	}
        
        if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        {
            $serviceString = "Transportation insurance for booking ".$bookingDataArr['szBookingRef'] ; 
        }
        else
        {
            if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
            {
                $serviceString .="Transportation insurance from EXW ".html_entities_flag($bookingDataArr['szShipperCity_pickup'],$utf8Flag)." to delivery in ".html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag);
            }
            else
            {
                $serviceString .="Transportation insurance from FOB ".html_entities_flag($bookingDataArr['szWarehouseFromCountry'],$utf8Flag)." to delivery in ".html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag);
            }
        }
	
        $kConfig_new = new cConfig();
        $kConfig_new->loadCountry($bookingDataArr['idCustomerDialCode']);
        $iInternationDialCode = $kConfig_new->iInternationDialCode;

        $szServiceDescriptionString = display_service_type_description($bookingDataArr,__LANGUAGE_ID_ENGLISH__,true); 
        
	$HeadingInvoice=$image_url."/images/HeadingInvoice.jpg";
	$string .= '
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <title>Booking Invoice</title>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                '.$script.'
                </head>
                <body '.$body_info.'>	
                '.$printStringStart;
                if($flag==true || $ret_html)
                {
                    $string .= '<table cellpadding="0" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                        <tr>
                            <td align="left" valign="bottom" width="60%" style="margin-left:5px;"><img src="'.$logoPath.'" width="150" hieght="60"></td>
                            <td align="right" valign="bottom" width="30%"><img src="'.$HeadingInvoice.'" width="200" hieght="50"></td>
                        </tr> 
                    </table>';
                }
                else
                {
                    $string .= '<table cellpadding="0" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                        <tr>
                            <td align="left" valign="bottom" width="60%" style="margin-left:5px;">&nbsp;</td>
                            <td align="right" valign="bottom" width="30%">&nbsp;</td>
                        </tr> 
                    </table>';
                }
                $string .= '<table cellpadding="3" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                    <tr>
                        <td valign="top" width="60%" style="font-family:Cambria;font-size:13px;background:#fff;">'.__TRANSPORTECA_ADDRESS__.'</td>	
                        <td valign="top" width="40%" style="font-family:Cambria;font-size:14px;border:solid 1px #000;padding:3px;" border="1">
                            <br /><strong>Invoice Number: '.$bookingDataArr['szInsuranceInvoice'].'</strong>
                            <br /><strong>Invoice Date: '.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).'</strong>
                            <br /><strong>Booking Reference: '.$bookingDataArr['szBookingRef'].'</strong> 
                            <br />'.$szMethodOfPayment.'
                            <br /><strong>THIS IS YOUR INSURANCE CONFIRMATION</strong>
                        </td>	
                    </tr> 
                 </table>
                 <table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
                    <tr>
                        <td '.$firstTdWidth.' valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
                            <strong style="font-size:14px;">Billing Address:</strong>
                            <br />'.$billingAddress.'<br />Contact: '.html_entities_flag($bookingDataArr['szFirstName'],$utf8Flag).' '.html_entities_flag($bookingDataArr['szLastName'],$utf8Flag).'<br />E-mail: '.html_entities_flag($bookingDataArr['szEmail'],$utf8Flag).'<br />Phone: +'.$iInternationDialCode." ".$bookingDataArr['szCustomerPhoneNumber'].'<br />
                            <br />
                        </td> 
                        <td valign="top" width="40%"  '.$szTableBorder.'>
                            '.$szBankDetailsBox.'
                        </td>
                    </tr>
                </table><br />
            ';
            if($bookingDataArr['szInvoiceComment']!='')
            {	
			$string .='<table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
					<td valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
					<strong style="font-size:14px;">Invoice Comments:</strong>
					<br />'.html_entities_flag($bookingDataArr['szInvoiceComment'],$utf8Flag).'
					</td>
				</tr>
			</table>
			<br />
			';
		}
		
		if(__ENVIRONMENT__ =="LIVE")
		{
                    $show_url=substr(__MAIN_SITE_HOME_PAGE_URL__,8);
		}
		else
		{
                    $show_url=substr(__MAIN_SITE_HOME_PAGE_URL__,7);
		}
                
                $quantityText = '';
                if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__ && (int)$bookingDataArr['idServiceProvider']>0)
                { 
                    $kCourierServices = new cCourierServices();
                    $packingTypeArr = $kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1);  
                    if((int)$bookingDataArr['iNumColli']>1)
                    {
                        $szPackingSingle = $packingTypeArr[0]['szPacking'];
                        $quantityText = number_format((int)$bookingDataArr['iNumColli'])." ".strtolower($szPackingSingle).", ";
                    }
                    else if($bookingDataArr['iNumColli']>0)
                    {
                        if(!empty($packingTypeArr[0]['szSingle']))
                        {
                            $szPackingSingle = $packingTypeArr[0]['szSingle'];
                        }
                        else
                        {
                            $szPackingSingle = $packingTypeArr[0]['szPackingSingle'];
                        }
                        $quantityText = number_format((int)$bookingDataArr['iNumColli'])." ".strtolower($szPackingSingle).", ";
                    } 
                }
                else if($bookingDataArr['iNumColli']>0 && ($bookingDataArr['iSearchMiniVersion']==7 || $bookingDataArr['iSearchMiniVersion']==8 || $bookingDataArr['iSearchMiniVersion']==9)) //Voga Landing Page
                {
                    $quantityText = number_format((int)$bookingDataArr['iNumColli'])." colli, ";
		}
		if($bookingDataArr['iNumColli']>0 && !empty($cargoDetailArr) && $bookingDataArr['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__)
		{
                    $quantityText = number_format((int)$bookingDataArr['iNumColli'])." colli, ";
		} 
                $szImaginaryProfitStr = "";
                if($bookingDataArr['iImaginaryProfitIncluded']==1)
                {
                    $szImaginaryProfitStr = "<br />".round($bookingDataArr['fImaginaryProfitPercentage'])."% imaginary profit : ".html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag)." ".number_format((float)$bookingDataArr['fImaginaryProfitAmount']);
                }
                $fTransportationCostForInsurance = round((float)$bookingDataArr['fTotalPriceCustomerCurrency']);
                if($bookingDataArr['iPrivateShipping']==1)
                {
                    $fTransportationCostForInsurance += $bookingDataArr['fTotalVat'];
                    if(__float($fTransportationCostForInsurance))
                    {
                        $szTransportationCostForInsurance = number_format((float)$fTransportationCostForInsurance,2);
                    }
                    else
                    {
                        $szTransportationCostForInsurance = number_format((float)$fTransportationCostForInsurance);
                    }
                }
                else
                {
                    $szTransportationCostForInsurance = number_format((float)$fTransportationCostForInsurance);
                }
			$string .='<table cellpadding="3" cellspacing="0" border="1" '.$width.' '.$align.'>
				<tr>
                                    <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                        <strong>Insurance Description</strong>
                                    </td>
                                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Total</strong></td>
				</tr>
				<tr>
                                    <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                        '.$szServiceDescriptionString.'.
                                        <br /><br />
                                        <br />Insurance value
                                        <br />Cargo : '.html_entities_flag($bookingDataArr['szGoodsInsuranceCurrency'],$utf8Flag).' '.number_format((float)$bookingDataArr['fValueOfGoods']).'
                                        <br />Transportation : '.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.$szTransportationCostForInsurance."".$szImaginaryProfitStr.'<br />
                                        <br />Cargo: Maximum '.$quantityText."".format_volume($bookingDataArr['fCargoVolume']).' cbm and '.get_formated_cargo_measure($fCargoWeight).' mt, containing '.$szCargoCommodity.'</td><td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;">'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fTotalPriceCustomerCurrency,$utf8Flag).'
                                    </td>
                                </tr>
				<tr>
                                    <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
                                        <strong>Invoice Total</strong>
                                    </td>
                                    <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fTotalPriceCustomerCurrency,$utf8Flag).'</strong></td>
				</tr>
				'.$szPaidByString.'
			</table>';
			if($flag==true)
			{
				$string .='<br /><table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
					<td>	
						This insurance has been issued by First Marine A/S, coverholder for certain Underwriters at Lloyd\'s, sold by Transporteca Limited.<br>
						<br>In case of loss or damage to your shipment, please follow this procedure: http://www.firstmarine.dk/public/Procedure.pdf <br><br>
						If you have any questions, or need assistance, do not hesitate to contact us on '.__STORE_SUPPORT_EMAIL__.' or '.$szCustomerCareNumer.'.<br><br>
						Insurance coverage and conditions available <a href="https://www.firstmarine.dk/?p=1&download=150">here</a> according to Institute Cargo Clause (A).
					</td>
				<tr>
				<tr>
					<td style="TEXT-ALIGN:CENTER;font-size:12px;";><br /><br />This invoice is available in soft copy on www.transporteca.com under My Bookings. 
						<br>If you have any questions or concerns about this invoice, please contact '.__STORE_SUPPORT_EMAIL__.'.
					</td>
				<tr>
				<tr>
                                    <td align="right"><img src='.$image_url.'/images/Powered_By.jpg width="170"></td>
				</tr>
				</table>';
			}
			else
			{
				$string .='
					<p style="left:20px;">This insurance has been issued by First Marine A/S, coverholder for certain Underwriters at Lloyd\'s, sold by Transporteca Limited.<br>
						<br>In case of loss or damage to your shipment, please follow this procedure: http://www.firstmarine.dk/public/Procedure.pdf <br>
						
						<br>If you have any questions, or need assistance, do not hesitate to contact us on '.__STORE_SUPPORT_EMAIL__.' or '.$szCustomerCareNumer.'.<br><br>
						<br><br>Insurance coverage and conditions available <a href="tt">here</a> according to Institute Cargo Clause (A).
					</p>
					<p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;"><br /><br /><p>This invoice is available in soft copy on www.transporteca.com under My Bookings. <br>
                                            If you have any questions or concerns about this invoice, please contact '.__STORE_SUPPORT_EMAIL__.'.</p>
					</p>
				';
			}	
		$string .=$printStringEnd;			
		$string .='</body>
		</html>';
	if($flag==true)
	{		
		echo $string;
	}
	else if($ret_html)
	{
		return $string;
	}
	else
	{
                $pdf->Image($logoPath,10,10,30,10);
                $pdf->Image($HeadingInvoice,170,10,30,10);
		$pdf->WriteHTML($string,true);
	
			
		$file_name = __APP_PATH_ROOT__."/invoice/Insurance-Invoice-".$bookingDataArr['szBookingRef']."-".$bookingDataArr['szInvoice'].".pdf"; 
		if(file_exists($file_name))
		{
			@unlink($file_name);
		}
   	
   		$pdf->Output($file_name); 
   		return $file_name;
	} 
}  
function getBookingConfirmationPdfFile($idBooking)
{
	require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
	$kBooking = new cBooking();
	$kForwarder = new cForwarder();
	$kForwarderContact = new cForwarderContact();
	$kConfig = new cConfig(); 
	
	$pdf=new HTML2FPDFBOOKING();
	$pdf->AddPage('',true);
	
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
        
        if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
        }
        if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
        }
        if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
        }
        if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
        }
        
	$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
	
	$forwarderDetails .= $bookingDataArr['szForwarderRegistName'].'<br />';
	if($bookingDataArr['szForwarderAddress']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderAddress'].'<br />';
	if($bookingDataArr['szForwarderAddress2']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderAddress2'].'<br />';
	if($bookingDataArr['szForwarderPostCode']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderPostCode'].'<br />';
	if($bookingDataArr['szForwarderCity']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderCity'].'<br />';
		
	$forwarderDetails .=$forwardCountry;
	
	$shipperDetails .=$bookingDataArr['szShipperCompanyName'].'<br />';
	if($bookingDataArr['szShipperAddress']!='')
		$shipperDetails .=$bookingDataArr['szShipperAddress'];
			
	if($bookingDataArr['szShipperAddress2']!='')	
		$shipperDetails .=', '.$bookingDataArr['szShipperAddress2'];
	
	if($bookingDataArr['szShipperAddress3']!='')	
		$shipperDetails .=', '.$bookingDataArr['szShipperAddress3'].'<br />';	
	else
		$shipperDetails .='<br />';			
		
	if($bookingDataArr['szShipperPostCode']!='')	
		$shipperDetails .=$bookingDataArr['szShipperPostCode'];
		
	if($bookingDataArr['szShipperCity']!='')	
		$shipperDetails .=' '.$bookingDataArr['szShipperCity'].'<br />';
	else
		$shipperDetails .='<br />';
		
	if($bookingDataArr['szShipperCountry']!='')	
		$shipperDetails .=$bookingDataArr['szShipperCountry'].'<br />';
		
	$consigneeDetails .=$bookingDataArr['szConsigneeCompanyName'].'<br />';
	if($bookingDataArr['szConsigneeAddress']!='')
		$consigneeDetails .=$bookingDataArr['szConsigneeAddress'];
	
	if($bookingDataArr['szConsigneeAddress2']!='')	
		$consigneeDetails .=', '.$bookingDataArr['szConsigneeAddress2'];
				
	if($bookingDataArr['szConsigneeAddress3']!='')	
		$consigneeDetails .=', '.$bookingDataArr['szConsigneeAddress3'].'<br />';
	else
		$consigneeDetails .='<br />';
	
	if($bookingDataArr['szConsigneePostCode']!='')	
		$consigneeDetails .=$bookingDataArr['szConsigneePostCode'];
		
	if($bookingDataArr['szConsigneeCity']!='')	
		$consigneeDetails .=' '.$bookingDataArr['szConsigneeCity'].'<br />';
	else
		$consigneeDetails .='<br />';
			
	if($bookingDataArr['szConsigneeCountry']!='')	
		$consigneeDetails .=$bookingDataArr['szConsigneeCountry'].'<br />';
	else
		$consigneeDetails .='<br />';	

	$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,true);
	if(!empty($cargoDetailArr))
	{
		$ctr=0;
		foreach($cargoDetailArr as $cargoDetailArrs)
		{
			$str='';
			$str .=++$ctr.": ".number_format(ceil($cargoDetailArrs['iQuantity']))."x".number_format(ceil($cargoDetailArrs['fLength']))."x".number_format(ceil($cargoDetailArrs['fWidth']))."x".number_format(ceil($cargoDetailArrs['fHeight']))."".$cargoDetailArrs['cmdes'].", ".number_format(ceil($cargoDetailArrs['fWeight']))." ".ucwords($cargoDetailArrs['wmdes']).", ".$cargoDetailArrs['szCommodity'].'<br />';	
		}
	}	
	
	if(!empty($bookingDataArr['dtCutOff']))
	{
		$pickUpDate=date('d F Y H:i',strtotime($bookingDataArr['dtCutOff']));
	}
	if(!empty($bookingDataArr['dtAvailable']))
	{
		$deliveryDate=date('d F Y H:i',strtotime($bookingDataArr['dtAvailable']));
	}
	
	if(!empty($bookingDataArr['dtWhsCutOff']))
	{
		$dtWhsCutOff=date('d F Y H:i',strtotime($bookingDataArr['dtWhsCutOff']));
	}
	
	
	
	$fCargoWeight=$bookingDataArr['fCargoWeight']/1000;
	
	
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTP__) // 1-DTD , 2-DTW
	{
		if(!empty($bookingDataArr['szShipperAddress_pickup']) || !empty($bookingDataArr['szShipperAddress2_pickup']) || !empty($bookingDataArr['szShipperAddress2_pickup']))
		{
			$szWareHouseFromStr.=$bookingDataArr['szShipperAddress_pickup']." ".$bookingDataArr['szShipperAddress2_pickup']." ".$bookingDataArr['szShipperAddress3_pickup']."<br />";
		}
		if(!empty($bookingDataArr['szShipperPostCode_pickup']) || !empty($bookingDataArr['szShipperCity_pickup']))
		{
			$szWareHouseFromStr.=$bookingDataArr['szShipperPostCode_pickup'];
			if($bookingDataArr['szShipperPostCode_pickup']!='')
				$szWareHouseFromStr.=" ".$bookingDataArr['szShipperCity_pickup']."<br />";
			else
				$szWareHouseFromStr.=$bookingDataArr['szShipperCity_pickup']."<br />";
		}
		if(!empty($bookingDataArr['szShipperCountry_pickup']))
		{
			$szWareHouseFromStr.=$bookingDataArr['szShipperCountry_pickup']."<br />";
		}
	}
	else
	{
		if(!empty($bookingDataArr['szWarehouseFromName']))
		{
			$szWareHouseFromStr.=$bookingDataArr['szWarehouseFromName']."<br />";
		}
		if(!empty($bookingDataArr['szWarehouseFromAddress']) || !empty($bookingDataArr['szWarehouseFromAddress2']) || !empty($bookingDataArr['szWarehouseFromAddress3']))
		{
			$szWareHouseFromStr.=$bookingDataArr['szWarehouseFromAddress'].", ".$bookingDataArr['szWarehouseFromAddress2'].", ".$bookingDataArr['szWarehouseFromAddress3']."<br />";
		}
		if(!empty($bookingDataArr['szWarehouseFromPostCode']) || !empty($bookingDataArr['szWarehouseFromCity']))
		{
			$szWareHouseFromStr.=$bookingDataArr['szWarehouseFromPostCode'];
			if($bookingDataArr['szWarehouseFromPostCode']!='')
				$szWareHouseFromStr.=" ".$bookingDataArr['szWarehouseFromCity']."<br />";
			else
				$szWareHouseFromStr.=$bookingDataArr['szWarehouseFromCity']."<br />";
		}						
		if(!empty($bookingDataArr['szWarehouseFromCountry']) || !empty($bookingDataArr['szWarehouseFromState']))
		{
			$szWareHouseFromStr.=$bookingDataArr['szWarehouseFromCountry']."<br />";
		}
	}
	
	
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_PTD__) // 1-DTD , 3-WTD
	{					
		if(!empty($bookingDataArr['szConsigneeAddress_pickup']) || !empty($bookingDataArr['szConsigneeAddress2_pickup']) || !empty($bookingDataArr['szConsigneeAddress2_pickup']))
		{
			//$szWareHouseToStr.=$bookingDataArr['szConsigneeAddress_pickup'].", ".$bookingDataArr['szConsigneeAddress2_pickup'].", ".$bookingDataArr['szConsigneeAddress3_pickup']."<br />";
			$szWareHouseToStr.=$bookingDataArr['szConsigneeAddress_pickup'];
			if($bookingDataArr['szConsigneeAddress2_pickup']!='')
				$szWareHouseToStr.=", ".$bookingDataArr['szConsigneeAddress2_pickup'];
			if($bookingDataArr['szConsigneeAddress3_pickup']!='')
				$szWareHouseToStr.=", ".$bookingDataArr['szConsigneeAddress3_pickup']."<br />";
			else
				$szWareHouseToStr.="<br />";
		
		}
		if(!empty($bookingDataArr['szConsigneePostCode_pickup']) || !empty($bookingDataArr['szConsigneeCity_pickup']))
		{
			$szWareHouseToStr.=$bookingDataArr['szConsigneePostCode_pickup'];
			if($bookingDataArr['szConsigneePostCode_pickup']!='')
				$szWareHouseToStr.=" ".$bookingDataArr['szConsigneeCity_pickup']."<br />";
			else
				$szWareHouseToStr.=$bookingDataArr['szConsigneeCity_pickup']."<br />";
		}
		if(!empty($bookingDataArr['szConsigneeCountry_pickup']))
		{
			$szWareHouseToStr.=$bookingDataArr['szConsigneeCountry_pickup']."<br />";
		}
	}
	else
	{
		if(!empty($bookingDataArr['szWarehouseToName']))
		{
			$szWareHouseToStr.=$bookingDataArr['szWarehouseToName']."<br />";
		}
		if(!empty($bookingDataArr['szWarehouseToAddress']) || !empty($bookingDataArr['szWarehouseToAddress2']) || !empty($bookingDataArr['szWarehouseToAddress3']))
		{
			$szWareHouseToStr.=$bookingDataArr['szWarehouseToAddress'];
			if($bookingDataArr['szWarehouseToAddress2']!='')
				$szWareHouseToStr.=", ".$bookingDataArr['szWarehouseToAddress2'];
			if($bookingDataArr['szWarehouseToAddress3']!='')
				$szWareHouseToStr.=", ".$bookingDataArr['szWarehouseToAddress3']."<br />";
			else
				$szWareHouseToStr.="<br />";
		}
		if(!empty($bookingDataArr['szWarehouseToPostCode']) || !empty($bookingDataArr['szWarehouseToCity']))
		{
			$szWareHouseToStr.=$bookingDataArr['szWarehouseToPostCode'];
			if($bookingDataArr['szWarehouseToPostCode']!='')
				$szWareHouseToStr.=" ".$bookingDataArr['szWarehouseToCity']."<br />";
			else
				$szWareHouseToStr.=$bookingDataArr['szWarehouseToCity']."<br />";
		}						
		if(!empty($bookingDataArr['szWarehouseToCountry']) || !empty($bookingDataArr['szWarehouseToState']))
		{
			$szWareHouseToStr.=$bookingDataArr['szWarehouseToCountry']."<br />";
		}	
	}
	
	 $kForwarder->load($bookingDataArr['idForwarder']);  

	if($kForwarder->szPdfLogo!='')
	{
		$wh='';
		$logoPath=__APP_PATH__."/images/forwarders/thumb/150x60_".$kForwarder->szPdfLogo;
		if(file_exists($logoPath))
		{
			$wh='width="70" hieght="30"';
			$logoPath=__MAIN_SITE_HOME_PAGE_URL__."/images/forwarders/thumb/150x60_".$kForwarder->szPdfLogo;
		}
		else
		{
			$wh='width="70" hieght="30"';
			$logoPath=__MAIN_SITE_HOME_PAGE_URL__."/images/forwarders/".$kForwarder->szPdfLogo;
		}
	}
	 
	 $kForwarderContact->getMainAdmin($bookingDataArr['idForwarder']);
	$bookingServiceEmailArr=$kForwarder->getForwarderCustomerServiceEmail($bookingDataArr['idForwarder'],__CUSTOMER_PROFILE_ID__);
	if(!empty($bookingServiceEmailArr))
	{
		//$strbookingServiceEmailArr=implode(",",$bookingServiceEmailArr);
		$iTotalCounter = count($bookingServiceEmailArr);
		$ctr=0;
		foreach($bookingServiceEmailArr as $customerServiceEmailArys)
		{
			if(empty($customerServiceEmailStr))
			{
				$customerServiceEmailStr = $customerServiceEmailArys ;
				$customerServiceEmailLink = $customerServiceEmailArys;
			}
			else
			{
				$customerServiceEmailLink .= ', '.$customerServiceEmailArys;
				
				if($ctr>0 && $ctr==($iTotalCounter-1))
				{
					$customerServiceEmailStr .= ' and '.$customerServiceEmailArys;
				}
				else
				{
					$customerServiceEmailStr .= ', '.$customerServiceEmailArys;
				}
			}							
			$ctr++;
		}
	}
	$serviceString="Freight from";
	$str_date='';
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
	{
		$serviceString .=" door at origin to door at destination (DTD)";
		$str_date="Pick-up";
		$pickupline="Pick-up after ".$pickUpDate." (local) will be coordinated directly with the shipper.";
		$deliveryLine="Delivery before ".$deliveryDate." (local) will be coordinated directly with the consignee.";
		$pickAddressText="Pick-up<br />address";
		$deliveryAddressText="Delivery<br />address";
		$nextstepText=$bookingDataArr['szForwarderDispName']." will coordinate pick-up of cargo with ".$bookingDataArr['szShipperCompanyName'].".";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__)
	{
		$serviceString .=" door at origin to warehouse at destination (DTW)";
		$str_date="Pick-up";
		$pickupline="Pick-up after ".$pickUpDate." (local) will be coordinated directly with the shipper.";
		$deliveryLine="Consignee can pick up cargo at destination warehouse after ".$deliveryDate." (local).";
		$pickAddressText="Pick-up<br />address";
		$deliveryAddressText="Destination<br />Warehouse";
		$nextstepText=$bookingDataArr['szForwarderDispName']." will coordinate pick-up of cargo with ".$bookingDataArr['szShipperCompanyName'].".";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__)
	{
		$serviceString .=" warehouse at origin to door at destination (WTD)";
		$str_date="Cut-Off";
		$pickupline="Shipper must deliver cargo at origin warehouse latest ".$pickUpDate." (local).";
		$deliveryLine="Delivery before ".$deliveryDate." (local) will be coordinated directly with the consignee.";
		$pickAddressText="Origin<br />Warehouse";
		$deliveryAddressText="Delivery<br />address";
		$nextstepText=$bookingDataArr['szShipperCompanyName']." must hand over cargo to ".$bookingDataArr['szForwarderDispName']." at the origin warehouse latest ".$pickUpDate.".";
		
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__)
	{
		$serviceString .=" warehouse at origin to warehouse at destination (WTW)";
		$str_date="Cut-Off";
		$pickupline="Shipper must deliver cargo at origin warehouse latest ".$pickUpDate." (local)";
		$deliveryLine="Consignee can pick up cargo at destination warehouse after ".$deliveryDate." (local)";
		$pickAddressText="Origin<br />Warehouse";
		$deliveryAddressText="Destination<br />Warehouse";
		$nextstepText=$bookingDataArr['szShipperCompanyName']." must hand over cargo to ".$bookingDataArr['szForwarderDispName']." at the origin warehouse latest ".$pickUpDate.".";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__)
	{
		$serviceString .=" door at origin to port at destination (DTP)";
		$str_date="Pick-up";
		$pickupline="Pick-up after ".$pickUpDate." (local) will be coordinated directly with the shipper.";
		$deliveryLine="Consignee can pick up cargo at destination warehouse after ".$deliveryDate." (local).";
		$pickAddressText="Pick-up<br />address";
		$deliveryAddressText="Destination<br />Warehouse";
		$nextstepText=$bookingDataArr['szForwarderDispName']." will coordinate pick-up of cargo with ".$bookingDataArr['szShipperCompanyName'].".";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__)
	{
		$serviceString .=" port at origin to door at destination (PTD)";
		$str_date="Cut-Off";
		$pickupline="Shipper must deliver cargo at origin warehouse latest ".$pickUpDate." (local).";
		$deliveryLine="Delivery before ".$deliveryDate." (local) will be coordinated directly with the consignee.";
		$pickAddressText="Origin<br />Warehouse";
		$deliveryAddressText="Delivery<br />address";
		$nextstepText=$bookingDataArr['szShipperCompanyName']." must hand over cargo to ".$bookingDataArr['szForwarderDispName']." at the origin warehouse latest ".$pickUpDate.".";
		
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTP__)
	{
		$serviceString .=" port at origin to port at destination (PTP)";
		$str_date="Cut-Off";
		$pickupline="Shipper must deliver cargo at origin warehouse latest ".$pickUpDate." (local)";
		$deliveryLine="Consignee can pick up cargo at destination warehouse after ".$deliveryDate." (local)";
		$pickAddressText="Origin<br />Warehouse";
		$deliveryAddressText="Destination<br />Warehouse";
		$nextstepText=$bookingDataArr['szShipperCompanyName']." must hand over cargo to ".$bookingDataArr['szForwarderDispName']." at the origin warehouse latest ".$pickUpDate.".";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__)
	{
		$serviceString .=" warehouse at origin to port at destination (WTP)";
		$str_date="Cut-Off";
		$pickupline="Shipper must deliver cargo at origin warehouse latest ".$pickUpDate." (local).";
		$deliveryLine="Consignee can pick up cargo at destination warehouse after ".$deliveryDate." (local).";
		$pickAddressText="Pick-up<br />address";
		$deliveryAddressText="Destination<br />Warehouse";
		$nextstepText=$bookingDataArr['szForwarderDispName']." will coordinate pick-up of cargo with ".$bookingDataArr['szShipperCompanyName'].".";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__) 
	{
		$serviceString .=" port at origin to warehouse at destination (PTW)";
		$str_date="Cut-Off";
		$pickupline="Shipper must deliver cargo at origin warehouse latest ".$pickUpDate." (local).";
		$deliveryLine="Consignee can pick up cargo at destination warehouse after ".$deliveryDate." (local)";
		$pickAddressText="Origin<br />Warehouse";
		$deliveryAddressText="Destination<br />Warehouse";
		$nextstepText=$bookingDataArr['szShipperCompanyName']." must hand over cargo to ".$bookingDataArr['szForwarderDispName']." at the origin warehouse latest ".$pickUpDate.".";
	}	
	if($bookingDataArr['iOriginCC']==1 && $bookingDataArr['iDestinationCC']==0)
	{
		$serviceString .=" and customs clearance at origin.";
	}	
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==0)
	{
		$serviceString .=" and customs clearance at destination.";
	}
	
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==1)
	{
		$serviceString .=" and customs clearance at origin and destination.";
	}
	if($bookingDataArr['iDestinationCC']==0 && $bookingDataArr['iOriginCC']==0)
	{
		$serviceString .=".";
	}
	
	
	$string .='
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<title>Booking Confirmation</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			</head>
			<body>
			<p style="TEXT-ALIGN:CENTER;"><img src='.__MAIN_SITE_HOME_PAGE_URL__.'/images/Heading_Booking_Confirmation.jpg width="590" hieght="35"></p>
			<br />
			<p style="TEXT-ALIGN:JUSTIFY;FONT-SIZE:MEDIUM;">This is a confirmation that your booking has been placed with '.$bookingDataArr['szForwarderDispName'].'. You will receive a notification by e-mail when '.$bookingDataArr['szForwarderDispName'].' has acknowledged receipt.</p>
			<table cellpadding="3" cellspacing="1" border="1" width="600" BORDER-COLOR="#7f7f7f">
				<tr>
					<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						<strong>Reference:</strong>
						<br />Date (Time):<br />Booked By:
					</td>
					<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						<H4>'.$bookingDataArr['szBookingRef'].'</H4>
						<br />'.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).' ('.date('H:i',strtotime($bookingDataArr['dtBookingConfirmed'])).' GMT)<br />'.html_entities_flag($bookingDataArr['szFirstName'], ENT_QUOTES,'UTF-8').' '.html_entities_flag($bookingDataArr['szLastName'], ENT_QUOTES,'UTF-8').'<br />'.$bookingDataArr['szEmail'].'
					</td>
					<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						<strong>Freight Forwarder:</strong>
						<img src="'.$logoPath.'" '.$wh.'>
					</td>
					<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						'.$forwarderDetails.'
						</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="1" border="1" width="600" BORDER-COLOR="#7f7f7f">
			<tr>	
				<td>
					<table>
						<tr>
							<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
								<strong>Shipper:</strong>
							</td>
							<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
								'.html_entities_flag($shipperDetails, ENT_QUOTES,'UTF-8').'
							</td>
							<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
								<strong>Consignee:</strong>
							</td>
							<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
								'.html_entities_flag($consigneeDetails, ENT_QUOTES,'UTF-8').'
							</td>
						</tr>
						<tr>
							<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
								Name:<br />
								E-mail:<br />
								Phone:
							</td>
							<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
								'.$bookingDataArr['szShipperFirstName'].' '.$bookingDataArr['szShipperLastName'].'<br />'.$bookingDataArr['szShipperEmail'].'<br />'.html_entities_flag($bookingDataArr['szShipperPhone'], ENT_QUOTES,'UTF-8').'
							</td>
							<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;" border="0">
								Name:<br />
								E-mail:<br />
								Phone:
							</td>
							<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;" border="0">
								'.$bookingDataArr['szConsigneeFirstName'].' '.$bookingDataArr['szConsigneeLastName'].'<br />'.$bookingDataArr['szConsigneeEmail'].'<br />'.html_entities_flag($bookingDataArr['szConsigneePhone'], ENT_QUOTES,'UTF-8').'
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="1" border="1" width="720">
				<tr>
					<td colspan="4" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						<strong>Transportation Service Description</strong>
					</td>
				</tr>
				<tr>
					<td colspan="4" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						'.$serviceString.'
						<br />
						<br /><br/>'.$pickupline.'<br />'.$deliveryLine.'				
					</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="1" border="1" width="720">
				<tr>
					<td valign="top" style="font-family:Cambria;font-size:10px;background:#fff;"><strong>Cargo</strong></td>
				</tr>
				<tr>
					<td valign="top" style="FONT-SIZE:SMALL;COLOR:#a5a5a5;">
						'.$str.'<font color="#a5a5a5;" size="4"><small><i>(Line: Number of units x L x W x H, weight, description)</i></small></font><br />
						<br /><br />Total: Maximum '.format_volume($bookingDataArr['fCargoVolume']).' cbm and '.get_formated_cargo_measure($fCargoWeight).' mt
					</td>
				</tr>
			</table>
			<table cellpadding="3" cellspacing="1" border="1" width="600">
				<tr>
					<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						<strong>'.$pickAddressText.':</strong>
					</td>
					<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						'.$szWareHouseFromStr.'
					</td>
					<td width="15%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;"><strong>'.$deliveryAddressText.':</strong></td>
					<td width="35%" valign="top" style="font-family:Cambria;font-size:10px;background:#fff;">
						'.$szWareHouseToStr.'
					</td>
				</tr>
			</table>
			<BR>
			<p style="TEXT-ALIGN:CENTER;FONT-SIZE:MEDIUM;">
			Next steps: '.$nextstepText.'
			</p>				
			
			<p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;">For any questions related to this booking, please write to '.$bookingDataArr['szForwarderDispName'].' customer service on '.$customerServiceEmailStr.'. '.$bookingDataArr['szForwarderDispName'].' reserves the right to reject the shipment if you have submitted inaccurate information in placing this booking. This booking was placed with '.$bookingDataArr['szForwarderDispName'].' on www.transporteca.com. The booking is subject the '.$bookingDataArr['szForwarderDispName'].' standard terms available on '.$kForwarder->szLink.'.</p>
					
			<br /></body>
			</html>
			'; 
	$pdf->WriteHTML($string,true);
	
	$file_name = __APP_PATH__.'/invoice/Booking-Confirmation-'.$bookingDataArr['szBookingRef'].".pdf";
   	$file = "Booking-Confirmation.pdf";
   	
   	$pdf->Output($file_name,'F');
   	
   	return $file_name;
} 
function getInvoiceConfirmationPdfFile($idBooking)
{
	require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
	$kBooking = new cBooking();
	$kForwarder = new cForwarder();
	$kForwarderContact = new cForwarderContact();
	$kConfig = new cConfig();
	 $kUser = new cUser(); 
	
	$pdf=new HTML2FPDFBOOKING();
	$pdf->AddPage('',true);
	
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
        
        if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
        }
        if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
        }
        if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
        }
        if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
        }
        
	$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
	
	$forwarderDetails .= $bookingDataArr['szForwarderRegistName'].'<br />';
	if($bookingDataArr['szForwarderAddress']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderAddress'].'<br />';
	if($bookingDataArr['szForwarderAddress2']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderAddress2'].'<br />';
	if($bookingDataArr['szForwarderPostCode']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderPostCode'].'<br />';
	if($bookingDataArr['szForwarderCity']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderCity'].'<br />';
		
		
	$kForwarder->load($bookingDataArr['idForwarder']);   
	 
	$kForwarderContact->getMainAdmin($bookingDataArr['idForwarder']);
	$bookingServiceEmailArr=$kForwarder->getForwarderCustomerServiceEmail($bookingDataArr['idForwarder'],__PAYMENT_AND_BILLING_PROFILE_ID__);
	$iTotalCounter=count($bookingServiceEmailArr);
	if(!empty($bookingServiceEmailArr))
	{
		foreach($bookingServiceEmailArr as $customerServiceEmailArys)
		{
			if(empty($customerServiceEmailStr))
			{
				$customerServiceEmailStr = $customerServiceEmailArys ;
				$customerServiceEmailLink = $customerServiceEmailArys;
			}
			else
			{
				//$customerServiceEmailLink .= ', '.$customerServiceEmailArys;
				
				if($ctr>0 && $ctr==($iTotalCounter-1))
				{
					$customerServiceEmailStr .= ' and '.$customerServiceEmailArys;
				}
				else
				{
					$customerServiceEmailStr .= ', '.$customerServiceEmailArys;
				}
			}							
			$ctr++;
		}
	}	
	$total=count($cargoDetailArr);
	$cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
	if(!empty($cargoDetailArr))
	{
		$str.="(";
		$ctr=0;
		foreach($cargoDetailArr as $cargoDetailArrs)
		{
				if($ctr==0)
				{
					$str .= $cargoDetailArrs['szCommodity'];
				}
				else if($ctr>0 && $ctr==($total-1))
				{
					$str .= ' and '.html_entities_flag($cargoDetailArrs['szCommodity'],$utf8Flag);
				}
				else
				{
					$str .= ', '.html_entities_flag($cargoDetailArrs['szCommodity'],$utf8Flag);
				}
				$ctr++;	
		}
		$str.=")";
	}
	
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_WTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_PTD__) // 1-DTD , 3-WTD
	{					
		if(!empty($bookingDataArr['szConsigneeAddress_pickup']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szConsigneeAddress2_pickup']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szConsigneeAddress3_pickup']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szConsigneePostCode_pickup']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneePostCode_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szConsigneeCity_pickup']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szConsigneeCity_pickup'],$utf8Flag);						
		}
		if(!empty($bookingDataArr['szConsigneeCountry_pickup']))
		{
			$szWareHouseToStr.=html_entities_flag($bookingDataArr['szConsigneeCountry_pickup'],$utf8Flag)."<br />";
		}
	}
	else
	{
		if(!empty($bookingDataArr['szWarehouseToName']))
		{
			$szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToName'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseToAddress']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseToAddress2']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress2'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseToAddress3']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToAddress3'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseToPostCode']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToPostCode'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseToCity']))
		{
			$szWareHouseToStr.=", ".html_entities_flag($bookingDataArr['szWarehouseToCity'],$utf8Flag);						
		}
		if(!empty($bookingDataArr['szWarehouseToCountry']))
		{
			$szWareHouseToStr.=html_entities_flag($bookingDataArr['szWarehouseToCountry'],$utf8Flag)."<br />";
		}	
	}
	
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTW__ || $bookingDataArr['idServiceType'] ==__SERVICE_TYPE_DTP__) // 1-DTD , 2-DTW
	{
		if(!empty($bookingDataArr['szShipperAddress_pickup']))
		{
			$szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperAddress_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szShipperAddress2_pickup']) )
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress2_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szShipperAddress3_pickup']) )
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperAddress3_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szShipperPostCode_pickup']))
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperPostCode_pickup'],$utf8Flag);
		}						
		if(!empty($bookingDataArr['szShipperCity_pickup']))
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szShipperCity_pickup'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szShipperCountry_pickup']))
		{
			$szWareHouseFromStr.=html_entities_flag($bookingDataArr['szShipperCountry_pickup'],$utf8Flag)."<br />";
		}
	}
	else
	{
		if(!empty($bookingDataArr['szWarehouseFromName']))
		{
			$szWareHouseFromStr.=html_entities_flag($bookingDataArr['szWarehouseFromName'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseFromAddress']))
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseFromAddress2']) )
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress2'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseFromAddress3']) )
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromAddress3'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseFromPostCode']))
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromPostCode'],$utf8Flag);
		}						
		if(!empty($bookingDataArr['szWarehouseFromCity']))
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromCity'],$utf8Flag);
		}
		if(!empty($bookingDataArr['szWarehouseFromCountry']))
		{
			$szWareHouseFromStr.=", ".html_entities_flag($bookingDataArr['szWarehouseFromCountry'],$utf8Flag)."<br />";
		}
	}
	
	
	$serviceString="Freight from";
	$str_date='';
	if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTD__)
	{
            $serviceString .=" door at origin to door at destination (DTD)";
            $pickAddressText="Pick-up address";
            $deliveryAddressText="Delivery";
            $picktimetext="Expected pick-up time";
            $deliverytimetext="Expected delivery time";
		
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTW__)
	{
            $serviceString .=" door at origin to warehouse at destination (DTW)";
            $pickAddressText="Pick-up address";
            $deliveryAddressText="Destination Warehouse";	
            $picktimetext="Expected pick-up time";
            $deliverytimetext="Expected cargo availability";	
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTD__)
	{
            $serviceString .=" warehouse at origin to door at destination (WTD)";
            $pickAddressText="Origin Warehouse";
            $deliveryAddressText="Delivery";		
            $picktimetext="Cargo cut off";
            $deliverytimetext="Expected delivery time";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTW__)
	{
            $serviceString .=" warehouse at origin to warehouse at destination (WTW)";
            $pickAddressText="Origin Warehouse";
            $deliveryAddressText="Destination Warehouse";
            $picktimetext="Cargo cut off";
            $deliverytimetext="Expected cargo availability";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_DTP__)
	{
            $serviceString .=" door at origin to port at destination (DTW)";
            $pickAddressText="Pick-up address";
            $deliveryAddressText="Destination Port";	
            $picktimetext="Expected pick-up time";
            $deliverytimetext="Expected cargo availability";	
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTD__)
	{
            $serviceString .=" port at origin to door at destination (PTD)";
            $pickAddressText="Origin Port";
            $deliveryAddressText="Delivery";		
            $picktimetext="Cargo cut off";
            $deliverytimetext="Expected delivery time";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTP__)
	{
            $serviceString .=" port at origin to port at destination (PTP)";
            $pickAddressText="Origin Port";
            $deliveryAddressText="Destination Port";
            $picktimetext="Cargo cut off";
            $deliverytimetext="Expected cargo availability";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_WTP__)
	{
            $serviceString .=" warehouse at origin to port at destination (WTP)";
            $pickAddressText="Origin Warehouse";
            $deliveryAddressText="Destination Port";
            $picktimetext="Cargo cut off";
            $deliverytimetext="Expected cargo availability";
	}
	else if($bookingDataArr['idServiceType']==__SERVICE_TYPE_PTW__)
	{
            $serviceString .=" port at origin to warehouse at destination (PTW)";
            $pickAddressText="Origin Port";
            $deliveryAddressText="Destination Warehouse";
            $picktimetext="Cargo cut off";
            $deliverytimetext="Expected cargo availability";
	}	
	if($bookingDataArr['iOriginCC']==1 && $bookingDataArr['iDestinationCC']==0)
	{
            $serviceString .=" and customs clearance at origin.";
	}
	
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==0)
	{
            $serviceString .=" and customs clearance at destination.";
	}
	
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==1)
	{
            $serviceString .=" and customs clearance at origin and destination.";
	}
	if($bookingDataArr['iDestinationCC']==0 && $bookingDataArr['iOriginCC']==0)
	{
            $serviceString .=".";
	}
	
	if($kForwarder->szPdfLogo!='')
	{
            $logoPath=__APP_PATH__."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
            if(file_exists($logoPath))
            {
                $logoPath=__MAIN_SITE_HOME_PAGE_URL__."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
            }
            else
            {
                $logoPath=__MAIN_SITE_HOME_PAGE_URL__."/images/forwarders/".$kForwarder->szPdfLogo;
            }
	} 
	if(!empty($bookingDataArr['dtCutOff']))
	{
            $pickUpDate=date('d/m/Y H:i',strtotime($bookingDataArr['dtCutOff']));
	}
	if(!empty($bookingDataArr['dtAvailable']))
	{
            $deliveryDate=date('d/m/Y H:i',strtotime($bookingDataArr['dtAvailable']));
	}
	$kUser->getUserDetails($bookingDataArr['idUser']);
	
	       	
	 $billcountry=$kConfig->getCountryName($bookingDataArr['szCustomerCountry']);
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerCompanyName'],$utf8Flag)."<br />";         	
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerAddress1'],$utf8Flag);
	
	if(!empty($bookingDataArr['szCustomerAddress2']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress2'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerAddress3']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress3'],$utf8Flag);			
	if(!empty($bookingDataArr['szCustomerPostCode']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerPostCode'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerCity']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerState']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerState'],$utf8Flag);
	if(!empty($billcountry))
			$billingAddress .=", ".html_entities_flag($billcountry,$utf8Flag);
	else
			$billingAddress .="<br />";
	$fTotalPriceCustomerCurrency=number_format($bookingDataArr['fTotalPriceCustomerCurrency'], 2);
	//$fTotalPriceCustomerCurrency=number_format((float)$bookingDataArr['fTotalPriceCustomerCurrency'],2)	;	
			
        $kConfig_new = new cConfig();
        $kConfig_new->loadCountry($bookingDataArr['idCustomerDialCode']);
        $iInternationDialCode = $kConfig_new->iInternationDialCode;

	$HeadingInvoice=__MAIN_SITE_HOME_PAGE_URL__."/images/HeadingInvoice.jpg";
	$string .= '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<title>Booking Confirmation</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			</head>
			<body>	
			<table cellpadding="3" cellspacing="1" border="0" width="650" valign="bottom">
				<tr>
					<td align="left" valign="bottom" width="70%"><img src="'.$logoPath.'" width="80" hieght="30"></td>
					<td align="right" valign="bottom" width="30%"><img src="'.$HeadingInvoice.'" width="200" hieght="50"></td>
				</tr>
			</table>	
			<table cellpadding="3" cellspacing="1" border="0" width="650">
				<tr>
					<td valign="top" width="60%" style="font-family:Cambria;font-size:13px;background:#fff;">
						'.$forwarderDetails.'E-mail: '.html_entities_flag($kForwarderContact->mainAdminEmail,$utf8Flag).'<br />Phone: '.$kForwarder->szPhone.'
					</td>	
					<td valign="top" width="40%" style="font-family:Cambria;font-size:14px;" border="1">
						<strong>Invoice Number: '.$bookingDataArr['szInvoice'].'</strong>
						<br /><strong>Invoice Date: '.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).'</strong>
						<br /><strong>Booking Reference: '.$bookingDataArr['szBookingRef'].'</strong>						
					</td>	
				</tr>
			 </table>
			 <table cellpadding="3" cellspacing="1" border="0" width="650">
				<tr>
					<td valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
					<strong style="font-size:14px;">Billing Address:</strong>
					<br />
					'.$billingAddress.'<br />Contact: '.html_entities_flag($bookingDataArr['szFirstName'],$utf8Flag).' '.html_entities_flag($bookingDataArr['szLastName'],$utf8Flag).'<br />E-mail: '.html_entities_flag($bookingDataArr['szEmail'],$utf8Flag).'<br />Phone: +'.$iInternationDialCode." ".$bookingDataArr['szCustomerPhoneNumber'].'<br />
					<br />
				</td>
				</tr>
			</table>';
		if($bookingDataArr['szInvoiceComment']!='')
		{	
			$string .='<table cellpadding="3" cellspacing="1" border="0" width="650">
				<tr>
					<td valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
					<strong style="font-size:14px;">Invoice Comments:</strong>
					<br />'.html_entities_flag($bookingDataArr['szInvoiceComment'],$utf8Flag).'
					</td>
				</tr>
			</table>';
		}
		
			$string .='<table cellpadding="3" cellspacing="1" border="1" width="650">
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						<strong>Transportation Service Description</strong>
					</td>
					<td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Total</strong></td>
				</tr>
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						'.$serviceString.', all inclusive<br />
						<br />'.$pickAddressText.': '.$szWareHouseFromStr.'<br />'.$picktimetext.': After '.$pickUpDate.'<br />
						<br />'.$deliveryAddressText.': '.$szWareHouseToStr.'<br />'.$deliverytimetext.': Before '.$deliveryDate.'<br />
						<br />
						<br />Cargo: Maximum '.format_volume($bookingDataArr['fCargoVolume']).' cbm and '.get_formated_cargo_measure($fCargoWeight).' mt, containing '.$str.'</td><td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;">'.$bookingDataArr['szCurrency'].' '.$fTotalPriceCustomerCurrency.'</td></tr>
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						<strong>Invoice Total</strong>
					</td>
					<td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.$bookingDataArr['szCurrency'].' '.$fTotalPriceCustomerCurrency.'</strong></td>
				</tr>
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						<strong>Paid by '.$bookingDataArr['iPaymentType'].' in '.date('d F Y',strtotime($bookingDataArr['dtBookingConfirmed'])).'</strong>
					</td>
					<td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.$bookingDataArr['szCurrency'].' '.$fTotalPriceCustomerCurrency.'</strong></td>
				</tr>
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						<strong>Balance</strong>
					</td>
					<td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.$bookingDataArr['szCurrency'].' 0.00</strong></td>
				</tr>
			</table>
			<p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;">Thank you for your business.<br />
			<br />Please review our service on www.transporteca.com after '.date('d. F Y',strtotime($bookingDataArr['dtAvailable'])).'<br /><br />
			</p>
			<p style="TEXT-ALIGN:CENTER;FONT-SIZE:MEDIUM;">
						This invoice is available in soft copy on www.transporteca.com under My Bookings.
						'.html_entities_flag($bookingDataArr['szForwarderDispName'],$utf8Flag).'\'s standard trading terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).' apply to this booking.
					If you have any questions or concerns about this invoice, please contact '.$customerServiceEmailStr.'.
			</p>				
		</body>
		</html>'; 
	$pdf->WriteHTML($string,true);
	
	$file_name = __APP_PATH__."/invoice/Booking-Invoice-".$bookingDataArr['szInvoice'].".pdf";
   	//$file = "Booking-Invoice.pdf";
   	
   	$pdf->Output($file_name,'F');
   	
   	return $file_name;
}


function getCreditNoteConfirmationPdfFileHTML($idBooking,$flag=false,$utf8Flag=false)
{
	require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php');
	$kBooking = new cBooking();
	$kForwarder = new cForwarder();
	$kForwarderContact = new cForwarderContact();
	$kConfig = new cConfig();
	 $kUser = new cUser(); 
	
	$pdf=new HTML2FPDFBOOKING();
	$pdf->AddPage('',true);
	
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
        
        if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
        }
        if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
        }
        if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
        }
        if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
        }
        
	$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
	$transactionDataAry = $kBooking->getTransionDetailsByBookingId($idBooking,true);
	$szCreditNoteNum = $transactionDataAry[0]['szInvoice'];
	
	$forwarderDetails .= $bookingDataArr['szForwarderRegistName'].'<br />';
	if($bookingDataArr['szForwarderAddress']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderAddress'].'<br />';
	if($bookingDataArr['szForwarderAddress2']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderAddress2'].'<br />';
	if($bookingDataArr['szForwarderPostCode']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderPostCode'].'<br />';
	if($bookingDataArr['szForwarderCity']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderCity'].'<br />';
		
		
	$kForwarder->load($bookingDataArr['idForwarder']);   
	 
	$idServiceType = $bookingDataArr['iOriginCC'];
	$kConfig = new cConfig();
	
	// geting service type 
	//$serviceTypeAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$idServiceType);
        $configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
        $serviceTypeAry = $configLangArr[$idServiceType];
	$serviceString = $serviceTypeAry['szDescription'];
	if($bookingDataArr['iOriginCC']==1 && $bookingDataArr['iDestinationCC']==0)
	{
            $serviceString .=" and customs clearance at origin";
	}
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==0)
	{
            $serviceString .=" and customs clearance at destination";
	}
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==1)
	{
            $serviceString .=" and customs clearance at origin and destination";
	}
	if($bookingDataArr['iDestinationCC']==0 && $bookingDataArr['iOriginCC']==0)
	{
            $serviceString .="";
	}
	
 	if($flag==true)
	{
            $image_url=__MAIN_SITE_HOME_PAGE_URL__;
	}
	else
	{
            $image_url=__APP_PATH_ROOT__;
	}
	
	if($kForwarder->szPdfLogo!='')
	{
            $logoPath=__APP_PATH_ROOT__."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
            if(file_exists($logoPath))
            {
                $logoPath=$image_url."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
            }
            else
            {
                $logoPath=$image_url."/images/forwarders/".$kForwarder->szPdfLogo;
            }
	}
	
	$kUser->getUserDetails($bookingDataArr['idUser']);
		       	
	$billcountry=$kConfig->getCountryName($bookingDataArr['szCustomerCountry']);
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerCompanyName'])."<br />";         	
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerAddress1'],$utf8Flag);
	
	if(!empty($bookingDataArr['szCustomerAddress2']))
            $billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress2'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerAddress3']))
            $billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress3'],$utf8Flag);			
	if(!empty($bookingDataArr['szCustomerPostCode']))
            $billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerPostCode'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerCity']))
	{
            if(!empty($bookingDataArr['szCustomerPostCode']))
                $billingAddress .=" ".html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
            else
                $billingAddress .=html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
	}
	if(!empty($bookingDataArr['szCustomerState']))
			$billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerState'],$utf8Flag);
	if(!empty($billcountry))
			$billingAddress .=", ".html_entities_flag($billcountry,$utf8Flag);
	else
			$billingAddress .="<br />";
         
        $fPriceCustomerCurrency = number_format($bookingDataArr['fTotalPriceCustomerCurrency'], 2);
        $fTotalVat = number_format($bookingDataArr['fTotalVat'], 2); 
	$fTotalPriceCustomerCurrency=number_format(($bookingDataArr['fTotalPriceCustomerCurrency'] +$bookingDataArr['fTotalVat']), 2); 
	
	if($flag==true)
	{
            $body_info='style="background:#000;text-align:center;"';
            $width='width="650"';

            $align='align="center"';
            $bodyfontsize='style="font-size:12px;"';
            $printStringStart='<p style="text-align:right;margin:5px auto 10px;width:690px;">
            <a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href='.__BASE_URL__.'/downloadCreditNote/'.$idBooking.'/ target="_blank">Download PDF</a>&nbsp;&nbsp;&nbsp;
            <a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href="javascript:void(0)" onclick="PrintDiv();">Print</a></p>
            <div id="viewInvoice" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;">';
            $printStringEnd='</div>';
            $script='<script>
            function PrintDiv()
            {    
                  var divToPrint = document.getElementById("viewInvoice");
                  var popupWin = window.open("", "_blank", "width=900,height=700");
                  popupWin.document.open();
                  popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                  popupWin.document.close();
            }
            </script>';
	}
	else
	{
            $width='width="650"';

            $align='';
            $bodyfontsize='';
            $script='';
            $printStringEnd='';
	}
	if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_1__ || trim($bookingDataArr['iPaymentType'])=="Zooz")
	{
            $iPaymentType="Credit Card";
	}
        else if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
	{
            $iPaymentType = "bank account";
	}
	else
	{
            $iPaymentType=$bookingDataArr['iPaymentType'];
	}
	
	if($flag==true)
	{
            $itemDescriptionText = "Booking ".$bookingDataArr['szBookingRef']." invoiced on ".date('j. F Y',strtotime($bookingDataArr['dtBookingConfirmed']))." with invoice number ".$bookingDataArr['szInvoice'].".<br/><br/>Payment will be reversed and refunded to your ".$iPaymentType.".<br/><br/>Refund can take up to 10 banking days for processing.";
	}
	else
	{
            $itemDescriptionText = "<p>Booking ".$bookingDataArr['szBookingRef']." invoiced on ".date('j. F Y',strtotime($bookingDataArr['dtBookingConfirmed']))." with invoice number ".$bookingDataArr['szInvoice'].".</p>
		<br /><p>Payment will be reversed and refunded to your ".$iPaymentType.".</p>
		<br /><p>Refund can take up to 10 banking days for processing.<p>";
	}
        
        $kConfig_new = new cConfig();
        $kConfig_new->loadCountry($bookingDataArr['idCustomerDialCode']);
        $iInternationDialCode = $kConfig_new->iInternationDialCode;
	
	$HeadingInvoice = $image_url."/images/CreditNote.jpg";
	$string .= '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <title>Credit Note</title>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            '.$script.'
            </head>
            <body '.$body_info.'>	
            '.$printStringStart.'
            <table cellpadding="3" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                    <tr>
                            <td align="left" valign="bottom" width="70%"><img src="'.$logoPath.'" width="150" hieght="60"></td>
                            <td align="right" valign="bottom" width="30%"><img src="'.$HeadingInvoice.'" width="200" hieght="50"></td>
                    </tr>
            </table>	
            <table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
                    <tr>
                            <td valign="top" width="60%" style="font-family:Cambria;font-size:13px;background:#fff;">
                                    '.$forwarderDetails.'E-mail: '.html_entities_flag($kForwarder->szGeneralEmailAddress,$utf8Flag).'<br />Phone: '.$kForwarder->szPhone.'
                            </td>	
                            <td valign="top" width="40%" style="font-family:Cambria;font-size:14px;border:solid 1px #000;" border="1">
                                    <strong>Credit Note  Number: '.$szCreditNoteNum.'</strong>
                                    <br /><strong>Credit Note Date: '.date('d F Y',strtotime($bookingDataArr['dtBookingCancelled'])).'</strong>
                                    <br /><strong>Booking Reference: '.$bookingDataArr['szBookingRef'].'</strong>						
                            </td>	
                    </tr>
             </table>
             <table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
                    <tr>
                            <td valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
                            <strong style="font-size:14px;">Customer:</strong>
                            <br />'.$billingAddress.'<br />Contact: '.html_entities_flag($bookingDataArr['szFirstName']).' '.html_entities_flag($bookingDataArr['szLastName']).'<br />E-mail: '.html_entities_flag($bookingDataArr['szEmail'],$utf8Flag).'<br />Phone: +'.$iInternationDialCode." ".$bookingDataArr['szCustomerPhoneNumber'].'<br />
                            <br />
                    </td>
                    </tr>
            </table>';
            $string .='<table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
                    <tr>
                            <td valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
                            <strong style="font-size:14px;">Reason for Credit Note:</strong>
                            <br />'.html_entities_flag($bookingDataArr['szInvoiceComment'],$utf8Flag).'
                            </td>
                    </tr>
            </table>
            <br />
            ';

            if($bookingDataArr['fTotalVat']>0)
            {
                 $szVatString = '
                     <tr>
                        <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;border-top:none;">VAT<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;border-top:none;">'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fTotalVat,$utf8Flag).'</td> 
                    </tr>
                ';
            }
            $string .='
                <table cellpadding="3" cellspacing="0" border="1" '.$width.' '.$align.'>
                    <tr>
                        <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Item Description</strong></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Total</strong></td>
                    </tr>
                    <tr>
                        <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;border-bottom:none;">'.$itemDescriptionText.'<br /></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;border-bottom:none;">'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fPriceCustomerCurrency,$utf8Flag).'</td> 
                    </tr>
                    '.$szVatString.'
                    <tr>
                        <td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Credit Note Total</strong></td>
                        <td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag(($fTotalPriceCustomerCurrency),$utf8Flag).'</strong></td>
                    </tr>
            </table>';
            $idForwarder = $bookingDataArr['idForwarder'];
            $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder,__BOOKING_PROFILE_ID__);
            $customerServiceEmailLinkAry = format_fowarder_emails($customerServiceEmailAry);
            $customerServiceEmailLink = $customerServiceEmailLinkAry[0];
            $customerServiceEmailStr = $customerServiceEmailLinkAry[1];

            if($flag==true)
            {
                $string .='<br /><table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
                    <tr>
                        <td style="TEXT-ALIGN:CENTER";>
                            '.html_entities_flag($bookingDataArr['szForwarderDispName'],$utf8Flag).'\'s standard trading terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).' apply.
                            If you have any questions or concerns about this credit note, please contact '.$customerServiceEmailStr.'.
                        </td>
                    <tr>
                    <tr>
                        <td align="right"><br /><br /><br /><br /><br /><br /><br /><img src='.$image_url.'/images/Powered_By.jpg width="170"></td>
                    </tr>
                </table>';
            }
            else
            {
                $string .='
                    <p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;">
                        '.html_entities_flag($bookingDataArr['szForwarderDispName'],$utf8Flag).'\'s standard trading terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).' apply.
                        If you have any questions or concerns about this credit note, please contact '.$customerServiceEmailStr.'.
                    </p>';
            }	
            $string .=$printStringEnd;			
            $string .='</body>
            </html>';
	if($flag==true)
	{		
            echo $string;
	}
	else
	{
            $pdf->WriteHTML($string,true);	
            $file_name = __APP_PATH_ROOT__."/invoice/Credit-Note-".$bookingDataArr['szInvoice'].".pdf";
            //$file = "Booking-Invoice.pdf";
	  		if(file_exists($file_name))
            {
                @unlink($file_name);
            }
            $pdf->Output($file_name);

            return $file_name;
	}
}


function getInsuranceCreditNotePdf($idBooking,$flag=false,$utf8Flag=false,$get_flag=false)
{
	require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php');
	$kBooking = new cBooking();
	$kForwarder = new cForwarder();
	$kForwarderContact = new cForwarderContact();
	$kConfig = new cConfig();
	 $kUser = new cUser(); 
	
	$pdf=new HTML2FPDFBOOKING();
	$pdf->AddPage('',true);
	
	$bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
	
        if(!empty($bookingDataArr['dtActualAvailable']) && $bookingDataArr['dtActualAvailable']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtAvailable'] = $bookingDataArr['dtActualAvailable'];
        }
        if(!empty($bookingDataArr['dtActualWhsAvailabe']) && $bookingDataArr['dtActualWhsAvailabe']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsAvailabe'] = $bookingDataArr['dtActualWhsAvailabe'];
        }
        if(!empty($bookingDataArr['dtActualCutOff']) && $bookingDataArr['dtActualCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtCutOff'] = $bookingDataArr['dtActualCutOff'];
        }
        if(!empty($bookingDataArr['dtActualWhsCutOff']) && $bookingDataArr['dtActualWhsCutOff']!='0000-00-00 00:00:00')
        {
            $bookingDataArr['dtWhsCutOff'] = $bookingDataArr['dtActualWhsCutOff'];
        }
        
	$forwardCountry=$kConfig->getCountryName($bookingDataArr['idForwarderCountry']);
	$transactionDataAry = $kBooking->getTransionDetailsByBookingId($idBooking,true);
	$szCreditNoteNum = $bookingDataArr['szInsuranceCreditNoteInvoice'];
	
	$forwarderDetails .= $bookingDataArr['szForwarderRegistName'].'<br />';
	if($bookingDataArr['szForwarderAddress']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderAddress'].'<br />';
	if($bookingDataArr['szForwarderAddress2']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderAddress2'].'<br />';
	if($bookingDataArr['szForwarderPostCode']!='')
		$forwarderDetails .= $bookingDataArr['szForwarderPostCode'].'<br />';
	if($bookingDataArr['szForwarderCity']!='')	
		$forwarderDetails .= $bookingDataArr['szForwarderCity'].'<br />';
		
		
	$kForwarder->load($bookingDataArr['idForwarder']);   
	 
	$idServiceType = $bookingDataArr['iOriginCC'];
	$kConfig = new cConfig();
	
	// geting service type 
	//$serviceTypeAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_SERVICE_TYPE__,$idServiceType);
	$configLangArr=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
        $serviceTypeAry = $configLangArr[$idServiceType];
	$serviceString = $serviceTypeAry['szDescription'];
        
	if($bookingDataArr['iOriginCC']==1 && $bookingDataArr['iDestinationCC']==0)
	{
		$serviceString .=" and customs clearance at origin";
	}
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==0)
	{
		$serviceString .=" and customs clearance at destination";
	}
	if($bookingDataArr['iDestinationCC']==2 && $bookingDataArr['iOriginCC']==1)
	{
		$serviceString .=" and customs clearance at origin and destination";
	}
	if($bookingDataArr['iDestinationCC']==0 && $bookingDataArr['iOriginCC']==0)
	{
		$serviceString .="";
	}
	
 	if($flag==true)
	{
		$image_url=__MAIN_SITE_HOME_PAGE_URL__;
	}
	else
	{
		$image_url=__APP_PATH_ROOT__;
	}
	
	if($kForwarder->szPdfLogo!='')
	{
		$logoPath=__APP_PATH_ROOT__."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
		if(file_exists($logoPath))
		{
			$logoPath=$image_url."/images/forwarders/thumbs/150x60_".$kForwarder->szPdfLogo;
		}
		else
		{
			$logoPath=$image_url."/images/forwarders/".$kForwarder->szPdfLogo;
		}
	}
	
	$kUser->getUserDetails($bookingDataArr['idUser']);
		       	
	$billcountry=$kConfig->getCountryName($bookingDataArr['szCustomerCountry']);
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerCompanyName'])."<br />";         	
	$billingAddress .=html_entities_flag($bookingDataArr['szCustomerAddress1'],$utf8Flag);
	
	if(!empty($bookingDataArr['szCustomerAddress2']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress2'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerAddress3']))
			$billingAddress .=", ".html_entities_flag($bookingDataArr['szCustomerAddress3'],$utf8Flag);			
	if(!empty($bookingDataArr['szCustomerPostCode']))
			$billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerPostCode'],$utf8Flag);
	if(!empty($bookingDataArr['szCustomerCity']))
	{
		if(!empty($bookingDataArr['szCustomerPostCode']))
			$billingAddress .=" ".html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
		else
			$billingAddress .=html_entities_flag($bookingDataArr['szCustomerCity'],$utf8Flag);
	}
	if(!empty($bookingDataArr['szCustomerState']))
			$billingAddress .="<br />".html_entities_flag($bookingDataArr['szCustomerState'],$utf8Flag);
	if(!empty($billcountry))
			$billingAddress .=", ".html_entities_flag($billcountry,$utf8Flag);
	else
			$billingAddress .="<br />";
	$fTotalPriceCustomerCurrency=number_format($bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'], 2);
	
	if($flag==true)
	{
		$body_info='style="background:#000;text-align:center;"';
		$width='width="650"';
		
		$align='align="center"';
		$bodyfontsize='style="font-size:12px;"';
		$printStringStart='<p style="text-align:right;margin:5px auto 10px;width:690px;">
		<a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href='.__BASE_URL__.'/downloadInsuranceCreditNote/'.$idBooking.'/ target="_blank">Download PDF</a>&nbsp;&nbsp;&nbsp;
		<a style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;" href="javascript:void(0)" onclick="PrintDiv();">Print</a></p>
		<div id="viewInvoice" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;">';
		$printStringEnd='</div>';
		$script='<script>
		function PrintDiv()
		{    
		      var divToPrint = document.getElementById("viewInvoice");
		      var popupWin = window.open("", "_blank", "width=900,height=700");
		      popupWin.document.open();
		      popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
		      popupWin.document.close();
		}
		</script>';
	}
	else
	{
            $width='width="650"';

            $align='';
            $bodyfontsize='';
            $script='';
            $printStringEnd='';
	} 
	if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_1__ || $bookingDataArr['iPaymentType']=="Zooz")
	{
            $iPaymentType="Credit Card";
	}
	else if($bookingDataArr['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
	{
            $iPaymentType = "bank account";
	}
        else
        {
            $iPaymentType = $bookingDataArr['iPaymentType'];
        }
	
	if($flag==true)
	{
            $itemDescriptionText = "Booking ".$bookingDataArr['szBookingRef']." invoiced on ".date('j. F Y',strtotime($bookingDataArr['dtBookingConfirmed']))." with invoice number ".$bookingDataArr['szInsuranceInvoice'].".<br/><br/>Payment will be reversed and refunded to your ".$iPaymentType.".<br/><br/>Refund can take up to 10 banking days for processing.";
	}
	else
	{
            $itemDescriptionText = "<p>Insurance for booking ".$bookingDataArr['szBookingRef']." invoiced on ".date('j. F Y',strtotime($bookingDataArr['dtBookingConfirmed']))." with invoice number ".$bookingDataArr['szInsuranceInvoice'].".</p>
                <br /><p>Payment will be reversed and refunded to your ".$iPaymentType.".</p>
                <br /><p>Refund can take up to 10 banking days for processing.<p>
            ";
	}
        $kConfig_new = new cConfig();
        $kConfig_new->loadCountry($bookingDataArr['idCustomerDialCode']);
        $iInternationDialCode = $kConfig_new->iInternationDialCode;
	
	$logoPath=$image_url."/images/transporteca-logo.jpg";
	$HeadingInvoice = $image_url."/images/CreditNote.jpg";
	$string .= '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<title>Credit Note</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			'.$script.'
			</head>
			<body '.$body_info.'>	
			'.$printStringStart;
                        if($flag==true || $get_flag)
                        {		
                            $string .= '<table cellpadding="3" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                                    <tr>
                                            <td align="left" valign="bottom" width="70%"><img src="'.$logoPath.'" width="150" hieght="60"></td>
                                            <td align="right" valign="bottom" width="30%"><img src="'.$HeadingInvoice.'" width="200" hieght="50"></td>
                                    </tr>
                            </table>';
                        }else
                        {
                            $string .= '<table cellpadding="3" cellspacing="1" border="0" '.$width.' valign="bottom" '.$align.'>
                                    <tr>
                                            <td align="left" valign="bottom" width="70%">&nbsp;</td>
                                            <td align="right" valign="bottom" width="30%">&nbsp;</td>
                                    </tr>
                            </table>';
                        }
			$string .= '<table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
					<td valign="top" width="60%" style="font-family:Cambria;font-size:13px;background:#fff;">
						'.__TRANSPORTECA_ADDRESS__.'
					</td>	
					<td valign="top" width="40%" style="font-family:Cambria;font-size:14px;border:solid 1px #000;" border="1">
						<strong>Invoice Number: '.$szCreditNoteNum.'</strong>
						<br /><strong>Invoice Date: '.date('d F Y',strtotime($bookingDataArr['dtInsuranceStatusUpdated'])).'</strong>
						<br /><strong>Booking Reference: '.$bookingDataArr['szBookingRef'].'</strong>						
					</td>	
				</tr>
			 </table>
			 <table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
					<td valign="top" style="font-family:Cambria;font-size:13px;background:#fff;">
					<strong style="font-size:14px;">Customer:</strong>
					<br />'.$billingAddress.'<br />Contact: '.html_entities_flag($bookingDataArr['szFirstName']).' '.html_entities_flag($bookingDataArr['szLastName']).'<br />E-mail: '.html_entities_flag($bookingDataArr['szEmail'],$utf8Flag).'<br />Phone: +'.$iInternationDialCode." ".$bookingDataArr['szCustomerPhoneNumber'].'<br />
					<br />
				</td>
				</tr>
			</table>'; 
			$string .='<table cellpadding="3" cellspacing="0" border="1" '.$width.' '.$align.'>
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						<strong>Item Description</strong>
					</td>
					<td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>Total</strong></td>
				</tr>
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						'.$itemDescriptionText.'<br />
						</td><td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;">'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fTotalPriceCustomerCurrency,$utf8Flag).'</td></tr>
				<tr>
					<td width="85%" valign="top" style="font-family:Cambria;font-size:14px;background:#fff;">
						<strong>Credit Note Total</strong>
					</td>
					<td width="15%" valign="top" align="right" style="font-family:Cambria;font-size:14px;background:#fff;"><strong>'.html_entities_flag($bookingDataArr['szCurrency'],$utf8Flag).' '.html_entities_flag($fTotalPriceCustomerCurrency,$utf8Flag).'</strong></td>
				</tr>
			</table>';
			$idForwarder = $bookingDataArr['idForwarder'];
			$customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder,__BOOKING_PROFILE_ID__);
			$customerServiceEmailLinkAry = format_fowarder_emails($customerServiceEmailAry);
			$customerServiceEmailLink = $customerServiceEmailLinkAry[0];
			$customerServiceEmailStr = $customerServiceEmailLinkAry[1];
			
			if($flag==true)
			{
				$string .='<br /><table cellpadding="3" cellspacing="1" border="0" '.$width.' '.$align.'>
				<tr>
					<td style="TEXT-ALIGN:CENTER";>
							'.html_entities_flag($bookingDataArr['szForwarderDispName'],$utf8Flag).'\'s standard trading terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).' apply.
						If you have any questions or concerns about this credit note, please contact '.$customerServiceEmailStr.'.
					</td>
				<tr>
				<tr>
					<td align="right"><br /><br /><br /><br /><br /><br /><br /><img src='.$image_url.'/images/Powered_By.jpg width="170"></td>
				</tr>
				</table>';
			}
			else
			{
				$string .='
				<p style="TEXT-ALIGN:CENTER;FONT-SIZE:SMALL;">
                                        '.html_entities_flag($bookingDataArr['szForwarderDispName'],$utf8Flag).'\'s standard trading terms available on '.html_entities_flag($kForwarder->szLink,$utf8Flag).' apply.
                                    If you have any questions or concerns about this credit note, please contact '.$customerServiceEmailStr.'.
				</p>';
			}	
		$string .=$printStringEnd;			
		$string .='</body>
		</html>';
	if($flag==true)
	{		
		echo $string;
	}
        else if($get_flag)
        {
            return $string;
        }
	else
	{
            $pdf->Image($logoPath,10,10,30,10);
            $pdf->Image($HeadingInvoice,170,10,30,10);
		$pdf->WriteHTML($string,true);	
		$file_name = __APP_PATH_ROOT__."/invoice/Credit-Note-".$bookingDataArr['szInsuranceCreditNoteInvoice'].".pdf";
   		//$file = "Booking-Invoice.pdf";
		if(file_exists($file_name))
            {
                @unlink($file_name);
            }
   		$pdf->Output($file_name);
   	
   		return $file_name;
	}
} 

function getForwarderInvoice($idForwarder,$batchNumber,$flag='',$get_string_flag=false)
{
	$version="Forwarder - Transporteca Invoices".$idForwarder;
	$kForwarder= new cForwarder();
	$kBooking = new cBooking();
	$addDetail=$kForwarder->detailsForwarder($idForwarder);
	//$emailDetails=$kForwarder->detailsForwarderBillingEmail($idForwarder);
	$emailDetails=$kForwarder->detailsForwarderBillingGeneralEmail($idForwarder);
	$invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,'',$batchNumber);
	$uploadServiceAmount = $kBooking->getUploadServiceAmount($idForwarder,$batchNumber);
	$date=date("Y-m-d H:i:s"); 
       
        $fTotalVal = 0;
	if($invoiceBillConfirmed)
	{	
            $data='';
            foreach($invoiceBillConfirmed as $inv)
            {	
                if($inv['iDebitCredit']==1 || $inv['iDebitCredit']==6)
                {
                    if(($inv['iDebitCredit']==6 && $inv['fReferalAmount']>0) || ($inv['iDebitCredit']==1))
                    {
                        $idBooking = $inv['idBooking'] ;
                        $postSearchAry = array();
                        $postSearchAry = $kBooking->getBookingDetailsCountryName($idBooking); 
                        $szTrade = $postSearchAry['szShipperCity']." - ".$postSearchAry['szConsigneeCity'] ;

                        if(!empty($postSearchAry['szForwarderReference']))
                        {
                            $szForwarderReference = $postSearchAry['szForwarderReference'] ;
                        }
                        else
                        {
                            $szBookingRefNo=explode(":",$inv['szBooking']);
                            $szForwarderReference = $szBookingRefNo[1];
                        } 
                        
                        $fTotalPriceForwarderCurrency=$inv['fTotalPriceForwarderCurrency'];
                        
                        $dtInvoiceOn = date('d-m-Y',strtotime($inv['dtInvoiceOn']));
                        
                        if(($inv['iDebitCredit']==1 || $inv['iDebitCredit']==6) && strtotime($dtInvoiceOn)>strtotime(__SHOW_MIN_HANDLING_FEE_DATE__))
                        {
                            $bookingDetailArr = $kBooking->getBookingDetails($idBooking);
                            if($bookingDetailArr['iHandlingFeeApplicable']==1)
                            {
                                    $fTotalPriceForwarderCurrency=$bookingDetailArr['fTotalForwarderPriceWithoutHandlingFee'];
//                                $fTotalHandlingFeeUSD=$bookingDetailArr['fTotalHandlingFeeUSD'];
//                                $fTotalHandlingFeeInForwarderCurrencyVat=0;
//                                $fTotalHandlingFeeInForwarderCurrency=round((float)($fTotalHandlingFeeUSD/$bookingDetailArr['fForwarderExchangeRate']),2);
//                                if($bookingDetailArr['fVATPercentage']>0)
//                                {
//                                    $fTotalHandlingFeeInForwarderCurrencyVat=round((float)($fTotalHandlingFeeInForwarderCurrency*.01*$bookingDetailArr['fVATPercentage']),2);
//                                }
//                                
//                                $fTotalPriceForwarderCurrency=$fTotalPriceForwarderCurrency-($fTotalHandlingFeeInForwarderCurrency+$fTotalHandlingFeeInForwarderCurrencyVat);
                            }
                        }
                        
					
					$data.="
							<tr>
							<td width='15%' valign='top'>".date('d F Y', strtotime($inv['dtPaymentConfirmed']))."</td>
							<td width='15%' valign='top'>".convertUTF($szForwarderReference)."</td>
							<td width='20%' valign='top'>".convertUTF($szTrade)."</td>";
							if($inv['iDebitCredit']==6)
                                                            $data.="<td ALIGN='RIGHT' width='15%' valign='top'>(".convertUTF($inv['szForwarderCurrency'])." ".convertUTF(number_format($fTotalPriceForwarderCurrency,2)).")</td>";
							else
                                                            $data.="<td ALIGN='RIGHT' width='15%' valign='top'>".convertUTF($inv['szForwarderCurrency'])." ".convertUTF(number_format($fTotalPriceForwarderCurrency,2))."</td>";
						
							$data.="<td ALIGN='CENTER' width='10%' valign='top'>".$inv['fReferalPercentage']." %</td>";
							if($inv['iDebitCredit']==6 && $inv['fReferalAmount']>0)
                                                        {
                                                            $fTotalVal -= $inv['fReferalAmount'] ;
                                                            $data.="<td ALIGN='RIGHT' width='15%' valign='top'>(".convertUTF($inv['szForwarderCurrency'])." ".number_format($inv['fReferalAmount'],2).")</td></tr>";
                                                        }
							else
                                                        {
                                                            $fTotalVal += $inv['fReferalAmount'] ;
                                                            $data.="<td ALIGN='RIGHT' width='15%' valign='top'>".convertUTF($inv['szForwarderCurrency'])." ".number_format($inv['fReferalAmount'],2)."</td></tr>";
                                                        }
							
						$curency=$inv['szForwarderCurrency'];
                                                
				}
			}
			if($inv['iDebitCredit']==3)
			{
                            if($inv['fTotalPriceForwarderCurrency']<0.00)
                            {
                                $amountValue=str_replace("-","",$inv['fTotalPriceForwarderCurrency']);
                                $array ="(".convertUTF($inv['szCurrency'])." ".($inv['fTotalPriceForwarderCurrency']?number_format((float)$amountValue,2):'').")";
                            }
                            else
                            {
				$array =convertUTF($inv['szCurrency'])." ".($inv['fTotalPriceForwarderCurrency']?number_format((float)$inv['fTotalPriceForwarderCurrency'],2):'');
                            }
				$to=$inv['dtPaymentConfirmed'];
				$ref=$inv['szInvoice'];
			}
			if(strtotime($date)>strtotime($inv['dtCreatedOn']))
			{
				 $date=$inv['dtCreatedOn'];
			}
		}
	
	} 
		$datefrom=($date!='0000-00-00 00:00:00')?date('d F Y',strtotime($date)):'';
		$timefrom=($date!='0000-00-00 00:00:00')?date('H:i',strtotime($date)):'';
		$dateto=($to!='0000-00-00 00:00:00')?date('d F Y',strtotime($to)):'';
		$timeto=($to!='0000-00-00 00:00:00')?date('H:i',strtotime($to)):'';
		$add='';
		if($addDetail['szCompanyName'])
		{
			$add.="<br />".$addDetail['szCompanyName']."<br />";
		}
		if($addDetail['szAddress'])
		{
			$add.=$addDetail['szAddress']."<br />";
		}
		if($addDetail['szAddress2'])
		{
			$add.=$addDetail['szAddress2']."<br />";
		}
		if($addDetail['szAddress3'])
		{
			$add.=$addDetail['szAddress3']."<br />";
		}
		if($addDetail['szCity'])
		{
			$add.=$addDetail['szCity'];
		}
		if($addDetail['szCity'] && $addDetail['szState'])
		{
			$add.=' , ';
		}
		if($addDetail['szState'])
		{
			$add.=$addDetail['szState'];
		}
		if($addDetail['szCountryName'])
		{
			$add.="<br />".$addDetail['szCountryName'];
		}
		if($emailDetails)
		{	
			$num=count($emailDetails);
			foreach($emailDetails as $key=>$value)
			{
				$emailLog.=$value['szGeneralEmailAddress'];
				$num--;
				if($num>1)
				{
					$emailLog.= ", ";
				}
				else if($num==1)
				{
					$emailLog.= ' and ';
				}
			}
		}
  		$link=$addDetail['szControlPanelUrl'];
  
	
  //$pdf->Image('invoice.jpg',120,12,0);
  //$array=explode(',',$array);
               // echo $fTotalVal;
  if($flag!='PDF')
  {
  	$body_info='style="background:#000;text-align:center;font-size:14px;"';
  	$image_url=__MAIN_SITE_HOME_PAGE_URL__;
  	$dataview='<p style="text-align:right;margin:5px auto 10px;width:690px;">
			<a target="_blank" href='.__BASE_URL__.'/downloadForwarderInvoice/'.$idForwarder.'/'.$batchNumber.'/ style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>
			<a onclick="PrintDiv();" href="javascript:void(0)" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a>
			</p>';
  	$script='<script>
			function PrintDiv()
			{    
                            var divToPrint = document.getElementById("forwarderTransfer");
                            var popupWin = window.open("", "_blank", "width=900,height=900");
                            popupWin.document.open();
                            popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                            popupWin.document.close();
			}
			</script>';
  }
  else
  {
  	$image_url=__APP_PATH_ROOT__;
  	$dataview='';
  	$script='';
  }
  	  $contents = '<?xml version="1.0" encoding="iso-8859-1"?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>Transporteca Invoice</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		'.$script.'
		</head>
		<body '.$body_info.'>'.$dataview.'
			<div id="forwarderTransfer" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" >';
                        if($get_string_flag)
                        {
                            $contents .= '<table  border="0" cellspacing="0" cellpadding="0" width="650px">
                                <tr style="margin-bottom:12px;">
                                <td colspan="2" valign="top" width="312px" align="left"><img src='.$image_url.'/images/TransportecaMail.jpg height="53px" width="217px"></td>
                                    <td colspan="2" valign="middle" width="40%" align="right"><img src='.$image_url.'/images/HeadingInvoice.jpg width="150" hieght="30"></td>
                                 </tr>
                                </table>
                                <br />';
                        }
                        else
                        {
                            $contents .= '<table  border="0" cellspacing="0" cellpadding="0" width="650px">
                                <tr style="margin-bottom:12px;">
                                <td colspan="2" valign="top" width="312px" align="left">&nbsp;</td>
                                    <td colspan="2" valign="middle" width="40%" align="right">&nbsp;</td>
                                 </tr>
                                </table>
                                <br />';
                        }
                                    $contents .= '<table  border="0" cellspacing="0" cellpadding="1" width="698px">
						<tr>
                                                <td colspan="2" valign="top" width="349px"  style="border:solid 1px #000;" border="1">
                                                <b>Invoice Date: '.$dateto.'</b></td>
                                                    <td colspan="2" valign="middle" width="349px" align="left" style="border:solid 1px #000;" border="1">
                                                    <b>Invoice Number:  '.$ref.'</b></td>
						</tr>
						</table>
						<br />						
						<table  border="0" cellspacing="0" cellpadding="0" width="650px">
						<tr> 
							<td valign="top" width="52%">
								<strong>From:</strong>
								<strong></strong>
								TRANSPORTECA_ADDRESS
							</td>
							<td valign="top" width="48%" >
								<strong>To:</strong>
								TO_TRANSPORTECA_ADD
								<br />E-mail: BILLING_ADD_EMAIL
							</td>
						</tr>
				</table>
				<br /><br />				
				<table style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" border="1" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td colspan="6" valign="top" width="700">
							<strong>Service Description:</strong>
							</td>
						</tr>
						<tr>
							<td colspan="6" valign="top" width="700">Referral fee for following bookings.<br /><br /></td>
						</tr>
						<tr>
							<td valign="top" width="15%">Settlement Date</td>
							<td valign="top" width="15%">Booking</td>
							<td valign="top" width="20%">Trade</td> 
							<td valign="top" width="15%" ALIGN="RIGHT">Booking Value</td>
							<td valign="top" width="10%" ALIGN="CENTER">Rate</td>
							<td valign="top" width="15%" ALIGN="RIGHT">Referral Fee</td>
							</tr>
							ALL_DETAILS
							<tr>
							<td colspan="5" valign="top" width="499"><strong>Invoice Total</strong></td>
							<td valign="top" width="120" ALIGN="RIGHT"><strong>INV_TOTAL</strong></td>
						</tr>
						<tr>
							<td colspan="5" valign="top" width="499"><strong>Withheld in next bank transfer</strong></td>
							<td valign="top" width="120" ALIGN="RIGHT"><strong>TOTAL_TRANSFER_AMOUNT</strong></td>
						</tr>
						<tr>
							<td colspan="5" valign="top" width="499"><strong>Payment Due</strong></td>
							<td valign="top" width="120" ALIGN="RIGHT"><strong>'.$curency.' 0.00</strong></td>
						</tr>
				</table>
					<div style="width:650px;">
						<center><span style="TEXT-ALIGN:CENTER;font-size:small;">This invoice is available in soft copy on FORWARDER_LINK_URL under Billing.</span></center><br />
						<center><span style="TEXT-ALIGN:CENTER;font-size:small;">Transporteca&rsquo;s Term &amp; Conditions are available and agreed on FORWARDER_LINK_URL.</span></center><br />
						<center><span style="TEXT-ALIGN:CENTER;font-size:small;">This is an electronic statement and no signature is required.</span></center>
					</div>
				</div>	
		</body>
		</html>'; 
          
                $fTotalInvoiceReferalFee = $array; 
	  	  $contents = str_replace("TRANSPORTECA_ADDRESS", __TRANSPORTECA_ADDRESS__, $contents);
	  	  $contents = str_replace("TO_TRANSPORTECA_ADD", $add, $contents);
	  	  $contents = str_replace("BILLING_ADD_EMAIL", $emailLog, $contents);
	  	  $contents = str_replace("ALL_DETAILS", $data, $contents);
	  	  $contents = str_replace("FORWARDER_LINK_URL", $link, $contents);
		  $contents = str_replace("INV_TOTAL", $fTotalInvoiceReferalFee, $contents);	
		  $contents = str_replace("PAYMENT_TRANSFER", $fTotalInvoiceReferalFee, $contents);
		  $contents = str_replace("TOTAL_TRANSFER_AMOUNT", $fTotalInvoiceReferalFee, $contents);	
                  
                  if($get_string_flag)
                  {
                      return $contents;
                  }else{
		 //$flag=''; 
		  if(empty($flag))
		  {
		  	echo $contents;
		  }
		  if(isset($flag) && $flag=="PDF")
		  {
		  
		  	  class transfer extends HTML2FPDFBOOKING
			  {
				  function Footer()
				  {
				  		 $this->SetY(-10);
					    //Copyright //especial para esta vers
					    $this->SetFont('Arial','B',9);
					  	$this->SetTextColor(0);
					    //Arial italic 9
					    $this->SetFont('Arial','B',9);
					    //Page number
					    $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
					    //Return Font to normal
					    $this->SetFont('Arial','',11);
				  }
			  }
			  $version="Referral-Fee-Invoice-".$batchNumber;
		  	  $filename=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";	
			  $pdf=new transfer();
			  $pdf->AddPage();
                          $pdf->Image($image_url.'/images/TransportecaMail.jpg',10,10,30,10);
                           $pdf->Image($image_url.'/images/HeadingInvoice.jpg',160,10,30,10);
			  $pdf->SetFontSize(12);
			  $pdf->SetTopMargin(5);
			  //$pdf->Rect(98,37,84,40);
			  $pdf->SetFont('Arial','',8);
			  $pdf->WriteHTML($contents);
			  
			$file_name=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";
		  	//$file = "Booking-Invoice.pdf";
			if(file_exists($file_name))
			{
				@unlink($file_name);
			}
			  
			  $pdf->Output($file_name,'F');
			 return $filename;
		  }
                  }
}

function getAllTransportecaInvoices()
{
    $version="Transporteca Invoices"; 
    $kBooking = new cBooking();  
    $flag = 'PDF';
    
    if($flag!='PDF')
    {
          $body_info='style="background:#000;text-align:center;font-size:14px;"';
          $image_url=__MAIN_SITE_HOME_PAGE_URL__;
          $dataview='<p style="text-align:right;margin:5px auto 10px;width:690px;">
                        <a target="_blank" href='.__BASE_URL__.'/downloadForwarderInvoice/'.$idForwarder.'/'.$batchNumber.'/ style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>
                        <a onclick="PrintDiv();" href="javascript:void(0)" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a>
                    </p>
                ';
          $script='<script>
                    function PrintDiv()
                    {    
                        var divToPrint = document.getElementById("forwarderTransfer");
                        var popupWin = window.open("", "_blank", "width=900,height=900");
                        popupWin.document.open();
                        popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                        popupWin.document.close();
                    }
                </script>';
    }
    else
    {
        $image_url=__APP_PATH_ROOT__;
        $dataview='';
        $script='';
    }
    
    $szFooterText = '<table  border="0" cellspacing="0" cellpadding="0" width="650px">
                    <tr>
                        <td align="right"><img src='.$image_url.'/images/Powered_By.jpg width="170"></td>
                    </tr>
                </table>';
    
    $contents = '<?xml version="1.0" encoding="iso-8859-1"?>
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
      <title>Transporteca Invoice</title>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      '.$script.'
      </head>
      <body '.$body_info.'>'.$dataview.'
                <h2 style="text-align:center;"><u>Transportecs Invoices</u></h2>  
              <div id="forwarderTransfer" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" > 
                
                <table style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" border="1" cellspacing="0" cellpadding="0"> 
                    <tr>
                        <td valign="top" width="10%">Date</td>
                        <td valign="top" width="10%">Booking</td>
                        <td valign="top" width="10%">Price(CC)</td>
                        <td valign="top" width="10%">Price(USD)</td>
                        <td valign="top" width="20%">Insurance Sell Price</td>
                        <td valign="top" width="20%">Insurance Buy Price</td>
                        <td valign="top" width="15%">GP(USD)</td>
                    </tr>  
                    <tr>
                        <td valign="top" width="10%">01/01/2015</td>
                        <td valign="top" width="10%">1501TF001</td>
                        <td valign="top" width="10%">DKK 200</td>
                        <td valign="top" width="10%">29</td>
                        <td valign="top" width="20%">DKK 300</td>
                        <td valign="top" width="20%">DKK 200</td> 
                        <td valign="top" width="15%">14</td>
                    </tr> 
                    <tr>
                        <td valign="top" width="10%">01/01/2015</td>
                        <td valign="top" width="10%">1501TF002</td>
                        <td valign="top" width="10%">DKK 200</td>
                        <td valign="top" width="10%">29</td>
                        <td valign="top" width="20%">DKK 300</td>
                        <td valign="top" width="20%">DKK 200</td> 
                        <td valign="top" width="15%">14</td>
                    </tr> 
                    <tr>
                      <td colspan="7"></td>
                    </tr>
                     <tr>
                        <td valign="top" width="10%"><strong>Total</strong></td>
                        <td valign="top" width="10%">&nbsp;</td>
                        <td valign="top" width="10%">&nbsp;</td>
                        <td valign="top" width="10%">58</td>
                        <td valign="top" width="20%">&nbsp;</td>
                        <td valign="top" width="20%">&nbsp;</td> 
                        <td valign="top" width="15%">28</td>
                    </tr> 
                </table>  
            </div> 
        </body>
      </html>'; 
           
               //$flag=''; 
    if(empty($flag))
    {
        echo $contents;
    }
    if(isset($flag) && $flag=="PDF")
    { 
          class transfer extends HTML2FPDFBOOKING
          {
                function Footer()
                {
                  $this->SetY(-10);
                  //Copyright //especial para esta vers
                  $this->SetFont('Arial','B',9);
                  $this->SetTextColor(0);
                  //Arial italic 9
                  $this->SetFont('Arial','B',9);
                  //Page number
                  $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
                  //Return Font to normal
                  $this->SetFont('Arial','',11);
                  $this->Image(__APP_PATH_ROOT__.'/images/Powered_By.jpg',150,278,50,10);
                }
          }
          $version="Referral-Fee-Invoice-".$batchNumber;
          $filename=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";	
          $pdf=new transfer();
          $pdf->AddPage();
          $pdf->SetFontSize(12);
          $pdf->SetTopMargin(5);
          //$pdf->Rect(98,37,84,40);
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML($contents);

        $file_name=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";
        //$file = "Booking-Invoice.pdf";
        if(file_exists($file_name))
        {
            @unlink($file_name);
        }

        $pdf->Output($file_name,'F');
         return $filename;
    }
} 

function getBookingHandlingFeeInvoice($idForwarder,$batchNumber,$flag='',$get_merege_pdf=false)
{
        require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php'); 
	$version="Forwarder - Transporteca Invoices".$idForwarder;
	$kForwarder= new cForwarder();
	$kBooking = new cBooking();
	$addDetail=$kForwarder->detailsForwarder($idForwarder); 
	$emailDetails=$kForwarder->detailsForwarderBillingGeneralEmail($idForwarder);
         
        $getBatchNumber = $kBooking->getBactchNumberByInvoiceNumber($batchNumber); 
       
        $invoiceBillConfirmed1 = $kBooking->invoicesBilling($idForwarder,2,'',$getBatchNumber);
 
	if($invoiceBillConfirmed1)
	{	
            $data='';
            foreach($invoiceBillConfirmed1 as $inv)
            {
                if($inv['iDebitCredit']==12)
                {  
                    $szCurrency = $inv['szCurrency'];
                      
                    if($inv['fTotalPriceForwarderCurrency']<0)
                    {
                        $array = "(".$szCurrency." ".($inv['fTotalPriceForwarderCurrency']?number_format((float)abs($inv['fTotalPriceForwarderCurrency']),2):'').")";
                    }
                    else
                    {
                        $array =$szCurrency." ".($inv['fTotalPriceForwarderCurrency']?number_format((float)$inv['fTotalPriceForwarderCurrency'],2):'');
                    } 
                    $to=$inv['dtPaymentConfirmed'];
                    $ref=$inv['szInvoice']; 
                } 
                if(strtotime($date)>strtotime($inv['dtCreatedOn']))
                {
                    $date=$inv['dtCreatedOn'];
                }
            }
        }  
        $fTotalInvoiceAmountPaid = 0;
	$invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,'',$getBatchNumber);
        
	$date=date("Y-m-d H:i:s"); 
	if($invoiceBillConfirmed)
	{	
            $data='';
            foreach($invoiceBillConfirmed as $inv)
            {	  
                if(($inv['iDebitCredit']==1 || $inv['iDebitCredit']==6) &&  $inv['fTotalHandlingFeeUSD']!='0.00')
                {  
                    if(($inv['iDebitCredit']==6) || ($inv['iDebitCredit']==1))
                    {
                        
                        $idBooking = $inv['idBooking'] ; 
                        $postSearchAry = array();
                        $postSearchAry = $kBooking->getBookingDetailsCountryName($idBooking);   
                        $kCourierServices = new cCourierServices();
                        $bookingHandlingFeeAry = $kBooking->getBookingHandlingDetails($idBooking);  
                        if($postSearchAry['iHandlingFeeApplicable']==1)
                        {
                            if(!empty($postSearchAry['szForwarderReference']))
                            {
                                $szForwarderReference = $postSearchAry['szForwarderReference'] ;
                            }
                            else
                            {
                                $szBookingRefNo=explode(":",$inv['szBooking']);
                                $szForwarderReference = $szBookingRefNo[1];
                            } 

                            $bookingLabelFee=0;    
                            $fTotalHandlingFee = $postSearchAry['fTotalHandlingFeeLocalCurrency']; 
                            $fBookingHandlingFeeRateROE = $postSearchAry['szHandlingFeeLocalCurrencyExchangeRate'];
                            $fBookingLabelFeeCurrencyId = $postSearchAry['idHandlingFeeLocalCurrency'];
                            $szHandlingFeeCurrency = $postSearchAry['szHandlingFeeLocalCurrency'];

                            if($fBookingLabelFeeCurrencyId==$inv['idForwarderCurrency'])
                            {
                                $forwarderExchangeRate='1.0000'; 
                                $fTotalHandlingFeeForwarderCurrency = $fTotalHandlingFee; 
                            }
                            else
                            { 
                                $forwarderExchangeRate1 = $postSearchAry['fForwarderExchangeRate'];
                                $forwarderExchangeRate = $postSearchAry['fForwarderExchangeRate']/$fBookingHandlingFeeRateROE; 
                                $fTotalHandlingFeeForwarderCurrency = $fTotalHandlingFee/$forwarderExchangeRate; 
                            }  
                            $data.="
                                <tr>
                                    <td valign='top'>".date('d F Y', strtotime($inv['dtPaymentConfirmed']))."</td> "
                                    . "<td valign='top'>".$szForwarderReference."</td> ";
                                    if($inv['iDebitCredit']==6)
                                        $data.="<td align='right' valign='top'>(".convertUTF($szHandlingFeeCurrency)." ".convertUTF(number_format($fTotalHandlingFee,2)).")</td>";
                                    else
                                        $data.="<td align='right' valign='top'>".convertUTF($szHandlingFeeCurrency)." ".convertUTF(number_format($fTotalHandlingFee,2))."</td>";

                                    $data.="<td align='right' valign='top'>".number_format((float)$forwarderExchangeRate,4)."</td>";
                                    if($inv['iDebitCredit']==6)
                                        $data.="<td align='right' valign='top'>(".convertUTF($inv['szForwarderCurrency'])." ".number_format($fTotalHandlingFeeForwarderCurrency,2).")</td>";
                                    else
                                        $data.="<td align='right' valign='top'>".convertUTF($inv['szForwarderCurrency'])." ".number_format($fTotalHandlingFeeForwarderCurrency,2)."</td>";
                                $data .= "</tr>";
                                $curency=$inv['szForwarderCurrency'];
                                if($inv['iDebitCredit']==6)
                                {
                                    $fTotalInvoiceAmountPaid -=  $fTotalHandlingFeeForwarderCurrency ;
                                }
                                else
                                {
                                    $fTotalInvoiceAmountPaid +=  $fTotalHandlingFeeForwarderCurrency ;
                                } 
                            }
                        }
                    }  
                    if(strtotime($date)>strtotime($inv['dtCreatedOn']))
                    {
                        $date=$inv['dtCreatedOn'];
                    } 
		} 
            }   
		$datefrom=($date!='0000-00-00 00:00:00')?date('d F Y',strtotime($date)):'';
		$timefrom=($date!='0000-00-00 00:00:00')?date('H:i',strtotime($date)):'';
		$dateto=($to!='0000-00-00 00:00:00')?date('d F Y',strtotime($to)):'';
		$timeto=($to!='0000-00-00 00:00:00')?date('H:i',strtotime($to)):'';
		$add='';
		if($addDetail['szCompanyName'])
		{
			$add.="<br />".$addDetail['szCompanyName']."<br />";
		}
		if($addDetail['szAddress'])
		{
			$add.=$addDetail['szAddress']."<br />";
		}
		if($addDetail['szAddress2'])
		{
			$add.=$addDetail['szAddress2']."<br />";
		}
		if($addDetail['szAddress3'])
		{
			$add.=$addDetail['szAddress3']."<br />";
		}
		if($addDetail['szCity'])
		{
			$add.=$addDetail['szCity'];
		}
		if($addDetail['szCity'] && $addDetail['szState'])
		{
			$add.=' , ';
		}
		if($addDetail['szState'])
		{
			$add.=$addDetail['szState'];
		}
		if($addDetail['szCountryName'])
		{
			$add.="<br />".$addDetail['szCountryName'];
		}
		if($emailDetails)
		{	
			$num=count($emailDetails);
			foreach($emailDetails as $key=>$value)
			{
				$emailLog.=$value['szGeneralEmailAddress'];
				$num--;
				if($num>1)
				{
					$emailLog.= ", ";
				}
				else if($num==1)
				{
					$emailLog.= ' and ';
				}
			}
		}
  		$link=$addDetail['szControlPanelUrl'];
  
	
  //$pdf->Image('invoice.jpg',120,12,0);
    if(!empty($array))
    {
        //$array=explode(',',$array);
    } 
                
                //echo $data;
                //die;
  if($flag!='PDF')
  {
  	$body_info='style="background:#000;text-align:center;font-size:14px;"';
  	$image_url=__MAIN_SITE_HOME_PAGE_URL__;
  	$dataview='<p style="text-align:right;margin:5px auto 10px;width:690px;">
			<a target="_blank" href='.__BASE_URL__.'/downloadForwarderInvoice/'.$idForwarder.'/'.$batchNumber.'/ style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>
			<a onclick="PrintDiv();" href="javascript:void(0)" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a>
			</p>';
  	$script='<script>
			function PrintDiv()
			{    
                            var divToPrint = document.getElementById("forwarderTransfer");
                            var popupWin = window.open("", "_blank", "width=900,height=900");
                            popupWin.document.open();
                            popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                            popupWin.document.close();
			}
			</script>';
  }
  else
  {
  	$image_url=__APP_PATH_ROOT__;
  	$dataview='';
  	$script='';
  }
  	  $contents = '<?xml version="1.0" encoding="iso-8859-1"?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>Transporteca Invoice</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		'.$script.'
		</head>
		<body '.$body_info.'>'.$dataview.'
                    <div id="forwarderTransfer" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" >';
                        if($get_merege_pdf)
                        {
                            $contents .= '<table  border="0" cellspacing="0" cellpadding="0" width="650px">
                                <tr style="margin-bottom:12px;">
                                    <td colspan="2" valign="top" width="312px" align="left">
                                            <img src='.$image_url.'/images/TransportecaMail.jpg height="53px" width="217px">
                                    </td> 
                                    <td colspan="2" valign="middle" width="40%" align="right">
                                        <img src='.$image_url.'/images/HeadingInvoice.jpg width="150" hieght="30"> 
                                    </td>	
                                </tr>
                            </table>
                            <br />';
                        }
                        else
                        {
                            $contents .= '<table  border="0" cellspacing="0" cellpadding="0" width="650px">
                                <tr style="margin-bottom:12px;">
                                    <td colspan="2" valign="top" width="312px" align="left">&nbsp;</td> 
                                    <td colspan="2" valign="middle" width="40%" align="right">&nbsp;</td>	
                                </tr>
                            </table>
                            <br />';
                        }
                        $contents .= '<table  border="0" cellspacing="0" cellpadding="1" width="650px">
                            <tr>
                                <td colspan="2" valign="top" width="52%" align="left" style="border:solid 1px #000;" border="1">
                                    <b>Invoice Date: '.$dateto.'</b>
                                </td>
                                <td colspan="2" valign="middle" width="48%" align="left" style="border:solid 1px #000;" border="1">
                                    <b>Invoice Number:  '.$ref.'</b>
                                </td>
                            </tr>
                        </table>
                        <br />						
                        <table  border="0" cellspacing="0" cellpadding="0" width="650px">
                            <tr> 
                                <td valign="top" width="52%">
                                    <strong>From:</strong>
                                    <strong></strong>
                                    TRANSPORTECA_ADDRESS
                                </td>
                                <td valign="top" width="48%" >
                                    <strong>To:</strong>
                                    TO_TRANSPORTECA_ADD
                                    <br />E-mail: BILLING_ADD_EMAIL
                                </td>
                            </tr>
                        </table>
                        <br /><br />				
                        <table width="650px" border="1" cellspacing="0" cellpadding="3" align="left">
                                <tbody>
                                    <tr>
                                        <td colspan="5" valign="top" width="100%"> <strong>Service Description:</strong> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" valign="top" width="100%">Handling fee for following bookings.<br /><br /></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" width="20%">Settlement Date</td>
                                        <td valign="top" width="20%">Booking</td> 
                                        <td valign="top" width="20%" ALIGN="RIGHT">Rate</td>
                                        <td valign="top" width="20%" ALIGN="RIGHT">ROE</td>
                                        <td valign="top" width="20%" ALIGN="RIGHT">Handling Fee</td>
                                    </tr>
                                        ALL_DETAILS
                                    <tr>
                                        <td colspan="4" valign="top" width="499px"><strong>Invoice Total</strong></td>
                                        <td valign="top" width="120px" ALIGN="RIGHT"><strong>INV_TOTAL</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" valign="top" width="499px"><strong>Withheld in next bank transfer</strong></td>
                                        <td valign="top" width="120px" ALIGN="RIGHT"><strong>TOTAL_TRANSFER_AMOUNT</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" valign="top" width="499px"><strong>Payment Due</strong></td>
                                        <td valign="top" width="120px" ALIGN="RIGHT"><strong>'.$curency.' 0.00</strong></td>
                                    </tr>
                                </tbody>
                        </table>
                        <div style="width:650px;">
                            <center><span style="TEXT-ALIGN:CENTER;font-size:small;">This invoice is available in soft copy on FORWARDER_LINK_URL under Billing.</span></center><br />
                            <center><span style="TEXT-ALIGN:CENTER;font-size:small;">Transporteca&rsquo;s Term &amp; Conditions are available and agreed on FORWARDER_LINK_URL.</span></center><br />
                            <center><span style="TEXT-ALIGN:CENTER;font-size:small;">This is an electronic statement and no signature is required.</span></center>
                        </div>
                    </div>	
                </body>
        </html>';  
                $fTotalInvoiceReferalFee = $curency." ".number_format((float)$fTotalInvoiceAmountPaid,2);  
                $contents = str_replace("TRANSPORTECA_ADDRESS", __TRANSPORTECA_ADDRESS__, $contents);
                $contents = str_replace("TO_TRANSPORTECA_ADD", $add, $contents);
                $contents = str_replace("BILLING_ADD_EMAIL", $emailLog, $contents);
                $contents = str_replace("ALL_DETAILS", $data, $contents);
                $contents = str_replace("FORWARDER_LINK_URL", $link, $contents);
                $contents = str_replace("INV_TOTAL", $fTotalInvoiceReferalFee, $contents);	
                $contents = str_replace("PAYMENT_TRANSFER", $fTotalInvoiceReferalFee, $contents);
                $contents = str_replace("TOTAL_TRANSFER_AMOUNT", $fTotalInvoiceReferalFee, $contents);	
		   
                
                
                if($get_merege_pdf)
                {
                    return $contents;
                }
                else
                {    
		  if(empty($flag))
		  {
		  	echo $contents;
		  }
		  if(isset($flag) && $flag=="PDF")
		  {
		  
		  	  class transfer1 extends HTML2FPDFBOOKING
			  {
				  function Footer()
				  {
				  		 $this->SetY(-10);
					    //Copyright //especial para esta vers
					    $this->SetFont('Arial','B',9);
					  	$this->SetTextColor(0);
					    //Arial italic 9
					    $this->SetFont('Arial','B',9);
					    //Page number
					    $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
					    //Return Font to normal
					    $this->SetFont('Arial','',11);
				  }
			  }
			  $version="Handling-fee-invoice-".$batchNumber;
		  	  $filename=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";	
			  $pdf=new transfer1();
			  $pdf->AddPage();
                          $pdf->Image($image_url.'/images/TransportecaMail.jpg',10,10,30,10);
                          $pdf->Image($image_url.'/images/HeadingInvoice.jpg',170,10,30,10);
			  $pdf->SetFontSize(12);
			  $pdf->SetTopMargin(5);
			  //$pdf->Rect(98,37,84,40);
			  $pdf->SetFont('Arial','',8);
			  $pdf->WriteHTML($contents);
			  
			$file_name=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";
		  	//$file = "Booking-Invoice.pdf";
			if(file_exists($file_name))
			{
				@unlink($file_name);
			}
			  
			  $pdf->Output($file_name,'F');
			 return $filename;
		  }
                }
}

function getAllTransportecaHandlingFeeInvoices()
{
    $version="Transporteca Invoices"; 
    $kBooking = new cBooking();  
    $flag = 'PDF';
    
    if($flag!='PDF')
    {
          $body_info='style="background:#000;text-align:center;font-size:14px;"';
          $image_url=__MAIN_SITE_HOME_PAGE_URL__;
          $dataview='<p style="text-align:right;margin:5px auto 10px;width:690px;">
                        <a target="_blank" href='.__BASE_URL__.'/downloadForwarderInvoice/'.$idForwarder.'/'.$batchNumber.'/ style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>
                        <a onclick="PrintDiv();" href="javascript:void(0)" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a>
                    </p>
                ';
          $script='<script>
                    function PrintDiv()
                    {    
                        var divToPrint = document.getElementById("forwarderTransfer");
                        var popupWin = window.open("", "_blank", "width=900,height=900");
                        popupWin.document.open();
                        popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                        popupWin.document.close();
                    }
                </script>';
    }
    else
    {
        $image_url=__APP_PATH_ROOT__;
        $dataview='';
        $script='';
    }
    
    $szFooterText = '<table  border="0" cellspacing="0" cellpadding="0" width="650px">
                    <tr>
                        <td align="right"><img src='.$image_url.'/images/Powered_By.jpg width="170"></td>
                    </tr>
                </table>';
    
    $contents = '<?xml version="1.0" encoding="iso-8859-1"?>
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
      <title>Transporteca Invoice</title>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      '.$script.'
      </head>
      <body '.$body_info.'>'.$dataview.'
                <h2 style="text-align:center;"><u>Transportecs Invoices</u></h2>  
              <div id="forwarderTransfer" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" > 
                
                <table style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" border="1" cellspacing="0" cellpadding="0"> 
                    <tr>
                        <td valign="top" width="10%">Date</td>
                        <td valign="top" width="10%">Booking</td>
                        <td valign="top" width="10%">Price(CC)</td>
                        <td valign="top" width="10%">Price(USD)</td>
                        <td valign="top" width="20%">Insurance Sell Price</td>
                        <td valign="top" width="20%">Insurance Buy Price</td>
                        <td valign="top" width="15%">GP(USD)</td>
                    </tr>  
                    <tr>
                        <td valign="top" width="10%">01/01/2015</td>
                        <td valign="top" width="10%">1501TF001</td>
                        <td valign="top" width="10%">DKK 200</td>
                        <td valign="top" width="10%">29</td>
                        <td valign="top" width="20%">DKK 300</td>
                        <td valign="top" width="20%">DKK 200</td> 
                        <td valign="top" width="15%">14</td>
                    </tr> 
                    <tr>
                        <td valign="top" width="10%">01/01/2015</td>
                        <td valign="top" width="10%">1501TF002</td>
                        <td valign="top" width="10%">DKK 200</td>
                        <td valign="top" width="10%">29</td>
                        <td valign="top" width="20%">DKK 300</td>
                        <td valign="top" width="20%">DKK 200</td> 
                        <td valign="top" width="15%">14</td>
                    </tr> 
                    <tr>
                      <td colspan="7"></td>
                    </tr>
                     <tr>
                        <td valign="top" width="10%"><strong>Total</strong></td>
                        <td valign="top" width="10%">&nbsp;</td>
                        <td valign="top" width="10%">&nbsp;</td>
                        <td valign="top" width="10%">58</td>
                        <td valign="top" width="20%">&nbsp;</td>
                        <td valign="top" width="20%">&nbsp;</td> 
                        <td valign="top" width="15%">28</td>
                    </tr> 
                </table>  
            </div> 
        </body>
      </html>'; 
           
               //$flag=''; 
    if(empty($flag))
    {
        echo $contents;
    }
    if(isset($flag) && $flag=="PDF")
    { 
          class transfer extends HTML2FPDFBOOKING
          {
                function Footer()
                {
                  $this->SetY(-10);
                  //Copyright //especial para esta vers
                  $this->SetFont('Arial','B',9);
                  $this->SetTextColor(0);
                  //Arial italic 9
                  $this->SetFont('Arial','B',9);
                  //Page number
                  $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
                  //Return Font to normal
                  $this->SetFont('Arial','',11);
                  $this->Image(__APP_PATH_ROOT__.'/images/Powered_By.jpg',150,278,50,10);
                }
          }
          $version="Referral-Fee-Invoice-".$batchNumber;
          $filename=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";	
          $pdf=new transfer();
          $pdf->AddPage();
          $pdf->SetFontSize(12);
          $pdf->SetTopMargin(5);
          //$pdf->Rect(98,37,84,40);
          $pdf->SetFont('Arial','',8);
          $pdf->WriteHTML($contents);

        $file_name=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";
        //$file = "Booking-Invoice.pdf";
        if(file_exists($file_name))
        {
            @unlink($file_name);
        }

        $pdf->Output($file_name,'F');
        return $filename;
    }
}   