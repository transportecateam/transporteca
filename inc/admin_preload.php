<?php
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$file_name='';
function __autoload($class_name)
{
    switch ($class_name)
    {
        case "cConfig":
            $file_name = __APP_PATH_CLASSES__."/config.class.php";
            break; 
        case "cDatabase":
            $file_name = __APP_PATH_CLASSES__."/database.class.php";
            break;
        case "CError":
            $file_name = __APP_PATH_CLASSES__."/error.class.php";
            break;
        case "Horde_Yaml":
            $file_name = __APP_PATH__."/lib/Horde/Yaml.php";
            break;
		case "Horde_Yaml_Loader":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Loader.php";
			break;
		case "Horde_Yaml_Node":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Node.php";
            break;
		case "Horde_Yaml_Dumper":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Dumper.php";
            break; 
		case "Horde_Yaml_Exception":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Exception.php";
            break;	
       case "cAdmin":
            $file_name =  __APP_PATH_CLASSES__.'/admin.class.php';
            break;  
       case "cForwarder":
            $file_name =  __APP_PATH_CLASSES__.'/forwarder.class.php';
            break;  
       case "cWHSSearch":
            $file_name =  __APP_PATH_CLASSES__.'/warehouseSearch.class.php';
            break;       
       case "cUser":
            $file_name =  __APP_PATH_CLASSES__."/user.class.php";
            break;   
       case "cRegisterShipCon":
        	$file_name =  __APP_PATH_CLASSES__."/registeredShippersConsignees.class.php";
            break;
	   case "cBooking":
            $file_name =  __APP_PATH_CLASSES__."/booking.class.php";
            break;
       case "cForwarder":
            $file_name =  __APP_PATH_CLASSES__."/forwarder.class.php";
            break;
       case "HTML2FPDF":
            $file_name =  __APP_PATH__.'/forwarders/html2pdf/html2fpdf.php';
            break;
       case "FPDF":
            $file_name =  __APP_PATH__.'/invoice/fpdf.php';
            break;
       case "cExport_Import":
            $file_name =  __APP_PATH_CLASSES__.'/exportimport.class.php';
            break;
       case "cPayPal":
            $file_name =  __APP_PATH_CLASSES__.'/paypal.class.php';
            break;
       case "cZooz":
            $file_name =  __APP_PATH_CLASSES__.'/zooz.class.php';
            break;
       case "FPDFHTML":
       		 $file_name =  __APP_PATH__.'/forwarders/html2pdf/fpdf.php';
       case "cForwarderContact":
            $file_name =  __APP_PATH_CLASSES__.'/forwarderContact.class.php';
            break;      
 	   case "cUploadBulkService":
            $file_name =  __APP_PATH_CLASSES__.'/uploadBulkService.class.php';
            break;
       case "cHaulagePricing":
            $file_name =  __APP_PATH_CLASSES__.'/haulagePricing.class.php';
            break; 
      case "cExplain":
            $file_name =  __APP_PATH_CLASSES__.'/explain.class.php';
            break;
      case "cBilling":
            $file_name =  __APP_PATH_CLASSES__.'/billing.class.php';
            break;  
	  case "cDashboardAdmin":
            $file_name =  __APP_PATH_CLASSES__.'/dashboardAdmin.class.php';
            break;
      case "cNPS":
            $file_name =  __APP_PATH_CLASSES__.'/nps.class.php';
            break;          
      case "cReport":
          	$file_name =  __APP_PATH_CLASSES__.'/report.class.php';
           	break;
      case "cSEO":
        	$file_name =  __APP_PATH_CLASSES__.'/seo.class.php';
           	break;     
      case "cRSS":
         	$file_name =  __APP_PATH_CLASSES__.'/rss.class.php';
           	break;
    }
   // echo $file_name;
    include($file_name);
}

$successful_connection = false;

function on_session_start($save_path, $session_name)
{
    global $dbh_sess, $successful_connection;

    if ($dbh_sess = mysql_connect(__DBC_HOST__, __DBC_USER__,__DBC_PASSWD__))
    {
        $successful_connection = true;
        mysql_set_charset("utf8",$dbh_sess);
        return mysql_select_db(__DBC_SCHEMATA__, $dbh_sess) or die(mysql_error_custom());
    }

    return false;
}

function on_session_end()
{
    global $dbh_sess;
    return mysql_close($dbh_sess);
}

function on_session_read($key)
{
    global $dbh_sess;

    $query = "
      SELECT
        session_data
      FROM
        ".__DBC_SCHEMATA_ADMIN_SESSIONS__."
      WHERE
        session_id = '".mysql_escape_custom($key)."'
      AND
        session_expiration > NOW()
      LIMIT
         1
    ";
//echo "user 121 read".__DBC_USER__ ;
    if ($result = mysql_query($query, $dbh_sess) or die(mysql_error_custom()))
    {
        if (mysql_num_rows($result))
        {
            $record = mysql_fetch_assoc($result);
            return $record['session_data'];
        }
    }
    return '';
}

function on_session_write($key, $val)
{
    global $dbh_sess;
    $dbh_sess = mysql_connect(__DBC_HOST__, __DBC_USER__,__DBC_PASSWD__);

    $query = "
        INSERT INTO
            ".__DBC_SCHEMATA_ADMIN_SESSIONS__."
            (
                session_id,
                session_data,
                session_expiration,
                idUser
            )
        VALUES
            (
                '".mysql_escape_custom($key)."',
                '".mysql_escape_custom($val)."',
                '".date('Y-m-d H:i:s', strtotime(__SESSION_LIFETIME__))."',
                ".(int)$_SESSION['user_id']."
            )
        ON DUPLICATE KEY UPDATE
            session_data = '".mysql_escape_custom($val)."',
            session_expiration = '".date('Y-m-d H:i:s', strtotime(__SESSION_LIFETIME__))."',
            idUser = ".(int)$_SESSION['user_id']."
    ";

    return mysql_query($query, $dbh_sess) or die(mysql_error_custom());
}

function on_session_destroy($key)
{
    global $dbh_sess;

    $query = "
        DELETE FROM
            ".__DBC_SCHEMATA_ADMIN_SESSIONS__."
        WHERE
            session_id = '".mysql_escape_custom($key)."'
    ";

	return mysql_query($query, $dbh_sess) or die(mysql_error_custom());
}

function on_session_gc($max_lifetime)
{
    global $dbh_sess;

    $query = "
        DELETE FROM
            ".__DBC_SCHEMATA_ADMIN_SESSIONS__."
        WHERE
            session_expiration < NOW()
    ";

	return mysql_query($query, $dbh_sess);
}

function strip_tags_array($data, $tags = null)
{
    $stripped_data = array();
    foreach ($data as $key => $value)
    {
        if (is_array($value))
        {
            $stripped_data[$key] = strip_tags_array($value, $tags);
        } 
        else 
        {
            while (($tmpval = url_decode_custom($value)) !== false)
            {
                $value = $tmpval;
            }
            while (($tmpval = html_decode_custom($value)) !== false)
            {
                $tmpval2 = $tmpval;
                while (($tmpval3 = url_decode_custom($tmpval2)) !== false)
                {
                    $tmpval2 = $tmpval3;
                }
                $value = $tmpval2;
            }
            $stripped_data[$key] = strip_tags($value, $tags);
        }
    }
    return $stripped_data;
}

function url_decode_custom($value)
{
    $url_decoded = urldecode($value);
    if ($url_decoded != $value)
    {
        return $url_decoded;
    }
    return false;
}

function html_decode_custom($value)
{
    $html_decoded = html_entity_decode($value);
    if ($html_decoded != $value)
    {
        return $html_decoded;
    }
    return false;
}

/*
 * use the below to debug the strip tags implementation.
$_GET['test']['yes']['owen'] = array('owen' => 'yes');

echo "<pre>";
print_r($_GET);
$_GET = strip_tags_array($_GET);
print_r($_GET);
echo "\n-----------------------------\n";
print_r($_POST);
$_POST = strip_tags_array($_POST);
print_r($_POST);
echo "\n-----------------------------\n";
print_r($_COOKIE);
$_COOKIE = strip_tags_array($_COOKIE);
print_r($_COOKIE);
echo "\n-----------------------------\n";
print_r($_REQUEST);
$_REQUEST = strip_tags_array($_REQUEST);
print_r($_REQUEST);
die();
*/

$_GET = strip_tags_array($_GET);
//$_POST = strip_tags_array($_POST);
$_COOKIE = strip_tags_array($_COOKIE);
$_REQUEST = strip_tags_array($_REQUEST);

// Set the save handlers
session_set_save_handler("on_session_start", "on_session_end", "on_session_read", "on_session_write", "on_session_destroy", "on_session_gc");
session_start();

if ($successful_connection === false)
{
    if ($dbh_sess = mysql_connect(__DBC_HOST__, __DBC_USER__,__DBC_PASSWD__))
    {
        return mysql_select_db(__DBC_SCHEMATA__, $dbh_sess) or die(mysql_error_custom());
    }
}

?>
