<?php
if( !isset( $_SESSION ) )
{
    session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Ajay
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once(__APP_PATH__ . "/inc/constants.php");
require_once(__APP_PATH__ . "/inc/functions.php"); 
require_once(__APP_PATH__ . "/inc/pendingTray_functions.php"); 
require_once(__APP_PATH_CLASSES__ . "/database.class.php");
include_classes();

/*
* This function displays main container for Management > Pending Tray > Customer Logs
* @author Ajay
*/
function display_task_customer_logs_container($kBooking,$iPageNumber=1)
{
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    
    $t_base="management/pendingTray/";    
    $formId = 'pending_task_tray_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }  
    $kCustomerLogs = new cCustomerLogs(); 
    $kChat = new cChat();
    ?> 
    <div class="clearfix">  
        <div id="pending_tray_customer_log_main_container">
            <?php echo display_customer_logs_details($kBooking,$iPageNumber); ?>
        </div> 
        <div class="clearfix email-fields-container" style="width:100%;" id="show_button_msg"> 
            <a href="javascript:void(0);" id="pending_task_show_more_files_customer_log" onclick="loadMoreCustomerLogs('<?php echo $idBookingFile; ?>','0','PENDING_TRAY_CUSTOMER_LOG');" class="button1">Show more</a> 
            <a href="javascript:void(0);" id="pending_task_show_more_files_historical" onclick="loadMoreCustomerLogs('<?php echo $idBookingFile; ?>','1','PENDING_TRAY_CUSTOMER_LOG');" class="button1" style='display:none;'>Show more</a> 
            <input type="hidden" name="iPageCounter" id="iPageCounter" value="1">
            <input type="hidden" name="iPageCounterHistorical" id="iPageCounterHistorical" value="1">
            <input type="hidden" name="iButtonAlreadyClicked" id="iButtonAlreadyClicked" value="0">
        </div> 
    </div>
    <?php
}
/*
* This function displays data for Management > Pending Tray > Customer Logs
* @author Ajay
*/
function display_customer_logs_details($kBooking,$iPageNumber=1)
{ 
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idOfflineChat = $kBooking->idOfflineChat; 
    
    $kBooking_new = new cBooking(); 
    $kChat = new cChat();
    $postSearchAry = $kBooking_new->getBookingDetails($idBooking);
    $idCustomer = $postSearchAry['idUser'];
    $szCustomerEmail = $postSearchAry['szEmail'];
    
    $kCustomerLogs = new cCustomerLogs();
    $logEmailsAry = array();
    $logEmailsAry = $kCustomerLogs->getAllDataForCustomerLogs($idBooking,$szCustomerEmail,$idCustomer,$iPageNumber); 
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');
    if(!empty($logEmailsAry))
    {
        echo display_customer_logs_rows($logEmailsAry,$kBooking,'PENDING_TRAY_CUSTOMER_LOG'); 
    }
    else
    { 
        ?> 
        <script type='text/javascript'>
            $("#pending_task_show_more_files_customer_log").attr('style','display:none;');
            $("#pending_task_show_more_files_historical").attr('style','display:inline-block;');
        </script>
        <?php
        echo display_historical_data_for_customer_logs($kBooking,1);
    } 
}

/*
* This function displays the individual rows for Management > Pending Tray > Customer Logs and Management > Pending Tray > File Logs pane.
* @author Ajay
*/

function display_customer_logs_rows($logEmailsAry,$kBooking,$szFromPage)
{
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idOfflineChat = $kBooking->idOfflineChat; 
    
    if(!empty($logEmailsAry))
    {
        $ctr = 0;
        foreach($logEmailsAry as $logEmailsArys)
        { 
            $ctr++;
            //echo "<br> Ctr: ".((25*($iPageNumber-1))+$ctr)." Type: ".$logEmailsArys['idCustomerLogDataType'];
            /*if(date('I')==1)
            {  
                //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
                $logEmailsArys['dtSentOriginal'] = $logEmailsArys['dtSent'];
                $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtSent'])));  
            }
            else
            {
                $logEmailsArys['dtSentOriginal'] = $logEmailsArys['dtSent'];
                $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtSent'])));  
            }*/
            //echo $logEmailsArys['dtSent']."dtSent";
            if($logEmailsArys['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_FILE_SEARCH_LOGS__)
            {  
                /*
                * This blocks will customer search history logs. e.g. If user has made search on www.transporteca.com from Mumbai, India to Copenhagen Denmark
                */ 
                echo display_booking_searched_logs($logEmailsArys);
            }
            else if($logEmailsArys['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_ZOPIM_CHAT_LOGS__)
            { 
                /*
                * This blocks will display all the chats what selected customer has with Transporteca support via Zopim.
                */ 
                echo display_zopim_chat_details($logEmailsArys,$idBookingFile,$idOfflineChat);
            }
            else if($logEmailsArys['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_OVERVIEW_TASK__)
            {
                /*
                * This blocks will display all the Task that have been added from 'New Task' link from Overview section.
                */ 
                echo display_overview_task_details($logEmailsArys,$kBooking,$szFromPage);
            }
            else if($logEmailsArys['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_REMINDER_NOTES__)
            { 
                /*
                * This blocks will display all the Reminder Notes that have been added from 'REMIND' pane.
                */ 
                echo display_reminder_log($logEmailsArys);
            } 
            else if($logEmailsArys['idCustomerLogDataType']==__CUSTOMER_LOG_SNIPPET_TYPE_EMAIL__) 
            {  
                if($logEmailsArys['iEmailType']==__CUSTOMER_LOG_EMAIL_TYPE_INCOMING__)
                {  
                    /*
                    * This blocks will display all the Incoming email to Transporteca i.e all the emails what have been imported from Gmail Inbox.
                    */
                    echo display_crm_message_details($logEmailsArys,$kBooking,$szFromPage);
                }  
                else 
                {
                    /*
                    * This blocks will display all the Outgoing email from Transporteca i.e all the system emails what have been sent to either Customer, Forwarder or Management.
                    */
                    echo display_crm_outgoing_emails($logEmailsArys,$kBooking,$szFromPage);
                } 
            }
        } 
    }
}
  
/*
* This function displays the data before six months for Management > Pending Tray > Customer Logs.
* @author Ajay
*/

function display_historical_data_for_customer_logs($kBooking,$iPageNumber=1)
{
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idOfflineChat = $kBooking->idOfflineChat;
    
    $kBooking_new = new cBooking(); 
    $kChat = new cChat();
    $postSearchAry = $kBooking_new->getBookingDetails($idBooking);
    $idCustomer = $postSearchAry['idUser'];
            
    //$kCrm = new cCRM();
    //$kCrm->updateCrmSnoozeFlag();
    $logEmailsAry = array();
    $logEmailsAry = $kBooking_new->getAllFileEmailLogs($idBooking,$postSearchAry['szEmail'],false,false,$idCustomer,true);
             
    if($postSearchAry['idUser']>0)
    {
        $logReminderAry = array();
        $logReminderAry = $kBooking_new->getAllFileReminderLogs($idBooking,$idBookingFile,true,$postSearchAry['idUser'],false,true);
    }
            
    if(!empty($logReminderAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logReminderAry);
    }
    
    $logFileSearchAry = array();
    $logFileSearchAry = $kBooking_new->getAllFileSearchLogs(false,$idCustomer,false,false,false,true);
     
    if(!empty($logFileSearchAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logFileSearchAry);
    }
    
    $zopimChatLogsAry = array();
    $zopimChatLogsAry = $kChat->getAllZopimChatLogs($idCustomer,false,true);
    
    if(!empty($zopimChatLogsAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$zopimChatLogsAry);
    } 
    $logEmailsAryNew=array();
    if(!empty($logEmailsAry))
    {
        $ctr=0;
        foreach($logEmailsAry as $logEmailsArys)
        {
            $logEmailsAryNew[$ctr] = $logEmailsArys;  
            if($logEmailsArys['iDSTDifference']>0)
            {  
                $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                $logEmailsAryNew[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+".(int)$logEmailsArys['iDSTDifference']." HOUR",strtotime($logEmailsArys['dtSent'])));  
            }
            else
            {
                $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                $logEmailsAryNew[$ctr]['dtSent'] = getDateCustom($logEmailsAryNew[$ctr]['dtSent']);
                /*
                if(date('I')==1)
                {  
                    //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
                    $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                    $logEmailsAryNew[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtSent'])));  
                }
                else
                {
                    $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                    $logEmailsAryNew[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtSent'])));  
                }
                 * 
                 */
            }
            ++$ctr;
            
        }
    }    
    //$iTotalNumEmails = $kBooking_new->getAllFileEmailLogs($idBooking,$postSearchAry['szEmail'],false,true); 
    $logEmailsAry = sortArray($logEmailsAryNew,'dtSent','DESC');  
    $iTotalNumEmails = count($logEmailsAry);
    
    $chunkedEmailAry = array_chunk($logEmailsAry, __CONTENT_PER_PAGE_CUSTOMER_LOG__);
    $logEmailsAry = $chunkedEmailAry[$iPageNumber-1]; 
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');   
    
    if(!empty($logEmailsAry))
    {
        echo display_customer_logs_rows($logEmailsAry,$kBooking,'PENDING_TRAY_CUSTOMER_LOG');
    }
    else
    {
        ?>
        <script type='text/javascript'> 
            $("#pending_task_show_more_files_historical").attr('style','opacity:0.4');
            $("#pending_task_show_more_files_historical").removeAttr('onclick');
            $("#show_button_msg").html('No more messages');
            $("#show_button_msg").attr('style','text-align:center;font-weight:bold;');
        </script>
        <?php
    }
}
 
/*
* This function displays the main container for Management > Pending Tray > File Logs.
* @author Ajay
*/
function display_task_logs_container($kBooking)
{
    $t_base="management/pendingTray/";    
    $formId = 'pending_task_tray_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    } 
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idEmailLogs = $kBooking->idEmailLogs; 
    $iPageNumber = 1;
    ?> 
    <div class="clearfix">  
        <div id="pending_tray_customer_log_main_container">
            <?php echo display_file_logs_details($kBooking,$iPageNumber); ?>
        </div> 
        <div class="clearfix email-fields-container" style="width:100%;" id="show_button_msg"> 
            <a href="javascript:void(0);" id="pending_task_show_more_files_customer_log" onclick="loadMoreCustomerLogs('<?php echo $idBookingFile; ?>','0','PENDING_TRAY_LOG');" class="button1">Show more</a> 
            <a href="javascript:void(0);" id="pending_task_show_more_files_historical" onclick="loadMoreCustomerLogs('<?php echo $idBookingFile; ?>','1','PENDING_TRAY_LOG');" class="button1" style='display:none;'>Show more</a> 
            <input type="hidden" name="iPageCounter" id="iPageCounter" value="1">
            <input type="hidden" name="iPageCounterHistorical" id="iPageCounterHistorical" value="1">
            <input type="hidden" name="iButtonAlreadyClicked" id="iButtonAlreadyClicked" value="0">
        </div> 
    </div>
    <?php
} 

/*
* This function displays the data for Management > Pending Tray > File Logs.
* @author Ajay
*/
function display_file_logs_details($kBooking,$iPageNumber)
{
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idOfflineChat = $kBooking->idOfflineChat; 
    
    $kBooking_new = new cBooking(); 
    $kChat = new cChat();
    $postSearchAry = $kBooking_new->getBookingDetails($idBooking);
    $idCustomer = $postSearchAry['idUser'];
    $szCustomerEmail = $postSearchAry['szEmail'];
    
    $kCustomerLogs = new cCustomerLogs();
    $logEmailsAry = array();
    $logEmailsAry = $kCustomerLogs->getAllDataForCustomerLogs($idBooking,false,false,$iPageNumber); 
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');
    if(!empty($logEmailsAry))
    {
        echo display_customer_logs_rows($logEmailsAry,$kBooking,'PENDING_TRAY_LOG'); 
    }
    else
    { 
        ?> 
        <script type='text/javascript'>
            $("#pending_task_show_more_files_customer_log").attr('style','display:none;');
            $("#pending_task_show_more_files_historical").attr('style','display:inline-block;');
        </script>
        <?php
        echo display_historical_data_for_file_logs($kBooking,1);
    } 
}

function display_historical_data_for_file_logs($kBooking,$iPageNumber)
{  
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idEmailLogs = $kBooking->idEmailLogs;
    
    $kBooking_new = new cBooking(); 
    $postSearchAry = $kBooking_new->getBookingDetails($idBooking);
            
    $logEmailsAry = array();
    $logEmailsAry = $kBooking_new->getAllFileEmailLogs($idBooking,false,false,false,false,true);
    
    $logReminderAry = array();
    $logReminderAry = $kBooking_new->getAllFileReminderLogs($idBooking,false,false,false,false,true);
    
    if(!empty($logReminderAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logReminderAry);
    }
    
    $logFileSearchAry = array();
    $logFileSearchAry = $kBooking_new->getAllFileSearchLogs($idBookingFile,false,false,false,false,true);
           
    if(!empty($logFileSearchAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logFileSearchAry);
    }
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');
    
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');  
    $iTotalNumEmails = count($logEmailsAry);
    
    $logEmailsAryNew=array();
    if(!empty($logEmailsAry))
    {
        $ctr=0;
        foreach($logEmailsAry as $logEmailsArys)
        {
            $logEmailsAryNew[$ctr] = $logEmailsArys;  
            if($logEmailsArys['iDSTDifference']>0)
            {  
                $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                $logEmailsAryNew[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+".(int)$logEmailsArys['iDSTDifference']." HOUR",strtotime($logEmailsArys['dtSent'])));  
            }
            else
            {
                $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                $logEmailsAryNew[$ctr]['dtSent'] = getDateCustom($logEmailsAryNew[$ctr]['dtSent']);
                /*
                if(date('I')==1)
                {  
                    //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
                    $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                    $logEmailsAryNew[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtSent'])));  
                }
                else
                {
                    $logEmailsAryNew[$ctr]['dtSentOriginal'] = $logEmailsArys['dtSent'];
                    $logEmailsAryNew[$ctr]['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtSent'])));  
                }
                 * 
                 */
            }
            ++$ctr; 
        }
    }
    $logEmailsAry = sortArray($logEmailsAryNew,'dtSent','DESC'); 
    
    $chunkedEmailAry = array_chunk($logEmailsAry, __CONTENT_PER_PAGE_CUSTOMER_LOG__);
    $logEmailsAry = $chunkedEmailAry[$iPageNumber-1]; 
    
      
    
    if(!empty($logEmailsAry))
    {
        echo display_customer_logs_rows($logEmailsAry,$kBooking,'PENDING_TRAY_LOG');
    }
    else
    {
        ?>
        <script type='text/javascript'>  
            
            $("#pending_task_show_more_files_historical").removeAttr('onclick');
            $("#pending_task_show_more_files_historical").attr('style','opacity:0.4'); 
            $("#show_button_msg").html('No more messages');
            $("#show_button_msg").attr('style','text-align:center;font-weight:bold;');
        </script>
        <?php
    } 
}

function updateCargoDetailsPendingTray($cargoDetailArr=array(),$kBooking='',$iAlreadyPaidBooking=0)
{
    $t_base="management/pendingTray/"; 
    $heading=t($t_base.'title/update_cargo_details');
    $i=0;
    $kWHSSearch=new cWHSSearch();
    $fCmFactor = $kWHSSearch->getCargoFactor(2);
    $hideFlag=true;
    /*if($_POST['iSearchMiniVersion']==8)
    {
        $hideFlag=true;
    }*/
    ?>
   <script type="text/javascript">
        $().ready(function() {	
             enableSaveButtonForAddingCargoLine(); 
             update_total_cargo_details_text();
        });
        
        function calculatePerColliVolume(iNumber)
        {
            var length=$("#iLength"+iNumber).attr('value');
            var width=$("#iWidth"+iNumber).attr('value');
            var height=$("#iHeight"+iNumber).attr('value');
            var idCargoMeasure=$("#idCargoMeasure"+iNumber).attr('value');

            if(parseFloat(length)>0 && parseFloat(width)>0 && parseFloat(height)>0)
            {
                var fVolume='';
                if(idCargoMeasure==1)
                {
                    fVolume = format_volume_js((length/100)*(width/100)*(height/100));
                    console.log("fVolume"+fVolume);
                    $("#iVolume"+iNumber).attr('value',fVolume);
                }
                else
                {
                    var fCmFactor = $("#fCmFactor").attr('value');
                    if(fCmFactor>0)
                    {
                        fVolume = format_volume_js(((length/fCmFactor )/100) * ((width/fCmFactor)/100) * ((height/fCmFactor)/100));
                        console.log("fVolume"+fVolume);
                        $("#iVolume"+iNumber).attr('value',fVolume);
                    }
                }
            }
            else
            {
               // $("#iVolume"+iNumber).attr('value','');
            }
            update_total_cargo_details_text();
        }
           
        function format_volume_js(fVolume)
        {
            if(fVolume!='')
            {
                var new_fVolume=fVolume.toFixed(4); 
                var volumeAry = new_fVolume.split(".");
                if(volumeAry[0]>99)
                {
                    new_fVolume = fVolume.toFixed(0);
                }
                else if(volumeAry[0]>9 && volumeAry[0]<=99)
                { 
                    new_fVolume = fVolume.toFixed(1);
                }
                else
                {
                    if(volumeAry[0]<1)
                    {
                        var iDecimalVal=volumeAry[1].substr(0,3);
                        if(iDecimalVal=='000')
                        {
                            new_fVolume = fVolume.toFixed(4);
                        }
                        else
                        {
                            iDecimalVal=volumeAry[1].substr(0,2);
                            if(iDecimalVal=='00')
                            {
                                new_fVolume = fVolume.toFixed(3);
                            }
                            else
                            {
                                new_fVolume = fVolume.toFixed(2);
                            }
                        }
                    }
                    else
                    { 
                        new_fVolume = fVolume.toFixed(2);
                    }
               } 
               return new_fVolume;
           }
       }
   </script>
    <div id="popup-bg"></div>
   <div id="popup-container">
       <div class="popup" style="width:850px;"> 
           <h3><?php echo $heading;?></h3> <br/>
           <br/>
           <form id="cargo_details_form" method="post" name="cargo_details_form">
           <table id="horizontal-scrolling-div-id">
               <tr>
                   <td><?=t($t_base.'fields/length');?></td>
                   <td><?=t($t_base.'fields/width');?></td>
                   <td><?=t($t_base.'fields/height');?></td>
                   <td></td>
                   <?php if($hideFlag){?>
                   <td>Cbm</td>
                   <?php }?>
                   <td><?=t($t_base.'fields/weight');?></td>
                   <td></td>
                   <td><?=t($t_base.'fields/quantity');?></td>
                   <td><?=t($t_base.'fields/description');?></td>
                   <td><?=t($t_base.'fields/colli');?></td>
                   <td><?=t($t_base.'fields/seller_reference');?></td>
                   <td>&nbsp;</td>
               </tr>
               <?php 
               if(!empty($_POST['cargodetailAry']) && empty($cargoDetailArr))
               {
                   $totalCargoLine=$_POST['totalCargoLine'];
                   for($i=1;$i<$totalCargoLine;++$i)
                   {
                       if($_POST['cargodetailAry']['idCargoFlag'][$i]=='1')
                       {
                          ?>
                           <tr id="add_booking_quote_container_<?php echo $i;?>">
                         <?php                         
                           pendingTrayCargoLineHtml($i,$_POST['iSearchMiniVersion']);
                         $totalCargoLineValue=$i;
                          ?>
                           </tr>
                           <?php 
                       }
                   }
                   ?>
                   <tr id="add_booking_quote_container_<?php echo $totalCargoLine;?>"> 
                   </tr>
                   <?php
                   if(!empty($kBooking->arErrorMessages))
                   { 
                       $szValidationErrorKey = '';
                       foreach($kBooking->arErrorMessages as $errorKey=>$errorValue)
                       {
                           $szValidationErrorKey = $errorKey ;
                           break;
                       } 
                       display_form_validation_error_message("cargo_details_form",$kBooking->arErrorMessages);
                   }
               }
               else if(!empty($cargoDetailArr) && empty($_POST['cargodetailAry'])){
                   foreach($cargoDetailArr as $cargoDetailArrs)
                   {
                        ++$i;
                       $_POST['cargodetailAry']['idCargo'][$i]=$cargoDetailArrs['id']; 
                       if(ceil($cargoDetailArrs['fLength'])>0)
                           $_POST['cargodetailAry']['iLength'][$i]=ceil($cargoDetailArrs['fLength']);

                       if(ceil($cargoDetailArrs['fWidth'])>0)
                           $_POST['cargodetailAry']['iWidth'][$i]=ceil($cargoDetailArrs['fWidth']);
                       if(ceil($cargoDetailArrs['fHeight'])>0)
                           $_POST['cargodetailAry']['iHeight'][$i]=ceil($cargoDetailArrs['fHeight']);
                       $_POST['cargodetailAry']['idCargoMeasure'][$i]=$cargoDetailArrs['idCargoMeasure'];
                       if(ceil($cargoDetailArrs['fWeight'])>0)
                           $_POST['cargodetailAry']['iWeight'][$i]=ceil($cargoDetailArrs['fWeight']);
                       if($hideFlag){
                           if((float)$cargoDetailArrs['fVolume']>0)
                           {
                               $_POST['cargodetailAry']['iVolume'][$i]=format_volume_value((float)$cargoDetailArrs['fVolume']);
                           }
                           else
                           {
                               if($cargoDetailArrs['idCargoMeasure']==1)  // cm
                               {
                                   $fTotalCargoVolume = ($cargoDetailArrs['fLength']/100) * ($cargoDetailArrs['fWidth']/100) * ($cargoDetailArrs['fHeight']/100);
                               }
                               else
                               {                                
                                   $kWHSSearch=new cWHSSearch();
                                   $fCmFactor = $kWHSSearch->getCargoFactor($cargoDetailArrs['idCargoMeasure']);
                                   if($fCmFactor>0)
                                   {
                                       $fTotalCargoVolume = (($cargoDetailArrs['fLength']/$fCmFactor )/100) * (($cargoDetailArrs['fWidth']/$fCmFactor)/100) * (($cargoDetailArrs['fHeight']/$fCmFactor)/100);
                                   }	
                               }
                               $_POST['cargodetailAry']['iVolume'][$i]=format_volume_value($fTotalCargoVolume);
                           }
                       }

                       $_POST['cargodetailAry']['idWeightMeasure'][$i]=$cargoDetailArrs['idWeightMeasure'];
                       $_POST['cargodetailAry']['iQuantity'][$i]=ceil($cargoDetailArrs['iQuantity']);
                       $_POST['cargodetailAry']['szCommodity'][$i]=$cargoDetailArrs['szCommodity'];
                       $_POST['cargodetailAry']['iColli'][$i]=$cargoDetailArrs['iColli'];
                       $_POST['cargodetailAry']['szSellerReference'][$i]=$cargoDetailArrs['szSellerReference'];
                       ?>
                       <tr id="add_booking_quote_container_<?php echo $i;?>">
                     <?php                         
                       pendingTrayCargoLineHtml($i,$_POST['iSearchMiniVersion']);
                     $totalCargoLineValue=$i;
                      ?>
                       </tr>
                       <?php
                   }
                   ?>
                   <tr id="add_booking_quote_container_<?php echo ++$i;?>"> 
                   </tr>
                   <?php
                    $totalCargoLine=$i;
                   } else{
                       $totalCargoLine=2;
                       $totalCargoLineValue=1;
?>                      
               <tr id="add_booking_quote_container_1">               
                   <?php pendingTrayCargoLineHtml(1,$_POST['iSearchMiniVersion']);?>
               </tr>
               <tr id="add_booking_quote_container_2"> 
                   </tr>
               <?php }?>
                   <input type="hidden" name="totalCargoLine" id="totalCargoLine" value="<?php echo $totalCargoLine;?>" />
                   <input type="hidden" name="totalCargoLineLeft" id="totalCargoLineLeft" value="<?php echo $totalCargoLineValue;?>" />
                   <input type="hidden" name="idBooking" id="idBooking" value="<?php echo $_POST['idBooking'];?>" />
                   <input type="hidden" name="iNoLineUpdated" id="iNoLineUpdated" value="<?php echo $_POST['idBooking'];?>" />
                   <input type="hidden" name="fCmFactor" id="fCmFactor" value="<?php echo $fCmFactor;?>" />
                   <input type="hidden" name="iSearchMiniVersion" id="iSearchMiniVersion" value="<?php echo $_POST['iSearchMiniVersion'];?>" />
                   
                   <input type="hidden" name="iDisplayVolume" id="iDisplayVolume" value="0" />
                   <input type="hidden" name="iDisplayWeight" id="iDisplayWeight" value="0" />
                   <input type="hidden" name="iDisplayColli" id="iDisplayColli" value="0" />
                   <input type="hidden" name="fTotalVolume" id="fTotalVolume" value="0" />
                   <input type="hidden" name="fTotalWeight" id="fTotalWeight" value="0" />
                   <input type="hidden" name="iTotalNumColli" id="iTotalNumColli" value="0" />
           </table>
           </form>
           <div class="clearfix" style="width:100%;margin-top:10px;">
               <div class="btn-container">
                   <p align="center">
                       <?php if((int)$iAlreadyPaidBooking==1){?>
                       <a href="javascript:void(0)"  class="button1" onclick="showHide('contactPopup');">
                           <span>Close</span>
                       </a>
                       <?php }else{?>
                        <span style="width:20%;">&nbsp;</span>
                        <span style="width:20%;">
                            <a href="javascript:void(0)"  class="button2" onclick="showHide('contactPopup');">
                                <span>Cancel</span>
                            </a>
                        </span>
                        <span style="width:20%;">
                            <a href="javascript:void(0)"  id="save_cargo_line_pending_tray" class="button1" style="opacity:0.4;">
                            <span>SAVE</span>
                            </a>
                        </span> 
                        <span class="wds-40" id="update_total_fields_container">
                            <input type="checkbox" name="iUpdateTotalCB" id="iUpdateTotalCB" value="1" checked="" disabled="disabled"><label id="update_total_label_container" class="grey_text">Update booking total</label>
                        </span>
                       <?php } ?>
                   </p>
               </div>
           </div>    
         </div>
   </div>  
   <?php
}

function displayForwarderReferralFeeDetails($idForwarder,$bEditFlag=false,$bAddNew=false)
{ 
    $kConfig = new cConfig();
    $kReferralPricing = new cReferralPricing();
    $transportModeAry = array();
    $transportModeAry = $kConfig->getAllTransportMode(); 
    
    $forwarderReferralFeeAry = array();
    $forwarderReferralFeeAry = $kReferralPricing->getAllReferralFeeByForwarder($idForwarder);
    
    if(!empty($transportModeAry))
    {
        foreach($transportModeAry as $transportModeArys)
        {
            if($transportModeArys['id']==8 || $transportModeArys['id']==9) //8. All mode, 9. Rest
            {
                continue;
            } 
            if($bEditFlag)
            {
                if(!empty($_POST['updatePrefControlArr']['fReferralFee']))
                {
                    $fReferralFee = $_POST['updatePrefControlArr']['fReferralFee'][$transportModeArys['id']];
                }
                else
                {
                    $fReferralFee = (float)$forwarderReferralFeeAry[$transportModeArys['id']]['fReferralFee'];
                }
                ?>
                <div class="profile-fields">
                    <span class="field-name"><?php echo $transportModeArys['szShortName'];?></span>  
                    <span class="field-container">
                        <input type="text" style="width: 100px;" id="fReferralFee_<?php echo $transportModeArys['id']; ?>" name="updatePrefControlArr[fReferralFee][<?php echo $transportModeArys['id']; ?>]" value='<?php echo $fReferralFee; ?>'> % 
                    </span>
                </div>
                <?php
                
            }
            else if($bAddNew)
            {
                ?>
                <div class="ui-fields">
                    <span class="field-name"><?php echo $transportModeArys['szShortName'];?></span>  
                    <span class="field-container">
                        <input type="text" style="width: 100px;" id="fReferralFee_<?php echo $transportModeArys['id']; ?>" name="createArr[fReferralFee][<?php echo $transportModeArys['id']; ?>]" value='<?php echo $fReferralFee; ?>'> % 
                    </span>
                </div>
                <?php
            }
            else
            {
                ?>
                <div class="ui-fields">
                    <span class="fl-30"><?php echo $transportModeArys['szShortName'];?></span>  
                    <span class="field-container"><?php echo number_format((float)$forwarderReferralFeeAry[$transportModeArys['id']]['fReferralFee'],2)." %"; ?></span>
                </div>
                <?php
            } 
        }
    } 
}