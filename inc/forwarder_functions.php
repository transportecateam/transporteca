<?php
if( !isset( $_SESSION ) )
{
	session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * forwarder_functions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Ashish
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/quick_quote_functions.php" );
require_once( __APP_PATH__ . "/inc/billing_functions.php" );
require_once( __APP_PATH__ . "/inc/forwarder_pricing_functions.php" );

function showUploadFileFormat($uploadServiceType='')
{

	$t_base="BulkUpload/";
	if($uploadServiceType=='CFS locations')
	{
            ?>
            <p align="left"><?=t($t_base.'messages/cfs_location_upload_service_line_1')?>:</p>
            <ul>
                <li><?=t($t_base.'messages/CFS_name')?></li>
                <li><?=t($t_base.'messages/a_s_n_p_c_c')?></li>
                <li><?=t($t_base.'messages/Phone_number')?></li>
                <li><?=t($t_base.'messages/exact_location')?></li>
            </ul>
            <?php	
	}
        else if($uploadServiceType=='Airport Warehouses')
	{
            ?>
            <p align="left"><?=t($t_base.'messages/airport_warehouse_location_upload_service_line_1')?>:</p>
            <ul>
                <li><?=t($t_base.'messages/airport_name')?></li>
                <li><?=t($t_base.'messages/a_s_n_p_c_c')?></li>
                <li><?=t($t_base.'messages/Phone_number')?></li>
                <li><?=t($t_base.'messages/exact_location')?></li>
            </ul>
            <?php	
	}
	else if($uploadServiceType=='LCL Services')
	{
            ?>
            <p align="left"><?=t($t_base.'messages/lcl_service_upload_service_line_1')?>:</p>
            <ul>
                <li><?=t($t_base.'messages/origin_and_destination_cfs')?></li>
                <li><?=t($t_base.'messages/pricing_data_lcl_service')?></li>
                <li><?=t($t_base.'messages/booking_requirment_data_lcl_service')?></li>
                <li><?=t($t_base.'messages/validity_of_the_rate')?></li>
            </ul>
            <?php
	}
        else if($uploadServiceType=='Airfreight Services')
	{
            ?>
            <p align="left"><?=t($t_base.'messages/air_service_upload_service_line_1')?>:</p>
            <ul>
                <li><?=t($t_base.'messages/air_origin_and_destination_cfs')?></li>
                <li><?=t($t_base.'messages/air_pricing_data_lcl_service')?></li>
                <li><?=t($t_base.'messages/air_booking_requirment_data_lcl_service')?></li>
                <li><?=t($t_base.'messages/validity_of_the_rate')?></li>
            </ul>
            <?php
	}
	else if($uploadServiceType=='Haulage')
	{
		?>
			<p align="left"><?=t($t_base.'messages/haulage_upload_service_line_1')?>:</p>
			<ul>
				<li><?=t($t_base.'messages/pricing_model_z_c_p_d')?></li>
				<li><?=t($t_base.'messages/wm_factor_and_fuel_surcharge')?></li>
				<li><?=t($t_base.'messages/haulagge_pricing_detail')?></li>
				<li><?=t($t_base.'messages/transit_time')?></li>
			</ul>
		<?
	}
	else if($uploadServiceType=='Customs Clearance')
	{
		?>
			<p align="left"><?=t($t_base.'messages/custom_clearance_upload_service_line_1')?>:</p>
			<ul>
				<li><?=t($t_base.'messages/cc_cfs_location_rates')?></li>
				<li><?=t($t_base.'messages/whether_rate_for_dest')?></li>
				<li><?=t($t_base.'messages/rate_for_i_e_cc')?></li>
				<li><?=t($t_base.'messages/currency_rate_for_cc')?></li>
			</ul>
		<?
	}
	else
	{
		?>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>				
		<?php
	}
}

function process_cost_approved_by_management($completedApprovedByManagementArr,$forwarderAwaitingCFSDataArr,$flag=false)
{
    $t_base = "BulkUpload/";
    ?>
    <table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="cost_pproved_by_management">
        <tr>
            <td width="11%"><strong><?=t($t_base.'fields/completed')?></strong></td>
            <td width="20%"><strong><?=t($t_base.'fields/cost_approved_by')?></strong></td>
            <td width="17%"><strong><?=t($t_base.'fields/data_relate_to')?></strong></td>
            <td width="6%" align="left"><strong><?=t($t_base.'fields/data')?></strong></td>
            <td width="11%" align="left"><strong><?=t($t_base.'fields/records')?></strong></td>
            <td width="14%" align="right"><strong><?=t($t_base.'fields/processing_cost')?></strong></td>
            <td width="17%" ><strong><?=t($t_base.'fields/submitted_by')?></strong></td>
            <td width="5%">&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <?php 
            if(!empty($completedApprovedByManagementArr))
            {
                $kUploadBulkService = new cUploadBulkService();
                foreach($completedApprovedByManagementArr as $completedApprovedByManagementArrs)
                { 	
                    $filenameArr=$kUploadBulkService->bulkUploadServiceFilesByManagement($completedApprovedByManagementArrs['id']);
                    $filename=$filenameArr[0]['szFileName'];
        ?>
                    <tr id="approved_cost_<?=++$i?>" onclick="sel_line_completed_for_cost_approval('approved_cost_<?=$i?>','<?=$completedApprovedByManagementArrs['id']?>','<?=$completedApprovedByManagementArrs['iStatus']?>','<?=__DATA_REVIEWED_BY_FORWARDER__?>','<?=$filename?>');">
                        <td>
                            <?php 
                                if($completedApprovedByManagementArrs['dtCompleted']!='' && $completedApprovedByManagementArrs['dtCompleted']!='0000-00-00 00:00:00')
                                {
                                    echo date('j. F',strtotime($completedApprovedByManagementArrs['dtCompleted']));
                                }	 				
                            ?>
                        </td>
                        <td>
                            <?php
                                if($flag)
                                    echo utf8_encode($completedApprovedByManagementArrs['szCostApprovalBy']);
                                else
                                    echo $completedApprovedByManagementArrs['szCostApprovalBy'];	
                            ?>
                        </td>
                        <td><?=$completedApprovedByManagementArrs['iFileType']?></td>
                        <td align="left"><?=$completedApprovedByManagementArrs['iFileCount']?> <?php if($completedApprovedByManagementArrs['iFileCount']>1){ echo t($t_base.'fields/Files'); }else { echo t($t_base.'fields/File');  }?></td>
                        <td align="left"><?=number_format((int)$completedApprovedByManagementArrs['iActualRecords'])?> <?=$completedApprovedByManagementArrs['szRecordType']?></td>
                        <td align="right"><?=$completedApprovedByManagementArrs['szCurrency']." ".number_format((float)$completedApprovedByManagementArrs['fProcessingCost'],2);?></td>
                        <td>
                            <?php
                                if($flag)
                                    echo utf8_encode($completedApprovedByManagementArrs['szForwarderContactName']);
                                else
                                    echo $completedApprovedByManagementArrs['szForwarderContactName'];
                            ?>
                        </td>
                        <td><a href="javascript:void(0)" id="comment_details_<?=$completedApprovedByManagementArrs['id']?>" onclick="open_comment_popup('<?=$completedApprovedByManagementArrs['id']?>');"><img src="<?=__BASE_URL_FORWARDER_LOGO__?>/comments.png" alt="Comments" title="Comments"></a></td>
                    </tr>
                <?php
                    }
                }
                if(!empty($forwarderAwaitingCFSDataArr))
                {
                    $kWHSSearch=new cWHSSearch();
                    foreach($forwarderAwaitingCFSDataArr as $forwarderAwaitingCFSDataArrs)
                    { 
                        $forwarderContactName = $kWHSSearch->findForwarderFirstLastName($forwarderAwaitingCFSDataArrs['idForwarderContact']); 
                            ?>
                            <tr id="approved_cost_<?=++$i?>" onclick="sel_line_completed_for_cost_awaiting_approval('approved_cost_<?=$i?>','<?=$forwarderAwaitingCFSDataArrs['id']?>','<?=$forwarderAwaitingCFSDataArrs['idForwarder']?>');">
                                <td>
                                    <?php 
                                        if($forwarderAwaitingCFSDataArrs['dtCompletedOn']!='' && $forwarderAwaitingCFSDataArrs['dtCompletedOn']!='0000-00-00 00:00:00')
                                        {
                                                      echo date('j. F',strtotime($forwarderAwaitingCFSDataArrs['dtCompletedOn']));
                                        }	 				
					?>
                                </td>
                                <td>N/A</td>
                                <td><?php echo $forwarderAwaitingCFSDataArrs['szDataRelatedTo']; ?></td>
                                <td align="left">N/A</td>
                                <td align="left"><?=number_format((int)$forwarderAwaitingCFSDataArrs['iRecords'])?> <?=t($t_base.'fields/Locations')?></td>
                                <td align="right">N/A</td>
                                <td>
                                    <?php
					if($flag)
                                            echo utf8_encode($forwarderContactName);
					else
                                            echo $forwarderContactName;	
                                    ?>
                                </td>
                                <td>N/A</td>
                            </tr>
                            <?php
                    }
                    }

                    if(empty($forwarderAwaitingCFSDataArr) && empty($completedApprovedByManagementArr))
                    {?>
			<tr>
				<td colspan="8" align="center"><?=t($t_base.'messages/no_record_found')?></td>
			</tr>
			<?php }?>
		</table>
		<p style="font-size:10px;"><i><?=t($t_base.'messages/note_heading')?>: <?=t($t_base.'messages/note_90_day_for_approval')?></i></p>
		<br/>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
			<td ><?=t($t_base.'fields/review_and_approve_one_by_one')?>&nbsp;&nbsp;&nbsp;</td>
			<td colspan="2">
				<?=t($t_base.'fields/review_and_approve_in_bulk')?>
			</td>
			</tr>
			<tr>
			<td width="40%"><a href="javascript:void(0)" class="button1" id="review_top_data" style="opacity:0.4"><span><?=t($t_base.'fields/review')?></span></a> </td>
			<td width="40%">
				<a href="javascript:void(0)" class="button1" id="download_top_data" style="opacity:0.4"><span><?=t($t_base.'fields/download')?></span></a>
				<a href="javascript:void(0)" class="button1" id="approved_top_data" style="opacity:0.4"><span><?=t($t_base.'fields/approve')?></span></a>
			</td>
			<td align="right" width="20%">
			<a href="javascript:void(0)" class="button2" id="delete_top_data" style="opacity:0.4"><span><?=t($t_base.'fields/delete')?></span></a>
			</td>
			</tr>
		</table>
	<?php
}


function waiting_for_approved_by_management($waitingForApprovalByManagement,$flag=false)
{
	$t_base = "BulkUpload/";
	?>
        <table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="wating_for_cost_approved_by_management">
            <tr>
                <td width="11%"><strong><?=t($t_base.'fields/submitted')?></strong></td>
                <td width="20%"><strong><?=t($t_base.'fields/Status')?></strong></td>
                <td width="16%"><strong><?=t($t_base.'fields/data_relate_to')?></strong></td>
                <td width="6%" align="left"><strong><?=t($t_base.'fields/data')?></strong></td>
                <td width="11%" align="left"><strong><?=t($t_base.'fields/records')?></strong></td>
                <td width="14%" align="right"><strong><?=t($t_base.'fields/processing_cost')?></strong></td>
                <td width="17%"><strong><?=t($t_base.'fields/submitted_by')?></strong></td>
                <td width="5%">&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <?php
                if(!empty($waitingForApprovalByManagement))
                {
                    $i=0;
                    $kUploadBulkService = new cUploadBulkService();
                    foreach($waitingForApprovalByManagement as $waitingForApprovalByManagements)
                    {
                        $filename='';
                        $filenameArr=$kUploadBulkService->bulkUploadServiceFiles($waitingForApprovalByManagements['id']);
                        $totalCount=count($filenameArr);
                        if($totalCount==1)
                        {
                            $filename=$filenameArr[0]['szFileName'];
                        }
                        else
                        {
                            $filename='';
                        }
                        ?>
                        <tr id="wating_for_cost_<?=++$i?>" onclick="sel_line_waiting_for_cost_approval('wating_for_cost_<?=$i?>','<?=$waitingForApprovalByManagements['id']?>','<?=$waitingForApprovalByManagements['iStatus']?>','<?=__COST_AWAITING_APPROVAL__?>','<?=$filename?>');">
                            <td>
                                <?php 
                                    if($waitingForApprovalByManagements['dtSubmitted']!='' && $waitingForApprovalByManagements['dtSubmitted']!='0000-00-00 00:00:00')
                                    {
                                        echo date('j. F',strtotime($waitingForApprovalByManagements['dtSubmitted']));
                                    }	 				
                                ?>
                            </td>
                            <td>
                                <?php 
                                    if($waitingForApprovalByManagements['iStatus']=='1')
                                    {
                                        echo t($t_base.'messages/data_submitted');
                                    }
                                    else if($waitingForApprovalByManagements['iStatus']=='2')
                                    {
                                        echo t($t_base.'messages/cost_awaiting_approval');
                                    }
                                    else if($waitingForApprovalByManagements['iStatus']=='3')
                                    {
                                        if($waitingForApprovalByManagements['dtExpected']!='' && $waitingForApprovalByManagements['dtExpected']!='0000-00-00 00:00:00')
                                        {
                                            echo t($t_base.'messages/expected')." ".date('j. F',strtotime($waitingForApprovalByManagements['dtExpected'])); 
                                        } 			  	 
                                    }	
                                ?>
                            </td>
                            <td><?=$waitingForApprovalByManagements['iFileType']?></td>
                            <td align="left"><?=$waitingForApprovalByManagements['iFileCount']?> <? if($waitingForApprovalByManagements['iFileCount']>1){ echo t($t_base.'fields/Files'); }else { echo t($t_base.'fields/File');  }?></td>
                            <td align="left">
                                <?php
                                    if($waitingForApprovalByManagements['iStatus']!=1)
                                    {
                                        echo number_format((int)$waitingForApprovalByManagements['iTotalRecords'])." ".$waitingForApprovalByManagements['szRecordType'];
                                    }
                                    else
                                    {
                                        echo t($t_base.'messages/unknown');
                                    }
                                ?>
                            </td>
                            <td align="right">
                                <?php
                                    if($waitingForApprovalByManagements['iStatus']=='1')
                                    {
                                        echo t($t_base.'messages/under_review');
                                    }
                                    else
                                    {
                                        echo $waitingForApprovalByManagements['szCurrency']." ". number_format((float)$waitingForApprovalByManagements['fProcessingCost'],2);
                                    }
                                ?> 			
                            </td>
                            <td>
                                <?php
                                    if($flag)
                                        echo utf8_encode($waitingForApprovalByManagements['szForwarderContactName']);
                                    else
                                        echo $waitingForApprovalByManagements['szForwarderContactName'];	
                                ?>
                            </td>
                            <td><a href="javascript:void(0)" id="comment_details_<?=$waitingForApprovalByManagements['id']?>" onclick="open_comment_popup('<?=$waitingForApprovalByManagements['id']?>');"><img src="<?=__BASE_URL_FORWARDER_LOGO__?>/comments.png" alt="Comments" title="Comments"></a></td>
                        </tr>
				
                        <?php }}else{?>
			<tr>
                            <td colspan="8" align="center"><?=t($t_base.'messages/no_record_found')?></td>
			</tr>
			<?php }?>
		</table>
		<br/>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td width="40%">&nbsp;</td>
                        <td width="40%">
                            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="view_files"><span><?=t($t_base.'fields/view_files')?></span></a>
                            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="approved_cost"><span><?=t($t_base.'fields/approved_cost')?></span></a></td>
                        <td align="right" width="20%">
                            <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="delete_files"><span><?=t($t_base.'fields/delete')?></span></a>
                        </td>
                    </tr>
		</table>
	<?php
}
function pricingHaulage($haulagePricingDataArr=array(),$idForwarder=0)
{
	$t_base = "HaulaugePricing/";

	if(!empty($haulagePricingDataArr)) {
?>
	<div id="view_haulage_model_div_id">
            <?php
                cfs_pricing_model_list($haulagePricingDataArr);
            ?>
	</div>
	<br/>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td width="10%"><?=t($t_base.'fields/priority');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/top_table_priority_tool_tip_heading');?>','<?=t($t_base.'messages/top_table_priority_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                <td width="30%" align="left"><a href="javascript:void(0)" id="priority_up" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/up')?></span></a><a href="javascript:void(0)" id="priority_down" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/down')?></span></a> </td>
                <td width="60%" align="right"><?=t($t_base.'title/select_edit_line')?> <a href="javascript:void(0)" id="change_status_button" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/activate')?></span></a></td>
            </tr>
	</table>
        <?php } ?>
		
<?php }

function getForwarderTransferPDF($user,$id,$flag,$showflag=false)
{ 
        require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
		
	$idForwarder=sanitize_all_html_input($user);
	$batchNumber=sanitize_all_html_input($id);
	if(isset($flag))
	{
            $flag=sanitize_all_html_input($flag);
	}
	//$version="Forwarder-Payment-Notice".$idForwarder;
	$kForwarder= new cForwarder();
	$kBooking = new cBooking();
	$kWarehouse = new cWHSSearch();
	$kBooking = new cBooking();
	
	$markUpValue=$kWarehouse->getManageMentVariableByDescription(__MARK_UP_PRICING_PERCENTAGE__);
	$debitCredit=1;
	$addDetail=$kForwarder->detailsTransferPdfForwarder($idForwarder);
	$invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,'',$batchNumber,false,false,true);
        $showNewPdf=false;
        if($invoiceBillConfirmed)
	{
            $checkDate='2017-04-01 23:59:59';
            foreach($invoiceBillConfirmed as $inv1)
            {
                if($inv1['iDebitCredit']==2)
                {
                    if(strtotime($inv1['dtInvoiceOn'])>strtotime($checkDate))
                    {
                         $showNewPdf=true;       
                    }                    
                    if(!$showNewPdf)
                    {
                        if(strtotime($inv1['dtCreatedOn'])>strtotime($checkDate))
                        {
                             $showNewPdf=true;       
                        }
                    }
                }
                
            }
        }
        if($showNewPdf){
        $filename=getForwarderTransferPDF_v2($user,$id,$flag,$showflag);
        return $filename;
        }
            
            
	$uploadServiceAmount = $kBooking->getUploadServiceAmount($idForwarder,$batchNumber);
	$uploadText='';
	if(!empty($uploadServiceAmount))
	{
            $amountUploadService=$uploadServiceAmount[0]['fUploadServiceAmount'];
            $total_upladserviceamount="(".$uploadServiceAmount[0]['szForwarderCurrency']." ".$amountUploadService.")";
            if((float)$amountUploadService>0)
            {
                $uploadText=' 
                    <tr>
                        <td colspan="3" valign="top" width="499">Other account debits since last transfer</td>
                        <td valign="top" width="120" ALIGN="RIGHT">'.$total_upladserviceamount.'</td>
                    </tr>
                ';
            }
	}
        
//        $CreateLabelBatch = $batchNumber + 1;
//        $szInvoice_formatted = $kAdmin->getFormattedString($CreateLabelBatch,6);
//        $customer_code = $szInvoice_formatted ;
//        $totalCourierLabelArr=$kAdmin->totalAmountCourierLabelFee($customer_code); 
//        
//        if(!empty($totalCourierLabelArr))
//	{
//            $amountCourierLabelService = $totalCourierLabelArr[0]['totalCurrencyValue'];
//            $amountCourierLabelService = $amountCourierLabelService + $amountCourierLabelService ;
//            $total_upladserviceamount="(".$totalCourierLabelArr[0]['szForwarderCurrency']." ".$amountCourierLabelService.")";
//            if((float)$amountCourierLabelService>0)
//            {
////                $uploadText .=' 
////                    <tr>
////                        <td colspan="5" valign="top" width="499">Courier Booking and Labels invoice</td>
////                        <td valign="top" width="120" ALIGN="RIGHT">'.$total_upladserviceamount.'</td>
////                    </tr>
////                ';
//            }
//	} 
        
	//print_r($invoiceBillConfirmed);
        //die();
        $courierLabelFee='';
	$kForwarder->load($idForwarder);
	if($invoiceBillConfirmed)
	{	
		$data='';
		$date=date("Y-m-d H:i:s");
		//static $date;
		$sum=0;
		$transferMoney=0; 
                $labelFeeTotal=0;
		foreach($invoiceBillConfirmed as $inv)
		{ 
                    //print_r($inv);
                    //echo "<br/><br/>";
			$idBooking = $inv['idBooking'] ;
                        
                        if($inv['iDebitCredit']==8)
			{
                            $labelFeeTotal=$inv['fTotalPriceForwarderCurrency'];
                            $courierLabelFee='<tr>
                                    <td colspan="3" valign="top" width="499"><strong>Courier Booking and Labels fee withed as per our invoice '.$inv['szInvoice'].'</strong></td>
                                    <td valign="top" width="120" ALIGN="RIGHT"><strong>('.$inv['szForwarderCurrency']." ".number_format((float)($inv['fTotalPriceForwarderCurrency']),2).')</strong></td>
                                    
                            </tr>';
                        }
                        if($inv['iDebitCredit']==12)
			{
                            $labelFeeTotal=$inv['fTotalPriceForwarderCurrency'];
                            $courierLabelFee='<tr>
                                    <td colspan="3" valign="top" width="499"><strong>Handling fee withheld as per our invoice '.$inv['szInvoice'].'</strong></td>
                                    <td valign="top" width="120" ALIGN="RIGHT"><strong>('.$inv['szForwarderCurrency']." ".number_format((float)($inv['fTotalPriceForwarderCurrency']),2).')</strong></td>
                                    
                            </tr>';
                        }
                        
			 
			if($inv['iDebitCredit']==1)
			{	
				//$totalAmount=$totalAmount+round($inv['fReferalAmount'],2);
                                $labelFee=0.00;
                                if($inv['fLabelFeeUSD']!='')
                                {
                                    $labelFee=$inv['fLabelFeeUSD']/$inv['fForwarderExchangeRate'];
                                }
                                if($inv['fTotalHandlingFeeUSD']!='')
                                {
                                    $fTotalHandlingFee = $inv['fTotalHandlingFeeUSD']/$inv['fForwarderExchangeRate'];
                                }
				$referralFee='0.00';
				$forwarderAmount='0.00';
				$referralFee=number_format((float)$inv['fReferalAmount'],2);
				$forwarderAmount=number_format((float)$inv['fTotalPriceForwarderCurrency'],2);
				$balance=round((float)$inv['fTotalPriceForwarderCurrency'],2)-(round((float)$inv['fReferalAmount'],2)+round((float)$labelFee,2)+round((float)$fTotalHandlingFee,2));
				
				$szBookingRefNum = explode(":",$inv['szBooking']);
				
                                if(!empty($inv['dtCreditOn']) && $inv['dtCreditOn']!='0000-00-00 00:00:00')
                                {
                                    $from=date('d F Y',strtotime($inv['dtCreditOn']));
                                }
                                else if(!empty($inv['dtInvoiceOn']) && $inv['dtInvoiceOn']!='0000-00-00 00:00:00')
                                {
                                    $from=date('d F Y',strtotime($inv['dtInvoiceOn']));
                                }
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetailsCountryName($idBooking); 
				$szTrade = $postSearchAry['szShipperCity']." - ".$postSearchAry['szConsigneeCity'] ;
				
				if(!empty($postSearchAry['szForwarderReference']))
				{
                                    $szForwarderReference = $postSearchAry['szForwarderReference'] ;
				}
				else
				{
                                    $szBookingRefNo=explode(":",$inv['szBooking']);
                                    $szForwarderReference = $szBookingRefNum[1] ;
				} 
                                $fTotalHandlingFeeInForwarderCurrencyVat=0;
                                $bookingDetailArr = $kBooking->getBookingDetails($idBooking);
                                $inv['fTotalPriceForwarderCurrency']=$inv['fTotalPriceForwarderCurrency'];
                                $dtInvoiceOn = date('d-m-Y',strtotime($inv['dtInvoiceOn']));
//                                if($bookingDetailArr['iHandlingFeeApplicable']==1 && strtotime($dtInvoiceOn)>strtotime(__SHOW_MIN_HANDLING_FEE_DATE__))
//                                {
//                                    
//                                    $fTotalHandlingFeeUSD=$bookingDetailArr['fTotalHandlingFeeUSD'];
//                                    $fTotalHandlingFeeInForwarderCurrencyVat=0;
//                                    $fTotalHandlingFeeInForwarderCurrency=round((float)($fTotalHandlingFeeUSD/$bookingDetailArr['fForwarderExchangeRate']),2);
//                                    
//                                    $fTotalPriceForwarderCurrency=$bookingDetailArr['fTotalForwarderPriceWithoutHandlingFee'];
//                                    $inv['fTotalPriceForwarderCurrency']=$fTotalPriceForwarderCurrency+$fTotalHandlingFeeInForwarderCurrency;
//                                }
				
                                
				$data .='
                                    <tr>
					<td>'.convertUTF($from).'</td>
					<td>'.convertUTF($szForwarderReference).'</td>
					<td>'.convertUTF($szTrade).'</td>
					<td ALIGN="RIGHT">'.convertUTF($inv['szForwarderCurrency']).' '.number_format($inv['fTotalPriceForwarderCurrency'],2).'</td>
					
				</tr>
				';
                                /*<td ALIGN="RIGHT">'.$inv['szForwarderCurrency'].' '.$referralFee.'</td>
                                        <td ALIGN="RIGHT">'.$inv['szForwarderCurrency'].' '.number_format($labelFee,2).'</td>
					<td ALIGN="RIGHT">'.$inv['szForwarderCurrency'].' '.number_format((float)$balance,2).'</td>*/
				$curency=$inv['szForwarderCurrency'];
				$sum=$sum+$inv['fTotalPriceForwarderCurrency'];
			}
			if($inv['iDebitCredit']==2)
			{	
                            $totalAmount=0;
                            $transferMoneyAmount=0;
                            if((float)$amountUploadService>0)
                            {
                                $totalAmount=$inv['fTotalPriceForwarderCurrency'];
                                $transferMoneyAmount = $inv['fTotalPriceForwarderCurrency'] + $amountUploadService;
                            }
                            else
                            {
                                $totalAmount=$inv['fTotalPriceForwarderCurrency'];
                                $transferMoneyAmount=$inv['fTotalPriceForwarderCurrency'];
                            }
                            $invoice=$inv['szInvoice'];
                            $transferDate=date('d F Y',strtotime($inv['dtPaymentConfirmed']));
                            $paymentRecieved=$inv['szCurrency']." ";
                            $paymentTransfer=$inv['szCurrency']." ".number_format((float)$totalAmount,2);
                            $transferMoney=$transferMoneyAmount;
                            $to=$inv['dtPaymentConfirmed'];
			}
			if($inv['iDebitCredit']==6)
			{	
                            
                                $labelFee=0.00;
                                if($inv['fLabelFeeUSD']!='')
                                {
                                    $labelFee=$inv['fLabelFeeUSD']/$inv['fForwarderExchangeRate'];
                                }
				//$totalAmount=$totalAmount+round($inv['fReferalAmount'],2);
				$referralFee='0.00';
				$forwarderAmount='0.00';
				$referralFee=number_format((float)$inv['fReferalAmount'],2);
				$forwarderAmount=number_format((float)$inv['fTotalPriceForwarderCurrency'],2);
				$balance=round((float)$inv['fTotalPriceForwarderCurrency'],2)-(round((float)$inv['fReferalAmount'],2)+round((float)$labelFee,2));
				//$from=date('d F Y',strtotime($inv['dtDebitOn']));
                                $szBookingRefNum = explode(":",$inv['szBooking']);
				
                                if(!empty($inv['dtPaymentConfirmed']) && $inv['dtPaymentConfirmed']!='0000-00-00 00:00:00')
				{
					$from = date('d F Y',strtotime($inv['dtPaymentConfirmed'])); 
				}
				else if(!empty($inv['dtDebitOn']) && $inv['dtDebitOn']!='0000-00-00 00:00:00')
				{
					$from = date('d F Y',strtotime($inv['dtDebitOn'])); 
				}
				else 
				{
					$from = date('d F Y',strtotime($inv['dtInvoiceOn'])); 
				}
				$postSearchAry = array();
				$postSearchAry = $kBooking->getBookingDetailsCountryName($idBooking); 
				$szTrade = $postSearchAry['szShipperCity']." - ".$postSearchAry['szConsigneeCity'] ;
				 
				if(!empty($postSearchAry['szForwarderReference']))
				{
                                    $szForwarderReference = $postSearchAry['szForwarderReference'] ;
				}
				else
				{
                                    $szBookingRefNo=explode(":",$inv['szBooking']);
                                    $szForwarderReference = $szBookingRefNum[1] ;
				} 
				$data .='
				<tr>
					<td>'.convertUTF($from).'</td>
					<td>'.convertUTF($szForwarderReference).'</td>
					<td>'.convertUTF($szTrade).'</td>
					<td ALIGN="RIGHT">('.convertUTF($inv['szForwarderCurrency']).' '.number_format($inv['fTotalPriceForwarderCurrency'],2).')</td>
					
				</tr>
				';
                                /*<td ALIGN="RIGHT">('.$inv['szForwarderCurrency'].' '.$referralFee.')</td>
                                        <td ALIGN="RIGHT">('.$inv['szForwarderCurrency'].' '.$labelFee.')</td>
					<td ALIGN="RIGHT">('.$inv['szForwarderCurrency'].' '.number_format((float)$balance,2).')</td>*/
				$curency=$inv['szForwarderCurrency'];
				$sum=$sum-$inv['fTotalPriceForwarderCurrency'];
			}
			
			if(strtotime($date)>strtotime($inv['dtCreatedOn']))
			{
                            $date=$inv['dtCreatedOn'];	 
			}
		}
               // die();
	}
       //echo $labelFeeTotal."labelFeeTotal";
	$sum=$sum-$labelFeeTotal;
        
	$paymentRecieved.=number_format((float)$sum,2);
		$datefrom=($date!='0000-00-00 00:00:00')?date('d F Y',strtotime($date)):'';
		$timefrom=($date!='0000-00-00 00:00:00')?date('H:i',strtotime($date)):'';
		$dateto=($to!='0000-00-00 00:00:00')?date('d F Y',strtotime($to)):'';
		$timeto=($to!='0000-00-00 00:00:00')?date('H:i',strtotime($to)):'';
	$add='';
	if($addDetail['szCompanyName'])
	{
            $add.="<br />".convertUTF($addDetail['szCompanyName'])."<br />";
	}
	$link=$addDetail['szControlPanelUrl'];
	  
	  
		if($flag!='PDF')
                {
		  	$bodyfontsize='style="background:#000;text-align:center;font-size:14px;"';
		  	$image_url=__MAIN_SITE_HOME_PAGE_URL__;
		  	$dataview='<p style="text-align:right;margin:5px auto 10px;width:690px;">
					<a target="_blank" href='.__BASE_URL__.'/downloadForwarderTransfer/'.$idForwarder.'/'.$batchNumber.'/ style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>
					<a onclick="PrintDiv();" href="javascript:void(0)" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a>
					</p>
					<div id ="forwarderTransfer" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;">';
		  	$script='<script>
			function PrintDiv()
			{    
			      var divToPrint = document.getElementById("forwarderTransfer");
			      var popupWin = window.open("", "_blank", "width=900,height=700");
			      popupWin.document.open();
			      popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
			      popupWin.document.close();
			}
			</script>';
		  	$footertext='<div style="width:650px;text-align:center;margin-top:150px;font-size:small;">
							Transporteca Limited, Suite 2009-10, 26/F, China Resources Building, No. 26 Harbour Road, Hong Kong
							Registration number: 1761706
						</div>';
		  }
		  else
		  {
                    $bodyfontsize='';
                    $image_url=__APP_PATH_ROOT__;
                    $dataview='';
                    $script='';
                    $footertext='';
		  }
	  
	  	  $contents = '<?xml version="1.0" encoding="iso-8859-1"?>
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<title>Forwarder Transfer</title>
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
			'.$script.'
			</head>
			<body '.$bodyfontsize.'>'.$dataview;
                                            if(empty($flag))
                                            {
                                            $contents .= '<table  border="0" cellpadding="3" cellspacing="1">
                                            <tbody>
                                                <tr>
                                                    <td  valign="top"  align="left">
                                                        <img src='.$image_url.'/images/TransportecaMail.jpg width="180" hieght="30">
                                                    </td>
                                                    <td colspan ="2" width="48%">
                                                    &nbsp;
                                                    </td>
                                                    <td  valign="middle"  align="right">
                                                        <img src='.$image_url.'/images/transfer.jpg width="180" height="30" >	
                                                    </td>
                                                </tr>
                                            </tbody>
					</table>
					<br />';
                                        }
                                        else
                                        {
                                            $contents .= '<table  border="0" cellpadding="3" cellspacing="1">
                                            <tbody>
                                                <tr>
                                                    <td  valign="top"  align="left">
                                                        &nbsp;
                                                    </td>
                                                    <td colspan ="2" width="48%">
                                                    &nbsp;
                                                    </td>
                                                    <td  valign="middle"  align="right">
                                                       &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
					</table>
					<br />';
                                        }
					$contents .= '<table width="720px" border="1" cellpadding="3" cellspacing="0" align="left" >
						
							<tr>
								<td colspan="4" valign="top">
								<strong>Message:</strong>
								</td>
							</tr>
							<tr>
								<td colspan="4" valign="top">
								Please find below details of bank transfer initiated to your  '.$kForwarder->szBankName.'  account '.$kForwarder->iAccountNumber.' on '.$dateto.'.<br />
								<br /></td>
							</tr>
							<tr>
								<td valign="top">Settlement Date</td>
								<td valign="top">Reference</td>
								<td valign="top">Trade</td>
								<td valign="top"  ALIGN="RIGHT"; >Invoice Value</td>
								
								</tr>
								ALL_DETAILS
							<tr>
								<td colspan="3" valign="top" width="499"><strong>Referral fee withheld as per our invoice '.$invoice.'</strong></td>
								<td valign="top" width="120" ALIGN="RIGHT"><strong>('.$curency." ".number_format((float)($sum-$transferMoney),2).')</strong></td>
								
							</tr>
                                                        '.$courierLabelFee.'
							'.$uploadText.'
							<tr>
								<td colspan="3" valign="top" width="499">
								<strong>Total transfer amount</strong>
								</td>
								<td valign="top" width="120" ALIGN="RIGHT"><strong>'.$paymentTransfer.'</strong></td>
							</tr>
						
					</table>			
					<div style="width:650px;">
						<p>&nbsp;</p>
						<center><span style="TEXT-ALIGN:CENTER;font-size:small;padding:0 0 0 0;">This transfer notice is available in soft copy on '.$addDetail['szControlPanelUrl'].' under Billing.<br />
						Transporteca&rsquo;s Term &amp; Conditions are available and agreed on '.$addDetail['szControlPanelUrl'].'.<br />
						This is an electronic statement and no signature is required.</span></center>
					</div>
					'.$footertext.'
					 </div>	
			</body>
			</html>';
			//$flag=1; 
                  /*<td valign="top"  ALIGN="RIGHT">Referral Fee</td>
                                                                <td valign="top"  ALIGN="RIGHT">Label Fee</td>
								<td valign="top" ALIGN="RIGHT">Amount</td>*/
		  	  $contents = str_replace("ALL_DETAILS", $data, $contents);
                          //$flag='';
		  if(empty($flag))
		  {
		  	echo $contents;
		  }
                  //echo $contents;
		   if(isset($flag) && $flag=="PDF")
		  {
		  	  class transfer extends HTML2FPDFBOOKING
			  {
                            function Footer()
                            {
                                  $this->SetY(-10);
                                  //Copyright //especial para esta vers�o
                                  $this->SetFont('Arial','B',9);
                                      $this->SetTextColor(0);
                                  //Arial italic 9
                                  $this->SetFont('Arial','B',9);
                                   $this->SetTextColor(105,105,105);
                                  //Page number
                                  $this->Cell(190,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
                                  //Return Font to normal
                                  $this->SetFont('Arial','',9);
                                  $this->SetTextColor(105,105,105);
                                  $this->SetY(-20);
                                  $this->Cell(0,10,"Transporteca Limited, Suite 2009-10, 26/F, China Resources Building, No. 26 Harbour Road, Hong Kong",0,0,'C');
                                  $this->SetY(-15);
                                  $this->Cell(0,10,"Registration number: 1761706 ",0,0,'C'); 
                              }
			  
			  }
			  
		  $dateto=($to!='0000-00-00 00:00:00')?date('Ymd',strtotime($to)):''; 
		  $version=$dateto."-Transfer"; 
	  	  $filename=__APP_PATH__."/forwarders/html2pdf/".$version.".pdf";	
		  $pdf=new transfer();
		  $pdf->AddPage();
                  $pdf->Image($image_url.'/images/TransportecaMail.jpg',10,10,30,10);
                  $pdf->Image($image_url.'/images/transfer.jpg',170,10,30,10);
		  //$pdf->SetFontSize(12);
		  //$pdf->SetTopMargin(5);
		  //$pdf->Rect(98,26,86,30);
		 // $pdf->SetFont('Arial','',8);
		  $pdf->WriteHTML($contents);
		  
		   $file_name=__APP_PATH__."/forwarders/html2pdf/".$version.".pdf";
                    //$file = "Booking-Invoice.pdf";
                    if(file_exists($file_name))
                    {
                            @unlink($file_name);
                    }
		  
		  $pdf->Output($file_name);
                  
		  if(!$showflag)
		  {
                    header('Content-type: application/pdf');
                    readfile(__APP_PATH__."/forwarders/html2pdf/".$version.".pdf");
                    @unlink($filename);
		  }
	}
	return $filename;
}

function price_changed_popup($szPriceString,$szBookingRandomNum,$type=false)
{
	$page_url = $_SESSION['booking_page_url'];
	if(empty($page_url))
	{
		$page_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum."/"; 
	}	
	$kBooking = new cBooking();
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	
	$kConfig_new = new cConfig();
	$kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
	$szCountryStrUrl = $kConfig_new->szCountryISO ;
	$kConfig_new->loadCountry($postSearchAry['idDestinationCountry']);
	$szCountryStrUrl .= $kConfig_new->szCountryISO ; 
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
	$cancel_page_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
	//$cancel_page_url = __SELECT_SERVICES_URL__.'/'.$szBookingRandomNum."/"; 
	$t_base_confimation = "BookingConfirmation/";
	
?>
		<div id="popup-bg"></div>
			<div id="popup-container">			
				<div class="popup abandon-popup">
				<h5><?=t($t_base_confimation.'title/price_update')?></h5>
				<p><?=t($t_base_confimation.'title/price_update_description')?> <?=$szPriceString?></p>
				<br>
				<div class="oh">
					<p align="center">
					<?
						if($type=='SHOW_POPUP_ONLY')
						{
							//recalculate_pricing('$cancel_page_url','$szBookingRandomNum','CANCEL');
							?>
							<a href="javascript:void(0);" onclick="showHide('change_price_div')" class="button1"><span><?=t($t_base_confimation.'fields/cancel')?></span></a>
							<a href="javascript:void(0);" onclick="display_price_changed_popup_cargo_changed('<?=$page_url?>','<?=$szBookingRandomNum?>','UPDATE_PRICING_ONLY');" class="button1"><span><?=t($t_base_confimation.'fields/continue')?></span></a>
							<?
						}
						else if($type=='PRICING_POPUP')
						{
							?>
							<a href="javascript:void(0);" onclick="redirect_url('<?=$cancel_page_url?>')" class="button1"><span><?=t($t_base_confimation.'fields/cancel')?></span></a>
							<a href="javascript:void(0);" onclick="redirect_url('<?=$page_url?>')" class="button1"><span><?=t($t_base_confimation.'fields/continue')?></span></a>
							<?
						}
						else
						{
					?>
						<a href="javascript:void(0);" onclick="cancel_booking('<?=__LANDING_PAGE_URL__?>')" class="button1"><span><?=t($t_base_confimation.'fields/cancel')?></span></a>
						<a href="javascript:void(0);" onclick="redirect_url('<?=$page_url?>')" class="button1"><span><?=t($t_base_confimation.'fields/continue')?></span></a>
					<?
						 }
					?>
					</p>
					</div>
			</div>
		</div>		
	<?
}

function recalculate_price_changed_popup($szPriceString,$page_url)
{
	$t_base_confimation = "BookingConfirmation/";

	?>
	<div id="popup-bg"></div>
			<div id="popup-container">			
				<div class="popup abandon-popup">
				<p class="close-icon" align="right">
				<a onclick="cancel_booking('<?=__LANDING_PAGE_URL__?>');" href="javascript:void(0);">
				<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
				</a>
				</p>
				<h5><?=t($t_base_confimation.'title/price_update')?></h5>
				<p><?=t($t_base_confimation.'title/recalculate_price_change_details')?> <?=$szPriceString?></p>
				<br>
				<div class="oh">
					<p align="center">
						<a href="javascript:void(0);" onclick="cancel_booking('<?=__LANDING_PAGE_URL__?>')" class="button1"><span>DELETE BOOKING</span></a>
						<a href="javascript:void(0);" onclick="redirect_url('<?=$page_url?>')" class="button1"><span>Continue</span></a>
					</p>
					</div>
			</div>
		</div>				
	<?
}

function write_text_on_image()
{
	$image_file = __APP_PATH_IMAGES__.'/CargoFlowLCL.png';
	//echo $image_file;
	$font_size = 10;
    $font_file_name = __APP_PATH__.'/fonts/font.ttf';
    
    header ("Content-type: image/png");
    
    $image = imagecreatefrompng($image_file);
    //$img=imagecreate($img_width,$img_height);
    $base_line_string = imagecolorallocatealpha($image,0,0,0,5);
	$string = 'heloo welcome ';
	imagettftext($image, $font_size, 0, 20, 20,$base_line_string, $font_file_name, $string);
	imagepng($image,"images2.png");
	imagedestroy($image);
}

function linkpricingHaulageModel($haulagePricingModelArr,$idForwarder,$idWarehouse,$idDirection)
{
	$t_base="HaulaugePricing/";
	
	if($idDirection=='1')
	{
		$formto=t($t_base.'fields/from');
	}
	else if($idDirection=='2')
	{
		$formto=t($t_base.'fields/to');
	}
	if($haulagePricingModelArr){
	?>
	<?
	
			foreach($haulagePricingModelArr as $haulagePricingDataArrs){
			?>
				<!-- <p><a href="javascript:void(0)" onclick="open_pricing_model_detail('<?=$haulagePricingDataArrs['idHaulageModel']?>','<?=$idWarehouse?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>');" id="haulage_img_<?=$haulagePricingDataArrs['idHaulageModel']?>"><img src="<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>/plus.png" width="15px;" ></a></a>
				<?
				if($haulagePricingDataArrs['iActive']=='1')
							echo "<strong>".$haulagePricingDataArrs['iPriority']." ".t($t_base.'fields/priority')." </strong>";
					   else
					   		echo t($t_base.'fields/not_active')." ";
					if($haulagePricingDataArrs['idHaulageModel']=='1')
					{
						echo "<strong>".t($t_base.'fields/pricing')." ".$formto." ".t($t_base.'fields/pricing_specific_zones_defined_on_a_map')."</strong>";
					}
					else if($haulagePricingDataArrs['idHaulageModel']=='2')
					{
						echo "<strong>".t($t_base.'fields/pricing')." ".$formto." ".t($t_base.'fields/pricing_named_cities')."</strong>";
					}
					else if($haulagePricingDataArrs['idHaulageModel']=='3')
					{
						echo "<strong>".t($t_base.'fields/pricing')." ".$formto." ".t($t_base.'fields/pricing_defined_postcodes')."</strong>";
					}
					else if($haulagePricingDataArrs['idHaulageModel']=='4')
					{
						echo "<strong>".t($t_base.'fields/pricing')." ".$formto." ".t($t_base.'fields/pricing_based_on_distance_from_the_CFS')."</strong>";
					}
					?>
					</p>-->
					<div id="haulage_model_link_<?=$haulagePricingDataArrs['idHaulageModel']?>" style="text-align:right;"></div>
					<div id="haulage_model_<?=$haulagePricingDataArrs['idHaulageModel']?>"></div>
					<?php
			}
	}	
}
function showZoneDetail($haulagePricingZoneDetailDataArr,$idDirection,$szWareHouseName,$zonename,$idHaulageModel,$idWarehouse,$wmfactor)
{
	$t_base="HaulaugePricing/";
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/from');
		$text_title=$zonename." ".t($t_base.'fields/to')." ".$szWareHouseName;
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/to');
		$text_title=$szWareHouseName." ".t($t_base.'fields/to')." ".$zonename;
	}
	
	$kHaulagePricing= new cHaulagePricing();
	$arrZone[]=$kHaulagePricing->loadZone($idHaulagePricingModel);
	$fWmFactor=$arrZone[0]['fWmFactor'];
	
?>
	<p style="margin-bottom:2px;">
	<strong style="font-size:14px;"><?=t($t_base.'title/princing_for')?> <?=$direction?> <?=t($t_base.'title/haulage');?> <?=t($t_base.'fields/from')?> <?=$text_title?></strong> <?  if(!isset($_SESSION['admin_id'])){?><a href="<?__FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/<?=$idWarehouse?>/<?=$idHaulageModel?>/"><?=t($t_base.'links/i_would_like_to_update_this_in_bulk')?></a><? } ?>
	</p>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_zone_detail">
		<tr>
			<td width="20%" style="text-align:left;width:20%"><strong><?=t($t_base.'fields/up_to')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/upto_tool_tip_1');?>','<?=t($t_base.'messages/upto_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_100_kg')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/per_100kg_tool_tip_1');?>','<?=t($t_base.'messages/per_100kg_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/minimum')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/minium_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_booking')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/per_booking_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/currency')?></strong> </td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/fuel_surcharge')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fuel_surcharge_ttool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
		</tr>
		<?php
			if(!empty($haulagePricingZoneDetailDataArr))
			{
				$k=0;
				foreach($haulagePricingZoneDetailDataArr as $haulagePricingZoneDetailDataArrs)
				{
					if((float)$haulagePricingZoneDetailDataArrs['fWmFactor']>0)
					{
						$factorvalue=number_format(($haulagePricingZoneDetailDataArrs['iUpToKg']/$haulagePricingZoneDetailDataArrs['fWmFactor']),1);
					}
				?>
					<tr id="haulage_pricing_zone_data_<?=++$k?>" onclick="sel_haulage_zone_detail_data('haulage_pricing_zone_data_<?=$k?>','<?=$haulagePricingZoneDetailDataArrs['id']?>','<?=$idHaulageModel?>','<?=$haulagePricingZoneDetailDataArrs['iUpToKg']?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$haulagePricingZoneDetailDataArrs['idHaulagePricingModel']?>','<?=$haulagePricingZoneDetailDataArrs['fWmFactor']?>')">
						<td><?=number_format((float)$haulagePricingZoneDetailDataArrs['iUpToKg'])?> <?=t($t_base.'fields/kg')?> (<?=$factorvalue?> <?=t($t_base.'fields/cbm')?>)</td>
						<td><?=number_format((float)$haulagePricingZoneDetailDataArrs['fPricePer100Kg'],2)?></td>
						<td><?=number_format((float)$haulagePricingZoneDetailDataArrs['fMinimumPrice'],2)?></td>
						<td><?=number_format((float)$haulagePricingZoneDetailDataArrs['fPricePerBooking'],2)?></td>
						<td><?=$haulagePricingZoneDetailDataArrs['szCurrency']?></td>
						<td><?=number_format((float)$haulagePricingZoneDetailDataArrs['fFuelPercentage'],1)?> % <?=t($t_base.'fields/on_top');?></td>
					</tr>
			<?	}
			}else
			{  
		?>
			<tr>
				<td colspan="6" align="center"><?=t($t_base.'messages/no_pricing_for_zone');?></td>
			</tr>
		<? }?>
		</table>
		<br/>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td width="45%"></td>
		<td width="55%" align="right"><a href="javascript:void(0)" id="delete_zone_detail" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/delete_line')?></span></a> <a href="javascript:void(0)" id="zone_edit_detail" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit')?></span></a></td>
		</tr>
		</table>
		
		<div id="zone_edit_form">
			<?=zone_pricing_form(array(),$wmfactor)?>
		</div>                                                    
<?php
}

function zone_pricing_form($editDataArr=array(),$wmfactor)
{

$t_base_error="Error";
$t_base = "HaulaugePricing/";
$Excel_export_import=new cExport_Import();
$kHaulagePricing = new cHaulagePricing();
$currencyArr=$Excel_export_import->getFreightCurrency('',true);
if(!empty($_POST['zonePricingArr']))
{
	if($_POST['zonePricingArr']['mode']=='edit')
	{
		if($kHaulagePricing->updateZonePricingLine($_POST['zonePricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_zone_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['zonePricingArr']['wmfactor']?>');
			</script>
			
		<?
			exit();
		}
	}
	
	if($_POST['zonePricingArr']['mode']=='add')
	{
		if($kHaulagePricing->addZonePricingLine($_POST['zonePricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_zone_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['zonePricingArr']['wmfactor']?>');
			</script>
			
		<?
			exit();
		}
	}
}

if(empty($_POST['zonePricingArr']))
{
	if(!empty($editDataArr))
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
	else
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
}
else
{
	$wmfactorCbm=(__WEIGHT_UP_TO__/$_POST['zonePricingArr']['wmfactor']);
	$weight=__WEIGHT_UP_TO__;
}
if(!empty($kHaulagePricing->arErrorMessages)){
?>
<br/>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kHaulagePricing->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php
}

$fMinimumPrice='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fMinimumPrice']!='')
	{
		$fMinimumPrice=round($editDataArr[0]['fMinimumPrice'],2);
	}
}
else if(!empty($_POST['zonePricingArr']))
{
	if($_POST['zonePricingArr']['fMinimumPrice']!='')
	{
		$fMinimumPrice=$_POST['zonePricingArr']['fMinimumPrice'];
	}
}
$fPricePerBooking='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePerBooking']!='')
	{
		$fPricePerBooking=round($editDataArr[0]['fPricePerBooking'],2);
	}
}
else if(!empty($_POST['zonePricingArr']))
{
	if($_POST['zonePricingArr']['fPricePerBooking']!='')
	{
		$fPricePerBooking=$_POST['zonePricingArr']['fPricePerBooking'];
	}
}

$fPricePer100Kg='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=round($editDataArr[0]['fPricePer100Kg'],2);
	}
}
else if(!empty($_POST['zonePricingArr']))
{
	if($_POST['zonePricingArr']['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=$_POST['zonePricingArr']['fPricePer100Kg'];
	}
}
?>
	<form name="updatePricingZoneData" id="updatePricingZoneData" method="post">
		<!--  <p><?=t($t_base.'fields/update_haulage_rate_line');?></p>-->	
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="pricingzonedata">
			<tr>
			<td><?=t($t_base.'fields/weight_up_to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/weight_up_to_tool_tip_heading');?>','<?=t($t_base.'messages/weight_up_to_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<th align="left" colspan="4" height="48"><?=t($t_base.'fields/Kg');?><br/><input  size="10" style="width: 100px;"  type="text" name="zonePricingArr[iUpToKg]" id="iUpToKg" value="<?=(($_POST['zonePricingArr']['iUpToKg'])?$_POST['zonePricingArr']['iUpToKg']:$editDataArr[0]['iUpToKg'])?>"  size="12" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
			</tr>
			<tr>
				<td width="40%"><?=t($t_base.'fields/haulage_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_rate_tool_tip_heading');?>','<?=t($t_base.'messages/haulage_rate_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<th width="15%" align="left" ><? echo "Per ".$weight." ".t($t_base.'fields/kg')?><br/><input type="text" size="10" name="zonePricingArr[fPricePer100Kg]" id="fPricePer100Kg" value="<?=$fPricePer100Kg?>" size="12" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/minimum');?><br/><input type="text"  size="10" name="zonePricingArr[fMinimumPrice]" id="fMinimumPrice" value="<?=$fMinimumPrice?>" size="12" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/per_booking');?><br/><input type="text"  size="10" name="zonePricingArr[fPricePerBooking]" id="fPricePerBooking" value="<?=$fPricePerBooking?>" size="12" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/currency');?><br/><select size="1" name="zonePricingArr[idCurrency]" id="idCurrency"  size="25" onchange="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');">
						<option value=" "><?=t($t_base.'fields/select');?></option>
						<? if(!empty($currencyArr))
							{
								foreach($currencyArr as $currencyArrs)
								{?>
									<option value="<?=$currencyArrs['id']?>" <?=((($_POST['zonePricingArr']['idCurrency'])?$_POST['zonePricingArr']['idCurrency']:$editDataArr[0]['idCurrency']) ==  $currencyArrs['id'] ) ? "selected":""?>><?=$currencyArrs['szCurrency']?></option>
								<?}
									
							}
						?>
					</select></th>
			</tr>
			<tr><td style="height:10px;line-height:10px;" colspan="5"></td></tr>
			<tr>
				<td colspan="3" align="right">
				<input type="hidden" name="zonePricingArr[mode]" id="mode" value="<?=(($_POST['zonePricingArr']['mode'])?$_POST['zonePricingArr']['mode']:"add")?>">
				<input type="hidden" name="zonePricingArr[idEdit]" id="idEdit" value="<?=(($_POST['zonePricingArr']['idEdit'])?$_POST['zonePricingArr']['idEdit']:$editDataArr[0]['id'])?>">
				<input type="hidden" name="zonePricingArr[idHaulagePricingModel]" id="idHaulagePricingModel" value="<?=(($_POST['zonePricingArr']['idHaulagePricingModel'])?$_POST['zonePricingArr']['idHaulagePricingModel']:$_POST['idHaulagePricingModel'])?>">
				<input type="hidden" name="zonePricingArr[wmfactor]" id="wmfactor" value="<?=(($_POST['zonePricingArr']['wmfactor'])?$_POST['zonePricingArr']['wmfactor']:$wmfactor)?>">
				Click to update changes</td>
				<td align="right" colspan="2">
				<a href="javascript:void(0)" id="cancel_zone_detail_line" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="save_zone_detail_line" class="button1" style="opacity:0.4"><span style="min-width:70px;"><?=t($t_base.'fields/save');?></span></a></td>
			</tr>			
			</table>			
		</form>
<?
if(!empty($kHaulagePricing->arErrorMessages)){
	if($_POST['zonePricingArr']['mode']=='edit'){?>
<script>

$("#cancel_zone_detail_line").unbind("click");
$("#cancel_zone_detail_line").attr('style',"opacity:1");
$("#cancel_zone_detail_line").click(function(){cancel_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['iUpToKg1']?>')});


$("#save_zone_detail_line").unbind("click");
$("#save_zone_detail_line").attr('style',"opacity:1");
$("#save_zone_detail_line").click(function(){save_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
	
</script>
<?php }else if($_POST['zonePricingArr']['mode']=='add'){?>
<script>
$("#save_zone_detail_line").unbind("click");
$("#save_zone_detail_line").attr('style',"opacity:1");
$("#save_zone_detail_line").click(function(){add_haulage_pricing_line('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
</script>
<?php }}
}
function checkForwarderStatus($idForwarder)
{
		$idForwarder 		=	(int)sanitize_all_html_input($idForwarder);
		
		$kForwarder 		=	new cForwarder();	
		
		$kForwarder->load($idForwarder);
		
		$logo =$kForwarder->szLogo;
		
		$checkComapnyProfile = $kForwarder->checkForwarderComleteCompanyInfomation($idForwarder);
		
		$checkComapnyEmails  = $kForwarder->checkBillingBookingCustomer($idForwarder);
		
		$checkTnc			 = $kForwarder->checkAgreeTNC($idForwarder);
		
		$checkCFS			 = $kForwarder->checkCFS($idForwarder);
		
		$checkPricingWareh	 = $kForwarder->checkWarehouses($idForwarder);
		
		if($checkComapnyProfile || $checkComapnyEmails  || $checkTnc || (!$checkCFS) || (!$checkPricingWareh) || ($logo=='')  )
		{	
		
			return false;
		
		}
		else
		{
		
			return true;	
		
		}
}

function update_zone_form($arrZone=array(),$idDirection,$idWarehouse,$idHaulageModel,$id=0,$mode,$iPriorityLevel=0)
{
$t_base_error="Error";
$t_base = "HaulaugePricing/";
$Excel_export_import=new cExport_Import();
$kHaulagePricing = new cHaulagePricing();
$kWHSSearch = new cWHSSearch();
$kWHSSearch->load($idWarehouse); 
$iWarehouseType = $kWHSSearch->iWarehouseType;

if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
{ 
    $szCfsName = t($t_base.'messages/airport_warehouse'); 
}
else
{
    $szCfsName = t($t_base.'messages/cfs'); 
}
$maxPoints=$kWHSSearch->getManageMentVariableByDescription('__MAX_NUMBER_POINT_FOR_ZONE__');
if($idDirection=='1')
{
    $direction=t($t_base.'fields/Export');
    $formto=t($t_base.'fields/to');
    $tofrom=t($t_base.'fields/from');
    $transt_time_text=t($t_base.'messages/this_zone_to')." ".$szCfsName;
}
else
{	
    $direction=t($t_base.'fields/Import');
    $formto=t($t_base.'fields/from');
    $tofrom=t($t_base.'fields/to');
    $transt_time_text=t($t_base.'messages/the')." ".t($t_base.'messages/cfs')." ".$szCfsName;
}
$haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);

	if(!empty($_POST['zoneArr']))
	{
		if($_POST['zoneArr']['mode']=='edit')
		{
                    if($kHaulagePricing->updateZone($_POST['zoneArr']))
                    {?>
                        <script type="text/javascript"> 
                            updateHaulageZonesCountries();
                            $("#loader").attr('style','display:block;');
                            open_pricing_model_detail('<?=$_POST['zoneArr']['idHaulageModel']?>','<?=$_POST['zoneArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$_POST['zoneArr']['iPriorityLevel']?>','<?=$_POST['zoneArr']['id']?>','<?=$idDirection?>','<?=$_POST['zoneArr']['fWmFactor']?>');
                            $("#callvalue").attr('value','1');	 
                        </script>	
                    <?php	
                    exit();		
                    }
		}
		
		if($_POST['zoneArr']['mode']=='add')
		{
			$idPricing=$kHaulagePricing->addZone($_POST['zoneArr']);
			if((int)$idPricing>0)
			{
				$data['idDirection']=$idDirection;
				$data['idWarehouse']=$_POST['zoneArr']['idWarehouse'];
				$data['idHaulageModel']=$_POST['zoneArr']['idHaulageModel'];
				$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
				$haulageZoneNewArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'zone');
				$position=getAddedCityPosition($haulageZoneNewArr,$_POST['zoneArr']['szName']);
				if(!empty($haulagePricingModelArr))
				{
					foreach($haulagePricingModelArr as $haulagePricingModelArrs)
					{
						if($haulagePricingModelArrs['idHaulageModel']=='1')
						{
							$count=$haulagePricingModelArrs['iCount'];		
							if((int)$haulagePricingModelArrs['iCount']>0)
							{
								if((int)$haulagePricingModelArrs['iCount']==1)
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/zone_defined');
								else
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/zones_defined');	
							}
							else
								$text=t($t_base.'fields/no_zones_defined');
						}
							
					}
				}
				else
				{
					$text=t($t_base.'fields/no_zones_defined');
				}
				$priority_active=$kHaulagePricing->getPriorityCFSHaulageModel($_POST['zoneArr']['idWarehouse'],$_POST['zoneArr']['idHaulageModel'],$idDirection);
				if((int)$priority_active>0)
				{
					$priority_active_text=$priority_active." ".t($t_base.'fields/priority');
				}
				else
				{
					$priority_active_text=t($t_base.'fields/not_active');
				}
				?>
				<script type="text/javascript">
                                    updateHaulageZonesCountries();
				$("#loader").attr('style','display:block;');
				$("#zone_popup_add_edit").html(''); 
				var openHaulageModelMapped=$("#openHaulageModelMapped").attr('value');
				change_haulage_model_status_model_add(openHaulageModelMapped,'A','<?=$kWHSSearch->idForwarder?>','<?=$priority_active?>','<?=$_POST['zoneArr']['idHaulageModel']?>','<?=$_POST['zoneArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$count?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['zoneArr']['fWmFactor']?>');
				//open_pricing_model_detail('<?=$_POST['zoneArr']['idHaulageModel']?>','<?=$_POST['zoneArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$count?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['zoneArr']['fWmFactor']?>');
				var divid="change_data_value_<?=$_POST['zoneArr']['idHaulageModel']?>";
				$("#"+divid).html('<?=$text?>');
				$("#callvalue").attr('value','1');	
                                
				</script>	
			<?php	
			exit();				
			}
		}
		
	}
	
	if(empty($arrZone) && empty($_POST['zoneArr']))
	{
		$arrZone[0]['fFuelPercentage']=__DEFAULT_FUEL__;
		$arrZone[0]['fWmFactor']=__DEFAULT_WM_FACTOR__;
	}
	
	if(!empty($arrZone))
	{
		$fFuelPercentageArr=explode(".",$arrZone[0]['fFuelPercentage']);
		if((int)$fFuelPercentageArr[1]>0)
		{
			$l=strlen($fFuelPercentageArr[1]);
			if($l==1)
			{
				$arrZone[0]['fFuelPercentage']=round($arrZone[0]['fFuelPercentage'],1);
			}
			else
			{
				$arrZone[0]['fFuelPercentage']=round($arrZone[0]['fFuelPercentage'],2);
			}
			
		}
		else
		{
			$arrZone[0]['fFuelPercentage']=$fFuelPercentageArr[0];
		}
	}
	if(!empty($kHaulagePricing->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?
	}
	else{
	?>

	<form name="zoneAddEditForm" id="zoneAddEditForm" method="post">
		<ol style="margin:0;padding:0 0 0 20px">
				<li><?=t($t_base.'title/give_the_zone_a_name');?><br /><input type="text" style="width:215px;" name="zoneArr[szName]" id="szName" value="<?=(($_POST['zoneArr']['szName'])?$_POST['zoneArr']['szName']:$arrZone[0]['szName'])?>" maxlength="30" onblur="enable_add_edit_button_zone('<?=$mode?>')"></li><br/>
				<li><?=t($t_base.'title/click_the_map_to_define_the_boarders_of_the_zone');?> (<?=t($t_base.'title/maximum');?> <?=$maxPoints?> <?=t($t_base.'title/points');?>) </li><br/>
				<li><?=t($t_base.'title/select_maximum_expected_transit_time_from');?> <?=$transt_time_text?> <br/>
				<select size="1" name="zoneArr[idTransitTime]" id="idTransitTime" onchange="enable_add_edit_button_zone('<?=$mode?>');">
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php if(!empty($haulageTransitTimeArr))
					{
						foreach($haulageTransitTimeArr as $haulageTransitTimeArrs)
						{
								$iHours='';
								if($haulageTransitTimeArrs['iHours']>24)
								{
									$iHours="< ".($haulageTransitTimeArrs['iHours']/24)." days";
								}
								else
								{
									$iHours="< ".$haulageTransitTimeArrs['iHours']." hours";
								}
							?>
							<option value="<?=$haulageTransitTimeArrs['id']?>" <?=((($_POST['zoneArr']['idTransitTime'])?$_POST['zoneArr']['idTransitTime']:$arrZone[0]['idHaulageTransitTime']) ==  $haulageTransitTimeArrs['id'] ) ? "selected":""?>><?=$iHours?></option>
						<?}
							
					}
				?>
			</select>
				</li><br/>
				<li><?=t($t_base.'title/type_the_weight_measure_factor_you_want_to_use_for_pricing');?> <?=$tofrom?> <?=t($t_base.'title/this_zone');?><br/><input type="text" style="width:70px" name="zoneArr[fWmFactor]" id="fWmFactor" value="<?=(($_POST['zoneArr']['fWmFactor'])?$_POST['zoneArr']['fWmFactor']:round($arrZone[0]['fWmFactor']))?>"> <em style="font-size:14px;"><?=t($t_base.'fields/kg')?>/<?=t($t_base.'fields/cbm')?></em></li><br/>
				<li><?=t($t_base.'title/If_you_would_like_to_add_fuel_surcharge_on_top_of_the_price_for_this_zone_type_the_percentage');?><br/><input type="text" style="width:70px" name="zoneArr[fFuelPercentage]" id="fFuelPercentage" value="<?=(((float)$_POST['zoneArr']['fFuelPercentage'])?(float)$_POST['zoneArr']['fFuelPercentage']:$arrZone[0]['fFuelPercentage'])?>"> <em style="font-size:14px;">% <?=t($t_base.'fields/on_top_of_price');?></em></li><br/>
			</ol>
			<input type="hidden" value="<?=(($_POST['zoneArr']['mode'])?$_POST['zoneArr']['mode']:$mode)?>" id="mode" name="zoneArr[mode]">
			<input type="hidden" value="<?=(($_POST['zoneArr']['szLatLongPipeline1'])?$_POST['zoneArr']['szLatLongPipeline1']:"")?>" id="szLatLongPipeline1" name="zoneArr[szLatLongPipeline1]">
			<input type="hidden" value="<?=(($_POST['zoneArr']['idDirection'])?$_POST['zoneArr']['idDirection']:$idDirection)?>" id="idDirection" name="zoneArr[idDirection]">
			<input type="hidden" value="<?=(($_POST['zoneArr']['idWarehouse'])?$_POST['zoneArr']['idWarehouse']:$idWarehouse)?>" id="idWarehouse" name="zoneArr[idWarehouse]">
			<input type="hidden" value="<?=(($_POST['zoneArr']['idHaulageModel'])?$_POST['zoneArr']['idHaulageModel']:$idHaulageModel)?>" id="idHaulageModel" name="zoneArr[idHaulageModel]">
			<input type="hidden" value="<?=(($_POST['zoneArr']['id'])?$_POST['zoneArr']['id']:$id)?>" id="id" name="zoneArr[id]">
			<input type="hidden" value="<?=(($_POST['zoneArr']['iPriorityLevel'])?$_POST['zoneArr']['iPriorityLevel']:$iPriorityLevel)?>" id="iPriorityLevel" name="zoneArr[iPriorityLevel]">
                        <div class="btn-container clearfix" style="margin: 95px 0px 12px 239px;position: relative;text-align: center;width: 315px;"><a href="javascript:void(0)" id="cancel_zone" class="button2" onclick="cancel_zone('<?=$idHaulageModel?>')"><span><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="save_zone" class="button1" style="opacity:0.4"><span id="change_text"><?=t($t_base.'fields/add_zone');?></span></a></div>
	</form>
	<?php
	if(!empty($kHaulagePricing->arErrorMessages) && $_POST['zoneArr']['mode']=='edit'){
	?>
	<script>
		$("#save_zone").attr("onclick","");
		$("#save_zone").unbind("click");
		$("#save_zone").click(function(){save_zone_data()});
		$("#save_zone").attr('style',"opacity:1");
		$("#change_text").html('<?=t($t_base.'fields/save_zone');?>');
	</script>
	<?php
	}}
}

function findPaymentFreq($paymentFreq)
{	
    $paymentFreq = (int)sanitize_all_html_input($paymentFreq);
    if( $paymentFreq>0 )
    {
        $kForwarderNew = new cForwarder();
        $paymentFrequency = $kForwarderNew->getAllPaymentFrequency(); 
        
        $iNumMonth = $paymentFrequency[$paymentFreq]['iNumMonth'];
        if($iNumMonth>0)
        {
            $month = date('F Y', strtotime('+'.(int)$iNumMonth.' months'));
            return date('j F Y', strtotime("first monday". $month)); 
        }
        else
        {
            $month = date('F Y', strtotime('+1 months'));
            return date('j F Y', strtotime("first monday". $month));
        }
        /*
        if($paymentFreq ==__FORWARDER_PAYMENT_FREQUENCY_BY_MONTHLY__) //Every 2 months
        {
            $month = date('F Y', strtotime('+2 months'));
            return date('j F Y', strtotime("first monday". $month)); 
        }
        else if( $paymentFreq ==__FORWARDER_PAYMENT_FREQUENCY_MONTHLY__ ) //Monthly
        {
            $month = date('F Y', strtotime('+1 months'));
            return date('j F Y', strtotime("first monday". $month));
        }
         * 
         */
    }
}

function copy_zone_popup($idForwarder,$idWarehouse,$idDirection,$idHaulageModel,$id)
{
$t_base = "HaulaugePricing/";
$kHaulagePricing = new cHaulagePricing();
$ret_ary=$kHaulagePricing->getActiveWarehouseZone($idForwarder,$idWarehouse,$idHaulageModel);
?>
	<h5 style="margin-bottom:6px;"><?=t($t_base.'title/select_zones_to_copy');?></h5>
	<p style="text-align:justify"><?=t($t_base.'messages/zone_heading_line_1');?></p>
			<br/>
			<form name="copyZone" id="copyZone" method="post">
				<div class="oh haulage-get-data">
					<div class="fl-25">
						<span class="f-size-12"><?=t($t_base.'fields/country');?></span>
						<select name="dataExportZoneArr[haulageCountryZone]" id="haulageCountryZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'country','city_warehouse_zone','<?=$idHaulageModel?>')">
							<option value=""><?=t($t_base.'fields/select');?></option>		
							<?php
										if(!empty($ret_ary))
										{
											foreach($ret_ary as $ret_arys)
											{
												?>
													<option value="<?=$ret_arys['id']?>"><?=$ret_arys['szCountryName']?></option>
													<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div id='city_warehouse_zone'>
						<div class="fl-25">
						<span class="f-size-12"><?=t($t_base.'fields/city');?></span>
						<select name="dataExportZoneArr[szCityZone]" disabled id="szCityZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'city','warehouse_zone','<?=$idHaulageModel?>')">
									<option value=""><?=t($t_base.'fields/all');?></option>
									<?php
										if(!empty($ret_ary))
										{
											foreach($ret_ary as $ret_arys)
											{
												?>
												<option value="<?=$ret_arys['szCity']?>"><?=$ret_arys['szCity']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div class="fl-25" id="warehouse_zone">
						<span class="f-size-12">Name</span>
						<select name="dataExportZoneArr[haulageWarehouseZone]" disabled id="haulageWarehouseZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value="">Select</option>
							<?php
								if(!empty($ret_ary))
								{
									foreach($ret_ary as $ret_arys)
									{
										?>
										<option value="<?=$ret_arys['warehouseId']?>"><?=$ret_arys['szWareHouseName']?></option>
										<?
									}
								}
							?>
						</select>
					</div>
					<div class="fl-25 direction">
						<span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
						<select name="dataExportZoneArr[idDirectionZone]" disabled id="idDirectionZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value='1'><?=t($t_base.'fields/export')?></option>
							<option value='2'><?=t($t_base.'fields/import')?></option>
						</select>
					</div>
					</div>
				</div>
				<br/>
				<div id="show_copy_data" class="tableScroll" style="height:119px;overflow: auto;"></div>
				<input type="hidden" value="<?=(($_POST['dataExportZoneArr']['idZoneDirection'])?$_POST['dataExportZoneArr']['idZoneDirection']:$idDirection)?>" id="idZoneDirection" name="dataExportZoneArr[idZoneDirection]">
				<input type="hidden" value="<?=(($_POST['dataExportZoneArr']['idZoneWarehouse'])?$_POST['dataExportZoneArr']['idZoneWarehouse']:$idWarehouse)?>" id="idZoneWarehouse" name="dataExportZoneArr[idZoneWarehouse]">
				<input type="hidden" value="<?=(($_POST['dataExportZoneArr']['idZoneHaulageModel'])?$_POST['dataExportZoneArr']['idZoneHaulageModel']:$idHaulageModel)?>" id="idZoneHaulageModel" name="dataExportZoneArr[idZoneHaulageModel]">
				<input type="hidden" value="<?=(($_POST['dataExportZoneArr']['idZone'])?$_POST['dataExportZoneArr']['idZone']:$id)?>" id="idZone" name="dataExportZoneArr[idZone]">
				<input type="hidden" value="<?=(($_POST['dataExportZoneArr']['iCheckedCount'])?$_POST['dataExportZoneArr']['iCheckedCount']:"0")?>" id="iCheckedCount" name="dataExportZoneArr[iCheckedCount]">
				<br/>
				<p align="center"><a href="javascript:void(0)" id="cancel_zone" class="button2" onclick="cancel_zone('<?=$idHaulageModel?>')"><span><?=t($t_base.'fields/cancel');?></span></a><a href="javascript:void(0)" id="copy_zone" class="button1" style="opacity:0.4"><span><?=t($t_base.'fields/copy');?></span></a></p>
				
			</form>	
<?	
}

function zone_data_list($haulagePricingZoneDataArr)
{
	$t_base = "HaulaugePricing/";
	$t_base_error="Error";
	$kHaulagePricing = new cHaulagePricing();
	if(!empty($_POST['dataExportZoneArr']))
	{
		if($kHaulagePricing->copyZoneData($_POST['dataExportZoneArr']))
		{
		
				$data['idDirection']=$_POST['dataExportZoneArr']['idZoneDirection'];
				$data['idWarehouse']=$_POST['dataExportZoneArr']['idZoneWarehouse'];
				$data['idHaulageModel']=$_POST['dataExportZoneArr']['idZoneHaulageModel'];
				$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
				if(!empty($haulagePricingModelArr))
				{
					foreach($haulagePricingModelArr as $haulagePricingModelArrs)
					{
						if($haulagePricingModelArrs['idHaulageModel']=='1')
						{
							if((int)$haulagePricingModelArrs['iCount']>0)
							{
								if((int)$haulagePricingModelArrs['iCount']==1)
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/zone_defined');
								else
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/zones_defined');	
							}
							else
								$text=t($t_base.'fields/no_zones_defined');
						}
					}
				}
				else
				{
					$text=t($t_base.'fields/no_zones_defined');
				}
		?>
			<script type="text/javascript">
				$("#zone_detail_delete_"+<?=$_POST['dataExportZoneArr']['idZoneHaulageModel']?>).attr('style','display:none;');
				$("#loader").attr('style','display:block;');
				
				open_pricing_model_detail('<?=$_POST['dataExportZoneArr']['idZoneHaulageModel']?>','<?=$_POST['dataExportZoneArr']['idZoneWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>');
				var divid="change_data_value_<?=$_POST['dataExportZoneArr']['idZoneHaulageModel']?>";
				$("#"+divid).html('<?=$text?>');
				</script>	
		<?		
		}
	}

if(!empty($kHaulagePricing->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?
	}
	
	?>
	<table cellspacing="0" cellpadding="0" border="0" width="100%"  class="format-2" style="position:relative;top:-1px;">
	<?
	if(!empty($haulagePricingZoneDataArr))
	{
		foreach($haulagePricingZoneDataArr as $haulagePricingZoneDataArrs)
		{?>
			<tr>
				<td class="noborder" width="54%"><?=$haulagePricingZoneDataArrs['szName']?></td>
				<td class="checkbox" width="23%"><input type="checkbox" name="dataExportZoneArr[szCopyZone][]" id="szCopyZone_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>" <? if(!empty($_POST['dataExportZoneArr']['szCopyZone']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportZoneArr']['szCopyZone'])) {?> checked <? }?>  onclick="enable_copy_pricing('<?=$haulagePricingZoneDataArrs['id']?>','zone');"> <?=t($t_base.'fields/zone_data')?></td>
				<td class="checkbox" width="23%"><input type="checkbox" name="dataExportZoneArr[szCopyZonePricing][]" id="szCopyZonePricing_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>"  <? if(!empty($_POST['dataExportZoneArr']['szCopyZonePricing']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportZoneArr']['szCopyZonePricing'])) {?> checked <? } if(empty($_POST['dataExportZoneArr']['szCopyZone'])){?> disabled <? }?> > <?=t($t_base.'fields/pricing_data')?></td>
			</tr>
		<?		
		}
	}
	else
	{?>
		<tr>
			<td colspan="3" align="center"><?=t($t_base.'fields/no_record_found_zone');?></td>
		</tr>
		<?
	}
	?></table>
	<?php
} 

function showPostCodeDetail($haulagePricingPostCodeDetailDataArr,$idDirection,$szWareHouseName,$postcodeset,$idHaulageModel,$idWarehouse,$wmfactor)
{
	$t_base = "HaulaugePricing/";
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/from');
		$text=$postcodeset." ".t($t_base.'fields/to')." ".$szWareHouseName;
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/from');
		$text=$szWareHouseName." ".t($t_base.'fields/to')." ".$postcodeset;
	}
	
	$kHaulagePricing= new cHaulagePricing();
	$arrPostCode[]=$kHaulagePricing->loadPostCode($idHaulagePricingModel);
	$fWmFactor=$arrPostCode[0]['fWmFactor'];
	
?>
	<p style="margin-bottom: 2px;">
            <strong style="font-size:14px;"><?=t($t_base.'title/princing_for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$formto?> <?=$text?></strong> <?  if(!isset($_SESSION['admin_id'])){?><a href="<?__FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/<?=$idWarehouse?>/<?=$idHaulageModel?>/"><?=t($t_base.'links/i_would_like_to_update_this_in_bulk')?></a><? } ?>
	</p>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_postcode_detail">
		<tr>
			<td style="text-align:left;width:20%"><strong><?=t($t_base.'fields/up_to')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/upto_tool_tip_1');?>','<?=t($t_base.'messages/upto_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_100_kg')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/per_100kg_tool_tip_1');?>','<?=t($t_base.'messages/per_100kg_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/minimum')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/minium_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_booking')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/per_booking_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/currency')?></strong> </td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/fuel_surcharge')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fuel_surcharge_ttool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
		</tr>
		<?php
			if(!empty($haulagePricingPostCodeDetailDataArr))
			{
				$k=0;
				foreach($haulagePricingPostCodeDetailDataArr as $haulagePricingPostCodeDetailDataArrs)
				{
					if((float)$haulagePricingPostCodeDetailDataArrs['fWmFactor']>0)
					{
						$factorvalue=number_format(($haulagePricingPostCodeDetailDataArrs['iUpToKg']/$haulagePricingPostCodeDetailDataArrs['fWmFactor']),1);
					}
				?>
					<tr id="haulage_pricing_postcode_data_<?=++$k?>" onclick="sel_haulage_postcode_detail_data('haulage_pricing_postcode_data_<?=$k?>','<?=$haulagePricingPostCodeDetailDataArrs['id']?>','<?=$idHaulageModel?>','<?=$haulagePricingPostCodeDetailDataArrs['iUpToKg']?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$haulagePricingPostCodeDetailDataArrs['idHaulagePricingModel']?>')">
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['iUpToKg'])?> <?=t($t_base.'fields/kg');?> (<?=$factorvalue?> <?=t($t_base.'fields/cbm');?>)</td>
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['fPricePer100Kg'],2)?></td>
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['fMinimumPrice'],2)?></td>
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['fPricePerBooking'],2)?></td>
						<td><?=$haulagePricingPostCodeDetailDataArrs['szCurrency']?></td>
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['fFuelPercentage'],1)?> % <?=t($t_base.'fields/on_top');?></td>
					</tr>
			<?	}
			}else
			{  
		?>
			<tr>
				<td colspan="6" align="center"><?=t($t_base.'messages/no_pricing_for_postcode');?></td>
			</tr>
		<? }?>
		</table>
		<br/>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td width="45%"></td>
		<td width="55%" align="right"><a href="javascript:void(0)" id="delete_postcode_detail" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/delete_line')?></span></a> <a href="javascript:void(0)" id="postcode_edit_detail" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit')?></span></a></td>
		</tr>
		</table>
		
		
		<div id="postcode_edit_form">
			<?=postcode_pricing_form(array(),$wmfactor)?>
		</div>                                                    
<?php
}

function postcode_pricing_form($editDataArr=array(),$wmfactor)
{
//print_r($_POST);
$t_base_error="Error";
$t_base = "HaulaugePricing/";
$Excel_export_import=new cExport_Import();
$kHaulagePricing = new cHaulagePricing();
$currencyArr=$Excel_export_import->getFreightCurrency('',true);
if(!empty($_POST['postcodePricingArr']))
{
	if($_POST['postcodePricingArr']['mode']=='edit')
	{
		if($kHaulagePricing->updateZonePricingLine($_POST['postcodePricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_postcode_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');
			</script>
			
		<?
			exit();
		}
	}
	
	if($_POST['postcodePricingArr']['mode']=='add')
	{
		if($kHaulagePricing->addZonePricingLine($_POST['postcodePricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_postcode_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');
			</script>
			
		<?php
			exit();
		}
	}
}
if(!empty($kHaulagePricing->arErrorMessages)){
?>
<br/>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kHaulagePricing->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php
}
if(empty($_POST['cityPricingArr']))
{
	if(!empty($editDataArr))
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
	else
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
}
else
{
	$wmfactorCbm=(__WEIGHT_UP_TO__/$_POST['cityPricingArr']['wmfactor']);
	$weight=__WEIGHT_UP_TO__;
}
$fMinimumPrice='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fMinimumPrice']!='')
	{
		$fMinimumPrice=round($editDataArr[0]['fMinimumPrice'],2);
	}
}
else if(!empty($_POST['postcodePricingArr']))
{
	if($_POST['postcodePricingArr']['fMinimumPrice']!='')
	{
		$fMinimumPrice=$_POST['postcodePricingArr']['fMinimumPrice'];
	}
}

$fPricePerBooking='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePerBooking']!='')
	{
		$fPricePerBooking=round($editDataArr[0]['fPricePerBooking'],2);
	}
}
else if(!empty($_POST['postcodePricingArr']))
{
	if($_POST['postcodePricingArr']['fPricePerBooking']!='')
	{
		$fPricePerBooking=$_POST['postcodePricingArr']['fPricePerBooking'];
	}
}

$fPricePer100Kg='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=round($editDataArr[0]['fPricePer100Kg'],2);
	}
}
else if(!empty($_POST['postcodePricingArr']))
{
	if($_POST['postcodePricingArr']['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=$_POST['postcodePricingArr']['fPricePer100Kg'];
	}
}
?>
	<form name="updatePricingPostCodeData" id="updatePricingPostCodeData" method="post">
		<!--  <p><?=t($t_base.'fields/update_haulage_rate_line');?></p>	-->
			<table cellspacing="0" cellpadding="0" border="0" width="100%"  class="pricingzonedata">
			<tr>
                            <td><?=t($t_base.'fields/weight_up_to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/weight_up_to_tool_tip_heading');?>','<?=t($t_base.'messages/weight_up_to_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                            <th align="left" height="48"><?=t($t_base.'fields/Kg');?><br/><input type="text" name="postcodePricingArr[iUpToKg]" id="iUpToKg" value="<?=(($_POST['postcodePricingArr']['iUpToKg'])?$_POST['postcodePricingArr']['iUpToKg']:$editDataArr[0]['iUpToKg'])?>"  size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
			</tr>
			<tr>
				<td width="40%"><?=t($t_base.'fields/haulage_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_rate_tool_tip_heading');?>','<?=t($t_base.'messages/haulage_rate_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<th width="15%" align="left" ><? echo "Per ".$weight." ".t($t_base.'fields/kg')?><br/><input type="text" name="postcodePricingArr[fPricePer100Kg]" id="fPricePer100Kg" value="<?=$fPricePer100Kg?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/minimum');?><br/><input type="text" name="postcodePricingArr[fMinimumPrice]" id="fMinimumPrice"  value="<?=$fMinimumPrice?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/per_booking');?><br/><input type="text" name="postcodePricingArr[fPricePerBooking]" id="fPricePerBooking" value="<?=$fPricePerBooking?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/currency');?><br/><select  size="1" name="postcodePricingArr[idCurrency]" id="idCurrency"  size="25" onchange="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');">
						<option value=" "><?=t($t_base.'fields/select');?></option>
						<? if(!empty($currencyArr))
							{
								foreach($currencyArr as $currencyArrs)
								{?>
									<option value="<?=$currencyArrs['id']?>" <?=((($_POST['postcodePricingArr']['idCurrency'])?$_POST['postcodePricingArr']['idCurrency']:$editDataArr[0]['idCurrency']) ==  $currencyArrs['id'] ) ? "selected":""?>><?=$currencyArrs['szCurrency']?></option>
								<?}
									
							}
						?>
					</select></th>
			</tr>
			<tr><td colspan="5" style="height:10px;line-height:10px;"></td></tr>
			<tr>
				<td align="right" colspan="3">
				<input type="hidden" name="postcodePricingArr[mode]" id="mode" value="<?=(($_POST['postcodePricingArr']['mode'])?$_POST['postcodePricingArr']['mode']:"add")?>">
				<input type="hidden" name="postcodePricingArr[idEdit]" id="idEdit" value="<?=(($_POST['postcodePricingArr']['idEdit'])?$_POST['postcodePricingArr']['idEdit']:$editDataArr[0]['id'])?>">
				<input type="hidden" name="postcodePricingArr[idHaulagePricingModel]" id="idHaulagePricingModel" value="<?=(($_POST['postcodePricingArr']['idHaulagePricingModel'])?$_POST['postcodePricingArr']['idHaulagePricingModel']:$_REQUEST['idHaulagePricingModel'])?>">
				<input type="hidden" name="postcodePricingArr[wmfactor]" id="wmfactor" value="<?=(($_POST['postcodePricingArr']['wmfactor'])?$_POST['postcodePricingArr']['wmfactor']:$wmfactor)?>">
				Click to update changes</td>
				<td align="right" colspan="2">
				<a href="javascript:void(0)" id="cancel_postcode_detail_line" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="save_postcode_detail_line" class="button1" style="opacity:0.4"><span style="min-width:70px;"><?=t($t_base.'fields/save');?></span></a></td>
			</tr>
			</table> 			
		</form>
<?php
if(!empty($kHaulagePricing->arErrorMessages)){
	if($_POST['postcodePricingArr']['mode']=='edit'){?>
<script>

$("#cancel_postcode_detail_line").unbind("click");
$("#cancel_postcode_detail_line").attr('style',"opacity:1");
$("#cancel_postcode_detail_line").click(function(){cancel_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['iUpToKg1']?>')});


$("#save_postcode_detail_line").unbind("click");
$("#save_postcode_detail_line").attr('style',"opacity:1");
$("#save_postcode_detail_line").click(function(){save_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
	
</script>
<?php }else if($_POST['postcodePricingArr']['mode']=='add'){?>
<script>
$("#save_postcode_detail_line").unbind("click");
$("#save_postcode_detail_line").attr('style',"opacity:1");
$("#save_postcode_detail_line").click(function(){add_haulage_pricing_line('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
</script>
<?php }}
}


function showCityDetail($haulagePricingCityDetailDataArr,$idDirection,$szWareHouseName,$postcodeset,$idHaulageModel,$idWarehouse,$idHaulagePricingModel,$wmfactor)
{
	
	$t_base = "HaulaugePricing/";
	if($idDirection=='1')
	{
		$direction=t($t_base.'fields/Export');
		$formto=t($t_base.'fields/from');
		$text=t($t_base.'fields/from')." ".$postcodeset." ".t($t_base.'fields/to')." ".$szWareHouseName;
	}
	else
	{
		$direction=t($t_base.'fields/Import');
		$formto=t($t_base.'fields/to');
		$text=t($t_base.'fields/from')." ".$szWareHouseName." ".t($t_base.'fields/to')." ".$postcodeset;
	}
	$kHaulagePricing= new cHaulagePricing();
	$arrCity[]=$kHaulagePricing->loadCity($idHaulagePricingModel);
	$fWmFactor=$arrCity[0]['fWmFactor'];
	
?>
	<p style="margin-bottom: 2px;">
            <strong style="font-size: 14px;"><?=t($t_base.'title/princing_for')?> <?=$direction?> <?=t($t_base.'title/haulage')?> <?=$text?></strong> <?  if(!isset($_SESSION['admin_id'])){?><a href="<?__FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/<?=$idWarehouse?>/<?=$idHaulageModel?>/"><?=t($t_base.'links/i_would_like_to_update_this_in_bulk')?></a><? } ?>
	</p>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_city_detail">
		<tr>
			<td style="text-align:left;width:20%"><strong><?=t($t_base.'fields/up_to')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/upto_tool_tip_1');?>','<?=t($t_base.'messages/upto_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_100_kg')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/per_100kg_tool_tip_1');?>','<?=t($t_base.'messages/per_100kg_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/minimum')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/minium_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_booking')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/per_booking_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/currency')?></strong> </td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/fuel_surcharge')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fuel_surcharge_ttool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
		
		</tr>
		<?
			if(!empty($haulagePricingCityDetailDataArr))
			{
				$k=0;
				foreach($haulagePricingCityDetailDataArr as $haulagePricingCityDetailDataArrs)
				{
					if((float)$haulagePricingCityDetailDataArrs['fWmFactor']>0)
					{
						$factorvalue=number_format(($haulagePricingCityDetailDataArrs['iUpToKg']/$haulagePricingCityDetailDataArrs['fWmFactor']),1);
					}
				?>
					<tr id="haulage_pricing_city_data_<?=++$k?>" onclick="sel_haulage_city_detail_data('haulage_pricing_city_data_<?=$k?>','<?=$haulagePricingCityDetailDataArrs['id']?>','<?=$idHaulageModel?>','<?=$haulagePricingCityDetailDataArrs['iUpToKg']?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$haulagePricingCityDetailDataArrs['idHaulagePricingModel']?>','<?=$haulagePricingCityDetailDataArrs['fWmFactor']?>')">
						<td><?=number_format((float)$haulagePricingCityDetailDataArrs['iUpToKg'])?> <?=t($t_base.'fields/kg')?> (<?=$factorvalue?> <?=t($t_base.'fields/cbm')?>)</td>
						<td><?=number_format((float)$haulagePricingCityDetailDataArrs['fPricePer100Kg'],2)?></td>
						<td><?=number_format((float)$haulagePricingCityDetailDataArrs['fMinimumPrice'],2)?></td>
						<td><?=number_format((float)$haulagePricingCityDetailDataArrs['fPricePerBooking'],2)?></td>
						<td><?=$haulagePricingCityDetailDataArrs['szCurrency']?></td>
						<td><?=number_format((float)$haulagePricingCityDetailDataArrs['fFuelPercentage'],1)?> % <?=t($t_base.'fields/on_top')?></td>
					</tr>
			<?	}
			}else
			{  
		?>
			<tr>
				<td colspan="6" align="center"><?=t($t_base.'messages/no_pricing_for_city');?></td>
			</tr>
		<? }?>
		</table>
		<br/>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td width="45%"></td>
		<td width="55%" align="right"><a href="javascript:void(0)" id="delete_city_detail" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/delete_line')?></span></a> <a href="javascript:void(0)" id="city_edit_detail" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit')?></span></a></td>
		</tr>
		</table>
		<div id="city_edit_form">
			<?=city_pricing_form(array(),$fWmFactor)?>
		</div>                                                    
<?php
}

function city_pricing_form($editDataArr=array(),$wmfactor)
{

$t_base_error="Error";
$t_base = "HaulaugePricing/";
$Excel_export_import=new cExport_Import();
$kHaulagePricing = new cHaulagePricing();
$currencyArr=$Excel_export_import->getFreightCurrency('',true);
if(!empty($_POST['cityPricingArr']))
{
	if($_POST['cityPricingArr']['mode']=='edit')
	{
		if($kHaulagePricing->updateZonePricingLine($_POST['cityPricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_city_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');
			</script>
			
		<?
			exit();
		}
	}
	
	if($_POST['cityPricingArr']['mode']=='add')
	{
		if($kHaulagePricing->addZonePricingLine($_POST['cityPricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_city_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');
			</script>
			
		<?
			exit();
		}
	}
}
if(empty($_POST['cityPricingArr']))
{
	if(!empty($editDataArr))
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
	else
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
}
else
{
	$wmfactorCbm=(__WEIGHT_UP_TO__/$_POST['cityPricingArr']['wmfactor']);
	$weight=__WEIGHT_UP_TO__;
}
if(!empty($kHaulagePricing->arErrorMessages)){
?>
<br/>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kHaulagePricing->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?
}
$fMinimumPrice='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fMinimumPrice']!='')
	{
		$fMinimumPrice=round($editDataArr[0]['fMinimumPrice'],2);
	}
}
else if(!empty($_POST['cityPricingArr']))
{
	if($_POST['cityPricingArr']['fMinimumPrice']!='')
	{
		$fMinimumPrice=$_POST['cityPricingArr']['fMinimumPrice'];
	}
}
$fPricePerBooking='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePerBooking']!='')
	{
		$fPricePerBooking=round($editDataArr[0]['fPricePerBooking'],2);
	}
}
else if(!empty($_POST['cityPricingArr']))
{
	if($_POST['cityPricingArr']['fPricePerBooking']!='')
	{
		$fPricePerBooking=$_POST['cityPricingArr']['fPricePerBooking'];
	}
}

$fPricePer100Kg='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=round($editDataArr[0]['fPricePer100Kg'],2);
	}
}
else if(!empty($_POST['cityPricingArr']))
{
	if($_POST['cityPricingArr']['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=$_POST['cityPricingArr']['fPricePer100Kg'];
	}
}
?>
	<form name="updatePricingCityData" id="updatePricingCityData" method="post">
		<!-- <p><?=t($t_base.'fields/update_haulage_rate_line');?></p>-->	
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="pricingzonedata">
			<tr>
			<td><?=t($t_base.'fields/weight_up_to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/weight_up_to_tool_tip_heading');?>','<?=t($t_base.'messages/weight_up_to_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<th align="left" height="48" colspan="4"><?=t($t_base.'fields/Kg');?><br/><input type="text" name="cityPricingArr[iUpToKg]" id="iUpToKg" style="width:100px;" value="<?=(($_POST['cityPricingArr']['iUpToKg'])?$_POST['cityPricingArr']['iUpToKg']:$editDataArr[0]['iUpToKg'])?>"  size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
			</tr>
			<tr>
				<td width="40%"><?=t($t_base.'fields/haulage_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_rate_tool_tip_heading');?>','<?=t($t_base.'messages/haulage_rate_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<th width="15%" align="left" ><?php echo "Per ".$weight." ".t($t_base.'fields/kg')?><br/><input type="text" name="cityPricingArr[fPricePer100Kg]" id="fPricePer100Kg" value="<?=$fPricePer100Kg?>" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/minimum');?><br/><input type="text" name="cityPricingArr[fMinimumPrice]" id="fMinimumPrice" value="<?=$fMinimumPrice?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/per_booking');?><br/><input type="text" name="cityPricingArr[fPricePerBooking]" id="fPricePerBooking" value="<?=$fPricePerBooking?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/currency');?><br/><select size="1" name="cityPricingArr[idCurrency]" id="idCurrency"  size="25" onchange="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');">
						<option value=" "><?=t($t_base.'fields/select');?></option>
						<?php if(!empty($currencyArr))
							{
								foreach($currencyArr as $currencyArrs)
								{?>
									<option value="<?=$currencyArrs['id']?>" <?=((($_POST['cityPricingArr']['idCurrency'])?$_POST['cityPricingArr']['idCurrency']:$editDataArr[0]['idCurrency']) ==  $currencyArrs['id'] ) ? "selected":""?>><?=$currencyArrs['szCurrency']?></option>
								<?}
									
							}
						?>
					</select></th>
			</tr>
			<tr><td colspan="5" style="height:10px;line-height:10px;"></td></tr>
			<tr>
				<td align="right" colspan="3">
				<input type="hidden" name="cityPricingArr[mode]" id="mode" value="<?=(($_POST['cityPricingArr']['mode'])?$_POST['cityPricingArr']['mode']:"add")?>">
				<input type="hidden" name="cityPricingArr[idEdit]" id="idEdit" value="<?=(($_POST['cityPricingArr']['idEdit'])?$_POST['cityPricingArr']['idEdit']:$editDataArr[0]['id'])?>">
				<input type="hidden" name="cityPricingArr[idHaulagePricingModel]" id="idHaulagePricingModel" value="<?=(($_POST['cityPricingArr']['idHaulagePricingModel'])?$_POST['cityPricingArr']['idHaulagePricingModel']:$_POST['idHaulagePricingModel'])?>">
				<input type="hidden" name="cityPricingArr[wmfactor]" id="wmfactor" value="<?=(($_POST['cityPricingArr']['wmfactor'])?$_POST['cityPricingArr']['wmfactor']:$wmfactor)?>">
				Click to update changes</td>
				<td align="right" colspan="2">
				<a href="javascript:void(0)" id="cancel_city_detail_line" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/cancel')?></span></a> <a href="javascript:void(0)" id="save_city_detail_line" class="button1" style="opacity:0.4;"><span style="min-width:70px;"><?=t($t_base.'fields/save')?></span></a>
				</td>
			</tr>
			</table>  			
		</form>
<?php
if(!empty($kHaulagePricing->arErrorMessages)){
	if($_POST['cityPricingArr']['mode']=='edit'){?>
<script>

$("#cancel_city_detail_line").unbind("click");
$("#cancel_city_detail_line").attr('style',"opacity:1");
$("#cancel_city_detail_line").click(function(){cancel_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['iUpToKg1']?>')});


$("#save_city_detail_line").unbind("click");
$("#save_city_detail_line").attr('style',"opacity:1");
$("#save_city_detail_line").click(function(){save_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
	
</script>
<?php }else if($_POST['cityPricingArr']['mode']=='add'){?>
<script>
$("#save_city_detail_line").unbind("click");
$("#save_city_detail_line").attr('style',"opacity:1");
$("#save_city_detail_line").click(function(){add_haulage_pricing_line('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
</script>
<? }}
}

function getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS)
{
	$t_base = "HaulaugePricing/";
	?>
	<table>
	    	 <tr>
	    	 <td width="450px">
	    	 <h4><b><?=t($t_base.'title/countries_serviced_by_this_cfs');?></b></h4>
	    	 <p style="width: 300px;margin: 5px;"><?=t($t_base.'title/add_the_local_market_where_normally');?>.</p>
	    	
	    	
	    	 <select style="width: 300px;height: 100px;margin-bottom:5px;margin: 5px;" multiple="multiple" name="cfsAddEditArr[selectNearCFS]">
						<?php
						if(!empty($allCountriesArr))
						{
							foreach($allCountriesArr as $allCountriesArrs)
							{
								if(in_array($allCountriesArrs['id'],$selectListCFS))
								{
									?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['cfsAddEditArr']['szCountry'])?$_POST['cfsAddEditArr']['szCountry']:$kWHSSearch->idCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						}
					?>	
	    	 </select>
	    	 <a class="button2"><span><?=t($t_base.'fields/remove');?></span></a>
	    	 <p style="margin: 5px;"><?=t($t_base.'title/add_from_this_list_sorted_in_order');?></p>
	    	 <select id="addFormList" style="width: 300px;height: 100px;margin-bottom:5px;margin: 5px;" multiple="multiple" name="cfsAddEditArr[selectNearCFS]">
	    	 <?
	    	 	if($countryList!=array())
	    	 	{
	    	 		foreach($countryList as $key=>$value)
	    	 		{
	    	 			if(!in_array($value['id'],$selectListCFS))
						{
	    	 				echo "<option value ='".$value['id']."'>".$value['szCountryName']."</option>";
						}
	    	 		}
	    	 	}
	    	 ?>
	    	 </select><a class="button1" onclick="addCfsCountries('addFormList')"><span><?=t($t_base.'fields/add');?></span></a>
	    	</td><td style="padding-top: 170px;">
	    	 	<div style="background-color: #b6dde8;padding: 5px;">
	    	 		<h4><b><?=t($t_base.'title/example');?></b></h4>
	    	 		<p><?=t($t_base.'title/example_city_1');?>.</p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'title/example_city_2');?>.</p>
	    	 	</div>
	    	 </td></tr>
	     </table>
	
	<?php
	
}

function showDistanceDetail($haulagePricingPostCodeDetailDataArr,$idDirection,$szWareHouseName,$iDistanceUpToKm,$idHaulageModel,$idWarehouse,$fWmFactor)
{
    $t_base = "HaulaugePricing/";
    if($idDirection=='1')
    {
        $direction=t($t_base.'fields/Export');
        $formto=t($t_base.'fields/pick_up');
    }
    else
    {
        $direction=t($t_base.'fields/Import');
        $formto=t($t_base.'fields/delivery');
    } 
?>
    <p style="margin-bottom: 2px;">
	<strong style="font-size:14px;"><?=t($t_base.'title/princing_for')?> <?=$formto?> <?=t($t_base.'title/between')?> <?=number_format($iDistanceUpToKm)?> <?=t($t_base.'fields/km')?> <?=t($t_base.'fields/of')?> <?=$szWareHouseName?></strong> <?  if(!isset($_SESSION['admin_id'])){?><a href="<?__FORWARDER_HOME_PAGE_URL__?>/HaulageBulk/<?=$idWarehouse?>/<?=$idHaulageModel?>/"><?=t($t_base.'links/i_would_like_to_update_this_in_bulk')?></a><? } ?>
    </p>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_distance_detail">
		<tr>
			<td style="text-align:left;width:20%"><strong><?=t($t_base.'fields/up_to')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/upto_tool_tip_1');?>','<?=t($t_base.'messages/upto_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_100_kg')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/per_100kg_tool_tip_1');?>','<?=t($t_base.'messages/per_100kg_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/minimum')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/minium_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_booking')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/per_booking_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/currency')?></strong> </td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/fuel_surcharge')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fuel_surcharge_ttool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
		
		</tr>
		<?php
			if(!empty($haulagePricingPostCodeDetailDataArr))
			{
				$k=0;
				foreach($haulagePricingPostCodeDetailDataArr as $haulagePricingPostCodeDetailDataArrs)
				{
					if((float)$haulagePricingPostCodeDetailDataArrs['fWmFactor']>0)
					{
						$factorvalue=number_format(($haulagePricingPostCodeDetailDataArrs['iUpToKg']/$haulagePricingPostCodeDetailDataArrs['fWmFactor']),1);
					}
				?>
					<tr id="haulage_pricing_distance_data_<?=++$k?>" onclick="sel_haulage_distance_detail_data('haulage_pricing_distance_data_<?=$k?>','<?=$haulagePricingPostCodeDetailDataArrs['id']?>','<?=$idHaulageModel?>','<?=$haulagePricingPostCodeDetailDataArrs['iUpToKg']?>','<?=$idWarehouse?>','<?=$idDirection?>','<?=$haulagePricingPostCodeDetailDataArrs['idHaulagePricingModel']?>')">
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['iUpToKg'])?> <?=t($t_base.'fields/kg')?> (<?=$factorvalue?> <?=t($t_base.'fields/cbm')?>)</td>
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['fPricePer100Kg'],2)?></td>
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['fMinimumPrice'],2)?></td>
						<td><?=number_format((float)$haulagePricingPostCodeDetailDataArrs['fPricePerBooking'],2)?></td>
						<td><?=$haulagePricingPostCodeDetailDataArrs['szCurrency']?></td>
						<td><?=$haulagePricingPostCodeDetailDataArrs['fFuelPercentage']?> % <?=t($t_base.'fields/on_top')?></td>
					</tr>
			<?php	}}
			else
			{  
		?>
			<tr>
				<td colspan="6" align="center"><?=t($t_base.'messages/no_pricing_for_distance');?></td>
			</tr>
		<?php } ?>
		</table>
		<br/>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td width="45%"></td>
		<td width="55%" align="right"><a href="javascript:void(0)" id="delete_distance_detail" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/delete_line')?></span></a> <a href="javascript:void(0)" id="distance_edit_detail" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit')?></span></a></td>
		</tr>
		</table>
		
		<div id="distance_edit_form">
			<?=distance_pricing_form(array(),$fWmFactor)?>
		</div>                                                    
<?php
}


function distance_pricing_form($editDataArr=array(),$wmfactor)
{

$t_base_error="Error";
$t_base = "HaulaugePricing/";
$Excel_export_import=new cExport_Import();
$kHaulagePricing = new cHaulagePricing();
$currencyArr=$Excel_export_import->getFreightCurrency('',true);
if(!empty($_POST['distancePricingArr']))
{
	if($_POST['distancePricingArr']['mode']=='edit')
	{
		if($kHaulagePricing->updateZonePricingLine($_POST['distancePricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_distance_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');
			</script>
			
		<?php
			exit();
		}
	}
	
	if($_POST['distancePricingArr']['mode']=='add')
	{
		if($kHaulagePricing->addZonePricingLine($_POST['distancePricingArr']))
		{?>
			<script type="text/javascript">
			$("#loader").attr('style','display:block;');
			save_haulage_distance_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');
			</script>
			
		<?php
			exit();
		}
	}
}
if(!empty($kHaulagePricing->arErrorMessages)){
?>
<br/>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kHaulagePricing->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php
}
                    
if(empty($_POST['distancePricingArr']))
{
	if(!empty($editDataArr))
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
	else
	{
		$wmfactorCbm=(__WEIGHT_UP_TO__/$wmfactor);
		$weight=__WEIGHT_UP_TO__;
	}
}
else
{
	$wmfactorCbm=(__WEIGHT_UP_TO__/$_POST['distancePricingArr']['wmfactor']);
	$weight=__WEIGHT_UP_TO__;
}
$fMinimumPrice='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fMinimumPrice']!='')
	{
		$fMinimumPrice=round($editDataArr[0]['fMinimumPrice'],2);
	}
}
else if(!empty($_POST['distancePricingArr']))
{
	if($_POST['distancePricingArr']['fMinimumPrice']!='')
	{
		$fMinimumPrice=$_POST['distancePricingArr']['fMinimumPrice'];
	}
}
$fPricePerBooking='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePerBooking']!='')
	{
		$fPricePerBooking=round($editDataArr[0]['fPricePerBooking'],2);
	}
}
else if(!empty($_POST['distancePricingArr']))
{
	if($_POST['distancePricingArr']['fPricePerBooking']!='')
	{
		$fPricePerBooking=$_POST['distancePricingArr']['fPricePerBooking'];
	}
}

$fPricePer100Kg='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=round($editDataArr[0]['fPricePer100Kg'],2);
	}
}
else if(!empty($_POST['distancePricingArr']))
{
    if($_POST['distancePricingArr']['fPricePer100Kg']!='')
    {
        $fPricePer100Kg=$_POST['distancePricingArr']['fPricePer100Kg'];
    }
}
?>
	<form name="updatePricingDistanceData" id="updatePricingDistanceData" method="post">
            <strong><?=t($t_base.'fields/update_haulage_rate_line');?></strong>	
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="pricingzonedata">
			<tr>
			<td><?=t($t_base.'fields/weight_up_to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/weight_up_to_tool_tip_heading');?>','<?=t($t_base.'messages/weight_up_to_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                        <th align="left" colspan="4" height="48"><?=t($t_base.'fields/Kg');?><br/><input type="text" style="width:100px;" name="distancePricingArr[iUpToKg]" id="iUpToKg" value="<?=(($_POST['distancePricingArr']['iUpToKg'])?$_POST['distancePricingArr']['iUpToKg']:$editDataArr[0]['iUpToKg'])?>"  size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
			</tr>
			<tr>
				<td width="40%"><?=t($t_base.'fields/haulage_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_rate_tool_tip_heading');?>','<?=t($t_base.'messages/haulage_rate_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<th width="15%" align="left" ><? echo "Per ".$weight." ".t($t_base.'fields/kg')?><br/><input type="text" name="distancePricingArr[fPricePer100Kg]" id="fPricePer100Kg" value="<?=$fPricePer100Kg?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/minimum');?><br/><input type="text" name="distancePricingArr[fMinimumPrice]" id="fMinimumPrice" value="<?=$fMinimumPrice?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/per_booking');?><br/><input type="text" name="distancePricingArr[fPricePerBooking]" id="fPricePerBooking" value="<?=$fPricePerBooking?>" size="10" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/currency');?><br/><select size="1" name="distancePricingArr[idCurrency]" id="idCurrency"  size="25" onchange="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');">
						<option value=" "><?=t($t_base.'fields/select');?></option>
						<?php if(!empty($currencyArr))
							{
								foreach($currencyArr as $currencyArrs)
								{?>
									<option value="<?=$currencyArrs['id']?>" <?=((($_POST['distancePricingArr']['idCurrency'])?$_POST['distancePricingArr']['idCurrency']:$editDataArr[0]['idCurrency']) ==  $currencyArrs['id'] ) ? "selected":""?>><?=$currencyArrs['szCurrency']?></option>
								<?}
									
							}
						?>
					</select></th>
			</tr>
			<tr><td colspan="5" style="height:10px;line-height:10px;"></td></tr>
			<tr>
				<td align="right" colspan="3">
				<input type="hidden" name="distancePricingArr[mode]" id="mode" value="<?=(($_POST['distancePricingArr']['mode'])?$_POST['distancePricingArr']['mode']:"add")?>">
				<input type="hidden" name="distancePricingArr[idEdit]" id="idEdit" value="<?=(($_POST['distancePricingArr']['idEdit'])?$_POST['distancePricingArr']['idEdit']:$editDataArr[0]['id'])?>">
				<input type="hidden" name="distancePricingArr[idHaulagePricingModel]" id="idHaulagePricingModel" value="<?=(($_POST['distancePricingArr']['idHaulagePricingModel'])?$_POST['distancePricingArr']['idHaulagePricingModel']:$_REQUEST['idHaulagePricingModel'])?>">
				<input type="hidden" name="distancePricingArr[wmfactor]" id="wmfactor" value="<?=(($_POST['distancePricingArr']['wmfactor'])?$_POST['distancePricingArr']['wmfactor']:$wmfactor)?>">
				Click to update changes</td>
				<td align="right" colspan="2">
				<a href="javascript:void(0)" id="cancel_distance_detail_line" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="save_distance_detail_line" class="button1" style="opacity:0.4"><span style="min-width:70px;"><?=t($t_base.'fields/save');?></span></a></td>
			</tr>
			</table>  			
		</form>
<?
if(!empty($kHaulagePricing->arErrorMessages)){
	if($_POST['distancePricingArr']['mode']=='edit'){?>
<script>

$("#cancel_distance_detail_line").unbind("click");
$("#cancel_distance_detail_line").attr('style',"opacity:1");
$("#cancel_distance_detail_line").click(function(){cancel_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['iUpToKg1']?>')});


$("#save_distance_detail_line").unbind("click");
$("#save_distance_detail_line").attr('style',"opacity:1");
$("#save_distance_detail_line").click(function(){save_haulage_zone_line('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
	
</script>
<?php }else if($_POST['distancePricingArr']['mode']=='add'){?>
<script>
$("#save_distance_detail_line").unbind("click");
$("#save_distance_detail_line").attr('style',"opacity:1");
$("#save_distance_detail_line").click(function(){add_haulage_pricing_line('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
</script>
<?php }}
}

function update_distance_form($arrDistance=array(),$idDirection,$idWarehouse,$idHaulageModel,$id=0,$mode,$iPriorityLevel=0)
{
    $t_base_error="Error";
    $t_base = "HaulaugePricing/";
    $Excel_export_import=new cExport_Import();
    $kHaulagePricing = new cHaulagePricing();
    $kWHSSearch = new cWHSSearch();
    $kWHSSearch->load($idWarehouse);

    $iWarehouseType = $kWHSSearch->iWarehouseType;
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    { 
        $szCFSName = "airport warehouse"; 
        $szExampleTitle = t($t_base.'title/air_example_distance_1');
    }
    else
    {
        $szCFSName = "CFS"; 
        $szExampleTitle = t($t_base.'title/example_distance_1');
    }
    $maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE_FOR_COUNTRY_INCLUDED__');

    $kConfig = new cConfig();
    $allCountriesArr=$kHaulagePricing->getAllCountriesInDefinedDistance($maxDistance,$kWHSSearch->szLatitude,$kWHSSearch->szLongitude);

    $szDistanceArr=array();

    $allDistanceCountriesArr=$kHaulagePricing->getAllDistanceHaulageModel($idWarehouse,$idDirection,$idHaulageModel,$id);

    $szDistanceArr=$kHaulagePricing->getAllCountriesForDistanceHaulageModel($id);
    //print_r($szDistanceArr);
    if(!empty($szDistanceArr))
    {
	foreach($szDistanceArr as $szDistanceArrs)
	{
            $idCountriesArr[]=$szDistanceArrs['idCountry'];
            $szCountriesNameArr[$szDistanceArrs['idCountry']]=$szDistanceArrs['szCountryName'];
	}
	$idCountriesStr=implode(",",$idCountriesArr);
	$szCountriesNameStr=implode(",",$szCountriesNameArr); 
    }
    if(!empty($_POST['distanceArr']['szCountriesStr']))
    {
        $idCountriesStr=explode(",",$_POST['distanceArr']['szCountriesStr']);
        if(!empty($idCountriesStr))
        {
            foreach($idCountriesStr as $idCountriesStrs)
            {
                $szCountriesNameArr[$idCountriesStrs]=$kConfig->getCountryName($idCountriesStrs);
            }
        }
    }
    if($idDirection=='1')
    {
            $direction=t($t_base.'fields/Export');
            $formto=t($t_base.'fields/from');
            $tofrom=t($t_base.'fields/to');
            $transt_time_text=t($t_base.'messages/pick_up_within_this_distance_to_the_cfs')." ".$szCFSName;
    }
    else
    {
            $direction=t($t_base.'fields/Import');
            $formto=t($t_base.'fields/to');
            $tofrom=t($t_base.'fields/from');
            $transt_time_text= "the ".$szCFSName." ".t($t_base.'messages/the_cfs_for_delivery_within_this_distance');
    }
    $haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);

	//print_r($_POST['distanceArr']);
	if(!empty($_POST['distanceArr']))
	{
		if($_POST['distanceArr']['mode']=='edit')
		{
			if($kHaulagePricing->updateDistance($_POST['distanceArr']))
			{?>
				<script type="text/javascript">
				$("#loader").attr('style','display:block;');
				$("#update_distance_form_add_edit").html('');
				open_pricing_model_detail('<?=$_POST['distanceArr']['idHaulageModel']?>','<?=$_POST['distanceArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$_POST['distanceArr']['iPriorityLevel']?>','<?=$_POST['distanceArr']['id']?>','<?=$idDirection?>','<?=$_POST['distanceArr']['fWmFactor']?>');
				</script>	
			<?php	
			exit();		
			}
		}
		
		if($_POST['distanceArr']['mode']=='add')
		{
			$idPricing=$kHaulagePricing->addDistance($_POST['distanceArr']);
			if((int)$idPricing>0)
			{
			
				$data['idDirection']=$idDirection;
				$data['idWarehouse']=$_POST['distanceArr']['idWarehouse'];
				$data['idHaulageModel']=$_POST['distanceArr']['idHaulageModel'];
				$position=$kHaulagePricing->getPositonDistanceAdded($_POST['distanceArr']['iDistanceKm'],$data['idWarehouse'],$idDirection,$data['idHaulageModel']);
				$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
				
				if(!empty($haulagePricingModelArr))
				{
					foreach($haulagePricingModelArr as $haulagePricingModelArrs)
					{
						if($haulagePricingModelArrs['idHaulageModel']=='4')
						{
							if((int)$haulagePricingModelArrs['iCount']>0)
							{
								if((int)$haulagePricingModelArrs['iCount']==1)
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/distance_bracket_defined');
								else
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/distance_brackets_defined');
							}
							else
								$text=t($t_base.'fields/no_distance_brackets_defined');
						}
					}
				}
				else
				{
					$text=t($t_base.'fields/no_distance_brackets_defined');
				}
				$priority_active=$kHaulagePricing->getPriorityCFSHaulageModel($_POST['distanceArr']['idWarehouse'],$_POST['distanceArr']['idHaulageModel'],$idDirection);
				if((int)$priority_active>0)
				{
					$priority_active_text=$priority_active." ".t($t_base.'fields/priority');
				}
				else
				{
					$priority_active_text=t($t_base.'fields/not_active');
				}
				?>
				
				<script type="text/javascript">
				$("#loader").attr('style','display:block;');
				var openHaulageModelMapped=$("#openHaulageModelMapped").attr('value');
				change_haulage_model_status_model_add(openHaulageModelMapped,'A','<?=$idForwarder?>','<?=$priority_active?>','<?=$_POST['distanceArr']['idHaulageModel']?>','<?=$_POST['distanceArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$position?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['distanceArr']['fWmFactor']?>');
				//open_pricing_model_detail('<?=$_POST['distanceArr']['idHaulageModel']?>','<?=$_POST['distanceArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$position?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['distanceArr']['fWmFactor']?>');
				var divid="change_data_value_<?=$_POST['distanceArr']['idHaulageModel']?>";
				$("#"+divid).html('<?=$text?>');
				$("#callvalue").attr('value','1');
				</script>	
			<?php	
			exit();				
			}
		}
		
	}
	if(!empty($kHaulagePricing->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php
	}
	if(empty($arrDistance) && empty($_POST['distanceArr']))
	{
		$idCountryWarehouse=$kWHSSearch->idCountry;
	}
	if(empty($arrDistance) && empty($_POST['distanceArr']))
	{
		$arrDistance[0]['fFuelPercentage']=__DEFAULT_FUEL__;
		$arrDistance[0]['fWmFactor']=__DEFAULT_WM_FACTOR__;
		
	}
	
	if(!empty($arrDistance))
	{
		$fFuelPercentageArr=explode(".",$arrDistance[0]['fFuelPercentage']);
		if((int)$fFuelPercentageArr[1]>0)
		{
			$l=strlen($fFuelPercentageArr[1]);
			if($l==1)
			{
				$arrDistance[0]['fFuelPercentage']=round($arrDistance[0]['fFuelPercentage'],1);
			}
			else
			{
				$arrDistance[0]['fFuelPercentage']=round($arrDistance[0]['fFuelPercentage'],2);
			}
			
		}
		else
		{
			$arrDistance[0]['fFuelPercentage']=$fFuelPercentageArr[0];
		}
	}
	
	?>
	<script type="text/javascript">
	<?php
		if(!empty($mode) && $mode=='edit')
		{
	?>
		enable_add_edit_button_distance();
	<?php
		}
	?>
	</script>
		<form name="distanceAddEditForm" id="distanceAddEditForm" method="post">
			<style>
				.pop-field{overflow:hidden;margin:0 0 5px;}
			</style>
			<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:10px;">
				<tr>
					<td width="72%" style="line-height:19px;">
						<ol style="width:94%;">
							<li><?=t($t_base.'title/set_the_distance_which_the_pricing_is_applicable');?></li>
							<li style="padding-top:2px;margin-bottom:22px;">
								<p><?=t($t_base.'title/select_the_countries_where_this_pricing_is_applicable');?></p>
								<em style="font-size:12px;line-height:15px;margin: 4px 0 10px;display:block;">
								<?=t($t_base.'title/example');?><br/>
								<?=$szExampleTitle;?>
								</em>
								<em style="font-size:12px;line-height:15px;display:block;"><?=t($t_base.'title/if_you_already_defined_the_countries');?>:</em>
								<p align="center" style="margin-top:5px">
									<select size="1" name="distanceArr[iCopyDistanceCountries]" id="iCopyDistanceCountries" onchange="copy_button_distance_countries('<?=$mode?>');" style="width:100px;">
										<option value=""><?=t($t_base.'fields/select')?></option>
										<?php
											if(!empty($allDistanceCountriesArr))
											{
												foreach($allDistanceCountriesArr as $allDistanceCountriesArrs)
												{
													?><option value="<?=$allDistanceCountriesArrs['id']?>"><?=t($t_base.'fields/up_to')?> <?=number_format($allDistanceCountriesArrs['iDistanceUpToKm'])?> <?=t($t_base.'fields/km')?></option>
													<?php
												}
											}
										?>
									</select>&nbsp;&nbsp;
									<a href="javascript:void(0)" id="copy_country_str" class="button1" style="opacity:0.4;"><span style="min-width:60px;"><?=t($t_base.'fields/copy');?></span></a>		
								</p>
							</li>
							<li><?=t($t_base.'title/select_maximum_expected_transit_time_from');?> <?=$transt_time_text?></li>
							<li><?=t($t_base.'title/type_the_weight_measure_factor_you_want_to_use_for_pricing_within_this_distance');?></li>
							<li><?=t($t_base.'title/fuel_surcharge_distance');?></li>
						</ol>
					</td>
					<td width="27%" valign="top">
						<div class="pop-field"><input type="text" value="<?=(($_POST['distanceArr']['iDistanceKm'])?$_POST['distanceArr']['iDistanceKm']:$arrDistance[0]['iDistanceUpToKm'])?>" id="iDistanceKm" name="distanceArr[iDistanceKm]" style="width:80px" onblur="enable_add_edit_button_distance('<?=$mode?>')"> <em style="font-size:14px;"> <?=t($t_base.'fields/km')?> </em></div>
						<div class="pop-field" style="padding-top:18px;">
							<div id="list_countries_arr_remove" style="float:left;margin-top: 3px;">
								<select size="1" name="distanceArr[szCountry]" id="szCountry" style="width:140px">
									<?php
										if(!empty($allCountriesArr))
										{
											foreach($allCountriesArr as $allCountriesArrs)
											{
												//if((($idDirection=='1' && $allCountriesArrs['iActiveFromDropdown']!='0') || ($idDirection=='2' && $allCountriesArrs['iActiveToDropdown']!='0')) && !in_array($allCountriesArrs['id'],$idCountriesArr))
												//{
													?><option value="<?=$allCountriesArrs['id']?>" <? if($idCountryWarehouse==$allCountriesArrs['id']){?> selected <? }?> ><?=$allCountriesArrs['szCountryName']?></option>
													<?php
												//}
											}
										}
									?>
								</select>
							</div>
							<a href="javascript:void(0)" id="add_country_str" class="button1" style="float:right;" onclick="add_countries('<?=$mode?>');enable_add_edit_button_distance('<?=$mode?>');"><span style="min-width:60px;float:left;"><?=t($t_base.'fields/add')?></span></a>
							<div class="pop-field" id="list_countries_arr" style="float:left;width:140px;margin-top: 7px;">
								<select  style="width: 140px;height: 100px;margin-bottom:5px;" multiple="multiple" id="szCourtriesNameStr" name="distanceArr[szCourtriesNameStr]" onclick="delete_str_countries('<?=$mode?>')">
									<? if(!empty($szCountriesNameArr))
									{
										foreach($szCountriesNameArr as $key=>$szCountriesNameArrs)
										{
											?>
											<option value="<?=$key?>"><?=$szCountriesNameArrs?></option>
											<?
										}
									}?>
								</select>
							</div>
							<a href="javascript:void(0)" id="delete_country_str" class="button2" style="opacity:0.4; float:right;margin-top:70px;"><span style="min-width:60px;"><?=t($t_base.'fields/delete')?></span></a>
						</div>
						<div class="pop-field" style="padding-top: 7px;">
							<select size="1" name="distanceArr[idTransitTime]" id="idTransitTime" style="min-width:86px;width:140px;" onchange="enable_add_edit_button_distance('<?=$mode?>')">
								<option value=""><?=t($t_base.'fields/select');?></option>
								<? if(!empty($haulageTransitTimeArr))
									{
										foreach($haulageTransitTimeArr as $haulageTransitTimeArrs)
										{
												$iHours='';
												if($haulageTransitTimeArrs['iHours']>24)
												{
													$iHours="< ".($haulageTransitTimeArrs['iHours']/24)." days";
												}
												else
												{
													$iHours="< ".$haulageTransitTimeArrs['iHours']." hours";
												}
											?>
											<option value="<?=$haulageTransitTimeArrs['id']?>" <?=((($_POST['distanceArr']['idTransitTime'])?$_POST['distanceArr']['idTransitTime']:$arrDistance[0]['idHaulageTransitTime']) ==  $haulageTransitTimeArrs['id'] ) ? "selected":""?>><?=$iHours?></option>
										<?}
											
									}
								?>
							</select>
						</div>
						<div class="pop-field" style="padding-top: 18px;"><input type="text" value="<?=(($_POST['distanceArr']['fWmFactor'])?$_POST['distanceArr']['fWmFactor']:$arrDistance[0]['fWmFactor'])?>" id="mode" name="distanceArr[fWmFactor]" style="width:80px"> <em style="font-size:14px;"><?=t($t_base.'fields/kg');?>/<?=t($t_base.'fields/cbm');?></em></div>
						<div class="pop-field" style="padding-top: 5px;"><input type="text" value="<?=(($_POST['distanceArr']['fFuelPercentage'])?$_POST['distanceArr']['fFuelPercentage']:$arrDistance[0]['fFuelPercentage'])?>" id="mode" name="distanceArr[fFuelPercentage]" style="width:80px"> <em style="font-size:14px;">% <?=t($t_base.'fields/on_top_of_price');?></em></div>
					</td>
				</tr>
			</table>			
			<input type="hidden" value="<?=(($_POST['distanceArr']['szCountriesStr'])?$_POST['distanceArr']['szCountriesStr']:$idCountriesStr)?>" id="szCountriesStr" name="distanceArr[szCountriesStr]">
			<input type="hidden" value="<?=(($_POST['distanceArr']['mode'])?$_POST['distanceArr']['mode']:$mode)?>" id="mode" name="distanceArr[mode]">
			<input type="hidden" value="<?=(($_POST['distanceArr']['idDirection'])?$_POST['distanceArr']['idDirection']:$idDirection)?>" id="idDirection" name="distanceArr[idDirection]">
			<input type="hidden" value="<?=(($_POST['distanceArr']['idWarehouse'])?$_POST['distanceArr']['idWarehouse']:$idWarehouse)?>" id="idWarehouse" name="distanceArr[idWarehouse]">
			<input type="hidden" value="<?=(($_POST['distanceArr']['idHaulageModel'])?$_POST['distanceArr']['idHaulageModel']:$idHaulageModel)?>" id="idHaulageModel" name="distanceArr[idHaulageModel]">
			<input type="hidden" value="<?=(($_POST['distanceArr']['id'])?$_POST['distanceArr']['id']:$id)?>" id="id" name="distanceArr[id]">
			<input type="hidden" value="<?=(($_POST['distanceArr']['iPriorityLevel'])?$_POST['distanceArr']['iPriorityLevel']:$iPriorityLevel)?>" id="iPriorityLevel" name="distanceArr[iPriorityLevel]">
			<p align="center"><a href="javascript:void(0)" id="cancel_distance" class="button2" onclick="cancel_distance('<?=$idHaulageModel?>')"><span><?=t($t_base.'fields/cancel')?></span></a><a href="javascript:void(0)" id="save_distance" class="button1" style="opacity:0.4"><span id="change_text"><?=t($t_base.'fields/add_line')?></span></a></p>
		
	</form>
	<?
	if(!empty($kHaulagePricing->arErrorMessages) && $_POST['distanceArr']['mode']=='edit'){
	?>
	<script>
		$("#save_distance").attr("onclick","");
		$("#save_distance").unbind("click");
		$("#save_distance").click(function(){save_distance_data()});
		$("#change_text").html('Save Line');
		$("#save_distance").attr('style',"opacity:1");
	</script>
	<?php
	}
} 
function distance_data_list($haulagePricingZoneDataArr)
{
	$t_base = "HaulaugePricing/";
	$t_base_error="Error";
	$kHaulagePricing = new cHaulagePricing();
	//print_r($_POST['dataExportDistanceArr']);
	if(!empty($_POST['dataExportDistanceArr']))
	{
		if($kHaulagePricing->copyDistanceData($_POST['dataExportDistanceArr']))
		{
				$data['idDirection']=$_POST['dataExportDistanceArr']['idZoneDirection'];
				$data['idWarehouse']=$_POST['dataExportDistanceArr']['idZoneWarehouse'];
				$data['idHaulageModel']=$_POST['dataExportDistanceArr']['idZoneHaulageModel'];
				$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
				if(!empty($haulagePricingModelArr))
				{
					foreach($haulagePricingModelArr as $haulagePricingModelArrs)
					{
						if($haulagePricingModelArrs['idHaulageModel']=='4')
						{
							if((int)$haulagePricingModelArrs['iCount']>0)
							{
								if((int)$haulagePricingModelArrs['iCount']==1)
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/distance_bracket_defined');
								else
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/distance_brackets_defined');
							}
							else
								$text=t($t_base.'fields/no_distance_brackets_defined');
						}
					}
				}
				else
				{
					$text=t($t_base.'fields/no_distance_brackets_defined');
				}
		
		?>
			<script type="text/javascript">
				$("#zone_detail_delete_"+<?=$_POST['dataExportDistanceArr']['idZoneHaulageModel']?>).attr('style','display:none;');
				$("#loader").attr('style','display:block;');
				
				open_pricing_model_detail('<?=$_POST['dataExportDistanceArr']['idZoneHaulageModel']?>','<?=$_POST['dataExportDistanceArr']['idZoneWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>');
				var divid="change_data_value_<?=$_POST['dataExportDistanceArr']['idZoneHaulageModel']?>";
				$("#"+divid).html('<?=$text?>');
				</script>	
		<?		
		}
	}

if(!empty($kHaulagePricing->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php
	}
	
	?>
	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="format-2" style="position:relative;top:-1px;">
	<?php
	if(!empty($haulagePricingZoneDataArr))
	{
            foreach($haulagePricingZoneDataArr as $haulagePricingZoneDataArrs)
            {?>
                <tr>
                    <td class="noborder" width="54%"><?=t($t_base.'fields/up_to')?> <?=number_format($haulagePricingZoneDataArrs['iDistanceUpToKm'])?> <?=t($t_base.'fields/km')?></td>
                    <td class="checkbox" width="23%"><input type="checkbox" name="dataExportDistanceArr[szCopyZone][]" id="szCopyZone_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>" <? if(!empty($_POST['dataExportDistanceArr']['szCopyZone']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportDistanceArr']['szCopyZone'])) {?> checked <? }?>  onclick="enable_copy_pricing('<?=$haulagePricingZoneDataArrs['id']?>','distance');"> <?=t($t_base.'fields/distance_data')?></td>
                    <td class="checkbox" width="23%"><input type="checkbox" name="dataExportDistanceArr[szCopyZonePricing][]" id="szCopyZonePricing_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>"  <? if(!empty($_POST['dataExportDistanceArr']['szCopyZonePricing']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportDistanceArr']['szCopyZonePricing'])) {?> checked <? } if(empty($_POST['dataExportDistanceArr']['szCopyZone'])){?> disabled <? }?> > <?=t($t_base.'fields/pricing_data')?></td>
                </tr>
            <?php		
            }
	}
        else
	{?>
            <tr>
                <td class="noborder" colspan="3" align="center"><?=t($t_base.'fields/no_record_found_distance');?></td>
            </tr>
            <?php
	}
	?>
	</table>
	<?php
}
function returnLimitData($data,$limit)
{
    if(strlen($data)>$limit)
    {
        $szDisplayName=substr_replace($data,'..',$limit);
    }
    else
    {
        $szDisplayName=$data;
    }
    return $szDisplayName;
}
function try_it_out_haulage($idWarehouse,$idDirection)
{
    if($idWarehouse>0)
    {
        $kWHSSearch = new cWHSSearch();
        $kWHSSearch->load($idWarehouse);
        $szWhsLatitude = $kWHSSearch->szLatitude ;
        $szWhsLongitude = $kWHSSearch->szLongitude ;
        $iWarehouseType = $kWHSSearch->iWarehouseType;

        $t_base = "HaulaugePricing/";
        if($idDirection=='1')
        {
            $direction=t($t_base.'fields/Export');
            $formto=t($t_base.'fields/try_from');

            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                $image_path = __BASE_STORE_IMAGE_URL__.'/CargoFlowHaulageAirExport.png' ;
            }
            else
            {
                $image_path = __BASE_STORE_IMAGE_URL__.'/new-haulagePricingBulk-2.png' ;
            } 
            $szOriginWhsName = $kWHSSearch->szWareHouseName ;
            $szDestinationWhsName='';
            $zone_type = 'HAULAGE_PRICING_EXPORT';
        }
        else
        {
            $direction=t($t_base.'fields/Import');
            $fromto=t($t_base.'fields/try_to');

            if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
            {
                $image_path = __BASE_STORE_IMAGE_URL__.'/CargoFlowHaulageAirImport.png' ;
            }
            else
            {
                $image_path = __BASE_STORE_IMAGE_URL__.'/new-haulagePricingBulk-3.png' ;
            }
            
            $szDestinationWhsName = $kWHSSearch->szWareHouseName ;
            $szOriginWhsName = '';
            $zone_type = 'HAULAGE_PRICING_IMPORT';
        }
        $kConfig = new cConfig();
        $allCountriesArr=$kConfig->getAllCountries();

        // geting all weight measure 
        $weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

?>
		 
<script type="text/javascript">
    $().ready(function() {	 
            var autocomplete1,place ; 
            function initialize1() 
            {
                var input1 = document.getElementById('szLocation');
                var options = {types: ['regions']}; 
                var autocomplete1 = new google.maps.places.Autocomplete(input1); 
                $(".pac-container:last").attr("id", 'szLocationPacContainer');
                google.maps.event.addListener(autocomplete1, 'place_changed', function() 
                {    
                    var szOriginAddress_js = $("#szLocation").val();   
                    //checkFromAddressAdmin(szOriginAddress_js,'FROM_COUNTRY');
                });  

                $('#szLocation').on('paste focus keypress keyup', function (e){  
                    $('#szLocation').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szLocationPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks
                        }
                        else
                        {
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display'); 
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);
                                }
                            });
                        }
                        var key_code = e.keyCode || e.which; 
                        if (key_code == 9 || key_code == 13) {
                            prefillAddressOnTabPress('szLocation',pac_container_id);
                        }  
                    }); 
                }); 
            } 
            function prefillAddressOnTabPress(input_field_id,pac_container_id)
            {  
                $(".pac-container").each(function(pac){
                   var disp = $(this).css('display'); 
                   var contaier_id = $(this).attr('id');  

                   //console.log("Container: "+contaier_id + " pac id: "+pac_container_id); 
                    if(contaier_id==pac_container_id)
                    {
                        var spanObj = $(this).children(".pac-item:first" );
                        var firstResult = ""; 
                        spanObj.children("span").each(function(){
                            var spanText = $(this).text(); 
                            spanText = jQuery.trim(spanText);
                            //console.log("Span Text: "+spanText);
                            if(spanText!='')
                            {
                                if(firstResult=='')
                                {
                                    firstResult += spanText;
                                }
                                else
                                {
                                    firstResult += ", "+ spanText;
                                } 
                                if(jQuery.trim(input_field_id)=='szLocation')
                                {
                                    console.log("Mathed: "+firstResult);
                                    $("#szLocation").val(firstResult);
                                    $("#szLocation").select();
                                } 
                            }
                        });  
                    }
                }); 
            } 
            function clean_pac_container()
            { 
                $("div").each(function(index){
                    if($( this ).hasClass("pac-container"))
                    {
                        $( this ).remove();
                    }
                }); 
            }   
            initialize1();   
    });
</script>
            <hr/>
            <h4><strong><?=t($t_base.'title/try_it_out')?></strong></h4>
            <p><?=t($t_base.'title/test_your')?> <?=$direction?> <?=t($t_base.'title/haulage_pricing_for')?> <?=$kWHSSearch->szWareHouseName?> <?=t($t_base.'title/by_completing_details_below')?>.</p>
            <br />
            <form id="obo_haulage_tryit_out_form" name="obo_haulage_tryit_out_form" method="post">
                <table cellpadding="0" cellspacing="0" border="0" class="tryhaulagepricing" style="width: 100%;">
		<tr>
                    <td class="wd-25">Location</td>	
                    <td class="wd-7">&nbsp;</td> 
                    <td class="wd-15"><?=t($t_base.'fields/cargo_volume')?></td>
                    <td class="wd-23"><?=t($t_base.'fields/cargo_weight')?></td>
                    <td class="wd-30">&nbsp</td>
		</tr>
                <tr>
                    <td> 
                        <input type="text" value="<?=(($_POST['tryoutArr']['szLocation'])?$_POST['tryoutArr']['szLocation']:"")?>" id="szLocation" name="tryoutArr[szLocation]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
                        <input type="hidden" id="idDirection" value="<?=$idDirection?>" name="tryoutArr[idDirection]">
                        <input type="hidden" id="idWarehouse" value="<?=$idWarehouse?>" name="tryoutArr[idWarehouse]">	 
                        <input type="hidden" name="tryoutArr[szWhsLatitude]" id="szWhsLatitude" value="<?=$szWhsLatitude?>">
                        <input type="hidden" name="tryoutArr[szWhsLongitude]" id="szWhsLongitude" value="<?=$szWhsLongitude?>"> 
                        <input type="hidden" name="tryoutArr[szLatitude]" id="szLatitude" value="">
                        <input type="hidden" name="tryoutArr[szLongitude]" id="szLongitude" value="">
                        <input type="hidden" name="tryoutArr[iForwarderTryitoutFlag]" id="iForwarderTryitoutFlag" value="1">
                        
                    </td>
                    <td style="text-align: center;">
                        <a href="javascript:void(0);" onclick="open_google_help_popup()" style="font-size: 12px;"><?=t($t_base.'fields/Select');?><br/><?=t($t_base.'fields/on_map');?></a>
                    </td> 
                    <td>
                        <input class="wd-60" type="text" value="<?=(($_POST['tryoutArr']['fVolume'])?$_POST['tryoutArr']['fVolume']:"")?>" id="szCargoVolume" name="tryoutArr[fVolume]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
                        <span class="wds-30 font-14"><?=t($t_base.'fields/cbm')?></span>
                    </td>
                    <td>
                        <input class="wd-50" type="text" value="<?=(($_POST['tryoutArr']['fTotalWeight'])?$_POST['tryoutArr']['fTotalWeight']:"")?>" id="szCargoWieght" name="tryoutArr[fTotalWeight]" onkeyup="enable_add_edit_button_try_test(event);">  
                        <select size="1" name="tryoutArr[idWeightMeasure]" id="idWeightMeasure" class="wd-40">
                            <?php
                                if(!empty($weightMeasureAry))
                                {
                                    foreach($weightMeasureAry as $weightMeasureArys)
                                    {
                                        ?>
                                        <option value="<?=$weightMeasureArys['id']?>" <? if(strtolower($weightMeasureArys['szDescription'])=='kg'){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
                                        <?
                                    }
                                }
                            ?>
                        </select>
                    </td>
                    <td style="padding:0;text-align:right;">
                        <a href="javascript:void(0)" style="opacity:0.4" id="test_try_out" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/test')?></span></a> 
                        <a href="javascript:void(0)" class="button2" onclick="clear_haulage_tryitout();"><span style="min-width:70px;"><?=t($t_base.'fields/clear')?></span></a>
                    </td>
                </tr>
            </table>
	</form>
	
	<div id="tryitout_result_div_error" style="display:none;margin-top:8px;" class="errorBox">
	</div>
	<div id="tryitout_result_div" style="display:none;">
	</div>		
	<hr>
	<h4><?=t($t_base.'title/illustration_of_what_haulage_services_cover')?></h4>
	<img src="<?=$image_path?>" id="Upload one by one Haulage Services" >
	<!-- <canvas style="margin-left:-10px;" id="myCanvas" width="730" height="180"></canvas>-->
	<?php	
	}
	else
	{
		$t_base = "HaulaugePricing/";
		?>
		<script type="text/javascript">
			$().ready(function() {	
				//draw_canvas_image_obo(' ',' ','<?=__BASE_STORE_IMAGE_URL__.'/haulagePricingBulk-1.png'?>','HAULAGE_PRICING');
			});
		</script>
		<div id="tryitout_result_div_error" style="display:none;margin-top:8px;" class="errorBox">
		</div>
		
		<div id="tryitout_result_div" style="display:none;">
		</div>		
		
		<hr>
		<h4><?=t($t_base.'title/illustration_of_what_haulage_services_cover')?></h4>
		<img src="<?=__BASE_STORE_IMAGE_URL__.'/new-haulagePricingBulk-1.png'?>" id="Upload one by one Haulage Services" >
		<?php
	} 
}
function display_haulage_tryitout_calculation_details($searchedAry,$idForwarder)
{
	$t_base = "TryItOut/";
	
	if(!empty($searchedAry))
	{
		$kForwarder = new cForwarder();
		$kForwarder->load($idForwarder);
		if($kForwarder->szCurrency >0)
		{
                    $idCurrency = $kForwarder->szCurrency ;
                    $kWhsSearch = new cWHSSearch();
                    if($idCurrency==1)
                    {
                        $forwarderCurrencyAry['idCurrency']=1;
                        $forwarderCurrencyAry['szCurrency'] = 'USD';
                        $forwarderCurrencyAry['fExchangeRate'] = 1;
                    }
                    else
                    {
                        $resultAry = $kWhsSearch->getCurrencyDetails($idCurrency);								
                        $forwarderCurrencyAry['idCurrency']=$resultAry['idCurrency'];
                        $forwarderCurrencyAry['szCurrency'] = $resultAry['szCurrency'];
                        $forwarderCurrencyAry['fExchangeRate'] = $resultAry['fUsdValue'];
                    }
                    if($forwarderCurrencyAry['fExchangeRate']>0)
                    {
                        $fHaulageTotalRoe = number_format((float)((1/((1/$forwarderCurrencyAry['fExchangeRate'])/$searchedAry['fExchangeRate']))),4) ;
                        //$fHaulageTotalRoe = number_format((float)($searchedAry['fExchangeRate']/$forwarderCurrencyAry['fExchangeRate']),4) ;
                    }
                    else
                    {
                        $fHaulageTotalRoe = 0;
                    }
		}
		else
		{
                    echo "Please update your currency then only we are able to display you Test result in your currency.";
                    die;
		}
		
		if($searchedAry['iDirection']==1)
		{
                    $direction_str = t($t_base.'fields/Export');
                    $to_from = t($t_base.'fields/from');
		}
		else
		{
                    $direction_str = t($t_base.'fields/Import');
                    $to_from = t($t_base.'fields/to');
		}
		
		$kWhsSearch = new cWHSSearch();	
		$idWarehouse = $searchedAry['idWarehouse'];
		$kWhsSearch->load($idWarehouse);
		$szWhsName = $kWhsSearch->szWareHouseName ;
		
		$weight_bracket_distance = t($t_base.'fields/upto')." ".number_format((float)$searchedAry['iDistanceUptoKm'])." km ".t($t_base.'fields/and_weight')." ".t($t_base.'fields/upto')." ".number_format((float)$searchedAry['iUpToKg'])." kg ";
		
		$weight_bracket  = t($t_base.'fields/upto')." ".number_format((float)$searchedAry['iUpToKg'])." kg ";
		
		if($searchedAry['idHaulageModel']==__HAULAGE_ZONE_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/Pricing')." ".$to_from." ".t($t_base.'fields/zone')." ".$searchedAry['szModelName']." ".$weight_bracket." ".t($t_base.'fields/applies_to_this');
			//." ".$direction_str." ".t($t_base.'fields/haulage')
		}
		else if($searchedAry['idHaulageModel']==__HAULAGE_CITY_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/Pricing')." ".$to_from." ".$searchedAry['szModelName']." ".$weight_bracket." ".t($t_base.'fields/applies');
		}
		else if($searchedAry['idHaulageModel']==__HAULAGE_POSTCODE_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/Pricing')." ".$to_from." ".t($t_base.'fields/Postcode')." ".t($t_base.'fields/set')." ".$searchedAry['szModelName']." ".$weight_bracket." ".t($t_base.'fields/applies');
		}
		else if($searchedAry['idHaulageModel']==__HAULAGE_DISTANCE_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/pricing_based_on_distance')." ".$weight_bracket_distance." ".t($t_base.'fields/applies');
		}
		//class="td-border-bottom"
		?>
		<p style="margin-top:10px;margin-bottom:6px;"><?=$header_text?>: </p>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr >
		    <th style="width:28%" align="left">&nbsp;</th>
		    <th style="width:24%" align="right"><?=t($t_base.'fields/price');?>*</th>
		    <th style="width:24%" align="right"><?=t($t_base.'fields/roe');?></th>
		    <th style="width:24%" align="right"><?=t($t_base.'fields/price');?></th>
		  </tr>
		  <?php
		  	if($searchedAry['iDirection']==1)
			{
                            $szOriginAddress =  $searchedAry['szCityPostCode']."<br /> ".$searchedAry['szCountryName'];
                            $szDestinationAddress = $searchedAry['szWhsPostcode']." - ".$searchedAry['szWhsCity']."<br />".$searchedAry['szWhsCountry'];
                            $origin_lat_lang_pipeline = $searchedAry['szLatitude']."|".$searchedAry['szLongitude']."|".$searchedAry['szWhsLatitude']."|".$searchedAry['szWhsLongitude']."|".number_format((float)$searchedAry['iDistance'])."|".$szOriginAddress."|".$szDestinationAddress."|1";
                            
                            $searchedAry['fExchangeRate'] = round($searchedAry['fExchangeRate'],3);
		  ?>
		  <tr>
		    <td><?=t($t_base.'fields/export_haulage');?> <a href="javascript:void(0);" onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');">show on map</a></td>
		    <td align="right"><?=$searchedAry['szHaulageCurrency']?> <?=$searchedAry['fExchangeRate']>0 ? number_format((float)($searchedAry['fTotalHaulgePrice'] * $searchedAry['fExchangeRate']),2) : 0.00?></td>
		    <td align="right"><?=$fHaulageTotalRoe?></p></td>
		    <td align="right"> <?=$forwarderCurrencyAry['szCurrency']?> <?=number_format((float)($searchedAry['fTotalHaulgePrice']/$forwarderCurrencyAry['fExchangeRate']),2)?></td>
		  </tr>
		   <tr>
		     <td colspan="4" align="left" class="color f-size-12" valign="top"><em>*<?=t($t_base.'fields/calculation');?>: (<?=t($t_base.'fields/max');?>(ROUNDUP(<?=t($t_base.'fields/max');?>(<?=number_format((float)$searchedAry['fCargoWeight'])?>kg ; <?=format_volume((float)$searchedAry['fCargoVolume'])?><i>cbm</i> x <?=number_format((float)$searchedAry['fWmFactor'])?>kg/cbm)/100kg) x <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePer100Kg'],2)?>) ; <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fMinimumPrice'],2)?>) + <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePerBooking'],2)?>) x (1 + <?=number_format((float)$searchedAry['fFuelPercentage'],1)?>%)  </em></td>
		  </tr>
		  <?php
			}
			else 
			{
                            $szOriginAddress =  $searchedAry['szCityPostCode']."<br /> ".$searchedAry['szCountryName'];
                            $szDestinationAddress = $searchedAry['szWhsPostcode']." - ".$searchedAry['szWhsCity']."<br />".$searchedAry['szWhsCountry'];
                            $origin_lat_lang_pipeline = $searchedAry['szLatitude']."|".$searchedAry['szLongitude']."|".$searchedAry['szWhsLatitude']."|".$searchedAry['szWhsLongitude']."|".number_format((float)$searchedAry['iDistance'])."|".$szOriginAddress."|".$szDestinationAddress."|2";
		  ?>
		  	<tr>
			    <td><?=t($t_base.'fields/import_haulage');?> <a href="javascript:void(0);" onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');">show on map</a></td>
			    <td align="right"><?=$searchedAry['szHaulageCurrency']?> <?=$searchedAry['fExchangeRate']>0 ? number_format((float)($searchedAry['fTotalHaulgePrice'] * $searchedAry['fExchangeRate']),2) : 0.00?></td>
			    <td align="right"><?=$fHaulageTotalRoe?></p></td>
			    <td align="right"><?=$forwarderCurrencyAry['szCurrency']?> <?=number_format((float)($searchedAry['fTotalHaulgePrice']/$forwarderCurrencyAry['fExchangeRate']),2)?></td>
			  </tr>
			  <tr>
			     <td colspan="4" align="left" class="color f-size-12" valign="top"><em>*<?=t($t_base.'fields/calculation');?>: (<?=t($t_base.'fields/max');?>(ROUNDUP(<?=t($t_base.'fields/max');?>(<?=number_format((float)$searchedAry['fCargoWeight'])?>kg ; <?=format_volume((float)$searchedAry['fCargoVolume'])?><i>cbm</i> x <?=number_format((float)$searchedAry['fWmFactor'])?>kg/cbm)/100kg) x <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePer100Kg'],2)?>) ; <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fMinimumPrice'],2)?>) + <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePerBooking'],2)?>) x (1 + <?=number_format((float)$searchedAry['fFuelPercentage'],1)?>%)  </em></td>
			  </tr>
		 <?php } ?>
		  </table>
		<?
	}
	else
	{
		?>
		<p style="margin-top:10px;"><?=t($t_base.'fields/no_active_haulage');?>.</p>		
		<?php
	}
}
function confirmationPOPup($content,$iWarehouseType=false)
{
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        $szCfsText = "airport warehouse";
    }
    else
    {
        $szCfsText = "CFS";
    } 
?>
    <div>
        <div id="popup-bg"></div>
        <div id="popup-container" style="padding-top:250px;">
            <div class="popup"> 
                <h5><b>Upload complete</b></h5>
                <p>You have successfully added <?php echo $content.' new '.$szCfsText.' '.($content==1?'location':'locations') ?> to Transporteca.</p>
                <br />
                <p align="center"><a class="button1" onclick="resetContentOfPage();"><span>ok</span></a></p>
            </div>
        </div>
    </div>
    <?php  
}
function unsetContent()
{	//sleep ( 10 );
    if(isset($_SESSION['completed']))
    {
        unset($_SESSION['completed']);
    }
}

function noHaulageServices()
{?>
	<div id="no_haulage_service">
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification" style="width:350px;">
				<h2>No pricing models defined</h2><br/>
				<p style="text-align:justity">You currently have no pricing models defined for any of your CFS locations. Please create the pricing models needed before trying to upload haulage pricing in bulk. Click below to go to Haulage update One by One to define your pricing models.</p>
				<br/>
				<p align="center"><a href="javascript:void(0)" onclick="close_popup_no_haulage_service()" class="button2"><span>Close</span></a><a href="<?__FORWARDER_HOME_PAGE_URL__?>/Haulage/" class="button1"><span>Go</span></a></p>
			</div>
		</div>
	</div>	
<?	
}

function getAddedCityPosition($haulageZoneNewArr,$szName)
{
	if(!empty($haulageZoneNewArr))
	{
		$i=0;
		foreach($haulageZoneNewArr as $haulageZoneNewArrs)
		{
			++$i;
			if($haulageZoneNewArrs['szName']==$szName)
			{
				return $i;
			}
		}
	}
}

function zoneHaulageList($haulagePricingZoneDataArr)
{
$t_base = "HaulaugePricing/";
?>
<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_zone">
		<tr>
			<!--  <td style="text-align:left;width:10%" class="firsttdnoborder"><strong><?=t($t_base.'fields/priority')?></strong></td>-->
			<td style="text-align:left;width:40%" class="firsttdnoborder"><strong><?=t($t_base.'fields/zone')?></strong> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:22%"><strong><?=t($t_base.'fields/transit_time')?></strong> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/transit_time_zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:22%"><strong><?=t($t_base.'fields/fWmFactor')?></strong> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fm_factor_zone_tool_tip_1');?>','<?=t($t_base.'messages/fm_factor_zone_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/fuel')?></strong> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fuel_zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
		</tr>
	<?
	if(!empty($haulagePricingZoneDataArr))
	{
		$j=0;
		foreach($haulagePricingZoneDataArr as $haulagePricingZoneDataArrs)
		{
			if($j==0)
			{
				$idPricing=$haulagePricingZoneDataArrs['id'];
				$fPricingWmFactor=$haulagePricingZoneDataArrs['fWmFactor'];
				$idPricingHaulageModel=$haulagePricingZoneDataArrs['idHaulageModel'];
				$idPricingWarehouse=$haulagePricingZoneDataArrs['idWarehouse'];
				$iPricingDirection=$haulagePricingZoneDataArrs['iDirection'];
			}
		?>
			<tr id="haulage_zone_<?=++$j?>" onclick="sel_haulage_model_zone_data('haulage_zone_<?=$j?>','<?=$haulagePricingZoneDataArrs['id']?>','<?=$haulagePricingZoneDataArrs['idHaulageModel']?>','<?=$j?>','<?=$haulagePricingZoneDataArrs['idWarehouse']?>','<?=$haulagePricingZoneDataArrs['iDirection']?>','<?=$haulagePricingZoneDataArrs['fWmFactor']?>');" <? if($j==1){?> style="background:#DBDBDB;color:#828282;" <? }?>>
				<!--<td class="firsttdnoborder"><?=$haulagePricingZoneDataArrs['iPriority']?>&nbsp;<?=t($t_base.'fields/priority')?></td>-->
				<td class="firsttdnoborder"><?=$haulagePricingZoneDataArrs['szName']?></td>
				<td><? 
				if((int)$haulagePricingZoneDataArrs['iHours']>24)
				{
					echo "< ".($haulagePricingZoneDataArrs['iHours']/24)." ".t($t_base.'fields/days');
				}
				else
				{
					echo "< ".$haulagePricingZoneDataArrs['iHours']." ".t($t_base.'fields/hours');
				}
				?></td>
				<td><?=number_format((float)$haulagePricingZoneDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base.'fields/Kg')?>/<?=t($t_base.'fields/cbm')?></td>
				<td><?=number_format((float)$haulagePricingZoneDataArrs['fFuelPercentage'],1)?> %</td>
			</tr>
		<?	
		}
	}
	else
	{?>
		<tr>
			<td colspan="4" align="center"><?=t($t_base.'messages/no_zone')?></td>
		</tr>
	<?	}
	?>
	<input type="hidden" value="<?=$j?>" id="totalActive1" name="totalActive1"/>
	</table>
<?php	
}
function postcode_data_list($haulagePricingZoneDataArr)
{
	$t_base = "HaulaugePricing/";
	$t_base_error="Error";
	$kHaulagePricing = new cHaulagePricing();
	//print_r($_POST['dataExportDistanceArr']);
	if(!empty($_POST['dataExportPostcodeArr']))
	{
		if($kHaulagePricing->copyPostCodeData($_POST['dataExportPostcodeArr']))
		{
				$data['idDirection']=$_POST['dataExportPostcodeArr']['idZoneDirection'];
				$data['idWarehouse']=$_POST['dataExportPostcodeArr']['idZoneWarehouse'];
				$data['idHaulageModel']=$_POST['dataExportPostcodeArr']['idZoneHaulageModel'];
				$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
				if(!empty($haulagePricingModelArr))
				{
					foreach($haulagePricingModelArr as $haulagePricingModelArrs)
					{
						if($haulagePricingModelArrs['idHaulageModel']=='3')
						{
							if((int)$haulagePricingModelArrs['iCount']>0)
							{
								if((int)$haulagePricingModelArrs['iCount']==1)
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/postcode_set_defined');
								else
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/postcode_sets_defined');
							}
							else
								$text=t($t_base.'fields/no_postcode_sets_defined');
						}
					}
				}
				else
				{
					$text=t($t_base.'fields/no_postcode_sets_defined');
				}
		
		?>
			<script type="text/javascript">
				$("#zone_detail_delete_"+<?=$_POST['dataExportPostcodeArr']['idZoneHaulageModel']?>).attr('style','display:none;');
				$("#loader").attr('style','display:block;');
				
				open_pricing_model_detail('<?=$_POST['dataExportPostcodeArr']['idZoneHaulageModel']?>','<?=$_POST['dataExportPostcodeArr']['idZoneWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>');
				var divid="change_data_value_<?=$_POST['dataExportPostcodeArr']['idZoneHaulageModel']?>";
				$("#"+divid).html('<?=$text?>');
				</script>	
		<?		
		}
	}

if(!empty($kHaulagePricing->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?
	}
	
	?>
	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="format-2" style="position:relative;top:-1px;">
	<?
	if(!empty($haulagePricingZoneDataArr))
	{
		foreach($haulagePricingZoneDataArr as $haulagePricingZoneDataArrs)
		{?>
			<tr>
				<td class="noborder" width="54%"><?=$haulagePricingZoneDataArrs['szName']?></td>
				<td class="checkbox" width="23%"><input type="checkbox" name="dataExportPostcodeArr[szCopyZone][]" id="szCopyZone_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>" <? if(!empty($_POST['dataExportPostcodeArr']['szCopyZone']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportPostcodeArr']['szCopyZone'])) {?> checked <? }?>  onclick="enable_copy_pricing('<?=$haulagePricingZoneDataArrs['id']?>','postcode');"> <?=t($t_base.'fields/postcode_data')?></td>
				<td class="checkbox" width="23%"><input type="checkbox" name="dataExportPostcodeArr[szCopyZonePricing][]" id="szCopyZonePricing_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>"  <? if(!empty($_POST['dataExportPostcodeArr']['szCopyZonePricing']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportPostcodeArr']['szCopyZonePricing'])) {?> checked <? } if(empty($_POST['dataExportPostcodeArr']['szCopyZone'])){?> disabled <? }?> > <?=t($t_base.'fields/pricing_data')?></td>
			</tr>
		<?		
		}
	}else
	{?>
		<tr>
			<td colspan="3" align="center"><?=t($t_base.'fields/no_record_found_postcode');?></td>
		</tr>
		<?
	}
	?>
	</table>
	<?
}
function getWorkingDays($tday)
{

	$startDate=date("Y-m-d");
    if($tday>0)
    {
		for($i=1;$i<=$tday;++$i)
		{
	    	$endDate=date("Y-m-d",strtotime(date("Y-m-d", strtotime($startDate)) . '+1 DAY'));
	    	
	    	$dayName=date("D",strtotime($endDate));
	    	
	    	
			if($dayName=='Sun')
			{
				///if day is sun
				$startDate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($endDate)) . '+1 DAY'));
				$endDate=date("Y-m-d",strtotime(date("Y-m-d", strtotime($startDate))));
			}
			else if($dayName=='Sat')
			{
				///if day is Sat
				$startDate = date("Y-m-d",strtotime(date("Y-m-d", strtotime($endDate)) . '+2 DAY'));
				$endDate=date("Y-m-d",strtotime(date("Y-m-d", strtotime($startDate))));
			}
			else
			{
				$startDate=date("Y-m-d",strtotime(date("Y-m-d", strtotime($endDate))));			
			}
		}
		return $endDate;
    }
}

function pricingCCApprovalList($idForwarder,$totalPricingServiceData,$idService)
{
	$t_base = "OBODataExport/";
	
	$t_base_approve = "BulkUpload/";
	$Excel_export_import= new cExport_Import();
	$kUploadBulkService= new cUploadBulkService();
	$forwardWareHouseArr = array();
	$forwardCountriesArr = array();
	$forwardWareHouseCityAry = array();
	// getting all countries where forwarder has warehouse listed.
	$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder);
	//$idOriginCountry = $forwardCountriesArr[0]['idCountry'];
	//$idDestinationCountry = $forwardCountriesArr[1]['idCountry'];
	$idData=$totalPricingServiceData[0];
	$pricingCCServiceData=array();
	$pricingCCServiceData=$kUploadBulkService->totalWaitingToBeApproved($idForwarder,$idService,$idData);
	//print_r($pricingCCServiceData);
	
	$idOriginCountry = $_POST['oboDataExportAry']['idOriginCountry'] ? $_POST['oboDataExportAry']['idOriginCountry'] : $pricingCCServiceData[0]['idCountryFrom'];
	$idDestinationCountry = $_POST['oboDataExportAry']['idDestinationCountry'] ? $_POST['oboDataExportAry']['idDestinationCountry'] : $pricingCCServiceData[0]['idCountryTo'];
	
	$szOrigin_city = $_POST['oboDataExportAry']['szOriginCity'] ? $_POST['oboDataExportAry']['szOriginCity'] : $pricingCCServiceData[0]['szCityFrom'];
	$szDestination_city = $_POST['oboDataExportAry']['szDestinationCity'] ? $_POST['oboDataExportAry']['szDestinationCity'] : $pricingCCServiceData[0]['szCityTo'];
	
	$idOrigin_warehouse_id = $_POST['oboDataExportAry']['idOriginWarehouse'] ? $_POST['oboDataExportAry']['idOriginWarehouse'] : $pricingCCServiceData[0]['idWarehouseFrom'];
	$idDestination_warehouse_id = $_POST['oboDataExportAry']['idDestinationWarehouse'] ? $_POST['oboDataExportAry']['idDestinationWarehouse'] : $pricingCCServiceData[0]['idWarehouseTo'];
	
	
	//$idOriginCountry = $pricingCCServiceData[0]['idCountryFrom'];
	//$idDestinationCountry = $pricingCCServiceData[0]['idCountryTo'];
	
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_origin=$Excel_export_import->getForwaderWarehouses($idForwarder,$idOriginCountry);
	
	$forwardWareHouseCityAry_origin = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idOriginCountry);
	
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_dest=$Excel_export_import->getForwaderWarehouses($idForwarder,$idDestinationCountry);
	
	$forwardWareHouseCityAry_dest = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idDestinationCountry);
	//echo $pricingCCServiceData[0]['idCountryFrom'];
	?>
	<form action="" id="obo_cc_data_export_form" name="obo_cc_data_export_form" method="post">
				<div class="oh">
				<div class="fl-49">
					<p><strong><?=t($t_base.'title/origin');?></strong></p>
					<div class="oh">
						<div class="fl-33 s-field">
							<span class="f-size-12"><?=t($t_base.'title/country');?></span>
							<span id="origin_country_span">
								<select name="oboDataExportAry[idOriginCountry]" id="idOriginCountry" onchange="select_country_approval_cc('<?=$idForwarder?>','origin',this.value);">
									<?php
										
										if(!empty($forwardCountriesArr))
										{
											foreach($forwardCountriesArr as $forwardCountriesArrs)
											{
												?>
												<option value="<?=$forwardCountriesArrs['idCountry']?>" <? if($idOriginCountry==$forwardCountriesArrs['idCountry']){?> selected <? }?>><?=$forwardCountriesArrs['szCountryName']?></option>
												<?
											}
										}
									?>
								</select>
							</span> 
						</div>
						<div id="city_warehouse_origin">
						<div class="fl-33 s-field">
							<span class="f-size-12"><?=t($t_base.'title/city');?></span>
							<span id="origin_city_span">
								<select id="szOriginCity" name="oboDataExportAry[szOriginCity]" onchange="select_city_approval_cc('<?=$idForwarder?>','origin',this.value);">
									<option value=""><?=t($t_base.'title/all');?></option>
									<?php
										if(!empty($forwardWareHouseCityAry_origin))
										{
											foreach($forwardWareHouseCityAry_origin as $forwardWareHouseCityArys)
											{
												?>
												<option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($szOrigin_city==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
												<?
											}
										}
									?>
								</select>
							</span>
						</div>
						<div class="fl-33">
							<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
							<span id="origin_warehouse_span">
								<select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse" onchange="sel_warehouse_for_approval_cc('<?=$idForwarder?>','origin',this.value);">
									<option value=""><?=t($t_base.'fields/select');?></option>
									<?php
										if(!empty($forwardWareHouseArr_origin))
										{
											foreach($forwardWareHouseArr_origin as $forwardWareHouseArrs)
											{
												?>
												<option value="<?=$forwardWareHouseArrs['id']?>" <? if($idOrigin_warehouse_id==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
												<?
											}
										}
									?>
								</select>
							</span>
						</div>
						</div>
					</div>
				</div>
				
				<div class="fl-49" style="float:right;">
					<p><strong><?=t($t_base.'title/destination');?></strong></p>
					<div class="oh">
						<div class="fl-33 s-field">
							<span class="f-size-12"><?=t($t_base.'title/country');?></span>
							<span id="destination_country_span">
								<select name="oboDataExportAry[idDestinationCountry]"  id="idDestinationCountry" onchange="select_country_approval_cc('<?=$idForwarder?>','des',this.value);">
									<?php
										if(!empty($forwardCountriesArr))
										{
											foreach($forwardCountriesArr as $forwardCountriesArrs)
											{
												?>
												<option value="<?=$forwardCountriesArrs['idCountry']?>" <? if($idDestinationCountry==$forwardCountriesArrs['idCountry']){?> selected <? }?>><?=$forwardCountriesArrs['szCountryName']?></option>
												<?
											}
										}
									?>
								</select>
							</span> 
						</div>
						<div id="city_warehouse_des">
						<div class="fl-33 s-field">
							<span class="f-size-12"><?=t($t_base.'title/city');?></span>
							<span id="destination_city_span">
								<select id="szDestinationCity" name="oboDataExportAry[szDestinationCity]">
									<option value=""><?=t($t_base.'title/all');?></option>
									<?php
										if(!empty($forwardWareHouseCityAry_dest))
										{
											foreach($forwardWareHouseCityAry_dest as $forwardWareHouseCityArys)
											{
												?>
												<option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($szDestination_city==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
												<?
											}
										}
									?>
								</select>
							</span>
						</div>
						<div class="fl-33">
							<span class="f-size-12"><?=t($t_base.'title/cfs_name');?></span>
							<span id="destination_warehouse_span">
								<select name="oboDataExportAry[idDestinationWarehouse]"  onchange="sel_warehouse_for_approval_cc('<?=$idForwarder?>','des',this.value);" id="idDestinationWarehouse">
									<option value=""><?=t($t_base.'fields/select');?></option>
									<?php
										if(!empty($forwardWareHouseArr_dest))
										{
											foreach($forwardWareHouseArr_dest as $forwardWareHouseArrs)
											{
												?>
												<option value="<?=$forwardWareHouseArrs['id']?>" <? if($idDestination_warehouse_id==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
												<?
											}
										}
									?>
								</select>
							</span>
						</div>
						</div>
					</div>
				</div>
				</div>
			</form>
			<?
			if(!$flag)
				approve_pricing_cc_bottom($pricingCCServiceData,$idForwarder);
			?>		
<?
}

function approve_pricing_cc_bottom($pricingCCServiceData,$idForwarder)
{
	$t_base = "OBODataExport/";
	
	$t_base_approve = "BulkUpload/";
	
	$kConfig = new cConfig();
	$allCurrencyArr=$kConfig->getBookingCurrency(false,false,false,true);
	//print_r($allCurrencyArr);	
	
	if(!empty($_POST['pricingCCApprove']))
	{
		$pricingCCServiceData=array();
	}
	?>
	<form id="update_obo_cc_form" method="post" action="">
	
	<?
		if($szOriginCountry=='' && empty($szOriginCountry))
		{
			$szOriginCountry="";
		}
		
		if($szDestinationCountry=='' && empty($szDestinationCountry))
		{
			$szDestinationCountry="";
		}
	?>
	<div id="obo_cc_error"  class="errorBox" style="display:none;"></div>
	<div class="oh">
		<p class="fl-40 gap-text-field" style="width:487px;"><?=t($t_base_approve.'title/export_cc');?>&nbsp;<?=($szOriginCountry?'for '.$szOriginCountry:'')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/EXPORT_CC_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15" style="width: 119px;"><span class="f-size-12"><?=t($t_base.'title/per_booking');?></span><br /><input id="fOriginPrice" type="text" style="width: 107px;" name="pricingCCApprove[fOriginPrice]" value="<?=$_POST['pricingCCApprove']['fOriginPrice'] ? $_POST['pricingCCApprove']['fOriginPrice'] : $pricingCCServiceData[0]['fPriceExport'] ?>" size="10" onkeyup="change_currency_select_box('origin');"/></p>
		<p class="fl-20" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="pricingCCApprove[idOriginCurrency]" id="idOriginCurrency" >
			<option value=""><?=t($t_base.'fields/select');?></option>			
			<?php
					if(!empty($allCurrencyArr))
					{
						foreach($allCurrencyArr as $allCurrencyArrs)
						{
							?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['pricingCCApprove']['idOriginCurrency'])?$_POST['pricingCCApprove']['idOriginCurrency']:$pricingCCServiceData[0]['szCurrencyExport']) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field" style="width:487px;"><?=t($t_base_approve.'title/import_cc');?>&nbsp;<?=($szDestinationCountry?'for '.$szDestinationCountry:'')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/IMPORT_CC_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15" style="width: 119px;"><span class="f-size-12"><?=t($t_base.'title/per_booking');?></span><br /><input id="fDestinationPrice" type="text" style="width: 107px;" id="fDestinationPrice" name="pricingCCApprove[fDestinationPrice]" value="<?=$_POST['pricingCCApprove']['fDestinationPrice'] ? $_POST['pricingCCApprove']['fDestinationPrice'] : $pricingCCServiceData[0]['fPriceImport'] ?>" size="10" onkeyup="change_currency_select_box('des');"/></p>
		<p class="fl-20" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/currency');?></span><br />
			<select size="1" name="pricingCCApprove[idDestinationCurrency]" id="idDestinationCurrency" >
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($allCurrencyArr))
					{
						foreach($allCurrencyArr as $allCurrencyArrs)
						{
							?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['pricingCCApprove']['idDestinationCurrency'])?$_POST['pricingCCApprove']['idDestinationCurrency']:$pricingCCServiceData[0]['szCurrencyImport']) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<br />
	<div class="oh">
		<p class="fl-33">
		Click to move to next
		</p>
		<p class="fl-40">
		<a href="javascript:void(0)"  class="button2" onclick="delete_pricing_cc_appoving_data('<?=$pricingCCServiceData[0]['id']?>','<?=$idForwarder?>');"><span><?=t($t_base_approve.'fields/delete');?></span></a>
		 <a href="javascript:void(0);" class="button1" onclick="approved_pricing_cc_data('<?=$pricingCCServiceData[0]['id']?>','<?=$idForwarder?>')" id="approve_button"><span><?=t($t_base_approve.'fields/approve');?></span></a></p>	
	</div>
	<input type="hidden" name="pricingCCApprove[id]" id="id" value="<?=$_POST['pricingCCApprove']['id'] ? $_POST['pricingCCApprove']['id'] : $pricingCCServiceData[0]['id'] ?>">
	<input type="hidden" name="pricingCCApprove[idOriginWarehouse]" id="idOriginWarehouse_hidden" value="<?=$_POST['pricingCCApprove']['idOriginWarehouse'] ? $_POST['pricingCCApprove']['idOriginWarehouse'] : $pricingCCServiceData[0]['idWarehouseFrom'] ?>">
	<input type="hidden" name="pricingCCApprove[idDestinationWarehouse]" id="idDestinationWarehouse_hidden" value="<?=$_POST['pricingCCApprove']['idDestinationWarehouse'] ? $_POST['pricingCCApprove']['idDestinationWarehouse'] : $pricingCCServiceData[0]['idWarehouseTo'] ?>">
</form>
<?php
}
function pricingWTWApprovalList($idForwarder,$totalPricingServiceData,$idService)
{ 
	$t_base = "OBODataExport/";
	
	$t_base_approve = "BulkUpload/";
	$Excel_export_import= new cExport_Import();
	$kUploadBulkService= new cUploadBulkService();
	$forwardWareHouseArr = array();
	$forwardCountriesArr = array();
	$forwardWareHouseCityAry = array();
                    
	$idData=$totalPricingServiceData[0];
	$pricingCCServiceData=array();
	$pricingCCServiceData=$kUploadBulkService->totalWaitingToBeApprovedPricingWTW($idForwarder,$idService,$idData);
	
	//print_r($_POST['oboDataExportAry']);
	
	$idOriginCountry = $_POST['oboDataExportAry']['idOriginCountry'] ? $_POST['oboDataExportAry']['idOriginCountry'] : $pricingCCServiceData[0]['idCountryFrom'];
	$idDestinationCountry = $_POST['oboDataExportAry']['idDestinationCountry'] ? $_POST['oboDataExportAry']['idDestinationCountry'] : $pricingCCServiceData[0]['idCountryTo'];
	
	$szOrigin_city = $_POST['oboDataExportAry']['szOriginCity'] ? $_POST['oboDataExportAry']['szOriginCity'] : $pricingCCServiceData[0]['szCityFrom'];
	$szDestination_city = $_POST['oboDataExportAry']['szDestinationCity'] ? $_POST['oboDataExportAry']['szDestinationCity'] : $pricingCCServiceData[0]['szCityTo'];
	
	$idOrigin_warehouse_id = $_POST['oboDataExportAry']['idOriginWarehouse'] ? $_POST['oboDataExportAry']['idOriginWarehouse'] : $pricingCCServiceData[0]['idWarehouseFrom'];
	$idDestination_warehouse_id = $_POST['oboDataExportAry']['idDestinationWarehouse'] ? $_POST['oboDataExportAry']['idDestinationWarehouse'] : $pricingCCServiceData[0]['idWarehouseTo'];
                    
        if($idOrigin_warehouse_id>0)
        {
            $kWarehouse = new cWHSSearch();
            $kWarehouse->load($idOrigin_warehouse_id);
            $iWarehouseType = $kWarehouse->iWarehouseType;
        }
        else if($idDestination_warehouse_id>0)
        {
            $kWarehouse = new cWHSSearch();
            $kWarehouse->load($idOrigin_warehouse_id);
            $iWarehouseType = $kWarehouse->iWarehouseType;
        }
        
        // getting all countries where forwarder has warehouse listed.
	$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder,$iWarehouseType);
                    
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_origin=$Excel_export_import->getForwaderWarehouses($idForwarder,$idOriginCountry,false,false,$iWarehouseType);
	
	$forwardWareHouseCityAry_origin = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idOriginCountry,$iWarehouseType);
	
	// getting all warehouse's of this forwarder.
	$forwardWareHouseArr_dest=$Excel_export_import->getForwaderWarehouses($idForwarder,$idDestinationCountry,false,false,$iWarehouseType);
	
	$forwardWareHouseCityAry_dest = $Excel_export_import->getForwaderWarehousesCity($idForwarder,$idDestinationCountry,$iWarehouseType);
                    
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $szCFSName = "Airport warehouse"; 
        }
        else
        {
            $szCFSName = "CFS name";  ;
        }
	?>
	<form action="" id="obo_cc_data_export_form" name="obo_cc_data_export_form" method="post">
				<div class="oh">
				<div class="fl-49">
					<p><strong><?=t($t_base.'title/origin');?></strong></p>
					<div class="oh">
						<div class="fl-33 s-field">
                                                    <span class="f-size-12"><?=t($t_base.'title/country');?></span>
                                                    <span id="origin_country_span">
                                                        <select name="oboDataExportAry[idOriginCountry]" id="idOriginCountry" onchange="select_country_approval('<?=$idForwarder?>','origin',this.value,'<?php echo $iWarehouseType; ?>');"> 
                                                            <?php 
                                                                if(!empty($forwardCountriesArr))
                                                                {
                                                                    foreach($forwardCountriesArr as $forwardCountriesArrs)
                                                                    {
                                                                        ?>
                                                                        <option value="<?=$forwardCountriesArrs['idCountry']?>" <?php if($idOriginCountry==$forwardCountriesArrs['idCountry']){?> selected <? }?>><?=$forwardCountriesArrs['szCountryName']?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </span> 
						</div>
						<div id="city_warehouse_origin">
						<div class="fl-33 s-field">
							<span class="f-size-12"><?=t($t_base.'title/city');?></span>
							<span id="origin_city_span">
								<select id="szOriginCity" name="oboDataExportAry[szOriginCity]" onchange="select_city_approval('<?=$idForwarder?>','origin',this.value,'<?php echo $iWarehouseType; ?>');">
									<option value=""><?=t($t_base.'title/all');?></option>
									<?php
										if(!empty($forwardWareHouseCityAry_origin))
										{
											foreach($forwardWareHouseCityAry_origin as $forwardWareHouseCityArys)
											{
												?>
												<option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($szOrigin_city==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
												<?
											}
										}
									?>
								</select>
							</span>
						</div>
						<div class="fl-33">
							<span class="f-size-12"><?=$szCFSName;?></span>
							<span id="origin_warehouse_span">
								<select name="oboDataExportAry[idOriginWarehouse]" id="idOriginWarehouse" onchange="sel_warehouse_for_approval('<?=$idForwarder?>','origin',this.value);">
									<option value=""><?=t($t_base.'fields/select');?></option>
									<?php
										if(!empty($forwardWareHouseArr_origin))
										{
											foreach($forwardWareHouseArr_origin as $forwardWareHouseArrs)
											{
												?>
												<option value="<?=$forwardWareHouseArrs['id']?>" <? if($idOrigin_warehouse_id==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
												<?
											}
										}
									?>
								</select>
							</span>
						</div>
						</div>
					</div>
				</div>
				
				<div class="fl-49" style="float:right;">
					<p><strong><?=t($t_base.'title/destination');?></strong></p>
					<div class="oh">
						<div class="fl-33 s-field">
							<span class="f-size-12"><?=t($t_base.'title/country');?></span>
							<span id="destination_country_span">
                                                            <select name="oboDataExportAry[idDestinationCountry]"  id="idDestinationCountry" onchange="select_country_approval('<?=$idForwarder?>','des',this.value,'<?php echo $iWarehouseType; ?>');"> 
                                                                <?php
                                                                    if(!empty($forwardCountriesArr))
                                                                    {
                                                                        foreach($forwardCountriesArr as $forwardCountriesArrs)
                                                                        {
                                                                            ?>
                                                                            <option value="<?=$forwardCountriesArrs['idCountry']?>" <?php if($idDestinationCountry==$forwardCountriesArrs['idCountry']){?> selected <?php }?>><?=$forwardCountriesArrs['szCountryName']?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>
                                                            </select>
							</span> 
						</div>
						<div id="city_warehouse_des">
						<div class="fl-33 s-field">
                                                    <span class="f-size-12"><?=t($t_base.'title/city');?></span>
                                                    <span id="destination_city_span">
                                                        <select id="szDestinationCity" name="oboDataExportAry[szDestinationCity]" onchange="select_city_approval('<?=$idForwarder?>','des',this.value,'<?php echo $iWarehouseType; ?>');">
                                                            <option value=""><?=t($t_base.'title/all');?></option>
                                                            <?php
                                                                if(!empty($forwardWareHouseCityAry_dest))
                                                                {
                                                                    foreach($forwardWareHouseCityAry_dest as $forwardWareHouseCityArys)
                                                                    {
                                                                        ?>
                                                                        <option value="<?=$forwardWareHouseCityArys['szCity']?>" <? if($szDestination_city==$forwardWareHouseCityArys['szCity']){?> selected <? }?>><?=$forwardWareHouseCityArys['szCity']?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </span>
						</div>
						<div class="fl-33">
                                                    <span class="f-size-12"><?=$szCFSName;?></span>
                                                    <span id="destination_warehouse_span">
                                                        <select name="oboDataExportAry[idDestinationWarehouse]"  onchange="sel_warehouse_for_approval('<?=$idForwarder?>','des',this.value);" id="idDestinationWarehouse">
                                                            <option value=""><?=t($t_base.'fields/select');?></option>
                                                            <?php
                                                                if(!empty($forwardWareHouseArr_dest))
                                                                {
                                                                    foreach($forwardWareHouseArr_dest as $forwardWareHouseArrs)
                                                                    {
                                                                        ?>
                                                                        <option value="<?=$forwardWareHouseArrs['id']?>" <? if($idDestination_warehouse_id==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </span>
						</div>
                                            </div>
					</div>
				</div>
				</div>
			</form>
			<?php 
                            display_obo_lcl_bottom_html_approved($pricingCCServiceData,$idForwarder);
			?>
				
<?php
}function display_obo_lcl_bottom_html_approved($pricingWtwAry,$idForwarder)
{	
	//print_r($_POST);
	$t_base = "OBODataExport/";
	
	$t_base_approve = "BulkUpload/";
	
	$kConfig = new cConfig();
	$frequencyAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_FREQUENCY__);
	$weekDaysAry = $kConfig->getAllWeekDays();
	$cutoffLocalTimeAry = fColumnData();
	$allCurrencyArr=$kConfig->getBookingCurrency(false,false,false,true);
	$cuttOffAry = cutoff_key_value_pair();
	
	$idOriginWarehouse = $pricingWtwAry[0]['idWarehouseFrom'];
	$idDestinationWarehouse = $pricingWtwAry[0]['idWarehouseTo'];
        
        $kWarehouse = new cWHSSearch();
        $kWarehouse->load($idOriginWarehouse);
        $iWarehouseType = $kWarehouse->iWarehouseType;
                    
	if(!empty($_POST['pricingWTW']))
	{
            $pricingWtwAry=array();
	}
	
	if(!empty($_POST['pricingWTW']))
	{
		if($_POST['pricingWTW']['iOriginChargesApplicable']!=1)
		{
                    $pricingWtwAry[0]['iOriginChargesApplicable']=0;
                    $pricingWtwAry[0]['fOriginChargeRateWM'] = (float)$_POST['pricingWTW']['fOriginChargeRateWM']>0?$_POST['pricingWTW']['fOriginChargeRateWM']:'';
                    $pricingWtwAry[0]['fOriginChargeMinRateWM'] = (float)$_POST['pricingWTW']['fOriginMinRateWM']>0?$_POST['pricingWTW']['fOriginMinRateWM']:'';
                    $pricingWtwAry[0]['fOriginChargeBookingRate'] = (float)$_POST['pricingWTW']['fOriginRate']>0?$_POST['pricingWTW']['fOriginRate']:'';
                    $pricingWtwAry[0]['szOriginChargeCurrency']=$_POST['pricingWTW']['szOriginFreightCurrency']>0?$_POST['pricingWTW']['szOriginFreightCurrency']:'';
		}
		else
		{
                    $pricingWtwAry[0]['fOriginChargeRateWM'] = (float)$_POST['pricingWTW']['fOriginChargeRateWM']>0?$_POST['pricingWTW']['fOriginChargeRateWM']:'';
                    $pricingWtwAry[0]['fOriginChargeMinRateWM'] = (float)$_POST['pricingWTW']['fOriginMinRateWM']>0?$_POST['pricingWTW']['fOriginMinRateWM']:'';
                    $pricingWtwAry[0]['fOriginChargeBookingRate'] = (float)$_POST['pricingWTW']['fOriginRate']>0?$_POST['pricingWTW']['fOriginRate']:'';
                    $pricingWtwAry[0]['szOriginChargeCurrency']=$_POST['pricingWTW']['szOriginFreightCurrency']>0?$_POST['pricingWTW']['szOriginFreightCurrency']:'';
		}
	}
	else if($pricingWtwAry[0]['iOriginChargesApplicable']!=1)
	{
            $pricingWtwAry[0]['fOriginChargeRateWM'] = (float)$pricingWtwAry[0]['fOriginChargeRateWM']>0?$pricingWtwAry[0]['fOriginChargeRateWM']:'';
            $pricingWtwAry[0]['fOriginChargeMinRateWM'] = (float)$pricingWtwAry[0]['fOriginMinRateWM']>0?$pricingWtwAry[0]['fOriginChargeMinRateWM']:'';
            $pricingWtwAry[0]['fOriginChargeBookingRate'] = (float)$pricingWtwAry[0]['fOriginRate']>0?$pricingWtwAry[0]['fOriginChargeBookingRate']:''; 
	}
	if(!empty($_POST['pricingWTW']))
	{
            if($_POST['pricingWTW']['iDestinationChargesApplicable']!=1)
            {
		$pricingWtwAry[0]['iDestinationChargesApplicable']=0;
		$pricingWtwAry[0]['fDestinationChargeRateWM'] = (float)$_POST['pricingWTW']['fDestinationChargeRateWM']>0?$_POST['pricingWTW']['fDestinationChargeRateWM']:'';
		$pricingWtwAry[0]['fDestinationChargeMinRateWM'] = (float)$_POST['pricingWTW']['fDestinationChargeMinRateWM']>0?$_POST['pricingWTW']['fDestinationChargeMinRateWM']:'';
		$pricingWtwAry[0]['fDestinationChargeBookingRate'] = (float)$_POST['pricingWTW']['fDestinationChargeBookingRate']>0?$_POST['pricingWTW']['fDestinationChargeBookingRate']:'';
		$pricingWtwAry[0]['szDestinationChargeCurrency']=$_POST['pricingWTW']['szDestinationFreightCurrency']>0?$_POST['pricingWTW']['szDestinationFreightCurrency']:'';
            }
            else
            {
                $pricingWtwAry[0]['fDestinationChargeRateWM'] = (float)$_POST['pricingWTW']['fDestinationChargeRateWM']>0?$_POST['pricingWTW']['fDestinationChargeRateWM']:'';
                $pricingWtwAry[0]['fDestinationChargeMinRateWM'] = (float)$_POST['pricingWTW']['fDestinationChargeMinRateWM']>0?$_POST['pricingWTW']['fDestinationChargeMinRateWM']:'';
                $pricingWtwAry[0]['fDestinationChargeBookingRate'] = (float)$_POST['pricingWTW']['fDestinationChargeBookingRate']>0?$_POST['pricingWTW']['fDestinationChargeBookingRate']:'';
                $pricingWtwAry[0]['szDestinationChargeCurrency']=$_POST['pricingWTW']['szDestinationFreightCurrency']>0?$_POST['pricingWTW']['szDestinationFreightCurrency']:'';
            }
	}
	else if($pricingWtwAry[0]['iDestinationChargesApplicable']!=1)
	{
            $pricingWtwAry[0]['fDestinationChargeRateWM'] = (float)$pricingWtwAry[0]['fDestinationChargeRateWM']>0?$pricingWtwAry[0]['fDestinationChargeRateWM']:'';
            $pricingWtwAry[0]['fDestinationChargeMinRateWM'] = (float)$pricingWtwAry[0]['fDestinationChargeMinRateWM']>0?$pricingWtwAry[0]['fDestinationChargeMinRateWM']:'';
            $pricingWtwAry[0]['fDestinationChargeBookingRate'] = (float)$pricingWtwAry[0]['fDestinationChargeBookingRate']>0?$pricingWtwAry[0]['fDestinationChargeBookingRate']:'';
	}
	
	
	if(!empty($_POST['pricingWTW']))
	{
            $pricingWtwAry[0]['fFreightRateWM'] = (float)$_POST['pricingWTW']['fRateWM']>0?$_POST['pricingWTW']['fRateWM']:'';
            $pricingWtwAry[0]['fFreightMinRateWM'] = (float)$_POST['pricingWTW']['fMinRateWM']>0?$_POST['pricingWTW']['fMinRateWM']:'';
            $pricingWtwAry[0]['fFreightMinRateWM'] = (float)$_POST['pricingWTW']['fMinRateWM']>0?$_POST['pricingWTW']['fMinRateWM']:'';
            $pricingWtwAry[0]['szFreightCurrency']=$_POST['pricingWTW']['szFreightCurrency']>0?$_POST['pricingWTW']['szFreightCurrency']:'';
	}
	
	if($_POST['pricingWTW']['idAvailableDay']>0)
	{
            $weekDaysArys = $kConfig->getAllWeekDays($_POST['pricingWTW']['idAvailableDay']);
            $_POST['pricingWTW']['idAvailableDay_dis']=$weekDaysArys[0]['szWeekDay'];
	}
	$dtValidFrom='';
	if($pricingWtwAry[0]['dtValidFrom']!='0000-00-00 00:00:00' && !empty($pricingWtwAry[0]['dtValidFrom']) && $pricingWtwAry[0]['dtValidFrom']>date('Y-m-d'))
        { 
            $dtValidFrom=date('d/m/Y',strtotime($pricingWtwAry[0]['dtValidFrom']));
	}
	else if($_POST['pricingWTW']['dtValidFrom']!='0000-00-00 00:00:00' && $_POST['pricingWTW']['dtValidFrom']!='')
	{	
            $dtValidFrom=$_POST['pricingWTW']['dtValidFrom'];
	}
	else
	{
            $dtValidFrom=date('d/m/Y');
	}
	
	$$dtExpiry='';
	if($pricingWtwAry[0]['dtExpiry']!='0000-00-00 00:00:00' && !empty($pricingWtwAry[0]['dtExpiry']))
        { 
            $dtExpiry=date('d/m/Y',strtotime($pricingWtwAry[0]['dtExpiry']));
	}
	else if($_POST['pricingWTW']['dtExpiry']!='0000-00-00 00:00:00' && $_POST['pricingWTW']['dtExpiry']!='')
	{
            $dtExpiry=$_POST['pricingWTW']['dtExpiry'];
	}
        
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $szPerWM = "Per KG";
            $szOriginChargeTipHeading = "";
            $szOriginChargeTipHeading = t($t_base.'messages/AIR_FREIGHT_ORIGIN_CHARGES_TOOL_TIP_HEADER_TEXT');
            $szFreightChargeTipHeading = t($t_base.'messages/AIR_FREIGHT_RATE_TOOL_TIP_HEADER_TEXT'); 
            $szDestinationChargeTipHeading = t($t_base.'messages/AIR_DESTINATION_CHARGES_TOOL_TIP_HEADER_TEXT'); 
            $szFrequencyTipHeading = t($t_base.'messages/AIR_SERVICE_FREQUENCY_TOOL_TIP_HEADER_TEXT'); 
            $szFrequencyTipText = t($t_base.'messages/AIR_SERVICE_FREQUENCY_TOOL_TIP_TEXT');
            $szCutOffTitle = t($t_base.'fields/airpot_cut_off_day');  
            $szCutoffTipHeading = t($t_base.'messages/AIRPORT_CUT_OFF_TOOL_TIP_HEADER_TEXT');
            $szTransitDaysTitle = t($t_base.'fields/airport_transit_hour'); 
            $szTransitDaysTipHeading = t($t_base.'messages/AIRPORT_TRANSIT_TOOL_TIP_HEADER_TEXT');
            $szAvailabelDaysTitle = t($t_base.'fields/airport_available_day'); 
            $szAvailableTipHeading = t($t_base.'messages/AIR_CFS_AVAILABLE_TOOL_TIP_HEADER_TEXT');
            $szIllustrationTitle = t($t_base.'title/illustration_of_air_service');
            $szImageName = "CargoFlowAirfreight.png"; 
        }
        else
        {
            $szPerWM = t($t_base.'fields/per_wm'); 
            $szOriginChargeTipHeading = t($t_base.'messages/ORIGIN_CHARGES_TOOL_TIP_HEADER_TEXT');
            $szFreightChargeTipHeading = t($t_base.'messages/FRAIGHT_RATE_TOOL_TIP_HEADER_TEXT'); 
            $szDestinationChargeTipHeading = t($t_base.'messages/DESTINATION_CHARGES_TOOL_TIP_HEADER_TEXT'); 
            $szFrequencyTipHeading = t($t_base.'messages/SERVICE_FREQUENCY_TOOL_TIP_HEADER_TEXT'); 
            $szFrequencyTipText = "";
            $szCutOffTitle = t($t_base.'fields/cut_off_day'); 
            $szCutoffTipHeading = t($t_base.'messages/CFS_CUT_OFF_TOOL_TIP_HEADER_TEXT');

            $szTransitDaysTitle = t($t_base.'fields/transit_hour'); 
            $szTransitDaysTipHeading = t($t_base.'messages/CFS_TRANSIT_TOOL_TIP_HEADER_TEXT');
            $szAvailabelDaysTitle = t($t_base.'fields/available_day');  
            $szAvailableTipHeading = t($t_base.'messages/CFS_AVAILABLE_TOOL_TIP_HEADER_TEXT');
            $szIllustrationTitle = t($t_base.'title/illustration_of_lcl');
            $szImageName = "CargoFlowLCL.png"; 
        }
	?>	
<script type="text/javascript">
function initDatePicker() 
{
	$("#datepicker1").datepicker();
	$("#datepicker2").datepicker();
}
initDatePicker();
</script>
<form name="obo_lcl_bottom_form" id="obo_lcl_bottom_form" method="post">
	<div id="obo_lcl_error"  class="errorBox" style="display:none;"></div>
	<br/>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/origin_freight_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?php echo $szOriginChargeTipHeading; ?>','<?=t($t_base.'messages/ORIGIN_CHARGES_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?php echo $szPerWM;?></span><br /><input <?=$disabled_str?> name="pricingWTW[fOriginRateWM]" id="fOriginRateWM" value="<?=$_POST['pricingWTW']['fOriginRateWM'] ? $_POST['pricingWTW']['fOriginRateWM'] : $pricingWtwAry[0]['fOriginChargeRateWM'] ?>" type="text" onkeyup="check_applicable_charges_checkbox('ORIGIN');" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="pricingWTW[fOriginMinRateWM]" id="fOriginMinRateWM" value="<?=$_POST['pricingWTW']['fOriginMinRateWM'] ? $_POST['pricingWTW']['fOriginMinRateWM'] :$pricingWtwAry[0]['fOriginChargeMinRateWM']?>" onkeyup="check_applicable_charges_checkbox('ORIGIN');" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="pricingWTW[fOriginRate]" id="fOriginRate" value="<?=$_POST['pricingWTW']['fOriginRate'] ? $_POST['pricingWTW']['fOriginRate'] :$pricingWtwAry[0]['fOriginChargeBookingRate']?>" size="10" onkeyup="check_applicable_charges_checkbox('ORIGIN');" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
                    <select size="1" <?=$disabled_str?> name="pricingWTW[szOriginFreightCurrency]" id="szOriginFreightCurrency" onchange="check_applicable_charges_checkbox('ORIGIN');" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
                            if(!empty($allCurrencyArr))
                            {
                                foreach($allCurrencyArr as $allCurrencyArrs)
                                {
                                    ?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['pricingWTW']['szOriginFreightCurrency'])?$_POST['pricingWTW']['szOriginFreightCurrency']:$pricingWtwAry[0]['szOriginChargeCurrency']) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                                }
                            }
			?>
                    </select>
		</p>
		<input name="pricingWTW[iOriginChargesApplicable]" id="iOriginChargesApplicable" value="<?=$_POST['pricingWTW']['iOriginChargesApplicable'] ? $_POST['pricingWTW']['iOriginChargesApplicable'] :$pricingWtwAry[0]['iOriginChargesApplicable']?>" type="hidden" />
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/freight_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szFreightChargeTipHeading;?>','<?=t($t_base.'messages/FRAIGHT_RATE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="pricingWTW[fRateWM]" id="fRateWM" value="<?=$_POST['pricingWTW']['fRateWM'] ? $_POST['pricingWTW']['fRateWM'] :$pricingWtwAry[0]['fFreightRateWM']?>" type="text" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="pricingWTW[fMinRateWM]" id="fMinRateWM" value="<?=$_POST['pricingWTW']['fMinRateWM'] ? $_POST['pricingWTW']['fMinRateWM'] :$pricingWtwAry[0]['fFreightMinRateWM']?>" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="pricingWTW[fRate]" id="fRate" value="<?=$_POST['pricingWTW']['fRate'] ? $_POST['pricingWTW']['fRate'] :$pricingWtwAry[0]['fFreightBookingRate']?>" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="pricingWTW[szFreightCurrency]" id="szFreightCurrency" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
                            if(!empty($allCurrencyArr))
                            {
                                foreach($allCurrencyArr as $allCurrencyArrs)
                                {
                                    ?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['pricingWTW']['szFreightCurrency'])?$_POST['pricingWTW']['szFreightCurrency']:$pricingWtwAry[0]['szFreightCurrency']) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                                }
                            }
			?>
			</select>
		</p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/destination_freight_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szDestinationChargeTipHeading;?>','<?=t($t_base.'messages/DESTINATION_CHARGES_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="pricingWTW[fDestinationRateWM]" id="fDestinationRateWM" value="<?=$_POST['pricingWTW']['fDestinationRateWM'] ? $_POST['pricingWTW']['fDestinationRateWM'] :$pricingWtwAry[0]['fDestinationChargeRateWM']?>" type="text" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="pricingWTW[fDestinationMinRateWM]" id="fDestinationMinRateWM" value="<?=$_POST['pricingWTW']['fDestinationMinRateWM'] ? $_POST['pricingWTW']['fDestinationMinRateWM'] :$pricingWtwAry[0]['fDestinationChargeMinRateWM']?>" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="pricingWTW[fDestinationRate]" id="fDestinationRate" value="<?=$_POST['pricingWTW']['fDestinationRate'] ? $_POST['pricingWTW']['fDestinationRate'] :$pricingWtwAry[0]['fDestinationChargeBookingRate']?>" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
                <select size="1" <?=$disabled_str?> name="pricingWTW[szDestinationFreightCurrency]" id="szDestinationFreightCurrency" onchange = "check_applicable_charges_checkbox('DESTINATION');">
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($allCurrencyArr))
                        {
                            foreach($allCurrencyArr as $allCurrencyArrs)
                            {
                                ?><option value="<?=$allCurrencyArrs['id']?>" <?=((($_POST['pricingWTW']['szDestinationFreightCurrency'])?$_POST['pricingWTW']['szDestinationFreightCurrency']:$pricingWtwAry[0]['szDestinationChargeCurrency']) ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
            <input name="pricingWTW[iDestinationChargesApplicable]" id="iDestinationChargesApplicable" value="<?=(int)$_POST['pricingWTW']['iDestinationChargesApplicable'] ? $_POST['pricingWTW']['iDestinationChargesApplicable'] :$pricingWtwAry[0]['iDestinationChargesApplicable']?>" type="hidden" />
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/validity');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/VALIDITY_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/VALIDITY_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/from');?></span><br /><input size="10" <?=$disabled_str?> id="datepicker1" name="pricingWTW[dtValidFrom]" type="text" value="<?=$dtValidFrom?>"></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/to');?></span><br /><input size="10" <?=$disabled_str?> id="datepicker2" name="pricingWTW[dtExpiry]" type="text" value="<?=$dtExpiry?>"></p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/booking_cut_off');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/BOOKING_CUT_OFF_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-20"><span class="f-size-12">&nbsp;</span><br />
                <select size="1" style="width:90px;" <?=$disabled_str?> name="pricingWTW[iBookingCutOffHours]" id="iBookingCutOffHours" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($cuttOffAry))
                        {
                            foreach($cuttOffAry as $key=>$cuttOffArys)
                            {
                                ?><option value="<?=$key?>" <?=((($_POST['pricingWTW']['iBookingCutOffHours'])?$_POST['pricingWTW']['iBookingCutOffHours']:$pricingWtwAry[0]['iBookingCutOffHours']) ==  $key ) ? "selected":""?>><?=$cuttOffArys?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=t($t_base.'fields/service_frequency');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szFrequencyTipHeading;?>','<?php echo $szFrequencyTipText; ?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-20"><span class="f-size-12">&nbsp;</span><br />
                <select size="1" style="width:90px;" <?=$disabled_str?> name="pricingWTW[idFrequency]" id="idFrequency" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($frequencyAry))
                        {
                            foreach($frequencyAry as $frequencyArys)
                            {
                                ?><option value="<?=$frequencyArys['id']?>" <?=((($_POST['pricingWTW']['idFrequency'])?$_POST['pricingWTW']['idFrequency']:$pricingWtwAry[0]['idFrequency']) ==  $frequencyArys['id'] ) ? "selected":""?>><?=$frequencyArys['szDescription']?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=$szCutOffTitle;?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szCutoffTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15" ><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/weekday');?></span><br />
                <select size="1" style="width:90px;" <?=$disabled_str?> name="pricingWTW[idCutOffDay]" onchange="getAvailableDay();" id="idCutOffDay" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
			<?php
                            if(!empty($weekDaysAry))
                            {
                                foreach($weekDaysAry as $weekDaysArys)
                                {
                                    ?><option value="<?=$weekDaysArys['id']?>" <?=((($_POST['pricingWTW']['idCutOffDay'])?$_POST['pricingWTW']['idCutOffDay']:$pricingWtwAry[0]['idCutOffDay']) ==  $weekDaysArys['id'] ) ? "selected":""?>><?=$weekDaysArys['szWeekDay']?></option><?php
                                }
                            }
			?>
                </select>
            </p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/local_time');?></span><br />
                <select size="1" style="width:90px;" <?=$disabled_str?> name="pricingWTW[szCutOffLocalTime]" id="szCutOffLocalTime" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
			<?php
                            if(!empty($cutoffLocalTimeAry))
                            {
                                foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
                                {
                                    ?><option value="<?=$cutoffLocalTimeArys?>" <?=((($_POST['pricingWTW']['szCutOffLocalTime'])?$_POST['pricingWTW']['szCutOffLocalTime']:$pricingWtwAry[0]['szCutOffLocalTime']) ==  $cutoffLocalTimeArys ) ? "selected":""?>><?=$cutoffLocalTimeArys?></option><?php
                                }
                            }
			?>
                </select>
            </p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=$szTransitDaysTitle; ?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szTransitDaysTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/days');?></span><br /><input <?=$disabled_str?> id="iTransitHours"  name="pricingWTW[iTransitHours]" value="<?=$_POST['pricingWTW']['iTransitHours'] ? $_POST['pricingWTW']['iTransitHours'] :$pricingWtwAry[0]['iTransitDays']?>" onblur="getAvailableDay();" type="text" size="10" /></p>
	</div>
	<div class="oh">
            <p class="fl-40 gap-text-field"><?=$szAvailabelDaysTitle;?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szAvailableTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
            <p class="fl-15" ><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/weekday');?></span><br />
                <input type="text" size="10" disabled name="pricingWTW[idAvailableDay_dis]" value="<?=$_POST['pricingWTW']['idAvailableDay_dis'] ? $_POST['pricingWTW']['idAvailableDay_dis'] :$pricingWtwAry[0]['szAvailableDay']?>" id="idAvailableDay" />			
                <input type="hidden" name="pricingWTW[idAvailableDay]" id="idAvailableDay_hidden" value="<?=$_POST['pricingWTW']['idAvailableDay'] ? $_POST['pricingWTW']['idAvailableDay'] :$pricingWtwAry[0]['idAvailableDay']?>" />
            </p>
            <p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/local_time');?></span><br />
                <select size="1" style="width:90px;" <?=$disabled_str?> name="pricingWTW[szAvailableLocalTime]" id="szAvailableLocalTime" >
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($cutoffLocalTimeAry))
                        {
                            foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
                            {
                                ?><option value="<?=$cutoffLocalTimeArys?>"  <?=((($_POST['pricingWTW']['szAvailableLocalTime'])?$_POST['pricingWTW']['szAvailableLocalTime']:$pricingWtwAry[0]['szAvailableLocalTime']) ==  $cutoffLocalTimeArys ) ? "selected":""?>><?=$cutoffLocalTimeArys?></option><?php
                            }
                        }
                    ?>
                </select>
            </p>
	</div> 
	<br />
	<div class="oh">
		<p class="fl-30" style="margin-top: 8px;">Click to move to next</p>
		<p class="fl-40">		
		<a href="javascript:void(0)"  class="button2" onclick="delete_pricing_wtw_appoving_data('<?=$pricingWtwAry[0]['id']?>','<?=$idForwarder?>');"><span><?=t($t_base_approve.'fields/delete');?></span></a>
		 <a href="javascript:void(0);" class="button1" id="approve_button" onclick="approved_pricing_wtw_data('<?=$pricingWtwAry[0]['id']?>','<?=$idForwarder?>');"><span><?=t($t_base_approve.'fields/approve');?></span></a></p>	
	</div>
	<input type="hidden" name="pricingWTW[idOriginWarehouse]" id="idOriginWarehouse_hidden" value="<?=$_POST['pricingWTW']['idOriginWarehouse'] ? $_POST['pricingWTW']['idOriginWarehouse'] :$idOriginWarehouse?>">
	<input type="hidden" name="pricingWTW[idDestinationWarehouse]" id="idDestinationWarehouse_hidden" value="<?=$_POST['pricingWTW']['idDestinationWarehouse'] ? $_POST['pricingWTW']['idDestinationWarehouse'] :$idDestinationWarehouse?>">
	<input type="hidden" name="pricingWTW[id]" value="<?=$_POST['pricingWTW']['id'] ? $_POST['pricingWTW']['id'] :$pricingWtwAry[0]['id']?>" id="idEdit">
</form>
<hr>
<h5><?=$szIllustrationTitle?></h5>
<canvas style="margin-left:-10px;" id="myCanvas" width="730" height="180"></canvas>
<script type="text/javascript">
draw_canvas_image_obo('<?=$szOriginWhsName?>','<?=$szDestinationWhsName?>','<?=__BASE_STORE_IMAGE_URL__.'/'.$szImageName; ?>','OBO');

</script>	
	<?php
}

function haulage_review_data($idForwarder,$idService,$totalPricingServiceHaulageData)
{
	$t_base = "BulkUpload/";
	$t_base_haulage = "HaulaugePricing/";
	$Excel_export_import= new cExport_Import();
	$kUploadBulkService= new cUploadBulkService();
	$kConfig = new cConfig();

	$dataId=$totalPricingServiceHaulageData[0]['idHaulgePricingModel'];
	
	$haulagePricingArr=$kUploadBulkService->totalWaitingToBeApprovedPricingHaulage($idForwarder,$idService,$dataId);
	//print_r($haulagePricingArr);
	
	$haulagePricingModelArr=$kUploadBulkService->selectTempHaulageModelData($dataId);
	$forwardCountriesArr=$Excel_export_import->getForwaderCountries($idForwarder);
	$forwardWareHouseArr=$Excel_export_import->getForwaderWarehouses($idForwarder,$haulagePricingModelArr[0]['idCountry']);

	$forwardCitiesArr=$Excel_export_import->getForwaderWarehousesCity($idForwarder,$haulagePricingModelArr[0]['idCountry']);
	
	$haualgeModelDetailArr=$kUploadBulkService->getPricingModelDetail($dataId);
	?>
		<form name="haulageOBO" id="haulageOBO" method="post">
				<div class="oh haulage-get-data">
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/country')?></span>
						<select name="dataExportArr[haulageCountry]" id="haulageCountry" disabled>
							<option value=""><?=t($t_base.'fields/select')?></option>		
							<?php
										if(!empty($forwardCountriesArr))
										{
											foreach($forwardCountriesArr as $forwardCountriesArrs)
											{
												?>
												<option value="<?=$forwardCountriesArrs['idCountry']?>" <? if($haulagePricingModelArr[0]['idCountry']==$forwardCountriesArrs['idCountry']){?> selected <? }?>><?=$forwardCountriesArrs['szCountryName']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div id='city_warehouse'>
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/city')?></span>
						<select name="dataExportArr[szCity]" disabled id="szCity">
									<option value=""><?=t($t_base.'fields/all');?></option>
									<?php
										if(!empty($forwardCitiesArr))
										{
											foreach($forwardCitiesArr as $forwardCitiesArrs)
											{
												?>
												<option value="<?=$forwardCitiesArrs['szCity']?>" <? if($haulagePricingModelArr[0]['szCity']==$forwardCitiesArrs['szCity']){?> selected <? }?>><?=$forwardCitiesArrs['szCity']?></option>
												<?
											}
										}
										
									
									?>
								</select>
					</div>
					<div class="fl-15 s-field" id="warehouse">
						<span class="f-size-12"><?=t($t_base.'fields/cfs_name')?></span>
						<select name="dataExportArr[haulageWarehouse]" disabled id="haulageWarehouse">
							<option value=""><?=t($t_base.'fields/select');?></option>
							<?php
								if(!empty($forwardWareHouseArr))
								{
									foreach($forwardWareHouseArr as $forwardWareHouseArrs)
									{
										?>
										<option value="<?=$forwardWareHouseArrs['id']?>" <? if($haulagePricingModelArr[0]['idWarehouse']==$forwardWareHouseArrs['id']){?> selected <? }?>><?=$forwardWareHouseArrs['szWareHouseName']?></option>
										<?
									}
								}
							?>
						</select>
					</div>
					</div>
					<div class="fl-15 s-field">
						<span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
						<select name="dataExportArr[idDirection]" disabled id="idDirection">
							<option value='1' <? if($haulagePricingModelArr[0]['iDirection']==1){?> selected <? }?>><?=t($t_base.'fields/export_data')?></option>
							<option value='2' <? if($haulagePricingModelArr[0]['iDirection']==2){?> selected <? }?>><?=t($t_base.'fields/import_data')?></option>
							
						</select>
					</div>
					<div class="fr-20" align="right" style="width: 19%; padding-top: 15px;">
						<p><?=t($t_base_haulage.'fields/click_to_move_to_next')?></p>							
					</div>
				</div>
				<div align="right">
				<a href="javascript:void(0)" class="button2" id="delete_haulage_data" onclick="delete_haulage_appoving_data('<?=$idService?>','<?=$idForwarder?>')"><span id="text_change" style="min-width: 70px;"><?=t($t_base.'fields/delete')?></span></a>
				<a href="javascript:void(0)" class="button1" id="approve_haulage_data"  onclick="approving_haulage_data('<?=$idForwarder?>','<?=$idService?>')"><span id="text_change" style="min-width: 70px;"><?=t($t_base.'fields/approve')?></span></a>
				<input type="hidden" value="<?=$haulagePricingModelArr[0]['idHaulageModel']?>" id="haulageModelApproveData" name="haulageModelApproveData">
					
				</div>
			</form>	
			<hr/>
			<input type="hidden" name="dataCounter" id="dataCounter" value="1">
			<div id="haulage_list_view">
			</div>
			<script>
				show_haulage_pricing_model_data('<?=$idService?>','<?=$idForwarder?>');
			</script>
			<div id="haulage_temp_try_it_out"></div>
<?
}
function cityHaulageList($haulagePricingCityDataArr)
{
$t_base = "HaulaugePricing/";
$kConfig = new cConfig();
?>
		<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_city">
		<tr>
			<!-- <td style="text-align:left;width:15%"><strong><?=t($t_base.'fields/priority')?></strong></td> -->
			<td style="text-align:left;width:21%"><strong><?=t($t_base.'fields/country')?></strong></td>
			<td style="text-align:left;width:21%"><strong><?=t($t_base.'fields/city_name')?></strong></td>
			<td style="text-align:left;width:9%"><strong><?=t($t_base.'fields/radius')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/radius_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:14%"><strong><?=t($t_base.'fields/distance_from_cfs')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/distance_from_cfs_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td style="text-align:left;width:14%"><strong><?=t($t_base.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/transit_time_city_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			<td style="text-align:left;width:14%"><strong><?=t($t_base.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fm_factor_for_city_tool_tip_heading');?>','<?=t($t_base.'messages/fm_factor_for_city_tool_tip_text');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			<td style="text-align:left;width:7%"><strong><?=t($t_base.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fuel_tool_tip_city');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>

		</tr>
	<?
	if(!empty($haulagePricingCityDataArr))
	{
		$j=0;
		foreach($haulagePricingCityDataArr as $haulagePricingCityDataArrs)
		{
		
			if($j==0)
			{
				$idPricing=$haulagePricingCityDataArrs['id'];
				$fPricingWmFactor=$haulagePricingCityDataArrs['fWmFactor'];
				$idPricingHaulageModel=$haulagePricingCityDataArrs['idHaulageModel'];
				$idPricingWarehouse=$haulagePricingCityDataArrs['idWarehouse'];
				$iPricingDirection=$haulagePricingCityDataArrs['iDirection'];
			}
			$CountryName=$kConfig->getCountryName($haulagePricingCityDataArrs['idCountry']);
		?>
			<tr id="haulage_city_<?=++$j?>" onclick="sel_haulage_model_city_data('haulage_city_<?=$j?>','<?=$haulagePricingCityDataArrs['id']?>','<?=$haulagePricingCityDataArrs['idHaulageModel']?>','<?=$j?>','<?=$haulagePricingCityDataArrs['idWarehouse']?>','<?=$haulagePricingCityDataArrs['iDirection']?>','<?=$haulagePricingCityDataArrs['fWmFactor']?>');">
				<!-- <td><?=$haulagePricingCityDataArrs['iPriority']?>&nbsp;Priority</td> -->
				<td><?=$CountryName?></td>
				<td><?=$haulagePricingCityDataArrs['szName']?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['iRadiousKM'])?> <?=t($t_base.'fields/km');?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['iDistanceUpToKm'])?> <?=t($t_base.'fields/km');?></td>
				<td><? 
				if((int)$haulagePricingCityDataArrs['iHours']>24)
				{
					echo "< ".($haulagePricingCityDataArrs['iHours']/24)." ".t($t_base.'fields/days');
				}
				else
				{
					echo "< ".$haulagePricingCityDataArrs['iHours']." ".t($t_base.'fields/hours');
				}
				?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base.'fields/kg')?>/<?=t($t_base.'fields/cbm')?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['fFuelPercentage'],1)?> %</td>
			</tr>
		<?	
		}?>
		<?
	}
	else
	{
	?>
		<tr>
			<td colspan="7" align="center"><?=t($t_base.'messages/no_city')?></td>
		</tr>
	<? }?>
	<input type="hidden" value="<?=$j?>" id="totalActive1" name="totalActive1"/>
	</table>
<?
}
function postcodeHaulageList($haulagePricingPostCodeDataArr)
{
$t_base = "HaulaugePricing/";
$kConfig = new cConfig();
?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_postcode">
		<tr>
			<!--  <td style="text-align:left;width:10%;"><strong><?=t($t_base.'fields/priority')?></strong></td> -->
			<th class="noborder" style="text-align:left;width:19%;"><strong><?=t($t_base.'fields/country')?></strong></th>
			<th style="text-align:left;width:39%;"><strong><?=t($t_base.'fields/postcode_set')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/postcode_set_tool_tip_1');?>','<?=t($t_base.'messages/postcode_set_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
			<th style="text-align:left;width:17%;"><strong><?=t($t_base.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/postcode_transit_time_heading');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
			<th style="text-align:left;width:16%;"><strong><?=t($t_base.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fm_factor_for_postcode_tool_tip_heading');?>','<?=t($t_base.'messages/fm_factor_for_postcode_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
			<th style="text-align:left;width:9%;"><strong><?=t($t_base.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fuel_postcode_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
		</tr>
		
	<?
	if(!empty($haulagePricingPostCodeDataArr))
	{
		$j=0;
		foreach($haulagePricingPostCodeDataArr as $haulagePricingPostCodeDataArrs)
		{
		
			if($j==0)
			{
				$idPricing=$haulagePricingPostCodeDataArrs['id'];
				$fPricingWmFactor=$haulagePricingPostCodeDataArrs['fWmFactor'];
				$idPricingHaulageModel=$haulagePricingPostCodeDataArrs['idHaulageModel'];
				$idPricingWarehouse=$haulagePricingPostCodeDataArrs['idWarehouse'];
				$iPricingDirection=$haulagePricingPostCodeDataArrs['iDirection'];
			}
			$CountryName=$kConfig->getCountryName($haulagePricingPostCodeDataArrs['idCountry']);
		?>
			<tr id="haulage_postcode_<?=++$j?>" onclick="sel_haulage_model_postcode_data('haulage_postcode_<?=$j?>','<?=$haulagePricingPostCodeDataArrs['id']?>','<?=$haulagePricingPostCodeDataArrs['idHaulageModel']?>','<?=$j?>','<?=$haulagePricingPostCodeDataArrs['idWarehouse']?>','<?=$haulagePricingPostCodeDataArrs['iDirection']?>','<?=$haulagePricingPostCodeDataArrs['fWmFactor']?>');">
				<!-- <td><?=$haulagePricingPostCodeDataArrs['iPriority']?>&nbsp;Priority</td>-->
				<td class="noborder"><?=$CountryName?></td>
				<td><?=$haulagePricingPostCodeDataArrs['szName']?></td>
				<td><? 
				if((int)$haulagePricingPostCodeDataArrs['iHours']>24)
				{
					echo "< ".($haulagePricingPostCodeDataArrs['iHours']/24)." ".t($t_base.'fields/days');;
				}
				else
				{
					echo "< ".$haulagePricingPostCodeDataArrs['iHours']." ".t($t_base.'fields/hours');;
				}
				?></td>
				<td><?=number_format((float)$haulagePricingPostCodeDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base.'fields/kg');?>/<?=t($t_base.'fields/cbm');?></td>
				<td><?=number_format((float)$haulagePricingPostCodeDataArrs['fFuelPercentage'],1)?> %</td>
			</tr>
		<?	
		}
	}
	else
	{
	?>
		<tr>
			<td class="noborder" colspan="5" align="center"><?=t($t_base.'messages/no_postcodes');?></td>
		</tr>
	<? }?>
	<input type="hidden" value="<?=$j?>" id="totalActive1" name="totalActive1"/>
	</table>
<?
}

function addHaulagePricingDataTemp($id,$idWarehouse,$iDirection,$idHaulageModel,$idService)
{

	$Excel_export_import= new cExport_Import();
	$kUploadBulkService= new cUploadBulkService();
	$kConfig = new cConfig();
	$haulagePricingDetailDataArr=$kUploadBulkService->getHaulagePricingZoneDetailDataTemp($id,$idService);
	$t_base = "HaulaugePricing/";
	//echo $id."sdsdf";
	$kWHSSearch = new cWHSSearch();
	$kWHSSearch->load($idWarehouse);
	$szWareHouseName=$kWHSSearch->szWareHouseName;
	if($idHaulageModel==1)
	{
		$kHaulagePricing= new cHaulagePricing();
		$arrZone[]=$kHaulagePricing->loadZone($id);
		
		$zonename=$arrZone[0]['szName'];
		if($iDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/from');
			$text_title=$zonename." ".t($t_base.'fields/to')." ".$szWareHouseName;
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$formto=t($t_base.'fields/to');
			$text_title=$szWareHouseName." ".t($t_base.'fields/to')." ".$zonename;
		}
		
		$text_line="<strong>".t($t_base.'title/princing_for')." ".$direction." ".t($t_base.'title/haulage')." ".t($t_base.'fields/from')." ".$text_title."</strong>";
	}
	else if($idHaulageModel==2)
	{
		$kHaulagePricing= new cHaulagePricing();
		$arrCity[]=$kHaulagePricing->loadCity($id);
		$szcity=$arrCity[0]['szName'];
		if($iDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/from');
			$text=t($t_base.'fields/from')." ".$szcity." ".t($t_base.'fields/to')." ".$szWareHouseName;
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$formto=t($t_base.'fields/to');
			$text=t($t_base.'fields/from')." ".$szWareHouseName." ".t($t_base.'fields/to')." ".$szcity;
		}
		$text_line="<strong>".t($t_base.'title/princing_for')." ".$direction." ".t($t_base.'title/haulage')." ".$text."</strong>";
	}
	else if($idHaulageModel==3)
	{
		$kHaulagePricing= new cHaulagePricing();
		$arrPostCode[]=$kHaulagePricing->loadPostCode($id);
		$szPostCode=$arrPostCode[0]['szName'];
		if($iDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/from');
			$text=$szPostCode." ".t($t_base.'fields/to')." ".$szWareHouseName;
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$formto=t($t_base.'fields/from');
			$text=$szWareHouseName." ".t($t_base.'fields/to')." ".$szPostCode;
		}
		$text_line="<strong>".t($t_base.'title/princing_for')." ".$direction." ".t($t_base.'title/haulage')." ".$formto." ".$text."</strong>";
	}
	else if($idHaulageModel==4)
	{
		$kHaulagePricing= new cHaulagePricing();
		$distanceArr[]=$kHaulagePricing->loadDistance($id);
		$iDistanceUpToKm=$distanceArr[0]['iDistanceUpToKm'];
		if($iDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/pick_up');
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$formto=t($t_base.'fields/delivery');
		}
		$text_line="<strong>".t($t_base.'title/princing_for')." ".$formto." ".t($t_base.'title/between')." ".number_format($iDistanceUpToKm)." ".t($t_base.'fields/km')." ".t($t_base.'fields/of')." ".$szWareHouseName."</strong>";
	}
?>
		<p style="margin-bottom:2px;"><?=$text_line?></p>
		<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_temp_detail">
		<tr>
			<td width="20%" style="text-align:left;width:20%"><strong><?=t($t_base.'fields/up_to')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/upto_tool_tip_1');?>','<?=t($t_base.'messages/upto_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_100_kg')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/per_100kg_tool_tip_1');?>','<?=t($t_base.'messages/per_100kg_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/minimum')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/minium_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/per_booking')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/per_booking_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/currency')?></strong> </td>
			<td width="16%" style="text-align:left;width:16%"><strong><?=t($t_base.'fields/fuel_surcharge')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fuel_surcharge_ttool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
		</tr>
		<?
			if(!empty($haulagePricingDetailDataArr))
			{
				$k=0;
				foreach($haulagePricingDetailDataArr as $haulagePricingDetailDataArrs)
				{
				?>
					<tr id="haulage_pricing_temp_data_<?=++$k?>" onclick="sel_haulage_zone_detail_data_temp('haulage_pricing_temp_data_<?=$k?>','<?=$haulagePricingDetailDataArrs['id']?>','<?=$idHaulageModel?>','<?=$haulagePricingDetailDataArrs['iUpToKg']?>','<?=$idWarehouse?>','<?=$iDirection?>','<?=$haulagePricingDetailDataArrs['idHaulagePricingModel']?>','<?=$haulagePricingDetailDataArrs['fWmFactor']?>','<?=$idService?>')">
						<td><?=number_format((float)$haulagePricingDetailDataArrs['iUpToKg'])?> <?=t($t_base.'fields/kg')?> (<?=$factorvalue?> <?=t($t_base.'fields/cbm')?>)</td>
						<td><?=number_format((float)$haulagePricingDetailDataArrs['fPricePer100Kg'],2)?></td>
						<td><?=number_format((float)$haulagePricingDetailDataArrs['fMinimumPrice'],2)?></td>
						<td><?=number_format((float)$haulagePricingDetailDataArrs['fPricePerBooking'],2)?></td>
						<td><?=$haulagePricingDetailDataArrs['szCurrency']?></td>
						<td><?=number_format((float)$haulagePricingDetailDataArrs['fFuelPercentage'],1)?> % <?=t($t_base.'fields/on_top');?></td>
					</tr>
			<?	}
			}else
			{  
		?>
			<tr>
				<td colspan="6" align="center"><? echo "No record found";?></td>
			</tr>
		<? }?>
		</table>
		
		<br/>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td width="45%"></td>
		<td width="55%" align="right"><a href="javascript:void(0)" id="delete_temp_detail" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/delete_line')?></span></a><a href="javascript:void(0)" id="temp_edit_detail" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit')?></span></a></td>
		</tr>
		</table>
		
		<div id="temp_edit_form">
			<?
				add_pricing_haulage_form(array(),$idWarehouse,$iDirection,$idHaulageModel,$id,$idService);
			?>
		</div> 
		
<?php	
}
function add_pricing_haulage_form($editDataArr=array(),$idWarehouse,$iDirection,$idHaulageModel,$id,$idService)
{
	//print_r($_POST);
	$Excel_export_import= new cExport_Import();
	$kUploadBulkService= new cUploadBulkService();
	$kConfig = new cConfig();
	$t_base = "HaulaugePricing/";
	$t_base_error="Error";
	$currencyArr=$Excel_export_import->getFreightCurrency('',true);
	
	if(!empty($_POST['zonePricingArr']))
	{
		if($_POST['zonePricingArr']['mode']=='edit')
		{
			if($kUploadBulkService->updateTempPricingLine($_POST['zonePricingArr']))
			{?>
				<script type="text/javascript">
				$("#loader").attr('style','display:block;');
				save_haulage_temp_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['zonePricingArr']['wmfactor']?>','<?=$_POST['zonePricingArr']['idServiceUpload']?>');
				</script>
				
			<?
				exit();
			}
		}
		
		if($_POST['zonePricingArr']['mode']=='add')
		{
			if($kUploadBulkService->addTempPricingLine($_POST['zonePricingArr']))
			{?>
				<script type="text/javascript">
				$("#loader").attr('style','display:block;');
				save_haulage_temp_line_confirm('<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['zonePricingArr']['wmfactor']?>','<?=$_POST['zonePricingArr']['idServiceUpload']?>');
				</script>
				
			<?php
				exit();
			}
		}
	}
	
$fMinimumPrice='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fMinimumPrice']!='')
	{
		$fMinimumPrice=round($editDataArr[0]['fMinimumPrice'],2);
	}
}
else if(!empty($_POST['zonePricingArr']))
{
	if($_POST['zonePricingArr']['fMinimumPrice']!='')
	{
		$fMinimumPrice=$_POST['zonePricingArr']['fMinimumPrice'];
	}
}
$fPricePerBooking='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePerBooking']!='')
	{
		$fPricePerBooking=round($editDataArr[0]['fPricePerBooking'],2);
	}
}
else if(!empty($_POST['zonePricingArr']))
{
	if($_POST['zonePricingArr']['fPricePerBooking']!='')
	{
		$fPricePerBooking=$_POST['zonePricingArr']['fPricePerBooking'];
	}
}

$fPricePer100Kg='';
if(!empty($editDataArr))
{
	if($editDataArr[0]['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=round($editDataArr[0]['fPricePer100Kg'],2);
	}
}
else if(!empty($_POST['zonePricingArr']))
{
	if($_POST['zonePricingArr']['fPricePer100Kg']!='')
	{
		$fPricePer100Kg=$_POST['zonePricingArr']['fPricePer100Kg'];
	}
}
if(!empty($kUploadBulkService->arErrorMessages)){
?>
<br/>
<div id="regError" class="errorBox ">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kUploadBulkService->arErrorMessages as $key=>$values)
	{
	?><li><?=$values?></li>
	<?php	
	}
?>
</ul>
</div>
</div>
<?php
}
?>

	<form name="updatePricingTempData" id="updatePricingTempData" method="post">
		<!--  <p><?=t($t_base.'fields/update_haulage_rate_line');?></p>-->	
			<table cellspacing="0" cellpadding="0" border="0" width="100%" class="pricingzonedata">
			<tr>
			<td><?=t($t_base.'fields/weight_up_to');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/weight_up_to_tool_tip_heading');?>','<?=t($t_base.'messages/weight_up_to_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			<th align="left" colspan="4" height="48"><?=t($t_base.'fields/Kg');?><br/><input  size="10" type="text" name="zonePricingArr[iUpToKg]" id="iUpToKg" value="<?=(($_POST['zonePricingArr']['iUpToKg'])?$_POST['zonePricingArr']['iUpToKg']:$editDataArr[0]['iUpToKg'])?>"  size="12" onblur="open_save_button('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
			</tr>
			<tr>
				<td width="40%"><?=t($t_base.'fields/haulage_rate');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_rate_tool_tip_heading');?>','<?=t($t_base.'messages/haulage_rate_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<th width="15%" align="left" ><? echo "Per ".$weight." ".t($t_base.'fields/kg')?><br/><input type="text" size="10" name="zonePricingArr[fPricePer100Kg]" id="fPricePer100Kg" value="<?=$fPricePer100Kg?>" size="12" onblur="open_save_button_temp('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/minimum');?><br/><input type="text"  size="10" name="zonePricingArr[fMinimumPrice]" id="fMinimumPrice" value="<?=$fMinimumPrice?>" size="12" onblur="open_save_button_temp('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/per_booking');?><br/><input type="text"  size="10" name="zonePricingArr[fPricePerBooking]" id="fPricePerBooking" value="<?=$fPricePerBooking?>" size="12" onblur="open_save_button_temp('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');"/></th>
				<th width="15%" align="left"><?=t($t_base.'fields/currency');?><br/><select size="1" name="zonePricingArr[idCurrency]" id="idCurrency"  size="25" onchange="open_save_button_temp('<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>');">
						<option value=" "><?=t($t_base.'fields/select');?></option>
						<? if(!empty($currencyArr))
							{
								foreach($currencyArr as $currencyArrs)
								{?>
									<option value="<?=$currencyArrs['id']?>" <?=((($_POST['zonePricingArr']['idCurrency'])?$_POST['zonePricingArr']['idCurrency']:$editDataArr[0]['idCurrency']) ==  $currencyArrs['id'] ) ? "selected":""?>><?=$currencyArrs['szCurrency']?></option>
								<?}
									
							}
						?>
					</select></th>
			</tr>
			<tr><td style="height:10px;line-height:10px;" colspan="5"></td></tr>
			<tr>
				<td colspan="3" align="right">
				<input type="hidden" name="zonePricingArr[mode]" id="mode" value="<?=(($_POST['zonePricingArr']['mode'])?$_POST['zonePricingArr']['mode']:"add")?>">
				<input type="hidden" name="zonePricingArr[idEdit]" id="idEdit" value="<?=(($_POST['zonePricingArr']['idEdit'])?$_POST['zonePricingArr']['idEdit']:$editDataArr[0]['id'])?>">
				<input type="hidden" name="zonePricingArr[idHaulagePricingModel]" id="idHaulagePricingModel" value="<?=(($_POST['zonePricingArr']['idHaulagePricingModel'])?$_POST['zonePricingArr']['idHaulagePricingModel']:$id)?>">
				<input type="hidden" name="zonePricingArr[wmfactor]" id="wmfactor" value="<?=(($_POST['zonePricingArr']['wmfactor'])?$_POST['zonePricingArr']['wmfactor']:$wmfactor)?>">
				<input type="hidden" name="zonePricingArr[idServiceUpload]" id="idServiceUpload" value="<?=(($_POST['zonePricingArr']['idServiceUpload'])?$_POST['zonePricingArr']['idServiceUpload']:$idService)?>">
				Click to update changes</td>
				<td align="right" colspan="2">
				<a href="javascript:void(0)" id="cancel_temp_detail_line" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/cancel');?></span></a><a href="javascript:void(0)" id="save_temp_detail_line" class="button1" style="opacity:0.4"><span style="min-width:70px;"><?=t($t_base.'fields/save');?></span></a></td>
			</tr>			
			</table>			
		</form>
<?php
if(!empty($kUploadBulkService->arErrorMessages)){
	if($_POST['zonePricingArr']['mode']=='edit'){?>
<script>

$("#cancel_temp_detail_line").unbind("click");
$("#cancel_temp_detail_line").attr('style',"opacity:1");
$("#cancel_temp_detail_line").click(function(){cancel_haulage_zone_line_temp('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>','<?=$_POST['iUpToKg1']?>')});


$("#save_temp_detail_line").unbind("click");
$("#save_temp_detail_line").attr('style',"opacity:1");
$("#save_temp_detail_line").click(function(){save_haulage_zone_line_temp('<?=$_POST['id']?>','<?=$_POST['idHaulageModel']?>','<?=$_POST['idWarehouse']?>','<?=$_POST['idDirection']?>','<?=$_POST['idHaulagePricingModel']?>')});
	
</script>
<?php }else if($_POST['zonePricingArr']['mode']=='add'){?>
<script>
$("#save_temp_detail_line").unbind("click");
$("#save_temp_detail_line").attr('style',"opacity:1");
$("#save_temp_detail_line").click(function(){add_haulage_pricing_line_temp('<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idHaulageModel?>','<?=$id?>')});
</script>
<?php }}
}
function cfs_location_review_data($idForwarder,$idService=0,$totalCFSLocationData,$idBatch=0,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
{
    $t_base = "BulkUpload/";
    $t_base_error="Error";
    $kUploadBulkService = new cUploadBulkService();
    $kWHSSearch= new cWHSSearch(); 
    $dataid = $totalCFSLocationData[0];  
    $CFSLocationDataArr = $kUploadBulkService->totalWaitingToBeApprovedCFSLocation($idForwarder,$idService,$dataid,$idBatch,$iWarehouseType);
                    
    $kConfig = new cConfig();
    $allCountriesArr=$kConfig->getAllCountries(true); 
	
    if(empty($_POST['cfsAddEditArr']))
    {
        if(!empty($CFSLocationDataArr[0]))
        {
            $kWHSSearch->setUploadServiceCFSError($CFSLocationDataArr[0],$iWarehouseType);

            if(!empty($kWHSSearch->arErrorMessages)){
                ?>
                <div id="regError" class="errorBox ">
                <div class="header"><?=t($t_base_error.'/please_following');?></div>
                <div id="regErrorList">
                <ul>
                <?php
                        foreach($kWHSSearch->arErrorMessages as $key=>$values)
                        {
                        ?><li><?=$values?></li>
                        <?php	
                        }
                ?>
                </ul>
                </div>
                </div>
                <?php }}
        }
	$flag=0;
	if($CFSLocationDataArr[0]['idCountry']!=NULL && ($CFSLocationDataArr[0]['szLatitude']==NULL && $CFSLocationDataArr[0]['szLongitude']==NULL))
	{
            $latLong = $kWHSSearch->getLatitudeLongitudeForCountry($CFSLocationDataArr[0]['idCountry']);
            $CFSLocationDataArr[0]['szLatitude'] = $latLong['szLatitude'];
            $CFSLocationDataArr[0]['szLongitude'] = $latLong['szLongitude'];
            $flag=true;
	}
	
	$countryId  = $_POST['cfsAddEditArr']['szCountry'] ? $_POST['cfsAddEditArr']['szCountry'] : $CFSLocationDataArr[0]['idCountry'];
	$szLatitute  = $_POST['cfsAddEditArr']['szLatitude'] ? $_POST['cfsAddEditArr']['szLatitude'] : $CFSLocationDataArr[0]['szLatitude'];
	$szLongitude = $_POST['cfsAddEditArr']['szLongitude'] ? $_POST['cfsAddEditArr']['szLongitude'] : $CFSLocationDataArr[0]['szLongitude'];
        $iWarehouseType = $_POST['cfsAddEditArr']['iWarehouseType'] ? $_POST['cfsAddEditArr']['iWarehouseType'] : $iWarehouseType;
        
	
        $selectListCFS = array();
        if(!empty($_POST['cfsAddEditArr']['selectNearCFS']))
        {
            $selectListCFS = sanitize_all_html_input($_POST['cfsAddEditArr']['selectNearCFS']);
            $selectListCFS = explode(',',$selectListCFS);
        }
	
	if(!empty($_POST['cfsAddEditArr']['szPhoneNumberUpdate']))
	{
            $_POST['cfsAddEditArr']['szPhone'] = urldecode(base64_decode($_POST['cfsAddEditArr']['szPhoneNumberUpdate']));
	}
        
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        { 
            $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text_airport_cfs'); 
            $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text_airport_warehouse');
            $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text_airport_cfs');
            $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text_airport_cfs'); 
        }
        else
        { 
            $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text'); 
            $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text');
            $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text'); 
            $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text');
        } 
        $szCfsNameBluText = t($t_base.'messages/cfs_name_blue_box_text'); 
	?>
	<form name="addEditWarehouse" id="addEditWarehouse" method="post" >
            <label class="profile-fields">
                <span class="field-name"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'fields/airport_warehouse'):t($t_base.'fields/cfs_name');?></span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_name');" onfocus="openTip('cfs_name');" name="cfsAddEditArr[szWareHouseName]" id="szCFSName" value="<?=$_POST['cfsAddEditArr']['szWareHouseName'] ? $_POST['cfsAddEditArr']['szWareHouseName'] : $CFSLocationDataArr[0]['szWareHouseName'] ?>"/></span>
                <div class="field-alert"><div id="cfs_name" style="display:none;"><?php echo $szCfsNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/address_line');?> 1</span>
                <span class="field-container"><input type="text" onblur="closeTip('address_1');" onfocus="openTip('address_1');" name="cfsAddEditArr[szAddress1]" id="szAddressLine1" value="<?=$_POST['cfsAddEditArr']['szAddress1'] ? $_POST['cfsAddEditArr']['szAddress1'] : $CFSLocationDataArr[0]['szAddress1'] ?>"/></span>
                <div class="field-alert"><div id="address_1" style="display:none;"><?php echo $szAddressNameBluText; ?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/address_line');?> 2&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input onblur="closeTip('address_2');" onfocus="openTip('address_2');"  type="text" name="cfsAddEditArr[szAddress2]" id="szAddressLine2" value="<?=$_POST['cfsAddEditArr']['szAddress2'] ? $_POST['cfsAddEditArr']['szAddress2'] : $CFSLocationDataArr[0]['szAddress2'] ?>"/></span>
                <div class="field-alert"><div id="address_2" style="display:none;"><?php echo $szAddressNameBluText; ?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/address_line');?> 3&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input type="text" onblur="closeTip('address_3');" onfocus="openTip('address_3');" name="cfsAddEditArr[szAddress3]" id="szAddressLine3" value="<?=$_POST['cfsAddEditArr']['szAddress3'] ? $_POST['cfsAddEditArr']['szAddress3'] : $CFSLocationDataArr[0]['szAddress3'] ?>"/></span>
                <div class="field-alert"><div id="address_3" style="display:none;"><?php echo $szAddressNameBluText; ?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/postcode');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_post_code');" onfocus="openTip('cfs_post_code');" name="cfsAddEditArr[szPostCode]" id="szOriginPostCode" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  value="<?=$_POST['cfsAddEditArr']['szPostCode'] ? $_POST['cfsAddEditArr']['szPostCode'] : $CFSLocationDataArr[0]['szPostCode'] ?>"/></span>
                <div class="field-alert"><div id="cfs_post_code" style="display:none;"><?php echo $szAddressNameBluText; ?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/city');?></span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_city');" onfocus="openTip('cfs_city');" name="cfsAddEditArr[szCity]" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')" id="szOriginCity" value="<?=$_POST['cfsAddEditArr']['szCity'] ? $_POST['cfsAddEditArr']['szCity'] : $CFSLocationDataArr[0]['szCity'] ?>"/></span>
                <div class="field-alert"><div id="cfs_city" style="display:none;"><?php echo $szAddressNameBluText; ?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/province');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_state');" onfocus="openTip('cfs_state');" name="cfsAddEditArr[szState]" id="szState" value="<?=$_POST['cfsAddEditArr']['szState'] ? $_POST['cfsAddEditArr']['szState'] : $CFSLocationDataArr[0]['szState'] ?>"/></span>
                <div class="field-alert"><div id="cfs_state" style="display:none;"><?php echo $szAddressNameBluText; ?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/country');?></span>
                <span class="field-container">
                    <select onchange="showCountriesByCountryCFSBulkUpload(this.value);" size="1" onblur="closeTip('cfs_country');" onfocus="openTip('cfs_country');" name="cfsAddEditArr[szCountry]" id="szCountry">
                        <option>Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['cfsAddEditArr']['szCountry'])?$_POST['cfsAddEditArr']['szCountry']:$CFSLocationDataArr[0]['idCountry']) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option><?php
                                }
                            }
                        ?>
                    </select>
                </span>
                <div class="field-alert"><div id="cfs_country" style="display:none;"><?php echo $szAddressNameBluText; ?></div></div>
            </label> 
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/phone_number');?></span>
                <input type="hidden" name="cfsAddEditArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="">
                <span class="field-container"><input type="text" onblur="closeTip('cfs_phone');" onfocus="openTip('cfs_phone');" name="cfsAddEditArr[szPhone]" id="szPhoneNumber" value="<?=$_POST['cfsAddEditArr']['szPhone'] ? $_POST['cfsAddEditArr']['szPhone'] : $CFSLocationDataArr[0]['szPhone'] ?>"/></span>
                <div class="field-alert"><div id="cfs_phone" style="display:none;"><?php echo$szPhoneNumberBluText; ?></div></div>
            </label>
            <br /> 	
            <h5><strong><?=t($t_base.'fields/exact_location');?></strong> <!--<a href="javascript:void(0);" onclick="open_help_me_select_popup()" class="f-size-14">Help me find this</a>--></h5>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/latitude');?></span>
                <span class="field-container">
                    <input type="text" size='5' onblur="closeTip('cfs_lat');reset1();" onfocus="openTip('cfs_lat');" name="cfsAddEditArr[szLatitude]" id="szLatitude" value="<?=$_POST['cfsAddEditArr']['szLatitude'] ? $_POST['cfsAddEditArr']['szLatitude'] : $CFSLocationDataArr[0]['szLatitude'] ?>"/>
                </span>		
                <div class="field-alert1"><div id="cfs_lat" style="display:none;"><?php echo $szLatitudeBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/longitude');?></span>
                <span class="field-container">
                    <input type="text" onblur="closeTip('cfs_long');reset1();" onfocus="openTip('cfs_long');" size='5' name="cfsAddEditArr[szLongitude]" id="szLongitude" value="<?=$_POST['cfsAddEditArr']['szLongitude'] ? $_POST['cfsAddEditArr']['szLongitude'] : $CFSLocationDataArr[0]['szLongitude'] ?>"/>
                </span>
                <div class="field-alert1"><div id="cfs_long" style="display:none;"><?php echo $szLongitudeBluText;?></div></div>
            </label>
            <br/>
            <input type="hidden" name="cfsAddEditArr[tempDataId]" value="<?=$_POST['cfsAddEditArr']['tempDataId'] ? $_POST['cfsAddEditArr']['tempDataId'] : $dataid ?>">
            <input type="hidden" id="CFSListing" name="cfsAddEditArr[selectNearCFS]" value="<?=$_POST['cfsAddEditArr']['selectNearCFS'] ? $_POST['cfsAddEditArr']['selectNearCFS'] :$CFSLocationDataArr[0]['idCountry'];?>">
            <input type="hidden" id="iWarehouseType" name="cfsAddEditArr[iWarehouseType]" value="<?=$_POST['cfsAddEditArr']['iWarehouseType'] ? $_POST['cfsAddEditArr']['iWarehouseType'] :$iWarehouseType;?>">
        </form>
        <div class="map_note" align="left" style="position: relative;">
        <iframe name="draw_map_iframe" id="draw_map_iframe" width="719" height="303px" scrolling="no" src="<?=__BASE_URL__?>/cfsLocationBulkMap.php?szLatitude=<?=$szLatitute?>&szLongitude=<?=$szLongitude?>&flag=<?=$flag?>"></iframe>
        <p><?=t($t_base.'messages/note_heading');?>: <?=t($t_base.'messages/note_for_forwarder_gmaps');?> </p>
        </div>    	 
    	 <div id="bottom-form">
    	 <?php $countryList = $kWHSSearch->findWarehouseCountryList($countryId,$szLatitute,$szLongitude);?>
	    	<div id="selectForCountrySelection">
	    	 <?php //$selectListCFS = array(1,2,3);
                    if($countryId!= NULL && empty($selectListCFS))
                    {
                        $selectListCFS = array($countryId);
                    }
	    	 ?>
                <h4><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_this_airport_warehouse'):t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
                <p style="margin-top: 5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/add_local_market_airport_cfs'):t($t_base.'messages/add_local_market');?></p>
	    	<div style="direction: ltr;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td style="padding-top:5px;" valign="top" width="215"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_airport_warehouse'):t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>  	
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			<?php
			if(!empty($allCountriesArr))
			{
				foreach($allCountriesArr as $allCountriesArrs)
				{
					if(in_array($allCountriesArrs['id'],$selectListCFS))
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			}
		?>	
	    	 </select>
	    	 <br />
	    	   <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$countryId?>','<?=$szLatitude?>','<?=$szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?> </span></a>
	    	  </td>
	    	 </tr>
	    	 <tr>
                    <td valign="top">
                        <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/sorted_on_airport_cfs'):t($t_base.'messages/sorted_on');?> </p>
                    </td>
	    	<td>	
                    <select  onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
                        <?php
                            if($countryList!=array())
                            {
                                foreach($countryList as $key=>$value)
                                {
                                    if(!in_array($value['id'],$selectListCFS))
                                    {
                                        echo "<option value ='".$value['id']."'>".$value['szCountryName']."</option>";
                                    }
                                }
                            }
                        ?>
                    </select> <br /> 
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
                        <div class="profile-fields" style="margin-top: 14px;float:left;">
                            <div class="field-alert" id="example" style="display:none;">
                            <div>
                                    <p><?=t($t_base.'messages/for_your_rotterdam');?></p>
                                    <br/>
                                    <p><?=t($t_base.'messages/for_your_hong_kong');?></p>
                            </div>
                            </div>
	    	 	</div>
	    	 </div>
		
	    
		<br /><br />
	
	    	 <?php //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>
	     </div>
	     <div style="clear: both;"></div>
    	 <br />
    	 <div>
            <p class="fl-30" align="left" style="padding-top: 24px;"><?=t($t_base.'messages/click_to_move');?></p>
            <p class="fl-40" align="left"  style="padding-top: 16px;">
    	 	<a class="button1" onclick="encode_string('szPhoneNumber','szPhoneNumberUpdate');submit_warehouse_data_bulk('<?=$idForwarder?>','<?=$dataid?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/approve');?></span></a>
    	 	<a class="button2" onclick="delete_cfs_location_appoving_data('<?=$dataid?>','<?=$idForwarder?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/delete');?></span></a>
            </p>
    	 </div>
<!--         
         <form method="post" id="google_map_hidden_form" action="<?=__FORWARDER_HOME_PAGE_URL__?>/googleMap.php" target="google_map_target_cfs_frame">
            <input type="hidden" name="cfsHiddenAry[szPostCode]" id="szPostCode_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szAddressLine1]" id="szAddressLine1_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szAddressLine2]" id="szAddressLine2_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szAddressLine3]" id="szAddressLine3_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szCity]" id="szCity_hidden" value=""> 
            <input type="hidden" name="cfsHiddenAry[szState]" id="szState_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szCountry]" id="szCountry_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szLatitude]" id="szLatitude_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[szLongitude]" id="szLongitude_hidden" value="">
            <input type="hidden" name="cfsHiddenAry[iWarehouseType]" id="iWarehouseType_hidden" value="<?php echo $iWarehouseType; ?>">
            <input type="hidden" name="cfsHiddenAry[iBulkUploadAprroveFlag]" id="iBulkUploadAprroveFlag_hidden" value="1"> 
        </form>
         <div id="popup_container_google_map" style="display:none;">
            <div id="popup-bg"></div>
            <div id="popup-container">
                <div class="popup" style="width:720px;margin-top:30px;">
                    <p class="close-icon" align="right">
                        <a href="javascript:void(0);" onclick="window.top.window.hide_google_map();">
                            <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                        </a>
                    </p>
                    <iframe id="google_map_target_cfs_frame" class="google_map_select"  style="width:720px;"  scrolling="no" name="google_map_target_cfs_frame" src="#" >
                    </iframe>
                </div>
            </div>
        </div>-->
    	 <input type="hidden" name="dataCounter" id="dataCounter" value="1"> 
	<?php
}
function showUploadSerivceComment($allCommentArr,$id)
{
$t_base="BulkUpload/";
?>
<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="multi-bulk-upload-comment-popup popup" style="width:400px;">
			<p class="close-icon" align="right">
			<a onclick="cancel_processing_cost();" href="javascript:void(0);">
			<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
			</a>
			</p>
			<strong><?=t($t_base.'title/comment')?></strong><br /><br />
			<? if(!empty($allCommentArr))
				{
					$kForwarder = new  	cForwarder();
				?>
			<div class="multi-comment-popup" style="border:1px solid #D0D0D0;height:150px;width:400px">
			<? 
					foreach($allCommentArr as $allCommentArrs)
					{
						$kForwarder->load($allCommentArrs['idForwarder']);
					?>
						<!-- <p style="margin-left:7px;"><?=$allCommentArrs['szForwarderContactName']?>, <?=$kForwarder->szDisplayName?>, <?=date('d.F Y',strtotime($allCommentArrs['dtComment']))?></p> -->
						<p><?=str_replace("<br/>","\n",$allCommentArrs['szComment'])?></p>
					<?	
					}
				?>		
			</div>
			<br/>
			<? }
			?>	
			<div>
			<p><?=t($t_base.'title/would_you_like_to_add_a_comment')?></p>
			<textarea cols="50" rows="5" id="szComment" name="szComment" style="width:396px;color:grey;font-style:italic;" onkeydown="blank_me_comment(this.id,'Type to add comments');" onkeyup="active_add_comment_button('<?=$id?>');" onblur="show_me(this.id,'Type to add comments');">Type to add comments</textarea>
			
			</div>
			<br/>
			<p align="center"><a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_comment_cost_processing"><span><?=t($t_base.'fields/add_comment')?></span></a><a href="javascript:void(0)" class="button1" onclick="cancel_processing_cost()" id="cancel_cost_processing"><span><?=t($t_base.'fields/close')?></span></a></p>
			</div>
		</div>
<?php }
function cfsApprovalCompletePopup()
{
	$t_base="BulkUpload/";
?>
	<script>
		$("#loader").attr('style','display:none');
	</script>
	<div id="popup-bg"></div>
		<div id="popup-container">
			<div class="popup signin-popup signin-popup-verification">
				<h5><b><?=t($t_base.'title/reviewing_data_completed');?></b></h5>
				<p><?=t($t_base.'messages/you_have_successfully_reviewed_data')?></p>
				<br />
				<p align="center"><a class="button1" onclick="resetContentOfPage();"><span><?=t($t_base.'fields/ok');?></span></a></p>
			</div>
		</div>
<?php	
}
function haulageMiddleLayer($idHaulageModel,$iDirection,$idWarehouse,$idService,$idForwarder,$haulageApprovalDataArr)
{
	
		$t_base = "BulkUpload/";
		$t_base_haulage = "HaulaugePricing/";
		$Excel_export_import= new cExport_Import();
		$kUploadBulkService= new cUploadBulkService();
		$kHaulagePricing= new cHaulagePricing();
		$kConfig = new cConfig();
		$idHaulagePricingModel=0;

		if($idHaulageModel=='1')
		{
			if($iDirection=='1')
			{
				$formto=t($t_base.'fields/from');
			}
			else
			{
				$formto=t($t_base.'fields/to');
			}
		?>
			<p style="margin-bottom:2px;"><strong><?=t($t_base_haulage.'title/zone_title_1')?> <?=$formto?> <?=t($t_base_haulage.'title/zone_title_2')?></strong></p>
			<div class="clearfix">
			<div style="float:left;border:1px solid #BFBFBF;border-top:0px;width:60%;min-height:229px;max-height:229px;overflow-y:auto;" class="viewhaulagezone" id="view_haulage_model_zone_div">
			<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_zone">
				<tr>
					<!--  <td style="text-align:left;width:10%" class="firsttdnoborder"><strong><?=t($t_base_haulage.'fields/priority')?></strong></td>-->
					<td style="text-align:left;width:40%" class="firsttdnoborder"><strong><?=t($t_base_haulage.'fields/zone')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
					<td style="text-align:left;width:22%"><strong><?=t($t_base_haulage.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/transit_time_zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
					<td style="text-align:left;width:22%"><strong><?=t($t_base_haulage.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/fm_factor_zone_tool_tip_1');?>','<?=t($t_base_haulage.'messages/fm_factor_zone_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
					<td style="text-align:left;width:16%"><strong><?=t($t_base_haulage.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/fuel_zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				</tr>
				<? if(!empty($haulageApprovalDataArr))
					{
						$j=0;
						foreach($haulageApprovalDataArr as $haulageApprovalDataArrs)
						{
							if($j==0)
							{
								$style='style="background:#DBDBDB;color:#828282;"';
								$idHaulagePricingModel=$haulageApprovalDataArrs['id'];
							}
							else
							{
								$style='style="background:#ffffff;color:#000000;"';
							}
						?>	
				<tr <?=$style?> id="haulage_zone_<?=++$j?>" onclick="sel_haulage_model_zone_data_temp('haulage_zone_<?=$j?>','<?=$haulageApprovalDataArrs['id']?>','<?=$idHaulageModel?>','<?=$j?>','<?=$idWarehouse?>','<?=$iDirection?>','<?=$haulageApprovalDataArrs['fWmFactor']?>','<?=$idService?>');">
					<td class="firsttdnoborder"><?=$haulageApprovalDataArrs['szName']?></td>
					<td><? 
					if((int)$haulageApprovalDataArrs['iHours']>24)
					{
						echo "< ".($haulageApprovalDataArrs['iHours']/24)." ".t($t_base_haulage.'fields/days');
					}
					else
					{
						echo "< ".$haulageApprovalDataArrs['iHours']." ".t($t_base_haulage.'fields/hours');
					}
					?></td>
					<td><?=number_format((float)$haulageApprovalDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base_haulage.'fields/Kg')?>/<?=t($t_base_haulage.'fields/cbm')?></td>
					<td><?=number_format((float)$haulageApprovalDataArrs['fFuelPercentage'],1)?> %</td>
				</tr>
				<? 		
						}
					}
				?>
			</table>
			</div>
			<div id="display_zone_map" class="displayzonemap">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
					 <td colspan="5"><iframe style="border:0" name="hssiframe" width="276px" height="228px" src="<?=__BASE_URL__?>/map_polygon.php?idHaulagePricingModel=<?=$idHaulagePricingModel?>"></iframe></td>
					</tr>
				</table>
			</div>
			</div>
		<?	
		}
		else if($idHaulageModel=='2')
		{
			if($iDirection=='1')
			{
				$formto=t($t_base.'fields/from');
			}
			else
			{
				$formto=t($t_base.'fields/to');
			}
		?>
		<p style="margin-bottom: 2px;"><strong><?=t($t_base_haulage.'title/city_title_1')?> <?=$formto?> <?=t($t_base_haulage.'title/city_title_2')?></strong></p>
		<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_city">
			<tr>
				<!-- <td style="text-align:left;width:15%"><strong><?=t($t_base_haulage.'fields/priority')?></strong></td> -->
				<td style="text-align:left;width:21%"><strong><?=t($t_base_haulage.'fields/country')?></strong></td>
				<td style="text-align:left;width:21%"><strong><?=t($t_base_haulage.'fields/city_name')?></strong></td>
				<td style="text-align:left;width:9%"><strong><?=t($t_base_haulage.'fields/radius')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/radius_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td style="text-align:left;width:14%"><strong><?=t($t_base_haulage.'fields/distance_from_cfs')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/distance_from_cfs_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td style="text-align:left;width:14%"><strong><?=t($t_base_haulage.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base_haulage.'messages/transit_time_city_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
				<td style="text-align:left;width:14%"><strong><?=t($t_base_haulage.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base_haulage.'messages/fm_factor_for_city_tool_tip_heading');?>','<?=t($t_base_haulage.'messages/fm_factor_for_city_tool_tip_text');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
				<td style="text-align:left;width:7%"><strong><?=t($t_base_haulage.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base_haulage.'messages/fuel_tool_tip_city');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
			</tr>
		
			<? if(!empty($haulageApprovalDataArr))
				{
					$j=0;
					foreach($haulageApprovalDataArr as $haulageApprovalDataArrs)
					{
						$CountryName=$kConfig->getCountryName($haulageApprovalDataArrs['idCountry']);
							
						if($j==0)
						{
							$style='style="background:#DBDBDB;color:#828282;"';
							$idHaulagePricingModel=$haulageApprovalDataArrs['id'];
						}
						else
						{
							$style='style="background:#ffffff;color:#000000;"';
						}
						?>
			<tr <?=$style?> id="haulage_city_<?=++$j?>" onclick="sel_haulage_model_city_data_temp('haulage_city_<?=$j?>','<?=$haulageApprovalDataArrs['id']?>','<?=$idHaulageModel?>','<?=$j?>','<?=$idWarehouse?>','<?=$iDirection?>','<?=$haulageApprovalDataArrs['fWmFactor']?>','<?=$idService?>');">
				<!-- <td><?=$haulagePricingCityDataArrs['iPriority']?>&nbsp;Priority</td> -->
				<td><?=$CountryName?></td>
				<td><?=$haulageApprovalDataArrs['szName']?></td>
				<td><?=number_format((float)$haulageApprovalDataArrs['iRadiousKM'])?> <?=t($t_base_haulage.'fields/km');?></td>
				<td><?=number_format((float)$haulageApprovalDataArrs['iDistanceUpToKm'])?> <?=t($t_base_haulage.'fields/km');?></td>
				<td><? 
				if((int)$haulageApprovalDataArrs['iHours']>24)
				{
					echo "< ".($haulageApprovalDataArrs['iHours']/24)." ".t($t_base_haulage.'fields/days');
				}
				else
				{
					echo "< ".$haulageApprovalDataArrs['iHours']." ".t($t_base_haulage.'fields/hours');
				}
				?></td>
				<td><?=number_format((float)$haulageApprovalDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base_haulage.'fields/kg')?>/<?=t($t_base_haulage.'fields/cbm')?></td>
				<td><?=number_format((float)$haulageApprovalDataArrs['fFuelPercentage'],1)?> %</td>
			</tr>
			<? 		
					}
				}
			?>
		</table>	
		<?
		}
		else if($idHaulageModel=='3')
		{
		
			if($iDirection=='1')
			{
				$formto=t($t_base.'fields/from');
			}
			else
			{
				$formto=t($t_base.'fields/to');
			}
		?>
		<p style="margin-bottom: 2px;"><strong><span style="text-align:left"><?=t($t_base_haulage.'title/postcode_title_1')?> <?=$formto?> <?=t($t_base_haulage.'title/postcode_title_2')?></span></strong></p>
		<div class="clearfix">
		<div class="tableScroll" style="height:134px;width:76%;float:left;border-top:none;" id="view_haulage_model_postcode_div">
		<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_postcode">
			<tr>
				<!--  <td style="text-align:left;width:10%;"><strong><?=t($t_base.'fields/priority')?></strong></td> -->
				<td class="noborder" style="text-align:left;width:19%;"><strong><?=t($t_base_haulage.'fields/country')?></strong></th>
				<td style="text-align:left;width:39%;"><strong><?=t($t_base_haulage.'fields/postcode_set')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/postcode_set_tool_tip_1');?>','<?=t($t_base_haulage.'messages/postcode_set_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td style="text-align:left;width:17%;"><strong><?=t($t_base_haulage.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/postcode_transit_time_heading');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td style="text-align:left;width:16%;"><strong><?=t($t_base_haulage.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/fm_factor_for_postcode_tool_tip_heading');?>','<?=t($t_base_haulage.'messages/fm_factor_for_postcode_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td style="text-align:left;width:9%;"><strong><?=t($t_base_haulage.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/fuel_postcode_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			</tr>
			<? if(!empty($haulageApprovalDataArr))
				{
					$j=0;
					foreach($haulageApprovalDataArr as $haulageApprovalDataArrs)
					{
							$CountryName=$kConfig->getCountryName($haulageApprovalDataArrs['idCountry']);
							if($j==0)
							{
								$style='style="background:#DBDBDB;color:#828282;"';
								$idHaulagePricingModel=$haulageApprovalDataArrs['id'];
							}
							else
							{
								$style='style="background:#ffffff;color:#000000;"';
							}
						?>
			<tr <?=$style?> id="haulage_postcode_<?=++$j?>" onclick="sel_haulage_model_postcode_data_temp('haulage_postcode_<?=$j?>','<?=$haulageApprovalDataArrs['id']?>','<?=$idHaulageModel?>','<?=$j?>','<?=$idWarehouse?>','<?=$iDirection?>','<?=$haulageApprovalDataArrs['fWmFactor']?>','<?=$idService?>');">
				<!-- <td><?=$haulagePricingPostCodeDataArrs['iPriority']?>&nbsp;Priority</td>-->
				<td class="noborder"><?=$CountryName?></td>
				<td><?=$haulageApprovalDataArrs['szName']?></td>
				<td><? 
				if((int)$haulageApprovalDataArrs['iHours']>24)
				{
					echo "< ".($haulageApprovalDataArrs['iHours']/24)." ".t($t_base_haulage.'fields/days');;
				}
				else
				{
					echo "< ".$haulageApprovalDataArrs['iHours']." ".t($t_base_haulage.'fields/hours');;
				}
				?></td>
				<td><?=number_format((float)$haulageApprovalDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base_haulage.'fields/kg');?>/<?=t($t_base_haulage.'fields/cbm');?></td>
				<td><?=number_format((float)$haulageApprovalDataArrs['fFuelPercentage'],1)?> %</td>
			</tr>
			<? 		
					}
					
					$szPostCodeArr=array();
					$szPostCodeArr=$kHaulagePricing->getSetOfPostCode($idHaulagePricingModel,true);
					if(!empty($szPostCodeArr))
					{
						$szPostCode_Str=implode("<br/>",$szPostCodeArr);
					}
				}
			?>
		</table>
		</div>
		<div id="display_postcode_string" class="tableScroll" style="width: 22%; float: right;height: 134px;">
			<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
				<tr>
				 <th class="noborder" align="left"><strong><?=t($t_base_haulage.'fields/postcodes_included');?></strong></th>
				</tr>
				<tr>
				 <td class="noborder" align="left"><?=$szPostCode_Str?></td>
				</tr>
			</table>
		</div>
		</div>
		<?
		}
		else if($idHaulageModel=='4')
		{
			$allHaulageDistanceCountriesArr=array();
			if($iDirection=='1')
			{
				$formto=t($t_base.'fields/from');
			}
			else
			{
				$formto=t($t_base.'fields/to');
			}
		?>
		<p style="margin-bottom: 2px;"><strong><span style="text-align:left"><?=t($t_base_haulage.'title/distance_title_1')?></span></strong></p>
		<div class="clearfix">
		<div class="tableScroll" style="height:134px;width:76%;float:left;border-top:none;">
		<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_distance" style="float:left;">
			<tr>
				<td class="noborder" width="20%" style="text-align:left;width:20%"><strong><?=t($t_base_haulage.'fields/distance');?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/distance_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td width="20%" style="text-align:left;width:20%"><strong><?=t($t_base_haulage.'fields/transit_time');?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/transit_time_tool_tip_distance');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td width="20%" style="text-align:left;width:20%"><strong><?=t($t_base_haulage.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/fm_factor_for_distance_tool_tip_heading');?>','<?=t($t_base_haulage.'messages/fm_factor_for_distance_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
				<td width="10%" style="text-align:left;width:20%"><strong><?=t($t_base_haulage.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base_haulage.'messages/fuel_distance_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
			</tr>
			<? if(!empty($haulageApprovalDataArr))
				{
					$j=0;
					foreach($haulageApprovalDataArr as $haulageApprovalDataArrs)
					{
						if($j==0)
						{
							$style='style="background:#DBDBDB;color:#828282;"';
							$idHaulagePricingModel=$haulageApprovalDataArrs['id'];
						}
						else
						{
							$style='style="background:#ffffff;color:#000000;"';
						}
			?>
			<tr <?=$style?> id="haulage_distance_<?=++$j?>" onclick="sel_haulage_model_distance_data_temp('haulage_distance_<?=$j?>','<?=$haulageApprovalDataArrs['id']?>','<?=$idHaulageModel?>','<?=$j?>','<?=$idWarehouse?>','<?=$iDirection?>','<?=$idService?>');">
				<td class="noborder"><?=t($t_base_haulage.'fields/up_to')?> <?=number_format((int)$haulageApprovalDataArrs['iDistanceUpToKm'])?> <?=t($t_base_haulage.'fields/km')?></td>
				<td><? 
				if((int)$haulageApprovalDataArrs['iHours']>24)
				{
					echo "< ".($haulageApprovalDataArrs['iHours']/24)." ".t($t_base_haulage.'fields/days');
				}
				else
				{
					echo "< ".$haulageApprovalDataArrs['iHours']." ".t($t_base_haulage.'fields/hours');
				}
				?></td>
				<td><?=number_format((int)$haulageApprovalDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base_haulage.'fields/kg')?>/<?=t($t_base_haulage.'fields/cbm')?></td>
				<td><?=number_format((float)$haulageApprovalDataArrs['fFuelPercentage'],1)?> %</td>
			</tr>
			<? 	
					}
										
					$allHaulageDistanceCountriesArr=$kHaulagePricing->getAllCountriesForDistanceHaulageModel($idHaulagePricingModel);
	
				}
			?>
		</table>
		</div>
		<div id="display_distance_string" class="tableScroll" style="width: 22%; float: right;height: 134px;">
			<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
				<tr >
				 <th class="noborder" align="left"><strong><?=t($t_base_haulage.'fields/countries_included')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base_haulage.'messages/countries_include_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
				</tr>
				<tr>
				 <td class="noborder">
				 <? if(!empty($allHaulageDistanceCountriesArr))
				 	{
				 		foreach($allHaulageDistanceCountriesArr as $allHaulageDistanceCountriesArrs)
				 		{
				 		
				 			echo $allHaulageDistanceCountriesArrs['szCountryName']."<br/>";
				 		}
				 	}?>
				 </td>
				</tr>
			</table>
		</div>
		</div>
		<?
		}
		?>
		
		<hr/>
		
		<div id="approve_haulage_data_popup"></div>
		<div id="approve_haulage_data_list">
		<?
		addHaulagePricingDataTemp($idHaulagePricingModel,$idWarehouse,$iDirection,$idHaulageModel,$idService);
		?>
		</div>
		<?php
}
function try_it_out_haulage_temp_data($idWarehouse,$idDirection,$idHaulageModel)
{
	if($idWarehouse>0)
	{
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($idWarehouse);
		$szWhsLatitude = $kWHSSearch->szLatitude ;
		$szWhsLongitude = $kWHSSearch->szLongitude ;
		
		$t_base = "HaulaugePricing/";
		if($idDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/try_from');
			
			$image_path = __BASE_STORE_IMAGE_URL__.'/new-haulagePricingBulk-2.png' ;
			$szOriginWhsName = $kWHSSearch->szWareHouseName ;
			$szDestinationWhsName='';
			$zone_type = 'HAULAGE_PRICING_EXPORT';
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$fromto=t($t_base.'fields/try_to');
			
			$image_path = __BASE_STORE_IMAGE_URL__.'/new-haulagePricingBulk-3.png' ;
			$szDestinationWhsName = $kWHSSearch->szWareHouseName ;
			$szOriginWhsName = '';
			$zone_type = 'HAULAGE_PRICING_IMPORT';
		}
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries();
		// geting all weight measure 
		$weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');
		
		?>
		
<script type="text/javascript">
$().ready(function() {	
	$("#szPostCodeCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	//draw_canvas_image_obo('<?=$szOriginWhsName?>','<?=$szDestinationWhsName?>','<?=$image_path?>','<?=$zone_type?>');
});
</script>
		<hr/>
		<p align="left" style="width:16px;"><b><?=t($t_base.'title/try_it_out')?></b></p>
		<p><?=t($t_base.'title/test_your')?> <?=$direction?> <?=t($t_base.'title/haulage_pricing_for')?> <?=$kWHSSearch->szWareHouseName?> <?=t($t_base.'title/by_completing_details_below')?>.</p>
		<br />
		<form id="obo_haulage_tryit_out_form" name="obo_haulage_tryit_out_form" method="post">
		<table cellpadding="0" cellspacing="0" border="0" class="tryhaulagepricing">
		<tr>
		<td width="112"><?=t($t_base.'fields/country')?></td>	
		<td colspan="2"><?=t($t_base.'fields/postcode')?></td>
		<td width="80"><?=t($t_base.'fields/cargo_weight')?></td>
		<td width="68" >&nbsp;</td>
		<td colspan="4"><?=t($t_base.'fields/cargo_volume')?></td>
		</tr>
			<tr>
			<td>
				<select size="1" name="tryoutArr[szCountry]" id="szCountry" onchange="clear_post_code()" style="width:122px;">
				<option value="">Select</option>
				<?php
					if(!empty($allCountriesArr))
					{ 
						foreach($allCountriesArr as $allCountriesArrs)
						{
						?><option value="<?=$allCountriesArrs['id']?>" <? if($kWHSSearch->idCountry==$allCountriesArrs['id']){?> selected <?}?>><?=$allCountriesArrs['szCountryName']?></option>
						<?php
						}
					}
				?>
				</td>
				<td width="81">
				<input style="width:75px;" type="text" value="<?=(($_POST['tryoutArr']['szPostCodeCity'])?$_POST['tryoutArr']['szPostCodeCity']:"")?>" id="szPostCodeCity" name="tryoutArr[szPostCodeCity]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
				<input type="hidden" id="idDirection" value="<?=$idDirection?>" name="tryoutArr[idDirection]">
				<input type="hidden" id="idWarehouse" value="<?=$idWarehouse?>" name="tryoutArr[idWarehouse]">				
				
				<input type="hidden" name="tryoutArr[szWhsLatitude]" id="szWhsLatitude" value="<?=$szWhsLatitude?>">
				<input type="hidden" name="tryoutArr[szWhsLongitude]" id="szWhsLongitude" value="<?=$szWhsLongitude?>">
				
				<input type="hidden" name="tryoutArr[szLatitude]" id="szLatitude" value="">
				<input type="hidden" name="mode" value="REVIEW">
				<input type="hidden" name="tryoutArr[idHaulageModel]" value="<?=$idHaulageModel?>">
				<input type="hidden" name="tryoutArr[szLongitude]" id="szLongitude" value="">				
				
				</td>
				<td width="40" style="line-height:12px;">
				<a href="javascript:void(0);" onclick="open_google_help_popup()" style="font-size: 12px;"><?=t($t_base.'fields/Select');?><br/><?=t($t_base.'fields/on_map');?></a>
				</td>
				<td>
				<input style="width:73px;" type="text" value="<?=(($_POST['tryoutArr']['fTotalWeight'])?$_POST['tryoutArr']['fTotalWeight']:"")?>" id="szCargoWieght" name="tryoutArr[fTotalWeight]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
				
				</td>
				<td>
					<select size="1" name="tryoutArr[idWeightMeasure]" id="idWeightMeasure" style="width:68px;">
						 <?php
						 	if(!empty($weightMeasureAry))
						 	{
						 		foreach($weightMeasureAry as $weightMeasureArys)
						 		{
						 			?>
						 				<option value="<?=$weightMeasureArys['id']?>" <? if(strtolower($weightMeasureArys['szDescription'])=='kg'){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
						 			<?
						 		}
						 	}
						 ?>
					</select>
				</td>
				<td width="80">
                                    <input style="width:70px;" type="text" value="<?=(($_POST['tryoutArr']['fVolume'])?$_POST['tryoutArr']['fVolume']:"")?>" id="szCargoVolume" name="tryoutArr[fVolume]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
				</td>
				<td><span><?=t($t_base.'fields/cbm')?></span></td>
				<td colspan="2" style="padding:0;width:248px;" align="right"><a href="javascript:void(0)" style="opacity:0.4" id="test_try_out" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/test')?></span></a> <a href="javascript:void(0)" class="button2" onclick="clear_haulage_tryitout();"><span style="min-width:70px;"><?=t($t_base.'fields/clear')?></span></a></td>
			</tr>
		</table>
	</form>
	
	<div id="tryitout_result_div_error" style="display:none;margin-top:8px;" class="errorBox">
	</div>
	<div id="tryitout_result_div" style="display:none;">
	</div>		
	<hr>
	<h5><?=t($t_base.'title/illustration_of_what_haulage_services_cover')?></h5>
	<canvas style="margin-left:-10px;" id="myCanvas" width="730" height="180"></canvas>
	<?	
	}
	else
	{
		$t_base = "HaulaugePricing/";
		?>
		<script type="text/javascript">
			$().ready(function() {	
				//draw_canvas_image_obo(' ',' ','<?=__BASE_STORE_IMAGE_URL__.'/haulagePricingBulk-1.png'?>','HAULAGE_PRICING');
			});
		</script>
		<div id="tryitout_result_div_error" style="display:none;margin-top:8px;" class="errorBox">
		</div>
		
		<div id="tryitout_result_div" style="display:none;">
		</div>		
		
		<hr>
		<h5><?=t($t_base.'title/illustration_of_what_haulage_services_cover')?></h5>
		<img src="<?=__BASE_STORE_IMAGE_URL__.'/new-haulagePricingBulk-1.png'?>" id="Upload one by one Haulage Services" >
		<?php
	}
}
function getForwarderInvoiceUploadService($idForwarder,$batchNumber,$flag='',$get_merege_flag=false)
{
	$version="Forwarder - Transporteca Upload Service Invoices".$idForwarder;
	$kForwarder= new cForwarder();
	$kBooking = new cBooking();
	$addDetail=$kForwarder->detailsForwarder($idForwarder);
	//$emailDetails=$kForwarder->detailsForwarderBillingEmail($idForwarder);
	$emailDetails=$kForwarder->detailsForwarderBillingGeneralEmail($idForwarder);
	//$invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,'',$batchNumber);
	$uploadServiceAmountInviceArr = $kBooking->getUploadServiceInvoiceData($idForwarder,$batchNumber);
	//print_r($uploadServiceAmountInviceArr);
	$date=date("Y-m-d H:i:s");
	
		$invoiceDate=($date!='0000-00-00 00:00:00')?date('d F Y',strtotime($uploadServiceAmountInviceArr[0]['dtPaymentConfirmed'])):'';
		$invoiceNumber=$uploadServiceAmountInviceArr[0]['szInvoice'];
		//$timefrom=($date!='0000-00-00 00:00:00')?date('H:i',strtotime($date)):'';
		//$dateto=($to!='0000-00-00 00:00:00')?date('d F Y',strtotime($to)):'';
		//$timeto=($to!='0000-00-00 00:00:00')?date('H:i',strtotime($to)):'';
		$data='';
		$curency='';
		if(!empty($uploadServiceAmountInviceArr))
		{
			$data .="<tr>
				<td>".date('d F Y', strtotime($uploadServiceAmountInviceArr[0]['dtPaymentConfirmed']))."</td>
				<td>".$uploadServiceAmountInviceArr[0]['iFileType']."</td>
				<td align='LEFT'>".$uploadServiceAmountInviceArr[0]['iActualRecords']." ".$uploadServiceAmountInviceArr[0]['szRecordType']."</td>
				<td ALIGN='LEFT'>".html_entities_flag($uploadServiceAmountInviceArr[0]['szCostApprovalBy'])."</td>
				<td ALIGN='RIGHT'>".$uploadServiceAmountInviceArr[0]['szForwarderCurrency']." ".number_format((float)$uploadServiceAmountInviceArr[0]['fUploadServiceAmount'],2)."</td>
			</tr>";
			
			$curency=$uploadServiceAmountInviceArr[0]['szForwarderCurrency'];
			$array=$uploadServiceAmountInviceArr[0]['szForwarderCurrency']." ".number_format((float)$uploadServiceAmountInviceArr[0]['fUploadServiceAmount'],2);
		}
		$add='';
		if($addDetail['szCompanyName'])
		{
			$add.="<br />".$addDetail['szCompanyName']."<br />";
		}
		if($addDetail['szAddress'])
		{
			$add.=$addDetail['szAddress']."<br />";
		}
		if($addDetail['szAddress2'])
		{
			$add.=$addDetail['szAddress2']."<br />";
		}
		if($addDetail['szAddress3'])
		{
			$add.=$addDetail['szAddress3']."<br />";
		}
		if($addDetail['szCity'])
		{
			$add.=$addDetail['szCity'];
		}
		if($addDetail['szCity'] && $addDetail['szState'])
		{
			$add.=' , ';
		}
		if($addDetail['szState'])
		{
			$add.=$addDetail['szState'];
		}
		if($addDetail['szCountryName'])
		{
			$add.="<br />".$addDetail['szCountryName'];
		}
		if($emailDetails)
		{	
			$num=count($emailDetails);
			foreach($emailDetails as $key=>$value)
			{
				$emailLog.=$value['szGeneralEmailAddress'];
				$num--;
				if($num>1)
				{
					$emailLog.= ", ";
				}
				else if($num==1)
				{
					$emailLog.= ' and ';
				}
			}
		}
  		$link=$addDetail['szControlPanelUrl'];
  
	
  //$pdf->Image('invoice.jpg',120,12,0);
  $array=explode(',',$array);
  if($flag!='PDF')
  {
  	$body_info='style="background:#000;text-align:center;font-size:14px;"';
  	$image_url=__MAIN_SITE_HOME_PAGE_URL__;
  	$dataview='<p style="text-align:right;margin:5px auto 10px;width:690px;">
			<a target="_blank" href='.__BASE_URL__.'/downloadUploadServiceInvoice/'.$idForwarder.'/'.$batchNumber.'/ style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>
			<a onclick="PrintDiv();" href="javascript:void(0)" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a>
			</p>';
  	$dividvalue='id="forwarderUploadServiceInvoice"';
  	$script='<script>
			function PrintDiv()
			{    
			      var divToPrint = document.getElementById("forwarderUploadServiceInvoice");
			      var popupWin = window.open("", "_blank", "width=900,height=700");
			      popupWin.document.open();
			      popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
			      popupWin.document.close();
			}
			</script>';
  }
  else
  {
  	$image_url=__APP_PATH_ROOT__;
  	$dataview='';
  	$script='';
  	$dividvalue='';
  }
  	  $contents = '<?xml version="1.0" encoding="iso-8859-1"?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>Transporteca Upload Service Invoice</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		'.$script.'
		</head>
		<body '.$body_info.'>'.$dataview.'
			<div style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;height:900px;" '.$dividvalue.'>';
				if($get_merege_flag)
                                {
                                    $contents .='<table  border="0" cellspacing="0" cellpadding="0" width="650px">
						<tr style="margin-bottom:12px;">
							<td colspan="2" valign="top" width="312px" align="left">
								<img src='.$image_url.'/images/TransportecaMail.jpg height="53px" width="217px">
							</td>
								
								<td colspan="2" valign="middle" width="40%" align="right">
									<img src='.$image_url.'/images/HeadingInvoice.jpg width="150" hieght="30">
										
								</td>	
						</tr>
						</table>
						<br />';
                                }
                                else
                                {
                                    $contents .='<table  border="0" cellspacing="0" cellpadding="0" width="650px">
						<tr style="margin-bottom:12px;">
							<td colspan="2" valign="top" width="312px" align="left">&nbsp;</td>
                                                        <td colspan="2" valign="middle" width="40%" align="right">&nbsp;</td>	
						</tr>
						</table>
						<br />';
                                }
                                    $contents .='<table  border="0" cellspacing="0" cellpadding="1" width="650px">
						<tr>
							<td colspan="2" valign="top" width="52%" align="left" style="border:solid 1px #000;" border="1">
								<b>Invoice Date: '.$invoiceDate.'</b>
							</td>
							<td colspan="2" valign="middle" width="48%" align="left" style="border:solid 1px #000;" border="1">
								<b>Invoice Number:  '.$invoiceNumber.'</b>
							</td>
						</tr>
						</table>
						<br />						
						<table  border="0" cellspacing="0" cellpadding="0" width="650px">
						<tr>
							
							<td valign="top" width="52%">
								<strong>From:</strong>
								<strong></strong>
								TRANSPORTECA_ADDRESS
							</td>
							<td valign="top" width="48%" >
								<strong>To:</strong>
								TO_TRANSPORTECA_ADD
								<br />E-mail: BILLING_ADD_EMAIL
							</td>
						</tr>
				</table>
				<br /><br />				
				<table width="650px" border="1" cellspacing="0" cellpadding="3" align="left">
					<tbody>
						<tr>
							<td colspan="5" valign="top" width="700">
							<strong>Service Description:</strong>
							</td>
						</tr>
						<tr>
							<td colspan="5" valign="top" width="700">Transporteca Upload Services.<br /><br /></td>
						</tr>
						<tr>
							<td valign="top" width="167">Completed Date</td>
							<td valign="top" width="135">Data related to</td>
							<td valign="top" width="130" ALIGN="LEFT">Records</td>
							<td valign="top" width="150" ALIGN="LEFT">Approved by</td>
							<td valign="top" width="97" ALIGN="RIGHT">Processing Fee</td>
							</tr>
							ALL_DETAILS
							<tr>
							<td colspan="4" valign="top" width="499"><strong>Invoice Total</strong></td>
							<td valign="top" width="120" ALIGN="RIGHT"><strong>INV_TOTAL</strong></td>
						</tr>
						<tr>
							<td colspan="4" valign="top" width="499"><strong>Withheld in next bank transfer</strong></td>
							<td valign="top" width="120" ALIGN="RIGHT"><strong>PAYMENT_TRANSFER</strong></td>
						</tr>
						<tr>
							<td colspan="4" valign="top" width="499"><strong>Payment Due</strong></td>
							<td valign="top" width="120" ALIGN="RIGHT"><strong>'.$curency.' 0.00</strong></td>
						</tr>
				</table>
					<div style="width:650px;">
						<p>&nbsp;</p>
						<center><span style="TEXT-ALIGN:CENTER;font-size:small;">This invoice is available in soft copy on FORWARDER_LINK_URL under Billing.</span></center>
						<center><span style="TEXT-ALIGN:CENTER;font-size:small;">Transporteca&rsquo;s Term &amp; Conditions are available and agreed on FORWARDER_LINK_URL.</span></center>
						<center><span style="TEXT-ALIGN:CENTER;font-size:small;">This is an electronic statement and no signature is required.</span></center>
					</div>
				</div>	
		</body>
		</html>';
		  
 		  $contents = str_replace("TRANSPORTECA_ADDRESS", __TRANSPORTECA_ADDRESS__, $contents);
	  	  $contents = str_replace("TO_TRANSPORTECA_ADD", $add, $contents);
	  	  $contents = str_replace("BILLING_ADD_EMAIL", $emailLog, $contents);
	  	  $contents = str_replace("ALL_DETAILS", $data, $contents);
	  	  $contents = str_replace("FORWARDER_LINK_URL", $link, $contents);
		  $contents = str_replace("INV_TOTAL", $array['0'], $contents);	
		  $contents = str_replace("PAYMENT_TRANSFER", $array['0'], $contents);	
                  
                  if($get_merege_flag)
                  {
                      return $contents;
                  }
                  else
                  {
                        if(empty($flag))
                        {
                              echo $contents;
                        }

                        if(isset($flag) && $flag=="PDF")
                        {

                                class transfer extends HTML2FPDFBOOKING
                                {
                                        function Footer()
                                        {
                                                       $this->SetY(-10);

                                                  $this->SetFont('Arial','B',9);
                                                      $this->SetTextColor(0);
                                                  //Arial italic 9
                                                  $this->SetFont('Arial','B',9);
                                                  //Page number
                                                  $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
                                                  //Return Font to normal
                                                  $this->SetFont('Arial','',11);
                                        }
                                }
                                $filename=__APP_PATH__."/forwarders/html2pdf/".$version.".pdf";	
                              if(file_exists($filename))
                  {
                      @unlink($filename);
                  }
                                $pdf=new transfer();
                                $pdf->AddPage();
                                $pdf->Image($image_url.'/images/TransportecaMail.jpg',10,10,30,10);
                                $pdf->Image($image_url.'/images/HeadingInvoice.jpg',170,10,30,10);
                                $pdf->SetFontSize(12);
                                $pdf->SetTopMargin(5);
                                //$pdf->Rect(98,37,84,40);
                                $pdf->SetFont('Arial','',8);
                                $pdf->WriteHTML($contents);
                                $pdf->Output(__APP_PATH__."/forwarders/html2pdf/".$version.".pdf");
                               return $filename;
                        }
                  }
		  
}
function city_data_list($haulagePricingZoneDataArr)
{
	$t_base = "HaulaugePricing/";
	$t_base_error="Error";
	$kHaulagePricing = new cHaulagePricing();
	//print_r($_POST['dataExportCityArr']);
	if(!empty($_POST['dataExportCityArr']))
	{
		if($kHaulagePricing->copyCityData($_POST['dataExportCityArr']))
		{
				$data['idDirection']=$_POST['dataExportCityArr']['idZoneDirection'];
				$data['idWarehouse']=$_POST['dataExportCityArr']['idZoneWarehouse'];
				$data['idHaulageModel']=$_POST['dataExportCityArr']['idZoneHaulageModel'];
				$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
				if(!empty($haulagePricingModelArr))
				{
					foreach($haulagePricingModelArr as $haulagePricingModelArrs)
					{
						if($haulagePricingModelArrs['idHaulageModel']=='2')
						{
							if((int)$haulagePricingModelArrs['iCount']>0)
							{
								if((int)$haulagePricingModelArrs['iCount']==1)
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/city_defined');
								else
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/cities_defined');
							}
							else
								$text=t($t_base.'fields/no_cities_defined');
						}
					}
				}
				else
				{
					$text=t($t_base.'fields/no_cities_defined');
				}
		
		?>
			<script type="text/javascript">
				$("#city_detail_delete_"+<?=$_POST['dataExportCityArr']['idZoneHaulageModel']?>).attr('style','display:none;');
				$("#loader").attr('style','display:block;');
				
				open_pricing_model_detail('<?=$_POST['dataExportCityArr']['idZoneHaulageModel']?>','<?=$_POST['dataExportCityArr']['idZoneWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>');
				var divid="change_data_value_<?=$_POST['dataExportCityArr']['idZoneHaulageModel']?>";
				$("#"+divid).html('<?=$text?>');
				</script>	
		<?		
		}
	}

if(!empty($kHaulagePricing->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?
	}
	
	?>
	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="format-2" style="position:relative;top:-1px;">
	<?
	if(!empty($haulagePricingZoneDataArr))
	{
		foreach($haulagePricingZoneDataArr as $haulagePricingZoneDataArrs)
		{?>
			<tr>
				<td class="noborder" width="54%"><?=$haulagePricingZoneDataArrs['szName']?></td>
				<td class="checkbox" width="23%"><input type="checkbox" name="dataExportCityArr[szCopyZone][]" id="szCopyZone_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>" <? if(!empty($_POST['dataExportCityArr']['szCopyZone']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportCityArr']['szCopyZone'])) {?> checked <? }?>  onclick="enable_copy_pricing('<?=$haulagePricingZoneDataArrs['id']?>','city');"> <?=t($t_base.'fields/city_data')?></td>
				<td class="checkbox" width="23%"><input type="checkbox" name="dataExportCityArr[szCopyZonePricing][]" id="szCopyZonePricing_<?=$haulagePricingZoneDataArrs['id']?>" value="<?=$haulagePricingZoneDataArrs['id']?>"  <? if(!empty($_POST['dataExportCityArr']['szCopyZonePricing']) && in_array($haulagePricingZoneDataArrs['id'],$_POST['dataExportCityArr']['szCopyZonePricing'])) {?> checked <? } if(empty($_POST['dataExportCityArr']['szCopyZone'])){?> disabled <? }?> > <?=t($t_base.'fields/pricing_data')?></td>
			</tr>
		<?		
		}
	}else
	{?>
		<tr>
			<td colspan="3" align="center"><?=t($t_base.'fields/no_record_found_city');?></td>
		</tr>
		<?
	}
	?>
	</table>
	<?php
}
function showBillingDetails_new_backup($t_base,$idForwarder,$dateArr,$flag=false)
{	 
    $kBooking = new cBooking();
    $kForwarder = new cForwarder();
    $kBilling= new cBilling();
    $kConfig= new cConfig();
    $kAdmin = new cAdmin();

    ///for current balance////
    $currenctBalaceArr=$kBilling->getCurrenctBalancePerForwarder($idForwarder);

    ///for pending balance////
    $pendingBalaceArr=$kBilling->getPendingBalancePerForwarder($idForwarder);


    //// using for bottom table data/////
    $invoiceBillPending=$kBilling->getInvoiceBillPending($idForwarder,$dateArr);


    //// using for top table data/////
    $invoiceBillConfirmed=$kBilling->getInvoiceBillConfirmed($idForwarder,$dateArr);



    //$sumAry=array();
    ///for net balance for that date,////
    $currentAvailableAmountAry=$kBilling->getNetBalancePerForwarder($idForwarder,$dateArr);


    $kForwarder->load($idForwarder);
    $currentBalArr=array();


    if($kForwarder->szPaymentFrequency>0)
    {
        $kForwarderNew = new cForwarder();
        $paymentFrequency = $kForwarderNew->getAllPaymentFrequency(); 
        $frequency = $paymentFrequency[$kForwarder->szPaymentFrequency]['szPaymentFrequency'];
    }
    else
    {
        $frequency= __FORWARDER_PAYMENT_FREQUENCY_MONTHLY_TEXT__;
    }

    $strlenBank=strlen($kForwarder->szBankName);
    if($strlenBank > 12)
    {
        $detailsBankName=mb_substr($kForwarder->szBankName,0,12,'UTF8')."...";
    }
    else
    {
        $detailsBankName=$kForwarder->szBankName;
    }

    $strlen=strlen($kForwarder->iAccountNumber);
    if($strlen>10)
    {
        for($i=6;$i>0;$i--)
        {
            $detailsBankAccount.='x';
        }
        $detailsBankAccount.=mb_substr($kForwarder->iAccountNumber,$strlen-4,4,'UTF8');
    }
    else
    {
        $detailsBankAccount=$kForwarder->iAccountNumber;
    }

    $currencyForwarderArr=$kConfig->getBookingCurrency($kForwarder->szCurrency);
    $currencyArr=array();
    
    if(!empty($currenctBalaceArr))
    {
        foreach($currenctBalaceArr as $key=>$values)
        {
            if(!in_array($key,$currencyArr))
            {
                $currencyArr[]=$key;
            }
            if($values>=0)
            {
                $currentBalArr[]=$key." ".number_format($values,2);
            }
            else
            {
                $values=str_replace("-","",$values);
                $currentBalArr[]="(".$key." ".number_format($values,2).")";
            } 
        }
    }
    $curr_str="";
    if(!in_array($currencyForwarderArr[0]['szCurrency'],$currencyArr))
    {
        $curr_str=$currencyForwarderArr[0]['szCurrency']." 0.00, ";
    }

    if(!empty($currentBalArr))
    {
        $newCurrentBalanceAry = format_fowarder_emails($currentBalArr);
        //print_r($newCurrentBalanceAry);
        $newCurrentBalanceStr = $curr_str."".$newCurrentBalanceAry[1];
    }
    else
    {
        $currencyArr=$kConfig->getBookingCurrency($kForwarder->szCurrency);
        $newCurrentBalanceStr = $currencyArr[0]['szCurrency']." 0.00";
    }
    $newPendingBalaceArr=array();
    $currencyPenArr=array();
    if(!empty($pendingBalaceArr))
    {
        foreach($pendingBalaceArr as $key=>$values)
        {
            if(!in_array($key,$currencyPenArr))
            {
                $currencyPenArr[]=$key;
            }
            $newPendingBalaceArr[]=$key." ".number_format($values,2);
        }
    }
    //print_r($currencyForwarderArr);
    $pend_str="";
    if(!in_array($currencyForwarderArr[0]['szCurrency'],$currencyPenArr))
    {
        $pend_str=$currencyForwarderArr[0]['szCurrency']." 0.00, ";
    }

    $newPendingBalanceStr='';
    if(!empty($newPendingBalaceArr))
    {
        $newPendingBalanceAry = format_fowarder_emails($newPendingBalaceArr);
        $newPendingBalanceStr = $pend_str."".$newPendingBalanceAry[1]; 
    }
    else
    {
        $currencyArr=$kConfig->getBookingCurrency($kForwarder->szCurrency);
        $newPendingBalanceStr = $currencyArr[0]['szCurrency']." 0.00";
    }  
    echo "ID: ".$idForwarder;             
     //$invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,$dateArr);//paid to forwarder
?>
<table style="margin-left:-3px;">
    <tr>
        <td>
            <?=t($t_base.'title/current_balance');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/current_balance');?>','<?=t($t_base.'messages/current_balance_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td><td></td><td id="payment_pending" style="font-style: italic;">
            <?=$newCurrentBalanceStr?>
        </td>
        <td class="bank_name">
            <?=t($t_base."title/transfer_are_made_to")?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/transfer_frequency');?>','<?=t($t_base.'messages/transfer_frequency_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td><td></td><td style="font-style: italic;"> <?if(!$kForwarder->checkForwarderComleteBankDetails($idForwarder)){ ?><?=$detailsBankName?></td> <td style="font-style: italic;">account  <?=$detailsBankAccount;?><?}else{echo 'To be updated';}?>
        </td>
    </tr>
    <tr>
        <td width="200px"> 
            <?=t($t_base.'title/pending_settlements');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/transfer_are_made_to');?>','<?=t($t_base.'messages/transfer_are_made_to_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td><td></td><td style="font-style: italic;">
            <?=$newPendingBalanceStr?>
        </td>
        <td><?=t($t_base."title/transfer_frequency")?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/pending_settlements');?>','<?=t($t_base.'messages/pending_settlements_details');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
        <td></td>
        <td style="font-style: italic;"><?=ucfirst($frequency)?></td>
    </tr>	
</table>
<div style="clear: both"></div>
<div id="forwarder_billing_bottom_form" class="oh" style="padding-top: 20px;padding-bottom: 0px;">

	<table cellpadding="0" style="width: 100%;" cellspacing="0" class="format-4" id="booking_table">
            <tr>
                <th width="8%" valign="top" ><?=t($t_base.'fields/date')?></th>
                <th width="23%" valign="top" ><?=t($t_base.'fields/description')?></th>
                <th width="13%" valign="top" ><?=t($t_base.'fields/your');?> <?=t($t_base.'fields/reference');?></th>	
                <th width="10%" valign="top" ><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/number')?></th>				
                <th width="14%" valign="top" style="text-align: right;"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/value')?></th>
                <th width="7%" valign="top" style="text-align: right;"><?=t($t_base.'fields/roe')?>*</th>
                <th width="12%" valign="top" style="text-align: right;"><?=t($t_base.'fields/your_currency')?> </th>
                <th width="13%" valign="top" style="text-align: right;"><?=t($t_base.'fields/balance')?></th>
            </tr>
            <?php  
                if(!empty($invoiceBillConfirmed) && count($invoiceBillConfirmed)>0)
                { 	
                    if($referalAndTransferFee)
                    {
                        $sum=(float)$total;
                        $gtotal=$newCurrentBalanceString;
                    }
                    else
                    {
                        $sum=0;
                        $gtotal="0.00";
                    }
				
                    $launchDate=strtotime('01-10-2012 00:00:00');		
                    $ctr=1;

                    foreach($invoiceBillConfirmed as $searchResults)
                    { 
                        $kBooking = new cBooking();
                        $kBooking->load($searchResults['idBooking']);
                        if($searchResults["dtPaymentConfirmed"]!='' && $searchResults["dtPaymentConfirmed"]!='0000-00-00 00:00:00')
                        {	
                            if($searchResults['iDebitCredit']==1 || $searchResults['iDebitCredit']==13)
                            {	
                                $idCurency = $searchResults['szForwarderCurrency'] ;

                                $i=1;
                                $currency=$searchResults['szForwarderCurrency'];
                                $minDate=strtotime($searchResults['dtCreditOn']);
                                if($minDate<$launchDate)
                                {
                                    $launchDate=$minDate;
                                }

                                $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] + $searchResults['fTotalPriceForwarderCurrency']);
                                $curr_sum='';
                                if($sumAry[$idCurency]>0)
                                { 
                                    $curr_sum=$searchResults["szForwarderCurrency"]." ".number_format((float)$sumAry[$idCurency],2);
                                }
                                else
                                { 
                                    $newAmount=str_replace("-","",$sumAry[$idCurency]);
                                    $curr_sum="(".$searchResults["szForwarderCurrency"]." ".number_format((float)$newAmount,2).")";
                                }  
                                if($searchResults['iDebitCredit']==1)
                                {
                                    $szBookingStr = '<span id="invoice_comment_'.$searchResults['idBooking'].'">'.createSubString($kBooking->szInvoiceFwdRef,0,12).'</span><br />
                                                     <a href="javascript:void(0)" onclick="update_invoice_comment(\''.$searchResults['idBooking'].'\',\''.$kBooking->szBookingRef.'\',\'booking\')" style="text-decoration:none;font-size:11px;"><img src="'.__BASE_STORE_IMAGE_URL__.'/Icon - Edit line.png" alt = "Click to update" style="float: right;margin-top:-17px;"></a>';

                                    $szTrStr = '<tr id="booking_data_'.$ctr.'" onclick="select_forwarder_billings_tr(\'booking_data_'.$ctr++.'\',\''.$searchResults[idBooking].'\')">';
                                }
                                else
                                {
                                    $szBookingStr = "";
                                    $szTrStr = '<tr id="booking_data_'.$ctr.'" >';
                                }
                                $forwarderTransferDetails[]=$szTrStr. ' 
                                        <td>'.((!empty($searchResults["dtPaymentConfirmed"]) && $searchResults["dtPaymentConfirmed"]!="0000-00-00 00:00:00")?DATE("d-M",strtotime($searchResults["dtPaymentConfirmed"])):"").'</td>
                                        <td>'.$searchResults["szBooking"].'</td>
                                        <td>'.$szBookingStr.'</td>
                                        <td>'.$searchResults["szInvoice"].'</td>
                                        <td style="text-align: right;">'.$searchResults["szCurrency"].' '.number_format($searchResults["fTotalAmount"],2).'</td>
                                        <td style="text-align: right;">'.number_format((float)$searchResults["fExchangeRate"],4).'</td>
                                        <td style="text-align: right;">'.$searchResults["szForwarderCurrency"].' '.number_format(abs((float)$searchResults["fTotalPriceForwarderCurrency"]),2).'</td>
                                        <td style="text-align: right;">'.$curr_sum.'</td>	
                                        </tr> ';
                                 //echo "<br> substracting ".$sumAry[$idCurency]." and ".$searchResults['fTotalPriceForwarderCurrency'] ;

                                 $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
                                }
                                else if(($searchResults['iDebitCredit']==2 || $searchResults['iDebitCredit']==3 || $searchResults['iDebitCredit']==8 || $searchResults['iDebitCredit']==12) && $searchResults['iStatus']==2)
                                {  
                                    $fTotalCourierLabelPaid = 0;
                                    if(trim($searchResults['szBooking'])==='Automatic transfer')
                                    {
                                        //Incase of Automatic Transfer We are deducting Label Fee
                                        $CreateLabelBatch = $searchResults['szInvoice'] + 1;
                                        $szInvoice_formatted = $kAdmin->getFormattedString($CreateLabelBatch,6);
                                        $customer_code = $szInvoice_formatted ; 
                                        $totalCourierLabelArr=$kAdmin->totalAmountCourierLabelFee($customer_code); 
                                        if(!empty($totalCourierLabelArr))
                                        {
                                            $fTotalCourierLabelPaid = $totalCourierLabelArr[0]['totalCurrencyValue']; 
                                            $searchResults['fTotalPriceForwarderCurrency'] = $searchResults['fTotalPriceForwarderCurrency']-$fTotalCourierLabelPaid;
                                        }
                                    } 

                                                    $idCurency = $searchResults['szCurrency'] ;		
                                                    /*if(trim($searchResults['szBooking'])==="Courier Booking and Labels Credit")
                                                    {    
                                                        $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] + $searchResults['fTotalPriceForwarderCurrency']);
                                                    }
                                                    else
                                                    {*/
                                                        $value=round($currentAvailableAmountAry[$idCurency]['fTotalBalance'],2) - round($searchResults['fTotalPriceForwarderCurrency'],2);

                                                        $sumAry[$idCurency] = (float)($value);
                                                    //}

                                                    $curr_sum='';
                                                    if($sumAry[$idCurency]>0)
                                                    {
                                                            $curr_sum=$searchResults['szCurrency']." ".number_format((float)$sumAry[$idCurency],2);
                                                    }
                                                    else
                                                    {
                                                            $newAmount=str_replace("-","",$sumAry[$idCurency]);
                                                            $curr_sum="(".$searchResults['szCurrency']." ".number_format((float)$newAmount,2).")";
                                                    }

                                                    $flag='';
                                                    if(trim($searchResults['szBooking'])==='Transporteca referral fee')
                                                    {
                                                            $flag="Referral";	 
                                                    }
                                                    else if(trim($searchResults['szBooking'])==="Automatic transfer")
                                                    {
                                                            $flag="Transfer";
                                                    }
                                                    else if(trim($searchResults['szBooking'])==="Courier Booking and Labels Invoice")
                                                    {
                                                            $flag="Labels Fee";
                                                    }
                                                    else if(trim($searchResults['szBooking'])==="Courier Booking and Labels Credit")
                                                    {
                                                            $flag="Labels Fee";
                                                    }
                                                    else if(trim($searchResults['szBooking'])==='Transporteca Referral Fee Credit')
                                                    {
                                                            $flag="Referral";	 
                                                    }
                                                    else if($searchResults['iDebitCredit']==12)
                                                    {
                                                        $flag="HANDLING_FEE";	 
                                                    } 

                                                    $forwarderTransfer.="			
                                                    <tr id='booking_data_".$ctr."' onclick='select_forwarder_billings_referral_tr(\"booking_data_".$ctr++."\",\"".$searchResults['iBatchNo']."\",\"".$idForwarder."\",\"".$flag."\")'><td>".((!empty($searchResults['dtPaymentConfirmed']) && $searchResults['dtPaymentConfirmed']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtPaymentConfirmed'])):'')."</td>";
                                                    if(trim($searchResults['szBooking'])=='Transporteca referral fee')
                                                    { 
                                                        $forwarderTransfer.= "<td>Transporteca Referral Fee Invoice</td>";
                                                    }
                                                    else if(trim($searchResults['szBooking'])==='Automatic transfer')
                                                    {
                                                        $forwarderTransfer.= "<td>".$searchResults['szBooking']."</td>";
                                                    } 
                                                    else if(trim($searchResults['szBooking'])==='Courier Booking and Labels Invoice')
                                                    {
                                                        $forwarderTransfer.= "<td>".$searchResults['szBooking']."</td>";
                                                    } 
                                                    else if(trim($searchResults['szBooking'])==='Courier Booking and Labels Credit')
                                                    {
                                                        $forwarderTransfer.= "<td>".$searchResults['szBooking']."</td>";
                                                    } 
                                                    else if(trim($searchResults['szBooking'])==='Transporteca Referral Fee Credit')
                                                    {
                                                        $forwarderTransfer.= "<td>".$searchResults['szBooking']."</td>";
                                                    }
                                                    else if($searchResults['iDebitCredit']==12)
                                                    {
                                                        $forwarderTransfer.= "<td>".$searchResults['szBooking']."</td>";
                                                    } 
                                                    if((int)$searchResults['idBooking']>0){
                                                    $forwarderTransfer.= "<td><span id='invoice_comment_".$searchResults['idBooking']."'>".createSubString($kBooking->szInvoiceFwdRef,0,12)."</span><br />
                                                        <a href='javascript:void(0)' onclick='update_invoice_comment(\"".$searchResults['idBooking']."\",\"".$kBooking->szBookingRef."\",\"booking\")' style='text-decoration:none;font-size:11px;'><img src='".__BASE_STORE_IMAGE_URL__."/Icon - Edit line.png' alt = 'Click to update' style='float: right;margin-top:-17px;'></a>
                                                    </td>";
                                                    }else
                                                    {
                                                        $forwarderTransfer.= "<td></td>";
                                                    }
                                                    $forwarderTransfer.= "<td>";
                                                    if(trim($searchResults['szBooking'])==="Transporteca referral fee" || trim($searchResults['szBooking'])==='Courier Booking and Labels Invoice' || trim($searchResults['szBooking'])==='Courier Booking and Labels Credit' || trim($searchResults['szBooking'])==='Transporteca Referral Fee Credit' || $searchResults['iDebitCredit']==12)
                                                    { 
                                                        $forwarderTransfer.= $searchResults['szInvoice'];
                                                    }
                                                    if(trim($searchResults['szBooking'])==='Courier Booking and Labels Invoice' || trim($searchResults['szBooking'])==="Transporteca referral fee" || $searchResults['iDebitCredit']==12)
                                                    {
                                                        $forwarderTransfer.= "</td><td></td><td></td>
                                                            <td style=\"text-align: right;\"> (".$searchResults['szCurrency'] ." ".number_format(abs((float)($searchResults['fTotalPriceForwarderCurrency'])),2).")</td>
                                                            <td style=\"text-align: right;\">".$curr_sum."</td>
                                                        </tr>";
                                                    }
                                                    else if(trim($searchResults['szBooking'])==='Courier Booking and Labels Credit' || trim($searchResults['szBooking'])==='Transporteca Referral Fee Credit')
                                                    {
                                                        $forwarderTransfer.= "</td><td></td><td></td>
                                                            <td style=\"text-align: right;\"> ".$searchResults['szCurrency'] ." ".number_format(abs((float)($searchResults['fTotalPriceForwarderCurrency'])),2)."</td>
                                                            <td style=\"text-align: right;\">".$curr_sum."</td>
                                                        </tr>";
                                                    }
                                                    else
                                                    {
                                                        $forwarderTransfer.= "</td><td></td><td></td>
                                                            <td style=\"text-align: right;\"> (".$searchResults['szCurrency'] ." ".number_format(abs((float)($searchResults['fTotalPriceForwarderCurrency'])),2).")</td>
                                                            <td style=\"text-align: right;\">".$curr_sum."</td>
                                                        </tr>";
                                                    }  

                                                    $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
                                                    $forwarderTransferDetails[] = $forwarderTransfer;
                                                    $forwarderTransfer='';
                                            }
                                            else if(($searchResults['iDebitCredit']==6 || $searchResults['iDebitCredit']==7)) // Cancelled booking
                                            { 
                                                $idCurency = $searchResults['szForwarderCurrency'] ;		
                                                //$sumAry[$idCurency]= ;
                                                $flag = 'CREDIT_NOTE';

                                                $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $searchResults['fTotalPriceForwarderCurrency']);
                                                //echo $sumAry[$idCurency]."total currency";
                                                //echo $idCurency."<br/><br/>";
                                                $curr_sum='';
                                                if($sumAry[$idCurency]>0)
                                                {
                                                    $curr_sum=$searchResults["szForwarderCurrency"]." ".number_format((float)$sumAry[$idCurency],2);
                                                }
                                                else
                                                {
                                                    $newAmount=str_replace("-","",$sumAry[$idCurency]);
                                                    $curr_sum="(".$searchResults["szForwarderCurrency"]." ".number_format((float)$newAmount,2).")";
                                                }
                                                $invoiceRoeValueArr=array();
                                                $invoiceRoeValueArr[]=$kBilling->getInvoiceRoeValueCancelBooking($searchResults['idBooking']);
                                                //print_r($invoiceRoeValueArr);

                                                $forwarderTransfer.="			
                                                <tr id='booking_data_".$ctr."' onclick='select_forwarder_billings_tr(\"booking_data_".$ctr++."\",\"".$searchResults['idBooking']."\",\"".$flag."\")'><td>".((!empty($searchResults['dtPaymentConfirmed']) && $searchResults['dtPaymentConfirmed']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtPaymentConfirmed'])):'')."</td>";
                                                $forwarderTransfer.= "<td>".$searchResults['szBooking']."</td>";
                                                if((int)$searchResults['idBooking']>0){
                                                    if($searchResults['iDebitCredit']==6){
                                                    $forwarderTransfer.= "<td><span id='invoice_cancel_comment_".$searchResults['idBooking']."'>".createSubString($kBooking->szInvoiceFwdRef,0,12)."</span><br />
                                                        <a href='javascript:void(0)' onclick='update_invoice_comment(\"".$searchResults['idBooking']."\",\"".$kBooking->szBookingRef."\",\"cancel\")' style='text-decoration:none;font-size:11px;'><img src='".__BASE_STORE_IMAGE_URL__."/Icon - Edit line.png' alt = 'Click to update' style='float: right;margin-top:-17px;'></a>
                                                    </td>";}else{
                                                        $forwarderTransfer.= "<td><span id='invoice_comment_".$searchResults['idBooking']."'>".createSubString($kBooking->szInvoiceFwdRef,0,12)."</span><br />
                                                        <a href='javascript:void(0)' onclick='update_invoice_comment(\"".$searchResults['idBooking']."\",\"".$kBooking->szBookingRef."\",\"booking\")' style='text-decoration:none;font-size:11px;'><img src='".__BASE_STORE_IMAGE_URL__."/Icon - Edit line.png' alt = 'Click to update' style='float: right;margin-top:-17px;'></a>
                                                    </td>";
                                                    }
                                                }else
                                                {
                                                    $forwarderTransfer.= "<td></td>";
                                                }
                                                $forwarderTransfer.= "<td>";
                                                $forwarderTransfer.= $searchResults['szInvoice'];

                                                        $forwarderTransfer.= "</td><td style=\"text-align: right;\">(".$invoiceRoeValueArr[0]['invoiceValue'].")</td><td style=\"text-align: right;\">".number_format((float)$invoiceRoeValueArr[0]['roe'],4)."</td>
                                                        <td style=\"text-align: right;\"> (".$searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")</td>
                                                        <td style=\"text-align: right;\">".$curr_sum."</td>
                                                </tr>";

                                                //echo "<br> adding ".$sumAry[$idCurency]." and ".$searchResults['fTotalPriceForwarderCurrency'] ;

                                                $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
                                                $forwarderTransferDetails[] = $forwarderTransfer;
                                                $forwarderTransfer='';

                                            }
                                            else if($searchResults['iDebitCredit']=='5')
                                            {

                                                    //print_r($sumAry['USD']);
                                                    //echo "currency value".$idCurency;
                                                    //die;
                                                    $idCurency = $searchResults['szForwarderCurrency'] ;		
                                                    //$sumAry[$idCurency]= ;
                                                    //print_r($searchResults);
                                                    //echo $searchResults['szForwarderCurrency']."----".$idCurency."<br/><br/>";
                                                    //echo $currentAvailableAmountAry[$idCurency]['fTotalBalance']."<br/>";
                                                    if($idCurency==$searchResults['szForwarderCurrency'])
                                                    {
                                                            //echo "ddd1";
                                                            $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $searchResults['fTotalPriceForwarderCurrency']);
                                                    }
                                                    else
                                                    {
                                                            //echo "ddd";
                                                            $sumAry[$idCurency] = (float)$currentAvailableAmountAry[$idCurency]['fTotalBalance'];
                                                            //echo $sumAry[$idCurency]."<br/>";
                                                    }
                                                    $curr_sum='';
                                                    if($sumAry[$idCurency]>0)
                                                    {
                                                        $curr_sum=$searchResults['szForwarderCurrency']." ".number_format((float)$sumAry[$idCurency],2);
                                                    }
                                                    else
                                                    {
                                                        $newAmount=str_replace("-","",$sumAry[$idCurency]);
                                                        $curr_sum="(".$searchResults['szForwarderCurrency']." ".number_format((float)$newAmount,2).")";
                                                    }
                                                    $flag="uploadService";
                                                    $forwarderTransfer.="			
                                                    <tr id='booking_data_".$ctr."' onclick='select_forwarder_billings_referral_tr(\"booking_data_".$ctr++."\",\"".$searchResults['szInvoice']."\",\"".$idForwarder."\",\"".$flag."\")'><td>".((!empty($searchResults['dtPaymentConfirmed']) && $searchResults['dtPaymentConfirmed']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtPaymentConfirmed'])):'')."</td>";

                                                    $forwarderTransfer.= "<td>".$searchResults['szBooking']."</td><td></td>";

                                                    $forwarderTransfer.= "<td>";
                                                            $forwarderTransfer.= $searchResults['szInvoice'];

                                                            $forwarderTransfer.= "</td><td></td><td></td>
                                                            <td style=\"text-align: right;\"> (".$searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")</td>
                                                            <td style=\"text-align: right;\">".$curr_sum."</td>
                                                    </tr>";

                                                    //echo "<br> adding ".$sumAry[$idCurency]." and ".$searchResults['fTotalPriceForwarderCurrency'] ;

                                                    $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];
                                                    $forwarderTransferDetails[] = $forwarderTransfer;
                                                    $forwarderTransfer='';
                                            }
                                            else if($searchResults['iDebitCredit']=='4')
                                            {
                                                $dateOfjoining = strtotime($searchResults["dtCreatedOn"]);

                                                if((!empty($invoicePendingByAdmin) || !empty($invoiceBillConfirmed)) && ($dateFormSearchResult<=$dateOfjoining ))
                                                {	
                                                    $date=DATE('d-M',$dateOfjoining);

                                                    $forwarderTransfer="<tr>
                                                            <td>".$date."</td><td>".$searchResults['szBooking']."</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style='text-align: right;'>".$searchResults["szForwarderCurrency"]." 0.00</td>
                                                    </tr>";
                                                }	
                                                $forwarderTransferDetails[]=$forwarderTransfer;
                                                $forwarderTransfer='';
                                            } 
                                        }
					echo "<br> DC: ".$searchResults['iDebitCredit']." Val: ".$searchResults['fTotalPriceForwarderCurrency']."ID: ".$searchResults['id'];					
				}
				$tableForwarderAdmin=array_reverse($forwarderTransferDetails);
				//$tableForwarderAdmin=$forwarderTransferDetails;
				if($tableForwarderAdmin!=array())
				{
                                    foreach($tableForwarderAdmin as $key=>$value)
                                    {
                                        echo $value;
                                    }
				}
				
			}
			
			if(empty($invoicePendingByAdmin) && empty($invoiceBillConfirmed) || count($invoiceBillConfirmed)<=0)
			{	
				$style="style='opacity:0.4'";
				?>
				<tr>
					<td colspan="9" align="center" style="padding: 5px 0 5px 0;"><?=t($t_base.'messages/no_transactions_in_the_selected_date_range');?></td>
				</tr>
				<?php
			}
			else
			{
                            $onClickFunction="forwarderBillingSearchFormSubmit()";
			}
		?>
	</table>
	<div id="invoice_comment_div" style="display:none">
	</div>
	<br/>

	<div id="viewInvoiceComplete" style="float: right;">
	<a class="button1" id="download_invoice" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
	<a class="button1" id="download_booking"  style="opacity:0.4;"><span><?=t($t_base.'fields/view_booking');?></span></a>
	<a href="javascript:void(0)"  id="download_table" onclick= "<?echo $onClickFunction;?>" <?=$style; ?> class="button1"><span><?=t($t_base.'fields/download_table');?></span></a>
	</div>	
	
	</div>

	<div>
	<h4><strong><?=t($t_base.'title/invoice_pending_settlements');?></strong></h4>
	<p><?=t($t_base.'title/overview');?></p> 
	<div style="padding: 10px 0 10px 0;">
	<table cellpadding="0" cellspacing="1" style="width: 100%" class="format-4" id="booking_table_pending">
		<tr>
                    <th width="8%" valign="top"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/date')?></th>
                    <th width="23%" valign="top"><?=t($t_base.'fields/description')?></th>
                    <th width="13%" valign="top"><?=t($t_base.'fields/your');?> <?=t($t_base.'fields/reference');?></th>	
                    <th width="10%" valign="top"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/number')?></th>				
                    <th width="14%" valign="top" style="text-align: right;"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/value')?></th>
                    <th width="7%" valign="top" style="text-align: right;"><?=t($t_base.'fields/roe')?>*</th>
                    <th width="12%" valign="top" style="text-align: right;"><?=t($t_base.'fields/pending_credit')?> **</th>
                    <th width="13%" valign="top" style="text-align: right;"><?=t($t_base.'fields/expected_date')?></th>
                </tr>
            <?php	
            $ctr=1;
            if($invoiceBillPending)
            { 
                foreach($invoiceBillPending as $searchResults)
                {	
                    $kBooking = new cBooking();
                    $kBooking->load($searchResults['idBooking']);
                   // print_r($kBooking);
                        ?>						
                         <tr id="booking_data_pend_<?=$ctr?>" onclick="select_forwarder_billings_pend_tr('booking_data_pend_<?=$ctr++?>','<?=$searchResults['idBooking']?>')">
                                 <td><?=(!empty($searchResults['dtInvoiceOn']) && $searchResults['dtInvoiceOn']!='0000-00-00 00:00:00')?DATE('d-M',strtotime($searchResults['dtInvoiceOn'])):''?></td>
                                 <td><?=$searchResults['szBooking']?></td>
                                 <td>  
                                     <span id="invoice_comment_<?=$searchResults['idBooking']?>"><?=createSubString($kBooking->szInvoiceFwdRef,0,12)?></span><br />
                                     <a href="javascript:void(0)" onclick="update_invoice_comment('<?=$searchResults['idBooking']?>','<?=$kBooking->szBookingRef;?>','booking')" style="text-decoration:none;font-size:11px;"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" alt = "Click to update" style="float: right;margin-top:-17px;"></a>
                                 </td>
                                 <td ><?=$searchResults['szInvoice']?></td>
                                 <td style="text-align: right;"><?=$searchResults['szCurrency']." ".number_format($searchResults['fTotalAmount'],2)?></td>
                                 <td style="text-align: right;"><?=number_format((float)$searchResults['fExchangeRate'],4)?></td>
                                 <td style="text-align: right;"><?=$searchResults['szForwarderCurrency']." ".number_format($searchResults['fTotalPriceForwarderCurrency'],2)?></td>
                                 <?php $expectedDate=DATE('d-M',strtotime( '+16 days' , strtotime($searchResults['dtInvoiceOn'])));?>
                                 <td style="text-align: right;"><?=$expectedDate;?></td>
                         </tr>
                         <?php
                                //$ctr++;
                            }
                    }
                    else
                    {
                            echo "<tr><td colspan='8' align='center' style='padding:5px 5px 5px 5px;'>".t($t_base.'messages/no_invoices_pending_settlement')."</td></tr>";
                    }
		?>
		</table>
		<br/>
		<div id="viewInvoiceComplete" style="float: right;">
			<a class="button1" id="download_invoice_pend" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
			<a class="button1" id="download_booking_pend"  style="opacity:0.4;"><span style="min-width:108px"><?=t($t_base.'fields/view_booking');?></span></a>
		</div>			
		<div style="font-size:12px;">* <?=t($t_base.'title/dob');?></div>
		<div style="font-size:12px;">** <?=t($t_base.'title/typically_allow');?></div>
		<br/>
	</div>
	<?php
}
function display_preference_email_html() 
{ 
    $idForwarder = $_SESSION['forwarder_id']; 
    $kForwarderContact = new cForwarderContact();
    $kConfig = new cConfig();	
    $t_base = "ForwardersCompany/Preferences/";
    
    $forwarderContactAry = $kForwarderContact->getAllForwarderContactsEmail($idForwarder);

    $szBookingEmailAry = (!empty($forwarderContactAry['szBookingEmail'])) ? $forwarderContactAry['szBookingEmail']:'';
    $szPaymentEmailAry = (!empty($forwarderContactAry['szPaymentEmail'])) ? $forwarderContactAry['szPaymentEmail']:'';
    $szCustomerServiceEmailAry = (!empty($forwarderContactAry['szCustomerServiceEmail'])) ? $forwarderContactAry['szCustomerServiceEmail']: '';

    $szAirBookingEmailAry = (!empty($forwarderContactAry['szAirBookingEmail'])) ? $forwarderContactAry['szAirBookingEmail']:'';
    $szRoadBookingEmailAry = (!empty($forwarderContactAry['szRoadBookingEmail'])) ? $forwarderContactAry['szRoadBookingEmail']:'';
    $szCourierBookingEmailAry = (!empty($forwarderContactAry['szCourierBookingEmail'])) ? $forwarderContactAry['szCourierBookingEmail']:''; 
    
    $forwarderControlPanelArr = $kForwarderContact->getSystemPanelDetails($idForwarder);
    ?>
    <h4><strong><?=t($t_base.'titles/email_address');?></strong> <a href="javascript:void(0);" <?php if(!empty($forwarderControlPanelArr)){ ?> onclick="edit_preferences_email('<?=$idForwarder?>')" <? } ?>><?=t($t_base.'titles/edit');?></a></h4>				

    <div class="ui-fields">
        <span class="field-name"><?php echo t($t_base.'titles/sea')." ".t($t_base.'titles/bookings');?></span>	
        <div class="field-container">	
        <?php
            if(!empty($szBookingEmailAry))
            {
                foreach($szBookingEmailAry as $szBookingEmailArys)
                {
        ?>
                <span class="field-container"><?=$szBookingEmailArys['szEmail']?></span>
        <?php
                }
            } 
        ?>
        </div>
    </div>
    <div class="ui-fields">
        <span class="field-name"><?php echo t($t_base.'titles/air')." ".t($t_base.'titles/bookings');?></span>	
        <div class="field-container">	
        <?php
            if(!empty($szAirBookingEmailAry))
            {
                foreach($szAirBookingEmailAry as $szAirBookingEmailArys)
                {
        ?>
                <span class="field-container"><?php echo $szAirBookingEmailArys['szEmail']?></span>
        <?php
                }
            } 
        ?>
        </div>
    </div>
    <div class="ui-fields">
        <span class="field-name"><?php echo t($t_base.'titles/road')." ".t($t_base.'titles/bookings');?></span>	
        <div class="field-container">	
        <?php
            if(!empty($szRoadBookingEmailAry))
            {
                foreach($szRoadBookingEmailAry as $szRoadBookingEmailArys)
                {
        ?>
                <span class="field-container"><?php echo $szRoadBookingEmailArys['szEmail']?></span>
        <?php
                }
            } 
        ?>
        </div>
    </div>
    <div class="ui-fields">
        <span class="field-name"><?php echo t($t_base.'titles/courier')." ".t($t_base.'titles/bookings');?></span>	
        <div class="field-container">	
        <?php
            if(!empty($szRoadBookingEmailAry))
            {
                foreach($szRoadBookingEmailAry as $szRoadBookingEmailArys)
                {
        ?>
                <span class="field-container"><?php echo $szRoadBookingEmailArys['szEmail']?></span>
        <?php
                }
            } 
        ?>
        </div>
    </div>

    <div class="ui-fields">
        <span class="field-name"><?=t($t_base.'titles/Payments_billing');?></span>
        <div class="field-container">
    <?php
        if(!empty($szPaymentEmailAry))
        {
            foreach($szPaymentEmailAry as $szPaymentEmailArys)
            {
    ?>
                <span class="field-container"><?=$szPaymentEmailArys['szEmail']?></span>
    <?php
            }
        } 
    ?> 
        </div>
    </div>
    <div class="ui-fields">
        <span class="field-name"><?=t($t_base.'titles/Customer_service');?></span>
        <div class="field-container">
        <?php
            if(!empty($szCustomerServiceEmailAry))
            {
                foreach($szCustomerServiceEmailAry as $szCustomerServiceEmailArys)
                {
        ?>
                    <span class="field-container"><?=$szCustomerServiceEmailArys['szEmail']?></span>
        <?php
                }
            } 
        ?> 
        </div>
    </div>
    <?php
}
function display_road_booking_preferences($i,$airBookingEmailAry=false)
{ 
    $t_base = "ForwardersCompany/Preferences/";  
    
    if(isset($_POST['updatePrefEmailAry']['szEmailRoadBooking'][$i]))
    {
        $szEmail = $_POST['updatePrefEmailAry']['szEmailRoadBooking'][$i] ;
    }
    else
    {
        $szEmail = $airBookingEmailAry['szEmail'] ;
    }
    if(isset($_POST['updatePrefEmailAry']['RoadBookingId'][$i]))
    {
        $idContact = $_POST['updatePrefEmailAry']['RoadBookingId'][$i] ;
    }
    else
    {
        $idContact = $airBookingEmailAry['id'] ;
    } 
    ?>
    <span class="profile-fields" id="air_bookings<?=$i;?>"> 
        <input type="text" name="updatePrefEmailAry[szEmailRoadBooking][<?=$i;?>]" id="szEmailRoadBooking_<?=$i;?>" value="<?php echo $szEmail; ?>" onblur="closeTip('road_book<?=$i?>');" onfocus="openTip('road_book<?=$i?>');">
        <input type="hidden" name="updatePrefEmailAry[RoadBookingId][<?=$i;?>]" value="<?php echo $idContact; ?>"> 	
        <div class="field-alert"><div id="road_book<?=$i?>" style="display:none;"><?=t($t_base.'messages/road_booking');?></div></div>
    </span>   
 <?php
}
function display_courier_booking_preferences($i,$airBookingEmailAry=false)
{ 
    $t_base = "ForwardersCompany/Preferences/";  
    
    if(isset($_POST['updatePrefEmailAry']['szEmailCourierBooking'][$i]))
    {
        $szEmail = $_POST['updatePrefEmailAry']['szEmailCourierBooking'][$i] ;
    }
    else
    {
        $szEmail = $airBookingEmailAry['szEmail'] ;
    }
    if(isset($_POST['updatePrefEmailAry']['CourierBookingId'][$i]))
    {
        $idContact = $_POST['updatePrefEmailAry']['CourierBookingId'][$i] ;
    }
    else
    {
        $idContact = $airBookingEmailAry['id'] ;
    } 
    ?>
    <span class="profile-fields" id="air_bookings<?=$i;?>"> 
        <input type="text" name="updatePrefEmailAry[szEmailCourierBooking][<?=$i;?>]" id="szEmailCourierBooking_<?=$i;?>" value="<?php echo $szEmail; ?>" onblur="closeTip('courier_book<?=$i?>');" onfocus="openTip('courier_book<?=$i?>');">
        <input type="hidden" name="updatePrefEmailAry[CourierBookingId][<?=$i;?>]" value="<?php echo $idContact; ?>"> 	
        <div class="field-alert"><div id="courier_book<?=$i?>" style="display:none;"><?=t($t_base.'messages/courier_booking');?></div></div>
    </span>   
 <?php
}

function display_air_booking_preferences($i,$airBookingEmailAry=false)
{  
    $t_base = "ForwardersCompany/Preferences/";  
    
    if(isset($_POST['updatePrefEmailAry']['szEmailAirBooking'][$i]))
    {
        $szEmail = $_POST['updatePrefEmailAry']['szEmailAirBooking'][$i] ;
    }
    else
    {
        $szEmail = $airBookingEmailAry['szEmail'] ;
    }
    if(isset($_POST['updatePrefEmailAry']['AirBookingId'][$i]))
    {
        $idContact = $_POST['updatePrefEmailAry']['AirBookingId'][$i] ;
    }
    else
    {
        $idContact = $airBookingEmailAry['id'] ;
    } 
    ?>
    <span class="profile-fields" id="air_bookings<?=$i;?>"> 
        <input type="text" name="updatePrefEmailAry[szEmailAirBooking][<?=$i;?>]" id="szEmailAirBooking_<?=$i;?>" value="<?php echo $szEmail; ?>" onblur="closeTip('air_book<?=$i?>');" onfocus="openTip('air_book<?=$i?>');">
        <input type="hidden" name="updatePrefEmailAry[AirBookingId][<?=$i;?>]" value="<?php echo $idContact; ?>"> 	
        <div class="field-alert"><div id="air_book<?=$i?>" style="display:none;"><?=t($t_base.'messages/air_booking');?></div></div>
    </span>   
 <?php
}
function forwarder_billing_search_form_new($t_base)
{
	//$idForwarder = $_SESSION['forwarder_id'];
	//$kBooking = new cBooking();
	//$bookingYearAry = $kBooking->getAllForwarderBookingYear($idForwarder);
	//$billingMonthAry = month_key_value_pair();
	?>
        <div class="oh" style="float: right;">
            <select id="forwarder_billing_search_form" name="billingSearchAry[iBillingMonth]" onchange="selectBillingSearchDetails_new(this.value)" style="max-width: none;width: 275px;">
                <option value='1'>Last 7 Days</option>
                <option value='2'>This Month</option>
                <option value='3'>Previous month</option>
                <option value='4' selected="selected">Last 3 month</option>
                <option value='5'>This quarter</option>
                <option value='6'>Previous quarter</option>
                <option value='7'>This Year</option>
                <option value='8'>Previous Year</option>
                <option value='9'>All time</option>
            </select>
        </div>
	<?php

}
function formatLatoitudeLongitude($szLatLongAry)
{
	if(!empty($szLatLongAry))
	{
		$ctr=0;
		$ret_ary = array();
		foreach($szLatLongAry as $latlongReplaceArrs)
		{
			if(!empty($latlongReplaceArrs))
			{
				$latlongNewArr=explode(",",$latlongReplaceArrs);
				if(!empty($ret_ary['szLatitude']) && in_array($latlongNewArr[0],$ret_ary['szLatitude']))
				{
					$ret_ary['szLatitude'][$ctr] = $latlongNewArr[0] + 0.000001;
				}
				else
				{
					$ret_ary['szLatitude'][$ctr] = $latlongNewArr[0];
				}
				
				if(!empty($ret_ary['szLongitude']) && in_array($latlongNewArr[1],$ret_ary['szLongitude']))
				{
					$ret_ary['szLongitude'][$ctr] = $latlongNewArr[1] + 0.000001;
				}
				else
				{
					$ret_ary['szLongitude'][$ctr] = $latlongNewArr[1];
				}
			}
			$ctr++;
		}
		return $ret_ary ;
	}
}
function price_changed_error_popup($page_url)
{

	$page_url = $_SESSION['booking_page_url'];
	if(empty($page_url))
	{
		$page_url = __BOOKING_DETAILS_PAGE_URL__.'/'.$szBookingRandomNum."/";
	}

	$kBooking = new cBooking();
	$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
	
	$kConfig_new = new cConfig();
	$kConfig_new->loadCountry($postSearchAry['idOriginCountry']);
	$szCountryStrUrl = $kConfig_new->szCountryISO ;
	$kConfig_new->loadCountry($postSearchAry['idDestinationCountry']);
	$szCountryStrUrl .= $kConfig_new->szCountryISO ; 
	$szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
	$cancel_page_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
	//$cancel_page_url = __SELECT_SERVICES_URL__.'/'.$szBookingRandomNum."/";
	$t_base_confimation = "BookingConfirmation/";

	?>
		<div id="popup-bg"></div>
			<div id="popup-container">			
				<div class="popup abandon-popup">
				<h5><?=t($t_base_confimation.'title/price_update')?></h5>
				<p><?=t($t_base_confimation.'title/price_update_description')?> <?=$szPriceString?></p>
				<br>
				<div class="oh">
					<p align="center">
						<a href="javascript:void(0);" onclick="redirect_url('<?=$page_url?>')" class="button1"><span><?=t($t_base_confimation.'fields/cancel')?></span></a>						
					</p>
					</div>
			</div>
		</div>		
	<?
}

function incomplete_comapny_details_popup_upload_service($t_base)
{?>
	 <div id="popup-bg"></div>
		 <div id="popup-container">			
			  <div class="popup compare-popup">
			      <h5><strong><?=t($t_base.'forwarder_header/submit_data_for_upload_service')?></strong></h5>
			      <p><?=t($t_base.'forwarder_header/upload_service_message')?></p><br/>
			      <p align="center"><a href="javascript:void(0)" onclick="close_upload_service_popup();" class="button1"><span><?=t($t_base.'forwarder_header/close')?></span></a></p>
			  </div>
		 </div>			  
<?php	
}
function forwarder_company_information_popup($t_base,$kForwarder)
{	
	$idForwarder = $kForwarder->id;
	$kConfig = new cConfig();
	$allCountriesArr=$kConfig->getAllCountries(true);
?>

	<!--<link href="<?=__BASE_STORE_CSS_URL__?>/uploadify.css" type="text/css" rel="stylesheet" />-->
	<script type="text/javascript">
	//__APP_PATH_FORWARDER_IMAGES_TEMP__
	/*$(document).ready(function() {
  	$('#file_upload').uploadify({ 	
            'uploader'  : '<?=__FORWARDER_HOME_PAGE_URL__?>/guide/uploadify.swf',
            'script'    : '<?=__FORWARDER_HOME_PAGE_URL__?>/popupUploadify.php',
            'cancelImg' : '<?=__BASE_STORE_IMAGE_URL__?>/cancel.png',
            'folder'    : '<?=__APP_PATH_FORWARDER_IMAGES__POP_UP_TEMP__?>',
            'buttonClass' : 'uploadifyProgressBar',
            'buttonText' : 'UPLOAD',
            'sizeLimit': 5*1024*1024,
            'auto'      : true,
            'height'        : 33,
            'multi': false,
            'queueSizeLimit': 1,
            'removeCompleted' : true,
            'fileDesc' : 'Image files',
            'removeTimeout' : 200,
            'successTimeout' : 30,
            'method' : 'post',
            'progressData' : 'all',
            'requeueErrors' : true,
            'fileExt': '*.jpg;*.jpeg;*.gif;*.png;',
            'formData'        : {},               
            'preventCaching'  : true,
            'onQueueFull': function(event, queueSizeLimit) {
		alert("Please don't put anymore files in me! You can upload " + queueSizeLimit + " files at once");
		return false;
	},
	 'onError': function (event, ID, fileObj, errorObj) {
            alert(errorObj.type + ' Error: ' + errorObj.info);
        },
        'onSelect': function(e, q, f) {
        	$('#loaderIn').css({'display':'block'});
            var validExtensions = new Array('jpg','jpeg','gif','png');				
            var fileName = f.name;
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            var res_ex=jQuery.inArray(fileNameExt,validExtensions);
           
            if (res_ex == -1)
            {
               //convert_image_to_jpg(fileName);
            }
            $('#file_uploadQueue').css( {'display':'none'});
        },
		'onComplete': function(event, ID, fileObj, response, data) {
			$('#loaderIn').css({'display':'none'});
			$('#file_uploadQueue').html( '');
			$('#uploadme').css({'display':'none'});
			$('.uploadfilelist').css({'display':'block'});
			var result = response.split('|||||');
			var filename_all = '';
			var newvalue='';	
			if(result[0]== true)
			{
				var size=fileObj.size;
				var sizeValue=size/1024;
				$('#file_uploadQueue').css( {'display':'block'});
				//alert("Filename: " + fileObj.name + "\nSize: " + fileObj.size + "\nFilepath: " + fileObj.filePath);
				//$("#nofile-selected").attr('style','display:none;');	
				$('#file_uploadQueue').html( '<span class="uploadfilelist" style="padding-left: 99px;"><a href="javascript:void(0)" onclick="deleteLogo();"></a></span>');
				var filename_all=$("#file_name").val();
				//var filecount=$("#filecount").attr('value');
				//var newfilecount=parseInt(filecount)+1;
				//$("#filecount").attr('value',newfilecount);
				$('#upload_file_success').html('<img alt="Logo" src="<?=__BASE_STORE_IMAGE_URL__?>/forwarders/temp/'+result[1]+'">');
				//$('#upload_file_success').html('<img alt="testign" src="<?=__BASE_STORE_IMAGE_URL__?>/forwarders/temp/'+result[1]+'">');
				$('#fileUpload').val(result[1]);
				checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');
				if(parseInt(newfilecount)=='5')
				{
					$("#fileuploadlink").attr('style','display:none');
				}
				
				if(filename_all!='')
				{
					var newvalue=filename_all+";"+fileObj.name+"|||||"+result[1]+"|||||"+sizeValue;
				}
				else
				{
					var newvalue=fileObj.name+"|||||"+result[1]+"|||||"+sizeValue;	
				}
				$("#file_name").attr('value',newvalue);
				
			}			
				// you can use here jQuery AJAX method to send info at server-side.
		} 
  });
});*/
    $(document).ready(function() {
                var settings = {
                url: "<?=__FORWARDER_HOME_PAGE_URL__?>/popupUploadify.php",
                method: "POST",
                allowedTypes:"jpg,jpeg,gif,png",
                fileName: "myfile",
                multiple: false,
                dragDrop: false,
                uploadButtonClass: "forwarder-logo-upload-button",
                maxFileSize: 1000000,
                dragDropStr:'Upload',
                onSuccess:function(files,data,xhr)
                { 
                    var IS_JSON = true;
                    try
                    {
                        var json = $.parseJSON(data);
                    }
                    catch(err)
                    {
                        IS_JSON = false;
                    } 
                    if(IS_JSON)
                    { 
                        var obj = JSON.parse(data); 

                        var iFileCounter = 1;
                        
                        var filecount=$("#filecount").attr('value');
                        var filecountOld=$("#filecount").attr('value');
                        
                        for (var i = 0, len = obj.length; i < len; i++)
                        { 
                            console.log(obj[i]);
                            var newfilecount = parseInt(filecount) + iFileCounter;  
                            var uploadedFileName = obj[i]['name']; 
                            var FileUrl="<?php echo __BASE_STORE_URL__?>/forwarders/forwarderBulkUploadServices/temp/"+uploadedFileName; 
                            var size  = obj[i]['size'];
                            var container_li_id = "id_"+newfilecount;
                            //var size=obj[i]['size'];
                            if(parseInt(filecount)==0)
                            {
                                $('#fileList').html('');
                            }
                            $("#upload_file_success").attr('style','display:block');
                            $('#upload_file_success').html( '<img alt="Logo" src="<?=__BASE_STORE_IMAGE_URL__?>/forwarders/temp/'+uploadedFileName+'">');  
                            //$('#'+container_li_id).html( '<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Page.png&temp=2&w=50&h=50'?>" border="0" /><span class="page">'+newfilecount+'</span>');

                            iFileCounter++; 

                            var filename_all=$("#file_name").val();  
                            if(filename_all!='')
                            { 
                                var newvalue=filename_all+";"+files+"|||||"+uploadedFileName+"|||||"+size;
                                $("#fileuploadlink").attr('style','display:block');
                                $("#file_uploade_plus_link").attr('style','display:block');
                            }
                            else
                            { 
                                var newvalue=files+"|||||"+uploadedFileName+"|||||"+size;
                                $("#fileuploadlink").attr('style','display:block');	
                                $("#file_uploade_plus_link").attr('style','display:block');
                            }
                        }
                        checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');
                        $("#filecount").attr('value',newfilecount);
                        $("#file_name").attr('value',newvalue);
                        $("#upload_process_list").attr('style','display:none');
                        $("#upload_process_list").html('');
                        
                        
                       // $(".upload").attr('style','position: relative; overflow: hidden; cursor: default;display:none;');
                        
                        

                         
                    }
                    else
                    {  
                       // $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='showHide(\"upload_process_list\")'>close</a></p>");
                    }

                },onSelect:function(s){
                    $("#upload_process_list").attr('style','display:block;top:93%;width:345px');
                    $("#upload_error_container").attr('style','display:none');
                },
                    afterUploadAll:function()
                    {
                        //alert("all images uploaded!!");
                    },
                    onError: function(files,status,errMsg)
                    {
                        //$("#status").html("<font color='red'>Upload is Failed</font>");
                       // alert('hi');
                    }
                }
                $("#upload_file").uploadFile(settings);
                
                
            });
	</script>
</script> 
	<div id="Error"></div>
	<h4 style="margin-bottom:5px;"><strong><?=t($t_base.'titles/display_customers_transporteca');?></strong></h4>		
	<form name="updateRegistComapnyForm" id="updateRegistComapnyForm" method="post" onkeyup="checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');">	
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/display_name');?></p>
			<p class="field-container"><input type="text" name="updateRegistComapnyAry[szDisplayName]" value="<?=(isset($_POST['updateRegistComapnyAry']['szDisplayName'])?$_POST['updateForwarderComapnyAry']['szDisplayName']:$kForwarder->szDisplayName);?>" onblur="closeTip('display_name');" onfocus="openTip('display_name');"></p>
			<div class="field-alert"><div id="display_name" style="display:none;"><?=t($t_base.'messages/display_name');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/general_email_address');?></p>
			<p class="field-container"><input type="text" name="updateRegistComapnyAry[szGeneralEmailAddress]" value="<?=(isset($_POST['updateRegistComapnyAry']['szGeneralEmailAddress'])?$_POST['updateForwarderComapnyAry']['szGeneralEmailAddress']:$kForwarder->szGeneralEmailAddress);?>" onblur="closeTip('general_email');" onfocus="openTip('general_email');"></p>
			<div class="field-alert"><div id="general_email" style="display:none;"><?=t($t_base.'messages/general_email');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/standard_terms');?></p>
			<p class="field-container"><input type="text" name="updateRegistComapnyAry[szLink]" value="<?=(isset($_POST['updateRegistComapnyAry']['szLink'])?$_POST['updateForwarderComapnyAry']['szLink']:$kForwarder->szLink);?>" onblur="closeTip('terms_standard');" onfocus="openTip('terms_standard');"></p>
			<div class="field-alert"><div id="terms_standard" style="display:none;"><?=t($t_base.'messages/terms_standard');?></div></div>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'titles/display_logo');?></p>
			<p class="field-container" style="width: 69%;height: 40px;">
			<?php
			if(isset($_POST['updateRegistComapnyAry']['szForwarderLogo'])?$_POST['updateRegistComapnyAry']['szForwarderLogo']:$kForwarder->szLogo)
			{
				$block="none";
				// if forwarder logo already uploaded then temp will image will render in this span after upload
				?>
				<span id="upload_file_success" class="forwarder_logo" onmouseover="close_otherTip();openTip('image_message');" onmouseout="closeTip('image_message');"><img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.($_POST['updateRegistComapnyAry']['szForwarderLogo']?$_POST['updateRegistComapnyAry']['szForwarderLogo']:$kForwarder->szLogo)."&w=".__MAX_ALLOWED_FORWARDER_LOGO_WIDTH__."&h=".__MAX_ALLOWED_FORWARDER_LOGO_HEIGHT__?>" alt="<?=$kForwarder->szDisplayName?>" ></span>
				<span class="uploadfilelist" style="padding-left: 99px;"><a href="javascript:void(0)" onclick="deleteLogo();"></a></span>
				<? 
			}
			else
			{
				$block="block";
			?>
			<span id="upload_file_success" class="forwarder_logo" onmouseover="close_otherTip();openTip('image_message');" onmouseout="closeTip('image_message');"></span>
			<span id="file_uploadQueue" ></span>
			<?php }?>
				<span id="uploadme" style="display:<?=$block?>;float: left;width: 300px;margin-left: -91px;"> 
				<span class="fl-20" id="loaderIn" style="float: right;display: none;margin-right: 60px;"><img src="<?=__BASE_STORE_IMAGE_URL__?>/325.gif"></span>
				<span class="fl-40" style="direction: ltr;" onmouseover="close_otherTip();openTip('image_message');" onmouseout="closeTip('image_message');">
				<!--<a id="file_upload" href="javascript:void(0);" style="float: left;"><?=t($t_base.'feilds/upload_new_file');?></a>-->
				<div class="field-alert"><div id="image_message" style="display:none;"><?=t($t_base.'messages/display_image_message');?></div></div>
				</span>
				</span>
                                <input type="hidden" id="file_name" name="updateRegistComapnyAry[szForwarderLogo]" value="<?=(isset($_POST['updateRegistComapnyAry']['szForwarderLogo'])?$_POST['updateRegistComapnyAry']['szForwarderLogo']:$kForwarder->szLogo);?>">    
				<!--<input type="hidden" id="file_name" name="updateRegistComapnyAry[iFileName]" value="">-->
				<input type="hidden" name="updateRegistComapnyAry[idForwarder]" value="<?=$idForwarder?>">
				<input type="hidden" name="updateRegistComapnyAry[iDeleteLogo]" id="delete_logo_hidden" value="">
				<input type="hidden" name="updateRegistComapnyAry[szOldLogo]" id="szOldLogo" value="<?=$kForwarder->szLogo?>">
			</p>
		</div>
                <div class="profile-fields">
                    <div class="fl-30">&nbsp;</div>
                    <div style="max-width:210px;position:relative;float:left;">
                        <div id="upload_file">
                           <span><?=t($t_base.'feilds/upload_new_file');?></span>	
                       </div>
                    </div>    
                </div>   
        </form>
		</div>	
		<h4><strong><?=t($t_base.'titles/registered_company_details');?></strong></h4>	
		<div id="registered_company_details">
	<form name="updateRegistComapnyForm" id="updateRegistComapnyForm2" method="post" onkeyup="checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');">	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/company_name');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCompanyName]" value="<?=(isset($_POST['updateRegistComapnyAry']['szCompanyName'])?$_POST['updateRegistComapnyAry']['szCompanyName']:$kForwarder->szCompanyName)?>" onblur="closeTip('name');" onfocus="openTip('name');"></p>
		<div class="field-alert"><div id="name" style="display:none;"><?=t($t_base.'messages/company_name');?></div></div>
		
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address1');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szAddress]" value="<?=(isset($_POST['updateRegistComapnyAry']['szAddress'])?$_POST['updateRegistComapnyAry']['szAddress']:$kForwarder->szAddress)?>" onblur="closeTip('add');" onfocus="openTip('add');"></p>
		<div class="field-alert"><div id="add" style="display:none;"><?=t($t_base.'messages/address_line_1');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address2');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" id="address2" name="updateRegistComapnyAry[szAddress2]" value="<?=(isset($_POST['updateRegistComapnyAry']['szAddress2'])?$_POST['updateRegistComapnyAry']['szAddress2']:$kForwarder->szAddress2)?>" onblur="closeTip('add2');" onfocus="openTip('add2');"></p>
		<div class="field-alert"><div id="add2" style="display:none;"><?=t($t_base.'messages/address_line_2');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/address3');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" id="address3" name="updateRegistComapnyAry[szAddress3]" value="<?=(isset($_POST['updateRegistComapnyAry']['szAddress3'])?$_POST['updateRegistComapnyAry']['szAddress3']:$kForwarder->szAddress3)?>" onblur="closeTip('add3');" onfocus="openTip('add3');"></p>
		<div class="field-alert"><div id="add3" style="display:none;"><?=t($t_base.'messages/address_line_3');?></div></div>
	</div>
	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/postcode');?></p>
		<p class="field-container"><input type="text" id="szPostcode" name="updateRegistComapnyAry[szPostCode]" value="<?=(isset($_POST['updateRegistComapnyAry']['szPostCode'])?$_POST['updateRegistComapnyAry']['szPostCode']:$kForwarder->szPostCode)?>" onblur="closeTip('postcode');" onfocus="openTip('postcode');"></p>
		<div class="field-alert"><div id="postcode" style="display:none;"><?=t($t_base.'messages/postal_code');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/city');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCity]" value="<?=(isset($_POST['updateRegistComapnyAry']['szCity'])?$_POST['updateRegistComapnyAry']['szCity']:$kForwarder->szCity)?>" onblur="closeTip('city');" onfocus="openTip('city');"></p>
		<div class="field-alert"><div id="city" style="display:none;"><?=t($t_base.'messages/city');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/province');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szState]" id="provinces" value="<?=(isset($_POST['updateRegistComapnyAry']['szState'])?$_POST['updateRegistComapnyAry']['szState']:$kForwarder->szState)?>" onblur="closeTip('state');" onfocus="openTip('state');"></p>
		<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/state');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/country');?></p>
		<p class="field-container">
			<select size="1" name="updateRegistComapnyAry[idCountry]" style="width:212px;" id="idCountry" onchange="checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');">
			<option value=""><?=t($t_base.'titles/select_country');?></option>
			<?php
				if(!empty($allCountriesArr))
				{
					foreach($allCountriesArr as $allCountriesArrs)
					{
						?><option value="<?=$allCountriesArrs['id']?>" <?php if(($allCountriesArrs['id']==(isset($_POST['updateRegistComapnyAry']['idCountry'])?$_POST['updateRegistComapnyAry']['idCountry']:$kForwarder->idCountry))){?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			?>
		   </select></p>
		<div class="field-alert"><div id="state" style="display:none;"><?=t($t_base.'messages/state');?></div></div>
	</div>
	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/phone_number');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szPhone]" value="<?=(isset($_POST['updateRegistComapnyAry']['szPhone'])?$_POST['updateRegistComapnyAry']['szPhone']:$kForwarder->szPhone)?>" onblur="closeTip('phone');" onfocus="openTip('phone');"></p>
		<div class="field-alert"><div id="phone" style="display:none;"><?=t($t_base.'messages/phone');?></div></div>
	</div>	
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/company_reg_num');?></p>
		<p class="field-container"><input type="text" name="updateRegistComapnyAry[szCompanyRegistrationNum]" value="<?=(isset($_POST['updateRegistComapnyAry']['szCompanyRegistrationNum'])?$_POST['updateRegistComapnyAry']['szCompanyRegistrationNum']:$kForwarder->szCompanyRegistrationNum)?>" onblur="closeTip('cmp');" onfocus="openTip('cmp');"></p>
		<div class="field-alert"><div id="cmp" style="display:none;"><?=t($t_base.'messages/company_reg_number');?></div></div>
	</div>
	<div class="profile-fields">
		<p class="fl-30"><?=t($t_base.'titles/vat_reg_num');?> <span class="optional">(<?=t($t_base.'titles/optional');?>)</span></p>
		<p class="field-container"><input type="text" id="vatDisplay" name="updateRegistComapnyAry[szVatRegistrationNum]" value="<?=(isset($_POST['updateRegistComapnyAry']['szVatRegistrationNum'])?$_POST['updateRegistComapnyAry']['szVatRegistrationNum']:$kForwarder->szVatRegistrationNum)?>" onblur="closeTip('reg');" onfocus="openTip('reg');"></p>
		<div class="field-alert1"><div id="reg" style="display:none;"><?=t($t_base.'messages/company_vat_reg_number');?></div></div>
	</div>	
</form>
	<div class="oh">
		<p class="fl-60" style="margin-top:5px"><?=t($t_base.'titles/you_can_edit');?></p>
		<p align="right" class="fr-40">
			<a id="submitPopUp1" href="javascript:void(0);" style="opacity:0.4" class="button1"><span><?=t($t_base.'feilds/next_step');?></span></a>
		</p>
	</div>	
	<script type="text/javascript">
	checkTheEnteties('updateRegistComapnyForm','updateRegistComapnyForm2');
	</script>		
<?php
} 
function convert_image_to_jpg($file_name)
{
	//$file_name = '5star_gray_small.png';
	if(!empty($file_name))
	{
		$image_path =__APP_PATH_FORWARDER_IMAGES__POP_UP_TEMP__.''.$file_name ;
		$image_file_type = exif_imagetype($image_path);
		
		switch ($image_file_type) 
		{
		    case IMAGETYPE_GIF :
		        $img = imagecreatefromgif($image_path);
		        break;
		    case IMAGETYPE_PNG :
		        $img = imagecreatefrompng($image_path);
		        break;
		    default :
		        break;
		}
		//echo $img;
		if($img)
		{
			$fileArr=explode(".",$file_name);
			$newImageName=$fileArr[0].".jpg";
			//echo $newImageName;
			$newImage = __APP_PATH_FORWARDER_IMAGES__POP_UP_TEMP__."".$fileArr[0].".jpg";
			//echo $newImage;
			imagejpeg($img, $newImage);
			return $newImageName;
		}
	}
}

function haulage_video_popup($iHaulageVideo=0)
{
	$t_base="BulkUpload/";
        $kWarehouse = new cWHSSearch();	
    $lclServiceVideo=$kWarehouse->getManageMentVariableByDescription('__HAULAGE_VIDEO__');	
    $youtubeId=get_youtube_id($lclServiceVideo);
?>
	<div id="show_haulage_pop_up">
	<div id="update_haulage_video_flag"></div>
	<div id="popup-bg"></div>
		<div id="popup-container" style="margin-top:60px;">
			<div class="multi-payment-transfer-parent-popup popup" style="width:740px;height:550px;"> 
				<p class="close-icon" align="right">
				<a onclick="window.top.window.close_haulage_video_popup();" href="javascript:void(0);">
				<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
				</a>
				</p>
				
				<div style="margin-top:5px;" class="haulage-video">
				<iframe id="google_map_target_video_popup" align="center" valign="middle" class="google_map_select"  name="google_map_target_video_popup" src="https://www.youtube.com/embed/<?php echo $youtubeId;?>?autoplay=1&rel=0" frameborder="0" allowfullscreen ></iframe>
				</div>
				
				<p style="margin-top:10px;">
				<input type="checkbox" id="checkBox" value="1" <? if($iHaulageVideo!='1') {?> checked <? }?> > <?=t($t_base.'title/haulage_video_checkbox_msg');?>
				</p>
				<p align="center" style="margin-top:10px;"><a href="javascript:void(0)" onclick="update_haulage_video();" class="button1"><span><?=t($t_base.'fields/close')?></span></a></p>
			</div>
		</div>
	</div>	
<?php
}
function lclservice_video_popup($iLCLServiceVideo=0)
{
    $t_base="BulkUpload/";
    $kWarehouse = new cWHSSearch();	
    $lclServiceVideo=$kWarehouse->getManageMentVariableByDescription('__LCL_SERVICE_VIDEO__');	
    $youtubeId=get_youtube_id($lclServiceVideo);
?>
    <div id="show_lclservice_pop_up">
    <div id="update_lclservice_video_flag"></div>
    <div id="popup-bg"></div>
        <div id="popup-container" style="margin-top:60px;">
            <div class="multi-payment-transfer-parent-popup popup" style="width:740px;height:550px;"> 
                <p class="close-icon" align="right">
                <a onclick="window.top.window.close_lclservice_video_popup();" href="javascript:void(0);">
                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
                </p> 
                <div style="margin-top:5px;" class="videoWrapper">
                    <iframe id="google_map_target_video_popup" align="center" valign="middle" class="google_map_select"  name="google_map_target_video_popup" src="https://www.youtube.com/embed/<?php echo $youtubeId;?>?autoplay=1&rel=0" frameborder="0" allowfullscreen ></iframe>
                </div>

                <p style="margin-top:10px;">
                <input type="checkbox" id="checkBox" value="1" <? if($iLCLServiceVideo!='1') {?> checked <? }?> > <?=t($t_base.'title/lclservice_video_checkbox_msg');?>
                </p>
                <p align="center" style="margin-top:10px;"><a href="javascript:void(0)" onclick="update_lclservice_video();" class="button1"><span><?=t($t_base.'fields/close')?></span></a></p>
            </div>
        </div>
    </div>	
<?php
} 
function display_forwarder_preferenced_emails()
{
    $t_base = "ForwardersCompany/Preferences/";  
    $table_width = "25";
    
    $idForwarder = $_SESSION['forwarder_id'] ;
    $kForwarderContact = new cForwarderContact();
    $kConfig = new cConfig();
    
    $preferencesListAry = array();
    $preferencesListAry = $kForwarderContact->getAllQuotesAndBookingEmails($idForwarder);
    
    $countryRegionAry = array();
    $countryRegionAry = $kConfig->getAllCountriesForPreferencesKeyValuePair();
    
    $uagent = $_SERVER['HTTP_USER_AGENT'];
    $szExtraClass = '';
    $szOSName = os_info($uagent) ; 
    if($szOSName=='MacOS' || $szOSName=='iPad' || $szOSName=='iPhone')
    {
        $szExtraClass = ' mac-table-header'; 
    } 
   //echo $szOSName."<br/>";
    //echo $szExtraClass;
    ?> 
    <script src="<?php echo __BASE_STORE_SECURE_JS_URL__;?>/nanoslider.js?v=2" type="text/javascript"></script>
    <script type="text/javascript">
        $().ready(function() {	
           // $(".nano").nanoScroller({scroll: 'top'});
        });
    </script> 
    <div class="clearfix courier-provider courier-provider-scroll" style="max-height: 450px;">
    <table cellpadding="0" class="format-7"  cellspacing="0"  width="100%" id="forwarder_preferences_table">
        <tr>
            <td width="<?=$table_width-15?>%" valign="top"><strong><?=t($t_base.'titles/mode')?></strong></td>
            <td width="<?=$table_width-10?>%" valign="top"><strong><?=t($t_base.'titles/from')?></strong></td>
            <td width="<?=$table_width-10?>%" valign="top"><strong><?=t($t_base.'titles/to');?></strong></td> 
            <td width="<?=$table_width+5?>%" valign="top"><strong><?=t($t_base.'titles/pricing_email');?></strong></td>
            <td width="<?=$table_width+5?>%" valign="top"><strong><?=t($t_base.'titles/booking_email');?></strong></td>
        </tr> 
        <?php
            $ctr = 1;
            if(!empty($preferencesListAry))
            { 
                foreach($preferencesListAry as $preferencesListArys)
                { 
                     $szOriginCountry = $countryRegionAry[$preferencesListArys['idOriginCountry']]['szCountryName'] ;
                     $szDestinationCountry = $countryRegionAry[$preferencesListArys['idDestinationCountry']]['szCountryName'] ;
                    ?>						
                    <tr id="forwarder_preferences_tr_<?php echo $ctr; ?>" onclick="select_forwarder_preference_tr('forwarder_preferences_tr_<?php echo $ctr; ?>','<?=$preferencesListArys['id']?>');">
                        <td width="<?=$table_width-15?>%" valign="top"><?php echo $preferencesListArys['szTransportMode']; ?></td>
                        <td width="<?=$table_width-10?>%" valign="top"><?php echo $szOriginCountry; ?></td>
                        <td width="<?=$table_width-10?>%" valign="top"><?php echo $szDestinationCountry; ?></td>
                        <td width="<?=$table_width+5?>%" valign="top"><?php echo $preferencesListArys['szEmail']; ?></td>
                        <td width="<?=$table_width+5?>%" valign="top"><?php echo $preferencesListArys['szBookingEmail']; ?></td>
                    </tr>
                    <?php
                    $ctr++;
                } 
            }
            else
            {
                ?>
                <tr>
                    <td colspan="5" align="center"><h4><b><?=t($t_base.'fields/no_record_found');?></b></h4></td>
                </tr> 
                <?php 
            }
        ?>
        </table>  
   </div>
    <br class="clear-all" />   
    <br>
    <div id="forwarder_preferences_main_form_container">
        <?php
            echo display_preference_emails_form($kForwarderContact);
        ?>	
    </div> 
    <br>
    <div class="btn-container">
        
        <span style="float: right;"> 
        	<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="delete_forwarder_preference_button" align="left"><span><?=t($t_base.'fields/delete');?></span></a>
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_forwarder_preference_button"><span><?=t($t_base.'fields/add');?></span></a>
            
        </span>
    </div> 
    <br class="clear-all" />
    <?php
}

function display_preference_emails_form($kForwarderContact,$preferencesDetailsAry=array())
{
    $t_base = "ForwardersCompany/Preferences/";   
    if(!empty($kForwarderContact->arErrorMessages))
    {
        $formId = 'add_forwarder_preferences_form';
        $szValidationErrorKey = '';
        foreach($kForwarderContact->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#"+'<?php echo $errorKey; ?>').addClass('red_border');
            </script>
            <?php
        }
    }
    $kConfig = new cConfig();
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false,false,true,true,true);
    
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountriesForPreferences();
    
    $onfocus_func = "addForwarderPreferences();";
    if(!empty($_POST['addPreferencesAry']))
    {
        $preferencesDetailsAry = $_POST['addPreferencesAry'];  
        $preferencesDetailsAry['id'] = $preferencesDetailsAry['idPreferences'] ;
    }
    if(!empty($preferencesDetailsAry))
    {
        $idTransportMode = $preferencesDetailsAry['idTransportMode'] ;
        $idOriginCountry = $preferencesDetailsAry['idOriginCountry'] ;
        $idDestinationCountry = $preferencesDetailsAry['idDestinationCountry'] ;
        $szEmail = $preferencesDetailsAry['szEmail'] ;
        $idPreferences = $preferencesDetailsAry['id'] ; 
        $szBookingEmail =$preferencesDetailsAry['szBookingEmail'];
    }
    
    ?>
    <div id="isurance_buy_rate_form">
	<form action="javascript:void(0);" name="add_forwarder_preferences_form" id="add_forwarder_preferences_form">
            <table cellpadding="0" cellspacing="3" width="100%">
		<tr>
                    <td style="width:10%;line-height: 10px;" class="f-size-12" valign="bottom"><?=t($t_base.'titles/mode')?></td>
                    <td style="width:15%;line-height: 10px;" class="f-size-12" valign="bottom"><?=t($t_base.'titles/from')?></td>
                    <td style="width:15%;line-height: 10px;" class="f-size-12" valign="bottom"><?=t($t_base.'titles/to')?></td>
                    <td style="width:30%;line-height: 10px;" class="f-size-12" valign="bottom"><?=t($t_base.'titles/pricing_email')?></td>
                    <td style="width:30%;line-height: 10px;" class="f-size-12" valign="bottom"><?=t($t_base.'titles/booking_email')?></td>
		</tr>
		<tr>
                    <td valign="top">
                        <select class="addInsurateRatesFields" style="width:100%;" onfocus="<?php echo $onfocus_func; ?> check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addPreferencesAry[idTransportMode]" id="idTransportMode">
                            <option value="">Select</option>
                        <?php
                            if(!empty($transportModeListAry))
                            {
                                foreach($transportModeListAry as $transportModeListArys)
                                {
                                    ?>
                                    <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($idTransportMode==$transportModeListArys['id'])?'selected':''); ?>><?=$transportModeListArys['szShortName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td valign="top">
                        <select class="addInsurateRatesFields" style="width:100%;" onfocus="<?php echo $onfocus_func; ?> check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addPreferencesAry[idOriginCountry]" id="idOriginCountry">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idOriginCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td valign="top">
                        <select class="addInsurateRatesFields" style="width:100%;" onfocus=" check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addPreferencesAry[idDestinationCountry]" id="idDestinationCountry">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idDestinationCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td valign="top">
                        <input type="text" style="width:97%;" class="addInsurateRatesFields" onfocus="<?php echo $onfocus_func; ?> check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addPreferencesAry[szEmail]" id="szEmail" value="<?php echo $szEmail; ?>">
                    </td>
                    <td valign="top">
                        <input type="text" style="width:99%;" class="addInsurateRatesFields" onfocus="<?php echo $onfocus_func; ?> check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addPreferencesAry[szBookingEmail]" id="szBookingEmail" value="<?php echo $szBookingEmail; ?>">
                    </td>
		</tr>
            </table> 
            <input type="hidden" name="addPreferencesAry[idPreferences]" id="idPreferences" value="<?php echo $idPreferences; ?>">
	</form>
    </div>
    <?php
}

function display_forwarder_preferences_delete_confirmation($preferencesAry)
{  
    $t_base = "ForwardersCompany/Preferences/";   
 ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup" style="top:75px;">
            <h3><?=t($t_base.'titles/delete_heading');?></h3>
            <p><?=t($t_base.'titles/confirmation_message');?>?</p>
            <br/>
            <p align="center">
                <a href="javascript:void(0)" onclick="forwarder_preferences_email_popup('<?php echo $preferencesAry['id']; ?>','DELETE_FORWARDER_PREFERENCES_EMAIL_CONFIRM')" class="button1" ><span><?=t($t_base.'fields/yes_pop');?></span></a> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/no_pop');?></span></a>
            </p>
        </div>
    </div>
<?php 
} 
function display_all_peding_tasks_forwarder($incompleteTaskAry,$idBookingQuote=false,$idForwarderSession=false,$past_quote_flag=false,$idForwarder=0,$page=1)
{  
    $t_base="management/pendingTray/";  
    //value for $idBookingFile is only passed on creating of new booking file. 
    $kBooking = new cBooking(); 
    $idForwarderSession = $_SESSION['forwarder_id'] ;
    if($idForwarder<=0)
    {
        $idForwarder = $idForwarderSession;
    }
    
    /*if($past_quote_flag)
    {
    	$incompleteTaskTotalAry = $kBooking->getAllBookingQuotesByForwarder($idForwarder,false,false,false,true,array(),0,true);
    	$incompleteTaskTotalAry1=$kBooking->getAllBookingCourierLabelByForwarder($idForwarder,0,true);
	
        $mergeArr=array_merge($incompleteTaskTotalAry,$incompleteTaskTotalAry1);
        $gtotal=count($mergeArr); 
    }
    else
    { 
    	$incompleteTaskTotalAry = $kBooking->getAllBookingQuotesByForwarder($idForwarder,false,true,false,false,array(),0,true);
        $incompleteTaskTotalAry1=$kBooking->getAllBookingCourierLabelByForwarder($idForwarder,0,false);
	
        $mergeArr=array_merge($incompleteTaskTotalAry,$incompleteTaskTotalAry1);
        $gtotal=count($mergeArr);
    }*/
    $gtotal=count($incompleteTaskAry);
    //echo $gtotal."gtotal<br/>";
    //echo count($incompleteTaskAry)."incompleteTaskAry<br/>";
    ?>      
    <form id="send_new_insured_booking" name="send_new_insured_booking" action="javascript:void(0);" method="post">  
        <table cellpadding="5" cellspacing="0" class="format-4" width="100%" style="min-height:200px;" id="insuranced_booking_table">
            <tr>
                <th style="width:8%;" valign="top"><?=t($t_base.'fields/time')?></th> 
                <?php if($past_quote_flag){?>
                    <th style="width:15%;" valign="top"><?=t($t_base.'fields/status');?></th>
                <?php } else {?>
                    <th style="width:15%;" valign="top"><?=t($t_base.'fields/task');?></th>
                <?php }?> 
                <th style="width:21%;" valign="top"><?=t($t_base.'fields/sent_to');?></th>
                <th style="width:6%;" valign="top"><?=t($t_base.'fields/mode');?></th> 
                <th style="width:15%;" valign="top"><?=t($t_base.'fields/from');?></th>
                <th style="width:15%;" valign="top"><?=t($t_base.'fields/to');?></th>
                <th style="width:20%;" valign="top"><?=t($t_base.'fields/cargo_text');?></th>
            </tr> 
	<?php  
		require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
		$kPagination  = new Pagination($gtotal,__CONTENT_PER_PAGE_PENDING_TRAY__,15);
                
		if(!empty($incompleteTaskAry))
		{
                    $ctr = 1;
                    $irowCount = count($incompleteTaskAry); 
                    $kConfig = new cConfig();
                    $taskStatusAry = array();
                    $taskStatusAry = $kConfig->getAllActiveTaskStatus(); 
                    $fileStatusAry = $kConfig->getAllActiveFileStatus();
                    
                    $selected_tr_id = '';
                    $startFlow=($page-1)*__CONTENT_PER_PAGE_PENDING_TRAY__;
                   
                    $end=$startFlow+__CONTENT_PER_PAGE_PENDING_TRAY__-1;
                    if($end>=$gtotal)
                    {
                            $end=$gtotal-1;
                    }	
                    for($t=$startFlow;$t<=$end;++$t)
                    {    
                        if($incompleteTaskAry[$t]['idTransportMode']==4) //courier 
                        {
                            if($incompleteTaskAry[$t]['iCourierBooking']==1)
                            {
                                $textPackage="package";
                                if($incompleteTaskAry[$t]['iTotalQuantity']>1)
                                {
                                    $textPackage="packages"; 
                                } 
                                $szVolWeight = format_volume($incompleteTaskAry[$t]['fCargoVolume'])."cbm / ".number_format((float)$incompleteTaskAry[$t]['fCargoWeight'])."kg / ".number_format((int)$incompleteTaskAry[$t]['iTotalQuantity'])."".$textPackage;
                            }
                            else
                            {
                                $szVolWeight = format_volume($incompleteTaskAry[$t]['fCargoVolume'])."cbm / ".number_format((float)$incompleteTaskAry[$t]['fCargoWeight'])."kg / ".number_format((int)$incompleteTaskAry[$t]['iNumColli'])."Col";
                            }    
                        }
                        else
                        {
                            $szVolWeight = format_volume($incompleteTaskAry[$t]['fCargoVolume'])."cbm / ".number_format((float)$incompleteTaskAry[$t]['fCargoWeight'])."kg ";
                        }
                        //echo $incompleteTaskAry[$t]['szForwarderContact']."---".$incompleteTaskAry[$t]['id'];
                        $szCustomerName = str_replace(" ","",$incompleteTaskAry[$t]['szForwarderContact']);
                        $szCustomerName = returnLimitData($szCustomerName,25) ;
                        $showManagmentEmail=false;   
                        
                            $bookingQuotePricingExistFlag=true;
                            if($incompleteTaskAry[$t]['iBookingType']==__BOOKING_TYPE_RFQ__ && (int)$incompleteTaskAry[$t]['idQuotePricingDetails']>0)
                            {
                                
                                $kBookingnew = new cBooking();
                                if(!$kBookingnew->idBookingQuotePricingExists($incompleteTaskAry[$t]['idQuotePricingDetails'],$incompleteTaskAry[$t]['id']))
                                {                                    
                                    $bookingQuotePricingExistFlag=false;
                                    if(!empty($incompleteTaskAry[$t]['szForwarderQuoteStatus']))
                                    {
                                        $szTaskStatus = $fileStatusAry[$incompleteTaskAry[$t]['szForwarderQuoteStatus']]['szDescription'];
                                    }
                                    else if(!empty($incompleteTaskAry[$t]['szForwarderQuoteTask']))
                                    {
                                        $szTaskStatus = $taskStatusAry[$incompleteTaskAry[$t]['szForwarderQuoteTask']]['szDescription'];
                                    }
                                }
                            }
                            if($bookingQuotePricingExistFlag)
                            {
                                if(!empty($incompleteTaskAry[$t]['szForwarderBookingTask']))
                                {
                                    $szTaskStatus = $taskStatusAry[$incompleteTaskAry[$t]['szForwarderBookingTask']]['szDescription'];
                                }
                                else if(!empty($incompleteTaskAry[$t]['szForwarderBookingStatus']))
                                {
                                    $szTaskStatus = $fileStatusAry[$incompleteTaskAry[$t]['szForwarderBookingStatus']]['szDescription'];
                                }
                                else if(!empty($incompleteTaskAry[$t]['szForwarderQuoteStatus']))
                                {
                                    $szTaskStatus = $fileStatusAry[$incompleteTaskAry[$t]['szForwarderQuoteStatus']]['szDescription'];
                                }
                                else if(!empty($incompleteTaskAry[$t]['szForwarderQuoteTask']))
                                {
                                    $szTaskStatus = $taskStatusAry[$incompleteTaskAry[$t]['szForwarderQuoteTask']]['szDescription'];
                                }
                                else if(!empty($incompleteTaskAry[$t]['szTransportecaStatus']))
                                {
                                    $showManagmentEmail=true; 
                                    $szTaskStatus = $fileStatusAry[$incompleteTaskAry[$t]['szTransportecaStatus']]['szDescription'];
                                }
                                else if(!empty($incompleteTaskAry[$t]['szTransportecaTask']))
                                {
                                    $showManagmentEmail=true; 
                                    $szTaskStatus = $taskStatusAry[$incompleteTaskAry[$t]['szTransportecaTask']]['szDescription'];
                                }
                            }
                        
                        
                        

                        if($szCustomerName!='')
                        {
                            $showManagmentEmail=false;
                        }
                        if($incompleteTaskAry[$t]['iClosed']==1)
                        {
                           $dtTaskStatusUpdatedOn = $incompleteTaskAry[$t]['dtStatusUpdatedOn']; 
                           //echo $dtTaskStatusUpdatedOn."dtTaskStatusUpdatedOn";
                        }
                        else
                        {
                            $dtTaskStatusUpdatedOn = $incompleteTaskAry[$t]['dtTaskStatusUpdatedOn']; 
                        } 
                        
                        
                        $iTaskCreatedTime = strtotime(date('Y-m-d',strtotime($dtTaskStatusUpdatedOn))) ;
                        $iTodayDateTime = strtotime(date('Y-m-d'));
                        $iDayLightSavingTime = 0;
                        if(date('I')==1)
                        {
                            $iDayLightSavingTime =1;
                            //$dtTaskStatusUpdatedOn= date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($dtTaskStatusUpdatedOn)));  
                        }
                         

                        $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ; 
                        $date = new DateTime($dtTaskStatusUpdatedOn); 
                        $date->setTimezone(new DateTimeZone($szCustomerTimeZone)); 

                        if($iTaskCreatedTime==$iTodayDateTime)
                        { 
                            $szTaskUpdatedOn = $date->format('H:i') ; //In local time zone
                            $szTaskUpdatedOn_1 = date('H:i',strtotime($dtTaskStatusUpdatedOn)) ; //In server time zone
                        }
                        else
                        {
                            $szTaskUpdatedOn = $date->format('d-M') ; //In local timezone
                            $szTaskUpdatedOn_1 = date('d-M',strtotime($dtTaskStatusUpdatedOn)); //In Server time zone
                        }  
                        
                        
                        $szTransportMode = $incompleteTaskAry[$t]['szTransportMode']; 
                        
                        $iDisplayPopup = 0;
                        // IF the ‘File owner’ is NULL OR IF the ‘File owner’ is different  than than the operator THEN a popup message should appear.
                        if($incompleteTaskAry[$t]['idFileOwner']<=0 || $incompleteTaskAry[$t]['idFileOwner']!=$_SESSION['admin_id'])
                        {
                            $iDisplayPopup = 1 ;
                        }  
                        
                        
                        if(($idBookingQuote>0) && ($incompleteTaskAry[$t]['iQuotationId']==$idBookingQuote))
                        {
                            $selected_tr_id = "booking_data_".$ctr ;
                            $iSelectedQuoteId = $incompleteTaskAry[$t]['id'];
                            $iCourierFlag = 0;
                        } 
                        else if($incompleteTaskAry[$t]['iCourierBooking']==1 || $incompleteTaskAry[$t]['isManualCourierBooking']==1)
                        {
                            
                            
                            if(($idBookingQuote>0) && ($incompleteTaskAry[$t]['idBooking']==$idBookingQuote))
                            {
                                $selected_tr_id = "booking_data_".$ctr ;
                            }
                            //$iSelectedQuoteId = $incompleteTaskAry[$t]['id'];
                            $kConfig = new cConfig();
                            $data = array();
                            $data['idForwarder'] = $incompleteTaskAry[$t]['idForwarder'];
                            $data['idTransportMode'] = $incompleteTaskAry[$t]['idTransportMode'];
                            $data['idOriginCountry'] = $incompleteTaskAry[$t]['idOriginCountry'];
                            $data['idDestinationCountry'] = $incompleteTaskAry[$t]['idDestinationCountry'];
                            
                            $forwarderContactAry = array();
                            $forwarderContactAry = $kConfig->getForwarderContact($data);
                            $incompleteTaskAry[$t]['szForwarderContact']=implode(",",$forwarderContactAry);
                            $szCustomerName = returnLimitData($incompleteTaskAry[$t]['szForwarderContact'],25) ;
                            
                            $szTransportMode="Courier";
                            
                            $iCourierFlag = 1;
                            
                            $idBooking=$incompleteTaskAry[$t]['idBooking'];
                        }
                        
                        if($szCustomerName!='')
                        {
                            $showManagmentEmail=false;
                        }
                        if($szCustomerName=='' || $showManagmentEmail)
                        {
                            $incompleteTaskAry[$t]['szForwarderContact']=implode(",",$incompleteTaskAry[$t]['szManagmentEmail']);
                            $szCustomerName = returnLimitData($incompleteTaskAry[$t]['szManagmentEmail'],25) ;
                        }
                        
                        ?>				 
                        <tr  id="booking_data_<?=$ctr?>" onclick="display_pending_task_details_forwarder('booking_data_<?=$ctr?>','<?php echo $incompleteTaskAry[$t]['id']?>','<?php echo $iDisplayPopup; ?>','<?php echo $past_quote_flag; ?>','<?php echo $incompleteTaskAry[$t]['iCourierBooking']?>','<?php echo $incompleteTaskAry[$t]['idBooking'];?>')">
                            <td><?php echo $szTaskUpdatedOn; ?></td>
                            <td><?php echo $szTaskStatus; ?></td>
                            <td><a href="javascript:void(0);" style="font-style:normal;text-decoration:none;color:#000000;" title="<?php echo $incompleteTaskAry[$t]['szForwarderContact']; ?>"><?php echo $szCustomerName ; ?></a></td>
                            <td><?php echo $szTransportMode; ?></td> 
                            <td title="<?php echo $incompleteTaskAry[$t]['szOriginCity'];?>"><?php echo returnLimitData($incompleteTaskAry[$t]['szOriginCity'],18); ?></td>
                            <td title="<?php echo $incompleteTaskAry[$t]['szDestinationCity'];?>"><?php echo returnLimitData($incompleteTaskAry[$t]['szDestinationCity'],18); ?></td>
                            <td><?php echo $szVolWeight; ?></td>
                        </tr>
                        <?php
                        $ctr++;
                    }
                    if($irowCount<10)
                    {
                        $loop_counter = 10-$irowCount ;
                        for($i=0;$i<=$loop_counter;$i++)
                        {
                            ?>
                            <tr> 
                                <td>&nbsp;</td>
                                <td>&nbsp;</td> 
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td> 
                                <td>&nbsp;</td> 
                            </tr>
                            <?php
                        } 
                    }
                    else
                    {
                        ?>
                        <tr> 
                            <td>&nbsp;</td>
                            <td>&nbsp;</td> 
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td> 
                            <td>&nbsp;</td> 
                        </tr>
                        <?php
                    }
		}
		else
		{
                    ?>
                    <tr>
                        <td colspan="7" align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
                    </tr>
                    <tr>
                        <td colspan="7" align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7" align="center">&nbsp;</td>
                    </tr>
	<?php }	?>
        </table> 
        <?php
            if($past_quote_flag)
            {
                $funcName='showPagePastQuotes';
            }
            else
            {
                $funcName='showPagePendingQuotes';
            }
            if($gtotal>0)
            {
                $kPagination->paginate($funcName);
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }
            }
        ?>
    </form> 
    <?php
    if($idBookingQuote>0)
    {
        if($_REQUEST['flag']!='courierBooking')
        {
            
            $idQuote = $kBooking->getQuoteIdBYiQuotationId($idBookingQuote);
            if($idQuote>0)
            {
                $quotesDetailsAry = $kBooking->getAllBookingQuotesByForwarder($idForwarderSession,$idQuote,false);
               
                if(!empty($quotesDetailsAry))
                {
                    $iSelectedQuoteId = $idQuote;
                    $iCourierFlag = 0;
                }
            }
        }
        if($iSelectedQuoteId>0)
        {
            ?>
            <script type="text/javascript">
                display_pending_task_details_forwarder('<?=$selected_tr_id?>','<?php echo $iSelectedQuoteId; ?>','','','<?php echo $iCourierFlag; ?>');
            </script>
            <?php
        }
        else
        { 
            $kBooking_new  = new cBooking();
            $quotesDetailsAry = array();
            if($_REQUEST['flag']=='courierBooking')
            {
                $quotesDetailsAry = $kBooking->getAllBookingCourierLabelByForwarder($idForwarderSession,$idBookingQuote);
                 if(empty($quotesDetailsAry))
                 {
                     $quotesDetailsAry = $kBooking_new->getAllBookingQuotesByForwarder($idForwarderSession,false,false,false,false,array(),false,false,$idBookingQuote);
                 }
            }
            else
            {    
                $quotesDetailsAry = $kBooking_new->getAllBookingQuotesByForwarder($idForwarderSession,false,false,$idBookingQuote); 
            }
            //print_r($quotesDetailsAry);
            $iSelectedQuoteId = $quotesDetailsAry[1]['id'];
            if($iSelectedQuoteId>0)
            {
                ?>
                <script type="text/javascript">
                    $(document).ready(function(){
                    display_pending_task_details_forwarder('<?=$selected_tr_id?>','<?php echo $iSelectedQuoteId; ?>','','','<?php echo $iCourierFlag; ?>');
                });
                </script>
                <?php
            }
        }
    } 
}

function display_pending_quotes_overview_pane($bookingQuotesAry,$kBooking,$iSuccessCount = false,$disabled_flag=false)
{   
    $t_base="management/pendingTray/"; 
     
    $kConfig = new cConfig();
    $kBooking_new = new cBooking();
    
    $taskStatusAry = array();    
    $postSearchAry = array();
    $fileStatusAry = array();
    $cargoDetailsAry = array();
     
    $taskStatusAry = $kConfig->getAllActiveTaskStatus();
    $fileStatusAry = $kConfig->getAllActiveFileStatus();
                     
    $postSearchAry = $bookingQuotesAry[1];
    $idBooking = $postSearchAry['idBooking'];
    $idBookingQuote = $postSearchAry['id'];
    $idBookingFile = $postSearchAry['idFile'];
    
    
    $kBooking_new = new cBooking();
    $bookingDataArr = array();
    $bookingDataArr = $kBooking_new->getBookingDetails($idBooking);  
    $iFinancialVersion = $bookingDataArr['iFinancialVersion'];
    
    if($postSearchAry['idTransportMode']==4) //courier 
    {
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg / ".number_format((int)$postSearchAry['iNumColli'])."Col";
    }
    else
    {
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg ";
    } 
    $serviceTermsAry = array();
    $serviceTermsAry = $kConfig->getAllServiceTerms($postSearchAry['idServiceTerms']);
    $szServiceTerms = $serviceTermsAry[0]['szDisplayNameForwarder'];
    $szHandoverCity = $postSearchAry['szHandoverCity'];
    if($szHandoverCity!='')
    {
        $szHandoverCity =", ".$szHandoverCity;
    }
    
    $fTotalVolume = $postSearchAry['fCargoVolume'];
    $fTotalWeight = $postSearchAry['fCargoWeight']/1000 ;  // convrting weight from kg to meteric ton (mt)

    $cargoDetailsAry = $kBooking_new->getCargoComodityDeailsByBookingId($idBooking); 
    
    $szCargoDescription = '';
    if(!empty($cargoDetailsAry))
    {
        $szCargoDescription = utf8_decode($cargoDetailsAry['1']['szCommodity']); 
    }

    if($postSearchAry['idTransportMode']==4) //courier 
    {
    	if($szCargoDescription!='')
    	{
            $szCargoDescription= $szCargoDescription."/";
    	}
        $szCargoDescription = $szCargoDescription." ".number_format((int)$postSearchAry['iNumColli'])."Col";
    }
    
    $searchAry = array();
    $searchAry['idQuote'] = $idBookingQuote;
    
    $quotesAry = array();
    $quotesAry = $kBooking_new->getAllBookingQuotesPricingDetails($searchAry); 
    
    $szDisabledStr = '';
    if($disabled_flag)
    {
        $szDisabledStr = 'disabled="disabled" ';
    }
    
    if($idBookingFile>0)
    {
        $kBookingFile = new cBooking();
        $kBookingFile->loadFile($idBookingFile);
        $szCommentToForwarder = $kBookingFile->szCommentToForwarder; 
    }
    if($postSearchAry['iPrivateShipping']==1)
    {
        $szCustomerCompanyName = 'Private';
    }
    else
    {
        $szCustomerCompanyName = $postSearchAry['szCustomerCompanyName'];
    }
    
    $fApplicableVatRate = 0; 
    if($_SESSION['forwarder_user_id']>0)
    {
        $kForwarderContact	= new 	cForwarderContact();
        $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
        $idForwarder=$kForwarderContact->idForwarder;
        
        $kForwarder = new cForwarder();
        $kForwarder->load($idForwarder); 
        
        $kVatApplication = new cVatApplication();
        if(!$kVatApplication->checkForwarderCountryExists($kForwarder->idCountry,true))
        {
            $vatInputAry = array();
            $vatInputAry['idForwarderCountry'] = $kForwarder->idCountry;
            $vatInputAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
            $vatInputAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
            
            $fApplicableVatRate = $kVatApplication->getVatRate($vatInputAry);  
            
        }
    }
    $cargoDetailArr=$kBooking_new->getCargoDeailsByBookingId($idBooking);
    
    $total=count($cargoDetailArr);
    $cargo_volume = format_volume($postSearchAry['fCargoVolume']); 
    $cargo_weight = number_format((float)$postSearchAry['fCargoWeight'],0,'.',','); 
    $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);                                    
    $total=count($cargoDetailArr);
    if(!empty($cargoDetailArr))
    {
        $szCargoFullDetails="";
        $ctr=0;
        $totalQuantity=0;
        $kCourierServices= new cCourierServices();
        foreach($cargoDetailArr as $cargoDetailArrs)
        { 
            $packingTypeArr=$kCourierServices->selectProviderPackingList($postSearchAry['idCourierPackingType'],1);
            $totalQuantity=$totalQuantity+$cargoDetailArrs['iColli'];
            
            $t=$total-1;
            if((float)$cargoDetailArrs['fLength']>0.00)
            {
                if((int)$cargoDetailArrs['iQuantity']>1)
                    $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szPacking']);
                else
                    $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szSingle']);
                    
                $lenWidthHeightStr='';
                if($cargoDetailArrs['fLength']>0 || $cargoDetailArrs['fWidth']>0 || $cargoDetailArrs['fHeight']>0)
                {
                    $lenWidthHeightStr=round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."".$cargoDetailArrs['cmdes']." (LxWxH), ";
                }
                
                if($ctr==0)
                {
                    $szCargoFullDetails .=$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                }
                else
                {
                    $szCargoFullDetails .="<br />".$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                }
                ++$ctr;	
            }
        }

        if($totalQuantity>1)
        {
            $textPacking=strtolower($packingTypeArr[0]['szPacking']);
        }
        else
        {
             $textPacking=strtolower($packingTypeArr[0]['szSingle']);   
        }
        
        $szCargoFullDetails .= "<br />Total: ".number_format($totalQuantity)." ".$textPacking.", ".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity; 
    }
    else
    {
        $totalQuantity = $postSearchAry['iNumColli'];
        $textPacking = 'colli';
        if((int)$totalQuantity>0){
        $szCargoFullDetails .= "Total: ".number_format($totalQuantity)." ".$textPacking.", ".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity; 
        }
        else {
            $szCargoFullDetails .= "Total: ".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity; 
        }
    }
    
    $estimateSearchAry=array();
    $estimateSearchAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
    $estimateSearchAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'];
    $estimateSearchAry['iForwarderFlag'] = '1';
    $estimateSearchAry['idForwarder'] = $_SESSION['forwarder_id'];
    //echo $fApplicableVatRate."fApplicableVatRate";
    
    $quoteEstimateAry = array();
    $quoteEstimateAry = $kBooking->getAllQuoteForEstimatePaneForForwarder($estimateSearchAry);
?> 
<h3><?=t($t_base.'title/customer_request')?></h3>
<div> 
    <div id="pending_task_overview_container">
        <table cellpadding="5" cellspacing="0" class="format-6" width="100%" >
            <tr>
                <td style="width:38%;border-right:none;" valign="top"> 
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/company')?>:</span>
                        <span class="field-value"><?php echo $szCustomerCompanyName; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/from')?>:</span>
                        <span class="field-value"><?php echo $postSearchAry['szOriginCountry']; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/to')?>:</span>
                        <span class="field-value"><?php echo $postSearchAry['szDestinationCountry']; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/mode'); ?>:</span>
                        <span class="field-value"><?php echo (!empty($postSearchAry['szTransportMode'])?$postSearchAry['szTransportMode']: __TRANSPORTECA_DEFAULT_TRANSPORT_MODE__) ; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/terms'); ?>:</span>
                        <span class="field-value"><?php echo $szServiceTerms."".$szHandoverCity ; ?></span>
                    </div> 
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/cargo_ready'); ?>:</span>
                        <span class="field-value"><?php echo date('j. F Y',strtotime($postSearchAry['dtTimingDate'])) ; ?></span>
                    </div> 
                </td>
                <td style="width:40%;" valign="top">
                   <div class="overview-detail">
                        <span class="field-name-cargo"><?php echo t($t_base.'fields/cargo_details'); ?>:<br/>
                        <?php echo $szCargoFullDetails ; ?></span>
                    </div> 
                                     
                     
                </td> 
               </tr>
               <?php if($szCommentToForwarder!=''){?>
               <tr>
               		<td colspan="2" style="padding-bottom:0;"><?php echo t($t_base.'fields/comments_from_transporteca')?>:</td>
               </tr>
               <tr>
               		<td colspan="2" style="padding-top:0;"><?php echo $szCommentToForwarder; ?></td>
               </tr>
               <?php }?>
              </table><br/>
              
              <h3>
            	<?php
                $strtext='';
            	$showSubmittedTextFlag=false;
                if($iSuccessCount==1)
                {
                    $showSubmittedTextFlag=true;
                    $strtext = t($t_base.'title/see_your_quote');
                }
                else if($iSuccessCount>1)
                {
                      $showSubmittedTextFlag=true;
                      $strtext= t($t_base.'title/see_your_quotes');
                }
                else if($disabled_flag)
                {
                     $showSubmittedTextFlag=true;
                     
                    if(count($quotesAry)==1)
                    {
                        $strtext = t($t_base.'title/see_your_quote');
                    }
                    else if(count($quotesAry)>1)
                    {
                        $strtext= t($t_base.'title/see_your_quotes');
                    }
                }
                if($showSubmittedTextFlag)
                {
                    
                    $submittedByFlag=false;
                    if($postSearchAry['iUpdatedByAdmin']==0 && $postSearchAry['idStatusUpdatedBy']>0)
                    {
                        
                        $submittedByFlag=true;
                        $kForwarderContact = new cForwarderContact();
                        $kForwarderContact->load($postSearchAry['idStatusUpdatedBy']);
                        $szSubmittedBy=t($t_base.'title/by')." ".$kForwarderContact->szFirstName." ".$kForwarderContact->szLastName;
                        
                    }
                    else if($postSearchAry['iUpdatedByAdmin']==1 && $postSearchAry['idStatusUpdatedBy']>0)
                    {
                        
                        $submittedByFlag=true;
                        $kAdmin = new cAdmin();
                        $kAdmin->getAdminDetails($postSearchAry['idStatusUpdatedBy']);
                        $szSubmittedBy=t($t_base.'title/by')." ".$kAdmin->szFirstName." ".$kAdmin->szLastName;
                    }
                    if($quotesAry[1]['dtCreatedOn']!='' && $quotesAry[1]['dtCreatedOn']!='0000-00-00 00:00:00')
                    {
                        $dtStatusUpdatedOn = date('j. F Y H:i',  strtotime($quotesAry[1]['dtCreatedOn']))." ".t($t_base.'title/gmt');
                    }
                    
                    if($submittedByFlag){
                    $strtext .=" ".t($t_base.'title/submitted_on')." ".$dtStatusUpdatedOn." ".$szSubmittedBy;
                    }
                    echo $strtext;
                }
                else
                {
                ?>  
                
                <?=t($t_base.'title/submit_your_quote')?>
                <?php }?>
    		</h3>
            
                   
                    <form id="forwarder_quote_price_form" name="forwarder_quote_price_form" method="post" action="">
                        <div id="forwarder_quote_main_container">
                        <?php 
                            $counter = 1; 
                            if(count($_POST['addBookingQuotePriceAry']['fTotalPriceForwarderCurrency'])>0 || !empty($quotesAry))
                            { 
                                if(count($_POST['addBookingQuotePriceAry']['fTotalPriceForwarderCurrency'])>0)
                                {
                                   $loop_counter = count($_POST['addBookingQuotePriceAry']['fTotalPriceForwarderCurrency']);  
                                }
                                else
                                {
                                    $loop_counter = count($quotesAry);  
                                } 
                                for($j=0;$j<$loop_counter;$j++)
                                {
                                    $counter = $j+1;
                                    ?>
                                     
                                        <?php
                                            echo display_forwarder_quote_form($kBooking,$counter,$quotesAry[$counter],$disabled_flag,$iFinancialVersion);
                                        ?>
                                    
                                    <?php
                                }
                            }
                            else
                            {  
                                ?>
                                
                                    <?php
                                       echo display_forwarder_quote_form($kBooking,$counter,array(),$disabled_flag,$iFinancialVersion);
                                    ?>
                               
                                <?php
                            }
                        ?>
                        <!--  <div id="add_booking_quote_container_<?php echo $counter; ?>" style="display:none;"></div>-->
                    </div>
                    
                        <input type="hidden" name="addBookingQuotePriceAry[fForwarderVatHidden]" id="fForwarderVatHidden" value="<?php echo $fApplicableVatRate;?>" />
                        <input type="hidden" name="addBookingQuotePriceAry[hiddenPosition]" id="hiddenPosition" value="<?php echo $counter?>" />
                        <input type="hidden" name="addBookingQuotePriceAry[hiddenPosition1]" id="hiddenPosition1" value="<?php echo $counter?>" />
                        <input type="hidden" name="addBookingQuotePriceAry[idBookingQuote]" id="idBookingQuote" value="<?php echo $idBookingQuote?>" />
                        <input type="hidden" name="addBookingQuotePriceAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>" />
                        <input type="hidden" name="addBookingQuotePriceAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>" /> 
                    </form>
                  
                   
                
        <?php
        if((int)$iSuccessCount==0 && !$disabled_flag)
        {?>
       
        	<a href="javascript:void(0);" id="add_more_booking_quote_pricing" title="Add another quote" <?php if($disabled_flag){?>style="float:right;opacity:0.4;margin-top:-16px;"<?php }else{ ?> style="float:right;margin-top:-16px;" onclick="add_more_booking_quote('ADD_MORE_BOOKING_QUOTES');" <?php } ?>><span style="min-width: 50px;"><?=t($t_base.'fields/add_one_more_quote');?></span></a>
        <br class="clear-all" /> 
         <div class="btn-container" align="center">
         	 <a href="javascript:void(0);" id="send_forwarder_quote_save_button" class="button2" title="Save your quote to Transporteca" style="opacity:0.4;"><span><?=t($t_base.'fields/save_data');?></span></a>
             <a href="javascript:void(0);" id="send_forwarder_quote_submit_button" class="button1" title="Submit your quote to Transporteca" style="opacity:0.4;"><span><?=t($t_base.'fields/submit');?></span></a>
             
         </div>
         <?php }?>
        <?php
        if(!empty($quoteEstimateAry) && $iSuccessCount==0 && !$disabled_flag)
        {?><br />
      <h3  class="earlier-quotes-heading">
          <span><?=t($t_base.'title/earlier_quotes_in_same_trade')?>
            <a id="pending_task_list_open_close_link" class="open-icon" href="javascript:void(0);" onclick="close_open_div_forwarder_pending_tray('forwarder_submit_quote_listing','pending_task_list_open_close_link','close');"></a></span>
      </h3>
      <div id="forwarder_submit_quote_listing" style="display:none;" class="clearfix">
          <?php forwarder_old_quote_listing($quoteEstimateAry,$estimateSearchAry);?>
      </div>
      <input type="hidden" name="szOldFieldQuote" id="szOldFieldQuote" value="">
      <input type="hidden" name="szSortTypeQuote" id="szSortTypeQuote" value="">
        <?php  }?>
    </div>
</div> 
<?php if(!$disabled_flag){?>
<script type="text/javascript">
    enableSendForwarderQuoteButton();
</script>
<?php } ?>
<?php 
    $formId = 'forwarder_quote_price_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
}
function display_forwarder_quote_form($kBooking,$counter,$quotesAry=array(),$disabled_flag=false,$iFinancialVersion=false)
{ 
    $t_base="management/pendingTray/";  
    $kConfig=new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true); 
     
    if(!empty($_POST['addBookingQuotePriceAry']))
    {
        $askForQuoteAry = $_POST['addBookingQuotePriceAry'];
        
        $fTotalPriceForwarderCurrency = $askForQuoteAry['fTotalPriceForwarderCurrency'][$counter];
        $idForwarderCurrency = $askForQuoteAry['idForwarderCurrency'][$counter];
        $fTotalVat = $askForQuoteAry['fTotalVat'][$counter];
        $idBookingQuote = $askForQuoteAry['idBookingQuote'][$counter];
        $iTransitHours = $askForQuoteAry['iTransitHours'][$counter]; 
        $dtQuoteValidTo = $askForQuoteAry['dtQuoteValidTo'][$counter];
        $szForwarderComment = $askForQuoteAry['szForwarderComment'][$counter]; 
        $idBookingQuoteDetails = $askForQuoteAry['idBookingQuoteDetails'][$counter];  
    }
    else
    {
        $fTotalPriceForwarderCurrency = $quotesAry['fTotalPriceForwarderCurrency'];
        $idForwarderCurrency = $quotesAry['idForwarderCurrency'];
        $fTotalVat = $quotesAry['fTotalVat'];
        $idBookingQuote = $quotesAry['id'];
        $iTransitHours = $quotesAry['iTransitHours'];    
        $szForwarderComment = $quotesAry['szForwarderComment'];  
        $idBookingQuoteDetails = $quotesAry['id'];  
        if(!empty($quotesAry['dtQuoteValidTo']) && ($quotesAry['dtQuoteValidTo']!='0000-00-00 00:00:00'))
        {
            $dtQuoteValidTo = date('d/m/Y',strtotime($quotesAry['dtQuoteValidTo']));
        }
        else
        {
            $dtQuoteValidTo = date('d/m/Y',strtotime("+1 MONTH"));
        }
    } 
    
    $szDisabledStr = '';
    if($disabled_flag)
    {
        $szDisabledStr = 'disabled="disabled" ';
    }
    
    $counterNew=$counter-1;
    $mon=date('m')-1;
    $date_picker_argument = "minDate: new Date(".date('Y').", ".$mon.", ".(date('d')).")" ;
    //echo $date_picker_argument; 
?>  

    <script type="text/javascript">
        $().ready(function(){
           $("#dtQuoteValidTo_"+'<?php echo $counter; ?>').datepicker({<?php echo $date_picker_argument;?>}); 
        });
    </script>
    <div id="add_booking_quote_container_<?php echo $counterNew; ?>">
     <table class="format-6" width="100%" cellspacing="0" cellpadding="5">
    <tr>
    <td style="width:9%;padding:10px;border-right:none;text-align:center;" valign="middle">
   	<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Quote.png&temp=2&w=200&h=100'?>" border="0" />
    </td><td style="width:65%;padding:10px;" valign="top">
   	<div class="clearfix submit-quote-form">
   		 <div class="currency courier-currency"><?php echo t($t_base.'fields/currency'); ?><br/>
   			<select <?php echo $szDisabledStr; ?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onchange="enableSendForwarderQuoteButton();" name="addBookingQuotePriceAry[idForwarderCurrency][<?php echo $counter; ?>]" id="idForwarderCurrency_<?php echo $counter; ?>">
                <?php
                    if(!empty($currencyAry))
                    {
                        foreach($currencyAry as $currencyArys)
                        {
                            ?>
                            <option value="<?=$currencyArys['id']?>" <?php echo (($currencyArys['id']==$idForwarderCurrency)?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                            <?php
                        }
                    }
                 ?>
            </select></div>
            <div class="all-price"><?php echo t($t_base.'fields/all_in_price'); ?><br/>
            <input type="text" class="send-forwarder-quote-class" onkeyup="enableSendForwarderQuoteButton();" <?php echo $szDisabledStr; ?> name="addBookingQuotePriceAry[fTotalPriceForwarderCurrency][<?php echo $counter; ?>]" onkeypress="return format_decimat(this,event);" onkeyup="return format_decimat(this,event);" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableSendForwarderQuoteButton();" id="fTotalPriceForwarderCurrency_<?php echo $counter; ?>" value="<?php echo $fTotalPriceForwarderCurrency; ?>">
            </div>
            <?php
                /*
                 * We have removed VAT section in Financial version 2 but for version 1 bookings we are still displaying this 
                 */
                if($iFinancialVersion==1)
                {
                    ?>
                    <div class="vat"><?php echo t($t_base.'title/vat'); ?><br/>
                        <input type="text" class="send-forwarder-quote-class" onkeyup="enableSendForwarderQuoteButton();" <?php echo $szDisabledStr; ?> name="addBookingQuotePriceAry[fTotalVat][<?php echo $counter; ?>]" onkeypress="return format_decimat(this,event);" onkeyup="return format_decimat(this,event);" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableSendForwarderQuoteButton();" id="fTotalVat_<?php echo $counter; ?>" value="<?php echo $fTotalVat; ?>">
                    </div>
                    <?php
                }
                else
                {
                    //For version 2, we are not displaying VAT fields
                }
            ?> 
            <div class="transit-time"><?php echo t($t_base.'title/transit_time'); ?><br/>
            	<input type="text" class="send-forwarder-quote-class" onkeyup="enableSendForwarderQuoteButton();" <?php echo $szDisabledStr; ?> name="addBookingQuotePriceAry[iTransitHours][<?php echo $counter; ?>]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableSendForwarderQuoteButton();" id="iTransitHours_<?php echo $counter; ?>" value="<?php echo $iTransitHours; ?>">
            </div>
    		<div class="transit-time-days"> days </div>
             <div class="valid"><?php echo t($t_base.'title/valid_to'); ?><br/>
            	<input type="text" class="send-forwarder-quote-class" <?php echo $szDisabledStr; ?> readonly="" name="addBookingQuotePriceAry[dtQuoteValidTo][<?php echo $counter; ?>]" onfocus="check_form_field_not_required(this.form.id,this.id);" id="dtQuoteValidTo_<?php echo $counter; ?>" onblur="enableSendForwarderQuoteButton();" value="<?php echo $dtQuoteValidTo; ?>">
            </div>
          </div>
    		<div class="submit-quote-form"><?php echo t($t_base.'title/comment_for_customer')?>
            	<textarea onkeyup="enableSendForwarderQuoteButton();" <?php echo $szDisabledStr; ?> style=" resize: none;" name="addBookingQuotePriceAry[szForwarderComment][<?php echo $counter; ?>]" onfocus="check_form_field_not_required(this.form.id,this.id);" id="szForwarderComment_<?php echo $counter; ?>" onblur="enableSendForwarderQuoteButton();"><?php echo $szForwarderComment;  ?></textarea>
      		</div>
    		
    </td>
    </tr>
    </table>
    <?php if(!$disabled_flag){ ?>
        <a style="cursor:pointer;" title="Delete quote" onclick="remove_booking_quote('add_booking_quote_container_<?php echo ($counter-1) ?>');">
           <?php echo t($t_base.'fields/remove_this_quote'); ?>
		
        </a>
    <?php } ?>
    <input type="hidden" name="addBookingQuotePriceAry[idBookingQuoteDetails][<?php echo $counter; ?>]" id="idBookingQuoteDetails_<?php echo $counter ?>" value="<?php echo $idBookingQuoteDetails; ?>" />
    </div>
   <?php
} 

function display_closed_quote_notification_popup($bookingQuoteAry)
{ 
    $t_base="management/pendingTray/";    
    $idStatusUpdatedBy = $bookingQuoteAry['idStatusUpdatedBy'];
    $dtStatusUpdatedOn = $bookingQuoteAry['dtStatusUpdatedOn'];
    $adminFlag=false;
    if($bookingQuoteAry['iUpdatedByAdmin']==1)
    {
        $adminFlag=true;
    }
    $kForwarderContact = new cForwarderContact();
    $kForwarderContact->getUserDetails($idStatusUpdatedBy,$adminFlag);
    
    $szForwarderName = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName; 
    $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ;
    
    $date = new DateTime($dtStatusUpdatedOn); 
    $date->setTimezone(new DateTimeZone($szCustomerTimeZone)); 
    $dtSubmittedOn = $date->format('d F Y');
    $dtSubmittedTime = $date->format('H:i');
    
    $dtSubmittedOn_1 = date('d F Y',strtotime($dtStatusUpdatedOn));
    $dtSubmittedTime_1 = date('H:i',strtotime($dtStatusUpdatedOn));
    
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container">			
        <div class="popup" style="margin-top:50px;"> 
            <?php if((int)$bookingQuoteAry['iQuoteNotSubmittedByForwarderFlag']==1){
            ?>
            <h3><?=t($t_base.'title/quote_request_expired');?></h3>
            <p><?=t($t_base.'messages/quote_request_expired_msg');?></p>
            <br/>
            <div class="btn-container" style="text-align: center;"> 
                <a href="javascript:void(0);" onclick="showHide('contactPopup')" class="button1"><span><?=t($t_base.'fields/close')?></span></a> 
            </div>
            <?php
            }else{?>
            <br><br>
        <p><?=t($t_base.'title/closed_quote_1')." ".$szForwarderName." on ".$dtSubmittedOn." ".t($t_base.'title/at')." ".$dtSubmittedTime.". ".t($t_base.'title/closed_quote_2')."." ?></p>
        <!--<p><?php echo "Actual time: ".$dtSubmittedOn_1." ".$dtSubmittedTime_1?></p>-->
        <br>
        <div class="btn-container" style="text-align: center;"> 
            <a href="javascript:void(0);" onclick="showHide('contactPopup')" class="button1"><span><?=t($t_base.'fields/ok')?></span></a> 
        </div>
            <?php }?>
        </div>
    </div>	
<?php 

  } 
  
  function display_forwarder_setting_emails($forwarderContactAry)
  {
    $kForwarderContact = new cForwarderContact(); 
    $szBookingEmailAry = (!empty($forwarderContactAry['szBookingEmail'])) ? $forwarderContactAry['szBookingEmail']:'';
    $szPaymentEmailAry = (!empty($forwarderContactAry['szPaymentEmail'])) ? $forwarderContactAry['szPaymentEmail']:'';
    $szCustomerServiceEmailAry = (!empty($forwarderContactAry['szCustomerServiceEmail'])) ? $forwarderContactAry['szCustomerServiceEmail']: '';

    $szCatchUpBookingQuoteEmailAry = (!empty($forwarderContactAry['szCatchUpBookingQuoteEmail'])) ? $forwarderContactAry['szCatchUpBookingQuoteEmail']:'';

    $szAirBookingEmailAry = (!empty($forwarderContactAry['szAirBookingEmail'])) ? $forwarderContactAry['szAirBookingEmail']:''; 
    $szRoadBookingEmailAry = (!empty($forwarderContactAry['szRoadBookingEmail'])) ? $forwarderContactAry['szRoadBookingEmail']:'';
    $szCourierBookingEmailAry = (!empty($forwarderContactAry['szCourierBookingEmail'])) ? $forwarderContactAry['szCourierBookingEmail']:'';
    
    $idForwarder = $_SESSION['forwarder_id'];
    $forwarderControlPanelArr = $kForwarderContact->getSystemPanelDetails($idForwarder);
  
    $t_base = "ForwardersCompany/Preferences/";  
    
    $szLeftSideStyle = 'style="width:35%;"' ;
    $szRightSideStyle = 'style="width:35%;float:right;"' ;
    ?> 
    <h4 style="padding-top:40px;"><strong><?=t($t_base.'titles/email_address');?></strong> <a href="javascript:void(0);" <?php if(!empty($forwarderControlPanelArr)){ ?> onclick="edit_preferences_email('<?=$idForwarder?>')" <?php } ?>><?=t($t_base.'titles/edit');?></a></h4>				
    <div class="ui-fields">
        <span class="field-name" <?php echo $szLeftSideStyle; ?>><?=t($t_base.'titles/catch_all_quote');?></span>
        <div class="field-container" <?php echo $szRightSideStyle; ?>>
        <?php
            if(!empty($szCatchUpBookingQuoteEmailAry))
            {
                foreach($szCatchUpBookingQuoteEmailAry as $szCatchUpBookingQuoteEmailArys)
                {
        ?>
            <span class="email-trim"><?=$szCatchUpBookingQuoteEmailArys['szEmail']?></span> 
        <?php
                }
            } 
        ?> 
        </div>
    </div>  
    <div class="ui-fields">
        <span class="field-name" <?php echo $szLeftSideStyle; ?>><?=t($t_base.'titles/Payments_billing');?></span>
        <div class="field-container" <?php echo $szRightSideStyle; ?>>
    <?php
        if(!empty($szPaymentEmailAry))
        {
            foreach($szPaymentEmailAry as $szPaymentEmailArys)
            {
    ?>
                <span class="email-trim"><?=$szPaymentEmailArys['szEmail']?></span>
    <?php
            }
        } 
    ?> 
        </div>
    </div> 
    <div class="ui-fields">
        <span class="field-name" <?php echo $szLeftSideStyle; ?>><?=t($t_base.'titles/Customer_service');?></span>
        <div class="field-container" <?php echo $szRightSideStyle; ?>>
        <?php
            if(!empty($szCustomerServiceEmailAry))
            {
                foreach($szCustomerServiceEmailAry as $szCustomerServiceEmailArys)
                {
        ?>
                    <span class="email-trim"><?=$szCustomerServiceEmailArys['szEmail']?></span>
        <?php
                }
            } 
        ?> 
        </div>
    </div> 
    <?php 
  } 
  
  function forwarder_system_control_emails()
  {
    $kForwarderContact = new cForwarderContact(); 
    $idForwarder = $_SESSION['forwarder_id'];
    $forwarderControlPanelArr = $kForwarderContact->getSystemPanelDetails($idForwarder);
    //constants for hardcoding data for preferences
    $arrBook=BookingReportsArr();
    //$bookingReports=array('Daily','Weekly','Monthy','Never');
    //$paymentReports=array('Weekly','Monthly');
    //$pricngUpdates=array('No','Yes');

    $bookingReports	= $arrBook[0];
    $paymentReports	= $arrBook[1];
    $pricngUpdates	= $arrBook[2]; 
    
    $szLeftSideStyle = 'style="width:35%;"' ;
    $szRightSideStyle = 'style="width:38%;float:right;"' ;
    $t_base = "ForwardersCompany/Preferences/";  
    
    $kForwarderNew = new cForwarder();
    $paymentFrequency = $kForwarderNew->getAllPaymentFrequency();
  
      ?> 
        <h4><strong><?=t($t_base.'titles/system_config');?></strong> <a href="javascript:void(0);" onclick="edit_preferences_system('<?=$idForwarder?>')"><?=t($t_base.'titles/edit');?></a></h4>	
        
        <div class="ui-fields">
            <span class="field-name" <?php echo $szLeftSideStyle; ?>><?=t($t_base.'fields/payment_currency');?></span>
            <div style="width:35%;float:right;">
            	<span><?=$forwarderControlPanelArr['szCurrency']?></span>
            </div>
        </div> 
    <?php
    
  }
function display_pending_quotes_overview_pane_courier($bookingQuotesAry,$kBooking,$iSuccessCount = false,$disabled_flag=false,$kCourierServices=false)
{   
    $t_base="management/pendingTray/"; 
    // print_r($kCourierServices);
    $kConfig = new cConfig();
    $kBooking_new = new cBooking();
    
    if(!empty($kCourierServices->arErrorMessages))
    { 
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        {
            $szValidationErrorKey = $errorKey ;
            break;
        }
        if($kCourierServices->arErrorMessages['iFileCount']!='')
        {
            $kCourierServices->arErrorMessages['iTotalPage']=$kCourierServices->arErrorMessages['iFileCount'];
        }
        display_form_validation_error_message("forwarder_courier_label_form",$kCourierServices->arErrorMessages);
    }
    
    $taskStatusAry = array();    
    $postSearchAry = array();
    $fileStatusAry = array();
    $cargoDetailsAry = array();
     
    $taskStatusAry = $kConfig->getAllActiveTaskStatus();
    $fileStatusAry = $kConfig->getAllActiveFileStatus();
                     
    //print_r($bookingQuotesAry);
    $postSearchAry = $bookingQuotesAry[1];
    $idBooking = $postSearchAry['idBooking'];
    $idBookingQuote = $postSearchAry['id'];
    $idBookingFile = $postSearchAry['idFile'];
    
    if($postSearchAry['idTransportMode']==4) //courier 
    {
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg / ".number_format((int)$postSearchAry['iNumColli'])."Col";
    }
    else
    {
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg ";
    }
    $serviceTermsAry = array();
    $serviceTermsAry = $kConfig->getAllServiceTerms($postSearchAry['idServiceTerms']);
    $szServiceTerms = $serviceTermsAry[0]['szDisplayNameForwarder'];
    
      // convrting weight from kg to meteric ton (mt)

    $cargoDetailsAry = $kBooking_new->getCargoComodityDeailsByBookingId($idBooking); 
    
    $szCargoDescription = '';
    if(!empty($cargoDetailsAry))
    {
        $szCargoDescription = utf8_decode($cargoDetailsAry['1']['szCommodity']); 
    }

    if($postSearchAry['idTransportMode']==4) //courier 
    {
    	if($szCargoDescription!='')
    	{
    		$szCargoDescription= $szCargoDescription."/";
    	}
        $szCargoDescription = $szCargoDescription." ".number_format((int)$postSearchAry['iNumColli'])."Col";
    }
    
       
    $szDisabledStr = '';
    if($disabled_flag)
    {
        $szDisabledStr = 'disabled="disabled" ';
    }
    
    if($idBookingFile>0)
    {
        $kBookingFile = new cBooking();
        $kBookingFile->loadFile($idBookingFile);
        $szCommentToForwarder = $kBookingFile->szCommentToForwarder; 
    }
   // print_r($postSearchAry);
    
    $bookingDataArr=$kBooking->getExtendedBookingDetails($postSearchAry['idBooking']);
    $fTotalVolume = format_volume($bookingDataArr['fCargoVolume']);
   $fTotalWeight = round((float)$bookingDataArr['fCargoWeight']);
   // echo $fTotalWeight; 
    
    //print_r($bookingDataArr);
    $kCourierServices= new cCourierServices();
    $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($postSearchAry['idBooking']);
    
    $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($postSearchAry['idBooking']);
    
    $courierBookingArr=$kCourierServices->getCourierBookingData($postSearchAry['idBooking']);
    if(empty($_POST['addBookingCourierLabelAry']))
    {
        $_POST['addBookingCourierLabelAry']['szMasterTrackingNumber']=$bookingDataArr['szTrackingNumber'];
        $_POST['addBookingCourierLabelAry']['idCourierProduct']=$bookingDataArr['idServiceProviderProduct']."-".$bookingDataArr['idServiceProvider'];
        $_POST['addBookingCourierLabelAry']['dtCollectionEndTime']=$courierBookingArr[0]['dtCollectionEndTime'];
        $_POST['addBookingCourierLabelAry']['dtCollectionStartTime']=$courierBookingArr[0]['dtCollectionStartTime'];
        $_POST['addBookingCourierLabelAry']['iCollection']=$courierBookingArr[0]['szCollection'];
        $_POST['addBookingCourierLabelAry']['filecount']=$courierBookingArr[0]['iTotalPage'];
        if($courierBookingArr[0]['dtCollection']!='0000-00-00'){ $_POST['addBookingCourierLabelAry']['dtCollection']=date('d/m/Y',strtotime($courierBookingArr[0]['dtCollection']));}
        
        $fileArr=$kCourierServices->getCourierLabelUploadedPdf($postSearchAry['idBooking']);
        $_POST['addBookingCourierLabelAry']['file_name']=$fileArr;
    }                           
    $total=count($cargoDetailArr);
    $cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
    $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',','); 
    $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);                                    
    $total=count($cargoDetailArr);
    if(!empty($cargoDetailArr))
    {
        $szCargoFullDetails="";
        $ctr=0;
        $totalQuantity=0;
        foreach($cargoDetailArr as $cargoDetailArrs)
        { 
            $packingTypeArr=$kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1);
            $totalQuantity=$totalQuantity+$cargoDetailArrs['iColli'];
            $t=$total-1;
               if((int)$cargoDetailArrs['iQuantity']>1)
                    $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szPacking']);
                else
                    $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szSingle']);

                    $lenWidthHeightStr='';
                    if($cargoDetailArrs['fLength']>0 || $cargoDetailArrs['fWidth']>0 || $cargoDetailArrs['fHeight']>0)
                    {
                        $lenWidthHeightStr=round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."".$cargoDetailArrs['cmdes']." (LxWxH), ";
                    }
                    if($ctr==0)
                    {
                        $szCargoFullDetails .=$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                    }
                    else
                    {
                        $szCargoFullDetails .="<br />".$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                    }
            ++$ctr;	
        }

        if($totalQuantity>1)
        {
            $textPacking=strtolower($packingTypeArr[0]['szPacking']);
        }
        else
        {
             $textPacking=strtolower($packingTypeArr[0]['szSingle']);   
        }
        
        $szCargoFullDetails .= "<br />Total: ".number_format($totalQuantity)." ".$textPacking.", ".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity; 
    }
    else
    {
        $totalQuantity = $bookingDataArr['iNumColli'];
        $textPacking = 'colli';
        
        $szCargoFullDetails .= "Total: ".number_format($totalQuantity)." ".$textPacking.", ".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity; 
    }
    
    
    if(!empty($bookingDataArr['dtCutOff']) && $bookingDataArr['dtCutOff']!='0000-00-00 00:00:00')
    {
        $pickUpDate=date('d. F Y',strtotime($bookingDataArr['dtCutOff'])); 
    }
    else
    {
        $pickUpDate = "N/A";
    } 
        $shipperAddress = '';
        if(!empty($bookingDataArr['szShipperCompanyName']))
        {
            $shipperAddress.= html_entities_flag($bookingDataArr['szShipperCompanyName']);
        }  
        if(!empty($bookingDataArr['szShipperAddress_pickup']))
        {
            $shipperAddress.= "<br/> ".html_entities_flag($bookingDataArr['szShipperAddress_pickup']);
        }
        if(!empty($bookingDataArr['szShipperAddress2']))
        {
            $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress2']) ;
        }
        if(!empty($bookingDataArr['szShipperAddress3']))
        {
            $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress3']);
        }
        if(!empty($bookingDataArr['szShipperPostCode']) || !empty($bookingDataArr['szShipperCity']))
        {
            if(!empty($bookingDataArr['szShipperPostCode']) && !empty($bookingDataArr['szShipperCity']))
            {
                $shipperAddress.="<br/> ".html_entities_flag($bookingDataArr['szShipperPostCode'])." ".html_entities_flag($bookingDataArr['szShipperCity'])."";
            }
            else if(!empty($bookingDataArr['szShipperPostCode']))
            {
                $shipperAddress.="<br/> ".html_entities_flag($bookingDataArr['szShipperPostCode']);
            }
            else if(!empty($bookingDataArr['szShipperCity']))
            {
                $shipperAddress.="<br/> ".html_entities_flag($bookingDataArr['szShipperCity']);
            }
        }
        if(!empty($bookingDataArr['szShipperCountry']))
        {
              $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperCountry']);
        }
            //echo $shipperAddress;
            $ConsigneeAddress = '';
            if(!empty($bookingDataArr['szConsigneeCompanyName']))
            {
                $ConsigneeAddress.= html_entities_flag($bookingDataArr['szConsigneeCompanyName'])." ";
            }  
            if(!empty($bookingDataArr['szConsigneeAddress']))
            {
                $ConsigneeAddress.= "<br/>".html_entities_flag($bookingDataArr['szConsigneeAddress']);
            }
            if(!empty($bookingDataArr['szConsigneeAddress2']))
            {
                $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2']);
            }
            if(!empty($bookingDataArr['szConsigneeAddress3']))
            {
                $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3']);
            }
            if(!empty($bookingDataArr['szConsigneePostCode']) || !empty($bookingDataArr['szConsigneeCity']))
            {
                $ConsigneeAddress.="<br/> ".html_entities_flag($bookingDataArr['szConsigneePostCode'])." ".html_entities_flag($bookingDataArr['szConsigneeCity'])."";
            }
            if(!empty($bookingDataArr['szConsigneeCountry']))
            {
                  $ConsigneeAddress.=", ".html_entities_flag($bookingDataArr['szConsigneeCountry']);
            }
         //   echo $ConsigneeAddress."<br/>";
      
    //print_r($bookingDataArr);
    $kConfig = new cConfig();
    $kConfig->loadCountry($bookingDataArr['idShipperCountry']);
    
    $idShipperDialCode=$kConfig->iInternationDialCode;
    
    $kConfig->loadCountry($bookingDataArr['idConsigneeCountry']);
    
    $idConsigneeDialCode=$kConfig->iInternationDialCode;
    
    
    ?>
            
<h3><?=t($t_base.'title/courier_booking_details')?></h3>
<div id="courierDetail"> 
    <div id="pending_task_overview_container">
        <table cellpadding="5" cellspacing="0" class="format-6" width="100%" >
            <tr>
                <td style="width:38%;border-right:none;" valign="top">
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/booking_reference')?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szBookingRef']; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/courier_provider')?>:</span>
                        <span class="field-value"><?php echo (!empty($newCourierBookingAry[0]['szProviderName'])?$newCourierBookingAry[0]['szProviderName']:'N/A'); ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/product'); ?>:</span>
                        <span class="field-value"><?php echo (!empty($newCourierBookingAry[0]['szProviderProductName'])?$newCourierBookingAry[0]['szProviderProductName']:'N/A'); ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/sales_price'); ?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szCustomerCurrency']." ".number_format((float)$bookingDataArr['fTotalPriceCustomerCurrency'],2) ; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/requested_collection'); ?>:</span>
                        <span class="field-value"><?php echo $pickUpDate; ?></span>
                    </div>
                     
                   </td><td style="width:40%;" valign="top">
                       <div class="overview-detail">
                        <span class="field-name-cargo"><?php echo t($t_base.'fields/cargo_details'); ?>:<br/>
                        <?php echo $szCargoFullDetails ; ?></span>
                    </div>
                     
                </td> 
               </tr>
               <tr><td colspan="2" style="height:1px;background:#bfbfbf;padding:0;"></td></tr>
               <tr>
                   <td style="width:38%;border-right:none;" valign="top">
                        <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/shipper'); ?>:</span>
                             <span class="field-value"><?php echo $shipperAddress ; ?></span>
                        </div> 
                         <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/name'); ?>:</span>
                             <span class="field-value"><?php echo $bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']; ?></span>
                         </div> 
                         <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/email'); ?>:</span>
                             <span class="field-value"><?php echo $bookingDataArr['szShipperEmail'] ; ?></span>
                         </div>
                        <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/phone_number'); ?>:</span>
                             <span class="field-value"><?php echo "+".$idShipperDialCode." ".$bookingDataArr['szShipperPhone'] ; ?></span>
                         </div>
                   </td>
                   <td style="width:40%;" valign="top">
                        <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/consignee'); ?>:</span>
                        <span class="field-value"><?php echo $ConsigneeAddress ; ?></span>
                    </div> 
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/name'); ?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szConsigneeLastName'] ; ?></span>
                    </div> 
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/email'); ?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szConsigneeEmail'] ; ?></span>
                    </div> 
                     
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/phone_number'); ?>:</span>
                        <span class="field-value">+<?php echo $idConsigneeDialCode ; ?> <?php echo $bookingDataArr['szConsigneePhone'] ; ?></span>
                    </div> 
                   </td>    
               </tr>
               <?php if($szCommentToForwarder!=''){?>
               <tr>
               		<td colspan="2" style="padding-bottom:0;"><?php echo t($t_base.'fields/comments_from_transporteca')?>:</td>
               </tr>
               <tr>
               		<td colspan="2" style="padding-top:0;"><?php echo $szCommentToForwarder; ?></td>
               </tr>
               <?php }?>
              </table><br/>
              <h3>
            	<?php
            	
				
                     echo t($t_base.'title/upload_shipping_labels_and_tracking_number');
                ?>
	    		
    		</h3>
                        
                   
                    <form enctype="multipart/form-data" id="forwarder_courier_label_form" name="forwarder_courier_label_form" method="post" action="">
                        <div id="forwarder_quote_main_container">
                        <?php 
                                $kBooking->idBooking=$idBooking;
                                echo display_forwarder_quote_form_courier($kBooking,$counter,array(),$disabled_flag,$totalQuantity);
                        ?>
                        <!--  <div id="add_booking_quote_container_<?php echo $counter; ?>" style="display:none;"></div>-->
                    </div>
                    
                   
                        <input type="hidden" name="addBookingCourierLabelAry[hiddenPosition]" id="hiddenPosition" value="<?php echo $counter?>" />
                        <input type="hidden" name="addBookingCourierLabelAry[hiddenPosition1]" id="hiddenPosition1" value="<?php echo $counter?>" />
                        <input type="hidden" name="addBookingCourierLabelAry[idBookingQuote]" id="idBookingQuote" value="<?php echo $idBookingQuote?>" />
                        <input type="hidden" name="addBookingCourierLabelAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>" />
                        <input type="hidden" name="addBookingCourierLabelAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>" /> 
                    </form>
                  
                   
                
        <?php
        if((int)$iSuccessCount==0 && !$disabled_flag)
        {?>
        <br class="clear-all" /> 
         <div class="btn-container" align="center">
         	 <a href="javascript:void(0);" id="send_forwarder_quote_save_button" class="button2" title="Save your quote to Transporteca" style="opacity:0.4;"><span><?=t($t_base.'fields/save_data');?></span></a>
             <a href="javascript:void(0);" id="send_forwarder_quote_submit_button" class="button1" title="Submit your quote to Transporteca" style="opacity:0.4;"><span><?=t($t_base.'fields/submit');?></span></a>
             
         </div>
         <?php }?>
    </div>
</div> 
<?php 
    $formId = 'forwarder_courier_label_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
}
function display_forwarder_quote_form_courier($kBooking,$counter,$quotesAry=array(),$disabled_flag=false,$totalQuantity)
{ 
    $t_base="management/pendingTray/";  
    $kConfig=new cConfig();
    
    //print_r($kBooking);
    $szDisabledStr = '';
    if($disabled_flag)
    {
        $szDisabledStr = 'disabled="disabled" ';
    }
    
    $szDisabled_str = 'disabled="disabled" ';
    $counterNew=$counter-1;
    $mon=date('m')-1;
    $date_picker_argument = "minDate: new Date(".date('Y').", ".$mon.", ".(date('d')).")" ;
    //echo $date_picker_argument;
    $kCourierServices = new cCourierServices();
    $cutoffLocalTimeAry = fColumnData();
    $idBooking=$kBooking->idBooking;
    $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__ && $bookingDataArr['isManualCourierBooking']=='1')
    {
        $fromCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry($bookingDataArr['idOriginCountry'],__TRADE_EXPORT__,$bookingDataArr['idForwarder']);
        $fromCount=count($fromCountryProviderArr);

        $ToCountryProviderArr=$kCourierServices->selectCourierAgreementByCountry($bookingDataArr['idDestinationCountry'],__TRADE_IMPORT__,$bookingDataArr['idForwarder']);
        $toCount=count($ToCountryProviderArr);

        $providerForwarderArr=array_merge($fromCountryProviderArr,$ToCountryProviderArr); 
        $courierProductArr=array();
        if(!empty($providerForwarderArr))
        {
            $ctr=0;
            $courierProviderComanyAry = array();
            $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails();
            $szCalculationLogString_0 = "<br><br><h3>Step 1: Test weight and measure fit with courier product groups</h3>";    
            foreach($providerForwarderArr as $providerForwarderArrs)
            { 
                $resArr = $kCourierServices->getCourierServices($providerForwarderArrs,false,false);  
                $szProviderName = $courierProviderComanyAry[$providerForwarderArrs['idCourierProvider']]['szName'];
                
                /*
                if($providerForwarderArrs['idCourierProvider']==__FEDEX_API__)
                {
                    $szProviderName = "Fedex";
                }
                else if($providerForwarderArrs['idCourierProvider']==__UPS_API__)
                {
                    $szProviderName = "UPS";
                }
                else
                {
                    $szProviderName = "TNT";
                }
                * 
                */
                if(count($resArr)>0)
                {
                    foreach($resArr as $key=>$values)
                    { 
                        if(!empty($values))
                        {
                            foreach($values as $value)
                            {

                                $szName=$szProviderName." - ".$value['szDisplayName'];
                                $courierProductArr[$ctr]['idCourierProviderProductid']=$value['idCourierProviderProductid'];
                                $courierProductArr[$ctr]['idCourierAgreement']=$value['idCourierAgreement'];
                                $courierProductArr[$ctr]['szName']=$szName;
                                ++$ctr;
                            }
                        }
                    }
                } 
            }
        }
        $courierProductArr=sortArray($courierProductArr,'szName');
        $div_width = "18%";
        $div_width_small = "10%";
    }
    else
    {
        $div_width = "22%";
        $div_width_small = "12%";
    }
    
    //echo $szDisabledStr;
   if($szDisabledStr=='')
   {
       
     //  echo $totalQuantity."ddd";
?>    
<script type="text/javascript">
	$(document).ready(function() {
  	var settings = {
	url: "<?php echo __FORWARDER_HOME_PAGE_URL__?>/courierLabel/uploadLabel.php",
	method: "POST",
	allowedTypes:"pdf",
	fileName: "myfile",
	multiple: true,
        dragDropStr:'<span><b>Click to upload your PDF labels or drop them here!</b></span>',
	onSuccess:function(files,data,xhr)
 	{ 
//            console.log("Files");
//            console.log(files);
//            console.log("data");
//            console.log(data);
//            console.log("xhr");
//            console.log(xhr);
//            
            var IS_JSON = true;
            try
            {
                var json = $.parseJSON(data);
            }
            catch(err)
            {
                IS_JSON = false;
            } 
            
            if(IS_JSON)
            { 
                var obj = JSON.parse(data); 
                var iFileCounter = 1;

                var filecount=$("#filecount").attr('value');
                var filecountOld=$("#filecount").attr('value');
                
                $("#deleteArea").attr('style','display:block'); 
                $("#upload_process_list").attr('style','display:none');
                $("#upload_process_list").html(' ');
                $(".file_list_con").attr('style','display:block');

                for (var i = 0, len = obj.length; i < len; i++)
                { 
                    var newfilecount = parseInt(filecount) + iFileCounter;  
                    var uploadedFileName = obj[i]['name']; 
                    var FileUrl="<?php echo __BASE_STORE_URL__?>/courierLabel/"+uploadedFileName; 

                    var container_li_id = "id_"+newfilecount;

                    $('#fileList #namelist').append( '<li id="'+container_li_id+'" onclick="openPdfLightBox(\''+FileUrl+'\')" >&nbsp;</li>');  
                    $('#'+container_li_id).html( '<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Page.png&temp=2&w=50&h=50'?>" border="0" /><span class="page">'+newfilecount+'</span>');
  
                    iFileCounter++; 
                    
                    var filename_all=$("#file_name").val();  
                    if(filename_all!='')
                    { 
                        var newvalue=filename_all+";"+files+"#####"+uploadedFileName;
                        $("#fileuploadlink").attr('style','display:block');
                        $("#file_uploade_plus_link").attr('style','display:block');
                    }
                    else
                    { 
                        var newvalue=files+"#####"+uploadedFileName;
                        $("#fileuploadlink").attr('style','display:block');	
                        $("#file_uploade_plus_link").attr('style','display:block');
                    } 

                    var textpage='page';
                    if(parseInt(newfilecount)>1)
                    {
                        textpage='pages';
                    }
                    textpage=newfilecount+" "+textpage; 
                    $("#iTotalPage").attr('value',textpage); 
                    $("#file_name").attr('value',newvalue);
                } 
                $("#filecount").attr('value',newfilecount); 

                if(parseInt(newfilecount)=='<?php echo $totalQuantity; ?>')
                {
                    $("#fileuploadlink").attr('style','display:none');
                    $("#file_uploade_plus_link").attr('style','display:none');
                } 
                $('div.ajax-upload-dragdrop:gt(0)').hide (); 
                
            }
            else
            {  
                 $("#upload_process_list").attr('style','display:block');
                    $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='emptyDiv(\"upload_process_list\")'>close</a></p>");
                    /*
                if($("#upload_process_list").length)
                {
                    $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='emptyDiv(\"upload_process_list\")'>close</a></p>");
                }
                else
                {
                    $("#upload_process_list").attr('style','display:block');
                    $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='emptyDiv(\"upload_process_list\")'>close</a></p>");
                }*/
                
            }
            enableSendForwarderCourierLabelButton();
            
	},onSelect:function(s){
            $("#upload_process_list").attr('style','display:block');
            $("#upload_error_container").attr('style','display:none');
        },
        afterUploadAll:function()
        {
            //alert("all images uploaded!!");
        },
        onError: function(files,status,errMsg)
        {		
            //$("#status").html("<font color='red'>Upload is Failed</font>");
        }
    }
    <?php if($szDisabledStr==''){?>
    $("#mulitplefileuploader").uploadFile(settings);
            <?php }?>
    $("#drop").uploadFile(settings);
    <?php    if($_POST['addBookingCourierLabelAry']['filecount']>0){?> $('div.ajax-upload-dragdrop:gt(0)').hide();<?php }?>
  
});
enableSendForwarderCourierLabelButton();
</script> 

<?php
   }
$counterNew=$counter-1;
    $mon=date('m')-1;
    $date_picker_argument = "minDate: new Date(".date('Y').", ".$mon.", ".(date('d')).")" ;
    //echo $date_picker_argument;
    $textpages="page";
    if($_POST['addBookingCourierLabelAry']['filecount']>1)
    {
        $textpages="pages";
    }
    $textpages=$_POST['addBookingCourierLabelAry']['filecount']." ".$textpages;
if($szDisabledStr=='')
   {
?> 
    <script type="text/javascript">
        jQuery(document).ready(function($){
         $("#namelist").sortable({
         connectWith: '#deleteArea'
         });
         $("#deleteArea").droppable({
         accept: '#namelist > li',
         hoverClass: 'dropAreaHover',
         drop: function(event, ui) {
         deleteImage(ui.draggable,ui.helper);
         },
         activeClass: 'dropAreaHover'
         });
         function deleteImage($draggable,$helper){
         params = $draggable.attr('id');
         var paramsArr=params.split("_");
         var idBooking=$("#idBooking").val();
         var idBookingFile=$("#idBookingFile").val();
         var file_name=$("#file_name").val();
         var value=$("#forwarder_courier_label_form").serialize();
	 var newValue=value+'&mode=DELETE_COURIER_LABEL_PDF&file_name='+file_name+'&counter='+paramsArr[1];
         $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){
                $("#pending_task_overview_main_container").html(result);
                 //$("#fileuploadlink").attr('style','display:block');
                  //$("#file_uploade_plus_link").attr('style','display:block');
          });
         $helper.effect('transfer', { to: '#deleteArea', className: 'ui-effects-transfer' },500);
         $draggable.remove();
         }
        });
    </script>
   <?php }?>  

    <script type="text/javascript">
        $().ready(function(){
           $("#dtCollection").datepicker({<?php echo $date_picker_argument;?>}); 
        });
    </script>
    <div id="add_booking_quote_container_<?php echo $counterNew; ?>">
     <table class="format-6" width="100%" cellspacing="0" cellpadding="5">
    <tr>
    <td style="width:9%;padding:15px 0px 0px;border-right:none;text-align:center;max-width: 80%;" valign="top">
   	<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=label.png&temp=2&w=100&h=100'?>" border="0" />
    </td><td style="width:91%;padding:10px;" valign="top" id="labelformDiv">
   	<div class="clearfix submit-quote-form">
            <div class="all-price" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/master_tracking_number'); ?><br/>
            <input type="text" style="text-transform: uppercase;" class="send-forwarder-quote-class" <?php echo $szDisabledStr;?> name="addBookingCourierLabelAry[szMasterTrackingNumber]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szMasterTrackingNumber" value="<?php echo $_POST['addBookingCourierLabelAry']['szMasterTrackingNumber']; ?>" onkeyup="enableSendForwarderCourierLabelButton();">
            </div>
             <div class="vat" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/upload_labels_pdf'); ?><br/>
            <?php
            if($szDisabledStr=='')
            {
                //echo $_POST['addBookingCourierLabelAry']['filecount']."----".$totalQuantity;
                ?>      
            <div  id="drop" class="upload_labe_plus">
                
            </div>
            <?php }?> <input <?php echo $szDisabled_str; ?> type="text" class="send-forwarder-quote-class" readonly name="addBookingCourierLabelAry[iTotalPage]"  id="iTotalPage" value="<?php echo $textpages; ?>">
            </div>
            <?php 
            if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__ && $bookingDataArr['isManualCourierBooking']=='1')
            {
               // print_r($_POST['addBookingCourierLabelAry']['idCourierProduct']);
                ?>
            <div class="transit-time" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/courier_product'); ?><br/>
            	<select id="idCourierProduct" name="addBookingCourierLabelAry[idCourierProduct]" <?php echo $szDisabledStr;?> onchange="enableSendForwarderCourierLabelButton();">
                    <option value="">Select</option>
                    <?php
                        if(!empty($courierProductArr))
                        {
                            foreach($courierProductArr as $courierProductArrs)
                            {
                                   $idCourierProduct=$courierProductArrs['idCourierProviderProductid']."-".$courierProductArrs['idCourierAgreement']; 
                                ?>
                                 <option value="<?php echo $idCourierProduct;?>" <?php if($_POST['addBookingCourierLabelAry']['idCourierProduct']==$idCourierProduct){?> selected <?php }?>><?php echo $courierProductArrs['szName'];?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
                <input type="hidden" name="addBookingCourierLabelAry[iManualCourierBooking]" id="iManualCourierBooking" value="1">
            </div>
            <?php }?>
            <div class="transit-time" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/collection'); ?><br/>
            	<select id="iCollection" name="addBookingCourierLabelAry[iCollection]" <?php echo $szDisabledStr;?> onchange="enableDisableDateTimeField(this.value);enableSendForwarderCourierLabelButton();">
                    <option value="">Select</option>
                    <option value="Scheduled" <?php if($_POST['addBookingCourierLabelAry']['iCollection']=='Scheduled'){?> selected <?php }?>>Scheduled</option>
                    <option value="Not-scheduled" <?php if($_POST['addBookingCourierLabelAry']['iCollection']=='Not-scheduled'){?> selected <?php }?>>Not scheduled</option>
                </select>
            </div>
             <div class="valid" style="width:<?php echo $div_width_small;?>"><?php echo t($t_base.'fields/date'); ?><br/>
            	<input type="text" class="send-forwarder-quote-class" <?php if($_POST['addBookingCourierLabelAry']['iCollection']!='Scheduled' && $szDisabledStr==''){ echo $szDisabled_str; }?> <?php echo $szDisabledStr;?>  readonly="" name="addBookingCourierLabelAry[dtCollection]" onfocus="check_form_field_not_required(this.form.id,this.id);" id="dtCollection" value="<?php echo $_POST['addBookingCourierLabelAry']['dtCollection']; ?>">
            </div>
            <div class="currency" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/time_between'); ?><br/>
                <div class="clearfix">
            	<select <?php if($_POST['addBookingCourierLabelAry']['iCollection']!='Scheduled' && $szDisabledStr==''){ echo $szDisabled_str; }?>  id="dtCollectionStartTime" <?php echo $szDisabledStr;?> onchange="changeEndTimeValue(this.value,'<?php echo implode(";",$cutoffLocalTimeAry)?>');enableSendForwarderCourierLabelButton();" name="addBookingCourierLabelAry[dtCollectionStartTime]" onchange="changeEndTimeValue(this.value,'<?php echo implode(";",$cutoffLocalTimeAry);?>')">
                    <option value=""></option>
                    <?php 
                    $t=0;
                    if(!empty($cutoffLocalTimeAry)){
                        foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
                        {?>    
                            <option value="<?php echo $t;?>"  <?php if($_POST['addBookingCourierLabelAry']['dtCollectionStartTime']!='' && $_POST['addBookingCourierLabelAry']['dtCollectionStartTime']==$t){?> selected <?php }?>><?php echo $cutoffLocalTimeArys;?></option>
                    <?php 
                    
                        ++$t;
                        }}?>                    
                </select><span>-</span><select <?php echo $szDisabledStr;?> <?php if($_POST['addBookingCourierLabelAry']['iCollection']!='Scheduled' && $szDisabledStr==''){ echo $szDisabled_str; }?>  onchange="enableSendForwarderCourierLabelButton();" id="dtCollectionEndTime" name="addBookingCourierLabelAry[dtCollectionEndTime]">
                    <option value=""></option>
                    <?php 
                    if(!empty($cutoffLocalTimeAry)){
                        $k=$_POST['addBookingCourierLabelAry']['dtCollectionStartTime'];
                        foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
                        {   
                            if($t1>=$k)
                            {
                            ?>    
                            <option value="<?php echo $t1;?>" <?php if($_POST['addBookingCourierLabelAry']['dtCollectionEndTime']!='' && $_POST['addBookingCourierLabelAry']['dtCollectionEndTime']==$t1){?> selected <?php }?>><?php echo $cutoffLocalTimeArys;?></option>
                    <?php 
                            }
                        ++$t1;
                        }}?>                    
                </select>
                </div>
            </div>
            
          </div>
                        <?php
                         if($szDisabledStr=='')
                         {?>
                       <!-- <div class="upload_label" id="fileuploadlink">
                            <div class="clearfix">

                                        <span id="nofile-selected"><?=t($t_base.'fields/click_to_upload_your_pdf');?></span>
                            <input id="file_upload" width="120" type="file" height="30" name="file_upload"  />
			
                            </div>
			</div>-->
                         <?php }?>
                       <?php
                         if($szDisabledStr=='')
                         {?>
                        <div id="upload_error_container" style="display:none;"></div>
                        <div id="mulitplefileuploader" class="upload_label" <?php if($_POST['addBookingCourierLabelAry']['filecount']>0){?>style="display:none;" <?php }?>>
				Upload

                            
			</div>
                        <?php }?>
                       <div id="upload_process_list" style="display:none;"></div>
                       <div class="file_list_con" <?php if($_POST['addBookingCourierLabelAry']['filecount']==0){?>style="display:none;" <?php }?>>
                        <div id="fileList">
			 <ul id="namelist" class="ui-sortable clearfix"> 
                             
			<?php 
				if(!empty($_POST['addBookingCourierLabelAry']['file_name']))
				{
					$allUploadedFileArr=explode(";",$_POST['addBookingCourierLabelAry']['file_name']);
					if(!empty($allUploadedFileArr))
					{
                                                $t=1;    
						for($i=0;$i<count($allUploadedFileArr);$i++)
						{
							$uploadFileArr=explode("#####",$allUploadedFileArr[$i]);
                                                        $fileUrl=__BASE_STORE_URL__."/courierLabel/".$uploadFileArr[1];    
							echo "<li id='id_".$t."_".$uploadFileArr[2]."' onclick='openPdfLightBox(\"".$fileUrl."\");'><img src='".__BASE_STORE_INC_URL__."/image.php?img=Page.png&temp=2&w=50&h=50' ><span class='page'>".$t."</span></span>";							
                                                        ++$t;
						}
					}
				}else{
			?>	
				
			
			<?php
				}
			?>
                         </ul>
                           
			</div>  
                         <?php
                            if($szDisabledStr=='')
                            {
                                //echo $_POST['addBookingCourierLabelAry']['filecount']."----".$totalQuantity;
                               ?> 
                           <div id="deleteArea" <?php if($_POST['addBookingCourierLabelAry']['filecount']==0){?>style="display:none;"<?php }?>>
                                <img src="<?php echo __BASE_STORE_INC_URL__?>/image.php?img=Trash.png&temp=2&w=50&h=50" title="Drag here to delete." alert="Drag here to delete."/>
                            </div>
                           <?php
                            }?>
                       </div>
                        <input type="hidden" name="addBookingCourierLabelAry[filecount]" id="filecount" value="<?=(($_POST['addBookingCourierLabelAry']['filecount'])?$_POST['addBookingCourierLabelAry']['filecount']:"0")?>">			
			<input type="hidden" name="addBookingCourierLabelAry[file_name]" id="file_name" value="<?=$_POST['addBookingCourierLabelAry']['file_name']?>">
			
    </table>
    </div>
   <?php
}
function sendShipperCourierLabel($idBooking,$kCourierServices,$bForwarderFlag=false)
{
    $t_base="management/pendingTray/";
    $kBooking= new cBooking();
    $bookingDataArr=$kBooking->getExtendedBookingDetails($idBooking);
    $iSendTrackingUpdates = $bookingDataArr['iSendTrackingUpdates'];
    //print_r($bookingDataArr);
    //$kCourierServices= new cCourierServices();
    $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($idBooking);
    
    $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking);
    
    $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);
    
    if((int)$bookingDataArr['iTotalQuantity']>1)
    {
        $textQuanity=$bookingDataArr['iTotalQuantity']." ".t($t_base.'messages/pieces');
    }
    else
    {
        $textQuanity=$bookingDataArr['iTotalQuantity']." ".t($t_base.'messages/pieces');
    }
    $textColli=$bookingDataArr['iTotalQuantity']." ".t($t_base.'messages/colli');
    if((int)$kCourierServices->iFileCount>1)
    {
        $textpage=$kCourierServices->iFileCount." ".t($t_base.'messages/pages');
    }
    else
    {
        $textpage=$kCourierServices->iFileCount." ".t($t_base.'messages/page');
    }
    
    if($kCourierServices->dtCollection!='0000-00-00' && $kCourierServices->dtCollection!='')
    {
         $dtCollectionArr=explode("/",$kCourierServices->dtCollection);
         $kCourierServices->dtCollection=$dtCollectionArr[2]."-".$dtCollectionArr[1]."-".$dtCollectionArr[0];  
        $dtCollection=date('l j M',strtotime($kCourierServices->dtCollection));
    }
    $cutoffLocalTimeAry = fColumnData();
    
    $kCourierServices = new cCourierServices(); 
    $courierProviderComanyAry = array();
    $courierProviderComanyAry = $kCourierServices->getCourierProviderDetails();
    
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__ && $bookingDataArr['isManualCourierBooking']=='1')
    {
        $idCourierProductArr=explode("-",$_POST['addBookingCourierLabelAry']['idCourierProduct']);
        $courierProviderArr=$kCourierServices->getCourierActiveProviderName($idCourierProductArr[0]);
        
        
        $idServiceProvider=$idCourierProductArr[1];
        
        $idCourierProviderCompany = $courierProviderArr[0]['id'];
        $iType = $idCourierProviderCompany;
        $szServiceProvider = $courierProviderComanyAry[$idCourierProviderCompany]['szName'];
        
        /*
        if($courierProviderArr[0]['id']==__FEDEX_API__)
        {    
            $iType=1;
            $szServiceProvider = "FedEx";
        }
        else if($courierProviderArr[0]['id']==__UPS_API__)
        {
            $iType=2;
            $szServiceProvider = "UPS";
        }
         * 
         */
        $newCourierBookingAry[0]['szProviderName']=$courierProviderArr[0]['szName']; 
        if($iType<=0)
        {
            $szServiceProvider = "N/A";
        } 
    }
    else
    { 
        $idCourierProviderCompany = $courierProviderArr[0]['id'];
        $iType = $idCourierProviderCompany;
        $szServiceProvider = $courierProviderComanyAry[$idCourierProviderCompany]['szName']; 
        
        /*
        if($newCourierBookingAry[0]['idProvider']==__FEDEX_API__)
        {    
            $iType=1;
            $szServiceProvider = "FedEx";
        }
        else if($newCourierBookingAry[0]['idProvider']==__UPS_API__)
        {
            $iType=2;
            $szServiceProvider = "UPS";
        } 
        if($iType<=0)
        {
            $szServiceProvider = "N/A";
        } 
        * 
        */
    }
    
    ?> 
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup abandon-popup" id="pdfDiv">
            <p class="close-icon" align="right">
                <a href="javascript:void(0);" onclick="closePopupUpdatedService('contactPopup');">
                <img src="<?php echo __BASE_STORE_IMAGE_URL__?>/close1.png" alt="close">
                </a>
            </p>
            <h5><?=t($t_base.'title/send_labels_for_booking')?> <?php echo $bookingDataArr['szBookingRef'];?></h5>
            <p><?=t($t_base.'messages/msg_1')?></p><br/>
            <!--<p><?=t($t_base.'messages/msg_2')?> <?php echo $newCourierBookingAry[0]['szProviderName'];?> <?=t($t_base.'messages/msg_3')?> <?php echo $kCourierServices->szTrackingNumber;?>: "<span id="loader_img"><img src="<?=__BASE_STORE_IMAGE_URL__?>/loader.gif" alt="" /></span> (<?php echo $newCourierBookingAry[0]['szProviderName'];?>)"</p><br/>-->
            <p><?=t($t_base.'messages/msg_6')?> <?php echo $textpage;?> <?=t($t_base.'messages/msg_7')?> <?php echo $textColli;?>.</p><br/>
            <?php 
            if($kCourierServices->iCollection=='Scheduled')
            {?>    
                <p><?=t($t_base.'messages/msg_8')?> <?php echo $dtCollection;?> <?=t($t_base.'messages/msg_9')?> <?php echo $cutoffLocalTimeAry[$kCourierServices->dtCollectionStartTime];?> <?=t($t_base.'messages/msg_10')?> <?php echo $cutoffLocalTimeAry[$kCourierServices->dtCollectionEndTime];?>.</p><br/>
            <?php    
            }
            else
            {?>
                <p><?=t($t_base.'messages/msg_12')." ".$szServiceProvider." ".t($t_base.'messages/msg_14'); ?></p><br/>
            <?php }?>   
            <p><?=t($t_base.'messages/msg_11')?></p><br/>
            <?php if(!$bForwarderFlag){ ?>
                <p class="private-cb">
                    <input type="checkbox" value="1" id="iSendTrackingUpdates" <?php if($iSendTrackingUpdates!=1){ echo 'checked'; } ?> name="iSendTrackingUpdates">&nbsp; Do not send tracking updates to customer
                </p>
            <?php } ?>
            <br class="clear-all" /> 
            <div class="btn-container" align="center">
                <a href="javascript:void(0);" onclick="closePopupUpdatedService('contactPopup');" id="cancel_button" class="button2" title="Cancel"><span><?=t($t_base.'fields/cancel');?></span></a>
                <a href="javascript:void(0);" style="opacity:0.4" id="confirm_button_popup" class="button1" title="Confirm" ><span><?=t($t_base.'fields/confirm');?></span></a> 
            </div>
        </div>
        <script>
            jQuery(document).ready(function($){     
               // showTrackingNumberStatus('<?php echo $idBooking;?>','<?php echo $_POST['addBookingCourierLabelAry']['szMasterTrackingNumber'];?>','<?php echo $iType;?>','<?php echo $idServiceProvider;?>');
               $("#confirm_button_popup").attr("style","opacity:1");
                $("#confirm_button_popup").unbind("click");
                $("#confirm_button_popup").click(function(){ confirmAddCourierLevel(); });
            });
        </script>
    </div>
 </div> 
    <?php
}

function display_tryitout_search_form($postSearchAry=array(),$kQuote=false,$iQuickQuotePage=false)
{ 
    $t_base = "BulkUpload/";
    $kConfig = new cConfig();
    $serviceTermsAry = array();
    $serviceTermsAry = $kConfig->getAllServiceTerms(false,false,false,true);
    $kForwarderContact= new cForwarderContact();   
    $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
                    
    if(!empty($_POST['tryitOutAry']))
    {
        $postQuickQuoteAry = $_POST['tryitOutAry'];
        $szOriginCountryStr = $postQuickQuoteAry['szOriginCountryStr'];
        $szDestinationCountryStr = $postQuickQuoteAry['szDestinationCountryStr'];
        $idServiceTerms = $postQuickQuoteAry['idServiceTerms'];
        $dtShipmentDate = $postQuickQuoteAry['dtShipmentDate']; 
        $szCustomerType = $postQuickQuoteAry['szCustomerType'];
        $idCustomerCurrency = $postQuickQuoteAry['idCustomerCurrency'];
        $idCustomerCountry = $postQuickQuoteAry['idCustomerCountry'];
                    
        if(!empty($postQuickQuoteAry['szShipmentType']))
        {
            if($postQuickQuoteAry['szShipmentType']=='BREAK_BULK')
            {
                $iBreakBulk = 1;
            }
            else if($postQuickQuoteAry['szShipmentType']=='PARCEL')
            {
                $iParcel = 1;
            }
            else if($postQuickQuoteAry['szShipmentType']=='PALLET')
            {
                $iPallets = 1;
            }
            else
            {
                $iBreakBulk=1;
            }
        }
        else
        {
            $iBreakBulk=1;
        }
    }
    else 
    {    
        $iBreakBulk = 1; 
        $dtShipmentDate = date('d/m/Y');
        //$idServiceTerms = __SERVICE_TERMS_FOB__;
        if($iQuickQuotePage)
        {
            $kForwarder= new cForwarder();
            $kForwarder->load($_SESSION['forwarder_id']);
            $idCustomerCurrency = $kForwarder->szCurrency ;
            
        }
        else
        {
            $kForwarderContact->findForwarderCurrency((int)$_SESSION['forwarder_id']);
            $idCustomerCurrency = $kForwarderContact->iOldCurrency;
        }
    }  
    $iQuickQuotePageGoogleFlag=0;
    if($iQuickQuotePage)
    {
        $iQuickQuotePageGoogleFlag = 1;
    }
    
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);
    
    $countryAry = array();
    $countryAry = $kConfig->getAllCountries();
    ?>
    <script type="text/javascript">
         $().ready(function() {	 
            var autocomplete1,place ; 
            function initialize1() 
            {
                var input1 = document.getElementById('szOriginCountryStr');
                var options = {types: ['regions']};

                var autocomplete1 = new google.maps.places.Autocomplete(input1); 
                $(".pac-container:last").attr("id", 'szOriginCountryStrPacContainer');
                google.maps.event.addListener(autocomplete1, 'place_changed', function() 
                {    
                    var szOriginAddress_js = $("#szOriginCountryStr").val();   
                    checkFromAddressForwarder(szOriginAddress_js,'FROM_COUNTRY','',<?php echo $iQuickQuotePageGoogleFlag;?>);
                }); 
                var input2 = document.getElementById('szDestinationCountryStr');
                var autocomplete2 = new google.maps.places.Autocomplete(input2); 
                $(".pac-container:last").attr("id", 'szDestinationCountryStrPacContainer');
                google.maps.event.addListener(autocomplete2, 'place_changed', function() 
                {
                    var szDestinationAddress_js = $("#szDestinationCountryStr").val();  
                    checkFromAddressForwarder(szDestinationAddress_js,'TO_COUNTRY','',<?php echo $iQuickQuotePageGoogleFlag;?>);
                }); 

                $('#szOriginCountryStr').on('paste focus keypress keyup', function (e){  
                    $('#szOriginCountryStr').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szOriginCountryStrPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks
                        }
                        else
                        {
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display'); 
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);
                                }
                            });
                        }
                        var key_code = e.keyCode || e.which;
                        console.log("ORG Code: "+key_code);
                        if (key_code == 9 || key_code == 13) {
                            prefillAddressOnTabPress('szOriginCountryStr',pac_container_id);
                        }  
                    }); 
                });

                $('#szDestinationCountryStr').on('paste focus', function (e){  
                    $('#szDestinationCountryStr').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szDestinationCountryStrPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks 
                            //console.log("div exist called");
                        }
                        else
                        {
                            var iFoundDiv = 1;
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display');  
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);  
                                    iFoundDiv = 2;
                                } 
                            });

                            if(iFoundDiv==1)
                            {
                                $(".pac-container").each(function(pac)
                                {
                                    var container_id = $(this).attr('id');  
                                    if(container_id=='' || container_id === undefined)
                                    {
                                        $(this).attr('id',pac_container_id); 
                                        return true;
                                    }
                                });
                            }
                        } 
                        //console.log("Dest Code: "+e.keyCode);
                        if (e.keyCode == 9 || e.keyCode == 13) {
                            prefillAddressOnTabPress('szDestinationCountryStr',pac_container_id);
                        }  
                    });
                }); 
            } 
            function prefillAddressOnTabPress(input_field_id,pac_container_id)
            {  
                $(".pac-container").each(function(pac){
                   var disp = $(this).css('display'); 
                   var contaier_id = $(this).attr('id');  

                   console.log("Container: "+contaier_id + " pac id: "+pac_container_id); 
                    if(contaier_id==pac_container_id)
                    {
                        var spanObj = $(this).children(".pac-item:first" );
                        var firstResult = ""; 
                        spanObj.children("span").each(function(){
                            var spanText = $(this).text(); 
                            spanText = jQuery.trim(spanText);
                            //console.log("Span Text: "+spanText);
                            if(spanText!='')
                            {
                                if(firstResult=='')
                                {
                                    firstResult += spanText;
                                }
                                else
                                {
                                    firstResult += ", "+ spanText;
                                }
                                //$(this).children( ".pac-item:first" ).addClass("pac-selected");
                                //$(this).css("display","none"); 
                                if(jQuery.trim(input_field_id)=='szOriginCountryStr')
                                {
                                    //console.log("Mathed: "+firstResult);
                                    $("#szOriginCountryStr").val(firstResult);
                                    $("#szOriginCountryStr").select();
                                }
                                else if(jQuery.trim(input_field_id)=='szDestinationCountryStr')
                                { 
                                    //console.log("Not Mathed");
                                    $("#szDestinationCountryStr").val(firstResult); 
                                    $("#szDestinationCountryStr").select();
                                } 
                            }
                        });  
                    }
                }); 
            } 
            function clean_pac_container()
            { 
                $("div").each(function(index){
                    if($( this ).hasClass("pac-container"))
                    {
                        $( this ).remove();
                    }
                }); 
            } 
            
            enableTryItOutGetRatesButton('',1);
            $("#szOriginCountryStr").focus();
            initialize1();  

            //$("#szOriginCountryStr").focus();

            $("#dtShipmentDate").bind('keydown', function (event) {
                if(event.which == 13){
                    var e = jQuery.Event("keydown");
                    e.which = 9;//tab 
                    e.keyCode = 9;
                    $(this).trigger(e);
                    return false;
                }
            }).datepicker({ <?=$date_picker_argument?> }); 

        });  
    </script>
    <form id="try_it_out_search_form" name="try_it_out_search_form" method="post" action="javascript:void(0);"> 
        <?php 
        $wd_22="wd-35";
        $wds_22="wds-35";
        $wds_10="wds-15";
        $wds_28="wds-35";
        $wds_8="wds-30";
        if($iQuickQuotePage){
        $wd_22="wd-22";    
        $wds_22="wds-22";
        $wds_10="wds-10";
        $wds_28="wds-28";
        $wds_8="wds-8";
        ?>
        <h4><strong><?=t($t_base.'fields/quick_quote');?> </strong></h4>
        <?php }else{?>
            <h5><?=t($t_base.'fields/pagetitle_try_it_out');?> </h5>
        <?php }?>
        <div class="clearfix-fwd email-fields-container" style="width:100%;"> 
            <span class="quote-field-container <?=$wd_22;?>">
                <span class="font-12">From</span><br>
                <input type="text" name="tryitOutAry[szOriginCountryStr]" placeholder="Start typing" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableTryItOutGetRatesButton(event);" onblur="checkFromAddressForwarder(this.value,'FROM_COUNTRY',1,<?php echo $iQuickQuotePageGoogleFlag;?>)" id="szOriginCountryStr" value="<?php echo $szOriginCountryStr; ?>">
            </span> 
            <span class="quote-field-container <?=$wds_22;?>">
                <span class="font-12">To</span><br>
                <input type="text" name="tryitOutAry[szDestinationCountryStr]" placeholder="Start typing" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableTryItOutGetRatesButton(event);" onblur="checkFromAddressForwarder(this.value,'TO_COUNTRY',1,<?php echo $iQuickQuotePageGoogleFlag;?>)" id="szDestinationCountryStr" value="<?php echo $szDestinationCountryStr; ?>">
            </span>
            <span class="quote-field-container <?=$wds_10;?>">
                <span class="font-12">Terms</span><br> 
                <select name="tryitOutAry[idServiceTerms]" id="idServiceTerms" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableTryItOutGetRatesButton(event);<?php if($iQuickQuotePage){?>update_ship_con(this.value); <?php }?>" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                   <option value="">Select</option>
                   <?php
                       if(!empty($serviceTermsAry))
                       {
                          foreach($serviceTermsAry as $serviceTermsArys)
                          {
                           ?>
                           <option value="<?php echo $serviceTermsArys['id']; ?>" <?php echo (($serviceTermsArys['id']==$idServiceTerms)?'selected':''); ?>><?php echo $serviceTermsArys['szDisplayNameForwarder']; ?></option>
                           <?php
                          }
                       }
                   ?> 
                </select>
            </span>
            <span class="quote-field-container <?=$wds_10;?>">
                <span class="font-12">Ready date</span><br>
                <input type="text" name="tryitOutAry[dtShipmentDate]"  id="dtShipmentDate" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableTryItOutGetRatesButton(event);" value="<?php echo $dtShipmentDate; ?>">
            </span>
            <?php if(!$iQuickQuotePage){?>
            </div>
            <div class="clearfix-fwd email-fields-container" style="width:100%;"> 
            <?php }?>
            <span class="quote-field-container <?=$wds_28;?>">
                <span class="font-12">Packing</span><br>
                <span class="checkbox-container tryout-packing"><label id="label_szShipmentType_BREAK_BULK"><input type="radio" name="tryitOutAry[szShipmentType]" <?php if($iBreakBulk==1){ echo 'checked'; }?> onclick="toggleTryItOutCargoFields(this.id,'BREAK_BULK');enableTryItOutGetRatesButton(event);" id="szShipmentType_BREAK_BULK" value="BREAK_BULK"></label>Break bulk &nbsp;</span>
                <span class="checkbox-container tryout-packing"><label id="label_szShipmentType_PARCEL"><input type="radio" name="tryitOutAry[szShipmentType]" <?php if($iParcel==1){ echo 'checked'; }?> onclick="toggleTryItOutCargoFields(this.id,'PARCEL');enableTryItOutGetRatesButton(event);" id="szShipmentType_PARCEL" value="PARCEL"></label>Packages &nbsp;</span>
                <span class="checkbox-container tryout-packing"><label id="label_szShipmentType_PALLET"><input type="radio" name="tryitOutAry[szShipmentType]" <?php if($iPallets==1){ echo 'checked'; }?> onclick="toggleTryItOutCargoFields(this.id,'PALLET');enableTryItOutGetRatesButton(event);" id="szShipmentType_PALLET" value="PALLET"></label>Pallets</span>
            </span>
            <span class="quote-field-container <?=$wds_8;?>">
                <span class="font-12">Customer</span><br>
                <span class="checkbox-container"><input type="checkbox" name="tryitOutAry[szCustomerType]" <?php if($szCustomerType==1){ echo 'checked'; }?>  id="szCustomerType" value="1" <?php if($iQuickQuotePage){?> onclick="prefillCompanyField(this.id);enableQQGetRatesButton(event);" <?php }else{?> onclick="enableTryItOutGetRatesButton(event);" <?php }?>>Private</span>
            </span>
        </div> 
        <!-- Cargo description's container -->
        <div id="tryitout_cargo_container">
            <div id="tryitout_cargo_container_BREAK_BULK" <?php if($iBreakBulk==1){ echo "style='display:block;'"; } else { echo "style='display:none;'"; }?>>
                <?php echo display_tryitout_cargo_fields('BREAK_BULK',$postSearchAry); ?>
            </div>
            <div id="tryitout_cargo_container_PARCEL" <?php if($iParcel==1){ echo "style='display:block;'"; } else { echo "style='display:none;'"; }?>>
                <?php echo display_tryitout_cargo_fields('PARCEL',$postSearchAry); ?>
            </div>
            <div id="tryitout_cargo_container_PALLET" <?php if($iPallets==1){ echo "style='display:block;'"; }else{ echo "style='display:none;'"; }?>>
                <?php echo display_tryitout_cargo_fields('PALLET',$postSearchAry); ?>
            </div>
        </div>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div class="clearfix email-fields-container tryitout-button-container" <?php if($iQuickQuotePage){?>  style="width:100%;box-sizing: border-box; height: 62px; padding-top: 6px;" <?php }else{?> style="width:100%;box-sizing: border-box; height: 62px; padding-top: 16px;" <?php }?>> 
            
            <?php
            if($iQuickQuotePage)
            {?>
            <input type="hidden" name="tryitOutAry[iQuickQuotePage]" id="iQuickQuotePage" value="1">
            <span class="quick-quote-currency">
                <span class="font-12">Customer country</span><br>
                <select style="width:120px;" name="tryitOutAry[idCustomerCountry]" id="idCustomerCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="update_ship_con_by_country_selection(this.value);enableTryItOutGetRatesButton(event);">
                    <option value="">Select</option>
                    <?php
                        if(!empty($countryAry))
                        {
                           foreach($countryAry as $countryArys)
                           {
                            ?>
                            <option value="<?php echo $countryArys['id']; ?>" <?php echo (($countryArys['id']==$idCustomerCountry)?'selected':''); ?>><?php echo $countryArys['szCountryName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select> 
            </span>
            <span class="quick-quote-currency"> 
                <span class="font-12">Currency</span><br>
                <select style="width:120px;" name="tryitOutAry[idCustomerCurrency]" id="idCustomerCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableTryItOutGetRatesButton(event);">
                    <option value="">Select</option>
                    <?php
                        if(!empty($currencyAry))
                        {
                           foreach($currencyAry as $currencyArys)
                           {
                            ?>
                            <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idCustomerCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select> 
                <input type="hidden" name="tryitOutAry[iUpdatedTermsDropDown]" id="iUpdatedTermsDropDown" value="0">
            </span>
             <?php   
            }else{?>
            <input type="hidden" name="tryitOutAry[idCustomerCurrency]" id="idCustomerCurrency" value="<?php echo $idCustomerCurrency;?>">
            <?php }?>
            <span style="width:127px;"> 
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="tryitout_get_price_button"><span>GET RATES</span></a>
            </span>
            <span style="width:127px;"> 
                <a href="javascript:void(0)" class="button2" onclick="clearTryItOutSerachForm('<?php echo $iQuickQuotePage;?>');"><span>Clear</span></a>
            </span> 
        </div>
    </form>
    <?php  
    if(!empty($kQuote->arErrorMessages))
    {  
        $formId = 'try_it_out_search_form'; 
        display_form_validation_error_message($formId,$kQuote->arErrorMessages);
    }
}
function display_tryitout_cargo_fields($szShipmentType,$postSearchAry=array())
{  
    if($szShipmentType=='BREAK_BULK')
    {
        if(!empty($_POST['tryitOutAry'][$szShipmentType]))
        {
            $cargoontainerAry = $_POST['tryitOutAry'][$szShipmentType];
            if($szShipmentType=='BREAK_BULK')
            {
                $fTotalVolume = $cargoontainerAry['fTotalVolume'];
                $fTotalWeight = $cargoontainerAry['fTotalWeight'];
            }
        } 
        else
        {
            $fTotalVolume = $postSearchAry['fCargoVolume'];
            $fTotalWeight = $postSearchAry['fCargoWeight'];
            
            if($fTotalVolume>0)
            {
                if(__float($fTotalVolume))
                {
                    $fTotalVolume = round_up($fTotalVolume,2); 
                }
                else
                {
                    $fTotalVolume = round($fTotalVolume);
                }
            } 
            if($fTotalWeight>0)
            {
                if(__float($fTotalWeight))
                {
                    $fTotalWeight = round_up($fTotalWeight,2); 
                }
                else
                {
                    $fTotalWeight = round($fTotalWeight);
                }
            } 
        }
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div class="email-fields-container clearfix" style="width:100%;">  
            <span class="quote-field-container wd-35">&nbsp;</span>
            <span class="quote-field-container wd-10"> 
                <span class="font-12">Volume</span><br>
                <input type="text" onkeyup="enableTryItOutGetRatesButton(event);" name="tryitOutAry[<?php echo $szShipmentType; ?>][fTotalVolume]" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="fTotalVolume_<?php echo $szShipmentType; ?>" value="<?php echo $fTotalVolume; ?>">
            </span> 
            <span class="quote-field-container wds-5 font-12">&nbsp;<br/>cbm</span>
            <span class="quote-field-container wds-10"> 
                <span class="font-12">Weight</span><br>
                <input type="text" onkeyup="enableTryItOutGetRatesButton(event);" name="tryitOutAry[<?php echo $szShipmentType; ?>][fTotalWeight]" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="fTotalWeight_<?php echo $szShipmentType; ?>" value="<?php echo $fTotalWeight; ?>">
            </span> 
            <span class="quote-field-container wds-5 font-12">&nbsp;<br/>kg</span> 
        </div> 
        <script type="text/javascript">
            validateTryItOutCargoDetails('<?php echo $szShipmentType; ?>','<?php echo $number; ?>');
        </script>
        <?php
    }
    else if($szShipmentType=='PARCEL')
    { 
        $szDivKey = "_PARCEL";
        $szChilDivClass = 'class="request-quote-fields"';
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div id="complete-box" class="active transporteca-box">    
            <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
                <?php 
                    $counter = 1; 
                    $parcelPostAry = array();
                    
                    if(!empty($_POST['tryitOutAry']))
                    {
                        $parcelPostAry = $_POST['tryitOutAry']['PARCEL'];
                    }
                    else 
                    {
                        //if($postSearchAry['iCargoPackingType']==1) //Parcel Services
                        $kBooking = new cBooking();
                        $parcelPostAry = $kBooking->getCargoDeailsByBookingId($postSearchAry['id'],false,true,$szShipmentType);
                    }
                    if(count($parcelPostAry['iLength'])>0)
                    {  
                        $lengthAry = $parcelPostAry['iLength'];
                        $counter_parcel = 0; 
                        $j = 0;
                        foreach($lengthAry as $key=>$lengthArys)
                        {
                            $counter = $j+1;
                            $parcelCargoAry = array();
                            $parcelCargoAry['iLength'] = sanitize_all_html_input($parcelPostAry['iLength'][$key]); 
                            $parcelCargoAry['iWidth'] = sanitize_all_html_input($parcelPostAry['iWidth'][$key]);
                            $parcelCargoAry['iHeight'] = sanitize_all_html_input($parcelPostAry['iHeight'][$key]);
                            $parcelCargoAry['iQuantity'] = sanitize_all_html_input($parcelPostAry['iQuantity'][$key]);
                            $parcelCargoAry['iWeight'] = sanitize_all_html_input($parcelPostAry['iWeight'][$key]);
                            $parcelCargoAry['idCargoMeasure'] = sanitize_all_html_input($parcelPostAry['idCargoMeasure'][$key]);
                            $parcelCargoAry['idWeightMeasure'] = sanitize_all_html_input($parcelPostAry['idWeightMeasure'][$key]);

                            ?>
                            <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" <?php echo $szChilDivClass; ?>> 
                                <?php
                                    echo display_tryitout_quote_cargo_details_form($szShipmentType,$counter,$parcelCargoAry);
                                ?>
                            </div>
                            <?php 
                            $j++;
                        } 
                    }
                    else
                    {   
                        ?>
                        <div id="add_booking_quote_container_0<?php echo $szDivKey;?>" <?php echo $szChilDivClass; ?> > 
                            <?php
                               echo display_tryitout_quote_cargo_details_form($szShipmentType,$counter);
                            ?>
                        </div>
                        <?php 
                    } 
                ?>
                <div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>" style="display:none;"></div>
            </div>    
            <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
            <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" /> 
        </div>
        <?php
    }
    else if($szShipmentType=='PALLET')
    {   
        $szDivKey = "_PALLET";
        $szChilDivClass = 'class="request-quote-fields"';
        ?>
        <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
        <div id="complete-box" class="active transporteca-box">    
            <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
                <?php 
                    $counter = 1; 
                    $palletPostAry = array(); 
                    if(!empty($_POST['tryitOutAry']['PALLET']))
                    {
                        $palletPostAry = $_POST['tryitOutAry']['PALLET'];
                    }
                    else if($postSearchAry['iCargoPackingType']==2) //Pallet Services
                    {
                        $kBooking = new cBooking();
                        $palletPostAry = array();
                        $palletPostAry = $kBooking->getCargoDeailsByBookingId($postSearchAry['id'],false,true,$szShipmentType);
                    }   
                    if(count($palletPostAry['iLength'])>0)
                    {  
                        $lengthAry = $palletPostAry['iLength'];
                        $counter_parcel = 0; 
                        $j = 0;
                        foreach($lengthAry as $key=>$lengthArys)
                        {
                            $counter = $j+1; 
                            $palletPostArys = array();
                            $palletPostArys['iLength'] = sanitize_all_html_input($palletPostAry['iLength'][$key]); 
                            $palletPostArys['iWidth'] = sanitize_all_html_input($palletPostAry['iWidth'][$key]);
                            $palletPostArys['iHeight'] = sanitize_all_html_input($palletPostAry['iHeight'][$key]);
                            $palletPostArys['iQuantity'] = sanitize_all_html_input($palletPostAry['iQuantity'][$key]);
                            $palletPostArys['iWeight'] = sanitize_all_html_input($palletPostAry['iWeight'][$key]);
                            $palletPostArys['idCargoMeasure'] = sanitize_all_html_input($palletPostAry['idCargoMeasure'][$key]);
                            $palletPostArys['idWeightMeasure'] = sanitize_all_html_input($palletPostAry['idWeightMeasure'][$key]);
 
                            ?>
                            <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" <?php echo $szChilDivClass; ?>> 
                                <?php
                                    echo display_tryitout_quote_cargo_details_form($szShipmentType,$counter,$palletPostArys);
                                ?>
                            </div>
                            <?php 
                            $j++;
                        } 
                    }
                    else
                    {   
                        ?>
                        <div id="add_booking_quote_container_0<?php echo $szDivKey;?>" <?php echo $szChilDivClass; ?> > 
                            <?php
                               echo display_tryitout_quote_cargo_details_form($szShipmentType,$counter);
                            ?>
                        </div>
                        <?php 
                    } 
                ?>
                <div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>" style="display:none;"></div>
            </div>   
            
            <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
            <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" />  
        </div>
        <?php
    }
}

function display_tryitout_quote_cargo_details_form($szShipmentType,$number,$cargoAry,$postSearchAry=array())
{   
    $t_base = "LandingPage/"; 
    $kConfig = new cConfig();  
    // geting all weight measure 
    $weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

    // geting all cargo measure 
    $cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
    
    if(!empty($cargoAry))
    { 
        $llop_counter = $number - 1 ;
        if($szShipmentType=='PALLET')
        { 
            $szPalletType = $cargoAry['szPalletType'];
        } 
        $fLength = $cargoAry['iLength'];
        $fWidth = $cargoAry['iWidth'];
        $fHeight = $cargoAry['iHeight'];
        $fWeight = $cargoAry['iWeight'];
        $iQuantity = $cargoAry['iQuantity']; 
        $idCargoMeasure = $cargoAry['idCargoMeasure']; 
        $idWeightMeasure = $cargoAry['idWeightMeasure'];  
    }
    else
    {
        if($szShipmentType=='PALLET')
        {
            $szPalletType = 1; //Euro Pallet
            $fLength = 120;
            $fWidth = 80;
        }
    }
    
    $szDottedDiv = ''; 
    $iSearchMiniPage = $szShipmentType; 
    $idSuffix = "_V_".$iSearchMiniPage ;   
    $szClassHide = '';
                    
    $iLanguage = 1;
    $palletTypesAry = array();
    $palletTypesAry = $kConfig->getAllPalletTypes(false,$iLanguage);
        
    $padding = "";
    if($number==1)
    {
        $padding = 'Padding-top:17px;';
    }
    else
    {
        $szClassHide = ' hide-cargo-heading'; 
    }
    echo $szDottedDiv; 
    if($szShipmentType=='PARCEL')
    {
        ?>
        <div class="email-fields-container clearfix" style="width:100%;"> 
            <span class="quote-field-container wd-12">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/length')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iLength][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fLength,1); }?>" id= "iLength<?php echo $number."".$idSuffix; ?>" class="first-cargo-fields<?php echo $idSuffix; ?>" onkeyup="enableTryItOutGetRatesButton(event);" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-12">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/width')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iWidth][<?=$number?>]" value="<?php if((float)$fWidth>0.00){ echo round_up($fWidth,1);}?>" id= "iWidth<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onkeyup="enableTryItOutGetRatesButton(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-12">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/height')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iHeight][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeight<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onkeyup="enableTryItOutGetRatesButton(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select name="tryitOutAry[<?php echo $szShipmentType;  ?>][idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?php echo $number."".$idSuffix; ?>" onchange="enableTryItOutGetRatesButton(event);" onclick="enableTryItOutGetRatesButton(event);">
                        <?php
                            if(!empty($cargoMeasureAry))
                            {
                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                {
                                    ?>
                                    <option value="<?=$cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            </span> 
            <span class="quote-field-container wds-12">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/count')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" onkeyup="enableTryItOutGetRatesButton(event);" pattern="[0-9]*" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);"/>
                </span>
            </span> 
            <span class="quote-field-container wds-12">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/weight')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iWeight][<?=$number?>]" value="<?php if((float)$fWeight>0.00){ echo round_up($fWeight,1);}?>" id= "iWeight<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" onkeyup="enableTryItOutGetRatesButton(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select size="1" id= "idWeightMeasure<?php echo $number."".$idSuffix; ?>" name="tryitOutAry[<?php echo $szShipmentType;  ?>][idWeightMeasure][<?=$number?>]" onchange="enableTryItOutGetRatesButton(event);" onclick="enableTryItOutGetRatesButton(event);">
                        <?php
                        if(!empty($weightMeasureAry))
                        {
                            foreach($weightMeasureAry as $weightMeasureArys)
                            {
                                ?>
                                <option value="<?=$weightMeasureArys['id']?>" <?php if($idWeightMeasure==$weightMeasureArys['id']){?> selected <?php }?>><?=$weightMeasureArys['szDescription']?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </span>
            </span>
            <span class="quote-field-container wds-15">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">
                    <select size="1" id= "szWeightType<?php echo $number."".$idSuffix; ?>" name="tryitOutAry[<?php echo $szShipmentType;  ?>][szWeightType][<?=$number?>]" onchange="enableTryItOutGetRatesButton(event);" onclick="enableTryItOutGetRatesButton(event);"> 
                        <option value="WEIGHT_PER_PACKAGE" <?php echo (($szWeightType=='WEIGHT_PER_PACKAGE')?'selected':''); ?>>Per package</option>
                        <option value="WEIGHT_IN_TOTAL" <?php echo (($szWeightType=='WEIGHT_IN_TOTAL')?'selected':''); ?>>In total</option> 
                    </select>
                </span>
            </span>
            <span class="quote-field-container wds-9" style="text-align:right;line-height:12px;<?php echo $padding; ?>"> 
                <a href="javascript:void(0);" onclick="add_more_cargo_tryitout_quote('<?php echo $szShipmentType ?>')" class="cargo-add-button"></a>  
                <a href="javascript:void(0);" id="cargo-line-remove-1-v<?php echo $szShipmentType ?>" class="cargo-remove-button" onclick="remove_cargo_tryitout_quote('add_booking_quote_container_<?php echo ($number-1)."_".$szShipmentType ?>','<?php echo $szShipmentType; ?>','<?php echo $number; ?>');"></a>
            </span>
            <input type="hidden" name="field_name_key_PARCEL[]" class="field_name_key_PARCEL" value="<?php echo $number."".$idSuffix; ?>">
        </div>
        <?php
    } 
    else if($szShipmentType=='PALLET')
    {
        ?>
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <span class="quote-field-container wd-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/count')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iQuantity][<?=$number?>]" value="<?=$iQuantity?>" onkeyup="enableTryItOutGetRatesButton(event);" id= "iQuantity<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" pattern="[0-9]*" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-17">
                <span class="input-title<?php echo $szClassHide;?>">Type<br></span>
                <span class="input-fields">  
                    <select name="tryitOutAry[<?php echo $szShipmentType;  ?>][szPalletType][<?=$number?>]" id= "szPalletType<?php echo $number."".$idSuffix; ?>" onchange="prefill_dimensions(this.value,'<?php echo $number."".$idSuffix; ?>');" onclick="enableTryItOutGetRatesButton(event);">
                        <?php
                            if(!empty($palletTypesAry))
                            {
                                foreach($palletTypesAry as $palletTypesArys)
                                {
                                    ?>
                                    <option value="<?=$palletTypesArys['id']?>" <?php if($szPalletType==$palletTypesArys['id']){?> selected <?php }?>><?php echo $palletTypesArys['szLongName']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            </span> 
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/length')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableTryItOutGetRatesButton(event);" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iLength][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fLength,1); }?>" id= "iLength<?php echo $number."".$idSuffix; ?>" class="first-cargo-fields<?php echo $idSuffix; ?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-9">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/width')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableTryItOutGetRatesButton(event);" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iWidth][<?=$number?>]" value="<?php if((float)$fWidth>0.00){ echo round_up($fWidth,1);}?>" id= "iWidth<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                </span>
            </span>
            <span class="quote-field-container wds-9">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/height')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableTryItOutGetRatesButton(event);" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iHeight][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeight<?php echo $number."".$idSuffix; ?>"  onkeypress="return isNumberKey(event);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="Optional" />
                    <input type="hidden"  name="tryitOutAry[<?php echo $szShipmentType;  ?>][iHeightDefault][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeightDefault<?php echo $number."".$idSuffix; ?>"  />
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select name="tryitOutAry[<?php echo $szShipmentType;  ?>][idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?php echo $number."".$idSuffix; ?>" onchange="enableTryItOutGetRatesButton(event);" onclick="enableTryItOutGetRatesButton(event);">
                        <?php
                            if(!empty($cargoMeasureAry))
                            {
                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                {
                                    ?>
                                    <option value="<?=$cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            </span> 
            <span class="quote-field-container wds-9">
                <span class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/weight')); ?><br></span>
                <span class="input-fields">  
                    <input type="text" onkeyup="enableTryItOutGetRatesButton(event);" name="tryitOutAry[<?php echo $szShipmentType;  ?>][iWeight][<?=$number?>]" value="<?php if((float)$fWeight>0.00){ echo round_up($fWeight,1);}?>" id= "iWeight<?php echo $number."".$idSuffix; ?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);"/>
                </span>
            </span>
            <span class="quote-field-container wds-8">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">  
                    <select size="1" id= "idWeightMeasure<?php echo $number."".$idSuffix; ?>" name="tryitOutAry[<?php echo $szShipmentType;  ?>][idWeightMeasure][<?=$number?>]" onchange="enableTryItOutGetRatesButton(event);" onclick="enableTryItOutGetRatesButton(event);">
                        <?php
                        if(!empty($weightMeasureAry))
                        {
                            foreach($weightMeasureAry as $weightMeasureArys)
                            {
                                ?>
                                <option value="<?=$weightMeasureArys['id']?>" <?php if($idWeightMeasure==$weightMeasureArys['id']){?> selected <?php }?>><?=$weightMeasureArys['szDescription']?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </span>
            </span>
            <span class="quote-field-container wds-15">
                <span class="input-title<?php echo $szClassHide;?>">&nbsp;<br></span>
                <span class="input-fields">
                    <select size="1" id= "szWeightType<?php echo $number."".$idSuffix; ?>" name="tryitOutAry[<?php echo $szShipmentType;  ?>][szWeightType][<?=$number?>]" onchange="enableTryItOutGetRatesButton(event);" onclick="enableTryItOutGetRatesButton(event);">  
                        <option value="WEIGHT_PER_PACKAGE" <?php echo (($szWeightType=='WEIGHT_PER_PACKAGE')?'selected':''); ?>>Per pallet</option>
                        <option value="WEIGHT_IN_TOTAL" <?php echo (($szWeightType=='WEIGHT_IN_TOTAL')?'selected':''); ?>>In total</option> 
                    </select>
                </span>
            </span>
            <span class="quote-field-container wd-9" style="text-align:right;line-height:12px;<?php echo $padding; ?>"> 
                <a href="javascript:void(0);" onclick="add_more_cargo_tryitout_quote('<?php echo $szShipmentType ?>')" class="cargo-add-button"></a>  
                <a href="javascript:void(0);" id="cargo-line-remove-1-v<?php echo $szShipmentType ?>" class="cargo-remove-button" onclick="remove_cargo_tryitout_quote('add_booking_quote_container_<?php echo ($number-1)."_".$szShipmentType ?>','<?php echo $szShipmentType; ?>');"></a>
            </span>
        </div>
        <input type="hidden" name="field_name_key_PALLET[]" class="field_name_key_PALLET" value="<?php echo $number."".$idSuffix; ?>">
        <?php
    }
    ?>
    <script type="text/javascript">
        validateTryItOutCargoDetails('<?php echo $szShipmentType; ?>','<?php echo $number; ?>');
    </script>
    <?php
}  

function display_forwarder_try_it_out_search_result($searchResultAry,$kQuote,$iQuickQuoteFlag=false)
{  
    $szPriceCalculationLogs = $kQuote->szPriceCalculationLogs;
    $szQuickQuoteToken = $kQuote->szQuickQuoteToken;
    $idQuickQuotePartner = $kQuote->idQuickQuotePartner;
    $idOriginCountry = $kQuote->idOriginCountry;
    $idDestinationCountry = $kQuote->idDestinationCountry;
                    
    $kConfig = new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true,false,false,false,true);  
    
    $bDTDTradesFlag = false;
    $kCourierServices = new cCourierServices(); 
    if($kCourierServices->checkFromCountryToCountryExists($idOriginCountry,$idDestinationCountry,true))
    { 
        $bDTDTradesFlag = true;
    }
    
    $kPartner = new cPartner(); 
    cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__;
    cPartner::$szGlobalToken = $szQuickQuoteToken; 
    cPartner::$szApiRequestMode = "QUICK_QUOTE";

    $postSearchAry = $kPartner->getQuickQuoteRequestData(); 
    
    $idForwarderSession =$_SESSION['forwarder_id'];
       
    $wd_15="wd-15";
    $wd_12="wd-12";
    $wd_13="wd-13";
    $colspan="9";
    $wd_10="wd-10";
    $wd_10_1="wd-10";
    if($iQuickQuoteFlag)
    {
        $wd_15="wd-15 font-12";
        $wd_12="wd-14 font-12";
        $wd_13="wd-14 font-12";
        $colspan="11";
        $wd_10="wd-7 font-12";
        $wd_10_1="wd-5 font-12";
        $wd_10_manual="wd-10 font-12";
        $wd_11_manual="wd-11 font-12";
        $wd_12_manual="wd-12 font-12";
        $wd_13_manual="wd-13 font-12";
    }
    $idCustomerCurrency = $_POST['tryitOutAry']['idCustomerCurrency'];
    
    if((int)$idCustomerCurrency==1)
    {
       $szCustomerCurrency = 'USD';
        $fCustomerCurrencyExchangeRate = '1.000'; 
    }
    else
    {
        $kWHSSearch = new cWHSSearch();
        $resultAry = $kWHSSearch->getCurrencyDetails($idCustomerCurrency);	
        $szCustomerCurrency = $resultAry['szCurrency'];
        $fCustomerCurrencyExchangeRate = $resultAry['fUsdValue'];
    } 
    $kConfig = new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true,false,false,false,true); 
    ?>
    <script>
        var idCustomerCurrency = '<?php echo $idCustomerCurrency; ?>';
        var fCustomerCurrencyExchangeRate = '<?php echo $fCustomerCurrencyExchangeRate ?>';
        var szCustomerCurrency = '<?php echo $szCustomerCurrency ?>';
        var currencyExchangeAry = new Array();
        <?php
            if(!empty($currencyAry))
            {
                foreach($currencyAry as $currencyArys)
                {
                    ?>
                    currencyExchangeAry[<?php echo $currencyArys['id']; ?>] = '<?php echo $currencyArys['fUsdValue']?>';
                    <?php
                }
            }
        ?> 
        function getExchangeRate(idCurrency)
        {
            if(idCurrency>0)
            {
                if(idCurrency==1)
                {
                    return 1;
                }
                else
                {
                    return currencyExchangeAry[idCurrency];
                } 
            }
        }    
        function calculate_prices(szServiceID)
        {
            var idForwarderCurrency = $("#idForwarderCurrency_"+szServiceID).val();
            console.log("idForwarderCurrency"+idForwarderCurrency);
            var fTotalForwarderManualFee = $("#fTotalForwarderManualFee_"+szServiceID).val();
            console.log("fTotalForwarderManualFee"+fTotalForwarderManualFee);
            var fTotalManualFee = $("#fTotalManualFee_"+szServiceID).val();
            var fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrency_"+szServiceID).val();
            var fVATPercentage = $("#fVATPercentage_"+szServiceID).val();
            var idForwarderCurrency = $("#idForwarderCurrency_"+szServiceID).val();
            var szCustomerCurrency = $("#szCustomerCurrency_"+szServiceID).val();
            var fForwarderManualFeeExchangeRate = getExchangeRate(idForwarderCurrency); 
            var fForwarderManualFeeUSD = fTotalForwarderManualFee * fForwarderManualFeeExchangeRate;
            var fTotalManualFeeCustomerCurrency = 0;
            var fTotalForwarderManualFeeCustomerCurrency = 0;
            var fTotalVat = 0;
            var fTotalPriceIncludingVat =0;
             
            if(idForwarderCurrency == idCustomerCurrency)
            {
                fTotalForwarderManualFeeCustomerCurrency = fTotalForwarderManualFee;
            } 
            else if(idForwarderCurrency==1)
            {
                fTotalForwarderManualFeeCustomerCurrency = fForwarderManualFeeUSD;
            }
            else if(fCustomerCurrencyExchangeRate>0)
            {
                fTotalForwarderManualFeeCustomerCurrency = Math.round(fForwarderManualFeeUSD/fCustomerCurrencyExchangeRate);
            }     
            console.log("fTotalForwarderManualFeeCustomerCurrency With out Markup"+fTotalForwarderManualFeeCustomerCurrency);
            if(idCustomerCurrency != idForwarderCurrency)
            {
                var fCurrencyMarkup = Math.round(fTotalForwarderManualFeeCustomerCurrency * 0.025);
                fTotalForwarderManualFeeCustomerCurrency = parseFloat(fTotalForwarderManualFeeCustomerCurrency);
                fTotalForwarderManualFeeCustomerCurrency = Math.round(fTotalForwarderManualFeeCustomerCurrency + fCurrencyMarkup);
                console.log("fTotalForwarderManualFeeCustomerCurrency With Markup"+fTotalForwarderManualFeeCustomerCurrency);
            }
            fTotalManualFeeCustomerCurrency=fTotalManualFee;
            
            //console.log("Manual Fee: "+fTotalManualFeeCustomerCurrency+ " Exchange: "+fManualFeeExchangeRate+" Fee USD: "+fManualFeeUSD+" Cust Exch: "+fCustomerCurrencyExchangeRate);
            
            fTotalPriceCustomerCurrency = parseInt(fTotalPriceCustomerCurrency);
            fTotalManualFeeCustomerCurrency = parseFloat(fTotalManualFeeCustomerCurrency);
            
            if(fTotalForwarderManualFeeCustomerCurrency!='')
            {
                fTotalForwarderManualFeeCustomerCurrency = parseFloat(fTotalForwarderManualFeeCustomerCurrency);
            }
            else
            {
                fTotalForwarderManualFeeCustomerCurrency=0.00;
            }
            fTotalForwarderManualFeeCustomerCurrency =parseFloat(fTotalForwarderManualFeeCustomerCurrency);
            console.log("fTotalForwarderManualFeeCustomerCurrency"+fTotalForwarderManualFeeCustomerCurrency);
            var fTotalPriceIncludingManualFee = Math.round((fTotalPriceCustomerCurrency + fTotalManualFeeCustomerCurrency+fTotalForwarderManualFeeCustomerCurrency)); 
            
            if(fVATPercentage>0)
            {
                fTotalVat = (fTotalPriceIncludingManualFee * fVATPercentage * 0.01);
                fTotalVat = fTotalVat.toFixed(2);
                fTotalVat = parseFloat(fTotalVat);
            }  
            fTotalPriceIncludingVat = fTotalPriceIncludingManualFee + fTotalVat;
            
            $("#fTotalPriceIncludingManualFee_"+szServiceID).val(fTotalPriceIncludingManualFee);
            $("#fTotalPriceIncludingVat_"+szServiceID).val(fTotalPriceIncludingVat);
            $("#fTotalVat_"+szServiceID).val(fTotalVat);
            
            fTotalVat = number_format(fTotalVat,'2','.',',');
            fTotalPriceIncludingVat = number_format(fTotalPriceIncludingVat,'2','.',',');
            fTotalPriceIncludingManualFee = number_format(fTotalPriceIncludingManualFee,'','.',',');
            $("#fTotalPriceIncludingManualFee_container_"+szServiceID).html(szCustomerCurrency +" " +fTotalPriceIncludingManualFee);
            $("#fTotalVat_container_"+szServiceID).html(szCustomerCurrency +" " +fTotalVat);
            $("#fTotalPriceIncludingVat_container_"+szServiceID).html(szCustomerCurrency +" " +fTotalPriceIncludingVat);
        }
    </script>
    <form id="quote_search_result_form" name="quote_search_result_form">
        <table class="quick-quote-results" style="width:100%;">
            <tr> 
                <th class="<?=$wd_10_1?>">Product</th>
                <th class="<?=$wd_10?>" style="text-align: center;">Pick-up</th>
                <th class="<?=$wd_10?>" style="text-align: center;">Cut-off</th>
                <th class="<?=$wd_10?>" style="text-align: center;">Available</th>
                <th class="<?=$wd_10?>" style="text-align: center;">Delivery</th>
                <th class="<?=$wd_10?>" style="text-align: center;">Time</th> 
                 <?php 
                if($iQuickQuoteFlag)
                {?>
                <th class="<?=$wd_11_manual?>" style="text-align: center;">Portal Price</th>
                <th class="<?=$wd_12_manual?>" style="text-align: center;">Manual Fee</th>
                <?php }?>
                <th class="<?=$wd_11_manual?>" style="text-align: center;">Price</th>
                <?php 
                if($iQuickQuoteFlag)
                {?>
                <th class="<?=$wd_11_manual?>" style="text-align: center;">VAT</th>
                <th class="<?=$wd_13_manual?>" style="text-align: center;">Price incl. VAT</th> 
                <?php }else{ ?>
                <th class="<?=$wd_11_manual?>" style="text-align: center;">Referral fee</th>
                <th class="<?=$wd_13_manual?>" style="text-align: center;">Net payment</th>                 
                <?php 
                }
                if($iQuickQuoteFlag)
                {?>
                    <th class="wd-2" style="text-align: center;"></th> 
                <?php }?>
            </tr>
            <?php
                if(!empty($searchResultAry))
                {
                    foreach($searchResultAry as $searchResultArys)
                    { 
                        if($iQuickQuoteFlag){
                            if($idForwarderSession!=$searchResultArys['idForwarder'])
                            {
                                /*
                                * On orwarder try-it-out we only services offered by the forwarder
                                */
                               continue;
                            } 
                            $idCustomerCurrency = $searchResultArys['idCurrency'];
                            $szCustomerCurrency = $searchResultArys['szCurrency'];
                            $idForwarderCurrency = $searchResultArys['idForwarderCurrency'];

                            $dtPickupDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtCutOffDate'])));
                            $dtDeliveryDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtAvailableDate']))); 

                            $szShipperCity = $postSearchAry['szShipperCity'];
                            $szConsigneeCity = $postSearchAry['szConsigneeCity'];

                            if($searchResultArys['idCourierProvider']>0)
                            {
                                $szProductName = 'Courier';
                                $dtCutOffDate = '-';
                                $dtAvailableDate = '-';
                                $szForwarderCompanyName = $searchResultArys['szForwarderCompanyName'];
                                $fCustomerCurrencyExchangeRate = $searchResultArys['fExchangeRate']; 
                                $idTransportMode = __BOOKING_TRANSPORT_MODE_COURIER__;

                                /*
                                * Pickup date from Door (origin)
                                */
                                $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;

                                /*
                                * Delivery date at Door(destination)
                                */
                                $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime); 
                                $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                
                                $szCollection = 'Yes';
                                $szDelivery = 'Yes';
                            }
                            else
                            {
                                $szForwarderCompanyName = $searchResultArys['szDisplayName']; 
                                $fCustomerCurrencyExchangeRate = $searchResultArys['fUsdValue']; 

                                $szWareHouseFromCity = $searchResultArys['szFromWHSCity'];
                                $szWareHouseToCity = $searchResultArys['szToWHSCity'];

                                if($bDTDTradesFlag)
                                {
                                    $szProductName = 'LTL';
                                    $dtCutOffDate = '-';
                                    $dtAvailableDate = '-'; 
                                    $idTransportMode = __BOOKING_TRANSPORT_MODE_ROAD__;

                                    /*
                                    * Pickup date from Door (origin)
                                    */
                                    $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                    $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;

                                    /*
                                    * Delivery date at Door(destination)
                                    */
                                    $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                    $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 

                                }
                                else
                                {
                                    $railTransportAry = array();
                                    $railTransportAry['idForwarder'] = $searchResultArys['idForwarder'];
                                    $railTransportAry['idFromWarehouse'] = $searchResultArys['idWarehouseFrom'];
                                    $railTransportAry['idToWarehouse'] = $searchResultArys['idWarehouseTo'];  
                    
                                    $kServices = new cServices();
                                    if($kServices->isRailTransportExists($railTransportAry))
                                    {
                                        $idTransportMode = __BOOKING_TRANSPORT_MODE_RAIL__;
                                        $szProductName = 'Rail'; 
                                    }
                                    else if($searchResultArys['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                                    {
                                        $idTransportMode = __BOOKING_TRANSPORT_MODE_AIR__;
                                        $szProductName = 'AIR'; 
                                    }
                                    else
                                    {
                                        $idTransportMode = __BOOKING_TRANSPORT_MODE_SEA__;
                                        $szProductName = 'LCL'; 
                                    } 
                                    $dtCutOffDate = '-';
                                    $dtAvailableDate = '-';
                                    $dtPickupDate = "-";
                                    $dtDeliveryDate = "-";

                                    $dtAvailableDay = ""; 
                                    $dtCutOffDay = "";
                                    $szPickupDay = "";
                                    $szDeliveryDay = "";

                                    $dtWhsCutOffDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtWhsCutOff'])));
                                    $dtWhsAvailableDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtWhsAvailable'])));
                                    if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__)
                                    {
                                        /*
                                        * Pickup date from Door (origin)
                                        */
                                        $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                        $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;

                                        /*
                                        * Pickup from warehouse(origin)
                                        */
                                        $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                        $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;

                                        /*
                                        * Delivery to warehouse(destination)
                                        */ 
                                        $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                        $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;

                                        /*
                                        * Delivery date at Door(destination)
                                        */
                                        $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                        $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                    }
                                    else if($searchResultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_PTD__)
                                    { 
                                        /*
                                        * Pickup from warehouse(origin)
                                        */
                                        $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                        $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;

                                        /*
                                        * Delivery to warehouse(destination)
                                        */ 
                                        $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                        $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;

                                        /*
                                        * Delivery date at Door(destination)
                                        */
                                        $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                        $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                    }
                                    else if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTP__)
                                    {
                                        /*
                                        * Pickup date from Door (origin)
                                        */
                                        $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                        $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;

                                        /*
                                        * Pickup from warehouse(origin)
                                        */
                                        $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                        $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;

                                        /*
                                        * Delivery to warehouse(destination)
                                        */ 
                                        $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                        $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;
                                    }
                                }
                                
                                if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTP__) // 1.DTD 2.DTD
                                {
                                    $szCollection = 'Yes';
                                }
                                else
                                {
                                    $szCollection = 'No';
                                } 
                                if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_PTD__) // 1.DTD 2.DTD
                                {
                                    $szDelivery = 'Yes';
                                }
                                else
                                {
                                    $szDelivery = 'No';
                                }
                            }   
                            
                            
                            $kAdmin = new cAdmin();
                            $idShipperCountry=$searchResultArys['idOriginCountry'];
                            $shipperCountryArr=$kAdmin->countriesView($idShipperCountry,$idShipperCountry);

                            if($szCollection=='Yes' && $shipperCountryArr['iUsePostcodes']==1)
                            {
                                $szShipperPostcodeRequired="Yes";
                            }
                            else
                            {
                                $szShipperPostcodeRequired="No";
                            }

                            $idConsigneeCountry==$searchResultArys['idDestinationCountry'];
                            $kAdmin = new cAdmin();
                            $consigneeCountryArr=$kAdmin->countriesView($idConsigneeCountry,$idConsigneeCountry);
                            if($szDelivery=='Yes' && $consigneeCountryArr['iUsePostcodes']==1)
                            {
                                $szConsigneePostcodeRequired="Yes";
                            }
                            else
                            {
                                $szConsigneePostcodeRequired="No";
                            }

                            $szServiceID = $searchResultArys['unique_id'];
                            $idManualFeeCurrency = $searchResultArys['idManualFeeCurrency']; 
                            $fTotalManualFee = round((float)$searchResultArys['fTotalManualFee'],2);
                            $fTotalManualFeeUSD = round((float)$searchResultArys['fTotalManualFeeUSD'],2);
                            //echo $idManualFeeCurrency."idManualFeeCurrency";
                            if($fTotalManualFee<=0)
                            {
                                //$fTotalManualFee = 0;
                            }
                            $fTotalPriceCustomerCurrency = round((float)$searchResultArys['fDisplayPrice']);
                            if($idManualFeeCurrency == $searchResultArys['idCurrency'])
                            {
                                $fTotalManualFeeCustomerCurrency = $fTotalManualFee;
                            } 
                            else if($searchResultArys['idCurrency']==1)
                            {
                                $fTotalManualFeeCustomerCurrency = $fTotalManualFeeUSD;
                            }
                            else if($fCustomerCurrencyExchangeRate>0)
                            {
                                $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeUSD/$fCustomerCurrencyExchangeRate));
                            } 
                            /*
                            * If Forwarder currency is not same as customer currency then currency mark-up should be applied on this 
                            */
                            if($idForwarderCurrency!=$idCustomerCurrency)
                            {
                                /*
                                * Adding currency mark-up on Manual Fee
                                */
                                $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeCustomerCurrency + ($fTotalManualFeeCustomerCurrency*.025))); 
                            }  
                            $fTotalPriceIncludingManualFee = round((float)($fTotalPriceCustomerCurrency + $fTotalManualFeeCustomerCurrency));

                            $fTotalPriceIncludingManualFee = round((float)$fTotalPriceCustomerCurrency);

                            $fVATPercentage = $searchResultArys['fVATPercentage'];
                            if($fVATPercentage>0)
                            {
                                $fTotalVat = round((float)($fTotalPriceIncludingManualFee * $searchResultArys['fVATPercentage'] * 0.01),2);
                            } 
                            $fTotalPriceIncludingVat = $fTotalPriceIncludingManualFee + $fTotalVat; 
                            $szSericeIDStr .= $szServiceID."||||";  
                            ?>
                            <tr class="fwd_try_it_out_rows" id="<?php echo "fwd_try_it_out_row_id_".$szServiceID; ?>" <?php if(!$iQuickQuoteFlag){?> onclick="display_forwarder_try_it_out_pricing_details('<?php echo $szQuickQuoteToken; ?>','<?php echo $szServiceID; ?>');" style="cursor:pointer;" <?php }?>> 
                                <td><?php echo $szProductName; ?></td>
                                <td title="<?php echo $szPickupDay; ?>" style="text-align: center;"><?php echo $dtPickupDate; ?></td>
                                <td title="<?php echo $dtCutOffDay; ?>" style="text-align: center;"><?php echo $dtCutOffDate;?></td>
                                <td title="<?php echo $dtAvailableDay; ?>" style="text-align: center;"><?php echo $dtAvailableDate;?></td>
                                <td title="<?php echo $szDeliveryDay; ?>" style="text-align: center;"><?php echo $dtDeliveryDate; ?></td>
                                <td style="text-align: center;"><?php echo $searchResultArys['iDaysBetween']." days"?></td> 
                                <?php 
                                if($iQuickQuoteFlag)
                                {?>
                                <td style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$searchResultArys['fDisplayPrice']); ?></td>
                                <td style="text-align: center;">
                                  <?php echo $searchResultArys['szForwarderCurrency']; ?> <input style="width:55px" type="text" onkeypress="return isNumberKey(event);"  id="fTotalForwarderManualFee_<?php echo $szServiceID; ?>" name="quoteResultAry[fTotalForwarderManualFee]" onkeyup="calculate_prices('<?php echo $szServiceID; ?>');prefill_selected_values('<?php echo $szServiceID; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" value="0">  
                                  <input  id="idManualFeeCurrency_<?php echo $szServiceID; ?>" name="quoteResultAry[idManualFeeCurrency][<?php echo $szServiceID; ?>]" value="<?php echo $idManualFeeCurrency?>" type="hidden"> 
                                  <input style="width:60px" id="fTotalManualFee_<?php echo $szServiceID; ?>" name="quoteResultAry[fTotalManualFee]" type="hidden" value="<?php echo (float)$fTotalManualFee; ?>"></td>
                                <?php }?>
                                <td id="fTotalPriceIncludingManualFee_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$fTotalPriceIncludingManualFee); ?></td>

                                <td id="fTotalVat_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$fTotalVat,2); ?></td>
                                <td id="fTotalPriceIncludingVat_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szCurrency']." ".number_format((float)$fTotalPriceIncludingVat,2); ?></td> 
                                <?php 
                                    if($iQuickQuoteFlag)
                                        {?>
                                        <input type="hidden" name="quoteResultAry[fTotalPriceCustomerCurrency][<?php echo $szServiceID; ?>]" id="fTotalPriceCustomerCurrency_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceCustomerCurrency; ?>">                                   
                                        <input type="hidden" name="quoteResultAry[fVATPercentage][<?php echo $szServiceID; ?>]" id="fVATPercentage_<?php echo $szServiceID; ?>" value="<?php echo $fVATPercentage; ?>">
                                        <input type="hidden" name="quoteResultAry[fTotalPriceIncludingManualFee][<?php echo $szServiceID; ?>]" id="fTotalPriceIncludingManualFee_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceIncludingManualFee; ?>">
                                        <input type="hidden" name="quoteResultAry[fTotalPriceIncludingVat][<?php echo $szServiceID; ?>]" id="fTotalPriceIncludingVat_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceIncludingVat; ?>">
                                        <input type="hidden" name="quoteResultAry[fTotalVat][<?php echo $szServiceID; ?>]" id="fTotalVat_<?php echo $szServiceID; ?>" value="<?php echo $fTotalVat; ?>">
                                        <input type="hidden" name="quoteResultAry[idShipmentType][<?php echo $szServiceID; ?>]" id="idShipmentType_<?php echo $szServiceID; ?>" value="<?php echo $searchResultArys['idShipmentType']; ?>"> 
                                        <input type="hidden" name="quoteResultAry[idTransportMode][<?php echo $szServiceID; ?>]" id="idTransportMode_<?php echo $szServiceID; ?>" value="<?php echo $idTransportMode; ?>"> 
                                        <input type="hidden" name="quoteResultAry[idForwarderCurrency][<?php echo $szServiceID; ?>]" id="idForwarderCurrency_<?php echo $szServiceID; ?>" value="<?php echo $idForwarderCurrency; ?>"> 
                                        <input type="hidden" name="quoteResultAry[szCustomerCurrency][<?php echo $szServiceID; ?>]" id="szCustomerCurrency_<?php echo $szServiceID; ?>" value="<?php echo $searchResultArys['szCurrency']; ?>">    
                                        
                                        <input type="hidden" name="quoteResultAry[szConsigneePostcodeRequiredFlag][<?php echo $szServiceID; ?>]" id="szConsigneePostcodeRequiredFlag_<?php echo $szServiceID; ?>" value="<?php echo $szConsigneePostcodeRequired; ?>"> 
                                        <input type="hidden" name="quoteResultAry[szShipperPostcodeRequiredFlag][<?php echo $szServiceID; ?>]" id="szShipperPostcodeRequiredFlag_<?php echo $szServiceID; ?>" value="<?php echo $szShipperPostcodeRequired; ?>"> 
                                        <input type="hidden" name="quoteResultAry[szCollectionFlag][<?php echo $szServiceID; ?>]" id="szCollectionFlag_<?php echo $szServiceID; ?>" value="<?php echo $szCollection; ?>"> 
                                        <input type="hidden" name="quoteResultAry[szDeliveryFlag][<?php echo $szServiceID; ?>]" id="szDeliveryFlag_<?php echo $szServiceID; ?>" value="<?php echo $szDelivery; ?>"> 

                                        
                                        <td>
                                        <input type="checkbox" name="quoteResultAry[szServiceIs][]" onclick="enableBookingButtons(this.id,'<?php echo $szServiceID; ?>');" id="szServiceIs_<?php echo $szServiceID; ?>" class="quick-quote-service-cb" value="<?php echo $szServiceID; ?>" />
                                        <?php
                                            if(!empty($searchResultArys['szCustomerPriceLogs']) && __ENVIRONMENT__!='LIVE')
                                            {    
                                                echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation_".$szServiceID."')>.</a> <div id='courier_service_calculation_".$szServiceID."' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation_".$szServiceID."') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                                                    </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$searchResultArys['szCustomerPriceLogs']." </div></div> </div></div>";                     
                                            }
                                        ?>
                                        </td>
                                    <?php }?>
                            </tr>
                            <?php 
                        }
                        else
                        {
                            if($idForwarderSession!=$searchResultArys['idForwarder'])
                        {
                            /*
                            * On orwarder try-it-out we only services offered by the forwarder
                            */
                           continue;
                        } 
                        $idCustomerCurrency = $searchResultArys['idCurrency'];
                        $szCustomerCurrency = $searchResultArys['szCurrency'];
                        $idForwarderCurrency = $searchResultArys['idForwarderCurrency'];
                        
                        $dtPickupDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtCutOffDate'])));
                        $dtDeliveryDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtAvailableDate']))); 
                        
                        $szShipperCity = $postSearchAry['szShipperCity'];
                        $szConsigneeCity = $postSearchAry['szConsigneeCity'];
                            
                        if($searchResultArys['idCourierProvider']>0)
                        {
                            $szProductName = 'Courier';
                            $dtCutOffDate = '-';
                            $dtAvailableDate = '-';
                            $szForwarderCompanyName = $searchResultArys['szForwarderCompanyName'];
                            $fCustomerCurrencyExchangeRate = $searchResultArys['fExchangeRate']; 
                            $fForwarderCurrencyExchangeRate = $searchResultArys['fForwarderCurrencyExchangeRate'];
                            $idTransportMode = __BOOKING_TRANSPORT_MODE_COURIER__;
                             
                            /*
                            * Pickup date from Door (origin)
                            */
                            $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                            $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                            
                            /*
                            * Delivery date at Door(destination)
                            */
                            $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime); 
                            $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity;  
                    
                            /*
                            *  For Courier searches, We have selfinvoice amount including referral Fee @AJAY
                            */ 
                            $fTotalPriceCustomerCurrency = $searchResultArys['fTotalSelfInvoiceAmount']; 
                        }
                        else
                        {
                            /*
                            *  For LCL searches, We have selfinvoice amount excluding referral Fee @AJAY
                            */ 
                            $fTotalPriceCustomerCurrency = $searchResultArys['fTotalSelfInvoiceAmount'] + $searchResultArys['fReferalAmount']; 
                            
                            $szForwarderCompanyName = $searchResultArys['szDisplayName']; 
                            $fCustomerCurrencyExchangeRate = $searchResultArys['fUsdValue']; 
                            $fForwarderCurrencyExchangeRate = $searchResultArys['fForwarderCurrencyExchangeRate'];
                        
                            $szWareHouseFromCity = $searchResultArys['szFromWHSCity'];
                            $szWareHouseToCity = $searchResultArys['szToWHSCity'];
                             
                            if($bDTDTradesFlag)
                            {
                                $szProductName = 'LTL';
                                $dtCutOffDate = '-';
                                $dtAvailableDate = '-'; 
                                $idTransportMode = __BOOKING_TRANSPORT_MODE_ROAD__;
                                
                                /*
                                * Pickup date from Door (origin)
                                */
                                $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                                
                                /*
                                * Delivery date at Door(destination)
                                */
                                $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 

                            }
                            else
                            {
                                $railTransportAry = array();
                                $railTransportAry['idForwarder'] = $searchResultArys['idForwarder'];
                                $railTransportAry['idFromWarehouse'] = $searchResultArys['idWarehouseFrom'];
                                $railTransportAry['idToWarehouse'] = $searchResultArys['idWarehouseTo'];  
                                 
                                $kServices = new cServices();
                                if($kServices->isRailTransportExists($railTransportAry))
                                {
                                    $idTransportMode = __BOOKING_TRANSPORT_MODE_RAIL__;
                                    $szProductName = 'Rail'; 
                                }
                                else if($searchResultArys['iWarehouseType']==__WAREHOUSE_TYPE_AIR__)
                                {
                                    $idTransportMode = __BOOKING_TRANSPORT_MODE_AIR__;
                                    $szProductName = 'AIR'; 
                                }
                                else
                                {
                                    $idTransportMode = __BOOKING_TRANSPORT_MODE_SEA__;
                                    $szProductName = 'LCL'; 
                                } 
                                $dtCutOffDate = '-';
                                $dtAvailableDate = '-';
                                $dtPickupDate = "-";
                                $dtDeliveryDate = "-";

                                $dtAvailableDay = ""; 
                                $dtCutOffDay = "";
                                $szPickupDay = "";
                                $szDeliveryDay = "";

                                $dtWhsCutOffDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtWhsCutOff'])));
                                $dtWhsAvailableDateTime = strtotime(str_replace("/","-",trim($searchResultArys['dtWhsAvailable'])));
                                if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTD__)
                                {
                                    /*
                                    * Pickup date from Door (origin)
                                    */
                                    $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                    $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                                    
                                    /*
                                    * Pickup from warehouse(origin)
                                    */
                                    $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                    $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;
                                    
                                    /*
                                    * Delivery to warehouse(destination)
                                    */ 
                                    $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                    $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;
                                    
                                    /*
                                    * Delivery date at Door(destination)
                                    */
                                    $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                    $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                }
                                else if($searchResultArys['idServiceType']==__SERVICE_TYPE_WTD__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_PTD__)
                                { 
                                    /*
                                    * Pickup from warehouse(origin)
                                    */
                                    $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                    $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;
                                    
                                    /*
                                    * Delivery to warehouse(destination)
                                    */ 
                                    $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                    $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;
                                    
                                    /*
                                    * Delivery date at Door(destination)
                                    */
                                    $dtDeliveryDate = (int)date("d",$dtDeliveryDateTime)." ".date("M",$dtDeliveryDateTime);  
                                    $szDeliveryDay = date("l",$dtDeliveryDateTime).", ".$szConsigneeCity; 
                                }
                                else if($searchResultArys['idServiceType']==__SERVICE_TYPE_DTW__ || $searchResultArys['idServiceType']==__SERVICE_TYPE_DTP__)
                                {
                                    /*
                                    * Pickup date from Door (origin)
                                    */
                                    $dtPickupDate = (int)date("d",$dtPickupDateTime)." ".date("M",$dtPickupDateTime);  
                                    $szPickupDay = date("l",$dtPickupDateTime).", ".$szShipperCity;
                                    
                                    /*
                                    * Pickup from warehouse(origin)
                                    */
                                    $dtCutOffDate = (int)date("d",$dtWhsCutOffDateTime)." ".date("M",$dtWhsCutOffDateTime);
                                    $dtCutOffDay = date("l",$dtWhsCutOffDateTime).", ".$szWareHouseFromCity;
                                    
                                    /*
                                    * Delivery to warehouse(destination)
                                    */ 
                                    $dtAvailableDate = (int)date("d",$dtWhsAvailableDateTime)." ".date("M",$dtWhsAvailableDateTime);  
                                    $dtAvailableDay = date("l",$dtWhsAvailableDateTime).", ".$szWareHouseToCity;
                                }
                            }
                        }   
                        
                        $szServiceID = $searchResultArys['unique_id'];
                        $idManualFeeCurrency = $searchResultArys['idManualFeeCurrency']; 
                        $fTotalManualFee = round((float)$searchResultArys['fTotalManualFee'],2);
                        $fTotalManualFeeUSD = round((float)$searchResultArys['fTotalManualFeeUSD'],2);
                    
                        if($idManualFeeCurrency == $searchResultArys['idForwarderCurrency'])
                        {
                            $fTotalManualFeeCustomerCurrency = $fTotalManualFee;
                        } 
                        else if($searchResultArys['idForwarderCurrency']==1)
                        {
                            $fTotalManualFeeCustomerCurrency = $fTotalManualFeeUSD;
                        }
                        else if($fForwarderCurrencyExchangeRate>0)
                        {
                            $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeUSD/$fForwarderCurrencyExchangeRate));
                        } 
                        /*
                        * If Forwarder currency is not same as customer currency then currency mark-up should be applied on this 
                        */
                        if($idForwarderCurrency!=$idCustomerCurrency)
                        {
                            /*
                            * Adding currency mark-up on Manual Fee
                            */
                           // $fTotalManualFeeCustomerCurrency = round((float)($fTotalManualFeeCustomerCurrency + ($fTotalManualFeeCustomerCurrency*.025))); 
                        }  
                        $fTotalPriceIncludingManualFee = $fTotalPriceCustomerCurrency + $fTotalManualFeeCustomerCurrency;
                        
                        $fTotalPriceIncludingManualFee = $fTotalPriceCustomerCurrency;
                    
                        $fReferalPercentage = $searchResultArys['fReferalPercentage'];
                        if($fReferalPercentage>0)
                        {
                            //$fTotalVat = round((float)($fTotalPriceIncludingManualFee * $searchResultArys['fVATPercentage'] * 0.01),2);
                            $fReferalAmount = round((float)($fTotalPriceIncludingManualFee * $fReferalPercentage * 0.01),2);
                        } 
                        $fTotalPriceIncludingVat = $fTotalPriceIncludingManualFee - $fReferalAmount; 
                        $szSericeIDStr .= $szServiceID."||||";  
                        ?>
                        <tr class="fwd_try_it_out_rows" id="<?php echo "fwd_try_it_out_row_id_".$szServiceID; ?>" <?php if(!$iQuickQuoteFlag){?> onclick="display_forwarder_try_it_out_pricing_details('<?php echo $szQuickQuoteToken; ?>','<?php echo $szServiceID; ?>');" style="cursor:pointer;" <?php }?>> 
                            <td><?php echo $szProductName; ?></td>
                            <td title="<?php echo $szPickupDay; ?>" style="text-align: center;"><?php echo $dtPickupDate; ?></td>
                            <td title="<?php echo $dtCutOffDay; ?>" style="text-align: center;"><?php echo $dtCutOffDate;?></td>
                            <td title="<?php echo $dtAvailableDay; ?>" style="text-align: center;"><?php echo $dtAvailableDate;?></td>
                            <td title="<?php echo $szDeliveryDay; ?>" style="text-align: center;"><?php echo $dtDeliveryDate; ?></td>
                            <td style="text-align: center;"><?php echo $searchResultArys['iDaysBetween']." days"?></td> 
                            <?php 
                            if($iQuickQuoteFlag)
                            {?>
                            <td style="text-align: center;"><?php echo $searchResultArys['szForwarderCurrency']." ".number_format((float)$fTotalPriceCustomerCurrency,2); ?></td>
                            <td style="text-align: center;">
                              <?php echo $searchResultArys['szForwarderCurrency']; ?> <input style="width:55px" type="text" onkeypress="return isNumberKey(event);"  id="fTotalForwarderManualFee_<?php echo $szServiceID; ?>" name="quoteResultAry[fTotalForwarderManualFee]" onkeyup="calculate_prices('<?php echo $szServiceID; ?>');prefill_selected_values('<?php echo $szServiceID; ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" value="0">  
                              <input  id="idManualFeeCurrency_<?php echo $szServiceID; ?>" name="quoteResultAry[idManualFeeCurrency][<?php echo $szServiceID; ?>]" value="<?php echo $idManualFeeCurrency?>" type="hidden"> 
                              <input style="width:60px" id="fTotalManualFee_<?php echo $szServiceID; ?>" name="quoteResultAry[fTotalManualFee]" type="hidden" value="<?php echo (float)$fTotalManualFee; ?>"></td>
                            <?php }?>
                            <td id="fTotalPriceIncludingManualFee_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szForwarderCurrency']." ".number_format((float)$fTotalPriceIncludingManualFee,2); ?></td>
                            
                            <td id="fTotalVat_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szForwarderCurrency']." ".number_format((float)$fReferalAmount,2); ?></td>
                            <td id="fTotalPriceIncludingVat_container_<?php echo $szServiceID; ?>" style="text-align: center;"><?php echo $searchResultArys['szForwarderCurrency']." ".number_format((float)$fTotalPriceIncludingVat,2); ?></td> 
                            <?php 
                                if($iQuickQuoteFlag)
                                    {?>
                                    <input type="hidden" name="quoteResultAry[fTotalPriceCustomerCurrency][<?php echo $szServiceID; ?>]" id="fTotalPriceCustomerCurrency_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceCustomerCurrency; ?>">                                   
                                    <input type="hidden" name="quoteResultAry[fVATPercentage][<?php echo $szServiceID; ?>]" id="fVATPercentage_<?php echo $szServiceID; ?>" value="<?php echo $fVATPercentage; ?>">
                                    <input type="hidden" name="quoteResultAry[fTotalPriceIncludingManualFee][<?php echo $szServiceID; ?>]" id="fTotalPriceIncludingManualFee_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceIncludingManualFee; ?>">
                                    <input type="hidden" name="quoteResultAry[fTotalPriceIncludingVat][<?php echo $szServiceID; ?>]" id="fTotalPriceIncludingVat_<?php echo $szServiceID; ?>" value="<?php echo $fTotalPriceIncludingVat; ?>">
                                    <input type="hidden" name="quoteResultAry[fTotalVat][<?php echo $szServiceID; ?>]" id="fTotalVat_<?php echo $szServiceID; ?>" value="<?php echo $fTotalVat; ?>">
                                    <input type="hidden" name="quoteResultAry[idShipmentType][<?php echo $szServiceID; ?>]" id="idShipmentType_<?php echo $szServiceID; ?>" value="<?php echo $searchResultArys['idShipmentType']; ?>"> 
                                    <input type="hidden" name="quoteResultAry[idTransportMode][<?php echo $szServiceID; ?>]" id="idTransportMode_<?php echo $szServiceID; ?>" value="<?php echo $idTransportMode; ?>"> 
                                    <input type="hidden" name="quoteResultAry[idForwarderCurrency][<?php echo $szServiceID; ?>]" id="idForwarderCurrency_<?php echo $szServiceID; ?>" value="<?php echo $idForwarderCurrency; ?>"> 
                                    <input type="hidden" name="quoteResultAry[szCustomerCurrency][<?php echo $szServiceID; ?>]" id="szCustomerCurrency_<?php echo $szServiceID; ?>" value="<?php echo $searchResultArys['szCurrency']; ?>">    
                                    <input type="hidden" name="quoteResultAry[fForwarderCurrencyExchangeRate][<?php echo $szServiceID; ?>]" id="fForwarderCurrencyExchangeRate_<?php echo $szServiceID; ?>" value="<?php echo $fForwarderCurrencyExchangeRate; ?>">    
                                    <input type="hidden" name="quoteResultAry[fReferalPercentage][<?php echo $szServiceID; ?>]" id="fReferalPercentage_<?php echo $szServiceID; ?>" value="<?php echo $fReferalPercentage; ?>">     
                                    <td>
                                    <input type="checkbox" name="quoteResultAry[szServiceIs][]" onclick="enableBookingButtons(this.id,'<?php echo $szServiceID; ?>');" id="szServiceIs_<?php echo $szServiceID; ?>" class="quick-quote-service-cb" value="<?php echo $szServiceID; ?>" />
                                    <?php
                                        if(!empty($searchResultArys['szCustomerPriceLogs']) && __ENVIRONMENT__!='LIVE')
                                        {    
                                            echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation_".$szServiceID."')>.</a> <div id='courier_service_calculation_".$szServiceID."' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation_".$szServiceID."') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
                                                </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$searchResultArys['szCustomerPriceLogs']." </div></div> </div></div>";                     
                                        }
                                    ?>
                                    </td>
                                <?php }?>
                            </tr>
                            <?php
                        }
                    }
                    if(!$iQuickQuoteFlag){
                    ?>
                    <tr>
                        <td colspan="<?=$colspan;?>">
                            <div class="fwd_try_it_declaration_text">Click on any service line to see how the price is calculated - this is not available to customers on Transporteca.</div>
                        </td>
                    </tr>
                    <?php
                    }
                }
                else
                {
                    ?>
                    <tr>
                        <td colspan="<?=$colspan;?>" style="text-align:center;"><strong>There are no services online for this search</strong></td>
                    </tr>
                    <?php
                }
            ?>
        </table>  
        <input type="hidden" name="quoteResultAry[idPackingType]" value="<?php echo $szPackingType; ?>" id="idPackingType">
        <input type="hidden" name="quoteResultAry[szAllServieIDS]" value="<?php echo $szSericeIDStr; ?>" id="szAllServieIDS">
        <input type="hidden" name="quoteResultAry[szToken]" value="<?php echo $szQuickQuoteToken; ?>" id="szToken">
        <input type="hidden" name="quoteResultAry[szInternalComment]" value="<?php echo $szInternalComment; ?>" id="szInternalComment"> 
    </form>  
    <div class="clearfix"></div>
    <br>
     <?php 
    if($iQuickQuoteFlag && !empty($searchResultAry))
    {?> 
    <div id="quick_quote_service_button_container" class="clearfix email-fields-container quick-quote-button-container" style="width:100%;text-align: center;padding: 10px 0;">
        <span style="width:127px;"> 
            <a id="quote_result_send_quote_button" class="button1 quick-quote-action-button" style="opacity:0.4;" href="javascript:void(0)">
                <span>Send Quote</span>
            </a>
        </span>
        <span style="width:127px;"> 
            <a id="quote_result_make_booking_button" class="button1 quick-quote-action-button" style="opacity:0.4;" href="javascript:void(0)">
                <span>Make Booking</span>
            </a>
        </span>
    </div>
       
    <?php }else{?>
    <div id="display_try_it_out_price_detailscontainer"></div>
    <?php }?>
    <?php   
    /*
    if(!empty($szPriceCalculationLogs))
    {  
        echo "<a href='javascript:void(0)' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
            </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$szPriceCalculationLogs." </div></div> </div></div>";                    

    }
     * 
     */
} 
function display_forwarder_private_customer_setting($kForwarder,$privateCustomerFeeAry=array())
{
    $possibleProductType = array('LCL','LTL','COURIER','AIR');
    $kConfig = new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);   
    
    $kForwarderContact = new cForwarderContact();
    $kForwarderContact->getUserDetails($_SESSION['forwarder_user_id']);
    $idForwarder = $kForwarderContact->idForwarder;
                
    $kForwarder_new = new cForwarder();
    $kForwarder_new->load($idForwarder); 
    
    $kForwarder = new cForwarder();
    $kWhsSearch = new cWHSSearch();
    
    $kForwarder->load($idForwarder);
    $idForwarderAccountCurrency = $kForwarder->szCurrency; 
    
    if($idForwarderAccountCurrency==1)
    {
        $szForwarderCurrency = 'USD';
    }
    else
    {
        $currencyResultAry = array();
        $currencyResultAry = $kWhsSearch->getCurrencyDetails($idForwarderAccountCurrency);	 
        $szForwarderCurrency = $currencyResultAry['szCurrency']; 
    }
    ?>
    <script type="text/javascript">
        //enablePrivateCustomerSetting();
    </script>
    <form id="forwarder_private_customer_setting_form" name="forwarder_private_customer_setting_form" method="post" action="javascript:void(0);">
        <?php
            if(!empty($possibleProductType))
            {
                foreach($possibleProductType as $possibleProductTypes)
                {
                    if($possibleProductTypes=='LCL')
                    {
                        $szDescription = "We accept LCL bookings from private customers, at a private customer fee of";
                    }
                    else if($possibleProductTypes=='LTL')
                    {
                        $szDescription = "We accept LTL bookings from private customers, at a private customer fee of";
                    }
                    else if($possibleProductTypes=='COURIER')
                    {
                        $szDescription = "We accept Courier bookings from private customers, at a private customer fee of";
                    }
                    else if($possibleProductTypes=='AIR')
                    {
                        $szDescription = "We accept Airfreight bookings from private customers, at a private customer fee of";
                    }
                    if($_POST['privateCustomerAry'])
                    {
                        $privateCustomerAry = $_POST['privateCustomerAry'];
                        $iPrivateCustomerAvailable = $privateCustomerAry['iPrivateCustomerAvailable'][$possibleProductTypes];
                        $idCurrency = $privateCustomerAry['idCurrency'][$possibleProductTypes];
                        $fCustomerFee = $privateCustomerAry['fCustomerFee'][$possibleProductTypes];
                    }
                    else
                    {
                        if(!empty($privateCustomerFeeAry[$possibleProductTypes]))
                        {
                            $iPrivateCustomerAvailable = $privateCustomerFeeAry[$possibleProductTypes]['iPrivateCustomerAvailable'];
                            $idCurrency = $privateCustomerFeeAry[$possibleProductTypes]['idCurrency'];
                            $fCustomerFee = $privateCustomerFeeAry[$possibleProductTypes]['fCustomerFee'];
                            if($fCustomerFee==0)
                            {
                                $fCustomerFee = "0.00";
                            }
                        }
                        else
                        {
                            $iPrivateCustomerAvailable = 0;
                            $fCustomerFee = '0.00';
                            $idCurrency = $kForwarder_new->szCurrency;
                        }
                    }  
                    //From now we always consider forwarder account currency here
                    $idCurrency = $idForwarderAccountCurrency; 
                    ?>
                    <div class="clearfix email-fields-container" style="width:100%;">  
                        <span class="quote-field-container wd-83 checkbox-container">
                            <input type="checkbox" name="privateCustomerAry[iPrivateCustomerAvailable][<?php echo $possibleProductTypes; ?>]" id="iPrivateCustomerAvailable_<?php echo $possibleProductTypes; ?>" value="1" <?php echo (($iPrivateCustomerAvailable==1)?'checked="checked"':'');?> onclick="enablePrivateCustomerSetting();">
                             <?php echo $szDescription; ?>
                        </span>
                        <span class="quote-field-container wds-17">
                            <span style="width:35%;"><?php echo $szForwarderCurrency; ?></span>
                            <input type="text" style="width:60%;" name="privateCustomerAry[fCustomerFee][<?php echo $possibleProductTypes; ?>]" value="<?php echo $fCustomerFee; ?>" onkeyup="enablePrivateCustomerSetting(event);" id= "<?php echo "fCustomerFee_".$possibleProductTypes; ?>" onkeypress="return isNumberKey(event);" pattern="[0-9]*" onblur="" onfocus="check_form_field_not_required(this.form.id,this.id);" />
                            <input type="hidden" name="privateCustomerAry[idCurrency][<?php echo $possibleProductTypes; ?>]" id="idCurrency_<?php echo $possibleProductTypes; ?>" value="<?php echo $idCurrency; ?>">
                        </span>
                    </div>
                    <?php
                }
            }
        ?> 
        <!--
        <select style="width:35%;" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enablePrivateCustomerSetting();" onchange="enablePrivateCustomerSetting();" name="privateCustomerAry[idCurrency][<?php echo $possibleProductTypes; ?>]" id="idCurrency_<?php echo $possibleProductTypes; ?>">
                                <?php
                                    if(!empty($currencyAry))
                                    {
                                        foreach($currencyAry as $currencyArys)
                                        {
                                            ?>
                                            <option value="<?=$currencyArys['id']?>" <?php echo (($currencyArys['id']==$idCurrency)?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                            <?php
                                        }
                                    }
                                 ?>
                            </select> -->
        <div style="clear: both;"></div><br>
        <div class="clearfix email-fields-container" style="width:100%;text-align: center;">
            <a href="javascript:void(0);" class="button1" id="private_customer_setting_save_button" style="opacity: 0.4"><span>Save</span></a>
        </div>
    </form>
    <?php
    if(!empty($kForwarder->arErrorMessages))
    {  
        $formId = 'forwarder_private_customer_setting_form'; 
        display_form_validation_error_message($formId,$kForwarder->arErrorMessages);
    }
}

function forwarder_old_quote_listing($bookingEstimateAry,$estimateSearchAry)
{ 
    $t_base="management/pendingTray/";     
    ?>
    <div class="clearfix courier-provider courier-provider-scroll" style="max-height:420px;width:99.8%;">	
        <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" id="mode-transport-table" style="border-right:0;">
            <tr id="table-header"> 
              <td style="cursor:pointer;width:8%;border-top:none;border-left:none;" onclick="sort_task_estimate_list_forwarder('szTransportMode','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/mode');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szTransportMode">&nbsp;</span></td>
              <td style="cursor:pointer;width:12%;border-top:none;" onclick="sort_task_estimate_list_forwarder('szServiceType','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/service_terms');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szServiceType">&nbsp;</span></td>
              <td style="cursor:pointer;width:14%;border-top:none;" onclick="sort_task_estimate_list_forwarder('szShipperCity','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/from_city');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szShipperCity">&nbsp;</span></td>
              <td style="cursor:pointer;width:14%;border-top:none;" onclick="sort_task_estimate_list_forwarder('szConsigneeCity','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/to_city');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szConsigneeCity">&nbsp;</span></td>
              <td style="cursor:pointer;width:8%;border-top:none;" onclick="sort_task_estimate_list_forwarder('fCargoVolume','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/volume');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-fCargoVolume">&nbsp;</span></td>
              <td style="cursor:pointer;width:8%;border-top:none;" onclick="sort_task_estimate_list_forwarder('fCargoWeight','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/weight');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-fCargoWeight">&nbsp;</span></td>
              <td style="cursor:pointer;width:10%;border-top:none;" onclick="sort_task_estimate_list_forwarder('fTotalPriceCustomerCurrency','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/price');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-fTotalPriceCustomerCurrency">&nbsp;</span></td>
              <td style="cursor:pointer;width:12%;border-top:none;" onclick="sort_task_estimate_list_forwarder('dtCreatedOn','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/quote');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-dtCreatedOn">&nbsp;</span></td>
              <td style="cursor:pointer;width:12%;border-top:none;border-right:none;" onclick="sort_task_estimate_list_forwarder('dtBookingConfirmed','<?php echo $estimateSearchAry['idOriginCountry']; ?>','<?php echo $estimateSearchAry['idDestinationCountry']; ?>')"><strong><?php echo t($t_base.'fields/szBooking_date');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-dtBookingConfirmed">&nbsp;</span></td>
          </tr>   	
      <?php
          if(!empty($bookingEstimateAry))
          {
              foreach($bookingEstimateAry as $bookingEstimateArys)
              { 
                  $szServiceType = ''; 
                  if(!empty($bookingEstimateArys['szDisplayNameForwarder']))
                  {
                      $szServiceType = $bookingEstimateArys['szDisplayNameForwarder'] ;
                  }
                  else
                  {
                      $szServiceType = $bookingEstimateArys['szDisplayNameForwarder'];
                  }
                  if(empty($bookingEstimateArys['szTransportMode']))
                  {
                      $bookingEstimateArys['szTransportMode'] = 'Sea';
                  }
                  //echo $bookingEstimateArys['fTotalPriceCustomerCurrency']."fTotalPriceCustomerCurrency<br />";
                  $fTotalPriceForwarderCurrency = $bookingEstimateArys['fTotalPriceForwarderCurrency']; 
                  $szForwarderCurrency =$bookingEstimateArys['szForwarderCurrency'];
                  //echo "<br> USD: ".$fBookingPriceUsd." Rate: ".$fCustomerExchangeRate." ORG: ".$bookingEstimateArys['fTotalPriceCustomerCurrency'] ;
                  $fBookingPriceUsd = $bookingEstimateArys['fTotalPriceCustomerCurrency'] * $bookingEstimateArys['fCustomerExchangeRate']; 
                  //echo "<br> USD: ".$fBookingPriceUsd." Rate: ".$fCustomerExchangeRate." ORG: ".$bookingEstimateArys['fTotalPriceCustomerCurrency'] ;
                  if($fCustomerExchangeRate>0)
                  {
                      $fBookingPrice = ($fBookingPriceUsd /$fCustomerExchangeRate);
                  }
                  else
                  {
                      $fBookingPrice = $fBookingPriceUsd ;
                      $szCustomerCurrency = 'USD';
                  }
                  
                  
                if((int)$bookingEstimateArys['idHandlingCurrency']>0)
                {

                    if((float)$bookingEstimateArys['fHandlingFeePerBooking']>0.00)
                    {
                        
                        $fMarkupPercentage = $bookingEstimateArys['fMarkupPercentage'];
                        $fMarkupPercentageAmountHandlingFee=$bookingEstimateArys['fHandlingFeePerBooking']*.01*$fMarkupPercentage;
                        //echo $fMarkupPercentageAmountHandlingFee."fMarkupPercentageAmountHandlingFee<br />";
                        $bookingEstimateArys['fHandlingFeePerBooking'] = $bookingEstimateArys['fHandlingFeePerBooking']-$fMarkupPercentageAmountHandlingFee;
                        if((int)$bookingEstimateArys['idHandlingCurrency']==1)
                        {
                            $fBookingPrice = $fBookingPrice - $bookingEstimateArys['fHandlingFeePerBooking'];
                        }
                        else
                        {
                            $fHandlingFeePerBooking =  $bookingEstimateArys['fHandlingFeePerBooking'] * $bookingEstimateArys['fHandlingCurrencyExchangeRate'];

                            $fBookingPrice = $fBookingPrice - $fHandlingFeePerBooking;
                        }
                    }
                    else
                    {
                        
                        $fMarkupPercentage = $bookingEstimateArys['fMarkupPercentage'];
                        $fMarkupPercentageAmountHandlingFee=$bookingEstimateArys['fTotalHandlingFee']*.01*$fMarkupPercentage;
                        //echo $fMarkupPercentageAmountHandlingFee."fMarkupPercentageAmountHandlingFee2<br />";
                        $bookingEstimateArys['fTotalHandlingFee'] = $bookingEstimateArys['fTotalHandlingFee']-$fMarkupPercentageAmountHandlingFee;
                        
                        if((int)$bookingEstimateArys['idHandlingCurrency']==1)
                        {
                            $fBookingPrice = $fBookingPrice - $bookingEstimateArys['fTotalHandlingFee'];
                        }
                        else
                        {
                            $fHandlingFeePerBooking =  $bookingEstimateArys['fTotalHandlingFee'] * $bookingEstimateArys['fHandlingCurrencyExchangeRate'];

                            $fBookingPrice = $fBookingPrice - $fHandlingFeePerBooking;
                        }
                    }
                }
                
                  $dtCreatedOn="-";
                  if($bookingEstimateArys['dtCreatedOn']!='' && $bookingEstimateArys['dtCreatedOn']!='0000-00-00 00:00:00')
                  {
                     $dtCreatedOn = date('d M, Y',strtotime($bookingEstimateArys['dtCreatedOn']));
                  }  
                  
                  $dtBookingDate="-";
                  if($bookingEstimateArys['dtBookingConfirmed']!='' && $bookingEstimateArys['dtBookingConfirmed']!='0000-00-00 00:00:00')
                  {
                     $dtBookingDate = date('d M, Y',strtotime($bookingEstimateArys['dtBookingConfirmed']));
                  }
      ?>
                 <tr id="truckin_tr____<?php echo $bookingEstimateArys['id']; ?>" title="<?php echo ($szMode=='SORT_ESTIMATE_QUOTE_LIST')?$bookingEstimateArys['szFileRef']:$bookingEstimateArys['szBookingRef'];?>"> 
                      <td style="border-left:none;"><?php echo $bookingEstimateArys['szTransportMode']?></td>
                      <td><?php echo $szServiceType; ?></td>
                      <td><?php echo $bookingEstimateArys['szShipperCity']?></td>
                      <td><?php echo $bookingEstimateArys['szConsigneeCity']; ?></td>                      
                      <td><?php echo format_volume($bookingEstimateArys['fCargoVolume']); ?></td>
                      <td><?php echo number_format((float)$bookingEstimateArys['fCargoWeight']); ?></td>
                      <td><?php echo $szCustomerCurrency." ".number_format((float)$fBookingPrice);?></td>
                      <td><?php echo $dtCreatedOn;?></td>
                      <td style="border-right:none;"><?php echo $dtBookingDate;?></td>
                  </tr> 
              <?php		
              } 				 
          }
          else
          {?>
            <tr>
               <td colspan="9" align="center">
                   <h4><b><?=t($t_base.'fields/no_record_found');?></b></h4>
               </td>
            </tr>
          <?php   }   ?>  
         </table>
      </div>
    <?php
}

function displayForwarderHeaderNotificaion($bOnload=false)
{ 
    $t_top_nav="popup/message/";
    if((int)$_SESSION['forwarder_user_id']>0)
    {
        $idForwarder   = (int)$_SESSION['forwarder_id']; 
        $idForwarderUser  = (int)$_SESSION['forwarder_user_id']; 
        $kForwarder = new cForwarder(); 
        $forwarderProfileContactRole = $kForwarder->adminEachForwarder($idForwarderUser);	 
        $kForwarder->load($idForwarder); 
        $logo =$kForwarder->szLogo;
        if($bOnload)
        {
            if(!empty($_SESSION['adminHeaderNoticationFlagsAry']))
            {
                $notificationFlagsAry = array();
                $notificationFlagsAry = $_SESSION['adminHeaderNoticationFlagsAry']; 

                $priceAwaitingForApproval = $notificationFlagsAry['priceAwaitingForApproval'];
                $waitingForwarderDataApproval = $notificationFlagsAry['waitingForwarderDataApproval'];
                $notAcknowledged = $notificationFlagsAry['notAcknowledged'];
                $strCountry = $notificationFlagsAry['strCountry'];
                $iNumLabelSent = $notificationFlagsAry['iNumLabelSent'];
                $iNumLabelNotSent = $notificationFlagsAry['iNumLabelNotSent'];
                $iNumPendingReview = $notificationFlagsAry['iNumPendingReview'];
                $subFlag = $notificationFlagsAry['subFlag']; 
                $iPayOverDueCount = $notificationFlagsAry['iPayOverDueCount'];  
                $iCountGmailError = $notificationFlagsAry['iCountGmailError'];
                $iNumSellRateWithoutBuyRate = $notificationFlagsAry['iNumSellRateWithoutBuyRate']; 
                $gmailSyncErrorAry = $notificationFlagsAry['gmailSyncErrorAry'];
            }
        }
        else
        {
            $kUploadBulkService = new cUploadBulkService(); 
            $kCourierServices = new cCourierServices(); 
            $kBooking = new cBooking(); 
            $kAdmin = new cAdmin();
            $kGmail = new cGmail();
            $kInsurance = new cInsurance(); 
            //$iNumSellRateWithoutBuyRate = $kInsurance->isBuyrateAvailableForSellRate();

            $label_n_sendFlag=checkManagementPermission($kAdmin,"__LABEL_NOT_SENT__",2);
            if($label_n_sendFlag)
            { 
                $iNumLabelNotSent = $kCourierServices->getAllNewBookingWithCourier(false,false,true,true);
            }

            $label_sendFlag=checkManagementPermission($kAdmin,"__LABEL_SENT__",2);
            if($label_sendFlag)
            { 
                $iNumLabelSent = $kCourierServices->getAllNewBookingWithCourier(0,__LABEL_SENT__,true,true);
            }
            /*
             * warning
             */   
            $iNumAckBlueIcon = false;
            $iNumAckRedIcon = false;
            $iNumAckRedIcon = $kBooking->bookingNotConfirmedDateMoreThanFiveDays($idForwarder,true);    
            if($iNumAckRedIcon<=0)
            {
                $iNumAckBlueIcon = $kBooking->bookingNotConfirmedDateMoreThanFiveDays($idForwarder,true,true);  
            }
            /*
            $biLogoNotification = false;
            if($forwarderProfileContactRole == __ADMINISTRATOR_PROFILE_ID__)		
            { 
                $checkComapnyProfile = $kForwarder->checkForwarderComleteCompanyInfomation($idForwarder); 
		$checkComapnyEmails  = $kForwarder->checkBillingBookingCustomer($idForwarder); 
		//$checkComapnyBankDet = $kForwarder->checkForwarderComleteBankInfomation($idForwarder); 
		$checkTnc = $kForwarder->checkAgreeTNC($idForwarder);  
		$checkCFS = $kForwarder->checkCFS($idForwarder);
		
		$checkPricingWareh = $kForwarder->checkWarehouses($idForwarder);
		
                if( !$checkComapnyEmails && !$checkComapnyProfile && ($logo==''))
                {
                    $biLogoNotification = true;
                }
		if($checkComapnyProfile || $checkComapnyEmails  || $checkTnc || (!$checkCFS) || (!$checkPricingWareh) || ($logo=='')  )
		{	 
                    $offline  = 1; 
		}
		else
		{ 
                    $offline  = 0;	 
		}  
                if($offline)
                {
                    //Forwarder is Offline
                }
                else
                {
                    $versionUpdate = $kForwarder->iVersionUpdate;		 
                    $checkPricingHaulage = $kForwarder->checkPricingHaulage($idForwarder); 
                    $checkCC = $kForwarder->checkCC($idForwarder); 
                    $checkforwarderContacts = $kForwarder->checkforwarderContacts($idForwarder); //default true
                    //status 2 for payment paid by ADMIN
                    $checkComapnyBankDet = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,2);
                    //status 1 for payment recieved by ADMIN
                    $checkComapnyBankDetils = $kForwarder->checkForwarderComleteBankInfomation($idForwarder,1); 
                    $kConfig = new cConfig; 
                    $updateLclServicesOBO = $kConfig->findForwarderOBOHaulageStauts($idForwarderUser); 
                    $updateTryItOut = $kConfig->findForwarderTryItOutStauts($idForwarderUser); //default true 
                    $kUploadBulkService = new cUploadBulkService(); 
                    $priceAwaitingForApproval = $kUploadBulkService->findStatusOfCostApprovalAlarm(__COST_AWAITING_APPROVAL__,$idForwarder); 
                    $waitingForwarderDataApproval = $kUploadBulkService->findStatusOfCostApprovalAlarm(__MANAGEMENT_COMPLETE_APPROVAL__,$idForwarder,__DATA_REVIEWED_BY_FORWARDER__);
                    $counter = 0;
                }
            }
             * 
             */
            
            $notificationFlagsAry = array();
            $notificationFlagsAry['iNumAckRedIcon'] = $iNumAckRedIcon;
            $notificationFlagsAry['waitingForwarderDataApproval'] = $waitingForwarderDataApproval;
            $notificationFlagsAry['notAcknowledged'] = $notAcknowledged;
            $notificationFlagsAry['strCountry'] = $strCountry;
            $notificationFlagsAry['iNumLabelSent'] = $iNumLabelSent;
            $notificationFlagsAry['iNumLabelNotSent'] = $iNumLabelNotSent;
            $notificationFlagsAry['iNumPendingReview'] = $iNumPendingReview;
            $notificationFlagsAry['iCountGmailError'] = $iCountGmailError;
            $notificationFlagsAry['iNumSellRateWithoutBuyRate'] = $iNumSellRateWithoutBuyRate; 
            $notificationFlagsAry['subFlag'] = $subFlag; 
            $notificationFlagsAry['iPayOverDueCount'] = $iPayOverDueCount;  
            $notificationFlagsAry['gmailSyncErrorAry'] = $gmailSyncErrorAry;


            unset($_SESSION['adminHeaderNoticationFlagsAry']);
            $_SESSION['adminHeaderNoticationFlagsAry'] = $notificationFlagsAry;
        } 

        $information  = ($iNumAckBlueIcon || $waitingForwarderDataApproval);
        $warning = ($iNumAckRedIcon || $strCountry || $iNumLabelSent || $iNumLabelNotSent || $iNumPendingReview || $iCountGmailError || $iNumSellRateWithoutBuyRate);

        if( $information || $warning  || $iPayOverDueCount)
        { 
            if(isset($_SESSION['message']))
            {
                $message = $_SESSION['message'];
                $style = 'style="display:none;"';
                ?>
                <script type="text/javascript">
                    $(document).ready(function()
                    { 
                        var a= $('#message').html(); 
                        if(a=='Show less')
                        {
                            $('#message').html('Show more');
                        }
                        else
                        { 
                            $('#message').html('Show less'); 
                        }

                    }); 
                </script>
                <?php
            }
            $count=0;
        ?>
        <div  class="show_message">
            <?php 
                if($warning) 
                {  
                    if($iNumAckRedIcon>0) 
                    {
                        ?>    
                        <div class="warning" id="NONACKNOWLEDGE">
                            <p><?php echo $iNumAckRedIcon." ".(($iNumAckRedIcon>1)?t($t_top_nav.'message28'):t($t_top_nav.'message29')) ;?> <a href="<?php echo __FORWARDER_BOOKING_URL__; ?>"><?=t($t_top_nav.'here');?></a>  </p>
                        </div> 
                <?php } if($iNumLabelSent>0) {  ?>
                        <div class="warning" id="LABELSENT">
                            <p>There are labels sent to shipper more than 1 day ago, not yet downloaded <a href="<?=__MANAGEMENT_OPRERATION_COUIER_LABEL_SEND__?>"><?=t($t_top_nav.'here');?></a> </p>
                        </div>
                <?php  } if($iNumLabelNotSent>0) { ?>
                        <div class="warning" id="LABELNOTSENT">
                            <p>There are booking more than 24 hours old, requiring labels <a href="<?php echo __MANAGEMENT_OPRERATION_COUIER_LABEL_NOT_SEND__?>"><?=t($t_top_nav.'here');?></a></p>
                        </div>
                <?php  } if($iNumPendingReview>0){ ?>
                    <div class="warning">
                        <p>There are courier agreements pending review <a href="<?php echo __MANAGEMENT_OPRERATION_COUIER_AGREEMENT__?>"><?=t($t_top_nav.'here');?></a></p>
                    </div> 
                <?php } ?> 
                <?php
                    if($offline)
                    {
                        ?>
                        <div class="warning">
                            <p><?=t($t_top_nav.'message1');?></p>
                        </div>
                        <?php if($checkComapnyProfile) {  
                        ?>
                            <div class="warning">
                                <p><?=t($t_top_nav.'message2');?> <a href="<?=__FORWARDER_COMPANY_INFORMATION_URL__?>"><?=t($t_top_nav.'here');?></a></p>
                            </div>
                        <?php } else if($checkComapnyEmails){ ?>
                            <div class="warning">
                                <p><?=t($t_top_nav.'message3');?> <a href="<?=__FORWARDER_COMPANY_PREFERENCES_URL__?>"><?=t($t_top_nav.'here');?></a></p>
                            </div>
                    <?php } else if($biLogoNotification){ ?>
                            <div class="warning">
                                <p><?=t($t_top_nav.'message4');?> <a href="<?=__FORWARDER_COMPANY_INFORMATION_URL__?>"><?=t($t_top_nav.'here');?></a></p>
                            </div> 
                    <?php } else if($checkCFS) { ?>
                        <div class="warning">
                            <p><?=t($t_top_nav.'message5');?> <a href="<?=__FORWARDER_CFS_LOCATIONS_URL__;?>"><?=t($t_top_nav.'here');?></a></p>
                        </div>
                    <?php } else if($checkCFS && !$checkPricingWareh){?>
                        <div class="warning">
                            <p><?=t($t_top_nav.'message6');?> <a href="<?=__FORWARDER_HOME_PAGE_URL__?>/LCLServicesBulk/"><?=t($t_top_nav.'here');?></a></p>
                        </div>
                    <?php } else if(!$checkComapnyProfile && $checkTnc){?>
                        <div class="warning">
                            <p><?=t($t_top_nav.'message7');?> <a href="<?=__BASE_URL__?>/T&C/"><?=t($t_top_nav.'here');?></a></p>
                        </div>
                    <?php }?>
                <?php } ?>
            <?php  } if($information){ ?>
                    <div class="information"> 
                        <?php if($iNumAckBlueIcon) { $count++; ?> 
                            <p id="NONACKNOWLEDGE"><?php echo $iNumAckBlueIcon." ".(($iNumAckBlueIcon>1)?t($t_top_nav.'message28'):t($t_top_nav.'message29')) ;?> <a href="<?php echo __FORWARDER_BOOKING_URL__; ?>"><?=t($t_top_nav.'here');?></a>  </p> 
                        <?php } ?>
                        <?php if($priceAwaitingForApproval && $subFlag){ 
                            $count++; 
                            if( $priceAwaitingForApproval && $waitingForwarderDataApproval) { ?>
                                <div id="foo" <?=$style?>>
                        <?php } ?>
                            <p><?=t($t_top_nav.'message2');?> <a href="<?=__MANAGEMENT_SERVICES_SUBMITTED_URL__?>"><?=t($t_top_nav.'here');?></a>  </p>
                            <?php if( $priceAwaitingForApproval && $waitingForwarderDataApproval ){ ?>
                            </div>
                        <?php } } ?>
                    </div>
            <?php } if($iPayOverDueCount>0){?>
                    <div class="information" id="OUTSTANDINGPAYMENT"><?=t($t_top_nav.'message4');?> <?php echo $iPayOverDueCount;  ?> <?=t($t_top_nav.'message5');?> <a href="<?=__MANAGEMENT_BILLING_URL__?>"><?=t($t_top_nav.'here');?></a></div>
            <?php } if($count>1){?>		
                <div class="show-more-less-alert" onclick="toggleMe();"><a id="message" href="javascript:void(0);"><?=t($t_top_nav.'show_less');?></a></div>
            <?php } ?> 
            </div>
            <?php
        }
    }
} 
function success_message_quick_quote_popup($iQuoteBookingSendFlag)
{
    $t_base="management/pendingTray/";
?>
    <div id="popup-bg"></div>
    <div id="popup-container">			
        <div class="popup" style="margin-top:50px;">
        <?php if($iQuoteBookingSendFlag==1){?>
            <h5><?=t($t_base.'title/quote_sent')?></h5>
            <p><?=t($t_base.'messages/quote_sent_msg')?></p>
        <?php }else if($iQuoteBookingSendFlag==2){?>
            <h5><?=t($t_base.'title/booking_sent')?></h5>
            <p><?=t($t_base.'messages/booking_sent_msg')?></p>
        <?php }?>
            <br class="clear-all" /> 
            <div class="btn-container" align="center">
                <a href="javascript:void(0);" onclick="location.reload();" id="cancel_button" class="button1" title="Cancel"><span><?=t($t_base.'fields/ok');?></span></a>
            </div>
        </div>
    </div>
<?php 
}
?>
