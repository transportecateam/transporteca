<?php
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

ini_set("soap.wsdl_cache_enabled", "0");
define('TRANSACTIONS_LOG_FILE', __APP_PATH__.'/logs/fedextransactions.log');  // Transactions log file

/**
 *  Print SOAP request and response
 */
define('Newline',"<br />");

function printSuccess($client, $response) {
    echo '<h2>Transaction Successful</h2>';  
    echo "\n";
    printRequestResponse($client);
}

function printRequestResponse($client){
	echo '<h2>Request</h2>' . "\n";
	echo '<pre>' . htmlspecialchars($client->__getLastRequest()). '</pre>';  
	echo "\n";
   
	echo '<h2>Response</h2>'. "\n";
	echo '<pre>' . htmlspecialchars($client->__getLastResponse()). '</pre>';
	echo "\n";
}

/**
 *  Print SOAP Fault
 */  
function printFault($exception, $client) {
    echo '<h2>Fault</h2>' . "<br>\n";                        
    echo "<b>Code:</b>{$exception->faultcode}<br>\n";
    echo "<b>String:</b>{$exception->faultstring}<br>\n";
    writeToLog($client);
    
    echo '<h2>Request</h2>' . "\n";
	echo '<pre>' . htmlspecialchars($client->__getLastRequest()). '</pre>';  
	echo "\n";
}

/**
 * SOAP request/response logging to a file
 */                                  
function writeToLog($client){  
if (!$logfile = fopen(TRANSACTIONS_LOG_FILE, "a"))
{
   error_func("Cannot open " . TRANSACTIONS_LOG_FILE . " file.\n", 0);
   exit(1);
}

fwrite($logfile, sprintf("\r%s:- %s",date("D M j G:i:s T Y"), $client->__getLastRequest(). "\n\n" . $client->__getLastResponse()));
}

/**
 * This section provides a convenient place to setup many commonly used variables
 * needed for the php sample code to function.
 */
function getProperty($var){
	if($var == 'key') Return __FEDEX_DEVELOPER_KEY__; 
	if($var == 'password') Return __FEDEX_DEVELOPER_PASSWORD__; 
		
	if($var == 'shipaccount') Return __FEDEX_ACCOUNT_NUMBER__; 
	if($var == 'billaccount') Return __FEDEX_ACCOUNT_NUMBER__; 
	if($var == 'dutyaccount') Return __FEDEX_ACCOUNT_NUMBER__; 
	if($var == 'freightaccount') Return __FEDEX_ACCOUNT_NUMBER__;  
	if($var == 'trackaccount') Return __FEDEX_ACCOUNT_NUMBER__; 

	if($var == 'meter') Return __FEDEX_METER_NUMBER__; 
		
	if($var == 'shiptimestamp') Return mktime(10, 0, 0, date("m"), date("d")+1, date("Y"));

	if($var == 'spodshipdate') Return '2013-05-21';
	if($var == 'serviceshipdate') Return '2013-04-26';

	if($var == 'readydate') Return '2010-05-31T08:44:07';
	if($var == 'closedate') Return date("Y-m-d");

	if($var == 'pickupdate') Return date("Y-m-d", mktime(8, 0, 0, date("m")  , date("d")+1, date("Y")));
	if($var == 'pickuptimestamp') Return mktime(8, 0, 0, date("m")  , date("d")+1, date("Y"));
	if($var == 'pickuplocationid') Return 'XXX';
	if($var == 'pickupconfirmationnumber') Return 'XXX';

	if($var == 'dispatchdate') Return date("Y-m-d", mktime(8, 0, 0, date("m")  , date("d")+1, date("Y")));
	if($var == 'dispatchlocationid') Return 'XXX';
	if($var == 'dispatchconfirmationnumber') Return 'XXX';		
	
	if($var == 'tag_readytimestamp') Return mktime(10, 0, 0, date("m"), date("d")+1, date("Y"));
	if($var == 'tag_latesttimestamp') Return mktime(20, 0, 0, date("m"), date("d")+1, date("Y"));	

	if($var == 'expirationdate') Return '2013-05-24';
	if($var == 'begindate') Return '2013-04-22';
	if($var == 'enddate') Return '2013-04-25';	

	if($var == 'trackingnumber') Return 'XXXTXN1234';

	if($var == 'hubid') Return 'XXX';
	
	if($var == 'jobid') Return 'XXX';

	if($var == 'searchlocationphonenumber') Return '5555555555';
			
	if($var == 'shipper') Return array(
		'Contact' => array(
			'PersonName' => 'Ajay Jha',
			'CompanyName' => 'Whiz Solutions',
			'PhoneNumber' => '1234567890'
		),
		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => 'Collierville',
			'StateOrProvinceCode' => 'TN',
			'PostalCode' => '38017',
			'CountryCode' => 'US',
			'Residential' => 1
		)
	);
	if($var == 'recipient') Return array(
		'Contact' => array(
			'PersonName' => 'Recipient Name',
			'CompanyName' => 'Recipient Company Name',
			'PhoneNumber' => '1234567890'
		),
		'Address' => array(
			'StreetLines' => array('Address Line 1'),
			'City' => 'Herndon',
			'StateOrProvinceCode' => 'VA',
			'PostalCode' => '20171',
			'CountryCode' => 'US',
			'Residential' => 1
		)
	);	

	if($var == 'address1') Return array(
		'StreetLines' => array('10 Fed Ex Pkwy'),
		'City' => 'Memphis',
		'StateOrProvinceCode' => 'TN',
		'PostalCode' => '38115',
		'CountryCode' => 'US'
    );
	if($var == 'address2') Return array(
		'StreetLines' => array('13450 Farmcrest Ct'),
		'City' => 'Herndon',
		'StateOrProvinceCode' => 'VA',
		'PostalCode' => '20171',
		'CountryCode' => 'US'
	);					  
	if($var == 'searchlocationsaddress') Return array(
		'StreetLines'=> array('240 Central Park S'),
		'City'=>'Austin',
		'StateOrProvinceCode'=>'TX',
		'PostalCode'=>'78701',
		'CountryCode'=>'US'
	);
									  
	if($var == 'shippingchargespayment') Return array(
		'PaymentType' => 'SENDER',
		'Payor' => array(
			'ResponsibleParty' => array(
				'AccountNumber' => getProperty('billaccount'),
				'Contact' => null,
				'Address' => array('CountryCode' => 'US')
			)
		)
	);	
	if($var == 'freightbilling') Return array(
		'Contact'=>array(
			'ContactId' => 'freight1',
			'PersonName' => 'Big Shipper',
			'Title' => 'Manager',
			'CompanyName' => 'Freight Shipper Co',
			'PhoneNumber' => '1234567890'
		),
		'Address'=>array(
			'StreetLines'=>array(
				'1202 Chalet Ln', 
				'Do Not Delete - Test Account'
			),
			'City' =>'Harrison',
			'StateOrProvinceCode' => 'AR',
			'PostalCode' => '72601-6353',
			'CountryCode' => 'US'
			)
	);
}

function setEndpoint($var){
	if($var == 'changeEndpoint') Return false;
}

function printNotifications($notes)
{
	if(!empty($notes))
	{
		foreach($notes as $noteKey => $note){
			if(is_string($note)){    
	            echo $noteKey . ': ' . $note . Newline;
	        }
	        else{
	        	printNotifications($note);
	        }
		}
		echo Newline;
	}	
}

function printError($client, $response)
{
    echo '<h2>Error returned in processing transaction</h2>';
    echo "\n";
    printNotifications($response -> Notifications);
    printRequestResponse($client, $response);
}

function trackDetails($details, $spacer){
	foreach($details as $key => $value){
		if(is_array($value) || is_object($value)){
        	$newSpacer = $spacer. '&nbsp;&nbsp;&nbsp;&nbsp;';
    		echo '<tr><td>'. $spacer . $key.'</td><td>&nbsp;</td></tr>';
    		trackDetails($value, $newSpacer);
    	}elseif(empty($value)){
    		echo '<tr><td>'.$spacer. $key .'</td><td>'.$value.'</td></tr>';
    	}else{
    		echo '<tr><td>'.$spacer. $key .'</td><td>'.$value.'</td></tr>';
    	}
    }
}

function display_courier_service_demo_form($kCourierService,$iProviderType=1)
{
    if(!empty($kCourierService->arErrorMessages))
    {
        $formId = 'calculate_courier_service_form';
        $szValidationErrorKey = '';
        foreach($kCourierService->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                 $("#"+'<?php echo $errorKey ; ?>').addClass('red_border');
            </script>
            <?php
        }
    }  
    $kConfig = new cConfig();
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true); 
    
    if($kCourierService->iSuccessMessage>0)
    {
        if($kCourierService->iSuccessMessage==1)
        {
            $szColor = 'style="color:green;" ';
        }
        else 
        {
            $szColor = 'style="color:red;" ';
        }
        ?>
        <div id="courier_service_success_div" <?php echo $szColor;?>>
            <?php echo $kCourierService->szApiResponseText ; ?>
        </div>
        <?php
    }
    
    if(!empty($_POST['getCourierRateAry']['iProviderType']))
    {
        $iProviderType = $_POST['getCourierRateAry']['iProviderType'] ;
    }
?>  
<form id="calculate_courier_service_form" name="calculate_courier_service_form" method="post">
    <table class="format-2" style="width:90%;" cellpadding="5">
        <tr>
            <td colspan="2"><strong>Shipper</strong></td>
            <td colspan="2"><strong>Consignee</strong></td> 
        </tr>
        <tr>
            <td style="width:15%;">First Name</td>
            <td style="width:35%;"><input type="text" name="getCourierRateAry[szFirstName]" id="szFirstName" value="<?php echo $_POST['getCourierRateAry']['szFirstName']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
            <td style="width:15%;">First Name</td>
            <td style="width:35%;"><input type="text" name="getCourierRateAry[szSFirstName]" id="szSFirstName" value="<?php echo $_POST['getCourierRateAry']['szSFirstName']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><input type="text" name="getCourierRateAry[szLastName]" id="szLastName" value="<?php echo $_POST['getCourierRateAry']['szLastName']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
            <td>Last Name</td>
            <td><input type="text" name="getCourierRateAry[szSLastName]" id="szSLastName" value="<?php echo $_POST['getCourierRateAry']['szSLastName']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
        </tr>
        <tr>
            <td>Address 1</td>
            <td><input type="text" name="getCourierRateAry[szAddress1]" id="szAddress1" value="<?php echo $_POST['getCourierRateAry']['szAddress1']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
            <td>Address 1</td>
            <td><input type="text" name="getCourierRateAry[szSAddress1]" id="szSAddress1" value="<?php echo $_POST['getCourierRateAry']['szSAddress1']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
         </tr>
         <tr>
            <td>Address 2</td>
            <td><input type="text" name="getCourierRateAry[szAddress2]" id="szAddress2" value="<?php echo $_POST['getCourierRateAry']['szAddress2']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
            <td>Address 2</td>
            <td><input type="text" name="getCourierRateAry[szSAddress2]" id="szSAddress2" value="<?php echo $_POST['getCourierRateAry']['szSAddress2']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
         </tr>
         <tr>
            <td>Postcode</td>
            <td><input type="text" name="getCourierRateAry[szPostCode]" id="szPostcode" value="<?php echo $_POST['getCourierRateAry']['szPostCode']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"></td>
            <td>Postcode</td>
            <td><input type="text" name="getCourierRateAry[szSPostCode]" id="szSPostcode" value="<?php echo $_POST['getCourierRateAry']['szSPostCode']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"></td>
         </tr>
         <tr>
            <td>City</td>
            <td><input type="text" name="getCourierRateAry[szCity]" id="szCity" value="<?php echo $_POST['getCourierRateAry']['szCity']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
            <td>City</td>
            <td><input type="text" name="getCourierRateAry[szSCity]" id="szSCity" value="<?php echo $_POST['getCourierRateAry']['szSCity']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></td>
         </tr>
         <tr>
            <td>State/Province</td>
            <td><input type="text" name="getCourierRateAry[szState]" id="szState" value="<?php echo $_POST['getCourierRateAry']['szState']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"></td>
            <td>State/Province</td>
            <td><input type="text" name="getCourierRateAry[szSState]" id="szSState" value="<?php echo $_POST['getCourierRateAry']['szSState']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"></td>
         </tr>
         <tr>
            <td>Country</td>
            <td>
                <select onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="getCourierRateAry[szCountry]" id="szCountry" >
                    <option value="">Select</option>
                <?php
                    if(!empty($allCountriesArr))
                    {
                        foreach($allCountriesArr as $allCountriesArrs)
                        {
                            ?>
                            <option value="<?php echo $allCountriesArrs['szCountryISO']; ?>" <?php echo (($_POST['getCourierRateAry']['szCountry']==$allCountriesArrs['szCountryISO'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                            <?php
                        }
                    }
                 ?>
                </select>
            </td>
            <td>Country</td>
            <td>
                <select onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="getCourierRateAry[szSCountry]" id="szSCountry" >
                    <option value="">Select</option>
                <?php
                    if(!empty($allCountriesArr))
                    {
                        foreach($allCountriesArr as $allCountriesArrs)
                        {
                            ?>
                            <option value="<?php echo $allCountriesArrs['szCountryISO']; ?>" <?php echo (($_POST['getCourierRateAry']['szSCountry']==$allCountriesArrs['szCountryISO'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                            <?php
                        }
                    }
                 ?>
                </select>
            </td>
         </tr>  
         <tr style="width:90%;">
             <td colspan="4" style="text-align:left;padding-left:10px;"><strong>Cargo Details</strong></td> 
        </tr>
        <tr>
            <td>Length</td>
            <td><input type="text" name="getCourierRateAry[fCargoLength]" id="fCargoLength" value="<?php echo $_POST['getCourierRateAry']['fCargoLength']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"> CM</td>
            <td>Width</td>
            <td><input type="text" name="getCourierRateAry[fCargoWidth]" id="fCargoWidth" value="<?php echo $_POST['getCourierRateAry']['fCargoWidth']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"> CM</td>
        </tr>
        <tr>
            <td>Height</td>
            <td><input type="text" name="getCourierRateAry[fCargoHeight]" id="fCargoHeight" value="<?php echo $_POST['getCourierRateAry']['fCargoHeight']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"> CM</td>
            <td>Weight</td>
            <td><input type="text" name="getCourierRateAry[fCargoWeight]" id="fCargoWeight" value="<?php echo $_POST['getCourierRateAry']['fCargoWeight']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"> KG</td>
        </tr>
        <?php if($iProviderType==1){ ?>
            <tr style="width:90%;">
                <td colspan="4" style="text-align:left;padding-left:10px;"><strong>Shipment Details</strong></td> 
            </tr> 
            <tr>
                <td>Service Type</td>
                <td>
                    <select name="getCourierRateAry[szServiceType]" id="szServiceType" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="">All</option>
                        <option value="INTERNATIONAL_PRIORITY" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='INTERNATIONAL_PRIORITY')?'selected':''); ?>>INTERNATIONAL_PRIORITY</option>
                        <option value="INTERNATIONAL_FIRST" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='INTERNATIONAL_FIRST')?'selected':''); ?>>INTERNATIONAL_FIRST</option>
                        <option value="INTERNATIONAL_ECONOMY" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='INTERNATIONAL_ECONOMY')?'selected':''); ?>>INTERNATIONAL_ECONOMY</option>
                        <option value="EUROPE_FIRST_INTERNATIONAL_PRIORITY" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='EUROPE_FIRST_INTERNATIONAL_PRIORITY')?'selected':''); ?>>EUROPE_FIRST_INTERNATIONAL_PRIORITY</option>
                        <option value="FEDEX_1_DAY_FREIGHT" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='FEDEX_1_DAY_FREIGHT')?'selected':''); ?>>FEDEX_1_DAY_FREIGHT</option>
                        <option value="FEDEX_2_DAY_FREIGHT" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='FEDEX_2_DAY_FREIGHT')?'selected':''); ?>>FEDEX_2_DAY_FREIGHT</option>
                        <option value="FEDEX_3_DAY_FREIGHT" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='FEDEX_3_DAY_FREIGHT')?'selected':''); ?>>FEDEX_3_DAY_FREIGHT</option>
                        <option value="FEDEX_FIRST_FREIGHT" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='FEDEX_FIRST_FREIGHT')?'selected':''); ?>>FEDEX_FIRST_FREIGHT</option>
                        <option value="FEDEX_GROUND" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='FEDEX_GROUND')?'selected':''); ?>>FEDEX_GROUND</option>
                        <option value="FIRST_OVERNIGHT" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='FIRST_OVERNIGHT')?'selected':''); ?>>FIRST_OVERNIGHT</option>
                        <option value="STANDARD_OVERNIGHT" <?php echo (($_POST['getCourierRateAry']['szServiceType']=='STANDARD_OVERNIGHT')?'selected':''); ?>>STANDARD_OVERNIGHT</option>
                   </select>
                </td>
                <td>Package Type</td>
                <td>
                    <select name="getCourierRateAry[szPackageType]" id="szPackageType" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="YOUR_PACKAGING" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='YOUR_PACKAGING')?'selected':''); ?>>YOUR_PACKAGING</option>
                        <option value="FEDEX_10KG_BOX" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='FEDEX_10KG_BOX')?'selected':''); ?>>FEDEX_10KG_BOX</option>
                        <option value="FEDEX_25KG_BOX" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='FEDEX_25KG_BOX')?'selected':''); ?>>FEDEX_25KG_BOX</option>
                        <option value="FEDEX_BOX" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='FEDEX_BOX')?'selected':''); ?>>FEDEX_BOX</option>
                        <option value="FEDEX_ENVELOPE" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='FEDEX_ENVELOPE')?'selected':''); ?>>FEDEX_ENVELOPE</option>
                        <option value="FEDEX_PAK" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='FEDEX_PAK')?'selected':''); ?>>FEDEX_PAK</option>
                        <option value="FEDEX_TUBE" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='FEDEX_GROUND')?'selected':''); ?>>FEDEX_TUBE</option>
                        <option value="INDIVIDUAL_PACKAGES" <?php echo (($_POST['getCourierRateAry']['szPackageType']=='INDIVIDUAL_PACKAGES')?'selected':''); ?>>INDIVIDUAL_PACKAGES</option>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <tr style="width:90%;">
            <td colspan="4" style="text-align:center;padding-left:10px;">
                <input type="hidden" name="getCourierRateAry[iProviderType]" id="iProviderType" value="<?php echo $iProviderType; ?>">
                <input type="button" name="getCourierRateAry[szSubmit]" value="Get Details" onclick="submit_courier_form();"> 
            </td> 
        </tr>
    </table> 
</form>
<?php

}

function addDisplayCouierProviderName($kCourierServices,$idCourierProvider=0)
{
    $t_base="management/providerProduct/"; 
    $providerProductArr=$kCourierServices->getCourierProviderList();
    $counter=count($providerProductArr);
?> 
    <div id="insurance_couier_provider_list_container">  
        <div>
            <h4><strong><?php echo t($t_base.'title/curier_service_proivders');?></strong></h4>
            <div class="clearfix courier-provider courier-provider-scroll"> 
                <table cellpadding="0" cellspacing="0" border="0" class="format-2" width="100%" id="insurance_couier_provider_list_table">
                    <tr>
                        <td width="33%" align="left" class="firsttdnoborder"><strong><?php echo t($t_base.'fields/provider');?></strong></td>
                        <td width="33%" align="left"><strong><?php echo t($t_base.'fields/display_name');?></strong></td>
                        <td width="33%" style="text-align:right;"><strong><?php echo t($t_base.'fields/booking_price');?></strong></td>
                    </tr>
                    <?php
                        if(!empty($providerProductArr))
                        {
                            foreach($providerProductArr as $providerProductArrs)
                            { 
                                ?>
                                <tr id="courier_provider_<?php echo $providerProductArrs['id'];?>" <?php if($providerProductArrs['id']==$idCourierProvider){?> style="background:#DBDBDB;cursor:pointer;" <?php }?> onclick="showEditDeleteDataoFProvider('<?php echo $providerProductArrs['id'];?>','<?=t($t_base.'fields/edit');?>','<?=t($t_base.'fields/save');?>','<?=t($t_base.'fields/cancel');?>')">
                                    <td><?php echo $providerProductArrs['szProviderName']?></td>
                                    <td><?php echo $providerProductArrs['szName']?></td>
                                    <td style="text-align:right;"><?php echo $providerProductArrs['szCurrency']." ".number_format((float)$providerProductArrs['fPrice'],2);?></td>
                                </tr>
                             <?php		
                            } 		 
                        }
                        else
                        { ?> 
                            <tr> 
                                <td colspan="3"><h4><b><?=t($t_base.'fields/no_courier_provider_found');?></b></h4></td>
                             </tr>  
                    <?php } ?> 
                </table>
            </div>
        </div>
    </div>   
    <div class="clear-all"></div>
    <br>
    <div id="courier_provider_product">
    <?php
        echo display_courier_provider_form();
    ?>	
    </div>   
    <div style="float: right;" class="btn-container">   
        <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_provider_proudct"><span style="min-width:50px;"><?=t($t_base.'fields/edit');?></span></a>
         <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_provider_proudct" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/cancel');?></span></a>
    </div>  
<?php
}

function display_courier_provider_form($kCourierServices=false)
{
    $t_base="management/providerProduct/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                    $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    }
    $kConfig= new cConfig();
    $currencyArr=$kConfig->getBookingCurrency(false,true);
   // $field_width = " style='width: 97%; '";
    ?>
    <div id="courier_provider_form" style="width:100%;">
        <form action="javascript:void(0);" name="courier_provider_product_form" method="post" id="courier_provider_product_form">
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="font-12" width="49%" align="left"><?php echo t($t_base.'fields/company_display_name');?></td>
                    <td class="font-12" width="29%" style="text-align:left;"><?php echo t($t_base.'fields/booking_price_usd');?></td>
                    <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/currency');?></td>
                </tr>
                <tr>
                    <td><input type="text" <?php echo $field_width; ?> class="courier-service-provider-input-fields" disabled="disabled" name="addProviderAry[szName]" id="szName" value="<?php echo $_POST['addProviderAry']['szName'];?>" onblur="check_form_field_empty_standard(this.form.id,this.id);checkProviderData();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" /></td>
                    <td style="padding:0 4px;"><input type="text" <?php echo $field_width; ?> class="courier-service-provider-input-fields" disabled="disabled" name="addProviderAry[fPrice]" id="fPrice" value="<?php echo $_POST['addProviderAry']['fPrice'];?>" onblur="check_form_field_empty_standard(this.form.id,this.id);checkProviderData();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" /></td>
                    <td>
                        <select name="addProviderAry[iBookingPriceCurrency]" class="courier-service-provider-input-fields" disabled="disabled" id="iBookingPriceCurrency" <?php echo $field_width; ?> onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <?php
                            if(!empty($currencyArr))
                            {
                                    foreach($currencyArr as $currencyArrs)
                                    {?>
                                            <option value="<?php echo $currencyArrs['id'];?>" <?php if($_POST['addProviderAry']['iBookingPriceCurrency']==$currencyArrs['id']){?> selected <?php }?>><?php echo $currencyArrs['szCurrency'];?></option>
                                    <?php					
                                    }

                            }
                        ?> 
                        </select>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="addProviderAry[id]" id="id" value="<?php echo $_POST['addProviderAry']['id'];?>">
        </form>
    </div>
	<?php
}
function addDisplayCouierProviderProductName($kCourierServices,$idCourierProvider=0,$idCourierProviderProduct=0)
{
	$t_base="management/providerProduct/";
	
	if((int)$idCourierProvider>0)
        {
            $providerProductListArr=$kCourierServices->getCourierProviderProductList($idCourierProvider);
        } 
	$counter=count($providerProductListArr);
	$_POST['addProviderProductAry']['idCourierProvider']=$idCourierProvider;
        
	?>
	<div id="couier_provider_product_list_container">   
            <h4><strong><?php echo t($t_base.'title/courier_product');?></strong></h4>
            <div class="clearfix courier-provider courier-provider-scroll">
                <table cellpadding="0" cellspacing="0" class="format-2" width="100%" id="couier_provider_product_list_table">
                    <tr>
                        <td width="23%" align="left" class="firsttdnoborder"><strong><?php echo t($t_base.'fields/product_name');?></strong></td>
                        <td width="15%" style="text-align:left;"><strong><?php echo t($t_base.'fields/api_code');?></strong></td>
                        <td width="18%" style="text-align:left;"><strong><?php echo t($t_base.'fields/packing');?></strong></td>
                        <td width="15%" style="text-align:left;"><strong><?php echo t($t_base.'fields/gruop');?></strong></td>
                        <td width="15%" style="text-align:left;"><strong><?php echo t($t_base.'fields/days');?></strong></td>
                        <td width="15%" style="text-align:left;"><strong><?php echo t($t_base.'fields/days')." ".t($t_base.'fields/min');?></strong></td>
                    </tr>
                    <?php 
                    if(!empty($providerProductListArr))
                    {
                        foreach($providerProductListArr as $providerProductListArrs)
                        {?>
                            <tr id="courier_provider_right_<?php echo $providerProductListArrs['id'];?>" <?php if($providerProductListArrs['id']==$idCourierProviderProduct){?> style="background:#DBDBDB;cursor:pointer;" <?php }?> onclick="showEditDeleteDataoFProviderProduct('<?php echo $providerProductListArrs['id'];?>','<?php echo $providerProductListArrs['idCourierProvider'];?>','<?=t($t_base.'fields/edit');?>','<?=t($t_base.'fields/save');?>','<?=t($t_base.'fields/cancel');?>')">
                                <td width="23%" align="left"><?php echo $providerProductListArrs['szName']?></td>
                                <td width="15%" style="text-align:left;" title="<?php echo $providerProductListArrs['szApiCode']; ?>"><?php echo returnLimitData($providerProductListArrs['szApiCode'],15);?></td>
                                <td width="18%" style="text-align:left;"><?php echo $providerProductListArrs['szPacking']?></td>
                                <td width="15%" style="text-align:left;"><?php echo $providerProductListArrs['szGroupName']?></td>
                                <td width="15%" style="text-align:left;"><?php echo $providerProductListArrs['iDaysStd']?></td>
                                <td width="15%" style="text-align:left;"><?php echo $providerProductListArrs['iDaysMin']?></td>
                            </tr>
                        <?php		
                        } 	 
                    }
                    else
                    {?> 
                        <tr> 
                            <td colspan="6" style="text-align:center;"><h4><b><?=t($t_base.'fields/no_courier_provider_found');?></b></h4></td>
                         </tr>
                    <?php  } ?>
                </table> 
            </div> 
        </div>   
        <div class="clear-all"></div>
        <br>
        <div id="courier_provider_product_right">
            <?php
                echo display_courier_provider_product_form($kCourierServices);
            ?>	
        </div>  
        <div style="float: right;" class="btn-container">  
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_provider_proudct_right"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
             <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_provider_proudct_right" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/delete');?></span></a>
        </div>
	
	<?php
}

function display_courier_provider_product_form($kCourierServices)
{
    $t_base="management/providerProduct/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    } 
    $packingListArr=$kCourierServices->selectProviderPackingList();
    
    $courierProductGroup=$kCourierServices->getCourierProductGroupByidProvider($_POST['addProviderProductAry']['idCourierProvider']);
   // $field_width = " style='width: 97%; '";
	
    ?>
		<div id="courier_provider_form" style="width:100%;">
			<form action="javascript:void(0);" name="courierProviderProductRightForm" method="post" id="courierProviderProductRightForm">
                            <table cellpadding="0" cellspacing="1" width="100%">
                                <tr>
                                    <td class="font-12" width="23%" style="text-align:left;"><?php echo t($t_base.'fields/product_name');?></td>
                                    <td class="font-12" width="15%" style="text-align:left;"><?php echo t($t_base.'fields/api_code');?></td>
                                    <td class="font-12" width="17%" style="text-align:left;"><?php echo t($t_base.'fields/packing');?></td>
                                    <td class="font-12" width="15%" style="text-align:left;"><?php echo t($t_base.'fields/gruop');?></td>
                                    <td class="font-12" width="15%" style="text-align:left;"><?php echo t($t_base.'fields/days')." ".t($t_base.'fields/std');?></td>
                                    <td class="font-12" width="15%" style="text-align:left;"><?php echo t($t_base.'fields/days')." ".t($t_base.'fields/min');?></td>
                		</tr>
                		<tr>
                                    <td><input type="text" <?php echo $field_width; ?> name="addProviderProductAry[szProductName]" id="szProductName" value="<?php echo $_POST['addProviderProductAry']['szProductName'];?>" onkeyup="checkProviderProductData();" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);"></td>
                                    <td style="padding:0 4px;"><input type="text" <?php echo $field_width; ?> name="addProviderProductAry[szApiCode]" id="szApiCode" value="<?php echo $_POST['addProviderProductAry']['szApiCode'];?>" onkeyup="checkProviderProductData();" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" /></td>
                                    <td style="padding:0 4px 0 0;">
                                        <select id="idPacking" <?php echo $field_width; ?> name="addProviderProductAry[idPacking]" onchange="checkProviderProductData();" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                                            <option value=''>Select</option>
                                            <?php 
                				if(!empty($packingListArr))
                				{
                                                    foreach($packingListArr as $packingListArrs)
                                                    {?>
                                                            <option value='<?php echo $packingListArrs['id']?>' <?php if($_POST['addProviderProductAry']['idPacking']==$packingListArrs['id']){?> selected <?php }?>><?php echo $packingListArrs['szPacking']?></option>
                                                    <?php
                                                    }
                				}
                                            ?>
                			</select>
                                    </td>
                                    <td>
                                        <select id="idGroup" <?php echo $field_width; ?> name="addProviderProductAry[idGroup]" onchange="checkProviderProductData();" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                                            <option value=''>Select</option>
                                            <?php 
                				if(!empty($courierProductGroup))
                				{
                                                    foreach($courierProductGroup as $courierProductGroups)
                                                    {?>
                                                            <option value='<?php echo $courierProductGroups['id']?>' <?php if($_POST['addProviderProductAry']['idGroup']==$courierProductGroups['id']){?> selected <?php }?>><?php echo $courierProductGroups['szName']?></option>
                                                    <?php
                                                    }
                				}
                                            ?>
                			</select>
                                    </td>
                                    <td style="padding:0 4px;"><input type="text" <?php echo $field_width; ?> name="addProviderProductAry[iDaysStd]" id="iDaysStd" value="<?php echo $_POST['addProviderProductAry']['iDaysStd'];?>" onkeyup="checkProviderProductData();" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" /></td>
                                    <td style="padding:0 4px;"><input type="text" <?php echo $field_width; ?> name="addProviderProductAry[iDaysMin]" id="iDaysMin" value="<?php echo $_POST['addProviderProductAry']['iDaysMin'];?>" onkeyup="checkProviderProductData();" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" /></td>
                                    
                		</tr>
				</table>
				<input type="hidden" name="addProviderProductAry[id]" id="id" value="<?php echo $_POST['addProviderProductAry']['id'];?>">
				<input type="hidden" name="addProviderProductAry[idCourierProvider]" id="idCourierProvider" value="<?php echo $_POST['addProviderProductAry']['idCourierProvider'];?>">
			</form>
		</div>
	<?php
}

function courierExcludedTradesList()
{ 
    $t_base="management/providerProduct/";  
    $kCourierServiceNew = new cCourierServices();
    $excludedTradeServiceAry = array();
    $excludedTradeServiceAry = $kCourierServiceNew->getAllExcludedTrades();
           
    ?> 
    <script type="text/javascript">
        $(function () {
            $('#couier_excluded_trades_list_table').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    $(".selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        console.log(tr_id);
                        var tr_id_ary = tr_id.split("courier_excluded_trades_");
                        console.log(tr_id_ary);
                        var idExcludedService = parseInt(tr_id_ary[1]);
                        
                        if(!isNaN(idExcludedService))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                        console.log(ctr_1);
                    });  
                    if(res_ary_new.length)
                    {
                        var excluded_trade_id = res_ary_new.join(';'); 
                        $("#idExcludedServiceType").attr('value',excluded_trade_id);
                        
                        if(ctr_1==1)
                        {
                            $("#add_edit_excluded_trades_data").unbind("click");
                            $("#add_edit_excluded_trades_data").click(function(){
                                update_courier_excluded_trades(excluded_trade_id,'EDIT_EXCLUDED_TRADES');
                            });
                            $("#add_edit_excluded_trades_data").attr('style','opacity:1');
                            $("#add_edit_excluded_trades_data_span").html('Edit');
                        }
                        else
                        {
                            $("#add_edit_excluded_trades_data").unbind("click"); 
                            $("#add_edit_excluded_trades_data").attr('style','opacity:0.4');
                            $("#add_edit_excluded_trades_data_span").html('Add');
                        }
                        $("#detete_excluded_trades_data").unbind("click");
                        $("#detete_excluded_trades_data").click(function(){
                            update_courier_excluded_trades(excluded_trade_id,'DELETE_MULTIPLE_EXCLUDED_TRADES_CONFIRM');
                        });
                        $("#detete_excluded_trades_data").attr("style",'opacity:1'); 
                    }
                    else
                    {
                        var szModeOfTransIds = '';
                        $("#idExcludedServiceType").attr('value',szModeOfTransIds);
                        $("#detete_excluded_trades_data").unbind("click");
                        $("#detete_excluded_trades_data").attr("style",'opacity:0.4');
                    } 
                }
            });
        })
    </script> 
    <div id="couier_excluded_trade_list_container">    
        <h4><strong><?php echo t($t_base.'title/excluded_service');?></strong></h4>
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:450px;"> 
            <table cellpadding="5" cellspacing="0" class="format-2" width="100%" id="couier_excluded_trades_list_table">
                <tr>
                    <td width="20%" style="text-align:left;" class="firsttdnoborder"><strong><?php echo t($t_base.'fields/provider');?></strong></td> 
                    <td width="20%" align="left"><strong><?php echo t($t_base.'fields/forwarder');?></strong></td>
                    <td width="20%" style="text-align:left;"><strong><?php echo t($t_base.'fields/from')." ".t($t_base.'fields/country');?></strong></td>
                    <td width="20%" style="text-align:left;"><strong><?php echo t($t_base.'fields/to')." ".t($t_base.'fields/country');?></strong></td>
                    <td width="20%" style="text-align:left;"><strong><?php echo t($t_base.'fields/type');?></strong></td>
                </tr>
                <?php
                    if(!empty($excludedTradeServiceAry))
                    {
                        foreach($excludedTradeServiceAry as $excludedTradeServiceArys)
                        { 
                            $szCustomerType = '';
                            if($excludedTradeServiceArys['iCustomerType']==1)
                            {
                                $szCustomerType = 'Business only';
                            }
                            else if($excludedTradeServiceArys['iCustomerType']==2)
                            {
                                $szCustomerType = 'Private only';
                            }
                            else if($excludedTradeServiceArys['iCustomerType']==3)
                            {
                                $szCustomerType = 'Business & Private';
                            }
                            ?>
                            <tr class="excluded-service-tr" id="courier_excluded_trades_<?php echo $excludedTradeServiceArys['id'];?>" <?php if($excludedTradeServiceArys['id']==$idCargoLimit){?> style="background:#DBDBDB;cursor:pointer;" <?php }?> onclick="show_edit_excluded_trades_list('<?php echo $excludedTradeServiceArys['id'];?>');">
                                <td><?php echo $excludedTradeServiceArys['szCourierProviderName']?></td>
                                <td><?php echo $excludedTradeServiceArys['szForwarderDisName']." (".$excludedTradeServiceArys['szForwarderAlies'].") ";?></td>
                                <td><?php echo $excludedTradeServiceArys['szOriginCountry']; ?></td>
                                <td><?php echo $excludedTradeServiceArys['szDestinationCountry']; ?></td>
                                <td><?php echo $szCustomerType; ?></td>
                            </tr>
                        <?php		
                        } 		 
                    }  ?> 
                </table>
            </div> 
    </div>   
    <div class="clear-all"></div>
    <br> 
    <div id="courier_excluded_trades_addedit_data">
        <?php
           echo display_courier_excluded_trades_add_edit_form($kCourierServices);
        ?>	
    </div>   
    <?php 
}
function display_courier_excluded_trades_add_edit_form($kCourierServices,$courierExcludedTradesAry=array())
{ 
    $t_base="management/providerProduct/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            if($errorKey=='szSpecialError')
            {
                $szSpecialErrorMessage = $errorValue ;
            }
            else
            {
                ?>
                <script type="text/javascript">
                    $("#<?php echo $errorKey?>").addClass('red_border');
                </script>
                <?php 
            }
        }
    } 
    
    $kCourierServices_new  = new cCourierServices();
    $kForwarder = new cForwarder();
    $kConfig = new cConfig(); 
    
    $providerProductArr = array();
    $providerProductArr = $kCourierServices_new->getCourierProviderList();
    
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(); 
    
    //$forwarderListAry = sortArray($forwarderListAry,'szDisplayName');
    
    $allCountryAry = array();
    $allCountryAry = $kConfig->getAllCountries(true); 
     
    if(!empty($courierExcludedTradesAry))
    {
        $idCourierProvider = $courierExcludedTradesAry['idCourierProvider'];
        $idForwarder = $courierExcludedTradesAry['idForwarder'];
        $idOriginCountry = $courierExcludedTradesAry['idOriginCountry'];
        $idDestinationCountry = $courierExcludedTradesAry['idDestinationCountry'];
        $idExludedTrades = $courierExcludedTradesAry['id'];
        $iCustomerType = $courierExcludedTradesAry['iCustomerType'];
    }
    else
    {
        $idCourierProvider = $_POST['addCourierExcludedTradesAry']['idCourierProvider'] ;
        $idForwarder = $_POST['addCourierExcludedTradesAry']['idForwarder'] ;
        $idOriginCountry = $_POST['addCourierExcludedTradesAry']['idOriginCountry'] ;
        $idDestinationCountry = $_POST['addCourierExcludedTradesAry']['idDestinationCountry'] ; 
        $idExludedTrades = $_POST['addCourierExcludedTradesAry']['idExludedTrades'];  
        $iCustomerType = $_POST['addCourierExcludedTradesAry']['iCustomerType']; 
    } 
    if($idExludedTrades>0)
    {
        $regionArr = array();
    }
    else
    {
        $regionArr = $kCourierServices_new->getAllRegion();
    }
    
    $excludedTradeServiceAry = array();
    $excludedTradeServiceAry = $kCourierServices_new->getAllExcludedTrades(); 
    
    $forwarderListAry = array();
    if($idCourierProvider>0)
    {
        $data = array();
        $data['idCourierProvider'] = $idCourierProvider;
        $forwarderListAry = $kCourierServices_new->getAllCourierAgreement($data,true); 
    }
    
    $customerTypeAry = array();
    $customerTypeAry[0]['id'] = 1;
    $customerTypeAry[0]['szType'] = 'Business only';
    $customerTypeAry[1]['id'] = 2;
    $customerTypeAry[1]['szType'] = 'Private only';
    $customerTypeAry[2]['id'] = 3;
    $customerTypeAry[2]['szType'] = 'Business & Private';
?>             
<script type="text/javascript">
    
    var serviceListAry = new Array();
    <?php
        if(!empty($excludedTradeServiceAry))
        { /*
            foreach($excludedTradeServiceAry as $excludedTradeServiceArys)
            {
                $szServiceString = $excludedTradeServiceArys['idCourierProvider']."_".$excludedTradeServiceArys['idForwarder']."_".$excludedTradeServiceArys['idOriginCountry']."_".$excludedTradeServiceArys['idDestinationCountry'];
                ?>
                    //serviceListAry['<?php echo $excludedTradeServiceArys['id']; ?>'] = '<?php echo $szServiceString; ?>';
                <?php
            } */
        }
    ?>  
        function check_duplicate_values_1()
        {
            var idCourierProvider = $("#courierExcludeTradesForm #idCourierProvider").val(); 
            var idForwarder = $("#idForwarder").val();
            var idOriginCountry = $("#idOriginCountry").val();
            var idDestinationCountry = $("#idDestinationCountry").val(); 
            
            var service_tring = idCourierProvider+"_"+idForwarder+"_"+idOriginCountry+"_"+idDestinationCountry ;
              
            if ($.inArray(service_tring, serviceListAry) > -1)
            { 
                return 2;
            } 
            else
            {
                return 1;
            }
        } 
</script> 
<div style="width:100%;">
    <?php if(!empty($szSpecialErrorMessage)){ ?>
        <p style="font-style:italic;color:red;font-size:14px;"> <?php echo $szSpecialErrorMessage; ?></p>
    <?php  }  ?>
    <form action="javascript:void(0);" name="courierExcludeTradesForm" method="post" id="courierExcludeTradesForm">
        <table cellpadding="0" cellspacing="1" width="100%">
            <tr>  
                <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/provider');?></td>
                <td class="font-12" width="20%" style="text-align:left; padding:0 4px;"><?php echo t($t_base.'fields/forwarder');?></td>
                <td class="font-12" width="20%" style="text-align:left; padding:0 4px;"><?php echo t($t_base.'fields/from')." ".t($t_base.'fields/country'); ?></td>
                <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/to')." ".t($t_base.'fields/country');?></td> 
                <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/type');?></td> 
            </tr>
            <tr>
                <td>
                    <select id="idCourierProvider" name="addCourierExcludedTradesAry[idCourierProvider]" onchange="update_courier_excluded_trades(this.value,'LOAD_FORWARDER_LIST');checkCourierExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($providerProductArr))
                            {
                                foreach($providerProductArr as $providerProductArrs)
                                {
                                    ?>
                                    <option  value='<?php echo $providerProductArrs['id']?>' <?php if($idCourierProvider==$providerProductArrs['id']){?> selected <?php }?>><?php echo $providerProductArrs['szProviderName'];?></option>
                                    <?php
                                }
                            }?>
                    </select> 
                </td>
                <td style="padding:0 4px;" id="excluded_trade_forwarder_container">
                    <select id="idForwarder" name="addCourierExcludedTradesAry[idForwarder]" onchange="checkCourierExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($forwarderListAry))
                            {
                                foreach($forwarderListAry as $forwarderListArys)
                                {
                                    ?>
                                    <option  value='<?php echo $forwarderListArys['idForwarder']?>' <?php if($idForwarder==$forwarderListArys['idForwarder']){?> selected <?php }?>><?php echo $forwarderListArys['szForwarderDisplayName']." (".$forwarderListArys['szForwarderAlias'].")"; ?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </td> 
                <td style="padding:0 4px;">
                    <select id="idOriginCountry" name="addCourierExcludedTradesAry[idOriginCountry]" onchange="checkCourierExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                                ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addCourierExcludedTradesAry']['idOriginCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                <?php
                                }
                            }?>
                        <?php 
                            if(!empty($allCountryAry))
                            {
                                foreach($allCountryAry as $allCountryArys)
                                {
                                    ?>
                                    <option  value='<?php echo $allCountryArys['id']?>' <?php if($idOriginCountry==$allCountryArys['id']){?> selected <?php }?>><?php echo $allCountryArys['szCountryName'];?></option>
                                    <?php
                                }
                            }?>
                    </select>
                </td> 
                <td>
                    <select id="idDestinationCountry" name="addCourierExcludedTradesAry[idDestinationCountry]" onchange="checkCourierExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                                ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addCourierExcludedTradesAry']['idDestinationCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                <?php
                                }
                            }?>
                        <?php 
                            if(!empty($allCountryAry))
                            {
                                foreach($allCountryAry as $allCountryArys)
                                {
                                    ?>
                                    <option  value='<?php echo $allCountryArys['id']?>' <?php if($idDestinationCountry==$allCountryArys['id']){?> selected <?php }?>><?php echo $allCountryArys['szCountryName'];?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </td>  
                <td>
                    <select id="iCustomerType" name="addCourierExcludedTradesAry[iCustomerType]" onchange="checkCourierExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($customerTypeAry))
                            {
                                foreach($customerTypeAry as $customerTypeArys)
                                {
                                    ?>
                                    <option  value='<?php echo $customerTypeArys['id']?>' <?php if($iCustomerType==$customerTypeArys['id']){?> selected <?php }?>><?php echo $customerTypeArys['szType'];?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select> 
                </td>
            </tr>
            <tr>
                <td colspan="5" style="text-align:right;padding-top:5px;">   
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_excluded_trades_data"><span style="min-width:50px;" id="add_edit_excluded_trades_data_span"><?php if($idExludedTrades>0){?><?=t($t_base.'fields/save');?> <?php }else{?><?=t($t_base.'fields/add');?><?php }?></span></a>
                    <a href="javascript:void(0)" class="button2" <?php if($_POST['addCourierExcludedTradesAry']['id']==0){?>style="opacity:0.4"<?php }?> id="detete_excluded_trades_data" align="left"><span style="min-width:50px;" id="detete_excluded_trades_data_span"><?php if($idExludedTrades>0){?><?=t($t_base.'fields/cancel');?> <?php }else{?><?=t($t_base.'fields/delete');?><?php }?></span></a>
                </td>
            </tr>
        </table>
        <input type="hidden" name="addCourierExcludedTradesAry[idExludedTrades]" id="idExludedTrades" value="<?php echo $idExludedTrades;?>"> 
    </form>
</div>
    <?php
}
function courierCargoLimitationList($kCourierServices,$idGroup=0,$idCargoLimit=0)
{
    $t_base="management/providerProduct/";
    
    if((int)$idGroup>0)
    {
        $providerProductListArr=$kCourierServices->getCourierCargoLimitationData($idGroup);
        /*$productTermsInstructionArr=$kCourierServices->getProductTermsInstructionData($idCourierProviderProduct);
        if(count($productTermsInstructionArr)>0)
        {
            if(count($productTermsInstructionArr)==2)
            {
                $_POST['addEditInsTermsArr']=$productTermsInstructionArr[0];
            }
            else
            {
                $_POST['addEditInsTermsArr']=$productTermsInstructionArr[0];
            }
        }*/
    }	
    $counter=count($providerProductListArr);
    if((int)$idGroup>0)
    {
        $_POST['addCourierCargoLimitAry']['idGroup']=$idGroup;
        //$_POST['addEditInsTermsArr']['idCourierProviderProduct']=$idCourierProviderProduct;
    } 
    ?>
    
    <div id="couier_cargo_limitation_list_container">    
        <h4><strong><?php echo t($t_base.'title/cargo_limitation');?></strong></h4>
        <div class="clearfix courier-provider courier-provider-scroll"> 
            <table cellpadding="5" cellspacing="0" class="format-2" width="100%" id="couier_provider_product_list_table">
                <tr>
                    <td width="40%" style="text-align:left;" class="firsttdnoborder"><strong><?php echo t($t_base.'fields/variable');?></strong></td> 
                    <td width="30%" align="left"><strong><?php echo t($t_base.'fields/restriction');?></strong></td>
                    <td width="30%" style="text-align:left;"><strong><?php echo t($t_base.'fields/limit');?></strong></td>
                </tr>
                <?php
                    if(!empty($providerProductListArr))
                    {
                        foreach($providerProductListArr as $providerProductListArrs)
                        {
                            if($providerProductListArrs['idCargoLimitationType']==7) //Total volume for shipment 
                            {
                                $szLimit = number_format($providerProductListArrs['szLimit'],2);
                            }
                            else
                            {
                                $szLimit = number_format($providerProductListArrs['szLimit']);
                            }
                            ?>
                            <tr id="courier_cargo_limit_<?php echo $providerProductListArrs['id'];?>" <?php if($providerProductListArrs['id']==$idCargoLimit){?> style="background:#DBDBDB;cursor:pointer;" <?php }?> onclick="showEditDeleteDataoFCargoLimit('<?php echo $providerProductListArrs['id'];?>','<?php echo $providerProductListArrs['idGroup'];?>','<?=t($t_base.'fields/edit');?>','<?=t($t_base.'fields/save');?>','<?=t($t_base.'fields/cancel');?>')">
                                <td><?php echo $providerProductListArrs['szVariable']?></td>
                                <td><?php echo $providerProductListArrs['szRestriction']?></td>
                                <td><?php echo $szLimit." ".$providerProductListArrs['szUnit']; ?></td>
                            </tr>
                        <?php		
                        } 		 
                    }  ?> 
                </table>
            </div> 
    </div>   
    <div class="clear-all"></div>
    <br> 
    <div id="courier_cargo_limitation_addedit_data">
        <?php
            echo display_courier_cargo_limitation_addedit_form($kCourierServices);
        ?>	
    </div>   
    <?php 
}
function display_courier_cargo_limitation_addedit_form($kCourierServices)
{
    $t_base="management/providerProduct/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    } 
    $cargoLimitationTypeListArr=$kCourierServices->getCourierCargoLimitionsType();
    
?>
            
<script type="text/javascript">
    var cargoUnitAry = new Array();
    <?php
    if(!empty($cargoLimitationTypeListArr))
    {
        foreach($cargoLimitationTypeListArr as $cargoLimitationTypeListArrs)
        {
            ?>
            cargoUnitAry['<?php echo $cargoLimitationTypeListArrs['id']; ?>'] = '<?php echo $cargoLimitationTypeListArrs['szUnit']; ?>';
            <?php
        }
    } 
    ?> 
    function display_cargo_units(rest_id)
    { 
        if(rest_id>0)
        {
            var szUnit = cargoUnitAry[rest_id];
            $("#cargo_limitation_unit").html(szUnit);
        }
    }
    <?php
        if($_POST['addCourierCargoLimitAry']['idCargoLimitationType']>0)
        {
            ?>
                display_cargo_units('<?php echo $_POST['addCourierCargoLimitAry']['idCargoLimitationType']; ?>');
            <?php
        }
    ?>
</script>

<div id="courier_provider_form" style="width:100%;">
    <form action="javascript:void(0);" name="courierCargoLimitationForm" method="post" id="courierCargoLimitationForm">
        <table cellpadding="0" cellspacing="1" width="100%">
            <tr>  
                <td class="font-12" width="34%" style="text-align:left;"><?php echo t($t_base.'fields/variable');?></td>
                <td class="font-12" width="33%" style="text-align:left; padding:0 4px;"><?php echo t($t_base.'fields/restriction');?></td>
                <td class="font-12" colspan="2" style="text-align:left;"><?php echo t($t_base.'fields/limit');?></td>
                
            </tr>
            <tr>
                <td>
                    <select id="idCargoLimitationType" <?php echo $field_width; ?> name="addCourierCargoLimitAry[idCargoLimitationType]" onchange="checkCourierCargoLimitData();display_cargo_units(value);">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($cargoLimitationTypeListArr))
                            {
                                foreach($cargoLimitationTypeListArr as $cargoLimitationTypeListArrs)
                                {
                                    ?>
                                    <option  value='<?php echo $cargoLimitationTypeListArrs['id']?>' <?php if($_POST['addCourierCargoLimitAry']['idCargoLimitationType']==$cargoLimitationTypeListArrs['id']){?> selected <?php }?>><?php echo $cargoLimitationTypeListArrs['szVariable'];?></option>
                                    <?php
                                }
                            }?>
                    </select> 
                </td>
                <td style="padding:0 4px;">
                    <select id="szRestriction" <?php echo $field_width; ?> name="addCourierCargoLimitAry[szRestriction]" onchange="checkCourierCargoLimitData();">
                        <option value=''>Select</option>
                        <option value='Minimum' <?php if($_POST['addCourierCargoLimitAry']['szRestriction']=='Minimum'){?>selected<?php }?>>Minimum</option>
                        <option value='Maximum' <?php if($_POST['addCourierCargoLimitAry']['szRestriction']=='Maximum'){?>selected<?php }?>>Maximum</option>
                    </select>
                </td>
                <td width="26%">
                    <input type="text" name="addCourierCargoLimitAry[szLimit]" id="szLimit" value="<?php echo $_POST['addCourierCargoLimitAry']['szLimit'];?>" onblur="check_form_field_empty_standard_courier(this.form.id,this.id);" onkeyup="checkCourierCargoLimitData();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" />
                    
                </td>
                <td width="7%" style="text-align:center"><span id="cargo_limitation_unit"></span></td>
                
            </tr>
            <tr>
                <td colspan="4" style="text-align:right;padding-top:5px;">   
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_cargo_limitation_data"><span style="min-width:50px;"><?php if($_POST['addCourierCargoLimitAry']['id']>0){?><?=t($t_base.'fields/save');?> <?php }else{?><?=t($t_base.'fields/add');?><?php }?></span></a>
                    <a href="javascript:void(0)" class="button2" <?php if($_POST['addCourierCargoLimitAry']['id']==0){?>style="opacity:0.4"<?php }?> id="detete_cargo_limitation_data" align="left"><span style="min-width:50px;"><?php if($_POST['addCourierCargoLimitAry']['id']>0){?><?=t($t_base.'fields/cancel');?> <?php }else{?><?=t($t_base.'fields/delete');?><?php }?></span></a>
                </td>
            </tr>
        </table>
        <input type="hidden" name="addCourierCargoLimitAry[id]" id="id" value="<?php echo $_POST['addCourierCargoLimitAry']['id'];?>">
        <input type="hidden" name="addCourierCargoLimitAry[idGroup]" id="idGroup" value="<?php echo $_POST['addCourierCargoLimitAry']['idGroup'];?>">
    </form>
</div>
    <?php
}

function display_courier_product_terms_instruction_form($kCourierServices)
{ 
    $t_base="management/providerProduct/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
                ?>
                <script type="text/javascript">
                        $("#<?php echo $errorKey?>").addClass('red_border');
                </script>
            <?php 
        }
    }
?>
    <div id="courier_provider_form" style="width:100%;">
        <form action="javascript:void(0);" name="courierProductTermsInstructionForm" method="post" id="courierProductTermsInstructionForm">
            <div class="twotextarea clearfix">
                <div class="table-content-left">
                    <h4><strong><?php echo t($t_base.'title/instruction');?></strong></h4>
                    <div class="provider-term-inst"><textarea onkeyup="checkProviderProductTermInsData();" onblur="check_form_field_empty_standard_courier(this.form.id,this.id);checkProviderProductTermInsData();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" id="szInstruction" name="addEditInsTermsArr[szInstruction]"><?php echo $_POST['addEditInsTermsArr']['szInstruction']?></textarea></div>
                    <div class="btns">
                        <select name="addEditInsTermsArr[idLanguage]" style="width:30%;" id="idLanguage" onchange="getTermInstructionByidLanguage('<?php echo $_POST['addEditInsTermsArr']['idCourierProviderProduct'];?>',this.value);">
                            <option value="1" <?php if($_POST['addEditInsTermsArr']['idLanguage']==1){?>selected<?php }?>><?php echo t($t_base.'fields/english');?></option>
                            <option value="2" <?php if($_POST['addEditInsTermsArr']['idLanguage']==2){?>selected<?php }?>><?php echo t($t_base.'fields/danish');?></option>
                        </select>
                        
                    </div>
                </div>
                <div class="table-content-right">
                    <h4><strong><?php echo t($t_base.'title/terms_contitions');?></strong></h4>
                    <div class="provider-term-inst"><textarea onkeyup="checkProviderProductTermInsData();" onblur="check_form_field_empty_standard_courier(this.form.id,this.id);checkProviderProductTermInsData();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" id="szTerms" name="addEditInsTermsArr[szTerms]"><?php echo $_POST['addEditInsTermsArr']['szTerms']?></textarea></div>
                    <div class="btns">
                        <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_terms_instruction_data"><span style="min-width:50px;"><?=t($t_base.'fields/save');?></span></a>
                    </div>
                </div>   
                <input type="hidden" name="addEditInsTermsArr[id]" id="id" value="<?php echo $_POST['addEditInsTermsArr']['id'];?>">
                <input type="hidden" name="addEditInsTermsArr[idCourierProviderProduct]" id="idCourierProviderProduct" value="<?php echo $_POST['addEditInsTermsArr']['idCourierProviderProduct'];?>">    
            </div>
        </form>
    </div>			
<?php
}

function addDisplayCouierModeTransport($kCourierServices,$bDTDFlag=false)
{ 
    $idPrimarySortKey=trim($_POST['idPrimarySortKey']);
    $idOtherSortKeyArr=trim($_POST['idOtherSortKeyArr']);
    $szSortValue=trim($_POST['szSortValue']); 
    $idOtherSortKeyArr=explode(";",$idOtherSortKeyArr);

    $t_base="management/modeTransport/";
    $courierTrackiocnArr=$kCourierServices->getAllCourierTruckiconData($idPrimarySortKey,$szSortValue,$idOtherSortKeyArr,$bDTDFlag);
    $counter=count($courierTrackiocnArr); 
?> 
    
    <script type="text/javascript">
        $(function () {
            $('#mode-transport-table').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    $(".selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        var tr_id_ary = tr_id.split("____");
                        var idModeTransport = parseInt(tr_id_ary[1]);
                        
                        if(!isNaN(idModeTransport))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                    });  
                    if(res_ary_new.length)
                    {
                        var szModeOfTransIds = res_ary_new.join(';'); 
                       $("#deleteTruckiconId").attr('value',szModeOfTransIds);
                        $("#detete_mode_transport_data").unbind("click");
                        $("#detete_mode_transport_data").click(function(){deleteTruckIconData();});
                        $("#detete_mode_transport_data").attr("style",'opacity:1'); 
                    }
                    else
                    {
                        var szModeOfTransIds = '';
                        $("#deleteTruckiconId").attr('value',szModeOfTransIds);
                        $("#detete_mode_transport_data").unbind("click");
                        $("#detete_mode_transport_data").attr("style",'opacity:0.4');
                    } 
                }
            });
        })
    </script> 
    <!--onclick="seletTheRowForDeleteTruckicon('<?php echo $courierTrackiocnArrs['id']?>')"-->
    <div id="couier_cargo_limitation_list_container"> 
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">	
          <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" id="mode-transport-table">
              <tr id="table-header">
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift(event,'szFromCountry')"><strong><?php echo t($t_base.'fields/from_country');?></strong> <span class="moe-transport-sort-span sort-arrow-up" id="moe-transport-sort-span-id-szFromCountry">&nbsp;</span></td>
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift(event,'szFromRegionName')"><strong><?php echo t($t_base.'fields/region');?></strong> <span class="moe-transport-sort-span" id="moe-transport-sort-span-id-szFromRegionName">&nbsp;</span></td>
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift(event,'szToCountry')"><strong><?php echo t($t_base.'fields/to_country');?></strong> <span class="moe-transport-sort-span" id="moe-transport-sort-span-id-szToCountry">&nbsp;</span></td>
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift(event,'szToRegionName')"><strong><?php echo t($t_base.'fields/region');?></strong> <span class="moe-transport-sort-span" id="moe-transport-sort-span-id-szToRegionName">&nbsp;</span></td>
            </tr>   	
        <?php
            if(!empty($courierTrackiocnArr))
            {
                foreach($courierTrackiocnArr as $courierTrackiocnArrs)
                {?>
                    <tr id="truckin_tr____<?php echo $courierTrackiocnArrs['id']; ?>" >
                        <td id="from_to_country_1_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szFromCountry']?></td>
                        <td id="from_to_country_2_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szFromRegionName']?></td>
                        <td id="from_to_country_3_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szToCountry']?></td>
                        <td id="from_to_country_4_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szToRegionName']?></td>
                    </tr>
                <?php		
                } 				 
            }
            else
            {?>
                <tr>
                   <td colspan="4" align="center">
                       <h4><b><?=t($t_base.'fields/no_courier_provider_found');?></b></h4>
                   </td>
                </tr>
            <?php   }   ?>  
           </table>
       </div> 
   </div>
    <div class="clear-all"></div>
    <br>
   <div id="courier_cargo_limitation_addedit_data">
        <?php
            echo display_courier_country_region_addedit_form($kCourierServices,$bDTDFlag);
        ?>	
    </div> 
<?php
}

function display_courier_country_region_addedit_form($kCourierServices,$bDTDFlag=false)
{
    $t_base="management/modeTransport/";
    $kConfig = new cConfig();
    $regionArr=$kCourierServices->getAllRegion();
    $getAllCountry=$kConfig->getAllCountries(true);

    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    } 
    $iDTDTrades = 0;
    if($bDTDFlag)
    {
        $iDTDTrades = 1;
    }
    else if($_POST['addModeTransportAry']['iDTDTrades']==1)
    {
        $iDTDTrades = 1;
    }
?>
    <div id="courier_provider_form" style="width:100%;">
        <form action="javascript:void(0);" name="modeTrasnportAddForm" method="post" id="modeTrasnportAddForm">
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="font-12" width="30%" style="text-align:left;"><?php echo t($t_base.'fields/from');?></td>
                    <td class="font-12" width="30%" style="text-align:left;"><?php echo t($t_base.'fields/to');?></td>
                    <td style="float:right;" width="40%"></td>
                </tr>
                <tr>
                    <td>
                        <select id="idFromCountry" style="width:98%;" name="addModeTransportAry[idFromCountry]" onchange="checkCountryIsSelected();">
                            <option value=''>Select</option>
                            <?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                                ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addModeTransportAry']['idFromCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                <?php
                                }
                            }?>
                            <?php 
                            if(!empty($getAllCountry))
                            {
                                foreach($getAllCountry as $getAllCountrys)
                                {
                                    ?>
                                        <option  value='<?php echo $getAllCountrys['id']?>' <?php if($_POST['addModeTransportAry']['idFromCountry']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName'];?></option>
                                    <?php
                                }
                            }?>
                        </select>
                    </td>
                    <td>
                        <select id="idToCountry" style="width:98%;" name="addModeTransportAry[idToCountry]" onchange="checkCountryIsSelected();">
                            <option value=''>Select</option>
                            <?php 
                            if(!empty($regionArr))
                            {
                                    foreach($regionArr as $regionArrs)
                                    {
                                            $regionValue=$regionArrs['id']."_r";
                                    ?>
                                            <option  value='<?php echo $regionValue?>' <?php if($_POST['addModeTransportAry']['idToCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                    <?php
                                    }
                            }?>
                            <?php 
                            if(!empty($getAllCountry))
                            {
                                    foreach($getAllCountry as $getAllCountrys)
                                    {?>
                                            <option  value='<?php echo $getAllCountrys['id']?>' <?php if($_POST['addModeTransportAry']['idToCountry']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName'];?></option>
                                    <?php
                                    }
                            }?>
                        </select>
                    </td>
                    <td style="float:right;">  
                        <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_mode_transport_data"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                        <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_mode_transport_data" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/delete');?></span></a>
                        <input type="hidden" name="addModeTransportAry[iDTDTrades]" id="iDTDTrades" value="<?php echo $iDTDTrades; ?>">
                    </td>
                </tr>
            </table>
        </form>
    </div>
	<?php
}

function courierPoviderAgreementList($kCourierServices,$idCourierProviderAgree=0)
{
    $t_base = "SERVICEOFFERING/";
    $courierAgreementArr=$kCourierServices->getCourierAgreementData();
?>
    <script type="text/javascript">
        document.onclick=check;
        function check(e){
            
        var deselectFlag=false;    
        var t = (e && e.target) || (event && event.srcElement);
        while(t.parentNode){
            if(t==document.getElementById('agreementList')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('btn-container')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('provider_agreement_service_trades')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('provider_agreement_price')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('hsbody-2')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('contactPopup')){    
                deselectFlag=true;
            }
                t=t.parentNode
            }
            
            if(!deselectFlag)
            {
                selectPricingServiceTradingData();
                
                $("#add_edit_courier_button").attr("onclick",'');
                $("#add_edit_courier_button").unbind("click");
                $("#add_edit_courier_button").html("<span style='min-width:50px;'>Add</span>");
                $("#add_edit_courier_button").click(function(){openProviderAgreementForm();});
                $("#add_edit_courier_button").attr("style",'opacity:1;');
                
                 $("#detete_provider_agreement_data").unbind("click");
                
                $("#detete_provider_agreement_data").attr("style",'opacity:0.4;');
            }
        }
     </script>   
    <div id="couier_cargo_limitation_list_container"> 
    <div class="clearfix courier-provider courier-provider-scroll" id="agreementList">
        <table class="format-7"  width="100%" cellspacing="0" cellpadding="0" border="0" id="service-offering-table">
        <?php if((int)$_SESSION['admin_id']>0){?>	
            <tr>
                <td style="width:18%;"><strong><?php echo t($t_base.'fields/forwarder');?></strong></td>
                <td style="width:15%;"><strong><?php echo t($t_base.'fields/provider');?></strong></td>
                <td style="width:15%;"><strong><?php echo t($t_base.'fields/booking_by');?></strong></td>
                <td style="width:15%;"><strong><?php echo t($t_base.'fields/account');?></strong></td>
                <!--<td style="width:6%;"><strong><?php echo t($t_base.'fields/type');?></strong></td>-->
                <td style="width:12%;"><strong><?php echo t($t_base.'fields/status');?></strong></td>
                <td style="width:12%;text-align:right;"><strong><?php echo t($t_base.'fields/Bookings_365d');?></strong></td>
                <td style="text-align:right;"><strong><?php echo t($t_base.'fields/Sales_365d');?></strong></td>
            </tr>
      <?php }else{?>
      	<tr>
            <td style="width:15%;"><strong><?php echo t($t_base.'fields/provider');?></strong></td>
            <td style="width:15%;"><strong><?php echo t($t_base.'fields/booking_by');?></strong></td>
            <td style="width:25%;"><strong><?php echo t($t_base.'fields/account');?></strong></td>
            <td style="width:25%;"><strong><?php echo t($t_base.'fields/user_name');?></strong></td> 
            <!--<td style="width:10%;"><strong><?php echo t($t_base.'fields/type');?></strong></td>-->
            <td style="width:15%;"><strong><?php echo t($t_base.'fields/status');?></strong></td>
        </tr>
        <?php 
        
        }
        if(count($courierAgreementArr)>0)
        {
            $counterValue=count($courierAgreementArr);
            foreach($courierAgreementArr as $courierAgreementArrs)
            {
                if((int)$courierAgreementArrs['iBookingIncluded']==1)
                {
                    $bookedBy=$courierAgreementArrs['szDisplayName'];
                }
                else
                {
                    $bookedBy="Transporteca";	
                }
                if((int)$courierAgreementArrs['idCourierProvider']==2)
                {
                    $szAccountNumberFullText = decrypt($courierAgreementArrs['szMeterNumber'],ENCRYPT_KEY);
                    $szAccountNumber = limit_text_forwarder(sanitize_all_html_input($szAccountNumberFullText),20);
                }else{
                $szAccountNumberFullText = decrypt($courierAgreementArrs['szAccountNumber'],ENCRYPT_KEY);
                $szAccountNumber = limit_text_forwarder(sanitize_all_html_input($szAccountNumberFullText),20);
                }
               $szUsernameFullText = decrypt($courierAgreementArrs['szUsername'],ENCRYPT_KEY);
               $szUsername = limit_text_forwarder(sanitize_all_html_input($szUsernameFullText),20);
               if($courierAgreementArrs['iServiceType']==1)
               {
                   $szServiceType = 'Import';
               }
               else
               {
                    $szServiceType = 'Export';
               }
        ?>
        <?php if((int)$_SESSION['admin_id']>0){?>
                <tr <?php if($courierAgreementArrs['id']==$idCourierProviderAgree){?> style="background:#DBDBDB;cursor:pointer;" <?php }?> onclick="selectPricingServiceTradingData('<?php echo $courierAgreementArrs['id'];?>')">
                    <td><?php echo $courierAgreementArrs['szDisplayName']?> (<?php echo $courierAgreementArrs['szForwarderAlias'];?>)</td>
                    <td><?php echo $courierAgreementArrs['szName']?></td>
                    <td><?php echo $bookedBy;?></td>
                    <td title="<?php echo $szAccountNumberFullText; ?>"><?php echo $szAccountNumber;?></td>
                    <!--<td><?php echo $szServiceType;?></td>-->
                    <td id="update_status_<?php echo $courierAgreementArrs['id'];?>"><?php echo $courierAgreementArrs['szManagementStatus'];?></td>
                    <td style="width:12%;text-align:right;"><?php echo number_format($courierAgreementArrs['totalBooking']);?></td>
                    <td style="text-align:right;"><?php echo "USD ".number_format((float)$courierAgreementArrs['totalSales']);?></td>
                </tr> 
        <?php }else{?>
                <tr <?php if($courierAgreementArrs['id']==$idCourierProviderAgree){?> style="background:#DBDBDB;cursor:pointer;" <?php }?> onclick="selectPricingServiceTradingData('<?php echo $courierAgreementArrs['id'];?>')">
                    <td><?php echo $courierAgreementArrs['szName']?></td>
                    <td><?php echo $bookedBy;?></td>
                    <td title="<?php echo $szAccountNumberFullText; ?>"><?php echo $szAccountNumber;?></td>
                    <td title="<?php echo $szUsernameFullText; ?>"><?php echo $szUsername?></td> 
                    <!--<td><?php echo $szServiceType;?></td>-->
                    <td id="update_status_<?php echo $courierAgreementArrs['id'];?>"><?php echo $courierAgreementArrs['szStatus'];;?></td>
                </tr> 
            <?php } 
                } 
            } 
            ?> 
        </table> 
    </div>
    <div class="clear-all"></div>
    <br>
    <div style="float: right;" class="btn-container" id="btn-container">  
        <a href="javascript:void(0)" id="add_edit_courier_button" class="button1" onclick="openProviderAgreementForm();"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
        <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_provider_agreement_data" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/remove');?></span></a>
   </div>
  </div>        	
<?php                	
} 

function addCourierProviderAgreement($kCourierServices,$errorFlag=false,$bForwarderFlag=false)
{
    $t_base = "SERVICEOFFERING/"; 
    $providerArr=$kCourierServices->getCourierProviderList();
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    }
    
    if((int)$_SESSION['admin_id']>0)
    {
        $kBooking = new cBooking();
        $forwarderIdName=$kBooking->forwarderIdName($_SESSION['admin_id']);
    }
    //showProviderDropDown(this.value);
    if($_POST['courierProviderAgreementArr']['idEdit']>0)
    {  
        $szButtonText = t($t_base.'fields/save'); 
        $szPopupHeading = t($t_base.'title/edit_courier_agreements');
    }
    else
    { 
        $szButtonText = t($t_base.'fields/add'); 
        $szPopupHeading = t($t_base.'title/add_courier_agreements');
    }
?>
	<div id="popup-bg"></div>
        <div id="popup-container" >
            <script type="text/javascript">
                activateCheckLoginButton('<?php echo $szButtonText;?>','<?php echo t($t_base.'fields/check_login');?>');
            </script>
            <div class="popup contact-popup"> 
                <h3><?php echo $szPopupHeading;?></h3> <br> 
		<p><?=t($t_base.'messages/delete_msg_courier_agreement');?>.</p>
		<br/> 
		<?php if($errorFlag){ 
                        if(!empty($kCourierServices->szSpecialErrorMesage))
                        {
                            $szErrorMessage = $kCourierServices->szSpecialErrorMesage;
                        }
                        else
                        {
                            $szErrorMessage = t($t_base.'errormsg/api_login_error');
                        }
                    ?>
                    <div id="regError" class="errorBox ">
                        <div id="regErrorList">
                            <ul>
                                <li><?php echo $szErrorMessage; ?></li>
                            </ul>
                        </div>	
                    </div>
		<?php } ?>
		<form name="addCourierAgreementForm" id="addCourierAgreementForm" method="post">
		<table  width="100%" cellspacing="0" cellpadding="3">
                    <?php  if((int)$_SESSION['admin_id']>0){
			 if((int)$_POST['courierProviderAgreementArr']['idEdit']>0){?>  
			 <input type="hidden" id="idForwarder" name="courierProviderAgreementArr[idForwarder]" value="<?php echo $_POST['courierProviderAgreementArr']['idForwarder']?>"/>
			  <?php }else{?> 
			 	<td width="40%"><?php echo t($t_base.'fields/forwarder');?></td>	
			 	<td>
                                    <select <?php if((int)$_POST['courierProviderAgreementArr']['idEdit']>0){?> disabled <?php }?> id="idForwarder" name="courierProviderAgreementArr[idForwarder]" onchange="activateCheckLoginButton('<?php echo $szButtonText; ?>','<?=t($t_base.'fields/check_login');?>');">
                                    <?php
                                        if($forwarderIdName!=array())
                                        {
                                            foreach($forwarderIdName as $fwdName)
                                            {?>
                                                <option value="<?php echo $fwdName['id'];?>" <?php if($fwdName['id']==$_POST['courierProviderAgreementArr']['idForwarder']){?> selected <?php }?>><?php echo $fwdName['szDisplayName']." (".$fwdName['szForwarderAlias'].")";?></option>
                                            <?php
                                            }
                                        }
                                    ?>
                                    </select>
                                </td>
                            <?php } }  ?> 
                            <tr>
				<td width="40%"><?php echo t($t_base.'fields/courier_provider');?></td>	
				<td id="show_provider_dropdown">
                                    <?php if((int)$_POST['courierProviderAgreementArr']['idEdit']>0 && $bForwarderFlag){?>
                                        <select  disabled="disabled" >
                                            <option value=''><?php echo t($t_base.'fields/select');?></option>
                                            <?php
                                                if(!empty($providerArr))
                                                {
                                                    foreach($providerArr as $providerArrs)
                                                    { 
                                                        ?> 
                                                        <option value="<?php echo $providerArrs['id']?>" <?php if($_POST['courierProviderAgreementArr']['idCourierProvider']==$providerArrs['id']){?> selected <?php }?>><?php echo $providerArrs['szName']?></option>
                                                        <?php    
                                                    }  
                                                }
                                            ?> 
                                        </select>
                                        <input type="hidden" name="courierProviderAgreementArr[idCourierProvider]" id="idCourierProvider" value="<?php echo $_POST['courierProviderAgreementArr']['idCourierProvider']; ?>">
                                    <?php } else {?>
                                        <select  name="courierProviderAgreementArr[idCourierProvider]" id="idCourierProvider"  onchange="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);activateCheckLoginButton('<?php echo $szButtonText;?>','<?php echo t($t_base.'fields/check_login');?>');">
                                            <option value=''><?php echo t($t_base.'fields/select');?></option>
                                            <?php
                                                if(!empty($providerArr))
                                                {
                                                    foreach($providerArr as $providerArrs)
                                                    { 
                                                        ?> 
                                                        <option value="<?php echo $providerArrs['id']?>" <?php if($_POST['courierProviderAgreementArr']['idCourierProvider']==$providerArrs['id']){?> selected <?php }?>><?php echo $providerArrs['szName']?></option>
                                                        <?php    
                                                    }  
                                                }
                                            ?> 
                                        </select>
                                    <?php }?> 
                                </td>		
                            </tr>
<!--                            <tr>
                                <td width="40%"><?php echo t($t_base.'fields/service_type');?></td>	
                                <td id="show_provider_dropdown">
                                    <select name="courierProviderAgreementArr[iServiceType]" id="iServiceType" onchange="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);activateCheckLoginButton('<?=t($t_base.'fields/add');?>','<?=t($t_base.'fields/check_login');?>');">  
                                        <option value="1" <?php if($_POST['courierProviderAgreementArr']['iServiceType']==1){?> selected <?php }?>><?php echo t($t_base.'fields/import');?></option>
                                        <option value="2" <?php if($_POST['courierProviderAgreementArr']['iServiceType']==2){?> selected <?php }?>><?php echo t($t_base.'fields/export');?></option>
                                    </select>
                                </td>		
			</tr>-->
			<tr>
                            <td id="acc_number"><?php if($_POST['courierProviderAgreementArr']['idCourierProvider']=='2'){?><?php echo t($t_base.'fields/access_key');?><?php }else {?> <?php echo t($t_base.'fields/account_number');?> <?php }?></td>
                            <td><input type="text" onblur="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);" onkeyup="activateCheckLoginButton('<?php echo $szButtonText;?>','<?=t($t_base.'fields/check_login');?>','<?=t($t_base.'fields/check_login');?>');" id="szAccountNumber" name="courierProviderAgreementArr[szAccountNumber]" value="<?php echo $_POST['courierProviderAgreementArr']['szAccountNumber']?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);"/></td>			
			</tr>
                        <?php 
                                //In case of TNT we are not showing this field
                            if($_POST['courierProviderAgreementArr']['idCourierProvider']!='3'){ $szStyle_tr = 'style="none;"'; } ?>
                            <tr id="meter_td_Container" <?php echo $szStyle_tr; ?>>
                                <td id="szMeterTr"><?php if($_POST['courierProviderAgreementArr']['idCourierProvider']=='2'){?><?php echo t($t_base.'fields/account_number');?> <?php }else{?><?php echo t($t_base.'fields/meter_number');?><?php }?></td>
                                <td><input type="text" autocomplete="off" onblur="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);" onkeyup="activateCheckLoginButton('<?php echo $szButtonText; ?>','<?=t($t_base.'fields/check_login');?>');" id="szMeterNumber" name="courierProviderAgreementArr[szMeterNumber]" value="<?php echo $_POST['courierProviderAgreementArr']['szMeterNumber']?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);"/></td>			
                            </tr>
                        
			<tr>
                            <td id="user_name"><?php if($_POST['courierProviderAgreementArr']['idCourierProvider']=='1'){?><?php echo t($t_base.'fields/authentication_key');?><?php }else {?> <?php echo t($t_base.'fields/user_name');?> <?php }?></td>	
                            <td><input type="text" autocomplete="off" onblur="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);" onkeyup="activateCheckLoginButton('<?php echo $szButtonText; ?>','<?=t($t_base.'fields/check_login');?>');" id="szUsername" name="courierProviderAgreementArr[szUsername]" value="<?php echo $_POST['courierProviderAgreementArr']['szUsername']?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);"/></td>	
			</tr>
			<tr>
                            <td><?php echo t($t_base.'fields/password');?></td>
                            <td><input type="password" style="width: 100%;" autocomplete="off" onblur="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);" onkeyup="activateCheckLoginButton('<?php echo $szButtonText;?>','<?=t($t_base.'fields/check_login');?>');" id="szPassword" name="courierProviderAgreementArr[szPassword]" value="<?php echo $_POST['courierProviderAgreementArr']['szPassword']?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);"/>

                            <input type="hidden" id="idEdit" name="courierProviderAgreementArr[idEdit]" value="<?php echo $_POST['courierProviderAgreementArr']['idEdit']?>"/>
                            </td>			
			</tr>
                        <?php if(!$bForwarderFlag){
                                /*
                                * This field will be displayed only in Management section 
                                */
                            ?>
                        <tr>
                            <td><?php echo t($t_base.'fields/carrier_account_id');?></td>
                            <td>
                                <input type="text" style="width: 100%;" onblur="check_form_field_empty_standard_courier(this.form.id,this.id,this.id);" id="szCarrierAccountID" name="courierProviderAgreementArr[szCarrierAccountID]" onkeyup="activateCheckLoginButton('<?php echo $szButtonText;?>','<?=t($t_base.'fields/check_login');?>');"  value="<?php echo $_POST['courierProviderAgreementArr']['szCarrierAccountID']?>" />                            
                            </td>			
			</tr> 
                        <?php } ?>
		</table> 
            </form><br/>
            <p align="center">
                <a href="javascript:void(0)" style="opacity:0.4" id="check_login_button"  class="button1" ><span id="loaderClass"><span><?=t($t_base.'fields/check_login');?></span></span></a> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
            </p>
	</div>
</div>
<?php
}
function addDisplayCouierAgreementService($kCourierServices,$idCourierProvider,$idServiceCoveredAgree=0)
{
    $t_base = "SERVICEOFFERING/";
    
    $_POST['agreeServiceCoverArr']['idCourierProvider']=$idCourierProvider;  
    
    if($idCourierProvider>0)
    {
        $servicDataArr=$kCourierServices->getCourierAgreeServiceData($_POST['agreeServiceCoverArr']['idCourierAgreeProvider']);
    }
    if((int)$_SESSION['admin_id']>0)
    {
        $secondFieldName=t($t_base.'fields/forwarder_product_name');
    }
    else
    { 
        $secondFieldName=t($t_base.'fields/your_name');
    }
?>        
    <div id="insurance_couier_provider_list_container">    
        <h4><strong><?php echo t($t_base.'title/services_covered');?></strong></h4> 
        <div class="clearfix courier-provider courier-provider-scroll" >
            <table cellpadding="0" cellspacing="0" class="format-7" width="100%" id="insurance_couier_provider_list_table">
                <tr>
                    <td><strong><?php echo t($t_base.'fields/service');?></strong></td>
                    <td><strong><?php echo $secondFieldName;?></strong></td>
                </tr>
                <?php
                if(!empty($servicDataArr))
                {
                    foreach($servicDataArr as $servicDataArrs)
                    { ?>
                        <tr onclick="selectAgreeCoveredData('<?php echo $idCourierProvider;?>','<?php echo $servicDataArrs['id'];?>','<?php echo $servicDataArrs['idCourierAgreement'];?>','<?=t($t_base.'fields/edit');?>','<?=t($t_base.'fields/save');?>','<?=t($t_base.'fields/cancel');?>');" id="service_covered_<?php echo $servicDataArrs['id']?>" <div <?php if($servicDataArrs['id']==$idServiceCoveredAgree){?> class="selected-row" <?php }?>>
                            <td><?php echo $servicDataArrs['szName'];?></td>
                            <td><?php echo $servicDataArrs['szDisplayName'];?></td>
                        </tr>
                <?php }	  }else { ?>
                    <tr>
                        <td colspan="2" style="text-align:center;"><?php echo t($t_base.'fields/no_record_found'); ?></td>
                    </tr>
                <?php }?>
            </table>
        </div> 
    </div>
    <div class="clear-all"></div>
    <br>
    <div id="addEditServiceCoverdDiv" class="service-provider-fields">
    	<?php addAgreementServiceCoverdFormHtml($kCourierServices,$idCourierProvider);?>
    </div>

                		
<?php
}
function addAgreementServiceCoverdFormHtml($kCourierServices,$idCourierProvider=0)
{
    $courierActiveProviderProductArr=$kCourierServices->getCourierActiveProviderProduct($idCourierProvider);
    $t_base = "SERVICEOFFERING/";

    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    }
    
    $servicDataArr=$kCourierServices->getCourierAgreeServiceData($_POST['agreeServiceCoverArr']['idCourierAgreeProvider']);
    $productProviderArr=array();
    if(!empty($servicDataArr))
    {
        foreach($servicDataArr as $servicDataArrs)
        {
            $productProviderArr[]=$servicDataArrs['idProviderProduct']; 
        } 
    }
    if((int)$_SESSION['admin_id']>0)
    {
        $secondFieldName=t($t_base.'fields/forwarder_product_name');
    }
    else
    { 
        $secondFieldName=t($t_base.'fields/your_name');
    }
    
     $witdh="26%";
    if((int)$_SESSION['forwarder_user_id']==0)
    {
        $witdh="50%";
    }
?>
    <form name="addAgreementServiceCoverForm" id="addAgreementServiceCoverForm" method="post">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="font-12" width="<?=$witdh?>"><?php echo t($t_base.'fields/service');?></td>
              	<td class="font-12" width="<?=$witdh?>"><?php echo $secondFieldName;?></td>
                <?php
                    if((int)$_SESSION['forwarder_user_id']>0)
                    {
                        ?>
                    
                <td width="48%">&nbsp;</td>
                    <?php }?>
            </tr>
            <tr>
                <td>
                    <select name="agreeServiceCoverArr[idProviderProduct]" id="idProviderProduct" onchange="activateAgreementServiceCovered();">
                        <option value=""><?php echo t($t_base.'fields/select');?></option>
                        <?php
                            if(!empty($courierActiveProviderProductArr))
                            {
                                foreach($courierActiveProviderProductArr as $courierActiveProviderProductArrs)
                                {
                                    $showFlag=true;
                                    if(!empty($productProviderArr) && in_array($courierActiveProviderProductArrs['id'],$productProviderArr))
                                    {
                                            $showFlag=false;	
                                    }

                                    if($_POST['agreeServiceCoverArr']['id']>0 && $_POST['agreeServiceCoverArr']['idProviderProduct']==$courierActiveProviderProductArrs['id'])
                                    {
                                            $showFlag=true;							
                                    }
                                    if($showFlag)
                                    {
					?>
                                        <option value="<?php echo $courierActiveProviderProductArrs['id']?>" <?php if($_POST['agreeServiceCoverArr']['idProviderProduct']==$courierActiveProviderProductArrs['id']){?> selected <?php }?>><?php echo $courierActiveProviderProductArrs['szName']?></option>
					<?php
                                    }	
                                }
                            }?>
                    </select>
                </td>
                <td>
                    <input type="text" name="agreeServiceCoverArr[szDisplayName]" id="szDisplayName" value="<?php echo $_POST['agreeServiceCoverArr']['szDisplayName']?>" onkeyup="activateAgreementServiceCovered();">
                    <input type="hidden" name="agreeServiceCoverArr[idCourierAgreeProvider]" id="idCourierAgreeProvider" value="<?php echo $_POST['agreeServiceCoverArr']['idCourierAgreeProvider']?>">
                    <input type="hidden" name="agreeServiceCoverArr[id]" id="id" value="<?php echo $_POST['agreeServiceCoverArr']['id']?>">
                </td>
                 <?php
                    if((int)$_SESSION['forwarder_user_id']>0)
                    {
                        ?>
                <td align="right">
                    <input type="hidden" name="agreeServiceCoverArr[idCourierProvider]" id="idCourierProvider" value="<?php echo $_POST['agreeServiceCoverArr']['idCourierProvider']?>">
                    <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_agree_service_covered"><span style="min-width:50px;"><?=t($t_base.'fields/remove');?></span></a>
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_agree_service_covered"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                                        
                </td>
                    <?php }?>
            </tr>
             <?php
                    if((int)$_SESSION['forwarder_user_id']==0)
                    {
                        ?>
            <tr>
                <td style="text-align:right;padding-top: 10px;" colspan="2">
                    <input type="hidden" name="agreeServiceCoverArr[idCourierProvider]" id="idCourierProvider" value="<?php echo $_POST['agreeServiceCoverArr']['idCourierProvider']?>">
                    
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_agree_service_covered"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                    <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_agree_service_covered"><span style="min-width:50px;"><?=t($t_base.'fields/remove');?></span></a>
                                        
                </td>
            </tr>
                    <?php }?>
            
        </table>
    </form>
<?php
}
function addDisplayCouierAgreementTrades($kCourierServices,$idTradeOffered=0)
{
    $t_base = "SERVICEOFFERING/";  
    if($_POST['agreeTradesOfferArr']['idCourierAgreeProvider']>0)
    {
        $tradesDataArr=$kCourierServices->getCourierAgreeTradeData($_POST['agreeTradesOfferArr']['idCourierAgreeProvider']); 
    }
?>
    <div id="insurance_couier_provider_list_container">   
        <h4><strong><?php echo t($t_base.'title/trades_offered');?></strong></h4> 
        <div class="clearfix courier-provider courier-provider-scroll">
            <table cellpadding="0" cellspacing="0" class="format-7" width="100%" id="insurance_couier_provider_list_table">
                    <tr>
                        <td><strong><?php echo t($t_base.'fields/country');?></strong></td>
                        <td><strong><?php echo t($t_base.'fields/trade');?></strong></td>
                    </tr>
                    <?php
                    if(!empty($tradesDataArr))
                    {
                        foreach($tradesDataArr as $tradesDataArrs)
                        {?>
                                <tr onclick="selectAgreeTradeData('<?php echo $tradesDataArrs['id'];?>','<?php echo $tradesDataArrs['idCourierAgreement'];?>','<?=t($t_base.'fields/edit');?>','<?=t($t_base.'fields/save');?>','<?=t($t_base.'fields/cancel');?>');" id="trade_covered_<?php echo $tradesDataArrs['id']?>"  <?php if($tradesDataArrs['id']==$idTradeOffered){?> style="background:#DBDBDB;cursor:pointer;" <?php }?>>
                                    <td><?php echo $tradesDataArrs['szCountryName'];?></td>
                                    <td><?php echo $tradesDataArrs['iTradeValue'];?></td>
                                </tr>
                        <?php		
                        }	 
                    } else {
                    ?>
                    <tr>
                        <td colspan="2" style="text-align:center;"><?php echo t($t_base.'fields/no_record_found'); ?></td>
                    </tr>
                    <?php } ?>
            </table>
        </div>
    </div> 
    <div class="clear-all"></div>
    <br>
    <div id="addEditTradesOfferDiv" class="service-provider-fields">
     <?php addAgreementTradesOfferedFormHtml($kCourierServices);?>
    </div> 		
<?php
}

function addAgreementTradesOfferedFormHtml($kCourierServices)
{
    $courierActiveProviderProductArr=$kCourierServices->getCourierActiveProviderProduct($idCourierProvider);
    $t_base = "SERVICEOFFERING/";

    $kConfig = new cConfig();
    $getAllCountry=$kConfig->getAllCountries(true);

    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    }
    
     $witdh="26%";
    if((int)$_SESSION['forwarder_user_id']==0)
    {
        $witdh="50%";
    }
?>
    <form name="addAgreementTradesOfferForm" id="addAgreementTradesOfferForm" method="post">
        <table style="width:100%;" cellspacing="0" cellpadding="0">
            <tr>
                <td class="font-12" width="<?=$witdh?>" style="text-align:left;"><?php echo t($t_base.'fields/country');?></td>
                <td class="font-12" width="<?=$witdh?>" style="text-align:left;"><?php echo t($t_base.'fields/trade');?></td>
                  <?php
                    if((int)$_SESSION['forwarder_user_id']>0)
                    {
                        ?>
                    
                <td width="48%">&nbsp;</td>
                    <?php }?>
            </tr>
            <tr>
                <td>
                    <select name="agreeTradesOfferArr[idCountryFrom]" id="idCountryFrom" onchange="activateAgreementTradeOffered();">
                        <option value=""><?php echo t($t_base.'fields/select');?></option>
                            <?php
                            if(!empty($getAllCountry))
                            {
                                foreach($getAllCountry as $getAllCountrys)
                                {?>
                                        <option value="<?php echo $getAllCountrys['id']?>" <?php if($_POST['agreeTradesOfferArr']['idCountryFrom']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName']?></option>
                                <?php	
                                }
                            }?>
                    </select>
                </td>
                <td>
                    <select name="agreeTradesOfferArr[iTrade]" id="iTrade" onchange="activateAgreementTradeOffered();">
                        <option value=""><?php echo t($t_base.'fields/select');?></option>
                        <option value="1" <?php if($_POST['agreeTradesOfferArr']['iTrade']==1){?>selected<?php }?>>Import</option>
                        <option value="2" <?php if($_POST['agreeTradesOfferArr']['iTrade']==2){?>selected<?php }?>>Export</option>
                        <option value="3" <?php if($_POST['agreeTradesOfferArr']['iTrade']==3){?>selected<?php }?>>Domestic</option> 
                    </select>
                    <input type="hidden" name="agreeTradesOfferArr[idCourierAgreeProvider]" id="idCourierAgreeProvider" value="<?php echo $_POST['agreeTradesOfferArr']['idCourierAgreeProvider']?>">
                    <input type="hidden" name="agreeTradesOfferArr[id]" id="id" value="<?php echo $_POST['agreeTradesOfferArr']['id']?>">
                </td>
                <?php
                    if((int)$_SESSION['forwarder_user_id']>0)
                    {
                        ?>
                <td align="right">
                    <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_agree_trade_offered" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/remove');?></span></a>
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_agree_trade_offered"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                    
                </td>
                    <?php }?>
            </tr>
            <?php
                    if((int)$_SESSION['forwarder_user_id']==0)
                    {
                        ?>
               <tr>
                <td colspan="2" style="text-align:right;padding-top: 10px;">
                   
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_agree_trade_offered"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                     <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_agree_trade_offered" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/remove');?></span></a>
                    
                </td>
               </tr>
                    <?php }?>
        </table>
    </form>
<?php
}

function addDisplayCouierAgreementPricing($kCourierServices,$bDisabledFlag=false)
{
    $t_base = "SERVICEOFFERING/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    }  
    if($_POST['agreeTradesOfferArr']['idCourierAgreeProvider']>0 && !$bDisabledFlag)
    {
        //$courierAgreementArr=$kCourierServices->getCourierAgreementData($_POST['agreeTradesOfferArr']['idCourierAgreeProvider']);
        $courierAgreementArr=$kCourierServices->getCourierAgreeServiceData($_POST['agreeTradesOfferArr']['idCourierAgreeProvider']); 
         
        $_POST['pricingArr']['iBookingIncluded'] = $courierAgreementArr[0]['iBookingIncluded'];
        $_POST['pricingArr']['fMarkupPercent'] = $courierAgreementArr[0]['fMarkupPercent'];
        $_POST['pricingArr']['fMarkupperShipment'] = $courierAgreementArr[0]['fMarkupperShipment'];
        $_POST['pricingArr']['fMinimumMarkup'] = $courierAgreementArr[0]['fMinimumMarkup'];
        $_POST['pricingArr']['idForwarder'] = $courierAgreementArr[0]['idForwarder'];
        $_POST['pricingArr']['idCourierAgreeProvider'] = $_POST['agreeTradesOfferArr']['idCourierAgreeProvider'];
        $_POST['pricingArr']['idCourierServiceOffered'] = $courierAgreementArr[0]['id']; 
    } 
    
    if($_SESSION['forwarder_id']>0)
    {
        $idForwarder =$_SESSION['forwarder_id'];
    }
    else
    {
        $idForwarder =$_POST['pricingArr']['idForwarder'];
    }
    $kForwarder = new cForwarder();
    $kWhsSearch = new cWHSSearch();
    
    $kForwarder->load($idForwarder);
    $idCurrency = $kForwarder->szCurrency; 
    
    if($idCurrency==1)
    {
        $szForwarderCurrency = 'USD';
    }
    else
    {
        $currencyResultAry = array();
        $currencyResultAry = $kWhsSearch->getCurrencyDetails($idCurrency);	 
        $szForwarderCurrency = $currencyResultAry['szCurrency']; 
    }
    $szDisabledString = '';
    if($bDisabledFlag)
    {
        $szDisabledString = "disabled='disabled'";
    }
?>
     <hr class="cargo-top-line">           
    <div id="insurance_couier_provider_list_container">  
        <form name="addAgreementPricingForm" id="addAgreementPricingForm" method="post">
                <table width="100%" cellpadding="1" cellspacing="0">
                    <tr>
                        <td style="width:100%;" colspan="2"><h4><strong><?php echo t($t_base.'title/pricing');?></strong></h4></td>
                    </tr>
                    <tr>
                        <td style="width:85%;"><?=t($t_base.'fields/booking_provider_sending_shipper');?></td>
                        <td style="width:15%">
                            <select <?php echo $szDisabledString; ?> id="iBookingIncluded" name="pricingArr[iBookingIncluded]" style="width:100%" onchange="activateAgreementPricing();">
                                <option value="0" <?php if($_POST['pricingArr']['iBookingIncluded']==0){?>selected<?php }?>><?=t($t_base.'fields/no');?></option>
                                <option value="1" <?php if($_POST['pricingArr']['iBookingIncluded']==1){?>selected<?php }?>><?=t($t_base.'fields/yes');?></option>
                            </select> 
                        </td>
                    </tr>
                    <tr>
                        <td><?=t($t_base.'fields/markup_on_buy_rate');?></td>
                        <td><input <?php echo $szDisabledString; ?> type="text" onkeyup="activateAgreementPricing();" style="width:79%;" value="<?php echo number_format((float)$_POST['pricingArr']['fMarkupPercent'],1, '.', '');?>" id="fMarkupPercent" name="pricingArr[fMarkupPercent]"> %</td>
                    </tr>
                    <tr>
                        <td><?=t($t_base.'fields/minimum_markup_on_buy_rate');?> </td>
                        <td><?php echo $szForwarderCurrency; ?> <input <?php echo $szDisabledString; ?> type="text" onkeyup="activateAgreementPricing();" style="width:64%;" value="<?php echo number_format((float)$_POST['pricingArr']['fMinimumMarkup'], 2, '.', '');?>" id="fMinimumMarkup" name="pricingArr[fMinimumMarkup]"></td>
                    </tr>
                    <tr>
                        <td><?=t($t_base.'fields/additional_fixed_markup_for_every_shipment');?></td>
                        <td><?php echo $szForwarderCurrency; ?> <input type="text" <?php echo $szDisabledString; ?> onkeyup="activateAgreementPricing();" style="width:64%;" value="<?php echo number_format((float)$_POST['pricingArr']['fMarkupperShipment'], 2, '.', '');?>" id="fMarkupperShipment" name="pricingArr[fMarkupperShipment]"></td>
                    </tr>
                    <tr>
                        <td style="width:100%;" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <input type="hidden" name="pricingArr[idCourierAgreeProvider]" id="idCourierAgreeProvider" value="<?php echo $_POST['pricingArr']['idCourierAgreeProvider']?>">
                            <input type="hidden" name="pricingArr[idCourierServiceOffered]" id="idCourierServiceOffered" value="<?php echo $_POST['pricingArr']['idCourierServiceOffered']?>">
                            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_agree_pricing"><span style="min-width:50px;"><?=t($t_base.'fields/save_changes');?></span></a>
                        </td>
                    </tr>
                </table>
            </form> 
	</div>

<?php

}

function checkFedexLoginDetails($data)
{  
    $newline = "<br />";  
    //Please include and reference in $path_to_wsdl variable.
    $path_to_wsdl = __APP_PATH__."/wsdl/RateService_v14.wsdl";

    //The WSDL is not included with the sample code.
    $client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

    $request['WebAuthenticationDetail'] = array(
            'UserCredential' =>array(
                    'Key' => $data['szUsername'], 
                    'Password' => $data['szPassword']
            )
    ); 
    $request['ClientDetail'] = array(
            'AccountNumber' => $data['szAccountNumber'], 
            'MeterNumber' => $data['szMeterNumber']
    );
    $request['TransactionDetail'] = array('CustomerTransactionId' => md5(time()));
    $request['Version'] = array(
            'ServiceId' => 'crs', 
            'Major' => '14', 
            'Intermediate' => '0', 
            'Minor' => '0'
    );

    $serviceType = 'INTERNATIONAL_PRIORITY'; 

    $request['ReturnTransitAndCommit'] = true;
    $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
    $request['RequestedShipment']['ShipTimestamp'] = date('c');


    $request['RequestedShipment']['Shipper'] = addShipper();
    $request['RequestedShipment']['Recipient'] = addRecipient();

    //$request['RequestedShipment']['ShippingChargesPayment'] = $this->addShippingChargesPayment();		
    $request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT'; 
    $request['RequestedShipment']['RateRequestTypes'] = 'LIST'; 
    $request['RequestedShipment']['PurposeOfShipmentType'] = 'SAMPLE';
    $request['RequestedShipment']['PackageCount'] = '1';
    $request['RequestedShipment']['RequestedPackageLineItems'] = addPackageLineItemTest();
 
    try {
     $obj = $client->getRates($request);
    } catch (SoapFault $exception) {

    } 
    if (is_object($obj)) $obj = (array)$obj;
    if (is_array($obj)) {
        $newArray = array();
        foreach ($obj as $key => $val) {
            $newArray[$key] = toArray($val);
        }
    } else {
        $newArray = $obj;
    } 
    if(strtoupper($newArray['HighestSeverity'])=='SUCCESS')
    {
        return 'SUCCESS';
    }
    else
    {
        return 'ERROR';
    }  
}
    
    
    function addPackageLineItemTest()
    {
        $packageLineItem = array(
            'SequenceNumber'=>1,
            'GroupPackageCount'=>1,
            'Weight' => array(
            'Value' => '12',
            'Units' => 'KG'
            )
        );
        return $packageLineItem;
    }
    
    function addShipper()
    {
        $szAddress = "Apt 21";
        $shipper = array(
            'Contact' => array(
            'PersonName' => "Morten Loekholm",
            'CompanyName' => "Transporteca",
            'PhoneNumber' => "9878675656"
            ),
            'Address' => array(
            'StreetLines' => array($szAddress),
            'City' => "Copenhagen",
            'PostalCode' => "2300",
            'CountryCode' => "DK"
            )
	);
        return $shipper;
    }
    
    function addRecipient()
    {
        $szAddress = "Bandra";
        $recipient = array(
            'Contact' => array(
            'PersonName' => "Thorsten Boek",
            'CompanyName' => "Transporteca",
            'PhoneNumber' => "9878675656"
            ),
            'Address' => array(
            'StreetLines' => array($szAddress),
            'City' => "Mumbai",
            'PostalCode' => "400001",
            'CountryCode' => "IN"
            )
	);
        return $recipient;
    }
    
    function checkDHLLoginDetails()
    {
        //@TO DO
        return "SUCCESS";
    }
    function checkUPSLoginDetails($data)
    {  
        //Configuration
        $access = $data['szAccountNumber'];
        $userid = $data['szUsername'];
        $passwd = $data['szPassword']; 
        $wsdl = __APP_PATH__."/wsdl/RateWS.wsdl";
        $operation = "ProcessRate";
        $endpointurl = 'http://wwwapps.ups.com/ctc/htmlTool';
        $outputFileName = __APP_PATH__."/wsdl/XOLTResult.xml";
  
        try
        { 
            $mode = array
            (
                 'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                 'trace' => 1
            ); 
            // initialize soap client
            $client = new SoapClient($wsdl , $mode);
            
             //set endpoint url
		   // $client->__setLocation($endpointurl);
 
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;
			
            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss); 
            $client->__setSoapHeaders($header); 
            
            //get response
            $resp = $client->__soapCall($operation ,array(processRateDemo())); 
              
            //get status
            $szResponseStatus = $resp->Response->ResponseStatus->Description; 
            if(strtolower($szResponseStatus)=='success' || strtolower($szResponseStatus)=='warning')
            {
               return "SUCCESS";
            } 
            else
            {
                return "ERROR";
            }
        }
        catch(Exception $ex)
        {   
            return "ERROR";
        }
    } 
    
    
    function processRateDemo()
    {
      //create soap request
      $option['RequestOption'] = 'Shop';
      $request['Request'] = $option;

      $pickuptype['Code'] = '01';
      $pickuptype['Description'] = 'Daily Pickup';
      $request['PickupType'] = $pickuptype;

      $customerclassification['Code'] = '01';
      $customerclassification['Description'] = 'Classfication';
      $request['CustomerClassification'] = $customerclassification;

      $shipper['Name'] = 'Imani Carr';
      $shipper['ShipperNumber'] = '222006';
      $address['AddressLine'] = array
      (
          'Southam Rd',
          '4 Case Cour',
          'Apt 3B'
      );
      $address['City'] = 'Timonium';
      $address['StateProvinceCode'] = 'MD';
      $address['PostalCode'] = '21093';
      $address['CountryCode'] = 'US';
      $shipper['Address'] = $address;
      $shipment['Shipper'] = $shipper;

      $shipto['Name'] = 'Imani Imaginarium';
      $addressTo['AddressLine'] = '21 ARGONAUT SUITE B';
      $addressTo['City'] = 'ALISO VIEJO';
      $addressTo['StateProvinceCode'] = 'CA';
      $addressTo['PostalCode'] = '92656';
      $addressTo['CountryCode'] = 'US';
      $addressTo['ResidentialAddressIndicator'] = '';
      $shipto['Address'] = $addressTo;
      $shipment['ShipTo'] = $shipto;

      $shipfrom['Name'] = 'Imani Imaginarium';
      $addressFrom['AddressLine'] = array
      (
          'Southam Rd',
          '4 Case Court',
          'Apt 3B'
      );
      $addressFrom['City'] = 'Timonium';
      $addressFrom['StateProvinceCode'] = 'MD';
      $addressFrom['PostalCode'] = '21093';
      $addressFrom['CountryCode'] = 'US';
      $shipfrom['Address'] = $addressFrom;
      $shipment['ShipFrom'] = $shipfrom;

      $service['Code'] = '03';
      $service['Description'] = 'Service Code';
      $shipment['Service'] = $service;

      $packaging1['Code'] = '02';
      $packaging1['Description'] = 'Rate';
      $package1['PackagingType'] = $packaging1;
      $dunit1['Code'] = 'IN';
      $dunit1['Description'] = 'inches';
      $dimensions1['Length'] = '5';
      $dimensions1['Width'] = '4';
      $dimensions1['Height'] = '10';
      $dimensions1['UnitOfMeasurement'] = $dunit1;
      $package1['Dimensions'] = $dimensions1;
      $punit1['Code'] = 'LBS';
      $punit1['Description'] = 'Pounds';
      $packageweight1['Weight'] = '1';
      $packageweight1['UnitOfMeasurement'] = $punit1;
      $package1['PackageWeight'] = $packageweight1;

      $packaging2['Code'] = '02';
      $packaging2['Description'] = 'Rate';
      $package2['PackagingType'] = $packaging2;
      $dunit2['Code'] = 'IN';
      $dunit2['Description'] = 'inches';
      $dimensions2['Length'] = '3';
      $dimensions2['Width'] = '5';
      $dimensions2['Height'] = '8';
      $dimensions2['UnitOfMeasurement'] = $dunit2;
      $package2['Dimensions'] = $dimensions2;
      $punit2['Code'] = 'LBS';
      $punit2['Description'] = 'Pounds';
      $packageweight2['Weight'] = '2';
      $packageweight2['UnitOfMeasurement'] = $punit2;
      $package2['PackageWeight'] = $packageweight2;

      $shipment['Package'] = array(	$package1 , $package2 );
      $shipment['ShipmentServiceOptions'] = '';
      $shipment['LargePackageIndicator'] = '';
      $request['Shipment'] = $shipment;
      return $request;
  }
  
function encrypt($string, $key)
{
    return $string ;
    //return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
}

function decrypt($encrypted, $key)
{
    return $encrypted ;
    //return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
}

function display_courier_confirmed_booking_listing($searchResult,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)
{  
    $t_base="SERVICEOFFERING/";
    $iColspan=8;
    $iGrandTotal=count($searchResult);
    require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
    $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
    
    if($iPage==1)
    {
        $start=0;
        $end=$limit;
        if($iGrandTotal<$end)
        {
            $end=$iGrandTotal;
        }
    }
    else
    {
        $start=$limit*($iPage-1);
        $end=$iPage*$limit;
        if($end>$iGrandTotal)
        {
            $end=$iGrandTotal;
        }
        $startValue=$start+1;
        if($startValue>$iGrandTotal)
        {
            $iPage=$iPage-1;
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            
        }
    }
    $kBooking = new cBooking();
    $dtCurrentTime = $kBooking->getRealNow();
    
    ?>   
    <form id="confirm_insured_booking_form" name="confirm_insured_booking_form" action="javascript:void(0);" method="post">
    <table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="booking_table">
        <tr>		
            <th style="width:5%;" valign="top"><?=t($t_base.'fields/date')?></th>
            <th style="width:7%;" valign="top"><?=t($t_base.'fields/booking')?></th>
            <th style="width:13%;" valign="top"><?=t($t_base.'fields/from');?></th>
            <th style="width:12%;" valign="top"><?=t($t_base.'fields/to');?></th>
            <th style="width:12%;" valign="top"><?=t($t_base.'fields/forwarder');?></th>
            <th style="width:13%;" valign="top"><?=t($t_base.'fields/courier_product');?></th>
            <th style="width:12%;" valign="top"><?=t($t_base.'fields/labels_by');?></th> 
        </tr>
        <?php
                $date2DayOld=date('Y-m-d H:i:s',strtotime('- 24 HOUR'));
                //echo $date2DayOld."$date2DayOld";
		if(!empty($searchResult))
                {
                    $ctr = 1;
                    for($i=$start;$i<$end;++$i)
                    { 
                        $szWarehouseFromCity=createSubString($searchResult[$i]['szShipperCity'],0,15);
                        $szWarehouseToCity=createSubString($searchResult[$i]['szConsigneeCity'],0,15); 
                        $idWarehouseFromCountry = $searchResult[$i]['idOriginCountry'] ;
                        $idWarehouseToCountry = $searchResult[$i]['idDestinationCountry'] ;
                       
                        if((int)$searchResult[$i]['iCourierAgreementIncluded']==1)
                        {
                            $labelBy=returnLimitData($searchResult[$i]['szDisplayName'],16);
                        }
                        else
                        {
                            $labelBy="Transporteca";
                        } 
                        if(!empty($searchResult[$i]['szProviderName']))
                        {
                            $courierProviderProductTrim = '';
                            $courierProviderProduct = $searchResult[$i]['szProviderName']." ".$searchResult[$i]['szProviderProductName'];
                            $courierProviderProductTrim = returnLimitData($courierProviderProduct,16);
                        } 
                        else
                        {
                            $courierProviderProductTrim = "N/A";
                        }
                        
                        $style='';
                        $showSnoozeFlag=false; 
                        if(strtotime(date('Y-m-d H:i:s',strtotime($dtCurrentTime)))>strtotime($searchResult[$i]['dtSnoozeDate']) && $searchResult[$i]['iSnoozeAdded']==0)
                        {
                            $showSnoozeFlag=true;
                        }
                        if(strtotime(date('Y-m-d H:i:s',strtotime($dtCurrentTime)))>strtotime($searchResult[$i]['dtSnooze']) && $showSnoozeFlag)
                        {
                            if(!empty($searchResult[$i]['dtBookingConfirmed']) && $searchResult[$i]['dtBookingConfirmed']!='0000-00-00 00:00:00')
                            {
                                if(strtotime($date2DayOld)>strtotime($searchResult[$i]['dtBookingConfirmed']))
                                {
                                    $style="class='snoozeDate'";
                                }
                            }
                        }
                        ?>						
                        <tr id="booking_data_<?=$ctr?>" onclick="select_forwarder_confirmation_tr('booking_data_<?=$ctr?>','<?=$searchResult[$i]['id']?>')">
                            <td <?=$style;?> <?php if($style!=''){?> onclick="openSnoozePopup('LABELNOTSENT','<?=$searchResult[$i]['id']?>');" <?php }?> id="LABELNOTSENT_<?=$searchResult[$i]['id']?>"><?=(!empty($searchResult[$i]['dtBookingConfirmed']) && $searchResult[$i]['dtBookingConfirmed']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResult[$i]['dtBookingConfirmed'])):''?></td>
                            <td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $searchResult[$i]['szBookingRandomNum'];?>/"><?=$searchResult[$i]['szBookingRef']?></a></td>
                            <td><?=$szWarehouseFromCity." (".isoidCountryCode($idWarehouseFromCountry).")"?></td>
                            <td><?=$szWarehouseToCity." (".isoidCountryCode($idWarehouseToCountry).")"?></td>
                            <td><?php echo (!empty($searchResult[$i]['szDisplayName'])? returnLimitData($searchResult[$i]['szDisplayName'],16):'N/A'); ?></td>
                            <td title="<?php echo $courierProviderProduct;?>"><?php echo (!empty($courierProviderProductTrim)?$courierProviderProductTrim:'N/A'); ?></td>
                            <td><?=$labelBy?></td>	                                                                
                        </tr> 
                        <?php
                        $ctr++;
                    }
                }
                else
                {
                    ?>
                    <tr>
                        <td colspan="7" align="center"><h4><b><?=t($t_base.'fields/no_record_found');?></b></h4></td>
                    </tr>
                    <?php
                }
		?>
	</table>
	<div id="invoice_comment_div" style="display:none">
	</div>
	<br>
        <div class="page_limit clearfix">
        <div class="pagination">
         <?php
        
        $kPagination->szMode = "DISPLAY_LABEL_LIST_PAGINATION";
        //$kPagination->idBookingFile = $idBookingFile;
        $kPagination->paginate('display_label_list_pagination'); 
        if($kPagination->links_per_page>1)
        {
            echo $kPagination->renderFullNav();
        }
        ?>
        </div>
               <div class="limit">Records per page
                   <select id="limit" name="limit" onchange="display_label_list_pagination('1')">
                       <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                        <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                        <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                        <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                   </select>
               </div>    
            </div> 
        <br>
        <div>
            <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="label_button"><span><?=t($t_base.'fields/upload_labels');?></span></a>
            <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="courier_label_button_sent"><span><?=t($t_base.'fields/label_sent');?></span></a>
            <span style="float: right;"> 
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_invoice"><span><?=t($t_base.'fields/invoice');?></span></a> 
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_confirmation"><span><?=t($t_base.'fields/confirmation');?></span></a>
                <a id="download_booking_lcl_booking" class="button1" style="opacity:0.4" href="javascript:void(0)">
                    <span><?=t($t_base.'fields/view_booking');?></span>
                </a> 
            </span>
	</div>
       
	<?php
}
function labelSentHtml($idBooking,$kCourierServices)
{
    $bookingDetail = $kCourierServices->getAllNewBookingWithCourier($idBooking);
    
    if(empty($_POST['szTrackingNumber']))
    {
        $_POST['szTrackingNumber']=$bookingDetail[0]['szTrackingNumber']; 
    }
    
    if(isset($_POST['iSendTrackingUpdates']))
    {
        $iSendTrackingUpdates = $_POST['iSendTrackingUpdates']; 
    }
    else
    {
        $iSendTrackingUpdates = $bookingDetail[0]['iSendTrackingUpdates'];
    } 
    $t_base="SERVICEOFFERING/"; 
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            if($errorKey=='szAfterShipTrackingNotification')
            {
                $szAfterShipTrackingNotification = $errorValue;
            }
            else
            {
                ?>
                <script type="text/javascript">
                    $("#<?php echo $errorKey?>").addClass('red_border');
                </script>
                <?php 
            } 
        }
    }
?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup">
            <h3><?=t($t_base.'title/master_tracking_number');?> </h3><br/>
            <?php
                if(!empty($szAfterShipTrackingNotification))
                {
                    echo '<p class="red_text">'.$szAfterShipTrackingNotification."</p><br>";
                } 
            ?>
            <p><?=t($t_base.'messages/track_msg_1');?> <?php echo $bookingDetail[0]['szProviderName'];?> <?=t($t_base.'messages/track_msg_2');?>.</p><br/>
            <p><input style="text-transform: uppercase;" type="text" name="szTrackingNumber" id="szTrackingNumber" value="<?php echo $_POST['szTrackingNumber'];?>"/>
                <input type="hidden" name="idBooking" id="idBooking" value="<?php echo $idBooking;?>"/>
                <input type="hidden" name="modeHidden" id="modeHidden" value="<?php echo $_POST['modeHidden'];?>"/>
            </p><br/>
            <p class="private-cb">
                <input type="checkbox" value="1" <?php if($iSendTrackingUpdates!=1){ echo 'checked'; }?> id="iSendTrackingUpdates" name="iSendTrackingUpdates">&nbsp; Do not send tracking updates to customer
            </p> <br/> 
            <p align="center">
                <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
                <a href="javascript:void(0)" class="button1" onclick="sendBookingLabel('<?php echo $idBooking;?>')" id="copy_button"><span><?php if($bookingDetail[0]['idBookingStatus']==__COURIER_BOOKING_LABEL_SENT_TO_SHIPPER__){ echo t($t_base.'fields/save');}else{ echo t($t_base.'fields/confirm'); }?></span></a>
            </p>
	</div>
    </div>
	<?php

}
function display_courier_confirmed_booking_listing_send($searchResult,$flag='',$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)
{  
    $t_base="SERVICEOFFERING/";
    $iColspan=8;
    $iGrandTotal=count($searchResult);
    require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
    $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
    
    if($iPage==1)
    {
        $start=0;
        $end=$limit;
        if($iGrandTotal<$end)
        {
            $end=$iGrandTotal;
        }
    }
    else
    {
        $start=$limit*($iPage-1);
        $end=$iPage*$limit;
        if($end>$iGrandTotal)
        {
            $end=$iGrandTotal;
        }
        $startValue=$start+1;
        if($startValue>$iGrandTotal)
        {
            $iPage=$iPage-1;
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            
        }
    }
    ?>   
    <form id="confirm_insured_booking_form" name="confirm_insured_booking_form" action="javascript:void(0);" method="post">
    <table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="booking_table">
        <tr>		
            <th style="width:5%;" valign="top"><?=t($t_base.'fields/date')?></th>
            <th style="width:9%;" valign="top"><?=t($t_base.'fields/booking')?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/from');?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/to');?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/forwarder');?></th>
            <th style="width:16%;" valign="top"><?=t($t_base.'fields/courier_product');?></th>
            <?php if($flag==__COURIER_BOOKING_LABEL_DOWNLOADED_BY_SHIPPER__){
               
                ?>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/download');?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/downloaded_by');?></th> 
            
            <?php }else{
                
                ?>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/labels_by');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/labels_sent');?></th> 
            <?php }?>
        </tr>
        <?php
                if(!empty($searchResult))
                {
                    $ctr = 1;
                    $date2DayOld=date('Y-m-d',strtotime('- 1 DAY'));
                    for($i=$start;$i<$end;++$i)
                    { 
                        $szWarehouseFromCity=createSubString($searchResult[$i]['szShipperCity'],0,12);
                        $szWarehouseToCity=createSubString($searchResult[$i]['szConsigneeCity'],0,12); 
                        $idWarehouseFromCountry = $searchResult[$i]['idOriginCountry'] ;
                        $idWarehouseToCountry = $searchResult[$i]['idDestinationCountry'] ; 
                        if((int)$searchResult[$i]['iCourierAgreementIncluded']==1)
                            $labelBy=returnLimitData($searchResult[$i]['szDisplayName'],12);
                        else
                            $labelBy="Transporteca";  
                        $shipperName='';
                        if($flag==__COURIER_BOOKING_LABEL_DOWNLOADED_BY_SHIPPER__)
                        {
                            $kUser = new cUser();
                            $kUser->getUserDetails($searchResult[$i]['idLabelDownloadedBy']);
                            $shipperName=$kUser->szFirstName." ".$kUser->szLastName;
                        }
                        if(!empty($searchResult[$i]['szProviderName']))
                        {
                            $courierProviderProductTrim='';
                            $courierProviderProduct=$searchResult[$i]['szProviderName']." ".$searchResult[$i]['szProviderProductName'];
                            $courierProviderProductTrim=returnLimitData($courierProviderProduct,16); 
                        }
                        else
                        {
                            $courierProviderProductTrim = "N/A";
                        }
                        
                        $style='';
                        if($flag=='')
                        {
                           
                            if(strtotime(date('Y-m-d'))>strtotime($searchResult[$i]['dtSnooze'])){
                                if(!empty($searchResult[$i]['dtLabelSent']) && $searchResult[$i]['dtLabelSent']!='0000-00-00 00:00:00')
                                {

                                    if(strtotime($date2DayOld)>strtotime($searchResult[$i]['dtLabelSent']))
                                    {
                                        $style="class='snoozeDate'";
                                    }
                                }
                            }
                        }
                        ?>						
                        <tr id="booking_data_<?=$ctr?>" onclick="select_forwarder_confirmation_tr('booking_data_<?=$ctr?>','<?=$searchResult[$i]['id']?>')">
                            <td><?=(!empty($searchResult[$i]['dtBookingConfirmed']) && $searchResult[$i]['dtBookingConfirmed']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResult[$i]['dtBookingConfirmed'])):''?></td>
                            <td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $searchResult[$i]['szBookingRandomNum'];?>/"><?=$searchResult[$i]['szBookingRef']?></a></td>
                            <td><?=$szWarehouseFromCity." (".isoidCountryCode($idWarehouseFromCountry).")"?></td>
                            <td><?=$szWarehouseToCity." (".isoidCountryCode($idWarehouseToCountry).")"?></td>
                            <td><?=returnLimitData($searchResult[$i]['szDisplayName'],12)?></td>
                            <td title="<?php echo $courierProviderProduct;?>"><?=$courierProviderProductTrim?></td>
                            <?php if($flag==__COURIER_BOOKING_LABEL_DOWNLOADED_BY_SHIPPER__){?>
                            <td><?=(!empty($searchResult[$i]['dtLabelDownloaded']) && $searchResult[$i]['dtLabelDownloaded']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResult[$i]['dtLabelDownloaded'])):''?></td>
                            <td><?php echo returnLimitData($shipperName,16);?></td>
                            <?php }else{?>
                            <td><?=$labelBy?></td>
                            <td <?=$style;?> <?php if($style!=''){?> id="LABELSENT_<?=$searchResult[$i]['id']?>" onclick="openSnoozePopup('LABELSENT','<?=$searchResult[$i]['id']?>');" <?php }?>><?=(!empty($searchResult[$i]['dtLabelSent']) && $searchResult[$i]['dtLabelSent']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResult[$i]['dtLabelSent'])):''?></td>
                            <?php }?>	                                                                
                        </tr>
                        <?php
                        $ctr++;
                    }
                }
                else
                {
                    ?>
                    <tr>
                            <td colspan="8" align="center"><h4><b><?=t($t_base.'fields/no_record_found');?></b></h4></td>
                    </tr>
                    <?php
                }
		?>
	</table>
	<div id="invoice_comment_div" style="display:none">
	</div>
        <div class="page_limit clearfix">
        <div class="pagination">
        <?php
        
        $kPagination->szMode = "DISPLAY_LABEL_LIST_PAGINATION";
        //$kPagination->idBookingFile = $idBookingFile;
        $kPagination->paginate('display_label_list_pagination'); 
        if($kPagination->links_per_page>1)
        {
            echo $kPagination->renderFullNav();
        }
        
    ?>
	</div>
               <div class="limit">Records per page
                   <select id="limit" name="limit" onchange="display_label_list_pagination('1')">
                       <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                        <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                        <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                        <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                   </select>
               </div>    
            </div>
	<br>
	<div class="clearfix">
                <span class="left-buttons">
		<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="label_resend"><span><?=t($t_base.'fields/resend_labels');?></span></a>
                <?php if($flag!=__COURIER_BOOKING_LABEL_DOWNLOADED_BY_SHIPPER__){?>
                <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="label_download"><span><?=t($t_base.'fields/download');?></span></a>
                <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="label_button_sent"><span><?=t($t_base.'fields/tracking_no');?></span></a>
		<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="label_button_new_sent"><span><?=t($t_base.'fields/new_labels');?></span></a>
                 <?php }else{?>
                
                
		<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="label_button_new_sent"><span><?=t($t_base.'fields/new_labels');?></span></a>
                 <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="label_button_sent"><span><?=t($t_base.'fields/tracking_no');?></span></a>
                 <?php }?>
		
                </span>  
		<span class="right-buttons">
			
                        <a href="javascript:void(0)" class="button1" target="_blank" style="opacity:0.4" id="see_booking_courier_label"><span><?=t($t_base.'fields/see_labels');?></span></a>    
			<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_invoice"><span><?=t($t_base.'fields/invoice');?></span></a>
		
			<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_confirmation"><span><?=t($t_base.'fields/confirmation');?></span></a>
			<a id="download_booking_lcl_booking" class="button1" style="opacity:0.4" href="javascript:void(0)">
			<span><?=t($t_base.'fields/view_booking');?></span>
			</a>
		
		</span>
	</div>
        
	<?php
}

function resendCourierBookingLabelEmail($kCourierServices)
{
    $t_base = "SERVICEOFFERING/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        {
           
                ?>
                <script type="text/javascript">
                        $("#<?php echo $errorKey?>").addClass('red_border');
                </script>
            <?php 
        }
    }
    ?>
             <div id="popup-bg"></div>
             <div id="popup-container">
                 <div class="popup signin-popup signin-popup-verification">
                     <h5><?php echo t($t_base.'title/resend')?></h5>
                     <p><?php echo t($t_base.'messages/resend_courier_label_msg')?>?</p><br/>
                     <p>
                        <input id="szEmail" type="text" style="width:277px;" value="<?php echo $_POST['szEmail']?>" name="szEmail">
                      </p><br/>
                      <p align="center">
                            <input id="idBooking" type="hidden" value="<?php echo $_POST['idBooking'];?>" name="idBooking">
                            <a id="send_button" class="button1" onclick="resend_courier_label_email_confirm();" href="javascript:void(0);">
                            <span><?php echo t($t_base.'fields/yes')?></span>
                            </a>
                            <a id="cancel_button" class="button2" onclick="showHide('contactPopup');" href="javascript:void(0);">
                            <span><?php echo t($t_base.'fields/no')?></span>
                            </a>
                    </p>
                 </div>
             </div>
    <?php
}
function download_courier_label_html($kCourierServices,$checkFlagLabelUploaded=false)
{
    $t_base = "COURIERLABEL/";
    
    $kConfig = new cConfig();
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
    $mon=date('m')-1;
    $date_picker_argument = "minDate: new Date(".date('Y').", ".$mon.", ".(date('d')).")" ;
    
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        } 
        if($kCourierServices->arErrorMessages['dtShipperPickUp1']!='')
        {
            $_SESSION['secondClickFlag']=1;
        }    
    }
    
    ?>
                    <script type="text/javascript">
                        jQuery().ready(function(){ 
                            
                            Custom.init();
                            $("#dtShipperPickUp").datepicker({<?php echo $date_picker_argument;?>}); 
                            });
                    </script>
             <form id="courier_label_download_form" action="javascript:void(0);" method="post" name="courier_label_download_form">
                <div id="search_header" class="courierLabel clearfix">
                        <div>
                            <span><label><?php echo t($t_base.'fields/company')?></label></span>
                            <span><input id="szShipperCompanyName" name="shipperArr[szShipperCompanyName]" value="<?php echo $_POST['shipperArr']['szShipperCompanyName']?>" tabindex="1"/></span>
                        </div>
                        <div>
                            <span><label><?php echo t($t_base.'fields/first_name')?></label></span>
                            <span><input id="szShipperFirstName" name="shipperArr[szShipperFirstName]" value="<?php echo $_POST['shipperArr']['szShipperFirstName']?>" tabindex="2"/></span>
                        </div>
                         <div>
                            <span><label><?php echo t($t_base.'fields/last_name')?></label></span>
                            <span><input id="szShipperLastName" name="shipperArr[szShipperLastName]" value="<?php echo $_POST['shipperArr']['szShipperLastName']?>" tabindex="3"/></span>
                        </div>
                        <div>
                            <span><label><?php echo t($t_base.'fields/email')?></label></span>
                            <span><input id="szShipperEmail" name="shipperArr[szShipperEmail]" value="<?php echo $_POST['shipperArr']['szShipperEmail']?>" tabindex="4"/></span>
                        </div>
                        <div class="phone-container-div">
                            <span><label><?php echo t($t_base.'fields/phone_number')?></label></span> 
                                <span class="telephone oh">
                                    <select  class="styled" name="shipperArr[idShipperCountry]" id="idShipperCountry" tabindex="5">
                                       <?php
                                          if(!empty($dialUpCodeAry))
                                          {
                                               $usedDialCode = array();
                                              foreach($dialUpCodeAry as $dialUpCodeArys)
                                              {
                                                   //if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                                   //{
                                                      // $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                   ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$_POST['shipperArr']['idShipperCountry']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                                   <?php
                                                   //}
                                              }
                                          }
                                      ?>
                                    </select><input lass="phone-container" tabindex="6" type="text" name="shipperArr[szShipperPhone]" id="szShipperPhone" value="<?php echo $_POST['shipperArr']['szShipperPhone'] ?>" />
                                   </span>                     
                        </div>
                        <div>
                            <span><label><?php echo t($t_base.'fields/shipping_date')?></label></span>
                            <span><input id="dtShipperPickUp" name="shipperArr[dtShipperPickUp]" value="<?php if($_POST['shipperArr']['dtShipperPickUp']!='' && $_POST['shipperArr']['dtShipperPickUp']!='0000-00-00') echo $_POST['shipperArr']['dtShipperPickUp']?>" tabindex="7"/>
                            </span>
                        </div>
                        <?php if($kCourierServices->arErrorMessages['dtShipperPickUp1']!=''){?>
                        <div>
                            <span class="error-msg"><?php if($kCourierServices->arErrorMessages['dtShipperPickUp1']!='') echo $kCourierServices->arErrorMessages['dtShipperPickUp1'];?></span>
                        </div>  
                        <?php }?>
                        <div class="booking_quotes_buttons clearfix">
                            <a id="download_courier_label" <?php if(!$checkFlagLabelUploaded){?> onclick="downloadCourierLabelPDF();" <?php }else{?> style="opacity:0.4" <?php }?> class="button1" tabindex="8" href="javascript:void(0);"><?php echo t($t_base.'fields/confirm_and_download_labels')?></a>
                        </div>
                     
                </div>
                 <input type="hidden" id="idBooking" name="shipperArr[idBooking]" value="<?php echo $_POST['shipperArr']['idBooking']?>" tabindex="7"/>
             </form>
     <?php
}
function downloadLabelHtml($kCourierServices)
{
	 $t_base="SERVICEOFFERING/";
	 
	if(!empty($kCourierServices->arErrorMessages))
        {
            $szValidationErrorKey = '';
            foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
            {

                    ?>
                    <script type="text/javascript">
                            $("#<?php echo $errorKey?>").addClass('red_border');
                    </script>
                <?php 
            }
        }
?>
	<div id="popup-bg"></div>
		<div id="popup-container" >
		<div class="popup">
               <form name="downloadCourierLabelForm" id="downloadCourierLabelForm" method="post">      
		<h3><?=t($t_base.'title/confirm_labels_downloaded');?> </h3><br/>
		<p><?=t($t_base.'messages/download_msg');?>:</p><br/>
		<div class="oh">
                    <p class="fl-33"><?=t($t_base.'fields/first_name');?></p>
                    <p class="fl-65"><input type="text" name="szShipperFirstName" id="szShipperFirstName" value="<?php echo $_POST['szShipperFirstName'];?>"/></p>
                    </div>
                <div class="oh">
                    <p class="fl-33"><?=t($t_base.'fields/last_name');?></p>
                    <p class="fl-65"><input type="text" name="szShipperLastName" id="szShipperLastName" value="<?php echo $_POST['szShipperLastName'];?>"/></p>
                    </div><input type="hidden" name="szShipperEmail" id="szShipperEmail" value="<?php echo $_POST['szShipperEmail'];?>"/>
		<br/>
			<input type="hidden" name="idBooking" id="idBooking" value="<?php echo $_POST['idBooking'];?>"/>
		<br/>
		<p align="center">
			 <a href="javascript:void(0)" class="button2" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
			 <a href="javascript:void(0)" class="button1" onclick="downloadBookingLabel()" id="copy_button"><span><?php if($bookingDetail[0]['idBookingStatus']==__COURIER_BOOKING_LABEL_SENT_TO_SHIPPER__){ echo t($t_base.'fields/save');} else{ echo t($t_base.'fields/confirm'); }?></span></a>
		</p>
               </form>
                
	</div>
</div>
	<?php

}

function fedexTrackingNumberApi($idBooking,$szTrackingNumber)
{
    $kBooking = new cBooking();
    $kCourierServices = new cCourierServices();
//$idBooking = (int)$_POST['idBooking'];
    //echo $idBooking;
    if((int)$idBooking>0)
    {
        $bookingArr=$kBooking->getExtendedBookingDetails($idBooking);
        //print_r($bookingArr);
        $courierForwarderAgreementArr=$kCourierServices->getCourierAgreementData($bookingArr['idServiceProvider'],'','',true);
        //print_r($courierForwarderAgreementArr);
        $szAccountNumber=decrypt($courierForwarderAgreementArr[0]['szAccountNumber'],ENCRYPT_KEY);
        $szUsername=decrypt($courierForwarderAgreementArr[0]['szUsername'],ENCRYPT_KEY);
        $szMeterNumber=decrypt($courierForwarderAgreementArr[0]['szMeterNumber'],ENCRYPT_KEY);
        $szPassword=decrypt($courierForwarderAgreementArr[0]['szPassword'],ENCRYPT_KEY);

        //print_r($courierForwarderAgreementArr);
        //The WSDL is not included with the sample code.
        //Please include and reference in $path_to_wsdl variable.
        $path_to_wsdl = __APP_PATH__."/wsdl/TrackService_v9.wsdl";

        ini_set("soap.wsdl_cache_enabled", "0");

        $client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

        $request['WebAuthenticationDetail'] = array(
                'UserCredential' =>array(
                        'Key' => $szUsername, 
                        'Password' => $szPassword
                )
        );
        $request['ClientDetail'] = array(
                'AccountNumber' => $szAccountNumber, 
                'MeterNumber' => $szMeterNumber
        );

        $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Track Request using PHP ***');

        $request['Version'] = array(
                'ServiceId' => 'trck', 
                'Major' => '9', 
                'Intermediate' => '1', 
                'Minor' => '0'
        );
        $request['SelectionDetails'] = array(
                'PackageIdentifier' => array(
                        'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
                        'Value' => $szTrackingNumber // Replace 'XXX' with a valid tracking identifier
                )
        );
        //print_r($request);
        try {
                if(setEndpoint('changeEndpoint')){
                        $newLocation = $client->__setLocation(setEndpoint('endpoint'));
                }

                $response = $client ->track($request);
                //print_r($response);
                $successFlag=false;
            if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
                        if($response->HighestSeverity != 'SUCCESS'){
                                $resArr=toArray($response->CompletedTrackDetails);
                              // echo "hello";
                               //print_r($resArr);
                                 return "AWB does not exist";

                        }else{
                        if ($response->CompletedTrackDetails->HighestSeverity != 'SUCCESS'){
                                    $resArr=toArray($response->CompletedTrackDetails);
                                     //echo "hello1";
                                        //print_r($resArr);
                                        return "AWB does not exist";
                                }else{
                                    $resArr=toArray($response->CompletedTrackDetails);
                                     //echo "hello2";
                                    //print_r($resArr);
                                    $trackingStatusArr=$resArr['TrackDetails']['StatusDetail'];
                                    if($resArr['TrackDetails']['StatusDetail']['Code']!='')
                                    {
                                         //echo "hello3";
                                        //$kCourierServices->updateBookingCourierStatus($trackingStatusArr,$idBooking);
                                        return $resArr['TrackDetails']['StatusDetail']['Description'];
                                    }
                                    else if($resArr['TrackDetails']['Notification']['Code']!='')
                                    {
                                         //echo "hello3";   
                                         return $resArr['TrackDetails']['Notification']['Message'];
                                    }
                                    
                                    //if($resArr['Notifications'])
                                    
                                    //$successFlag=true;
                                }
                        }
                //printSuccess($client, $response);
            }else{
                //printError($client, $response);
                 $resArr=toArray($response);
                 //print_r($resArr['Notifications']['Message']);
                 return $resArr['Notifications']['Message'];
            } 

           // writeToLog($client);    // Write to log file   
        } catch (SoapFault $exception) {
            //printFault($exception, $client);
             return "AWB does not exist";
        }
    }
}

function upsTrackingNumberApi($idBooking,$szTrackingNumber)
{
     $kBooking = new cBooking();
    $kCourierServices = new cCourierServices();
//$idBooking = (int)$_POST['idBooking'];
    if((int)$idBooking>0)
    {
        $bookingArr=$kBooking->getExtendedBookingDetails($idBooking);
        //echo $bookingArr['szTrackingNumber'];
        $courierForwarderAgreementArr=$kCourierServices->getCourierAgreementData($bookingArr['idServiceProvider'],'','',true);
        $szAccountNumber=decrypt($courierForwarderAgreementArr[0]['szAccountNumber'],ENCRYPT_KEY);
        $szUsername=decrypt($courierForwarderAgreementArr[0]['szUsername'],ENCRYPT_KEY);
        $szPassword=decrypt($courierForwarderAgreementArr[0]['szPassword'],ENCRYPT_KEY);

      //Configuration
        $wsdl = __APP_PATH__."/wsdl/Track.wsdl";
        $operation = "ProcessTrack";
        $endpointurl = 'https://wwwcie.ups.com/webservices/Track';
        $outputFileName = "XOLTResult.xml";

        function processTrack($bookingArr)
        {
            //create soap request
          $req['RequestOption'] = '15';
          $tref['CustomerContext'] = 'UPS Booking';
          $req['TransactionReference'] = $tref;
          $request['Request'] = $req;
          $request['InquiryNumber'] = $szTrackingNumber;
          $request['TrackingOption'] = '02';


          return $request;
        }

        try
        {

          $mode = array
          (
               'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
               'trace' => 1
          );

          // initialize soap client
              $client = new SoapClient($wsdl , $mode);

              //set endpoint url
              $client->__setLocation($endpointurl);


          //create soap header
          $usernameToken['Username'] = $szUsername;
          $usernameToken['Password'] = $szPassword;
          $serviceAccessLicense['AccessLicenseNumber'] = $szAccountNumber;
          $upss['UsernameToken'] = $usernameToken;
          $upss['ServiceAccessToken'] = $serviceAccessLicense;

          $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
          $client->__setSoapHeaders($header);


          //get response
              $resp = $client->__soapCall($operation ,array(processTrack($bookingArr)));

          //get status
          //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
          $resArr=toArray($resp);   
          //print_r($resArr ['Shipment']);
          if($resArr ['Shipment']['Package']['Activity']['Status']['Description']!='')
          {
              $trackingStatusArr=$resArr ['Shipment']['Package']['Activity']['Status'];
              //$kCourierServices->updateBookingCourierStatus($trackingStatusArr,$idBooking);
              return $resArr['Shipment']['Package']['Activity']['Status']['Description'];    
          }
          else if($resArr ['Shipment']['Package']['Status']['Description']!='')
          {
              $trackingStatusArr=$resArr ['Shipment']['Package']['Status'];
              //$kCourierServices->updateBookingCourierStatus($trackingStatusArr,$idBooking);
              return $resArr['Shipment']['Package']['Status']['Description'];
          }
          else
          {
              return "AWB does not exist";
          }

          //save soap request and response to file
          $fw = fopen($outputFileName , 'w');
          fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
          fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
          fclose($fw);

        }
        catch(Exception $ex)
        {
              //print_r ($ex);
            return "AWB does not exist";
        }
    }
}

function courierProductGroupList($kCourierProvider,$idProvider=0,$idCourierProductGroup=0)
{
     $t_base="management/providerProduct/";
     
    $courierProductGroup=$kCourierProvider->getCourierProductGroupByidProvider($idProvider);
    //print_r($courierProductGroup);
    $_POST['groupArr']['idCourierProvider']=$idProvider;
     ?>
     <div id="couier_cargo_limitation_list_container">    
        <h4><strong><?php echo t($t_base.'title/courier_product_group');?></strong></h4>
        <div class="clearfix courier-provider courier-provider-scroll"> 
            <table cellpadding="5" cellspacing="0" class="format-2" width="100%" id="couier_provider_product_list_table">
                <tr>
                    <td style="text-align:left;" class="firsttdnoborder"><strong><?php echo t($t_base.'fields/groupName');?></strong></td> 
                </tr>
                <?php
                if(!empty($courierProductGroup))
                {
                    foreach($courierProductGroup as $courierProductGroups)
                    {?>
                           <tr <?php if($courierProductGroups['id']==$idCourierProductGroup){?> class="selected-row" <?php }?>id="courier_product_group_<?php echo $courierProductGroups['id'];?>" onclick="selectCourierProductGroup('<?php echo $courierProductGroups['id'];?>','<?php echo $idProvider;?>','<?=t($t_base.'fields/edit');?>','<?=t($t_base.'fields/save');?>','<?=t($t_base.'fields/cancel');?>')">
                               <td><?php echo $courierProductGroups['szName']?></td>                               
                           </tr> 
                       <?php 
                    }
                    
                }?>    
            </table>
        </div>
     </div>
    <div class="clear-all"></div>
    <br> 
    <div id="courier_product_group_addedit_data">
        <?php
            echo display_courier_product_group_addedit_form($kCourierServices);
        ?>	
    </div>   
        <?php
}

function display_courier_product_group_addedit_form($kCourierServices)
{
    $t_base="management/providerProduct/";
	
	if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        {
           
                ?>
                <script type="text/javascript">
                        $("#<?php echo $errorKey?>").addClass('red_border');
                </script>
            <?php 
        }
    }
	?>
    <div id="courier_provider_form" style="width:100%;">
        <form action="javascript:void(0);" name="courierProductGroupForm" method="post" id="courierProductGroupForm">
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="font-12" style="text-align:left;"><?php echo t($t_base.'fields/groupName');?></td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="groupArr[szName]" id="szName" value="<?php echo $_POST['groupArr']['szName'];?>" onkeyup="checkProductGroupData();"/>
                        <input type="hidden" name="groupArr[idCourierProvider]" id="idCourierProvider" value="<?php echo $_POST['groupArr']['idCourierProvider'];?>"/>
                        <input type="hidden" name="groupArr[id]" id="id" value="<?php echo $_POST['groupArr']['id'];?>"/>
                    </td>
                    </tr>
                   <tr> 
                    <td style="float:right;padding-top:5px;">  
                        <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_group_limitation_data"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                        <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_group_limitation_data" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/delete');?></span></a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
	<?php
}

function display_courier_service_new_demo_form($kCourierService,$iProviderType=3)
{
    if(!empty($kCourierService->arErrorMessages))
    {
        $formId = 'calculate_courier_service_form';
        $szValidationErrorKey = '';
        foreach($kCourierService->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                 $("#"+'<?php echo $errorKey ; ?>').addClass('red_border');
            </script>
            <?php
        }
    }  
    $kConfig = new cConfig();
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true); 
    
    if($kCourierService->iSuccessMessage>0)
    {
        if($kCourierService->iSuccessMessage==1)
        {
            $szColor = 'style="color:green;" ';
        }
        else 
        {
            $szColor = 'style="color:red;" ';
        }
        ?>
        <div id="courier_service_success_div" <?php echo $szColor;?>>
            <?php echo $kCourierService->szApiResponseText ; ?>
        </div>
        <?php
            if(!empty($kCourierService->szApiResponseInformationText))
            {
                echo '<p>'.$kCourierService->szApiResponseInformationText."</p>";
            }
    } 
?>  
<form id="calculate_courier_service_form" name="calculate_courier_service_form" method="post">
    <TABLE WIDTH="90%" style="text-align: left;" class="format-2" BORDER="0">  
               <TR VALIGN="top"> 
                   <TD COLSPAN="2"><B>ORIGIN</B></TD>
               </TR>
               <TR VALIGN="top"> 
                   <TD style="text-align: right;"><B>Country:</B></TD>  
                   <TD> 
                       <select onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="getCourierRateAry[szCountry]" id="szCountry" >
                           <option value="">Select</option>
                       <?php
                           if(!empty($allCountriesArr))
                           {
                               foreach($allCountriesArr as $allCountriesArrs)
                               {
                                   ?>
                                   <option value="<?php echo $allCountriesArrs['szCountryISO']; ?>" <?php echo (($_POST['getCourierRateAry']['szCountry']==$allCountriesArrs['szCountryISO'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                   <?php
                               }
                           }
                        ?>
                       </select> 
                   </TD>
               </TR>
               <TR VALIGN="top">
                   <TD style="text-align: right;">City:</TD> 
                   <TD><input type="text" name="getCourierRateAry[szCity]" id="szCity" value="<?php echo $_POST['getCourierRateAry']['szCity']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></TD>
               </TR>
               <TR VALIGN="top">
                   <TD style="text-align: right;"><B>Postal Code:</B></TD> 
                   <TD><input type="text" name="getCourierRateAry[szPostCode]" id="szPostcode" value="<?php echo $_POST['getCourierRateAry']['szPostCode']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"></TD>
               </TR> 
               <TR VALIGN="top">  
                   <TD Colspan="2"><B>DESTINATION</B></TD>
               </TR>
               <TR VALIGN="top"> 
                   <TD style="text-align: right;"><B>Country:</B></TD>  
                   <TD>
                       <select onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="getCourierRateAry[szSCountry]" id="szSCountry" >
                           <option value="">Select</option>
                       <?php
                           if(!empty($allCountriesArr))
                           {
                               foreach($allCountriesArr as $allCountriesArrs)
                               {
                                   ?>
                                   <option value="<?php echo $allCountriesArrs['szCountryISO']; ?>" <?php echo (($_POST['getCourierRateAry']['szSCountry']==$allCountriesArrs['szCountryISO'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                   <?php
                               }
                           }
                        ?>
                       </select> 
                   </TD> 
               </TR>
               <TR VALIGN="top"> 
                   <TD style="text-align: right;">City:</TD>  
                   <TD><input type="text" name="getCourierRateAry[szSCity]" id="szSCity" value="<?php echo $_POST['getCourierRateAry']['szSCity']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);"></TD>
               </TR>
               <TR VALIGN="top"> 
                   <TD style="text-align: right;"><B>Postal Code:</B></td> 
                   <TD><input type="text" name="getCourierRateAry[szSPostCode]" id="szSPostcode" value="<?php echo $_POST['getCourierRateAry']['szSPostCode']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"></TD>
               </TR> 
               <?php if($iProviderType==3){ ?>
                    <TR VALIGN="top"> 
                        <TD style="text-align: right;"><B>Weight:</B></TD>   
                        <TD>
                            <input type="text" name="getCourierRateAry[fCargoWeight]" id="fCargoWeight" value="<?php echo $_POST['getCourierRateAry']['fCargoWeight']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" size="6">
                        </TD> 
                    </TR> 
               <?php } else {?>
                     <TR VALIGN="top"> 
                        <TD style="text-align: right;"><B>Package Dimensions:</B></TD>  
                        <TD>
                            <TABLE>
                                <TR>
                                    <TD>Length</TD>
                                    <TD>Width</TD>
                                    <TD>Height</TD>
                                    <TD>Weight</TD> 
                                </TR>
                                <TR>
                                    <TD><input type="text" name="getCourierRateAry[fCargoLength]" id="fCargoLength" value="<?php echo $_POST['getCourierRateAry']['fCargoLength']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" size="6"></TD>
                                    <TD><input type="text" name="getCourierRateAry[fCargoWidth]" id="fCargoWidth" value="<?php echo $_POST['getCourierRateAry']['fCargoWidth']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" size="6"></TD>
                                    <TD><input type="text" name="getCourierRateAry[fCargoHeight]" id="fCargoHeight" value="<?php echo $_POST['getCourierRateAry']['fCargoHeight']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" size="6"></TD> 
                                    <TD><input type="text" name="getCourierRateAry[fCargoWeight]" id="fCargoWeight" value="<?php echo $_POST['getCourierRateAry']['fCargoWeight']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" size="6"></TD>
                                </TR> 
                            </TABLE>
                        </TD>
                    </TR> 
               <?php } ?> 
               <TR VALIGN="top">  
                   <TD colspan="2" style="text-align: center;">
                       <input type="hidden" name="getCourierRateAry[iProviderType]" id="iProviderType" value="<?php echo $iProviderType; ?>">
                       <input type="button" name="getCourierRateAry[szSubmit]" value="Get Details" onclick="submit_courier_form();"> 
                       <?php if($iProviderType==5){ ?>
                        <input type="button" onclick="submit_courier_form(1);" value="Create Label" name="create_label">
                       <?php } ?>
                   </TD>
               </TR>
           </TABLE>
</form>
<?php 
} 
function display_forwarder_courier_try_it_out($kBooking)
{
    $t_base = "SERVICEOFFERING/"; 
    $kConfig = new cConfig();
    $countryAry=$kConfig->getAllCountries();
    if(!empty($_POST['searchAry']))
    {
        $szOriginPostCode = $_POST['searchAry']['szOriginPostCode'];
        $idOriginCountry = $_POST['searchAry']['idOriginCountry'];
        
        $szDestinationPostCode = $_POST['searchAry']['szDestinationPostCode'];
        $idDestinationCountry = $_POST['searchAry']['idDestinationCountry'];
    }
?>
    <h4><strong><?php echo t($t_base.'title/try_it_out');?></strong></h4>
    <p><?php echo t($t_base.'title/try_it_out_page_title');?></p><br>
    <form method="post" id="landing_page_form"> 
	<div class="oh clearfix" style="width: 100%;">
            <div class="fl-45">
                <table style="width:100%;border-spacing: 0px;">
                    <tr>
                        <td style="width:20%;margin-top: -5px;padding: 0;" valign="middle"><br><?php echo t($t_base.'fields/from');?></td>
                        <td style="width:40%;padding-left:7px;" valign="top">
                            <span class="font-12">Postcode</span><br>
                            <input name="searchAry[szOriginPostCode]" id="szOriginPostCode" style="width:90%;" value="<?php echo $szOriginPostCode; ?>" type="text" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);">
                        </td>
                        <td style="width:40%" valign="top">
                            <span class="font-12">Country</span><br>
                            <select name="searchAry[idOriginCountry]" id="szOriginCountry" size="1" style="width:100%;" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($countryAry))
                                    {
                                        foreach($countryAry as $countryArys)
                                        {
                                            ?>
                                            <option value="<?=$countryArys['id']?>" <?php if($idOriginCountry==$countryArys['id']){?> selected <?php }?>><?=substr($countryArys['szCountryName'],0,15)?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="fr-45">
                <table style="width:100%;">
                    <tr>
                        <td style="width:20%;margin-top: -5px;padding-left:9px;" valign="middle"><br><?php echo t($t_base.'fields/to');?></td>
                        <td style="width:40%" valign="top">
                            <span class="font-12">Postcode</span><br>
                            <input name="searchAry[szDestinationPostCode]" style="width:90%;" id="szDestinationPostCode" value="<?php echo $szDestinationPostCode; ?>" type="text" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);">
                        </td>
                        <td style="width:40%" valign="top">
                            <span class="font-12">Country</span><br>
                            <select name="searchAry[idDestinationCountry]" id="szDestinationCountry" size="1" style="width:100%;" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);">
                                <option value="">Select</option>
                                <?php
                                    if(!empty($countryAry))
                                    {
                                        foreach($countryAry as $countryArys)
                                        {
                                            ?>
                                            <option value="<?=$countryArys['id']?>" <?php if($idDestinationCountry==$countryArys['id']){?> selected <?php }?>><?=substr($countryArys['szCountryName'],0,15)?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="clear-all"></div>
        <?php echo forwarder_try_it_out_cargo_container($kBooking);?>
        <div class="clear-all"></div>
        <div class="btn-container" style="text-align:center;margin-top:20px;">
            <a href="javascript:void(0)" class="button2" onclick="clear_forwarder_courier_try_id_out();"><span>Clear</span></a>
            <a href="javascript:void(0)" class="button1" onclick="validateLandingPageForm_forwarder('landing_page_form','','1')"><span>Submit</span></a>
        </div>
    </form>
    <div class="clear-all"></div>
    <div  id="bottom_search_result" style="display:none;">
    </div>
    <?php 
    if(!empty($kBooking->arErrorMessages))
    {  
        $formId = 'landing_page_form'; 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
} 

function forwarder_try_it_out_cargo_container($kBooking,$postSearchAry=array())
{ 
    $t_base = "LandingPage/";  
     
    $szCargoDimentionHeader = '';
    $szDivKey = ''; 
    $szDivKey = "_".$iSearchMiniPage;
    $szChilDivClass = 'class="request-quote-fields"'; 
     
    ?> 
    <div id="complete-box" class="active transporteca-box" style="padding-top:15px;">    
        <div id="horizontal-scrolling-div-id">
            <?php 
                $counter = 1; 
                if(count($_POST['cargodetailAry']['iLength'])>0)
                { 
                    if(count($_POST['cargodetailAry']['iLength'])>0)
                    {
                       $loop_counter = count($_POST['cargodetailAry']['iLength']);  
                       $postDataFlag = true;
                    }  
                    for($j=0;$j<$loop_counter;$j++)
                    {
                        $counter = $j+1;
                        if($postDataFlag)
                        {  
                            $_POST['cargodetailAry']['iLength'][$counter] = format_numeric_vals($_POST['cargodetailAry']['iLength'][$counter]); 
                            $_POST['cargodetailAry']['iWidth'][$counter] = format_numeric_vals($_POST['cargodetailAry']['iWidth'][$counter]); 
                            $_POST['cargodetailAry']['iHeight'][$counter] = format_numeric_vals($_POST['cargodetailAry']['iHeight'][$counter]); 
                            $_POST['cargodetailAry']['iWeight'][$counter] = format_numeric_vals($_POST['cargodetailAry']['iWeight'][$counter]); 
                        }
                        ?>
                        <div id="add_booking_quote_container_<?php echo $j; ?>" <?php echo $szChilDivClass; ?>> 
                            <?php
                                echo forwarder_try_it_out_cargo_fields($counter,$cargoAry,$postSearchAry,$iSearchMiniPage);
                            ?>
                        </div>
                        <?php
                    }
                }
                else
                {   
                    ?>
                    <div id="add_booking_quote_container_0" <?php echo $szChilDivClass; ?> > 
                        <?php
                           echo forwarder_try_it_out_cargo_fields($counter,$postSearchAry);
                        ?>
                    </div>
                    <?php 
                } 
            ?>
            <div id="add_booking_quote_container_<?php echo $counter; ?>" style="display:none;"></div>
        </div>   
    </div>  
    <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition" value="<?=$counter?>" />
    <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1" value="<?=$counter?>" /> 
    <input type="hidden" name="iDangerCargo_hidden" id="iDangerCargo_hidden" value="No"> 
    <?php   
} 

function forwarder_try_it_out_cargo_fields($number,$postSearchAry=array())
{  
    if(!empty($_POST['cargodetailAry']))
    {
        $cargoAry = $_POST['cargodetailAry'] ; 
        $llop_counter = $number;
        $fLength = $cargoAry['iLength'][$llop_counter];
        $fWidth = $cargoAry['iWidth'][$llop_counter];
        $fHeight = $cargoAry['iHeight'][$llop_counter];
        $fWeight = $cargoAry['iWeight'][$llop_counter];
        $iQuantity = $cargoAry['iQuantity'][$llop_counter];
        $idCargo = $cargoAry['id'][$llop_counter];
        $idCargoMeasure = $cargoAry['idCargoMeasure'][$llop_counter]; 
        $idWeightMeasure = $cargoAry['idWeightMeasure'][$llop_counter]; 
    }
    else
    {
        if($number==1)
        {
            /*
            * In Case of Searchmin3 and SearchMini4 we fill in cargo dimension fields with default packing dimensions if it is empty
            */    
            $fLength = __DEFAULT_CARGO_PACKET_LENGTH__;
            $fWidth = __DEFAULT_CARGO_PACKET_WIDTH__;
            $fHeight = __DEFAULT_CARGO_PACKET_HEIGHT__;
            $fWeight = __DEFAULT_CARGO_PACKET_WEIGHT__;
            $iQuantity = __DEFAULT_CARGO_PACKET_QUANTITY__; 
            $idCargoMeasure = 1; //Cm
            $idWeightMeasure = 1; //Kg
        } 
    }
    $kBooking = new cBooking();
    $kConfig = new cConfig(); 

    // geting all weight measure 
    $weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

    // geting all cargo measure 
    $cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
    $t_base = "LandingPage/"; 
    $szDottedDiv = '';   
     
    if($number>1)
    {
        //$szDottedDiv = '<hr>';
        $szCargoHeadingTitle = "&nbsp;";
    } 
    else
    {
        $szLengthTitle = t($t_base.'fields/length')."<br/>";
        $szWidthTitle = t($t_base.'fields/width')."<br/>";
        $szHeightTitle = t($t_base.'fields/height')."<br/>";
        $szWeightTitle = t($t_base.'fields/weight')."<br/>";
        $szCountTitle = t($t_base.'fields/count')."<br/>";
        $szCargoMeasureTitle = "&nbsp;<br/>";
        $szCargoHeadingTitle = "&nbsp;<br/>Cargo"; 
        $szMargin = "margin-top: -5px;";
    }
   // echo $szDottedDiv;
    $iSearchMiniPage = 3;
    $idSuffix = "_V_".$iSearchMiniPage ;
?>  
    <div class="oh tryitout_cargo_container clearfix"> 
    <p class="fl-10" style="<?php echo $szMargin;?>"><?php echo $szCargoHeadingTitle; ?></p>
    <div class="fl-90 dimensions-field clearfix">				
        <span class="f-size-12">
            <?php echo $szLengthTitle; ?>
            <input type="text" class="first-cargo-fields" name="cargodetailAry[iLength][<?php echo $number?>]" value="<?php echo $fLength?>" id= "iLength<?php echo $number."".$idSuffix?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/length')); ?>" />
        </span>
        <span class="f-size-14 cross_seperator"><?php echo $szCargoMeasureTitle; ?>X</span>
        <span class="f-size-12">
            <?php echo $szWidthTitle; ?>
            <input type="text" name="cargodetailAry[iWidth][<?php echo $number?>]" value="<?php echo $fWidth?>" id= "iWidth<?php echo $number."".$idSuffix?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/width')); ?>"  /> 
        </span>
        <span class="f-size-14 cross_seperator"><?php echo $szCargoMeasureTitle; ?>X</span>
        <span class="f-size-12">
            <?php echo $szHeightTitle; ?>
            <input type="text" name="cargodetailAry[iHeight][<?php echo $number?>]" value="<?php echo $fHeight; ?>" id= "iHeight<?php echo $number."".$idSuffix?>"  onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/height')); ?>" />
        </span>
        <span class="f-size-12">
            <?php echo $szCargoMeasureTitle; ?>
            <select name="cargodetailAry[idCargoMeasure][<?=$number?>]" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" >
                <?php
                    if(!empty($cargoMeasureAry))
                    {
                        foreach($cargoMeasureAry as $cargoMeasureArys)
                        {
                            ?>
                            <option value="<?php echo $cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                            <?php
                        }
                    }
                ?>
            </select>
        </span>
        <span style="width:3%;">&nbsp;</span>
        <span class="f-size-12">
            <?php echo $szWeightTitle; ?>
            <input type="text" name="cargodetailAry[iWeight][<?=$number?>]" value="<?php echo $fWeight; ?>" id= "iWeight<?php echo $number."".$idSuffix?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/weight')); ?>" /> 
        </span>
        <span class="f-size-12"><?php echo $szCargoMeasureTitle; ?>
            <select size="1" name="cargodetailAry[idWeightMeasure][<?=$number?>]" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);">
                <?php
                   if(!empty($weightMeasureAry))
                   {
                       foreach($weightMeasureAry as $weightMeasureArys)
                       {
                           ?>
                           <option value="<?=$weightMeasureArys['id']?>" <?php if($idWeightMeasure==$weightMeasureArys['id']){?> selected <?php }?>><?=$weightMeasureArys['szDescription']?></option>
                           <?php
                       }
                   }
                ?>
            </select>
        </span>  
        <span class="f-size-14 cross_seperator"><?php echo $szCargoMeasureTitle; ?>X</span>
        <span class="f-size-12">
            <?php echo ucfirst($szCountTitle);?>
            <input type="text" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?php echo $iQuantity; ?>" id= "iQuantity<?php echo $number."".$idSuffix?>" onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/count')); ?>" /> 
        </span>
        <span style="<?php echo $szMargin; ?>float:right;text-align:right;"><?php echo $szCargoMeasureTitle;?>
            <a id="pending_task_cargo_line_add_more_link" class="cargo-add-button" href="javascript:void(0);" onclick="add_more_cargo_details('FORWARDER_TRY_IT_OUT_COURIER')"></a>    
            <a id="pending_task_cargo_line_remove_link" class="cargo-remove-button" href="javascript:void(0);" onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo ($number-1) ?>','<?php echo $number; ?>');"></a>
        </span>
    </div> 
</div>	 
<?php    
}
function display_courier_services_list($postSearchAry,$searchResult,$szCourierCalculationLogString=false) //DTD
{
    $kWHSSearch=new cWHSSearch();  
    $t_base = "SERVICEOFFERING/";  
    ?> 
    <br>
    <h4><strong>Services and price calculation</strong></h4><br>
    <table cellpadding="0" id="service_table"  cellspacing="0" border="0" class="format-6" width="100%">
        <tr>
            <th width="12%" align="center" valign="top"><?php echo t($t_base.'fields/provider'); ?></th>
            <th width="11%" align="center" valign="top">Service</th>
            <th width="11%" align="center" valign="top" style="text-align: center;">API<br>Response</th>
            <th width="11%" align="center" valign="top" style="text-align: center;">ROE</th>
            <th width="11%" align="center" valign="top" style="text-align: center;">Your Currency</th>
            <th width="11%" align="center" valign="top" style="text-align: center;">Markup</th>
            <th width="11%" align="center" valign="top" style="text-align: center;">Additional<br>Markup</th> 
            <th width="11%" style="text-align: center;">Label fee for<br>Transporteca</th>
            <th width="11%" style="text-align: center;">Total sales<br>price</th> 
        </tr>
        <?php
            if(!empty($searchResult))
            { 
                $tr_counter = 1;
                foreach($searchResult as $searchResults)
                { 
                    if($searchResults['markUpByRate']<0)
                    {
                        $szMarkupRateStr = "(".$searchResults['szForworderCurrency']."<br />".number_format((float)$searchResults['fCourierPrductMarkupForwarderCurrency'],2).")";
                    }
                    else
                    {
                        $szMarkupRateStr = $searchResults['szForworderCurrency']."<br />".number_format((float)$searchResults['fCourierPrductMarkupForwarderCurrency'],2);
                    }
                    if($searchResults['fMarkupperShipment']<0)
                    {
                        $searchResults['fMarkupperShipment'] = $searchResults['fMarkupperShipment'] * (-1);
                        $szAdditionalMarkupRateStr = "(".$searchResults['szForworderCurrency']."<br />".number_format((float)$searchResults['fMarkupperShipment'],2).")";
                    }
                    else
                    {
                        $szAdditionalMarkupRateStr = $searchResults['szForworderCurrency']."<br />".number_format((float)$searchResults['fMarkupperShipment'],2);
                    }
                    if($tr_counter%2==1)
                    {
                        $szTrClassName = "class='dark-bg'";
                    }
                    else
                    {
                        $szTrClassName = "";
                    }
        ?> 				
                    <tr id="select_services_tr_<?=$tr_counter?>" <?php echo $szTrClassName; ?>>
                        <td><?php echo $searchResults['szCarrierCompanyName']; ?></td>
                        <td><?php echo $searchResults['szCourierProductName']; ?></td>
                        <td style="text-align: center;"><?php echo "USD<br />".number_format((float)$searchResults['fShipmentAPIUSDCost'],2); ?></td>
                        <td style="text-align: center;"><?php echo ($searchResults['fForwarderCurrencyExchangeRate']>0)?number_format((float)(1/$searchResults['fForwarderCurrencyExchangeRate']),4):0.0000; ?></td>
                        <td style="text-align: center;"><?php echo $searchResults['szForworderCurrency']."<br />".number_format((float)$searchResults['fApiPriceForwarderCurrency'],2); ?></td> 
                        <td style="text-align: center;"><?php echo $szMarkupRateStr; ?></td> 
                        <td style="text-align: center;"><?php echo $szAdditionalMarkupRateStr; ?></td>
                        <td style="text-align: center;"><?php echo $searchResults['szForworderCurrency']."<br />".number_format((float)$searchResults['fLabelFeeForwarderCurrency'],2); ?></td> 
                        <td style="text-align: center;"><?php echo $searchResults['szCurrency']; ?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?><br /></td> 
                    </tr>
                <?php
                        $tr_counter++;
                } 
            }
            else
            {
                ?>
                <tr>
                        <td colspan="9"><h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2> </td>
                </tr>
                <?php
            }
        ?>
	</table> 
    <?php
    if(!empty($szCourierCalculationLogString))
    { 
        echo "<a href='javascript:void(0)' style='color:#eff0f2;' onclick=showHideCourierCal('courier_service_calculation')>.</a> <div id='courier_service_calculation' class='booking-page clearfix' style='display:none'><div id='popup-bg'></div><div id='popup-container'><div class='popup abandon-popup' style='margin-left:20px;width:85%;'><p class='close-icon' align='right'><a onclick=showHideCourierCal('courier_service_calculation') href='javascript:void(0);'><img alt='close' src='".__BASE_STORE_IMAGE_URL__."/close1.png'>
            </a></p><div style='text-align:left;height:400px;overflow-y:auto;overflow-x:hidden;width:100%;'>".$szCourierCalculationLogString." </div></div> </div></div>";
    }  
}
function dutyCalculatorHtml()
{
    $t_base="dutyCalculator/";
    $kAffiliate = new cAffiliate();
    $productListArr=$kAffiliate->getDutyProductList();
            
    $kConfig = new cConfig();        
    $curreniesArr=$kConfig->getBookingCurrency('',true);  
    
    $userIpDetailsAry = array();
    $userIpDetailsAry = getCountryCodeByIPAddress();
    $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;
    
    if(!empty($szUserCountryName))
    {
        $kConfig1 = new cConfig();
        $kConfig1->loadCountry(false,$szUserCountryName); 
        if((int)$kConfig1->iDefaultCurrency>0)
        {
            $idGoodResultCurrency = $kConfig1->iDefaultCurrency;
        }
        else
        {
            $idGoodResultCurrency=1;
        }
    }
    else 
    {
        $idGoodResultCurrency=1;
    }
    ?>
    <div class="duty-calculator">
        <div class="duty-calculator-calculation">
            <form id="dutyCalculatorForm"  method="post" name="dutyCalculatorForm">
                <div class="clearfix duty-calculator-fields-container">
                    <span class="duty-calculator-field-container"><strong><?=t($t_base.'fields/which_product_want_import');?></strong></span>
                    <span class="duty-calculator-value-container">
                        <select id="fProductRate" name="fProductRate" onchange="calculateDutyRate()">
                            <option value=""><?=t($t_base.'fields/select');?></option>
                            <?php 
                            if(!empty($productListArr))
                            {
                                foreach($productListArr as $productListArrs)
                                {?>
                                    <option value="<?php echo $productListArrs['fDutyRate']?>"><?php echo $productListArrs['szProductName']?></option>
                                <?php 
                                }
                            }?>
                        </select>
                    </span>
                </div>
                <div class="clearfix duty-calculator-fields-container">
                    <span class="duty-calculator-field-container"><strong><?=t($t_base.'fields/what_is_the_purchase_price');?></strong></span>
                    <span class="duty-calculator-value-container" >
                        <span class="duty-calculator-select">
                            <select id="idGoodCurrency" name="idGoodCurrency" onchange="calculateDutyRate('GoodCurrencyFlag')">
                                <option value=""><?=t($t_base.'fields/select');?></option>
                                <?php 
                                if(!empty($curreniesArr))
                                {
                                    foreach($curreniesArr as $curreniesArrs)
                                    {?>
                                        <option value="<?php echo $curreniesArrs['id']?>" <?php if($curreniesArrs['id']==$idGoodResultCurrency){?> selected <?php }?> ><?php echo $curreniesArrs['szCurrency']?></option>
                                    <?php 
                                    }
                                }?>
                            </select>
                        </span>
                        <span class="duty-calculator-input">
                            <input id="fGoodPrice" type="text"  value="" onkeyup="calculateDutyRate();" name="fGoodPrice">
                        </span>
                    </span>
                </div>
                <div class="clearfix duty-calculator-fields-container">
                    <span class="duty-calculator-field-container"><strong><?=t($t_base.'fields/how_much_you_pay_for_load');?></strong></span>
                    <span class="duty-calculator-value-container">
                        <span class="duty-calculator-select">
                            <select id="idLoadCurrency" name="idLoadCurrency" onchange="calculateDutyRate('LoadCurrencyFlag')">
                                <option value=""><?=t($t_base.'fields/select');?></option>
                                <?php 
                                if(!empty($curreniesArr))
                                {
                                    foreach($curreniesArr as $curreniesArrs)
                                    {?>
                                        <option value="<?php echo $curreniesArrs['id']?>" <?php if($curreniesArrs['id']==$idGoodResultCurrency){?> selected <?php }?>><?php echo $curreniesArrs['szCurrency']?></option>
                                    <?php 
                                    }
                                }?>
                            </select>
                        </span>
                        <span class="duty-calculator-input">
                             <input id="fLoadPrice" type="text"  value="" onkeyup="calculateDutyRate();" name="fLoadPrice">
                        </span>
                    </span>
                </div>
            </form>
        </div>
        <div class="duty-calculator-result">
            <div class="clearfix duty-calculator-fields-container">
                    <span class="duty-calculator-field-container"><strong><?=t($t_base.'fields/indicative_duty');?></strong></span>
                    <span class="duty-calculator-value-container" id="percentage-text"></span>
                </div>
                <div class="clearfix duty-calculator-fields-container">
                    <span class="duty-calculator-field-container"><strong><?=t($t_base.'fields/indicative_tariff_rate');?></strong></span>
                    <span class="duty-calculator-value-container">
                    <span class="duty-calculator-select">
                        <select id="idGoodResultCurrency" name="idGoodResultCurrency" onchange="calculateDutyRate();">
                            <?php 
                            if(!empty($curreniesArr))
                            {
                                foreach($curreniesArr as $curreniesArrs)
                                {?>
                                    <option value="<?php echo $curreniesArrs['id']?>" <?php if($curreniesArrs['id']==$idGoodResultCurrency){?> selected <?php }?> ><?php echo $curreniesArrs['szCurrency']?></option>
                                <?php 
                                }
                            }?>
                        </select>
                    </span>
                    <span class="duty-calculator-input" id="price-text"></span>
                    </span>
                </div>
        </div>
    </div>
 <?php 
}

function addDisplayCustomerClearance($kCourierServices,$bDTDFlag=false)
{ 
    $idPrimarySortKey=trim($_POST['idPrimarySortKey']);
    $idOtherSortKeyArr=trim($_POST['idOtherSortKeyArr']);
    $szSortValue=trim($_POST['szSortValue']); 
    $idOtherSortKeyArr=explode(";",$idOtherSortKeyArr);

    $t_base="management/modeTransport/";
    $courierTrackiocnArr=$kCourierServices->getAllCustomerClearanceData($idPrimarySortKey,$szSortValue,$idOtherSortKeyArr,$bDTDFlag);
    $counter=count($courierTrackiocnArr); 
?> 
    
    <script type="text/javascript">
        $(function () {
            $('#mode-transport-table').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    $(".selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        var tr_id_ary = tr_id.split("____");
                        var idModeTransport = parseInt(tr_id_ary[1]);
                        
                        if(!isNaN(idModeTransport))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                    });  
                    if(res_ary_new.length)
                    {
                        var szModeOfTransIds = res_ary_new.join(';'); 
                       $("#deleteTruckiconId").attr('value',szModeOfTransIds);
                        $("#detete_mode_transport_data").unbind("click");
                        $("#detete_mode_transport_data").click(function(){deleteCustomerClearanceData();});
                        $("#detete_mode_transport_data").attr("style",'opacity:1'); 
                    }
                    else
                    {
                        var szModeOfTransIds = '';
                        $("#deleteTruckiconId").attr('value',szModeOfTransIds);
                        $("#detete_mode_transport_data").unbind("click");
                        $("#detete_mode_transport_data").attr("style",'opacity:0.4');
                    } 
                }
            });
        })
    </script> 
    <!--onclick="seletTheRowForDeleteTruckicon('<?php echo $courierTrackiocnArrs['id']?>')"-->
    <div id="couier_cargo_limitation_list_container"> 
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">	
          <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" id="mode-transport-table">
              <tr id="table-header">
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift_cc(event,'szFromCountry')"><strong><?php echo t($t_base.'fields/from_country');?></strong> <span class="moe-transport-sort-span sort-arrow-up" id="moe-transport-sort-span-id-szFromCountry">&nbsp;</span></td>
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift_cc(event,'szFromRegionName')"><strong><?php echo t($t_base.'fields/region');?></strong> <span class="moe-transport-sort-span" id="moe-transport-sort-span-id-szFromRegionName">&nbsp;</span></td>
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift_cc(event,'szToCountry')"><strong><?php echo t($t_base.'fields/to_country');?></strong> <span class="moe-transport-sort-span" id="moe-transport-sort-span-id-szToCountry">&nbsp;</span></td>
                <td style="width:24%;cursor:pointer" onclick="on_enter_key_press_shift_cc(event,'szToRegionName')"><strong><?php echo t($t_base.'fields/region');?></strong> <span class="moe-transport-sort-span" id="moe-transport-sort-span-id-szToRegionName">&nbsp;</span></td>
            </tr>   	
        <?php
            if(!empty($courierTrackiocnArr))
            {
                foreach($courierTrackiocnArr as $courierTrackiocnArrs)
                {?>
                    <tr id="truckin_tr____<?php echo $courierTrackiocnArrs['id']; ?>" >
                        <td id="from_to_country_1_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szFromCountry']?></td>
                        <td id="from_to_country_2_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szFromRegionName']?></td>
                        <td id="from_to_country_3_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szToCountry']?></td>
                        <td id="from_to_country_4_<?php echo $courierTrackiocnArrs['id']?>" ><?php echo $courierTrackiocnArrs['szToRegionName']?></td>
                    </tr>
                <?php		
                } 				 
            }
            else
            {?>
                <tr>
                   <td colspan="4" align="center">
                       <h4><b><?=t($t_base.'fields/no_courier_provider_found');?></b></h4>
                   </td>
                </tr>
            <?php   }   ?>  
           </table>
       </div> 
   </div>
    <div class="clear-all"></div>
    <br>
   <div id="courier_cargo_limitation_addedit_data">
        <?php
            echo display_customer_clearance_addedit_form($kCourierServices,$bDTDFlag);
        ?>	
    </div> 
<?php
}

function display_customer_clearance_addedit_form($kCourierServices,$bDTDFlag=false)
{
    $t_base="management/modeTransport/";
    $kConfig = new cConfig();
    $regionArr=$kCourierServices->getAllRegion();
    $getAllCountry=$kConfig->getAllCountries(true);

    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    } 
    $iDTDTrades = 0;
    if($bDTDFlag)
    {
        $iDTDTrades = 1;
    }
    else if($_POST['addModeTransportAry']['iDTDTrades']==1)
    {
        $iDTDTrades = 1;
    }
?>
    <div id="courier_provider_form" style="width:100%;">
        <form action="javascript:void(0);" name="customerClearanceAddForm" method="post" id="customerClearanceAddForm">
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="font-12" width="30%" style="text-align:left;"><?php echo t($t_base.'fields/from');?></td>
                    <td class="font-12" width="30%" style="text-align:left;"><?php echo t($t_base.'fields/to');?></td>
                    <td style="float:right;" width="40%"></td>
                </tr>
                <tr>
                    <td>
                        <select id="idFromCountry" style="width:98%;" name="addModeTransportAry[idFromCountry]" onchange="checkCountryIsSelectedForCC();">
                            <option value=''>Select</option>
                            <?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                                ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addModeTransportAry']['idFromCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                <?php
                                }
                            }?>
                            <?php 
                            if(!empty($getAllCountry))
                            {
                                foreach($getAllCountry as $getAllCountrys)
                                {
                                    ?>
                                        <option  value='<?php echo $getAllCountrys['id']?>' <?php if($_POST['addModeTransportAry']['idFromCountry']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName'];?></option>
                                    <?php
                                }
                            }?>
                        </select>
                    </td>
                    <td>
                        <select id="idToCountry" style="width:98%;" name="addModeTransportAry[idToCountry]" onchange="checkCountryIsSelectedForCC();">
                            <option value=''>Select</option>
                            <?php 
                            if(!empty($regionArr))
                            {
                                    foreach($regionArr as $regionArrs)
                                    {
                                            $regionValue=$regionArrs['id']."_r";
                                    ?>
                                            <option  value='<?php echo $regionValue?>' <?php if($_POST['addModeTransportAry']['idToCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                    <?php
                                    }
                            }?>
                            <?php 
                            if(!empty($getAllCountry))
                            {
                                    foreach($getAllCountry as $getAllCountrys)
                                    {?>
                                            <option  value='<?php echo $getAllCountrys['id']?>' <?php if($_POST['addModeTransportAry']['idToCountry']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName'];?></option>
                                    <?php
                                    }
                            }?>
                        </select>
                    </td>
                    <td style="float:right;">  
                        <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_mode_transport_data"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                        <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_mode_transport_data" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/delete');?></span></a>
                        <input type="hidden" name="addModeTransportAry[iDTDTrades]" id="iDTDTrades" value="<?php echo $iDTDTrades; ?>">
                    </td>
                </tr>
            </table>
        </form>
    </div>
	<?php
}

function showCourierProductAttentionPopup()
{?>
     <div id="popup-bg"></div>
        <div id="popup-container" >
            <div class="popup">
                <h3>Attention</h3><br/>
		<p>You have changed the courier product. Please check that your selection is correct, to avoid incurring excess cost from an expensive courier product</p><br/>
		
                <div class="btn-container" style="text-align:center;">
                    <a href="javascript:void(0)" class="button1" onclick="$('#courier_product_attention_popup').attr('style','display:none;');"><span>OK</span></a>
                </div>    
            </div>
        </div>           
  <?php  
}
?>