<?php
if( !isset( $_SESSION ) )
{
	session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * forwarder_functions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Ashish
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/forwarder_functions.php" ); 

function pricingHaulageCityListing($haulagePricingCityDataArr,$idHaulageModel,$idDirection,$idWarehouse)
{
	$kConfig = new cConfig();
	$t_base = "HaulaugePricing/";
	if($idDirection=='1')
	{
            $formto=t($t_base.'fields/from');
	}
	else
	{
            $formto=t($t_base.'fields/to');
	}
        $kWhsSearch = new cWHSSearch();
        $kWhsSearch->load($idWarehouse);
        $iWarehouseType = $kWhsSearch->iWarehouseType;
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {  
            $szNotCityModelsFound = t($t_base.'messages/air_no_city'); 
            $szTransitToolTip = t($t_base.'messages/air_transit_time_city_tool_tip');
            $szDistanceToolTip = t($t_base.'messages/air_distance_from_cfs_tool_tip'); 
        }
        else
        { 
            $szNotCityModelsFound = t($t_base.'messages/no_city');
            $szTransitToolTip = t($t_base.'messages/transit_time_city_tool_tip');
            $szDistanceToolTip = t($t_base.'messages/distance_from_cfs_tool_tip'); 
        }
	?>
	<hr/>
	<p style="margin-bottom: 2px;"><strong style="font-size: 14px;"><?=t($t_base.'title/city_title_1')?> <?=$formto?> <?=t($t_base.'title/city_title_2')?></strong> <a href='javascript:void(0)' id='copy_link' onclick="open_copy_haulage_city_popup('<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>');"><?=t($t_base.'links/show_all_cfs_cities');?></a> <a href='javascript:void(0)' id='copy_link' onclick="open_warehouse_city_circle_poup('<?=$idWarehouse?>','<?=$idDirection?>','<?=$idHaulageModel?>');"><?=t($t_base.'links/show_all_cfs_city');?></a></p>
	<div id="view_haulage_model_city_div">
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_city">
		<tr>
                    <!-- <td style="text-align:left;width:15%"><strong><?=t($t_base.'fields/priority')?></strong></td> -->
                    <td style="text-align:left;width:21%"><strong><?=t($t_base.'fields/country')?></strong></td>
                    <td style="text-align:left;width:21%"><strong><?=t($t_base.'fields/city_name')?></strong></td>
                    <td style="text-align:left;width:9%"><strong><?=t($t_base.'fields/radius')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/radius_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                    <td style="text-align:left;width:14%"><strong><?=t($t_base.'fields/distance_from_cfs')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szDistanceToolTip;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                    <td style="text-align:left;width:14%"><strong><?=t($t_base.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?php echo $szTransitToolTip;?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
                    <td style="text-align:left;width:14%"><strong><?=t($t_base.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fm_factor_for_city_tool_tip_heading');?>','<?=t($t_base.'messages/fm_factor_for_city_tool_tip_text');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
                    <td style="text-align:left;width:7%"><strong><?=t($t_base.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/fuel_tool_tip_city');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>

		</tr>
	<?php
	if(!empty($haulagePricingCityDataArr))
	{
		$j=0;
		foreach($haulagePricingCityDataArr as $haulagePricingCityDataArrs)
		{
		
			if($j==0)
			{
				$idPricing=$haulagePricingCityDataArrs['id'];
				$fPricingWmFactor=$haulagePricingCityDataArrs['fWmFactor'];
				$idPricingHaulageModel=$haulagePricingCityDataArrs['idHaulageModel'];
				$idPricingWarehouse=$haulagePricingCityDataArrs['idWarehouse'];
				$iPricingDirection=$haulagePricingCityDataArrs['iDirection'];
			}
			$CountryName=$kConfig->getCountryName($haulagePricingCityDataArrs['idCountry']);
		?>
			<tr id="haulage_city_<?=++$j?>" onclick="sel_haulage_model_city_data('haulage_city_<?=$j?>','<?=$haulagePricingCityDataArrs['id']?>','<?=$haulagePricingCityDataArrs['idHaulageModel']?>','<?=$j?>','<?=$haulagePricingCityDataArrs['idWarehouse']?>','<?=$haulagePricingCityDataArrs['iDirection']?>','<?=$haulagePricingCityDataArrs['fWmFactor']?>');">
				<!-- <td><?=$haulagePricingCityDataArrs['iPriority']?>&nbsp;Priority</td> -->
				<td><?=$CountryName?></td>
				<td><?=$haulagePricingCityDataArrs['szName']?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['iRadiousKM'])?> <?=t($t_base.'fields/km');?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['iDistanceUpToKm'])?> <?=t($t_base.'fields/km');?></td>
				<td><?php 
				if((int)$haulagePricingCityDataArrs['iHours']>24)
				{
					echo "< ".($haulagePricingCityDataArrs['iHours']/24)." ".t($t_base.'fields/days');
				}
				else
				{
					echo "< ".$haulagePricingCityDataArrs['iHours']." ".t($t_base.'fields/hours');
				}
				?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base.'fields/kg')?>/<?=t($t_base.'fields/cbm')?></td>
				<td><?=number_format((float)$haulagePricingCityDataArrs['fFuelPercentage'],1)?> %</td>
			</tr>
		<?php	
		}
	}
	else
	{
	?>
		<tr>
			<td colspan="7" align="center"><?=$szNotCityModelsFound; ?></td>
		</tr>
	<?php }  ?>
	<input type="hidden" value="<?=$j?>" id="totalActive1" name="totalActive1"/>
	</table>
	</div>
	<br/>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td width="10%"><?=t($t_base.'fields/priority')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/city_priority_tool_tip_heading');?>','<?=t($t_base.'messages/priority_tool_tip_city');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
		<td width="35%" align="left"><a href="javascript:void(0)" id="priority_up_city" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/up')?></span></a> <a href="javascript:void(0)" id="priority_down_city" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/down')?></span></a></td>
		<td align="right" width="55%"><a href="javascript:void(0)" id="delete_city" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/delete_city')?></span></a> <a href="javascript:void(0)" id="edit_city" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit_city')?></span></a> <a href="javascript:void(0)" id="add_city"  class="button1" onclick="add_city_popup('<?=$idHaulageModel?>','<?=$idDirection?>','<?=$idWarehouse?>');"><span style="min-width:70px;"><?=t($t_base.'fields/add_city')?></span></a></td>
		</tr>
	</table>
	<?php if($idPricing>0){ ?>
	<script>
            var callvalue=$("#callvalue").attr('value');
            if(parseInt(callvalue)!='1')
            {
                    sel_haulage_model_city_data('haulage_city_1','<?=$idPricing?>','<?=$idPricingHaulageModel?>','1','<?=$idPricingWarehouse?>','<?=$iPricingDirection?>','<?=$fPricingWmFactor?>');
            }
            $("#callvalue").attr('value','0');
	</script>
	<?php } ?>
	<?php if(!empty($haulagePricingCityDataArr)) {?>
            <hr/>
	<?php }?>
	<div id="city_detail_list_<?=$idHaulageModel?>"></div>
	<div id="city_detail_delete_<?=$idHaulageModel?>"></div>		
	<?php
} 

function pricingHaulageZoneListing($haulagePricingZoneDataArr,$idHaulageModel,$idDirection,$idWarehouse)
{
	$t_base="HaulaugePricing/";
	if($idDirection=='1')
	{
            $formto=t($t_base.'fields/from');
	}
	else
	{
            $formto=t($t_base.'fields/to');
	}
        $kWhsSearch = new cWHSSearch();
        $kWhsSearch->load($idWarehouse);
        $iWarehouseType = $kWhsSearch->iWarehouseType;
        
        
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        { 
            $szNoZoneFound = t($t_base.'messages/air_no_zone');
            $szTransitTypeZoneTip = t($t_base.'messages/air_transit_time_zone_tool_tip'); 
        }
        else
        {
            $szNoZoneFound = t($t_base.'messages/no_zone');
            $szTransitTypeZoneTip = t($t_base.'messages/transit_time_zone_tool_tip');
        }
	?>
	<hr/>
	<p style="margin-bottom:2px;"><strong style="font-size:14px;"><?=t($t_base.'title/zone_title_1')?> <?=$formto?> <?=t($t_base.'title/zone_title_2')?></strong> <a href='javascript:void(0)' id='copy_link' onclick="open_copy_haulage_zone_popup('<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>');"><?=t($t_base.'links/show_all_cfs_zones');?></a></p>
	<div style="float:left;border:1px solid #BFBFBF;border-top:0px;width:60%;min-height:229px;max-height:229px;overflow-y:auto;" class="viewhaulagezone" id="view_haulage_model_zone_div">
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_zone">
            <tr>
                <!--  <td style="text-align:left;width:10%" class="firsttdnoborder"><strong><?=t($t_base.'fields/priority')?></strong></td>-->
                <td style="text-align:left;width:40%" class="firsttdnoborder"><strong><?=t($t_base.'fields/zone')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                <td style="text-align:left;width:22%"><strong><?=t($t_base.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?php echo $szTransitTypeZoneTip; ;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                <td style="text-align:left;width:22%"><strong><?=t($t_base.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fm_factor_zone_tool_tip_1');?>','<?=t($t_base.'messages/fm_factor_zone_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                <td style="text-align:left;width:16%"><strong><?=t($t_base.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fuel_zone_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
            </tr>
	<?php
	if(!empty($haulagePricingZoneDataArr))
	{
		$j=0;
		foreach($haulagePricingZoneDataArr as $haulagePricingZoneDataArrs)
		{
                    if($j==0)
                    {
                        $idPricing=$haulagePricingZoneDataArrs['id'];
                        $fPricingWmFactor=$haulagePricingZoneDataArrs['fWmFactor'];
                        $idPricingHaulageModel=$haulagePricingZoneDataArrs['idHaulageModel'];
                        $idPricingWarehouse=$haulagePricingZoneDataArrs['idWarehouse'];
                        $iPricingDirection=$haulagePricingZoneDataArrs['iDirection'];
                    }
		?>
                    <tr id="haulage_zone_<?=++$j?>" onclick="sel_haulage_model_zone_data('haulage_zone_<?=$j?>','<?=$haulagePricingZoneDataArrs['id']?>','<?=$haulagePricingZoneDataArrs['idHaulageModel']?>','<?=$j?>','<?=$haulagePricingZoneDataArrs['idWarehouse']?>','<?=$haulagePricingZoneDataArrs['iDirection']?>','<?=$haulagePricingZoneDataArrs['fWmFactor']?>');" <? if($j==1){?> style="background:#DBDBDB;color:#828282;" <? }?>>
                        <!--<td class="firsttdnoborder"><?=$haulagePricingZoneDataArrs['iPriority']?>&nbsp;<?=t($t_base.'fields/priority')?></td>-->
                        <td class="firsttdnoborder"><?=$haulagePricingZoneDataArrs['szName']?></td>
                        <td>
                            <?php 
                                if((int)$haulagePricingZoneDataArrs['iHours']>24)
                                {
                                    echo "< ".($haulagePricingZoneDataArrs['iHours']/24)." ".t($t_base.'fields/days');
                                }
                                else
                                {
                                    echo "< ".$haulagePricingZoneDataArrs['iHours']." ".t($t_base.'fields/hours');
                                }
                            ?>
                        </td>
                        <td><?=number_format((float)$haulagePricingZoneDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base.'fields/Kg')?>/<?=t($t_base.'fields/cbm')?></td>
                        <td><?=number_format((float)$haulagePricingZoneDataArrs['fFuelPercentage'],1)?> %</td>
                    </tr>
		<?php	
		}
	}
	else
	{?>
            <tr>
                <td colspan="4" align="center" class="firsttdnoborder"><?php echo $szNoZoneFound; ?></td>
            </tr>
	<?php } ?>
	<input type="hidden" value="<?=$j?>" id="totalActive1" name="totalActive1"/>
	</table>
	</div>
	<div id="display_zone_map" class="displayzonemap">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
		 <td colspan="5"><iframe style="border:0" name="hssiframe" width="276px" height="228px" src=""></iframe></td>
            </tr>
	</table>
	</div>
	<br class="clear-all" />
	<br/>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
		<td width="10%"><?=t($t_base.'fields/priority')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/priority_tool_tip_zone_heading');?>','<?=t($t_base.'messages/priority_tool_tip_zone');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
		<td width="35%" align="left"><a href="javascript:void(0)" id="priority_up_zone" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/up')?></span></a> <a href="javascript:void(0)" id="priority_down_zone" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/down')?></span></a></td>
		<td align="right" width="55%"><a href="javascript:void(0)" id="delete_zone" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/delete_zone')?></span></a> <a href="javascript:void(0)" id="edit_zone" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit_zone')?></span></a> <a href="javascript:void(0)" id="add_zone"  class="button1" onclick="add_zone_popup('<?=$idHaulageModel?>','<?=$idDirection?>','<?=$idWarehouse?>');"><span style="min-width:70px;"><?=t($t_base.'fields/add_zone')?></span></a></td>
            </tr>
	</table>	
	<?php if($idPricing>0){?>
	<script type="text/javascript">
	var callvalue=$("#callvalue").attr('value');
	if(parseInt(callvalue)!='1')
	{
		sel_haulage_model_zone_data('haulage_zone_1','<?=$idPricing?>','<?=$idPricingHaulageModel?>','1','<?=$idPricingWarehouse?>','<?=$iPricingDirection?>','<?=$fPricingWmFactor?>');
	}
	$("#callvalue").attr('value','0');
	</script>
	<?php }?> 
	<?php if(!empty($haulagePricingZoneDataArr))
	{?>
	<hr />
	<?php }?>
	<div id="zone_detail_list_<?=$idHaulageModel?>">
	</div>
	<div id="zone_detail_delete_<?=$idHaulageModel?>"></div>		
	<?php
} 

function copy_city_popup($idForwarder,$idWarehouse,$idDirection,$idHaulageModel,$id)
{
    $t_base = "HaulaugePricing/";
    $kHaulagePricing = new cHaulagePricing();
    
    
    $kWhsSearch = new cWHSSearch();
    $kWhsSearch->load($idWarehouse);
    $iWarehouseType = $kWhsSearch->iWarehouseType;
    $ret_ary=$kHaulagePricing->getActiveWarehouseZone($idForwarder,$idWarehouse,$idHaulageModel);
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    { 
        $szCFSText = "airport warehouse"; 
    }
    else
    {
        $szCFSText = "CFS"; 
    }
?>
    <h5 style="margin-bottom:6px;"><?=t($t_base.'title/select_city_to_copy')?></h5>
    <p style="text-align:justify"><?=t($t_base.'messages/city_heading_line_1'); ?></p><br/>
    <form name="copyPostCode" id="copyPostCode" method="post">
        <div class="oh haulage-get-data">
            <div class="fl-25">
                <span class="f-size-12"><?=t($t_base.'fields/country')?></span>
                <select name="dataExportCityArr[haulageCountryZone]" id="haulageCountryZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'country','city_warehouse_zone','<?=$idHaulageModel?>','<?php echo $iWarehouseType; ?>')">
                    <option value=""><?=t($t_base.'fields/select')?></option>		
                    <?php
                        if(!empty($ret_ary))
                        {
                            foreach($ret_ary as $ret_arys)
                            {
                                ?>
                                <option value="<?=$ret_arys['id']?>"><?=$ret_arys['szCountryName']?></option>
                                <?php
                            }
                        }									
                    ?>
                </select>
            </div>
            <div id='city_warehouse_zone'>
                <div class="fl-25">
                    <span class="f-size-12"><?=t($t_base.'fields/city');?></span>
                    <select name="dataExportCityArr[szCityZone]" disabled id="szCityZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'city','warehouse_zone','<?=$idHaulageModel?>','<?php echo $iWarehouseType; ?>')">
                        <option value=""><?=t($t_base.'fields/all');?></option>
                        <?php
                            if(!empty($ret_ary))
                            {
                                foreach($ret_ary as $ret_arys)
                                {
                                    ?>
                                    <option value="<?=$ret_arys['szCity']?>"><?=$ret_arys['szCity']?></option>
                                    <?php
                                }
                            } 
                        ?>
                    </select>
                </div>
                <div class="fl-25" id="warehouse_zone">
                    <span class="f-size-12">Name</span>
                    <select name="dataExportCityArr[haulageWarehouseZone]" disabled id="haulageWarehouseZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
                        <option value="">Select</option>
                            <?php
                                if(!empty($ret_ary))
                                {
                                    foreach($ret_ary as $ret_arys)
                                    {
                                        ?>
                                        <option value="<?=$ret_arys['warehouseId']?>"><?=$ret_arys['szWareHouseName']?></option>
                                        <?php
                                    }
                                }
                            ?>
                    </select>
                </div>
                <div class="fl-25 direction">
                    <span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
                    <select name="dataExportCityArr[idDirectionZone]" disabled id="idDirectionZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
                        <option value='1'><?=t($t_base.'fields/export')?></option>
                        <option value='2'><?=t($t_base.'fields/import')?></option>
                    </select>
                </div> 	
            </div>
        </div>	
        <br/>			
        <div id="show_copy_data" class="tableScroll" style="height: 119px; overflow: auto;"></div>
        <input type="hidden" value="<?=(($_POST['dataExportCityArr']['idZoneDirection'])?$_POST['dataExportCityArr']['idZoneDirection']:$idDirection)?>" id="idZoneDirection" name="dataExportCityArr[idZoneDirection]">
        <input type="hidden" value="<?=(($_POST['dataExportCityArr']['idZoneWarehouse'])?$_POST['dataExportCityArr']['idZoneWarehouse']:$idWarehouse)?>" id="idZoneWarehouse" name="dataExportCityArr[idZoneWarehouse]">
        <input type="hidden" value="<?=(($_POST['dataExportCityArr']['idZoneHaulageModel'])?$_POST['dataExportCityArr']['idZoneHaulageModel']:$idHaulageModel)?>" id="idZoneHaulageModel" name="dataExportCityArr[idZoneHaulageModel]">
        <input type="hidden" value="<?=(($_POST['dataExportCityArr']['idZone'])?$_POST['dataExportCityArr']['idZone']:$id)?>" id="idZone" name="dataExportCityArr[idZone]">
        <input type="hidden" value="<?=(($_POST['dataExportCityArr']['iCheckedCount'])?$_POST['dataExportCityArr']['iCheckedCount']:"0")?>" id="iCheckedCount" name="dataExportCityArr[iCheckedCount]">
        <br/>
        <p align="center">
            <a href="javascript:void(0)" id="cancel_zone" class="button2" onclick="cancel_zone('<?=$idHaulageModel?>')"><span>Cancel</span></a><a href="javascript:void(0)" id="copy_city" class="button1" style="opacity:0.4"><span><?=t($t_base.'fields/copy')?></span></a>
        </p>
    </form>	
<?php	

}


function update_city_form($arrCity=array(),$idDirection,$idWarehouse,$idHaulageModel,$id=0,$mode,$iPriorityLevel=0)
{
    $t_base_error="Error";
    $t_base = "HaulaugePricing/";
    $Excel_export_import=new cExport_Import();
    $kHaulagePricing = new cHaulagePricing();
    $kWHSSearch = new cWHSSearch();

    $kWHSSearch->load($idWarehouse);
    $iWarehouseType = $kWHSSearch->iWarehouseType;
      
    $maxRadius=$kWHSSearch->getManageMentVariableByDescription('__MAX_RADIUS_CIRCLE__');
    if($idDirection=='1')
    {
        $direction=t($t_base.'fields/Export');
        $formto=t($t_base.'fields/to');
        $tofrom=t($t_base.'fields/from');
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $transt_time_text = t($t_base.'messages/air_to_this_city'); 
        }
        else
        {
            $transt_time_text = t($t_base.'messages/to_this_city'); 
        } 
    }
    else
    {
        $direction=t($t_base.'fields/Import');
        $formto=t($t_base.'fields/from');
        $tofrom=t($t_base.'fields/to'); 
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $transt_time_text = t($t_base.'messages/air_this_city_to'); 
        }
        else
        {
            $transt_time_text = t($t_base.'messages/this_city_to'); 
        }
    }
    $haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);
    //print_r($_POST['cityArr']);
    if(!empty($_POST['cityArr']))
    { 
        if($_POST['cityArr']['mode']=='edit')
        {
            if($kHaulagePricing->updateCity($_POST['cityArr']))
            {?>
                <script type="text/javascript">
                    $("#loader").attr('style','display:block;');
                    $("#update_city_form_add_edit").html('');
                    open_pricing_model_detail('<?=$_POST['cityArr']['idHaulageModel']?>','<?=$_POST['cityArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$_POST['cityArr']['iPriorityLevel']?>','<?=$_POST['cityArr']['id']?>','<?=$idDirection?>','<?=$_POST['cityArr']['fWmFactor']?>');
                </script>	
            <?php	
            exit();		
            }
        }

        if($_POST['cityArr']['mode']=='add')
        {
            $idPricing=$kHaulagePricing->addCity($_POST['cityArr']);
            if((int)$idPricing>0)
            {
                $data['idDirection']=$idDirection;
                $data['idWarehouse']=$_POST['cityArr']['idWarehouse'];
                $data['idHaulageModel']=$_POST['cityArr']['idHaulageModel'];
                $haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
                $haulageZoneNewArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'city');
                $position=getAddedCityPosition($haulageZoneNewArr,$_POST['cityArr']['szName']);
                if(!empty($haulagePricingModelArr))
                {
                    foreach($haulagePricingModelArr as $haulagePricingModelArrs)
                    {
                        if($haulagePricingModelArrs['idHaulageModel']=='2')
                        {
                            $count=$haulagePricingModelArrs['iCount'];
                            if((int)$haulagePricingModelArrs['iCount']>0)
                            {
                                if((int)$haulagePricingModelArrs['iCount']==1)
                                    $text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/city_defined');
                                else
                                    $text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/cities_defined');
                            }
                            else
                                $text=t($t_base.'fields/no_cities_defined');
                        }
                    }
                }
                else
                {
                    $text=t($t_base.'fields/no_cities_defined');
                }

                $priority_active=$kHaulagePricing->getPriorityCFSHaulageModel($_POST['cityArr']['idWarehouse'],$_POST['cityArr']['idHaulageModel'],$idDirection);
                if((int)$priority_active>0)
                {
                    $priority_active_text=$priority_active." ".t($t_base.'fields/priority');
                }
                else
                {
                    $priority_active_text=t($t_base.'fields/not_active');
                }
                ?>
                    <script type="text/javascript">
                        $("#loader").attr('style','display:block;');
                        var openHaulageModelMapped=$("#openHaulageModelMapped").attr('value');
                        change_haulage_model_status_model_add(openHaulageModelMapped,'A','<?=$kWHSSearch->idForwarder?>','<?=$priority_active?>','<?=$_POST['cityArr']['idHaulageModel']?>','<?=$_POST['cityArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$count?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['cityArr']['fWmFactor']?>');
                        //open_pricing_model_detail('<?=$_POST['cityArr']['idHaulageModel']?>','<?=$_POST['cityArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$position?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['cityArr']['fWmFactor']?>');
                        var divid="change_data_value_<?=$_POST['cityArr']['idHaulageModel']?>";
                        $("#"+divid).html('<?=$text?>');
                        $("#callvalue").attr('value','1');
                    </script>	
                    <?php	
                    exit();				
            }
        } 
    }

    if(empty($arrCity) && empty($_POST['cityArr']))
    {
        $arrCity[0]['fFuelPercentage']=__DEFAULT_FUEL__;
        $arrCity[0]['fWmFactor']=__DEFAULT_WM_FACTOR__; 
    }
    if(!empty($arrCity))
    {
        $fFuelPercentageArr=explode(".",$arrCity[0]['fFuelPercentage']);
        if((int)$fFuelPercentageArr[1]>0)
        {
            $l=strlen($fFuelPercentageArr[1]);
            if($l==1)
            {
                $arrCity[0]['fFuelPercentage']=round($arrCity[0]['fFuelPercentage'],1);
            }
            else
            {
                $arrCity[0]['fFuelPercentage']=round($arrCity[0]['fFuelPercentage'],2);
            } 
        }
        else
        {
            $arrCity[0]['fFuelPercentage']=$fFuelPercentageArr[0];
        }
    }
    //print_r($arrCity);
    if($arrCity[0]['iDistance']==0 && $_POST['cityArr']['iDistance']==0)
    {
        //$arrCity[0]['iDistance']=__MINI_RADIUS__;
    }
    if($_POST['cityArr']['szLatLong']=='')
    {
        if($mode=='add')
        {
            $szLatitude=$kWHSSearch->szLatitude;
            $szLongitude=$kWHSSearch->szLongitude;
        }
        else if($mode=='edit')
        {
            $szLatitude=$arrCity[0]['szLatitude'];
            $szLongitude=$arrCity[0]['szLongitude'];
        }
    }
    else
    {
        $szLatLongArr=explode(",",$_POST['cityArr']['szLatLong']);
        $szLatitude=$szLatLongArr[0];
        $szLongitude=$szLatLongArr[1];
    }
    if(!empty($kHaulagePricing->arErrorMessages))
    {
            ?>
            <div id="regError" class="errorBox ">
                <div class="header"><?=t($t_base_error.'/please_following');?></div>
                <div id="regErrorList">
                    <ul>
                        <?php foreach($kHaulagePricing->arErrorMessages as $key=>$values) {
                                ?><li><?=$values?></li>
                        <?php  }  ?>
                    </ul>
                </div>
            </div>
    <?php  }else{ ?> 
            <form name="cityAddEditForm" id="cityAddEditForm" method="post">		
                            <ol style="margin:0;padding:0 0 0 20px">
                                    <li><?=t($t_base.'title/city_and_radius_distance_rate')?><br/>
                                    <div class="oh" style="margin-top:10px;">
                                        <span class="fl-50">
                                            <span class="f-size-12" style="display:block;"><?=t($t_base.'fields/city_name')?></span>
                                            <input type="text" name="cityArr[szName]" id="szName" value="<?=(($_POST['cityArr']['szName'])?$_POST['cityArr']['szName']:$arrCity[0]['szName'])?>" maxlength="30" style="width:110px;">
                                        </span>
                                        <span class="fl-50">
                                            <span class="f-size-12" style="display:block;"><?=t($t_base.'title/distance_radius')?></span>				
                                            <input type="text" name="cityArr[iDistance]" style="width:89px;" id="iDistance" size="8" value="<?=(($_POST['cityArr']['iDistance'])?$_POST['cityArr']['iDistance']:$arrCity[0]['iRadiousKM'])?>" onblur="show_city('<?=$szLatitude?>','<?=$szLongitude?>','<?=$maxRadius?>','<?=$mode?>')"> <em><?=t($t_base.'fields/km');?></em>
                                        </span>
                                    </div>
                                    <br/> 
                                    </li>
                                    <li><?=t($t_base.'title/select_maximum_expected_transit_time_from')?> <?=$transt_time_text?> <br />
                                    <select style="width:110px;margin-top:5px;" size="1" name="cityArr[idTransitTime]" id="idTransitTime" onchange="show_add_edit_city_button('<?=$mode?>')">
                                        <option value=""><?=t($t_base.'fields/select');?></option>
                                            <?php 
                                                if(!empty($haulageTransitTimeArr))
                                                {
                                                    foreach($haulageTransitTimeArr as $haulageTransitTimeArrs)
                                                    {
                                                        $iHours='';
                                                        if($haulageTransitTimeArrs['iHours']>24)
                                                        {
                                                            $iHours="< ".($haulageTransitTimeArrs['iHours']/24)." days";
                                                        }
                                                        else
                                                        {
                                                            $iHours="< ".$haulageTransitTimeArrs['iHours']." hours";
                                                        }
                                                ?>
                                                        <option value="<?=$haulageTransitTimeArrs['id']?>" <?=((($_POST['cityArr']['idTransitTime'])?$_POST['cityArr']['idTransitTime']:$arrCity[0]['idHaulageTransitTime']) ==  $haulageTransitTimeArrs['id'] ) ? "selected":""?>><?=$iHours?></option>
                                            <?php } 
                                            }
                                    ?>
                                </select>
                                <br/><br/>
                                    </li>
                                    <li><?=t($t_base.'title/type_the_weight_measure_factor_you_want_to_use_for_pricing')?><br/><input style="width:60px;margin-top:10px;" type="text" name="cityArr[fWmFactor]" id="fWmFactor" value="<?=(($_POST['cityArr']['fWmFactor'])?$_POST['cityArr']['fWmFactor']:$arrCity[0]['fWmFactor'])?>"> <em><?=t($t_base.'fields/kg')?>/<?=t($t_base.'fields/cbm')?></em><br/><br/></li>
                                    <li><?=t($t_base.'title/if_you_would_like_to_add_fuel_surcharge_on_top_of_the_price_for_this_zone_type_the_percentage_city')?><br/><input style="width:60px;margin-top:10px;" type="text" name="cityArr[fFuelPercentage]" id="fFuelPercentage" value="<?=(($_POST['cityArr']['fFuelPercentage'])?$_POST['cityArr']['fFuelPercentage']:$arrCity[0]['fFuelPercentage'])?>"> <em>% <?=t($t_base.'fields/on_top_of_price');?></em><br/><br/></li>
                            </ol>
                            <input type="hidden" value="<?=(($_POST['cityArr']['mode'])?$_POST['cityArr']['mode']:$mode)?>" id="mode" name="cityArr[mode]">
                            <input type="hidden" value="<?=(($_POST['cityArr']['szLatLong'])?$_POST['cityArr']['szLatLong']:"")?>" id="szLatLong" name="cityArr[szLatLong]">
                            <input type="hidden" value="<?=(($_POST['cityArr']['idDirection'])?$_POST['cityArr']['idDirection']:$idDirection)?>" id="idDirection" name="cityArr[idDirection]">
                            <input type="hidden" value="<?=(($_POST['cityArr']['idWarehouse'])?$_POST['cityArr']['idWarehouse']:$idWarehouse)?>" id="idWarehouse" name="cityArr[idWarehouse]">
                            <input type="hidden" value="<?=(($_POST['cityArr']['idHaulageModel'])?$_POST['cityArr']['idHaulageModel']:$idHaulageModel)?>" id="idHaulageModel" name="cityArr[idHaulageModel]">
                            <input type="hidden" value="<?=(($_POST['cityArr']['id'])?$_POST['cityArr']['id']:$id)?>" id="id" name="cityArr[id]">
                            <input type="hidden" value="<?=(($_POST['cityArr']['iPriorityLevel'])?$_POST['cityArr']['iPriorityLevel']:$iPriorityLevel)?>" id="iPriorityLevel" name="cityArr[iPriorityLevel]">
                            <p style="margin: 15px 0 12px 275px;position:absolute;bottom:0px;text-align: left;width: 315px;"><a href="javascript:void(0)" id="cancel_city" class="button2" onclick="cancel_city('<?=$idHaulageModel?>')"><span><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="save_city" class="button1" style='opacity:0.4'><span id="change_name"><?=t($t_base.'fields/add_city');?></span></a></p>
            </form>
            <?php
            if(!empty($kHaulagePricing->arErrorMessages)){
                    if($_POST['cityArr']['mode']=='edit'){
            ?>
            <script>
                    $("#save_city").attr("onclick","");
                    $("#save_city").unbind("click");
                    $("#save_city").click(function(){save_city_data()});
                    $("#save_city").attr('style',"opacity:1");
                    $("#change_name").html('Save City');
            </script>
            <?
                    }else{
                    ?>
                            <script>
                    $("#save_city").attr("onclick","");
                    $("#save_city").unbind("click");
                    $("#save_city").click(function(){add_city_data()});
                    $("#save_city").attr('style',"opacity:1");
                    $("#change_name").html('Add City');
            </script>
                    <?php
                    }
                    }
            }
    } 
    
function pricingHaulagePostCodeListing($haulagePricingPostCodeDataArr,$idHaulageModel,$idDirection,$idWarehouse)
{
    $t_base = "HaulaugePricing/";
    $kHaulagePricing = new cHaulagePricing();
    $kConfig = new cConfig();

    if($idDirection=='1')
    {
        $formto=t($t_base.'fields/from');
    }
    else
    {
        $formto=t($t_base.'fields/to');
    }
    
    $kWhsSearch = new cWHSSearch();
    $kWhsSearch->load($idWarehouse);
    $iWarehouseType = $kWhsSearch->iWarehouseType;
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    { 
        $szNoPostcodeFound = t($t_base.'messages/air_no_postcodes'); 
        $szTransitTypeZoneTip = t($t_base.'messages/air_postcode_transit_time_heading'); 
    }
    else
    {
        $szNoPostcodeFound = t($t_base.'messages/no_postcodes'); 
        $szTransitTypeZoneTip = t($t_base.'messages/postcode_transit_time_heading'); 
    }
?>	
    <hr/>
    <p style="margin-bottom: 2px;"><strong><span style="text-align:left;font-size:14px;"><?=t($t_base.'title/postcode_title_1')?> <?=$formto?> <?=t($t_base.'title/postcode_title_2')?></span></strong> <a href="javascript:void(0)" onclick="open_copy_haulage_postcode_popup('<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>');"><?=t($t_base.'links/show_all_cfs_postcode');?></a></p>
    <div class="tableScroll" style="height:134px;width:76%;float:left;" id="view_haulage_model_postcode_div">
    <table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_postcode">
        <tr>
            <!--  <td style="text-align:left;width:10%;"><strong><?=t($t_base.'fields/priority')?></strong></td> -->
            <th class="noborder" style="text-align:left;width:19%;"><strong><?=t($t_base.'fields/country')?></strong></th>
            <th style="text-align:left;width:38%;"><strong><?=t($t_base.'fields/postcode_set')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/postcode_set_tool_tip_1');?>','<?=t($t_base.'messages/postcode_set_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
            <th style="text-align:left;width:17%;"><strong><?=t($t_base.'fields/transit_time')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?php echo $szTransitTypeZoneTip; ?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
            <th style="text-align:left;width:17%;"><strong><?=t($t_base.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fm_factor_for_postcode_tool_tip_heading');?>','<?=t($t_base.'messages/fm_factor_for_postcode_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
            <th style="text-align:left;width:9%;"><strong><?=t($t_base.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fuel_postcode_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
        </tr> 
	<?php
	if(!empty($haulagePricingPostCodeDataArr))
	{
            $j=0;
            foreach($haulagePricingPostCodeDataArr as $haulagePricingPostCodeDataArrs)
            { 
                if($j==0)
                {
                    $idPricing=$haulagePricingPostCodeDataArrs['id'];
                    $fPricingWmFactor=$haulagePricingPostCodeDataArrs['fWmFactor'];
                    $idPricingHaulageModel=$haulagePricingPostCodeDataArrs['idHaulageModel'];
                    $idPricingWarehouse=$haulagePricingPostCodeDataArrs['idWarehouse'];
                    $iPricingDirection=$haulagePricingPostCodeDataArrs['iDirection'];
                }
                $CountryName=$kConfig->getCountryName($haulagePricingPostCodeDataArrs['idCountry']);
		?>
                <tr id="haulage_postcode_<?=++$j?>" onclick="sel_haulage_model_postcode_data('haulage_postcode_<?=$j?>','<?=$haulagePricingPostCodeDataArrs['id']?>','<?=$haulagePricingPostCodeDataArrs['idHaulageModel']?>','<?=$j?>','<?=$haulagePricingPostCodeDataArrs['idWarehouse']?>','<?=$haulagePricingPostCodeDataArrs['iDirection']?>','<?=$haulagePricingPostCodeDataArrs['fWmFactor']?>');">
                    <!-- <td><?=$haulagePricingPostCodeDataArrs['iPriority']?>&nbsp;Priority</td>-->
                    <td class="noborder"><?=$CountryName?></td>
                    <td><?=$haulagePricingPostCodeDataArrs['szName']?></td>
                    <td>
                        <?php 
                            if((int)$haulagePricingPostCodeDataArrs['iHours']>24)
                            {
                                echo "< ".($haulagePricingPostCodeDataArrs['iHours']/24)." ".t($t_base.'fields/days');;
                            }
                            else
                            {
                                echo "< ".$haulagePricingPostCodeDataArrs['iHours']." ".t($t_base.'fields/hours');;
                            }
                        ?>
                    </td>
                    <td><?=number_format((float)$haulagePricingPostCodeDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base.'fields/kg');?>/<?=t($t_base.'fields/cbm');?></td>
                    <td><?=number_format((float)$haulagePricingPostCodeDataArrs['fFuelPercentage'],1)?> %</td>
                </tr>
		<?php	
		}
	}
	else
	{
	?>
            <tr>
                <td class="noborder" colspan="5" align="center"><?php echo $szNoPostcodeFound;?></td>
            </tr>
	<?php }?>
	<input type="hidden" value="<?=$j?>" id="totalActive1" name="totalActive1"/>
	</table>
	</div>
	<div id="display_postcode_string" class="tableScroll" style="width: 22%; float: right;height: 134px;">
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
		 <th class="noborder" align="left"><strong><?=t($t_base.'fields/postcodes_included');?></strong></th>
		</tr>
		<tr>
		 <td class="noborder">&nbsp;</td>
		</tr>
	</table>
	</div>
	<br class="clear-all" />
	<br/>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
		<td width="10%"><?=t($t_base.'fields/priority')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/postcode_priority_heading')?>','<?=t($t_base.'messages/priority_tool_tip_postcode');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
		<td width="35%" align="left"><a href="javascript:void(0)" id="priority_up_postcode" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/up')?></span></a> <a href="javascript:void(0)" id="priority_down_postcode" style="opacity:0.4;" class="button2"><span style="min-width:60px;"><?=t($t_base.'fields/down')?></span></a></td>
		<td align="right" width="55%"><a href="javascript:void(0)" id="delete_postcode" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/delete_set')?></span></a> <a href="javascript:void(0)" id="edit_postcode" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit_set')?></span></a> <a href="javascript:void(0)" id="add_postcode"  class="button1" onclick="add_postcode_popup('<?=$idHaulageModel?>','<?=$idDirection?>','<?=$idWarehouse?>');"><span style="min-width:70px;"><?=t($t_base.'fields/add_set')?></span></a></td>
		</tr>
	</table>
	<? if($idPricing>0){?>
	<script type="text/javascript">
	var callvalue=$("#callvalue").attr('value');
	if(parseInt(callvalue)!='1')
	{
		sel_haulage_model_postcode_data('haulage_postcode_1','<?=$idPricing?>','<?=$idPricingHaulageModel?>','1','<?=$idPricingWarehouse?>','<?=$iPricingDirection?>','<?=$fPricingWmFactor?>');
	}
	$("#callvalue").attr('value','0');
	</script>
	<? }?>
	<?
	if(!empty($haulagePricingPostCodeDataArr))
	{?>
	<hr/>
	<? }?>
	<div id="postcode_detail_list_<?=$idHaulageModel?>"></div>
	<div id="postcode_detail_delete_<?=$idHaulageModel?>"></div>		
		
	<?php	
}

function copy_postcode_popup($idForwarder,$idWarehouse,$idDirection,$idHaulageModel,$id)
{
    $t_base = "HaulaugePricing/";
    $kHaulagePricing = new cHaulagePricing();
    
    $kWhsSearch = new cWHSSearch();
    $kWhsSearch->load($idWarehouse);
    $iWarehouseType = $kWhsSearch->iWarehouseType;
    
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    { 
        $szNoZoneFound = t($t_base.'messages/air_no_zone');
        $szTransitTypeZoneTip = t($t_base.'messages/air_transit_time_zone_tool_tip'); 
    }
    else
    {
        $szNoZoneFound = t($t_base.'messages/no_zone');
        $szTransitTypeZoneTip = t($t_base.'messages/transit_time_zone_tool_tip');
    } 
    
    $ret_ary = array();
    $ret_ary = $kHaulagePricing->getActiveWarehouseZone($idForwarder,$idWarehouse,$idHaulageModel);
    
?>
	<h5 style="margin-bottom:6px;"><?=t($t_base.'title/select_postcode_to_copy')?></h5>
	<p style="text-align:justify"><?=t($t_base.'messages/copy_postcode_sets_heading_line')?></p><br/>
			<form name="copyPostCode" id="copyPostCode" method="post">
				<div class="oh haulage-get-data">
					<div class="fl-25">
						<span class="f-size-12"><?=t($t_base.'fields/country')?></span>
						<select name="dataExportPostcodeArr[haulageCountryZone]" id="haulageCountryZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'country','city_warehouse_zone','<?=$idHaulageModel?>','<?php echo $iWarehouseType; ?>')">
							<option value=""><?=t($t_base.'fields/select')?></option>		
							<?php
								if(!empty($ret_ary))
								{
									foreach($ret_ary as $ret_arys)
									{
										?>
											<option value="<?=$ret_arys['id']?>"><?=$ret_arys['szCountryName']?></option>
											<?
									}
								}									
							?>
						</select>
					</div>
					<div id='city_warehouse_zone'>
					<div class="fl-25">
						<span class="f-size-12"><?=t($t_base.'fields/city');?></span>
						<select name="dataExportZoneArr[szCityZone]" disabled id="szCityZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'city','warehouse_zone','<?=$idHaulageModel?>','<?php echo $iWarehouseType; ?>')">
									<option value=""><?=t($t_base.'fields/all');?></option>
									<?php
                                                                            if(!empty($ret_ary))
                                                                            {
                                                                                foreach($ret_ary as $ret_arys)
                                                                                {
                                                                                    ?>
                                                                                    <option value="<?=$ret_arys['szCity']?>"><?=$ret_arys['szCity']?></option>
                                                                                    <?php
                                                                                }
                                                                            } 
									?>
								</select>
					</div>
					<div class="fl-25" id="warehouse_zone">
						<span class="f-size-12">Name</span>
						<select name="dataExportZoneArr[haulageWarehouseZone]" disabled id="haulageWarehouseZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value="">Select</option>
							<?php
								if(!empty($ret_ary))
								{
									foreach($ret_ary as $ret_arys)
									{
										?>
										<option value="<?=$ret_arys['warehouseId']?>"><?=$ret_arys['szWareHouseName']?></option>
										<?
									}
								}
							?>
						</select>
					</div>
					<div class="fl-25 direction">
						<span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
						<select name="dataExportZoneArr[idDirectionZone]" disabled id="idDirectionZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value='1'><?=t($t_base.'fields/export')?></option>
							<option value='2'><?=t($t_base.'fields/import')?></option>
						</select>
					</div>
					
					</div>
				</div>	
				<br/>			
				<div id="show_copy_data" class="tableScroll" style="height: 119px; overflow: auto;"></div>
				<input type="hidden" value="<?=(($_POST['dataExportPostcodeArr']['idZoneDirection'])?$_POST['dataExportPostcodeArr']['idZoneDirection']:$idDirection)?>" id="idZoneDirection" name="dataExportPostcodeArr[idZoneDirection]">
				<input type="hidden" value="<?=(($_POST['dataExportPostcodeArr']['idZoneWarehouse'])?$_POST['dataExportPostcodeArr']['idZoneWarehouse']:$idWarehouse)?>" id="idZoneWarehouse" name="dataExportPostcodeArr[idZoneWarehouse]">
				<input type="hidden" value="<?=(($_POST['dataExportPostcodeArr']['idZoneHaulageModel'])?$_POST['dataExportPostcodeArr']['idZoneHaulageModel']:$idHaulageModel)?>" id="idZoneHaulageModel" name="dataExportPostcodeArr[idZoneHaulageModel]">
				<input type="hidden" value="<?=(($_POST['dataExportPostcodeArr']['idZone'])?$_POST['dataExportPostcodeArr']['idZone']:$id)?>" id="idZone" name="dataExportPostcodeArr[idZone]">
				<input type="hidden" value="<?=(($_POST['dataExportPostcodeArr']['iCheckedCount'])?$_POST['dataExportPostcodeArr']['iCheckedCount']:"0")?>" id="iCheckedCount" name="dataExportPostcodeArr[iCheckedCount]">
				<br/><p align="center">
				<a href="javascript:void(0)" id="cancel_zone" class="button2" onclick="cancel_zone('<?=$idHaulageModel?>')"><span>Cancel</span></a><a href="javascript:void(0)" id="copy_postcode" class="button1" style="opacity:0.4"><span><?=t($t_base.'fields/copy')?></span></a></p>
			</form>	
<?php	
}



function update_postcode_form($arrPostCode=array(),$idDirection,$idWarehouse,$idHaulageModel,$id=0,$mode,$iPriorityLevel=0)
{
    $t_base_error="Error";
    $t_base = "HaulaugePricing/";
    $Excel_export_import=new cExport_Import();
    $kHaulagePricing = new cHaulagePricing();
    $kWHSSearch = new cWHSSearch();
    $kWHSSearch->load($idWarehouse); 
    $iWarehouseType = $kWHSSearch->iWarehouseType;
    
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    { 
        $szCFSText = "airport warehouse"; 
    }
    else
    {
        $szCFSText = "CFS"; 
    }
    
    $maxDistance=$kWHSSearch->getManageMentVariableByDescription('__MAX_DISTANCE_FOR_COUNTRY_INCLUDED__');

    $kConfig = new cConfig();
    $allCountriesArr=$kHaulagePricing->getAllCountriesInDefinedDistance($maxDistance,$kWHSSearch->szLatitude,$kWHSSearch->szLongitude);

    $szPostCodeArr=array();
    $szPostCodeArr=$kHaulagePricing->getSetOfPostCode($id,true);
    if(!empty($szPostCodeArr))
    {
            $szPostCodeStr1=implode(",",$szPostCodeArr);
    }
    if(!empty($_POST['postcodeArr']['szPostCodeStr1']))
    {
            $szPostCodeArr=explode(",",$_POST['postcodeArr']['szPostCodeStr1']);
    }
    if($idDirection=='1')
    {
            $direction=t($t_base.'fields/Export');
            $formto=t($t_base.'fields/to');
            $tofrom=t($t_base.'fields/from');
            $transt_time_text=t($t_base.'messages/these_postcodes_to')." ".$szCFSText;
    }
    else
    {
            $direction=t($t_base.'fields/Import');
            $formto=t($t_base.'fields/from');
            $tofrom=t($t_base.'fields/to');

            $transt_time_text=t($t_base.'messages/the')." ".$szCFSText." ".t($t_base.'messages/to_these_postodes');
    }
    $haulageTransitTimeArr=$Excel_export_import->gColumnHaulageData(true);

	if(!empty($_POST['postcodeArr']))
	{
		if($_POST['postcodeArr']['mode']=='edit')
		{
                    if($kHaulagePricing->updatePostCode($_POST['postcodeArr']))
                    {?>
                            <script type="text/javascript">
                            $("#loader").attr('style','display:block;');
                            $("#update_postcode_form_add_edit").html('');
                            open_pricing_model_detail('<?=$_POST['postcodeArr']['idHaulageModel']?>','<?=$_POST['postcodeArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$_POST['postcodeArr']['iPriorityLevel']?>','<?=$_POST['postcodeArr']['id']?>','<?=$idDirection?>','<?=$_POST['postcodeArr']['fWmFactor']?>');
                            </script>	
                    <?php	
                    exit();		
                    }
		}
		
		if($_POST['postcodeArr']['mode']=='add')
		{
			$idPricing=$kHaulagePricing->addPostCode($_POST['postcodeArr']);
			if((int)$idPricing>0)
			{
				$data['idDirection']=$idDirection;
				$data['idWarehouse']=$_POST['postcodeArr']['idWarehouse'];
				$data['idHaulageModel']=$_POST['postcodeArr']['idHaulageModel'];
				$haulagePricingModelArr=$kHaulagePricing->getWarehouseHaulageModelList($data);
				$haulageZoneNewArr=$kHaulagePricing->viewPricingHaulageZoneModel($idWarehouse,$idHaulageModel,$idDirection,'postcode');
				$position=getAddedCityPosition($haulageZoneNewArr,$_POST['postcodeArr']['szName']);
				if(!empty($haulagePricingModelArr))
				{
					foreach($haulagePricingModelArr as $haulagePricingModelArrs)
					{
						if($haulagePricingModelArrs['idHaulageModel']=='3')
						{
							$count=$haulagePricingModelArrs['iCount'];
							if((int)$haulagePricingModelArrs['iCount']>0)
							{
								if((int)$haulagePricingModelArrs['iCount']==1)
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/postcode_set_defined');
								else
									$text=$haulagePricingModelArrs['iCount']." ".t($t_base.'fields/postcode_sets_defined');	
							}
							else
								$text=t($t_base.'fields/no_postcode_sets_defined');
						}
					}
				}
				else
				{
					$text=t($t_base.'fields/no_postcode_sets_defined');
				}
				
				$priority_active=$kHaulagePricing->getPriorityCFSHaulageModel($_POST['postcodeArr']['idWarehouse'],$_POST['postcodeArr']['idHaulageModel'],$idDirection);
				if((int)$priority_active>0)
				{
					$priority_active_text=$priority_active." ".t($t_base.'fields/priority');
				}
				else
				{
					$priority_active_text=t($t_base.'fields/not_active');
				}
				?>
				<script type="text/javascript">
				$("#loader").attr('style','display:block;');
				var openHaulageModelMapped=$("#openHaulageModelMapped").attr('value');
				change_haulage_model_status_model_add(openHaulageModelMapped,'A','<?=$idForwarder?>','<?=$priority_active?>','<?=$_POST['postcodeArr']['idHaulageModel']?>','<?=$_POST['postcodeArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$count?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['postcodeArr']['fWmFactor']?>');
				//open_pricing_model_detail('<?=$_POST['postcodeArr']['idHaulageModel']?>','<?=$_POST['postcodeArr']['idWarehouse']?>','<?=__BASE_STORE_FORWARDER_IMAGE_URL__?>','<?=$count?>','<?=$idPricing?>','<?=$idDirection?>','<?=$_POST['postcodeArr']['fWmFactor']?>');
				var divid="change_data_value_<?=$_POST['postcodeArr']['idHaulageModel']?>";
				$("#"+divid).html('<?=$text?>');
				$("#callvalue").attr('value','1');
				</script>	
			<?php	
			exit();				
			}
		}
		
	}
	if(empty($arrPostCode) && empty($_POST['postcodeArr']))
	{
		$arrPostCode[0]['fFuelPercentage']=__DEFAULT_FUEL__;
		$arrPostCode[0]['fWmFactor']=__DEFAULT_WM_FACTOR__;
	}
	
	if(!empty($arrPostCode))
	{
		$fFuelPercentageArr=explode(".",$arrPostCode[0]['fFuelPercentage']);
		if((int)$fFuelPercentageArr[1]>0)
		{
			$l=strlen($fFuelPercentageArr[1]);
			if($l==1)
			{
				$arrPostCode[0]['fFuelPercentage']=round($arrPostCode[0]['fFuelPercentage'],1);
			}
			else
			{
				$arrPostCode[0]['fFuelPercentage']=round($arrPostCode[0]['fFuelPercentage'],2);
			}
			
		}
		else
		{
			$arrPostCode[0]['fFuelPercentage']=$fFuelPercentageArr[0];
		}
	}
	
	if(empty($arrPostCode[0]['idCountry']) && empty($_POST['postcodeArr']))
	{
		$arrPostCode[0]['idCountry']=$kWHSSearch->idCountry;
	}
	if(!empty($kHaulagePricing->arErrorMessages)){
	?>
	<div id="regError" class="errorBox ">
	<div class="header"><?=t($t_base_error.'/please_following');?></div>
	<div id="regErrorList">
	<ul>
	<?php
		foreach($kHaulagePricing->arErrorMessages as $key=>$values)
		{
		?><li><?=$values?></li>
		<?php	
		}
	?>
	</ul>
	</div>
	</div>
	<?php
	}
	
	?>

	<form name="postcodeAddEditForm" id="postcodeAddEditForm" method="post">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom:10px;">
				<tr>
					<td width="70%" style="line-height:19px;" valign="top">
						<ol style="width:94%;">
							<li><?=t($t_base.'title/give_the_postcode_set_a_name_you_will_refer_to_it_by');?></li>
							<li><?=t($t_base.'title/select_the_country_where_the_postcodes_reside');?></li>
							<li  style="padding-bottom:13px"><p><?=t($t_base.'title/develop_the_postcoed_list');?></p>
							<i class="f-size-12" style="line-height: 15px;margin-top:10px;display:block;">
									<?=t($t_base.'title/example');?><br/>
									<?=t($t_base.'title/example_1');?><br/>
									<?=t($t_base.'title/example_2');?><br/>
									<?=t($t_base.'title/example_3');?></i>									
							</li>
							<li><?=t($t_base.'title/select_maximum_expected_transit_time_from');?> <?=$transt_time_text?></li>
							<li><?=t($t_base.'title/type_the_weight_measure');?> <?=$tofrom?> <?=t($t_base.'title/these_postcodes');?></li>
							<li><?=t($t_base.'title/if_fuel_surcharge_on_top_postcode');?></li>
						</ol>
					</td>
					<td width="30%" valign="top">
						<div><input style="width:230px" type="text" value="<?=(($_POST['postcodeArr']['szName'])?$_POST['postcodeArr']['szName']:$arrPostCode[0]['szName'])?>" id="szName" name="postcodeArr[szName]" maxlength="30" onblur="enable_add_edit_button('<?=$mode?>')"></div>
						<div>
							<select style="width:236px" size="1" name="postcodeArr[szCountry]" id="szCountry">
								<option value=""><?=t($t_base.'fields/select');?></option>
								<?php
									if(!empty($allCountriesArr))
									{
										foreach($allCountriesArr as $allCountriesArrs)
										{
											//if(($idDirection=='1' && $allCountriesArrs['iActiveFromDropdown']!='0') || ($idDirection=='2' && $allCountriesArrs['iActiveToDropdown']!='0'))
											//{
												?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['postcodeArr']['szCountry'])?$_POST['postcodeArr']['szCountry']:$arrPostCode[0]['idCountry']) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option>
												<?php
											//}
										}
									}
								?>
							</select>
						</div>
						<div><input style="width:135px;color:grey;font-style:italic;" type="text" value="<?=(($_POST['postcodeArr']['szPostCode'])?$_POST['postcodeArr']['szPostCode']: ((!empty($arrPostCode[0]['szPostCode'])))? $arrPostCode[0]['szPostCode']: 'Type code' )?>" onblur="show_me_postcode(this.id,'Type code');" onfocus="blank_me_postcode(this.id,'Type code');" id="szPostCode" name="postcodeArr[szPostCode]" onkeyup="press_enter_key_add_postcode_string(event,'<?=$mode?>')">
							<a href="javascript:void(0)" id="add_postcode_str" class="button1" onclick="add_postcode_string('<?=$mode?>');enable_add_edit_button('<?=$mode?>')"><span style="min-width: 63px;"><?=t($t_base.'fields/add')?></span></a>
						</div>
						
						<div style="overflow:hidden;width: 236px;">
							<div id="list_postcode_arr" style="float:left;width:140px;margin:0;">
								<select  style="width: 140px;height: 100px;" multiple="multiple" id="szPostCodeStr" name="postcodeArr[szPostCodeStr]" onclick="delete_str_postcode('<?=$mode?>')">
								<? if(!empty($szPostCodeArr))
								{
									foreach($szPostCodeArr as $szPostCodeArrs)
									{
										?>
										<option value="<?=$szPostCodeArrs?>"><?=$szPostCodeArrs?></option>
										<?
									}
								}?>
								</select>
							</div>
							<a href="javascript:void(0)" id="delete_postcode_str" class="button2" style="opacity:0.4;float:right;margin-top:70px;"><span style="min-width:62px;"><?=t($t_base.'fields/delete')?></span></a>
						</div>
                                                <div <?php if($iWarehouseType==__WAREHOUSE_TYPE_AIR__) {?> style="margin-bottom: 27px;" <?php }?>>
							<select style="width:132px;" size="1" name="postcodeArr[idTransitTime]" id="idTransitTime" onchange="enable_add_edit_button('<?=$mode?>')">
								<option value=""><?=t($t_base.'fields/select');?></option>
								<?php if(!empty($haulageTransitTimeArr))
									{
										foreach($haulageTransitTimeArr as $haulageTransitTimeArrs)
										{
												$iHours='';
												if($haulageTransitTimeArrs['iHours']>24)
												{
													$iHours="< ".($haulageTransitTimeArrs['iHours']/24)." days";
												}
												else
												{
													$iHours="< ".$haulageTransitTimeArrs['iHours']." hours";
												}
											?>
											<option value="<?=$haulageTransitTimeArrs['id']?>" <?=((($_POST['postcodeArr']['idTransitTime'])?$_POST['postcodeArr']['idTransitTime']:$arrPostCode[0]['idHaulageTransitTime']) ==  $haulageTransitTimeArrs['id'] ) ? "selected":""?>><?=$iHours?></option>
										<?php }
									}
								?>
							</select>
						</div>
						<div style="margin-bottom: 26px;"><input style="width:80px;" type="text" value="<?=(($_POST['postcodeArr']['fWmFactor'])?$_POST['postcodeArr']['fWmFactor']:round($arrPostCode[0]['fWmFactor']))?>" id="mode" name="postcodeArr[fWmFactor]"> <em style="font-size:14px;"><?=t($t_base.'fields/kg');?>/<?=t($t_base.'fields/cbm');?></em></div>
						<div><input style="width:80px;" type="text" value="<?=(($_POST['postcodeArr']['fFuelPercentage'])?$_POST['postcodeArr']['fFuelPercentage']:$arrPostCode[0]['fFuelPercentage'])?>" id="mode" name="postcodeArr[fFuelPercentage]"> <em style="font-size:14px;">% <?=t($t_base.'fields/on_top_of_price');?></em></div>
					</td>
				</tr>
			</table>			
			<input type="hidden" value="<?=(($_POST['postcodeArr']['szPostCodeStr1'])?$_POST['postcodeArr']['szPostCodeStr1']:$szPostCodeStr1)?>" id="szPostCodeStr1" name="postcodeArr[szPostCodeStr1]">
			<input type="hidden" value="<?=(($_POST['postcodeArr']['mode'])?$_POST['postcodeArr']['mode']:$mode)?>" id="mode" name="postcodeArr[mode]">
			<input type="hidden" value="<?=(($_POST['postcodeArr']['idDirection'])?$_POST['postcodeArr']['idDirection']:$idDirection)?>" id="idDirection" name="postcodeArr[idDirection]">
			<input type="hidden" value="<?=(($_POST['postcodeArr']['idWarehouse'])?$_POST['postcodeArr']['idWarehouse']:$idWarehouse)?>" id="idWarehouse" name="postcodeArr[idWarehouse]">
			<input type="hidden" value="<?=(($_POST['postcodeArr']['idHaulageModel'])?$_POST['postcodeArr']['idHaulageModel']:$idHaulageModel)?>" id="idHaulageModel" name="postcodeArr[idHaulageModel]">
			<input type="hidden" value="<?=(($_POST['postcodeArr']['id'])?$_POST['postcodeArr']['id']:$id)?>" id="id" name="postcodeArr[id]">
			<input type="hidden" value="<?=(($_POST['postcodeArr']['iPriorityLevel'])?$_POST['postcodeArr']['iPriorityLevel']:$iPriorityLevel)?>" id="iPriorityLevel" name="postcodeArr[iPriorityLevel]">
			<p align="center"><a href="javascript:void(0)" id="cancel_postcode" class="button2" onclick="cancel_postcode('<?=$idHaulageModel?>')"><span><?=t($t_base.'fields/cancel');?></span></a> <a href="javascript:void(0)" id="save_postcode" class="button1" style="opacity:0.4"><span id="change_text"><?=t($t_base.'fields/add_set')?></span></a></p>
	</form>
	<?php
	if(!empty($kHaulagePricing->arErrorMessages) && $_POST['postcodeArr']['mode']=='edit'){
	?>
	<script>
		$("#save_postcode").attr("onclick","");
		$("#save_postcode").unbind("click");
		$("#save_postcode").click(function(){save_postcode_data()});
		$("#change_text").html('Save Set');
		$("#save_postcode").attr('style',"opacity:1");
	</script>
	<?php
	}
} 

function cfs_pricing_model_list($haulagePricingDataArr)
{
    $idWarehouse = $haulagePricingDataArr[0]['idWarehouse']; 
    $kWhsSearch = new cWHSSearch();
    $kWhsSearch->load($idWarehouse);
    $iWarehouseType = $kWhsSearch->iWarehouseType;
    
    $t_base = "HaulaugePricing/";
     if($haulagePricingDataArr){?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model">
            <tr>
                <td width="10%" style="text-align:left;width:10%"><strong><?=t($t_base.'fields/status')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_pricing_model_status_tool_tip_1');?>','<?=t($t_base.'messages/haulage_pricing_model_status_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                <td width="50%" style="text-align:left;width:50%"><strong><?=t($t_base.'fields/pricing_model')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/haulage_pricing_model_tool_tip_1');?>','<?=t($t_base.'messages/haulage_pricing_model_tool_tip_2');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></td>
                <td width="40%" style="text-align:left;width:40%"><strong><?=t($t_base.'fields/current_content')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/haulage_pricing_model_current_content_tool_tip_1');?>','<?=t($t_base.'messages/haulage_pricing_model_current_content_tool_tip_2');?>','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></td>
            </tr>
            <?php
            $j=0;
            $a=0;  
            foreach($haulagePricingDataArr as $haulagePricingDataArrs)
            { 
                $updownflag='';
                if($haulagePricingDataArrs['iActive']=='1')
                {
                    $updownflag='y';
                    ++$a;
                }
                else
                {
                    $updownflag='n';
                } 	
                if($haulagePricingDataArrs['idHaulageModel']==4)
                {
                    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
                    {  
                        $szModelLongName = __DISTANCE_MODEL_LONG_NAME_AIR__;
                    }
                    else
                    {
                        $szModelLongName = __DISTANCE_MODEL_LONG_NAME_CFS__;
                    } 
                }
                else
                {
                    $szModelLongName = $haulagePricingDataArrs['szModelLongName'];
                }
		?>
                <tr id="haulage_data_<?=++$j?>" onclick="sel_haulage_model_data('haulage_data_<?=$j?>','<?=$idForwarder?>','<?=$updownflag?>','<?=$haulagePricingDataArrs['idMapping']?>','<?=$j?>','<?=$haulagePricingDataArrs['idHaulageModel']?>');">
                    <td width="10%" style="text-align:left;width:10%">
                        <div id="change_priority_value_<?=$haulagePricingDataArrs['idHaulageModel']?>">
                            <?php 
                                if($haulagePricingDataArrs['iActive']=='1')
                                    echo $haulagePricingDataArrs['iPriority']." ".t($t_base.'fields/priority');
                                else
                                    echo t($t_base.'fields/not_active'); 
                            ?>
                        </div>
                    </td>
                    <td width="50%" style="text-align:left;width:50%"><?php echo $szModelLongName; ?></td>
                    <td width="40%" style="text-align:left;width:40%">
                        <div id="change_data_value_<?=$haulagePricingDataArrs['idHaulageModel']?>">
                        <?php
                            if($haulagePricingDataArrs['idHaulageModel']=='1')
                            {
                                if((int)$haulagePricingDataArrs['iCount']>0)
                                {
                                    if((int)$haulagePricingDataArrs['iCount']==1)
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/zone_defined');
                                    else
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/zones_defined');	
                                }
                                else
                                {
                                    echo t($t_base.'fields/no_zones_defined');
                                }
                            }
                            else if($haulagePricingDataArrs['idHaulageModel']=='2')
                            {
                                if((int)$haulagePricingDataArrs['iCount']>0)
                                {
                                    if((int)$haulagePricingDataArrs['iCount']==1)
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/city_defined');
                                    else
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/cities_defined');
                                }
                                else
                                    echo t($t_base.'fields/no_cities_defined');
                            }
                            else if($haulagePricingDataArrs['idHaulageModel']=='3')
                            {
                                if((int)$haulagePricingDataArrs['iCount']>0)
                                {
                                    if((int)$haulagePricingDataArrs['iCount']==1)
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/postcode_set_defined');
                                    else
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/postcode_sets_defined');	
                                }
                                else
                                    echo t($t_base.'fields/no_postcode_sets_defined');
                            }
                            else if($haulagePricingDataArrs['idHaulageModel']=='4')
                            {
                                if((int)$haulagePricingDataArrs['iCount']>0)
                                {
                                    if((int)$haulagePricingDataArrs['iCount']==1)
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/distance_bracket_defined');
                                    else
                                        echo $haulagePricingDataArrs['iCount']." ".t($t_base.'fields/distance_brackets_defined');
                                }
                                else
                                    echo t($t_base.'fields/no_distance_brackets_defined');
                            }
                        ?>
                        </div>
                    </td>
                </tr>
		<?php } ?>
                <input type="hidden" value="<?=$a?>" id="totalActive" name="totalActive"/> 
        </table>
        <?php
    }
} 

function pricingHaulageDistanceListing($haulagePricingDistanceDataArr,$idHaulageModel,$idDirection,$idWarehouse)
{
    $t_base = "HaulaugePricing/";
    $kHaulagePricing = new cHaulagePricing();
    $kConfig = new cConfig();
    if($idDirection=='1')
    {
        $formto=t($t_base.'fields/from');
    }
    else
    {
        $formto=t($t_base.'fields/to');
    }
    
    $kWhsSearch = new cWHSSearch();
    $kWhsSearch->load($idWarehouse);
    $iWarehouseType = $kWhsSearch->iWarehouseType;
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    { 
        $szNoDistanceFound = t($t_base.'messages/air_no_distance');
        $szDistanceSectionTitle = t($t_base.'title/air_distance_title_1'); 
        
        $szDistanceHelpToolTipe = t($t_base.'messages/air_distance_tool_tip');
        $szTransitTimeHelpToolTipe = t($t_base.'messages/air_transit_time_tool_tip_distance');
    }
    else
    {
        $szNoDistanceFound = t($t_base.'messages/no_distance');
        $szDistanceSectionTitle = t($t_base.'title/distance_title_1');
        
        $szDistanceHelpToolTipe = t($t_base.'messages/distance_tool_tip');
        $szTransitTimeHelpToolTipe = t($t_base.'messages/transit_time_tool_tip_distance'); 
    }
?>	
    <hr/>
    <p style="margin-bottom: 2px;"><strong><span style="text-align:left;font-size:14px;"><?php echo $szDistanceSectionTitle; ?></span></strong> <a href='javascript:void(0)' id='copy_link' onclick="open_copy_haulage_distance_popup('<?=$idHaulageModel?>','<?=$idWarehouse?>','<?=$idDirection?>');"><?=t($t_base.'links/show_all_cfs_distance');?></a></p>
    <div class="tableScroll" style="height:134px;width:76%;float:left;">
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="view_haulage_model_distance" style="float:left;">
            <tr>
                <th class="noborder" width="20%" style="text-align:left;width:20%"><strong><?=t($t_base.'fields/distance');?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?php echo $szDistanceHelpToolTipe; ?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
                <th width="20%" style="text-align:left;width:20%"><strong><?=t($t_base.'fields/transit_time');?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?php echo $szTransitTimeHelpToolTipe;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
                <th width="20%" style="text-align:left;width:20%"><strong><?=t($t_base.'fields/fWmFactor')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fm_factor_for_distance_tool_tip_heading');?>','<?=t($t_base.'messages/fm_factor_for_distance_tool_tip_text');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
                <th width="10%" style="text-align:left;width:20%"><strong><?=t($t_base.'fields/fuel')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/fuel_distance_tool_tip');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></th>
            </tr> 
	<?php
            if(!empty($haulagePricingDistanceDataArr))
            {
                $j=0;
                foreach($haulagePricingDistanceDataArr as $haulagePricingDistanceDataArrs)
                { 
                    if($j==0)
                    {
                        $idPricing=$haulagePricingDistanceDataArrs['id'];
                        $fPricingWmFactor=$haulagePricingDistanceDataArrs['fWmFactor'];
                        $idPricingHaulageModel=$haulagePricingDistanceDataArrs['idHaulageModel'];
                        $idPricingWarehouse=$haulagePricingDistanceDataArrs['idWarehouse'];
                        $iPricingDirection=$haulagePricingDistanceDataArrs['iDirection'];
                    }
		?>
                <tr id="haulage_distance_<?=++$j?>" onclick="sel_haulage_model_distance_data('haulage_distance_<?=$j?>','<?=$haulagePricingDistanceDataArrs['id']?>','<?=$haulagePricingDistanceDataArrs['idHaulageModel']?>','<?=$j?>','<?=$haulagePricingDistanceDataArrs['idWarehouse']?>','<?=$haulagePricingDistanceDataArrs['iDirection']?>');">
                    <td class="noborder"><?=t($t_base.'fields/up_to')?> <?=number_format((int)$haulagePricingDistanceDataArrs['iDistanceUpToKm'])?> <?=t($t_base.'fields/km')?></td>
                    <td>
                        <?php 
                            if((int)$haulagePricingDistanceDataArrs['iHours']>24)
                            {
                                echo "< ".($haulagePricingDistanceDataArrs['iHours']/24)." ".t($t_base.'fields/days');
                            }
                            else
                            {
                                    echo "< ".$haulagePricingDistanceDataArrs['iHours']." ".t($t_base.'fields/hours');
                            }
                        ?>
                    </td>
                    <td><?=number_format((int)$haulagePricingDistanceDataArrs['fWmFactor'])?>&nbsp;<?=t($t_base.'fields/kg')?>/<?=t($t_base.'fields/cbm')?></td>
                    <td><?=number_format((float)$haulagePricingDistanceDataArrs['fFuelPercentage'],1)?> %</td>
                </tr>
		<?php	
		}
	}else{
	?>
            <tr>
                <td class="noborder" colspan="4" align="center"><?php echo $szNoDistanceFound; ?></td>
            </tr>
	<?php } ?>
	<input type="hidden" value="<?=$j?>" id="totalActive1" name="totalActive1"/>
	</table>
	</div>
	<div id="display_distance_string" class="tableScroll" style="width: 22%; float: right;height: 134px;">
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr >
		 <th class="noborder" align="left"><strong><?=t($t_base.'fields/countries_included')?></strong><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip_right('<?=t($t_base.'messages/countries_include_tool_tip');?>','','customs_clearance_pop_right',event);" onmouseout="hide_tool_tip('customs_clearance_pop_right')">&nbsp;</a></th>
		</tr>
		<tr>
		 <td class="noborder">&nbsp;</td>
		</tr>
	</table>
	</div>
	<?php if($idPricing>0){?>
	<script>
	var callvalue=$("#callvalue").attr('value');
	if(parseInt(callvalue)!='1')
	{
		sel_haulage_model_distance_data('haulage_distance_1','<?=$idPricing?>','<?=$idPricingHaulageModel?>','1','<?=$idPricingWarehouse?>','<?=$iPricingDirection?>');
	}
	$("#callvalue").attr('value','0');
	</script>
	<? }?>
	<br class="clear-all" />
	<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top: 8px;">
		<tr>
		<td align="right"><a href="javascript:void(0)" id="delete_distance" style="opacity:0.4;" class="button2"><span style="min-width:70px;"><?=t($t_base.'fields/delete_line')?></span></a> <a href="javascript:void(0)" id="edit_distance" style="opacity:0.4;" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/edit_line')?></span></a> <a href="javascript:void(0)" id="add_distance"  class="button1" onclick="add_distance_popup('<?=$idHaulageModel?>','<?=$idDirection?>','<?=$idWarehouse?>');"><span style="min-width:70px;"><?=t($t_base.'fields/add_line')?></span></a></td>
		</tr>
	</table>
	<?php
	if(!empty($haulagePricingDistanceDataArr))
	{?>
	<hr/>
	<? }?>
	<div id="distance_detail_list_<?=$idHaulageModel?>"></div>
	<div id="distance_detail_delete_<?=$idHaulageModel?>"></div>		
		
	<?php	
} 


function copy_distance_popup($idForwarder,$idWarehouse,$idDirection,$idHaulageModel,$id)
{
    $t_base = "HaulaugePricing/";
    $kHaulagePricing = new cHaulagePricing();
    $ret_ary=$kHaulagePricing->getActiveWarehouseZone($idForwarder,$idWarehouse,$idHaulageModel);
?>
	<h5 style="margin-bottom:6px;"><?=t($t_base.'title/select_distance_to_copy')?></h5>
	<p style="text-align:justify"><?=t($t_base.'messages/copy_distance_heading_line')?></p><br/>
			<form name="copyDistance" id="copyDistance" method="post">
				<div class="oh haulage-get-data">
                                    <div class="fl-25">
                                        <span class="f-size-12"><?=t($t_base.'fields/country')?></span>
                                        <select name="dataExportDistanceArr[haulageCountryZone]" id="haulageCountryZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'country','city_warehouse_zone','<?=$idHaulageModel?>')">
                                            <option value=""><?=t($t_base.'fields/select')?></option>		
                                            <?php
                                                if(!empty($ret_ary))
                                                {
                                                    foreach($ret_ary as $ret_arys)
                                                    {
                                                        ?>
                                                        <option value="<?=$ret_arys['id']?>"><?=$ret_arys['szCountryName']?></option>
                                                        <?php
                                                    }
                                                } 
                                            ?>
                                        </select>
                                    </div>
					<div id='city_warehouse_zone'>
						<div class="fl-25">
						<span class="f-size-12"><?=t($t_base.'fields/city');?></span>
						<select name="dataExportZoneArr[szCityZone]" disabled id="szCityZone" onchange="showCopyWarehouse('<?=$idForwarder?>',this.value,'city','warehouse_zone','<?=$idHaulageModel?>')">
									<option value=""><?=t($t_base.'fields/all');?></option>
									<?php
                                                                            if(!empty($ret_ary))
                                                                            {
                                                                                foreach($ret_ary as $ret_arys)
                                                                                {
                                                                                    ?>
                                                                                    <option value="<?=$ret_arys['szCity']?>"><?=$ret_arys['szCity']?></option>
                                                                                    <?php
                                                                                }
                                                                            } 
									?>
								</select>
					</div>
					<div class="fl-25" id="warehouse_zone">
						<span class="f-size-12">Name</span>
						<select name="dataExportZoneArr[haulageWarehouseZone]" disabled id="haulageWarehouseZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value="">Select</option>
							<?php
								if(!empty($ret_ary))
								{
									foreach($ret_ary as $ret_arys)
									{
                                                                            ?>
                                                                            <option value="<?=$ret_arys['warehouseId']?>"><?=$ret_arys['szWareHouseName']?></option>
                                                                            <?php
									}
								}
							?>
						</select>
					</div>
					<div class="fl-25 direction">
						<span class="f-size-12"><?=t($t_base.'fields/direction')?></span>
						<select name="dataExportZoneArr[idDirectionZone]" disabled id="idDirectionZone" onchange="showWarehouseZoneData('<?=$idForwarder?>','<?=$idHaulageModel?>')">
							<option value='1'><?=t($t_base.'fields/export')?></option>
							<option value='2'><?=t($t_base.'fields/import')?></option>
						</select>
					</div>
					
					</div>
				</div>
				<br/>
				<div id="show_copy_data" class="tableScroll" style="height: 119px; overflow: auto;">
				
				</div>
				<input type="hidden" value="<?=(($_POST['dataExportDistanceArr']['idZoneDirection'])?$_POST['dataExportDistanceArr']['idZoneDirection']:$idDirection)?>" id="idZoneDirection" name="dataExportDistanceArr[idZoneDirection]">
				<input type="hidden" value="<?=(($_POST['dataExportDistanceArr']['idZoneWarehouse'])?$_POST['dataExportDistanceArr']['idZoneWarehouse']:$idWarehouse)?>" id="idZoneWarehouse" name="dataExportDistanceArr[idZoneWarehouse]">
				<input type="hidden" value="<?=(($_POST['dataExportDistanceArr']['idZoneHaulageModel'])?$_POST['dataExportDistanceArr']['idZoneHaulageModel']:$idHaulageModel)?>" id="idZoneHaulageModel" name="dataExportDistanceArr[idZoneHaulageModel]">
				<input type="hidden" value="<?=(($_POST['dataExportDistanceArr']['idZone'])?$_POST['dataExportDistanceArr']['idZone']:$id)?>" id="idZone" name="dataExportDistanceArr[idZone]">
				<input type="hidden" value="<?=(($_POST['dataExportDistanceArr']['iCheckedCount'])?$_POST['dataExportDistanceArr']['iCheckedCount']:"0")?>" id="iCheckedCount" name="dataExportDistanceArr[iCheckedCount]">
				<br/><p align="center">
				<a href="javascript:void(0)" id="cancel_zone" class="button2" onclick="cancel_zone('<?=$idHaulageModel?>')"><span>Cancel</span></a><a href="javascript:void(0)" id="copy_distance" class="button1" style="opacity:0.4"><span><?=t($t_base.'fields/copy')?></span></a></p>
			</form>	
<?php	
}