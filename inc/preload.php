<?php
ini_set('short_open_tag',"On");
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) ); 
require_once( __APP_PATH__ . "/inc/constants.php" );
//require_once( __APP_PATH_CLASSES__ . "/config.class.php" );
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(E_ALL); 
        
$file_name='';
function __autoload($class_name)
{
    switch ($class_name)
    {
        case "cConfig":
            $file_name = __APP_PATH_CLASSES__."/config.class.php";
            break; 
        case "cWHSSearch":
            $file_name = __APP_PATH_CLASSES__."/warehouseSearch.class.php";
            break;
        case "cDatabase":
            $file_name = __APP_PATH_CLASSES__."/database.class.php";
            break;
        case "CError":
            $file_name = __APP_PATH_CLASSES__."/error.class.php";
            break;
        case "Horde_Yaml":
            $file_name = __APP_PATH__."/lib/Horde/Yaml.php";
            break;
        case "Horde_Yaml_Loader":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Loader.php";
            break;
        case "Horde_Yaml_Node":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Node.php";
            break;
        case "Horde_Yaml_Dumper":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Dumper.php";
            break; 
        case "Horde_Yaml_Exception":
            $file_name = __APP_PATH__."/lib/Horde/Yaml/Exception.php";
            break;	
        case "cAdmin":
            $file_name =  __APP_PATH_CLASSES__.'/admin.class.php';
            break;  
        case "cServices":
            $file_name =  __APP_PATH_CLASSES__."/services.class.php";
            break;	
        case "cUser":
            $file_name =  __APP_PATH_CLASSES__."/user.class.php";
            break;
        case "cRegisterShipCon":
            $file_name =  __APP_PATH_CLASSES__."/registeredShippersConsignees.class.php";
            break;
        case "cBooking":
            $file_name =  __APP_PATH_CLASSES__."/booking.class.php";
            break;
       case "cForwarder":
            $file_name =  __APP_PATH_CLASSES__."/forwarder.class.php";
            break;
       case "HTML2FPDF":
            $file_name =  __APP_PATH__.'/forwarders/html2pdf/html2fpdf.php';
            break;
       case "FPDF":
            $file_name =  __APP_PATH__.'/invoice/fpdf.php';
            break;
       case "cExport_Import":
            $file_name =  __APP_PATH_CLASSES__.'/exportimport.class.php';
            break;
       case "cPayPal":
            $file_name =  __APP_PATH_CLASSES__.'/paypal.class.php';
            break;
       case "cZooz":
            $file_name =  __APP_PATH_CLASSES__.'/zooz.class.php';
            break;
       case "FPDFHTML":
            $file_name =  __APP_PATH__.'/forwarders/html2pdf/fpdf.php';
       case "cForwarderContact":
            $file_name =  __APP_PATH_CLASSES__.'/forwarderContact.class.php';
            break;
       case "cUploadBulkService":
            $file_name =  __APP_PATH_CLASSES__.'/uploadBulkService.class.php';
            break;
       case "cAffiliate":
            $file_name =  __APP_PATH_CLASSES__.'/affiliate.class.php';
            break;
       case "cDBImport":
            $file_name =  __APP_PATH_CLASSES__.'/dbimport.class.php';
            break;           
       case "cAdmin":
            $file_name =  __APP_PATH_CLASSES__.'/admin.class.php';
            break;
        case "cRSS":
            $file_name =  __APP_PATH_CLASSES__.'/rss.class.php';
            break;         
         case "cHaulagePricing":
            $file_name =  __APP_PATH_CLASSES__.'/haulagePricing.class.php';
            break;
         case "cExplain":
            $file_name =  __APP_PATH_CLASSES__.'/explain.class.php';
            break;    
        case "cBilling":
            $file_name =  __APP_PATH_CLASSES__.'/billing.class.php';
            break; 
        case "cDashboardAdmin":
            $file_name =  __APP_PATH_CLASSES__.'/dashboardAdmin.class.php';
            break;  
       case "cReport":
            $file_name =  __APP_PATH_CLASSES__.'/report.class.php';
            break;
        case "cNPS":
            $file_name =  __APP_PATH_CLASSES__.'/nps.class.php';
            break;    
        case "cSEO":
            $file_name =  __APP_PATH_CLASSES__.'/seo.class.php';
            break;
       case "cInsurance":
            $file_name =  __APP_PATH_CLASSES__.'/insurance.class.php';
            break;
       case "PHPExcel":
            $file_name =  __APP_PATH_CLASSES__.'/PHPExcel.class.php';
            break; 
      case "cBookingPdf":
            $file_name =  __APP_PATH_CLASSES__.'/pdf.class.php';
            break; 
        case "cCapsuleCrm":
            $file_name =  __APP_PATH_CLASSES__.'/capsuleCrm.class.php';
            break; 
        case "cXmlParser":
            $file_name =  __APP_PATH_CLASSES__.'/parser.class.php';
            break; 
        case "cArray2XML":
            $file_name =  __APP_PATH_CLASSES__.'/Array2Xml.class.php';
            break;  
        case "cCourierServices":
            $file_name =  __APP_PATH_CLASSES__.'/courierServices.class.php';
            break;  
        case "cMetics":
            $file_name =  __APP_PATH_CLASSES__.'/metrics.class.php';
            break; 
        case "cVatApplication":
            $file_name =  __APP_PATH_CLASSES__.'/vatApplication.class.php';
            break;     
        case "cZoozLib":
            $file_name =  __APP_PATH_CLASSES__.'/zoozLib.class.php';
            break; 
        case "cQuote":
            $file_name =  __APP_PATH_CLASSES__.'/quote.class.php';
            break; 
        case "cWebServices":
            $file_name =  __APP_PATH_CLASSES__.'/webServices.class.php';
            break; 
        case "cCourierAPI":
            $file_name =  __APP_PATH_CLASSES__.'/multithread.class.php';
            break;
        case "cPartner":
            $file_name =  __APP_PATH_CLASSES__.'/partner.class.php';
            break; 
        case "cResponseError":
            $file_name =  __APP_PATH_CLASSES__.'/responseErrors.class.php';
            break; 
        case "cApi":
            $file_name =  __APP_PATH_CLASSES__.'/api.class.php';
            break;
        case "cEasyPost":
            $file_name =  __APP_PATH_CLASSES__.'/easyPost.class.php';
            break;
        case "cCourierTracking":
            $file_name =  __APP_PATH_CLASSES__.'/courierTracking.class.php';
            break;
        case "cTracking":
            $file_name =  __APP_PATH_CLASSES__.'/tracking.class.php';
            break;
        case "cMautic":
            $file_name =  __APP_PATH_CLASSES__.'/mautic.class.php';
            break; 
        case "cSendEmail":
            $file_name =  __APP_PATH_CLASSES__.'/sendEmail.class.php';
            break;
        case "cGmail":
            $file_name =  __APP_PATH_CLASSES__.'/gmail.class.php';
            break;
        case "cImap":
            $file_name =  __APP_PATH_CLASSES__.'/imap.class.php';
            break; 
        case "cChat":
            $file_name =  __APP_PATH_CLASSES__.'/chat.class.php';
            break; 
        case "cCRM":
            $file_name =  __APP_PATH_CLASSES__.'/crm.class.php';
            break; 
        case "cPostmen":
            $file_name =  __APP_PATH_CLASSES__.'/postmen.class.php';
            break;
        case "PostmenException":
            $file_name =  __APP_PATH_CLASSES__.'/PostmenException.php';
            break; 
        case "cPendingTray":
            $file_name =  __APP_PATH_CLASSES__.'/pendingTray.class.php';
            break;
        case "cForwarderBooking":
            $file_name =  __APP_PATH_CLASSES__.'/forwarderBooking.class.php';
            break; 
        case "cDeveloper":
            $file_name =  __APP_PATH_CLASSES__.'/developer.class.php';
            break; 
        case "cTransportecaApi":
            $file_name =  __APP_PATH_CLASSES__.'/transportecaApi.class.php';
            break; 
        case "cLabels":
            $file_name =  __APP_PATH_CLASSES__.'/labels.class.php';
            break;  
        case "cLabelPdf":
            $file_name =  __APP_PATH_CLASSES__.'/labelPdf.class.php';
            break; 
        case "cCustomerLogs":
            $file_name =  __APP_PATH_CLASSES__.'/customerLogs.class.php';
            break;  
        case "cPDFDocuments":
            $file_name =  __APP_PATH_CLASSES__.'/pdfDocuments.class.php';
            break;  
        case "cRoutingLabels":
            $file_name =  __APP_PATH_CLASSES__.'/routingLabel.class.php';
            break; 
        case "cCustomerEevent":
            $file_name =  __APP_PATH_CLASSES__.'/customerEvent.class.php';
            break; 
        case "cReferralPricing":
            $file_name =  __APP_PATH_CLASSES__.'/referralPricing.class.php';
            break;
        
    }
   //echo "file name ".$file_name ;
    if(!empty($file_name))
    {
        require_once($file_name);
    }
}
function sanitize_output($buffer)
{
    require_once (__APP_PATH__."/phpwee/phpwee.php"); 
    return  Minify::html($buffer); 
} 
$successful_connection = false; 

function on_session_start($save_path, $session_name)
{
    global $dbh_sess, $successful_connection;  
    $dbh_sess = new mysqli(__DBC_HOST__, __DBC_USER__,__DBC_PASSWD__,__DBC_SCHEMATA__,__DBC_PORT__); 
    if ($dbh_sess)
    {
        $dbh_sess->set_charset("utf8");
        $successful_connection = true; 
        return true; 
    } 
    return false;
}

function on_session_end()
{  
    global $dbh_sess;
    return mysqli_close($dbh_sess);
}
if(isset($_REQUEST['lang']))
{
    $szLanguage = trim($_REQUEST['lang']);
} 

function on_session_read($key)
{
    global $dbh_sess; 

    $filename = __APP_PATH_ROOT__."/logs/session.log";
        
    $query = "
      SELECT
        session_data
      FROM
        ".__DBC_SCHEMATA_SESSIONS__."
      WHERE
        session_id = '".mysql_escape_custom($key)."'
      AND
        session_expiration > NOW()
      LIMIT
         1
    "; 
    if ($result = $dbh_sess->query($query))
    {
        if ($dbh_sess->num_rows)
        {
            $record = $result->fetch_assoc();
            
            $ipaddress = getRealIp_session();
            $strdata=array();
            $strdata[0]=" \n\n Read session Key ".$key." value:  ".$record['session_data']." host ".$_SERVER['HTTP_HOST']." URI: ".$_SERVER['REQUEST_URI']." IP: ".$ipaddress." datetime: ".date('Y-m-d H:i:s')."\n\n";
            //file_log_session(true, $strdata, $filename); 
            return $record['session_data'];
        }
    }
    return '';
}

function getRealIp_session()
{
    $ipaddress = '';
    if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) 
    {
        $ipaddress =  $_SERVER['HTTP_CF_CONNECTING_IP'];
    } 
    else if (isset($_SERVER['HTTP_X_REAL_IP'])) 
    {
        $ipaddress = $_SERVER['HTTP_X_REAL_IP'];
    }
    else if (isset($_SERVER['HTTP_CLIENT_IP']))
    {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
    {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED']))
    {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    }
    else if(isset($_SERVER['REMOTE_ADDR']))
    {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    }        
    else
    {
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress ;
}
function on_session_write($key, $val)
{
    global $dbh_sess;
    createMysqliConnection();  
    
    $query = "
        INSERT INTO
            ".__DBC_SCHEMATA_SESSIONS__."
            (
                session_id,
                session_data,
                session_expiration,
                idUser
            )
        VALUES
            (
                '".mysql_escape_custom($key)."',
                '".mysql_escape_custom($val)."',
                '".date('Y-m-d H:i:s', strtotime(__SESSION_LIFETIME__))."',
                ".(int)$_SESSION['user_id']."
            )
        ON DUPLICATE KEY UPDATE
            session_data = '".mysql_escape_custom($val)."',
            session_expiration = '".date('Y-m-d H:i:s', strtotime(__SESSION_LIFETIME__))."',
            idUser = ".(int)$_SESSION['user_id']."
    ";

    return $dbh_sess->query($query) or die($dbh_sess->error);
}

function on_session_destroy($key)
{
    global $dbh_sess;
 
    $filename = __APP_PATH_ROOT__."/logs/session.log";
					
    $ipaddress = getRealIp_session();
    $strdata=array();
    $strdata[0]=" \n\n destroy session Key ".$key." value:  ".$val." host ".$_SERVER['HTTP_HOST']." URI: ".$_SERVER['REQUEST_URI']." IP: ".$ipaddress." datetime: ".date('Y-m-d H:i:s')."\n\n";
   // file_log_session(true, $strdata, $filename); 
    
    $query = "
        DELETE FROM
            ".__DBC_SCHEMATA_SESSIONS__."
        WHERE
            session_id = '".mysql_escape_custom($key)."'
    ";

	return $dbh_sess->query($query) or die($dbh_sess->error);
}

function on_session_gc($max_lifetime)
{
    global $dbh_sess;
 
    $query = "
        DELETE FROM
            ".__DBC_SCHEMATA_SESSIONS__."
        WHERE
            session_expiration < NOW()
    ";

	return $dbh_sess->query($query) or die($dbh_sess->error);
}

function strip_tags_array($data, $tags = null)
{
    $stripped_data = array();
    foreach ($data as $key => $value)
    {
        if (is_array($value))
        {
            $stripped_data[$key] = strip_tags_array($value, $tags);
        } 
        else 
        {
            while (($tmpval = url_decode_custom($value)) !== false)
            {
                $value = $tmpval;
            }
            while (($tmpval = html_decode_custom($value)) !== false)
            {
                $tmpval2 = $tmpval;
                while (($tmpval3 = url_decode_custom($tmpval2)) !== false)
                {
                    $tmpval2 = $tmpval3;
                }
                $value = $tmpval2;
            }
            $stripped_data[$key] = strip_tags($value, $tags);
        }
    }
    return $stripped_data;
}

function url_decode_custom($value)
{
    $url_decoded = urldecode($value);
    if ($url_decoded != $value)
    {
        return $url_decoded;
    }
    return false;
}

function html_decode_custom($value)
{
    $html_decoded = html_entity_decode($value);
    if ($html_decoded != $value)
    {
        return $html_decoded;
    }
    return false;
}


function global_exception_handler($exception = NULL)
{ 
    if($exception)
    {  
        $file_name = $exception->getFile() ;
        //logError('EXCEPTION',print_r($exception,true),' Exception: ',"class: ".get_class($exception)." ".$file_name,__FUNCTION__,$exception->getLine(),'exception');
        $log_file = __APP_PATH_LOGS__."/general_exception.log"; 
        $handle = fopen($log_file, "a+");
        $szExceptionString = "\n\n ******************************* EXCEPTION ".date("Y-m-d H:i:s")."*****************************\n\n";
        fwrite($handle, $szExceptionString."".print_r($exception,true));
        fclose($handle); 
        ?>
        <script type="text/javascript">
         // window.location.replace("<?php echo __BASE_ERROR_505_PAGE_URL__ ?>");
        </script>
        <?php
        die;
    }	
}

function global_error_handler($errno, $errstr,$error_file,$error_line)
{ 
	/*	
	* we only log 3 types of error
	* $errno=1 means Fatal Error
	* $errno=2 means Warnig Error
	* $errno=4 means Parser Error 
	* 
	*/
//	$log_file = __APP_PATH_LOGS__."/general_error.log"; 
//        $handle = fopen($log_file, "a+");
//        $szExceptionString = "\n\n ******************************* ERROR ".date("Y-m-d H:i:s")."*****************************\n\n";
//        $szExceptionString .= "\n\n Error#: ".$errno."\n Message: ".$errstr." \n File: ".$error_file."\n Line: ".$error_line;
//        fwrite($handle, $szExceptionString);
//        
//        fclose($handle);
        
	if($errno=='1' || $errno=='2' || $errno=='4' || $errno=='8')
	{ 
            /*
            logError('ERROR',$errstr,'General Error: ',$error_file,false,$error_line,'error');
            
            if(cPartner::$bApiValidation)
            { 
                $kError = new CError();
                $kPartner = new cPartner(); 
                $idPartnerApiEventApi = cApi::$idPartnerApiEventApi;
                
                cPartner::$szDeveloperNotes .= ">. Create booking function returning with error message ".PHP_EOL; 
                $errorMessageAry =  array();
                $errorMessageAry['status'] = 'Error';
                $errorMessageAry['token'] = cPartner::$szGlobalToken;    
                $errorAry = array(); 
                $errorAry['code'] = "E1098"; 
                $errorAry['description'] = "Communication failure with Transporteca API";
                $errorMessageAry['errors'][] = $errorAry;
                cResponseError::addMessage($errorMessageAry);
                
                $szResponseSerialized = cResponseError::getSerializedMessages();
                $kPartner->updateApiEventLogResponse($szResponseSerialized,404,$idPartnerApiEventApi);
                $szResponseJson = cResponseError::getMessages();
                return $szResponseJson; 
            } 

            //we are only displaying errors on localhost 
            if(__ENVIRONMENT__ == 'DEV')
            {
                //echo "<b>Error:</b> [$errno] $errstr - $error_file:$error_line";
            }		
            return true;
             * 
             */
	}	
}

function file_log_session($log = false, $strdata, $filename)
{  
    if($log)
    {
        $f = fopen($filename, "a");
        foreach($strdata as $key => $value)
        {
            fwrite($f, $value);
        }
        fclose($f);
    }
}

function logError( $error_field, $message, $error_type, $class_name=false, $function=false, $line=false, $error_severity="critical")
{
	$error_severity = strtolower(trim($error_severity));
	$class_name = strtolower(trim($class_name));
	$function = trim($function);
	$line = (int)$line;
	$message = trim($message);
	$error_field = trim($error_field);
	
	switch ($error_severity)
	{				
            case "critical":
                $log_file = "critical";
                break;
            case "error":
                $log_file = "general_error";
                break;
            case "exception":
                $log_file = "general_exception";
                break;
            case "pricing":
                $log_file = "pricing";
                break;
            case "new_product":
                $log_file = "new_product";
                break;
            default:
                $log_file = "critical";
                break;
	}
	
	$find_ary = array("{TIME}", "{SEVERITY}", "{ERROR_TYPE}", "{FILE}", "{FUNCTION}", "{LINE}", "{ERROR}");
	$replace_ary = array(date(__LOG_DATE_FORMAT__, time()), strtoupper($error_severity), $error_type, $class_name, $function, $line, $message);
	$error_string = str_replace($find_ary, $replace_ary, __LOG_LINE_FORMAT__);

	$log_file = __APP_PATH_LOGS__."/".$error_severity.".log";
	$log_file_class = __APP_PATH_LOG_CLASSES__."/".$class_name."_".$error_severity.".log";

	if ($error_severity == "stdout")
	{
		//echo $error_string;
	}
	else
	{
            $handle = fopen($log_file, "a+");
            fwrite($handle, $error_string);
            fclose($handle);
	}	
	return true;
}


/*
 * use the below to debug the strip tags implementation.
$_GET['test']['yes']['owen'] = array('owen' => 'yes');

echo "<pre>";
print_r($_GET);
$_GET = strip_tags_array($_GET);
print_r($_GET);
echo "\n-----------------------------\n";
print_r($_POST);
$_POST = strip_tags_array($_POST);
print_r($_POST);
echo "\n-----------------------------\n";
print_r($_COOKIE);
$_COOKIE = strip_tags_array($_COOKIE);
print_r($_COOKIE);
echo "\n-----------------------------\n";
print_r($_REQUEST);
$_REQUEST = strip_tags_array($_REQUEST);
print_r($_REQUEST);
die();
*/

if(!empty($_SERVER['PHP_SELF']))
{
    $self_url = $_SERVER['PHP_SELF'] ;
    $self_url_ary = explode("/",$self_url);
} 

$replaceAry = array('http://','https://');
$managementHost = str_replace($replaceAry,"",__BASE_MANAGEMENT_URL__); 
              
if(($self_url_ary[1] != 'evp') && isset($_POST['post_type']) && isset($_POST['panels_data']) && $_POST['post_type']!='page' && $_POST['panels_data']!='')
{  
    if($_SERVER['HTTP_HOST']!=$managementHost)
    {
        $_POST = strip_tags_array($_POST);  
    } 
    
    $_GET = strip_tags_array($_GET);
    $_COOKIE = strip_tags_array($_COOKIE);
    $_REQUEST = strip_tags_array($_REQUEST); 
}

//set error handler
set_error_handler("global_error_handler");

set_exception_handler('global_exception_handler');

// Set the save handlers
session_set_save_handler("on_session_start", "on_session_end", "on_session_read", "on_session_write", "on_session_destroy", "on_session_gc");
session_start();

if ($successful_connection === false)
{
    global $dbh_sess;
    createMysqliConnection(); 
}

function mysql_escape_custom($szFieldValue)
{ 
    $handle = fopen(__APP_PATH_LOGS__."/dbConnection.log", "a+"); 
    try
    {
        global $dbh_sess;
        /*
        if(is_a(cDatabase::$szDbConnection, 'mysqli'))
        { 
            $dbh_sess = cDatabase::$szDbConnection; 
        }
         * 
         */
        if(!is_a($dbh_sess, 'mysqli'))
        { 
            createMysqliConnection();  
        }
        return mysqli_real_escape_string($dbh_sess, $szFieldValue);  
    } 
    catch (Exception $ex) 
    {
        fwrite($handle, "Exception occured: " . print_R($ex,true) . "\r\n");
        fclose($handle);
    } 
}  

function createMysqliConnection()
{
    global $dbh_sess;   
    try
    {
        $dbh_sess = new mysqli(__DBC_HOST__, __DBC_USER__,__DBC_PASSWD__,__DBC_SCHEMATA__,__DBC_PORT__); 
        /* check connection */
        if ($dbh_sess->connect_errno) 
        {
            $handle = fopen(__APP_PATH_LOGS__."/dbConnection.log", "a+");
            fwrite($handle, "Database connection Failed.". $dbh_sess->connect_error . "\r\n");
            fclose($handle); 
            return false;
        }
        if(is_a($dbh_sess, 'mysqli'))
        {  
            $dbh_sess->set_charset("utf8");  
            return $dbh_sess;
        }
        else
        {
            $handle = fopen(__APP_PATH_LOGS__."/dbConnection.log", "a+");
            fwrite($handle, "Could not able to connect with mysqli server " . date("d/m/Y H:i:s") . "\r\n");
            fclose($handle);
            return false;
        } 
    } catch (Exception $ex) {
        echo "Exception ...";
    } 
}

define_page_meta_tags($szLanguage);
function define_page_meta_tags($szLanguage)
{
    $kConfig= new cConfig();
    $languageArr=$kConfig->getLanguageDetails('','','',$szLanguage);
    //print_r($languageArr);
    $languageMetaTags=$kConfig->getLanguageMetaTags($languageArr[0]['id']);
    
    if(empty($languageMetaTags))
    {
        $languageMetaTags=$kConfig->getLanguageMetaTags(__LANGUAGE_ID_ENGLISH__);
    }
    //print_r($languageMetaTags);
    if(!empty($languageMetaTags))
    {
        foreach($languageMetaTags as $languageMetaTagss)
        {
            define("".$languageMetaTagss['szKey']."","".$languageMetaTagss['szValue']."");
        }
    } 
}

function mysql_error_custom()
{
    if(is_a(cDatabase::$szDbConnection, 'mysqli'))
    { 
        $dbh_sess = cDatabase::$szDbConnection; 
        $iMySQLError = $dbh_sess->errno;
        $szMySQLError = $dbh_sess->error;	
        return $szMySQLError;
    }
    else if(is_a($dbh_sess, 'mysqli'))
    {
        $iMySQLError = $dbh_sess->errno;
        $szMySQLError = $dbh_sess->error;	
        return $szMySQLError;
    }
}
?>
