<?php
if( !isset( $_SESSION ) )
{
    session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/pdf_functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
include_classes();

function viewLandingPageContent($t_base,$data,$maxOrder)
{  
	$t_base="management/landingPageData/";
?>
 <table cellpadding="0" cellspacing="0" class="format-4" width="100%">
		<tr>
			<th width="4%" align="center" valign="top" style="text-align:center;">&nbsp;</th>
			<th width="25%" align="left" valign="top"><?=t($t_base.'fields/optimised_for_these_keyword')?></th>
			<th width="10%" align="left" valign="top" ><?=t($t_base.'fields/status')?></th>
			<th width="10%" align="left" valign="top" ><?=t($t_base.'fields/language')?></th>
			<th width="13%" align="center" valign="top" style="text-align:center;"><?=t($t_base.'fields/created')?></th>
			<th width="14%" align="center" valign="top" style="text-align:center;"><?=t($t_base.'fields/b_button_clicked')?></th>
			<th width="15%" align="left" valign="top" >&nbsp;</th>
			<th width="5%" align="left" valign="top" >&nbsp;</th>
		</tr>
 <?php
 	if(!empty($data))
 	{	
 		$count=1;
	 	foreach($data as $terms)
	 	{ 
                    $kConfig =new cConfig();
                    $langArr=$kConfig->getLanguageDetails('',$terms['iLanguage']);
	 ?>
                <tr>
                        <td valign="top" style="text-align:left;"><?=$count;?></td>
                        <td valign="top"><?=$terms['szSeoKeywords'];?> <?php echo $terms['iDefault']?' (<strong>home page</strong>)':''; ?></td>
                        <td valign="top"><?=($terms['iPublished']?t($t_base.'fields/publish'):t($t_base.'fields/published_non'))?></td>
                        <td valign="top"><?php echo $langArr[0]['szName']; ?></td>
                        <td valign="top" style="text-align:center;"><?php echo date('d/m/Y',strtotime($terms['dtCreatedOn']))?></td> 	
                        <td valign="top" style="text-align:center;"><?php echo number_format($terms['iNumButtonClicked'])?></td>
                        <td valign="top">
                                <a href="<?=__MANANAGEMENT_EDIT_LANDING_PAGE_URL__.$terms['id']."/"?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" alt="edit" ></a>
                                <?php if($terms['iDefault']!=1){ ?>
                                        <?php if($terms['iOrder']>1){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/up-icon.png" class="cursor-pointer" alt="up" onclick="orderChangeLandingPage('<?=$terms['id']?>','UP')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
                                        <?php if($maxOrder > $terms['iOrder']){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="cursor-pointer" alt="down" onclick="orderChangeLandingPage('<?=$terms['id']?>','DOWN')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
                                        <img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" class="cursor-pointer" alt="delete"  onclick="deleteLandingPage('<?=$terms['id']?>')">
                                 <?php } ?>
                         </td>
                         <td></td>
                 </tr>
			 
	 <?php $count++; }
	}
	elseif(empty($data))
	{
		echo "<tr align='center'><td colspan='6'><strong>".t($t_base.'fields/no_data_to_preview')."</strong></td></tr>";
	}
?>
</table>	
<div style="float:left;padding-top: 10px;cursor: pointer;"><a href="<?=__MANANAGEMENT_ADD_LANDING_PAGE_URL__?>">Add new Page</a></div>
	
<?php
 }
 
function viewNewLandingPageContent($t_base,$data,$maxOrder)
{  
    $t_base="management/landingPageData/";
?>
 <table cellpadding="0" cellspacing="0" class="format-4" width="100%">
    <tr>
            <th width="4%" align="center" valign="top" style="text-align:center;">&nbsp;</th>
            <th width="25%" align="left" valign="top"><?=t($t_base.'fields/optimised_for_these_keyword')?></th>
            <th width="10%" align="left" valign="top" ><?=t($t_base.'fields/status')?></th>
            <th width="10%" align="left" valign="top" ><?=t($t_base.'fields/language')?></th>
            <th width="13%" align="center" valign="top" style="text-align:center;"><?=t($t_base.'fields/created')?></th>
            <th width="14%" align="center" valign="top" style="text-align:center;"><?=t($t_base.'fields/b_button_clicked')?></th>
            <th width="15%" align="left" valign="top" >&nbsp;</th>
            <th width="5%" align="left" valign="top" >&nbsp;</th>
    </tr>
 <?php
 	if(!empty($data))
 	{	
 		$count=1;
	 	foreach($data as $terms)
	 	{ 
                    $szLanguageName = getLanguageName($terms['iLanguage']); 
	 ?>
	 	<tr>
		 	<td valign="top" style="text-align:left;"><?=$count;?></td>
                        <td valign="top"><?=$terms['szSeoKeywords'];?> <?php if($terms['iHoldingPage']=='1'){ echo " (<strong>holding page</strong>)"; } else if($terms['iDefault']=='1'){ echo " (<strong>home page</strong>)"; } else if($terms['iDefault']=='2'){ echo " (<strong>courier label download</strong>)";}  ?></td>
		 	<td valign="top"><?=($terms['iPublished']?t($t_base.'fields/publish'):t($t_base.'fields/published_non'))?></td>
		 	<td valign="top"><?php echo ucfirst($szLanguageName); ?></td>
		 	<td valign="top" style="text-align:center;"><?php echo date('d/m/Y',strtotime($terms['dtCreatedOn']))?></td> 	
		 	<td valign="top" style="text-align:center;"><?php echo $terms['iNumButtonClicked']; ?></td>
		 	<td valign="top">
		 		<a href="<?=__MANANAGEMENT_EDIT_NEW_LANDING_PAGE_URL__.$terms['id']."/"?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" alt="edit" ></a>
			 	<?php if($terms['iDefault']!=1){ ?>
				 	<?php if($terms['iOrder']>1){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/up-icon.png" class="cursor-pointer" alt="up" onclick="orderChangeLandingPage('<?=$terms['id']?>','UP','NEW_LANDING_PAGE')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
				 	<?php if($maxOrder > $terms['iOrder']){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="cursor-pointer" alt="down" onclick="orderChangeLandingPage('<?=$terms['id']?>','DOWN','NEW_LANDING_PAGE')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
				 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" class="cursor-pointer" alt="delete"  onclick="deleteLandingPage('<?=$terms['id']?>','NEW_LANDING_PAGE')">
				 <?php } ?>
			 </td>
			 <td></td>
		 </tr>
			 
	 <?php $count++; }
	}
	elseif(empty($data))
	{
		echo "<tr align='center'><td colspan='6'><strong>".t($t_base.'fields/no_data_to_preview')."</strong></td></tr>";
	}
?>
</table>	
<div style="float:left;padding-top: 10px;cursor: pointer;"><a href="<?=__MANANAGEMENT_ADD_NEW_LANDING_PAGE_URL__?>">Add new Page</a></div>
	
<?php
 }
 
 function viewExplainData($content,$idExplain)
	{ 		
		$t_base="management/textEditor/";
	 ?>
	 <table cellpadding="0" cellspacing="0" class="format-7" width="100%">
            <tr>
                <th width="4%" align="center" valign="top"></th>
                <th width="38%" align="left" valign="top"><?=t($t_base.'fields/current_section')?></th>
                <th width="38%" align="left" valign="top"><?=t($t_base.'fields/draft_section')?></th>
                <th width="15%" align="left" valign="top" >&nbsp;</th>
                <th width="5%" align="left" valign="top" >&nbsp;</th>
            </tr>
	 <?php
	 //print_r($data);
	 	if(!empty($content))
	 	{	$kExplain = new cExplain();
	 		$maxorder = $kExplain->getMaxOrderExplain($idExplain);
	 		$count=1;
		 	foreach($content as $explainDetails)
		 	{ 
		 ?>
			 	<tr>
				 	<td valign="top"><?=$count;?></td>
				 	<td valign="top"><?=$explainDetails['szPageName'];?></td>
				 	<td valign="top"><?=$explainDetails['szDraftPageName']?></td>
				 	<td valign="top">
					 	 <?php if($explainDetails['szDraftPageName']!=''){?>
					 			<img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" id="add" onclick='editTextEditor("<?=$idExplain?>","<?=$explainDetails['id']?>")' alt="edit">	
					 	<?php if($explainDetails['iOrderByDraft']>1){ ?>
					 			<img src="<?=__BASE_STORE_IMAGE_URL__?>/up-icon.png" class="cursor-pointer" onclick="orderChangeExplain('<?=$explainDetails['id']?>',1,'<?=$idExplain?>')" alt="up">
					 	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
					 	<?php if($maxorder!=$explainDetails['iOrderByDraft']){?>
					 			<img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="cursor-pointer" onclick="orderChangeExplain('<?=$explainDetails['id']?>',2,'<?=$idExplain?>')" alt="down">
					 	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
						 		<img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" id="deleteTextEditor" class="cursor-pointer" onclick="deleteTextData('DELETE_EXPLAIN_CONTENT','<?=$explainDetails['id']?>')" alt="delete">
					 </td> 	
					<?php } ?>
					<td valign="top"><?=($explainDetails['iUpdated']?'<span class="info-icon">&nbsp;</span>':'')?></td>
				 </tr>
				 
		 <?php $count++; }
		}
		elseif(empty($data))
		{
			echo "<tr align='center'><td colspan='5'><strong>NO DATA TO PREVIEW</strong></td></tr>";
		}
	?>
	</table>	
	<?php
	 }
	 
	function mainExplainContentDetails($textEditor,$idExplain)
	{	 
		$t_base = 'management/explaindata/';
		$kExplain = new cExplain();
		
		$level2Arr=$kExplain->selectPageDetails($idExplain);
		$iExplainPageType=$level2Arr[0]['iExplainPageType'];
		$level2Url=$level2Arr[0]['szLink'];
		if($iExplainPageType==1)
		{
			if($_SESSION['session_admin_language']==2)
			{
				$subUrl=__HOW_IT_WORKS_URL_DA__."/".$level2Url."/...)";
				//$szPreviewUrl = "howitworks-preview";
			}
			else
			{
				$subUrl=__HOW_IT_WORKS_URL__."/".$level2Url."/...)";
			}
		}
		else if($iExplainPageType==2)
		{
			if($_SESSION['session_admin_language']==2)
			{
				$subUrl=__COMPANY_URL_DA__."/".$level2Url."/...)";
				//$szPreviewUrl = "howitworks-preview";
			}
			else
			{
				$subUrl=__COMPANY_URL__."/".$level2Url."/...)";
				//$szPreviewUrl = "company-preview";
			}
		}
		else if($iExplainPageType==3)
		{
			if($_SESSION['session_admin_language']==2)
			{
				$subUrl=__TRANSTPORTPEDIA_URL_DA__."/".$level2Url."/...)";
				//$szPreviewUrl = "howitworks-preview";
			}
			else
			{
				$subUrl=__TRANSTPORTPEDIA_URL__."/".$level2Url."/...)";
				//$szPreviewUrl = "transportpedia-preview";
			}
		} 
		
		$szPreviewUrl = __BASE_URL__."/individualPagePreview/";
	?>
		<!--  <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.form.js"></script>-->
		
		<form method="post" id="textEditorSubmit" onkeypress="saveDetailsCheck(event);">
		<!-- 
			<label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/meta_title');?></span> 
				<span class="field-container" style="float:right;"><input type="text" style="width:225px;padding: 3px;" id="szMetaTitle" name="textEditArr[szMetaTitle]" value="<?=$textEditor['szMetaTitle'];?>"></span>
			</label>	       
			<label class="profile-fields">
				<span class="field-name" style="width:65%;"><?=t($t_base.'fields/url_link_for_user');?><?php echo $subUrl;?></span> 
				<span class="field-container" style="float:right;"><input type="text" style="width:225px;padding: 3px;" id="szUrl" name="textEditArr[szUrl]" value="<?=$textEditor['szUrl'];?>" ></span>
			</label>
			 <label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/pageheading_new');?></span> 
				<span class="field-container" style="float:right;"><input type="text" style="width:225px;padding: 3px;" id="szPageName" name="textEditArr[szPageName]" value="<?=$textEditor['szDraftPageName'];?>"></span>
			</label>
			<label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/meta_keyword');?>:</span> 
				<span class="field-container" style="float:right;">
					<textarea name="textEditArr[szMetaKeyword]" style="width:225px;padding: 3px;" cols="25" rows="3"><?=$textEditor['szDraftMetaKeyWord'];?></textarea>
				</span>
			</label>
			<label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/meta_description');?>: </span> 
				<span class="field-container" style="float:right;">
					<textarea rows="3" cols="25" style="width:225px;padding: 3px;" onkeyup="checkDescriptionLength(this.id);" name="textEditArr[szMetaDescription]" id="szMetaDescription"><?=$textEditor['szDraftMetaDescription'];?></textarea>
				</span>
			</label>
			 -->
			  <label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/link_title');?></span> 
				<span class="field-container" style="float:right;"><input type="text" style="width:225px;padding: 3px;" id="szPageName" name="textEditArr[szPageName]" value="<?=$textEditor['szDraftPageName'];?>"></span>
			</label>     
			<label class="profile-fields">
				<span class="field-name" style="width:65%;"><?=t($t_base.'fields/anchor_tag');?></span> 
				<span class="field-container" style="float:right;"><input type="text" style="width:225px;padding: 3px;" id="szUrl" name="textEditArr[szUrl]" value="<?=$textEditor['szUrl'];?>" ></span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/background_color');?>: </span> 
				<span class="field-container" style="float:right;"><input type="text" id="szBackgroundColor" onblur="change_editor_background(this.value);" style="width:225px;padding: 3px;" name="textEditArr[szBackgroundColor]" value="<?=$textEditor['szBackgroundColor'];?>"></span>
			</label>
			<!--  <label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/links_color');?>: </span> 
				<span class="field-container" style="float:right;"><input type="text" id="szLinkColor" onblur="change_link_color(this.value);" style="width:225px;padding: 3px;" name="textEditArr[szLinkColor]" value="<?=$textEditor['szLinkColor'];?>"></span>
			</label>-->  
			<!-- <label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/picture_title');?>: </span> 
				<span class="field-container" style="float:right;"><input type="text" id="szPictureTitle" name="textEditArr[szPictureTitle]" value="<?=$textEditor['szDraftPictureTitle'];?>" style="padding: 3px;"></span>
			</label>
			<label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/picture');?>: </span> 
				<span class="field-container" style="float:right;"><input type="file" name="file_upload" id="file_upload" <?php if($data['szDraftPicture']!='') {?> style="width: 99%;display:none;" <?php }else{?> style="width: 99%;" <?php }?> />
				<div id='preview'>
					 <?php if($textEditor['szDraftPicture']!='' && $textEditor['iFileUpload']!=1){ ?>
						  <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/image.php?img=<?php echo $textEditor['szDraftPicture']?>&w=100&h=75&temp=explainLevel3" alt="<?php echo $textEditor['szDraftPicture']?>" >
						  <a href="javascript:void(0);" onclick="delete_explain_level3_image('<?php echo $textEditor['szDraftPicture']?>','main','<?php echo $textEditor["id"];?>');" title="Delete Image"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/delete-icon.png"></a>
						  <?php }else if($data['szDraftPicture']!='' && $textEditor['iFileUpload']==1){?>
						  <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/image.php?img=<?php echo $textEditor['szDraftPicture']?>&w=100&h=75&temp=explainLevel3" alt="<?php echo $textEditor['szDraftPicture']?>" >
						  <a href="javascript:void(0);" onclick="delete_explain_level3_image('<?php echo $textEditor['szDraftPicture']?>','temp');" title="Delete Image"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/delete-icon.png"></a>
					  <?php }?>
					</div></span>
			</label> -->
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/heading');?>: </span> 
                            <span class="field-container" style="width:70%"><input type="text" id="heading" style="padding: 3px;" name="textEditArr[heading]" value="<?=$textEditor['szDraftHeading'];?>"></span>
			</label>
			<label class="profile-fields">
                            <span class="field-name" style="width:15%"><?=t($t_base.'fields/content');?>: </span> 
                            <span class="field-container" style="width:85%"><textarea name="content" class="myTextEditor" id="content" style="<?php echo $bgColor;?>height:60%;padding: 3px;"><?=$textEditor['szDraftDescription']?></textarea> </span>
			</label> 
	        <!-- <input type="hidden" id="checkMode" name="mode" value="SAVE_FORWARDER_FAQ"> -->
	        <input type="hidden" id="id" value="<?=($textEditor['id'])?$textEditor['id']:0;?>">
	        <input type="hidden" id="flag" value="<?=$idExplain?>">
	        <input type="hidden" id="szUploadFileName" name="textEditArr[szUploadFileName]" value="<?php echo $textEditor['szDraftPicture']?>"/>
			<input type="hidden" id="iFileUpload" name="textEditArr[iFileUpload]" value="<?php echo $textEditArr['iFileUpload']?>"/>
			
	        <br/><br/><br/>
	        <label class="profile-fields" style="float:right;">
	        	<a id="save" class="button1" style="opacity:" onclick="preview_data_level3('<?php echo $szPreviewUrl; ?>');"><span><?=t($t_base.'fields/preview');?></span></a>
		        <a id="save" class="button1" style="opacity:" onclick="saveExplainDetails();"><span><?=t($t_base.'fields/save');?></span></a>
				<a class="button1" onclick="cancel_text_edit(0)"><span><?=t($t_base.'fields/cancel');?></span></a>
			</label>
		</form> 
		<script type="text/javascript">
			$(document).ready(function()
		    {   		
		    	
		    		//$('#file_upload').live('change', function()			{ 
					//$("#preview").html('');
						//    $("#preview").html('<img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/loader_image.gif" alt="Uploading...."/>');
						//$("#textEditorSubmit").ajaxForm({
							//		target: '#preview',url: '<?php echo __BASE_MANAGEMENT_URL__?>/imageUpload.php?flag=explainLevel3'
					//}).submit();
					
						//});	 
						<?php	
		   			if($textEditor['szBackgroundColor']!='')
					{
						$bgColor=$textEditor['szBackgroundColor'];
						 $bgColor = substr($bgColor, 1);
						?>
						setTimeout(function(){change_editor_background('<?php echo $textEditor['szBackgroundColor'];?>')},1200);						
						<?php
					}
					?>
		    });  
		    
		  
		</script>
	<?php
	}
	function createNewExplanationPage($t_base,$data = array())
	{
		if(isset($data['iExplainPageType']))
		{
			$iExplainPageType = (int)$data['iExplainPageType'] ;
		}
		else		
		{
			$iExplainPageType = (int)$_REQUEST['type'] ;
		} 
		if($iExplainPageType==1)
		{
                    if($_SESSION['session_admin_language']==2)
                    {
                        $subUrl=__HOW_IT_WORKS_URL_DA__."/...)";
                        $subUrl2=__HOW_IT_WORKS_URL__."/...)";
                    }
                    else
                    {
                        $subUrl=__HOW_IT_WORKS_URL__."/...)";
                        $subUrl2=__HOW_IT_WORKS_URL_DA__."/...)";
                    }
                    $szPreviewUrl = "howitworks-preview";
		}
		else if($iExplainPageType==2)
		{
                    if($_SESSION['session_admin_language']==2)
                    {
                        $subUrl=__COMPANY_URL_DA__."/...)";
                        $subUrl2=__COMPANY_URL__."/...)";
                    }
                    else
                    {
                        $subUrl=__COMPANY_URL__."/...)";
                        $subUrl2=__COMPANY_URL_DA__."/...)";
                    }
                    $szPreviewUrl = "company-preview";
		}
		else if($iExplainPageType==3)
		{
                    if($_SESSION['session_admin_language']==2)
                    {
                        $subUrl=__TRANSTPORTPEDIA_URL_DA__."/...)";
                        $subUrl2 = __TRANSTPORTPEDIA_URL__."/...)";
                    }
                    else
                    {
                        $subUrl=__TRANSTPORTPEDIA_URL__."/...)";
                        $subUrl2 =__TRANSTPORTPEDIA_URL_DA__."/...)";
                    }
                    $szPreviewUrl = "transportpedia-preview";
		} 
                if($_SESSION['session_admin_language']==2)
                {
                    $szLanguageText = "English";
                    $subUrl2 = "(". __MAIN_SITE_HOME_PAGE_URL__."/...)" ;
                }
                else
                {
                    $szLanguageText = "Danish";
                    $subUrl2 = "(". __MAIN_SITE_HOME_PAGE_URL__."/da/...)" ;
                }
		$szPreviewUrl = __BASE_URL__."/contentPagePreview/";
                
                $kConfig =new cConfig();
                $langArr=$kConfig->getLanguageDetails();
	?>
		<?php if($data== array()){?>
			<h4><b><?=t($t_base.'title/add_explain_pages');?></b></h4>
		<?}else if($data!= array()){ ?>
			<h4><b><?=t($t_base.'title/manage_explain_pages');?></b></h4>
		<?php } ?>
		
		<div id ="Error-log"></div>
		<script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__?>/jquery.form.js"></script>
		<script type="text/javascript">
			$(document).ready(function()
		    {   
	    		$('#file_upload').live('change', function(){ 
					$("#preview").html('');
						    $("#preview").html('<img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/loader_image.gif" alt="Uploading...."/>');
						$("#addExplainPage").ajaxForm({
									target: '#preview',url: '<?php echo __BASE_MANAGEMENT_URL__?>/imageUpload.php?flag=explainLevel2'
					}).submit(); 
				});  
				
				$('#og_file_upload').live('change', function(){ 
					$("#og_preview").html('');
					$("#og_preview").html('<img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/loader_image.gif" alt="Uploading...."/>');
						$("#addExplainPage").ajaxForm({
							target: '#og_preview',url: '<?php echo __BASE_MANAGEMENT_URL__?>/imageUpload.php?flag=explainLevel2_og'
					}).submit(); 
				}); 
				checkDescriptionLength('szMetaDescription');
		    });  
		</script>
		<form name="addExplainPage" id="addExplainPage" method="post" action="javascription:void();"  target="_blank">
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_title');?></span>
				<span class="field-container"><input type="text" name="explainArr[szMetaTitle]" value="<?php if(isset($data['szMetaTitle'])){echo $data['szMetaTitle']; }?>"/></span>
			</label>
			
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/url_link_for_user')."".$subUrl;?></span>
                            <span class="field-container"><input type="text" name="explainArr[szLink]" value="<?php if(isset($data['szLink'])){echo $data['szLink']; }?>"/></span>
			</label>
                        <label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/url')." ".$szLanguageText." ".$subUrl2;?></span>
                            <span class="field-container"><input type="text" name="explainArr[szLinkedPageUrl]" value="<?php if(isset($data['szLinkedPageUrl'])){echo $data['szLinkedPageUrl']; }?>"/></span>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/pageheading');?></span>
                            <span class="field-container"><input type="text" name="explainArr[szPageHeading]" value="<?php if(isset($data['szPageHeading'])){echo $data['szPageHeading']; }?>"/></span>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/link_title');?></span>
                            <span class="field-container"><input type="text" name="explainArr[szLinkTitle]" value="<?php if(isset($data['szLinkTitle'])){ echo $data['szLinkTitle']; }?>"/></span>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/sub_title');?></span>
                            <span class="field-container"><input type="text" name="explainArr[szSubTitle]" value="<?php if(isset($data['szSubTitle'])){echo $data['szSubTitle']; }?>"/></span>
			</label>
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/meta_description');?></span>
                            <span class="field-container">
                                    <textarea onkeyup="checkDescriptionLength(this.id);" name="explainArr[szMetaDescription]" id="szMetaDescription" ><?php if(isset($data['szMetaDescription'])){ echo $data['szMetaDescription']; }?></textarea>
                            </span>
                            <span style="color: #7F7F7F;float: left;font-size: 12px;font-style: italic;text-align: right;width: 30px;">(<span id="char_left">160</span>)</span>
			</label> 
			<label class="profile-fields">
                            <span class="field-name"><?=t($t_base.'fields/open_graph_image');?></span>
                            <span class="field-container">
                                <input type="file" name="og_file_upload" id="og_file_upload" <?php if($data['szOpenGraphImage']!='') {?> style="width: 99%;display:none;" <?php }else{?> style="width: 99%;" <?php }?> />
                                <div id='og_preview'>
                                <?php if($data['szOpenGraphImage']!='' && $data['iOGFileUpload']!=1){ ?>
                              <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/image.php?img=<?php echo $data['szOpenGraphImage']?>&w=100&h=75&temp=explainLevel2" alt="<?php echo $data['szOpenGraphImage']?>" >
                              <a href="javascript:void(0);" onclick="delete_explain_level2_image('<?php echo $data['szOpenGraphImage']?>','main','<?php echo $data["id"];?>','OPEN_GRAPH');" title="Delete Image"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/delete-icon.png"></a>
                              <?php }else if($data['szOpenGraphImage']!='' && $data['iOGFileUpload']==1){?>
                              <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/image.php?img=<?php echo $data['szOpenGraphImage']?>&w=100&h=75&temp=explainLevel2" alt="<?php echo $data['szOpenGraphImage']?>" >
                              <a href="javascript:void(0);" onclick="delete_explain_level2_image('<?php echo $data['szOpenGraphImage']?>','temp','OPEN_GRAPH');" title="Delete Image"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/delete-icon.png"></a>
                          <?php }?>
                                </div>
                            </span>
			</label>
			<label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/open_graph_author');?>: </span> 
				<span class="field-container" style="float:right;"><input type="text" id="szOpenGraphAuthor" name="explainArr[szOpenGraphAuthor]" value="<?=$data['szOpenGraphAuthor'];?>" style="padding: 3px;"></span>
			</label>
			<label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/picture_title');?>: </span> 
				<span class="field-container" style="float:right;"><input type="text" id="szPictureTitle" name="explainArr[szPictureTitle]" value="<?=$data['szPictureTitle'];?>" style="padding: 3px;"></span>
			</label>
			<label class="profile-fields">
				<span class="field-name" style="width:50%;"><?=t($t_base.'fields/picture_description');?>: </span> 
				<span class="field-container" style="float:right;"><input type="text" id="szPictureDescription" name="explainArr[szPictureDescription]" value="<?=$data['szPictureDescription'];?>" style="padding: 3px;"></span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/image_url');?></span>
				<span class="field-container">
					<input type="file" name="file_upload" id="file_upload" <?php if($data['szHeaderImage']!='') {?> style="width: 99%;display:none;" <?php }else{?> style="width: 99%;" <?php }?> />
					<div id='preview'>
						 <?php if($data['szHeaderImage']!='' && $data['iFileUpload']!=1){ ?>
                              <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/image.php?img=<?php echo $data['szHeaderImage']?>&w=100&h=75&temp=explainLevel2" alt="<?php echo $data['szHeaderImage']?>" >
                              <a href="javascript:void(0);" onclick="delete_explain_level2_image('<?php echo $data['szHeaderImage']?>','main','<?php echo $data["id"];?>');" title="Delete Image"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/delete-icon.png"></a>
                              <?php }else if($data['szHeaderImage']!='' && $data['iFileUpload']==1){?>
                              <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__?>/image.php?img=<?php echo $data['szHeaderImage']?>&w=100&h=75&temp=explainLevel2" alt="<?php echo $data['szHeaderImage']?>" >
                              <a href="javascript:void(0);" onclick="delete_explain_level2_image('<?php echo $data['szHeaderImage']?>','temp');" title="Delete Image"><img src="<?php echo __BASE_STORE_IMAGE_URL__;?>/delete-icon.png"></a>
                          <?php }?>
					</div>
				</span>
			</label>
			<!-- <label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_keyword');?></span>
				<span class="field-container"><textarea rows="3" cols="25" name="explainArr[szMetaKeyword]"><?php if(isset($data['szMetaKeyWord'])){echo $data['szMetaKeyWord']; }?></textarea></span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_description');?></span>
				
				<span class="field-container"><textarea rows="3" cols="25" onkeyup="checkDescriptionLength(this.id);" name="explainArr[szMetaDescription]" id="szMetaDescription"><?php if(isset($data['szMetaDescription'])){echo $data['szMetaDescription']; }?></textarea></span>
				<p style="color: #7F7F7F;float: left;font-size: 12px;font-style: italic;margin: 63px 0 0 17px;text-align: right;width: 30px;">(<span id="char_left">160</span>)</p>
			</label> -->
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/active_status');?></span>
				<span class="field-container">
					<select name="explainArr[iActive]">
						<option value = '1' <?php if(isset($data['iActive']) && $data['iActive'] ==1){echo "selected='selected'"; } ?>><?=t($t_base.'fields/yes');?></option>
						<option value = '0' <?php if(isset($data['iActive']) && $data['iActive'] ==0){echo "selected='selected'"; } ?>><?=t($t_base.'fields/no');?></option>	
					</select>
				</span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/language');?></span>
				<span class="field-container">
                                    <select name="explainArr[iLanguage]">
                                        <?php 
                                        if(!empty($langArr))
                                        {
                                            foreach($langArr as $langArrs)
                                            {?>
                                                <option value = '<?php echo $langArrs['id']?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==$langArrs['id']){echo "selected='selected'"; } ?>><?=ucwords($langArrs['szName']);?></option>
                                            <?php
                                            }
                                        }?>
<!--                                            <option value = '<?php echo __LANGUAGE_ID_ENGLISH__?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_ENGLISH__){echo "selected='selected'"; } ?>><?=t($t_base.'fields/english');?></option>
                                            <option value = '<?php echo __LANGUAGE_ID_DANISH__?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_DANISH__){echo "selected='selected'"; } ?>><?=t($t_base.'fields/danish');?></option>	-->
                                    </select>
				</span>
			</label>
			<input type="hidden" id="szUploadFileName" name="explainArr[szUploadFileName]" value="<?php echo $data['szUploadFileName']?>"/>
			<input type="hidden" id="iFileUpload" name="explainArr[iFileUpload]" value="<?php echo $data['iFileUpload']?>"/>
			
			<input type="hidden" id="szUploadOGFileName" name="explainArr[szUploadOGFileName]" value="<?php echo $data['szUploadOGFileName']?>"/>
			<input type="hidden" id="iOGFileUpload" name="explainArr[iOGFileUpload]" value="<?php echo $data['iOGFileUpload']?>"/>
			
			<input type="hidden" name="explainArr[iExplainPageType]" value="<?php echo $iExplainPageType; ?>" id="iExplainPageType">
			<?php if(isset($data['id'])){ echo '<input type="hidden" name="explainArr[id]" value="'.$data["id"].'">'; } ?>
		</form>
		<br/>
			<div style="text-align: center;">
				<a class="button1" id="addNewPage" onclick="addNewExpanationPage('PREVIEW_EXPLAIN_PAGE','<?php echo $szPreviewUrl; ?>');"><span><?php echo t($t_base.'fields/preview'); ?></span></a>
				<a class="button1" id="addNewPage" onclick="addNewExpanationPage();"><span><?php if(isset($data['id'])){echo t($t_base.'fields/save');}else{echo t($t_base.'fields/add_1');}?></span></a>
				<a class="button1" id="cancelAddingPage" onclick="cancelAddingPage()"><span><?=t($t_base.'fields/cancel');?></span></a>
			</div>
	<?php
	}
	function viewExplainPage($t_base,$data,$maxOrder,$iType)
	{  ?>
	 <table cellpadding="0" cellspacing="0" class="format-7" width="100%">
			<tr>
				<th width="4%" align="center" valign="top">&nbsp;</th>
				<th width="38%" align="left" valign="top"><?=t($t_base.'fields/PageHeadings')?></th>
				<th width="20%" align="left" valign="top" ><?=t($t_base.'fields/status')?></th>
				<th width="10%" align="left" valign="top" ><?=t($t_base.'fields/language')?></th>
				<th width="20%" align="left" valign="top" >&nbsp;</th>
			</tr>
	 <?php
	 	if(!empty($data))
	 	{	
	 		$count=1;
		 	foreach($data as $terms)
		 	{ 
                            $kConfig =new cConfig();
                            $langArr=$kConfig->getLanguageDetails('',$terms['iLanguage']);
		 ?>
			 	<tr>
				 	<td valign="top"><?=$count;?></td>
				 	<td valign="top"><?=$terms['szPageHeading'];?></td>
				 	<td valign="top"><?=($terms['iActive']?t($t_base.'fields/publish'):t($t_base.'fields/published_non'))?></td>
				 	<td valign="top"><?php echo $langArr[0]['szName']; ?>
				 	<td valign="top">
					 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" alt="edit" onclick="editExplainPage('<?=$terms['id']?>')">
					 	<?php if($terms['iOrderBy']>1){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/up-icon.png" class="cursor-pointer" alt="up" onclick="orderChangeExplainPage('<?=$terms['id']?>','<?php echo $iType; ?>','UP')">	<?php }else{ ?><span style=" display: inline-block;  width: 16px;"></span><?php } ?>
					 	<?php if($maxOrder > $terms['iOrderBy']){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="cursor-pointer" alt="down" onclick="orderChangeExplainPage('<?=$terms['id']?>','<?php echo $iType; ?>','DOWN')">	<?php }else{ ?><span style=" display: inline-block;  width: 16px;"></span><?php } ?>
					 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" class="cursor-pointer" alt="delete"  onclick="deleteExpalinPage('<?=$terms['id']?>')">
				 	</td> 
				 </tr>
				 
		 <?php $count++; }
		}
		elseif(empty($data))
		{
			echo "<tr align='center'><td colspan='5'><strong>".t($t_base.'title/no_data_to_preview')."</strong></td></tr>";
		}
	?>
	</table>	
	<div style="float:left;padding-top: 10px;cursor: pointer;">
	<?php if($iType==1){ ?>
		<a href="<?=__MANANAGEMENT_ADD_EXPLAIN_PAGE_URL__; ?>">Add new Page</a>
	<?php }else if($iType==2){?>
		<a href="<?=__MANAGEMENT_CREATE_COMPANY_EXPLIAN_URL__ ; ?>">Add new Page</a>
	<?php } else {?>
		<a href="<?=__MANAGEMENT_CREATE_TRANSPORTPEDIA_EXPLIAN_URL__ ; ?>">Add new Page</a>
	<?php } ?>
	</div>
		
	<?php
	 }
         function viewBlogPage($t_base,$data,$maxOrder)
	{ ?>
	 <table cellpadding="0" cellspacing="0" class="format-4" width="100%">
			<tr>
				<th width="4%" align="center" valign="top"></th>
				<th width="38%" align="left" valign="top"><?=t($t_base.'fields/PageHeadings')?></th>
				<th width="30%" align="left" valign="top" ><?=t($t_base.'fields/status')?></th>
				<th width="10%" align="left" valign="top" ><?=t($t_base.'fields/language')?></th>
				<th width="15%" align="left" valign="top" >&nbsp;</th>
			</tr>
	 <?php
	 	if(!empty($data))
	 	{	
	 		$count=1;
		 	foreach($data as $terms)
		 	{ 
                            
                             $kConfig =new cConfig();
                            $langArr=$kConfig->getLanguageDetails('',$terms['iLanguage']);
		 ?>
			 	<tr>
				 	<td valign="top"><?=$count;?></td>
				 	<td valign="top"><?=$terms['szHeading'];?></td>
				 	<td valign="top"><?=($terms['iActive']?t($t_base.'fields/published'):t($t_base.'fields/published_non'))?></td>
				 	<td valign="top"><?php echo $langArr[0]['szName'];?></td>
				 	<td valign="top">
					 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" alt="edit" onclick="editBlogPage('<?=$terms['id']?>')">
					 	<?php if($terms['iOrderBy']>1){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/up-icon.png" class="cursor-pointer" alt="up" onclick="orderChangeBlogPage('<?=$terms['id']?>','UP')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
					 	<?php if($maxOrder > $terms['iOrderBy']){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="cursor-pointer" alt="down" onclick="orderChangeBlogPage('<?=$terms['id']?>','DOWN')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
					 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" class="cursor-pointer" alt="delete"  onclick="deleteBlogPage('<?=$terms['id']?>')">
				 	</td>
				 </tr>
				 
		 <?php $count++; }
		}
		elseif(empty($data))
		{
			echo "<tr align='center'><td colspan='5'><strong>".t($t_base.'fields/no_data_to_preview')."</strong></td></tr>";
		}
	?>
	</table>	
	<div style="float:left;padding-top: 10px;cursor: pointer;"><a href="<?=__MANANAGEMENT_ADD_BLOG_PAGE_URL__?>"><?=t($t_base.'fields/add_new_blog_page')?></a></div>
		
	<?php
} 
function createNewBlogPage($t_base,$data = array())
{
    $kConfig =new cConfig();
    $langArr=$kConfig->getLanguageDetails();
	if($data== array()){?>
			<h4><b><?=t($t_base.'title/knowledge_centre_page_setup');?></b></h4>
		<?}else if($data!= array()){ ?>
			<h4><b><?=t($t_base.'title/knowledge_centre_page_setup');?></b></h4>
		<?php } ?>
		
		<div id ="Error-log"></div>
		<script type="text/javascript">
			$(function(){
			checkDescriptionLength('szMetaDescription');
			});
		</script>
		<form name="addBlogPage" id="addBlogPage" method="post" >
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/pageheading');?></span>
				<span class="field-container"><input type="text" id="szHeading" name="blogArr[szHeading]" value="<?php if(isset($data['szHeading'])){echo $data['szHeading']; }?>"/></span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/url_link_for_user');?></span>
				<span class="field-container"><input type="text" id="szLink" name="blogArr[szLink]" value="<?php if(isset($data['szLink'])){echo $data['szLink']; }?>"/></span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_title');?></span>
				<span class="field-container"><input type="text" id="szMetaTitle" name="blogArr[szMetaTitle]" value="<?php if(isset($data['szMetaTitle'])){echo $data['szMetaTitle']; }?>"/></span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_keyword');?></span>
				<span class="field-container"><textarea rows="3" cols="25" id="szMetaKeyword" name="blogArr[szMetaKeyword]"><?php if(isset($data['szMetaKeyWord'])){echo $data['szMetaKeyWord']; }?></textarea></span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/meta_description');?></span>
				
				<span class="field-container"><textarea rows="3" cols="25" onkeyup="checkDescriptionLength(this.id);" name="blogArr[szMetaDescription]" id="szMetaDescription"><?php if(isset($data['szMetaDescription'])){echo $data['szMetaDescription']; }?></textarea></span>
				<p style="color: #7F7F7F;float: left;font-size: 12px;font-style: italic;margin: 63px 0 0 10px;">(<span id="char_left">160</span>)</p>
			</label>			
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/active_status');?></span>
				<span class="field-container">
					<select name="blogArr[iActive]" id="iActive">
						<option value = '1' <?php if(isset($data['iActive']) && $data['iActive'] ==1){echo "selected='selected'"; } ?>><?=t($t_base.'fields/yes');?></option>
						<option value = '0' <?php if(isset($data['iActive']) && $data['iActive'] ==0){echo "selected='selected'"; } ?>><?=t($t_base.'fields/no');?></option>	
					</select>
				</span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/language');?></span>
				<span class="field-container">
					<select name="blogArr[iLanguage]" id="iLanguage">
                                            <?php
                                                if(!empty($langArr))
                                                {
                                                    foreach($langArr as $langArrs)
                                                    {?>
                                                        <option value = '<?php echo $langArrs['id']?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==$langArrs['id']){echo "selected='selected'"; } ?>><?php echo ucfirst($langArrs['szName']);?></option>
                                                     <?php   
                                                    }
                                                }?>
						<!--<option value = '<?php echo __LANGUAGE_ID_ENGLISH__?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_ENGLISH__){echo "selected='selected'"; } ?>><?php echo ucfirst(__LANGUAGE_TEXT_ENGLISH__);?></option>
						<option value = '<?php echo __LANGUAGE_ID_DANISH__?>' <?php if(isset($data['iLanguage']) && $data['iLanguage'] ==__LANGUAGE_ID_DANISH__){echo "selected='selected'"; } ?>><?php echo ucfirst(__LANGUAGE_TEXT_DANISH__);?></option>-->	
					</select>
				</span>
			</label>
			<label class="profile-fields">
				<span class="field-name"><?=t($t_base.'fields/content');?></span>
			</label>
				<script>setup();</script>
				<div style="clear:both;"></div>
					<div id="viewContent">
			        	<textarea  id="content" class="myTextEditor" name="blogArr[szContent]" style="width:100%;height:60%;"><?php if(isset($data['szContent'])){echo $data['szContent']; }?></textarea>
					</div>
		
			<div style="clear:both;"></div>
			<input type="hidden" id="mode"  value="CREATE_NEW_PAGE">
			<?php if(isset($data['id'])){
			echo '
				<input type="hidden" id="id" name="blogArr[id]" value="'.$data["id"].'">';
			 }?>
		</form>
		<br/>
		<div style="clear: both;"></div>
			<div style="text-align: center;">
				<a class="button1"  onclick="previewPageData();"><span><?=t($t_base.'fields/preview');?></span></a>
				<a class="button1" id="addNewPage" onclick="addNewBlogPage();"><span><?php if(isset($data['id'])){echo t($t_base.'fields/save');}else{echo t($t_base.'fields/add');}?></span></a>
				<a class="button1" id="cancelAddingPage" onclick="cancelAddingBlogPage();"><span><?=t($t_base.'fields/cancel');?></span></a>
			</div>
			<br/>
			<br/>
			<div id="preview" style="display: none;"></div>	
	<?php
	}
        
        function display_search_aid_words_lists()
{  
    $t_base="management/landingPageData/";
    $searchAidwordsAry = array();
    $kExplain = new cExplain();  
    $searchAidwordsAry = $kExplain->getAllsearchAidWords();
?>
 <hr /><br />
 <p>Define URL keys for specific search option on front page. Format of URL is e.g. www.transporteca.com/search/IN_to_DK/ where "IN" and "DK" are URL keys.</p>
 <br />
 <table cellpadding="0" cellspacing="0" class="format-4" width="100%">
    <tr> 
        <th width="50%" align="left" valign="top"><?=t($t_base.'fields/search_string')?></th> 
        <th width="40%" align="left" valign="top"><?=t($t_base.'fields/url_key')?></th> 
        <th width="10%" align="left">&nbsp;</th>
    </tr>
    <tr> 
        <td valign="top">User location</td> 
        <td valign="top">IP</td> 
        <td>&nbsp;</td>
     </tr>
    <?php
 	if(!empty($searchAidwordsAry))
 	{	
            $count=1;
            foreach($searchAidwordsAry as $searchAidwordsArys)
            { 
	 ?>
            <tr> 
                <td valign="top"><?=$searchAidwordsArys['szOriginStr'];?></td> 
                <td valign="top"><?=$searchAidwordsArys['szOriginKey']?></td> 
                <td>
                    <a href="javascript:void(0);" onclick="update_aid_words('<?php echo $searchAidwordsArys['id']; ?>','EDIT_SEARCH_AID_WORDS')"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" alt="edit" ></a> 
                    <a href="javascript:void(0);" onclick="update_aid_words('<?php echo $searchAidwordsArys['id']; ?>','DELETE_SEARCH_AID_WORDS')"><img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" class="cursor-pointer" alt="delete"> </a>
                </td>
             </tr> 
	 <?php $count++; }
	}
	elseif(empty($data))
	{
            echo "<tr align='center'><td colspan='7'><strong>".t($t_base.'fields/no_data_to_preview')."</strong></td></tr>";
	}
?>
</table> 
<?php
 }
 
 function add_edit_search_aid_words_lists($kExplain,$searchAidwordsAry=array())
 { 
    $t_base="management/landingPageData/";  
    if(!empty($kExplain->arErrorMessages))
    {
        $formId = 'insurance_form_type_'.$iType;
        $szValidationErrorKey = '';
        foreach($kExplain->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            if($errorKey=='szSpecialError')
            {
                $szSpecialErrorMessage = $errorValue ;
            }
            else
            {
                ?>
                <script type="text/javascript">
                    $("#"+'<?php echo $errorKey; ?>').addClass('red_border');
                </script>
                <?php  
            }
        }
    }  
    
    if(!empty($_POST['addSearchAidWordsAry']))
    {
        $addSearchAidWordsAry = $_POST['addSearchAidWordsAry']; 
        
        $idSearchAdwords = $addSearchAidWordsAry['idSearchAdwords'];
        $szOriginStr = $addSearchAidWordsAry['szOriginStr']; 
        $szDestinationStr = $addSearchAidWordsAry['szDestinationStr']; 
        $szOriginKey = $addSearchAidWordsAry['szOriginKey']; 
        $szDestinationKey = $addSearchAidWordsAry['szDestinationKey'];
    }
    else
    {
        $idSearchAdwords = $searchAidwordsAry['id'];
        $szOriginStr = $searchAidwordsAry['szOriginStr']; 
        $szDestinationStr = $searchAidwordsAry['szDestinationStr']; 
        $szOriginKey = $searchAidwordsAry['szOriginKey']; 
        $szDestinationKey = $searchAidwordsAry['szDestinationKey'];
    } 
    if($idSearchAdwords>0)
    {
        $szButtonText = t($t_base.'fields/update');
    }
    else
    {
        $szButtonText = t($t_base.'fields/add');
    }
?> 
    <div id="isurance_buy_rate_form" style="width:100%;">
        <?php if(!empty($szSpecialErrorMessage)){ ?>
            <p style="font-style:italic;color:red;font-size:14px;"> <?php echo $szSpecialErrorMessage; ?></p>
        <?php } ?>
	<form action="javascript:void(0);" name="search_aid_words_add_edit_form" id="search_aid_words_add_edit_form">
            <table cellpadding="0" cellspacing="1" width="100%">
		<tr>     
                    <td style="width:50%;"><?=t($t_base.'fields/search_string')?></td> 
                    <td style="width:40%;"><?=t($t_base.'fields/url_key')?></td> 
                    <td style="width:20%;" class="font-14">&nbsp;</td> 
		</tr>
		<tr>   
                    <td valign="top">
                         <input type="text" class="addInsurateRatesFields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addSearchAidWordsAry[szOriginStr]" id="szOriginStr" value="<?php echo $szOriginStr; ?>">
                    </td>
                    <td valign="top">
                        <input type="text" class="addInsurateRatesFields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addSearchAidWordsAry[szOriginKey]" id="szOriginKey" value="<?php echo $szOriginKey; ?>">
                    </td>
                    <td valign="top" >  
                        <a href="javascript:void(0)" class="button1" style="float:right;" onclick="validate_aid_word_form_data();" id="search_aid_words_add_button"><span><?php echo $szButtonText; ?></span></a>  
                    </td>  
		</tr> 
            </table> 
            <input type="hidden" name="addSearchAidWordsAry[idSearchAdwords]" id="idSearchAdwords" value="<?php echo $idSearchAdwords; ?>"> 
	</form>
	</div>
	<?php
 }
 
 function viewSEOPageContent($data,$maxOrder)
 {
 	$t_base="management/seoPageData/";
 	?>
  <table cellpadding="0" cellspacing="0" class="format-4" width="100%">
 		<tr>
 			<th width="4%" align="center" valign="top">&nbsp;</th>
 			<th width="25%" align="left" valign="top"><?=t($t_base.'fields/heading_in_sitemap')?></th>
 			<th width="10%" align="left" valign="top" ><?=t($t_base.'fields/status')?></th>
 			<th width="10%" align="left" valign="top" ><?=t($t_base.'fields/language')?></th>
 			<th width="13%" align="center" valign="top" style="text-align:center;"><?=t($t_base.'fields/created')?></th>
 			<th width="14%" align="center" valign="top" style="text-align:center;"><?=t($t_base.'fields/acquisitions')?></th>
 			<th width="15%" align="left" valign="top" >&nbsp;</th>
 			<th width="5%" align="left" valign="top" >&nbsp;</th>
 		</tr>
  <?php
  	if(!empty($data))
  	{	
  		$count=1;
 	 	foreach($data as $terms)
 	 	{ 
                     $kConfig =new cConfig();
        		$langArr=$kConfig->getLanguageDetails('',$terms['iLanguage']);
 	 ?>
 		 	<tr>
 			 	<td valign="top"><?=$count;?></td>
 			 	<td valign="top"><?=$terms['szPageHeading'];?></td>
 			 	<td valign="top"><?=($terms['iPublished']?t($t_base.'fields/publish'):t($t_base.'fields/published_non'))?></td>
 			 	<td valign="top"><?php echo $langArr[0]['szName'];?></td>
 			 	<td valign="top" style="text-align:center;"><?php echo date('d/m/Y',strtotime($terms['dtCreatedOn']))?></td> 	
 			 	<td valign="top" style="text-align:center;"><?php echo number_format($terms['iAquisionCount'])?></td>
 			 	<td valign="top">
 			 		<a href="<?=__MANANAGEMENT_EDIT_SEO_PAGE_URL__.$terms['id']."/"?>"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" alt="edit" ></a>
 				 	<?php if($terms['iOrder']>1){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/up-icon.png" class="cursor-pointer" alt="up" onclick="orderChangeSeoPage('<?=$terms['id']?>','UP')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
 				 	<?php if($maxOrder > $terms['iOrder']){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="cursor-pointer" alt="down" onclick="orderChangeSeoPage('<?=$terms['id']?>','DOWN')">	<?php }else{ ?><span style=" display: inline-block;  width: 19px;"></span><?php } ?>
 				 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" class="cursor-pointer" alt="delete"  onclick="deleteSeoPage('<?=$terms['id']?>')">
 				 </td>
 				 <td>&nbsp;</td>
 			 </tr>
 			 
 	 <?php $count++; }
 	}
 	elseif(empty($data))
 	{
 		echo "<tr align='center'><td colspan='6'><strong>".t($t_base.'fields/no_data_to_preview')."</strong></td></tr>";
 	}
 ?>
 </table>	
 <div style="float:left;padding-top: 10px;cursor: pointer;"><a href="<?=__MANANAGEMENT_ADD_SEO_PAGE_URL__?>">Add new Page</a></div>
 	
 <?php
  }
  
  function previewLandingPage_admin($landingPageDataAry)
 {
 	?>
	<div id="hsbody" class="search-option">
		<div class="search-new">
			<h6 align="center"><?php echo $landingPageDataAry['szTextA']?></h6>
			<form action="" id="start_search_form" name="start_search_form" method="post">
				<div class="fields">
					<p>
						<select name="landingPageAry[szOriginCountry]" class="styled" id="szOriginCountry">
							<option value="">Shipping From</option>
						</select>
					</p>
					<p>
						<select name="landingPageAry[szDestinationCountry]" class="styled" id="szDestinationCountry">
							<option value="">Shipping To</option>
						</select>
						<input name="landingPageAry[szBookingRandomNum]" type="hidden" value="" id="szBookingRandomNum">
						<input name="landingPageAry[iCheck]" type="hidden" value="1" id="iCheck">
					</p>
					<p>
					<a href="javascript:void(0);" class="orange-button2" tabindex="3"><span><?php echo $landingPageDataAry['szTextB']?></span></a>
					</p>
				</div>
			</form>
			<div class="text">
				<p class="book_in_minutes"><?php echo $landingPageDataAry['szTextC']?></p>
				<?php echo $landingPageDataAry['szTextD']?>				
			</div>
		</div>
		<div class="home-new">
			<h2><?php echo $landingPageDataAry['szTextE']?></h2>
			<p><?php echo $landingPageDataAry['szTextF']?></p>
						
			<div class="intro_video">	
			<div align="center" style="width:500px;height:280px;" id="evp-c4e48429cc58c6896f1cb705d41ec6da-wrap" class="evp-video-wrap"></div>
			<script type="text/javascript" src="http://staging.transporteca.com/evp/framework.php?div_id=evp-c4e48429cc58c6896f1cb705d41ec6da&id=dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi0xLm1wNA%3D%3D&v=1352203827&profile=default"></script>
			<script type="text/javascript"><!--
			_evpInit('dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi0xLm1wNA==[evp-c4e48429cc58c6896f1cb705d41ec6da]');//-->
			</script>
						  <!-- <img src="images/see-yourself-transporteca.jpg" alt="" title="" /> -->
		 </div>					
			<h3><?php echo $landingPageDataAry['szTextG']?></h3>
			<div class="benefits">					
				<div class="first common">
					<h5><?php echo $landingPageDataAry['szTextH']?></h5>
					<p><?php echo $landingPageDataAry['szTextI']?></p>
				</div>
				<div class="common">
					<h5><?php echo $landingPageDataAry['szTextJ']?></h5>
					<p><?php echo $landingPageDataAry['szTextK']?></p>
				</div>
				<div class="last common">
					<h5><?php echo $landingPageDataAry['szTextL']?></h5>
					<p><?php echo $landingPageDataAry['szTextM']?></p>
				</div>
				<p align="center" class="button"><a href="<?=__BASE_URL__.'/createAccount/'?>" class="register-button">&nbsp;</a></p>
			</div>
		</div>
	</div>
 	<?php
 }
 function deleteConfirmation($idLandingPage,$szFromPage=false)
 {
 	$t_base="management/LandingPage/";
 	?>
 	<div id="popup-bg"></div>
	 	<div id="popup-container">	
			<div class="popup contact-popup" style="height: auto;">
				<?php echo t($t_base.'title/are_you_sure_to_delete'); ?>
				<br /><br />
				<p align="center">	
					<a href="javascript:void(0);" onclick="deleteLandingPageConfirm('<?php echo $idLandingPage; ?>','<?php echo $szFromPage; ?>')" class="button1"><span><?php echo t($t_base.'fields/ok'); ?></span></a>			
					<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?php echo t($t_base.'fields/cancel');?></span></a>			
				</p>
			</div>
	 	</div>
	</div> 	
 	<?php 
 }
 function deleteConfirmation_SEO($idSeoPage)
 {
 	$t_base="management/LandingPage/";
 	?>
  	<div id="popup-bg"></div>
 	 	<div id="popup-container">	
 			<div class="popup contact-popup" style="height: auto;">
 				<?php echo t($t_base.'title/are_you_sure_to_delete'); ?>
 				<br /><br />
 				<p align="center">	
 					<a href="javascript:void(0);" onclick="deleteSeoPageConfirm('<?php echo $idSeoPage; ?>')" class="button1"><span><?php echo t($t_base.'fields/ok'); ?></span></a>			
 					<a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?php echo t($t_base.'fields/cancel');?></span></a>			
 				</p>
 			</div>
 	 	</div>
 	</div> 	
  	<?php 
  }
  
  function display_rss_edit_form($disabled=false,$idTemplate=false)
{
	if($disabled)
	{
		$disabled_str = "disabled='disabled' ";
		$opacity_str = 'style="opacity:0.4;"';
	}
	else {
		$onclick_preview = " onclick='preview_rss_template()'";
		$onclick_save = " onclick='update_rss_template()'";
	}
	$kRSS = new cRSS();
	if($idTemplate>0)
	{
		$kRSS->loadRssTemplates($idTemplate);
	}
	$t_base="management/rss/";
	
?>
<form id="rss_template_form" name="rss_template_form">
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/title');?></p>
		<p class="fl-65 pos-rel">
			<input type="text" id="szTitle" name="rssTemplateAry[szTitle]" value="<?php echo $kRSS->szTitle;?>" style="width: 460px;">					
		</p>
	</div>
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/description');?></p>
		<p class="fl-65 pos-rel">
			<textarea rows="3" cols="59" name="rssTemplateAry[szDescription]" id="szDescription" style="width: 460px;"><?php echo $kRSS->szDescription;?></textarea> 					
		</p>
	</div>
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/descriptionhtml');?></p>
		<p class="fl-65 pos-rel">
			<textarea rows="5" cols="59" name="rssTemplateAry[szDescriptionHTML]" id="szDescriptionHTML" style="width: 460px;"><?php echo $kRSS->szDescriptionHTML;?></textarea>
			<span style="float:right;margin-top:5px;">
			<a class="button1" <?php echo $opacity_str;?> <?php echo $onclick_save;?> id="save"><span><?=t($t_base.'fields/save');?></span></a>
			</span>
			<input type="hidden" name="rssTemplateAry[idRssTemplate]" id="idRssTemplate" value="<?php echo $idTemplate; ?>" >					
		</p>
	</div>
</form>	
	<?php
}

function rss_template_top_form($rssFeedAry)
{
	$t_base="management/rss/";
	?>
	<div class="oh">
		<p class="fl-33"><?=t($t_base.'fields/rss_message');?></p>
		<p class="fl-65 pos-rel">
			<select id="selectEditor" name="messageName" onchange="selectRssTemplate(this.value);" >
			<?
				if(!empty($rssFeedAry))
				{
					echo "<option ''>".t($t_base.'fields/select')."</option>";
					foreach($rssFeedAry as $rssFeedArys)
					{
						echo "<option value='".$rssFeedArys['id']."'>".$rssFeedArys['szFriendlyName']."</option>";
					}
				}
			?>	
			</select>					
		</p>
	</div>
	<?php
}
function customerMessageView()
{
	$kConfig = new cConfig();
	$allCountriesArr=$kConfig->getAllCountriesOfVerifiedUser(true);
	$t_base="management/AdminMessages/";
	
	$szCountrySelArr=array();
	if($_REQUEST['szCountryArr']!='All' && $_REQUEST['szCountryArr']!='' && $_REQUEST['szCountryArr']!='null')
		$szCountrySelArr=$_REQUEST['szCountryArr'];
	//print_r($_REQUEST);
	$iNewsUpdateArr=array();
	$iNewsUpdate=$_REQUEST['iNewsUpdate'];
	if($iNewsUpdate!='')
	{
		$iNewsUpdateArr=explode("_",$iNewsUpdate);
	}
	
	$iActiveFlagArr=array();
	$iActiveFlag=$_REQUEST['iActiveFlag'];
	if($iActiveFlag!='')
	{
		$iActiveFlagArr=explode("_",$iActiveFlag);
		//print_r($iNewsUpdateArr);
	}
?>
	<div id="show_preview"></div>
	<form name="customerMsg" id="customerMsg" method="post">
		<table cellpadding="3"  cellspacing="3" border="0"  width="100%" style="margin-bottom:20px;">
			<tr>
				<td valign="top"><?=t($t_base.'fields/receivers');?></td>
				<td valign="top"><select size="6" name="sendEmailArr[szCountry]" id="szCountry" multiple="multiple" onchange="select_country(this.value);" style="width:200px;margin-top:7px;">
						<?php
							if(!empty($allCountriesArr))
							{
								?>
									<option value="All"  <?php if($_REQUEST['szCountryArr'][0]=='All') {?> selected <?}?>><?=t($t_base.'fields/all')?></option>
								<?
								foreach($allCountriesArr as $allCountriesArrs)
								{
									?><option value="<?=$allCountriesArrs['id']?>" <?php if(!empty($szCountrySelArr)  && in_array($allCountriesArrs['id'],$szCountrySelArr)) {?> selected <?php }?>><?=$allCountriesArrs['szCountryName']?></option>
									<?php
								}
							}
						?>
					</select>
				</td>
				<td colspan="2" valign="top" align="left" class="message_options">
					<p><?=t($t_base.'fields/accepted_news_and_updates');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iNewsUpdate[]]" id="iNewsUpdate1" value="1" onclick="select_country('news');" <?php if(!empty($iNewsUpdateArr) && in_array('1',$iNewsUpdateArr)) {?> checked <?}else if(empty($iNewsUpdateArr)){?>checked <?}?>/> <?=t($t_base.'fields/yes');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iNewsUpdate[]]" id="iNewsUpdate2" value="2" onclick="select_country('news');" <?php if(!empty($iNewsUpdateArr) && in_array('0',$iNewsUpdateArr)) {?> checked <?}?>/> <?=t($t_base.'fields/no');?></p>
					<p style="margin-top: 7px;"><?=t($t_base.'fields/currently_active');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iActiveFlag[]]" id="iActiveFlag1" value="1" onclick="select_country('active');" <?php if(!empty($iActiveFlagArr) && in_array('1',$iActiveFlagArr)) {?> checked <?}else if(empty($iActiveFlagArr)){?>checked <?}?>/> <?=t($t_base.'fields/yes');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iActiveFlag[]]" id="iActiveFlag2" value="2" onclick="select_country('active');" <?php if(!empty($iActiveFlagArr) && in_array('0',$iActiveFlagArr)) {?> checked <?}?>/> <?=t($t_base.'fields/no');?></p>
				</td>
			</tr>
			<tr>
				<td align="left"><?=t($t_base.'fields/subject')?></td><td colspan="3" align="left"> <input type="text" name="sendEmailArr[szSubject]" id="szSubject" value="<?=$_POST['szSubject']?>" style="width:644px;"/></td>
			</tr>
			<tr>
				<td align="left" valign="top"><?=t($t_base.'fields/html')?></td><td valign="top" colspan="3" align="left"><textarea cols="94" rows="15" name="sendEmailArr[szMessage]" id="szMessage" style="width:644px;"><?=$_POST['szMessage']?></textarea></td>
			</tr>
			</table>
			<table width="100%" cellspacing="3" cellpadding="3" border="0" style="margin-bottom:20px;">
			<tr>
				<td width="30%" align="right"><a href="javascript:void(0)" onclick="open_html_var('user');"><?=t($t_base.'fields/html_variables_available');?></a></td>
				<td width="30%" align="right"  valign="top"><p style="padding-top: 6px;"><a href="javascript:void(0)" onclick="get_customer_email_list();"><span id="user_count">0</span> <?=t($t_base.'fields/recipients_selected');?></a></p></td>
				<td valign="top" colspan="2" align="right" width="40%">
				
				<a href="javascript:void(0)" class="button1" tabindex="7" onclick="preview_send_email_to_selected_user()" id="confirm_email"><span><?=t($t_base.'fields/preview')?></span></a>
				<a href="javascript:void(0)" class="button1" tabindex="7" onclick="confirm_send_email_to_selected_user()" id="cancel_email"><span><?=t($t_base.'fields/send')?></span></a>
				</td>
			</tr>
		</table>
	</form>
	<div id="message_preview"></div>
	<?php
}
function getAllHtmlVariable($flag)
{
//echo $flag;
$t_base="management/AdminMessages/";
?>
	<div id="popup-bg"></div>
	<div id="popup-container">
		<div class="popup signin-popup signin-popup-verification">
		<h5><?=t($t_base.'fields/available_codes');?></h5>
		<?php if($flag=='forwarder') {?>
		<p>szFirstName<br/>
		szLastName<br/>
		szDisplayName</p>
		<?php }else if($flag=='user') {?>
		<p>szFirstName<br/>
		szLastName<br/>
		szCompanyName</p>
		<?php }?><br/>
		<p align="center"><a href="javascript:void(0);" class="button1"  onclick="cancelPreviewMsg('show_preview');"><span><?=t($t_base.'fields/close');?></span></a></p>
		</div>
	</div>	
<?php	
}
function forwarderBulkMsgView()
{
	$t_base="management/AdminMessages/";
	$kForwarder	= new 	cForwarder();
	$kAdmin	= new 	cAdmin();
	$getAllforwarderArr=$kAdmin->getAllForwarderHaveVerifiedUser();
	$getAllforwarderProfileArr=$kForwarder->getAllForwarderProfiles();
	
	if($_REQUEST['szForwarderArr']!='All' && $_REQUEST['szForwarderArr']!='' && $_REQUEST['szForwarderArr']!='null')
		$szForwarderSelArr=$_REQUEST['szForwarderArr'];
		
	if($_REQUEST['szProfileArr']!='All' && $_REQUEST['szProfileArr']!='' && $_REQUEST['szProfileArr']!='null')
		$szProfileSelArr=$_REQUEST['szProfileArr'];	
		
		$iForwarderStatusFlag=$_REQUEST['iForwarderStatusFlag'];
		if($iForwarderStatusFlag!='')
		{
			$iForwarderStatusArr=explode("_",$iForwarderStatusFlag);
		}
		$iForwarderUserStatusFlag=$_REQUEST['iForwarderUserStatusFlag'];
		if($iForwarderUserStatusFlag!='')
		{
			$iForwarderUserStatusFlagArr=explode("_",$iForwarderUserStatusFlag);
		}
	?>
	<div id="show_preview"></div>
	<form name="fowarderMsg" id="fowarderMsg" method="post">
		<table cellpadding="3"  cellspacing="3" border="0"  width="100%" style="margin-bottom:20px;">
			<tr>
				<td width="15%" valign="top"><?=t($t_base.'fields/receivers');?></td>
				<td valign="top" width="30%"> 
				<select size="6" name="sendEmailArr[szForwarder]" id="szForwarder" multiple="multiple" onchange="select_forwarder();" style="width:300px;margin-top:7px;">
						<?php
							if(!empty($getAllforwarderArr))
							{?>
								<option value="All"  <?php if($_REQUEST['szForwarderArr'][0]=='All') {?> selected <?}?>><?=t($t_base.'fields/all')?></option>
							<?
								foreach($getAllforwarderArr as $getAllforwarderArrs)
								{
									$szDisplayNameText='';
									if($getAllforwarderArrs['szDisplayName']!='')
									{
										$szDisplayNameText=$getAllforwarderArrs['szDisplayName'];
									}
									else
									{
										$szDisplayNameText=$getAllforwarderArrs['szControlPanelUrl'];
									}
									?><option value="<?=$getAllforwarderArrs['id']?>" <?php if(!empty($szForwarderSelArr)  && in_array($getAllforwarderArrs['id'],$szForwarderSelArr)) {?> selected <?php }?>><?=$szDisplayNameText?></option>
									<?php
								}
							}
						?>
					</select>
				</td>
				<td valign="top" width="25%">
				<select size="6" name="sendEmailArr[szProfile]" id="szProfile" multiple="multiple" onchange="select_forwarder_profile();" style="margin-top:7px;">
						<?php
							if(!empty($getAllforwarderProfileArr))
							{
								?>
								<option value="All"  <?php if($_REQUEST['szProfileArr'][0]=='All') {?> selected <?}?>><?=t($t_base.'fields/all')?></option>
								<?
								foreach($getAllforwarderProfileArr as $getAllforwarderProfileArrs)
								{
									?><option value="<?=$getAllforwarderProfileArrs['id']?>" <?php if(!empty($szProfileSelArr)  && in_array($getAllforwarderProfileArrs['id'],$szProfileSelArr)) {?> selected <?php }?>><?=$getAllforwarderProfileArrs['szRoleName']?></option>
									<?php
								}
							}
						?>
					</select>
				</td>
				<td valign="top" align="left" class="message_options" width="20%">
					<p><?=t($t_base.'fields/active_forwarders');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iForwarderStatus[]]" id="iForwarderStatus1" value="1" onclick="getForwarderUserData();" <?php if(!empty($iForwarderStatusArr) && in_array('1',$iForwarderStatusArr)) {?> checked <?}else if(empty($iForwarderStatusArr)){?> checked<?php }?>/> <?=t($t_base.'fields/yes');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iForwarderStatus[]]" id="iForwarderStatus2" value="2" onclick="getForwarderUserData();" <?php if(!empty($iForwarderStatusArr) && in_array('0',$iForwarderStatusArr)) {?> checked <?}?>/> <?=t($t_base.'fields/no');?></p>
					<p style="margin-top: 7px;"><?=t($t_base.'fields/active_users');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iForwarderUserStatus[]]" id="iForwarderUserStatus1" value="1" onclick="getForwarderUserData();" <?php if(!empty($iForwarderUserStatusFlagArr) && in_array('1',$iForwarderUserStatusFlagArr)) {?> checked <?}else if(empty($iForwarderUserStatusFlagArr)){?> checked<?php }?>/> <?=t($t_base.'fields/yes');?></p>
					<p class="checkbox-ab"><input type="checkbox" name="sendEmailArr[iForwarderUserStatus[]]" id="iForwarderUserStatus2" value="2" onclick="getForwarderUserData();" <?php if(!empty($iForwarderUserStatusFlagArr) && in_array('0',$iForwarderUserStatusFlagArr)) {?> checked <?}?>/> <?=t($t_base.'fields/no');?></p>
				</td>
			</tr>
			<tr>
				<td align="left"><?=t($t_base.'fields/subject')?></td><td colspan="3" align="left"> <input type="text" name="sendEmailArr[szSubject]" id="szSubject" value="<?=$_POST['szSubject']?>" style="width:644px;"/></td>
			</tr>
			<tr>
				<td align="left" valign="top"><?=t($t_base.'fields/html')?></td><td valign="top" colspan="3" align="left"><textarea cols="94" rows="15" name="sendEmailArr[szMessage]" id="szMessage" style="width:644px;"><?=$_POST['szMessage']?></textarea></td>
			</tr>
			</table>
			<table width="100%" cellspacing="3" cellpadding="3" border="0" style="margin-bottom:20px;">
			<tr>
				<td width="30%" align="right"><a href="javascript:void(0)" onclick="open_html_var('forwarder');"><?=t($t_base.'fields/html_variables_available');?></a></td>
				<td width="30%" align="right" valign="top"><p style="padding-top: 6px;"><a href="javascript:void(0)" onclick="get_forwarder_email_list();"><span id="user_count">0</span> <?=t($t_base.'fields/recipients_selected');?></a></p></td>
				<td valign="top" colspan="2" align="right" width="40%">
				
				<a href="javascript:void(0)" class="button1" tabindex="7" onclick="preview_send_email_to_selected_forwarder()" id="confirm_email"><span><?=t($t_base.'fields/preview')?></span></a>
				<a href="javascript:void(0)" class="button1" tabindex="7" onclick="confirm_send_email_to_selected_forwarder()" id="cancel_email"><span><?=t($t_base.'fields/send')?></span></a>
				</td>
			</tr>
		</table>
	</form>
	
	<div id="forwarder_preview_msg"></div>	
	<?php
}
function sendMessageLists($gtotal,$page=1)
{
	$kAdmin = new cAdmin();
	$t_base="management/AdminMessages/";
	$allSendMessageArr=$kAdmin->getAllMessageSend('',$page);
	//print_r($allSendMessageArr);
	
	require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
	$kPagination  = new Pagination($gtotal,__CONTENT_PER_PAGE_MESSAGES_REVIEWS__,15);
?>
	<table cellspacing="0" id="send_message_list" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
			<td style="text-align:left;width:16%;"><strong><?=t($t_base.'fields/date_time')?></strong></td>
			<td style="text-align:left;width:25%;"><strong><?=t($t_base.'fields/email_address')?></strong></td>
			<td style="text-align:left;width:52%;"><strong><?=t($t_base.'fields/subject')?></strong></td>
			<td style="text-align:left;width:7%;"></td>
		</tr>
		<?php
			if(!empty($allSendMessageArr))
			{
				$i=1;
				foreach($allSendMessageArr as $allSendMessageArrs)
				{
					//print_r($allSendMessageArrs['szSubject']);
					//die;
					?>
						<tr id="message_data_<?=$i?>" onclick="select_message_send_tr('message_data_<?=$i?>','<?=$allSendMessageArrs['id']?>')">
							<td><?=((!empty($allSendMessageArrs["dtSend"]) && $allSendMessageArrs["dtSend"]!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($allSendMessageArrs["dtSend"])):"")?></td>
							<td><?=returnLimitData($allSendMessageArrs['szEmail'],20)?></td>
							<td><?=returnLimitData($allSendMessageArrs['szSubject'],50)?></td>
							<td><a href="javascript:void(0)" onclick="resend_messages('<?=$allSendMessageArrs['id']?>')"><?=t($t_base.'fields/resend');?></a></td>
						</tr>
					<?php
					++$i;
				}
			}
		?>
	</table>
	<?php
		if($gtotal>0)
		{
			$kPagination->paginate('showPageMessageReview');
			if($kPagination->links_per_page>1)
			{
				echo $kPagination->renderFullNav();
			}
		}
	?>
	<br/>
	<p align="right"><a href="javascript:void(0)" id="preview_button" style="opacity:0.4;" class="button1"><span><?=t($t_base.'fields/preview');?></span></a></p>
<?php
} 
function searchNotificationListingHtml()
{
    $kExplain = new cExplain();
    $searchNotificationArr=$kExplain->getSearchNotificationListing();
    $t_base = "management/uploadService/";
?>
<script type="text/javascript">
        document.onclick=check;
        function check(e){
            
        var deselectFlag=false;    
        var t = (e && e.target) || (event && event.srcElement);
        while(t.parentNode){
            if(t==document.getElementById('search_noftification_listing')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('search_noti_addedit')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('search_noti_delete')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('delete-button')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('cancel-button')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('search_noftification_popup')){    
                deselectFlag=true;
            }
            
                t=t.parentNode
            }
            
            if(!deselectFlag)
            {
                //selectPricingServiceTradingData();
                
                $("#mode-transport-table .selected-row").removeClass("selected-row");
                var page_id=$("#idAutomatedRfqResponse").val();
                $("#search_noti_addedit").attr("onclick",'');
                $("#search_noti_addedit").unbind("click");
                $("#search_noti_addedit").html("<span>Add</span>");
                $("#search_noti_addedit").click(function(){openAddEditSearchNotificationForm('','OPEN_ADD_SEARCH_NOTIFICTION_FORM');});
                $("#search_noti_addedit").attr("style",'opacity:1;');
                
                $("#search_noti_delete").unbind("click");                
                $("#search_noti_delete").attr("style",'opacity:0.4;');
            }
        }
     </script>

        <div  class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">
        <table id="mode-transport-table" class="format-2" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr id="table-header">
                <td style="text-align:left;width:18%"><strong><?=t($t_base.'fields/from_country')?></strong></td>
                <td style="text-align:left;width:18%"><strong><?=t($t_base.'fields/search_contains')?></strong></td>
                <td style="text-align:left;width:18%"><strong><?=t($t_base.'fields/to_country')?></strong></td>
                <td style="text-align:left;width:18%"><strong><?=t($t_base.'fields/to_contains')?></strong></td>
                <td style="text-align:left;width:10%"><strong><?=t($t_base.'fields/language')?></strong></td>
                <td style="text-align:left;width:18%"><strong><?=t($t_base.'fields/user_options')?></strong></td>
            </tr>
            <?php
            if(!empty($searchNotificationArr))
            {
                foreach($searchNotificationArr as $searchNotificationArrs)
                {
                    
                    /*if($searchNotificationArrs['iLanguage']==__LANGUAGE_ID_ENGLISH__)
                        $szLanguage =  ucwords(__LANGUAGE_TEXT_ENGLISH__);
                    else if($searchNotificationArrs['iLanguage']==__LANGUAGE_ID_DANISH__)
                        $szLanguage =  ucwords(__LANGUAGE_TEXT_DANISH__);
                    else if($searchNotificationArrs['iLanguage']==__LANGUAGE_ID_SWEDISH__)
                        $szLanguage =  ucwords(__LANGUAGE_TEXT_SWEDISH__);
                    else if($searchNotificationArrs['iLanguage']==__LANGUAGE_ID_NORWEGIAN__)
                        $szLanguage = ucwords(__LANGUAGE_TEXT_NORWEGIAN__);*/
                    
                    $kConfig =new cConfig();
                    $langArr=$kConfig->getLanguageDetails('',$searchNotificationArrs['iLanguage']);
                    $szLanguage=ucwords($langArr[0]['szName']);
                    
                    $userOptionStr='';
                   
                    if($searchNotificationArrs['idButtonType']!="")
                    {
                        $idButtonTypeArr=explode(";",$searchNotificationArrs['idButtonType']);
                       //print_r($idButtonTypeArr);
                        if(!empty($idButtonTypeArr) && in_array(__CLOSE_BUTTON__, $idButtonTypeArr))
                        {
                            $userOptionStr=__CLOSE_TEXT_BUTTON__;
                        }
                        
                        if(!empty($idButtonTypeArr) && in_array(__CONTINUE_BUTTON__, $idButtonTypeArr))
                        {
                            if($userOptionStr!='')
                                $userOptionStr .=", ".__CONTINUE_TEXT_BUTTON__;
                           else
                               $userOptionStr .=__CONTINUE_TEXT_BUTTON__;
                        }
                        
                        if(!empty($idButtonTypeArr) && in_array(__GO_TO_BUTTON__, $idButtonTypeArr))
                        {
                            if($userOptionStr!='')
                                $userOptionStr .=", ".__GO_TO_TEXT_BUTTON__;
                           else
                               $userOptionStr .=__GO_TO_TEXT_BUTTON__;
                        }
                    }
                
                    
?>
            <tr id="search_noti_<?php echo $searchNotificationArrs['id'];?>" onclick="selectSearchNotificationRow('<?php echo $searchNotificationArrs['id'];?>');">
                <td style="text-align:left;width:18%"><?php echo $searchNotificationArrs['szFromCountry'];?></td>
                <td style="text-align:left;width:18%"><?php if($searchNotificationArrs['szFromContains']!=''){echo $searchNotificationArrs['szFromContains'];}else{ echo "*";}?></td>
                <td style="text-align:left;width:18%"><?php echo $searchNotificationArrs['szToCountry'];?></td>
                <td style="text-align:left;width:18%"><?php if($searchNotificationArrs['szToContains']!=''){echo $searchNotificationArrs['szToContains'];}else{ echo "*";}?></td>
                <td style="text-align:left;width:10%"><?php echo $szLanguage;?></td>
                <td style="text-align:left;width:18%"><?php echo $userOptionStr;?></td>
            </tr>
                <?php
                }
            }
            else
            {?>
            <tr><td colspan="6" style="text-align:center;"><strong><?php echo t($t_base.'messages/no_record_found'); ?></strong></td></tr>
            
            <?php
            }
            ?>
        </table>
        </div>
        <div class="btn-container clearfix">
            <p style="text-align:right;">
                <a id="search_noti_addedit" class="button1" href="javascript:void(0);" onclick="openAddEditSearchNotificationForm('','OPEN_ADD_SEARCH_NOTIFICTION_FORM');">
                        <span>Add</span>
                </a>
                <a id="search_noti_delete" class="button2" style="opacity: 0.4" href="javascript:void(0);">
                        <span>Delete</span>
                </a>
            </p>
        </div>
   <?php 
}

function searchNotificationAddEditForm($kExplain)
{
    $kConfig = new cConfig();
    $allCountryAry = array();
    $allCountryAry = $kConfig->getAllCountryInKeyValuePair(false,false,false,false,true); 
    $t_base = "management/uploadService/";
    $disabled="disabled='true'";
    if(!empty($_POST['addEditSearchNotificationAry']['idButtonType']))
    {
        if(in_array(__GO_TO_BUTTON__, $_POST['addEditSearchNotificationAry']['idButtonType']))
        {
            $disabled='';
        }
    } 
    $langArr=$kConfig->getLanguageDetails();
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:430px;"> 
            <h3><?php echo t($t_base.'title/setup_search_notification'); ?></h3> <br/>
            <span>Comparison is case-insensitive, asterix (*) can be used.</span>
            <form name="addSearchNotificationForm" id="addSearchNotificationForm" method="post" action="javascription:void(0);">
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/from_country'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addEditSearchNotificationAry[idFromCountry]" id="idFromCountry" onchange="enableSaveButtonForSearchNotification('addSearchNotificationForm');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value="">Select</option>
                            <option value="All" <?php echo (('All'==$_POST['addEditSearchNotificationAry']['idFromCountry'])?'selected':''); ?>>Any</option>
                            <?php
                                if(!empty($allCountryAry))
                                {
                                    foreach($allCountryAry as $key=>$allCountryArys)
                                    {
                                        ?>
                                        <option value="<?php echo $allCountryArys['id']?>" <?php echo (($allCountryArys['id']==$_POST['addEditSearchNotificationAry']['idFromCountry'])?'selected':''); ?>><?php echo $allCountryArys['szCountryName']?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                </div> 
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/from_contains'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addEditSearchNotificationAry[szFromContains]" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm');" id="szFromContains" value="<?php echo $_POST['addEditSearchNotificationAry']['szFromContains'];?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/to_country'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addEditSearchNotificationAry[idToCountry]" onchange="enableSaveButtonForSearchNotification('addSearchNotificationForm');" id="idToCountry"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value="">Select</option>
                            <option value="All" <?php echo (('All'==$_POST['addEditSearchNotificationAry']['idToCountry'])?'selected':''); ?>>Any</option>
                            <?php
                                if(!empty($allCountryAry))
                                {
                                    foreach($allCountryAry as $key=>$allCountryArys)
                                    {
                                        ?>
                                        <option value="<?php echo $allCountryArys['id']?>" <?php echo (($allCountryArys['id']==$_POST['addEditSearchNotificationAry']['idToCountry'])?'selected':''); ?>><?php echo $allCountryArys['szCountryName']?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/to_contains'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addEditSearchNotificationAry[szToContains]" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm');" id="szToContains" value="<?php echo $_POST['addEditSearchNotificationAry']['szToContains'];?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/language'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addEditSearchNotificationAry[iLanguage]" id="iLanguage" onchange="enableSaveButtonForSearchNotification('addSearchNotificationForm');"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value="">Select</option>
                            <?php
                            if(!empty($langArr))
                            {
                                foreach($langArr as $langArrs)
                                {?>
                                        <option value="<?php echo $langArrs['id'];?>" <?php echo (($langArrs['id']==$_POST['addEditSearchNotificationAry']['iLanguage'])?'selected':''); ?>><?php echo ucwords($langArrs['szName']);?></option>
                                   <?php 
                                }
                            }?>
                            
<!--                            <option value="<?php echo __LANGUAGE_ID_ENGLISH__;?>" <?php echo ((__LANGUAGE_ID_ENGLISH__==$_POST['addEditSearchNotificationAry']['iLanguage'])?'selected':''); ?>><?php echo ucwords(__LANGUAGE_TEXT_ENGLISH__);?></option>
                            <option value="<?php echo __LANGUAGE_ID_NORWEGIAN__;?>" <?php echo ((__LANGUAGE_ID_NORWEGIAN__==$_POST['addEditSearchNotificationAry']['iLanguage'])?'selected':''); ?>><?php echo ucwords(__LANGUAGE_TEXT_NORWEGIAN__);?></option>
                            <option value="<?php echo __LANGUAGE_ID_SWEDISH__;?>" <?php echo ((__LANGUAGE_ID_SWEDISH__==$_POST['addEditSearchNotificationAry']['iLanguage'])?'selected':''); ?>><?php echo ucwords(__LANGUAGE_TEXT_SWEDISH__);?></option>-->
                            
                        </select>
                    </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <p style="margin-top:10px;"><?php echo t($t_base.'fields/popup_heading'); ?></p>
                    <span class="quote-field-container" style="width:100%;">
                        <input type="text" name="addEditSearchNotificationAry[szHeading]" id="szHeading" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm');" value="<?php echo $_POST['addEditSearchNotificationAry']['szHeading'];?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:100%;"><?php echo htmlentities(t($t_base.'fields/popup_message_text')); ?><br/>
                        <textarea style="height:100px;resize:none;" name="addEditSearchNotificationAry[szMessage]" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm');" id="szMessage" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" ><?php echo $_POST['addEditSearchNotificationAry']['szMessage'];?></textarea>
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:100%;" id="ButtonDivId">
                        <?php echo t($t_base.'fields/buttons'); ?><br/>
                        <span class="checkbox-container"><input type="checkbox" <?php if(!empty($_POST['addEditSearchNotificationAry']['idButtonType'])){ if(in_array(__CLOSE_BUTTON__, $_POST['addEditSearchNotificationAry']['idButtonType'])){?> checked <?php } }?> name="addEditSearchNotificationAry[idButtonType][]" id="idButtonType1" onclick="enableSaveButtonForSearchNotification('addSearchNotificationForm');" value="1" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />&nbsp;Close</span><br />
                        <span class="checkbox-container"><input type="checkbox" <?php if(!empty($_POST['addEditSearchNotificationAry']['idButtonType'])){ if(in_array(__CONTINUE_BUTTON__, $_POST['addEditSearchNotificationAry']['idButtonType'])){?> checked <?php } }?> name="addEditSearchNotificationAry[idButtonType][]" id="idButtonType2" onclick="enableSaveButtonForSearchNotification('addSearchNotificationForm');" value="2" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />&nbsp;Continue</span><br />
                        <div class="clearfix setup-search-row"><span class="checkbox-container"><input type="checkbox" <?php if(!empty($_POST['addEditSearchNotificationAry']['idButtonType'])){ if(in_array(__GO_TO_BUTTON__, $_POST['addEditSearchNotificationAry']['idButtonType'])){?> checked <?php } }?> name="addEditSearchNotificationAry[idButtonType][]" id="idButtonType3" onclick="enableTextUrlField();enableSaveButtonForSearchNotification('addSearchNotificationForm');" value="3" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />&nbsp;Go to</span>&nbsp;<span class="text-input"><input type="text" <?php echo $disabled;?> name="addEditSearchNotificationAry[szButtonText]" placeholder="Text" id="szButtonText" value="<?php echo $_POST['addEditSearchNotificationAry']['szButtonText'];?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm');"/></span>&nbsp;<span class="url-input"><input type="text" <?php echo $disabled;?> name="addEditSearchNotificationAry[szButtonUrl]" id="szButtonUrl" value="<?php echo $_POST['addEditSearchNotificationAry']['szButtonUrl'];?>" placeholder="Url" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm');"/></span></div>
                        
                     </span>
                </div>
                <div class="btn-container">
                    <p align="center">
                        <input type="hidden" name="addEditSearchNotificationAry[idSearchNoti]" id="idSearchNoti" value="<?php echo $_POST['addEditSearchNotificationAry']['idSearchNoti'];?>" />
                        <a href="javascript:void(0)" id="save-button" style="opacity:0.4" class="button1" ><span><?=t($t_base.'fields/save');?></span></a>
                        <a href="javascript:void(0)" id="cancel-button" class="button2" onclick="closeTip('search_noftification_popup')"><span><?=t($t_base.'fields/close');?></span></a></p>
                    </div>
            </form>
        </div>
    </div>
     <?php
}
function getRoleListing($t_base)
{
     echo selectPageFormat('RolePermission');
     $kAdmin = new cAdmin();
     $roleArr=$kAdmin->getAllAdminRoles();
     
     
     ?>
     <table cellpadding="0" id="management" cellspacing="0" border="0" class="format-4" width="100%">
		<tr>
                    <th width="75%" align="left" valign="top"><?=t($t_base.'fields/role_name')?></th> 
                    <th width="25%" align="left" valign="top"><?=t($t_base.'fields/action')?></th>
		</tr>
		<?php
		$i=1;
		if(!empty($roleArr))
		{
                    foreach($roleArr as $roleArrs)
                    {
                       ?>
                        <tr id="role_type_<?php echo $roleArrs['id']; ?>" onclick="selectRoleTypeRow('<?php echo $roleArrs['id']; ?>')">
                            <td><?php echo $roleArrs['szFriendlyName']; ?></td>
                            <td><?php if($roleArrs['szRoleCode']=='__ADMIN__'){ echo "All Access"; }else{?> <a href="javascript:void(0);" onclick="setPermissionForRole('<?php echo $roleArrs['szRoleCode']?>','<?php echo $roleArrs['id']?>');">Set Permission</a><?php }?></td>
                        </tr>
                        <?php
                       $i++;
                    }
		}
		?>
		</table>
                <br>
                <div style="float: right;">
                    <a id="new_admin" class="button1" style="opacity:1;" onclick="createNewAdminRole('CREATE_NEW_ADMIN_ROLE')">
                        <span>Add New Role</span>
                    </a>
                    <a id="delete_role" class="button2" style="opacity:0.4;">
                        <span>Delete</span>
                    </a>
                </div>    
<?php
}

function getRolePermissionListing($t_base,$idRole,$szRoleType)
{
    $kAdmin = new cAdmin();
     $roleArr=$kAdmin->getAllAdminRoles($idRole);
     //print_r($roleArr);
     $permissionArr=$kAdmin->getMenuPagePermmissionList();
     
     $permissionSubPageArr=$kAdmin->getMenuPagePermmissionList(3);
     
     
     $permissionRoleArr= $kAdmin->getAllPermissionByRoles($szRoleType,'',true);
     ?>
     <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:90%;">
            <h3><?php echo t($t_base.'title/set_role_permissions')." For ".$roleArr[0]['szFriendlyName']; ?></h3> <br/>
            <form name="setRolePermissionForm" id="setRolePermissionForm" method="post">
                <div style="max-height: 400px;overflow-y: auto">
                    <?php
                    
                        if(!empty($permissionArr))
                        {
                            foreach($permissionArr as $key=>$permissionArrs)
                            {
                                //print_r($permissionArrs);

                                $counter=count($permissionArrs)-1;
                                $t=0;
                                $j=0;
                                $openPageLinkCheckBox=false;
                                foreach($permissionArrs as $permissionArrss)
                                {
                                    if($permissionArrss['iType']=='1')
                                    {?>
                                            <h5 class="checkbox-container"><input name="permissionArr[]" onclick="enableDiablePagePermissionBlock('<?php echo $permissionArrss['id'];?>');" type="checkbox" id="per_<?php echo $permissionArrss['id'];?>" <?php if(!empty($permissionRoleArr) && in_array($permissionArrss['id'],$permissionRoleArr)){?> checked <?php  }?> value="<?php echo $permissionArrss['id'];?>"/><?php echo $permissionArrss['szFriendlyName']?></h5>
                                       <?php 
                                       if(!empty($permissionRoleArr) && in_array($permissionArrss['id'],$permissionRoleArr)){
                                           $openPageLinkCheckBox=true;
                                           
                                       }
                                       $idMainMenu=$permissionArrss['id'];
                                    }
                                    else
                                    {

                                        if($t==0)
                                        {?>
                                            <table width="100%" cellspacing="0" cellpadding="1" id="check_box_list_<?php echo $idMainMenu;?>" <?php if(!$openPageLinkCheckBox){?> style="display:none;" <?php }?> >
                                                <tr>
                                        <?php 
                                        }
                                        if($j<=4)
                                        {?>
                                                    <td style="width:20%">
                                                        <span class="checkbox-container" >
                                                            <input name="permissionArr[]" type="checkbox" <?php if(!empty($permissionRoleArr) && in_array($permissionArrss['id'],$permissionRoleArr)){?> checked <?php  }?> id="per_<?php echo $permissionArrss['id'];?>" value="<?php echo $permissionArrss['id'];?>"/><?php echo $permissionArrss['szFriendlyName']?>
                                                            <?php if($permissionArrss['szPageName']=='__OUTSTANDING__'){?>
                                                            
                                                            <input style="left:90px;" name="permissionArr[]" type="checkbox" <?php if(!empty($permissionRoleArr) && in_array($permissionSubPageArr[__UPDATE_STATUS__]['id'],$permissionRoleArr)){?> checked <?php  }?> id="per_<?php echo $permissionSubPageArr[__UPDATE_STATUS__]['id'];?>" value="<?php echo $permissionSubPageArr[__UPDATE_STATUS__]['id'];?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $permissionSubPageArr[__UPDATE_STATUS__]['szFriendlyName']?>
                                                            <?php }?>
                                                            <?php if($permissionArrss['szPageName']=='__SETTLED__'){?>
                                                            
                                                            <input style="left:60px;" name="permissionArr[]" type="checkbox" <?php if(!empty($permissionRoleArr) && in_array($permissionSubPageArr[__UPDATE_COMMENT__]['id'],$permissionRoleArr)){?> checked <?php  }?> id="per_<?php echo $permissionSubPageArr[__UPDATE_COMMENT__]['id'];?>" value="<?php echo $permissionSubPageArr[__UPDATE_COMMENT__]['id'];?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $permissionSubPageArr[__UPDATE_COMMENT__]['szFriendlyName']?>
                                                            <?php }?>
                                                        </span>
                                                    </td>
                                        <?php 
                                            ++$j;
                                        }
                                        if($j==5 && $t<$counter)
                                        {
                                         $j=0;
                                         ?>
                                            </tr><tr>
                                           <?php 
                                        }
                                         ++$t;
                                        if($t==$counter)
                                        {?>
                                            </tr>           
                                            </table>
                                           <?php 
                                        }


                                    }
                                }?>
                                <hr class="cargo-top-line">
                               <?php
                            }
                        }
                    ?>
                </div>
            </form>
            <div class="oh">
                <p align="center">
                    <a class="button1" onclick="confrimRolePermission('<?php echo $idRole;?>')" href="javascript:void(0);">
                        <span>Confirm</span>
                    </a>
                    <a class="button2" onclick="cancelEditMAnageVar(0)" href="javascript:void(0);">
                        <span>Cancel</span>
                </a>
                </p>
            </div>
        </div>
    </div>
     <?php
     
}

function checkManagementPermission($kAdmin,$pagePermission,$iType=1)
{
    if($kAdmin->szProfileType!='__ADMIN__' && $kAdmin->szProfileType!='')
    {       
       // $kAdmin=new cAdmin();
        $permissionArr= $kAdmin->getAllPermissionByRoles($kAdmin->szProfileType);
        if(!empty($permissionArr))
        {              
            $idPermissionMapped=$permissionArr[$iType][$pagePermission];
            if((int)$idPermissionMapped>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else if($kAdmin->szProfileType=='__ADMIN__')
    { 
        return true;
    }
    else if($kAdmin->szProfileType=='')
    {
        return false;
    }
}
function redirectPagePermission($menuFlag,$checkPerissionUserFlag,$checkPerissionMessageFlag,$checkPerissionSystemFlag,$checkPerissionFinancialFlag,$checkPerissionOperationsFlag,$checkPerissionFilesFlag,$checkPerissionContentFlag,$checkPerissionDashboardFlag,$pagePermission,$kAdmin)
{
    $redirectFlag=false;
    if($menuFlag=='content'  && !$checkPerissionContentFlag)
    {
        $redirectFlag=true;
    }
    else if($menuFlag=='graph'  && !$checkPerissionUserFlag)
    {
       $redirectFlag=true;
    }
    else if($menuFlag=='msg'  && !$checkPerissionMessageFlag)
    {
        $redirectFlag=true;;
    }
    else if($menuFlag=='tncfwd'  && !$checkPerissionSystemFlag)
    {
        $redirectFlag=true;
    }
    else if($menuFlag=='fin'  && !$checkPerissionFinancialFlag)
    {
        $redirectFlag=true;
    }
    else if($menuFlag=='opr'  && !$checkPerissionOperationsFlag)
    {
        $redirectFlag=true;
    }
    else if($menuFlag=='file'  && !$checkPerissionFilesFlag)
    {
       $redirectFlag=true;
    }
    
    if(!$redirectFlag && $kAdmin->szProfileType!='__ADMIN__' && $kAdmin->szProfileType!='' && trim($pagePermission)!='')
    {
        $perPageArr=$kAdmin->getAllPermissionByRoles($kAdmin->szProfileType,$pagePermission,true);
        
        if((int)$perPageArr[0]==0)
        {
            $redirectFlag=true;
        }
    }
    
    if($redirectFlag)
    {
        $redirect_url=__MANANAGEMENT_DASHBOARD_URL__;
        header("Location:".$redirect_url);
        die();
    }
}

function addNewAdminRoleType($t_base)
{?>
     <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:20%;">
            <form name="addAdminRoleForm" id="addAdminRoleForm" action="javascript:void(0)" method="post">
            <h3><?php echo t($t_base.'title/add_role_type'); ?></h3> <br/>
            <p><input name="szRoleType" id="szRoleType" value="<?php echo $_POST['szRoleType'];?>" onkeyup="enableAddRoleButton(this.id,this.value);"></p><br />
            </form>
            <div class="oh">
                <p align="center">
                    <a class="button1" id="add_role_button" style="opacity:0.4" href="javascript:void(0);">
                        <span>Confirm</span>
                    </a>
                    <a class="button2" onclick="cancelEditMAnageVar(0)" href="javascript:void(0);">
                        <span>Cancel</span>
                </a>
                </p>
            </div>
        </div>
    </div>    
 <?php   
}

function changePasswordHtml($kAdmin,$successFlag=false)
{
    
$t_base = "management/myprofile/";
$t_base_error = "Error";
?>
     <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:30%;">
         <?php   
        if(!empty($kAdmin->arErrorMessages)){
?>
<div id="regError" class="errorBox">
<div class="header"><?=t($t_base_error.'/please_following');?></div>
<div id="regErrorList">
<ul>
<?php
	foreach($kAdmin->arErrorMessages as $key=>$values)
	{
		?><li><?=$values?></li>
		<?php	
	}
?>
</ul>
</div>
</div>
<?php }
    if($successFlag){?>
          <h5><strong><?=t($t_base.'fields/password');?></strong>	</h5>  
          <p style="color:green;"><?php echo  ucwords(t($t_base.'messages/password_change_successfully'));?></p><br />
          <p align="center"><a href="javascript:void(0)" class="button1" onclick="cancelEditMAnageVar(0);"><span><?=t($t_base.'fields/close');?></span></a></p>
        <?php
    }else{
?>
     <form name="changePassword" id="changePassword" method="post">
		<h5><strong><?=t($t_base.'fields/password');?></strong>	</h5>	
		<div class="clearfix email-fields-container">
			<span class="quote-field-container" style="width:35%;"><?=t($t_base.'fields/current_password')?></span>
			<span class="quote-field-container" style="width:65%;text-align: right;"><input type="password" name="editUserInfoArr[szOldPassword]" id="szOldPassword"/></span>
		</div>
		<div class="clearfix email-fields-container">
			<span class="quote-field-container" style="width:35%;"><?=t($t_base.'fields/password');?></span>
			<span class="quote-field-container" style="width:65%;text-align: right;" ><input type="password" name="editUserInfoArr[szNewPassword]" id="szNewPassword" /></span>
			
		</div>
		
		<div class="clearfix email-fields-container">
			<span class="quote-field-container" style="width:35%;"><?=t($t_base.'fields/con_new_password')?></span>
			<span class="quote-field-container" style="width:65%;text-align: right;" ><input type="password" name="editUserInfoArr[szConPassword]" id="szConPassword"/></span>
		</div>
		<br />
		<input type='hidden' name='showflag' value="<?=$showflag;?>">
		<p align="center"><a href="javascript:void(0);" class="button1" onclick="changeForwarderPasswordNew();"><span>Save</span></a> <a href="javascript:void(0)" class="button2" onclick="cancelEditMAnageVar(0);"><span><?=t($t_base.'fields/cancel');?></span></a> </p>
	</form>
        </div></div>
 <?php  
    }
}

function manualFeePricingHtml()
{
    $t_base = "management/uploadService/"; 
    $kVatApplication = new cVatApplication(); 
    $manualFeeArr=$kVatApplication->getManualFeeListing(false);  
    ?>
    <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">
       <table id="manual_fee-table" class="format-2" cellspacing="0" cellpadding="0" border="0" width="100%">
           <tr id="table-header">
               <td style="width:30%"><strong><?=t($t_base.'fields/forwarders')?></strong></td>
               <td style="width:20%"><strong><?=t($t_base.'fields/product')?></strong></td>
               <td style="width:30%"><strong><?=t($t_base.'fields/cargo')?></strong></td>
               <td style="text-align:right;width:20%"><strong><?=t($t_base.'fields/manual_fee')?></strong></td>
           </tr>
           <?php
            $ctr = 1;
            if(!empty($manualFeeArr))
            {
                foreach($manualFeeArr as $manualFeeArrs)
                {
                    if($manualFeeArrs['szProduct']==__MANUAL_FEE_COURIER__)
                    {
                        $szProductName = 'Courier';
                    } 
                    else if($manualFeeArrs['szProduct']==__MANUAL_FEE_LCL__)
                    {
                        $szProductName = 'LCL';
                    }
                    else if($manualFeeArrs['szProduct']==__MANUAL_FEE_LTL__)
                    {
                        $szProductName = 'LTL';
                    }
                    
                 ?>
                    <tr id="manual_fee_<?php echo $ctr;?>" onclick="select_manual_fee_tr('manual_fee_<?php echo $ctr?>','<?php echo $manualFeeArrs['id']; ?>');">
                       <td style="width:30%"><?php if((int)$manualFeeArrs['idForwarder']>0){ echo $manualFeeArrs['szDisplayName']." (".$manualFeeArrs['szForwarderAlias'].")";}else { echo "All other";}?></td>
                       <td style="width:20%"><?php echo $manualFeeArrs['szProductName']; ?></td>
                       <td style="width:30%"><?=$manualFeeArrs['szName']?></td>
                       <td style="text-align:right;width:20%"><?=$manualFeeArrs['szCurrency']." ".number_format((float)$manualFeeArrs['fManualFee'],2)?></td>
                    </tr>
                 <?php $ctr++;
                }
            }
           ?>
       </table>
    </div><br />
    <div class="clear-all"></div>
    <div id="manual_fee_form_container" class="manual_fee_form_container">
        <?php echo display_add_manual_fee_form($kVatApplication); ?>
    </div>
    <?php
}

function display_add_manual_fee_form($kVatApplication,$manualFeeAry=false)
{
    $t_base = "management/uploadService/"; 
    $kVatApplication_new = new cVatApplication();
    $kForwarder = new cForwarder();
    $kConfig = new cConfig();
    $forwarderListAry = array();
    if(!empty($_POST['manualFeeArr']))
    {
        $manualFeeArr = $_POST['manualFeeArr'];
        $idForwarder = $manualFeeArr['idForwarder'];
        $szProduct = $manualFeeArr['szProduct'];
        $szCargo = $manualFeeArr['szCargo'];
        $idCurrency = $manualFeeArr['idCurrency'];
        $fManualFee = $manualFeeArr['fManualFee'];
        $idManualFee = $manualFeeArr['idManualFee'];
    }
    else if(!empty($manualFeeAry))
    {
        $idForwarder = $manualFeeAry['idForwarder'];
        if((int)$idForwarder==0)
        {
            $idForwarder="AQ";
        }
        $szProduct = $manualFeeAry['szProduct'];
        $szCargo = $manualFeeAry['szCargo'];
        $idCurrency = $manualFeeAry['idCurrency'];
        $fManualFee = round((float)$manualFeeAry['fManualFee'],2);
        $idManualFee = $manualFeeAry['id'];
    } 
    $forwarderListAry = $kForwarder->getForwarderArr(true);  
    $manualFeeCargoTypeArr=$kVatApplication_new->getManualFeeCaroTypeList();  
    $curreniesArr=$kConfig->getBookingCurrency('',true);
    
    $transportArr=$kConfig->getAllTransportMode();
?>
    <script type="text/javascript">
        $().ready(function(){
            enableManualFeeButton();
        });
    </script>
    <form id="manual_fee_form" name="manual_fee_form">
        <div class="clearfix email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-15">
               <span class="input-title"><?php echo ucfirst(t($t_base.'fields/forwarders')); ?><br></span>
               <span class="input-fields">  
                   <select id="idForwarder" name="manualFeeArr[idForwarder]" onchange="enableManualFeeButton();" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                       <option value="">Select</option>
                        <?php
                            if(!empty($forwarderListAry))
                            {
                                foreach($forwarderListAry as $forwarderListArys)
                                {
                                    ?><option value="<?php echo $forwarderListArys['id'];?>" <?php echo (($forwarderListArys['id']==$idForwarder)?'selected':''); ?>><?php echo $forwarderListArys['szDisplayName']." (".$forwarderListArys['szForwarderAlias'].")";?></option><?php 
                                }
                            }
                        ?>
                        <option value="AQ" <?php echo (('AQ'==$idForwarder)?'selected':''); ?>>All other</option>            
                  </select>
               </span>
            </span>
            <span class="quote-field-container wd-10">
              <span class="input-title"><?php echo ucfirst(t($t_base.'fields/product')); ?><br></span>
              <span class="input-fields">  
                  <select id="szProduct" name="manualFeeArr[szProduct]" onchange="enableManualFeeButton();" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                       <option value="">Select</option>
                       <?php
                       if(!empty($transportArr))
                       {
                          foreach($transportArr as $transportArrs)
                          {?>
                            <option value="<?php echo $transportArrs['id'];?>" <?php echo (($szProduct==$transportArrs['id'])?'selected':''); ?>><?php echo $transportArrs['szShortName'];?></option>
                            <?php  
                          }                         
                       }?>
<!--                       <option value="<?php echo __MANUAL_FEE_COURIER__;?>" <?php echo (($szProduct==__MANUAL_FEE_COURIER__)?'selected':''); ?>>Courier</option>
                       <option value="<?php echo __MANUAL_FEE_LCL__;?>" <?php echo (($szProduct==__MANUAL_FEE_LCL__)?'selected':''); ?>>LCL</option>
                       <option value="<?php echo __MANUAL_FEE_LTL__;?>" <?php echo (($szProduct==__MANUAL_FEE_LTL__)?'selected':''); ?>>LTL</option>-->
                   </select>
              </span>
            </span>
            <span class="quote-field-container wd-15">
                <span class="input-title"><?php echo ucfirst(t($t_base.'fields/cargo')); ?><br></span>
                <span class="input-fields">  
                  <select id="szCargo" name="manualFeeArr[szCargo]" onchange="enableManualFeeButton();" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                    <option value="">Select</option>
                     <?php
                     if(!empty($manualFeeCargoTypeArr))
                     {
                        foreach($manualFeeCargoTypeArr as $manualFeeCargoTypeArrs)
                        {?>
                            <option value="<?php echo $manualFeeCargoTypeArrs['szCode'];?>" <?php echo (($manualFeeCargoTypeArrs['szCode']==$szCargo)?'selected':''); ?>><?php echo $manualFeeCargoTypeArrs['szName'];?></option>
                        <?php
                        }
                     }
                     ?>
                    </select>
                </span>
            </span>
            <span class="quote-field-container wd-30">
              <span class="input-title"><?php echo ucfirst(t($t_base.'fields/manual_fee')); ?><br></span>
              <span class="input-fields">  
                <select id="idCurrency" name="manualFeeArr[idCurrency]" style="width:30%" onchange="enableManualFeeButton();" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                    <option value="">Select</option>
                    <?php
                    if(!empty($curreniesArr))
                    {
                        foreach($curreniesArr as $curreniesArrs)
                        {?>
                            <option value="<?php echo $curreniesArrs['id'];?>" <?php echo (($curreniesArrs['id']==$idCurrency)?'selected':''); ?>><?php echo $curreniesArrs['szCurrency'];?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
                <input id="fManualFee" name="manualFeeArr[fManualFee]" onkeyup="enableManualFeeButton();" onfocus="check_form_field_not_required(this.form.id,this.id);" type="text" style="width:60%" value="<?php echo $fManualFee?>" onblur="check_form_field_empty_standard(this.form.id,this.id);">
              </span>
            </span>
            <span class="quote-field-container wd-30" style="text-align:right;">  
                <span class="input-title">&nbsp;<br></span>
                <a id="manual_fee_add_button" class="button1" href="javascript:void(0);" style="opacity: 0.4">
                    <span id="manual_fee_add_button_span"><?php echo (($idManualFee>0)?'Save':'Add');?></span>
                </a>
                <a id="manual_fee_delete_button" class="button2" style="opacity: 0.4" href="javascript:void(0);">
                    <span>Delete</span>
                </a> 
           </span>
        </div> 
        <input id="idManualFee" name="manualFeeArr[idManualFee]" type="hidden" value="<?php echo $idManualFee?>">
    </form> 
<?php 
    if(!empty($kVatApplication->arErrorMessages))
    {  
        $formId = 'manual_fee_form'; 
        display_form_validation_error_message($formId,$kVatApplication->arErrorMessages);
    }
} 

function display_default_manual_fee_form($kVatApplication,$manualFeeAry=array())
{ 
    $t_base = "management/uploadService/"; 
    $kVatApplication_new = new cVatApplication();
    $kForwarder = new cForwarder();
    $kConfig = new cConfig();
    $forwarderListAry = array(); 
    if(!empty($_POST['manualFeeArr']))
    {
        $manualFeeArr = $_POST['manualFeeArr']; 
        $idCurrency = $manualFeeArr['idCurrency'];
        $fManualFee = $manualFeeArr['fManualFee'];
        $idManualFee = $manualFeeArr['idManualFee'];
    }
    else if(!empty($manualFeeAry))
    { 
        $idCurrency = $manualFeeAry['idCurrency'];
        $fManualFee = round((float)$manualFeeAry['fManualFee'],2);
        $idManualFee = $manualFeeAry['id'];
    } 
    $forwarderListAry = $kForwarder->getForwarderArr();  
    $manualFeeCargoTypeArr=$kVatApplication_new->getManualFeeCaroTypeList();  
    $curreniesArr=$kConfig->getBookingCurrency('',true); 
?>
    <script type="text/javascript">
        $().ready(function(){
           // enableManualFeeButton(1);
        });
    </script>
    <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
    <form id="default_manual_fee_form" name="default_manual_fee_form">
        <div class="clearfix email-fields-container" style="width:100%;">
              
            <span class="quote-field-container wd-40">
                <span class="input-title">&nbsp;<br></span>
                <span class="input-fields">  
                   If combination not defined above, default manual fee applied is
                </span>
            </span>
            <span class="quote-field-container wd-30">
              <span class="input-title">&nbsp;<br></span>
              <span class="input-fields">  
                <select id="idCurrency" name="manualFeeArr[idCurrency]" style="width:30%" onchange="enableManualFeeButton(1);" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                    <option value="">Select</option>
                    <?php
                    if(!empty($curreniesArr))
                    {
                        foreach($curreniesArr as $curreniesArrs)
                        {?>
                            <option value="<?php echo $curreniesArrs['id'];?>" <?php echo (($curreniesArrs['id']==$idCurrency)?'selected':''); ?>><?php echo $curreniesArrs['szCurrency'];?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
                <input id="fManualFee" name="manualFeeArr[fManualFee]" onkeyup="enableManualFeeButton(1);" onfocus="check_form_field_not_required(this.form.id,this.id);" type="text" style="width:60%" value="<?php echo $fManualFee?>" onblur="check_form_field_empty_standard(this.form.id,this.id);">
              </span>
            </span>
            <span class="quote-field-container wd-30" style="text-align:right;">  
                <span class="input-title">&nbsp;<br></span>
                <a id="default_manual_fee_save_button" class="button1" href="javascript:void(0);" style="opacity: 0.4">
                    <span id="default_manual_fee_save_button_span"><?php echo 'Save'; ?></span>
                </a> 
           </span>
        </div> 
        <input id="idManualFee" name="manualFeeArr[idManualFee]" type="hidden" value="<?php echo $idManualFee?>">
        <input id="iDefaultManualPricing" name="manualFeeArr[iDefaultManualPricing]" type="hidden" value="1">
    </form> 
<?php 
    if(!empty($kVatApplication->arErrorMessages))
    {  
        $formId = 'manual_fee_form'; 
        display_form_validation_error_message($formId,$kVatApplication->arErrorMessages);
    }
}
function paypalCountriesPOpup($kAdmin)
{
    $kConfig = new cConfig();
    $allCountriesArr=$kConfig->getAllCountries(true);
   // print_r($allCountriesArr);
    $t_base = "management/uploadService/";
    
   $payPalCountriesArr= $kAdmin->getListPaypalCoutries()
?>
     <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:40%;">
            <h4><strong><?=t($t_base.'title/paypal_countries');?></strong></h4> 
            <p><?php echo t($t_base.'messages/paypal_countries_msg'); ?></p>
            <div class="clearfix courier-provider courier-provider-scroll" style="max-height:200px;min-height:200px;">
                <table class="format-2" width="100%" cellspacing="0" cellpadding="0" style="width:100%;">
                    <?php
                    $alreadyAddArr=array();
                    if(!empty($payPalCountriesArr))
                    {
                        foreach($payPalCountriesArr as $payPalCountriesArrs)
                        {
                            
                            $alreadyAddArr[]=$payPalCountriesArrs['idCountry'];
                            ?>
                                <tr id="sel_paypal_<?php echo $payPalCountriesArrs['idCountry'];?>" onclick="selectPaypalCountry('<?php echo $payPalCountriesArrs['idCountry'];?>');">
                                    <td><?php echo $payPalCountriesArrs['szCountryName']?></td>
                                </tr>
                             <?php   
                        }
                    }else
                    {?>
                                <tr><td><strong><?php echo t($t_base.'messages/no_record_found'); ?></strong></td></tr>       
                       <?php 
                    }?>
                </table>
            </div><br />
            <div class="btn-container clearfix"> 
                <div style="float:left;width:50%;">
                    <select id="idCountry" name="idCountry" onchange="checkCountryAlreadyAdded(this.value);">
                        <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    $showFlag=false;
                                    if(empty($alreadyAddArr) || (!empty($alreadyAddArr) && !in_array($allCountriesArrs['id'],$alreadyAddArr)))
                                    {
                                        $showFlag=true;
                                    }
                                    if($showFlag){
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['id'];?>"><?php echo $allCountriesArrs['szCountryName'];?></option>
                                   <?php 
                                    }
                                }
                            }
                        ?>
                    </select>
                </div>
                <div style="float:right;">
                        <a href="javascript:void(0)" class="button1" id="add_button_paypal" style="opacity:0.4;"><span><?=t($t_base.'fields/add');?></span></a>
                </div>
            </div>    
            <p align="center"> <a href="javascript:void(0)" class="button1" onclick="cancelEditMAnageVar(0);"><span><?=t($t_base.'fields/ok');?></span></a> </p>
        </div>
    </div>
<?php    
}
function viewLanguageConfiguration($configLangArr,$idLangConfigType)
{
    
    ?><form name="languageConfigurationForm" id="languageConfigurationForm" method="post">

        <table width="100%" class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0">
            <?php
            if($idLangConfigType=='__TABLE_TRANSPORT_MODE__' || $idLangConfigType=='__TABLE_PALLET_TYPE__')
            {?>
            <tr>
                <td><strong>Short Name</strong></td>
                <td><strong>Long Name</strong></td>
                <?php
                if($idLangConfigType=='__TABLE_TRANSPORT_MODE__'){?>
                <td><strong>Frequency</strong></td>
                <?php }?>
            </tr>
            <?php }
            else if($idLangConfigType=='__TABLE_MONTH_NAME__')
            {?>
            <tr>
                <td><strong>Month Name</strong></td>
            </tr>
            <?php }
            else if($idLangConfigType=='__TABLE_STANDARD_CARGO_CATEGORY__')
            {?>
            <tr>
                <td><strong>Category Name</strong></td>
            </tr>
            <?php }
            else if($idLangConfigType=='__TABLE_SERVICE_TYPE__')
            {
               ?>
                <tr>
                    <td width="10%"><strong>Short Name</strong></td>
                    <td width="45%"><strong>Description For CFS</strong></td>
                    <td width="45%"><strong>Description For Airport</strong></td>
                </tr>
                <?php 
            }
            else if($idLangConfigType=='__TABLE_WEIGHT_MEASURE__' || $idLangConfigType=='__TABLE_CARGO_MEASURE__'  || $idLangConfigType=='__TABLE_TIMING_TYPE__' || $idLangConfigType=='__TABLE_ORIGIN_DESTINATION__' || $idLangConfigType=='__TABLE_SHIP_CON_GROUP__')
            {?>
            <tr>
                <td><strong>Description</strong></td>
            </tr>
            <?php }
            else if($idLangConfigType=='__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__')
            {?>
            <tr>
                <td><strong>Packing</strong></td>
                <td><strong>Single Packing</strong></td>
            </tr>
            <?php }else if($idLangConfigType=='__SITE_STANDARD_TEXT__')
            {?>
            <tr>
                <td><strong>Key</strong></td>
                <td><strong>Value</strong></td>
            </tr>
            <?php }?>
            <?php
            if(!empty($configLangArr))
            {
                
                if($idLangConfigType=='__TABLE_MONTH_NAME__')
                {
                    ksort($configLangArr);
                }
                $i=0;
                //print_r($configLangArr);
                foreach($configLangArr as $configLangArrs)
                {
                                      
                    if($idLangConfigType=='__TABLE_TRANSPORT_MODE__' || $idLangConfigType=='__TABLE_PALLET_TYPE__')
                    {
                        if($_POST['langConfigArr'][$i]['szShortName']!='')
                        {
                            $configLangArrs['szShortName']=$_POST['langConfigArr'][$i]['szShortName'];
                            $configLangArrs['szLongName']=$_POST['langConfigArr'][$i]['szLongName'];
                            if($idLangConfigType=='__TABLE_TRANSPORT_MODE__'){
                                $configLangArrs['szFrequency']=$_POST['langConfigArr'][$i]['szFrequency'];
                            }
                        }
?>
                     <tr>
                         <td>
                             <input type="hidden" name="langConfigArr[<?=$i?>][id]" id="id_<?=$i?>" value="<?php echo $configLangArrs['id'];?>" />
                             <input type="text" name="langConfigArr[<?=$i?>][szShortName]" id="szShortName_<?=$i?>" value="<?php echo $configLangArrs['szShortName'];?>" /></td>
                        <td><input type="text" name="langConfigArr[<?=$i?>][szLongName]" id="szLongName_<?=$i?>" value="<?php echo $configLangArrs['szLongName'];?>" /></td>
                        <?php
                if($idLangConfigType=='__TABLE_TRANSPORT_MODE__'){?>
                        <td><input type="text" name="langConfigArr[<?=$i?>][szFrequency]" id="szFrequency_<?=$i?>" value="<?php echo $configLangArrs['szFrequency'];?>" />
                        </td>
                <?php }?>
                    </tr>
                   <?php
                    }
                    else if($idLangConfigType=='__TABLE_STANDARD_CARGO_CATEGORY__')
                    {
                        
                        if($_POST['langConfigArr'][$i]['szCategoryName']!='')
                        {
                            $configLangArrs['szCategoryName']=$_POST['langConfigArr'][$i]['szCategoryName'];
                        }
                        ?>
                        <tr>
                        <td><input type="text" name="langConfigArr[<?=$i?>][szCategoryName]" id="szCategoryName_<?=$i?>" value="<?php echo $configLangArrs['szCategoryName'];?>" />
                        <input type="hidden" name="langConfigArr[<?=$i?>][id]" id="id_<?=$i?>" value="<?php echo $configLangArrs['id'];?>" /></td>
                        </tr>
                       <?php 
                    }
                    else if($idLangConfigType=='__TABLE_SERVICE_TYPE__')
                    {
                        
                        if($_POST['langConfigArr'][$i]['szShortName']!='')
                        {
                            $configLangArrs['szShortName']=$_POST['langConfigArr'][$i]['szShortName'];
                            $configLangArrs['szDescription']=$_POST['langConfigArr'][$i]['szDescription'];
                            $configLangArrs['szDescription1']=$_POST['langConfigArr'][$i]['szDescription1'];
                        }
                        ?>
                        <tr>
                        <td width="10%"><input type="text" name="langConfigArr[<?=$i?>][szShortName]" id="szShortName_<?=$i?>" value="<?php echo $configLangArrs['szShortName'];?>" />
                        <input type="hidden" name="langConfigArr[<?=$i?>][id]" id="id_<?=$i?>" value="<?php echo $configLangArrs['id'];?>" /></td>
                        <td width="45%"><input type="text" name="langConfigArr[<?=$i?>][szDescription]" id="szDescription_<?=$i?>" value="<?php echo $configLangArrs['szDescription'];?>" />
                        <td width="45%"><input type="text" name="langConfigArr[<?=$i?>][szDescription1]" id="szDescription1_<?=$i?>" value="<?php echo $configLangArrs['szDescription1'];?>" />    
                        </td>
                       
                        </tr>
                       <?php 
                    }
                    else if($idLangConfigType=='__TABLE_WEIGHT_MEASURE__' || $idLangConfigType=='__TABLE_CARGO_MEASURE__'  || $idLangConfigType=='__TABLE_TIMING_TYPE__' || $idLangConfigType=='__TABLE_ORIGIN_DESTINATION__' || $idLangConfigType=='__TABLE_SHIP_CON_GROUP__')
                    {
                        
                        if($_POST['langConfigArr'][$i]['szDescription']!='')
                        {
                            $configLangArrs['szDescription']=$_POST['langConfigArr'][$i]['szDescription'];
                        }
                        ?>
                        <tr>
                        <td><input type="text" name="langConfigArr[<?=$i?>][szDescription]" id="szDescription_<?=$i?>" value="<?php echo $configLangArrs['szDescription'];?>" />
                        <input type="hidden" name="langConfigArr[<?=$i?>][id]" id="id_<?=$i?>" value="<?php echo $configLangArrs['id'];?>" /></td>
                        </tr>
                       <?php 
                    }
                    else if($idLangConfigType=='__TABLE_COURIER_PROVIDER_PRODUCT_PACKING__'){
                                           
                        if($_POST['langConfigArr'][$i]['szPacking']!='')
                        {
                            $configLangArrs['szPackingSingle']=$_POST['langConfigArr'][$i]['szPackingSingle'];
                            $configLangArrs['szPacking']=$_POST['langConfigArr'][$i]['szPacking'];
                        }
                        ?>
                        <tr>
                        <td><input type="text" name="langConfigArr[<?=$i?>][szPacking]" id="szPacking_<?=$i?>" value="<?php echo $configLangArrs['szPacking'];?>" />
                        <input type="hidden" name="langConfigArr[<?=$i?>][id]" id="id_<?=$i?>" value="<?php echo $configLangArrs['id'];?>" /></td>
                        <td><input type="text" name="langConfigArr[<?=$i?>][szPackingSingle]" id="szPackingSingle_<?=$i?>" value="<?php echo $configLangArrs['szPackingSingle'];?>" /></td>
                        </tr>
                        <?php
                    }
                    else if($idLangConfigType=='__SITE_STANDARD_TEXT__')
                    {
                        ?>
                            <?php
                            $i=0;
                            $id=$configLangArrs['id'];
                        foreach($configLangArrs as $key=>$values)
                        {
                            
                            if($key!='id')
                            {
                                $keyArr=explode("_",$key);
                                
                                if($_POST['langConfigArr'][$i]['szFieldName']!='')
                                {
                                    $key=$_POST['langConfigArr'][$i]['szFieldName'];
                                    $values=$_POST['langConfigArr'][$i]['szValue'];
                                }
                            
                        ?>
                        <tr>
                            <td><input readony disabled="true" type="text" name="langConfigArr[<?=$i?>][szFieldNameKey]" id="szFieldNameKey_<?=$i?>" value="<?php echo $keyArr[0];?>" />
                            <input type="hidden" name="langConfigArr[<?=$i?>][szFieldName]" id="szFieldName_<?=$i?>" value="<?php echo $key;?>" />
                            <input type="hidden" name="langConfigArr[<?=$i?>][id]" id="id_<?=$i?>" value="<?php echo $id;?>" />
                        
                        </td>
                        <td><input type="text" name="langConfigArr[<?=$i?>][szValue]" id="szValue_<?=$i?>" value="<?php echo $values;?>" /></td>
                        </tr>
                    <?php 
                            ++$i;
                        }
                        
                        }     
                    }
                    else if($idLangConfigType=='__TABLE_MONTH_NAME__')
                    {
                        
                        $t=$i+1;                        
                        if($_POST['langConfigArr'][$i][$i]!='')
                        {
                            $configLangArrs[$t]=$_POST['langConfigArr'][$i][$i];
                        }
                    ?>
                    <tr>
                        <td><input type="text" name="langConfigArr[<?=$i?>][<?=$i?>]" id="<?=$i?>_<?=$i?>" value="<?php echo $configLangArrs[$t];?>" />
                        <input type="hidden" name="langConfigArr[<?=$i?>][id]" id="id_<?=$i?>" value="<?php echo $configLangArrs['id'];?>" /></td>
                    </tr>   
                    <?php 
                    }
                   ++$i;
                }
            }
            ?>            
        </table>
    </form>
    <?php
}

function displayDummyServiceListAddEditForm()
{ 
   $t_base = "management/uploadService/";
   $kService = new cServices();
   $dummyServiceAry = array();
   if(!empty($_POST['idPrimarySortKey']))
    {
        $szSortField = $_POST['idPrimarySortKey'];
        $szSortOrder = $_POST['szSortValue'];  
        $dummyServiceAry = $kService->getAllDummyServices($szSortField,$szSortOrder);
    }
    else
    {
        $dummyServiceAry = $kService->getAllDummyServices(); 
    } 
    
    ?>   
     <script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script> 
     <script type="text/javascript">
        $(function () {
            $('#couier_cargo_limitation_list_container').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    var idModeTransport = 0;
                    $("#couier_cargo_limitation_list_container .selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        var tr_id_ary = tr_id.split("____");
                        idModeTransport = parseInt(tr_id_ary[1]);
                        
                        if(!isNaN(idModeTransport))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                    });   
                    if(res_ary_new.length)
                    {
                        var idDummyService = $("#idDummyService").val();
                        idDummyService = parseInt(idDummyService);
                        if(idDummyService>0 && idModeTransport==idDummyService && parseInt(res_ary_new.length)==1)
                        {
                            /*
                            * If a record is already selected for edit then we don't overwrite buttons texts
                            */
                        }
                        else
                        {
                            console.log("In delete section...");
                            var szModeOfTransIds = res_ary_new.join(';'); 
                            $("#deleteDummyServiceId").attr('value',szModeOfTransIds);
                            $("#dummy_services_clear_button").unbind("click");
                            $("#dummy_services_clear_button").click(function(){deleteDummyServiceData();});
                            $("#dummy_services_clear_button").attr("style",'opacity:1'); 
                            $("#dummy_services_clear_button span").html("Delete"); 
                        } 
                    }
                    else
                    {
                        var szModeOfTransIds = '';
                        $("#deleteTruckiconId").attr('value',szModeOfTransIds); 
                        $("#dummy_services_clear_button").unbind("click");
                        $('#dummy_services_clear_button').attr('style','opacity:1');
                        $("#dummy_services_clear_button").click(function(){ clearDummyServicesForm("CLEAR_DUMMY_SERVICES_FORM"); }); 
                        $("#dummy_services_clear_button span").html("Clear"); 
                    } 
                    if(parseInt(res_ary_new.length)==1)
                    {
                        var idDummyService = $("#idDummyService").val();
                        idDummyService = parseInt(idDummyService);
                        if(idDummyService>0 && idModeTransport==idDummyService)
                        {
                            /*
                            * If a record is already selected for edit then we don't overwrite buttons texts
                            */
                        }
                        else
                        { 
                            $("#dummy_services_add_button").unbind("click");
                            $("#dummy_services_add_button").click(function(){editDummyServiceData();});
                            $("#dummy_services_add_button").attr("style",'opacity:1');
                            $("#dummy_services_add_button_span").html("Edit"); 
                            resetForm('dummy_service_form');
                        } 
                    }
                    else
                    { 
                        $("#dummy_services_add_button").unbind("click");
                        $("#dummy_services_add_button").attr("style",'opacity:0.4');
                        $("#dummy_services_add_button_span").html("ADD"); 
                    }
                }
            });
        })
    </script> 
    <div id="couier_cargo_limitation_list_container"> 
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">	
            <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" id="mode-transport-table">
                <tr id="table-header">
                  <td style="width:19%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'cnm.szName')"><strong><?php echo t($t_base.'fields/country');?></strong> <span class="moe-transport-sort-span <?php if($_POST['idPrimarySortKey']=='cnm.szName' || $_POST['idPrimarySortKey']==''){if($_POST['szSortValue']=='ASC'){?> sort-arrow-up <?php }else{?> sort-arrow-down <?php } }?>" id="moe-transport-sort-span-id-szName">&nbsp;</span></td>
                  <td style="width:19%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'dp.idTrade')"><strong><?php echo t($t_base.'fields/trade');?></strong> <span class="moe-transport-sort-span <?php if($_POST['idPrimarySortKey']=='dp.idTrade'){ if($_POST['szSortValue']=='ASC'){?> sort-arrow-up <?php }else{?> sort-arrow-down <?php } }?>" id="moe-transport-sort-span-id-idTrade">&nbsp;</span></td>
                  <td style="width:19%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'dp.idProduct')"><strong><?php echo t($t_base.'fields/product');?></strong> <span class="moe-transport-sort-span <?php if($_POST['idPrimarySortKey']=='dp.idProduct'){if($_POST['szSortValue']=='ASC'){?> sort-arrow-up <?php }else{?> sort-arrow-down <?php } }?>" id="moe-transport-sort-span-id-idProduct">&nbsp;</span></td>
                  <td style="width:19%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'dp.fPriceIncrease')"><strong><?php echo t($t_base.'fields/price_increase');?></strong> <span class="moe-transport-sort-span <?php if($_POST['idPrimarySortKey']=='dp.fPriceIncrease'){if($_POST['szSortValue']=='ASC'){?> sort-arrow-up <?php }else{?> sort-arrow-down <?php } }?>" id="moe-transport-sort-span-id-fPriceIncrease">&nbsp;</span></td>
                  <td style="width:19%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'dp.iTransitTimeIncrease')"><strong><?php echo t($t_base.'fields/transit_time_increase');?></strong> <span class="moe-transport-sort-span <?php if($_POST['idPrimarySortKey']=='dp.iTransitTimeIncrease'){if($_POST['szSortValue']=='ASC'){?> sort-arrow-up <?php }else{?> sort-arrow-down <?php } }?>" id="moe-transport-sort-span-id-iTransitTimeIncrease">&nbsp;</span></td>
                </tr>   	
        <?php
            if(!empty($dummyServiceAry))
            {
                foreach($dummyServiceAry as $dummyServiceArys)
                {
                    if($dummyServiceArys['idTrade']==__TRADE_IMPORT__)
                    {
                        $szTradeStr = "Import";
                    }
                    else
                    {
                        $szTradeStr = "Export";
                    }
?>
                    <tr id="truckin_tr____<?php echo $dummyServiceArys['id']; ?>" >
                        <td id="from_to_country_1_<?php echo $dummyServiceArys['id']?>" ><?php echo $dummyServiceArys['szCountryName']?></td>
                        <td id="from_to_country_2_<?php echo $dummyServiceArys['id']?>" ><?php echo $szTradeStr; ?></td>
                        <td id="from_to_country_3_<?php echo $dummyServiceArys['id']?>" ><?php echo $dummyServiceArys['szProductName']?></td>
                        <td id="from_to_country_4_<?php echo $dummyServiceArys['id']?>" ><?php echo number_format((float)$dummyServiceArys['fPriceIncrease'],2)."%"?></td>
                        <td id="from_to_country_4_<?php echo $dummyServiceArys['id']?>" ><?php echo $dummyServiceArys['iTransitTimeIncrease']." days"?></td>
                    </tr>
                <?php		
                } 				 
            }
            else
            {?>
                <tr>
                   <td colspan="5" align="center">
                       <h4><b><?=t($t_base.'fields/no_dummy_service_found');?></b></h4>
                   </td>
                </tr>
            <?php   }   ?>  
           </table>
        </div> 
    </div>
    <div class="clear-all"></div> 
    <div id="dummy_service_form_container" class="manual_fee_form_container">
        <?php echo display_dummy_service_form($kService); ?>
    </div>
    <?php
}
function display_dummy_service_form($kService,$dummyServiceAry=array())
{ 
    $t_base = "management/uploadService/";  
    $kForwarder = new cForwarder();
    $kConfig = new cConfig();
    $forwarderListAry = array();
    
    if(!empty($_POST['dummyServiceAry']))
    {
        $dummyServiceAry = $_POST['dummyServiceAry']; 
        $idDummyService = $dummyServiceAry['idDummyService'];
        $idCountry = $dummyServiceAry['idCountry'];
        $idTrade = $dummyServiceAry['idTrade'];
        $idProduct = $dummyServiceAry['idProduct'];
        $fPriceIncrease = $dummyServiceAry['fPriceIncrease'];
        $iTransitTimeIncrease = $dummyServiceAry['iTransitTimeIncrease'];
    }  
    else
    {
        $idDummyService = $dummyServiceAry['id'];
        $idCountry = $dummyServiceAry['idCountry'];
        $idTrade = $dummyServiceAry['idTrade'];
        $idProduct = $dummyServiceAry['idProduct']; 
        $iTransitTimeIncrease = $dummyServiceAry['iTransitTimeIncrease'];
        
        if(__float($dummyServiceAry['fPriceIncrease']))
        {
            $fPriceIncrease = round((float)$dummyServiceAry['fPriceIncrease'],2);
        }
        else if(!empty($dummyServiceAry['fPriceIncrease']))
        {
            $fPriceIncrease = round((float)$dummyServiceAry['fPriceIncrease']);
        } 
    } 
    
        
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
     
    $curreniesArr=$kConfig->getBookingCurrency('',true); 
?>
    <script type="text/javascript">
        $().ready(function(){
            enableDummyServiceButton('<?php echo $idDummyService; ?>');
            <?php if($idDummyService>0){ ?> 
                $("#dummy_services_clear_button").unbind("click");
                $('#dummy_services_clear_button').attr('style','opacity:1');
                $("#dummy_services_clear_button").click(function(){ cancelDummyServicesForm("CLEAR_DUMMY_SERVICES_FORM"); }); 
            <?php } else { ?>
                $("#dummy_services_clear_button").unbind("click");
                $('#dummy_services_clear_button').attr('style','opacity:1');
                $("#dummy_services_clear_button").click(function(){ clearDummyServicesForm("CLEAR_DUMMY_SERVICES_FORM"); }); 
            <?php } ?> 
        });
    </script>
    <form id="dummy_service_form" name="dummy_service_form">
        <div class="clearfix email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-15">
               <span class="input-title"><?php echo ucfirst(t($t_base.'fields/country')); ?><br></span>
               <span class="input-fields">  
                    <select id="idCountry" name="dummyServiceAry[idCountry]" onchange="enableDummyServiceButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?><option value="<?php echo $allCountriesArrs['id'];?>" <?php echo (($allCountriesArrs['id']==$idCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName'];?></option><?php 
                                }
                            }
                        ?>           
                   </select>
               </span>
            </span>
            <span class="quote-field-container wds-11">
              <span class="input-title"><?php echo ucfirst(t($t_base.'fields/trade')); ?><br></span>
              <span class="input-fields">  
                    <select id="idTrade" name="dummyServiceAry[idTrade]" onchange="enableDummyServiceButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                       <option value="">Select</option>
                       <option value="<?php echo __TRADE_IMPORT__;?>" <?php echo (($idTrade==__TRADE_IMPORT__)?'selected':''); ?>>Import</option>
                       <option value="<?php echo __TRADE_EXPORT__;?>" <?php echo (($idTrade==__TRADE_EXPORT__)?'selected':''); ?>>Export</option> 
                    </select>
              </span>
            </span>
            <span class="quote-field-container wds-11">
              <span class="input-title"><?php echo ucfirst(t($t_base.'fields/product')); ?><br></span>
              <span class="input-fields">  
                  <select id="idProduct" name="dummyServiceAry[idProduct]" onchange="enableDummyServiceButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                       <option value="">Select</option>
                       <option value="<?php echo __BOOKING_TRANSPORT_MODE_COURIER__;?>" <?php echo (($idProduct==__BOOKING_TRANSPORT_MODE_COURIER__)?'selected':''); ?>>Courier</option>
                       <option value="<?php echo __BOOKING_TRANSPORT_MODE_SEA__;?>" <?php echo (($idProduct==__BOOKING_TRANSPORT_MODE_SEA__)?'selected':''); ?>>LCL</option> 
                   </select>
              </span>
            </span>
            <span class="quote-field-container wds-15">
              <span class="input-title"><?php echo ucfirst(t($t_base.'fields/price_increase')); ?><br></span>
              <span class="input-fields"> 
                  <input id="fPriceIncrease" name="dummyServiceAry[fPriceIncrease]" onkeyup="enableDummyServiceButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" type="text" value="<?php echo $fPriceIncrease; ?>" onblur="check_form_field_empty_standard(this.form.id,this.id);" style="width:80%"><span style="vertical-align:middle;padding: 5px 0 0 0;">%</span>
              </span>
            </span>
            <span class="quote-field-container wds-18">
              <span class="input-title"><?php echo ucfirst(t($t_base.'fields/transit_time_increase')); ?><br></span>
              <span class="input-fields"> 
                  <input id="iTransitTimeIncrease" name="dummyServiceAry[iTransitTimeIncrease]" onkeyup="enableDummyServiceButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" type="text" style="width:66%" value="<?php echo $iTransitTimeIncrease; ?>" onblur="check_form_field_empty_standard(this.form.id,this.id);"><span style="vertical-align:middle;padding: 4px 0 0 0;">days</span>
              </span>
            </span>
            <span class="quote-field-container wd-30" style="text-align:right;">  
                <span class="input-title">&nbsp;<br></span>
                <a id="dummy_services_add_button" class="button1" href="javascript:void(0);" style="opacity: 0.4">
                    <span id="dummy_services_add_button_span"><?php echo (($idDummyService>0)?'Save':'Add');?></span>
                </a>
                <a id="dummy_services_clear_button" class="button2" style="opacity: 0.4" href="javascript:void(0);">
                    <span><?php echo (($idDummyService>0)?'Cancel':'Clear');?></span>
                </a> 
                <input id="idDummyService" name="dummyServiceAry[idDummyService]" type="hidden" style="width:66%" value="<?php echo $idDummyService; ?>">
           </span>
        </div>  
    </form> 
<?php 
    if(!empty($kService->arErrorMessages))
    {  
        $formId = 'dummy_service_form'; 
        display_form_validation_error_message($formId,$kService->arErrorMessages);
    }
}
function displayDummyServiceExceptionDetails()
{
    $t_base = "management/uploadService/";
    $kService = new cServices();
    $dummyServiceExceptionAry = array();
    if(!empty($_POST['idPrimarySortKey']))
    {
        $szSortField = $_POST['idPrimarySortKey'];
        $szSortOrder = $_POST['szSortValue'];  
        $dummyServiceExceptionAry = $kService->getAllDummyServicesException($szSortField,$szSortOrder); 
    }
    else
    {
        $dummyServiceExceptionAry = $kService->getAllDummyServicesException(); 
    } 
    
    if($szSortField=='szDestinationCoutntryName')
    {
        if($szSortOrder=='DESC')
        {
            $szDestSortClass = "sort-arrow-down";
        }
        else
        {
            $szDestSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='szProductName')
    {
        if($szSortOrder=='DESC')
        {
            $szProductSortClass = "sort-arrow-down";
        }
        else
        {
            $szProductSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='szOriginCoutntryName' || empty($szSortField))
    {
        if($szSortOrder=='DESC')
        {
            $szOrigSortOrder = "sort-arrow-down";
        }
        else
        {
            $szOrigSortOrder = "sort-arrow-up";
        }
    } 
    ?>   
    <script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script> 
    <script type="text/javascript">
        $(function () {
            $('#dummy_service_exception_div').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    $("#dummy_service_exception_div .selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        var tr_id_ary = tr_id.split("____");
                        var idModeTransport = parseInt(tr_id_ary[1]);
                        
                        if(!isNaN(idModeTransport))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                    });   
                    if(res_ary_new.length)
                    {
                        var szModeOfTransIds = res_ary_new.join(';'); 
                        $("#deleteDummyServiceExceptionId").attr('value',szModeOfTransIds);
                        $("#dummy_services_exception_add_button").unbind("click");
                        $("#dummy_services_exception_add_button").click(function(){deleteDummyServiceDataException();});
                        $("#dummy_services_exception_add_button").attr("style",'opacity:1'); 
                        $("#dummy_services_exception_add_button span").html("Delete"); 
                    }
                    else
                    {
                        var szModeOfTransIds = ''; 
                        $("#dummy_services_exception_add_button span").html("Add"); 
                        enableDummyServiceExceptionButton();
                    }  
                }
            });
        })
    </script> 
    <div id="dummy_service_exception_div"> 
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">	
            <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tr id="table-header">
                  <td style="width:33%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'szOriginCoutntryName',1)"><strong><?php echo t($t_base.'fields/from_country');?></strong> <span class="moe-transport-sort-span <?php  echo $szOrigSortOrder; ?>" id="moe-transport-sort-span-id-szName">&nbsp;</span></td>
                  <td style="width:33%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'szDestinationCoutntryName',1)"><strong><?php echo t($t_base.'fields/to_country');?></strong> <span class="moe-transport-sort-span <?php  echo $szDestSortClass; ?>" id="moe-transport-sort-span-id-idTrade">&nbsp;</span></td>
                  <td style="width:33%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'szProductName',1)"><strong><?php echo t($t_base.'fields/product');?></strong> <span class="moe-transport-sort-span <?php  echo $szProductSortClass; ?>" id="moe-transport-sort-span-id-idProduct">&nbsp;</span></td> 
                </tr>   	
        <?php
            if(!empty($dummyServiceExceptionAry))
            {
                foreach($dummyServiceExceptionAry as $dummyServiceExceptionArys)
                { 
                    ?>
                    <tr id="truckin_tr____<?php echo $dummyServiceExceptionArys['id']; ?>" >
                        <td id="from_to_country_1_<?php echo $dummyServiceArys['id']?>" ><?php echo $dummyServiceExceptionArys['szOriginCoutntryName']?></td>
                        <td id="from_to_country_2_<?php echo $dummyServiceArys['id']?>" ><?php echo $dummyServiceExceptionArys['szDestinationCoutntryName']; ?></td>
                        <td id="from_to_country_3_<?php echo $dummyServiceArys['id']?>" ><?php echo $dummyServiceExceptionArys['szProductName']?></td> 
                    </tr>
                <?php		
                } 				 
            }
            else
            {?>
                <tr>
                   <td colspan="3" align="center">
                       <h4><b><?=t($t_base.'fields/no_dummy_service_found');?></b></h4>
                   </td>
                </tr>
            <?php   }   ?>  
           </table>
        </div> 
    </div>
    <div class="clear-all"></div> 
    <div id="dummy_service_exception_form_container" class="manual_fee_form_container">
        <?php echo display_dummy_service_exception_form($kService); ?>
    </div>
    <?php
}

function display_dummy_service_exception_form($kService,$dummyServiceExceptionAry=array())
{ 
    $t_base = "management/uploadService/";  
    $kForwarder = new cForwarder();
    $kConfig = new cConfig();
    $forwarderListAry = array();
    
    if(!empty($_POST['dummyServiceExceptionAry']))
    {
        $dummyServiceExceptionAry = $_POST['dummyServiceExceptionAry']; 
        $idDummyServiceException = $dummyServiceExceptionAry['idDummyServiceException'];
        $idOriginCountry = $dummyServiceExceptionAry['idOriginCountry'];
        $idDestinationCountry = $dummyServiceExceptionAry['idDestinationCountry'];
        $idProduct = $dummyServiceExceptionAry['idProduct']; 
    }  
    else
    {
        $idDummyServiceException = $dummyServiceExceptionAry['id'];
        $idOriginCountry = $dummyServiceExceptionAry['idOriginCountry'];
        $idDestinationCountry = $dummyServiceExceptionAry['idDestinationCountry'];
        $idProduct = $dummyServiceExceptionAry['idProduct'];   
    } 
     
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage); 
?>
    <script type="text/javascript">
        $().ready(function(){
            enableDummyServiceExceptionButton();
        });
    </script>
    <form id="dummy_service_exception_form" name="dummy_service_exception_form">
        <div class="clearfix email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-20">
               <span class="input-title"><?php echo ucfirst(t($t_base.'fields/from_country')); ?><br></span>
               <span class="input-fields">  
                    <select id="idOriginCountry" name="dummyServiceExceptionAry[idOriginCountry]" onchange="enableDummyServiceExceptionButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?><option value="<?php echo $allCountriesArrs['id'];?>" <?php echo (($allCountriesArrs['id']==$idOriginCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName'];?></option><?php 
                                }
                            }
                        ?>           
                   </select>
               </span>
            </span> 
            <span class="quote-field-container wds-20">
               <span class="input-title"><?php echo ucfirst(t($t_base.'fields/to_country')); ?><br></span>
               <span class="input-fields">  
                    <select id="idDestinationCountry" name="dummyServiceExceptionAry[idDestinationCountry]" onchange="enableDummyServiceExceptionButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?><option value="<?php echo $allCountriesArrs['id'];?>" <?php echo (($allCountriesArrs['id']==$idDestinationCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName'];?></option><?php 
                                }
                            }
                        ?>           
                   </select>
               </span>
            </span>
            <span class="quote-field-container wds-20">
              <span class="input-title"><?php echo ucfirst(t($t_base.'fields/product')); ?><br></span>
              <span class="input-fields">  
                  <select id="idProduct" name="dummyServiceExceptionAry[idProduct]" onchange="enableDummyServiceExceptionButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                       <option value="">Select</option>
                       <option value="<?php echo __BOOKING_TRANSPORT_MODE_COURIER__;?>" <?php echo (($idProduct==__BOOKING_TRANSPORT_MODE_COURIER__)?'selected':''); ?>>Courier</option>
                       <option value="<?php echo __BOOKING_TRANSPORT_MODE_SEA__;?>" <?php echo (($idProduct==__BOOKING_TRANSPORT_MODE_SEA__)?'selected':''); ?>>LCL</option> 
                   </select>
              </span>
            </span>  
            <span class="quote-field-container wds-40" style="text-align:right;">  
                <span class="input-title">&nbsp;<br></span>
                <a id="dummy_services_exception_add_button" class="button1" href="javascript:void(0);" style="opacity: 0.4">
                    <span id="dummy_services_exception_add_button_span">Add</span>
                </a> 
                <input id="idDummyServiceException" name="dummyServiceExceptionAry[idDummyServiceException]" type="hidden" style="width:66%" value="<?php echo $idDummyServiceException; ?>">
           </span>
        </div>  
    </form> 
<?php 
    if($kService->arErrorMessages['szSpecialError'])
    {
        echo '<span class="red_text">'.$kService->arErrorMessages['szSpecialError']."</span>";
    }
    else if(!empty($kService->arErrorMessages))
    {  
        $formId = 'dummy_service_exception_form'; 
        display_form_validation_error_message($formId,$kService->arErrorMessages);
    }
}

function gmailInboxEmailsListing()
{
    $kGmail = new cGmail();
    $gmailInboxAry = array();
    $gmailInboxAry = $kGmail->getAllEmails(); 
?> 
    <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">	
        <table cellspacing="0" id="send_message_list" cellpadding="0" border="0" class="format-2" width="100%">
            <tr>
                <td style="text-align:left;width:16%;"><strong>Date</strong></td>
                <td style="text-align:left;width:32%;"><strong>From</strong></td>
                <td style="text-align:left;width:45%;"><strong>Subject</strong></td>
                <td style="text-align:left;width:7%;"></td>
            </tr>
            <?php
                if(!empty($gmailInboxAry))
                {
                    $i=1;
                    foreach($gmailInboxAry as $gmailInboxArys)
                    { 
                        if($gmailInboxArys['iSeen']==1)
                        {
                            $szClass = "read-email";
                        }
                        else
                        {
                            $szClass = "unread-email";
                        }  
                        $gmailInboxArys['dtReceivedOn'] = getDateCustom($gmailInboxArys['dtReceivedOn']);
                        /*
                        if(date('I')==1)
                        {  
                            //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date  
                            $gmailInboxArys['dtReceivedOn'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($gmailInboxArys['dtReceivedOn'])));  
                        }
                        else
                        { 
                            $gmailInboxArys['dtReceivedOn'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($gmailInboxArys['dtReceivedOn'])));  
                        }
                         * 
                         */
    ?>
                        <tr id="message_data_<?php echo $gmailInboxArys['id']; ?>" class="<?php echo $szClass; ?>">
                            <td><?php echo ((!empty($gmailInboxArys["dtReceivedOn"]) && $gmailInboxArys["dtReceivedOn"]!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($gmailInboxArys["dtReceivedOn"])):""); ?></td>
                            <td><?php echo $gmailInboxArys['szFromEmail']; ?></td>
                            <td>
                                <?php 
                                    echo returnLimitData($gmailInboxArys['szSubject'],50); 
                                    if(!empty($gmailInboxArys['szAttachments']))
                                    {
                                        ?>
                                        <a href="javascript:void(0)" class="btn-email-attachment" style="float:right;" onclick="preview_gmail_data('<?php echo $gmailInboxArys['id']?>','1')">&nbsp;</a>
                                        <?php
                                    }
                                ?>
                            </td>
                            <td><a href="javascript:void(0)" onclick="preview_gmail_data('<?php echo $gmailInboxArys['id']?>')">Preview</a></td>
                        </tr>
                        <?php
                        ++$i;
                    }
                }
                else
                {
                    ?>
                    <tr>
                        <td colspan="4" style="text-align: center;"><strong>No record found</strong></td>     
                    </tr>
                    <?php
                }
            ?>
        </table>
    </div>
    <br>
    <div class="clearfix">
        <a hre="javascript:void(0);" class="button1" onclick="sync_transporteca_gmail();" style="float:right;" id="sync_with_gmail_button"><span>Sync with Gmail</span></a>
    </div>
    <?php
}
function display_gmail_preview($messageDetailArr)
{  
    $attachmentAry = array(); 
    $iAttachmentCount = 0;
    if(!empty($messageDetailArr['szAttachments']))
    {
        $attachmentAry = unserialize($messageDetailArr['szAttachments']);
        $iAttachmentCount = count($attachmentAry);
    } 
?>
    <br>
    <div class="clearfix dotted-line" style="width:100%;">&nbsp;</div>
    <br>
    <div class="clearfix">
        <p>From: <?php echo $messageDetailArr['szFromEmail']; ?></p>
        <p>Subject: <?php echo $messageDetailArr['szSubject']; ?></p>
        <p><?php echo $messageDetailArr['szEmailMessage'];?></p>
        <?php
            if($iAttachmentCount>0)
            {
                echo '<br><p><strong>'.$iAttachmentCount." Attachments</strong></p><br>";
                foreach($attachmentAry as $attachmentArys)
                {
                    ?>
                    <a href="<?php echo __BASE_URL_GMAIL_ATTACHMENT__."/".$messageDetailArr['iEmailNumber']."/".$attachmentArys; ?>" target="_blank"><?php echo ++$ctr.". ".$attachmentArys; ?></a><br>
                    <?php
                }
            }
        ?>
    </div>
    <?php
}
function showCustomerAdminBookingInvoiceList($billingDetails,$idAdmin,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)
{	
$t_base="management/billings/";

        $iGrandTotal=count($billingDetails);
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
        //echo $limit;
        if($iPage==1)
        {
            $start=0;
            $end=$limit;
            if($iGrandTotal<$end)
            {
                $end=$iGrandTotal;
            }
        }
        else
        {
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            $startValue=$start+1;
            if($startValue>$iGrandTotal)
            {
                $iPage=$iPage-1;
                $start=$limit*($iPage-1);
                $end=$iPage*$limit;
                if($end>$iGrandTotal)
                {
                    $end=$iGrandTotal;
                }

            }
        }
?>
    
<form action="" id="forwarder_booking_seach_form" method="post">
	
		<div class="oh">
				
			
		<p class="fl-30">
			<span class="f-size-12"><?=t($t_base.'fields/from_date');?></span><br />
			<span id="origin_warehouse_span">
                            <input readonly="" type="text" id="datepicker1" name="bookingSearchInvoiceAry[dtFromDate]" value="<?=$_POST['bookingSearchInvoiceAry']['dtFromDate']?>">
			</span>
		</p>	
		
		<p class="fl-30">
			<span class="f-size-12"><?=t($t_base.'fields/to_date');?></span><br />
			<span id="origin_warehouse_span">
				<input readonly="" type="text" id="datepicker2" name="bookingSearchInvoiceAry[dtToDate]" value="<?=$_POST['bookingSearchInvoiceAry']['dtToDate']?>">
			</span>
		</p>	
				
			
		
		<p class="fr-40" align="right">
			<a href="javascript:void(0)" onclick="serch_forwarder_invoice_booking()" class="button1"><span><?=t($t_base.'fields/search');?></span></a>
			<a href="javascript:void(0)" onclick="clear_booking_invoice_form()" class="button2"><span><?=t($t_base.'fields/clear');?></span></a>
		</p>
	</div>	
</form>	
    <script type="text/javascript">
	function initDatePicker() 
	{
		$("#datepicker1").datepicker();
		$("#datepicker2").datepicker();
	}
	initDatePicker();
</script>
    <input type="hidden" id="page" name="page" value="<?php echo $iPage;?>">
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
		<tr>
			<td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/payment_date')?></strong> </td>
			<td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/booking_reference')?></strong></td>
			<td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/billing_invoice')?></strong> </td>
			<td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/forwarder')?></strong> </td>
			<td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/customer')?></strong> </td>
			<td width="15%" style='text-align:right;' valign="top"><strong><?=t($t_base.'fields/value')?></strong> </td>
			<td width="25%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/comment')?></strong> </td>
		</tr>
			<?php
				if($billingDetails!=array())
				{	$ctr=1;
					for($i=$start;$i<$end;++$i)
					{	
					
						$fInsurancePrice = 0;
						if($billingDetails[$i]['iInsuranceIncluded']==1)
						{
							$fInsurancePrice = $billingDetails[$i]['fTotalInsuranceCostForBookingCustomerCurrency'];
							$bills['fTotalAmount'] +=  $fInsurancePrice ;
						} 
						$dtPaymentConfirmed='';
						if($billingDetails[$i]['dtBookingConfirmed']!='' && $billingDetails[$i]['dtBookingConfirmed']!='0000-00-00 00:00:00')
						{
							$dtPaymentConfirmed=date('d/m/Y',strtotime($billingDetails[$i]['dtBookingConfirmed']));
						}
						
						if(strlen($billingDetails[$i]['szEmail'])>25)
						{
							$szEmail=substr_replace($billingDetails[$i]['szEmail'],'..',25,strlen($billingDetails[$i]['szEmail']));
						}
						else
						{
							$szEmail=$billingDetails[$i]['szEmail'];
						}
						
						$id=base64_encode($billingDetails[$i]['id']);
						
						if(!empty($billingDetails[$i]['szDisplayName']) && strlen($billingDetails[$i]['szDisplayName'])>15)
						{
							$szForwarderName = mb_substr($billingDetails[$i]['szDisplayName'],0,13,'UTF8')."..";
						}
						else
						{
							$szForwarderName = $billingDetails[$i]['szDisplayName'];
						}
                                                $szForwarderName="Transporteca";
						$mode = '';
						if($billingDetails[$i]['iDebitCredit']==6)
						{
                                                    $mode = 'CREDIT_NOTE';
                                                    $szAmountStr = "(".$billingDetails[$i]['szCurrency']." ".number_format((float)$billingDetails[$i]['fTotalAmount'],2).")";
                                                    $szInvoiceStr = $billingDetails[$i]['szInvoice'];
						}
						else
						{
                                                    $szAmountStr = $billingDetails[$i]['szCurrency']." ".number_format((float)$billingDetails[$i]['fTotalAmount'],2);
                                                    $szInvoiceStr = $billingDetails[$i]['szInvoice'];
						}
						
						?>
						<tr align='center'  id='booking_data_<?=$ctr?>' onclick=select_admin_tr('booking_data_<?=$ctr++?>','<?=$billingDetails[$i]['idBooking']?>','<?=$mode?>')>
                                                    <td style='text-align:left;'><?=$dtPaymentConfirmed?></td>
                                                    <td style='text-align:left;'><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $billingDetails[$i]['szBookingRandomNum'];?>/"><?=$billingDetails[$i]['szBookingRef']?></a></td>
                                                    <td style='text-align:left;'><?=$szInvoiceStr?></td>
                                                    <td style='text-align:left;'><?=$szForwarderName?></td>
                                                    <td style='text-align:left;'><?=$szEmail?></td>
                                                    <td style='text-align:right;'><?=$szAmountStr?></td>
                                                    <td style='text-align:left;'><span id="payment_comment_<?=$billingDetails[$i]['id']?>"><?=createSubString($billingDetails[$i]['szComment'],0,21)?></span>
                                                    <a href="javascript:void(0)" onclick="update_payment_comment('<?=$billingDetails[$i]['id']?>','<?=$billingDetails[$i]['szComment']?>','<?=$billingDetails[$i]['szBooking']?>')" id="update_comment_link_<?=$billingDetails[$i]['id']?>" style="text-decoration:none;font-size:11px;"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" alt ="Click to update" style="float: right;margin-top:-1px;"></a>
                                                    </td>
							</tr>	
								<?php
						$id='';
					}
				 }
				else
				{
					echo "<tr><td colspan='7'><CENTER><b>".t($t_base.'fields/no_record_found')."</b><CENTER></td></tr>";
				}
			?>
		</table>
		<div id="invoice_comment_div" style="display:none">
	</div>
    <div class="page_limit clearfix">
        <div class="pagination">
            <?php

                $kPagination->szMode = "DISPLAY_BOOKING_INVOICE_LIST_PAGINATION";
                //$kPagination->idBookingFile = $idBookingFile;
                $kPagination->paginate('display_booking_invoice_list_pagination'); 
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }        
            ?>
               </div>
               <div class="limit">Records per page
                   <select id="limit" name="limit" onchange="display_booking_invoice_list_pagination('1')">
                       <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                        <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                        <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                        <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                   </select>
               </div>    
            </div>
			<div id="viewInvoiceComplete" style="float: right;">
				<a class="button1" id="download_invoice" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
				<a class="button1" id="download_booking"  style="opacity:0.4;"><span><?=t($t_base.'fields/view_booking');?></span></a>
			</div>	
<?php
} 
function display_lcl_rail_icon_listing($kServices,$iSuccessFlag=false)
{  
    $kServices = new cServices(); 
    $t_base="management/railTransport/"; 
    $railTransportListAry = array();
    if(!empty($_POST['idPrimarySortKey']))
    {
        $szSortField = $_POST['idPrimarySortKey'];
        $szSortOrder = $_POST['szSortValue'];  
        $railTransportListAry = $kServices->getAllRailTransport(false,$szSortField,$szSortOrder);
    }
    else
    {
        $railTransportListAry = $kServices->getAllRailTransport(); 
    } 
    
    if($szSortField=='szFromWarehouse')
    {
        if($szSortOrder=='DESC')
        {
            $szFromWHSSortClass = "sort-arrow-down";
        }
        else
        {
            $szFromWHSSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='szToWarehouse')
    {
        if($szSortOrder=='DESC')
        {
            $szToWHSSortClass = "sort-arrow-down";
        }
        else
        {
            $szToWHSSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='iDTD')
    {
        if($szSortOrder=='DESC')
        {
            $iDTDSortClass = "sort-arrow-down";
        }
        else
        {
            $iDTDSortClass = "sort-arrow-up";
        }
    }
    else if($szSortField=='szDisplayName' || empty($szSortField))
    {
        if($szSortOrder=='DESC')
        {
            $szFwdSortOrder = "sort-arrow-down";
        }
        else
        {
            $szFwdSortOrder = "sort-arrow-up";
        }
    }  
    $counter=count($courierTrackiocnArr); 
?>  
    <script src="<?php echo __BASE_STORE_JS_URL__;?>/multiselect.js" type="text/javascript"></script> 
    <script type="text/javascript">
        $(function () {
            $('#rail_transport_container_div').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    var idModeTransport = 0;
                    $("#rail_transport_container_div .selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        var tr_id_ary = tr_id.split("____");
                        idModeTransport = parseInt(tr_id_ary[1]); 
                        if(!isNaN(idModeTransport))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                    });   
                    if(res_ary_new.length)
                    {
                        var idRailTransport = $("#idRailTransport").val();
                        idRailTransport = parseInt(idRailTransport);
                        if(idRailTransport>0 && idModeTransport==idRailTransport && parseInt(res_ary_new.length)==1)
                        {
                            /*
                            * If a record is already selected for edit then we don't overwrite buttons texts
                            */
                        }
                        else
                        {
                            //console.log("In delete section...");
                            var szModeOfTransIds = res_ary_new.join(';'); 
                            $("#railTransportIds").attr('value',szModeOfTransIds);
                            $("#dummy_services_clear_button").unbind("click");
                            $("#dummy_services_clear_button").click(function(){editRailTransportData('DELETE_RAIL_TRANSPORT');});
                            $("#dummy_services_clear_button").attr("style",'opacity:1'); 
                            $("#dummy_services_clear_button span").html("Delete"); 
                        } 
                    }
                    else
                    {
                        var szModeOfTransIds = '';
                        $("#deleteTruckiconId").attr('value',szModeOfTransIds); 
                        $("#dummy_services_clear_button").unbind("click");
                        $('#dummy_services_clear_button').attr('style','opacity:1');
                        $("#dummy_services_clear_button").click(function(){ clearRailTransportForm("CLEAR_RAIL_TRANSPORT_FORM"); }); 
                        $("#dummy_services_clear_button span").html("Clear"); 
                    } 
                    if(parseInt(res_ary_new.length)==1)
                    {
                        var idRailTransport = $("#idRailTransport").val();
                        idRailTransport = parseInt(idRailTransport);
                        if(idRailTransport>0 && idModeTransport==idRailTransport)
                        {
                            /*
                            * If a record is already selected for edit then we don't overwrite buttons texts
                            */
                        }
                        else
                        { 
                            $("#dummy_services_add_button").unbind("click");
                            $("#dummy_services_add_button").click(function(){editRailTransportData('EDIT_RAIL_TRANSPORT');});
                            $("#dummy_services_add_button").attr("style",'opacity:1');
                            $("#dummy_services_add_button_span").html("Edit"); 
                            resetForm('railTrasnportAddForm');
                        } 
                    }
                    else
                    { 
                        $("#dummy_services_add_button").unbind("click");
                        $("#dummy_services_add_button").attr("style",'opacity:0.4');
                        $("#dummy_services_add_button_span").html("ADD"); 
                    }
                }
            });
        })
    </script> 
    <!--onclick="seletTheRowForDeleteTruckicon('<?php echo $courierTrackiocnArrs['id']?>')"-->
    <div id="rail_transport_container_div"> 
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">	
          <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" id="mode-transport-table">
              <tr id="table-header">
                <td style="width:30%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'szDisplayName',2);"><strong><?php echo t($t_base.'fields/forwarder');?></strong> <span class="moe-transport-sort-span <?php echo $szFwdSortOrder; ?>" id="moe-transport-sort-span-id-szDisplayName">&nbsp;</span></td>
                <td style="width:30%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'szFromWarehouse',2);"><strong><?php echo t($t_base.'fields/from');?></strong> <span class="moe-transport-sort-span <?php echo $szFromWHSSortClass; ?>" id="moe-transport-sort-span-id-szFromWarehouse">&nbsp;</span></td>
                <td style="width:30%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'szToWarehouse',2);"><strong><?php echo t($t_base.'fields/to');?></strong> <span class="moe-transport-sort-span <?php echo $szToWHSSortClass; ?>" id="moe-transport-sort-span-id-szToWarehouse">&nbsp;</span></td> 
                <td style="width:10%;cursor:pointer" onclick="on_enter_key_press_shift_dummy_service(event,'iDTD',2);"><strong><?php echo t($t_base.'fields/dtd');?></strong> <span class="moe-transport-sort-span <?php echo $iDTDSortClass; ?>" id="moe-transport-sort-span-id-iDTD">&nbsp;</span></td> 
            </tr>   	
        <?php
            if(!empty($railTransportListAry))
            {
                foreach($railTransportListAry as $railTransportListArys)
                { 
                    ?>
                    <tr id="truckin_tr____<?php echo $railTransportListArys['idRailTransport']; ?>" >
                        <td id="from_to_country_1_<?php echo $railTransportListArys['idRailTransport']?>" ><?php echo $railTransportListArys['szDisplayName']." (".$railTransportListArys['szForwarderAlias'].") "; ?></td>
                        <td id="from_to_country_2_<?php echo $railTransportListArys['idRailTransport']?>" ><?php echo $railTransportListArys['szFromWarehouseCountry']." - ".$railTransportListArys['szFromWarehouseCity']." - ".$railTransportListArys['szFromWarehouseName']?></td>
                        <td id="from_to_country_3_<?php echo $railTransportListArys['idRailTransport']?>" ><?php echo $railTransportListArys['szToWarehouseCountry']." - ".$railTransportListArys['szToWarehouseCity']." - ".$railTransportListArys['szToWarehouseName']?></td> 
                        <td id="from_to_country_3_<?php echo $railTransportListArys['idRailTransport']?>" ><?php echo (($railTransportListArys['iDTD']==1)?'Yes':'No'); ?></td>
                    </tr>
                <?php		
                } 				 
            }
            else
            {?>
                <tr>
                   <td colspan="4" align="center">
                       <h4><b><?=t($t_base.'fields/no_courier_provider_found');?></b></h4>
                   </td>
                </tr>
            <?php   }   ?>  
           </table>
       </div> 
   </div> 
    <div class="clear-all"></div>
    <br>
   <div id="rail_transport_addedit_data">
        <?php
            echo display_lcl_rail_transport_addedit_form($kServices,array(),$iSuccessFlag);
        ?>	
    </div> 
<?php
}

function display_lcl_rail_transport_addedit_form($kServices,$railTransportAry=array(),$iSuccessFlag=false)
{
    $t_base="management/railTransport/";
    $kConfig = new cConfig();
    $kForwarder = new cForwarder();
    $kWhsSearch = new cWHSSearch();
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getAllForwarder(false,true);
     
    if(!empty($_POST['addRailTransportAry']))
    {
        $addRailTransportAry = $_POST['addRailTransportAry']; 
        $idForwarder = $addRailTransportAry['idForwarder'];
        $idFromWarehouse = $addRailTransportAry['idFromWarehouse'];
        $idToWarehouse = $addRailTransportAry['idToWarehouse'];
        $iDTD = $addRailTransportAry['iDTD'];  
        $idRailTransport = $addRailTransportAry['idRailTransport'];  
    }
    else
    {
        $idForwarder = $railTransportAry['idForwarder'];
        $idFromWarehouse = $railTransportAry['idFromWarehouse'];
        $idToWarehouse = $railTransportAry['idToWarehouse'];
        $iDTD = $railTransportAry['iDTD']; 
        $idRailTransport = $railTransportAry['idRailTransport'];
    } 
    $warehouseListAry = array();
    if($idForwarder>0)
    {
        $warehouseListAry = $kWhsSearch->getAllWareHouses('forwarder_warehouse',$idForwarder); 
    }
?>
    <script type="text/javascript">
        $().ready(function(){
            enableRailTransportButton(); 
            $("#dummy_services_clear_button").unbind("click");
            $('#dummy_services_clear_button').attr('style','opacity:1');
            $("#dummy_services_clear_button").click(function(){ clearRailTransportForm("CLEAR_RAIL_TRANSPORT_FORM"); });  
        });
    </script>
    <div id="courier_provider_form" style="width:100%;">
        <form action="javascript:void(0);" name="railTrasnportAddForm" method="post" id="railTrasnportAddForm">
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/forwarder');?></td>
                    <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/from');?></td>
                    <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/to');?></td>
                    <td class="font-12" width="10%" style="text-align:left;"><?php echo "Only ".t($t_base.'fields/dtd');?></td>
                    <td style="float:right;" width="30%"></td>
                </tr>
                <tr>
                    <td>
                        <select id="idForwarder" style="width:98%;" name="addRailTransportAry[idForwarder]" onchange="updateWarehouseDrodown(this.value);" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value=''>Select</option>
                            <?php 
                                if(!empty($forwarderListAry))
                                {
                                    foreach($forwarderListAry as $forwarderListArys)
                                    { 
                                        ?>
                                        <option  value='<?php echo $forwarderListArys['id']; ?>' <?php if($idForwarder==$forwarderListArys['id']){?> selected <?php }?>><?php echo $forwarderListArys['szDisplayName']." (".$forwarderListArys['szForwarderAlias'].") ";?></option>
                                        <?php
                                    }
                                }
                            ?> 
                        </select>
                    </td>
                    <td id="from_warehouse_container"> 
                        <?php echo display_warehouse_dropdown($warehouseListAry,'idFromWarehouse',$idFromWarehouse); ?>
                    </td>
                    <td id="to_warehouse_container">
                        <?php echo display_warehouse_dropdown($warehouseListAry,'idToWarehouse',$idToWarehouse); ?>
                    </td>
                    <td>
                        <select id="iDTD" style="width:98%;" name="addRailTransportAry[iDTD]" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onchange="enableRailTransportButton();">  
                            <option value='' <?php if($iDTD==0){?>selected<?php }?>>No</option>
                            <option value='1' <?php if($iDTD==1){?>selected<?php }?>>Yes</option>
                        </select>
                    </td>
                    <td style="float:right;">  
                        <a href="javascript:void(0);" class="button1" style="opacity:0.4" id="dummy_services_add_button"><span style="min-width:50px;" id="dummy_services_add_button_span"><?php echo (($idRailTransport>0)?t($t_base.'fields/save'):t($t_base.'fields/add')); ?></span></a>
                        <a href="javascript:void(0);" class="button2" style="opacity:1.0" id="dummy_services_clear_button" ><span>Clear</span></a> 
                        <?php
                            if($iSuccessFlag)
                            { 
                                //echo '<br><span class="green_text">Saved!</span>';
                            }
                        ?>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="addRailTransportAry[idRailTransport]" id="idRailTransport" value="<?php echo $idRailTransport; ?>">
        </form>
    </div>
    <?php 
    
    if($kServices->arErrorMessages['szSpecialError'])
    {
        echo '<span class="red_text">'.$kServices->arErrorMessages['szSpecialError']."</span>";
    }
    else if(!empty($kServices->arErrorMessages))
    {  
        $formId = 'railTrasnportAddForm'; 
        display_form_validation_error_message($formId,$kServices->arErrorMessages);
    }
}

function display_warehouse_dropdown($warehouseListAry,$szFieldName,$szFieldValue)
{
    ?>
    <select id="<?php echo $szFieldName; ?>" style="width:98%;" name="addRailTransportAry[<?php echo $szFieldName; ?>]" onchange="enableRailTransportButton();" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
        <option value=''>Select</option>
        <?php 
            if(!empty($warehouseListAry))
            {
                foreach($warehouseListAry as $warehouseListArys)
                { 
                    ?>
                    <option  value='<?php echo $warehouseListArys['id']; ?>' <?php if($szFieldValue==$warehouseListArys['id']){?> selected <?php }?>><?php echo $warehouseListArys['szCountryName']." - ".$warehouseListArys['szCity']." - ".$warehouseListArys['szWareHouseName']; ?></option>
                    <?php
                }
            }
        ?>
    </select>
    <?php
}

function display_remove_crm_popup($emailInputAry)
{ 
    if(!empty($_POST['removeCrmEmailAry']))
    {
        $szCrmEmail = $_POST['removeCrmEmailAry']['szCrmEmail'];
        $idCrmEmail = $_POST['removeCrmEmailAry']['idCrmEmail'];
        $szFromPage = $_POST['removeCrmEmailAry']['szFromPage'];
        $idBookingFile = $_POST['removeCrmEmailAry']['idBookingFile'];
    }
    else
    { 
        $kWhsSearch = new cWHSSearch(); 
        $szCrmEmail = $kWhsSearch->getManageMentVariableByDescription('__CONTACT_EMAIL__');
        
        $idCrmEmail = $emailInputAry['idCrmEmail'];
        $szFromPage = $emailInputAry['szFromPage'];
        $idBookingFile = $emailInputAry['idBookingFile']; 
    } 
?>
    <div id="popup-bg" onclick="showHide('content_body')"></div>
    <div id="popup-container">
        <div class="popup contact-popup" style="height: auto;">
            <div class="clearfix email-fields-container" style="width:100%;"> 
                <h4>This message is not for customer service</h4>
            </div>
            <div class="clearfix email-fields-container" style="width:100%;"> 
                Remove this message from system and forward it another e-mail address
            </div>  
            <form id="remove_crm_email_form" name="remove_crm_email_form" action="javascript:void(0);">
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:98%;">
                        <input type="text" name="removeCrmEmailAry[szCrmEmail]" id="szCrmEmail" placeholder="Type e-mail" value="<?php echo $szCrmEmail; ?>" onfocus="check_form_field_not_required_courier(this.form.id,this.id);" onkeyup="updateButtonText();">
                        <input type="hidden" name="removeCrmEmailAry[idCrmEmail]" id="idCrmEmail" value="<?php echo $idCrmEmail; ?>"> 
                        <input type="hidden" name="removeCrmEmailAry[szFromPage]" id="szFromPage" value="<?php echo $szFromPage; ?>">
                        <input type="hidden" name="removeCrmEmailAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">
                    </span>
                    <span class="quote-field-container red_text" style="width:1%;margin-top:4px;cursor: pointer;" onclick="emptyValue('szCrmEmail')">&nbsp;<strong>X</strong></span>
                </div> 
                <div align="center" class="btn-container">	
                    <a href="javascript:void(0);" class="button1" id="remove_crm_email_button"><span id="remove_crm_email_button_span">Forward and Remove</span></a>			 
                </div>
            </form> 
            <script type="text/javascript">
                $().ready(function(){
                    updateButtonText();
                });
            </script>
        </div>
    </div> 
    <?php
}

function languageVersionsListingView($languagesDetails)
{ 
    $t_base="management/languageConfig/";
?>
    <table cellspacing="0" cellpadding="0" border="0" class="format-4" width="99%" id="languageVersionsTable" > 
        <tr>
            <th width="15%" style="text-align:left"><span class="cursor-pointer"><?=t($t_base.'fields/english_name')?></span></th>
            <th width="15%" style="text-align:left"><span class="cursor-pointer"><?=t($t_base.'fields/language')?></span></th>
            <th width="15%" style="text-align:left"><span class="cursor-pointer"><?=t($t_base.'fields/status')?></span></th>
            <th width="25%" style="text-align:left"><span class="cursor-pointer"><?=t($t_base.'fields/domain')?></span></th> 
            <th width="30%" style="text-align:left"><span class="cursor-pointer"><?=t($t_base.'fields/comments')?></span></th>
        </tr>
        <?php
	 if(!empty($languagesDetails))
	 { 
            foreach($languagesDetails as $language)
            {
	?>
            <tr id="<?=$language['id'];?>" onclick="selectlanguageVersions('<?=$language['id'];?>')" style="cursor: pointer;">
                <td style="text-align:left"><?=$language['szName'];?></td>
                <td style="text-align:left"><?=$language['szNameLocal'];?></td>
                <td style="text-align:left"><?=($language['iDoNotShow'])?t($t_base.'fields/online'):t($t_base.'fields/offline');?></td>  
                <td style="text-align:left"><?=$language['szDomain'];?></td> 
                <td style="text-align:left"><?=$language['szComments'];?></td> 
            </tr>
<?php
            }
	}
        else
        {
            ?>
            <tr>
                <td colspan="3" style="text-align: center;padding:10px;"><strong><?php echo t($t_base.'fields/no_record_found'); ?></strong></td>
            </tr>
            <?php
        }
	?>
    </table>
    
<?php	  
}

function editLanguageVersionsForm($kConfig,$arrLangugeVesrion=array())
{
    $t_base ='management/languageConfig/';
    $szName = '' ;
    $iStatus = 0; 
    $idlanguage = 0; 
    $szComments='';
    if(!empty($arrLangugeVesrion))
    {
        $szName = $arrLangugeVesrion['szNameLocal'] ;
        $iStatus = $arrLangugeVesrion['iDoNotShow'] ; 
        $szComments = $arrLangugeVesrion['szComments'] ; 
        $idlanguage= $arrLangugeVesrion['id'] ;
        $szDomain = $arrLangugeVesrion['szDomain'];
        
    }
    
    if(!empty($kConfig->arErrorMessages))
    {
        foreach($kConfig->arErrorMessages as $errorKey=>$errorValue)
        {
            ?>
            <script type="text/javascript">
                $("#"+'<?php echo $errorKey; ?>').addClass('red_border');
            </script>
            <?php 
        }
    }
?>
    <form action="" mathod="post" id="edit_language_versions_form" name="edit_language_versions_form">
        <table width="100%" cellspacing="2" cellpadding="0">
            <tbody>
                <tr>
                    <td class="f-size-12" align="left"><?=t($t_base.'fields/language');?></td>
                    <td class="f-size-12" align="left"><?=t($t_base.'fields/status');?></td>
                    <td class="f-size-12" align="left"><?=t($t_base.'fields/domain');?></td>
                    <td class="f-size-12" align="left"><?=t($t_base.'fields/comments');?></td> 
                </tr>
            <tr>
            <td width="20%">
                <input type="text" name="arrVersions[szName]" id="szName" value="<?php echo $szName; ?>" size="25"/> 
            </td>
            <td width="13%">
                <select name="arrVersions[iDoNotShow]" id="iDoNotShow" >
                    <option value="1" <?php echo (($iStatus==1)?'selected':''); ?>><?=t($t_base.'fields/online');?></option>
                    <option value="0" <?php echo ((isset($iStatus) && $iStatus==0)?'selected':''); ?> ><?=t($t_base.'fields/offline');?></option>
                </select>
            </td>
            <td width="27%">
                <input type="text" name="arrVersions[szDomain]" id="szDomain" value="<?php echo $szDomain; ?>" /> 
            </td>
            <td width="40%"> 
                <textarea style="resize:none;height:20px;" name="arrVersions[szComments]" id="szComments"><?=$szComments?></textarea>
            </td>
            </tr>
            </tbody>
        </table>
        <input type="hidden" name="arrVersions[idlanguage]" id="idlanguage" value="<?php echo $idlanguage; ?>" />
        <div style="float: right;padding-top: 10px;"  class="btn-container">
            <a class="button2" id="cancelLanguageSubversions" style="opacity:0.4;"><span><?=t($t_base.'fields/cancel');?></span></a>
            <a class="button1" id="editLanguageSubversions" style="opacity:0.4;"><span><?php if((int)$idlanguage>0){ echo t($t_base.'fields/save');}else{ echo t($t_base.'fields/edit'); }?></span></a>
        </div>
    </form>
<?php 
}  

function showAdminChecks($bOnload=false)
{
    $t_top_nav="management/popup/message/";
    if((int)$_SESSION['admin_id']>0)
    {
        if($bOnload)
        {
            if(!empty($_SESSION['adminHeaderNoticationFlagsAry']))
            {
                $notificationFlagsAry = array();
                $notificationFlagsAry = $_SESSION['adminHeaderNoticationFlagsAry']; 
                 
                $priceAwaitingForApproval = $notificationFlagsAry['priceAwaitingForApproval'];
                $waitingForwarderDataApproval = $notificationFlagsAry['waitingForwarderDataApproval'];
                $notAcknowledged = $notificationFlagsAry['notAcknowledged'];
                $strCountry = $notificationFlagsAry['strCountry'];
                $iNumLabelSent = $notificationFlagsAry['iNumLabelSent'];
                $iNumLabelNotSent = $notificationFlagsAry['iNumLabelNotSent'];
                $iNumPendingReview = $notificationFlagsAry['iNumPendingReview'];
                $subFlag = $notificationFlagsAry['subFlag']; 
                $iPayOverDueCount = $notificationFlagsAry['iPayOverDueCount'];  
                $iCountGmailError = $notificationFlagsAry['iCountGmailError'];
                $iNumSellRateWithoutBuyRate = $notificationFlagsAry['iNumSellRateWithoutBuyRate']; 
                $gmailSyncErrorAry = $notificationFlagsAry['gmailSyncErrorAry'];
                $iTransferDueCount = $notificationFlagsAry['iTransferDueCount'];
            }
        }
        else
        {
            $kUploadBulkService = new cUploadBulkService(); 
            $kCourierServices = new cCourierServices(); 
            $kBooking = new cBooking(); 
            $kAdmin = new cAdmin(); 
            $kGmail = new cGmail();
            $kInsurance = new cInsurance();
            //$iNumSellRateWithoutBuyRate = $kInsurance->isBuyrateAvailableForSellRate();

            $iCountGmailError = false;
            $gmailSyncErrorAry = array();
            $gmailSyncErrorAry = $kGmail->getAllGmailSyncMessages();

            if(!empty($gmailSyncErrorAry))
            {
                $iCountGmailError = count($gmailSyncErrorAry);
            }

            $kAdmin->getAdminDetails($_SESSION['admin_id']);
            $billFlag=checkManagementPermission($kAdmin,"__OUTSTANDING__",2);

            if($billFlag)
            { 
                $iPayOverDueCount = $kBooking->getAllOverDuePaymentCount();
            } 
            
            $iTransferDue = checkManagementPermission($kAdmin,"__TRANSFER_DUE__",2);
            if($iTransferDue)
            { 
                $forwarderTransferAry = array();
                $forwarderTransferAry = $kAdmin->showForwarderTransferPayment();
                
                if(count($forwarderTransferAry)>0)
                {
                    $iTransferDueCount = 1;
                }
            }
            
            $countryArrVat = array();
            $getForwarderCountryArr = array();  
            $getForwarderCountryArr = $kAdmin->countries($_SESSION['admin_id']);
            
            if(!empty($getForwarderCountryArr))
            {
                foreach($getForwarderCountryArr as $getForwarderCountryArrs)
                {  
                    if((float)$getForwarderCountryArrs['fVATRate']>0 && empty($getForwarderCountryArrs['szVATRegistration']))
                    { 
                        if(!empty($countryArrVat) && !in_array($getForwarderCountryArrs['szCountryName'],$countryArrVat))
                        {  
                            $countryArrVat[] = $getForwarderCountryArrs['szCountryName'];
                        }
                        else if(empty($countryArrVat))
                        {
                            $countryArrVat[] = $getForwarderCountryArrs['szCountryName'];
                        } 
                    } 
                } 
                if(count($countryArrVat)>0)
                {
                    $strCountry = implode(", ",$countryArrVat);
                }
            }  
            $subFlag=checkManagementPermission($kAdmin,"__UPLOAD_SERVICE_SUBMITTED__",2);
            if($subFlag)
            {
                $priceAwaitingForApproval = $kUploadBulkService->findStatusOfCostApprovalCheckAdmin(__DATA_SUBMITTED__);
            }

            $workProgressFlag=checkManagementPermission($kAdmin,"__UPLOAD_SERVICE_WORK_IN_PROGRESS__",2);
            if($workProgressFlag)
            {
                $waitingForwarderDataApproval = $kUploadBulkService->findStatusOfCostApprovalCheckAdmin(__COST_APPROVED_BY_FORWARDER__);
            }

            $curr_agrrFlag=checkManagementPermission($kAdmin,"__CURRENT_AGREEMENT__",2);
            if($curr_agrrFlag)
            { 
                $iNumPendingReview = $kCourierServices->getAgreementPendingReviewCount();
            }  
            $label_n_sendFlag=checkManagementPermission($kAdmin,"__LABEL_NOT_SENT__",2);
            if($label_n_sendFlag)
            { 
                $iNumLabelNotSent = $kCourierServices->getAllNewBookingWithCourier(false,false,true,true);
            }

            $label_sendFlag=checkManagementPermission($kAdmin,"__LABEL_SENT__",2);
            if($label_sendFlag)
            { 
                $iNumLabelSent = $kCourierServices->getAllNewBookingWithCourier(0,__LABEL_SENT__,true,true);
            }
            /*
             * warning
             */

            $kBooking = new cBooking();
            $non_ackFlag=checkManagementPermission($kAdmin,"__BOOKING_NOT_ACKNOWLEDGED__",2);
            if($non_ackFlag)
            { 
                $notAcknowledged = $kBooking->bookingNotConfirmedDateMoreThanFiveDays(); 
            }  
            $notificationFlagsAry = array();
            $notificationFlagsAry['priceAwaitingForApproval'] = $priceAwaitingForApproval;
            $notificationFlagsAry['waitingForwarderDataApproval'] = $waitingForwarderDataApproval;
            $notificationFlagsAry['notAcknowledged'] = $notAcknowledged;
            $notificationFlagsAry['strCountry'] = $strCountry;
            $notificationFlagsAry['iNumLabelSent'] = $iNumLabelSent;
            $notificationFlagsAry['iNumLabelNotSent'] = $iNumLabelNotSent;
            $notificationFlagsAry['iNumPendingReview'] = $iNumPendingReview;
            $notificationFlagsAry['iCountGmailError'] = $iCountGmailError;
            $notificationFlagsAry['iNumSellRateWithoutBuyRate'] = $iNumSellRateWithoutBuyRate; 
            $notificationFlagsAry['subFlag'] = $subFlag; 
            $notificationFlagsAry['iPayOverDueCount'] = $iPayOverDueCount;  
            $notificationFlagsAry['gmailSyncErrorAry'] = $gmailSyncErrorAry;
            $notificationFlagsAry['iTransferDueCount'] = $iTransferDueCount; 
            
            unset($_SESSION['adminHeaderNoticationFlagsAry']);
            $_SESSION['adminHeaderNoticationFlagsAry'] = $notificationFlagsAry;
        } 
        
        $information  = ($priceAwaitingForApproval || $waitingForwarderDataApproval);
        $warning = ($notAcknowledged || $strCountry || $iNumLabelSent || $iNumLabelNotSent || $iNumPendingReview || $iCountGmailError || $iNumSellRateWithoutBuyRate || $iTransferDueCount);
            
        if( $information || $warning  || $iPayOverDueCount)
        {
            if(isset($_SESSION['message']))
            {
                $message = $_SESSION['message'];
                $style = 'style="display:none;"';
                ?>
                <script type="text/javascript">
                    $(document).ready(function()
                    { 
                        var a= $('#message').html(); 
                        if(a=='Show less')
                        {
                            $('#message').html('Show more');
                        }
                        else
                        { 
                            $('#message').html('Show less'); 
                        }

                    }); 
                </script>
                <?php
            }
            $count=0;
            ?>
            <div  class="show_message">
                <?php if($warning) { ?> 
                    <?php if($notAcknowledged) { ?>
                        <div class="warning" id="NONACKNOWLEDGE">
                            <p><?=t($t_top_nav.'message3');?> <a href="<?=__MANAGEMENT_OPERATIONS__NON_ACKNOWLEDGE_URL__?>"><?=t($t_top_nav.'here');?></a>  </p>
                        </div>
                    <?php } if($strCountry) { ?>
                        <div class="warning">
                            <p><?=t($t_top_nav.'message6');?> <?php echo $strCountry;?>  <a href="<?=__MANAGEMENT_COUNTRY_URL__?>"><?=t($t_top_nav.'here');?></a> </p>
                        </div>
                    <?php } if($iNumLabelSent>0) {  ?>
                            <div class="warning" id="LABELSENT">
                                <p>There are labels sent to shipper more than 1 day ago, not yet downloaded <a href="<?=__MANAGEMENT_OPRERATION_COUIER_LABEL_SEND__?>"><?=t($t_top_nav.'here');?></a> </p>
                            </div>
                    <?php  } if($iNumLabelNotSent>0) { 
                             if($iNumLabelNotSent==1)
                             {
                                 $iNumLabelNotSentText =$iNumLabelNotSent." booking";
                             }
                             else
                             {
                                 $iNumLabelNotSentText =number_format((int)$iNumLabelNotSent)." bookings";
                             }
                            
                            ?>
                            <div class="warning" id="LABELNOTSENT">
                                <p>There are <?=$iNumLabelNotSentText;?> more than 24 hours old, requiring labels <a href="<?php echo __MANAGEMENT_OPRERATION_COUIER_LABEL_NOT_SEND__?>"><?=t($t_top_nav.'here');?></a></p>
                            </div>
                    <?php  } if($iNumPendingReview>0){ ?>
                        <div class="warning">
                            <p>There are courier agreements pending review <a href="<?php echo __MANAGEMENT_OPRERATION_COUIER_AGREEMENT__?>"><?=t($t_top_nav.'here');?></a></p>
                        </div>
                    <?php  } if($iNumSellRateWithoutBuyRate>0) { ?>
                        <div class="warning">
                            <p>Update insurance sales rates <a href="<?php echo __MANAGEMENT_SERVICES_INSURANCE_RATES__?>"><?=t($t_top_nav.'here');?></a></p>
                        </div>
                    <?php } if($iTransferDueCount){ ?>
                        <div class="warning">
                            <p>There are forwarder payments needing to be scheduled <a href="<?php echo __MANAGEMENT_FORWARDER_TRANSFER_URL__?>"><?=t($t_top_nav.'here');?></a></p>
                        </div>
                    <?php }?>
                    <?php 
                        if(!empty($gmailSyncErrorAry))
                        {
                            echo displayGmailSyncErrorMessage($gmailSyncErrorAry);
                        }
                    ?>
                <?php  } if($information){ ?>
                        <div class="information"> 
                            <?php if($waitingForwarderDataApproval) { $count++; ?>
                                <p><?=t($t_top_nav.'message1');?> <a href="<?=__MANAGEMENT_WORK_IN_PROGRESS_URL__?>"><?=t($t_top_nav.'here');?></a>  </p>
                            <?php } ?>
                            <?php if($priceAwaitingForApproval && $subFlag){ 
                                $count++; 
                                if( $priceAwaitingForApproval && $waitingForwarderDataApproval) { ?>
                                    <div id="foo" <?=$style?>>
                            <?php } ?>
                                <p><?=t($t_top_nav.'message2');?> <a href="<?=__MANAGEMENT_SERVICES_SUBMITTED_URL__?>"><?=t($t_top_nav.'here');?></a>  </p>
                                <?php if( $priceAwaitingForApproval && $waitingForwarderDataApproval ){ ?>
                                </div>
                            <?php } } ?>
                        </div>
                <?php } if($iPayOverDueCount>0){?>
                        <div class="information" id="OUTSTANDINGPAYMENT"><?=t($t_top_nav.'message4');?> <?php echo $iPayOverDueCount;  ?> <?=t($t_top_nav.'message5');?> <a href="<?=__MANAGEMENT_BILLING_URL__?>"><?=t($t_top_nav.'here');?></a></div>
                <?php } if($count>1){?>		
                    <div class="show-more-less-alert" onclick="toggleMe();"><a id="message" href="javascript:void(0);"><?=t($t_top_nav.'show_less');?></a></div>
                <?php } ?> 
                </div>
                <?php
            }
        }
}

function viewLanguageConfigurationMeta($configLangArr,$idLanguage)
{
    //print_r($configLangArr);
    ?><form name="languageConfigurationForm" id="languageConfigurationForm" method="post">

        <table width="100%" class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0">
            <?php
            if(!empty($configLangArr))
            {
               foreach($configLangArr as $configLangArrs)
               {
                   
                   foreach ($configLangArrs as $key=>$values)
                   {
                       if($key=='id')
                       {
                            $idValue=$values;
                       }
                       if($key!='id')
                       {
                           if(!empty($_POST['langConfigArr']))
                           {
                                $values=$_POST['langConfigArr'][$key][$idValue];
                           }
                    ?>
                    <tr>
                        <td valign="top"><strong><?php echo $key;?></strong></td>
                        <td><textarea style="resize:none;" name="langConfigArr[<?=$key?>][<?=$idValue?>]" id="<?=$key?>_<?=$idValue?>" ><?php echo $values;?></textarea></td>
                    </tr>
               <?php
                       }
                   }
               }
            }?>
        </table>
        <input type="hidden" value="<?php echo $idLanguage;?>" id="idLanguage" name="langConfigArr[idLanguage]" />
    </form>
<?php
}


function searchNotificationAddEditPrivateCustomerForm($kExplain,$privateNotificationAry=array())
{ 
    $t_base = "management/uploadService/";
    $disabled="disabled='true'";
    $disabled_continue = "disabled='disabled'";
    $disabled_goto = $disabled_continue;
    if(!empty($_POST['addEditSearchNotificationAry']))
    {
        $addEditSearchNotificationAry = $_POST['addEditSearchNotificationAry'];
        
        $idButtonType = $addEditSearchNotificationAry['idButtonType'];
        $iLanguage = $addEditSearchNotificationAry['iLanguage']; 
        $szHeading = $addEditSearchNotificationAry['szHeading'];
        $szMessage = $addEditSearchNotificationAry['szMessage'];
        $szContinueButtonText = $addEditSearchNotificationAry['szContinueButtonText'];
        $szContinue2ButtonText = $addEditSearchNotificationAry['szContinue2ButtonText'];
        $szContinue3ButtonText = $addEditSearchNotificationAry['szContinue3ButtonText'];
        $szButtonText = $addEditSearchNotificationAry['szButtonText']; 
        $szButtonUrl = $addEditSearchNotificationAry['szButtonUrl']; 
        $szButtonTextRfq = $addEditSearchNotificationAry['szButtonTextRfq']; 
    }
    else
    { 
        if(!empty($privateNotificationAry['idButtonType']))
        {
            $idButtonType = explode(";",$privateNotificationAry['idButtonType']);
        } 
        $iLanguage = $privateNotificationAry['iLanguage'];
        $szHeading = $privateNotificationAry['szHeading']; 
        $szMessage = $privateNotificationAry['szMessage'];
        $szContinueButtonText = $privateNotificationAry['szContinueButtonText'];
        $szButtonTextRfq = $privateNotificationAry['szButtonTextRfq'];
        $szContinue3ButtonText = $privateNotificationAry['szContinue3ButtonText'];
        $szButtonText = $privateNotificationAry['szButtonText'];
        $szButtonUrl = $privateNotificationAry['szButtonUrl']; 
        $idSearchNotification = $privateNotificationAry['id']; 
    }
    
    if(!empty($idButtonType) && in_array(__GO_TO_RFQ_BUTTON__, $idButtonType))
    {
        $disabled='';
    }
    if(!empty($idButtonType) && in_array(__GO_TO_BUTTON__, $idButtonType))
    {
        $disabled_goto='';
    }
    
    if(!empty($idButtonType) &&  in_array(__CONTINUE_BUTTON__, $idButtonType))
    {
        $disabled_continue = '';
    } 
    $kConfig = new cConfig();
    $langArr=$kConfig->getLanguageDetails();
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:430px;"> 
            <h3>Setup search notification for private customers</h3> <br/> 
            <form name="addSearchNotificationForm" id="addSearchNotificationForm" method="post" action="javascription:void(0);"> 
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/language'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addEditSearchNotificationAry[iLanguage]" id="iLanguage" onchange="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value="">Select</option>
                            <?php
                            if(!empty($langArr))
                            {
                                foreach($langArr as $langArrs)
                                {?>
                                        <option value="<?php echo $langArrs['id'];?>" <?php echo (($langArrs['id']==$iLanguage)?'selected':''); ?>><?php echo ucwords($langArrs['szName']);?></option>
                                   <?php 
                                }
                            }?> 
                        </select>
                    </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <p style="margin-top:10px;"><?php echo t($t_base.'fields/popup_heading'); ?></p>
                    <span class="quote-field-container" style="width:100%;">
                        <input type="text" name="addEditSearchNotificationAry[szHeading]" id="szHeading" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');" value="<?php echo $szHeading;?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:100%;"><?php echo htmlentities(t($t_base.'fields/popup_message_text')); ?><br/>
                        <textarea style="height:100px;resize:none;" name="addEditSearchNotificationAry[szMessage]" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');" id="szMessage" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" ><?php echo $szMessage;?></textarea>
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:100%;" id="ButtonDivId">
                        <div><span class="checkbox-container"><input type="checkbox" <?php if(!empty($idButtonType) && in_array(__CLOSE_BUTTON__, $idButtonType)){ ?> checked <?php }?> name="addEditSearchNotificationAry[idButtonType][]" id="idButtonType1" onclick="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');" value="1" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />&nbsp;Close</span></div>
                        <div class="clearfix setup-search-row"><span class="checkbox-container"><input type="checkbox" <?php if(!empty($idButtonType) && in_array(__CONTINUE_BUTTON__, $idButtonType)){ ?>checked <?php } ?> name="addEditSearchNotificationAry[idButtonType][]" id="idButtonType2" onclick="enableTextUrlField('<?php echo __CONTINUE_BUTTON__; ?>');enableSaveButtonForSearchNotification('addSearchNotificationForm','1');" value="<?php echo __CONTINUE_BUTTON__; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />&nbsp;Continue</span>&nbsp;<span class="text-input wds-60"><input type="text" <?php echo $disabled_continue;?> name="addEditSearchNotificationAry[szContinueButtonText]" placeholder="Text" id="szContinueButtonText" value="<?php echo $szContinueButtonText;?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');"/> </span></div>
                        <div class="clearfix setup-search-row"><span class="checkbox-container"><input type="checkbox" <?php if(!empty($idButtonType) && in_array(__GO_TO_BUTTON__, $idButtonType)){ ?>checked<?php } ?> name="addEditSearchNotificationAry[idButtonType][]" id="idButtonType<?php echo __GO_TO_BUTTON__; ?>" onclick="enableTextUrlField();enableSaveButtonForSearchNotification('addSearchNotificationForm','1');" value="<?php echo __GO_TO_BUTTON__; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />&nbsp;Go to</span>&nbsp;<span class="text-input"><input type="text" <?php echo $disabled_goto;?> name="addEditSearchNotificationAry[szButtonText]" placeholder="Text" id="szButtonText" value="<?php echo $szButtonText;?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');"/></span><span class="url-input wds-36-5" style="float:left;"><input type="text" <?php echo $disabled_goto;?> name="addEditSearchNotificationAry[szButtonUrl]" placeholder="Url" id="szButtonUrl" value="<?php echo $szButtonUrl;?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');"/></span></div>   
                        <div class="clearfix setup-search-row"><span class="checkbox-container"><input type="checkbox" <?php if(!empty($idButtonType) && in_array(__GO_TO_RFQ_BUTTON__, $idButtonType)){ ?>checked<?php } ?> name="addEditSearchNotificationAry[idButtonType][]" id="idButtonType<?php echo __GO_TO_RFQ_BUTTON__; ?>" onclick="enableTextUrlField();enableSaveButtonForSearchNotification('addSearchNotificationForm','1');" value="<?php echo __GO_TO_RFQ_BUTTON__; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" />&nbsp;Go to RFQ</span>&nbsp;<span class="text-input wds-60"><input type="text" <?php echo $disabled;?> name="addEditSearchNotificationAry[szButtonTextRfq]" placeholder="Text" id="szButtonTextRfq" value="<?php echo $szButtonTextRfq;?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enableSaveButtonForSearchNotification('addSearchNotificationForm','1');"/></span></div>
                    </span>
                </div> 
                <div class="btn-container">
                    <p align="center">
                        <input type="hidden" name="addEditSearchNotificationAry[idSearchNotification]" id="idSearchNotification" value="<?php echo $idSearchNotification;?>" />
                        <a href="javascript:void(0)" id="save-button" style="opacity:0.4" class="button1" ><span><?=t($t_base.'fields/save');?></span></a>
                        <a href="javascript:void(0)" id="cancel-button" class="button2" onclick="closeTip('search_noftification_popup')"><span><?=t($t_base.'fields/close');?></span></a>
                    </p>
                    </div>
            </form>
        </div>
    </div>
     <?php
    if(!empty($kExplain->arErrorMessages))
    {  
        $formId = 'addSearchNotificationForm'; 
        display_form_validation_error_message($formId,$kExplain->arErrorMessages);
    }
} 
function searchNotificationListingPrivateCustomerHtml()
{
    $kExplain = new cExplain();
    $searchNotificationArr=$kExplain->getSearchNotificationListingPrivateCustomer();
    $t_base = "management/uploadService/";
?> 
    <div  class="clearfix courier-provider courier-provider-scroll" style="max-height:620px;">
        <table id="private-customer-search-notification" class="format-2" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr id="table-header"> 
                <td style="text-align:left;width:50%"><strong><?=t($t_base.'fields/language')?></strong></td>
                <td style="text-align:left;width:50%"><strong><?=t($t_base.'fields/user_options')?></strong></td>
            </tr>
            <?php
            if(!empty($searchNotificationArr))
            {
                foreach($searchNotificationArr as $searchNotificationArrs)
                {  
                    $kConfig =new cConfig();
                    $langArr=$kConfig->getLanguageDetails('',$searchNotificationArrs['iLanguage']);
                    $szLanguage=ucwords($langArr[0]['szName']);
                    
                    $userOptionStr='';
                   
                    if($searchNotificationArrs['idButtonType']!="")
                    {
                        $idButtonTypeArr=explode(";",$searchNotificationArrs['idButtonType']);
                       //print_r($idButtonTypeArr);
                        if(!empty($idButtonTypeArr) && in_array(__CLOSE_BUTTON__, $idButtonTypeArr))
                        {
                            $userOptionStr=__CLOSE_TEXT_BUTTON__;
                        }
                        
                        if(!empty($idButtonTypeArr) && in_array(__CONTINUE_BUTTON__, $idButtonTypeArr))
                        {
                            if($userOptionStr!='')
                                $userOptionStr .=", ".__CONTINUE_TEXT_BUTTON__;
                           else
                               $userOptionStr .=__CONTINUE_TEXT_BUTTON__;
                        }
                        if(!empty($idButtonTypeArr) && in_array(__GO_TO_BUTTON__, $idButtonTypeArr))
                        {
                            if($userOptionStr!='')
                                $userOptionStr .=", ".__GO_TO_TEXT_BUTTON__;
                           else
                               $userOptionStr .=__GO_TO_TEXT_BUTTON__;
                        } 
                        
                        if(!empty($idButtonTypeArr) && in_array(__GO_TO_RFQ_BUTTON__, $idButtonTypeArr))
                        {
                            if($userOptionStr!='')
                                $userOptionStr .=", ".__GO_TO_RFQ_TEXT_BUTTON__;
                           else
                               $userOptionStr .=__GO_TO_RFQ_TEXT_BUTTON__;
                        }
                    }  
                ?>
                <tr id="search_private_customer_notification_<?php echo $searchNotificationArrs['id'];?>" onclick="selectSearchNotificationRow('<?php echo $searchNotificationArrs['id'];?>','1');"> 
                    <td><?php echo $szLanguage;?></td>
                    <td><?php echo $userOptionStr;?></td>
                </tr>
                <?php
                }
            }
            else
            {?>
            <tr><td colspan="2" style="text-align:center;"><strong><?php echo t($t_base.'messages/no_record_found'); ?></strong></td></tr>
            
            <?php
            }
            ?>
        </table>
    </div>
    <div class="btn-container clearfix">
        <p style="text-align:right;">
            <a id="search_noti_pc_addedit" class="button1" href="javascript:void(0);" onclick="openAddEditSearchNotificationForm('','OPEN_ADD_SEARCH_NOTIFICTION_FORM_PRIVATE_CUSTOMER');">
                <span>Add</span>
            </a>
            <a id="search_noti_pc_delete" class="button2" style="opacity: 0.4" href="javascript:void(0);">
                <span>Delete</span>
            </a>
        </p>
    </div>
   <?php 
}

function forwarderNewContactForm()
{
    $kAdmin = new cAdmin();
    $forwarderArr=$kAdmin->getAllForwarderNameList();
    
    $kConfig = new cConfig();
    $allCountriesArr=$kConfig->getAllCountries(true);
?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:430px;"> 
            <h3>New forwarder contact</h3> <br/> 
            <form name="addNewForwarderContactForm" id="addNewForwarderContactForm" method="post" action="javascription:void(0);"> 
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">Forwarder</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addContactOnlyForwarderArr[idForwarder]" id="idForwarder" onchange="enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');changeCountryAfterSelectingForwarder(this.value);"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value="">Select</option>
                            <?php
                            if(!empty($forwarderArr))
                            {
                                foreach($forwarderArr as $forwarderArrs)
                                {
                                    $countryForwarder=$forwarderArrs['id']."-".$forwarderArrs['idCountry'];
                                    if($countryForwarderStr!='')
                                    {
                                        $countryForwarderStr .=";".$countryForwarder;
                                    }
                                    else
                                    {
                                        $countryForwarderStr = $countryForwarder;
                                    }
                                
                                ?>
                                        <option value="<?php echo $forwarderArrs['id'];?>" <?php echo (($forwarderArrs['id']==$_POST['addContactOnlyForwarderArr']['idForwarder'])?'selected':''); ?>><?php echo $forwarderArrs['szDisplayName']." (".$forwarderArrs['szForwarderAlias'].")";?></option>
                                   <?php 
                                }
                            }?> 
                        </select>
                    </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">First Name</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addContactOnlyForwarderArr[szFirstName]" id="szFirstName" value="<?php echo $_POST['addContactOnlyForwarderArr']['szFirstName'];?>" onkeyup="enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');" />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">Last Name</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addContactOnlyForwarderArr[szLastName]" id="szLastName" value="<?php echo $_POST['addContactOnlyForwarderArr']['szLastName'];?>" onkeyup="enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');" />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">Responsibility</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addContactOnlyForwarderArr[szResponsibility]" id="szResponsibility" value="<?php echo $_POST['addContactOnlyForwarderArr']['szResponsibility'];?>" onkeyup="enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');"  />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">E-mail</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addContactOnlyForwarderArr[szEmail]" id="szEmail" value="<?php echo $_POST['addContactOnlyForwarderArr']['szEmail'];?>" onkeyup="enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"/>
                     </span>
                </div>
               
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">Phone</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addContactOnlyForwarderArr[szPhone]" id="szPhone" value="<?php echo $_POST['addContactOnlyForwarderArr']['szPhone'];?>" onkeyup="enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');" />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">Mobile phone</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <input type="text" name="addContactOnlyForwarderArr[szMobile]" id="szMobile" value="<?php echo $_POST['addContactOnlyForwarderArr']['szMobile'];?>" onkeyup="enableSaveButtonForContactOnlyForwarderProfile('addNewForwarderContactForm');"   />
                     </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;">Country</span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addContactOnlyForwarderArr[idCountry]" id="idCountry">
                            <option value="">Select</option>
                            <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {?>
                                        <option value="<?php echo $allCountriesArrs['id'];?>" <?php echo (($allCountriesArrs['id']==$_POST['addContactOnlyForwarderArr']['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName'];?></option>
                                   <?php 
                                }
                            }?> 
                        </select>
                    </span>
                </div>
                <div class="btn-container">
                    <p align="center">
                        <input id="szSearchStr" name="addContactOnlyForwarderArr[szSearchStr]" value="<?php echo $_POST['addContactOnlyForwarderArr']['szSearchStr'];?>" type="hidden">
                        <input id="szFlag" name="addContactOnlyForwarderArr[szFlag]" value="<?php echo $_POST['addContactOnlyForwarderArr']['szFlag'];?>" type="hidden">
                        <input id="szOperationType" name="addContactOnlyForwarderArr[szOperationType]" value="<?php echo $_POST['addContactOnlyForwarderArr']['szOperationType'];?>" type="hidden">
                        <input id="idCrmEmail" name="addContactOnlyForwarderArr[idCrmEmail]" value="<?php echo $_POST['addContactOnlyForwarderArr']['idCrmEmail'];?>" type="hidden">
                        <input id="szPhoneUpdate" name="addContactOnlyForwarderArr[szPhoneUpdate]" value="<?php echo $_POST['addContactOnlyForwarderArr']['szPhoneUpdate'];?>" type="hidden">
                        <input id="szMobileUpdate" name="addContactOnlyForwarderArr[szMobileUpdate]" value="<?php echo $_POST['addContactOnlyForwarderArr']['szMobileUpdate'];?>" type="hidden">
                        <input type="hidden" name="addContactOnlyForwarderArr[idForwarderContact]" id="idForwarderContact" value="<?php echo $_POST['addContactOnlyForwarderArr']['idForwarderContact'];?>" />
                        <a href="javascript:void(0)" id="save-button" style="opacity:0.4" class="button1" ><span><?php if($_POST['addContactOnlyForwarderArr']['idForwarderContact']>0){?>Save <?php }else{?> Add<?php }?></span></a>
                        <?php if($_POST['addContactOnlyForwarderArr']['szFlag']=='FROM_HEADER'){?>
                        <a href="javascript:void(0)" id="cancel-button" class="button2" onclick="display_remind_pane_add_popup('','','','','FROM_HEADER','<?php echo $_POST['addContactOnlyForwarderArr']['szSearchStr'];?>');"><span>Cancel</span></a>
                        <?php } else{?>
                        <a href="javascript:void(0)" id="cancel-button" class="button2" onclick="closeTip('contactPopup')"><span>Cancel</span></a>
                        <?php }?>
                    </p>
                    </div>
            </form>
        </div>
    </div>
    <script>
        var countryForwarderStr ='<?php echo $countryForwarderStr;?>';
    </script>
<?php 
}

function format_volume_value($fVolume)
{
    if($fVolume!='')
    {
        $new_fVolume=round((float)$fVolume,4);

        $volumeAry = explode(".",$new_fVolume);
        if($volumeAry[0]>99)
        {
            $new_fVolume = round((float)$fVolume);
        }
        else if($volumeAry[0]>9 && $volumeAry[0]<=99)
        { 
            $new_fVolume = round((float)$fVolume,1);
        }
        else
        {
            if($volumeAry[0]<1)
            {
                $iDecimalVal=substr($volumeAry[1],0,3);
                if($iDecimalVal=='000')
                {
                    $new_fVolume = round((float)$fVolume,4);
                }
                else
                {
                    $iDecimalVal=substr($volumeAry[1],0,2);
                    if($iDecimalVal=='00')
                    {
                        $new_fVolume = round((float)$fVolume,3);
                    }
                    else
                    {
                        $new_fVolume = round((float)$fVolume,2);
                    }
                }
            }
            else
            {

                $new_fVolume = round((float)$fVolume,2);
            }
        }

        return $new_fVolume;
    }
}
function addNewForwarder($t_base)
{
    ?>
    <script type="text/javascript"> 
        $().ready(function() {	 
            $("#datepicker2").datepicker();
            checkLimit('szCommentText');
        });
    </script>
    <div id="popup-bg"></div>
    <div id="popup-container" style="margin-top: -41px;">  
            <div class="popup" style="width: 650px;">
                <div class="clearfix courier-provider courier-provider-scroll" style="max-height:550px;border:none;">
            <div id="Error"></div>			
				<h5><strong><?=t($t_base.'title/create_new_forwardar');?></strong></h5>	
					<form name="addNewForwarderProfile" id="addNewForwarderProfile" method="post" onload="setFocus()">
					<label class="ui-fields">
						<span class="field-name">
							<?=t($t_base.'fields/email');?> <?=t($t_base.'fields/address');?>
						</span>							
						<span class="field-container" style="width: 417px;">
							<input type="text" id="email"  name="createArr[szEmail]" />
						</span>						
					</label>
					<label class="ui-fields">
						<span class="field-name">
							<?=t($t_base.'fields/url');?> (<?=t($t_base.'fields/dns_update_req');?>)
						</span>								
						<span class="field-container">
							<input type="text" name="createArr[szControlPanelUrl]" style="width: 150px;">.transporteca.com
						</span>
					</label>
					<label class="ui-fields">
						<span class="field-name">
							<?=t($t_base.'fields/codeForBooking');?>
						</span>							
						<span class="field-container" >
							<input type="text" name="createArr[code]" style="width: 100px;"> (2 capital letters)
						</span>
					</label>
					<label class="ui-fields">
                                            <span class="field-name"><?=t($t_base.'fields/referalFee');?></span>
                                            <!--<span class="field-container"><input style="width:58px;" type="text" name="createArr[RefFee]" value='<?=$forwarderCP2['fReferalFee']?>'> <span style="font-style: italic;"><?=t($t_base.'fields/pctg_of_invoice_value');?></span></span>-->
					</label>
                                        <?php echo displayForwarderReferralFeeDetails($idForwarder,false,true); ?>
                                        <label class="ui-fields">
                                            <span class="field-name"><?=t($t_base.'fields/credit_offered_transporteca');?></span>
                                            <span class="field-container"><input type="text" style="width: 100px;" name="createArr[iCreditDays]" value='<?php echo $forwarderCP2['iCreditDays']; ?>'> &nbsp;days</span>
					</label>
					<label class="ui-fields">
                                            <span class="field-name"><?=t($t_base.'fields/validto');?></span>
                                            <span class="field-container"><input type="text" style="width: 100px;" id="datepicker2" onblur="show_me(this.id,'dd/mm/yyyy')" onfocus="blank_me(this.id,'dd/mm/yyyy')" name="createArr[dtValidTo]" value='<?=date('d/m/Y',strtotime(date("Y-m-d").'+ 1 year' ))?>'></span>
					</label>
                                        <label class="ui-fields">
                                            <span class="field-name"><?=t($t_base.'fields/available_formmanual_quotes');?></span>
                                            <span class="field-container" style="width:50%;" >
                                                <select name="createArr[iAvailableForManualQuotes]" id="iAvailableForManualQuotes"> 
                                                    <option value="1"><?=t($t_base.'fields/yes');?></option>
                                                    <option value="0"><?=t($t_base.'fields/no');?></option> 
                                                </select>
                                            </span>
                                        </label>
                                        <label class="ui-fields">
                                            <span class="field-name"><?=t($t_base.'fields/rfq_response');?></span>
                                            <span class="field-container" style="width:50%;" >
                                                <select name="createArr[iOffLineQuotes]" id="iOffLineQuotes"> 
                                                    <option value="0" <?php echo (((int)$iOffLineQuotes==0)?'selected':''); ?>><?php echo __RFQ_RESPONSE_IN_SYSTEM__;?></option>
                                                    <option value="1" <?php echo (((int)$iOffLineQuotes==1)?'selected':''); ?>><?php echo __RFQ_RESPONSE_BY_EMAIL__;?></option>
                                                </select>
                                            </span>
                                        </label>
					<label class="ui-fields">
                                            <span class="field-name"><?=t($t_base.'fields/automatic_pay');?></span>
                                            <span class="field-container" style="width:50%;" >
						<select name="createArr[iAutomaticPaymentInvoicing]">
                                                    <option value="1" >Yes</option>
                                                    <option value="0" >No</option>
						</select>
                                            </span>
                                            <br/>
					</label> 
					
					<label class="ui-fields">
						<p style="padding: 5px 0 2px 0;"><?=t($t_base.'fields/comments_for_pricing');?></p>
						<textarea style="width:594px;" name="createArr[szComment]" rows="5" cols="70" id="szCommentText" maxlength="400" onkeyup="checkLimit(this.id)"><?=t($t_base.'title/referal_fee_levide_on');?></textarea>
					</label>
					<div style="clear: both;"></div>
					<span style="font: italic;float: right;">You have <span id="char_left">400</span> characters left to write</span>
					<div style="clear: both;"></div>		
					<p align="center"><br/><br/>
					<div style="clear: both;"></div>
					<input type="hidden" name="flag" value="ADD_NEW_FORWARDER_CONFIRMATION">					
						<p align="center" ><a href="javascript:void(0)" class="button2" id="cancel_button" onclick="cancelEditMAnageVar(0);"><span><?=t($t_base.'fields/cancel');?></span></a>
						<a href="javascript:void(0)" class="button1" id="createNewForwarder"   onclick="addNewForwarderConfirmValidate()"><span><?=t($t_base.'fields/create');?></span></a></p>						
					</form>	
				</div>
			</div>
		</div>	
</div>
	<?php
}

function get_working_hours_for_graph($ini_str,$end_str){
    
    $kDashboardAdmin = new cDashboardAdmin();
    $transportecaOfficeHourArr=$kDashboardAdmin->getTransportecaOfficeHour();
    
    
    $ini_time_arr = $transportecaOfficeHourArr['iOpenTime']; //hr, min
    $end_time_arr = $transportecaOfficeHourArr['iCloseTime']; //hr, min
    
//date objects
    $startDateTimeArr = explode(":",date('H:i',strtotime($ini_str)));
    $ini = date_create($ini_str);
    
    $iDayStartNum=date('N',strtotime($ini));
    $ini_time=$ini_time_arr[$iDayStartNum];	
    $ini_wk = date_time_set(date_create($ini_str),$ini_time[0],$ini_time[1]);
    $start_end_time=$end_time_arr[$iDayStartNum];
    
    $removeStartTimeTaskCloseBeforeOfficeOpen=false;
    if($startDateTimeArr[0]>$start_end_time[0])
    {
        $removeStartTimeTaskCloseBeforeOfficeOpen=true;
    }
    else if($startDateTimeArr[0]==$start_end_time[0])
    {
        if($startDateTimeArr[1]>$start_end_time[1])
        {
            $removeStartTimeTaskCloseBeforeOfficeOpen=true;
        }
    }
    
    
    $endDateTimeArr = explode(":",date('H:i',strtotime($end_str)));
    
    $end = date_create($end_str);
    $iDayEndNum=date('N',strtotime($end ));
    $end_time=$end_time_arr[$iDayEndNum];	
    $end_wk = date_time_set(date_create($end_str),$end_time[0],$end_time[1]);
    
    $end_start_time=$ini_time_arr[$iDayEndNum];
    
    $removeEndTimeTaskCloseBeforeOfficeOpen=false;
    if($endDateTimeArr[0]<$end_start_time[0])
    {
        $removeEndTimeTaskCloseBeforeOfficeOpen=true;
    }
    else if($endDateTimeArr[0]==$end_start_time[0])
    {
        if($endDateTimeArr[1]<$end_start_time[1])
        {
            $removeEndTimeTaskCloseBeforeOfficeOpen=true;
        }
    }
    //echo $removeEndTimeTaskCloseBeforeOfficeOpen."removeEndTimeTaskCloseBeforeOfficeOpen";
    
    //days
    $workdays_arr = get_workdays_for_graph($ini,$end);
    $workdays_count = count($workdays_arr);
    
    for($i=0;$i<$workdays_count;++$i)
    {
        $iDayWorking=date('N',strtotime($workdays_arr[$i] ));
        $iWorkingDayStartTime=$ini_time_arr[$iDayWorking];
        $iWorkingDayEndTime=$end_time_arr[$iDayWorking];
        $workday_seconds1 = (($iWorkingDayEndTime[0] * 60 + $iWorkingDayEndTime[1]) - ($iWorkingDayStartTime[0] * 60 + $iWorkingDayStartTime[1])) * 60;
        $workday_seconds +=$workday_seconds1;
	
    }
    //get time difference
    $ini_seconds = 0;
    $end_seconds = 0;
    
    //echo $removeStartTimeTaskCloseBeforeOfficeOpen."removeStartTimeTaskCloseBeforeOfficeOpen<br />";
    if($removeStartTimeTaskCloseBeforeOfficeOpen)
    {
        $ini_seconds =(($start_end_time[0] * 60 + $start_end_time[1]) - ($ini_time[0] * 60 + $ini_time[1])) * 60;
        //echo $ini_seconds."ini_seconds<br />";
    }else{
    if(in_array($ini->format('Y-m-d'),$workdays_arr)) $ini_seconds = $ini->format('U') - $ini_wk->format('U');
    }
    if($removeEndTimeTaskCloseBeforeOfficeOpen)
    {
        $end_seconds =(($end_time[0] * 60 + $end_time[1]) - ($end_start_time[0] * 60 + $end_start_time[1])) * 60;
    }
    else{
        if(in_array($end->format('Y-m-d'),$workdays_arr)) $end_seconds = $end_wk->format('U') - $end->format('U');
    }
    //echo ($ini_seconds/ 60)."ini_seconds<br />";
    //echo ($end_seconds/ 60)."end_seconds<br />";
    $seconds_dif = $ini_seconds > 0 ? $ini_seconds : 0;
    if($end_seconds > 0) $seconds_dif += $end_seconds;
	
    //echo ($workday_seconds/ 60)."workday_seconds".($seconds_dif/ 60)."<br/>";
    //final calculations
    $working_seconds = ( $workday_seconds) - $seconds_dif;
    
    if((float)$working_seconds<0.00)
    {
        $working_seconds =0;
    }
    return round($working_seconds / 60); //return min
}

function get_workdays_for_graph($ini,$end){
    //config
    $skipdays = array(6,0); //saturday:6; sunday:0
    $skipdates = array(); //eg: ['2016-10-10'];
    //vars
    $current = clone $ini;
    $current_disp = $current->format('Y-m-d');
    $end_disp = $end->format('Y-m-d');
    $days_arr = array();
    //days range
    while($current_disp <= $end_disp){
        if(!in_array($current->format('w'),$skipdays) && !in_array($current_disp,$skipdates)){
            $days_arr[] = $current_disp;
        }
        $current->add(new DateInterval('P1D')); //adds one day
        $current_disp = $current->format('Y-m-d');
    }
    return $days_arr;
}

function checkUrlHavingHttpOrHttps($szUrl)
{
    if (substr($szUrl, 0, 7) == "http://" || substr($szUrl, 0, 8) == "https://")
    {
        return true;
    }
    else
    {
       return false;
    }
}

function bulkuploadFileHtmlSection($iNumber,$szName)
{
    $t_base = "BulkUpload/";
?>
    <div class="file-upload-remove-section" id="file_upload_remove_<?php echo $iNumber;?>">            
        <div class="file" id="file_<?php echo $iNumber;?>">
            <input type="file" id="file_upload_<?php echo $iNumber;?>" name="file_upload[]" value="<?php echo $szName;?>"/>
            <span class="button"><?=t($t_base.'fields/no_file_selected_services');?></span>
        </div>
        <div class="remove_section_<?php echo $iNumber;?>" <?php if($iNumber==1){?> style="display:none;" <?php }?>>
            <span class="remove"><a href="javascript:void(0);" onclick="remove_file_upload_section('<?php echo $iNumber;?>');">Remove</a></span>
        </div>            
    </div>
   <?php 
}

function automateRfqResponseHtmlForm($kExplain)
{?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:430px;"> 
            <h3>Automated RFQ Response</h3> <br/> 
            <p>Please give the Automated RFQ Response a description. This could be the heading used on the standard cargo in Wordpress, which will use this response.</p>
            <form name="addEditAutoMatedRfqResponseForm" id="addEditAutoMatedRfqResponseForm" method="post" action="javascription:void(0);"> 
            <label class="ui-fields">
                <input name="automatedRfqArr[szCommentText]"  id="szCommentText" maxlength="256" onkeyup="checkLimitForAll(this.id,256);enable_save_button_for_auto_rfq(this.id);" value="<?php echo $_POST['automatedRfqArr']['szComment'];?>">
            </label>
            <div style="clear: both;"></div>
            <span style="font: italic;float: right;">You have <span id="char_left">256</span> characters left to write</span>
            <div style="clear: both;"></div>		
            <p align="center"><br/><br/>
            
            <div class="btn-container">
                <p align="center">
                    <p align="center" >
                        <input id="id" name="automatedRfqArr[id]" value="<?php echo $_POST['automatedRfqArr']['id'];?>" type="hidden">
                        <a href="javascript:void(0)" class="button2" id="cancel_button_automated_rfq" onclick="closeConactPopUp(0);"><span>Cancel</span></a>
                        <a href="javascript:void(0)" class="button1" style="opacity:0.4;" id="save_automated_rfq"><span>Save</span></a></p>						
                </p>
            </div>    
            </form>
        </div>
    </div>
   <?php 
}

function confirmationPopupForDeletingAutomatedRfqResponse($idAutomatedRfq)
{?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:430px;"> 
            <h3>Delete Automated RFQ Response</h3> <br/> 
            <p>This Automated RFQ Response will be deleted. This action cannot be reversed</p>
             <div class="btn-container">
                <p align="center">
                    <p align="center" >
                        <a href="javascript:void(0)" class="button2" id="cancel_button_automated_rfq" onclick="closeConactPopUp(0);"><span>Cancel</span></a>
                        <a href="javascript:void(0)" class="button1" id="save_automated_rfq" onclick="confrimedDeleteAutomatedRfqResponse(<?php echo $idAutomatedRfq;?>);"><span>Confirm</span></a></p>						
                </p>
            </div>
        </div>
    </div>
   <?php 
}

function showWarningMessageWhenChangeTheValue()
{
  ?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:430px;"> 
            <h3>Change not saved</h3> <br/> 
            <p>Any changes you have made will be lost</p>
             <div class="btn-container">
                <p align="center">
                    <p align="center" >
                        <a href="javascript:void(0)" class="button2" id="cancel_button_automated_rfq" onclick="$('#warning_msg').attr('style','display:none;');"><span>Cancel</span></a>
                        <a href="javascript:void(0)" class="button1" id="save_automated_rfq" onclick="closeAutomatedRfqResponseInformationSection();"><span>Confirm</span></a></p>						
                </p>
            </div>
        </div>
    </div>
   <?php 
}

function display_add_standard_cargo_pricing_try_it_out_new($idLandingPage)
{
    $t_base = "management/uploadService/"; 
    $kConfig=new cConfig();
    $allCountryAry = array();
    $allCountryAry = $kConfig->getAllCountries(true); 
    if(!empty($_POST['searchAry']))
    {
        $szDestinationPostCode = $_POST['searchAry']['szConsigneePostcode'];
        $szDestinationCity = $_POST['searchAry']['szConsigneeCity'];
        $idDestinationCountry = $_POST['searchAry']['idConsigneeCountry'];
        
        $szOriginCountryStr = $_POST['searchAry']['szOriginCountryStr'];
        $szDestinationCountryStr = $_POST['searchAry']['szDestinationCountryStr'];
    }     
    ?>
    <br><br>
    <div class="clear-all"></div> 
    <hr>
    <script type="text/javascript" defer="">
        $().ready(function() {	 
            var autocomplete1,place ; 
            function initialize1() 
            {
                var input1 = document.getElementById('szOriginCountry');
                var options = {types: ['regions']};

                var autocomplete1 = new google.maps.places.Autocomplete(input1); 
                $(".pac-container:last").attr("id", 'szOriginCountryPacContainer' );
                google.maps.event.addListener(autocomplete1, 'place_changed', function() 
                {    
                    var szOriginAddress_js = $("#szOriginCountry").val();   
                }); 
                var input2 = document.getElementById('szDestinationCountry');
                var autocomplete2 = new google.maps.places.Autocomplete(input2); 
                $(".pac-container:last").attr("id", 'szDestinationCountryPacContainer' );
                google.maps.event.addListener(autocomplete2, 'place_changed', function() 
                {
                    var szDestinationAddress_js = $("#szDestinationCountry").val();   
                }); 

                $('#szOriginCountry').bind('paste focus', function (e){  
                    $('#szOriginCountry').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szOriginCountryPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks
                        }
                        else
                        {
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display'); 
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);
                                }
                            });
                        }
                        var key_code = e.keyCode || e.which;
                        if (key_code == 9 || key_code == 13) {
                            prefillAddressOnTabPress('szOriginCountry',pac_container_id);
                        }  
                    }); 
                });

                $('#szDestinationCountry').bind('paste focus', function (e){  
                    $('#szDestinationCountry').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szDestinationCountryPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks 
                            //console.log("div exist called");
                        }
                        else
                        {
                            var iFoundDiv = 1;
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display');  
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);  
                                    iFoundDiv = 2;
                                } 
                            });

                            if(iFoundDiv==1)
                            {
                                $(".pac-container").each(function(pac)
                                {
                                    var container_id = $(this).attr('id');  
                                    if(container_id=='' || container_id === undefined)
                                    {
                                        $(this).attr('id',pac_container_id); 
                                        return true;
                                    }
                                });
                            }
                        } 
                        if (e.keyCode == 9 || e.keyCode == 13) {
                            prefillAddressOnTabPress('szDestinationCountry',pac_container_id);
                        }  
                    });
                }); 
            } 
            function prefillAddressOnTabPress(input_field_id,pac_container_id)
            {  
                $(".pac-container").each(function(pac){
                   var disp = $(this).css('display'); 
                   var contaier_id = $(this).attr('id');  

                   //console.log("Container: "+contaier_id + " pac id: "+pac_container_id);

                   if(contaier_id==pac_container_id)
                   {
                        var spanObj = $(this).children(".pac-item:first" );
                        var firstResult = ""; 
                        spanObj.children("span").each(function(){
                            var spanText = $(this).text(); 
                            spanText = jQuery.trim(spanText);
                            //("Span Text: "+spanText);
                            if(spanText!='')
                            {
                                if(firstResult=='')
                                {
                                    firstResult += spanText;
                                }
                                else
                                {
                                    firstResult += ", "+ spanText;
                                }
                                //$(this).children( ".pac-item:first" ).addClass("pac-selected");
                                //$(this).css("display","none");
                                $("#"+input_field_id).val(firstResult); 
                                $("#"+input_field_id).select();
                            }
                        }); 

                   }
                }); 
            } 
            function clean_pac_container()
            { 
                $("div").each(function(index){
                    if($( this ).hasClass("pac-container"))
                    {
                        $( this ).remove();
                    }
                }); 
            }
            initialize1();  
        }); 
   
    </script> 
    <h4><strong><?php echo t($t_base.'title/try_it_out'); ?></strong></h4>
    <form id="standardPricingTryItOutForm" method="post" name="standardPricingTryItOutForm" action="javascript:void(0);">
        <div class="clearfix email-fields-container" style="width:100%;">
            <span class="quote-field-container wd-34">
                 From<br>
                 <input type="text" name="searchAry[szOriginCountryStr]" placeholder="Start typing" id="szOriginCountry"  onblur="check_form_field_empty_standard(this.form.id,this.id);" value="<?php echo $szOriginCountryStr;?>" onkeyup="enable_voga_try_it_out_submit_new();" onfocus="check_form_field_not_required(this.form.id,this.id);" /> 
            </span>
            <span class="quote-field-container wds-35">
                 To<br>
                 <input type="text" name="searchAry[szDestinationCountryStr]" placeholder="Start typing" id="szDestinationCountry" value="<?php echo $szDestinationCountryStr;?>" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enable_voga_try_it_out_submit_new();" onfocus="check_form_field_not_required(this.form.id,this.id);" /> 
            </span>
            <span class="quote-field-container wds-31" style="text-align: right;"> <br>
                 <a id="standard-cargo-test-button" class="button1" style="opacity:0.4"  href="javascript:void(0)">
                     <span style="min-width:50px;">Test</span>
                 </a> 
                 <a id="standard-cargo-clear-button" class="button2" onclick="reset_standard_tryit_out_new('<?php echo $idLandingPage; ?>');" href="javascript:void(0)">
                     <span style="min-width:50px;">Clear</span>
                 </a>
            </span>
        </div> 
        <input type="hidden" name="searchAry[idLandingPage]" id="idLandingPage" value="<?php echo $idLandingPage; ?>">
        
        <div id="update_cargo_details_container_div" class="search-min-v7-cargo-field">
            <?php echo display_vog_cargo_fields_management_new($kBooking,$idLandingPage); ?>
        </div> 
    </form>
    <script type="text/javascript">
    </script>    
    <?php   
    if(!empty($kBooking->arErrorMessages))
    {  
        $formId = 'standardPricingTryItOutForm'; 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
}

function display_vog_cargo_fields_management_new($kBooking,$idLandingPage)
{
    $cargoAry = array(); 
    $t_base = "LandingPage/";  
    
    $iLanguage = getLanguageId();  
    //$iSearchMiniPage = 7;
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__); 
    $szDanishClass=''; 
    $kCourierService = new cCourierServices();   
    
    $kExplain = new cExplain();
    $vogaPageDetailsAry = $kExplain->getAllNewLandingPageData($idLandingPage);
    $landingPageDataAry = $vogaPageDetailsAry[0];
    
    
    $iPageSearchType = $landingPageDataAry['iPageSearchType'];
    
    if($iPageSearchType==__SEARCH_TYPE_VOGUE__)
    {
        $iSearchMiniPage = 7;
    }
    else if($iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_)
    {
        $iSearchMiniPage = 8;
    }
    else if($iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_)
    {
        $iSearchMiniPage = 9;
    }
    $szDivKey = "_7";  
   
    
                    
    $kConfig = new cConfig();
    $standardCargoCategoryAry = array();
    $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,'','',true);
    
    /*
    * Building json data to pre-populate in category and subcategory menu
    */
//    $standardSubCategoryAry = array();
//    $standardVogaSubCategoryAry = array();
//    if(!empty($standardCargoCategoryAry))
//    {
//        foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
//        { 
//            //echo "<br> Cat: ".$standardCargoCategoryArys['id']." Lang: ".$iSelectedLanguage." src Min: ".$landingPageDataAry['idSearchMini'];
//            $standardCargoAry = array();
//            $standardCargoAry = $kConfig->getStandardCargoList($standardCargoCategoryArys['id'],$iSelectedLanguage);  
//            $idStandardCategory = $standardCargoCategoryArys['id'];
//         //            print_r($standardCargoAry);
//           //          die();
//            $ctr = 0;
//            if(!empty($standardCargoAry))
//            {
//                foreach($standardCargoAry as $standardCargoArys)
//                {
//                    $standardSubCategoryAry[$idStandardCategory][$ctr]['id'] = $standardCargoArys['id'];
//                    $standardSubCategoryAry[$idStandardCategory][$ctr]['szLongName'] = stripslashes(addslashes(($standardCargoArys['szLongName'])));
//                    $standardVogaSubCategoryAry[$standardCargoArys['id']]['iColli'] = $standardCargoArys['iColli'];
//                    $standardVogaSubCategoryAry[$standardCargoArys['id']]['idSearchMini'] = $standardCargoArys['idSearchMini']; 
//                    $ctr++;
//                }
//            } 
//        }
//    } 
    //print_r($standardSubCategoryAry);
    if(!empty($standardSubCategoryAry))
    {
        $standardSubCategoryJsonStr = json_encode($standardSubCategoryAry);
    }
    if(!empty($standardVogaSubCategoryAry))
    {
        $standardVogaSubCategoryJsonStr = json_encode($standardVogaSubCategoryAry);
    } 
    ?>  
<!--    <script type="text/javascript">
        __JS_ONLY_VOGA_CATEGORY_LISTING__ = "";
        __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__ = "";
        
        __JS_ONLY_VOGA_CATEGORY_LISTING__ = <?php echo $standardSubCategoryJsonStr ?>; 
        __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__ = <?php echo $standardVogaSubCategoryJsonStr ?>; 
    </script>-->
    <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
        <?php 
            $counter = 1; 
            if(count($_POST['cargodetailAry']['idCargoCategory'])>0)
            { 
                if(count($_POST['cargodetailAry']['idCargoCategory'])>0)
                {
                   $loop_counter = count($_POST['cargodetailAry']['idCargoCategory']);  
                   $postDataFlag = true;
                } 
                for($j=0;$j<$loop_counter;$j++)
                {
                    $counter = $j+1; 
                    ?>
                    <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" class="request-quote-fields clearfix"> 
                        <?php
                            echo display_furniture_order_form_management_new($counter,$iSearchMiniPage,array(),$landingPageDataAry['idSearchMini']);
                        ?>
                    </div>
                    <?php
                }
            }
            else
            {   
                ?>
                <div id="add_booking_quote_container_1<?php echo $szDivKey;?>" class="request-quote-fields clearfix" > 
                    <?php
                       echo display_furniture_order_form_management_new($counter,$iSearchMiniPage,array(),$landingPageDataAry['idSearchMini']);
                    ?>
                </div>
                <?php 
            } 
        ?>
        <!--<div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>"></div>-->
    </div>     
    <?php
        if(!empty($kBooking->arErrorMessages['szSpecialError']))
        {
            echo '<div class="cargo-success-text">'.$kBooking->arErrorMessages['szSpecialError']."</div>" ;
        }
    ?>
    <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
    <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" />
    <input type="hidden" name="voga_sub_category_type_field" id="voga_sub_category_type_field" value="<?php echo t($t_base.'fields/furniture_type');?>" /> 
    <?php   
}

function display_furniture_order_form_management_new($number,$iSearchMiniPage=false,$cargoAry=array(),$idSearchMini=0)
{    
    if(!empty($_POST['cargodetailAry']))
    {
        $cargoAry = $_POST['cargodetailAry'] ; 
        $llop_counter = $number;
        $idCargoCategory = $cargoAry['idCargoCategory'][$llop_counter];
        $fWidth = $cargoAry['iWidth'][$llop_counter]; 
        $iQuantity = $cargoAry['iQuantity'][$llop_counter];
        $idCargo = $cargoAry['id'][$llop_counter]; 
        $idStandardCargoType = $cargoAry['idStandardCargoType'][$llop_counter];
        $idSearchMini = $cargoAry['idSearchMini'][$llop_counter];
    }
    else
    { 
        $idCargoCategory = $cargoAry[$number]['idCargoCategory'];
        $fWidth = $cargoAry[$number]['fWidth']; 
        $iQuantity = $cargoAry[$number]['iQuantity'];
        $idCargo = $cargoAry[$number]['id'];  
    }  
    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $idBooking = $postSearchAry['id'];
                    
    $t_base = "LandingPage/";  
    $szStandardClass="search-mini-cargo-fields"; 
    $idSuffix = "_V_7" ; 
    $iLanguage = getLanguageId();
    
    $standardCargoCategoryAry = array();
    $standardCargoAry = array();
    
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__); 
    if((int)$idSearchMini>0){
    $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,'',$idSearchMini);
    }
    $standardCargoListArr=$kConfig->getDistinctStandardCargoList();
    
//    if($iSearchMiniPage==9){
//        $idCargoCategory=$standardCargoCategoryAry[0]['id'];
//    }
    if($idCargoCategory>0)
    { 
          $standardCargoAry = $kConfig->getStandardCargoList($idCargoCategory,$iSelectedLanguage,'',$idSearchMini); 
    }
?>   
    <div class="clearfix email-fields-container" style="width:100%;">
        <span class="quote-field-container" style="width:15%;">  
            <span id="idSearchMini<?php echo $number."".$idSuffix."_container"; ?>"> 
                <select class="standard-cargo-category" onfocus="check_form_field_not_required(this.form.id,'idSearchMini<?php echo $number."".$idSuffix; ?>');" name="cargodetailAry[idSearchMini][<?=$number?>]" id="idSearchMini<?php echo $number."".$idSuffix; ?>" style="width:90%;" onchange="update_category_type_management(this.value,'<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>','SEARCH_MINI','','1');enable_voga_try_it_out_submit_new();">
                    <option value=""><?=t($t_base.'fields/search_mini');?></option>
                    <?php
                        if(!empty($standardCargoListArr))
                        {
                            foreach($standardCargoListArr as $standardCargoListArrs)
                            {
                                ?>
                                <option value="<?php echo $standardCargoListArrs['idSearchMini']?>" <?php if($standardCargoListArrs['idSearchMini']==$idSearchMini){?> selected <?php }?> ><?php echo $standardCargoListArrs['idSearchMini']; ?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </span> 
        </span>
        <span class="quote-field-container" style="width:23%;">  
            <span id="idCargoCategory<?php echo $number."".$idSuffix."_container"; ?>"> 
                <?php echo display_category_type_dropdown_management_new($standardCargoCategoryAry,$number,$idSuffix,$idCargoCategory);?>
            </span> 
        </span>
        <span class="quote-field-container" style="<?php if($iSearchMiniPage==9){?>width:50%; <?php }else{ ?> width:23%; <?php }?>">
            <span class="input-fields" id="idStandardCargoType<?php echo $number."".$idSuffix."_container"; ?>"> 
                <?php echo display_cargo_type_dropdown_management_new($standardCargoAry,$number,$idSuffix,$idStandardCargoType); ?>
            </span> 
        </span>
        
        <span class="quote-field-container" style="width:10%;">
            <span class="input-box" id="iQuantity<?php echo $number."".$idSuffix."_container"; ?>">  
                <input type="text" onkeypress="return isNumberKey(event);" onkeyup="enable_voga_try_it_out_submit_new();" <?=$disabled_str?> pattern="[0-9]*" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'iQuantity<?php echo $number."".$idSuffix; ?>');" onfocus="check_form_field_not_required(this.form.id,'iQuantity<?php echo $number."".$idSuffix."_container"; ?>');" placeholder="<?php echo ucfirst(t($t_base.'fields/furniture_qty')); ?>" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id="iQuantity<?php echo $number."".$idSuffix; ?>" class="first-cargo-fields<?php echo $idSuffix; ?>" />
            </span> 
        </span>
        <span class="quote-field-container" style="width:10.5%;text-align: right;">
            <a href="javascript:void(0);" onclick="add_more_cargo_details_management_new()" class="cargo-add-button"></a>  
            <a href="javascript:void(0);" class="cargo-remove-button" <?php echo $szMinusButtonId; ?> onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo $number."_".$iSearchMiniPage ?>','<?php echo $number; ?>','7');"></a>
        </span>
        
    </div> 
    <?php 
}

function display_cargo_type_dropdown_management_new($standardCargoAry,$number,$idSuffix,$idStandardCargoType=false)
{
    $t_base = "LandingPage/";   
?>
    <select onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,'idStandardCargoType<?php echo $number."".$idSuffix."_container"; ?>');" name="cargodetailAry[idStandardCargoType][<?=$number?>]" id= "idStandardCargoType<?php echo $number."".$idSuffix; ?>" style="width:90%;" onchange="enable_voga_try_it_out_submit_new();">
        <option value=""><?=t($t_base.'fields/furniture_type');?></option>
        <?php
            if(!empty($standardCargoAry))
            {
                foreach($standardCargoAry as $standardCargoDetail)
                {
                    ?>
                    <option value="<?=$standardCargoDetail['id']?>" <?php if($standardCargoDetail['id']==$idStandardCargoType){?> selected <?php }?>><?=$standardCargoDetail['szLongName']?></option>
                    <?php
                }
            }
        ?>
    </select>  
    <?php
}
function display_category_type_dropdown_management_new($standardCargoCategoryAry,$number,$idSuffix,$idCargoCategory=false)
{
    $t_base = "LandingPage/";   
?>
    <select class="standard-cargo-category" onfocus="check_form_field_not_required(this.form.id,'idCargoCategory<?php echo $number."".$idSuffix; ?>');" name="cargodetailAry[idCargoCategory][<?=$number?>]" id="idCargoCategory<?php echo $number."".$idSuffix; ?>" style="width:90%;" onchange="update_cargo_type_management(this.value,'<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>','CATEGORY','','1');enable_voga_try_it_out_submit_new();">
            <option value=""><?=t($t_base.'fields/furniture_category');?></option>
            <?php
                if(!empty($standardCargoCategoryAry))
                {
                    foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
                    {
                        ?>
                        <option value="<?php echo $standardCargoCategoryArys['id']?>" <?php if($standardCargoCategoryArys['id']==$idCargoCategory){?> selected <?php }?> ><?php echo $standardCargoCategoryArys['szCategoryName']; ?></option>
                        <?php
                    }
                }
            ?>
        </select>
    <?php
}
?>