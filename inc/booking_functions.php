<?php
if(!isset($_SESSION))
{
	session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
  
function display_new_search_form($kBooking,$postSearchAry,$hidden_search_form=false,$service_page_flag=false,$hide_search_image=false,$szDefaultFromField=false,$idSeoPage=false,$bBookingQuotes=false,$idDefaultLandingPage=false,$bTestPage=false,$iIncludedSearchMini=false,$szContactMe='',$szMiddleText='')
{  
        $updateButtonFlagSearchMini=false;
	$iHiddenValue = 0;
        if(isset($_POST['searchAry']['iSearchMiniPage']))
	{
            $iSearchMiniPage = $_POST['searchAry']['iSearchMiniPage'];
            $bSearchMini = true;
	}
	else if($iIncludedSearchMini>0)
	{
            $iSearchMiniPage = $iIncludedSearchMini;
            $bSearchMini = true;
	}
        else if(($postSearchAry['iSearchMiniVersion']=='5' || $postSearchAry['iSearchMiniVersion']=='6')&& !$hidden_search_form)
        {
            $kCourierServices= new cCourierServices();
            $bDTDTradesFlag = false;
            if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
            { 
                $iSearchMiniPage = $postSearchAry['iSearchMiniVersion'];
                $bSearchMini = true;
                $updateButtonFlagSearchMini=true;
                $kCourierService_new = new cCourierServices();  
            }
        }
	if($iSearchMiniPage>0)
	{
		$id_suffix = "_V_".$iSearchMiniPage;
	}        
	else if($hidden_search_form)
	{
            $id_suffix = "_hiden";
            $iHiddenValue = 1;
	}
	$bDisplayPickupField = true;
	$kBooking_new = new cBooking(); 
	//following sesssion will used only for multitabing issue  
	if(!empty($_REQUEST['booking_random_num']))
	{
            $szBookingRandomNum = $_REQUEST['booking_random_num'] ;
	}
	else if(!empty($postSearchAry['szBookingRandomNum']) && $postSearchAry['idBookingStatus']!='3' &&  $postSearchAry['idBookingStatus']!='4')
	{
            $szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
            $szBookingKey = $szBookingRandomNum ; 
	}
	else
	{
            $szBookingRandomNum = $kBooking_new->getUniqueBookingKey(10);
            $szBookingRandomNum = $kBooking_new->isBookingRandomNumExist($szBookingRandomNum);
	} 
        
        $bDisplayBooknowButton = true;
        if(isset($_SESSION['admin_test_search_'.$szBookingRandomNum]))
        {
            /* When admin clicked on TEST SEARCH button under Pending Tray=>Task=>Remind Pane than we do not allow user to changes anything in original booking.
             * So this is why we hiding Search/Update button in this case. 
             */
            
            $bDisplayBooknowButton = false;
        }  
        $iShipperConsigneeFlag = false;
        if((int)$_SESSION['user_id']>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($_SESSION['user_id']);
            $szFirstName = $kUser->szFirstName;			
            $szLastName = $kUser->szLastName;
            $szEmail = $kUser->szEmail;
            $szPhone = $kUser->szPhoneNumber;
            $idDialCode = $kUser->idInternationalDialCode;
            $szAddress = $kUser->szAddress1;
            $szPostcode = $kUser->szPostCode;
            $szCity = $kUser->szCity;
            $idCustomerCountry = $kUser->szCountry;
            
            $szConsigneeFirstName = $szFirstName;
            $szConsigneeLastName = $szLastName;
            $szConsigneeEmail = $szEmail;
            $szConsigneePhone = $szPhone; 
        }
        else
        {
            /*if(__LANGUAGE__==__LANGUAGE_TEXT_SWEDISH__)
            {
                $idDialCode = '215'; //Sweden
            }
            else if(__LANGUAGE__==__LANGUAGE_TEXT_NORWEGIAN__)
            {
                $idDialCode = '166'; //Norway
            }
            else
            {
                $idDialCode = '61'; //Denmark
            } */
            $idDialCode = '61';        
            $userIpDetailsAry = array();
            $userIpDetailsAry = getCountryCodeByIPAddress(); 
            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;  
            
            if(!empty($szUserCountryName))
            {
                $kConfigCountry = new cConfig();
                $kConfigCountry->loadCountry(false,$szUserCountryName);
                
                $idCustomerCountry = $kConfigCountry->idCountry;
                $idDialCode = $kConfigCountry->idCountry;
            } 
        }    
        if($idCustomerCountry==$postSearchAry['idOriginCountry'])
        {
            $iShipperConsigneeFlag = 1; //Customer is consignee 
        }
        else if($idCustomerCountry==$postSearchAry['idDestinationCountry'])
        {
            $iShipperConsigneeFlag = 2; //Customer is consignee
        }
        else
        {
            $iShipperConsigneeFlag = 3; //Customer is billing
        }
        
        $idConsigneeDialCode = $idDialCode;
        $idConsigneeCountry = $idDialCode;
        
	if(!empty($kBooking->arErrorMessages))
	{
            foreach($kBooking->arErrorMessages as $errorKey=>$errorValue)
            {
                if($errorKey=='szOriginCountry')
                {
                    $errorKey = 'szOriginCountryStr'.$id_suffix.'_container';
                } 
                if($errorKey=='szDestinationCountry')
                {
                    $errorKey = 'szDestinationCountryStr'.$id_suffix.'_container';
                } 
                if($errorKey=='szDestinationCountry')
                {
                    $errorKey = 'szDestinationCountryStr'.$id_suffix.'_container';
                }
                if($errorKey=='fCargoWeight_landing_page')
                {
                    $errorKey = 'iWeight'.$id_suffix.'_container';
                }
                if($errorKey=='iVolume')
                {
                    $errorKey = 'iVolume'.$id_suffix.'_container';
                }
                if($errorKey=='dtTiming')
                {
                    $errorKey = 'dtTiming'.$id_suffix.'_container';
                } 
                ?>
                <script type="text/javascript" defer="1">$("#"+'<?php echo $errorKey; ?>').addClass('red_border');</script>
                <?php
            }
	} 
        if(isset($_POST['searchAry']['idSeoPage']))
	{
            $idSeoPage = $_POST['searchAry']['idSeoPage'];
	}
	else
	{
            $idSeoPage = $idSeoPage ;
	}
        
        if(isset($_POST['searchAry']['iNumColli']))
	{
            $iNumColli = $_POST['searchAry']['iNumColli'];
	} 
        else if(isset($postSearchAry['iNumColli']) && ($postSearchAry['iSearchMiniVersion']=='5' || $postSearchAry['iSearchMiniVersion']=='6') && !$hidden_search_form)
	{
            $iNumColli = $postSearchAry['iNumColli'];
	}
        else
        {
            $iNumColli = 1;
        }
        if(isset($_POST['searchAry']['iPalletType']))
	{
            $iPalletType = $_POST['searchAry']['iPalletType'];
	}
        else if(isset($postSearchAry['iPalletType'])  && ($postSearchAry['iSearchMiniVersion']=='5' || $postSearchAry['iSearchMiniVersion']=='6') && !$hidden_search_form)
	{
            $iPalletType = $postSearchAry['iPalletType'];
	}
        else
        {
            $iPalletType = 1;
        }
        if(isset($_POST['searchAry']['szPalletDescriptions']))
	{
            $szPalletDescriptions = $_POST['searchAry']['szPalletDescriptions'];
	} 
        if(isset($_POST['searchAry']['szPalletDimensions']))
	{
            $szPalletDimensions = $_POST['searchAry']['szPalletDimensions'];
	} 
	
        if(($service_page_flag || $hide_search_image) && $postSearchAry['id']>0)
        {   
            $kCourierServices = new cCourierServices();
            if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
            {
                $postSearchAry['idServiceType'] = __SERVICE_TYPE_DTD__ ;
                $bDisplayPickupField = false;
                if($iSearchMiniPage=='5' || $iSearchMiniPage=='6')
                    $szWithoutPickUpCLass = " booking-quote";
                else
                    $szWithoutPickUpCLass = " dtd-services-list";
            }
        } 
        if($hidden_search_form)
        {
            //$szWithoutPickUpCLass = " dtd-services-list"; 
            //$bDisplayPickupField = false;
        }  
            
        $fStaticDefaultWeight = cBooking::$fDefaultCargoWeight; 
                    
	$button_flag=true; 
	if(empty($_COOKIE['__SIMPLE_SEARCH_COOKIE__']) && empty($postSearchAry))
	{	 
            $bGetDefaultValues = true;
            if($idSeoPage>0)
            {
                $kSeoLanding = new cSEO();
                $seoDetailsAry = array();
                $seoDetailsAry = $kSeoLanding->getAllSEOPageData($idSeoPage);
                
                if(!empty($seoDetailsAry))
                {
                    //$bGetDefaultValues = false;
                    $seoDetailsArys = $seoDetailsAry[0]; 
                    if($seoDetailsArys['iFromIPLocation']==1)
                    {
                        if(!empty($_COOKIE['__USER_IP_DETAILS___']))
                        {
                            $userIpDetailsStr = $_COOKIE['__USER_IP_DETAILS___'] ;
                            $userIpDetailsAry = array();
                            $userIpDetailsAry = unserialize($userIpDetailsStr);
                            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;  
                        }
                        else
                        {   
                            $userIpDetailsAry = array();
                            $userIpDetailsAry = getCountryCodeByIPAddress();

                            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ; 
                            $userIpDetailsStr = serialize($userIpDetailsAry);
                            
                            //setting cookien variable
                            if($_COOKIE['__USER_COUNTRY_NAME___']!=$szUserCountryName)
                            {
                                setCookie('__USER_COUNTRY_NAME___', $szUserCountryName ,time()+3600, '', $_SERVER['HTTP_HOST']);
                            }  
                            if($_COOKIE['__USER_IP_DETAILS___']!=$userIpDetailsStr)
                            {
                                setCookie('__USER_IP_DETAILS___', $userIpDetailsStr ,time()+3600, '', $_SERVER['HTTP_HOST']);  
                            } 
                        }
                        
                        $visitor_location = array();
                        $visitor_location['CityName'] = $userIpDetailsAry['szCityName'] ;
                        $visitor_location['RegionName'] = $userIpDetailsAry['szRegionName'] ;
                        $visitor_location['szZipCode'] = $userIpDetailsAry['szZipCode'] ;
                        $visitor_location['szCountryName'] = $userIpDetailsAry['szCountryName'] ;
                         
                        $toAddress='';
                        if(!empty($visitor_location['szZipCode']))
                        {
                            $toAddress =$visitor_location['szZipCode']." ";
                        }
                        if(!empty($visitor_location['CityName']))
                        {
                            $toAddress .=$visitor_location['CityName'].", ";
                        } 
                        if(!empty($visitor_location['RegionName']))
                        {
                            //$toAddress .=$visitor_location['RegionName'].", ";
                        } 
                        $toAddress .= $visitor_location['szCountryName'];

                        $postSearchAry['szOriginCountryStr']=ucwords(strtolower($toAddress));
                        $postSearchAry['szOriginCountry']=ucwords(strtolower($toAddress));
                        $bGetDefaultValues = false;
                    }
                    else if(!empty($seoDetailsArys['szFromSelection']))
                    {
                        $postSearchAry['szOriginCountryStr'] = $seoDetailsArys['szFromSelection'];
                        $postSearchAry['szOriginCountry'] = $seoDetailsArys['szFromSelection'];
                        $bGetDefaultValues = false;
                    }   
                    if($seoDetailsArys['iToIPLocation']==1)
                    {
                        if(!empty($_COOKIE['__USER_IP_DETAILS___']))
                        {
                            $userIpDetailsStr = $_COOKIE['__USER_IP_DETAILS___'] ;
                            $userIpDetailsAry = array();
                            $userIpDetailsAry = unserialize($userIpDetailsStr);
                            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;  
                        }
                        else
                        {   
                            $userIpDetailsAry = array();
                            $userIpDetailsAry = getCountryCodeByIPAddress();

                            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ; 
                            $userIpDetailsStr = serialize($userIpDetailsAry);
                            
                            //setting cookien variable
                            if($_COOKIE['__USER_COUNTRY_NAME___']!=$szUserCountryName)
                            {
                                setCookie('__USER_COUNTRY_NAME___', $szUserCountryName ,time()+3600, '', $_SERVER['HTTP_HOST']);
                            }  
                            if($_COOKIE['__USER_IP_DETAILS___']!=$userIpDetailsStr)
                            {
                                setCookie('__USER_IP_DETAILS___', $userIpDetailsStr ,time()+3600, '', $_SERVER['HTTP_HOST']);  
                            } 
                        } 
                        
                        $visitor_location = array();
                        $visitor_location['CityName'] = $userIpDetailsAry['szCityName'] ;
                        $visitor_location['RegionName'] = $userIpDetailsAry['szRegionName'] ;
                        $visitor_location['szZipCode'] = $userIpDetailsAry['szZipCode'] ;
                        $visitor_location['szCountryName'] = $userIpDetailsAry['szCountryName'] ;
                         
                        $toAddress='';
                        if(!empty($visitor_location['szZipCode']))
                        {
                            $toAddress =$visitor_location['szZipCode']." ";
                        }
                        if(!empty($visitor_location['CityName']))
                        {
                            $toAddress .=$visitor_location['CityName'].", ";
                        } 
                        if(!empty($visitor_location['RegionName']))
                        {
                            //$toAddress .=$visitor_location['RegionName'].", ";
                        } 
                        $toAddress .= $visitor_location['szCountryName'];

                        $postSearchAry['szDestinationCountryStr']=ucwords(strtolower($toAddress));
                        $postSearchAry['szDestinationCountry']=ucwords(strtolower($toAddress));
                        $bGetDefaultValues = false;
                    }
                    else if(!empty($seoDetailsArys['szToSelection']))
                    {
                        $postSearchAry['szDestinationCountryStr'] = $seoDetailsArys['szToSelection'];
                        $postSearchAry['szDestinationCountry'] = $seoDetailsArys['szToSelection'];
                        $bGetDefaultValues = false;
                    }   
                    
                    if($seoDetailsArys['fCargoVolume']>0)
                    {
                        $postSearchAry['fCargoVolume'] = $seoDetailsArys['fCargoVolume'];
                    }
                    else
                    {
                        $postSearchAry['fCargoVolume']=__DEFAULT_CARGO_VOLUME__; 
                    }  
                    if($seoDetailsArys['fCargoWeight']>0)
                    {
                        $postSearchAry['fCargoWeight'] = $seoDetailsArys['fCargoWeight'];
                    }
                    else if($fStaticDefaultWeight>0)
                    {
                        $postSearchAry['fCargoWeight'] = $fStaticDefaultWeight;
                    }
                    else
                    {
                        $postSearchAry['fCargoWeight']=__DEFAULT_CARGO_WEIGHT__;
                    } 
                }
            } 
            if($bGetDefaultValues)
            {
                if(!empty($_COOKIE['__USER_IP_DETAILS___']))
                {
                    $userIpDetailsStr = $_COOKIE['__USER_IP_DETAILS___'] ;
                    $userIpDetailsStr = stripslashes($userIpDetailsStr);
                    $userIpDetailsAry = array();
                    $userIpDetailsAry = unserialize($userIpDetailsStr);
                    $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ;  
                }
                else
                {   
                    $userIpDetailsAry = array();
                    $userIpDetailsAry = getCountryCodeByIPAddress();

                    $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ; 
                    $userIpDetailsStr = serialize($userIpDetailsAry);
                    
                    //setting cookien variable
                    if($_COOKIE['__USER_COUNTRY_NAME___']!=$szUserCountryName)
                    {
                        setCookie('__USER_COUNTRY_NAME___', $szUserCountryName ,time()+3600, '', $_SERVER['HTTP_HOST']);
                    }  
                    if($_COOKIE['__USER_IP_DETAILS___']!=$userIpDetailsStr)
                    {
                        setCookie('__USER_IP_DETAILS___', $userIpDetailsStr ,time()+3600, '', $_SERVER['HTTP_HOST']);  
                    } 
                }
                
                $visitor_location = array();
                $visitor_location['CityName'] = $userIpDetailsAry['szCityName'] ;
                $visitor_location['RegionName'] = $userIpDetailsAry['szRegionName'] ;
                $visitor_location['szZipCode'] = $userIpDetailsAry['szZipCode'] ;
                $visitor_location['szCountryName'] = $userIpDetailsAry['szCountryName'] ;

                if(trim($szUserCountryName)=='DK')
                {
                    $toAddress='';
                    if(!empty($visitor_location['szZipCode']))
                    {
                        $toAddress =$visitor_location['szZipCode']." ";
                    }
                    if(!empty($visitor_location['CityName']))
                    {
                        $toAddress .=$visitor_location['CityName'].", ";
                    } 
                    if(!empty($visitor_location['RegionName']))
                    {
                        //$toAddress .=$visitor_location['RegionName'].", ";
                    } 
                    $toAddress .= $visitor_location['szCountryName'];

                    $postSearchAry['szDestinationCountryStr']=ucwords(strtolower($toAddress));
                    $postSearchAry['szDestinationCountry']=ucwords(strtolower($toAddress));
                }
                else
                {
                    $button_flag=false;
                }

                if(!empty($szDefaultFromField))
                {
                    $postSearchAry['szOriginCountryStr'] = $szDefaultFromField;
                    $postSearchAry['szOriginCountry'] = $szDefaultFromField;
                }
                else
                {
                    $postSearchAry['szOriginCountryStr'] = "Shanghai, China";
                    $postSearchAry['szOriginCountry'] = "Shanghai, China"; 
                } 

                $postSearchAry['fCargoVolume']=__DEFAULT_CARGO_VOLUME__;
                if($fStaticDefaultWeight>0)
                {
                    $postSearchAry['fCargoWeight'] = $fStaticDefaultWeight;
                }
                else
                {
                    $postSearchAry['fCargoWeight']=__DEFAULT_CARGO_WEIGHT__;
                } 
            }   
	}
	if(!empty($_COOKIE['__SIMPLE_SEARCH_COOKIE__']) && (empty($postSearchAry) || $iSearchMiniPage==8 || $iSearchMiniPage==9))
	{	 
            
            //echo "hello";
            //print_r($_COOKIE['__SIMPLE_SEARCH_COOKIE__']);
            $simpleSearchCookieArr=explode("||||",$_COOKIE['__SIMPLE_SEARCH_COOKIE__']);
            if($iSearchMiniPage!=8){
                $today=date('Y-m-d');
                $dtTiming=date('Y-m-d',strtotime($simpleSearchCookieArr[7]));
                if(strtotime($dtTiming)<strtotime($today))
                {
                    $postSearchAry['dtTimingDate']=$today;
                }
                else
                {
                    $postSearchAry['dtTimingDate']=$dtTiming;
                }  
                //$postSearchAry['idServiceType']=$simpleSearchCookieArr[4];
                $postSearchAry['idServiceType'] = __SERVICE_TYPE_PTD__ ; //From cookie we always search for PTD
                $postSearchAry['fCargoVolume']=$simpleSearchCookieArr[5];
                $postSearchAry['fCargoWeight']=$simpleSearchCookieArr[6]; 
                $bPickedDataFromCookie = true;                 
            }
            $postSearchAry['szOriginCountryStr']=ucwords(strtolower($simpleSearchCookieArr[0]));
            $postSearchAry['szOriginCountry']=ucwords(strtolower($simpleSearchCookieArr[0]));
            $postSearchAry['szDestinationCountryStr']=ucwords(strtolower($simpleSearchCookieArr[2]));
            $postSearchAry['szDestinationCountry']=ucwords(strtolower($simpleSearchCookieArr[2]));
            if($iSearchMiniPage==8 || $iSearchMiniPage==9){
                $szOriginCountryStr=$postSearchAry['szOriginCountryStr'];
                $szDestinationCountryStr=$postSearchAry['szDestinationCountryStr'];
            }
	} 
	if(isset($_POST['searchAry']['szOriginCountryStr']))
	{
            $szOriginCountryStr = ucwords(strtolower(sanitize_all_html_input($_POST['searchAry']['szOriginCountryStr'])));
	}
	else
	{
            $szOriginCountryStr = ucwords(strtolower($postSearchAry['szOriginCountry'])) ;
	} 
			
	if(isset($_POST['searchAry']['szDestinationCountryStr']))
	{
            $szDestinationCountryStr = ucwords(strtolower(sanitize_all_html_input($_POST['searchAry']['szDestinationCountryStr'])));
	}
	else
	{
            $szDestinationCountryStr = ucwords(strtolower($postSearchAry['szDestinationCountry'])) ;
	} 
	if(isset($_POST['searchAry']['iVolume']))
	{
            $fCargoVolume = sanitize_all_html_input($_POST['searchAry']['iVolume']);
	}
	else if(isset($postSearchAry['fCargoVolume']))
	{
            $fCargoVolume = format_volume((float)$postSearchAry['fCargoVolume']);
	}
	else
	{
            $fCargoVolume = __DEFAULT_CARGO_VOLUME__ ;
	}
                    
	if(isset($_POST['searchAry']['iWeight']))
	{
            $iWeight = sanitize_all_html_input($_POST['searchAry']['iWeight']);
	}
	else if(isset($postSearchAry['fCargoWeight']))
	{
            $iWeight = round((float)$postSearchAry['fCargoWeight'],3);
	}
	else if($fStaticDefaultWeight>0)
        {
            $iWeight = $fStaticDefaultWeight;
        }
        else
	{
            $iWeight = __DEFAULT_CARGO_WEIGHT__ ;
	}
	
	if(isset($_POST['searchAry']['idServiceType']))
	{
            $idServiceType = sanitize_all_html_input($_POST['searchAry']['idServiceType']);
	}
	else if(isset($postSearchAry['idServiceType']))
	{
            $idServiceType = (int)$postSearchAry['idServiceType'];
	}
	else
	{
            $idServiceType = __SERVICE_TYPE_PTD__ ;
	}   
        $isMobileValue= checkMobileDevice();
        if((int)$isMobileValue==1)
        {
            if(isset($_POST['searchAry']['idDefaultLandingPage']))
            {
                $idDefaultLandingPage = $_POST['searchAry']['idDefaultLandingPage'];
            } 

            if($postSearchAry['dtTimingDate']!='0000-00-00 00:00:00' && !empty($postSearchAry['dtTimingDate']))
            {
                $dtTimingDate = date('Y-m-d',strtotime($postSearchAry['dtTimingDate']));
               // $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
            }
            else
            {
                $dtTimingDate = date('Y-m-d');
                $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
            }
        }
	else
        {
            if(isset($_POST['searchAry']['idDefaultLandingPage']))
            {
                $idDefaultLandingPage = $_POST['searchAry']['idDefaultLandingPage'];
            } 

            if($postSearchAry['dtTimingDate']!='0000-00-00 00:00:00' && !empty($postSearchAry['dtTimingDate']))
            {
                $dtTimingDate = date('d/m/Y',strtotime($postSearchAry['dtTimingDate']));
                $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
            }
            else
            {
                $dtTimingDate = date('d/m/Y');
                $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
            }
        }
	 
	$t_base = "home/homepage/";
	
	$kWHSSearch=new cWHSSearch();
        
        $arrDescriptions = array("'__MAX_CARGO_DIMENSION_LENGTH__'","'__MAX_CARGO_DIMENSION_WIDTH__'","'__MAX_CARGO_DIMENSION_HEIGHT__'","'__MAX_CARGO_WEIGHT__'","'__MAX_CARGO_QUANTITY__'","'__MAX_CARGO_VOLUME__'"); 
        $bulkManagemenrVarAry = array();
        $bulkManagemenrVarAry = $kWHSSearch->getBulkManageMentVariableByDescription($arrDescriptions);    
                    
	$maxCargoLength = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_LENGTH__'];
	$maxCargoWidth = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_WIDTH__'];  
	$maxCargoHeight = $bulkManagemenrVarAry['__MAX_CARGO_DIMENSION_HEIGHT__'];  
	$maxCargoWeight = $bulkManagemenrVarAry['__MAX_CARGO_WEIGHT__']; 
	$maxCargoQty = $bulkManagemenrVarAry['__MAX_CARGO_QUANTITY__']; 
	$maxVolume = $bulkManagemenrVarAry['__MAX_CARGO_VOLUME__']; 
	
	$szClass_update='';
	if($service_page_flag || $updateButtonFlagSearchMini)
	{
            $szClass_update = "update-btn";
	}  
	if(isset($_POST['searchAry']['iBookingQuote']))
	{
            $iBookingQuote = $_POST['searchAry']['iBookingQuote'];
	}
	else if($bBookingQuotes)
	{
            $iBookingQuote = 1 ;
	} 
        if(isset($_POST['searchAry']['iPageVersion']))
	{
            $iPageVersion = $_POST['searchAry']['iPageVersion'];
	}
	else if($bTestPage)
	{
            $iPageVersion = $bTestPage ;
	}
        
       
        if((int)$isMobileValue==1)
        {
            $szSearchButtonText = t($t_base.'fields/search_price_mobile_button');
        }
        else
        {
            if($iSearchMiniPage)
            {
                if($iSearchMiniPage==5)
                {
                    $szSearchButtonText = t($t_base.'fields/search');  
                }
                else if($iSearchMiniPage==7 || $iSearchMiniPage==8 || $iSearchMiniPage==9)
                {
                    $szSearchButtonText = t($t_base.'fields/get_price_button');  
                }
                else
                {
                   $szSearchButtonText = t($t_base.'fields/get_rates');  
                } 
            }
            else
            {
                $szSearchButtonText = t($t_base.'fields/search');
            }
        }
	if($iBookingQuote==1)
	{
            $szInput_class="booking-quote";
	} 
        $iLanguage = getLanguageId();  
        $kConfig = new cConfig();
        $palletTypesAry = array();
        $palletTypesAry = $kConfig->getAllPalletTypes(false,$iLanguage);
    
    
    $typeForDate='text'; 
    $typeVolWeightDate='text';
    if((int)$isMobileValue==1)
    {
        $typeForDate='date';
        $typeVolWeightDate='number';
    }
    
    if($iShipperConsigneeFlag==1)
    {
        $iPickupDefaultValue_option1 = t($t_base.'fields/pick_no')." - ".t($t_base.'fields/cfr');
        $iPickupDefaultValue_option2 = t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/dap');
        $iPickupDefaultValue_option3 = "";

        $iPickupDropdownValue_option1 = t($t_base.'fields/pick_no');
        $iPickupDropdownValue_option2 = t($t_base.'fields/pick_yes');
        $iPickupDropdownValue_option3 = "";
    } 
    else if($iShipperConsigneeFlag==3)
    {
        $iPickupDefaultValue_option1 = t($t_base.'fields/fob');
        $iPickupDefaultValue_option2 = t($t_base.'fields/cfr');
        $iPickupDefaultValue_option3 = t($t_base.'fields/dtd');

        $iPickupDropdownValue_option1 = t($t_base.'fields/fob');
        $iPickupDropdownValue_option2 = t($t_base.'fields/cfr');
        $iPickupDropdownValue_option3 = t($t_base.'fields/dtd');
    }
    else
    {
        $iPickupDefaultValue_option1 = t($t_base.'fields/pick_no')." - ".t($t_base.'fields/fob');
        $iPickupDefaultValue_option2 = t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/exw');
        $iPickupDefaultValue_option3 = "";

        $iPickupDropdownValue_option1 = t($t_base.'fields/pick_no');
        $iPickupDropdownValue_option2 = t($t_base.'fields/pick_yes');
        $iPickupDropdownValue_option3 = "";
    }
?> 
<form id="landing_page_form<?php echo $id_suffix; ?>" name="landing_page_form<?php echo $id_suffix; ?>" method="post" action="javascript:void(0);">  
<?php if($iSearchMiniPage!=7){ ?> 
<script type="text/javascript" defer="">
$().ready(function() {	 
    var autocomplete1,place ; 
    function initialize1() 
    {
        var input1 = document.getElementById('szOriginCountryStr<?php echo $id_suffix; ?>');
        var options = {types: ['regions']};
        var len = googleApiCall.length; 
        googleApiCall[len]= 'szOriginCountryStrPacContainer<?php echo $id_suffix; ?>';
        var autocomplete1 = new google.maps.places.Autocomplete(input1); 
        $(".pac-container:last").attr("id", 'szOriginCountryStrPacContainer<?php echo $id_suffix; ?>' );
        google.maps.event.addListener(autocomplete1, 'place_changed', function() 
        {    
            var szOriginAddress_js = $("#szOriginCountryStr<?php echo $id_suffix; ?>").val();   
            checkFromAddress(szOriginAddress_js,'FROM_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');
        }); 
        var input2 = document.getElementById('szDestinationCountryStr<?php echo $id_suffix; ?>');
        var autocomplete2 = new google.maps.places.Autocomplete(input2); 
        $(".pac-container:last").attr("id", 'szDestinationCountryStrPacContainer<?php echo $id_suffix; ?>' );
        
        var len = googleApiCall.length; 
        googleApiCall[len]= 'szDestinationCountryStrPacContainer<?php echo $id_suffix; ?>';
        google.maps.event.addListener(autocomplete2, 'place_changed', function() 
        {
            var szDestinationAddress_js = $("#szDestinationCountryStr<?php echo $id_suffix; ?>").val();  
            checkFromAddress(szDestinationAddress_js,'TO_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');
        }); 
        
        $('#szOriginCountryStr<?php echo $id_suffix; ?>').bind('paste focus', function (e){  
            $('#szOriginCountryStr<?php echo $id_suffix; ?>').bind('keydown keyup keypress click', function (e) {  
                var pac_container_id = 'szOriginCountryStrPacContainer<?php echo $id_suffix; ?>';
                if($("#"+pac_container_id).length)
                {
                    //This means id already there no need for further checks
                }
                else
                {
                    $(".pac-container").each(function(pac)
                    {
                        var disp = $(this).css('display'); 
                        if(disp!='none')
                        {
                            $(this).attr('id',pac_container_id);
                        }
                    });
                }
                var key_code = e.keyCode || e.which;
                if (key_code == 9 || key_code == 13) {
                    prefillAddressOnTabPress('szOriginCountryStr<?php echo $id_suffix; ?>',pac_container_id);
                }  
            }); 
        });
        
        
        $('#szDestinationCountryStr<?php echo $id_suffix; ?>').bind('paste focus', function (e){  
            $('#szDestinationCountryStr<?php echo $id_suffix; ?>').bind('keydown keyup keypress click', function (e) {  
                var pac_container_id = 'szDestinationCountryStrPacContainer<?php echo $id_suffix; ?>';
                if($("#"+pac_container_id).length)
                {
                    //This means id already there no need for further checks 
                    //console.log("div exist called");
                }
                else
                {
                    var iFoundDiv = 1;
                    $(".pac-container").each(function(pac)
                    {
                        var disp = $(this).css('display');  
                        if(disp!='none')
                        {
                            $(this).attr('id',pac_container_id);  
                            iFoundDiv = 2;
                        } 
                    });
                    
                    if(iFoundDiv==1)
                    {
                        $(".pac-container").each(function(pac)
                        {
                            var container_id = $(this).attr('id');  
                            if(container_id=='' || container_id === undefined)
                            {
                                $(this).attr('id',pac_container_id); 
                                return true;
                            }
                        });
                    }
                } 
                if (e.keyCode == 9 || e.keyCode == 13) {
                    prefillAddressOnTabPress('szDestinationCountryStr<?php echo $id_suffix; ?>',pac_container_id);
                }  
            });
        }); 
        console.log($(".pac-container").length+"pac-container");
    } 
    function prefillAddressOnTabPress(input_field_id,pac_container_id)
    {  
        $(".pac-container").each(function(pac){
           var disp = $(this).css('display'); 
           var contaier_id = $(this).attr('id');  
           
           console.log("Container: "+contaier_id + " pac id: "+pac_container_id);
           
           if(contaier_id==pac_container_id)
           {
                var spanObj = $(this).children(".pac-item:first" );
                var firstResult = ""; 
                spanObj.children("span").each(function(){
                    var spanText = $(this).text(); 
                    spanText = jQuery.trim(spanText);
                    console.log("Span Text: "+spanText);
                    if(spanText!='')
                    {
                        if(firstResult=='')
                        {
                            firstResult += spanText;
                        }
                        else
                        {
                            firstResult += ", "+ spanText;
                        }
                        //$(this).children( ".pac-item:first" ).addClass("pac-selected");
                        //$(this).css("display","none");
                        $("#"+input_field_id).val(firstResult); 
                        $("#"+input_field_id).select();
                    }
                }); 
                
           }
        }); 
    } 
    function clean_pac_container()
    { 
        $("div").each(function(index){
            if($( this ).hasClass("pac-container"))
            {
                $( this ).remove();
            }
        }); 
    }
    initialize1(); 
   <?php
   if((int)$isMobileValue!=1)
    {?>
// google.maps.event.addDomListener(window, 'load', initialize1);
    $("#datepicker1_search_services<?php echo $id_suffix; ?>").bind('keydown', function (event) {
        if(event.which == 13){
            var e = jQuery.Event("keydown");
            e.which = 9;//tab 
            e.keyCode = 9;
            $(this).trigger(e);
            return false;
        }
    }).datepicker({ <?=$date_picker_argument?> });
    
    <?php } if($iSearchMiniPage==5 || $iSearchMiniPage==6){ ?>   
        timesSelectClicked = 0; 
        var browser = {
                chrome: false,
                mozilla: false,
                opera: false,
                msie: false,
                safari: false
            };
            var sUsrAg = navigator.userAgent;
            if(sUsrAg.indexOf("Chrome") > -1) {
                browser.chrome = true;
            } else if (sUsrAg.indexOf("Safari") > -1) {
                browser.safari = true;
            } else if (sUsrAg.indexOf("Opera") > -1) {
                browser.opera = true;
            } else if (sUsrAg.indexOf("Firefox") > -1) {
                browser.mozilla = true;
            } else if (sUsrAg.indexOf("MSIE") > -1) {
                browser.msie = true;
            }
            console.log(browser.chrome);


        $("#iPalletType_V_"+'<?php echo $iSearchMiniPage;?>').bind('click keypress', function (e) {
            var type_val = $("#iPalletType_V_5").val();
            var userAgent = navigator.userAgent;
            //console.log("Clicked: "+timesSelectClicked+" type: "+type_val+ " Agent: "+userAgent); 
            if(browser.safari)
            {
                if(timesSelectClicked == 0)
                {
                    timesSelectClicked = 0; 
                    autofill_pallet_type(type_val,'<?php echo $iSearchMiniPage; ?>');
                }
            }
            else
            {
                if(timesSelectClicked == 0)
                {
                    timesSelectClicked += 1;
                }
                else if (timesSelectClicked == 1)
                {
                    timesSelectClicked = 0; 
                    autofill_pallet_type(type_val,'<?php echo $iSearchMiniPage; ?>');
                }
            } 
        }); 
    <?php } ?>
}); 

var palletTypesAry = new Array();
<?php 
    if(!empty($palletTypesAry))
    {
        foreach($palletTypesAry as $palletTypesArys)
        {
            ?>
            palletTypesAry['<?php echo $palletTypesArys['id'] ?>'] = '<?php echo $palletTypesArys['szShortName']; ?>';
            <?php
        }
    } 
?> 
function autofill_pallet_type(type_id,search_mini_version)   
{
    var idPalletType = parseInt(type_id); 
    console.log("Type: "+type_id+" Mini: "+search_mini_version);
    if(palletTypesAry[idPalletType].length)
    { 
        $("#selectiPalletType_V_"+search_mini_version).html(palletTypesAry[idPalletType]); 
        if(idPalletType==3)
        {
            $("#iNumColli_V_"+search_mini_version).attr('readonly','readonly');
            display_pallet_details_popup(search_mini_version);
        }
        else
        {
            $("#iNumColli_V_"+search_mini_version).removeAttr('readonly');
        }
    }
}  
</script> 
<?php  
}
    if($bSearchMini && $iSearchMiniPage>2)
    {
        $id_suffix = "_V_".$iSearchMiniPage;  
        $szVersion5Clearfix = " clearfix";  
        $dialUpCodeAry = array();
        $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
        ?> 
        <div class="search <?php echo $szInput_class; ?><?php echo $szWithoutPickUpCLass; ?> clearfix" id="search_header">
            <?php if($iSearchMiniPage==7 || $iSearchMiniPage==8 || $iSearchMiniPage==9){  $idServiceType = __SERVICE_TYPE_DTD__; ?>
            <div class="search-mini-v3-fields-container clearfix">
                <div class="contact-feilds-container clearfix"> 
                    <div class="fisrt-name">
                        <div id="szFirstName<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szFirstName]" id="szFirstName<?php echo $id_suffix; ?>" placeholder="<?php echo t($t_base.'fields/f_name');?>" value="<?php echo $szFirstName;?>" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szFirstName<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/f_name');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szFirstName<?php echo $id_suffix; ?>_container');"></div>
                    </div> 
                    <div class="last-name">
                        <div id="szLastName<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szLastName]" id="szLastName<?php echo $id_suffix; ?>" placeholder="<?php echo t($t_base.'fields/l_name');?>" value="<?php echo $szLastName;?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szLastName<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/l_name');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szLastName<?php echo $id_suffix; ?>_container');"></div>
                    </div>
                    <div class="email">
                        <div id="szEmail<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szEmail]" id="szEmail<?php echo $id_suffix; ?>" value="<?php echo $szEmail;?>" placeholder="<?php echo t($t_base.'fields/email');?>" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szEmail<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/email');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szEmail<?php echo $id_suffix; ?>_container');"></div>
                    </div> 
                    <div class="phone">
                        <div id="idDialCode<?php echo $id_suffix; ?>_container" class="input-fields">
                            <select size="1" name="searchAry[IDDialCode]" class="styled" id="idDialCode"  onchange="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onblur="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" autocomplete="off">
                                <?php
                                   if(!empty($dialUpCodeAry))
                                   {
                                       $usedDialCode = array();
                                       foreach($dialUpCodeAry as $dialUpCodeArys)
                                       {
                                            if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                            {
                                                $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                            }
                                       }
                                   }
                               ?>
                            </select>
                        </div> 
                        <div id="szPhone<?php echo $id_suffix; ?>_container" class="shipper-phone input-box"><input type="text" name="searchAry[szPhone]" id="szPhone<?php echo $id_suffix; ?>" placeholder="<?php echo t($t_base.'fields/phone_vog');?>" value="<?php echo $szPhone;?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szPhone<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/phone');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szPhone<?php echo $id_suffix; ?>_container');"></div> 
                    </div>
                </div>
                <?php 
                    if($iSearchMiniPage==8 || $iSearchMiniPage==9)
                    { 
                        /*
                         * For search mini V: 8, From fields popuplated from  
                         */
                        if(isset($_POST['searchAry']['szOriginCountryStr']))
                        {
                            $szOriginCountryStr = ucwords(strtolower(sanitize_all_html_input($_POST['searchAry']['szOriginCountryStr'])));
                        }
                        else
                        {
                            if($szOriginCountryStr==''){
                            $szOriginCountryStr = $postSearchAry['szFromInstruction'] ;
                            }
                            
                        } 
                        //echo $szOriginCountryStr."szOriginCountryStr";
                        if(isset($_POST['searchAry']['szDestinationCountryStr']))
                        {
                            $szDestinationCountryStr = ucwords(strtolower(sanitize_all_html_input($_POST['searchAry']['szDestinationCountryStr'])));
                        }
                        else
                        {
                            if($szDestinationCountryStr==''){                                
                                $szDestinationCountryStr = $postSearchAry['szToInstruction'] ;
                            }
                        }
                    ?>
                        <div class="voga-automatic-location-container clearfix">
                            <div class="explanation-from"> 
                                <div id="szOriginCountryStr<?php echo $id_suffix; ?>_container" class="input-box">
                                    <input type="text" name="searchAry[szOriginCountryStr]" placeholder="<?php echo t($t_base.'fields/from_explanation'); ?>" id="szOriginCountryStr<?php echo $id_suffix; ?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/from');?>');openSearchNotificationPopup(this.form.id,'<?php echo $id_suffix; ?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $service_page_flag;?>','<?php echo $iHiddenValue; ?>','<?php echo $iSearchMiniPage; ?>');" value="<?php echo $szOriginCountryStr;?>" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="reste_search_flags('FROM_COUNTRY');autoSelectTexts(this.id);check_form_field_not_required(this.form.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container');" onclick="autoSelectTexts(this.id);"/> 
                                </div>
                            </div>
                            <div class="explanation-to">  
                                <div id="szDestinationCountryStr<?php echo $id_suffix; ?>_container" class="input-box">
                                    <input type="text" name="searchAry[szDestinationCountryStr]" placeholder="<?php echo t($t_base.'fields/to_explanation'); ?>" id="szDestinationCountryStr<?php echo $id_suffix; ?>" value="<?php echo $szDestinationCountryStr;?>" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/to');?>');openSearchNotificationPopup(this.form.id,'<?php echo $id_suffix; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="reste_search_flags('TO_COUNTRY');autoSelectTexts(this.id);check_form_field_not_required(this.form.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container');" onclick="autoSelectTexts(this.id);" /> 
                                </div>
                            </div>
                        </div>
                <?php } else {?>
                    <div class="location-feilds-container clearfix"> 
                        <div class="address">
                            <div id="szAddress<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szAddress]" id="szAddress<?php echo $id_suffix; ?>" value="<?php echo $szAddress;?>" placeholder="<?php echo t($t_base.'fields/address');?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szAddress<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/address');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szAddress<?php echo $id_suffix; ?>_container');"></div>
                        </div> 
                        <div class="postcode">
                            <div id="szPostcode<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szPostcode]" id="szPostcode<?php echo $id_suffix; ?>" value="<?php echo $szPostcode;?>"  placeholder="<?php echo t($t_base.'fields/postcode');?>" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szPostcode<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/postcode');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szPostcode<?php echo $id_suffix; ?>_container');"></div>
                        </div>
                        <div class="city">
                            <div id="szCity<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szCity]" id="szCity<?php echo $id_suffix; ?>" value="<?php echo $szCity;?>" placeholder="<?php echo t($t_base.'fields/city');?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szCity<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/city');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szCity<?php echo $id_suffix; ?>_container');"></div>
                        </div>
                        <div class="shipping-cb"> 
                            <div class="checkbox-container-custom"><span id="iDeliveryAddress<?php echo $id_suffix; ?>_container" onclick="enableDiableVogaPageNewDeliveryAddress('iNewDeliveryAddress');"><input name="searchAry[iDeliveryAddress]" class="styled" id="iDeliveryAddress<?php echo $id_suffix; ?>" value="1" type="checkbox" checked="checked" ></span><?php echo t($t_base.'fields/this_is_delvery_address');?></div>
                        </div>
                    </div>
                    <div id="iNewDeliveryAddress" style="display:none;">
                        <h5><?php echo t($t_base.'fields/this_is_delvery_address');?></h5>
                        <div class="contact-feilds-container clearfix"> 
                            <div class="fisrt-name">
                                <div id="szConsigneeFirstName<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szConsigneeFirstName]" id="szConsigneeFirstName<?php echo $id_suffix; ?>" placeholder="<?php echo t($t_base.'fields/f_name');?>" value="<?php echo $szConsigneeFirstName;?>" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szConsigneeFirstName<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/f_name');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szConsigneeFirstName<?php echo $id_suffix; ?>_container');"></div>
                            </div> 
                            <div class="last-name">
                                <div id="szConsigneeLastName<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szConsigneeLastName]" id="szConsigneeLastName<?php echo $id_suffix; ?>" placeholder="<?php echo t($t_base.'fields/l_name');?>" value="<?php echo $szConsigneeLastName;?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szConsigneeLastName<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/l_name');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szConsigneeLastName<?php echo $id_suffix; ?>_container');"></div>
                            </div>
                            <div class="email">
                                <div id="szConsigneeEmail<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szConsigneeEmail]" id="szEmail<?php echo $id_suffix; ?>" value="<?php echo $szConsigneeEmail;?>" placeholder="<?php echo t($t_base.'fields/email');?>" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szConsigneeEmail<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/email');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szConsigneeEmail<?php echo $id_suffix; ?>_container');"></div>
                            </div> 
                            <div class="phone">
                                <div id="idConsigneeDialCode<?php echo $id_suffix; ?>_container" class="input-fields">
                                    <select size="1" name="searchAry[IDConsigneeDialCode]" class="styled" id="idConsigneeDialCode"  onchange="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onblur="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" autocomplete="off">
                                        <?php
                                            if(!empty($dialUpCodeAry))
                                            {
                                                $usedDialCode = array();
                                                foreach($dialUpCodeAry as $dialUpCodeArys)
                                                {
                                                    if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                                    {
                                                        $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                        ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idConsigneeDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                                    }
                                                }
                                            }
                                       ?>
                                    </select>
                                </div> 
                                <div id="szConsigneePhone<?php echo $id_suffix; ?>_container" class="shipper-phone input-box"><input type="text" name="searchAry[szConsigneePhone]" id="szConsigneePhone<?php echo $id_suffix; ?>" placeholder="<?php echo t($t_base.'fields/phone_vog');?>" value="<?php echo $szConsigneePhone;?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szConsigneePhone<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/phone');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szConsigneePhone<?php echo $id_suffix; ?>_container');"></div> 
                            </div>
                        </div> 
                        <div class="location-feilds-container clearfix"> 
                            <div class="address">
                                <div id="szConsigneeAddress<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szConsigneeAddress]" id="szConsigneeAddress<?php echo $id_suffix; ?>" value="<?php echo $szConsigneeAddress;?>" placeholder="<?php echo t($t_base.'fields/address');?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szConsigneeAddress<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/address');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szConsigneeAddress<?php echo $id_suffix; ?>_container');"></div>
                            </div> 
                            <div class="postcode">
                                <div id="szConsigneePostcode<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szConsigneePostcode]" id="szConsigneePostcode<?php echo $id_suffix; ?>" value="<?php echo $szConsigneePostcode;?>"  placeholder="<?php echo t($t_base.'fields/postcode');?>" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szConsigneePostcode<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/postcode');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szConsigneePostcode<?php echo $id_suffix; ?>_container');"></div>
                            </div>
                            <div class="city">
                                <div id="szConsigneeCity<?php echo $id_suffix; ?>_container" class="input-box"><input type="text" name="searchAry[szConsigneeCity]" id="szConsigneeCity<?php echo $id_suffix; ?>" value="<?php echo $szConsigneeCity;?>" placeholder="<?php echo t($t_base.'fields/city');?>"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szConsigneeCity<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/city');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="check_form_field_not_required(this.form.id,'szConsigneeCity<?php echo $id_suffix; ?>_container');"></div>
                            </div>
                            <div class="country">
                                <div class="input-fields"> 
                                    <select name="searchAry[idConsigneeCountry]" class="styled" id="idConsigneeCountry"  onblur="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                        <option value=""><?php echo t($t_base.'fields/select'); ?></option>
                                        <?php
                                            if(!empty($dialUpCodeAry))
                                            {
                                                $dialUpCodeAry = sortArray($dialUpCodeAry,'szCountryName') ;
                                                foreach($dialUpCodeAry as $dialUpCodeArys)
                                                {  
                                                    ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idConsigneeCountry){?> selected <?php }?>><?=$dialUpCodeArys['szCountryName']?></option><?php 
                                                }
                                            }
                                       ?>
                                    </select>
                                </div>
                            </div>
                        </div> 
                    </div>
                <?php } ?>
                <div class="vog-feilds-container clearfix"> 
                    <div class="cargo-heading-v7"><?php echo $szMiddleText;?></div>
                    <div id="update_cargo_details_container_div" class="search-min-v7-cargo-field">
                        <?php echo display_vog_cargo_fields($kBooking,$postSearchAry,$iSearchMiniPage); ?>
                    </div> 
                    <div class="contact-shipper-cb cargo-heading-v7"> 
                        <div class="checkbox-container-custom"><span id="iContactShipper<?php echo $id_suffix; ?>_container"><input name="searchAry[iContactShipper]" class="styled" id="iContactShipper<?php echo $id_suffix; ?>" value="1" <?php if($iContactShipper==1){ echo 'checked'; } ?> type="checkbox"></span><?php echo $szContactMe;?></div>
                    </div>
                    <div class="search-min-v3-button-cotainer">
                        <div <?php if($button_flag){ ?> class="btn <?php echo $szClass_update; ?>" <?php }else{?> class="btn-grey" <?php }?> id="search-btn-container<?php echo $id_suffix; ?>"> 
                            <a href="javascript:void(0);" id="search_listing<?php echo $id_suffix; ?>" onclick="return validateNewLandingPageForm('landing_page_form<?php echo $id_suffix; ?>','<?php echo $_SESSION['user_id']; ?>','<?php echo __NEW_SERVICES_PAGE_URL__?>','<?php echo $iHiddenValue; ?>','<?php echo $iSearchMiniPage; ?>');"><?php echo $szSearchButtonText; ?></a> 
                        </div>  
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            <div class="search-mini-v3-fields-container clearfix"> 
                <div class="location-container">
                    <div class="from">
                        <label><?=t($t_base.'fields/from');?></label>
                        <div id="szOriginCountryStr<?php echo $id_suffix; ?>_container">
                            <input type="text" name="searchAry[szOriginCountryStr]" id="szOriginCountryStr<?php echo $id_suffix; ?>"  onblur="check_location(this.id,'FROM_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');check_form_field_empty_standard_search(this.form.id,this.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/from');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" value="<?php echo $szOriginCountryStr;?>" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="reste_search_flags('FROM_COUNTRY');autoSelectTexts(this.id);check_form_field_not_required(this.form.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container');" onclick="autoSelectTexts(this.id);">
                            <span id="szOriginCountryStr<?php echo $id_suffix; ?>_clear_box" class="clear-data" onclick="fillAddressForIPAddress('szOriginCountryStr<?php echo $id_suffix; ?>');"><a>+</a></span>
                        </div>
                    </div> 
                    <div class="to">
                        <label><?=t($t_base.'fields/to');?></label>
                        <span class="updownarrow" onclick="swapFromToTextFieldValue('<?php echo $id_suffix; ?>');">up/down</span>
                        <div id="szDestinationCountryStr<?php echo $id_suffix; ?>_container" >
                            <input type="text" name="searchAry[szDestinationCountryStr]" id="szDestinationCountryStr<?php echo $id_suffix; ?>" value="<?php echo $szDestinationCountryStr;?>" onblur="check_location(this.id,'TO_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');check_form_field_empty_standard_search(this.form.id,this.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/to');?>');enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="reste_search_flags('TO_COUNTRY');autoSelectTexts(this.id);check_form_field_not_required(this.form.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container');" onclick="autoSelectTexts(this.id);">
                            <span id="szDestinationCountryStr<?php echo $id_suffix; ?>_clear_box" class="clear-data" onclick="fillAddressForIPAddress('szDestinationCountryStr<?php echo $id_suffix; ?>');"><a>+</a></span>
                        </div>
                    </div>
                </div>
                <div class="dimensions-container<?php echo $szVersion5Clearfix; ?>">  
                    <?php if($iSearchMiniPage==5 || $iSearchMiniPage==6){ ?>
                        <div class="date">
                            <label><?=t($t_base.'fields/date');?></label>
                            <div id="dtTiming<?php echo $id_suffix; ?>_container">
                                <input id="datepicker1_search_services<?php echo $id_suffix; ?>" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onblur="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" name="searchAry[dtTiming]"  type="<?php echo $typeForDate;?>" value="<?php echo $dtTimingDate; ?>" >
                                <input type="hidden" name="searchAry[idTimingType]" id="szTiming" value="1">
                                <input id="isMobileFlag<?php echo $id_suffix; ?>" name="searchAry[isMobileFlag]"  type="hidden" value="<?php echo (int)$isMobileValue; ?>"> 
                            </div>
                        </div>
                        <div class="kg" id="pallet-id">
                            <label><?=t($t_base.'fields/pallet');?></label>
                            <div id="iNumColli<?php echo $id_suffix; ?>_container">
                                <input id="iNumColli<?php echo $id_suffix; ?>" name="searchAry[iNumColli]" onkeypress="return isNumberKeySearch(event);" onblur="check_form_field_empty_standard_search(this.form.id,this.id,'iNumColli<?php echo $id_suffix; ?>_container');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');"  type="text" value="<?php echo $iNumColli; ?>" > 
                                <input type="hidden" name="searchAry[szPalletDescriptions]" id="szPalletDescriptions<?php echo $id_suffix; ?>" value="<?php echo $szPalletDescriptions; ?>">
                                <input type="hidden" name="searchAry[szPalletDimensions]" id="szPalletDimensions<?php echo $id_suffix; ?>" value="<?php echo $szPalletDimensions; ?>">
                            </div>
                        </div>
                        <div class="pickup">
                            <label><?=t($t_base.'fields/type');?></label>
                            <div style="text-align:center;" id="iPalletType<?php echo $id_suffix; ?>_container">				
                                <select name="searchAry[iPalletType]" class="styled" id="iPalletType<?php echo $id_suffix; ?>"> 
                                   <?php
                                    if(!empty($palletTypesAry))
                                    {
                                        foreach($palletTypesAry as $palletTypesArys)
                                        {
                                            if($iSearchMiniPage==5)
                                            {
                                                $szDisplayText = $palletTypesArys['szLongName'];
                                            }
                                            else
                                            {
                                                $szDisplayText = $palletTypesArys['szLongName'];
                                            }
                                            ?>
                                            <option id="pallet-type-options-<?php echo $palletTypesArys['id']."-".$id_suffix; ?>" value="<?php echo $palletTypesArys['id']; ?>" <?php echo (($iPalletType==$palletTypesArys['id'])?'selected':'');?> ><?php echo $szDisplayText; ?></option> 
                                            <?php
                                        }
                                    }
                                   ?>
                                </select> 
                            </div>
                        </div>
                        <div class="kg">
                            <label><?php echo ucfirst(t($t_base.'fields/kg'))." (".t($t_base.'fields/total').")";?></label>
                            <div id="iWeight<?php echo $id_suffix; ?>_container">
                                <input id="iWeight<?php echo $id_suffix; ?>" type="text"  onkeypress="return isNumberKeySearch(event);" onblur="check_form_field_empty_standard_search(this.form.id,this.id,'iWeight<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/weight');?>','1');" onkeyup="checkFromAddressWeightVolume(this.value,'WEIGHT','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?php echo t('Error/weight')." ".t('Error/must_be_more');?>','<?php echo t('Error/kg');?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" value="<?php echo $iWeight; ?>" name="searchAry[iWeight]" onfocus="check_form_field_not_required(this.form.id,'iWeight<?php echo $id_suffix; ?>_container');validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');"> 
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="date">
                            <label><?=t($t_base.'fields/date')." (".t($t_base.'fields/requested_pickup').")";;?></label>
                            <div id="dtTiming<?php echo $id_suffix; ?>_container">
                                <input id="datepicker1_search_services<?php echo $id_suffix; ?>" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onblur="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" name="searchAry[dtTiming]"  type="<?php echo $typeForDate;?>" value="<?php echo $dtTimingDate; ?>" > 
                                <input id="isMobileFlag<?php echo $id_suffix; ?>" name="searchAry[isMobileFlag]"  type="hidden" value="<?php echo (int)$isMobileValue; ?>"> 
                            </div>
                        </div>
                    <?php } ?>
                </div> 
                <?php if($iSearchMiniPage==5 || $iSearchMiniPage==6){ ?>
                    <div <?php if($button_flag){ ?> class="btn <?php echo $szClass_update; ?>" <?php }else{?> class="btn-grey" <?php }?> id="search-btn-container<?php echo $id_suffix; ?>"> 
                        <?php if($service_page_flag){ ?> 
                        <a href="javascript:void(0);" id="update_search<?php echo $id_suffix; ?>" onclick="update_service_listing('landing_page_form<?php echo $id_suffix; ?>','<?php echo $iHiddenValue; ?>');"> <?php echo t($t_base.'fields/update');?></a>
                        <?php }else{?>
                        <a href="javascript:void(0);" id="search_listing<?php echo $id_suffix; ?>" onclick="return validateNewLandingPageForm('landing_page_form<?php echo $id_suffix; ?>','<?php echo $_SESSION['user_id']; ?>','<?php echo __NEW_SERVICES_PAGE_URL__?>','<?php echo $iHiddenValue; ?>','<?php echo $iSearchMiniPage; ?>');"><?php echo $szSearchButtonText; ?></a> 
                        <?php }?>
                    </div>  
                <?php } ?>
            </div> 
            <?php if($iSearchMiniPage==3 || $iSearchMiniPage==4){ ?>
                <div class="dotted-line">&nbsp;</div>
                <div id="update_cargo_details_container_div" class="search-min-v3-cargo-field"  style="display:block;">
                    <?php echo update_cargo_dimension($kBooking,$postSearchAry,false,$iSearchMiniPage); ?>
                </div> 
                <div class="search-min-v3-button-cotainer">
                    <div <?php if($button_flag){ ?> class="btn <?php echo $szClass_update; ?>" <?php }else{?> class="btn-grey" <?php }?> id="search-btn-container<?php echo $id_suffix; ?>"> 
                        <a href="javascript:void(0);" id="search_listing<?php echo $id_suffix; ?>" onclick="return validateNewLandingPageForm('landing_page_form<?php echo $id_suffix; ?>','<?php echo $_SESSION['user_id']; ?>','<?php echo __NEW_SERVICES_PAGE_URL__?>','<?php echo $iHiddenValue; ?>','<?php echo $iSearchMiniPage; ?>');"><?php echo $szSearchButtonText; ?></a> 
                    </div>  
                </div>
            <?php } } ?> 
        </div>
        <input type="hidden" name="searchAry[idTimingType]" id="szTiming" value="1">
        <script type="text/javascript">
            Custom.init();
            <?php if($iSearchMiniPage==5 || $iSearchMiniPage==6){ ?>   
                autofill_pallet_type('<?php echo $iPalletType; ?>','<?php echo $iSearchMiniPage; ?>');
            <?php } ?>
        </script> 
        <?php
    }
    else
    {
        ?> 
        <div class="search <?php echo $szInput_class; ?><?php echo $szWithoutPickUpCLass; ?> clearfix" id="search_header">	  
        <?php if(!$hidden_search_form && !$service_page_flag && !$hide_search_image && !$bSearchMini){ ?>
            <!--<div class="location-pointer"><img src="<?php echo __BASE_STORE_IMAGE_URL__?>/search-from-to-arrow.png" width="219px" height="49px;" alt=""></div>-->
                <?php } ?> 
                <div class="location-container">
                    <div class="from">
                        <label><?=t($t_base.'fields/from');?></label>
                        <div id="szOriginCountryStr<?php echo $id_suffix; ?>_container">
                            <input type="text" name="searchAry[szOriginCountryStr]" id="szOriginCountryStr<?php echo $id_suffix; ?>"  onblur="check_location(this.id,'FROM_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');check_form_field_empty_standard_search(this.form.id,this.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/from');?>');" value="<?php echo $szOriginCountryStr;?>" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="reste_search_flags('FROM_COUNTRY');autoSelectTexts(this.id);check_form_field_not_required(this.form.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container');" onclick="autoSelectTexts(this.id);"/>
                            <span id="szOriginCountryStr<?php echo $id_suffix; ?>_clear_box" class="clear-data" onclick="fillAddressForIPAddress('szOriginCountryStr<?php echo $id_suffix; ?>');"><a>+</a></span>
                        </div>
                    </div>
                    <div class="to">
                        <label><?=t($t_base.'fields/to');?></label>
                        <span class="updownarrow" onclick="swapFromToTextFieldValue('<?php echo $id_suffix; ?>');">up/down</span>
                        <div id="szDestinationCountryStr<?php echo $id_suffix; ?>_container" >
                            <input type="text" name="searchAry[szDestinationCountryStr]" id="szDestinationCountryStr<?php echo $id_suffix; ?>" value="<?php echo $szDestinationCountryStr;?>" onblur="check_location(this.id,'TO_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');check_form_field_empty_standard_search(this.form.id,this.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/to');?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" onfocus="reste_search_flags('TO_COUNTRY');autoSelectTexts(this.id);check_form_field_not_required(this.form.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container');" onclick="autoSelectTexts(this.id);"/>
                            <span id="szDestinationCountryStr<?php echo $id_suffix; ?>_clear_box" class="clear-data" onclick="fillAddressForIPAddress('szDestinationCountryStr<?php echo $id_suffix; ?>');"><a>+</a></span>
                        </div>
                    </div>
                </div>
                <div class="dimensions-container">
                    <?php if(!$bBookingQuotes && $bDisplayPickupField){ ?>
                        <div class="pickup">
                            <?php  
                                if($iShipperConsigneeFlag==1)
                                { 
                                    /*
                                    * This means customer is shipper
                                    */ 
                            ?>
                                <label><?=t($t_base.'fields/delivery');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/DELIVERY_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                                <div style="text-align:center;">				
                                    <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                                        <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_DTP__)?'selected':'');?>><?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/cfr');?></option>
                                        <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/dap');?></option>
                                    </select>  
                                </div>
                            <?php 
                            
                                } 
                                else if($iShipperConsigneeFlag==3)
                                { 
                                    /*
                                    * This means customer is billing
                                    */ 
                                    ?>
                                    <label><?=t($t_base.'fields/terms');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/TERMS_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                                    <div style="text-align:center;">				
                                        <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                                            <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_PTD__)?'selected':'');?>><?php echo t($t_base.'fields/fob');?></option>
                                            <option value="2" <?php echo (($idServiceType==__SERVICE_TYPE_DTP__)?'selected':'');?>><?php echo t($t_base.'fields/cfr');?></option>
                                            <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/dtd');?></option>
                                        </select>  
                                    </div> 
                            <?php } else { ?>
                                <label><?=t($t_base.'fields/pick_up_new');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/PICKUP_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
                                <div style="text-align:center;">				
                                    <select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');" onchange="change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>','<?php echo __SERVICE_TYPE_DTP__; ?>');"> 
                                        <option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_PTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/fob');?></option>
                                        <option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/exw');?></option>
                                    </select>  
                                </div>
                            <?php } ?>  
                        </div>
                    <?php } ?>
                    <div class="date">
                        <label><?=t($t_base.'fields/date');?></label>
                        <div id="dtTiming<?php echo $id_suffix; ?>_container">
                            <input id="datepicker1_search_services<?php echo $id_suffix; ?>" name="searchAry[dtTiming]"  type="<?php echo $typeForDate;?>" value="<?php echo $dtTimingDate; ?>"> 
                            <input id="isMobileFlag<?php echo $id_suffix; ?>" name="searchAry[isMobileFlag]"  type="hidden" value="<?php echo (int)$isMobileValue; ?>"> 
                        </div>
                    </div>
                    <div class="cbm">
                        <?php if($iLanguage==__LANGUAGE_ID_DANISH__){ ?>
                            <label> m<span class="sup">3</span></label>
                        <?php } else {  ?>
                            <label><?=t($t_base.'fields/cm');?></label>
                        <?php } ?> 
                        <div id="iVolume<?php echo $id_suffix; ?>_container"><input type="<?php echo $typeVolWeightDate;?>"  onkeypress="return isNumberKeySearch(event);" onfocus="check_form_field_not_required(this.form.id,'iVolume<?php echo $id_suffix; ?>_container');validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" name="searchAry[iVolume]"  onblur="check_form_field_empty_standard_search(this.form.id,this.id,'iVolume<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/volume');?>');" onkeyup="checkFromAddressWeightVolume(this.value,'VOLUME','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?php echo t('Error/volume')." ".t('Error/must_be_more');?>','<?php echo t('Error/cbm');?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" id="iVolume<?php echo $id_suffix; ?>" value="<?php echo $fCargoVolume;?>"></div>
                    </div>
                    <div class="kg">
                        <label><?=t($t_base.'fields/kg');?></label>
                        <div id="iWeight<?php echo $id_suffix; ?>_container">
                            <input id="iWeight<?php echo $id_suffix; ?>" type="<?php echo $typeVolWeightDate;?>"  onkeypress="return isNumberKeySearch(event);" onblur="check_form_field_empty_standard_search(this.form.id,this.id,'iWeight<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/weight');?>','1');" onkeyup="checkFromAddressWeightVolume(this.value,'WEIGHT','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?php echo t('Error/weight')." ".t('Error/must_be_more');?>','<?php echo t('Error/kg');?>','<?php echo $iHiddenValue; ?>',event,'<?php echo $iSearchMiniPage; ?>');" value="<?php echo ceil($iWeight); ?>" name="searchAry[iWeight]" onfocus="check_form_field_not_required(this.form.id,'iWeight<?php echo $id_suffix; ?>_container');validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');"> 
                        </div>
                    </div>
                </div>
                <?php if($bDisplayBooknowButton) { ?>
                <div <?php if($button_flag){?> class="btn <?php echo $szClass_update; ?>" <?php }else{?> class="btn-grey" <?php }?> id="search-btn-container<?php echo $id_suffix; ?>">
                    <?php if($service_page_flag){ ?> 
                        <a href="javascript:void(0);" id="update_search<?php echo $id_suffix; ?>" onclick="update_service_listing('landing_page_form<?php echo $id_suffix; ?>','<?php echo $iHiddenValue; ?>');"> <?php echo t($t_base.'fields/update');?></a>
                    <?php }else{ ?> 
                        <a href="javascript:void(0);" id="search_listing<?php echo $id_suffix; ?>" onclick="return validateNewLandingPageForm('landing_page_form<?php echo $id_suffix; ?>','<?php echo $_SESSION['user_id']; ?>','<?php echo __NEW_SERVICES_PAGE_URL__?>','<?php echo $iHiddenValue; ?>','<?php echo $iSearchMiniPage; ?>');"><span class="tiny-loader-span"></span><?php echo $szSearchButtonText; ?></a>
                    <?php } ?>
                </div>
            <?php } ?>
            <input type="hidden" name="searchAry[idTimingType]" id="szTiming" value="1">
            </div>
        <?php if(!$bBookingQuotes){ ?>
            <script type="text/javascript">
                Custom.init();
            </script>
        <?php } ?>
        <?php
    }
?>  
<input type="hidden" name="searchAry[szPageLoaction]" id="szPageLoaction" value="1" />
<input type="hidden" name="searchAry[szPageName]" id="szPageName" value="NEW_LANDING_PAGE">
<input type="hidden" name="searchAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?php echo $szBookingRandomNum?>" />
<input type="hidden" name="searchAry[idServiceType]" id="idServiceType<?php echo $id_suffix; ?>" value="<?php echo $idServiceType; ?>">
<input type="hidden" name="searchAry[iBookingQuote]" id="iBookingQuote<?php echo $id_suffix; ?>" value="<?php echo $iBookingQuote; ?>">
<input type="hidden" name="searchAry[iHiddenChanged]" id="iHiddenChanged" value="2">
<input type="hidden" name="searchAry[iHiddenValue]" id="iHiddenValue" value="<?php echo $iHiddenValue; ?>" />
<input type="hidden" name="searchAry[iServiceFlag]" id="iServiceFlag" value="<?php echo $service_page_flag; ?>" /> 
<input type="hidden" name="iPickupDefaultValue" id="iPickupDefaultValue" value="<?php echo t($t_base.'fields/pick_no');?>">
 
<input type="hidden" name="iPickupDefaultValue_option1" id="iPickupDefaultValue_option1" value="<?php echo $iPickupDefaultValue_option1; ?>">
<input type="hidden" name="iPickupDefaultValue_option2" id="iPickupDefaultValue_option2" value="<?php echo $iPickupDefaultValue_option2; ?>">  
<input type="hidden" name="iPickupDefaultValue_option3" id="iPickupDefaultValue_option3" value="<?php echo $iPickupDefaultValue_option3; ?>">  
 
<input type="hidden" name="iPickupDropdownValue_option1" id="iPickupDropdownValue_option1" value="<?php echo $iPickupDropdownValue_option1; ?>">
<input type="hidden" name="iPickupDropdownValue_option2" id="iPickupDropdownValue_option2" value="<?php echo $iPickupDropdownValue_option2; ?>">
<input type="hidden" name="iPickupDropdownValue_option3" id="iPickupDropdownValue_option3" value="<?php echo $iPickupDropdownValue_option3; ?>">
<input type="hidden" name="iShipperConsigneeFlag" id="iShipperConsigneeFlag" value="<?php echo $iShipperConsigneeFlag; ?>"> 
    
<input type="hidden" name="fMaxVolume" id="fMaxVolume" value="<?php echo number_format((float)$maxVolume);?>">
<input type="hidden" name="fMaxWeight" id="fMaxWeight" value="<?php echo round($maxCargoWeight);?>">
<input type="hidden" name="searchAry[szReuquestPageUrl]" id="szReuquestPageUrl" value="<?php echo $_SERVER['REQUEST_URI'];?>">
<input type="hidden" name="iCheckedForFromField" id="iCheckedForFromField" value="">
<input type="hidden" name="iCheckedToFromField" id="iCheckedToFromField" value="">
<input type="hidden" name="searchAry[idSeoPage]" id="idSeoPage" value="<?php echo $idSeoPage; ?>">
<input type="hidden" name="searchAry[idDefaultLandingPage]" id="idDefaultLandingPage" value="<?php echo $idDefaultLandingPage; ?>">
<input type="hidden" name="searchAry[iSearchMiniPage]" id="iSearchMiniPage" value="<?php echo $iSearchMiniPage; ?>"> 
<input type="hidden" name="searchAry[iPageVersion]" id="iPageVersion" value="<?php echo $iPageVersion; ?>"> 
<!--<input type="hidden" name="searchAry[iCalculateOnlyCourier]" id="iCalculateOnlyCourier" value="0">-->  
</form> 
<?php
} 


function display_services_quotes_listing($postSearchAry,$t_base) //WTW
{	  
    $kRegisterShipCon = new cRegisterShipCon();
    $kWHSSearch=new cWHSSearch();
    $kConfig=new cConfig();
    $kBooking = new cBooking();

    $idBooking = $postSearchAry['id']; 
    $idWTW = $postSearchAry['idUniqueWTW']; 
    $t_base_select_service = "SelectService/"; 
        
    /*
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
    { 
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        cPartner::$idGlobalPartner = __QUICK_QUOTE_DEFAULT_PARTNER_ID__; 
        cPartner::$szGlobalToken = $postSearchAry['szReferenceToken']; 
        $tempResultAry = $kWHSSearch->getSearchedDataFromTempData(false,false,false,true); 
        $searchResultAry = unserialize($tempResultAry['szSerializeData']);  
        $szCourierCalculationLogString = $tempResultAry['szCourierCalculationLogString'];
        $iSearchResultLimit = count($searchResultAry);

        $res_ary = $searchResultAry ;
        $final_ary = sortArray($searchResultAry,'fDisplayPrice');  // sorting array ASC to fPrice  
        $searchResultAry = sortArray($searchResultAry,'fDisplayPrice',false,'iCuttOffDate_time',false,'iAvailabeleDate_time',false,true); 
        
        echo '<div id="service-listing-container">';
            echo display_new_searvice_listing($postSearchAry,$searchResultAry,$t_base_select_service,$countryKeyValueAry,false,false,false,$szCourierCalculationLogString);
        echo '</div>';
    }
    else
    {
     * 
     */
        echo '<div id="service-listing-container" class="quotation-service-listing">';
        echo display_quotes_searvice_details($postSearchAry);
        echo '</div>';
    //}
    ?> 
    <div id="booking_details_container"  class="booking-page quotation-contact-details-container clearfix">
        <?php echo display_booking_details($postSearchAry,$kRegisterShipCon,true);?>
    </div> 
    <div id="rating_popup"></div> 
  <?php
} 
function display_quotes_searvice_details($postSearchAry)
{
    $t_base = "SelectService/"; 
    $kWHSSearch=new cWHSSearch();
    $distance_1_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szOriginCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_1_TOOL_TIP_HEADER_TEXT_2');
    $distance_2_header = t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_1')." ".$postSearchAry['szDestinationCity'].".".t($t_base.'messages/DISTANCE_TO_WAREHOUSE_2_TOOL_TIP_HEADER_TEXT_2');
    $detail_style="style='text-decoration:none;color:white;'" ;

    $danishMonthArr=array('Januar','Februar','Marts','April','Maj','Juni','Juli','August','September','Oktober','November','December');

    $kConfig=new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);

    $szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ; 
    
    $iPrivateShipping = false;
    if($_SESSION['user_id']>0)
    {
        $kUser = new cUser();
        $kForwarder = new cForwarder();
        $kUser->getUserDetails($_SESSION['user_id']);
        
        if($kUser->iPrivate==1)
        {
            $iPrivateShipping = true;
        } 
    }
?>
	<script type="text/javascript">
            Custom.init();
	</script>
	<div class="results">
            <div class="head clearfix">
                <div class="logo">&nbsp;</div>
                <div class="rating"><?php echo t($t_base.'fields/rating'); ?></div>
                <div class="cut-off"><?=t($t_base.'fields/shipping_mode');?></div>
                <div class="delivery"><?php echo t($t_base.'fields/days');?></div>
                <div class="days"><?php echo ucwords(t($t_base.'fields/frequency'));?></div>
                <div class="price">
                    <?php echo t($t_base.'fields/all_in_price'); ?>
                    <select name="idBookingCurrency" id="idBookingCurrency" onfocus="on_focus_currency_dropdown(this.id)" class="styled" onchange="update_booking_currency(this.value,'<?php echo $szBookingRandomNum; ?>','SERVICE_QUOTE_PAGE')">
                            <?php
                                if(!empty($currencyAry))
                                {
                                    foreach($currencyAry as $currencyArys)
                                    {
                                        ?>
                                        <option value="<?php echo $currencyArys['id']?>" <?php echo (($currencyArys['id']==$postSearchAry['idCustomerCurrency'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                        <?php
                                    }
                                }
                             ?>
                    </select> 
                    <input type="hidden" name="from_page_flag" value="SERVICE_QUOTE_PAGE" id="from_page_flag">
                </div>
            </div>
		<?php
                    if(!empty($postSearchAry))
                    { 
                        $minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
                        $tr_counter = 1;
                        $filterServiceAry = array();
  
                        $iLanguage = getLanguageId();
                        $fTotalPriceDisplay = $postSearchAry['fTotalPriceCustomerCurrency'];

                        $szClassName = "odd";
                        if($tr_counter%2==0)
                        {
                            $szClassName = "";
                        }
                        $iLanguage = getLanguageId();  

                        $forwarderRatingAr = $kWHSSearch->getForwarderAvgRating($postSearchAry['idForwarder']);

                        $forwarderReviewsAry = $kWHSSearch->getForwarderAvgReviews($postSearchAry['idForwarder']);

                        $iNumRating = $forwarderRatingAr['iNumRating'] ;
                        $fAvgRating = $forwarderRatingAr['fAvgRating'] ;

                        $kConfig = new cConfig();
                        $transportModeListAry = array();
                        $transportModeListAry = $kConfig->getAllTransportMode($postSearchAry['idTransportMode'],$iLanguage);
                        $szShippingMode = $transportModeListAry[0]['szLongName'];
                        
                        if($iPrivateShipping)
                        {
                            //if user is Private, then add VAT to the price we show
                            $fTotalDisplayPrice = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat'];
                        } 
                        else
                        {
                            $fTotalDisplayPrice = $postSearchAry['fTotalPriceCustomerCurrency'];
                        }
                        //echo "<br> Price: ".$searchResults['fDisplayPrice']." VAT: ".$searchResults['fTotalVat']."<br>";
//                        if($iLanguage==__LANGUAGE_ID_DANISH__)
//                        { 
//                            $fTotalBookingPrice = number_format((float)$fTotalDisplayPrice,0,'.','.');
//                        }
//                        else
//                        { 
//                            $fTotalBookingPrice = number_format((float)$fTotalDisplayPrice,0,'.',',');
//                        }
                        $fTotalBookingPrice = number_format_custom((float)$fTotalDisplayPrice,$iLanguage);
                        ?>		
                        <div class="<?php echo $szClassName; ?> search-result-show-hide-div clearfix" id="select_services_tr_<?php echo $postSearchAry['id']; ?>">
                            <div class="logo">
                                <a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$postSearchAry['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                                    <?php if(!empty($postSearchAry['szForwarderLogo'])){ ?>
                                        <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$postSearchAry['szForwarderLogo']."&w=138&h=61"?>" alt="<?=$searchResults['szForwarderDispName']?>" border="0" />
                                    <?php }else { echo (strlen($searchResults['szForwarderDispName'])<=22)?$searchResults['szForwarderDispName']:mb_substr($searchResults['szForwarderDispName'],0,20,'UTF8')."..";	 } ?>
                                </a>
                            </div>
                            <div class="rating">
                                <a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$postSearchAry['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                                    <?php 
                                        if($iNumRating<$minRatingToShowStars)
                                        { 
                                            echo displayRatingStar(0); 
                                        }
                                        else 
                                        {									
                                            // one filled star's width is 10px so that we are multiplying Avg Rating with 10 
                                            $fAvgRating = round(($fAvgRating*__RATING_STAR_WIDTH__),2);
                                            echo displayRatingStar($fAvgRating);
                                        }
                                    ?>
                                    <span>(<?php echo (int)$iNumRating; ?>)</span>
                                </a>
                            </div>
                            <div class="cut-off"><?php echo $szShippingMode;?></div>
                            <div class="delivery"><?php echo $postSearchAry['iTransitHours'];?></div>
                            <div class="days"><?php echo $postSearchAry['szFrequency']?></div>
                            <div class="price"><span id="fullResolution"><?php echo $postSearchAry['szCustomerCurrency']?></span> <?php echo $fTotalBookingPrice; ?></div>
                         </div>	
                            <?php 
			}
			else
			{
                            ?>
                            <div class="odd clearfix" >
                                <h2><?=t($t_base.'messages/NO_SERVICE_MATCHING');?></h2>
                            </div>
                            <?php
			}
		?>
	</div>
	<?php
}
function display_shipper_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry=array(),$iBookingQuotes=false)
{ 
	$kConfig = new cConfig();	
	$t_base = "BookingDetails/";  
	$kRegisterShipCon = new cRegisterShipCon();
	    
	if(empty($shipperConsigneeAry))
	{ 
            $show_from_landing_page = true ;  
	}
	else
	{
            $show_from_landing_page = false;  
	}
        $kBooking= new cBooking();
        $idBooking = $postSearchAry['id'] ;
        $kBooking->load($idBooking);
	if($show_from_landing_page)
	{  
            $origionGeoCountryAry = array(); 
            $replaceAry = array(" ",",");
            $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szOriginCountry']); 
            if(!empty($_COOKIE[$szCountryStrKey]))
            {
                $origionGeoCountryAry = unserialize($_COOKIE[$szCountryStrKey]); 
            }
            else
            {
                //Fetching details of Origin location from google
                $origionGeoCountryAry = array();
               // $origionGeoCountryAry = reverse_geocode($postSearchAry['szOriginCountry'],true); 
            }
            $origionGeoCountryAry = array();
            $origionGeoCountryAry = reverse_geocode($postSearchAry['szOriginCountry'],true); 
            $szShipperAddressGoe = trim($origionGeoCountryAry['szStreetNumber']);
             
            $postcodeCheckAry=array();
            $postcodeResultAry = array();

            $postcodeCheckAry['idCountry'] = $postSearchAry['idOriginCountry'] ;
            $postcodeCheckAry['szLatitute'] = $origionGeoCountryAry['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $origionGeoCountryAry['szLng'] ;
            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);
            $iGotDataFromDB = false ;
            if(!empty($postcodeResultAry))
            {
                if(empty($origionGeoCountryAry['szCityName']))
                {
                    $origionGeoCountryAry['szCityName'] = $postcodeResultAry['szCity'];
                }                
                if(empty($origionGeoCountryAry['szPostCode']))
                {
                    //$origionGeoCountryAry['szPostCode'] = $postcodeResultAry['szPostCode'];
                    //$iGotDataFromDB = true;
                }
            }
            if(!empty($szShipperAddressGoe) && !empty($origionGeoCountryAry['szRoute']))
            {
                $szShipperAddressGoe .= ", ".$origionGeoCountryAry['szRoute'] ;
            }
            else
            {
                $szShipperAddressGoe = $origionGeoCountryAry['szRoute'] ;
            }

            if(!empty($szShipperAddressGoe))
            {
                $shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 1 ;
                $shipperConsigneeAry['szShipperAddress'] = $szShipperAddressGoe ;
            }
            if(!empty($origionGeoCountryAry['szCityName']))
            {
                $shipperConsigneeAry['szShipperCity'] = $origionGeoCountryAry['szCityName'];
                $shipperConsigneeAry['iGetShipperCityFromGoogle'] = 1 ;
            }
            if(!empty($origionGeoCountryAry['szPostCode']))
            {
                $shipperConsigneeAry['szShipperPostcode'] = $origionGeoCountryAry['szPostCode'];
                if(!$iGotDataFromDB)
                {
                    $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 1 ;		  
                }
            } 
            $shipperConsigneeAry['szShipperLatitute'] = $origionGeoCountryAry['szLat']; 
            $shipperConsigneeAry['szShipperLongitute'] = $origionGeoCountryAry['szLng'];		
            $shipperConsigneeAry['idShipperDialCode'] = $postSearchAry['idOriginCountry'];
            $shipperConsigneeAry['idShipperCountry'] = $postSearchAry['idOriginCountry']; 

            $iGetDataFromCookie =true;
            if(!$iBookingQuotes)
            {
                if($_SESSION['user_id']>0)
                {
                    $kUser=new cUser();
                    $kUser->getUserDetails($_SESSION['user_id']);
                    if(($kUser->szPostcode == $shipperConsigneeAry['szShipperPostcode']) && ($kUser->szCountry==$shipperConsigneeAry['idShipperCountry']))
                    {
                        $iGetDataFromCookie = false;
                        $shipperConsigneeAry['szShipperCompanyName'] = $kUser->szCompanyName;
                        $shipperConsigneeAry['szShipperFirstName'] = $kUser->szFirstName;
                        $shipperConsigneeAry['szShipperLastName'] = $kUser->szLastName;
                        $shipperConsigneeAry['szShipperEmail'] = $kUser->szEmail;
                        $shipperConsigneeAry['idShipperDialCode'] = $kUser->idInternationalDialCode;
                        $shipperConsigneeAry['szShipperPhone'] = $kUser->szPhoneNumber;
                        $shipperConsigneeAry['szShipperAddress'] = $kUser->szAddress1;
                        $shipperConsigneeAry['szShipperPostcode'] = $kUser->szPostcode;
                        $shipperConsigneeAry['szShipperCity'] = $kUser->szCity ;
                        $shipperConsigneeAry['idShipperCountry'] = $kUser->szCountry; 
                    } 
                }
            }
            
            if(!empty($_COOKIE['__TRANSPORTECA_SHIPPER_CONSIGNEE_DETAILS_COOKIE__']) && $iGetDataFromCookie)
            {
                $shipperConsigneeTempAry = array();
                $shipperConsigneeTempAry = unserialize($_COOKIE['__TRANSPORTECA_SHIPPER_CONSIGNEE_DETAILS_COOKIE__']);

                if(($shipperConsigneeTempAry['idShipperCountry']==$shipperConsigneeAry['idShipperCountry'] && empty($shipperConsigneeAry['szShipperCity'])) || ($shipperConsigneeTempAry['idShipperCountry']==$shipperConsigneeAry['idShipperCountry'] && $shipperConsigneeTempAry['szShipperCity']==$shipperConsigneeAry['szShipperCity']))
                {
                    $shipperConsigneeAry = $shipperConsigneeTempAry ;
                } 
            }  
	} 
	$szDisableAddress = '';
	if($shipperConsigneeAry['iDonotKnowShipperAddress']==1)
	{
            $szDisableAddress = 'disabled class="disable-field"';
	}
	
	$szDisablePostcode = '';
	if($shipperConsigneeAry['iDonotKnowShipperPostcode']==1)
	{
            $szDisablePostcode = 'disabled class="disable-field"';
	}
	
	if(!empty($shipperConsigneeAry['szShipperPostcode']))
	{
            $szShipperPostcode = $shipperConsigneeAry['szShipperPostcode'] ;
	}
	else
	{
            $szShipperPostcode = $shipperConsigneeAry['szShipperPostCode'] ;
	} 
	
	if(!empty($shipperConsigneeAry['szShipperCity']))
	{
            $shipperConsigneeAry['iGetShipperCityFromGoogle'] = 1;
	}
        
        if($iBookingQuotes)
        {
            if(!empty($shipperConsigneeAry['szShipperHeading']))
            {
                $szShipperHeading = strtolower($shipperConsigneeAry['szShipperHeading']) ;
            }
            else
            {
                $szShipperHeading = strtolower(t($t_base.'fields/courier_shipper'));
            } 
        }
        else
        {
            if($kBooking->iCourierBooking==1)
            {
                $szShipperHeading = strtolower(t($t_base.'fields/courier_shipper'));
            }
            else
            {
                $szShipperHeading = strtolower(t($t_base.'messages/supplier')) ;
            }
        }
        $szPostcodeMouseOverString = t($t_base.'messages/donot_know_tool_tip_1')." ".$szShipperHeading." ".t($t_base.'messages/donot_know_tool_tip_2') ;
        
        $szPostcodeMouseOverString = $szPostcodeMouseOverString;
        $bDisplayDonotKnowCheckbox = true ; 
                    
        if($shipperConsigneeAry['iShipperConsignee']==1)
        {
            $bDisplayDonotKnowCheckbox = false; 
        } 
        else
        {
            $bDisplayDonotKnowCheckbox = true ;
        } 
       
        if($postSearchAry['iPrivateShipping'] == 1)
        {
             $kBooking = new cBooking();
             $kUser = new cUser();
            // $bookingDetailAry = $kBooking->getBookingDetails($postSearchAry['id']);
             $userDetailAry = $kUser->getUserDetails($postSearchAry['idUser']);

             $privateUser = (int)$postSearchAry['iPrivateShipping'];
        }
        $disableFlag = false;
        if((int)$privateUser == 1)
        {
            if($postSearchAry['iShipperConsignee']==1)
            {
                $disableFlag = true;
                if(!empty($_POST['shipperConsigneeAry']))
                {
                    if((int)$_POST['shipperConsigneeAry']['iThisIsMe']=='0')
                    {
                        $disableFlag = false;
                    }
                }
                if($disableFlag)
                {
                    if(!empty($_POST['shipperConsigneeAry']))
                    {
                        $compnayName = $_POST['shipperConsigneeAry']['szShipperFirstName']." ".$_POST['shipperConsigneeAry']['szShipperLastName'];
                    }else
                    {
                        $compnayName = $userDetailAry['szFirstName']." ".$userDetailAry['szLastName'];
                    }
                }
                else
                    $compnayName = $shipperConsigneeAry['szShipperCompanyName'];
            }
            else
            {
                $compnayName = $shipperConsigneeAry['szShipperCompanyName'];
            }

        }
        else
        {
            $compnayName = $shipperConsigneeAry['szShipperCompanyName'];
        }
?>

<script type="text/javascript">
function initAutodropDowns_shipper() 
{ 
    $("#szShipperCompanyName").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
        width: 260,
        matchContains: true,
        mustMatch: true   
    });
}
<?php if((int)$disableFlag != true){?>
$().ready(function(){ 
    initAutodropDowns_shipper();	
});
<?php }?>
</script>
<?php 
$szClass_1 = 'fl-55' ;
$szClass_2 = 'fl-45' ;

$shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 0;
$shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 0; 

$disabledClassForVogaPage="";
$disabledReadonlyFlagForVogaPage=false;
if($postSearchAry['iSearchMiniVersion']==7)
{
    $disabledClassForVogaPage="disabled";
    $disabledReadonlyFlagForVogaPage=true;
    $disabledReadonlyForVogaPage="readonly";
    
}

?>
    <div class="oh even">
        <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/c_name');?></p>
        <p  id="szShipperCompanyName_p" <?php if((int)$disableFlag == true){?> class="<?php echo $szClass_2; ?> disabled" <?php }else{?> class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>" <?php }?>>
            <input <?php if((int)$disableFlag == true || $disabledReadonlyFlagForVogaPage){?> readonly="" <?php } else { ?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php } ?> type="text" tabindex="9"  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperCompanyName]" id="szShipperCompanyName" value="<?php echo $compnayName;?>"/>
        </p>
    </div>
    <div class="oh odd">
        <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/f_name');?></p>
        <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
            <input type="text" tabindex="10" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" <?php }?> <?php if((int)$disableFlag == true){?> onblur="check_form_field_empty_standard(this.form.id,this.id);updateszCompanyName('Shipper');" <?php }else {?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperFirstName]" id="szShipperFirstName" value="<?=$shipperConsigneeAry['szShipperFirstName']?>"/>
        </p>
    </div>				
    <div class="oh even">
        <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/l_name');?></p>
        <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
            <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="11" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" <?php }?> <?php if((int)$disableFlag == true){?> onblur="check_form_field_empty_standard(this.form.id,this.id);updateszCompanyName('Shipper');" <?php } else{?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperLastName]" id="szShipperLastName" value="<?=$shipperConsigneeAry['szShipperLastName']?>"/>
        </p>
    </div>
    <div class="oh odd">
        <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/email');?></p>
        <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
            <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="12" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="validate_email(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperEmail]" id="szShipperEmail" value="<?=$shipperConsigneeAry['szShipperEmail']?>"/>
        </p>
    </div>
    <div class="oh even">
        <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/phone_number');?></p>
        <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
            <select size="1"  <?php if($disabledReadonlyFlagForVogaPage){ echo "disabled";}?> tabindex="13" name="shipperConsigneeAry[idShipperDialCode]" id="idShipperDialCode" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?>>
            <?php
            if(!empty($dialUpCodeAry))
            {
                $usedDialCode = array();
                foreach($dialUpCodeAry as $dialUpCodeArys)
                {
                    if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                    {
                        $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                        ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$shipperConsigneeAry['idShipperDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                    }
                }
            }
            ?>
           </select>
            <?php if($disabledReadonlyFlagForVogaPage){?>
            <input type="hidden" tabindex="14" name="shipperConsigneeAry[idShipperDialCode]" id="idShipperDialCode" value="<?=$shipperConsigneeAry['idShipperDialCode']?>"/>
                <?php
            }?>
            <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="14" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperPhone]" id="szShipperPhone" value="<?=$shipperConsigneeAry['szShipperPhone']?>"/>
        </p>
    </div> 
    <?php if($bDisplayDonotKnowCheckbox){ 
        
            if($iBookingQuotes){
        ?>
        <div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><span class="fl"><?=t($t_base.'fields/address_new');?></span></p>
            <?php if($shipperConsigneeAry['iGetShipperAddressFromGoogle']==1){ ?>	
            <div class="<?php echo $szClass_2; ?>" > 
                <p class="text-box">
                    <label><?php echo $shipperConsigneeAry['szShipperAddress']; ?></label>
                    <input type="hidden" name="shipperConsigneeAry[szShipperAddress]" id="szShipperAddress1" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/>
                </p>
                 <?php if($shipperConsigneeAry['iGetShipperAddressFromGoogle']!=1 && $kBooking->iCourierBooking!=1){ 
                     
                     
                     ?> 
                <span class="checkbox-container fr" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')"><?php if($disabledReadonlyFlagForVogaPage){ ?><span class="booking-disable-checkbox"><?php }?><input <?php if($disabledReadonlyFlagForVogaPage){ echo "disabled"; ?> class="styled" <?php }?> type="checkbox"  <?php echo (($shipperConsigneeAry['iDonotKnowShipperAddress']==1)?'checked':''); ?> id="iDonotKnowShipperAddress" <?php if(!$disabledReadonlyFlagForVogaPage){?> name="shipperConsigneeAry[iDonotKnowShipperAddress]" onclick="change_shipper_fields(this.id,'szShipperAddress1')"<?php }?> value="1"><?php if($disabledReadonlyFlagForVogaPage){?></span> <?php }?> <?=t($t_base.'fields/dont_know');?></span>
                <?php if($disabledReadonlyFlagForVogaPage){?>
                  <input type="hidden" name="shipperConsigneeAry[iDonotKnowShipperAddress]" id="iDonotKnowShipperAddress" value="<?php echo $shipperConsigneeAry['iDonotKnowShipperAddress'];?>" />      
                <?php }} ?>
            </div>
            <?php } else {
                
                ?>
            <div class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>" >
                <p class="text-box">
                    <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="15" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);"  onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?>  <?php echo $szDisableAddress; ?> name="shipperConsigneeAry[szShipperAddress]" id="szShipperAddress1" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/>
                </p>
                 <?php if($shipperConsigneeAry['iGetShipperAddressFromGoogle']!=1 && $kBooking->iCourierBooking!=1){ ?> 
                    <span class="checkbox-container fr" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')"><?php if($disabledReadonlyFlagForVogaPage){ ?><span class="booking-disable-checkbox"><?php }?><input type="checkbox" id="iDonotKnowShipperAddress" <?php if($disabledReadonlyFlagForVogaPage){ echo "disabled"; ?> class="styled" <?php }?>  <?php echo (($shipperConsigneeAry['iDonotKnowShipperAddress']==1)?'checked':''); ?>  <?php if(!$disabledReadonlyFlagForVogaPage){?>  name="shipperConsigneeAry[iDonotKnowShipperAddress]" onclick="change_shipper_fields(this.id,'szShipperAddress1')" <?php }?> value="1"><?php if($disabledReadonlyFlagForVogaPage){?></span> <?php }?><?=t($t_base.'fields/dont_know');?></span>
                  <?php if($disabledReadonlyFlagForVogaPage){?>
                    <input type="hidden" id="iDonotKnowShipperAddress" name="shipperConsigneeAry[iDonotKnowShipperAddress]" value="<?php echo $shipperConsigneeAry['iDonotKnowShipperAddress'];?>" />
                  <?php  }}?>
            </div>
            <?php } ?> 
        </div>
        <div class="oh even">
            <p class="<?php echo $szClass_1; ?>"><span class="fl"><?=t($t_base.'fields/postcode');?> </span></p>
            <?php if($shipperConsigneeAry['iGetShipperPostcodeFromGoogle']==1){ ?>	
            <div class="<?php echo $szClass_2; ?>" > 
                <p class="text-box">	
                    <label><?php echo $szShipperPostcode; ?></label>
                    <input type="hidden" name="shipperConsigneeAry[szShipperPostcode]" id="szShipperPostcode"  value="<?php echo $szShipperPostcode ; ?>"/>
                </p>
                <?php if($shipperConsigneeAry['iGetShipperPostcodeFromGoogle']!=1 && $kBooking->iCourierBooking!=1){ ?>
                    <span class="checkbox-container fr" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')" ><?php if($disabledReadonlyFlagForVogaPage){ ?><span class="booking-disable-checkbox"><?php }?><input type="checkbox" id="iDonotKnowShipperPostcode"  <?php echo (($shipperConsigneeAry['iDonotKnowShipperPostcode']==1)?'checked':''); ?> <?php if(!$disabledReadonlyFlagForVogaPage){?>  name="shipperConsigneeAry[iDonotKnowShipperPostcode]" onclick="change_shipper_fields(this.id,'szShipperPostcode')" <?php }?>  value="1"><?php if($disabledReadonlyFlagForVogaPage){?></span> <?php }?><?=t($t_base.'fields/dont_know');?></span>
                <?php if($disabledReadonlyFlagForVogaPage){?>
                  <input type="hidden" name="shipperConsigneeAry[iDonotKnowShipperPostcode]" id="iDonotKnowShipperPostcode" value="<?php echo $shipperConsigneeAry['iDonotKnowShipperPostcode'];?>" />      
                <?php }} ?>
            </div>
            <?php }else { 
                ?>
            <div class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>" > 
                <p class="text-box">
                    <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="16" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);"  onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> <?php echo $szDisablePostcode; ?> name="shipperConsigneeAry[szShipperPostcode]" id="szShipperPostcode"  value="<?php echo $szShipperPostcode; ?>"/>
                </p>
                <?php if($shipperConsigneeAry['iGetShipperPostcodeFromGoogle']!=1 && $kBooking->iCourierBooking!=1){ ?>
                    <span class="checkbox-container fr" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')"><?php if($disabledReadonlyFlagForVogaPage){ ?><span class="booking-disable-checkbox"><?php }?><input id="iDonotKnowShipperPostcode" type="checkbox" <?php if($disabledReadonlyFlagForVogaPage){ echo "disabled"; ?> class="styled" <?php } ?>  <?php echo (($shipperConsigneeAry['iDonotKnowShipperPostcode']==1)?'checked':''); ?> <?php if(!$disabledReadonlyFlagForVogaPage){?> name="shipperConsigneeAry[iDonotKnowShipperPostcode]"  onclick="change_shipper_fields(this.id,'szShipperPostcode')" <?php }?>  value="1"><?php if($disabledReadonlyFlagForVogaPage){?></span> <?php }?><?=t($t_base.'fields/dont_know');?></span>
                <?php if($disabledReadonlyFlagForVogaPage){?>
                  <input type="hidden" name="shipperConsigneeAry[iDonotKnowShipperPostcode]" id="iDonotKnowShipperPostcode" value="<?php echo $shipperConsigneeAry['iDonotKnowShipperPostcode'];?>" />      
                <?php } } ?>
            </div>
            <?php } ?> 
        </div>	
        <div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/city');?></p> 
            <?php if($shipperConsigneeAry['iGetShipperCityFromGoogle']==1){?>	
            <p class="<?php echo $szClass_2; ?>">	
                <label><?php echo $shipperConsigneeAry['szShipperCity']; ?></label>
                <input type="hidden" name="shipperConsigneeAry[szShipperCity]" id="szShipperCity" value="<?php echo $shipperConsigneeAry['szShipperCity'];?>"/>
            </p>
            <?php } else {?>
            <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
                <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="17" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperCity]" id="szShipperCity" value="<?php echo $shipperConsigneeAry['szShipperCity'];?>"/>
            </p>
            <?php }?> 
        </div> 
    <?php } else {
        
        ?>
         <div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><span class="fl"><?=t($t_base.'fields/address_new');?></span>
                <?php if($shipperConsigneeAry['iGetShipperAddressFromGoogle']!=1 && $kBooking->iCourierBooking!=1 ){ ?> 
                    <span class="checkbox-container fr" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')"><?php if($disabledReadonlyFlagForVogaPage){ ?><span class="booking-disable-checkbox"><?php }?><input id="iDonotKnowShipperAddress" <?php if($disabledReadonlyFlagForVogaPage){ echo "disabled"; ?> class="styled" <?php }?> type="checkbox"  <?php echo (($shipperConsigneeAry['iDonotKnowShipperAddress']==1)?'checked':''); ?>  <?php if(!$disabledReadonlyFlagForVogaPage){ ?>  name="shipperConsigneeAry[iDonotKnowShipperAddress]" onclick="change_shipper_fields(this.id,'szShipperAddress1')" <?php }?> value="1"><?php if($disabledReadonlyFlagForVogaPage){ ?></span><?php }?><?=t($t_base.'fields/dont_know');?></span>
                <?php if($disabledReadonlyFlagForVogaPage){?>
                  <input type="hidden" name="shipperConsigneeAry[iDonotKnowShipperAddress]" id="iDonotKnowShipperAddress" value="<?php echo $shipperConsigneeAry['iDonotKnowShipperAddress'];?>" />      
                <?php }} ?> 
            </p>
            <?php if($shipperConsigneeAry['iGetShipperAddressFromGoogle']==1){ ?>	
            <p class="<?php echo $szClass_2; ?>" >  
                <label><?php echo $shipperConsigneeAry['szShipperAddress']; ?></label>
                <input type="hidden" name="shipperConsigneeAry[szShipperAddress]" id="szShipperAddress1" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/>
            </p> 
            <?php } else {?>
            <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>" > 
                <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="15" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" <?php echo $szDisableAddress; ?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperAddress]" id="szShipperAddress1" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/>
            </p> 
            <?php } ?> 
        </div>
        <div class="oh even">
            <p class="<?php echo $szClass_1; ?>"><span class="fl"><?=t($t_base.'fields/postcode');?> </span>
                <?php if($shipperConsigneeAry['iGetShipperPostcodeFromGoogle']!=1 && $kBooking->iCourierBooking!=1){ ?>
                    <span class="checkbox-container fr" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')"><?php if($disabledReadonlyFlagForVogaPage){ ?><span class="booking-disable-checkbox"><?php }?><input type="checkbox" <?php if($disabledReadonlyFlagForVogaPage){ echo "disabled";}?>   <?php echo (($shipperConsigneeAry['iDonotKnowShipperPostcode']==1)?'checked':''); ?> <?php if(!$disabledReadonlyFlagForVogaPage){ ?> name="shipperConsigneeAry[iDonotKnowShipperPostcode]" onclick="change_shipper_fields(this.id,'szShipperPostcode')" <?php }?> id="iDonotKnowShipperPostcode" value="1"><?php if($disabledReadonlyFlagForVogaPage){ ?></span><?php }?><?=t($t_base.'fields/dont_know');?></span>
                <?php if($disabledReadonlyFlagForVogaPage){?>
                  <input type="hidden" name="shipperConsigneeAry[iDonotKnowShipperPostcode]" id="iDonotKnowShipperPostcode" value="<?php echo $shipperConsigneeAry['iDonotKnowShipperPostcode'];?>" />      
                <?php }} ?> 
            </p>
            <?php if($shipperConsigneeAry['iGetShipperPostcodeFromGoogle']==1){ ?>	
            <p class="<?php echo $szClass_2; ?>" >  
                <label><?php echo $szShipperPostcode; ?></label>
                <input type="hidden" name="shipperConsigneeAry[szShipperPostcode]" id="szShipperPostcode"  value="<?php echo $szShipperPostcode ; ?>"/>
            </p> 
            <?php } else {?>
            <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>" >  
                <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage; }?> tabindex="16" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" <?php echo $szDisablePostcode; ?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperPostcode]" id="szShipperPostcode"  value="<?php echo $szShipperPostcode; ?>"/>
            </p> 
            <?php } ?> 
        </div>	
        <div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/city');?></p> 
            <?php if($shipperConsigneeAry['iGetShipperCityFromGoogle']==1){?>	
            <p class="<?php echo $szClass_2; ?>">	
                <label><?php echo $shipperConsigneeAry['szShipperCity']; ?></label>
                <input type="hidden" name="shipperConsigneeAry[szShipperCity]" id="szShipperCity" value="<?php echo $shipperConsigneeAry['szShipperCity'];?>"/>
            </p>
            <?php } else {?>
            <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
                <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="17" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperCity]" id="szShipperCity" value="<?php echo $shipperConsigneeAry['szShipperCity'];?>"/>
            </p>
            <?php }?> 
        </div> 
    <?php } ?>
    <?php } else{ ?>
        <div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><span class="fl"><?=t($t_base.'fields/address_new');?></span></p> 
            <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>" >
                <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="15" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);"  onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> <?php echo $szDisableAddress; ?> name="shipperConsigneeAry[szShipperAddress]" id="szShipperAddress1" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/>
            </p> 
        </div>
        <div class="oh even">
            <p class="<?php echo $szClass_1; ?>"><span class="fl"><?=t($t_base.'fields/postcode');?> </span></p> 
            <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
                <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="16" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" <?php echo $szDisablePostcode; ?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperPostcode]" id="szShipperPostcode"  value="<?php echo $szShipperPostcode; ?>"/>
            </p> 
        </div>	
        <div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/city');?></p>   
            <p class="<?php echo $szClass_2; ?> <?php echo $disabledClassForVogaPage;?>">
                <input type="text" <?php if($disabledReadonlyFlagForVogaPage){ echo $disabledReadonlyForVogaPage;}?> tabindex="17" <?php if(!$disabledReadonlyFlagForVogaPage){?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szShipperCity]" id="szShipperCity" value="<?php echo $shipperConsigneeAry['szShipperCity'];?>"/>
            </p> 
        </div> 
    <?php } ?>
    <input type="hidden" name="shipperConsigneeAry[idShipperCountry]" id="idShipperCountry" value="<?php echo $shipperConsigneeAry['idShipperCountry']; ?>">
    <input type="hidden" name="shipperConsigneeAry[iGetShipperCityFromGoogle]" id="iGetShipperCityFromGoogle" value="<?php echo $shipperConsigneeAry['iGetShipperCityFromGoogle']; ?>">
    <input type="hidden" name="shipperConsigneeAry[iGetShipperPostcodeFromGoogle]" id="iGetShipperPostcodeFromGoogle" value="<?php echo $shipperConsigneeAry['iGetShipperPostcodeFromGoogle']; ?>">
    <input type="hidden" name="shipperConsigneeAry[iGetShipperAddressFromGoogle]" id="iGetShipperAddressFromGoogle" value="<?php echo $shipperConsigneeAry['iGetShipperAddressFromGoogle']; ?>">
    <?php if($disabledReadonlyFlagForVogaPage){?>
    <script type="text/javascript">
        Custom.init();
    </script> 
    <?php
    }
} 

function display_billing_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry=array())
{
    $kConfig = new cConfig();	
    $t_base = "BookingDetails/";  
    $kRegisterShipCon = new cRegisterShipCon();

    if(empty($shipperConsigneeAry))
    { 
        $show_from_landing_page = true ;  
    }
    else
    {
        $show_from_landing_page = false;  
    }  
    if($show_from_landing_page)
    { 
        $origionGeoCountryAry = array();
        $replaceAry = array(" ",",");
        $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szDestinationCountry']); 
        if(!empty($_COOKIE[$szCountryStrKey]))
        {
            $destinationGeoCountryAry = unserialize($_COOKIE[$szCountryStrKey]); 
        }
        else
        {
            //Fetching details of Origin location from google
//            $destinationGeoCountryAry = array();
//            $destinationGeoCountryAry = reverse_geocode($postSearchAry['szDestinationCountry'],true);
        }   
        $origionGeoCountryAry = array();
        $origionGeoCountryAry = reverse_geocode($postSearchAry['szDestinationCountry'],true);
        
        $postcodeCheckAry=array();
        $postcodeResultAry = array();

        $postcodeCheckAry['idCountry'] = $postSearchAry['idDestinationCountry'] ;
        $postcodeCheckAry['szLatitute'] = $origionGeoCountryAry['szLat'] ;
        $postcodeCheckAry['szLongitute'] = $origionGeoCountryAry['szLng'] ;
        $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

        if(!empty($postcodeResultAry))
        {
            if(empty($origionGeoCountryAry['szCityName']))
            {
                $origionGeoCountryAry['szCityName'] = $postcodeResultAry['szCity'];
            }
            if(empty($origionGeoCountryAry['szPostCode']))
            {
                //$origionGeoCountryAry['szPostCode'] = $postcodeResultAry['szPostCode'];
            }
        }

        $szShipperAddressGoe = trim($origionGeoCountryAry['szStreetNumber']);
        if(!empty($szShipperAddressGoe) && !empty($origionGeoCountryAry['szRoute']))
        {
            $szShipperAddressGoe .= ", ".$origionGeoCountryAry['szRoute'] ;
        }
        else
        {
            $szShipperAddressGoe = $origionGeoCountryAry['szRoute'] ;
        }

        $shipperConsigneeAry['szBillingAddress'] = $szShipperAddressGoe ;
        $shipperConsigneeAry['szBillingCity'] = $origionGeoCountryAry['szCityName'];		  
        $shipperConsigneeAry['szBillingPostcode'] = $origionGeoCountryAry['szPostCode'];
        $shipperConsigneeAry['szBillingLatitute'] = $origionGeoCountryAry['szLat']; 
        $shipperConsigneeAry['szBillingLongitute'] = $origionGeoCountryAry['szLng'];		
        $shipperConsigneeAry['idBillingDialCode'] = $postSearchAry['idDestinationCountry'];
        $shipperConsigneeAry['idBillingCountry'] = $postSearchAry['idDestinationCountry'];  
    } 
    if($postSearchAry['iQuotesStatus'] == 2)
    {
        $kBooking = new cBooking();
        $kUser = new cUser();
        $bookingDetailAry = $kBooking->getBookingDetails($postSearchAry['id']);
        $userDetailAry = $kUser->getUserDetails($bookingDetailAry['idUser']); 
    }
    $privateUser = (int)$postSearchAry['iPrivateShipping'];
    $disableFlag = false;
    if((int)$privateUser == 1)
    { 
        if(!empty($_POST['shipperConsigneeAry']) && (int)$_POST['shipperConsigneeAry']['iThisIsMe']=='0')
        {
            $disableFlag = true;
            if(!empty($_POST['shipperConsigneeAry']))
            {
                $compnayName = $_POST['shipperConsigneeAry']['szBillingFirstName']." ".$_POST['shipperConsigneeAry']['szBillingLastName'];
            }else
            {
                $compnayName = $userDetailAry['szFirstName']." ".$userDetailAry['szLastName'];
            }
          //  echo "helo";
        }
        else
        {
            if($bookingDetailAry['iShipperConsignee']==3)
            {
                $disableFlag = true;
                if(!empty($_POST['shipperConsigneeAry']))
                {
                    $compnayName = $_POST['shipperConsigneeAry']['szBillingFirstName']." ".$_POST['shipperConsigneeAry']['szBillingLastName'];
                }else
                {
                    $compnayName = $userDetailAry['szFirstName']." ".$userDetailAry['szLastName'];
                }
            }
            else
            {
                $compnayName = $shipperConsigneeAry['szBillingCompanyName'];
            }
        }

    }
    else
    {
        $compnayName = $shipperConsigneeAry['szBillingCompanyName'];
    }
    $szClass_1 = 'fl-55' ;
    $szClass_2 = 'fl-45' ;
?>
	<div class="oh even">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/c_name');?></p>
            <p id="szBillingCompanyName_p" <?php if((int)$disableFlag == true){?> class="<?php echo $szClass_2; ?> disabled"  readonly=""<?php }else{?>class="<?php echo $szClass_2; ?>" <?php }?>>
                <input type="text" tabindex="28" <?php if((int)$disableFlag == true){ ?> readonly="" <?php } else { ?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingCompanyName]" id="szBillingCompanyName" value="<?php echo $compnayName;?>"/>
            </p>
	</div>
	<div class="oh odd">
		<p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/f_name');?></p>
		<p class="<?php echo $szClass_2; ?>">
                        <input type="text" tabindex="29" onfocus="check_form_field_not_required(this.form.id,this.id);" <?php if((int)$disableFlag == true){?>  onblur="check_form_field_empty_standard(this.form.id,this.id);updateszCompanyName('Billing');" <?php }else {?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szBillingFirstName]" id="szBillingFirstName" value="<?=$shipperConsigneeAry['szBillingFirstName']?>"/>
		</p>
	</div>				
	<div class="oh even">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/l_name');?></p>
            <p class="<?php echo $szClass_2; ?>">
                <input type="text" tabindex="30" onfocus="check_form_field_not_required(this.form.id,this.id);" <?php if((int)$disableFlag == true){?>  onblur="check_form_field_empty_standard(this.form.id,this.id);updateszCompanyName('Billing');" <?php }else {?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szBillingLastName]" id="szBillingLastName" value="<?=$shipperConsigneeAry['szBillingLastName']?>"/>
            </p>
	</div>
	<div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/email');?></p>
            <p class="<?php echo $szClass_2; ?>">
                <input type="text" tabindex="31" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="validate_email(this.form.id,this.id);" name="shipperConsigneeAry[szBillingEmail]" id="szBillingEmail" value="<?=$shipperConsigneeAry['szBillingEmail']?>"/>
            </p>
	</div>
	<div class="oh even">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/phone_number');?></p>
            <p class="<?php echo $szClass_2; ?>">
                    <select size="1"  tabindex="32" name="shipperConsigneeAry[idBillingDialCode]" id="idBillingDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                    <?php
                        if(!empty($dialUpCodeAry))
                        {
                            $usedDialCode = array();
                            foreach($dialUpCodeAry as $dialUpCodeArys)
                            {
                                if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                {
                                    $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                    ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$shipperConsigneeAry['idBillingDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>                                        <?php
                                }
                            }
                        }
                    ?>
               </select>
                <input type="text" tabindex="33" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szBillingPhone]" id="szBillingPhone" value="<?=$shipperConsigneeAry['szBillingPhone']?>"/>
            </p>
	</div> 
	<div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/address_new');?></p>
            <p class="<?php echo $szClass_2; ?>" title="<?=t($t_base.'messages/donot_know_tool_tip');?>">			
                <input type="text" tabindex="34" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szBillingAddress]" id="szBillingAddress1" value="<?=$shipperConsigneeAry['szBillingAddress']?>"/>
            </p>
	</div>
	<div class="oh even">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/postcode');?></p>
            <p class="<?php echo $szClass_2; ?>" title="<?=t($t_base.'messages/donot_know_tool_tip');?>">			
                <input type="text" tabindex="35" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szBillingPostcode]" id="szBillingPostcode"  value="<?php echo (!empty($shipperConsigneeAry['szBillingPostCode'])?$shipperConsigneeAry['szBillingPostCode']:$shipperConsigneeAry['szBillingPostcode']); ?>"/>
            </p>
	</div>	
	<div class="oh odd">
            <p class="<?php echo $szClass_1; ?>"><?=t($t_base.'fields/city');?></p>
            <p class="<?php echo $szClass_2; ?>">			
                <input type="text" tabindex="36" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szBillingCity]" id="szBillingCity" value="<?php echo $shipperConsigneeAry['szBillingCity'];?>"/>
            </p>
	</div>
    <?php
}
function display_consignee_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry=array(),$iBookingQuotes=false)
{ 
    if(empty($shipperConsigneeAry))
    { 
        $show_from_landing_page = true ;
    }
    else
    {
        $show_from_landing_page = false;
    } 
    $kConfig=new cConfig(); 
    if($show_from_landing_page)
    {
        $fetchAddressFromGoogle = true ; 

        $idUserSession = $_SESSION['user_id'];
        
        $destinationGeoCountryAry = array();
        $destinationGeoCountryAry = reverse_geocode($postSearchAry['szDestinationCountry'],true);
        
        $postcodeCheckAry=array();
        $postcodeResultAry = array();

        $postcodeCheckAry['idCountry'] = $postSearchAry['idDestinationCountry'] ;
        $postcodeCheckAry['szLatitute'] = $destinationGeoCountryAry['szLat'] ;
        $postcodeCheckAry['szLongitute'] = $destinationGeoCountryAry['szLng'] ;
        $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);
         
        $iGotDataFromDB = false;
        if(!empty($postcodeResultAry))
        {
            if(empty($destinationGeoCountryAry['szCityName']))
            {
                $destinationGeoCountryAry['szCityName'] = $postcodeResultAry['szCity'];
            }
            if(empty($destinationGeoCountryAry['szPostCode']))
            {
                //$destinationGeoCountryAry['szPostCode'] = $postcodeResultAry['szPostCode'];
                //$iGotDataFromDB = true;
            }
        }

        $szShipperAddressGoe = trim($destinationGeoCountryAry['szStreetNumber']);
        $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 0 ;

        if(!empty($szShipperAddressGoe) && !empty($destinationGeoCountryAry['szRoute']))
        {
            $szShipperAddressGoe .= ", ".$destinationGeoCountryAry['szRoute'] ;
        }
        else
        {
            $szShipperAddressGoe .= $destinationGeoCountryAry['szRoute'] ;
        }  
        if(!empty($szShipperAddressGoe))
        {
            $shipperConsigneeAry['szConsigneeAddress'] = $szShipperAddressGoe ;
            $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 1 ;
        }
        if(!empty($destinationGeoCountryAry['szPostCode']))
        {
            $shipperConsigneeAry['szConsigneePostCode'] = $destinationGeoCountryAry['szPostCode'];
            if(!$iGotDataFromDB)
            {
                $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 1 ;
            }
        }
        if(!empty($destinationGeoCountryAry['szCityName']))
        {
            $shipperConsigneeAry['szConsigneeCity'] = $destinationGeoCountryAry['szCityName'];

            $szConsigneeCity = $destinationGeoCountryAry['szCityName'] ;
            $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 1 ;
        }  

        $shipperConsigneeAry['szConsigneeLatitute'] = $destinationGeoCountryAry['szLat']; 
        $shipperConsigneeAry['szConsigneeLongitute'] = $destinationGeoCountryAry['szLng']; 

        $shipperConsigneeAry['idConsigneeDialCode'] = $postSearchAry['idDestinationCountry']; 
        $shipperConsigneeAry['idConsigneeCountry'] = $postSearchAry['idDestinationCountry']; 

        $iGetDataFromCookie =true;
        if(!$iBookingQuotes)
        {
            if($_SESSION['user_id']>0)
            {
                $kUser=new cUser();
                $kUser->getUserDetails($_SESSION['user_id']);
                //echo "User pt: ".$kUser->szPostcode." Countr: ".$kUser->szCountry." <br> Cong pt: ".$shipperConsigneeAry['szConsigneePostCode']." count: ".$kUser->szCountry ;
                if(($kUser->szPostcode == $shipperConsigneeAry['szConsigneePostCode']) && ($kUser->szCountry==$shipperConsigneeAry['idConsigneeCountry']))
                {
                    $iGetDataFromCookie = false;
                    $shipperConsigneeAry['szConsigneeCompanyName'] = $kUser->szCompanyName;
                    $shipperConsigneeAry['szConsigneeFirstName'] = $kUser->szFirstName;
                    $shipperConsigneeAry['szConsigneeLastName'] = $kUser->szLastName;
                    $shipperConsigneeAry['szConsigneeEmail'] = $kUser->szEmail;
                    $shipperConsigneeAry['idConsigneeDialCode'] = $kUser->idInternationalDialCode;
                    $shipperConsigneeAry['szConsigneePhone'] = $kUser->szPhoneNumber;
                    $shipperConsigneeAry['szConsigneeAddress'] = $kUser->szAddress1;
                    $shipperConsigneeAry['szConsigneePostcode'] = $kUser->szPostcode;
                    $shipperConsigneeAry['szConsigneeCity'] = $kUser->szCity ;
                    $shipperConsigneeAry['idConsigneeCountry'] = $kUser->szCountry; 
                } 
            }
        }
        if(!empty($_COOKIE['__TRANSPORTECA_SHIPPER_CONSIGNEE_DETAILS_COOKIE__']) && $iGetDataFromCookie)
        {
            $shipperConsigneeTempAry = array();
            $shipperConsigneeTempAry = unserialize($_COOKIE['__TRANSPORTECA_SHIPPER_CONSIGNEE_DETAILS_COOKIE__']);

            if(($shipperConsigneeTempAry['idConsigneeCountry']==$shipperConsigneeAry['idConsigneeCountry'] && empty($shipperConsigneeAry['szConsigneeCity'])) || ($shipperConsigneeTempAry['idConsigneeCountry']==$shipperConsigneeAry['idConsigneeCountry'] && ($shipperConsigneeTempAry['szConsigneeCity']==$shipperConsigneeAry['szConsigneeCity'])))
            {
                $shipperConsigneeAry = $shipperConsigneeTempAry ;
                $fetchAddressFromGoogle = false ;
            }
            else
            {
                $fetchAddressFromGoogle = true ;
            }
        }
        else if($idUserSession>0)
        { 
            $kUser=new cUser();
            $kUser->getUserDetails($idUserSession);
            if($kUser->szCountry==$postSearchAry['idDestinationCountry'])
            { 
                $shipperConsigneeAry['szConsigneeCompanyName'] = $kUser->szCompanyName ;
                $shipperConsigneeAry['szConsigneeFirstName'] = $kUser->szFirstName ;
                $shipperConsigneeAry['szConsigneeLastName'] = $kUser->szLastName ;
                $shipperConsigneeAry['szConsigneeEmail'] = $kUser->szEmail ;
                $shipperConsigneeAry['szConsigneePhone'] = $kUser->szPhoneNumber ; 
            }
        } 
    }
    if(!empty($shipperConsigneeAry['szConsigneePostCode']))
    {
        $szConsigneePostcode = $shipperConsigneeAry['szConsigneePostCode'] ;
    }
    else
    {
        $szConsigneePostcode = $shipperConsigneeAry['szConsigneePostcode'] ;
    } 

    if(!empty($shipperConsigneeAry['szConsigneeCity']))
    {
        $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 1;
    } 
    
    $t_base = "BookingDetails/";  
    
    $bDisplayDonotKnowCheckbox = false ; 
                    
        
    if($iBookingQuotes)
    {  
        if($shipperConsigneeAry['iShipperConsignee']==2)
        {
            $bDisplayDonotKnowCheckbox = false; 
        } 
        else
        {
            $bDisplayDonotKnowCheckbox = true ;
        }
        $szDisableAddress = ''; 
	if($shipperConsigneeAry['iDonotKnowConsigneeAddress']==1)
	{
            $szDisableAddress = 'disabled class="disable-field"';
	}
	
	$szDisablePostcode = '';
	if($shipperConsigneeAry['iDonotKnowConsigneePostcode']==1)
	{
            $szDisablePostcode = 'disabled class="disable-field"';
	}
    }  
    if($iBookingQuotes)
    {
        //$szShipperHeading = lcfirst($shipperConsigneeAry['szConsigneeHeading']);
        $szShipperHeading = strtolower(t($t_base.'fields/consignee')); 
    }
    else
    {
        $szShipperHeading = strtolower(t($t_base.'messages/supplier')) ;
    }
    $szPostcodeMouseOverString = t($t_base.'messages/donot_know_tool_tip_1')." ".$szShipperHeading." ".t($t_base.'messages/donot_know_tool_tip_2') ;
    $szPostcodeMouseOverString = $szPostcodeMouseOverString;
    
    $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 0;
    $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 0;
    
    if($postSearchAry['iPrivateShipping'] == 1)
    {
         $kBooking = new cBooking();
         $kUser = new cUser();
         //$bookingDetailAry = $kBooking->getBookingDetails($postSearchAry['id']);
         $userDetailAry = $kUser->getUserDetails($postSearchAry['idUser']);

         $privateUser = (int)$postSearchAry['iPrivateShipping'];
    }
    //print_r($postSearchAry);
    $disableFlag = false;
    if((int)$privateUser == 1)
    {
        if($postSearchAry['iShipperConsignee']==2)
        {
            $disableFlag = true;
            if(!empty($_POST['shipperConsigneeAry']))
            {
                if((int)$_POST['shipperConsigneeAry']['iThisIsMe']=='0')
                {
                    $disableFlag = false;
                }
            }
            if($disableFlag)
            {
                if(!empty($_POST['shipperConsigneeAry']))
                {
                    $compnayName = $_POST['shipperConsigneeAry']['szConsigneeFirstName']." ".$_POST['shipperConsigneeAry']['szConsigneeLastName'];
                }else
                {
                    $compnayName = $userDetailAry['szFirstName']." ".$userDetailAry['szLastName'];
                }
            }
            else 
                $compnayName = $shipperConsigneeAry['szConsigneeCompanyName'];
        }
        else
        {
            $compnayName = $shipperConsigneeAry['szConsigneeCompanyName'];
        }

    }
    else
    {
        $compnayName = $shipperConsigneeAry['szConsigneeCompanyName'];
    }
?> 
    
    <script type="text/javascript">
	function initAutodropDowns_consignee() 
	{	 
            $("#szConsigneeCompanyName").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
                width: 230,
                matchContains: true,
                mustMatch: true   
            }); 
	}
      </script>  
     <?php if((int)$disableFlag != true){?> 
	<script type="text/javascript">
	$().ready(function(){ 
		initAutodropDowns_consignee();	
	}); 
        
    </script>
    <?php }?>
    <div class="oh even">
        <p <?php if((int)$disableFlag == true){?> class="disabled" <?php }?> id="szConsigneeCompanyName_p">
            <input type="text" tabindex="18"  <?php if((int)$disableFlag == true){?> readonly="" <?php } else {?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php } ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeCompanyName]" id="szConsigneeCompanyName" value="<?php echo $compnayName;?>" >
        </p>
    </div>
    <div class="oh odd">
        <p>
            <input type="text" tabindex="19" onfocus="check_form_field_not_required(this.form.id,this.id);" <?php if((int)$disableFlag == true){?> onblur="check_form_field_empty_standard(this.form.id,this.id);updateszCompanyName('Consignee');" <?php } else {?> onblur="check_form_field_empty_standard(this.form.id,this.id);" <?php }?> name="shipperConsigneeAry[szConsigneeFirstName]" id="szConsigneeFirstName" value="<?php echo $shipperConsigneeAry['szConsigneeFirstName']?>" >
        </p>
    </div>
    <div class="oh even">
        <p>
            <input type="text" tabindex="20" onfocus="check_form_field_not_required(this.form.id,this.id);" <?php if((int)$disableFlag == true){?> onblur="check_form_field_empty_standard(this.form.id,this.id);updateszCompanyName('Consignee');" <?php } else{?> onblur="check_form_field_empty_standard(this.form.id,this.id);"  <?php }?> name="shipperConsigneeAry[szConsigneeLastName]" id="szConsigneeLastName" value="<?php echo $shipperConsigneeAry['szConsigneeLastName']?>" >
        </p>
    </div>
    <div class="oh odd">
        <p>
            <input type="text" tabindex="21" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="validate_email(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeEmail]" id="szConsigneeEmail" value="<?php echo $shipperConsigneeAry['szConsigneeEmail']?>" >
        </p>
    </div>
    <div class="oh even">
        <p>
            <select size="1"  tabindex="22" name="shipperConsigneeAry[idConsigneeDialCode]" id="idConsigneeDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
            <?php
                if(!empty($dialUpCodeAry))
                {
                    $usedDialCode = array();
                    foreach($dialUpCodeAry as $dialUpCodeArys)
                    {
                        if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                        {
                            $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                            ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$shipperConsigneeAry['idConsigneeDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                        }
                    }
                }
            ?>
            </select>
            <input type="text" tabindex="23" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneePhone]" id="szConsigneePhone" value="<?php echo $shipperConsigneeAry['szConsigneePhone']?>" >
        </p>
    </div>  
    <div class="oh odd">
        <?php if($shipperConsigneeAry['iGetConsigneeAddressFromGoogle']==1){ ?>
        <p class="text">
            <label><?php echo $shipperConsigneeAry['szConsigneeAddress']; ?></label>
            <input type="hidden" name="shipperConsigneeAry[szConsigneeAddress]" id="szConsigneeAddress1" value="<?php echo $shipperConsigneeAry['szConsigneeAddress']; ?>" >
        </p>
        <?php } else {?>	
        <p>
            <input type="text" <?php echo $szDisableAddress ;?> tabindex="24" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeAddress]" id="szConsigneeAddress1" value="<?php echo $shipperConsigneeAry['szConsigneeAddress']; ?>" >
        </p>
        <?php } ?> 
        <?php if($shipperConsigneeAry['iGetShipperAddressFromGoogle']!=1 && $bDisplayDonotKnowCheckbox && $kBooking->iCourierBooking!=1){ ?>
            <p class="dont-know"><span class="checkbox-container fl" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')"><input type="checkbox" name="shipperConsigneeAry[iDonotKnowConsigneeAddress]" <?php echo (($shipperConsigneeAry['iDonotKnowConsigneeAddress']==1)?'checked':''); ?> id="iDonotKnowConsigneeAddress" onclick="change_shipper_fields(this.id,'szConsigneeAddress1')" value="1"><?=t($t_base.'fields/dont_know');?></span></p>

        <?php } ?>
    </div>  
    <div class="oh even">
        <?php if($shipperConsigneeAry['iGetConsigneePostcodeFromGoogle']==1){ ?>
            <p class="text">
                <label><?php echo $szConsigneePostcode; ?></label>
                <input type="hidden" name="shipperConsigneeAry[szConsigneePostcode]" id="szConsigneePostcode" value="<?php echo $szConsigneePostcode; ?>" >
            </p>
        <?php } else {?>	
            <p>
                <input type="text" <?php echo $szDisablePostcode ;?> tabindex="25" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneePostcode]" id="szConsigneePostcode" value="<?php echo $szConsigneePostcode; ?>" >
            </p>
        <?php }?> 
        <?php if($shipperConsigneeAry['iGetConsigneePostcodeFromGoogle']!=1 && $bDisplayDonotKnowCheckbox && $kBooking->iCourierBooking!=1){ ?> 
            <p class="dont-know"><span class="checkbox-container fl" onmouseover="show_mainpage_tooltip('<?php echo $szPostcodeMouseOverString; ?>','','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')" ><input type="checkbox"  name="shipperConsigneeAry[iDonotKnowConsigneePostcode]" <?php echo (($shipperConsigneeAry['iDonotKnowConsigneePostcode']==1)?'checked':''); ?> onclick="change_shipper_fields(this.id,'szConsigneePostcode')" id="iDonotKnowConsigneePostcode" value="1"><?=t($t_base.'fields/dont_know');?></span></p>

        <?php } ?>  
    </div>

    <div class="oh odd">	 
        <?php if($shipperConsigneeAry['iGetConsigneeCityFromGoogle']==1){ ?>
        <p>
            <label><?php echo $shipperConsigneeAry['szConsigneeCity']; ?></label>
            <input type="hidden" name="shipperConsigneeAry[szConsigneeCity]" id="szConsigneeCity" value="<?php echo $shipperConsigneeAry['szConsigneeCity']; ?>" >
        </p>
        <?php } else {?>
        <p>	
            <input type="text" tabindex="26" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeCity]" id="szConsigneeCity" value="<?php echo $shipperConsigneeAry['szConsigneeCity']; ?>" >
        </p>
        <?php } ?> 
    </div> 
    <input type="hidden" name="shipperConsigneeAry[idConsigneeCountry]" id="idConsigneeCountry" value="<?php echo $shipperConsigneeAry['idConsigneeCountry']; ?>">
    <input type="hidden" name="shipperConsigneeAry[iGetConsigneeAddressFromGoogle]" id="iGetConsigneeAddressFromGoogle" value="<?php echo$shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] ?>">
    <input type="hidden" name="shipperConsigneeAry[iGetConsigneeCityFromGoogle]" id="iGetConsigneeCityFromGoogle" value="<?php echo$shipperConsigneeAry['iGetConsigneeCityFromGoogle'] ?>">
    <input type="hidden" name="shipperConsigneeAry[iGetConsigneePostcodeFromGoogle]" id="iGetConsigneePostcodeFromGoogle" value="<?php echo $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle']?>">
    <?php
}

function display_booking_details($postSearchAry,$kRegisterShipCon,$iBookingQuotes=false,$szCargoLogString=false)
{
    $iDonotCalculateAgain = $kRegisterShipCon->iDonotCalculateAgain;
    if(!empty($kRegisterShipCon->arErrorMessages))
    {
        $formId = 'booking_detail_form';
        $szValidationErrorKey = '';
        foreach($kRegisterShipCon->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            ?>
            <script type="text/javascript">
                    $("#"+'<?php echo $errorKey; ?>').attr('class','red_border');
            </script>
            <?php
        }
    }
    $kRegisterShipCon_new = new cRegisterShipCon(); 
    $kConfig = new cConfig();
    $kBooking= new cBooking();
    $idBooking = $postSearchAry['id'] ;
    $kBooking->load($idBooking);

    $t_base = "BookingDetails/";
    $t_base_user = "Users/AccountPage/"; 

    if(!empty($_POST['shipperConsigneeAry']))
    {
        $shipperConsigneeAry = $_POST['shipperConsigneeAry'] ;
        $shipperConsigneeAry['szShipperPhone'] = urldecode($shipperConsigneeAry['szShipperPhone']);

        if(isset($shipperConsigneeAry['iThisIsMe']))
        {
            $iThisIsMe = $shipperConsigneeAry['iThisIsMe'];
        }
        else
        {
            $iThisIsMe = 1;
        } 
        $szCargoCommodity = $shipperConsigneeAry['szCargoCommodity'] ;
        $idCargo = $shipperConsigneeAry['idCargo'] ;
        $iGetDataFromDB = false;
        $iThisismeDisplayed = $shipperConsigneeAry['iThisismeDisplayed'];
    } 
    else
    {
        $shipperConsigneeAry = $kRegisterShipCon_new->getShipperConsigneeDetails($idBooking);
        $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($idBooking);
                    
        $iGetDataFromDB = true; 
        if(!empty($cargoDetailsAry))
        {
            $szCargoCommodity = utf8_decode($cargoDetailsAry['1']['szCommodity']);
            $idCargo = $cargoDetailsAry['1']['id'] ;
        } 
        if(empty($szCargoCommodity))
        {
           $szCargoCommodity =  utf8_decode($postSearchAry['szCargoDescription']);
        }
        $iThisIsMe = 1 ;
        $iThisismeDisplayed = $shipperConsigneeAry['iThisismeDisplayed']; 
    } 
    
    if(empty($shipperConsigneeAry['iShipperConsignee']))
    {
        $iShipperConsignee = $postSearchAry['iShipperConsignee']; 
    }
    else
    {
        $iShipperConsignee = $shipperConsigneeAry['iShipperConsignee']; 
    }
    
    $iLanguage = getLanguageId();

    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);

    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);

    $idUserSession = $_SESSION['user_id'];

    $bDisplayThisisMe = false; 
    if($idUserSession<=0 || $_SESSION['logged_in_by_booking_process']==1)
    {  
        $bDisplayThisisMe = true;
    } 
    if($_SESSION['user_id']>0)
    {
        $kUser=new cUser();
        $kUser->getUserDetails($_SESSION['user_id']);
        $postSearchAry['iPrivateShipping'] = $kUser->iPrivate;
    } 
    $bShowBookingFields = false;
    
    $bShowBookingFields = true;
    $bDisplayThisisMeShipper = false ;
    $bDisplayThisisMe = false;
                    
    if($iBookingQuotes)
    {
        $iBookingQuotesFlag = 1; 
    }
    else
    {
        if($iShipperConsignee!=1 && $iShipperConsignee!=2)
        { 
            /*
            * If customer is neither shipper nor consignee in that cse we have this extra check so
            * If User country = from country, then "Billing" check box next to shipper, default unchecked
            * If user country = to counter, then "Billing" check box next to consignee, default unchecked 
            * Otherwise, "Billing" check box next to both shipper and consignee. As default they are unchecked. Obviously, user can only check one of them (so they work as combo), but can also have both unchecked. 
            */
            if($_SESSION['user_id']>0)
            {
                $kUser=new cUser();
                $kUser->getUserDetails($_SESSION['user_id']);
                $idCustomerCountry = $kUser->szCountry;
            }
            if($idCustomerCountry==$postSearchAry['idOriginCountry'])
            {
                $iThisismeDisplayed = 1;
            }
            else if($idCustomerCountry==$postSearchAry['idDestinationCountry'])
            {
                $iThisismeDisplayed = 2;
            }
            else
            {
                $bDisplayThisisMe = true;
                $bDisplayThisisMeShipper = true;
                $iThisismeDisplayed = 3;
                $iThisismeDisplayedTemp = 3;
            } 
        }
        else
        {
            if($iThisismeDisplayed==3)
            {
                $iThisismeDisplayedTemp = 3;
            }
        }
    }
    
    /*
     * Prviously $bShipperConsigneeChecks only being true in case of Manual Quote but after Morten's request we made this true all the time.
     * 
     */ 
    $bShipperConsigneeChecks = true;
    if($bShipperConsigneeChecks)
    {  
        if($iShipperConsignee==1)
        {
            $bShowBookingFields = false;
            $bDisplayThisisMeShipper = true ;
            $bDisplayThisisMe = false;
            $iThisismeDisplayed = 1;
        }
        else if($iShipperConsignee==2)
        {  
            $bShowBookingFields = false;
            $bDisplayThisisMeShipper = false ;
            $bDisplayThisisMe = true;
            $iThisismeDisplayed = 2;
        } 
    } 
    else if($idUserSession<=0 && $iThisIsMe<=0)
    { 
        $bShowBookingFields = true;
    }
    else if(!empty($_POST['shipperConsigneeAry']) && $iThisIsMe<=0)
    { 
        $bShowBookingFields = true;
    } 
    if(isset($shipperConsigneeAry['iBookingQuotesFlag']))
    {
        $iBookingQuotesFlag = $shipperConsigneeAry['iBookingQuotesFlag'] ;
    }   
    if($iShipperConsignee==3 || $iShipperConsignee==0)
    { 
        $bShowBookingFields = true;
        $iThisIsMe = 0;
        if($iThisismeDisplayed==1)
        {
            $bDisplayThisisMeShipper = true ;
            $bDisplayThisisMe = false;
        }
        else if($iThisismeDisplayed==2)
        {
            $bDisplayThisisMeShipper = false ;
            $bDisplayThisisMe = true;
        }
        else if($iThisismeDisplayed==3)
        {
            $iThisismeDisplayedTemp =3;
        }
    }
    
    if($iThisismeDisplayedTemp==3)
    {
        $bDisplayThisisMe = true;
        $bDisplayThisisMeShipper = true;
        $iThisismeDisplayed = 3;
    }
    $cargoDesDisabledClass="";
    $cargoDesReadonly="";
    if($postSearchAry['iSearchMiniVersion']==7 || $postSearchAry['iSearchMiniVersion']==8 || $postSearchAry['iSearchMiniVersion']==9)
    {
        $cargoDesDisabledClass="disabled";
        $cargoDesReadonly="readonly";
        
    }
    ?>
    <script type="text/javascript">
     $(document).ready(function()
     {  
        $("#booking_detail_form").validationEngine('attach'); 
        <?php
        if(!empty($szValidationErrorKey))
        {
            ?>		       	
            $('#<?php echo $szValidationErrorKey?>').focus();
            $('html, body').animate({ scrollTop: ($("#<?php echo $szValidationErrorKey; ?>").offset().top - 60) }, "7000"); 
            <?php
        }
        if(($iShipperConsignee==1 || $iShipperConsignee==2) && $iThisismeDisplayed==3)
        { 
   ?>       toggle_billing_address('','<?php echo $iShipperConsignee; ?>','<?php echo $postSearchAry['iPrivateShipping']?>');
        <?php } ?>    
        Custom.init();
       }); 
    </script>	
    <form action="" id="booking_detail_form" name="booking_detail_form" method="post">
        <?php  
            echo '<a href="javascript:void(0)" style="color:#eff0f2;" onclick=showHideCourierCal("courier_service_calculation_cargo_detail")>.</a><div class="clearfix" id="courier_service_calculation_cargo_detail" style="font-size: 16px;text-align:left;display:none;"><div id="popup-bg"></div><div id="popup-container"><div class="popup abandon-popup" style="width:900px;"><p class="close-icon" align="right"><a onclick=showHideCourierCal("courier_service_calculation_cargo_detail") href="javascript:void(0);"><img alt="close" src='.__BASE_STORE_IMAGE_URL__.'/close1.png>
            </a></p><div style="height:400px;overflow-y:auto;overflow-x:hidden" id="cargo_calculation_details_container">'.$szCargoLogString."</div></div></div></div>";
                    
        ?>
		<div class="clearfix">
                    <div class="shipper-details-container">
                        <h5>
                            <?php 
                            
                            if(!empty($shipperConsigneeAry['szShipperHeading']))
                            { 
                                echo $shipperConsigneeAry['szShipperHeading'];
                            }
                            else
                            {
                                if($kBooking->iCourierBooking==1)
                                {
                                    echo t($t_base.'fields/courier_shipper');
                                }
                                else
                                {
                                     echo t($t_base.'fields/shipper');
                                }    
                            }  
                            if($bDisplayThisisMeShipper){?> 
                                <span class="checkbox-container"><input type="checkbox"  name="shipperConsigneeAry[iThisIsMe]" onclick="toggle_billing_address(this.id,'1','<?=$postSearchAry['iPrivateShipping']?>');" id="iThisIsMe_shipper" <?php echo (($iThisIsMe==1)?'checked':''); ?> value="1"><?=t($t_base.'fields/this_is_billing_address');?></span>
                            <?php }?>
                        </h5>
                        <div id="shipper_info_div">
                            <?php 
                                echo display_shipper_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry,$iBookingQuotes);
                            ?>
                        </div>
                    </div>
                    <div class="consignee-details-container" > 
                        <h5 class="clearfix">
                            <?php 
                            if(!empty($shipperConsigneeAry['szConsigneeHeading']))
                            { 
                                echo '<span class="fl">'.$shipperConsigneeAry['szConsigneeHeading']."</span>";
                            }
                            else
                            { 
                                if($kBooking->iCourierBooking==1)
                                {
                                    echo '<span class="fl">'.t($t_base.'fields/courier_receiver').'</span>';
                                }
                                else
                                {
                                    echo '<span class="fl">'. t($t_base.'fields/consignee').'</span>';
                                }
                            } 
                            if($bDisplayThisisMe){
                                ?> 
                                <span class="checkbox-container fr"><input type="checkbox"  name="shipperConsigneeAry[iThisIsMe]" onclick="toggle_billing_address(this.id,'2','<?=$postSearchAry['iPrivateShipping']?>');" id="iThisIsMe_consignee" <?php echo (($iThisIsMe==1)?'checked':''); ?> value="1"><?=t($t_base.'fields/this_is_billing_address');?></span>
                            <?php }?> 
                        </h5> 
                        <div id="consignee_info_div">
                            <?php
                                echo display_consignee_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry,$iBookingQuotes);
                            ?>
                        </div>
                    </div> 
		</div>
		<div class="even cargo-description clearfix">
                    <p class="text"><?=t($t_base.'fields/commodity');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/CARGO_DESCRIPTION_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/CARGO_DESCRIPTION_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>		
                    <p class="field <?php echo $cargoDesDisabledClass;?>">
                        <input  <?php echo $cargoDesReadonly;?> type="text" tabindex="27" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szCargoCommodity]" id="szCargoCommodity" value="<?php echo $szCargoCommodity; ?>">
                        <input type="hidden" name="shipperConsigneeAry[idCargo]" id="idCargo" value="<?php echo $idCargo; ?>">
                        <input type="hidden" id="szTransportation" name="shipperConsigneeAry[idServiceType]" value="<?=$postSearchAry['idServiceType']?>">
                        <input type="hidden" id="idForwarder" name="shipperConsigneeAry[idForwarder]" value="<?=$postSearchAry['idForwarder']?>">
                        <input type="hidden" id="szForwarderDispName" name="shipperConsigneeAry[szForwarderDispName]" value="<?=$postSearchAry['szForwarderDispName']?>">
                        <input type="hidden" id="szOperationType" name="shipperConsigneeAry[szOperationType]" value="NEW_BOOKING_PAGE">
                        <input type="hidden" name="shipperConsigneeAry[szBookingRandomNum]" id="szBookingRandomNum_booking_details" value=""/> 
                        <input type="hidden" name="shipperConsigneeAry[iShipperConsignee]" id="iShipperConsignee" value="<?php echo $iShipperConsignee; ?>"> 
                        <input type="hidden" name="shipperConsigneeAry[szConsigneeHeading]" id="szConsigneeHeading" value="<?php echo $shipperConsigneeAry['szConsigneeHeading']?>"/>
                        <input type="hidden" name="shipperConsigneeAry[szShipperHeading]" id="szShipperHeading" value="<?php echo $shipperConsigneeAry['szShipperHeading']?>"/> 
                        <input type="hidden" name="shipperConsigneeAry[iBookingQuotesFlag]" id="iBookingQuotesFlag" value="<?php echo $iBookingQuotesFlag; ?>"/>
                        <input type="hidden" name="shipperConsigneeAry[iDonotCalculateAgain]" id="iDonotCalculateAgain" value="<?php echo $iDonotCalculateAgain; ?>"/>
                        <input type="hidden" name="shipperConsigneeAry[iThisismeDisplayed]" id="iThisismeDisplayed" value="<?php echo $iThisismeDisplayed; ?>"/>
                        
                        
                    </p> 
		</div>
		<div class="billing-details-container" id="billing-address-container" <?php if($bShowBookingFields){?>style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                    <h5><?=t($t_base.'fields/billing_address');?></h5>
                    <div id="billing_info_div">
                        <?php 
                        if($iGetDataFromDB)
                        {
                            if($iBookingQuotes)
                            {
                                $bookingIdAry = array();
                                $bookingIdAry[0] = $idBooking ;
                                $newInsuredBookingAry = array();
                                $newBookingQuotesAry = $kBooking->getAllBookingQuotes(false,$bookingIdAry);

                                $shipperConsigneeAry['szBillingCompanyName'] = $newBookingQuotesAry[0]['szCustomerCompanyName'];
                                $shipperConsigneeAry['szBillingFirstName'] = $newBookingQuotesAry[0]['szFirstName'];
                                $shipperConsigneeAry['szBillingLastName'] = $newBookingQuotesAry[0]['szLastName'];
                                $shipperConsigneeAry['szBillingEmail'] = $newBookingQuotesAry[0]['szEmail'];
                                $shipperConsigneeAry['idBillingDialCode'] = $newBookingQuotesAry[0]['idCustomerDialCode'];
                                $shipperConsigneeAry['szBillingPhone'] = $newBookingQuotesAry[0]['szCustomerPhoneNumber'];
                                $shipperConsigneeAry['szBillingAddress'] = $newBookingQuotesAry[0]['szCustomerAddress1'];
                                $shipperConsigneeAry['szBillingPostcode'] = $newBookingQuotesAry[0]['szCustomerPostCode'];
                                $shipperConsigneeAry['szBillingCity'] = $newBookingQuotesAry[0]['szCustomerCity'];
                                $shipperConsigneeAry['idBillingCountry'] = $newBookingQuotesAry[0]['szCustomerCountry'];
                            }
                            else if($_SESSION['user_id']>0)
                            {
                                $kUser=new cUser();
                                $kUser->getUserDetails($_SESSION['user_id']);

                                $shipperConsigneeAry['szBillingCompanyName'] = $kUser->szCompanyName;
                                $shipperConsigneeAry['szBillingFirstName'] = $kUser->szFirstName;
                                $shipperConsigneeAry['szBillingLastName'] = $kUser->szLastName;
                                $shipperConsigneeAry['szBillingEmail'] = $kUser->szEmail;
                                $shipperConsigneeAry['idBillingDialCode'] = $kUser->idInternationalDialCode;
                                $shipperConsigneeAry['szBillingPhone'] = $kUser->szPhoneNumber;
                                $shipperConsigneeAry['szBillingAddress'] = $kUser->szAddress1;
                                $shipperConsigneeAry['szBillingPostcode'] = $kUser->szPostcode;
                                $shipperConsigneeAry['szBillingCity'] = $kUser->szCity ;
                                $shipperConsigneeAry['idBillingCountry'] = $kUser->szCountry;  
                            }
                        }
                        echo display_billing_information_html_new($postSearchAry,$allCountriesArr,$dialUpCodeAry,$shipperConsigneeAry,$kRegisterShipCon);
                        ?>
                    </div> 
		</div> 
                <?php
                    if(!empty($kRegisterShipCon->szSpecialErrorMessage))
                    {
                        echo '<br><div class="cargo-success-text" style="text-align:center"> '; 
                        echo $kRegisterShipCon->szSpecialErrorMessage ;  
                        echo '</div>'; 
                    }
                    else 
                    { 
                        echo "<br>";
                    }
                ?> 
		<div class="booking_details_buttons clearfix">
                    <!--<p class="back-button"><a href="<?=__SELECT_SIMPLE_SERVICES_URL__.'/'.$postSearchAry['szBookingRandomNum'].'/'?>" ><< <span><?=t($t_base.'fields/back');?></span></a></p> -->
                    <p class="continue-button"><a href="javascript:void(0);" onclick="submit_bookig_details();"  class="button1"><span><?=t($t_base.'fields/continue');?></span></a></p>
		</div>
	</form>  
	<?php
}

function confirmBookingForBankTransfer($postSearchAry)
{ 
    $kForwarder = new cForwarder();
    $bookingRefLogAry = array();
    $iLanguage = getLanguageId();

    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $kForwarderContact= new cForwarderContact();
    $kUser = new cUser();
    $kWHSSearch = new cWHSSearch();

//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    { 
//        $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//    } 
//    else 
//    {
        $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$iLanguage); 
    //}

    $kForwarder->load($postSearchAry['idForwarder']);
    $iCreditDays= $kForwarder->iCreditDays;
    $szForwarderVatRegistrationNum= $kForwarder->szVatRegistrationNum;
    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
    {
        $szBankTransferDueDateTime = strtotime($postSearchAry['dtCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);
        //$szBankTransferDueDateTime = date('j. F',$szBankTransferDueDateTime);
        
        $iMonth = (int)date("m",$szBankTransferDueDateTime);
        $szMonthName = getMonthName($iLanguage,$iMonth);
        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime)." ".$szMonthName;
    }
    else
    {
        $szBankTransferDueDateTime = strtotime($postSearchAry['dtWhsCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);
        
        $iMonth = (int)date("m",$szBankTransferDueDateTime);
        $szMonthName = getMonthName($iLanguage,$iMonth);
        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime)." ".$szMonthName;
    } 
    $iLanguage = getLanguageId();

    $kAdmin  =new cAdmin();
    $invoiceNumber = '';
    if($postSearchAry['iInsuranceIncluded']==1)
    { 
        //$invoiceNumber=$kAdmin->generateInvoiceForPayment();
        $invoiceNumber = $postSearchAry['szInvoice'];
    }
    
    $kWhsSearch = new cWHSSearch();  
    $iNumDaysPayBeforePickup = $kWhsSearch->getManageMentVariableByDescription('__NUMBER_OF_WEEKDAYS_PAYMENT_REQUIRED_BEFORE_REQUESTED_PICKUP__'); 
    
    $dtBookingConfirmed = $kBooking->getRealNow();
    
    $iBookingType = $postSearchAry['iBookingType'];
    /*
    * Re-validating iBookingType Flag to make system robust
    */
    if($postSearchAry['idServiceProvider']>0 && $postSearchAry['iCourierBooking']==1)
    {
        $iBookingType = __BOOKING_TYPE_COURIER__ ;
    }
    else if($postSearchAry['iQuotesStatus']>0)
    {
        $iBookingType = __BOOKING_TYPE_RFQ__ ;
    }
    else
    {
        $iBookingType = __BOOKING_TYPE_AUTOMATIC__ ;
    } 
    //$kBooking->generateInvoice($postSearchAry['idForwarder'],true); 
    $szInvoice = $kAdmin->generateInvoiceForPayment(true);
    $kBooking = new cBooking();		
    $updateAry['szInvoice'] = $szInvoice;
    $updateAry['dtBookingConfirmed'] = $dtBookingConfirmed;
    $updateAry['dtBooked'] = $dtBookingConfirmed;
    $updateAry['idBookingStatus'] = 3 ;
    $updateAry['szPaymentStatus'] = 'Completed';
    $updateAry['iBookingStep'] = 13 ;
    $updateAry['iTransferConfirmed'] = 1;
    $updateAry['iCreditDays'] = $iCreditDays;
    $updateAry['szForwarderVatRegistrationNum'] = $szForwarderVatRegistrationNum;
    $updateAry['iNumDaysPayBeforePickup'] = $iNumDaysPayBeforePickup;
    if($postSearchAry['iInsuranceIncluded']==1)
    {
        $updateAry['fCustomerPaidAmount'] = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency'] + $postSearchAry['fTotalVat'];
    }
    else
    {
        $updateAry['fCustomerPaidAmount'] = $postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat'];
    } 
    $updateAry['szInsuranceInvoice'] = $invoiceNumber ;
    $updateAry['iBookingType'] = $iBookingType;
    
    if($postSearchAry['iBookingType']==__BOOKING_TYPE_RFQ__)
    { 
        if($postSearchAry['iIncludeComments']==1)
        {
            $updateAry['szOtherComments'] = $postSearchAry['szForwarderComment'];
        }
        $updateAry['iQuotesStatus'] = __BOOKING_QUOTES_STATUS_WON__ ; 
    } 
    
    if($postSearchAry['iRailTransport']==1 && $postSearchAry['idServiceType']!=__SERVICE_TYPE_DTD__)
    {
        $updateAry['idServiceType'] = __SERVICE_TYPE_DTD__;
        $updateAry['idServiceTerms'] = __SERVICE_TERMS_EXW__;
    }
    if(!empty($updateAry))
    {
        $update_query = '';
        foreach($updateAry as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }
    $update_query = rtrim($update_query,",");  

    $kBooking->updateDraftBooking($update_query,$_SESSION['booking_id']); 
    
    $bookingDataArr=$kBooking->getExtendedBookingDetails($_SESSION['booking_id']);
    $isManualCourierBooking = $bookingDataArr['isManualCourierBooking'];
    
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
    {
        $szBankTransferDueDateTime=date('Y-m-d',strtotime($bookingDataArr['dtCutOff']));
        $szBankTransferDueDateTime=getBusinessDaysForCourier($szBankTransferDueDateTime,$iNumDaysPayBeforePickup.' DAY');
        
        $iMonth = (int)date("m",strtotime($szBankTransferDueDateTime));
        $szMonthName = getMonthName($iLanguage,$iMonth);
        $szBankTransferDueDateTime = date('d.',strtotime($szBankTransferDueDateTime))." ".$szMonthName;		 
    } 
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__ || $bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
    {
        $kBooking = new cBooking();
        $dtResponseTime = $kBooking->getRealNow();
    
        $idBookingFile = $bookingDataArr['idFile'] ;  
        $fileLogsAry = array();
        $fileLogsAry['szTransportecaStatus'] = "S6";
        $fileLogsAry['szTransportecaTask'] = ""; //T4
        $fileLogsAry['dtFileStatusUpdatedOn'] = $dtResponseTime;
        $fileLogsAry['dtTaskStatusUpdatedOn'] = $dtResponseTime; 
        $fileLogsAry['iStatus'] = 6; 
        $fileLogsAry['iClosed'] = 0; 
        
        if(!empty($fileLogsAry))
        {
            foreach($fileLogsAry as $key=>$value)
            {
                $file_log_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
            }
        }  
        $file_log_query = rtrim($file_log_query,",");   
        $kBooking->updateBookingFileDetails($file_log_query,$idBookingFile,true); 
    } 
  
    $bookingDataArr['iTransferConfirmed'] = 1;  
    if($bookingDataArr['iCourierBooking']==1)
    {
        $kCourierServices=new cCourierServices();
        $courierBookingArr=$kCourierServices->getCourierBookingData($bookingDataArr['id']);
        if($courierBookingArr[0]['iCourierAgreementIncluded']==0)
        {
            $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate'];
            if($courierBookingArr[0]['idBookingLabelFeeCurrency']!=1)
            {
                $courLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate']*$courierBookingArr[0]['fBookingLabelFeeROE'];
            }
            $bookingDataArr['fLabelFeeUSD']=$courLabelFee;
        }
    }

    $kBooking->addrForwarderTranscaction($bookingDataArr); 
    $idCurrency = $postSearchAry['idCustomerCurrency'];
    $currencyDetailsAry = array();
    $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency);

    $replace_ary = array();

    $idForwarder = $postSearchAry['idForwarder'] ;
    $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
    $iBookingLanguage = $postSearchAry['iBookingLanguage'] ;
    //Send Bank Transfer recieved success email to user				
    $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$postSearchAry['idOriginCountry'],false,$iBookingLanguage);
    $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$postSearchAry['idDestinationCountry'],false,$iBookingLanguage) ;

    $szOriginCountry = $szOriginCountryAry[$postSearchAry['idOriginCountry']]['szCountryName'] ;
    $szDestinationCountry = $szDestinationCountryAry[$postSearchAry['idDestinationCountry']]['szCountryName'] ;
    if($bookingDataArr['iInsuranceIncluded']==1)
    {
        $fTotalBookingCost = ($bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalInsuranceCostForBookingCustomerCurrency'] + $bookingDataArr['fTotalVat']);
    }
    else
    {
        $fTotalBookingCost = ($bookingDataArr['fTotalPriceCustomerCurrency'] + $bookingDataArr['fTotalVat']);
    }
    
//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    { 
//        $fTotalBookingCost = number_format((float)$fTotalBookingCost,0,'.','.');
//    }
//    else
//    { 
//        $fTotalBookingCost = number_format((float)$fTotalBookingCost,$iLanguage);
//    }
    $fTotalBookingCost = number_format_custom((float)$fTotalBookingCost,$iLanguage);
    
    $replace_ary['szTotalAmount'] = $bookingDataArr['szCurrency']." ".$fTotalBookingCost;
    $replace_ary['idUser'] = $postSearchAry['idUser'];
    $replace_ary['szFirstName'] = $postSearchAry['szFirstName'];
    $replace_ary['szLastName'] = $postSearchAry['szLastName'];
    $replace_ary['szEmail'] = $postSearchAry['szEmail'];
    $replace_ary['szForwarderDispName'] = $postSearchAry['szForwarderDispName'];
    $replace_ary['szForwarderCustServiceEmail'] = $customerServiceEmailAry[0];
    $replace_ary['szBookingRef'] = $postSearchAry['szBookingRef'];
    $replace_ary['szLatestPaymentDate'] =  $szBankTransferDueDateTime ;

    $replace_ary['szBank'] = $currencyDetailsAry['szBankName'] ;
    $replace_ary['szSortCode'] = $currencyDetailsAry['szSortCode'] ;
    $replace_ary['szAccountNumber'] = $currencyDetailsAry['szAccountNumber'] ;
    $replace_ary['szSwift'] = $currencyDetailsAry['szSwiftNumber'] ;
    $replace_ary['szNameOnAccount'] = $currencyDetailsAry['szNameOnAccount'] ;
    $replace_ary['szSuuportEmail'] = __STORE_SUPPORT_EMAIL__;
    $replace_ary['szSupportPhone'] = $szCustomerCareNumer;  
    $replace_ary['szOriginCFSContact'] = $postSearchAry['szWarehouseFromContactPerson'];;
    $replace_ary['szOriginCFSEmail'] = $postSearchAry['szWarehouseFromEmail'];
    $replace_ary['szShipperCompany'] = $postSearchAry['szShipperCompanyName']; 
    $replace_ary['szOriginCountry'] = $szOriginCountry;
    $replace_ary['szDestinationCountry'] = $szDestinationCountry;
   
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
        if($postSearchAry['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        {  
            //$replace_ary['szTransactionDetailsText'] = "We have enclosed your Invoice for your kind settlement. Please arrange for a bank transfer of ".$replace_ary['szTotalAmount']." to our account. We will send you the booking confirmation when the payment has been received.";
        } 
        else
        {
            //$replace_ary['szTransactionDetailsText'] = "We have enclosed your Invoice for your kind settlement. Please arrange for a bank transfer of ".$replace_ary['szTotalAmount']." to our account, so we receive the payment latest by ".$replace_ary['szLatestPaymentDate'].":";
        } 
    }
    else
    { 
        if($postSearchAry['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__)
        {  
            //$replace_ary['szTransactionDetailsText'] = "Vi har vedlagt din faktura. For at bekræfte din bestilling skal du lave en bankoverførsel på ".$replace_ary['szTotalAmount'].". Vi sender dig en bekræftelse så snart vi har modtaget betalingen.";
        } 
        else
        {
           // $replace_ary['szTransactionDetailsText'] = "Vi har vedlagt din faktura. For at bekræfte din bestilling skal du lave en bankoverførsel på ".$replace_ary['szTotalAmount'].", så vi modtager det fulde beløb på vores konto senest ".$replace_ary['szLatestPaymentDate'].":";
        } 
    }
    
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_COURIER__)
    {
        //$replace_ary['szName']=$postSearchAry['szFirstName']." ".$postSearchAry['szLastName'];
        //$replace_ary['szSenderEmail']=$postSearchAry['szShipperEmail'];
        //createEmail(__COURIER_BOOKING_CONFIRMATION_AND_INVOICE_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true,$postSearchAry['id']);
        createEmail(__BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true);
    }
    else
    {
    
        if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__)
        {  
            createEmail(__MANUAL_BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true);
        }
        else
        { 
            createEmail(__BOOKING_BY_BANK_TRANSFER_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true);
        } 
    }
    // 13-06-2016 Could you please also turn off and delete the e-mail template "Transporteca - New booking with bank transfer"?
   // createEmail(__BANK_TRANSFER_BOOKING_TO_SUPPORT__, $replace_ary,__STORE_SUPPORT_EMAIL__,' ', __STORE_SUPPORT_EMAIL__,'', __STORE_SUPPORT_EMAIL__);
}
function customer_feedback_form()
{
    $t_base = "BookingReceipt/";
    ?>
    <form name="customerFeedBack" id="customerFeedBack" method="post">
        <h2><?=t($t_base.'fields/your_opinion_means')?></h2>
        <h5><?=t($t_base.'fields/would_you_recomend')?></h5> 
        <p class="radio-btns clearfix"><span class="left-text" ><?=t($t_base.'fields/not_at_all_likely')?></span>
        <?php 
            if(__CUSTOMER_FEEDBACK_RATING__>0)
            {
                for($i=0;$i<=__CUSTOMER_FEEDBACK_RATING__;++$i)
                {
            ?>
                <span class="readio-container"><?=$i?><input type="radio" name="custfeedbackArr[iScore]" id="iScore_<?=$i?>" value="<?=$i?>" onclick="enable_feedback_submit_button_new(this.value);"></span>
            <?php	
                }
            }
        ?>
        <span class="right-text"><?=t($t_base.'fields/extremely_likely')?></span></p>
        <p><?=t($t_base.'fields/feedback_popup_line_2')?></p>
        <p><textarea cols="30" rows="5" name="custfeedbackArr[szComment]" id="szComment"></textarea></p> 
        <p align="center">
            <input type="hidden" name="path" value="<?=$path?>" id="path">
            <a href="javascript:void(0)" style="opacity:0.4" class="button1" id="submit_feedback_button"><span><?=t($t_base.'fields/submit')?></span></a>
        </p>
    </form>
    <?php
}

function display_email_verification_form($kUser,$iFirstTimeFlag=false,$success_flag=false)
{
	if(!empty($kUser->arErrorMessages))
	{
		$formId = 'user-email-verification-form';
		$szValidationErrorKey = '';
		foreach($kUser->arErrorMessages as $errorKey=>$errorValue)
		{
			if(empty($szValidationErrorKey))
			{
				$szValidationErrorKey = $errorKey;
			}
			?>
			<script type="text/javascript">
				$("#"+'<?php echo $errorKey; ?>').attr('class','red_border');
			</script>
			<?php
		}
	}
	$t_base = "home/homepage/"; 
?>	 
<div class="booking-feedback-container">
	<p><?php echo t($t_base.'fields/verify_your_email_heading');?> </p>
	<form action="" method="post" id="user-email-verification-form" name="user-email-verification-form">
		<div class="clearfix">
			<input type="text" value="<?php echo (!empty($_POST['updateUserEmail']['szEmail'])?$_POST['updateUserEmail']['szEmail']:$kUser->szEmail); ?>" id="szEmail" onfocus="validate_verfify_email_field(event,this.form.id,this.id);check_form_field_not_required(this.form.id,this.id);" onkeypress="validate_verfify_email_field(event,this.form.id,this.id);" onblur="validate_email(this.form.id,this.id);" onkeypress="" name="updateUserEmail[szEmail]">
			<input type="hidden" name="showflag" value="VERIFY_USER_EMAIL"id="showflag">
			<a id="email_verification_button" style="opacity:0.4" class="button1"><?php echo t($t_base.'fields/send');?></a>
		</div>
	</form> 
<?php if($success_flag){  
	echo "<p style='margin-top:25px;'>".t($t_base.'fields/success_message_1')." ".$kUser->szEmail.". ".t($t_base.'fields/success_message_2')."</p>";
} if($iFirstTimeFlag){ ?>
<script type="text/javascript">
validate_verfify_email_field('','user-email-verification-form','szEmail',1); 
</script>
<?php }?>
</div>
	<?php
}
function display_confirmation_popup()
{ 
	$t_base_user = "Users/AccountPage/"; 
	$redirect_url = __HOME_PAGE_URL__ ;
	if($_SESSION['valid_confirmation']==1)
	{ 
		if((int)$_SESSION['user_id']<=0) 
		{ 
			$_SESSION['verified_email_user'] = $_SESSION['idUser'];
			?>
			<script type="text/javascript">
				ajaxLogin('<?=$redirect_url?>');
			</script>
	<?php }else{ ?>
				<div id="email_vification_popup">
				<div id="popup-bg"></div>
				<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
				<h5><?=t($t_base_user.'messages/verified_header')?></h5> 
				<p><?=t($t_base_user.'messages/your_email_has_verifed_2');?></p><br/>
				<p align="center">
					<a href="javascript:void(0);" onclick="closePopup('email_vification_popup');" class="button1"><span><?=t($t_base_user.'messages/close');?></span></a>
				</p> 
				</div>
				</div>
				</div>
				<?php 
					$_SESSION['valid_confirmation'] = '';
					unset($_SESSION['valid_confirmation']);	
				}
			}else if($_SESSION['valid_confirmation']==2){?>
				<div id="popup-bg"></div>
					<div id="popup-container">
					<div class="popup signin-popup signin-popup-verification">
						<h5><?=t($t_base_user.'messages/verification')?></h5>
						<p><?=t($t_base_user.'messages/invalid_con_key');?></p>
						<br/>
						<p align="center"><a href="<?=$redirect_url?>" class="button1"><span><?=t($t_base_user.'fields/close');?></span></a></p>
					</div>
				</div>
			<?php 
			$_SESSION['valid_confirmation'] = '';
			unset($_SESSION['valid_confirmation']);
		} else if($_SESSION['display_booking_saved_popup']==1){ ?>
			<div id="email_vification_popup">
				<div id="popup-bg"></div>
					<div id="popup-container">
					<div class="popup signin-popup signin-popup-verification">
						<h5><?=t($t_base_user.'messages/your_booking_saved_title')?></h5>
						<p><?=t($t_base_user.'messages/your_booking_saved_message');?></p>
						<br/>
						<p align="center"><a href="javascript:void(0);" onclick="closePopup('email_vification_popup');" class="button1"><span><?=t($t_base_user.'fields/close');?></span></a></p>
					</div>
				</div>
			</div>
			<?php 
			$_SESSION['display_booking_saved_popup'] = '';
			unset($_SESSION['display_booking_saved_popup']);
		}
}

function display_form_validation_error_message($formId,$validationErrorAry,$iDisplay_long=false,$iDisplay_popup=false)
{
    if(!empty($validationErrorAry))
    {
        foreach($validationErrorAry as $key=>$validationErrorArys)
        {			
            ?>
            <script type="text/javascript">
                displayFormFieldIsRequired('<?php echo $formId; ?>', '<?php echo $key; ?>', '<?php echo $validationErrorArys;?>','<?php echo $iDisplay_long ;?>','<?php echo $iDisplay_popup; ?>');
            </script>
            <?php
        }
    }
} 

function success_message_popup($szTitle,$szMessage,$div_id=false,$iRedirect_flag=false,$szPageUrl=false)
{
    if(empty($div_id))
    {
        $div_id = 'success_message_div';
    } 
    if($iRedirect_flag && !empty($szPageUrl))
    {
        $onclick_func = "redirect_url('".$szPageUrl."'); ";
    }
    else
    {
        $onclick_func = "showHide('".$div_id."'); ";
    }
    $t_base = "Users/AccountPage/";
?> 
<div id="popup-bg"></div>
<div id="popup-container">
 <div class="popup signin-popup signin-popup-verification">
	<p class="close-icon" align="right"><a onclick="<?php echo $onclick_func;?>" href="javascript:void(0);"><img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png"></a></p>
		<h4><?php echo $szTitle;?></h4>			
		<div>			
                    <p><?php echo $szMessage;?></p>
		</div> 
		<p align="center" style="padding:10px;">	  
			<a class="button1" onclick="<?php echo $onclick_func;?>"><span><?=t($t_base.'fields/close')?></span></a> 
		</p>
	</div>
</div>
	<?php
}
function display_first_time_password_popup($kUser,$iPageType=false)
{ 
    if(!empty($kUser->arErrorMessages))
    {
        $formId = 'firstTimeChangePassword';
        $szValidationErrorKey = '';
        foreach($kUser->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            if($errorKey == 'szPassword' || $errorKey == 'szConPassword_1')
            {
                $szSpecialKeyAry[$errorKey] = $errorValue;
            } 
            if($errorKey == 'szPassword_1')
            {
                $errorKey = 'szPassword';
            } 
        ?>
            <script type="text/javascript">
                    $("#"+'<?php echo $errorKey; ?>').attr('class','red_border');
            </script>
            <?php
        }
    } 
    if(isset($_POST['firstTimePassword']['iPageType']))
    {
        $iPageType = $_POST['firstTimePassword']['iPageType'] ;
    } 
    $t_base = "Users/AccountPage/";
    $t_base_error = "Error";
?>	
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup signin-popup signin-popup-verification">
           <p class="close-icon" align="right"><a onclick="showHide('user_account_login_popup');" href="javascript:void(0);"><img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png"></a></p>
           <form name="firstTimeChangePassword" id="firstTimeChangePassword" method="post">
               <h5><strong><?=t($t_base.'fields/new_password_heading')?></strong></h5>
               <?php if(!empty($szSpecialKeyAry)){	?>
               <div id="regError" class="errorBox ">
                   <div class="header"><?=t($t_base_error.'/please_following');?></div>
                   <div id="regErrorList">
                       <ul>
                           <?php foreach($szSpecialKeyAry as $key=>$values){ ?><li><?php echo $values?></li><?php }?>
                       </ul>
                   </div>
               </div>
               <?php } ?>
               <p><?=t($t_base.'fields/first_time_password_message'); ?></p>	 
               <div class="oh">
                   <p class="fl-35"><?=t($t_base.'fields/password');?></p>
                   <p class="fl-65" style="width:95%;">
                       <input type="password" style="width:95%;" name="firstTimePassword[szPassword]" id="szPassword" value="<?php echo $_POST['firstTimePassword']['szPassword']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                   </p>
               </div>
               <div class="oh">
                   <p class="fl-35"><?=t($t_base.'fields/repeat_password')?></p>
                   <p class="fl-65" style="width:95%;"><input type="password" style="width:95%;" name="firstTimePassword[szConPassword]" value="<?php echo $_POST['firstTimePassword']['szConPassword']; ?>" id="szConPassword" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);"></p>
               </div>  
               <input type="hidden" name="firstTimePassword[iPageType]" value="<?php echo $iPageType; ?>"> 
               <div align="center" class="btn-container" style="margin-top:20px;">
                   <a href="javascript:void(0);" class="button1" id="one-time-popup-save-button" onclick="firstTimeChangeUserPassword();"><span><?=t($t_base.'fields/save')?></span></a>
               </div>
           </form>
        </div>
    </div>
	<?php
}
function display_authentication_password($kUser,$iPageType=false)
{ 
	if(!empty($kUser->arErrorMessages))
	{
		$formId = 'firstTimeChangePassword';
		$szValidationErrorKey = '';
		foreach($kUser->arErrorMessages as $errorKey=>$errorValue)
		{
			if(empty($szValidationErrorKey))
			{
				$szValidationErrorKey = $errorKey;
			} 
		?>
			<script type="text/javascript">
				$("#"+'<?php echo $errorKey; ?>').attr('class','red_border');
			</script>
			<?php
		}
	} 
	if(isset($_POST['typePasswordAry']['iPageType']))
	{
		$iPageType = $_POST['typePasswordAry']['iPageType'] ;
	}
	
	$t_base = "Users/AccountPage/";
	$t_base_error = "Error";
	
?>	
<div id="popup-bg"></div>
<div id="popup-container">
 <div class="popup signin-popup signin-popup-verification"> 
	<p class="close-icon" align="right"><a onclick="showHide('user_account_login_popup');" href="javascript:void(0);"><img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png"></a></p>
	<form name="typePasswordForm" id="typePasswordForm" method="post" action="javascript:void(0);">
		<h5><strong><?=t($t_base.'fields/type_password')?></strong></h5> 
		<div class="oh">
			<p class="fl-33"><?=t($t_base.'fields/password');?></p>
			<p class="fl-65">
				<input type="password" style="width:97%;" name="typePasswordAry[szPassword]" onkeyup="check_authentication_keyup(event)" id="szPassword" value="<?php echo $_POST['typePasswordAry']['szPassword']; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
			</p>
		</div> 
		<p align="right"><a href="javascript:void(0)" tabindex="3" class="f-size-12" tabindex="3" onclick="open_forgot_passward_popup('user_account_login_popup','','TYPE_PASSWORD');"><?=t($t_base.'links/forgot_pass')?></a></p>
		<input type="hidden" name="typePasswordAry[iPageType]" value="<?php echo $iPageType; ?>"> 
		<p align="center"><a href="javascript:void(0);" class="button1" onclick="check_authentication();"><span><?=t($t_base.'fields/continue')?></span></a></p>
	</form>
 </div>
</div>
	<?php
}

function display_booking_insurance_form($kBooking,$bookingDetailsAry,$iBookingQuotes=false,$iIncludeWithZero=false)
{  
    $t_base = "BookingConfirmation/";	
    $szClassName_1 = "fl-40";
    $szClassName_2 = "fl-60";

    $idBooking = $bookingDetailsAry['id'];
    $postSearchAry = array();
    $postSearchAry = $kBooking->getBookingDetails($idBooking); 
    if($postSearchAry['fTotalVat']>0) 
    {
        $szContainerClass = "even";
    } 
    else
    {
        $szContainerClass = "odd";
    }
    if(!empty($kBooking->arErrorMessages))
    {
        $formId = 'insurance_form_type_'.$iType;
        $szValidationErrorKey = '';
        foreach($kBooking->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            $szSpecialKeyAry[] = $errorKey ;
            if($errorKey=='fValueOfGoods_1')
            {
                $errorKey  = 'fValueOfGoods';
            }
            ?>
            <script type="text/javascript">
                $("#"+'<?php echo $errorKey; ?>').addClass('red_border');
            </script>
            <?php
        }
    }
    
    if($postSearchAry['idTransportMode']<=0)
    {
        $postSearchAry['idTransportMode'] = 2; //LCL
    }
    if(empty($postSearchAry['szCargoType']))
    {
        $postSearchAry['szCargoType'] = '__GENERAL_CODE__';
    }
    
    $insuranceDataAry = array();
    $insuranceDataAry['idOriginCountry'] = $postSearchAry['idOriginCountry'];
    $insuranceDataAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry']; 
    $insuranceDataAry['idTransportMode'] = $postSearchAry['idTransportMode']; 
    $insuranceDataAry['szCargoType'] = $postSearchAry['szCargoType']; 
    $idTransportMode = $postSearchAry['idTransportMode'];
                    
    $kInsurance = new cInsurance();
    $buyRateListAry = array();
    $buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_BUY_RATE_TYPE__); 
    
    if(!empty($buyRateListAry))
    {
        if(!empty($buyRateListAry[$idTransportMode]))
        {
            $buyRateListAry[$idTransportMode] = $buyRateListAry[$idTransportMode] ;
        }
        else if(!empty($insuranceBuyRateAry[__ALL_WORLD_TRANSPORT_MODE_ID__]))
        {
            $buyRateListAry[$idTransportMode] = $buyRateListAry[__ALL_WORLD_TRANSPORT_MODE_ID__] ;
        }
        else if(!empty($insuranceBuyRateAry[__REST_TRANSPORT_MODE_ID__]))
        {
            $buyRateListAry[$idTransportMode] = $buyRateListAry[__REST_TRANSPORT_MODE_ID__] ;
        }
    }
    
    $insuranceSellRateAry = array();
    $insuranceSellRateAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_SELL_RATE_TYPE__,true); 
                    
    if(!empty($insuranceSellRateAry))
    {
        if(!empty($insuranceSellRateAry[$idTransportMode]))
        {
            $insuranceSellRateAry = $insuranceSellRateAry[$idTransportMode] ;
        }
        else if(!empty($insuranceSellRateAry[__ALL_WORLD_TRANSPORT_MODE_ID__]))
        {
            $insuranceSellRateAry = $insuranceSellRateAry[__ALL_WORLD_TRANSPORT_MODE_ID__] ;
        }
        else if(!empty($insuranceSellRateAry[__REST_TRANSPORT_MODE_ID__]))
        {
            $insuranceSellRateAry = $insuranceSellRateAry[__REST_TRANSPORT_MODE_ID__] ;
        }
    }  
    
    if(isset($_POST['booking_key']))
    {
        $booking_key = $_POST['booking_key'];
    }
    else
    {
        $booking_key = $postSearchAry['szBookingRandomNum'] ;
    }

    if(isset($_POST['bookingInsuranceAry']['iInsurance']))
    {
        $iInsurance = (int)$_POST['bookingInsuranceAry']['iInsurance'] ;
    }
    else
    {
        if($postSearchAry['iInsuranceIncluded']==1)
        {
            $iInsurance = $postSearchAry['iInsuranceIncluded'];
        }
        else if($postSearchAry['iInsuranceUpdatedFlag']==1)
        {
            $iInsurance = 2;
        }
    }

    if(isset($_POST['bookingInsuranceAry']['fValueOfGoods']))
    {
        $fValueOfGoods = $_POST['bookingInsuranceAry']['fValueOfGoods'] ;
    }
    else
    {
        if($postSearchAry['fValueOfGoods']>0)
        { 
            $fValueOfGoods = round((float)$postSearchAry['fValueOfGoods']);
        }
    }

    if(isset($_POST['bookingInsuranceAry']['idInsuranceCurrency']))
    {
        $idInsuranceCurrency = $_POST['bookingInsuranceAry']['idInsuranceCurrency'] ;
    }
    else if($postSearchAry['idGoodsInsuranceCurrency']>0)
    {
        $idInsuranceCurrency = $postSearchAry['idGoodsInsuranceCurrency'];
    }
    else
    {
        $idInsuranceCurrency = $postSearchAry['idCurrency'];
    }

    $kConfig=new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);

    $bDisplayInsurancePrices = true; 
    if($postSearchAry['iUpdateInsuranceWithBlankValue']==1)
    {
        $bDisplayInsurancePrices = false;
    }

    if($postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']>0 && $bDisplayInsurancePrices)
    {
        $szInsuranceCostStr = " (".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).") ";
    }
    //echo $szInsuranceCostStr."szInsuranceCostStr";
    $onClickYes='';
    if($postSearchAry['fValueOfGoods']>0)
    {
        $onClickYes = "addInsuranceDetails();";
    } 
    
    /*
    * While calculating insurance prices we always uses the current exchange of customer currency
    */
    if($postSearchAry['idCurrency']==1)
    {
       $postSearchAry['idCurrency'] = 1; 
       $postSearchAry['szCurrency'] = 'USD'; 
       $postSearchAry['fExchangeRate'] = 1; 
    }
    else
    {
        $kWarehouseSearch = new cWHSSearch();
        $currencyExAry = array();
        $currencyExAry = $kWarehouseSearch->getCurrencyDetails($postSearchAry['idCurrency']);	

        if(!empty($currencyExAry))
        {
            $postSearchAry['idCurrency'] = $currencyExAry['idCurrency'];
            $postSearchAry['szCurrency'] = $currencyExAry['szCurrency'];
            $postSearchAry['fExchangeRate'] = $currencyExAry['fUsdValue'];
        } 
    }
    
    $kUser = new cUser();
    $kUser->getUserDetails($postSearchAry['idUser']);
    $postSearchAry['iPrivateShipping'] = $kUser->iPrivate; 
    
    //$fTotalBookingPriceUsd = $postSearchAry['fTotalPriceUSD'] ;
    $fBookingExchangeRate = $postSearchAry['fExchangeRate'];
    $szBookingCurrency = $postSearchAry['szCurrency'];
    $idBookingCurrency = $postSearchAry['idCurrency'];
    $fTotalBookingPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency'] ;
    $fTotalVat = $postSearchAry['fTotalVat'] ;
    $iIncludeInsuranceWithZeroCargoValue = $postSearchAry['iIncludeInsuranceWithZeroCargoValue'];

    /*
    *  Processing Transportation cost for the booking
    */
    $fTotalPriceCustomerCurrency = $postSearchAry['fTotalPriceCustomerCurrency']; 
    //$fTotalVat = 0;
    if($postSearchAry['iPrivateShipping']==1)
    {
        $fTotalPriceCustomerCurrency += $postSearchAry['fTotalVat']; 
    }
    else
    {
        $iImaginaryProfitPecentage = __INSURANCE_IMAGINARY_PROFIT__;
        $iImaginaryProfit = round((float)($fTotalPriceCustomerCurrency * $iImaginaryProfitPecentage * 0.01),2);
        $iImaginaryProfitUsd = $iImaginaryProfit * $postSearchAry['fExchangeRate']; 
    }
    $fTotalBookingPriceUsd = ($fTotalPriceCustomerCurrency * $postSearchAry['fExchangeRate']);
    
    $kInsurance_new  = new cInsurance();
    /*
    * Fetching max sell rate type insurance
    */

    $insuranceDetailsAry = array();
    $kWarehouseSearch = new cWHSSearch();
    
    $largetInsuranceDataAry = array();
    $largetInsuranceDataAry = $insuranceDataAry;
    $largetInsuranceDataAry['iLargestSellRate'] = 1;

    $insuranceLargestRateAry = array();
    $insuranceLargestRateAry = $kInsurance->getBuyrateCombination($largetInsuranceDataAry,__INSURANCE_SELL_RATE_TYPE__); 
    $insuranceLargestRateArys = $insuranceLargestRateAry[$idTransportMode];

    $fLargetSellPrice = $insuranceLargestRateArys['fInsurancePriceUSD'];  
    
    /*
    * geting exchange rate for goods currency
    */
    $idInsuranceCurrency = $idInsuranceCurrency ;
    
    if($idInsuranceCurrency==1)
    {
        $idGoodsInsuranceCurrency = 1;
        $szGoodsInsuranceCurrency = 'USD';
        $fGoodsInsuranceExchangeRate = 1;  
    }
    else
    {
        $resultAry = $kWarehouseSearch->getCurrencyDetails($idInsuranceCurrency);		

        $idGoodsInsuranceCurrency = $resultAry['idCurrency'];
        $szGoodsInsuranceCurrency = $resultAry['szCurrency'];
        $fGoodsInsuranceExchangeRate = $resultAry['fUsdValue']; 
    } 
    
    $szInsuranceErrorMessage = t($t_base.'fields/maximum_insurance_value')." ".$szGoodsInsuranceCurrency;
    $szOptionYesText = t($t_base.'fields/yes_please_add_insurance'); 
    $szBookingPriceText = " (".t($t_base.'fields/including_insurance');
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    $iLanguage = getLanguageId();   
    
    
    $langArr=$kConfig->getLanguageDetails('',$iLanguage);
    
    if(!empty($langArr))
    {
        $decimal_point = $langArr[0]['szDecimalSeparator'];
        $thousand_sep = $langArr[0]['szThousandsSeparator'];
    }
    else
    {
        $decimal_point = ".";
        $thousand_sep = ",";
    }  
?>  

<script type="text/javascript">  
    
    var insuranceCalculationAry = new Array();
    insuranceCalculationAry['fLargetSellPrice'] = '<?php echo $fLargetSellPrice; ?>';
    insuranceCalculationAry['fTotalBookingPriceUSD'] = '<?php echo $fTotalBookingPriceUsd; ?>';
    insuranceCalculationAry['fBookingExchangeRate'] = '<?php echo $fBookingExchangeRate; ?>'; 
    insuranceCalculationAry['idBookingCurrency'] = '<?php echo (int)$idBookingCurrency; ?>';  
    insuranceCalculationAry['idGoodsInsuranceCurrency'] = '<?php echo $idGoodsInsuranceCurrency; ?>';
    insuranceCalculationAry['szGoodsInsuranceCurrency'] = '<?php echo $szGoodsInsuranceCurrency; ?>';
    insuranceCalculationAry['fGoodsInsuranceExchangeRate'] = '<?php echo $fGoodsInsuranceExchangeRate; ?>';
     
    var fGoodsInsurancePriceUSD = '<?php echo $fValueOfGoodsUSD ; ?>'; 
    var fTotalVat = '<?php echo $fTotalVat; ?>';
    var fTotalBookingPriceCustomerCurrency = '<?php echo $fTotalBookingPriceCustomerCurrency ?>'; 
    var decimal_point = '<?php echo $decimal_point; ?>';
    var thousand_sep = '<?php echo $thousand_sep; ?>';
    var iIncludeInsuranceWithZeroCargoValue = '<?php echo $iIncludeInsuranceWithZeroCargoValue; ?>';
    

    var insuranceBuySellRateAry  = new Array(); 
    var fGoodsInsurancePriceUSD = '<?php echo $fValueOfGoodsUSD ; ?>'; 
    var fTotalBookingPriceCustomerCurrency = '<?php echo $fTotalBookingPriceCustomerCurrency ?>'; 
    var idTransportMode = '<?php echo $idTransportMode; ?>';
    var iPrivateShipping = '<?php echo $postSearchAry['iPrivateShipping']; ?>'
    var insuranceSellRateAry  = new Array();   
    var iImaginaryProfitPecentage = '<?php echo __INSURANCE_IMAGINARY_PROFIT__; ?>';
<?php 
    if(!empty($insuranceSellRateAry))
    {
        $ctr=0;
        foreach($insuranceSellRateAry as $insuranceDetailsArys)
        { 
            ?> 
            var insuranceRateAry = new Array(); 
            insuranceRateAry['fInsuranceUptoPrice'] = '<?php echo $insuranceDetailsArys['fInsuranceUptoPrice']; ?>';
            insuranceRateAry['idInsuranceCurrency'] = '<?php echo $insuranceDetailsArys['idInsuranceCurrency']; ?>'; 
            insuranceRateAry['szInsuranceCurrency'] = '<?php echo $insuranceDetailsArys['szInsuranceCurrency']; ?>';
            insuranceRateAry['fInsuranceExchangeRate'] = '<?php echo $insuranceDetailsArys['fLatestInsuranceExchangeRate']; ?>';
            insuranceRateAry['fInsuranceRate'] = '<?php echo $insuranceDetailsArys['fInsuranceRate']; ?>';
            insuranceRateAry['fInsuranceMinPrice'] = '<?php echo $insuranceDetailsArys['fInsuranceMinPrice']; ?>';
            insuranceRateAry['szInsuranceMinCurrency'] = '<?php echo $insuranceDetailsArys['szInsuranceMinCurrency']; ?>';
            insuranceRateAry['fInsuranceMinExchangeRate'] = '<?php echo $insuranceDetailsArys['fLatestInsuranceExchangeRate']; ?>'; 
            insuranceRateAry['fInsurancePriceUSD'] = '<?php echo ($insuranceDetailsArys['fInsuranceUptoPrice'] * $insuranceDetailsArys['fLatestInsuranceExchangeRate']); ?>';
            insuranceRateAry['fInsuranceMinPriceUSD'] = '<?php echo ($insuranceDetailsArys['fInsuranceMinPrice'] * $insuranceDetailsArys['fLatestInsuranceExchangeRate']); ?>';

            insuranceSellRateAry.push(insuranceRateAry); 
            <?php 
            $ctr++;
        }
    } 
?>   
    //console.log(insuranceSellRateAry);
    function calculate_insurance_price(formId,inputId)
    {  
	var fValueOfGoods = $("#"+inputId).val();
	fValueOfGoods = parseInt(fValueOfGoods);
	 
	if(fValueOfGoods==0)
	{
            $("#option_container_span").html('<?php echo $szOptionYesText ?>');
	} 	
	if(isNaN(fValueOfGoods))
	{ 
            //fValueOfGoods = 0;
            fTotalVat = parseFloat(fTotalVat);
            var fTotalVatCustomerCurrency = 0;
            if(fTotalVat>0)
            {
                fTotalVatCustomerCurrency = fTotalVat; 
            }
            var fBookingPriceWithoutVat = parseFloat(fTotalBookingPriceCustomerCurrency);
            var fTotalInvoiceValue = fBookingPriceWithoutVat + fTotalVatCustomerCurrency;
            var booking_price_text = '<?php echo $szBookingCurrency; ?> ' + $.number(fBookingPriceWithoutVat,0,decimal_point,thousand_sep); 
            
            if($("#option_container_span").length)
            {
                $("#option_container_span").html('<?php echo $szOptionYesText ?>'); 
            }
            
            $("#booking_price_span").html(booking_price_text);
            $("#total_invoice_conatiner").html('<?php echo $szBookingCurrency; ?>' +" "+ $.number(fTotalInvoiceValue,2,decimal_point,thousand_sep)); 
            return false;
	}
        var iDisablePayButton = 0; 
        if(iIncludeInsuranceWithZeroCargoValue==1 && fValueOfGoods==0)
        {
            iDisablePayButton = 1;
        }
        var bCalculateInsurance = 1; 
        if(bCalculateInsurance==1)
        {
            var iArrayLength = insuranceSellRateAry.length ; 
            if(iArrayLength>0)
            {  
                /*
                * Converting goods price to USD
                */

                var fGoodsInsurancePriceUSD = fValueOfGoods * insuranceCalculationAry['fGoodsInsuranceExchangeRate']  ; 

                var fMaxBookingAmountToBeInsurancedUSD = parseFloat(insuranceCalculationAry['fLargetSellPrice']) - parseFloat(insuranceCalculationAry['fTotalBookingPriceUSD']);
                var fTotalBookingAmountToBeInsurancedUsd = parseFloat(insuranceCalculationAry['fTotalBookingPriceUSD']) + parseFloat(fGoodsInsurancePriceUSD);

                var szInsuranceLogText = "\n Booking Price(USD): "+insuranceCalculationAry['fTotalBookingPriceUSD'];
                szInsuranceLogText += "\n Goods Price(USD): "+fGoodsInsurancePriceUSD;
                
                if(iPrivateShipping!=1)
                {
                    /*
                    * For no-private customers we are adding 10% imaginary profit
                    */
                    var fImaginaryProfitAmount = (fTotalBookingAmountToBeInsurancedUsd*iImaginaryProfitPecentage*0.01);
                    szInsuranceLogText += "\n Imaginary Proit(USD): "+fImaginaryProfitAmount;
                    fTotalBookingAmountToBeInsurancedUsd = fTotalBookingAmountToBeInsurancedUsd + fImaginaryProfitAmount;
                }
                szInsuranceLogText += "Total Amount to be insured(USD): "+fTotalBookingAmountToBeInsurancedUsd
                //console.log(szInsuranceLogText); 
                //console.log("goods USD: "+fGoodsInsurancePriceUSD+" Booking usd: "+insuranceCalculationAry['fTotalBookingPriceUSD']+" goods exchange rate: "+insuranceCalculationAry['fGoodsInsuranceExchangeRate']);

                if(insuranceCalculationAry['fGoodsInsuranceExchangeRate']>0)
                {
                    var fMaxBookingAmountToBeInsuranced = fMaxBookingAmountToBeInsurancedUSD/insuranceCalculationAry['fGoodsInsuranceExchangeRate'];
                }
                else
                {
                    var fMaxBookingAmountToBeInsuranced = 0;
                } 
                /*
                * If max amount to be insured is greater then max sell rate then we gives an error
                */ 
                //console.log(" max usd "+fTotalBookingAmountToBeInsurancedUsd+" sell price "+insuranceCalculationAry['fLargetSellPrice']+" max amt: "+fMaxBookingAmountToBeInsuranced);
                if(fTotalBookingAmountToBeInsurancedUsd > insuranceCalculationAry['fLargetSellPrice'])
                {   
                    console.log(" ------------------ DISPLAY ERROR --------------------");
                   /*
                   * Rounding down to nearest 1000
                   */
                   if(fMaxBookingAmountToBeInsuranced>0)
                   {
                        fMaxBookingAmountToBeInsuranced = Math.floor(parseFloat(fMaxBookingAmountToBeInsuranced/1000)) * 1000 ;
                        fMaxBookingAmountToBeInsuranced = parseFloat(fMaxBookingAmountToBeInsuranced);
                   }
                   else
                   {
                        fMaxBookingAmountToBeInsuranced = 0 ;
                   }		
                   var szErrorMessage = '<?php echo $szInsuranceErrorMessage; ?>' + " " + $.number(fMaxBookingAmountToBeInsuranced) ;

                   displayFormFieldIsRequired('addInsuranceForm', 'fValueOfGoods', szErrorMessage,1,1);	

                   $("#pay_button").removeAttr('onclick');
                   $("#pay_button").attr('style','opacity:0.5');
                   $("#pay_button").attr('class','button2');
                }
                else
                {
                    check_form_field_not_required('addInsuranceForm','fValueOfGoods');

                    var insuranceDetailsAry = new Array();
                    insuranceDetailsAry = getNearestRecord(fTotalBookingAmountToBeInsurancedUsd); 

                    if (insuranceDetailsAry['fInsuranceUptoPrice'] != null) 
                    {  
                        var idCalculationInsuranceCurrency = insuranceDetailsAry['idInsuranceCurrency'];
                        
                        var fTotalInsuranceCostUsd = (fTotalBookingAmountToBeInsurancedUsd * parseFloat(insuranceDetailsAry['fInsuranceRate']))/100;   
                        var fTotalInsuranceMinCost = parseFloat(insuranceDetailsAry['fInsuranceMinPrice']); 
                        var fTotalInsuranceMinCostUsd = parseFloat(insuranceDetailsAry['fInsuranceMinPrice']) * parseFloat(insuranceDetailsAry['fInsuranceMinExchangeRate']) ;

                        var fTotalInsuranceCost = 0;
                        if(insuranceDetailsAry['fInsuranceExchangeRate']>0)
                        {
                            fTotalInsuranceCost  = Math.ceil(parseFloat(fTotalInsuranceCostUsd / parseFloat(insuranceDetailsAry['fInsuranceExchangeRate'])));
                        }  
                        //console.log(" Ins Amt: " +fTotalBookingAmountToBeInsurancedUsd+" Rate: "+insuranceDetailsAry['fInsuranceRate']+ " Min "+fTotalInsuranceMinCostUsd);
                        if(fTotalInsuranceMinCostUsd>fTotalInsuranceCostUsd)
                        { 
                            fTotalInsuranceCostUsd = fTotalInsuranceMinCostUsd ;
                            fTotalInsuranceCost = fTotalInsuranceMinCost;
                        } 
                        
                        var fTotalInsuranceCostForBookingCustomerCurrency = 0; 
                        if(insuranceCalculationAry['idBookingCurrency']==idCalculationInsuranceCurrency)
                        {
                            fTotalInsuranceCostForBookingCustomerCurrency = fTotalInsuranceCost;
                        }
                        else if(insuranceCalculationAry['fBookingExchangeRate']>0)
                        {
                            fTotalInsuranceCostForBookingCustomerCurrency  = Math.ceil(parseFloat(fTotalInsuranceCostUsd / parseFloat(insuranceCalculationAry['fBookingExchangeRate'])));
                        }
                        else
                        {
                            fTotalInsuranceCostForBookingCustomerCurrency = 0 ;
                        }
                        
                        var amount_text = '<?php echo $szBookingCurrency; ?>'+" "+ $.number(fTotalInsuranceCostForBookingCustomerCurrency);
                        $("#option_container_span").html('<?php echo $szOptionYesText ?>' + " ("+amount_text+")"); 

                        fTotalVat = parseFloat(fTotalVat);
                        var fTotalVatCustomerCurrency = 0;
                        if(fTotalVat>0)
                        {
                            fTotalVatCustomerCurrency = fTotalVat; 
                        }
                        var fBookingPriceWithoutVat = parseFloat(fTotalBookingPriceCustomerCurrency) + parseInt(fTotalInsuranceCostForBookingCustomerCurrency);
                        var fTotalInvoiceValue = fBookingPriceWithoutVat + fTotalVatCustomerCurrency;
                        var booking_price_text = '<?php echo $szBookingCurrency; ?> ' + $.number(fBookingPriceWithoutVat,0,decimal_point,thousand_sep); 

                        $("#booking_price_span").html(booking_price_text +" "+ '<?php echo $szBookingPriceText ?> '+amount_text+")");
                        $("#total_invoice_conatiner").html('<?php echo $szBookingCurrency; ?>' +" "+ $.number(fTotalInvoiceValue,2,decimal_point,thousand_sep)); 

                        $("#pay_button").removeAttr('style');
                        $("#pay_button").attr('onclick',"booking_on_hold('CONFIRMED')"); 
                        $("#pay_button").attr('class','button1'); 
                    }
                    else
                    {
                        var szErrorMessage = "No insurance service available for this trade";
                        displayFormFieldIsRequired('addInsuranceForm', 'fValueOfGoods', szErrorMessage,1,1);	

                        $("#pay_button").removeAttr('onclick');
                        $("#pay_button").attr('style','opacity:0.5');
                        $("#pay_button").attr('class','button2');
                    }
                }
            }
            else
            {
                var szErrorMessage = "No insurance service available for this trade";
                displayFormFieldIsRequired('addInsuranceForm', 'fValueOfGoods', szErrorMessage,1,1);	

                $("#pay_button").removeAttr('onclick');
                $("#pay_button").attr('style','opacity:0.5');
                $("#pay_button").attr('class','button2');
            } 
        }  
        
        if(iDisablePayButton==1)
        { 
            /*
            * console.log("Disabled details");
            $("#pay_button").removeAttr('onclick');
            $("#pay_button").attr('style','opacity:0.5');
            $("#pay_button").attr('class','button2');
            bCalculateInsurance = 1;
            */ 
        }
    }

    function getNearestRecord(fMaxBookingAmountToBeInsuranced)
    {   
        var length = insuranceSellRateAry.length ; 
        var ret_ary = new Array();
        for (var i = 0; i < length; i++) 
        {   
            if(fMaxBookingAmountToBeInsuranced <= insuranceSellRateAry[i]['fInsurancePriceUSD'])
            {     
                //console.log("found record ");
                //console.log(insuranceSellRateAry[i]);
                ret_ary = insuranceSellRateAry[i]  ;
                break;
            } 
        }
        return ret_ary; 
    }
Custom.init(); 
</script> 
<form name="addInsuranceForm" id="addInsuranceForm" method="post" action="javascript:void(0);">  
    <?php if($iIncludeWithZero){ ?> 
    <div class="oh quote-insurance">
        <p class="<?php echo $szClassName_1; ?>">&nbsp;</p>
        <div class="<?php echo $szClassName_2; ?>"> 
            <div class="quote-cargo-value-container clearfix" id="insurance-form-container">
                <span><?php echo t($t_base.'fields/purchase_value_of_goods');?></span> 
                <span class="field"><input type="text" onkeypress="return format_decimat(this,event)" onkeyup="calculate_insurance_price(this.form.id,this.id);" onblur="validate_insurance_box(this.form.id,this.id,'1','1');" name="bookingInsuranceAry[fValueOfGoods]" id="fValueOfGoods" value="<?php echo $fValueOfGoods; ?>"></span>
                <span class="madatory-insurance insurance-currency-container">
                    <select onfocus="check_form_field_not_required(this.form.id,this.id);" class="styled" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="bookingInsuranceAry[idInsuranceCurrency]" id="idInsuranceCurrency" onchange="validate_insurance_box(this.form.id,'fValueOfGoods');">
                        <?php
                            if(!empty($currencyAry))
                            {
                                foreach($currencyAry as $currencyArys)
                                {
                                    ?> 
                                        <option value="<?=$currencyArys['id']?>" <?php echo (($idInsuranceCurrency==$currencyArys['id'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                    <?php 
                                }
                            }
                         ?>
                    </select>
                </span>
                <?php 
                    if(!empty($szSpecialKeyAry) && in_array('fValueOfGoods_1',$szSpecialKeyAry))
                    {
                        $errorMesaageAry = array();
                        $errorMesaageAry['fValueOfGoods'] = $kBooking->arErrorMessages['fValueOfGoods_1'];
                        echo display_form_validation_error_message('addInsuranceForm',$errorMesaageAry,false,1);
                    } 
                ?> 
            </div>  
            <input type="hidden" name="bookingInsuranceAry[iInsurance]" id="iInsurance_yes" value="1">
            <input type="hidden" name="bookingInsuranceAry[iIncludeInsuranceWithZeroCargo]" id="iIncludeInsuranceWithZeroCargo" value="1">
        </div>
    </div>
    <?php } else { ?>
        <div <?php if($iBookingQuotes){ ?> class="oh quote-insurance" <?php } else {?> class="oh <?php echo $szContainerClass; ?>" <?php }?>>
        <p class="<?php echo $szClassName_1; ?>">&nbsp;</p>
            <p class="<?php echo $szClassName_2; ?> insurance-checkbox">
                <span class="checkbox-container">
                    <input type="radio" onclick="display_insurance_fields(this.id);<?php echo $onClickYes; ?>" name="bookingInsuranceAry[iInsurance]" <?php echo (($iInsurance==1)?'checked':''); ?> id="iInsurance_yes" value="1"><span id="option_container_span"><?php echo $szOptionYesText."".$szInsuranceCostStr;?></span>
                </span>
                <span class="clearfix" id="insurance-form-container" <?php if($iInsurance!=1){ ?>style="display:none;"<?php } ?>>
                    <span><?php echo t($t_base.'fields/purchase_value_of_goods');?></span> 
                    <span class="field"><input type="text" onkeypress="return format_decimat(this,event)" onkeyup="calculate_insurance_price(this.form.id,this.id);" onblur="validate_insurance_box(this.form.id,this.id,'1','1');" name="bookingInsuranceAry[fValueOfGoods]" id="fValueOfGoods" value="<?php echo $fValueOfGoods; ?>"></span>
                    <span class="insurance-currency-container">
                        <select onfocus="check_form_field_not_required(this.form.id,this.id);" class="styled" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="bookingInsuranceAry[idInsuranceCurrency]" id="idInsuranceCurrency" onchange="validate_insurance_box(this.form.id,'fValueOfGoods');">
                            <?php
                                if(!empty($currencyAry))
                                {
                                    foreach($currencyAry as $currencyArys)
                                    {
                                        ?> 
                                        <option value="<?=$currencyArys['id']?>" <?php echo (($idInsuranceCurrency==$currencyArys['id'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                        <?php 
                                    }
                                }
                             ?>
                        </select>
                    </span>
                    <?php 
                        if(!empty($szSpecialKeyAry) && in_array('fValueOfGoods_1',$szSpecialKeyAry))
                        {
                            $errorMesaageAry = array();
                            $errorMesaageAry['fValueOfGoods'] = $kBooking->arErrorMessages['fValueOfGoods_1'];
                            echo display_form_validation_error_message('addInsuranceForm',$errorMesaageAry,false,1);
                        } 
                    ?> 
                </span>   
                <span class="checkbox-container">
                    <input type="radio" onclick="display_insurance_fields(this.id);addInsuranceDetails('REMOVE_INSURANCE_DETAILS');" name="bookingInsuranceAry[iInsurance]" <?php echo (($iInsurance==2)?'checked':''); ?> id="iInsurance_no" value="2"><span><?=t($t_base.'fields/no_i_dont_need_this');?></span> 
                </span>
            </p>
        </div>
    <?php }?> 
    <input type="hidden" name="booking_key" value="<?php echo $booking_key; ?>">  
    <input type="hidden" name="bookingInsuranceAry[iBookingQuoteFlag]" id="iBookingQuoteFlag" value="<?php echo $iBookingQuotes; ?>">
</form> 
	<?php
}
function display_booking_insurance_form_backup($kBooking,$postSearchAry)
{ 
	$t_base = "BookingConfirmation/";	
	$szClassName_1 = "fl-40";
	$szClassName_2 = "fl-33";
	if(!empty($kBooking->arErrorMessages))
	{
		$formId = 'insurance_form_type_'.$iType;
		$szValidationErrorKey = '';
		foreach($kBooking->arErrorMessages as $errorKey=>$errorValue)
		{
			if(empty($szValidationErrorKey))
			{
				$szValidationErrorKey = $errorKey;
			}
			$szSpecialKeyAry[] = $errorKey ;
			if($errorKey=='fValueOfGoods_1')
			{
				$errorKey  = 'fValueOfGoods';
			}
			?>
			<script type="text/javascript">
				$("#"+'<?php echo $errorKey; ?>').addClass('red_border');
			</script>
			<?php
		}
	}
	
	if(isset($_POST['booking_key']))
	{
		$booking_key = $_POST['booking_key'];
	}
	else
	{
		$booking_key = $postSearchAry['szBookingRandomNum'] ;
	}
	
	if(isset($_POST['bookingInsuranceAry']['iInsurance']))
	{
		$iInsurance = (int)$_POST['bookingInsuranceAry']['iInsurance'] ;
	}
	else
	{
		$iInsurance = $postSearchAry['iInsuranceIncluded'];
	} 
	
	if(isset($_POST['bookingInsuranceAry']['fValueOfGoods']))
	{
		$fValueOfGoods = $_POST['bookingInsuranceAry']['fValueOfGoods'] ;
	}
	else
	{
		if($postSearchAry['fValueOfGoods']>0){
			$fValueOfGoods = $postSearchAry['fValueOfGoods'];
		}
	}
	
	if(isset($_POST['bookingInsuranceAry']['idInsuranceCurrency']))
	{
		$idInsuranceCurrency = $_POST['bookingInsuranceAry']['idInsuranceCurrency'] ;
	}
	else if($postSearchAry['idGoodsInsuranceCurrency']>0)
	{
		$idInsuranceCurrency = $postSearchAry['idGoodsInsuranceCurrency'];
	}
	else
	{
		$idInsuranceCurrency = $postSearchAry['idCurrency'];
	}
	
	$kConfig=new cConfig();
	$currencyAry = array();
	$currencyAry = $kConfig->getBookingCurrency(false,true);
	
	if($postSearchAry['fValueOfGoods']>0)
	{
		$szInsuranceCostStr = " (".$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']).") ";
	} 
	if($postSearchAry['fValueOfGoods']>0)
	{
		$onClickYes = "addInsuranceDetails();";
	}
?>
	<script type="text/javascript">
		Custom.init();
	</script>
	
	<div class="booking-insurance-container"> 
		<div class="fixed-body-container clearfix">
			
			<p><strong><?php echo t($t_base.'fields/insurance_main_heading');?>?</strong></p>
			<form name="addInsuranceForm" id="addInsuranceForm" method="post" action="javascript:void(0);">
				<div class="oh">
					<div class="insurance-heading-image"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/CONFIRMATION_Insurance.png"?>"></div>
					<p class="<?php echo $szClassName_1; ?>"></p> 
					<p class="<?php echo $szClassName_2; ?> payment-options-checkbox">
						<span class="checkbox-container">
							<input type="radio" onclick="display_insurance_fields(this.id);<?php echo $onClickYes; ?>" name="bookingInsuranceAry[iInsurance]" <?php echo (($iInsurance==1)?'checked':''); ?> id="iInsurance_yes" value="1"><?php echo t($t_base.'fields/yes_please_add_insurance').$szInsuranceCostStr;?> 
						</span><br>
						<span class="checkbox-container">
							<input type="radio" onclick="display_insurance_fields(this.id);addInsuranceDetails('REMOVE_INSURANCE_DETAILS');" name="bookingInsuranceAry[iInsurance]" <?php echo (($iInsurance==2)?'checked':''); ?> id="iInsurance_no" value="2"><?=t($t_base.'fields/no_i_dont_need_this');?> 
						</span>
					</p>
				</div>  
				<div class="oh" id="insurance-form-container" <?php if($iInsurance!=1){ ?>style="display:none;"<?php } ?>>
					<p class="<?php echo $szClassName_1; ?>"><?php echo t($t_base.'fields/purchase_value_of_goods');?></p> 
					<div class="<?php echo $szClassName_2; ?>"> 
						<input type="text" onkeypress="return format_decimat(this,event)" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);addInsuranceDetails();" name="bookingInsuranceAry[fValueOfGoods]" id="fValueOfGoods" value="<?php echo $fValueOfGoods; ?>">
						<div class="insurance-currency-container">
						<select onfocus="check_form_field_not_required(this.form.id,this.id);" class="styled" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="bookingInsuranceAry[idInsuranceCurrency]" id="idInsuranceCurrency" onchange="addInsuranceDetails();">
							<?php
							 	if(!empty($currencyAry))
							 	{
							 		foreach($currencyAry as $currencyArys)
							 		{
							 			?>
							 				<option value="<?=$currencyArys['id']?>" <?php echo (($idInsuranceCurrency==$currencyArys['id'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
							 			<?
							 		}
							 	}
							 ?>
						</select>
						</div>
						<a href="javascript:void(0);" onclick="addInsuranceDetails();"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/CONFIRMATION_Update.png"?>"></a>
						<?php
							if(!empty($szSpecialKeyAry) && in_array('fValueOfGoods_1',$szSpecialKeyAry))
							{
								$errorMesaageAry = array();
								$errorMesaageAry['fValueOfGoods'] = $kBooking->arErrorMessages['fValueOfGoods_1'];
								echo display_form_validation_error_message('addInsuranceForm',$errorMesaageAry,false,1);
							}
						?>
					</div>
				</div> 
				<input type="hidden" name="booking_key" value="<?php echo $booking_key; ?>">
			</form>
		</div>
	</div>
	<div class="price-guaranty-container">
		<div class="fixed-body-container clearfix">
			<div class="insurance-heading-image"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/CONFIRMATION_Award.png"; ?>"></div>
			<div class="price-guarantee-div">
				<p><strong><?php echo t($t_base.'fields/price_guarantee');?></strong></p>
				<div class="oh">
					<p><?php echo t($t_base.'fields/find_the_service');?></p>
				</div>
				<div class="oh" id="price_gurantee_details_text" style="display:none;">
					<p><?php echo t($t_base.'fields/price_guarantee_details_text');?></p>
				</div>
				<div class="btn-container">			  
					<span><a href="javascript:void(0);" id="price_gurantee_more_span" onclick="toggle_price_gurantee('SHOW')"><?php echo t($t_base.'fields/more'); ?></a></span>
					<span><a href="javascript:void(0);" id="price_gurantee_less_span" onclick="toggle_price_gurantee('HIDE')" style="display:none;"><?php echo t($t_base.'fields/less'); ?></a></span>
				</div>
			</div>
		</div>
	</div>
	<?php
}
function display_weight_volume_exceed_popup($postSearchAry)
{
    $kWHSSearch = new cWHSSearch();
    $maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
    $maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
    $maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');

    $maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');

    $fKgFactor = $kWHSSearch->getWeightFactor(2);
    $maxCargoWeight_pounds = round((float)$maxCargoWeight * $fKgFactor);
    $maxCargoWeight_pounds = (int)(($maxCargoWeight_pounds)/1000) * 1000;
    $t_base = "LandingPage/";
	
?> 
<script type="text/javascript">
	$().ready(function(){
            Custom.init();
	});
</script>

<div id="popup-bg" class="white-bg"></div>
<div id="popup-container"> 
<form id="cargo_exceed_form" method="post" name="cargo_exceed_form" action="">
	<div class="standard-popup-rk popup" style="margin-top:-100px;">	
		<p align="right" class="close-icon" ><a heref="javascript:void(0);" onclick="showHide('ajaxLogin')"><img src="<?=__BASE_STORE_IMAGE_URL__.'/close.png'?>" alt="close"></a></p>
		<h5><?=t($t_base.'title/rk1_popup_header')?></h5>
		<p><?=t($t_base.'title/rk1_popup_detail_text1');?>: </p>
		<p align="center">
			<?php
				echo $maxCargoLength." ".t($t_base.'title/cm')." x ".$maxCargoWidth." ".t($t_base.'title/cm')." X ".$maxCargoHeight." cm<br /> (".t($t_base.'title/length')." x ".t($t_base.'title/width')." x ".t($t_base.'title/height').")";						
			?>
		</p>
		<p align="center">
			<?php
			echo number_format((float)$maxCargoWeight)." ".t($t_base.'title/kg')." ~ ".number_format((float)$maxCargoWeight_pounds)." ".t($t_base.'title/pounds')." "; 
			?>
		</p>
		<p><?=t($t_base.'title/rk1_popup_detail_text2');?></p>
		<p style="margin-top:14px;"><input class="styled" type="checkbox" value="1" onclick = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" name="addExpactedServiceAry[iSendNotification]" id="iSendNotification_popup" > <?=t($t_base.'title/rk1_popup_checkbox_text');?></p>
		<?php
		$notify_me_class = "class='gray-button1'";
		
		if($_SESSION['user_id']<=0)
		{
			?>
			<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>				
			<div class="oh" style="margin-top:10px;">
				<p class="fl-20"><?=t($t_base.'fields/name');?></p>
				<p class="fl-80">
					<input type="text" id="szCustomerName_popup"  onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" name="addExpactedServiceAry[szCustomerName]" value="<?=$postSearchResult['szCustomerName']?>">
					<span id="szCustomerName_popup_error" class="red_text" style="display:none;float:right;"></span>
				</p>
			</div>
			<?php
		}
		else
		{
			$emailfieldtopspace="style='margin-top:10px;'";
			$kUser = new cUser();
			$kUser->getUserDetails($_SESSION['user_id']);
			if(empty($postSearchResult['szEmail']))
			{
				$postSearchResult['szEmail'] = $kUser->szEmail ;
				if(!empty($postSearchResult['szEmail']))
				{
					//$notify_me_class = "class='button1'";
					//$notify_me_onclick = "onclick=\"send_quotation_to_forwarder('".$szBookingRandomNum."')\"";
				}
			}
			?>
			<div id="popupRegError" class="errorBox1-requirement" style="display:none;"></div>					
			<?
		}
		?>
            <div class="oh" <?=$emailfieldtopspace;?>>
                <p class="fl-20"><?=t($t_base.'fields/email');?></p>
                <p class="fl-80">
                    <input type="text" id="szCustomerEmail_popup" name="addExpactedServiceAry[szEmail]" onkeyup = "enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" onblur="enable_cargo_exceed_popup('<?=(int)$_SESSION['user_id']?>','<?=$landing_page_url?>','<?=$szBookingRandomNum?>',false,this.id);" value="<?=$postSearchResult['szEmail']?>">
                    <span id="szCustomerEmail_popup_error" class="red_text" style="display:none;float:right;"></span>
                </p>
            </div>
            <div class="new-button-container-topgap">
            <p align="center" class="button-container">	
                    <a href="javascript:void(0);" <?=$notify_me_class?> <?=$notify_me_onclick?> id="notify_me_button" ><span><?=t($t_base.'fields/send_my_requirement');?></span></a>
            </p>
            <p align="center" class="button-container">	
                    <a href="javascript:void(0);" class="orange-button1" id="go_back_change_cargo_button" onclick="showHide('ajaxLogin');" ><span><?=t($t_base.'fields/go_back_and_change_cargo');?></span></a>
            </p>
            </div>
            <input type="hidden" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>">				
	</div>
</form>
</div>
<?php 
}

function display_chat_bubble()
{
    $t_base = "SelectService/"; 
    ?>
    <div>
        <p class="close-icon" align="right">
            <a onclick="showHide('terms_condition_popup');" href="javascript:void(0);">
                <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
            </a>
        </p>
    </div>
    <?php
}
function display_insurance_terms_condition($szTermsText)
{
    $t_base = "BookingConfirmation/";
    ?>
    <script type="text/javascript">
        $().ready(function() {	
            $(".nano").nanoScroller({scroll: 'top'});
        });
    </script>
    <div id="popup-bg"></div>
    <div id="popup-container">			
      <div class="popup map-popup" style="width:500px;">
       <p class="close-icon" align="right"><a onclick="showHide('insurance_terms_container');" href="javascript:void(0);"><img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png"></a></p> 
        <h4><?=t($t_base.'fields/insurance_terms');?></h4>
        <div class="nano has-scrollbar">
            <div class="content">
                <?php echo $szTermsText; ?>
            </div>
        </div> 
        <br>
        <p align="center"><a href="javascript:void(0);" onclick="showHide('insurance_terms_container');" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>
      </div>
   </div> 
    <?php
}
 
function getRealIP() 
{ 
        $ipaddress = '';
     	if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_CF_CONNECTING_IP'])) 
        {
            $ipaddress =  $_SERVER['HTTP_CF_CONNECTING_IP'];
        } 
        else if (isset($_SERVER['HTTP_X_REAL_IP'])) 
        {
            $ipaddress = $_SERVER['HTTP_X_REAL_IP'];
        }
        else if (isset($_SERVER['HTTP_CLIENT_IP']))
        {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
        {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED']))
        {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        }
        else if(isset($_SERVER['REMOTE_ADDR']))
        {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        }        
        else
        {
            $ipaddress = 'UNKNOWN';
        }  
        
        if(__ENVIRONMENT__ == "ANILDEV")
        {
            $ipaddress = '122.160.151.237';
            return $ipaddress;
        }
        else
        {
            return $ipaddress;
        } 
    }
    
    function getCountryCodeByIPAddress($szUserIpAddress=false)
    {     
        if(empty($szUserIpAddress))
        {
            $szUserIpAddress = getRealIP();  
        } 
        $szUserIpAddress = getRealIP();  
        $ipReturnAry = array();
        $kUser = new cUser(); 
        
        //$szUserIpAddress = '131.253.35.23' ; 
        //$szUserIpAddress = '83.89.96.69';
        
        $ipReturnAry = ip_info($szUserIpAddress); 
                    
        $iStep = 1;
        if(empty($ipReturnAry['szCountryCode']))
        {
            $ipReturnAry = $kUser->getDetailsByIP($szUserIpAddress);
            $iStep = 2;
        } 
        
        $ret_ary = array();
        if(!empty($ipReturnAry))
        {
            $ret_ary['szZipCode'] = $ipReturnAry['szPostcode'] ;
            $ret_ary['szCityName'] = $ipReturnAry['szCity'] ;
            $ret_ary['szRegionName'] = $ipReturnAry['szRegion'] ;
            $ret_ary['szCountryCode'] = $ipReturnAry['szCountryCode'] ;
            $ret_ary['szCountryName'] = $ipReturnAry['szCountryName'] ; 
            $ret_ary['iFoudBYAPI'] = $iStep;  
            $ret_ary['szIPAddress'] = $szUserIpAddress;  
        }  
        return $ret_ary ;  
    }
                
    function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) 
    { 
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        $kWhsSearch = new cWHSSearch();  
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $posturl = "http://www.geoplugin.net/json.gp?ip=" . $ip;
            
            $response = $kWhsSearch->curl_get_file_contents($posturl,true);  
            
            $ipdat = @json_decode($response);
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "szCity"     => @$ipdat->geoplugin_city,
                            "szRegion"   => @$ipdat->geoplugin_regionName,
                            "szCountryName"  => @$ipdat->geoplugin_countryName,
                            "szCountryCode"  => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }  
    
    function getCountryCodeByIPAddress_backup()
    {    	 /*
        $szUserIpAddress = getRealIP();  
        $query = 'http://freegeoip.net/json/' . $szUserIpAddress; 
       // $query = 'http://ip-api.com/json/' . $szUserIpAddress; 

        //echo "<br><br> You are searching from IP ".$szUserIpAddress ;

        $omni_keys = 
          array(
            'country_code',
            'country_name',
            'region_code',
            'region_name',
            'city_name',
            'latitude',
            'longitude',
            'metro_code',
            'area_code',
            'time_zone',
            'continent_code',
            'postal_code',
            'isp_name',
            'organization_name',
            'domain',
            'as_number',
            'netspeed',
            'user_type',
            'accuracy_radius',
            'country_confidence',
            'city_confidence',
            'region_confidence',
            'postal_confidence',
            'error'
        );

        $curl = curl_init();
        curl_setopt_array(
                $curl,
                array(
                    CURLOPT_URL => $query,
                    CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],
                    CURLOPT_RETURNTRANSFER => true
                )
        );

        $resp = curl_exec($curl);  

        if(curl_errno($curl)) 
        { 
                $strdata=array();
                $strdata[0]=" \n\n MM API throws exception: \n\n ".curl_errno($curl)."\n\n"; 
                file_log(true, $strdata, $filename); 
        }

        $ipReturnAry = array();
        $ipReturnAry = (array)json_decode($resp);  
        $ret_ary = array();
        if(!empty($ipReturnAry))
        {
            $ret_ary['szZipCode'] = $ipReturnAry['zipcode'] ;
            $ret_ary['szCityName'] = $ipReturnAry['city'] ;
            $ret_ary['szRegionName'] = $ipReturnAry['region_name'] ;
            $ret_ary['szCountryCode'] = $ipReturnAry['country_code'] ;
            $ret_ary['szCountryName'] = $ipReturnAry['country_name'] ; 
        }*/
        return $ipReturnAry ;  
    }
    
function crawlerDetect($USER_AGENT)
{
    $crawlers = array(
    array('Google', 'Google'),
    array('msnbot', 'MSN'),
    array('Rambler', 'Rambler'),
    array('Yahoo', 'Yahoo'),
    array('AbachoBOT', 'AbachoBOT'),
    array('accoona', 'Accoona'),
    array('AcoiRobot', 'AcoiRobot'),
    array('ASPSeek', 'ASPSeek'),
    array('CrocCrawler', 'CrocCrawler'),
    array('Dumbot', 'Dumbot'),
    array('FAST-WebCrawler', 'FAST-WebCrawler'),
    array('GeonaBot', 'GeonaBot'),
    array('Gigabot', 'Gigabot'),
    array('Lycos', 'Lycos spider'),
    array('MSRBOT', 'MSRBOT'),
    array('Scooter', 'Altavista robot'),
    array('AltaVista', 'Altavista robot'),
    array('IDBot', 'ID-Search Bot'),
    array('eStyle', 'eStyle Bot'),
    array('Scrubby', 'Scrubby robot')
    ); 

    foreach ($crawlers as $c)
    {
        if (stristr($USER_AGENT, $c[0]))
        {
            return($c[1]);
        }
    }
    return false;
}
function getFooterLanguageUrl($szUri,$iLanguage,$szLinkedPageUrl)
{   
    if(!empty($szUri))
    {
        $specialUrlAry = array('1'=>'explain','2'=>'learn','3'=>'howitworks','4'=>'how-it-works','5'=>'company','6'=>'transportpedia','7'=>'saadan-virker-det','8'=>'virksomheden','9'=>'leksikon','10'=>'information');
        $uriAry = explode("/",$szUri);  
        
        if(__ENVIRONMENT__ == "DEV_LIVE" || __ENVIRONMENT__ == "LIVE")
        {
            $iLanguage_sys = getLanguageId();   
            if($iLanguage_sys==__LANGUAGE_ID_DANISH__)
            {
                $szFirstKey = $uriAry[2] ;
                $szFirstKeyUrl = $uriAry[1] ;
            }
            else
            {
                $szFirstKey = $uriAry[1] ;
                $szFirstKeyUrl = $uriAry[1] ;
            } 
        }
        else
        {
            $szFirstKey = $uriAry[1] ;
            $szFirstKeyUrl = $szFirstKey ;
        }
        
        if($iLanguage==__LANGUAGE_ID_ENGLISH__)
        {	 
            $key = array_search($szFirstKey,$specialUrlAry); 
            if($key>0)
            {  
                if(!empty($szLinkedPageUrl))
                {
                    $redirect_url = __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".$szLinkedPageUrl;
                }
                else
                {
                    $redirect_url =  __MAIN_SITE_HOME_PAGE_SECURE_URL__ ;
                } 
            }
            else
            { 
                $szRequestUri = __MAIN_SITE_HOME_PAGE_SECURE_URL__.$szUri ;
                if($szFirstKeyUrl==__TRANSPORTECA_DANISH_URL__)
                {
                    $redirect_url = str_replace(__MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__,__MAIN_SITE_HOME_PAGE_SECURE_URL__,$szRequestUri); 
                }
                else
                {
                    $redirect_url = str_replace(__MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__,__MAIN_SITE_HOME_PAGE_SECURE_URL__,$szRequestUri); 
                } 
                $redirect_url = $redirect_url ;
            }
        }
        else
        {   
            $key = array_search($szFirstKey,$specialUrlAry);   
            if($key>0)
            { 
                if(!empty($szLinkedPageUrl))
                {
                    $redirect_url =  __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__."/".$szLinkedPageUrl   ;
                }
                else
                {
                    $redirect_url =  __MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__   ;
                }  
            }
            else
            { 
                $szRequestUri = __MAIN_SITE_HOME_PAGE_SECURE_URL__.$szUri ;
                $redirect_url = str_replace(__MAIN_SITE_HOME_PAGE_SECURE_URL__,__MAIN_SITE_HOME_PAGE_SECURE_URL__."/".__TRANSPORTECA_DANISH_URL__,$szRequestUri); 
            }		 
            $redirect_url = $redirect_url ;
        }  
        return $redirect_url;
    }
}

function convertFromCP1252($string)
{
    if(!empty($string))
    {
        //$string = utf8_encode($string);
        //$output = iconv('UTF-8', 'ASCII//TRANSLIT', $string); 
         
        return $string ; 
    }
}

function format_date_time_admin($dtDateTitme)
{
   if(!empty($dtDateTitme))
   {
       $dateTimeAry = explode("/",$dtDateTitme); 
       $month = $dateTimeAry[1];
       $date = $dateTimeAry[0];
       $year = $dateTimeAry[2];
       $ret_date_time = $year.'-'.$month.'-'.$date ;
       return $ret_date_time ;
    }
} 
function display_cargo_details_form($number,$cargoAry,$postSearchAry=array(),$iSearchMiniPage=false)
{  
    if(!empty($_POST['cargodetailAry']))
    {
        $cargoAry = $_POST['cargodetailAry'] ; 
        $llop_counter = $number - 1 ;
        $fLength = $cargoAry['iLength'][$llop_counter];
        $fWidth = $cargoAry['iWidth'][$llop_counter];
        $fHeight = $cargoAry['iHeight'][$llop_counter];
        $fWeight = $cargoAry['iWeight'][$llop_counter];
        $iQuantity = $cargoAry['iQuantity'][$llop_counter];
        $idCargo = $cargoAry['id'][$llop_counter];
        $idCargoMeasure = $cargoAry['idCargoMeasure'][$llop_counter]; 
        $idWeightMeasure = $cargoAry['idWeightMeasure'][$llop_counter]; 
    }
    else
    {
        if($iSearchMiniPage>0 && empty($cargoAry[$number]) && $number==1)
        {
            /*
            * In Case of Searchmin3 and SearchMini4 we fill in cargo dimension fields with default packing dimensions if it is empty
            */    
            $fLength = __DEFAULT_CARGO_PACKET_LENGTH__;
            $fWidth = __DEFAULT_CARGO_PACKET_WIDTH__;
            $fHeight = __DEFAULT_CARGO_PACKET_HEIGHT__;
            $fWeight = __DEFAULT_CARGO_PACKET_WEIGHT__;
            $iQuantity = __DEFAULT_CARGO_PACKET_QUANTITY__;
            $idCargo = $cargoAry[$number]['id']; 
            $idCargoMeasure = 1; //Cm
            $idWeightMeasure = 1; //Kg
        }
        else
        {
            $fLength = $cargoAry[$number]['fLength'];
            $fWidth = $cargoAry[$number]['fWidth'];
            $fHeight = $cargoAry[$number]['fHeight'];
            $fWeight = $cargoAry[$number]['fWeight'];
            $iQuantity = $cargoAry[$number]['iQuantity'];
            $idCargo = $cargoAry[$number]['id']; 
            $idCargoMeasure = $cargoAry[$number]['idCargoMeasure']; 
            $idWeightMeasure = $cargoAry[$number]['idWeightMeasure']; 
            if($postSearchAry['iPalletType']>0 && $fHeight==0)
            {
                $fHeight = '';
            }
        }  
    } 
    
    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $idBooking = $postSearchAry['id'];

    // geting all weight measure 
    $weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');

    // geting all cargo measure 
    $cargoMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_CARGO_MEASURE__');
    $t_base = "LandingPage/"; 
    $szDottedDiv = '';
    if($iSearchMiniPage>0)
    {
        $szStandardClass="search-mini-cargo-fields"; 
        $idSuffix = "_V_".$iSearchMiniPage ;
        
        if($iSearchMiniPage==3)
        {
            $bShowHeading = false;
            if($number==1)
            {
                $bShowHeading = true;
                $szMinusButtonId = 'id="cargo-line-remove-1-v3" ';
            }
        } 
        else
        {
            $bShowHeading = true;
            if($number>1)
            {
                $szDottedDiv = '<div class="dotted-line">&nbsp;</div>';
            }
        } 
        $szClassHide = '';
        if(!$bShowHeading)
        {
            //$szClassHide = ' hide-cargo-heading';
            $szClassHide_container = " v3-cargo-container";
        }  
?>
        <script type="text/javascript">
            Custom.init();
        </script>
        <?php echo $szDottedDiv; ?>
        <div class="cargo1 clearfix<?php echo $szClassHide_container; ?>">
            <?php if($iSearchMiniPage==3){ ?>
                <p class="field-name"> 
                    <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=CartonSmallBlue.png&temp=2&w=40&h=42'?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />  
                </p>
            <?php } ?>
            <div class="cargo-volume-measures clearfix"> 
                <div class="<?php echo $szStandardClass; ?>">
                    <p class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/length')); ?></p> 
                    <p class="input-fields"> 
                        <input type="hidden" name="cargodetailAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
                        <input type="text" class="first-cargo-fields<?php echo $idSuffix; ?>" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/length')); ?>" name="cargodetailAry[iLength][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fLength,1); }?>" id= "iLength<?php echo $number."".$idSuffix; ?>" />
                    </p> 
                </div>
                <div class="<?php echo $szStandardClass; ?>">
                    <p class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/height')); ?></p>
                    <p class="input-fields"> 
                        <input type="text" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/height')); ?>" name="cargodetailAry[iHeight][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeight<?php echo $number."".$idSuffix; ?>" />
                    </p> 
                </div>
                <div class="<?php echo $szStandardClass; ?>">
                    <p class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/width')); ?></p>
                    <p class="input-fields"> 
                        <input type="text" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/width')); ?>" name="cargodetailAry[iWidth][<?=$number?>]" value="<?php if((float)$fWidth>0.00){ echo round_up($fWidth,1);}?>" id= "iWidth<?php echo $number."".$idSuffix; ?>" />
                    </p> 
                </div> 
                <div class="<?php echo $szStandardClass; ?>">
                    <p class="input-title<?php echo $szClassHide;?>">&nbsp;</p>
                    <p class="input-fields"><span>&nbsp;</span>
                        <select name="cargodetailAry[idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?php echo $number."".$idSuffix; ?>"  <?=$disabled_str?> class="styled">
                            <?php
                                if(!empty($cargoMeasureAry))
                                {
                                    foreach($cargoMeasureAry as $cargoMeasureArys)
                                    {
                                        ?>
                                        <option value="<?=$cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </p> 
                </div>  
            </div>
            <div class="cargo-weight-measures clearfix">
                <?php if($iSearchMiniPage==4){ $szExclassWeight = " weight-v4"; }?>
                    <div class="<?php echo $szStandardClass; ?>">
                        <p class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/count')); ?></p>
                        <p class="input-fields">  
                            <input type="text" onkeypress="return isIntegerKey(event);" <?=$disabled_str?> pattern="[0-9]*" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/count')); ?>" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?php echo $number."".$idSuffix; ?>" />
                        </p>
                    </div> 
                <div class="<?php echo $szStandardClass."".$szExclassWeight; ?>">
                    <p class="input-title<?php echo $szClassHide;?>"><?php echo ucfirst(t($t_base.'fields/weight'))." (".t($t_base.'fields/per_packet').")"; ?></p> 
                    <div class="clearfix">
                        <p class="input-fields">   
                            <input type="text" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/weight')); ?>" name="cargodetailAry[iWeight][<?=$number?>]" value="<?php if((float)$fWeight>0.00){ echo round_up($fWeight,1);}?>" id= "iWeight<?php echo $number."".$idSuffix; ?>" />
                        </p> 
                        <p class="input-fields">    
                            <select size="1" id= "idWeightMeasure<?php echo $number."".$idSuffix; ?>" <?=$disabled_str?> name="cargodetailAry[idWeightMeasure][<?=$number?>]" class="styled">
                                <?php
                                if(!empty($weightMeasureAry))
                                {
                                    foreach($weightMeasureAry as $weightMeasureArys)
                                    {
                                        ?>
                                        <option value="<?=$weightMeasureArys['id']?>" <?php if($idWeightMeasure==$weightMeasureArys['id']){?> selected <?php }?>><?=$weightMeasureArys['szDescription']?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </p> 
                    </div>
                </div> 
                <?php if($iSearchMiniPage==3){ ?>    
                    <div class="cargo-button-container">
                        <div class="v3-cargo-buttons-container clearfix">
                            <a href="javascript:void(0);" onclick="add_more_cargo_details('REQUIREMENT_PAGE','<?php echo $postSearchAry['szBookingRandomNum']?>','<?php echo $iSearchMiniPage; ?>')" class="cargo-add-button">+</a>  
                            <a href="javascript:void(0);" class="cargo-remove-button" <?php echo $szMinusButtonId; ?> onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo ($number-1)."_".$iSearchMiniPage ?>','<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>');">-</a>
                        </div>
                    </div>
                <?php }else {?>
                    <div class="cargo-button-container">
                        <a href="javascript:void(0);" onclick="add_more_cargo_details('REQUIREMENT_PAGE','<?php echo $postSearchAry['szBookingRandomNum']?>','<?php echo $iSearchMiniPage; ?>')" class="cargo-add-button">+</a>  
                        <a href="javascript:void(0);" class="cargo-remove-button" <?php echo $szMinusButtonId; ?> onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo ($number-1)."_".$iSearchMiniPage ?>','<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>');">-</a>
                    </div>
                <?php }?>
                
            </div> 
        </div>
        <?php
    }
    else
    {
        ?>
        <script type="text/javascript">
            Custom.init();
        </script>
        <div class="cargo1 clearfix">
            <p class="field-name">
                <?php if($postSearchAry['idCourierPackingType']==1){ ?>
                    <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=CartonSmallBlue.png&temp=2&w=40&h=42'?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
                <?php } else if($postSearchAry['idCourierPackingType']==2) {?>
                    <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Pallet.png&temp=2&w=40&h=42'?>" alt="<?=$searchResults['szDisplayName']?>" border="0" />
                <?php } else {?>
                    <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=CartonPalletSmall.png&temp=2&w=40&h=42'?>" alt="<?=$searchResults['szDisplayName']?>" border="0" /> 
                <?php } ?>
            </p>
            <div class="cargo-volume-measures">
                <p class="input-title"> 
                    <input type="hidden" name="cargodetailAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" />
                    <input type="text" class="first-cargo-fields" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/length')); ?>" name="cargodetailAry[iLength][<?=$number?>]" value="<?php if((float)$fLength>0.00){ echo round_up($fLength,1); }?>" id= "iLength<?=$number?>" />
                </p> 
                <p class="input-title"> 
                    <input type="text" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/width')); ?>" name="cargodetailAry[iWidth][<?=$number?>]" value="<?php if((float)$fWidth>0.00){ echo round_up($fWidth,1);}?>" id= "iWidth<?=$number?>" />
                </p> 
                <p class="input-title"> 
                    <input type="text" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/height')); ?>" name="cargodetailAry[iHeight][<?=$number?>]" value="<?php if((float)$fHeight>0.00){ echo round_up($fHeight,1);}?>" id= "iHeight<?=$number?>" />
                </p>
                <p class="input-title"><span>&nbsp;</span>
                    <select name="cargodetailAry[idCargoMeasure][<?=$number?>]" id= "idCargoMeasure<?=$number?>"  <?=$disabled_str?> class="styled">
                        <?php
                            if(!empty($cargoMeasureAry))
                            {
                                foreach($cargoMeasureAry as $cargoMeasureArys)
                                {
                                    ?>
                                    <option value="<?=$cargoMeasureArys['id']?>" <?php if($idCargoMeasure==$cargoMeasureArys['id']){?> selected <?php }?>><?=$cargoMeasureArys['szDescription']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </p> 
            </div>
            <div class="cargo-weight-measures">
                <p class="input-title">
                    <input type="text" <?=$disabled_str?> onkeypress="return isNumberKey(event);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/weight')); ?>" name="cargodetailAry[iWeight][<?=$number?>]" value="<?php if((float)$fWeight>0.00){ echo round_up($fWeight,1);}?>" id= "iWeight<?=$number?>" />
                </p> 
                <p class="input-title">
                    <select size="1" id= "idWeightMeasure<?=$number?>" <?=$disabled_str?> name="cargodetailAry[idWeightMeasure][<?=$number?>]" class="styled">
                        <?php
                        if(!empty($weightMeasureAry))
                        {
                            foreach($weightMeasureAry as $weightMeasureArys)
                            {
                                ?>
                                <option value="<?=$weightMeasureArys['id']?>" <?php if($idWeightMeasure==$weightMeasureArys['id']){?> selected <?php }?>><?=$weightMeasureArys['szDescription']?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </p>
                <p class="input-title"> 
                    <input type="text" onkeypress="return isIntegerKey(event);" <?=$disabled_str?> pattern="[0-9]*" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="<?php echo ucfirst(t($t_base.'fields/count')); ?>" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id= "iQuantity<?=$number?>" />
                </p>
                <div class="cargo-button-container">
                    <a href="javascript:void(0);" onclick="add_more_cargo_details('REQUIREMENT_PAGE','<?php echo $postSearchAry['szBookingRandomNum']?>','<?php echo $iSearchMiniPage; ?>')" class="cargo-add-button">+</a>  
                    <a href="javascript:void(0);" class="cargo-remove-button" onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo $number-1 ?>','<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>');">-</a>
                </div>
            </div> 
        </div>
        <?php
    } 
}

function update_cargo_dimension($kBooking,$postSearchAry,$szCargoLogString=false,$iSearchMiniPage=false)
{
    $cargoAry = array(); 
    $idBooking = $postSearchAry['id'];
    
    $kBooking_new = new cBooking();
    $cargoAry = $kBooking_new->getCargoDeailsByBookingId($idBooking);
    $t_base = "LandingPage/";  
    
    $iLanguage = getLanguageId();  
    $szDanishClass='';
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
        $szDanishClass = " CargoHeadingDa";
    }
    $kCourierService = new cCourierServices();
    $courierPackingAry = array();
    $courierPackingAry = $kCourierService->selectProviderPackingList($postSearchAry['idCourierPackingType'],$iLanguage);
    $szPackingType = $courierPackingAry[0]['szPacking'];
    if(!empty($szPackingType))
    {
        $szPackingType = strtolower($szPackingType);
    }
                
    $szCargoDimentionHeader = '';
    $szDivKey = '';
    if(!$iSearchMiniPage)
    {
        echo '<form action="" id="cargo_details_form" name="cargo_details_form" method="post">';
        if(__ENVIRONMENT__ == "LIVE")
        {
            echo '<a href="javascript:void(0)" style="color:#eff0f2;" onclick=showHideCourierCal("courier_service_calculation_cargo_detail1")>.</a><div class="clearfix" id="courier_service_calculation_cargo_detail1" style="font-size: 16px;text-align:left;display:none;"><div id="popup-bg"></div><div id="popup-container"><div class="popup abandon-popup" style="width:900px;"><p class="close-icon" align="right"><a onclick=showHideCourierCal("courier_service_calculation_cargo_detail1") href="javascript:void(0);"><img alt="close" src='.__BASE_STORE_IMAGE_URL__.'/close1.png>
            </a></p><div style="height:400px;overflow-y:auto;overflow-x:hidden" id="cargo_calculation_details_container_cargo">'.$szCargoLogString."</div></div></div></div>"; 
        }
        else
        { 
            echo '<a href="javascript:void(0)" class="button-green1" onclick=showHideCourierCal("courier_service_calculation_cargo_detail1")>View Courier Calculation After Cargo Updated</a><div class="clearfix" id="courier_service_calculation_cargo_detail1" style="font-size: 16px;text-align:left;display:none;"><div id="popup-bg"></div><div id="popup-container"><div class="popup abandon-popup" style="width:900px;"><p class="close-icon" align="right"><a onclick=showHideCourierCal("courier_service_calculation_cargo_detail1") href="javascript:void(0);"><img alt="close" src='.__BASE_STORE_IMAGE_URL__.'/close1.png>
            </a></p><div style="height:400px;overflow-y:auto;overflow-x:hidden" id="cargo_calculation_details_container_cargo">'.$szCargoLogString."</div></div></div></div>";
        }
        $szCargoDimentionHeader = '<p class="cargo-heading'. $szDanishClass.'">'.t($t_base.'title/cargo_details_title_1')." ".$szPackingType." ".t($t_base.'title/cargo_details_title_2').'</p>'; 
        $szChilDivClass = 'class="request-quote-fields service-page-cargo-container"';
        
    } 
    else
    {
        $szDivKey = "_".$iSearchMiniPage;
        $szChilDivClass = 'class="request-quote-fields"';
    } 
    ?> 
    <div id="complete-box" class="active transporteca-box">   
        <?php   echo $szCargoDimentionHeader; ?> 
        <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
            <?php 
                $counter = 1; 
                if(count($_POST['cargodetailAry']['iLength'])>0 || !empty($cargoAry))
                { 
                    if(count($_POST['cargodetailAry']['iLength'])>0)
                    {
                       $loop_counter = count($_POST['cargodetailAry']['iLength']);  
                       $postDataFlag = true;
                    }
                    else
                    {
                        $loop_counter = count($cargoAry);  
                    } 
                    for($j=0;$j<$loop_counter;$j++)
                    {
                        $counter = $j+1;
                        if($postDataFlag)
                        { 
                            $_POST['cargodetailAry']['iLength'][$counter-1] = format_numeric_vals($_POST['cargodetailAry']['iLength'][$counter-1]); 
                            $_POST['cargodetailAry']['iWidth'][$counter-1] = format_numeric_vals($_POST['cargodetailAry']['iWidth'][$counter-1]); 
                            $_POST['cargodetailAry']['iHeight'][$counter-1] = format_numeric_vals($_POST['cargodetailAry']['iHeight'][$counter-1]); 
                            $_POST['cargodetailAry']['iWeight'][$counter-1] = format_numeric_vals($_POST['cargodetailAry']['iWeight'][$counter-1]); 
                        }
                        ?>
                        <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" <?php echo $szChilDivClass; ?>> 
                            <?php
                                echo display_cargo_details_form($counter,$cargoAry,$postSearchAry,$iSearchMiniPage);
                            ?>
                        </div>
                        <?php
                    }
                }
                else
                {   
                    ?>
                    <div id="add_booking_quote_container_0<?php echo $szDivKey;?>" <?php echo $szChilDivClass; ?> > 
                        <?php
                           echo display_cargo_details_form($counter,$cargoAry,$postSearchAry,$iSearchMiniPage);
                        ?>
                    </div>
                    <?php 
                } 
            ?>
            <div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>" style="display:none;"></div>
        </div>   
    </div> 
    <?php if(!$iSearchMiniPage){ $szFormCloseTag = '</form>'; ?>
        <div class="cargo-success-text">
            <?php
                if(!empty($kBooking->arErrorMessages['szSpecialError']))
                {
                    echo $kBooking->arErrorMessages['szSpecialError'] ;
                }
            ?>
        </div>  
        <div class="btn-container" style="text-align:center;">
            <a href="javascript:void(0);" class="button-green1" onclick="submit_cargo_details_form('<?php echo $_REQUEST['lang']?>')" ><span><?=t($t_base.'fields/countinue');?></span></a>  
        </div>
        <br> 
    <?php } ?>
    <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
    <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" /> 
    <input type="hidden" name="idCargoMeasure_hidden" id="idCargoMeasure_hidden" value="1">
    <input type="hidden" name="idWeightMeasure_hidden" id="idWeightMeasure_hidden" value="1">
    <input type="hidden" name="iDonotCalculateAgain" id="iDonotCalculateAgain" value="<?php echo $kBooking->iDonotCalculateAgain; ?>">
    
    <input type="hidden" name="iDangerCargo_hidden" id="iDangerCargo_hidden" value="No">
    <input type="hidden" name="mode" value="CARGO_DETAILS">
    <input type="hidden" id="booking_key" name="booking_key" value="<?=$postSearchAry['szBookingRandomNum']?>"> 
    <?php 
    echo $szFormCloseTag;
    if(!empty($kBooking->arErrorMessages))
    {  
        $formId = 'cargo_details_form'; 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
} 
function display_cookie_text_details($iFromLandingPage)
{
    $t_base = "home/";
    if($iFromLandingPage==1)
    {
        $szClassName = "cookie-blue-box";
    }
    else
    {
        $szClassName = "cookie-blue-box-container";
    }
    $iLanguage = getLanguageId();  
    $szBrText='';
    if($iLanguage==__LANGUAGE_ID_ENGLISH__)
    {
        $szBrText = "<br>";
    }
    $isMobileValue= checkMobileDevice();
    ?>
    <div class="<?php echo $szClassName; ?>" id="cookie-blue-box-container" <?php if($isMobileValue!=1){?> style="display:block;" <?php }else{?> style="display:none;" <?php }?>>
        <?php echo t($t_base.'header/cookie_policy')."".$szBrText; ?> <a href="<?php echo __BASE_URL__?>/privacyNotice/"><?php echo t($t_base.'header/privacy_notice'); ?></a> | <a href="<?php echo __BASE_URL__?>/cookies/"><?php echo t($t_base.'header/read_more_cookie'); ?></a> 
    </div>
    <?php
} 
function getAddressDetailsByCordinate($result)
{
    if(!empty($result))
    {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$result['szLatitute'].','.$result['szLongitute'].'&key='.__GOOGLE_MAP_V3_API_KEY__.'&sensor=false';
        //$url = 'http://maps.google.com/maps/geo?q='.$result['szLatitute'].','.$result['szLongitute'].'&output=json&sensor=true_or_false&key=' . __GOOGLE_MAP_V3_API_KEY__;
        //echo $url;
        $kWhsSearch = new cWHSSearch();  
        $geocode = $kWhsSearch->curl_get_file_contents($url);

        $response = json_decode($geocode); 
        $ret_ary['szLatitute'] = $result['szLatitute'];
        $ret_ary['szLongitute'] = $result['szLongitute'];

        $responseAry = array();
        $responseAry = toArray($response);    
        
        if(!empty($responseAry['results'][0]))
        {
            $responseArys = $responseAry['results'][0];
            foreach($responseArys as $responseAryss)
            {
                $iRowCount = count($responseAryss) ;
                if(!empty($responseAryss))
                {
                    foreach($responseAryss as $responseArysss)
                    {  
                        if($responseArysss['types'][0]=='street_number' || $responseArysss['types'][0]=='route')
                        {
                            if(!empty($ret_ary['szStreet']))
                            {
                                $ret_ary['szStreet'] .=", ". $responseArysss['long_name'] ;
                            }
                            else
                            {
                                $ret_ary['szStreet'] = $responseArysss['long_name'] ;
                            }
                        }
                        if($responseArysss['types'][0]=='postal_code')
                        { 
                            $ret_ary['szPostCode'] = $responseArysss['long_name']; 
                        } 
                        if($responseArysss['types'][0]=='postal_town')
                        {
                            $ret_ary['szCity'] .= $responseArysss['long_name'] ;
                        }
                        if($responseArysss['types'][0]=='administrative_area_level_2')
                        {
                            $ret_ary['szCounty'] = $responseArysss['long_name'] ;
                        }
                        if($responseArysss['types'][0]=='country')
                        {
                            $ret_ary['szCountry'] = $responseArysss['long_name'] ;
                            $ret_ary['szCountryCode'] = $responseArysss['short_name'] ;
                        }
                    }
                }
                break; 
            }
        }
        else if(!empty($responseAry['error_message']))
        {
            $ret_ary['szErrorMessage'] = $responseAry['error_message'] ;
        }
        return $ret_ary;
    }
}
 
function split_pdf($filename, $end_directory = false)
{ 
    require_once(__APP_PATH__.'/forwarders/html2pdf/html2pdfBooking.php');
    require_once(__APP_PATH__.'/pdfLib/fpdi/fpdi.php');

    //$filename = "puram.pdf";
    $end_directory = "";  
    $end_directory = $end_directory ? $end_directory : './';
    $new_path = preg_replace_callback('/[\/]+/', '/', $end_directory.'/'.substr($filename, 0, strrpos($filename, '/'))); 
     
    if (!is_dir($new_path))
    {
        // Will make directories under end directory that don't exist
        // Provided that end directory exists and has the right permissions
        mkdir($new_path, 0777, true);
    }
    $new_path = "c://xampp/htdocs/transporteca/fddi/".$filename;
    $pdf = new FPDI();
    $pagecount = $pdf->setSourceFile($new_path); // How many pages?
    echo "pagecount: ".$pagecount ;

    // Split each page into a new PDF
    for ($i = 1; $i <= $pagecount; $i++) 
    {             
        $new_pdf = new FPDI();
        $new_pdf->AddPage();
        $new_pdf->setSourceFile($filename);
        $new_pdf->useTemplate($new_pdf->importPage($i));

            try 
            { 
                $new_filename = $end_directory.str_replace('.pdf', '', $filename).'_'.$i.".pdf";
                 
                $new_pdf->Output($new_filename, "F");
                echo "Page ".$i." split into ".$new_filename."<br />\n";
            } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
    }
}
function build_stripe_payment_form($postSearchAry)
{ 
    if(!empty($postSearchAry))
    {
        ?>
        <form action="<?=__MAIN_SITE_HOME_PAGE_URL__?>/zooz/callback.php" method="POST" id="stripePaymentForm">
            <script 
                src="https://checkout.stripe.com/checkout.js" 
                class="stripe-button" data-key="pk_test_2yyVQoUP5SeF79wt1IiWUB8Z" 
                data-amount="<?php echo $postSearchAry['fTotalPayableAmount']; ?>" 
                data-name="Transporteca" 
                data-currency="<?php echo $postSearchAry['szCurrency']; ?>"
                data-description="<?php echo $postSearchAry['szDescription']; ?>" 
                data-image="<?php echo __MAIN_SITE_HOME_PAGE_URL__."/images/transporteca-logo-new.png"; ?>"  
                data-locale="auto">
            </script>
        </form>
        <?php
    }  
}

function display_search_form_Newest()
{  
    $kBooking = new cBooking();
    $kConfig = new cConfig();

    $t_base = "BookingVagoFurniture/";
    
    $iLanguage = __LANGUAGE_ID_ENGLISH__;
    
    $iSearchMiniPage=7;
    
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
    
?> 
    <div class="hsbody-2-right">
        <div id="" class="get-price">
            <div class="clearfix">
                <div id="booking-form-vago-furniture" class="price-form" style="width:100%">
                    <form method="post" onsubmit="return false;" name="transportationOfferForm" id="transportationOfferForm" action=""> 
                        <div id="booking-quotes-form-furniture">
                            <div class="form-line3 clearfix">
                                <div id="szFirstName_container">
                                    <input type="text" value="<?=$_POST['transportationOffer']['szFirstName'];?>" id="szFirstName" name="transportationOffer[szFirstName]"  placeholder="<?=t($t_base.'fields/f_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szFirstName_container');validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                </div>
                                <div id="szLastName_container">
                                    <input type="text" value="<?=$_POST['transportationOffer']['szLastName'];?>" id="szLastName" name="transportationOffer[szLastName]" placeholder="<?=t($t_base.'fields/l_name');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szLastName_container');validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                </div>
                                <div id="szEmail_container">
                                    <input type="text" value="<?=$_POST['transportationOffer']['szEmail'];?>" id="szEmail" name="transportationOffer[szEmail]" placeholder="<?=t($t_base.'fields/email');?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szEmail_container');validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                </div>
                                <div class="get-quote-phone-container" id="idShipperDialCode_container">
                                    <select size="1" name="transportationOffer[idShipperDialCode]" class="" id="idShipperDialCode"  onchange="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" onblur="validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                        <?php
                                           if(!empty($dialUpCodeAry))
                                           {
                                               $usedDialCode = array();
                                               foreach($dialUpCodeAry as $dialUpCodeArys)
                                               {
                                                    if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                                    {
                                                        $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                                        ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$_POST['transportationOffer']['idShipperDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                                    }
                                               }
                                           }
                                       ?>
                                    </select>
                                </div>
                                <div id="szPhoneNumber_container">
                                    <input type="text" class="get-quote-phone" placeholder="<?=t($t_base.'fields/phone_number');?>" name="transportationOffer[szPhoneNumber]" id="szPhoneNumber" value="<?=$_POST['transportationOffer']['szPhoneNumber'];?>" onfocus="check_form_field_not_required(this.form.id,this.id);"  onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szPhoneNumber_container');validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                </div>
                            </div>
                            <div class="form-line3 clearfix">
                                <div id="szAddress_container">
                                    <input type="text" class="get-quote-phone" placeholder="<?=t($t_base.'fields/address');?>" name="transportationOffer[szAddress]" id="szAddress" value="<?=$_POST['transportationOffer']['szAddress'];?>"  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szAddress_container');validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');" >
                                </div>
                                <div id="szPostCode_container">
                                    <input type="text" class="get-quote-phone" placeholder="<?=t($t_base.'fields/postcode');?>" name="transportationOffer[szPostCode]" id="szPostCode" value="<?=$_POST['transportationOffer']['szPostCode'];?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szPostCode_container');validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                </div>
                                <div id="szBy_container">
                                    <input type="text" class="get-quote-phone" placeholder="By" name="transportationOffer[szBy]" id="szBy" value="<?=$_POST['transportationOffer']['szBy'];?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'szBy_container');validate_pallet_fields(event,'<?php echo $iSearchMiniPage; ?>');">
                                </div>
                                <div class=""> 
                                    <p><button id="" class=""> X </button><?=t($t_base.'heading/heading-button');?></p>
                                </div>
                            </div>
                            <div class="form-line3 clearfix">
                                <div id="complete-box" class="active transporteca-box">   
                                    <h4><?=t($t_base.'heading/heading-4');?></h4> 
                                    <div id="horizontal-scrolling-div-id">
                                        <?php 
                                            $counter = 1; 
                                        ?>
                                        <div id="add_booking_quote_container_<?php echo $counter."_".$iSearchMiniPage;?>" class="request-quote-fields <?php if($iSearchMiniPage==9){?> cargo-detail-search-mini-9 <?php }?>"> 
                                            <?php
                                               echo display_furniture_order_form($counter,$iSearchMiniPage,$iLanguage);
                                            ?>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                            <div class="form-line3 clearfix">
                                <div id="iOfferNotification_container">
                                    <p><input type="checkbox" class="get-quote-phone" name="transportationOffer[iOfferNotification]" id="iOfferNotification" value="1"/><?=t($t_base.'heading/heading-checkbox');?></p>
                                </div>
                            </div>
                            <div style="text-align:center;" class="btn-container">
                                <a onclick="" class="button-green1" href="javascript:void(0);"><span><?=t($t_base.'heading/submit-button');?></span></a>  
                            </div>
                            <input type="hidden" name="hiddenPosition_<?php echo $iSearchMiniPage;?>" id="hiddenPosition_<?php echo $iSearchMiniPage;?>" value="<?php echo $counter;?>">
                        </div>  
                    </form>  
                </div>
            </div>
        </div> 
    </div>
        <?php
    }
    
function display_furniture_order_form($number,$iSearchMiniPage=false,$cargoAry=array(),$idSearchMini=0)
{   
    if(!empty($_POST['cargodetailAry']))
    {
        $cargoAry = $_POST['cargodetailAry'] ; 
        $llop_counter = $number - 1 ;
        $idCargoCategory = $cargoAry['idCargoCategory'][$llop_counter];
        $fWidth = $cargoAry['iWidth'][$llop_counter]; 
        $iQuantity = $cargoAry['iQuantity'][$llop_counter];
        $idCargo = $cargoAry['id'][$llop_counter]; 
    }
    else
    { 
        $idCargoCategory = $cargoAry[$number]['idCargoCategory'];
        $fWidth = $cargoAry[$number]['fWidth']; 
        $iQuantity = $cargoAry[$number]['iQuantity'];
        $idCargo = $cargoAry[$number]['id'];  
    } 
    
    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $idBooking = $postSearchAry['id'];
                    
    $t_base = "LandingPage/";  
    $szStandardClass="search-mini-cargo-fields"; 
    $idSuffix = "_V_".$iSearchMiniPage ; 
    $iLanguage = getLanguageId();
    
    $standardCargoCategoryAry = array();
    $standardCargoAry = array();
    
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__);
    
    $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,false,$idSearchMini); 
    
    if($iSearchMiniPage==9)
    {
        $idCargoCategory=$standardCargoCategoryAry[0]['id'];
    }
    if($idCargoCategory>0)
    { 
        $standardCargoAry = $kConfig->getStandardCargoList($idCargoCategory,$iSelectedLanguage,'',$idSearchMini); 
    }
?>   
    <?php
    if($iSearchMiniPage!=9){?>
    <div class="cargo-category"> 
        <div class="input-fields" id="idCargoCategory<?php echo $number."".$idSuffix."_container"; ?>"> 
            <input type="hidden" name="cargodetailAry[idCargo][<?=$number?>]" value="<?=$idCargo?>" /> 
            
            <select class="styled standard-cargo-category" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,'idCargoCategory<?php echo $number."".$idSuffix."_container"; ?>');" name="cargodetailAry[idCargoCategory][<?=$number?>]" id="idCargoCategory<?php echo $number."".$idSuffix; ?>">
                <option value=""><?=t($t_base.'fields/furniture_category');?></option>
                <?php
                    if(!empty($standardCargoCategoryAry))
                    {
                        foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
                        {
                            ?>
                            <option value="<?php echo $standardCargoCategoryArys['id']?>" <?php if($standardCargoCategoryArys['id']==$idCargoCategory){?> selected <?php }?> ><?php echo $standardCargoCategoryArys['szCategoryName']; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>
        </div> 
    </div>
    <?php }else{?>
    <input type="hidden" name="cargodetailAry[idCargoCategory][<?=$number?>]" id="idCargoCategory<?php echo $number."".$idSuffix; ?>" value="<?php echo $standardCargoCategoryAry[0]['id'];?>"/>
    <?php }?>
    <div class="cargo-type <?php if($iSearchMiniPage==9){?> cargo-type-search-mini-9 <?php }?>"> 
        <div class="input-fields" id="idStandardCargoType<?php echo $number."".$idSuffix."_container"; ?>"> 
            <?php echo display_cargo_type_dropdown($standardCargoAry,$number,$idSuffix,$idStandardCargoType); ?>
        </div> 
    </div>
    
    <?php
    if($iSearchMiniPage!=9){?>
    <div class="cargo-quantity"> 
        <div class="input-box" id="iQuantity<?php echo $number."".$idSuffix."_container"; ?>">  
            <input type="text" onkeypress="return isIntegerKey(event);" <?=$disabled_str?> pattern="[0-9]*" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'iQuantity<?php echo $number."".$idSuffix."_container"; ?>');" onfocus="check_form_field_not_required(this.form.id,'iQuantity<?php echo $number."".$idSuffix."_container"; ?>');" placeholder="<?php echo ucfirst(t($t_base.'fields/furniture_qty')); ?>" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id="iQuantity<?php echo $number."".$idSuffix; ?>" class="first-cargo-fields<?php echo $idSuffix; ?>" />
        </div>
    </div> 
    <div class="cargo-button-container">
        <div class="v3-cargo-buttons-container clearfix">
            <a href="javascript:void(0);" onclick="add_more_cargo_details('FURNITURE_ORDER_PAGE','<?php echo $postSearchAry['szBookingRandomNum']?>','<?php echo $iSearchMiniPage; ?>','<?php echo $idSearchMini;?>')" class="cargo-add-button">+</a>  
            <a href="javascript:void(0);" class="cargo-remove-button" <?php echo $szMinusButtonId; ?> onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo ($number-1)."_".$iSearchMiniPage ?>','<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>');">-</a>
        </div>
    </div> 
    <?php }?>
    <script type="text/javascript">
        Custom.init();
    </script>
    <?php 
}

function display_cargo_type_dropdown($standardCargoAry,$number,$idSuffix,$idStandardCargoType=false)
{
    $t_base = "LandingPage/";   
?>
    <select class="styled" onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,'idStandardCargoType<?php echo $number."".$idSuffix."_container"; ?>');" name="cargodetailAry[idStandardCargoType][<?=$number?>]" id= "idStandardCargoType<?php echo $number."".$idSuffix; ?>">
        <option value=""><?=t($t_base.'fields/furniture_type');?></option>
        <?php
            if(!empty($standardCargoAry))
            {
                foreach($standardCargoAry as $standardCargoDetail)
                {
                    ?>
                    <option value="<?=$standardCargoDetail['id']?>" <?php if($standardCargoDetail['szLongName']==$idStandardCargoType){?> selected <?php }?>><?=$standardCargoDetail['szLongName']?></option>
                    <?php
                }
            }
        ?>
    </select>
    <script type="text/javascript">
        Custom.init();
    </script>
    <?php
}

function display_vog_cargo_fields($kBooking,$postSearchAry,$iSearchMiniPage=false)
{
    $cargoAry = array(); 
    $idBooking = $postSearchAry['id'];
    
    $kBooking_new = new cBooking();
    $cargoAry = $kBooking_new->getCargoDeailsByBookingId($idBooking);
    $t_base = "LandingPage/";  
    
    $iLanguage = getLanguageId();  
    
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__); 
    $szDanishClass='';
    if($iLanguage==__LANGUAGE_ID_DANISH__)
    {
        $szDanishClass = " CargoHeadingDa";
    }
    $kCourierService = new cCourierServices();   
    $szDivKey = "_".$iSearchMiniPage;  
                    
    $kConfig = new cConfig();
    $standardCargoCategoryAry = array();
    $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,false,$postSearchAry['idSearchMini']);
    
    /*
    * Building json data to pre-populate in category and subcategory menu
    */
    $standardSubCategoryAry = array();
    $standardVogaSubCategoryAry = array();
    if(!empty($standardCargoCategoryAry))
    {
        foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
        { 
            $standardCargoAry = array();
            $standardCargoAry = $kConfig->getStandardCargoList($standardCargoCategoryArys['id'],$iSelectedLanguage,'',$postSearchAry['idSearchMini']);  
            $idStandardCategory = $standardCargoCategoryArys['id'];
                    
            $ctr = 0;
            if(!empty($standardCargoAry))
            {
                foreach($standardCargoAry as $standardCargoArys)
                {
                    $standardSubCategoryAry[$idStandardCategory][$ctr]['id'] = $standardCargoArys['id'];
                    $standardSubCategoryAry[$idStandardCategory][$ctr]['szLongName'] = stripslashes(addslashes($standardCargoArys['szLongName']));
                    $standardVogaSubCategoryAry[$standardCargoArys['id']]['iColli'] = $standardCargoArys['iColli'];
                    $standardVogaSubCategoryAry[$standardCargoArys['id']]['idSearchMini'] = $standardCargoArys['idSearchMini']; 
                    $ctr++;
                }
            } 
        }
    }  
    if(!empty($standardSubCategoryAry))
    {
        $standardSubCategoryJsonStr = json_encode($standardSubCategoryAry,true);
    }
    if(!empty($standardVogaSubCategoryAry))
    {
        $standardVogaSubCategoryJsonStr = json_encode($standardVogaSubCategoryAry);
    }
    ?>  
    <script type="text/javascript">
        <?php
            if(!empty($standardSubCategoryJsonStr))
            {
                ?>
                __JS_ONLY_VOGA_CATEGORY_LISTING__ = <?php echo $standardSubCategoryJsonStr ?>; 
                <?php
            }
        ?> 
        __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__ = <?php echo $standardVogaSubCategoryJsonStr ?>; 
    </script>
    <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
        <?php 
            //echo $iSearchMiniPage."iSearchMiniPage";
            $counter = 1; 
            if(count($_POST['cargodetailAry']['iLength'])>0 || !empty($cargoAry))
            { 
                if(count($_POST['cargodetailAry']['iLength'])>0)
                {
                   $loop_counter = count($_POST['cargodetailAry']['iLength']);  
                   $postDataFlag = true;
                }
                else
                {
                    $loop_counter = count($cargoAry);  
                } 
                for($j=0;$j<$loop_counter;$j++)
                {
                    $counter = $j+1; 
                    ?>
                    <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" class="request-quote-fields clearfix <?php if($iSearchMiniPage==9){?> cargo-detail-search-mini-9 <?php }?>"> 
                        <?php
                            echo display_furniture_order_form($counter,$iSearchMiniPage,array(),$postSearchAry['idSearchMini']);
                        ?>
                    </div>
                    <?php
                }
            }
            else
            {   
                ?>
                <div id="add_booking_quote_container_0<?php echo $szDivKey;?>" class="request-quote-fields clearfix <?php if($iSearchMiniPage==9){?> cargo-detail-search-mini-9 <?php }?>" > 
                    <?php
                       echo display_furniture_order_form($counter,$iSearchMiniPage,array(),$postSearchAry['idSearchMini']);
                    ?>
                </div>
                <?php 
            } 
        ?>
        <div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>"></div>
    </div>     
    <?php
        if(!empty($kBooking->arErrorMessages['szSpecialError']))
        {
            echo '<div class="cargo-success-text">'.$kBooking->arErrorMessages['szSpecialError']."</div>" ;
        }
    ?>
    <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
    <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" />
    <input type="hidden" name="voga_sub_category_type_field" id="voga_sub_category_type_field" value="<?php echo t($t_base.'fields/furniture_type');?>" />
    <input type="hidden" name="iSiteLanguageVogaPages_hidden" id="iSiteLanguageVogaPages_hidden" value="<?php echo __LANGUAGE__;?>" />
    
    <?php  
    if(!empty($kBooking->arErrorMessages))
    {  
        $formId = 'cargo_details_form'; 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
}

function display_booking_notification_popup($bookingDetailsAry)
{
    $t_base = "Booking/MyBooking/"; 
    $redirect_url = __BASE_URL__."/myBooking";
?>
    <div id="popup-bg"></div>
    <div id="popup-container">			
        <div class="popup abandon-popup">
            <p class="close-icon" align="right">
                <a href="<?php echo $redirect_url; ?>">
                    <img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
                </a>
            </p>
            <p>
            <h3><?php echo $bookingDetailsAry['szBookingRef'];?></h3><br>
            <p>
                <?php echo t($t_base.'messages/trackingUpdates_message')." ".$bookingDetailsAry['szBookingRef'].".";?>
            </p>
            <div class="oh">
                <p align="center">
                    <a href="<?php echo $redirect_url; ?>" class="button1"><span><?=t($t_base.'fields/close');?></span></a>
                </p>
            </div>
        </div>
    </div>
    <?php
}
function checkMobileDevice()
{
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
    {
        return 1;
    } else {
        return 0;
    }
}

function display_quick_quotes_searvice_details($postSearchAry)
{
    $t_base = "SelectService/"; 
    $kWHSSearch=new cWHSSearch(); 
    $kBooking = new cBooking();                
    $iPrivateShipping = false;
    if($_SESSION['user_id']>0)
    {
        $kUser = new cUser();
        $kForwarder = new cForwarder();
        $kUser->getUserDetails($_SESSION['user_id']); 
                    
        if($kUser->iPrivate==1)
        {
            $iPrivateShipping = true;
        } 
    }
    
    $iLanguage   = getLanguageId(); 
    $dtCutOffMonth=date("m",strtotime(str_replace("/","-",trim($postSearchAry['dtCutOff'])))); 
    $dtCutOffMonthName=getMonthName($iLanguage,$dtCutOffMonth,true);
    $dtCutOffDay=date("j",strtotime(str_replace("/","-",trim($postSearchAry['dtCutOff'])))); 
    $dtCutOffDate=$dtCutOffDay.". ".$dtCutOffMonthName;  

    $dtAvailableDay=date("j",strtotime(str_replace("/","-",trim($postSearchAry['dtAvailable'])))); 
    $dtAvailableMonth=date("m",strtotime(str_replace("/","-",trim($postSearchAry['dtAvailable'])))); 
    $dtAvailableMonthName=getMonthName($iLanguage,$dtAvailableMonth,true);
    $dtAvailableDate=$dtAvailableDay.". ".$dtAvailableMonthName;  

    $dtCutOffDate_str = date("j F",strtotime(str_replace("/","-",trim($postSearchAry['dtCutOff'])))); 
    $dtAvailableDate_str = date("j F",strtotime(str_replace("/","-",trim($postSearchAry['dtAvailable']))));

    $iCuttOffDateTime = strtotime($dtCutOffDate_str) ;
    $iAvailableDateTime = strtotime($dtAvailableDate_str) ;
    
    $kCourierServices = new cCourierServices();  
    $modeTransport=false;
    $bDTDTradesFlag = false;
    if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true))
    { 
        $bDTDTradesFlag = true;
    }

    if($kCourierServices->checkFromCountryToCountryExists($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry']))
    {
        $modeTransport=true;
    }  
    
    $szPackingType= t($t_base.'fields/pallets_and_cartons'); 
    $packingText_only= t($t_base.'fields/only');
    $courierBookingArr=array();
    if($postSearchAry['iCourierBooking']==1){
        
        $courierBookingArr=$kCourierServices->getCourierBookingData($postSearchAry['id']);
    }
    
    if($courierBookingArr['idCourierProvider']>0)
    {    
        $packingArr = $kCourierServices->getCourierProviderProductList($courierBookingArr['idCourierProvider'],$postSearchAry['idServiceProviderProduct'],$iLanguage); 
                    
        $iCourierBooking = 1;  
        $courier_service_ctr++; 
        $szPackingType=''; 
        if($packingArr[0]['idPackingType'] == __COURIER_PACKAGING_TYPE_PALLETS_AND_CARTONS__)
        {
            $szPackingType = ucfirst($packingArr[0]['szPacking']);
        } 
        else
        {
            $szPackingType = $packingText_only." ".strtolower($packingArr[0]['szPacking']);
        }  
        $idPackingType = $packingArr[0]['idPackingType'];
        if($idPackingType==1)
        {
            $iOnlyCartonServiceDisplayedCtr++;
        }
        if($modeTransport)
        { 
            $showOfferFlag=false;
            $szForwarderImage = "Truck.png"; 
        }
        else
        {  
            $szForwarderImage = "Plane.png"; 
        } 
    }
    else
    {  
        $kServices = new cServices();
        $railTransportAry = array();
        $railTransportAry['idForwarder'] = $postSearchAry['idForwarder'];
        $railTransportAry['idFromWarehouse'] = $postSearchAry['idWarehouseFrom'];
        $railTransportAry['idToWarehouse'] = $postSearchAry['idWarehouseTo'];  
        if($kServices->isRailTransportExists($railTransportAry))
        {
            $szForwarderImage = "Rail.png"; 
            $bDTDTradesFlag = false;
        }
        else if($bDTDTradesFlag)
        { 
            if($postSearchAry['fCargoVolume']>0.6 || $postSearchAry['fCargoWeight']>270)
            {
                $szPackingType = t($t_base.'fields/euro_pallets'); 
                $LCDTDQuantityFlag=2;
            }
            else
            {
                $szPackingType = t($t_base.'fields/half_euro_pallets'); 
                $LCDTDQuantityFlag=1;
            }
            $szForwarderImage = "Truck.png"; 
        } 
        else 
        {  
            $szForwarderImage = "Ship.png"; 
        }  
    } 
    
     $start = date("Y-m-d",strtotime($postSearchAry['dtCutOff']));
    $end = date("Y-m-d",strtotime($postSearchAry['dtAvailable']));
    $iDaysBetween = ceil(abs(strtotime($end) - strtotime($start)) / 86400);
               
    $tr_counter=1;
    $szClassName = "odd";
    $szBookingRandomNum = $postSearchAry['szBookingRandomNum'];
    $idUniqueWTW=$postSearchAry['idUniqueWTW'];
    $szCustomerCurrency=$postSearchAry['szCurrency']; 
    if($iPrivateShipping)
    {
        $fTotalBookingPrice = number_format_custom((float)($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalVat']),$iLanguage); 
    }
    else
    {
        $fTotalBookingPrice = number_format_custom((float)$postSearchAry['fTotalPriceCustomerCurrency'],$iLanguage); 
    } 
    $forwarderRatingAr = $kWHSSearch->getForwarderAvgRating($postSearchAry['idForwarder']);

    $forwarderReviewsAry = $kWHSSearch->getForwarderAvgReviews($postSearchAry['idForwarder']);

    $iNumRating = $forwarderRatingAr['iNumRating'] ;
    $fAvgRating = $forwarderRatingAr['fAvgRating'] ;
    //echo $szCustomerCurrency."szCustomerCurrency";
?>
	<div class="results quick-quotes-results"> 
            <div class="head clearfix">
                <div class="logo" onclick="sort_service_list('szDisplayName','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_szDisplayName"><?php echo t($t_base.'fields/mode_of_transport'); ?></span></div>
                <div class="rating" onclick="sort_service_list('iNumRating','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iNumRating"><?php echo t($t_base.'fields/rating'); ?></span></div>
                <div class="cut-off"><?php if($postSearchAry['idServiceType']==__SERVICE_TYPE_PTD__){?><?=t($t_base.'fields/packing');?> <?php }else{?> <?=t($t_base.'fields/packing');?> <?php }?></div>
                <?php if($bDTDTradesFlag){ ?>  
                    <div class="delivery" onclick="sort_service_list('iCuttOffDate_time','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iCuttOffDate_time"><?php echo ucwords(t($t_base.'fields/pick_up'));?></span></div>
                    <div class="days" onclick="sort_service_list('iAvailabeleDate_time','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iAvailabeleDate_time"><?php echo t($t_base.'fields/delivery');?></span></div>
                <?php } else { ?>
                    <div class="delivery" onclick="sort_service_list('iAvailabeleDate_time','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iAvailabeleDate_time"><?php echo t($t_base.'fields/delivery');?></span></div>
                    <div class="days" onclick="sort_service_list('iDaysBetween','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><span class="service-sorter-span" id="service_sorter_iDaysBetween"><?php echo ucwords(t($t_base.'fields/days'));?></span></div>
                <?php } ?> 
                <div class="price"> 
                    <span class="service-sorter-span" id="service_sorter_fDisplayPrice" onclick="sort_service_list('fDisplayPrice','<?php echo $szBookingRandomNum; ?>','<?php echo $_REQUEST['lang']?>');" style="cursor: pointer;"><?php echo t($t_base.'fields/all_in_price'); ?> </span> 
                </div>
            </div>
            <div class="<?php echo $szClassName; ?> search-result-show-hide-div clearfix" id="select_services_tr_<?php echo $idUniqueWTW; ?>">
                <div class="logo">
                    <a href="javascript:void(0)" <?php if($iDummyService!=1){ ?> onclick="openForwarderRatingPopUp('<?=$postSearchAry['idForwarder']?>')" <?php } ?> style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                        <img src="<?=__BASE_STORE_INC_URL__.'/image.php?img='.$szForwarderImage.'&temp=2&w=138&h=61'?>" alt="<?=$postSearchAry['szDisplayName']?>" border="0" />
                    </a> 
                </div>
                <div class="rating">
                    <a href="javascript:void(0)" onclick="openForwarderRatingPopUp('<?=$postSearchAry['idForwarder']?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                        <?php 
                            if($iNumRating<$minRatingToShowStars)
                            { 
                                echo displayRatingStar(0); 
                            }
                            else 
                            {									
                                // one filled star's width is 10px so that we are multiplying Avg Rating with 10 
                                $fAvgRating = round(($fAvgRating*__RATING_STAR_WIDTH__),2);
                                echo displayRatingStar($fAvgRating);
                            }
                        ?>
                        <span>(<?php echo (int)$iNumRating; ?>)</span>
                    </a>
                </div>
                <div class="cut-off"><?php echo $szPackingType; ?></div>
                <?php if($bDTDTradesFlag==1){?>
                    <div class="delivery"><?php echo $dtCutOffDate;?></div>
                    <div class="days"><?php echo $dtAvailableDate;?></div>
                <?php } else { ?>
                    <div class="delivery"><?php echo $dtAvailableDate;?></div>
                    <div class="days"><?php echo $iDaysBetween; ?></div>
                <?php } ?> 
                <div class="price"><span id="fullResolution"><?php echo $szCustomerCurrency ?></span>&nbsp;<span id="select_services_tr_price_container_<?php echo $idUniqueWTW; ?>"><?php echo $fTotalBookingPrice; ?></span></div>
                <?php if($bDisplayBooknowButton){ ?>
                    <div class="btn">  
                        <?php if(!$forwarder_flag){  ?>
                            <a href="javascript:void(0)" onclick="toggleServiceList('<?=$idUniqueWTW?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__NEW_BOOKING_DETAILS_PAGE_URL__?>','NEW_BOOKING_PROCESS','<?php echo $iCourierBooking; ?>');" class="button-yellow" style="display:none;" id="hidden-search-service-book-now-button_<?php echo $idUniqueWTW; ?>"><span><?php echo t($t_base.'fields/selected'); ?></span></a>
                            <a href="javascript:void(0)" onclick="display_forwarder_non_accepted_popup('<?=$idUniqueWTW?>','<?=$searchResults['idForwarder']?>','<?=$_SESSION['user_id']?>','<?=__NEW_BOOKING_DETAILS_PAGE_URL__?>','NEW_BOOKING_PROCESS','<?php echo $iCourierBooking; ?>','<?php echo $idPackingType; ?>','<?php echo $_REQUEST['lang']?>','<?php echo $LCDTDQuantityFlag;?>','<?php echo $iDummyService; ?>');" class="button-green1" id="search-service-book-now-button_<?php echo $idUniqueWTW; ?>"><span><?php echo t($t_base.'fields/book_now'); ?></span></a>
                        <?php } else { ?>
                            <a href="javascript:void(0)" onclick="display_fowarder_price_details('<?=$idUniqueWTW?>','select_services_tr_<?=$tr_counter?>','<?php echo $szBookingRandomNum; ?>')" style="text-decoration: none;color:#000000;font-style:normal;font-size:12px;">
                            <?=$searchResults['szCurrency']?><br /><?=number_format((float)$searchResults['fDisplayPrice'],2)?></a>
                        <?php } ?>  
                    </div>
                <?php } ?>
            </div>
        </div>
<?php
}

function display_dtd_forwarder_price_details($searchedAry,$t_base,$postSearchAry=array(),$bDTDTradesFlag=false)
{
    if(empty($postSearchAry))
    {
        $postSearchAry = array();
        $postSearchAry = $_SESSION['try_it_out_searched_data'] ;
    } 
                    
    $kConfig = new cConfig();
    $szServiceTerm = trim($postSearchAry['szServiceTerm']);
    $iWarehouseType =  $searchedAry['iWarehouseType'];               
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        $szMaxChargeableWeightCalculation = 'max('.number_format((float)$searchedAry['fCargoWeight']).';'. format_volume((float)$searchedAry['fCargoVolume']).'cbm x 167kg/cbm)';
    }
    else
    {
        $szMaxChargeableWeightCalculation = 'max('.ceil($searchedAry['fCargoVolume']).'cbm;'.ceil($searchedAry['fCargoWeight']/1000).'mt)';
    }
    $szForwarderServiceTerm = '';
    if(!empty($szServiceTerm))
    {
        $serviceTermsAry = array();
        $serviceTermsAry =  $kConfig->getAllServiceTerms(false,$szServiceTerm);
        if(!empty($serviceTermsAry))
        {
            $szForwarderServiceTerm = $serviceTermsAry[0]['szDisplayNameForwarder'];
        } 
        
        if($szServiceTerm=='FOB' || $szServiceTerm=='EXW-WTD')
        {
            $szCustomerCity = $searchedAry['szFromWHSCity'];
        }
        else if($szServiceTerm=='EXW')
        {
            $szCustomerCity = $postSearchAry['szShipperCity'];
        }
        else if($szServiceTerm=='CFR')
        {
            $szCustomerCity = $searchedAry['szToWHSCity'];
        }
        else
        {
            $szCustomerCity = $postSearchAry['szConsigneeCity'];
        }
    }
    
    if($searchedAry['szOriginHaulageCurrency']>0)
    {
        $OriginHaulAry = $kConfig->getBookingCurrency($searchedAry['szOriginHaulageCurrency']);
    }
    if($searchedAry['fOriginCCCurrency']>0)
    {
        $fOriginCCCurrencyAry = $kConfig->getBookingCurrency($searchedAry['fOriginCCCurrency']);
    }
    if($searchedAry['szFreightCurrency']>0)
    {
        $FreightCurrencyAry = $kConfig->getBookingCurrency($searchedAry['szFreightCurrency']);
    }
    if($searchedAry['szDestinationHaulageCurrency']>0)
    {
        $DestinationHaulAry = $kConfig->getBookingCurrency($searchedAry['szDestinationHaulageCurrency']);
    }
    if($searchedAry['fDestinationCCCurrency']>0)
    {
        $fDestinationCCCurrencyAry = $kConfig->getBookingCurrency($searchedAry['fDestinationCCCurrency']);
    }  
    $fGrandTotalCustomerCurrency = 0;
                    
    $fTotalSelfInvoiceAmount = $searchedAry['fTotalSelfInvoiceAmount'] + $searchedAry['fReferalAmount'];
                    
    $fReferralPercentage = $searchedAry['fReferalPercentage'];
    $fReferralFeeAmount = round((float)$searchedAry['fReferalAmount'],2);
    $fTotalSelfInvoiceAmountNetPayable = $fTotalSelfInvoiceAmount - $fReferralFeeAmount;
    
    if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTP__ ) //1.DTD , 2.DTW
    {
        if($postSearchAry['fExchangeRate']>0)
        {
            $fOriginHaulagePriceCustomerCurrency = round((float)$searchedAry['fOriginHaulagePrice']/$postSearchAry['fExchangeRate'],2);
        }
        else
        {
            $fOriginHaulagePriceCustomerCurrency = 0;
        }
        $fGrandTotalCustomerCurrency += $fOriginHaulagePriceCustomerCurrency;
    }
    if($searchedAry['fOriginCCPrice']>0) //1.DTD , 2.DTW
    {
        if($postSearchAry['fExchangeRate']>0)
        {
            $fOriginCCPriceCustomerCurrency = round((float)$searchedAry['fOriginCCPrice']/$postSearchAry['fExchangeRate'],2);
        }
        else
        {
            $fOriginCCPriceCustomerCurrency = 0.00;
        }
        $fGrandTotalCustomerCurrency += $fOriginCCPriceCustomerCurrency;
    }
    
    /*
    * Calculation freight price customer currency
    */
    if($postSearchAry['fExchangeRate']>0)
    {
        $fPTPPriceCustomerCurrency = round((float)$searchedAry['fPTPPrice']/$postSearchAry['fExchangeRate'],2);
    }
    else
    {
        $fPTPPriceCustomerCurrency = 0.00;
    }
    $fGrandTotalCustomerCurrency += $fPTPPriceCustomerCurrency;
    
    if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTP__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTP__ ) //1.DTD , 2.DTW
    {
         /*
        * Calculation origin port price customer currency
        */
        if($postSearchAry['fExchangeRate']>0)
        {
            $fTotalOriginPortPriceCustomerCurrency = round((float)$searchedAry['fTotalOriginPortPrice']/$postSearchAry['fExchangeRate'],2);
        }
        else
        {
            $fTotalOriginPortPriceCustomerCurrency = 0.00;
        }
        $fGrandTotalCustomerCurrency += $fTotalOriginPortPriceCustomerCurrency;
    }
    
    if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_PTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_PTW__ ) //1.DTD , 2.DTW
    {
        /*
        * Calculation Destination port price customer currency
        */
        if($postSearchAry['fExchangeRate']>0)
        {
            $fTotalDestinationPortPriceCustomerCurrency = round((float)$searchedAry['fTotalDestinationPortPrice']/$postSearchAry['fExchangeRate'],2);
        }
        else
        {
            $fTotalDestinationPortPriceCustomerCurrency = 0.00;
        }
        $fGrandTotalCustomerCurrency += $fTotalDestinationPortPriceCustomerCurrency;
    }
    if($searchedAry['fDestinationCCPrice']>0) //1.DTD , 2.DTW
    {
        /*
        * Calculation Destination port price customer currency
        */
        if($postSearchAry['fExchangeRate']>0)
        {
            $fDestinationCCPriceCustomerCurrency = round((float)$searchedAry['fDestinationCCPrice']/$postSearchAry['fExchangeRate'],2);
        }
        else
        {
            $fDestinationCCPriceCustomerCurrency = 0.00;
        }
        $fGrandTotalCustomerCurrency += $fDestinationCCPriceCustomerCurrency;
    }
    if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_PTD__) //1.DTD , 3.WTD
    {

        /*
        * Calculation Destination port price customer currency
        */
        if($postSearchAry['fExchangeRate']>0)
        {
            $fDestinationHaulagePriceCustomerCurrency = round((float)$searchedAry['fDestinationHaulagePrice']/$postSearchAry['fExchangeRate'],2);
        }
        else
        {
            $fDestinationHaulagePriceCustomerCurrency = 0.00;
        }
        $fGrandTotalCustomerCurrency += $fDestinationHaulagePriceCustomerCurrency;
    }
    
    $fTotalPriceCustomerCurrency = $fGrandTotalCustomerCurrency;
    $iPrivateFeeApplicable = $searchedAry['iPrivateCustomerFeeApplicable']; 
    if($iPrivateFeeApplicable==1)
    { 
        if($searchedAry['idPrivateCustomerFeeCurrency']==$postSearchAry['idCurrency'])
        {
            $fPrivateCustomerFeeLocalCurrencyExchangeRate='1';
            $fTotalPrivateCustomerFeeApplicable = $searchedAry['fPrivateCustomerFee'];
        }
        else if($searchedAry['fPrivateCustomerFeeExchangeRate']>0 && $postSearchAry['fExchangeRate']>0)
        {
            $fPrivateCustomerFeeLocalCurrencyExchangeRate =$postSearchAry['fExchangeRate']/$searchedAry['fPrivateCustomerFeeExchangeRate'];
            $fTotalPrivateCustomerFeeApplicable = $searchedAry['fPrivateCustomerFeeUSD'] / $postSearchAry['fExchangeRate'];
        } 
        $fGrandTotalCustomerCurrency += $fTotalPrivateCustomerFeeApplicable;
        if((float)$fTotalPrivateCustomerFeeApplicable>0.00){
            $szPrivateCustomerFeeDetailsString .='	
                <tr>
                    <td valign="top" align="left">Private customer fee<br /></td>
                    <td valign="top" align="right"> '.$searchedAry['szPrivateCustomerFeeCurrency'].' '.number_format((float)$searchedAry['fPrivateCustomerFee'],2).'</td>
                    <td valign="top" align="right">'.number_format((float)$fPrivateCustomerFeeLocalCurrencyExchangeRate,4).'</td>
                    <td valign="top" align="right">'.$postSearchAry['szCurrency']." ".number_format((float)$fTotalPrivateCustomerFeeApplicable,2).'</td>
                </tr>
                <tr>
                    <td colspan="4" align="left">&nbsp;</td>
                </tr>
            ';   
        }  
    }
    
    ?> 
    <table cellpadding="3" cellspacing="0" border="0" width="100%">
        <tr class="td-border-bottom">
            <th style="width:55%" align="left"><?php echo $szForwarderServiceTerm." ".$szCustomerCity; ?></th>
            <th style="width:15%" align="right"><?=t($t_base.'fields/price');?></th>
            <th style="width:15%" align="right"><?=t($t_base.'fields/roe');?></th>
            <th style="width:15%" align="right"><?=t($t_base.'fields/price');?></th>
        </tr>
        <?php if($bDTDTradesFlag){ ?> 
            <tr>
                <td>Freight<br /></td>
                <td align="right"><?php echo $postSearchAry['szCurrency']." ".number_format((float)$fTotalPriceCustomerCurrency,2); ?> </td>
                <td align="right"><?php echo '1.0000' ?> </td>
                <td align="right"><?php echo $postSearchAry['szCurrency']." ".number_format((float)$fTotalPriceCustomerCurrency,2); ?></td>
            </tr> 
	  <?php
        }
        else
        {
            if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTP__ ) //1.DTD , 2.DTW
            {			
                //gathering data for google map
                $originPostCodeAry=array();
                $originPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szOriginCountry'],$postSearchAry['szOriginPostCode'],$postSearchAry['szOriginCity']);

                $szOriginAddress =  $postSearchAry['szOriginPostCode']." - ".$postSearchAry['szOriginCity']."<br /> ".$originPostCodeAry['szCountry'];
                $szDestinationAddress = $searchedAry['szFromWHSPostCode']." - ".$searchedAry['szFromWHSCity']."<br />".$searchedAry['szFromWHSCountry'];
                $origin_lat_lang_pipeline = $originPostCodeAry['szLatitude']."|".$originPostCodeAry['szLongitude']."|".$searchedAry['szFromWHSLat']."|".$searchedAry['szFromWHSLong']."|".$searchedAry['fOriginHaulageDistance']."|".$szOriginAddress."|".$szDestinationAddress;

                    //this code block is displayed only in DTD and DTW 

                //for ROE we are showing inverse of actual Rate of exchange
                //formula for calculating ROE is ((1/customer currency exchange rate) * Origin haulage exchange rate)
                if($searchedAry['fOriginHaulageExchangeRate']>0)
                { 
                    $searchedAry['fOriginHaulageExchangeRate'] = 1/$searchedAry['fOriginHaulageExchangeRate'] ;
                    $fOriginHaulageRoe = (1/$postSearchAry['fExchangeRate']) * $searchedAry['fOriginHaulageExchangeRate'] ;		    	
                    $fOriginHaulageTotalRoe =number_format((float)(1/$fOriginHaulageRoe),4);
                }
                else
                {
                    $fOriginHaulageTotalRoe=0.0000 ;
                } 
	  ?>
	  <tr>
	    <td><?=t($t_base.'fields/export_haulage');?></td>
	    <td align="right"><?=$searchedAry['szOriginHaulageCurrency']?> <?=$searchedAry['fOriginHaulageExchangeRate']>0 ? number_format((float)($searchedAry['fOriginHaulagePrice']/$searchedAry['fOriginHaulageExchangeRate']),2) : 0.00?></td>
	    <td align="right"><?=$fOriginHaulageTotalRoe?></p></td>
	    <td align="right"><?=$searchedAry['szCurrency']?> <?php echo number_format((float)$fOriginHaulagePriceCustomerCurrency,2);?></td>
	  </tr>
	  <tr>
	     <td colspan="4" align="left" class="color f-size-12" valign="top"><em><?=t($t_base.'fields/calculation');?>: (<?=t($t_base.'fields/max');?>(ROUNDUP(<?=t($t_base.'fields/max');?>(<?=number_format((float)$searchedAry['fCargoWeight'])?>kg ; <?=format_volume((float)$searchedAry['fCargoVolume'])?><i>cbm</i> x <?=number_format((float)$searchedAry['fOriginHaulageWmFactor'])?>kg/cbm)/100kg) x <?=$searchedAry['szOriginHaulageCurrency']?> <?=number_format((float)$searchedAry['fOriginHaulageRateWM_KM'],2)?>) ; <?=$searchedAry['szOriginHaulageCurrency']?> <?=number_format((float)$searchedAry['fOriginHaulageMinRateWM_KM'],2)?>) + <?=$searchedAry['szOriginHaulageCurrency']?> <?=number_format((float)$searchedAry['fOriginHaulageBookingRateWM_KM'],2)?>) x (1 + <?=number_format((float)$searchedAry['fOriginHaulageFuelPercentage'],1)?>%)  </em></td>
	  </tr>
	  <?php
            }
            if($searchedAry['fOriginCCPrice']>0) //1.DTD , 2.DTW
            {
                if($searchedAry['fOriginCCExchangeRate']>0)
                {
                        $fOriginCCRoe = (1/$postSearchAry['fExchangeRate']) * $searchedAry['fOriginCCExchangeRate'] ;
                        $fTotalOriginCCRoe = number_format((float)(1/$fOriginCCRoe),4);
                }
                else
                {
                        $fTotalOriginCCRoe = 0.0000;
                } 
	   ?>
                <tr>
                 <td><?=t($t_base.'fields/export_custom_clearance');?></td>
                 <td align="right"><?=$fOriginCCCurrencyAry[0]['szCurrency']?> <?php echo (($searchedAry['fOriginCCExchangeRate']>0) ? number_format((float)($searchedAry['fOriginCCPrice']/$searchedAry['fOriginCCExchangeRate']),2) : '0.00'); ?></td>
                 <td align="right"><?=$fTotalOriginCCRoe?></p></td>
                 <td align="right"><?=$searchedAry['szCurrency']?> <?php echo number_format($fOriginCCPriceCustomerCurrency,2); ?></td>
                </tr>
                <tr>
                    <th colspan="4" align="left" class="color f-size-12" valign="top">&nbsp;</th>
                </tr>
		  <?php
		}
		if($searchedAry['fFreightExchangeRate']>0)
		{
                    $fFreightRoe = (1/$postSearchAry['fExchangeRate']) * $searchedAry['fFreightExchangeRate'] ;
                    $fTotalFreightRoe = number_format((float)(1/$fFreightRoe),4);
		}
		else
		{
                    $fTotalFreightRoe = 0.0000;
		} 
		if($searchedAry['fOriginFreightExchangeRate']>0)
		{
                    $fOriginFreightRoe = (1/$postSearchAry['fExchangeRate']) * (1/$searchedAry['fOriginFreightExchangeRate']);
                    $fTotalOriginFreightRoe = number_format((float)(1/$fOriginFreightRoe),4);
		}
		else
		{
                    $fTotalOriginFreightRoe = 0.0000;
		} 
		if($searchedAry['fDestinationFreightExchangeRate']>0)
		{
                    $fDestinationFreightRoe = (1/$postSearchAry['fExchangeRate']) * (1/$searchedAry['fDestinationFreightExchangeRate']);
                    $fTotalDestinationFreightRoe = number_format((float)(1/$fDestinationFreightRoe),4);
		}
		else
		{
                    $fTotalDestinationFreightRoe = 0.0000;
		}  
		if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTP__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTP__ ) //1.DTD , 2.DTW
	  	{ 
                    ?>
		    <tr>
                        <td><?=t($t_base.'fields/origin_charges');?></td>
                        <td align="right"><?=$searchedAry['szOriginPortCurrency']?> <?=$searchedAry['fOriginFreightExchangeRate']>0 ? number_format((float)($searchedAry['fTotalOriginPortPrice'] * $searchedAry['fOriginFreightExchangeRate']),2) : 0.00?></td>
                        <td align="right"><?=$fTotalOriginFreightRoe?></p></td>
                        <td align="right"><?=$searchedAry['szCurrency']?> <?php echo number_format($fTotalOriginPortPriceCustomerCurrency,2); ?></td>
		    </tr>
		    <tr>
		    	<td colspan="4" align="left" class="color f-size-12" valign="top"><em><?=t($t_base.'fields/calculation');?>: <?=t($t_base.'fields/max');?>(<?=$searchedAry['szOriginPortCurrency']?> <?=$searchedAry['fOriginRateWM']?> x <?php echo $szMaxChargeableWeightCalculation; ?>;<?=$searchedAry['szOriginPortCurrency']?> <?=$searchedAry['fOriginMinRateWM']?>) + <?=$searchedAry['szOriginPortCurrency']?> <?=$searchedAry['fOriginRate']?> </em> </td>
		    </tr>
	    <?php
		}
	    ?>
	   <tr>
	    <td><?=t($t_base.'fields/ptp_to_ptp');?></td>
	    <td align="right"><?=$FreightCurrencyAry[0]['szCurrency']?> <?=$searchedAry['fFreightExchangeRate']>0 ? number_format((float)(($searchedAry['fPTPPrice'])/$searchedAry['fFreightExchangeRate']),2) : 0.00?></td>
	    <td align="right"><?=$fTotalFreightRoe?></p></td>
	    <td align="right"><?=$searchedAry['szCurrency']?> <?php echo number_format($fPTPPriceCustomerCurrency,2); ?></td>
	  </tr>
	  <tr>
	    <td colspan="4" align="left" class="color f-size-12" valign="top"><em><?=t($t_base.'fields/calculation');?>: <?=t($t_base.'fields/max');?>(<?=$FreightCurrencyAry[0]['szCurrency']?> <?=$searchedAry['fRateWM']?> x <?php echo $szMaxChargeableWeightCalculation; ?>;<?=$FreightCurrencyAry[0]['szCurrency']?> <?=$searchedAry['fMinRateWM']?>) + <?=$FreightCurrencyAry[0]['szCurrency']?> <?=$searchedAry['fRate']?> </em> </td>
	  </tr>
	  <?php if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_DTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTW__ || $searchedAry['idServiceType']==__SERVICE_TYPE_PTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_PTW__ ){ //1.DTD , 2.DTW ?> 
	  <tr>
	    <td><?=t($t_base.'fields/destination_charges');?></td>
	    <td align="right"><?=$searchedAry['szDestinationPortCurrency']?> <?=$searchedAry['fDestinationFreightExchangeRate']>0 ? number_format((float)(($searchedAry['fTotalDestinationPortPrice']* $searchedAry['fDestinationFreightExchangeRate'])),2) : 0.00?></td>
	    <td align="right"><?=$fTotalDestinationFreightRoe?></p></td>
	    <td align="right"><?=$searchedAry['szCurrency']?> <?=number_format($fTotalDestinationPortPriceCustomerCurrency,2); ?></td>
	    </tr>
	    <tr>
	    	<td colspan="4" align="left" class="color f-size-12" valign="top"><em><?=t($t_base.'fields/calculation');?>: <?=t($t_base.'fields/max');?>(<?=$searchedAry['szDestinationPortCurrency']?> <?=$searchedAry['fDestinationRateWM']?> x <?php echo $szMaxChargeableWeightCalculation; ?>;<?=$searchedAry['szDestinationPortCurrency']?> <?=$searchedAry['fDestinationMinRateWM']?>) + <?=$searchedAry['szDestinationPortCurrency']?> <?=$searchedAry['fDestinationRate']?> </em> </td>
	    </tr>
	    <?php }?>
	    <?php
	  	if($searchedAry['fDestinationCCPrice']>0) //1.DTD , 2.DTW
	  	{
                    if($searchedAry['fDestinationCCExchangeRate']>0)
                    {
                        $fDestinationCCRoe = (1/$postSearchAry['fExchangeRate']) * $searchedAry['fDestinationCCExchangeRate'] ;
                        $fTotalDestinationCCRoe = number_format((float)(1/$fDestinationCCRoe),4) ;
                    }
                    else
                    {
                        $fTotalDestinationCCRoe=0.0000 ;
                    }
                   
		    ?>
                    <tr>
                        <td><?=t($t_base.'fields/import_custom_clearance');?></td>
                        <td align="right"><?=$fDestinationCCCurrencyAry[0]['szCurrency']?> <?=$searchedAry['fDestinationCCExchangeRate']>0 ? number_format((float)($searchedAry['fDestinationCCPrice']/$searchedAry['fDestinationCCExchangeRate']),2) : 0.00?></td>
                        <td align="right"><?=$fTotalDestinationCCRoe?></p></td>
                        <td align="right"><?=$searchedAry['szCurrency']?> <?php echo number_format($fDestinationCCPriceCustomerCurrency,2);?></td>
                    </tr>
                    <tr>
	    	<th colspan="4" align="left" class="color f-size-12" valign="top">&nbsp;</th>
	  	  </tr>
		   <?php
		}	  	
	  	if($searchedAry['idServiceType']==__SERVICE_TYPE_DTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_WTD__ || $searchedAry['idServiceType']==__SERVICE_TYPE_PTD__) //1.DTD , 3.WTD
	  	{
                    $destinationPostCodeAry=array();
                    $destinationPostCodeAry = $kConfig->getPostCodeDetails($postSearchAry['szDestinationCountry'],$postSearchAry['szDestinationPostCode'],$postSearchAry['szDestinationCity']);

                    $szOriginAddress2 =  $postSearchAry['szDestinationPostCode']." - ".$postSearchAry['szDestinationCity']."<br /> ".$destinationPostCodeAry['szCountry'];
		    $szDestinationAddress2 = $searchedAry['szToWHSPostCode']." - ".$searchedAry['szToWHSCity']."<br />".$searchedAry['szToWHSCountry'];
		
		    $destination_lat_lang_pipeline = $destinationPostCodeAry['szLatitude']."|".$destinationPostCodeAry['szLongitude']."|".$searchedAry['szToWHSLat']."|".$searchedAry['szToWHSLong']."|".$searchedAry['fDestinationHaulageDistance']."|".$szOriginAddress2."|".$szDestinationAddress2;
	  		//this code block is displayed only in DTD and WTD
	  		  		
		    if($searchedAry['fDestinationHaulageExchangeRate']>0)
		    {
		    	$searchedAry['fDestinationHaulageExchangeRate'] = 1/$searchedAry['fDestinationHaulageExchangeRate'] ;
		    	$fDestinationHaulageRoe = (1/$postSearchAry['fExchangeRate']) * $searchedAry['fDestinationHaulageExchangeRate'] ;
		    	$fTotalDestinationHaulageRoe = number_format((float)(1/$fDestinationHaulageRoe),4);
		    }
		    else
		    {
		    	$fTotalDestinationHaulageRoe = 0.0000;
		    } 
	  ?>
            <tr>
                <td><?=t($t_base.'fields/import_haulage');?></td>
                <td align="right"><?=$searchedAry['szDestinationHaulageCurrency']?> <?=$searchedAry['fDestinationHaulageExchangeRate']>0 ? number_format((float)($searchedAry['fDestinationHaulagePrice'] / $searchedAry['fDestinationHaulageExchangeRate']),2) : 0.00?></td>
                <td align="right"><?=$fTotalDestinationHaulageRoe?></p></td>
                <td align="right"><?=$searchedAry['szCurrency']?> <?php echo number_format($fDestinationHaulagePriceCustomerCurrency,2); ?></td>
            </tr>
            <tr>
                <td colspan="4" align="left" class="color f-size-12" valign="top"><em><?=t($t_base.'fields/calculation');?>: (<?=t($t_base.'fields/max');?>(ROUNDUP(<?=t($t_base.'fields/max');?>(<?=number_format((float)$searchedAry['fCargoWeight'])?>kg ; <?=format_volume((float)$searchedAry['fCargoVolume'])?><i>cbm</i> x <?=ceil($searchedAry['fDestinationHaulageWmFactor'])?>kg/cbm)/100kg) x <?=$searchedAry['szDestinationHaulageCurrency']?> <?=number_format((float)$searchedAry['fDestinationHaulageRateWM_KM'],2)?>) ; <?=$searchedAry['szDestinationHaulageCurrency']?> <?=number_format((float)$searchedAry['fDestinationHaulageMinRateWM_KM'],2)?>) + <?=$searchedAry['szDestinationHaulageCurrency']?> <?=number_format((float)$searchedAry['fDestinationHaulageBookingRateWM_KM'],2)?>) x (1 + <?=number_format((float)$searchedAry['fDestinationHaulageFuelPercentage'],1)?>%)  </em></td>
            </tr>
	 <?php
	  }
        } 
          echo $szPrivateCustomerFeeDetailsString;
	  ?>
        <tr class="td-border-top">
            <td align="left">Price paid by customer</td>
            <td></td>
            <td></td>
            <td align="right"><?php echo $searchedAry['szForwarderCurrency']." ".number_format((float)$fTotalSelfInvoiceAmount,2)?></td>
        </tr>  
        <tr>
            <td colspan="4" align="left">&nbsp;</td>
        </tr>
        <tr>
            <td align="left">Referral fee for Transporteca</td>
            <td></td>
            <td></td>
            <td align="right"><?php echo $searchedAry['szForwarderCurrency']." ".number_format((float)$fReferralFeeAmount,2)?></td>
        </tr>
        <tr>
            <td colspan="4" align="left" class="color f-size-12" valign="top"><em>Calculation: <?php echo $searchedAry['szForwarderCurrency']." ".number_format((float)$fTotalSelfInvoiceAmount,2)?> x <?php echo $fReferralPercentage."%" ?></em></td>
        </tr>
        <tr class="td-border-top">
            <td align="left">Net payment transferred to you</td>
            <td></td>
            <td></td>
            <td align="right"><?php echo $searchedAry['szForwarderCurrency']." ".number_format((float)$fTotalSelfInvoiceAmountNetPayable,2)?></td>
        </tr> 
    </table> 
	<?php 
}  

function display_forwarder_try_it_out_courier_price_details($searchResults,$postSearchAry) //DTD
{
    $kWHSSearch=new cWHSSearch();  
    $t_base = "SERVICEOFFERING/";   
    $iPrivateFeeApplicable = $searchResults['iPrivateCustomerFeeApplicable']; 
    
    $fTotalSelfInvoiceAmount = $searchResults['fTotalSelfInvoiceAmount'];
    $szPrivateCustomerFeeDetailsString = "";
    $fGrandTotalCustomerCurrency = 0;
    if($iPrivateFeeApplicable==1)
    { 
        if($searchResults['idPrivateCustomerFeeCurrency']==$searchResults['idForwarderCurrency'])
        {
            $fPrivateCustomerFeeLocalCurrencyExchangeRate='1';
            $fTotalPrivateCustomerFeeApplicable = $searchResults['fPrivateCustomerFee'];
        }
        else if($searchResults['fPrivateCustomerFeeExchangeRate']>0 && $postSearchAry['fForwarderCurrencyExchangeRate']>0)
        {
            $fPrivateCustomerFeeLocalCurrencyExchangeRate =$postSearchAry['fForwarderCurrencyExchangeRate']/$searchResults['fPrivateCustomerFeeExchangeRate'];
            $fTotalPrivateCustomerFeeApplicable = $searchResults['fPrivateCustomerFeeUSD'] / $postSearchAry['fForwarderCurrencyExchangeRate'];
        }  
        $fGrandTotalCustomerCurrency += $fTotalPrivateCustomerFeeApplicable;
        if((float)$fTotalPrivateCustomerFeeApplicable>0.00){
            $szPrivateCustomerFeeDetailsString .='	 
                <tr>
                    <td valign="top" align="left">Private customer fee<br /></td>
                    <td valign="top" align="right"> '.$searchResults['szPrivateCustomerFeeCurrency'].' '.number_format((float)$searchResults['fPrivateCustomerFee'],2).'</td>
                    <td valign="top" align="right">'.number_format((float)$fPrivateCustomerFeeLocalCurrencyExchangeRate,4).'</td>
                    <td valign="top" align="right">'.$postSearchAry['szCurrency']." ".number_format((float)$fTotalPrivateCustomerFeeApplicable,2).'</td>
                </tr>
                <tr>
                    <td colspan="4" align="left">&nbsp;</td>
                </tr>
            ';   
        }  
    } 
    if($searchResults['markUpByRate']<0)
    {
        $szMarkupRateStr = "(".$searchResults['szCurrency']." ".number_format((float)$searchResults['fCourierPrductMarkupForwarderCurrency'],2).")";
    }
    else
    {
        $szMarkupRateStr = $searchResults['szCurrency']." ".number_format((float)$searchResults['fCourierPrductMarkupForwarderCurrency'],2);
    }
    if($searchResults['fMarkupperShipment']<0)
    {
        $searchResults['fMarkupperShipment'] = $searchResults['fMarkupperShipment'] * (-1);
        $szAdditionalMarkupRateStr = "(".$searchResults['szForworderCurrency']." ".number_format((float)$searchResults['fMarkupperShipment'],2).")";
    }
    else
    {
        $szAdditionalMarkupRateStr = $searchResults['szForworderCurrency']." ".number_format((float)$searchResults['fMarkupperShipment'],2);
    } 
    
    $fReferralPercentage = $searchResults['fReferalPercentage'];
    $fReferralFeeAmount = round((float)($fTotalSelfInvoiceAmount * $fReferralPercentage * 0.01),2);
    $fTotalSelfInvoiceAmountNetPayable = $fTotalSelfInvoiceAmount - $fReferralFeeAmount;
    ?>  
    <table cellpadding="3" cellspacing="0" border="0" width="100%"> 
        <tr class="td-border-bottom">
            <th style="width:55%;" align="left"><?php echo $searchResults['szCarrierCompanyName']. " - ".$searchResults['szCourierProductName']; ?></th>
            <th style="width:15%;" align="right">Price</th>
            <th style="width:15%;" align="right">ROE</th>
            <th style="width:15%;" align="right">Price</th>
        </tr>
        <tr>
            <td align="left">Buy rate returned from <?php echo $searchResults['szCarrierCompanyName']; ?></td>
            <td align="right"><?php echo "USD ".number_format((float)$searchResults['fShipmentAPIUSDCost'],2); ?></td>
            <td align="right"><?php echo ($searchResults['fForwarderCurrencyExchangeRate']>0)?number_format((float)(1/$searchResults['fForwarderCurrencyExchangeRate']),4):0.0000; ?></td>
            <td align="right"><?php echo $searchResults['szForworderCurrency']." ".number_format((float)$searchResults['fApiPriceForwarderCurrency'],2); ?></td>
        </tr>
        <tr>
            <td colspan="4" align="left">&nbsp;</td>
        </tr>
        <?php if((float)$searchResults['fCourierPrductMarkupForwarderCurrency']!=0){ ?>
            <tr>
                <td align="left">Markup on buy rate</td>
                <td align="right"><?php echo $szMarkupRateStr; ?></td>
                <td align="right"><?php echo "1.0000"; ?></td>
                <td align="right"><?php echo $szMarkupRateStr; ?></td>
            </tr>
            <tr>
                <td colspan="4" align="left"><span class="grey-text">Calculation: max(<?php echo round($searchResults['fMarkupPercent'],2)."% x ".$searchResults['szForworderCurrency']." ".number_format((float)$searchResults['fApiPriceForwarderCurrency'],2)." ; ".$searchResults['szAdditionalMarkupCurrency']." ".number_format((float)$searchResults['fMinimumMarkup']); ?>)</span></td>
            </tr>
        <?php } ?>
        <?php if((float)$searchResults['fMarkupperShipment']!=0){ ?>
            <tr>
                <td align="left">Additional markup</td>
                <td align="right"><?php echo $szAdditionalMarkupRateStr; ?></td>
                <td align="right"><?php echo "1.0000"; ?></td>
                <td align="right"><?php echo $szAdditionalMarkupRateStr; ?></td>
            </tr>
            <tr>
                <td colspan="4" align="left">&nbsp;</td>
            </tr>
        <?php } ?>
        <?php echo $szPrivateCustomerFeeDetailsString; ?> 
        <tr class="td-border-top">
            <td align="left">Price paid by customer</td>
            <td></td>
            <td></td>
            <td align="right"><?php echo $searchResults['szForwarderCurrency']." ".number_format((float)$fTotalSelfInvoiceAmount,2)?></td>
        </tr>   
        <tr>
            <td colspan="4" align="left">&nbsp;</td>
        </tr>
        <tr>
            <td align="left">Referral fee for Transporteca</td>
            <td></td>
            <td></td>
            <td align="right"><?php echo $searchResults['szForwarderCurrency']." ".number_format((float)$fReferralFeeAmount,2)?></td>
        </tr>
        <tr>
            <td colspan="4" align="left" class="color f-size-12" valign="top"><em>Calculation: <?php echo $searchResults['szForwarderCurrency']." ".number_format((float)$fTotalSelfInvoiceAmount,2)?> x <?php echo $fReferralPercentage."%" ?></em></td>
        </tr>
        <tr class="td-border-top">
            <td align="left">Net payment transferred to you</td>
            <td></td>
            <td></td>
            <td align="right"><?php echo $searchResults['szForwarderCurrency']." ".number_format((float)$fTotalSelfInvoiceAmountNetPayable,2)?></td>
        </tr>
    </table> 
    <?php 
}
?>