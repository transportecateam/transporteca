<?php
class I18n
{
	static $language_order = array();	
	static $translations = array();
        static $translationsManagement = array();
	
	static function add_language($lang,$managementFlag=false)
	{
		if (empty($lang))
			return false;
		
		if (!in_array($lang, self::$language_order))
		{
			try
			{
				self::load_translations($lang,$managementFlag);
				array_push(self::$language_order, $lang);
				return true;
			}
			catch (Exception $e)
			{
				// TODO: log file not found
				return false;
			}
		}
		
		return false;
	}	
	
	static function load_translations($lang,$managementFlag=false)
	{
		//echo $lang;
		$translations = Horde_Yaml::loadFile(__APP_PATH__.'/translations/' . $lang . '.yml');
                //print_r($translations);
                //echo "<br /><br />";
                if($managementFlag)
                {
                    $translationsManagement = Horde_Yaml::loadFile(__APP_PATH__.'/translations/manageForwarder.yml');
                    //print_r($translationsManagement);
                    $translations=  array_merge($translations,$translationsManagement);
                    
                }
		self::$translations[$lang] = $translations;
	}
	
	static function translate()
	{
		if (func_num_args() < 1)
			return NULL;
		
		$args = func_get_args();
		$path = array_shift($args);
		
		$string = self::get_translation_string($path);
		
		if (empty($string))
		{
			if (is_array($path))
				$path = join(', ', $path);
			return '**' . $path . '**' . NULL;
		}
		elseif(count($args) == 0)
			return $string;
		else
			return vsprintf($string, $args);
	}	
	
	private static function get_translation_string($path)
	{
		$paths = array();
		if (is_array($path))
		{
			foreach($path as $p)
			{
				$paths[] = explode('/', $p);
			}
		}
		else
			$paths[] = explode('/', $path);
		
		foreach (self::$language_order as $lang)
		{
			foreach ($paths as $path)
			{
				$current = self::$translations[$lang];
				
				if (is_null($current))
					break;
				
				foreach ($path as $part)
				{
					if (!is_array($current))
					{
						$current = NULL;
						break;
					}
					
					$current = $current[$part];
					
					if (is_null($current))
						break;
				}
				
				if (!is_null($current) && !is_array($current))
					return $current;
			}
		}
		return NULL;
	}	
}
?>
