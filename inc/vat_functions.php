<?php
if( !defined( "__APP_PATH__" ) )
  define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");

function forwarderCountriesHTML($kVatApplication,$idCountryForwarder=0)
{
	$getForwarderCountryArr=$kVatApplication->getForwarderCountryData();
	$t_base ="management/vatApplication/"
?>
<script type="text/javascript">

        $(function () {
            $('#forwarder_country_table').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]','#contactPopup','#popup-container'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    var szCountryName='';
                    var idCountryForwarder='';
                    $("#forwarder_country_table .selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        var tr_id_ary = tr_id.split("____");
                        //alert(tr_id_ary);
                        idCountryForwarder = parseInt(tr_id_ary[1]);
                        szCountryName = tr_id_ary[2];
                        
                        if(!isNaN(idCountryForwarder))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                        
                    });  
                    if(res_ary_new.length)
                    {
                        if(res_ary_new.length==1)
                        {
                            $("#copy_forwarder_country_trade_data").unbind("click");
                            $("#copy_forwarder_country_trade_data").click(function(){copyForwarderCourtyData(idCountryForwarder,szCountryName);});
                            $("#copy_forwarder_country_trade_data").attr("style",'opacity:1');
                            $("#idCountryTradeForwarderName").attr('value',szCountryName);
                            $("#idCountryTradeForwarder").attr('value',idCountryForwarder);
                            //alert(res_ary_new.length);
                            if(res_ary_new.length==1)
                            {
                                selectForwarderCountryData(idCountryForwarder,szCountryName,'');
                            }
                        }
                        else
                        {
                             $("#copy_forwarder_country_trade_data").unbind("click");
                            $("#copy_forwarder_country_trade_data").attr("style",'opacity:0.4');
                            $("#idCountryTradeForwarderName").attr('value','');   
                            $("#idCountryTradeForwarder").attr('value',0);
                            if(res_ary_new.length==2)
                            {
                              selectForwarderCountryData('0','','');
                            }
                        }
                       var szCountryForwardersIds = res_ary_new.join(';'); 
                       $("#deleteCountryForwardersId").attr('value',szCountryForwardersIds);
                        $("#detete_forwarder_country_data").unbind("click");
                        $("#detete_forwarder_country_data").click(function(){deleteForwarderCourtyData();});
                        $("#detete_forwarder_country_data").attr("style",'opacity:1'); 
                    }
                    else
                    {
                        var szCountryForwardersIds = '';
                        $("#deleteCountryForwardersId").attr('value',szCountryForwardersIds);
                        $("#detete_forwarder_country_data").unbind("click");
                        $("#detete_forwarder_country_data").attr("style",'opacity:0.4');
                        
                        $("#copy_forwarder_country_trade_data").unbind("click");
                        $("#copy_forwarder_country_trade_data").attr("style",'opacity:0.4');
                        $("#idCountryTradeForwarderName").attr('value','');  
                        // selectForwarderCountryData('0','','');
                        $("#idCountryTradeForwarder").attr('value',0);
                    } 
                }
            });
        });
    
</script>
	<div id="insurance_couier_provider_list_container">  
        <div>
            
        	<h4><strong><?php echo t($t_base.'title/forwarder_country');?></strong></h4>
        		<div class="tableScroll" style="height:350px;">
        		<table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" style="float:left;" id="forwarder_country_table">
                	<tr id="table-header">
                		<th class="noborder" width="33%" align="left"><strong><?php echo t($t_base.'fields/country');?></strong></th>
                		<th width="33%" align="left"><strong><?php echo t($t_base.'fields/trades');?><strong></th>
                		<th width="33%" style="text-align:right;"><strong><?php echo t($t_base.'fields/vat');?></strong></th>
                	</tr>
                
            		<?php 
            		if(!empty($getForwarderCountryArr))
            		{
            			foreach($getForwarderCountryArr as $key=>$getForwarderCountryArrs)
            			{
            				$trades=count($getForwarderCountryArrs);
            				if($getForwarderCountryArrs[0]['idCountryFrom']==0)
            				{
            					$trades=0;
            				}
            				$textColor='';
            				$flag=0;
            				$seletedClass="selected-row";
            				if((int)$trades>0 && (float)$getForwarderCountryArrs[0]['fVATRate']<='0.00')
            				{
            					$textColor="style='color:red;'";
            					$seletedClass="selected-row-red";
            					$flag=1;
            				}
            		?>	
                	<tr <?php echo $textColor;?>  id="sel_forwarder____<?php echo $key;?>____<?php echo $getForwarderCountryArrs[0]['szForwaderCountryName'];?>" <?php if($idCountryForwarder==$key){?> class="<?php echo $seletedClass?>" <?php }?>>
                		<td class="noborder" width="33%" align="left"><?php echo $getForwarderCountryArrs[0]['szForwaderCountryName'];?></td>
                		<td width="33%" align="left"><?php echo $trades;?></td>
                		<td width="33%" style="text-align:right;"><?php echo number_format((float)$getForwarderCountryArrs[0]['fVATRate'],2)."%";?></td>
                	</tr>
                	<?php }
							
		}?>
                </table>
              	</div>
          </div>
            <div class="clear-all"></div>
            <br>
          <div id="left_form_div">
          <?php echo addEditForwarderCountryHtml($kVatApplication);?>
          
          </div>
       </div>         	
           
<?php
}

function addEditForwarderCountryHtml($kVatApplication)
{
	$kConfig = new cConfig();
	$kCourierServices= new cCourierServices();
	$regionArr=$kCourierServices->getAllRegion();
	$getAllCountry=$kConfig->getAllCountries(true);
	
	$t_base ="management/vatApplication/";
	
	if(!empty($kVatApplication->arErrorMessages))
        {
            $szValidationErrorKey = '';
            foreach($kVatApplication->arErrorMessages as $errorKey=>$errorValue)
            {
                    ?>
                    <script type="text/javascript">
                            $("#<?php echo $errorKey?>").addClass('red_border');
                    </script>
                <?php 
            }
        }
        $alreadYAdded=array();
        $getForwarderCountryArr=$kVatApplication->getForwarderCountryData();
        if(!empty($getForwarderCountryArr))
        {
                foreach($getForwarderCountryArr as $key=>$getForwarderCountryArrs)
                {
                        $alreadYAdded[]=$key;
                }
        }
       // print_r($alreadYAdded);
	?>
	<form action="javascript:void(0);" name="forwarderCountryForm" method="post" id="forwarderCountryForm">
		<table cellpadding="0" cellspacing="1" width="100%">
			<tr>
                <td colspan="2" align="left" class="f-size-12"><?php echo t($t_base.'fields/country');?></td>
            </tr>
            <tr>
            	<td width="39%"><select style="width:100%" id="idCountryForwarder" name="addForwarderCountryAry[idCountryForwarder]" onchange="checkForwarderCountrySeleted();">
                        <option value=''>Select</option>
                        <?php 
                        if(!empty($regionArr))
                        {
                                foreach($regionArr as $regionArrs)
                                {
                                        $regionValue=$regionArrs['id']."_r";
                                ?>
                                        <option  value='<?php echo $regionValue?>' <?php if($_POST['addForwarderCountryAry']['idCountryForwarder']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                <?php
                                }
                        }?>
                        <?php 
                        if(!empty($getAllCountry))
                        {
                                foreach($getAllCountry as $getAllCountrys)
                                {
                                    $showFlag=true;
                                    if(!empty($alreadYAdded) && in_array($getAllCountrys['id'],$alreadYAdded))
                                    {
                                        $showFlag=false;
                                    }
                                    if($showFlag)
                                    {
                                ?>
                                        <option  value='<?php echo $getAllCountrys['id']?>' <?php if($_POST['addForwarderCountryAry']['idCountryForwarder']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName'];?></option>
                                <?php
                                    }
                                }
                        }?>
                </select></td>
            </tr>
            <tr>
                <td style="text-align:right;padding-top:4px;">  <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_forwarder_country_data"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                         <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_forwarder_country_data" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/delete');?></span></a>
                  </td>
            </tr>    	
		</table>
	
	</form>
	<?php	
}

function forwarderVatTradesHTML($kVatApplication,$szSortField="cform.szCountryName",$szSortOrder="ASC")
{
    $t_base ="management/vatApplication/"; 
    $getForwarderCountryArr=$kVatApplication->getForwarderCountryData($szSortField,$szSortOrder); 
    
    $szFromCountrySorter = "";
    $szToCountrySorter = "";
    
    $szFromSortOrder = "ASC";
    $szToSortOrder = "ASC";
    if($szSortField=='cform.szCountryName')
    {
        if($szSortOrder=='DESC')
        {
            $szFromCountrySorter = "sort-arrow-up";
        }
        else
        {
            $szFromCountrySorter = "sort-arrow-down";
            $szFromSortOrder = "DESC";
        } 
    }
    else
    {
        if($szSortOrder=='DESC')
        {
            $szToCountrySorter = "sort-arrow-up";
        }
        else
        {
            $szToCountrySorter = "sort-arrow-down";
            $szToSortOrder = "DESC";
        } 
    }
?>
    <script type="text/javascript">

        $(function () {
            $('#forwarder_country_trades').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    var szCountryName='';
                    var idCountryForwarder='';
                    $("#forwarder_country_trades .selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        var tr_id_ary = tr_id.split("____");
                        //alert(tr_id_ary);
                        idCountryForwarder = parseInt(tr_id_ary[1]);
                        szCountryName = tr_id_ary[2]; 
                        if(!isNaN(idCountryForwarder))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                    });  
                    
                    if(res_ary_new.length)
                    { 
                       var szCountryForwardersIds = res_ary_new.join(';'); 
                       $("#deleteCountryForwardersTradesId").attr('value',szCountryForwardersIds);
                        $("#detete_forwarder_country_trade_data").unbind("click");
                        $("#detete_forwarder_country_trade_data").click(function(){deleteForwarderCourtyTradeData(szCountryName);});
                        $("#detete_forwarder_country_trade_data").attr("style",'opacity:1'); 
                    }
                    else
                    {
                        var szCountryForwardersIds = '';
                        $("#deleteCountryForwardersTradesId").attr('value',szCountryForwardersIds);
                        $("#detete_forwarder_country_trade_data").unbind("click");
                        $("#detete_forwarder_country_trade_data").attr("style",'opacity:0.4'); 
                    } 
                }
            });
        })
    
</script>
<div id="insurance_couier_provider_list_container">  
    <div>
        <h4><strong><?php echo t($t_base.'title/vat_will_be_applied_for_these_trades');?></strong></h4>
        <p style="padding-bottom: 10px;padding-top:5px;"><?php echo t($t_base.'messages/vat_application_screen_subheading');?></p>
        <div class="tableScroll" style="height:350px;">
            <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" style="float:left;" id="forwarder_country_trades">
                <tr id="table-header" style="cursor:pointer;">
                    <th class="noborder" width="49%" align="left" onclick="sort_vat_application('cform.szCountryName','<?php echo $szFromSortOrder; ?>')"><strong><?php echo t($t_base.'fields/from_country');?></strong><span id="vat_application_header_szCountryFrom" class="vat-application-sorter <?php echo $szFromCountrySorter; ?>">&nbsp;</span></th>
                    <th width="49%" align="left" onclick="sort_vat_application('cto.szCountryName','<?php echo $szToSortOrder; ?>')"><strong><?php echo t($t_base.'fields/to_country');?></strong><span id="vat_application_header_szCountryTo" class="vat-application-sorter <?php echo $szToCountrySorter; ?>">&nbsp;</span></th>
                </tr>
                <?php
                    if(!empty($getForwarderCountryArr))
                    {
                        foreach($getForwarderCountryArr as $getForwarderCountryArrs)
                        {
                          // $szCountryListJsonStr .= $getForwarderCountryArrs['idCountryFrom']."_".$getForwarderCountryArrs['idCountryTo'].";";
                ?>	
                        <tr id="vat_trade____<?php echo $getForwarderCountryArrs['id'];?>">
                            <td class="noborder" align="left"><?php echo $getForwarderCountryArrs['szFromCountryName'];?></td>
                            <td align="left"><?php echo $getForwarderCountryArrs['szToCountryName'];?></td>
                        </tr>
                <?php  
                       }
                    } 
                ?>
            </table>
        </div>
    </div>
    <input type="hidden" name="szVatApplicationSort" id="szVatApplicationSort" value="ASC">
    <div class="clear-all"></div>
    <br>
    <div id="right_form_div">
        <?php echo addEditForwarderTradeCountryHtml($kVatApplication,$idCountryForwarder);?> 
    </div>
</div>	
<?php 
}
function addEditForwarderTradeCountryHtml($kVatApplication,$idCountryForwarder)
{
    $kConfig = new cConfig();
    $kCourierServices= new cCourierServices();
    $regionArr=$kCourierServices->getAllRegion();
    $getAllCountry=$kConfig->getAllCountries(true);

    $t_base ="management/vatApplication/";

    if(!empty($kVatApplication->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kVatApplication->arErrorMessages as $errorKey=>$errorValue)
        {
            ?>
            <script type="text/javascript">
                $("#<?php echo $errorKey?>").addClass('red_border');
            </script>
            <?php 
        }
    } 
    ?>
    <form action="javascript:void(0);" name="forwarderCountryTradeForm" method="post" id="forwarderCountryTradeForm">
        <table cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td  align="left" class="f-size-12"><?php echo t($t_base.'fields/from_country');?></td>
                <td  align="left" class="f-size-12"><?php echo t($t_base.'fields/to_country');?></td>
            </tr>
            <tr>
            	<td width="50%">
                    <select <?php echo $disableFlag?> style="width:100%" id="idCountryForm" name="addForwarderCountryTradeAry[idCountryForm]" onchange="activeTradeFromToButton();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                        ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addForwarderCountryTradeAry']['idCountryForm']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                        <?php
                                }
                            }
                        ?>
                	<?php 
                            if(!empty($getAllCountry))
                            {
                                foreach($getAllCountry as $getAllCountrys)
                                {
                        ?>
                                    <option  value='<?php echo $getAllCountrys['id']?>' <?php if($_POST['addForwarderCountryTradeAry']['idCountryForm']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName'];?></option>
                        <?php
                                }
                            }
                        ?>
                    </select>
                </td>
                <td width="50%">
                    <select <?php echo $disableFlag?> style="width:100%" id="idToCountry" name="addForwarderCountryTradeAry[idToCountry]" onchange="activeTradeFromToButton();">
                        <option value=''>Select</option>
                	<?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                        ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addForwarderCountryTradeAry']['idToCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                        <?php
                                }
                            }
                        ?>
                        <?php 
                            if(!empty($getAllCountry))
                            {
                                foreach($getAllCountry as $getAllCountrys)
                                {
                        ?>
                                    <option  value='<?php echo $getAllCountrys['id']?>' <?php if($_POST['addForwarderCountryTradeAry']['idToCountry']==$getAllCountrys['id']){?> selected <?php }?>><?php echo $getAllCountrys['szCountryName'];?></option>
                        <?php
                                }
                            }
                        ?>
                    </select> 
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:right;padding-top:4px;">  <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_forwarder_country_trade_data"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
                    <!--<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="copy_forwarder_country_trade_data"><span style="min-width:50px;"><?=t($t_base.'fields/copy');?></span></a>-->
                    <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="detete_forwarder_country_trade_data" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/delete');?></span></a>
                 </td>
            </tr>    	
        </table> 
    </form>
	<?php	
}
function copyVatApplicationData($id,$szCountryName,$kVatApplication)
{

	$getForwarderCountryArr=$kVatApplication->getForwarderCountryData();
	if(!empty($getForwarderCountryArr))
    {
    	foreach($getForwarderCountryArr as $key=>$getForwarderCountryArrs)
        {
        	$trades=count($getForwarderCountryArrs);
            if($getForwarderCountryArrs[0]['idCountryFrom']==0)
            {
            	$trades=0;
            }
            $textColor='';
            if((int)$trades>0)
            {
            	$countryArrTrades[]=$getForwarderCountryArrs[0];
            }
        }
    }	
	$t_base ="management/vatApplication/";
	?>
	<div id="popup-bg"></div>
		<div id="popup-container" >
		<div class="popup">
		<h3><?=t($t_base.'title/copy_vat_application');?> </h3>
		<p><?=t($t_base.'messages/copy_msg1');?></p><br/>
		<p>
		<select style="width:70%" id="idCountry" name="idCountry" onchange="activateCopyButton(this.value,'<?php echo $id?>','<?php echo $szCountryName;?>');">
		<option value="">Select</option>
		<?php 
		if(!empty($countryArrTrades))
		{
			foreach($countryArrTrades as $countryArrTrade)
			{   
                            if($id!=$countryArrTrade['idCountryForwarder'])
                            {
                            ?>	
				<option value="<?php echo $countryArrTrade['idCountryForwarder']?>" <?php if($_POST['idCountry']==$countryArrTrade['idCountryForwarder']){?> selected <?php }?>><?php echo $countryArrTrade['szForwaderCountryName'];?></option>
			<?php
                            }
			}
		}
		?>
		</select>
		</p><br/>
		<p><?=t($t_base.'messages/copy_msg2')." ".$szCountryName;?></p>
		<br/>
		<p align="center">
			<a href="javascript:void(0)" style="opacity:0.4" class="button1" id="copy_button"><span><?=t($t_base.'fields/copy');?></span></a> 
			<a href="javascript:void(0)" id="copy_cancel_button" class="button1" onclick="showHide('contactPopup')"><span><?=t($t_base.'fields/cancel');?></span></a>
		</p>
	</div>
</div>
	<?php
}
function getForwarderCourierInvoice($idForwarder,$batchNumber,$flag='',$get_merege_flag=false)
{
        require_once(__APP_PATH_ROOT__.'/forwarders/html2pdf/html2pdfBooking.php'); 
	$version="Forwarder - Transporteca Invoices".$idForwarder;
	$kForwarder= new cForwarder();
	$kBooking = new cBooking();
	$addDetail=$kForwarder->detailsForwarder($idForwarder);
	//$emailDetails=$kForwarder->detailsForwarderBillingEmail($idForwarder);
	$emailDetails=$kForwarder->detailsForwarderBillingGeneralEmail($idForwarder);
        
        $getBatchNumber = $kBooking->getBactchNumberByInvoiceNumber($batchNumber);
        //print_r($getBatchNumber);
        $invoiceBillConfirmed1 = $kBooking->invoicesBilling($idForwarder,2,'',$getBatchNumber);
	//print_r($invoiceBillConfirmed1);
        //die();
	//$date=date("Y-m-d H:i:s"); 
	if($invoiceBillConfirmed1)
	{	
            $data='';
            foreach($invoiceBillConfirmed1 as $inv)
            {
                if($inv['iDebitCredit']==8)
                {
                   // echo $inv['fTotalPriceForwarderCurrency']."<br/>";
                    if($inv['fTotalPriceForwarderCurrency']<0)
                    {
                        $array = "(".convertUTF($inv['szCurrency'])." ".($inv['fTotalPriceForwarderCurrency']?number_format((float)abs($inv['fTotalPriceForwarderCurrency']),2):'').")";
                    }
                    else
                    {
                        $array =convertUTF($inv['szCurrency'])." ".($inv['fTotalPriceForwarderCurrency']?number_format((float)$inv['fTotalPriceForwarderCurrency'],2):'');
                    } 
                    $to=$inv['dtPaymentConfirmed'];
                    $ref=$inv['szInvoice'];
                }
                
                if(strtotime($date)>strtotime($inv['dtCreatedOn']))
                {
                    $date=$inv['dtCreatedOn'];
                }
            }
        } 
       // print_r($array);
        //die();
        $fTotalInvoiceAmountPaid = 0;
	$invoiceBillConfirmed = $kBooking->invoicesBilling($idForwarder,2,'',$getBatchNumber);
         //print_r($invoiceBillConfirmed);
         //die();
	$date=date("Y-m-d H:i:s"); 
	if($invoiceBillConfirmed)
	{	
            $data='';
            foreach($invoiceBillConfirmed as $inv)
            {	 
                if(($inv['iDebitCredit']==1 || $inv['iDebitCredit']==6) &&  $inv['fLabelFeeUSD']!='0.00')
                { 
                    if(($inv['iDebitCredit']==6) || ($inv['iDebitCredit']==1))
                    { 
                        $idBooking = $inv['idBooking'] ;
                        $postSearchAry = array();
                        $postSearchAry = $kBooking->getBookingDetailsCountryName($idBooking); 
                        //print_r($postSearchAry);
                        $kCourierServices = new cCourierServices();
                        $courierBookingArr=$kCourierServices->getCourierBookingData($idBooking);
                      // print_r($courierBookingArr);                                
                        $szServiceProviderName = $courierBookingArr[0]['szServiceProviderName'] ;
                        $szTrackingNumber=$postSearchAry['szTrackingNumber'];
                        if(!empty($postSearchAry['szForwarderReference']))
                        {
                            $szForwarderReference = $postSearchAry['szForwarderReference'] ;
                        }
                        else
                        {
                            $szBookingRefNo=explode(":",$inv['szBooking']);
                            $szForwarderReference = $szBookingRefNo[1];
                        } 
                        
                        $bookingLabelFee=0;    
                        $fBookingLabelFeeRate=$inv['fLabelFeeUSD'];
                        //echo $fBookingLabelFeeRate."fBookingLabelFeeRate<br/>";
                        $fBookingLabelFeeRateROE=$courierBookingArr[0]['fBookingLabelFeeROE'];
                        $fBookingLabelFeeCurrencyId=$courierBookingArr[0]['idBookingLabelFeeCurrency'];
                        if($fBookingLabelFeeCurrencyId==$inv['idForwarderCurrency'])
                        {
                            $forwarderExchangeRate='1.0000';
                            if($fBookingLabelFeeCurrencyId!='1')
                            {
                                $forwarderExchangeRate1=$courierBookingArr[0]['fBookingLabelFeeROE'];
                            }
                           $bookingLabelFee=$fBookingLabelFeeRate/$forwarderExchangeRate1;
                            //$bookingLabelFee=$courierBookingArr[0]['fBookingLabelFeeRate'];
                            //$forwarderExchangeRate='1.000';
                            //echo "helooo";
                        }
                        else
                        {
                              //echo "helooo222";
                            $forwarderExchangeRate1=$postSearchAry['fForwarderExchangeRate'];
                            $forwarderExchangeRate=$courierBookingArr[0]['fBookingLabelFeeROE']/$postSearchAry['fForwarderExchangeRate'];
                            //echo $forwarderExchangeRate1;
                            /*if($courierBookingArr[0]['idBookingLabelFeeCurrency']=='1')
                            {
                                $forwarderExchangeRate=$courierBookingArr[0]['fBookingLabelFeeROE'];
                            }
                            else
                            {
                                $forwarderExchangeRate=$inv['fBookingLabelFeeROE'];
                            }    */

                              $bookingLabelFee=$fBookingLabelFeeRate/$forwarderExchangeRate1;
                              
                        }
                       // echo $bookingLabelFee."bookingLabelFee<br/>";
                        $data.="
                            <tr>
                                <td valign='top'>".date('d F Y', strtotime($inv['dtPaymentConfirmed']))."</td>
                                <td valign='top'>".convertUTF($szForwarderReference)."</td>
                                <td valign='top'>".convertUTF($szServiceProviderName)."</td>
                                <td valign='top'>".convertUTF($szTrackingNumber)."</td>";
                                if($inv['iDebitCredit']==6)
                                    $data.="<td align='right' valign='top'>(".convertUTF($courierBookingArr[0]['szBookingLabelFeeCurrency'])." ".convertUTF(number_format($courierBookingArr[0]['fBookingLabelFeeRate'],2)).")</td>";
                                else
                                    $data.="<td align='right' valign='top'>".convertUTF($courierBookingArr[0]['szBookingLabelFeeCurrency'])." ".convertUTF(number_format($courierBookingArr[0]['fBookingLabelFeeRate'],2))."</td>";

                                $data.="<td align='right' valign='top'>".number_format((float)$forwarderExchangeRate,4)."</td>";
                                if($inv['iDebitCredit']==6)
                                    $data.="<td align='right' valign='top'>(".convertUTF($inv['szForwarderCurrency'])." ".number_format($bookingLabelFee,2).")</td>";
                                else
                                    $data.="<td align='right' valign='top'>".convertUTF($inv['szForwarderCurrency'])." ".number_format($bookingLabelFee,2)."</td>";
                            $data .= "</tr>";
                            $curency=$inv['szForwarderCurrency'];
                            $fTotalInvoiceAmountPaid +=  $bookingLabelFee ;
                        }
                    } 
                    if(strtotime($date)>strtotime($inv['dtCreatedOn']))
                    {
                             $date=$inv['dtCreatedOn'];
                    } 
		}
               //die();
	
	}
        
		$datefrom=($date!='0000-00-00 00:00:00')?date('d F Y',strtotime($date)):'';
		$timefrom=($date!='0000-00-00 00:00:00')?date('H:i',strtotime($date)):'';
		$dateto=($to!='0000-00-00 00:00:00')?date('d F Y',strtotime($to)):'';
		$timeto=($to!='0000-00-00 00:00:00')?date('H:i',strtotime($to)):'';
		$add='';
		if($addDetail['szCompanyName'])
		{
			$add.="<br />".$addDetail['szCompanyName']."<br />";
		}
		if($addDetail['szAddress'])
		{
			$add.=$addDetail['szAddress']."<br />";
		}
		if($addDetail['szAddress2'])
		{
			$add.=$addDetail['szAddress2']."<br />";
		}
		if($addDetail['szAddress3'])
		{
			$add.=$addDetail['szAddress3']."<br />";
		}
		if($addDetail['szCity'])
		{
			$add.=$addDetail['szCity'];
		}
		if($addDetail['szCity'] && $addDetail['szState'])
		{
			$add.=' , ';
		}
		if($addDetail['szState'])
		{
			$add.=$addDetail['szState'];
		}
		if($addDetail['szCountryName'])
		{
			$add.="<br />".$addDetail['szCountryName'];
		}
		if($emailDetails)
		{	
			$num=count($emailDetails);
			foreach($emailDetails as $key=>$value)
			{
				$emailLog.=$value['szGeneralEmailAddress'];
				$num--;
				if($num>1)
				{
					$emailLog.= ", ";
				}
				else if($num==1)
				{
					$emailLog.= ' and ';
				}
			}
		}
  		$link=$addDetail['szControlPanelUrl'];
  
	
  //$pdf->Image('invoice.jpg',120,12,0);
  //$array=explode(',',$array);
                
                //echo $data;
                //die;
  if($flag!='PDF')
  {
  	$body_info='style="background:#000;text-align:center;font-size:14px;"';
  	$image_url=__MAIN_SITE_HOME_PAGE_URL__;
  	$dataview='<p style="text-align:right;margin:5px auto 10px;width:690px;">
			<a target="_blank" href='.__BASE_URL__.'/downloadForwarderInvoice/'.$idForwarder.'/'.$batchNumber.'/ style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Download PDF</a>
			<a onclick="PrintDiv();" href="javascript:void(0)" style="color: #fff;font-style:italic;font-size:16px;font-weight:bold;font-family:Cambria;">Print</a>
			</p>';
  	$script='<script>
			function PrintDiv()
			{    
                            var divToPrint = document.getElementById("forwarderTransfer");
                            var popupWin = window.open("", "_blank", "width=900,height=900");
                            popupWin.document.open();
                            popupWin.document.write("<html><body onload=window.print()>" + divToPrint.innerHTML + "</html>");
                            popupWin.document.close();
			}
			</script>';
  }
  else
  {
  	$image_url=__APP_PATH_ROOT__;
  	$dataview='';
  	$script='';
  }
  	  $contents = '<?xml version="1.0" encoding="iso-8859-1"?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>Transporteca Invoice</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		'.$script.'
		</head>
		<body '.$body_info.'>'.$dataview.'
                    <div id="forwarderTransfer" style="background:#fff;margin:auto;padding:10px 20px;width:650px;text-align:left;min-height:500px;" >
                        <table  border="0" cellspacing="0" cellpadding="0" width="650px">
                            <tr style="margin-bottom:12px;">
                                <td colspan="2" valign="top" width="312px" align="left">
                                        <img src='.$image_url.'/images/TransportecaMail.jpg height="53px" width="217px">
                                </td> 
                                <td colspan="2" valign="middle" width="40%" align="right">
                                    <img src='.$image_url.'/images/HeadingInvoice.jpg width="150" hieght="30"> 
                                </td>	
                            </tr>
                        </table>
                        <br />
                        <table  border="0" cellspacing="0" cellpadding="1" width="650px">
                            <tr>
                                <td colspan="2" valign="top" width="52%" align="left" style="border:solid 1px #000;" border="1">
                                    <b>Invoice Date: '.$dateto.'</b>
                                </td>
                                <td colspan="2" valign="middle" width="48%" align="left" style="border:solid 1px #000;" border="1">
                                    <b>Invoice Number:  '.$ref.'</b>
                                </td>
                            </tr>
                        </table>
                        <br />						
                        <table  border="0" cellspacing="0" cellpadding="0" width="650px">
                            <tr> 
                                <td valign="top" width="52%">
                                    <strong>From:</strong>
                                    <strong></strong>
                                    TRANSPORTECA_ADDRESS
                                </td>
                                <td valign="top" width="48%" >
                                    <strong>To:</strong>
                                    TO_TRANSPORTECA_ADD
                                    <br />E-mail: BILLING_ADD_EMAIL
                                </td>
                            </tr>
                        </table>
                        <br /><br />				
                        <table width="650px" border="1" cellspacing="0" cellpadding="3" align="left">
                                <tbody>
                                    <tr>
                                        <td colspan="7" valign="top" width="100%"> <strong>Service Description:</strong> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" valign="top" width="100%">Fee for creating bookings and labels for following courier bookings.<br /><br /></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" width="15%">Settlement Date</td>
                                        <td valign="top" width="15%">Booking</td>
                                        <td valign="top" width="10%">Courier</td> 
                                        <td valign="top" width="15%">Tracking no.</td>
                                        <td valign="top" width="14%" ALIGN="RIGHT">Rate</td>
                                        <td valign="top" width="6%" ALIGN="RIGHT">ROE</td>
                                        <td valign="top" width="15%" ALIGN="RIGHT">Label Fee</td>
                                    </tr>
                                        ALL_DETAILS
                                    <tr>
                                        <td colspan="6" valign="top" width="499px"><strong>Invoice Total</strong></td>
                                        <td valign="top" width="120px" ALIGN="RIGHT"><strong>INV_TOTAL</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" valign="top" width="499px"><strong>Withheld in next bank transfer</strong></td>
                                        <td valign="top" width="120px" ALIGN="RIGHT"><strong>TOTAL_TRANSFER_AMOUNT</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" valign="top" width="499px"><strong>Payment Due</strong></td>
                                        <td valign="top" width="120px" ALIGN="RIGHT"><strong>'.$curency.' 0.00</strong></td>
                                    </tr>
                                </tbody>
                        </table>
                        <div style="width:650px;">
                            <center><span style="TEXT-ALIGN:CENTER;font-size:small;">This invoice is available in soft copy on FORWARDER_LINK_URL under Billing.</span></center><br />
                            <center><span style="TEXT-ALIGN:CENTER;font-size:small;">Transporteca&rsquo;s Term &amp; Conditions are available and agreed on FORWARDER_LINK_URL.</span></center><br />
                            <center><span style="TEXT-ALIGN:CENTER;font-size:small;">This is an electronic statement and no signature is required.</span></center>
                        </div>
                    </div>	
                </body>
        </html>'; 
         // echo $array;
                $fTotalInvoiceReferalFee = number_format((float)$fTotalInvoiceAmountPaid,2); 
	  	  $contents = str_replace("TRANSPORTECA_ADDRESS", __TRANSPORTECA_ADDRESS__, $contents);
	  	  $contents = str_replace("TO_TRANSPORTECA_ADD", $add, $contents);
	  	  $contents = str_replace("BILLING_ADD_EMAIL", $emailLog, $contents);
	  	  $contents = str_replace("ALL_DETAILS", $data, $contents);
	  	  $contents = str_replace("FORWARDER_LINK_URL", $link, $contents);
		  $contents = str_replace("INV_TOTAL", $fTotalInvoiceReferalFee, $contents);	
		  $contents = str_replace("PAYMENT_TRANSFER", $fTotalInvoiceReferalFee, $contents);
		  $contents = str_replace("TOTAL_TRANSFER_AMOUNT", $fTotalInvoiceReferalFee, $contents);	
		  if($get_merege_flag)
                  {
                      return $contents;
                  }
                  else
                  {
		  if(empty($flag))
		  {
		  	echo $contents;
		  }
		  if(isset($flag) && $flag=="PDF")
		  {
		  
		  	  class transfer1 extends HTML2FPDFBOOKING
			  {
				  function Footer()
				  {
				  		 $this->SetY(-10);
					    //Copyright //especial para esta vers
					    $this->SetFont('Arial','B',9);
					  	$this->SetTextColor(0);
					    //Arial italic 9
					    $this->SetFont('Arial','B',9);
					    //Page number
					    $this->Cell(10,10,"Page ".$this->PageNo().' of {nb}',0,0,'C');
					    //Return Font to normal
					    $this->SetFont('Arial','',11);
				  }
			  }
			  $version="Courier-Booking-and-Labels-Invoice-".$batchNumber;
		  	  $filename=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";	
			  $pdf=new transfer1();
			  $pdf->AddPage();
			  $pdf->SetFontSize(12);
			  $pdf->SetTopMargin(5);
			  //$pdf->Rect(98,37,84,40);
			  $pdf->SetFont('Arial','',8);
			  $pdf->WriteHTML($contents);
			  
			$file_name=__APP_PATH_ROOT__."/forwarders/html2pdf/".$version.".pdf";
		  	//$file = "Booking-Invoice.pdf";
			if(file_exists($file_name))
			{
				@unlink($file_name);
			}
			  
			  $pdf->Output($file_name,'F');
			 return $filename;
		  }
                  }
}
?>