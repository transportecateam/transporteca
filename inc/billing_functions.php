<?php
if( !isset( $_SESSION ) )
{
    session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Ajay
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once(__APP_PATH__ . "/inc/constants.php");
require_once(__APP_PATH__ . "/inc/functions.php"); 
require_once(__APP_PATH__ . "/inc/pendingTray_functions.php"); 
require_once(__APP_PATH_CLASSES__ . "/database.class.php");
include_classes();


function showBillingDetails_new($t_base,$idForwarder,$dateArr,$bReturnData=false)
{	 
    $kBooking = new cBooking();
    $kForwarder = new cForwarder();
    $kBilling= new cBilling();
    $kConfig= new cConfig();
    $kAdmin = new cAdmin(); 
      
    $invoiceBillConfirmed = array();
    $billingHeaderValuesAry = array();
    
    $invoiceBillConfirmed = $kBilling->getInvoiceBillConfirmed($idForwarder,$dateArr);  
    $billingHeaderValuesAry = $kBilling->getBillingScreenHeadingValues($idForwarder);
    $iDisplayAstrix = $kBilling->iDisplayAstrix;
    $szAstrixText = "";
    if($iDisplayAstrix)
    {
        $szAstrixText = "<sup>*</sup>";
    }
    
    $currentAvailableAmountAry = array();  
    if(!empty($dateArr))
    {
        $billingDateAry = array();
        $billingDateAry['dtFromDate'] = $dateArr['0'];
        $billingDateAry['dtToDate'] = date('Y-m-d',strtotime($dateArr['1'])); 
        
        //for current balance//
        $currenctBalaceArr = array();
        $currenctBalaceArr = $kBilling->getNetBalancePerForwarder($idForwarder,$billingDateAry);
        
        if(!empty($currenctBalaceArr))
        {
            foreach($currenctBalaceArr as $szCurrency=>$currenctBalaceArrs)
            {
                if(!empty($szCurrency))
                {
                    $currentAvailableAmountAry[$szCurrency]['fTotalBalance'] = $currenctBalaceArrs['fTotalBalance'];
                }
            }
        }
    } 
    
    if(!empty($invoiceBillConfirmed))
    { 	 
        if($referalAndTransferFee)
        {
            $sum=(float)$total;
            $gtotal=$newCurrentBalanceString;
        }
        else
        {
            $sum=0;
            $gtotal="0.00";
        }

        $launchDate=strtotime('01-10-2012 00:00:00');		
        $ctr=1;
        $billingRowDataAry = array();
        foreach($invoiceBillConfirmed as $searchResults)
        {    
            $kBooking = new cBooking();
            $kBooking->load($searchResults['idBooking']);
            if($searchResults["dtPaymentConfirmed"]!='' && $searchResults["dtPaymentConfirmed"]!='0000-00-00 00:00:00')
            {	 
                /*
                * iDebitCredit = 1 Means, New booking received ot Transporteca
                * iDebitCredit = 13 means, Balance Adjustment i.e any amount that was added or substracted manually to make Financial screen's amount correct
                */
                if($searchResults["iFinancialVersion"]==__TRANSPORTECA_FINANCIAL_VERSION__ && !empty($searchResults['szSelfInvoice']))
                {
                    $searchResults["szInvoice"] = $searchResults['szSelfInvoice'];
                }
                if($searchResults['iDebitCredit']==1 || $searchResults['iDebitCredit']==13)
                {	
                    $idCurency = $searchResults['szForwarderCurrency'] ;

                    $i=1;
                    $currency=$searchResults['szForwarderCurrency'];
                    $minDate=strtotime($searchResults['dtCreditOn']);
                    if($minDate<$launchDate)
                    {
                        $launchDate = $minDate;
                    } 
                    $fTotalPriceForwarderCurrency = $searchResults['fTotalPriceForwarderCurrency']; 
                    $fTotalPriceForwarderCurrency_positive += $fTotalPriceForwarderCurrency;

                    /*
                    * Summing up Invoice value for the booking
                    */
                    if($searchResults['iDebitCredit']==13)
                    {
                        //echo "Curr bal: ".$currentAvailableAmountAry[$idCurency]['fTotalBalance']." Price: ".$fTotalPriceForwarderCurrency." ".$sumAry[$idCurency];
                        $sumAry[$idCurency] = (float)(round((float)$currentAvailableAmountAry[$idCurency]['fTotalBalance'],2) + $fTotalPriceForwarderCurrency);
                    }
                    else
                    {
                        $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] + $fTotalPriceForwarderCurrency);
                    }

                    $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];

                    $iDisplayTrOnlick = false;
                    $curr_sum='';
                    $szYourReference = ""; 
                    if($sumAry[$idCurency]>=0)
                    { 
                        $curr_sum = $searchResults["szForwarderCurrency"]." ".number_format((float)$sumAry[$idCurency],2);
                    }
                    else
                    { 
                        /*
                        * If value for Balance column is negative then we display value in small braces i.e ()
                        */
                        $newAmount = str_replace("-","",$sumAry[$idCurency]);
                        $curr_sum = "(".$searchResults["szForwarderCurrency"]." ".number_format((float)$newAmount,2).")";
                    }   
                    if($searchResults['iDebitCredit']==1)
                    {
                        $szYourReference = '<span id="invoice_comment_'.$searchResults['idBooking'].'">'.createSubString($kBooking->szInvoiceFwdRef,0,12).'</span><br />
                                         <a href="javascript:void(0)" onclick="update_invoice_comment(\''.$searchResults['idBooking'].'\',\''.$kBooking->szBookingRef.'\',\'booking\')" style="text-decoration:none;font-size:11px;"><img src="'.__BASE_STORE_IMAGE_URL__.'/Icon - Edit line.png" alt = "Click to update" style="float: right;margin-top:-17px;"></a>';
                        $iDisplayTrOnlick = 1; 
                    } 

                    $dtPaymentConfirmed = "";
                    if((!empty($searchResults["dtPaymentConfirmed"]) && $searchResults["dtPaymentConfirmed"]!="0000-00-00 00:00:00"))
                    {
                        $dtPaymentConfirmed = date("d-M",strtotime($searchResults["dtPaymentConfirmed"]));
                    }  
                    if($searchResults['fTotalPriceForwarderCurrency']>0)
                    {
                        $szInvoiceValue = $searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2);
                    }
                    else
                    {
                        $szInvoiceValue = "(".$searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")";
                    }
                    /*
                    * Building data to display Billing Rows
                    */
                    $billingRowData = array();
                    $billingRowData['dtPaymentConfirmed'] = $dtPaymentConfirmed;
                    $billingRowData['szBooking'] = $searchResults["szBooking"];
                    $billingRowData['szYourReference'] = $szYourReference;
                    $billingRowData['szInvoiceNumber'] = $searchResults["szInvoice"];
                    $billingRowData['szInvoiceValue'] = $szInvoiceValue;
                    $billingRowData['szCurrentBalance'] = $curr_sum; 
                    $billingRowData['idBooking'] = $searchResults['idBooking']; 
                    $billingRowData['iCounter'] = $ctr++; 
                    $billingRowData['iDisplayTrOnlick'] = $iDisplayTrOnlick; 
                    $billingRowData['szBillingFlag'] = false; 
                    $billingRowData['iFinancialVersion'] = $searchResults['iFinancialVersion']; 
                    $billingRowData['dtPaymentConfirmedSorting'] = $searchResults["dtPaymentConfirmed"];  
                    $billingRowData['dtCreatedOn'] = $searchResults["dtCreatedOn"];
                    $billingRowData['id'] = $searchResults["id"];

                    /*
                    * Adding row data to array
                    */
                    $billingRowDataAry[] = $billingRowData; 
                }
                else if(($searchResults['iDebitCredit']==2) || (($searchResults['iDebitCredit']==3 || $searchResults['iDebitCredit']==8 || $searchResults['iDebitCredit']==12) && $searchResults['iStatus']==2))
                {   
                    $fTotalCourierLabelPaid = 0;
                    if(trim($searchResults['szBooking'])===__TRANSPORTECA_FINANCIAL_TAGS_AUTOMATIC_TRANSFER__)
                    {
                        //Incase of Automatic Transfer We are deducting Label Fee
                        $CreateLabelBatch = $searchResults['szInvoice'] + 1;
                        $szInvoice_formatted = $kAdmin->getFormattedString($CreateLabelBatch,6);
                        $customer_code = $szInvoice_formatted ; 
                        $totalCourierLabelArr=$kAdmin->totalAmountCourierLabelFee($customer_code);  
                        if(!empty($totalCourierLabelArr))
                        {
                            $fTotalCourierLabelPaid = $totalCourierLabelArr[0]['totalCurrencyValue']; 
                            $searchResults['fTotalPriceForwarderCurrency'] = $searchResults['fTotalPriceForwarderCurrency']-$fTotalCourierLabelPaid;
                        }
                    } 

                    /*
                    * Summing up Invoice value for the booking
                    */
                    $idCurency = $searchResults['szCurrency'];
                    $fTotalPriceForwarderCurrency = round($searchResults['fTotalPriceForwarderCurrency'],2);
                    $value = round($currentAvailableAmountAry[$idCurency]['fTotalBalance'],2) - $fTotalPriceForwarderCurrency; 
                    $sumAry[$idCurency] = (float)($value); 
                    $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];  

                    $curr_sum = '';
                    if($sumAry[$idCurency]>=0)
                    {
                        $curr_sum = $searchResults['szCurrency']." ".number_format((float)$sumAry[$idCurency],2);
                    }
                    else
                    {
                        $newAmount=str_replace("-","",$sumAry[$idCurency]);
                        $curr_sum = "(".$searchResults['szCurrency']." ".number_format((float)$newAmount,2).")";
                    }

                    if($searchResults['iDebitCredit']==12)
                    {
                        $szBillingFlag = "HANDLING_FEE";	 
                    } 
                    else
                    {
                        $szBillingFlag = getBookingTags($searchResults['szBooking']);
                    }

                    $dtPaymentConfirmed = "";
                    if((!empty($searchResults["dtPaymentConfirmed"]) && $searchResults["dtPaymentConfirmed"]!="0000-00-00 00:00:00"))
                    {
                        $dtPaymentConfirmed = date("d-M",strtotime($searchResults["dtPaymentConfirmed"]));
                    } 

                    if($searchResults["iFinancialVersion"]==__TRANSPORTECA_FINANCIAL_VERSION__ && $szBillingFlag=='Transfer')
                    {
                        $szBooking = "Transfer - scheduled ".date("d F",strtotime($searchResults["dtPaymentScheduled"]));
                    }
                    else if(trim($searchResults['szBooking'])=='Transporteca referral fee')
                    { 
                        $szBooking = "Transporteca Referral Fee Invoice";
                    }
                    else
                    {
                        $szBooking = $searchResults['szBooking'];
                    }  

                    $szYourReference = "";
                    if((int)$searchResults['idBooking']>0)
                    {
                        $szYourReference = '<span id="invoice_comment_'.$searchResults['idBooking'].'">'.createSubString($kBooking->szInvoiceFwdRef,0,12).'</span><br />
                                         <a href="javascript:void(0)" onclick="update_invoice_comment(\''.$searchResults['idBooking'].'\',\''.$kBooking->szBookingRef.'\',\'booking\')" style="text-decoration:none;font-size:11px;"><img src="'.__BASE_STORE_IMAGE_URL__.'/Icon - Edit line.png" alt = "Click to update" style="float: right;margin-top:-17px;"></a>';

                    }  
                    $szInvoiceNumber = "";
                    if(trim($searchResults['szBooking'])==="Transporteca referral fee" || trim($searchResults['szBooking'])==='Courier Booking and Labels Invoice' || trim($searchResults['szBooking'])==='Courier Booking and Labels Credit' || trim($searchResults['szBooking'])==='Transporteca Referral Fee Credit' || $searchResults['iDebitCredit']==12)
                    { 
                        $szInvoiceNumber = $searchResults['szInvoice'];
                    }

                    if(trim($searchResults['szBooking'])==='Courier Booking and Labels Credit' || trim($searchResults['szBooking'])==='Transporteca Referral Fee Credit')
                    {
                        $szInvoiceValue = $searchResults['szCurrency'] ." ".number_format(abs((float)($searchResults['fTotalPriceForwarderCurrency'])),2); 
                    }
                    else
                    {
                        $szInvoiceValue = "(".$searchResults['szCurrency'] ." ".number_format(abs((float)($searchResults['fTotalPriceForwarderCurrency'])),2).")";  
                    }  

                    /*
                    * Building data to display Billing Rows
                    */
                    $billingRowData = array();
                    $billingRowData['dtPaymentConfirmed'] = $dtPaymentConfirmed;
                    $billingRowData['szBooking'] = $szBooking;
                    $billingRowData['szYourReference'] = $szYourReference;
                    $billingRowData['szInvoiceNumber'] = $szInvoiceNumber;
                    $billingRowData['szInvoiceValue'] = $szInvoiceValue;
                    $billingRowData['szCurrentBalance'] = $curr_sum; 
                    $billingRowData['idBooking'] = $searchResults['idBooking'];  
                    $billingRowData['iBatchNo'] = $searchResults['iBatchNo']; 
                    $billingRowData['idForwarder'] = $idForwarder; 
                    $billingRowData['szBillingFlag'] = $szBillingFlag; 
                    $billingRowData['iCounter'] = $ctr++; 
                    $billingRowData['iDisplayTrOnlick'] = 2; 
                    $billingRowData['dtPaymentConfirmedSorting'] = $searchResults["dtPaymentConfirmed"]; 
                    $billingRowData['dtCreatedOn'] = $searchResults["dtCreatedOn"];
                    $billingRowData['id'] = $searchResults["id"];
                    /*
                    * Adding row data to array
                    */
                    $billingRowDataAry[] = $billingRowData;  
                }
                else if(($searchResults['iDebitCredit']==6 || $searchResults['iDebitCredit']==7)) // Cancelled booking
                { 
                    $idCurency = $searchResults['szForwarderCurrency'] ;	 
                    $szBillingFlag = 'CREDIT_NOTE';

                    //echo "<br>Cancelled: ".$searchResults['szBooking']." - currency - ".$idCurency." B4 balance: ".$sumAry[$idCurency];
                    /*
                    * Summing up Invoice value for the booking
                    */
                    $fTotalPriceForwarderCurrency = round($searchResults['fTotalPriceForwarderCurrency'],2);
                    $value = round($currentAvailableAmountAry[$idCurency]['fTotalBalance'],2) - $fTotalPriceForwarderCurrency;   
                    $sumAry[$idCurency] = (float)($value); 
                    $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];   
                    $fTotalPriceForwarderCurrency_neg += $fTotalPriceForwarderCurrency;
                    $curr_sum='';
                    if($sumAry[$idCurency]>=0)
                    {
                        $curr_sum=$searchResults["szForwarderCurrency"]." ".number_format((float)$sumAry[$idCurency],2);
                    }
                    else
                    {
                        $newAmount=str_replace("-","",$sumAry[$idCurency]);
                        $curr_sum="(".$searchResults["szForwarderCurrency"]." ".number_format((float)$newAmount,2).")";
                    }
                    $invoiceRoeValueArr=array();
                    $invoiceRoeValueArr[]=$kBilling->getInvoiceRoeValueCancelBooking($searchResults['idBooking']); 

                    $dtPaymentConfirmed = "";
                    if((!empty($searchResults["dtPaymentConfirmed"]) && $searchResults["dtPaymentConfirmed"]!="0000-00-00 00:00:00"))
                    {
                        $dtPaymentConfirmed = date("d-M",strtotime($searchResults["dtPaymentConfirmed"]));
                    }  

                    $szYourReference = "";
                    if((int)$searchResults['idBooking']>0)
                    {
                        if($searchResults['iDebitCredit']==6)
                        {
                            $szYourReference = "<span id='invoice_cancel_comment_".$searchResults['idBooking']."'>".createSubString($kBooking->szInvoiceFwdRef,0,12)."</span><br />
                                <a href='javascript:void(0)' onclick='update_invoice_comment(\"".$searchResults['idBooking']."\",\"".$kBooking->szBookingRef."\",\"cancel\")' style='text-decoration:none;font-size:11px;'><img src='".__BASE_STORE_IMAGE_URL__."/Icon - Edit line.png' alt = 'Click to update' style='float: right;margin-top:-17px;'></a>
                            "; 
                        }
                        else
                        {
                            $szYourReference = "<span id='invoice_comment_".$searchResults['idBooking']."'>".createSubString($kBooking->szInvoiceFwdRef,0,12)."</span><br />
                                <a href='javascript:void(0)' onclick='update_invoice_comment(\"".$searchResults['idBooking']."\",\"".$kBooking->szBookingRef."\",\"booking\")' style='text-decoration:none;font-size:11px;'><img src='".__BASE_STORE_IMAGE_URL__."/Icon - Edit line.png' alt = 'Click to update' style='float: right;margin-top:-17px;'></a>
                            ";
                        }
                    }   
                    $szInvoiceValue = "(".$searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")"; 

                    /*
                    * Building data to display Billing Rows
                    */
                    $billingRowData = array();
                    $billingRowData['dtPaymentConfirmed'] = $dtPaymentConfirmed;
                    $billingRowData['szBooking'] = $searchResults['szBooking'];
                    $billingRowData['szYourReference'] = $szYourReference;
                    $billingRowData['szInvoiceNumber'] = $searchResults['szInvoice'];
                    $billingRowData['szInvoiceValue'] = $szInvoiceValue;
                    $billingRowData['szCurrentBalance'] = $curr_sum; 
                    $billingRowData['idBooking'] = $searchResults['idBooking'];  
                    $billingRowData['iBatchNo'] = $searchResults['iBatchNo']; 
                    $billingRowData['idForwarder'] = $idForwarder; 
                    $billingRowData['szBillingFlag'] = $szBillingFlag; 
                    $billingRowData['iCounter'] = $ctr++; 
                    $billingRowData['iDisplayTrOnlick'] = 1; 
                    $billingRowData['iFinancialVersion'] = $searchResults['iFinancialVersion'];
                    $billingRowData['dtPaymentConfirmedSorting'] = $searchResults["dtPaymentConfirmed"];  
                    $billingRowData['dtCreatedOn'] = $searchResults["dtCreatedOn"];
                    $billingRowData['id'] = $searchResults["id"];

                    /*
                    * Adding row data to array
                    */
                    $billingRowDataAry[] = $billingRowData;  
                }
                else if($searchResults['iDebitCredit']=='5')
                {  
                    $sumAry[$idCurency] = (float)($currentAvailableAmountAry[$idCurency]['fTotalBalance'] - $searchResults['fTotalPriceForwarderCurrency']);
                    
                    $currentAvailableAmountAry[$idCurency]['fTotalBalance'] = $sumAry[$idCurency];

                    $curr_sum='';
                    if($sumAry[$idCurency]>=0)
                    {
                        $curr_sum=$searchResults['szForwarderCurrency']." ".number_format((float)$sumAry[$idCurency],2);
                    }
                    else
                    {
                        $newAmount=str_replace("-","",$sumAry[$idCurency]);
                        $curr_sum="(".$searchResults['szForwarderCurrency']." ".number_format((float)$newAmount,2).")";
                    }

                    $dtPaymentConfirmed = "";
                    if((!empty($searchResults["dtPaymentConfirmed"]) && $searchResults["dtPaymentConfirmed"]!="0000-00-00 00:00:00"))
                    {
                        $dtPaymentConfirmed = date("d-M",strtotime($searchResults["dtPaymentConfirmed"]));
                    }
                    $fTotalPriceForwarderCurrency = $searchResults['fTotalPriceForwarderCurrency'];
                    $szInvoiceValue = "(".$searchResults['szForwarderCurrency'] ." ".number_format(abs((float)$searchResults['fTotalPriceForwarderCurrency']),2).")";
                    $szBillingFlag = "uploadService";  
                    /*
                    * Building data to display Billing Rows
                    */
                    $billingRowData = array();
                    $billingRowData['dtPaymentConfirmed'] = $dtPaymentConfirmed;
                    $billingRowData['szBooking'] = $searchResults['szBooking'];
                    $billingRowData['szYourReference'] = $szYourReference;
                    $billingRowData['szInvoiceNumber'] = $searchResults['szInvoice'];
                    $billingRowData['szInvoiceValue'] = $szInvoiceValue;
                    $billingRowData['szCurrentBalance'] = $curr_sum; 
                    $billingRowData['idBooking'] = $searchResults['idBooking'];  
                    $billingRowData['iBatchNo'] = $searchResults['iBatchNo']; 
                    $billingRowData['idForwarder'] = $idForwarder; 
                    $billingRowData['szBillingFlag'] = $szBillingFlag; 
                    $billingRowData['iCounter'] = $ctr++; 
                    $billingRowData['iDisplayTrOnlick'] = 2; 
                    $billingRowData['dtPaymentConfirmedSorting'] = $searchResults["dtPaymentConfirmed"];
                    $billingRowData['dtCreatedOn'] = $searchResults["dtCreatedOn"];
                    $billingRowData['id'] = $searchResults["id"];
                    /*
                    * Adding row data to array
                    */
                    $billingRowDataAry[] = $billingRowData;  
                }
                else if($searchResults['iDebitCredit']=='4')
                {
                    $dateOfjoining = strtotime($searchResults["dtCreatedOn"]); 
                    if((!empty($invoicePendingByAdmin) || !empty($invoiceBillConfirmed)) && ($dateFormSearchResult<=$dateOfjoining ))
                    {	 
                        $dtPaymentConfirmed = date('d-M',$dateOfjoining); 
                        $curr_sum = $searchResults["szForwarderCurrency"]." 0.00";
                        /*
                        * Building data to display Billing Rows
                        */
                        $billingRowData = array();
                        $billingRowData['dtPaymentConfirmed'] = $dtPaymentConfirmed;
                        $billingRowData['szBooking'] = $searchResults['szBooking'];   
                        $billingRowData['szCurrentBalance'] = $curr_sum; 
                        $billingRowData['idBooking'] = $searchResults['idBooking'];   
                        $billingRowData['iCounter'] = $ctr++; 
                        $billingRowData['iDisplayTrOnlick'] = false; 
                        $billingRowData['dtPaymentConfirmedSorting'] = $searchResults["dtPaymentConfirmed"];
                        $billingRowData['dtCreatedOn'] = $searchResults["dtCreatedOn"];
                        $billingRowData['id'] = $searchResults["id"];
                        /*
                        * Adding row data to array
                        */
                        $billingRowDataAry[] = $billingRowData;  
                    } 
                } 
                //echo "<br> Currency: ".$idCurency." Invoice Amt: ".$fTotalPriceForwarderCurrency." Sum: ".$currentAvailableAmountAry[$idCurency]['fTotalBalance']." DC: ".$searchResults['iDebitCredit']." ID: ".$searchResults['id']; 
            } 						
        }
        if(!empty($billingRowDataAry))
        {
            //$billingRowDataAry = array_reverse($billingRowDataAry); 
            //$billingRowDataAry = sortArray($billingRowDataAry,'dtPaymentConfirmedSorting','DESC'); 
            $billingRowDataAry = sortArray($billingRowDataAry,'dtPaymentConfirmed',false,'fTotalPriceForwarderCurrency',false,'id',false,true);   
            
        } 
    }
    
    if($bReturnData)
    {
        return $billingRowDataAry;
    }
    else
    {
        ?>
        <table style="margin-left:-3px;width:100%;">
            <tr>
                <td class="wd-25 billing-heading-values"><span><?php echo $billingHeaderValuesAry['fTotalAmountLastTransferred']; ?></span><br>Last transfer</td>
                <td class="wd-25 billing-heading-values"><span><?php echo $billingHeaderValuesAry['fTotalAmountNextTransferred']; ?></span><br>Next transfer<?php echo $szAstrixText; ?></td>
                <td class="wd-25 billing-heading-values"><span><?php echo $billingHeaderValuesAry['dtNextTransferDate']; ?></span><br>Transfer date</td>
                <td class="wd-25 billing-heading-values"><span><?php echo $billingHeaderValuesAry['fTotalForwarderValue']; ?></span><br>Total volume</td> 
            </tr>
        </table>
        <div style="clear: both"></div>
        <div id="forwarder_billing_bottom_form" class="oh" style="padding-top: 20px;padding-bottom: 0px;"> 
            <table cellpadding="0" style="width: 100%;" cellspacing="0" class="format-4" id="booking_table">
                <tr>
                    <th width="8%" valign="top" ><?=t($t_base.'fields/date')?></th>
                    <th width="33%" valign="top" ><?=t($t_base.'fields/description')?></th>
                    <th width="24%" valign="top" ><?=t($t_base.'fields/your');?> <?=t($t_base.'fields/reference');?></th>	
                    <th width="10%" valign="top" ><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/number')?></th>				
                    <!--<th width="14%" valign="top" style="text-align: right;"><?=t($t_base.'fields/invoice')?> <?=t($t_base.'fields/value')?></th>-->
                    <!--<th width="7%" valign="top" style="text-align: right;"><?=t($t_base.'fields/roe')?>*</th>-->
                    <th width="12%" valign="top" style="text-align: right;"><?php echo ucfirst(t($t_base.'fields/value'));?> </th>
                    <th width="13%" valign="top" style="text-align: right;"><?=t($t_base.'fields/balance')?></th>
                </tr>
            <?php
                if(!empty($billingRowDataAry))
                {
                    $onClickFunction="forwarderBillingSearchFormSubmit()";
                    foreach($billingRowDataAry as $billingRowDataArys)
                    {
                        echo display_billing_rows($billingRowDataArys);  
                    }
                }
                else
                {
                    $style="style='opacity:0.4'";
                    ?>
                    <tr>
                        <td colspan="6" align="center" style="padding: 5px 0 5px 0;"><?=t($t_base.'messages/no_transactions_in_the_selected_date_range');?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <div id="invoice_comment_div" style="display:none"></div><br/> 
            <div id="viewInvoiceComplete" style="float: right;">
                <a class="button1" id="download_invoice" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
                <a class="button1" id="download_booking"  style="opacity:0.4;"><span><?=t($t_base.'fields/view_booking');?></span></a>
                <a href="javascript:void(0)"  id="download_table" onclick= "<?echo $onClickFunction;?>" <?=$style; ?> class="button1"><span><?=t($t_base.'fields/download_table');?></span></a>
            </div>
            <?php
                if($iDisplayAstrix)
                {
                    ?>
                    <div>* Estimated – the amount may change as you receive more bookings</div>
                    <?php
                }
             ?>
        </div>    
        <?php
    }
}

function display_billing_rows($billingRowData)
{ 
    if(!empty($billingRowData))
    {          
        ?>
        <?php if($billingRowData['iDisplayTrOnlick']==1){?>
            <tr id="booking_data_<?php echo $billingRowData['iCounter']; ?>"  onclick="select_forwarder_billings_tr(this.id,'<?php echo $billingRowData['idBooking']; ?>','<?php echo $billingRowData['szBillingFlag']; ?>','<?php echo $billingRowData['iFinancialVersion'];?>');">
        <?php } else if($billingRowData['iDisplayTrOnlick']==2){?>
            <tr id='booking_data_<?php echo $billingRowData['iCounter']; ?>' onclick="select_forwarder_billings_referral_tr(this.id,'<?php echo $billingRowData['iBatchNo']; ?>','<?php echo $billingRowData['idForwarder']; ?>','<?php echo $billingRowData['szBillingFlag']; ?>')">
        <?php } else {?>
            <tr id='booking_data_<?php echo $billingRowData['iCounter']; ?>'>
        <?php }?> 
            <td><?php echo $billingRowData['dtPaymentConfirmed']; ?></td>
            <td><?php echo $billingRowData['szBooking']; ?></td>
            <td><?php echo $billingRowData['szYourReference']; ?></td>
            <td><?php echo $billingRowData['szInvoiceNumber']; ?></td>
            <td style="text-align: right;"><?php echo $billingRowData['szInvoiceValue']; ?></td>
            <td style="text-align: right;"><?php echo $billingRowData['szCurrentBalance']; ?></td>
        </tr>
        <?php 
    }
}

function getBookingTags($szBooking)
{
    if(trim($szBooking)===__TRANSPORTECA_FINANCIAL_TAGS_REFERAL_FEE__)
    {
        $flag="Referral";	 
    }
    else if(trim($szBooking)==="Automatic transfer")
    {
        $flag="Transfer";
    }
    else if(trim($szBooking)==="Courier Booking and Labels Invoice")
    {
        $flag="Labels Fee";
    }
    else if(trim($szBooking)==="Courier Booking and Labels Credit")
    {
        $flag="Labels Fee";
    }
    else if(trim($szBooking)==='Transporteca Referral Fee Credit')
    {
        $flag="Referral";	 
    } 
    return $flag;
}