<?php
if( !isset( $_SESSION ) )
{
    session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Ajay
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/pdf_functions.php" ); 
require_once( __APP_PATH__ . "/inc/logs_functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH_CLASSES__ . "/referralPricing.class.php");

include_classes();

function display_all_peding_tasks($incompleteTaskAry,$idBookingFile=false,$only_highlight_bc=false,$szBookingRefRedirectFlag=false,$szCopyMode='',$bReturnArray=false,$bOnload=false)
{ 
    $t_base="management/pendingTray/";  
    //value for $idBookingFile is only passed on creating of new booking file.
    
    $uagent = $_SERVER['HTTP_USER_AGENT'];
    $szExtraClass = '';
    $szOSName = os_info($uagent) ; 
    if($szOSName=='MacOS' || $szOSName=='iPad' || $szOSName=='iPhone')
    {
        $szExtraClass = ' mac-table-header'; 
    }    
    $returnPendingTrayAry = array();
    if(!empty($incompleteTaskAry))
    {
        $ctr = 1;
        $task_ctr = 0;
        $irowCount = count($incompleteTaskAry); 
        $kConfig = new cConfig();
        $taskStatusAry = array();
        $taskStatusAry = $kConfig->getAllActiveTaskStatus();

        $transportModeListAry = array();
        $transportModeListAry = $kConfig->getAllTransportMode(false,false,true);
        $selected_tr_id = '';
        foreach($incompleteTaskAry as $incompleteTaskArys)
        {   
            if($incompleteTaskArys['idTransportMode']==4) //courier 
            {
                $szVolWeight = format_volume($incompleteTaskArys['fCargoVolume'])."cbm / ".number_format((float)$incompleteTaskArys['fCargoWeight'])."kg / ".number_format((int)$incompleteTaskArys['iNumColli'])."col";
            }
            else
            {
                $szVolWeight = format_volume($incompleteTaskArys['fCargoVolume'])."cbm / ".number_format((float)$incompleteTaskArys['fCargoWeight'])."kg ";
            } 
            $szCustomerName = $incompleteTaskArys['szFirstName']." ".$incompleteTaskArys['szLastName'] ;
            $szCustomerNameHover = $szCustomerName; //This text will displayed as mouse over string
            if(strlen($szCustomerName)>20)
            {
                $szCustomerName = substrwords($szCustomerName,17)."...";
            }    
            //$szCustomerName = returnLimitData($szCustomerName,20);

            $szTask = $taskStatusAry[$incompleteTaskArys['szTaskStatus']]['szDescription'];
            $szTaskStatus = $incompleteTaskArys['szTaskStatus']; 

            $iTaskCreatedTime = strtotime(date('Y-m-d',strtotime($incompleteTaskArys['dtTaskStatusUpdatedOn']))) ;
            $iTodayDateTime = strtotime(date('Y-m-d'));
            $iDayLightSavingTime = 0;
 
            /*
             * if(date('I')==1)
            {
                $iDayLightSavingTime = 1;  
                $incompleteTaskArys['dtTaskStatusUpdatedOn'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($incompleteTaskArys['dtTaskStatusUpdatedOn'])));  
            }  
             */
            
            $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'];

            $date = new DateTime($incompleteTaskArys['dtTaskStatusUpdatedOn']); 
            if(!empty($szCustomerTimeZone))
            {
                $date->setTimezone(new DateTimeZone($szCustomerTimeZone)); 
            } 
            if($iTaskCreatedTime==$iTodayDateTime)
            { 
                $szTaskUpdatedOn = $date->format('H:i') ; //In local time zone 
                $szTaskUpdatedOn_1 = date('H:i',strtotime($incompleteTaskArys['dtTaskStatusUpdatedOn'])) ; //In server time zone 
            }
            else
            {
                $szTaskUpdatedOn = $date->format('d M H:i') ; //In local timezone
                $szTaskUpdatedOn_1 = date('d M H:i',strtotime($incompleteTaskArys['dtTaskStatusUpdatedOn'])); //In Server time zone
            } 
            $szTransportMode = '';
            if($incompleteTaskArys['idTransportMode']>0)
            {
                $szTransportMode = $transportModeListAry[$incompleteTaskArys['idTransportMode']]['szShortName'] ; 
            } 
            $iDisplayPopup = 0;
            // IF the ‘File owner’ is NULL OR IF the ‘File owner’ is different  than than the operator THEN a popup message should appear.
            if($incompleteTaskArys['idFileOwner']<=0 || $incompleteTaskArys['idFileOwner']!=$_SESSION['admin_id'])
            {
                $iDisplayPopup = 1 ;
            }   
            if(($idBookingFile>0) && ($incompleteTaskArys['idBookingFile']==$idBookingFile))
            { 
                $selected_tr_id = "booking_data_".$ctr ; 
            } 
            $task_ctr++;
            $iCrmEmail = false;
            $iCrmEmail = 0;
            $idReminderTask = false;
            if($szTaskStatus == 'T210')
            {
                $iCrmEmail = 1;
                if($incompleteTaskArys['iNumUnreadMessage']>1)
                {
                    $szTask = "Read ".$incompleteTaskArys['iNumUnreadMessage']." messages";
                } 
                /*
                * When task status is T210 then we always open customer log/ file log so that updated the follwing value
                */
                $incompleteTaskArys['iBookingFlag'] = 0;
            }
            else if($szTaskStatus == 'T220')
            {
                $idReminderTask = $incompleteTaskArys['idReminderTask'];
                /*
                * When task status is T220 then we always open customer log/ file log so that updated the follwing value
                */
                $incompleteTaskArys['iBookingFlag'] = 0;
            }
            else if($szTaskStatus=='T200')
            {
                $idOfflineChat = $incompleteTaskArys['idOfflineChat'];
                $incompleteTaskArys['iBookingFlag'] = 0;
            } 
            $szOnclickFunc = "display_pending_task_details('booking_data_".$ctr."','".$incompleteTaskArys['idBookingFile']."','".$iDisplayPopup."','".$incompleteTaskArys['idBookingQuote']."','".$incompleteTaskArys['iQuoteClosed']."','".$incompleteTaskArys['iBookingFlag']."','','".$iCrmEmail."','".$idReminderTask."','".$idOfflineChat."')";  

            $finalPrintableAry = array();
            $finalPrintableAry['iCounter'] = $ctr;
            $finalPrintableAry['szTrId'] = "booking_data_".$ctr;
            $finalPrintableAry['idBookingFile'] = $incompleteTaskArys['idBookingFile'];
            $finalPrintableAry['szTaskUpdatedOn'] = $szTaskUpdatedOn;
            $finalPrintableAry['szTask'] = $szTask;
            $finalPrintableAry['szCustomerName'] = $szCustomerName;
            $finalPrintableAry['szCustomerNameHover'] = $szCustomerNameHover; 
            $finalPrintableAry['szTransportMode'] = $szTransportMode;
            $finalPrintableAry['szOriginCity'] = $incompleteTaskArys['szOriginCity'];
            $finalPrintableAry['szDestinationCity'] = $incompleteTaskArys['szDestinationCity'];
            $finalPrintableAry['szVolWeight'] = $szVolWeight;
            $finalPrintableAry['szTaskStatus'] = $incompleteTaskArys['szTaskStatus'];
            $finalPrintableAry['iDisplayPopup'] = $iDisplayPopup;
            $finalPrintableAry['idBookingQuote'] = $incompleteTaskArys['idBookingQuote'];
            $finalPrintableAry['iQuoteClosed'] = $incompleteTaskArys['iQuoteClosed'];
            $finalPrintableAry['iBookingFlag'] = $incompleteTaskArys['iBookingFlag'];
            $finalPrintableAry['iCrmEmail'] = $iCrmEmail;
            $finalPrintableAry['idReminderTask'] = $idReminderTask;
            $finalPrintableAry['idOfflineChat'] = $idOfflineChat;
            $finalPrintableAry['bOnlyHighlight'] = (int)$only_highlight_bc;  
            $finalPrintableAry['idSelectedBookingFileId'] = $idBookingFile;
            $finalPrintableAry['szCopyMode'] = $szCopyMode; 
            $returnPendingTrayAry[] = $finalPrintableAry;
            $ctr++;
        } 
    }
    if($bReturnArray)
    {
        return $returnPendingTrayAry;
    }
    else 
    {
        $jSonString = ''; 
        if(!empty($returnPendingTrayAry))
        {  
            $kPendingTray = new cPendingTray();
            $jSonString = $kPendingTray->jsonEncode($returnPendingTrayAry);   
        } 
        if(!$bOnload)
        { 
            return $jSonString;
        } 
        $szOnclickFunc = "display_pending_task_details(pendingTask.szTrId,pendingTask.idBookingFile,pendingTask.iDisplayPopup,pendingTask.idBookingQuote,pendingTask.iQuoteClosed,pendingTask.iBookingFlag,'',pendingTask.iCrmEmail,pendingTask.idReminderTask,pendingTask.idOfflineChat)";  
        ?>
        <div id="pending_task_main_list_container_div" <?php if($szBookingRefRedirectFlag){?> style="display: none;" <?php }?>>
            <form id="send_new_insured_booking" name="send_new_insured_booking" action="javascript:void(0);" method="post"> 
                <table cellpadding="5" cellspacing="0" class="table-scroll-head <?php echo $szExtraClass; ?>" width="100%">
                    <tr>
                        <th style="width:10%;" valign="top"><?=t($t_base.'fields/time')?></th> 
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/task');?></th>
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/customer');?></th>
                        <th style="width:10%;" valign="top"><?=t($t_base.'fields/mode');?></th> 
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/from');?></th>
                        <th style="width:15%;" valign="top"><?=t($t_base.'fields/to');?></th>
                        <th style="width:18%;" valign="top"><?=t($t_base.'fields/cargo_text');?></th>
                        <th style="width: 2%"><!--<input type="checkbox" id="iAllCheckbox" name="iAllCheckbox" onclick="selectDeselectAllPendingTray();"/>All--></th>
                    </tr>
                </table> 
                <div id="pending_task_list_container_div" class="pending-tray-scrollable-div"> 
                    <div style='display:block;text-align: center;' id="pending_tray_listing_loader_div">
                        <img src="<?php echo __MAIN_SITE_HOME_PAGE_URL__."/images/loader.gif"; ?>" alt="Loading...">
                    </div>
                    <div class="scrolling-div pending-tray-lists" id='pending-tray-angular-lis-container' style='display:none;'>
                        <table cellpadding="2" cellspacing="0" class="format-3" width="100%" id="insuranced_booking_table"> 
                            <tr id="{{pendingTask.szTrId}}" class="pending_tray_listing_row_{{pendingTask.idBookingFile}}" ng-repeat="pendingTask in pendingTasks">
                                <td class="wd-10 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="display_pending_task_details(pendingTask.szTrId,pendingTask.idBookingFile,pendingTask.iDisplayPopup,pendingTask.idBookingQuote,pendingTask.iQuoteClosed,pendingTask.iBookingFlag,'',pendingTask.iCrmEmail,pendingTask.idReminderTask,pendingTask.idOfflineChat)">{{pendingTask.szTaskUpdatedOn}}</td>
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="display_pending_task_details(pendingTask.szTrId,pendingTask.idBookingFile,pendingTask.iDisplayPopup,pendingTask.idBookingQuote,pendingTask.iQuoteClosed,pendingTask.iBookingFlag,'',pendingTask.iCrmEmail,pendingTask.idReminderTask,pendingTask.idOfflineChat)">{{pendingTask.szTask}}</td>
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="<?php echo $szOnclickFunc; ?>" title="{{pendingTask.szCustomerNameHover}}">{{pendingTask.szCustomerName}}</td>
                                <td class="wd-10 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="<?php echo $szOnclickFunc; ?>">{{pendingTask.szTransportMode}}</td> 
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="<?php echo $szOnclickFunc; ?>">{{pendingTask.szOriginCity}}</td>
                                <td class="wd-15 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="<?php echo $szOnclickFunc; ?>">{{pendingTask.szDestinationCity}}</td>
                                <td class="wd-18 pending-tray-td-{{pendingTask.idBookingFile}}" ng-click="<?php echo $szOnclickFunc; ?>">{{pendingTask.szVolWeight}}</td>
                                <td class="wd-2" ng-if="pendingTask.szTaskStatus!= 'T140'"><input type="checkbox" class="pendingTrayList" name="pendingTrayList" id="pendingTrayList_{{pendingTask.idBookingFile}}" value="{{pendingTask.idBookingFile}}" ng-click="selectLineForClosingTheFile(pendingTask.idBookingFile,'pendingTrayList_'+pendingTask.idBookingFile,pendingTask.iCounter)"/></td>
                            </tr>
                        </table>
                    </div>
                </div> 
                <input type="hidden" value="<?php echo $idBookingFile; ?>" name="hidden_selected_file_id" id="hidden_selected_file_id">
                <input type="hidden" value="<?php echo $task_ctr; ?>" name="hidden_number_of_files_displayed" id="hidden_number_of_files_displayed"> 
            </form>  
        </div> 
        <script type="text/javascript">  
            var __PENDING_TRAY_JSON_DATA__ = <?php echo $jSonString; ?>;
        </script>
        <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__."/angular.min.js?v=".time(); ?>"></script>
        <script type="text/javascript" src="<?php echo __BASE_STORE_ANGULAR_JS_URL__."/taskController.js?v=".time(); ?>"></script>
        <?php
    }
    
    if($idBookingFile>0)
    {
        if($only_highlight_bc)
        {
            ?>
            <script type="text/javascript"> 
                var idSelectedBooking = '<?php echo $idBookingFile; ?>';
                var idLastClickedBooking = $('#idLastClikedBookingFile').val();

                idSelectedBooking = parseInt(idSelectedBooking);
                idLastClickedBooking = parseInt(idLastClickedBooking);
                var szRow_id = "";
                var iSelectClass = 0;
                if(idSelectedBooking!=idLastClickedBooking)
                { 
                    szRow_id = "pending_tray_listing_row_"+idLastClickedBooking;
                    iSelectClass = 1;
                }
                else
                { 
                    szRow_id = '<?=$selected_tr_id?>';
                } 
                scrollPendingTask(szRow_id,iSelectClass); 
            </script>
            <?php
        }
        else
        {
            ?>
            <script type="text/javascript">
                display_pending_task_details('<?=$selected_tr_id?>','<?php echo $idBookingFile; ?>','','','','','<?php echo $szCopyMode;?>');
            </script>
            <?php
        }
    }
    else
    {
?>
    <script type="text/javascript"> 
        scrollPendingTask('<?=$selected_tr_id?>');
    </script>        
    <?php }?>  
    <script type="text/javascript">  
        $(".scrolling-div").scroll(function()
        {
            var topzValue=$(this).scrollTop(); 
            $("#iScrollValue").attr('value',topzValue);
        });
    </script>          
<?php   
}

function display_pending_tray_row($pendingTrayDataAry,$ctr)
{
    $szOnclickFunc = "display_pending_task_details('booking_data_".$ctr."','".$pendingTrayDataAry['idBookingFile']."','".$pendingTrayDataAry['iDisplayPopup']."','".$pendingTrayDataAry['idBookingQuote']."','".$pendingTrayDataAry['iQuoteClosed']."','".$pendingTrayDataAry['iBookingFlag']."','','".$pendingTrayDataAry['iCrmEmail']."','".$pendingTrayDataAry['idReminderTask']."','".$pendingTrayDataAry['idOfflineChat']."')";  
    $szPendingTrayColumnClass = " pending-tray-td-".$pendingTrayDataAry['idBookingFile'];
    $szTrId = "booking_data_".$ctr;
    
    $idBookingFile = $pendingTrayDataAry['idBookingFile'];
    $iDisplayPopup = $pendingTrayDataAry['iDisplayPopup'];
    $idBookingQuote = $pendingTrayDataAry['idBookingQuote'];
    $iQuoteClosed = $pendingTrayDataAry['iQuoteClosed'];
    $iBookingFlag = $pendingTrayDataAry['iBookingFlag']; 
    $iCrmEmail = $pendingTrayDataAry['iCrmEmail'];
    $idReminderTask = $pendingTrayDataAry['idReminderTask'];
    $idOfflineChat = $pendingTrayDataAry['idOfflineChat']; 
    
    ?>
    <tr id="<?php echo $szTrId; ?>" class="pending_tray_listing_row_<?php echo $pendingTrayDataAry['idBookingFile']; ?>">
        <td class="wd-10<?php echo $szPendingTrayColumnClass; ?>" onclick="<?php echo $szOnclickFunc; ?>"><?php echo $pendingTrayDataAry['szTaskUpdatedOn']; ?></td>
        <td class="wd-15<?php echo $szPendingTrayColumnClass; ?>" onclick="<?php echo $szOnclickFunc; ?>"><?php echo $pendingTrayDataAry['szTask']; ?></td>
        <td class="wd-15<?php echo $szPendingTrayColumnClass; ?>" onclick="<?php echo $szOnclickFunc; ?>"><?php echo $pendingTrayDataAry['szCustomerName'] ; ?></td>
        <td class="wd-10<?php echo $szPendingTrayColumnClass; ?>" onclick="<?php echo $szOnclickFunc; ?>"><?php echo $pendingTrayDataAry['szTransportMode']; ?></td> 
        <td class="wd-15<?php echo $szPendingTrayColumnClass; ?>" onclick="<?php echo $szOnclickFunc; ?>"><?php echo $pendingTrayDataAry['szOriginCity']; ?></td>
        <td class="wd-15<?php echo $szPendingTrayColumnClass; ?>" onclick="<?php echo $szOnclickFunc; ?>"><?php echo $pendingTrayDataAry['szDestinationCity'];; ?></td>
        <td class="wd-18<?php echo $szPendingTrayColumnClass; ?>" onclick="<?php echo $szOnclickFunc; ?>"><?php echo $pendingTrayDataAry['szVolWeight']; ?></td>
        <td class="wd-2"><?php if ($pendingTrayDataAry['szTaskStatus']!='T140'){?><input type="checkbox" class="pendingTrayList" name="pendingTrayList" id="pendingTrayList_<?=$ctr?>" value="<?php echo $pendingTrayDataAry['idBookingFile']?>" onclick="selectLineForClosingTheFile('<?php echo $pendingTrayDataAry['idBookingFile']?>',this.id,'<?=$ctr?>')"/><?php }?></td>
    </tr>    
    <script type="text/javascript">
        /*
        $("."+'<?php echo trim($szPendingTrayColumnClass); ?>').unbind("click");
        $("."+'<?php echo trim($szPendingTrayColumnClass); ?>').click(function(){
            display_pending_task_details('<?php echo $szTrId; ?>','<?php echo $idBookingFile; ?>','<?php echo $iDisplayPopup; ?>','<?php echo $idBookingQuote; ?>','<?php echo $iQuoteClosed; ?>','<?php echo $iBookingFlag; ?>','<?php echo $iCrmEmail; ?>','<?php echo $idReminderTask; ?>','<?php echo $idOfflineChat; ?>');
        });
        */
    </script>
    <?php
}

function display_pending_tray_overview($kBooking)
{
    $t_base="management/pendingTray/"; 
            
    $kConfig = new cConfig();
    $kBooking_new = new cBooking();
    
    $taskStatusAry = array();    
    $postSearchAry = array();
    $fileStatusAry = array();
    $cargoDetailsAry = array();
    
    $idBooking = $kBooking->idBooking ;
    $idBookingFile = $kBooking->idBookingFile ;
    $bUploadLabel = false;
    if($kBooking->szTaskStatus=='T140' || $kBooking->szTaskStatus=='T150')
    {
        $bUploadLabel = true;
    }
    $postSearchAry = $kBooking_new->getBookingDetails($idBooking);
    $iQuickQuote = $postSearchAry['iQuickQuote'];
    
    $taskStatusAry = $kConfig->getAllActiveTaskStatus();    
    $fileStatusAry = $kConfig->getAllActiveFileStatus($iQuickQuote);
          
    $szTaskStatus = $taskStatusAry[$kBooking->szTaskStatus]['szDescription']; 
    $szFileStatus = $fileStatusAry[$kBooking->szFileStatus]['szDescription']; 
     
    if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_DRAFT_QUOTE__)
    {
        $szFileStatus = __S1_DRAFT_QUOTE_DESCRIPTION__;
    }
    $quotesAry = array();  
    $quotesAry = $kBooking_new->getAllBookingQuotesByFile($idBookingFile);
    
    $kUser = new cUser();
    $kUser->getUserDetails($postSearchAry['idUser']);
    
    $kConfig = new cConfig();
    $kConfig->loadCountry($postSearchAry['idCustomerDialCode']);
    $iInternationDialCode = $kConfig->iInternationDialCode;
      
    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
    {
        $szServiceType = 'EXW';
    }
    else
    {
        $szServiceType = 'FOB';
    }
    
    $showColliTextFlag=false;
    if((int)$postSearchAry['iNumColli']>0)
    {
        $showColliTextFlag=true;
    }
    
    if($postSearchAry['idTransportMode']==4 || $showColliTextFlag) //courier & if iNumColli greater than zero.
    { 
        $maxWeightText='';
        if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
        {
            $convertWeightFromVolume=round_up($postSearchAry['fCargoVolume']*__VOLUME_CONVERT_INTO_WEIGHT_AIR__);
            $maxWeightConvertByVolume=round((float)(max($postSearchAry['fCargoWeight'],$convertWeightFromVolume)));
            if((float)$maxWeightConvertByVolume>0)
            {
                $maxWeightText=" (".$maxWeightConvertByVolume."kg)";
            }
        }
        else if($postSearchAry['idTransportMode']==4){
            $convertWeightFromVolume=round_up($postSearchAry['fCargoVolume']*__VOLUME_CONVERT_INTO_WEIGHT_COURIER__);
            $maxWeightConvertByVolume=round((float)(max($postSearchAry['fCargoWeight'],$convertWeightFromVolume)));
            if((float)$maxWeightConvertByVolume>0)
            {
                $maxWeightText=" (".$maxWeightConvertByVolume."kg)";
            }        
        }
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg".$maxWeightText." / ".number_format((int)$postSearchAry['iNumColli'])."col";
    }
    else
    {
        $maxWeightText='';
        if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
        {
            $convertWeightFromVolume=round_up($postSearchAry['fCargoVolume']*__VOLUME_CONVERT_INTO_WEIGHT_AIR__);
            $maxWeightConvertByVolume=round((float)(max($postSearchAry['fCargoWeight'],$convertWeightFromVolume)));
            if((float)$maxWeightConvertByVolume>0)
            {
                $maxWeightText=" (".$maxWeightConvertByVolume."kg)";
            }
        }
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg".$maxWeightText;
    }
    if($postSearchAry['idServiceTerms']>0)
    {
        $serviceTermsAry = array();
        $serviceTermsAry = $kConfig->getAllServiceTerms($postSearchAry['idServiceTerms']);
        $szServiceTerms = $serviceTermsAry[0]['szShortTerms'];
    }
    else
    {
        $szServiceTerms = "";
    }
    
    
    $fTotalVolume = $postSearchAry['fCargoVolume'];
    $fTotalWeight = $postSearchAry['fCargoWeight']/1000 ;  // convrting weight from kg to meteric ton (mt)

    //$cargoDetailsAry = $kBooking_new->getCargoComodityDeailsByBookingId($idBooking); 
    
    $szCargoDescription = '';
    if(!empty($postSearchAry['szCargoDescription']))
    {
        $szCargoDescription = html_entities_flag(utf8_decode($postSearchAry['szCargoDescription'])); 
    } 
    // we are calculating price on MAX('total weight','total volume')
    if($fTotalWeight > $fTotalVolume)
    {
        $fChanrgeable = number_format((float)$postSearchAry['fCargoWeight'])."kg ";
    }
    else
    {
        $fChanrgeable = format_volume($postSearchAry['fCargoVolume'])."cbm ";
    }
    
    if(!empty($postSearchAry['szBookingRef']) && ($postSearchAry['idBookingStatus']=='3' || $postSearchAry['idBookingStatus']=='4' || $postSearchAry['idBookingStatus']=='7'))
    {
        if($postSearchAry['iInsuranceIncluded']==1)
        {
           
            $szInsuranceText = "Yes, ";
            $szInsuranceText .= $postSearchAry['szGoodsInsuranceCurrency']." ".number_format((float)$postSearchAry['fValueOfGoods']) ; 
            
            $fTotalInsuranceCostForBookingCustomerCurrencyText='-';
            if((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']>0.00)
            {
                $fTotalInsuranceCostForBookingCustomerCurrencyText=number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']);
            }
        }
        else
        {
            $szInsuranceText = "No";
            $fTotalInsuranceCostForBookingCustomerCurrencyText='-';
        }
    }
    else
    {
        if($postSearchAry['iInsuranceChoice']==1 || $postSearchAry['iInsuranceChoice']==3)
        {
            if($postSearchAry['iInsuranceChoice']==3)
            {
                $szInsuranceText = "Optional, ";
            }
            else
            {
                $szInsuranceText = "Yes, ";
            }
            $szInsuranceText .= $postSearchAry['szGoodsInsuranceCurrency']." ".number_format((float)$postSearchAry['fValueOfGoods']) ; 
            $fTotalInsuranceCostForBookingCustomerCurrencyText='-';
            if((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']>0.00)
            {
                $fTotalInsuranceCostForBookingCustomerCurrencyText=number_format((float)$postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']);
            }
        }
        else if($postSearchAry['iInsuranceChoice']==2)
        {
            $szInsuranceText = "No";
            $fTotalInsuranceCostForBookingCustomerCurrencyText='-';
        }
        else
        {
            $szInsuranceText = "N/A";
            $fTotalInsuranceCostForBookingCustomerCurrencyText='-';
            
        }
    }
    $kForwarder = new cForwarder();
    $kForwarder->load($postSearchAry['idForwarder']);
    
    $szVerfified_yes = 'style="display:none;"';
    $szVerfified_no = 'style="display:none;"';
    
    $szMouseOverText_yes = "Email verified";
    $szMouseOverText_no = "Email not verified";
    if($kUser->iConfirmed==1)
    { 
        $szVerfified_yes = 'style="display:inline-block;"';
        $iCustomerVerified = 1;
    } 
    else
    { 
        $szVerfified_no = 'style="display:inline-block;"';
        $iCustomerVerified = 0;
    } 
    if($kUser->iAcceptNewsUpdate==1)
    {
        $szNewsLetterText = 'Yes';
    }
    elseif($kUser->iAcceptNewsUpdate==2)
    { 
        $szNewsLetterText = 'No';
    }
    else
    {
        $szNewsLetterText = 'N/A';
    }
    
//    $disabalabelStatusAry = array();
//    $disabalabelStatusAry[0] = 'S7'; //Payment received
//    $disabalabelStatusAry[1] = 'S8'; //Booking sent
//    $disabalabelStatusAry[2] = 'S9'; //Booking confirmed
//    $disabalabelStatusAry[3] = 'S10'; //Booking advice sent
//    $disabalabelStatusAry[4] = 'S130'; //File closed  
//    $disabalabelStatusAry[5] = 'S160'; //Cancel closed 
//            
//    $iAlreadyPaidBooking = 0;  
//    if(in_array($szBookingFileStatus,$disabalabelStatusAry)|| $postSearchAry['idBookingStatus']==3 || $postSearchAry['idBookingStatus']==4 || $postSearchAry['idBookingStatus']==7)
//    {
//        $iAlreadyPaidBooking = 1;
//    }  
    $iAlreadyPaidBooking = checkalreadyPaidBooking($postSearchAry['idBookingStatus'],'',$szBookingFileStatus);
    if($iAlreadyPaidBooking==1)
    {
        $iBookingLanguage = $postSearchAry['iBookingLanguage'];  
    }
    else
    {
        $iBookingLanguage = $kUser->iLanguage;   
    } 
    
    $languageArr=$kConfig->getLanguageDetails('',$iBookingLanguage);
    if(!empty($languageArr))
    {
        $szBookingLaguage = ucwords(strtolower($languageArr[0]['szName']));
    }
    else
    {
        $szBookingLaguage = 'Danish';
    } 
    $kAdmin = new cAdmin();
    $szAdminName = ''; 
    if($kBooking->idFileOwner>0)
    {
        $kAdmin->getAdminDetails($kBooking->idFileOwner);
        $szAdminName = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
    } 
    $incompleteTaskAry = array(); 
    
    $iNumQuotes = 0;
    $iNumBookings = 0; 
    if($kUser->id>0)
    {
        $searchTaskAry = array(); 
        $searchTaskAry['idCustomer'] = $kUser->id; 
        $iNumQuotes = $kBooking_new->getTotalQuoteSentToCustomer($searchTaskAry,true);  
        
        $iNumFiles = $kBooking_new->getTotalFileForCustomer($searchTaskAry);

        $booking_searched_data = array();
        $booking_searched_data = $kBooking_new->search_all_forwarder_bookings(false,false,true,$kUser->id); 
        $iNumBookings = $booking_searched_data[0]['iNumBookings']; 
        
        $iNumQuotes = $iNumBookings + $iNumQuotes ;
    }
    if(!empty($postSearchAry['szBookingRef']) && ($postSearchAry['idBookingStatus']=='3' || $postSearchAry['idBookingStatus']=='4' || $postSearchAry['idBookingStatus']=='7'))
    {
        $szFileBookingRef = $kBooking->szFileRef." / ".$postSearchAry['szBookingRef'];
    }
    else
    {
        $szFileBookingRef = $kBooking->szFileRef;
    }
        
    $szPhoneNumber = "+".trim($iInternationDialCode)."".str_replace(' ','',trim($postSearchAry['szCustomerPhoneNumber']));
    $szPhoneNumberFormatted = "+".trim($iInternationDialCode)." ".trim($postSearchAry['szCustomerPhoneNumber']);
            
    $szInternalComment = nl2br($postSearchAry['szInternalComment']); 
    
    $kConfigNew = new cConfig();
    $transportModeListAry = array();
    $transportModeListAry = $kConfigNew->getAllTransportMode(false,__LANGUAGE_ID_ENGLISH__,true,true);
    $szTransportMode = $transportModeListAry[$postSearchAry['idTransportMode']]['szShortName'];
   
    if($postSearchAry['dtBookingConfirmed']!='0000-00-00 00:00:00' && strtotime($postSearchAry['dtBookingConfirmed'])>strtotime(__SHOWING_BOOKING_DOWNLOAD_DATE__))
    {
        $bookingDownloadAry=$kBooking->getAcknowledgeDownloadedByUserDetails($idBooking,$postSearchAry['idForwarder']); 
    } 
    ?> 
    <div class="clearfix accordion-container">
        <span class="accordian"><?php echo $szFileBookingRef;  ?> 
            <a href="javascript:void(0);" onclick="close_open_div('pending_task_overview_container','pending_task_overview_open_close_link','close');" id="pending_task_overview_open_close_link" class="close-icon"></a> 
        </span> 
        <span style="float:left;padding:6px 0 0 5px;">
            <a href="javascript:void(0);" onclick="display_remind_pane_add_new_task_popup('<?php echo $idBookingFile; ?>')">New Task</a>
        </span>
        <?php  
            if($iAlreadyPaidBooking==1)
            { 
                echo display_overview_links($kBooking,$postSearchAry);   
            } 
            else if($postSearchAry['iQuickQuote']==__QUICK_QUOTE_SEND_QUOTE__)
            {   
                echo display_overview_links($kBooking,$postSearchAry,false,true);
            } 
        ?>
    </div>
    <div class="scrolling-div-overview grey-container-div">
<!--        <table cellpadding="5" cellspacing="0" class="format-6" width="100%" >
            <tr>
                <th style="width:36%;text-align:center;border-bottom:0px;" valign="top"><?php echo t($t_base.'fields/customer')?></th> 
                <th style="width:32%;text-align:center;border-bottom:0px;" valign="top"><?php echo t($t_base.'fields/file')?></th> 
                <th style="width:32%;text-align:center;border-bottom:0px;" valign="top"><?php echo t($t_base.'fields/forwarder')?></th> 
            </tr>
        </table>-->
        <div id="pending_task_overview_container" <?php if($_POST['szCopyMode']=='copy'){?> style="display:none;" <?php }?> >
            <table cellpadding="5" cellspacing="0" class="format-6" width="100%" >
            <tr>
                <td style="width:36%;" valign="top">
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/customer')?>:</span>
                        <span class="field-value"><?php echo html_entities_flag($postSearchAry['szFirstName'])." ".html_entities_flag($postSearchAry['szLastName']); ?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/company')?>:</span>
                        <span class="field-value"><?php echo html_entities_flag($postSearchAry['szCustomerCompanyName']) ;?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/private')?>:</span>
                        <span class="field-value"><input type="checkbox" disabled <?php echo (($postSearchAry['iPrivateShipping']==1)?'checked':'');?>></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/phone')?>:</span>
                        <span class="field-value"><a href="tel:<?php echo $szPhoneNumber; ?>"><?php echo $szPhoneNumberFormatted;?></a></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/email')?>:</span>
                        <span class="field-value" style="width:60%;"> 
                            <a href="javascript:void(0);" title="<?php echo $postSearchAry['szEmail']; ?>" class="quote-comment-a truncate-text" <?php if(!empty($postSearchAry['szEmail'])){?>onclick="display_task_menu_details('REMIND','<?php echo $idBookingFile; ?>','DISPLAY_TASK_FORM_MENU','','<?php echo $postSearchAry['szEmail']; ?>')" <?php } ?>><?php echo $postSearchAry['szEmail']; ?></a>
                            <span id="confirmed_email_yes" <?php echo $szVerfified_yes; ?>> 
                                <a href="javascript:void(0);" class="verfied-yes" title="<?php echo $szMouseOverText_yes; ?>">&nbsp;</a> 
                            </span>
                            <span id="confirmed_email_no" <?php echo $szVerfified_no; ?>>
                                <a href="javascript:void(0);" class="verfied-no" title="<?php echo $szMouseOverText_no; ?>">&nbsp;</a> 
                            </span>
                            <input type="hidden" name="szCustomerOldEmail" id="szCustomerOldEmail" value="<?php echo $postSearchAry['szEmail']; ?>">
                            <input type="hidden" name="iCustomerVerified" id="iCustomerVerified" value="<?php echo $iCustomerVerified; ?>">
                            <input type="hidden" name="idBookingOverView" id="idBookingOverView" value="<?php echo $idBooking; ?>"> 
                        </span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/history')?>:</span>
                        <span class="field-value"><a href="javascript:void(0);" onclick="openSearchPopupClickOnHistory('<?php echo $kUser->id;?>','iBookingConfirmed');"><?php echo $iNumBookings; ?> bookings</a>, <a href="javascript:void(0);" onclick="openSearchPopupClickOnHistory('<?php echo $kUser->id;?>','iQuoteSent');"><?php echo $iNumQuotes; ?> quotes</a>, <a onclick="openSearchPopupClickOnHistory('<?php echo $kUser->id;?>','iFileFlag');" href="javascript:void(0);"><?php echo $iNumFiles;?> files</a></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/currency')?>:</span>
                        <span class="field-value"><?php echo $postSearchAry['szCurrency'];?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/language')?>:</span>
                        <span class="field-value"><?php echo $szBookingLaguage;?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/newsletter')?>:</span>
                        <span class="field-value"><?php echo $szNewsLetterText; ?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/owner')?>:</span>
                        <span class="field-value"><?php echo $szAdminName;?></span>
                    </div> 
                </td> 
                <td style="width:32%;" valign="top">
                    
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/status')?>:</span>
                        <span class="field-value" id="file_status_text"><?php echo $kBooking->szFileStatus.". ".$szFileStatus; ?>
                            <?php
                                if($kBooking->iClosed==1)
                                {
                                    ?>
                                     <a href="javascript:void(0);" onclick="reopen_booking_file('<?php echo $kBooking->id; ?>','REOPEN_BOOKING_FILE')" style="color: #7f7f7f;font-style: italic;"><?php echo t($t_base.'fields/reopen');?></a>
                                    <?php
                                }
                            ?>
                        </span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/from')?>:</span>
                        <span class="field-value truncate-text" title="<?php echo $postSearchAry['szOriginCountry']; ?>" onclick="open_pending_google_map('FROM')" style="cursor: pointer;"><u><?php echo $postSearchAry['szOriginCountry']; ?></u></span> 
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/to')?>:</span>
                        <span class="field-value truncate-text" title="<?php echo $postSearchAry['szDestinationCountry']; ?>" onclick="open_pending_google_map('TO')" style="cursor: pointer;"><u><?php echo $postSearchAry['szDestinationCountry']; ?></u></span>
                        <input type="hidden" name="szOverviewFromField" id="szOverviewFromField" value="<?php echo $postSearchAry['szOriginCountry']; ?>">
                        <input type="hidden" name="szOverviewToField" id="szOverviewToField" value="<?php echo $postSearchAry['szDestinationCountry']; ?>">
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/mode')."/".t($t_base.'fields/terms')."/".t($t_base.'fields/city'); ?>:</span>
                        <span class="field-value"><?php echo (!empty($szTransportMode)?$szTransportMode: '')." / ".$szServiceTerms." / ".$postSearchAry['szHandoverCity'] ; ?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/cargo_text'); ?>:</span>
                        <span class="field-value truncate-text" id="cargo_text_div" title="<?php echo $szVolWeight ; ?>"><?php echo $szVolWeight ; ?></span>
                    </div>
<!--                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/chargeable'); ?>:</span>
                        <span class="field-value"><?php echo $fChanrgeable ; ?></span>
                    </div>-->
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/description'); ?>:</span>
                        <span class="field-value overview_comments" title="<?php echo $szCargoDescription ; ?>"><?php echo $szCargoDescription ; ?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/date_of_shipment'); ?>:</span>
                        <span class="field-value"><?php echo date('d-F-Y',strtotime($postSearchAry['dtTimingDate'])) ; ?></span>
                    </div>
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/insurance'); ?>:</span>
                        <span class="field-value"><?php echo $szInsuranceText ; ?></span>
                    </div>
                    
                    <div class="overview">
                        <span class="field-name truncate-text"><?php echo t($t_base.'fields/price'); ?>/<?php echo t($t_base.'fields/vat'); ?>/<?php echo t($t_base.'fields/insurance'); ?>:</span>
                        <span class="field-value">
                            <?php
                            if(!empty($postSearchAry['szBookingRef']) && ($postSearchAry['idBookingStatus']=='3' || $postSearchAry['idBookingStatus']=='4' || $postSearchAry['idBookingStatus']=='7'))
                            {
                                $szBookingValueText=$postSearchAry['szCurrency']." ".number_format((float)$postSearchAry['fTotalPriceCustomerCurrency']);
                                $fTotalVatText="-";
                                if((float)$postSearchAry['fTotalVat']>0.00)
                                {
                                    $fTotalVatText=number_format((float)$postSearchAry['fTotalVat']);
                                }                                
                                echo $szBookingValueText." / ".$fTotalVatText." / ".$fTotalInsuranceCostForBookingCustomerCurrencyText; 
                                
                            }
                            ?>
                        </span>
                    </div>
                    
                    <div class="overview">
                        <span class="field-name"><?php echo t($t_base.'fields/internal_comments')?>:</span>
                        <span class="field-value overview_comments"><?php echo $szInternalComment; ?></span>
                    </div>
                </td> 
                <td style="width:32%;" valign="top">
                    <?php  
                        if(!empty($bookingDownloadAry))
                        {
                            $szForwarderContactNameDownloadBy='';
                            $szForwarderContactNameDownloadBy=$bookingDownloadAry[0]['szFirstName']." ".$bookingDownloadAry[0]['szLastName'];
                            $szForwarderContactEmailDownloadBy=$bookingDownloadAry[0]['szEmail'];        
                            $szForwarderContactPhoneDownloadBy=$bookingDownloadAry[0]['szPhone'];   
                            ?>
                            <div class="overview">
                                <span class="field-name"><?php echo t($t_base.'fields/forwarder')?>:</span>
                                <span class="field-value truncate-text" title="<?php echo $postSearchAry['szForwarderDispName']; ?>"><?php echo $postSearchAry['szForwarderDispName']; ?></span>
                            </div>
                            <div class="overview">
                                <span class="field-name"><?php echo t($t_base.'fields/contact')?>:</span>
                                <span class="field-value truncate-text" title="<?php echo $szForwarderContactNameDownloadBy; ?>"><?php echo $szForwarderContactNameDownloadBy; ?></span>
                            </div>
                            <div class="overview">
                                <span class="field-name"><?php echo t($t_base.'fields/phone')?>:</span>
                                <span class="field-value truncate-text"><a href="tel:<?php echo str_replace(' ', '',trim($szForwarderContactPhoneDownloadBy)); ?>"><?php echo trim($szForwarderContactPhoneDownloadBy); ?></a></span>
                            </div>
                            <div class="overview">
                                <span class="field-name"><?php echo t($t_base.'fields/email')?>:</span>
                                <span class="field-value truncate-text"><a href="javascript:void(0);" onclick="display_task_menu_details('REMIND','<?php echo $idBookingFile; ?>','DISPLAY_TASK_FORM_MENU','','<?php echo $szForwarderContactEmailDownloadBy; ?>')"><?php echo $szForwarderContactEmailDownloadBy; ?></a></span>
                            </div>
                            <?php
                        }
                        else
                        {
                            if(!empty($quotesAry))
                            {
                                foreach($quotesAry as $quotesArys)
                                { 
                                    $kForwarder = new cForwarder(); 
                                    $idForwarder = $quotesArys['idForwarder'] ;
                                    $kForwarder->load($idForwarder); 

                                    $szEmailFormatted = '';
                                    if($quotesArys['idStatusUpdatedBy']>0)
                                    {
                                        if($quotesArys['iUpdatedByAdmin']==1) //Quote was updated by Transporteca Admin
                                        { 
                                            $kAdmin = new cAdmin();
                                            $kAdmin->getAdminDetails($quotesArys['idStatusUpdatedBy']); 
                                            $szContactDetail = $kAdmin->szFirstName." ".$kAdmin->szLastName." (Admin)" ;
                                            $szForwarderContactStr = $kAdmin->szEmail ;
                                            $szEmailFormatted = $kAdmin->szEmail;

                                            $kConfig_new = new cConfig();
                                            if(!empty($kAdmin->szPhone))
                                            {
                                                $kConfig_new->loadCountry($kAdmin->idInternationalDialCode); 
                                                $szContactPhone = "+".$kConfig_new->iInternationDialCode." ".$kAdmin->szPhone;
                                            }
                                            else
                                            {
                                                $szContactPhone = "N/A";
                                            } 
                                        }
                                        else
                                        {
                                            $kForwarderContact = new cForwarderContact();  
                                            $kForwarderContact->load($quotesArys['idStatusUpdatedBy']); 

                                            $szContactDetail = $kForwarderContact->szFirstName." ".$kForwarderContact->szLastName ;
                                            $szForwarderContactStr = $kForwarderContact->szEmail ;
                                            $szEmailFormatted = $kForwarderContact->szEmail ;

                                            if(!empty($kForwarderContact->szPhone))
                                            {
                                                $szContactPhone =$kForwarderContact->szPhone;
                                            }
                                            else
                                            {
                                                $szContactPhone = "N/A";
                                            } 
                                        } 
                                    }
                                    else
                                    { 
                                        $szContactDetail = "N/A";
                                        if(!empty($quotesArys['szForwarderContact']))
                                        {
                                            $szForwarderContactAry = explode(",",$quotesArys['szForwarderContact']);
                                            if(count($szForwarderContactAry)>1)
                                            {
                                                $szForwarderContactStr = implode('<br>',$szForwarderContactAry);
                                                $szEmailFormatted = implode(",",$szForwarderContactAry);
                                            }
                                            else
                                            {
                                                $szForwarderContactStr = $quotesArys['szForwarderContact'] ;
                                                $szEmailFormatted = $quotesArys['szForwarderContact'] ;
                                            }
                                        }
                                        else
                                        {
                                            $szForwarderContactStr = 'N/A';
                                            $szEmailFormatted = '';
                                        } 

                                        if(!empty($kForwarder->szPhone))
                                        {
                                            $szContactPhone = $kForwarder->szPhone ;
                                        }
                                        else
                                        {
                                            $szContactPhone = "N/A";
                                        }
                                    }  
                                    if(!empty($szContactPhone))
                                    {
                                        $szContactPhone = trim($szContactPhone);
                                    }

                                    ?>
                                    <div class="overview">
                                        <span class="field-name"><?php echo t($t_base.'fields/forwarder')?>:</span>
                                        <span class="field-value truncate-text" title="<?php echo $kForwarder->szDisplayName." (".$kForwarder->szForwarderAlies.")"; ?>"><?php echo $kForwarder->szDisplayName." (".$kForwarder->szForwarderAlies.")"; ?></span>
                                    </div>
                                    <div class="overview">
                                        <span class="field-name"><?php echo t($t_base.'fields/contact')?>:</span>
                                        <span class="field-value truncate-text" title="<?php echo $szContactDetail; ?>"><?php echo $szContactDetail; ?></span>
                                    </div>
                                    <div class="overview">
                                        <span class="field-name"><?php echo t($t_base.'fields/phone')?>:</span>
                                        <span class="field-value truncate-text" title="<?php echo $szContactPhone; ?>"><a href="tel:<?php echo str_replace(' ', '',$szContactPhone); ?>"><?php echo $szContactPhone; ?></a></span>
                                    </div>
                                    <div class="overview">
                                        <span class="field-name"><?php echo t($t_base.'fields/email')?>:</span>
                                        <span class="field-value truncate-text" title="<?php echo $quotesArys['szForwarderContact']; ?>"><a href="javascript:void(0);" <?php if(!empty($szForwarderContactStr) && ($szForwarderContactStr!='N/A')){ ?> onclick="display_task_menu_details('REMIND','<?php echo $idBookingFile; ?>','DISPLAY_TASK_FORM_MENU','','<?php echo $szEmailFormatted; ?>')" <?php } ?>><?php echo $szForwarderContactStr; ?></a></span>
                                    </div>
                                    <hr>
                                    <?php
                                }
                            }
                            else
                            {
                                ?>
                                <div class="overview">
                                    <span class="field-name"><?php echo t($t_base.'fields/forwarder')?>:</span>
                                    <span class="field-value truncate-text" title="<?php echo $postSearchAry['szForwarderDispName']; ?>"><?php echo $postSearchAry['szForwarderDispName']; ?></span>
                                </div>
                                <div class="overview">
                                    <span class="field-name"><?php echo t($t_base.'fields/contact')?>:</span>
                                    <span class="field-value truncate-text" title="<?php echo $postSearchAry['szWarehouseFromContactPerson']; ?>"><?php echo $postSearchAry['szWarehouseFromContactPerson']; ?></span>
                                </div>
                                <div class="overview">
                                    <span class="field-name"><?php echo t($t_base.'fields/phone')?>:</span>
                                    <span class="field-value truncate-text"><a href="tel:<?php echo str_replace(' ', '',trim($kForwarder->szPhone)); ?>"><?php echo trim($kForwarder->szPhone); ?></a></span>
                                </div>
                                <div class="overview">
                                    <span class="field-name"><?php echo t($t_base.'fields/email')?>:</span>
                                    <span class="field-value truncate-text"><a href="javascript:void(0);" onclick="display_task_menu_details('REMIND','<?php echo $idBookingFile; ?>','DISPLAY_TASK_FORM_MENU','','<?php echo $kForwarder->szGeneralEmailAddress; ?>')"><?php echo $kForwarder->szGeneralEmailAddress; ?></a></span>
                                </div>
                                <?php
                            }
                        }
                    ?> 
                </td> 
            </tr>
        </table>
        </div>
    </div> 
    <?php
} 
function display_pending_task_tray_container($kBooking,$iMessageType=false,$bOpenLabelUploadForm=false)
{
    $t_base="management/pendingTray/";  
   
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->idBookingFile ;  
    $szTaskStatus = trim($kBooking->szTaskStatus);
    $szFileStatus =  $kBooking->szFileStatus ; 
    $iSearchType =  $kBooking->iSearchType ;  
    $iNoReferenceFound =  $kBooking->iNoReferenceFound ;  
    $iLabelStatus = $kBooking->iLabelStatus ;  
    
    if($bOpenLabelUploadForm)
    {
        $bookingQuotesAry[1]['idBooking']=$kBooking->idBooking;
        $bookingQuotesAry[1]['idFile']=$kBooking->idBookingFile; 
        ?>
        <div class="scrolling-div-task-pane grey-container-div">
            <div id="pending_task_tray_container">
                <?php
                    if((int)$_REQUEST['iQuoteClosed']==1 || ($iLabelStatus==__LABEL_SEND_FLAG__ || $iLabelStatus==__LABEL_DOWNLOAD_FLAG__))
                    { 
                        display_pending_quotes_overview_pane_courier_admin($bookingQuotesAry,$kBooking,false,true); 
                    }
                    else
                    {
                        display_pending_quotes_overview_pane_courier_admin($bookingQuotesAry,$kBooking);
                    }
                ?>
            </div>
        </div>   
        <?php
    }
    else
    {
        ?> 
        <div class="clearfix accordion-container-task">
            <!--<span class="accordian"><?=t($t_base.'title/task')?></span>-->
        </div>
        <div class="scrolling-div-task-pane grey-container-div">
            <div id="pending_tray_task_heading_container">
                <?php echo display_pending_task_header($kBooking,$idBookingFile);  ?>       
            </div>
            <div id="pending_task_tray_container">
                <?php 
                    
                    if($_SESSION['iShowFileLog_Tab']==1)
                    {
                        unset($_SESSION['iShowFileLog_Tab']);
                       $_SESSION['iShowFileLog_Tab']=0;
                        echo display_task_logs_container($kBooking);
                    }
                    else
                    {
                        if($szTaskStatus=='T1')
                        {
                            echo display_pending_task_validate_form($kBooking,$iMessageType);
                        }
                        else if($szTaskStatus=='T2')
                        {
                            echo display_task_ask_for_quote_container($kBooking);
                        } 
                        else if($szTaskStatus=='T3' && $szFileStatus=='S3')
                        {
                            echo display_task_ask_for_quote_container($kBooking);
                        }
                        else if($szTaskStatus=='T3' && $szFileStatus=='S4')
                        {
                            echo display_task_quote_container($kBooking);
                        }
                        else if($szTaskStatus=='T100' && $szFileStatus=='S2')
                        {
                            echo display_task_estimate_container($kBooking);
                        }
                        else if($szTaskStatus=='T200')
                        {
                            echo display_task_customer_logs_container($kBooking);
                        }
                        else if($szTaskStatus=='T220')
                        {  
                            if($iNoReferenceFound>0)
                            {
                                echo display_task_customer_logs_container($kBooking);
                            }
                            else
                            {
                                echo display_task_logs_container($kBooking);
                            }
                        }
                        else if($szTaskStatus=='T210')
                        {
                            if($iNoReferenceFound>0)
                            {
                                echo display_task_customer_logs_container($kBooking);
                            }
                            else
                            {
                                echo display_task_logs_container($kBooking);
                            } 
                        }
                        else if($szTaskStatus=='T110' || $szTaskStatus=='T120' || $szTaskStatus=='T160' || $iSearchType==1)
                        {
                            echo display_task_reminder_container($kBooking);
                        }
                        else if($szTaskStatus=='T170')
                        { 
                            echo display_task_logs_container($kBooking);
                        }
                        else
                        {
                            echo display_pending_task_validate_form($kBooking,$iMessageType);
                        } 
                    }
                ?>
            </div>
        <?php
    } 
} 

function display_task_ask_for_quote_container($kBooking,$szSuccessType=false,$iNumQuoteSent=false)
{ 
    $t_base="management/pendingTray/";     
    $idBooking = $kBooking->idBooking ;    
    $szMainContainerClass="overview-form afq";
    $szFirstSpanClass="field-name";
    $szSecondSpanClass="field-container";
    
    $kConfig = new cConfig();
    $kForwarder = new cForwarder();
    $kAdmin = new cAdmin(); 
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(true); 
    $kBooking_new = new cBooking(); 
    
    $langArr=$kConfig->getLanguageDetails();
            
    if(!empty($_POST['askForQuoteAry']))
    {
        $askForQuoteAry = $_POST['askForQuoteAry']; 
        $szEmailSubject = $askForQuoteAry['szEmailSubject'];
        $szEmailBody = $askForQuoteAry['szEmailBody'];
        $dtReminderDate = $askForQuoteAry['dtReminderDate'];
        $szReminderTime = $askForQuoteAry['szReminderTime'];
        $idBookingFile = $askForQuoteAry['idBookingFile']; 
        $idBooking = $askForQuoteAry['idBooking'];  
        $szCommentToForwarder = htmlentities($askForQuoteAry['szCommentToForwarder']); 
        
        $bookingDataArr =  $kBooking_new->getBookingDetails($idBooking);
        $idBookingStatus = $bookingDataArr['idBookingStatus'];
        
        $kBooking_new->loadFile($idBookingFile);
        $szBookingFileStatus =  $kBooking_new->szFileStatus;
        $szBookingTaskStatus =  $kBooking_new->szTaskStatus;
        $iBookingSearchedType = $kBooking_new->iSearchType;
        
    }
    else
    { 
        $szBookingFileStatus =  $kBooking->szFileStatus;
        $szBookingTaskStatus =  $kBooking->szTaskStatus;
        $iBookingSearchedType = $kBooking->iSearchType;
        
        $bookingDataArr =  $kBooking_new->getBookingDetails($idBooking); 
        $idBookingStatus = $bookingDataArr['idBookingStatus'];
        $kAdmin->getAdminDetails($_SESSION['admin_id']);
        $idTransportMode = $bookingDataArr['idTransportMode'];
        $szCommentToForwarder = htmlentities($kBooking->szCommentToForwarder);
        
        /*$kConfig = new cConfig();
        $kConfig->loadCountry($kAdmin->idInternationalDialCode);
        $iInternationDialCode = $kConfig->iInternationDialCode;
    
        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry']) ; 
        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	 
        $fCargoVolume = format_volume($bookingDataArr['fCargoVolume']);
        $fCargoWeight = number_format((float)$bookingDataArr['fCargoWeight']);
            
        $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone;
        $kWHSSearch=new cWHSSearch();
//        if($bookingDataArr['iBookingLanguage'] ==__LANGUAGE_ID_DANISH__)
//        {
//            $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE_DANISH__');
//        }
//        else
//        {
            $szCustomerCareNumer = $kWHSSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$bookingDataArr['iBookingLanguage']);
        //} 
        
        $szBaseUrl_str = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);	
        $szBaseUrl = addhttp(__MAIN_SITE_HOME_PAGE_URL__);

        $szSiteLink = "<a href='".$szBaseUrl."' target='_blank'>".$szBaseUrl_str."</a>";
                        
        $replace_ary = array(); 
        $replace_ary['szOriginCity'] = $bookingDataArr['szOriginCity'] ;
        $replace_ary['szOriginCountry'] = $szOriginCountry ;
        $replace_ary['szDestinationCity'] = $bookingDataArr['szDestinationCity'] ;
        $replace_ary['szDestinationCountry'] = $szDestinationCountry ;
        //$replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'] ;
        $replace_ary['fCargoVolume'] = $fCargoVolume ;
        $replace_ary['fCargoWeight'] = $fCargoWeight ; 
        $replace_ary['szQuoteUrl'] = $szQuoteUrl ;  
        $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
        $replace_ary['szTitle'] = $kAdmin->szTitle ;
        $replace_ary['szOfficePhone'] = $szCustomerCareNumer ;
        $replace_ary['szMobile'] = $szMobile ;
        $replace_ary['idBooking'] = $idBooking;
        $replace_ary['iAdminFlag'] = 1;
        $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'] ;
        //$replace_ary['szSiteUrl'] =  $szSiteLink;
        $emailMessageAry = array(); 
        $emailMessageAry = createEmailMessage('__FORWARDER_ASK_FOR_QUOTE__', $replace_ary);*/
        $idBookingFile = $kBooking->idBookingFile ;
        $dtReminderDateTime = $kBooking->dtReminderDate ;
        
        $quotesAry = array();  
        $quotesAry = $kBooking_new->getAllBookingQuotesByFile($idBookingFile,false,false,array(),true);
        
        $showSubjectBodyDivFlag=true;
        if(!empty($quotesAry))
        {
            $showSubjectBodyDivFlag=false;
            foreach($quotesAry  as $quotesArys)
            {
                if($quotesArys['iOffLine']==1)
                {
                    $idForwarder=$quotesArys['idForwarder'];
                    $showSubjectBodyDivFlag=true;
                    break;
                }
            }
        }
        else
        {
            $showSubjectBodyDivFlag=false;
            /*$iDefaultLanguage=$bookingDataArr['iBookingLanguage'];
            $emailMessageAry=createOffEmailData($idBooking,$iDefaultLanguage);
            $szEmailBody = $emailMessageAry['szEmailMessage']; 

            //$szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
            $szEmailSubject = $emailMessageAry['szEmailSubject'];*/
        }
        //echo $idForwarder."idForwarder";
        if((int)$idForwarder>0)
        {
            $kForwarder = new cForwarder();
            $kForwarder->load($idForwarder);
            $idCountryForwarder=$kForwarder->idCountry;
            $kConfig = new cConfig();
            $kConfig->loadCountry($idCountryForwarder);
            $iDefaultLanguage=$kConfig->iDefaultLanguage;
            //echo $kConfig->iDefaultLanguage."iDefaultLanguage";
            $emailMessageAry=createOffEmailData($idBooking,$kConfig->iDefaultLanguage);
            
            $breaksAry = array("<br />","<br>","<br/>");  
            $szEmailBody = $emailMessageAry['szEmailMessage']; 

            //$szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
            $szEmailSubject = $emailMessageAry['szEmailSubject'];
        }   
        $add_js_hour = false;
        if(!empty($dtReminderDateTime) && $dtReminderDateTime!='0000-00-00 00:00:00')
        {  
            $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ;  
            $date = new DateTime($dtReminderDateTime);  
            $date->setTimezone(new DateTimeZone($szCustomerTimeZone));   
            
            $dtReminderDate = $date->format('d/m/Y');
            $szReminderTime = $date->format('H:i');  
            $add_js_hour = false;
        }
        else
        {
            $kWHSSearch=new cWHSSearch();
            $iForwarderResponseTime = $kWHSSearch->getManageMentVariableByDescription('__FORWARDER_RESPONSE_TIME__');

            $dtResponseDateTime = strtotime("+".$iForwarderResponseTime." HOUR"); 
            $dtReminderDate = date('d/m/Y',strtotime("+".$iForwarderResponseTime." HOUR"));
            $szReminderTime = date('H:i',strtotime("+".$iForwarderResponseTime." HOUR")); 
            $add_js_hour = true;
        }
        $quotesAry = array();  
        $quotesAry = $kBooking_new->getAllBookingQuotesByFile($idBookingFile,false,false,array(),true);  
        
    } 
    $iForwarderResponseTime = 20;
    
//    $disabalabelStatusAry = array();
//    $disabalabelStatusAry[0] = 'S7'; //Payment received
//    $disabalabelStatusAry[1] = 'S8'; //Booking sent
//    $disabalabelStatusAry[2] = 'S9'; //Booking confirmed
//    $disabalabelStatusAry[3] = 'S10'; //Booking advice sent
//    $disabalabelStatusAry[4] = 'S130'; //File closed  
            
    $iAlreadyPaidBooking = checkalreadyPaidBooking($postSearchAry['idBookingStatus'],$szBookingTaskStatus,$szBookingFileStatus,$iBookingSearchedType);
    
    $szMainContainerClass="overview-form";
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container"; 
            
    $kWarehouse = new cWHSSearch();  
    $iMaxNumQuotePerfile = $kWarehouse->getManageMentVariableByDescription(__MAX_NUMBER_OF_QUOTES_PER_FILE__); 
    ?>  
    <script type="text/javascript">
        $("#dtReminderDate").datepicker(); 
        <?php  if($add_js_hour){ ?> 
            setDefaultReminderDate('<?php echo $iForwarderResponseTime; ?>');    
        <?php } ?>  
        NiceEditorInstance = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance("szEmailBody",{hasPanel : true});
        
        NiceEditorInstance.addEvent('blur', function() {
            enableSendButton();
        });

    </script>
    <form spellcheck="false" id="pending_task_tray_form" name="pending_task_tray_form" method="post" action="">
    <div class="clearfix">  
        <?php
            if(!empty($kBooking->arErrorMessages['szSpecialError']))
            {
                echo '<div class="red_text">'.$kBooking->arErrorMessages['szSpecialError']." </div> ";
            }
        ?>
        <div id="booking_quote_main_container_table" class="clearfix">  
            <div class="horizontal-scrolling-div-container" >
                <div id="horizontal-scrolling-div-id">
                <?php 
                    $counter = 1; 
                    if(count($_POST['askForQuoteAry']['idTransportMode'])>0 || !empty($quotesAry))
                    { 
                        if(count($_POST['askForQuoteAry']['idTransportMode'])>0)
                        {
                           $loop_counter = count($_POST['askForQuoteAry']['idTransportMode']);  
                        }
                        else
                        {
                            $loop_counter = count($quotesAry);  
                        } 
                        for($j=0;$j<$loop_counter;$j++)
                        {
                            $counter = $j+1;
                            if(count($_POST['askForQuoteAry']['idTransportMode'])>0)
                            {
                                $askForQuoteAry = $_POST['askForQuoteAry']['idTransportMode'];
                                $counter_1 = 1;
                                foreach($askForQuoteAry as $key=>$askForQuoteArys)
                                {
                                    $_POST['askForQuoteAry']['idTransportMode'][$counter_1] = $_POST['askForQuoteAry']['idTransportMode'][$key];
                                    $_POST['askForQuoteAry']['idForwarder'][$counter_1] = $_POST['askForQuoteAry']['idForwarder'][$key];
                                    $_POST['askForQuoteAry']['szForwarderContact'][$counter_1] = $_POST['askForQuoteAry']['szForwarderContact'][$key];
                                    $_POST['askForQuoteAry']['iOffLine'][$counter_1] = $_POST['askForQuoteAry']['iOffLine'][$key];
                                    $_POST['askForQuoteAry']['idBookingQuote'][$counter_1] = $_POST['askForQuoteAry']['idBookingQuote'][$key];
                                    $_POST['askForQuoteAry']['iActive'][$counter_1] = $_POST['askForQuoteAry']['iActive'][$key];
                                    $counter_1++;
                                }
                            }
                            ?>
                            <div id="add_booking_quote_container_<?php echo $j; ?>" class="request-quote-fields"> 
                                <?php
                                    echo display_forwarder_quote($kBooking,$counter,$transportModeListAry,$forwarderListAry,true,$quotesAry[$counter],$bookingDataArr);
                                 ?>
                            </div>
                            <?php
                        }
                    }
                    else
                    {  
                         $kBooking->idTransportMode = $idTransportMode ;
                        ?>
                        <div id="add_booking_quote_container_0" class="request-quote-fields" > 
                             <?php
                                echo display_forwarder_quote($kBooking,$counter,$transportModeListAry,$forwarderListAry,true,array(),$bookingDataArr);
                             ?>
                        </div>
                        <?php 
                    }
                    $style_add_button = "style='display:block;'"; 
                    if($counter >= $iMaxNumQuotePerfile)
                    {
                        $style_add_button = "style='display:none;'";
                    }
                ?>
                <div id="add_booking_quote_container_<?php echo $counter; ?>" style="display:none;"></div>
            </div>   
            </div>
            <div class="quote-button-container" id="request_quote_add_more_button_container" <?php echo $style_add_button; ?>>
                <a class="button1" onclick="add_more_booking_quote('ADD_MORE_BOOKING_QUOTES');" href="javascript:void(0);" id="request_quote_add_more_button">
                    <span style="min-width: 50px;">+ Add</span>
                </a>
            </div>
        </div>
        <!--<div style="float:right;width:6%;margin-top: 50px;"><a href="javascript:void(0)" class="open-button" onclick="add_more_booking_quote('ADD_MORE_BOOKING_QUOTES');">Add More</a></div>-->
        
    </div>
    <div class="clear-all"></div> 
    <div class="email-box-container">
        <div class="clearfix email-fields-container">
            <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/comments_to_forwarder')?></span>
            <span class="<?php echo $szSecondSpanClass; ?>"> 
                <input type="text" name="askForQuoteAry[szCommentToForwarder]" onblur="enableSendButton();" id="szCommentToForwarder" value="<?php echo $szCommentToForwarder; ?>">
            </span>
        </div>
        <div class="clearfix email-fields-container" id="subject-div" <?php if(!$showSubjectBodyDivFlag){?>style="display:none;" <?php }?>>
            <span class="<?php echo $szFirstSpanClass; ?>">Off-line request</span>
            <span class="<?php echo $szSecondSpanClass; ?> wds-request-70"> 
                <input type="text" name="askForQuoteAry[szEmailSubject]" onblur="enableSendButton();" id="szEmailSubject" value="<?php echo $szEmailSubject; ?>">
            </span>
            <span class="quote-field-label-language">   <?php echo t($t_base.'fields/language')?></span>
            <span class="<?php echo $szSecondSpanClass; ?> wds-request-10"> 
                <select id="idLanguage" name="askForQuoteAry[idLanguage]" onchange="updateEmailTemplateForOffLineForwarder(this.value);">
                    <?php 
                    if(!empty($langArr))
                    {
                        foreach($langArr as $langArrs)
                        {?>
                            <option value="<?php echo $langArrs['id'];?>" <?php if($langArrs['id']==$iDefaultLanguage){?> selected <?php }?> ><?php echo $langArrs['szName'];?></option>
                          <?php  
                        }
                        
                    }
                    ?>
                </select>
            </span> 
        </div>
        <div class="clearfix email-fields-container" id="body-div" <?php if(!$showSubjectBodyDivFlag){?>style="display:none;" <?php }?>>
            <textarea style="min-height: 200px;" name="askForQuoteAry[szEmailBody]" id="szEmailBody"><?php echo $szEmailBody; ?></textarea>
        </div> 
        <div class="clearfix reminder-fields-container">
            <span class="<?php echo $szFirstSpanClass; ?> wds-14"><?php echo t($t_base.'fields/remind'); ?></span>
            <span class="<?php echo $szSecondSpanClass; ?>">
                <input type="text" name="askForQuoteAry[dtReminderDate]" id="dtReminderDate" readonly value="<?php echo $dtReminderDate; ?>" onblur="enableSendButton();">
                <input type="text" maxlength="5" name="askForQuoteAry[szReminderTime]" id="szReminderTime" onblur="format_reminder_time(this.value);enableSendButton();" value="<?php echo $szReminderTime; ?>">  
            </span>
            <span class="quote-remider-time" style="float: left;color:#999;font-style: italic;padding: 2px 0 0;width: 5%;">(hh:mm)</span> 
            <?php if($iAlreadyPaidBooking!=1){?>
                <span style="float:right;">
                    <a href="javascript:void(0);" id="request_quote_send_button"  class="button1" style="opacity:0.4"><span>Send</span></a>      
                    <a href="javascript:void(0);" id="request_quote_save_button" class="button1" ><span>Save</span></a>   
                </span>
            <?php } ?>
        </div>
       
        <?php  if($szSuccessType>0){ ?>
        <span class="success-text-div">
            <?php  
            if($szSuccessType==1)
            {
                if($iNumQuoteSent==1)
                {
                    $szSentMessage = (int)$iNumQuoteSent." request was sent";
                }
                else
                {
                    $szSentMessage = (int)$iNumQuoteSent." requests were sent";
                } 
                echo '<br /> <span style="color:green;text-align:center;"> '.$szSentMessage.' </span>'; 
                
            } ?> 
            <?php  if($szSuccessType==2){ echo '<br /> <span style="color:green;text-align:center;float:right"> Saved! </span>'; }   ?>
        </span>
        <?php }  
            if(!empty($kBooking->arErrorMessages))
            {
                echo '<br /> <span style="color:red;text-align:center;float:right">Not Saved! </span>'; 
            }
        ?>
    <div class="clear-all"></div>   
        <input type="hidden" name="askForQuoteAry[hiddenPosition]" id="hiddenPosition" value="<?php echo $counter?>" />
        <input type="hidden" name="askForQuoteAry[hiddenPosition1]" id="hiddenPosition1" value="<?php echo $counter?>" />
        <input type="hidden" name="askForQuoteAry[hiddenPosition2]" id="hiddenPosition2" value="<?php echo $counter?>" />
        <input type="hidden" name="askForQuoteAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>" /> 
        <input type="hidden" name="askForQuoteAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>" />
        <input type="hidden" name="askForQuoteAry[iAlreadyPaidBooking]" id="iAlreadyPaidBooking" value="<?php echo $iAlreadyPaidBooking; ?>" />
        <input type="hidden" name="askForQuoteAry[iMaxNumQuotePerfile]" id="iMaxNumQuotePerfile" value="<?php echo $iMaxNumQuotePerfile; ?>" /> 
    </form> 
    <?php 
    $formId = 'pending_task_tray_form'; 
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    } 
}

function display_task_estimate_container($kBooking,$sorterAry=false)
{ 
    $t_base="management/pendingTray/";    
    $formId = 'pending_task_tray_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    } 
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;  
    $bookingDataArr = $kBooking->getBookingDetails($idBooking); 
    
    $fCustomerExchangeRate = $bookingDataArr['fExchangeRate'];
    $szCustomerCurrency = $bookingDataArr['szCurrency']; 
    $szBookingRandomKey = $bookingDataArr['szBookingRandomNum'];
    
    $estimateSearchAry = array();
    $estimateSearchAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
    $estimateSearchAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
    if(!empty($sorterAry))
    {
        $estimateSearchAry['szSortField'] = $sorterAry['szServiceSortField'];
        $estimateSearchAry['szSortType'] = $sorterAry['szServiceSortType'];
    }
    $bookingEstimateAry = array();
    $bookingEstimateAry = $kBooking->getAllBookingForEstimatePane($estimateSearchAry); 
    
    $quoteEstimateAry = array();
    $quoteEstimateAry = $kBooking->getAllQuoteForEstimatePane($estimateSearchAry);  
    
    ?> 
    <div id="task-estimate-container">
        <div id="estimated-booking-container">
            <?php echo estimate_booking_data_container($bookingEstimateAry,$idBookingFile,'SORT_ESTIMATE_BOOKING_LIST'); ?>
        </div> 
        <div class="clearfix"></div>
        <br>
        <div id="estimated-quote-container">
            <?php echo estimate_booking_data_container($quoteEstimateAry,$idBookingFile,'SORT_ESTIMATE_QUOTE_LIST'); ?>
        </div>  
    </div>
    <input type="hidden" name="szOldField" id="szOldField" value="">
    <input type="hidden" name="szSortType" id="szSortType" value=""> 
    
    <input type="hidden" name="szOldFieldQuote" id="szOldFieldQuote" value="">
    <input type="hidden" name="szSortTypeQuote" id="szSortTypeQuote" value="">
    <?php
} 

function estimate_booking_data_container($bookingEstimateAry,$idBookingFile,$szMode)
{ 
    $t_base="management/pendingTray/";     
    
    ?>
    <div class="clearfix courier-provider courier-provider-scroll" style="max-height:520px;width:99.8%;">	
        <table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" id="mode-transport-table">
            <tr id="table-header"> 
              <td style="cursor:pointer;width:8%;" onclick="sort_task_estimate_list('szTransportMode','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/mode');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szTransportMode">&nbsp;</span></td>
              <td style="cursor:pointer;width:12%;" onclick="sort_task_estimate_list('szServiceType','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/service_terms');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szServiceType">&nbsp;</span></td>
              <td style="cursor:pointer;width:14%;" onclick="sort_task_estimate_list('szShipperCity','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/from_city');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szShipperCity">&nbsp;</span></td>
              <td style="cursor:pointer;width:14%;" onclick="sort_task_estimate_list('szConsigneeCity','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/to_city');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szConsigneeCity">&nbsp;</span></td>
              <td style="cursor:pointer;width:10%;" onclick="sort_task_estimate_list('szForwarderAlias','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/forwarder');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-szForwarderAlias">&nbsp;</span></td>
              <td style="cursor:pointer;width:8%;" onclick="sort_task_estimate_list('fCargoVolume','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/volume');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-fCargoVolume">&nbsp;</span></td>
              <td style="cursor:pointer;width:8%;" onclick="sort_task_estimate_list('fCargoWeight','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/weight');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-fCargoWeight">&nbsp;</span></td>
              <td style="cursor:pointer;width:14%;" onclick="sort_task_estimate_list('fTotalPriceCustomerCurrency','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo t($t_base.'fields/price');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-fTotalPriceCustomerCurrency">&nbsp;</span></td>
              <td style="cursor:pointer;width:12%;" onclick="sort_task_estimate_list('dtBookingConfirmed','<?php echo $idBookingFile; ?>','<?php echo $szMode; ?>')"><strong><?php echo ($szMode=='SORT_ESTIMATE_QUOTE_LIST')?t($t_base.'fields/date_of_quote'):t($t_base.'fields/date_of_booking');?></strong> <span class="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>" id="moe-transport-sort-span<?php echo $szQuoteSuffix; ?>-id-dtBookingConfirmed">&nbsp;</span></td>
          </tr>   	
      <?php
          if(!empty($bookingEstimateAry))
          {
              foreach($bookingEstimateAry as $bookingEstimateArys)
              { 
                  $szServiceType = ''; 
                  if(!empty($bookingEstimateArys['szShortTerms']))
                  {
                      $szServiceType = $bookingEstimateArys['szShortTerms'] ;
                  }
                  else
                  {
                      $szServiceType = $bookingEstimateArys['szShortName'];
                  }
                  if(empty($bookingEstimateArys['szTransportMode']))
                  {
                      $bookingEstimateArys['szTransportMode'] = 'Sea';
                  }

                  $fBookingPriceUsd = $bookingEstimateArys['fTotalPriceCustomerCurrency'] * $bookingEstimateArys['fCustomerExchangeRate']; 
                  //echo "<br> USD: ".$fBookingPriceUsd." Rate: ".$fCustomerExchangeRate." ORG: ".$bookingEstimateArys['fTotalPriceCustomerCurrency'] ;
                  if($fCustomerExchangeRate>0)
                  {
                      $fBookingPrice = ($fBookingPriceUsd /$fCustomerExchangeRate);
                  }
                  else
                  {
                      $fBookingPrice = $fBookingPriceUsd ;
                      $szCustomerCurrency = 'USD';
                  }
                  if($szMode=='SORT_ESTIMATE_QUOTE_LIST')
                  {
                      $dtBookingDate = date('d M, Y',strtotime($bookingEstimateArys['dtCreatedOn']));
                  }
                  else
                  {
                     $dtBookingDate = date('d M, Y',strtotime($bookingEstimateArys['dtBookingConfirmed']));
                  }
                  
                    $maxWeightText='';
                    if($bookingEstimateArys['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
                    {
                        $convertWeightFromVolume=round_up($bookingEstimateArys['fCargoVolume']*__VOLUME_CONVERT_INTO_WEIGHT_COURIER__);
                        $maxWeightConvertByVolume=round((float)(max($bookingEstimateArys['fCargoWeight'],$convertWeightFromVolume)));
                        if((float)$maxWeightConvertByVolume>0)
                        {
                            $maxWeightText=" (".$maxWeightConvertByVolume.")";
                        }
                    }
                    else if($bookingEstimateArys['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
                    {
                        $convertWeightFromVolume=round_up($bookingEstimateArys['fCargoVolume']*__VOLUME_CONVERT_INTO_WEIGHT_AIR__);
                        $maxWeightConvertByVolume=round((float)(max($bookingEstimateArys['fCargoWeight'],$convertWeightFromVolume)));
                        if((float)$maxWeightConvertByVolume>0)
                        {
                            $maxWeightText=" (".$maxWeightConvertByVolume.")";
                        }
                    }
                    $szShipperAdrress =  $bookingEstimateArys['szOriginCountry'];
                    $szConsigneeAdrress =  $bookingEstimateArys['szDestinationCountry'];
      ?>
                 <tr id="truckin_tr____<?php echo $bookingEstimateArys['id']; ?>" title="<?php echo ($szMode=='SORT_ESTIMATE_QUOTE_LIST')?$bookingEstimateArys['szFileRef']:$bookingEstimateArys['szBookingRef'];?>"> 
                      <td><?php echo $bookingEstimateArys['szTransportMode']?></td>
                      <td><?php echo $szServiceType; ?></td>
                      <td><span onclick="open_pending_google_map('ESTIMATE','<?php echo $szShipperAdrress; ?>');" style="cursor: pointer;"><?php echo $bookingEstimateArys['szShipperCity']?></span></td>
                      <td><span onclick="open_pending_google_map('ESTIMATE','<?php echo $szConsigneeAdrress; ?>');" style="cursor: pointer;"><?php echo $bookingEstimateArys['szConsigneeCity']; ?></span></td>
                      <td><?php echo $bookingEstimateArys['szForwarderAlias']; ?></td>
                      <td><?php echo format_volume($bookingEstimateArys['fCargoVolume']); ?></td>
                      <td><?php echo number_format((float)$bookingEstimateArys['fCargoWeight'])."".$maxWeightText; ?></td>
                      <td><?php echo $szCustomerCurrency." ".number_format((float)$fBookingPrice);?></td>
                      <td><?php echo $dtBookingDate;?></td>
                  </tr> 
              <?php		
              } 				 
          }
          else
          {?>
            <tr>
               <td colspan="9" align="center">
                   <h4><b><?=t($t_base.'fields/no_record_found');?></b></h4>
               </td>
            </tr>
          <?php   }   ?>  
         </table>
      </div>
    <?php
}

function display_file_owner_confimation($kBooking,$iCourierBooking=false)
{
    $t_base="management/pendingTray/";     
    $iCrmEmail = false;
    $idReminderNotes = false;
    if($kBooking->szTaskStatus == 'T210')
    {
        $iCrmEmail = 1;
    }
    else if($kBooking->szTaskStatus == 'T220')
    {
        $idReminderTask = $kBooking->idReminderTask;
    }
    $szOnclickFunc = "display_pending_task_details('booking_data_".$ctr."','".$incompleteTaskArys['idBookingFile']."','".$iDisplayPopup."','".$incompleteTaskArys['idBookingQuote']."','".$incompleteTaskArys['iQuoteClosed']."','".$incompleteTaskArys['iBookingFlag']."','','".$iCrmEmail."','".$idReminderTask."')"; 
    ?> 
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup" style="width:430px;"> 
            <h3><?=t($t_base.'title/popup_heading');?></h3> <br/>
            <?php
            if((int)$kBooking->idFileOwner == 0)
            {?>
                <p><?=t($t_base.'title/text_currently_not_allocated_to_anyone');?></p> <br/>
            <?php }else if((int)$kBooking->idFileOwner >0){
                    $kAdmin = new cAdmin();
                    $kAdmin->getAdminDetails($kBooking->idFileOwner);
                    $szName= $kAdmin->szFirstName." ".$kAdmin->szLastName;
                ?>
                <p><?php echo t($t_base.'title/this_file_currently_owned_by')." ".$szName.".";?></p> <br/>
            <?php }?>
            <?php if($iCourierBooking==1){?>
                <p><?php echo t($t_base.'title/yes_popup');?>: <?=t($t_base.'title/yes_popup_description_courier');?></p>
                <p><?php echo t($t_base.'title/no_popup');?>: <?=t($t_base.'title/no_popup_description_courier');?></p>
            <?php } else {?>
                <p><?php echo t($t_base.'title/yes_popup');?>: <?=t($t_base.'title/yes_popup_description');?></p>
                <p><?php echo t($t_base.'title/no_popup');?>: <?=t($t_base.'title/no_popup_description');?></p>
            <?php }?>
            <br/>
            <p align="center">
                <?php if($iCourierBooking==1){ ?>
                    <a href="javascript:void(0)" onclick="display_pending_task_overview('DISPLAY_PENDING_TASK_COURIER_LABEL_UPLOAD_FORM','<?php echo $kBooking->idBookingFile; ?>');" class="button1" ><span><?=t($t_base.'fields/no_popup');?></span></a> 
                    <a href="javascript:void(0)" class="button1" onclick="display_pending_task_overview('ADD_BOOKING_FILE_OWNER','<?php echo $kBooking->idBookingFile; ?>','','','','1');"><span><?php echo t($t_base.'fields/yes_popup');?></span></a>
                <?php } else { ?>
                    <a href="javascript:void(0)" onclick="display_pending_task_overview('DISPLAY_PENDING_TASK_DETAILS','<?php echo $kBooking->idBookingFile; ?>','','','','','','','','<?php echo $iCrmEmail; ?>','<?php echo $idReminderTask; ?>');" class="button1" ><span><?=t($t_base.'fields/no_popup');?></span></a> 
                    <a href="javascript:void(0)" class="button1" onclick="display_pending_task_overview('ADD_BOOKING_FILE_OWNER','<?php echo $kBooking->idBookingFile; ?>','','','','','','','','<?php echo $iCrmEmail; ?>','<?php echo $idReminderTask; ?>');"><span><?php echo t($t_base.'fields/yes_popup');?></span></a>
                <?php } ?> 
            </p>
        </div>
    </div>
    <?php
}
function display_pending_task_popup($incompleteTaskAry,$mode='RECENT_TASK',$kBooking=false,$incompleteTaskArySearch=array(),$iRecentPopupToggler=false)
{
    $t_base="management/pendingTray/"; 
    if($mode=='RECENT_TASK')
    {
        if($iRecentPopupToggler==__FILE_RECENT_VISITED__)
        {
            $szPopupTitle = t($t_base.'title/recent_popup_heading_seen');
        }
        else
        {
            $szPopupTitle = t($t_base.'title/recent_popup_heading');
        }
        ?> 
        <div id="popup-bg" onclick="showHide('contactPopup')" style="cursor:pointer;"></div>
        <div id="popup-container">
            <div class="task-popup" id="task-popup-id-recent"> 
                <div class="clearfix accordion-container">
                    <span class="accordian"><?php echo $szPopupTitle; ?></span>
                    <span class="recent-popup-toggle-container"> 
                        <span class="checkbox-ab"><input type="radio" name="iRecentPopupToggler" id="iRecentPopupToggler_1" value="<?php echo __FILE_RECENT_VISITED__; ?>" <?php echo ($iRecentPopupToggler==__FILE_RECENT_VISITED__)?'checked':''; ?> onclick="display_recent_files_by_owner('DISPLAY_RECENT_FILES_SEEN_BY_OPERATOR','<?php echo __FILE_RECENT_VISITED__; ?>');">Seen</span>
                        <span class="checkbox-ab"><input type="radio" name="iRecentPopupToggler" id="iRecentPopupToggler_2" value="<?php echo __FILE_RECENT_MODIFIED__; ?>" <?php echo ($iRecentPopupToggler==__FILE_RECENT_MODIFIED__)?'checked':''; ?> onclick="display_recent_files_by_owner('DISPLAY_RECENT_FILES_SEEN_BY_OPERATOR','<?php echo __FILE_RECENT_MODIFIED__; ?>');">Modified</span>
                    </span>
                </div>  
                <?php echo display_all_recent_task_by_operator($incompleteTaskAry,$mode); ?>
            </div> 
        </div> 
        <script type="text/javascript"> 
            $().ready(function(){   
                $(".task-popup").fadeIn(300,function(){$(this).focus();});  
            });
        </script>
        <?php
    }
    else 
    {
         ?> 
        <div id="popup-bg" onclick="showHide('contactPopup')" style="cursor:pointer;"></div>
        <div id="popup-container" style="margin-top:-50px;">
            <div class="task-popup" id="task-popup-id-recent"> 
                <div id="pending_task_search_form_container">
                    <?php
                        echo display_pending_task_search_form($kBooking,$incompleteTaskAry);
                    ?>
                </div> 
                <div id="pending_task_searched_list_container">
                    <?php echo display_all_recent_task_by_operator($incompleteTaskAry,$mode,'',1,$incompleteTaskArySearch); ?>
                </div>
            </div> 
        </div>
        <script type="text/javascript"> 
            $().ready(function() {   
                $(".task-popup").fadeIn(300,function(){$(this).focus();});  
            });
        </script>
        <?php
    }
    ?>
    <?php
}
function display_all_recent_task_by_operator($incompleteTaskAry,$mode='RECENT_TASK',$kBooking=false,$iPageNumber=1,$incompleteTaskArySearch=array())
{ 
    $t_base="management/pendingTray/"; 
    if($mode=='RECENT_TASK')
    {
        $szStyle = 'style="max-height:250px;"';
    }
    else
    {
        $idCRMEmailLogs = $incompleteTaskAry['idCRMEmailLogs']; 
    
        // in case of SEARCH popup we recieved form data as first parameter ($incompleteTaskAry ) and in case of RECENT popup we receive actual array 
        $szStyle = 'style="max-height:300px;"'; 
        $kBooking_new = new cBooking();
        /*
        *  This code block is used for pagination
        *  $iTotalNumRecords = $kBooking_new->getAllBookingWithOpenedTask($incompleteTaskAry,false,true);  
        *  $iTotalNumRecords = 500;
        */
       if(!empty($incompleteTaskArySearch))
       {
           $incompleteTaskAry =$incompleteTaskArySearch;
       }
       else 
       {
           $incompleteTaskAry = $kBooking_new->getAllBookingWithOpenedTaskSearchPopup($incompleteTaskAry,false,false,$iPageNumber);
       }
        
            
        /*
         * This code block is used for pagination
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iTotalNumRecords,__CONTENT_PER_PAGE_PENDING_TRAY_SEARCH__,5); 
         * 
         */
    }
    
    $uagent = $_SERVER['HTTP_USER_AGENT'];
    $szExtraClass = '';
    $szOSName = os_info($uagent) ; 
    if($szOSName=='MacOS' || $szOSName=='iPad' || $szOSName=='iPhone')
    {
        $szExtraClass = ' mac-table-header'; 
    }  
    if($idCRMEmailLogs>0)
    {
        $szIdSuffix = "_".$idCRMEmailLogs;
    }
    ?>    
    <div> 
        <script>
            $(document).keydown(function(event){
                <?php if($szOSName=='MacOS' || $szOSName=='iPad' || $szOSName=='iPhone')
                {?>                           
                if(event.which=="91")
                    cntrlIsPressed = true;
                <?php }else{?>       
                if(event.which=="17")
                    cntrlIsPressed = true;
                <?php }?> 
            });

        $(document).keyup(function(){
            cntrlIsPressed = false;
        });

var cntrlIsPressed = false;
            </script>
    <table cellpadding="5" cellspacing="0" class="table-scroll-head <?php echo $szExtraClass; ?>" width="100%">
        <tr>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/file')?></th> 
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/booking');?></th>
            <th style="width:13%;" valign="top"><?=t($t_base.'fields/status');?></th>
            <th style="width:13%;" valign="top"><?=t($t_base.'fields/customer');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/company');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/mode');?></th> 
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/from');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/to');?></th>
            <th style="width:14%;" valign="top"><?=t($t_base.'fields/cargo_text');?></th>
        </tr>
    </table> 
    <div id="pending_task_list_container_popup" class="pending-tray-scrollable-div"> 
        <div class="scrolling-div pending-tray-lists" <?php echo $szStyle; ?>>
            <table cellpadding="5" cellspacing="0" class="format-3" width="100%" id="pending_task_tray_popup<?php echo $szIdSuffix; ?>">
                <?php echo showSearchBookingTables($incompleteTaskAry,false,$mode,$idCRMEmailLogs); ?>
            </table>     
        </div> 
    </div> 
    <?php
        if($mode=='CRM_FILE_LINKS')
        {
            //This section is called from Pending Tray > Customer log
        } 
        else
        {
            ?>
            <div class="btn-container clearfix">
                <table style="width:100%;">
                    <tr>
                        <td>
                            <?php
                                if($mode!='RECENT_TASK')
                                { 
                                    ?>
                                    <a href="javascript:void(0);" id="pending_task_show_more_files" onclick="show_more_files_search('pending_task_serch_form');" class="button1">Show more files</a> 
                                    <input type="hidden" name="bookingFileSearchAry[iPageCounter]" id="iPageCounter" value="1">
                                    <input type="hidden" name="bookingFileSearchAry[iButtonAlreadyClicked]" id="iButtonAlreadyClicked" value="0">
                                    <?php
                                } 
                            ?> 
                        </td>
                        <td align="center">
                            <span style="text-align:center;"></span>
                        </td>
                        <td> 
                            <span style="float:right">
                                <a href="javascript:void(0);" class="button1" id="recent_task_quick_quote_button" style="opacity:0.4;"><span>Quick Quote</span></a>
                                <a href="javascript:void(0);" class="button1" onclick="showHide('contactPopup');"><span><?php echo t($t_base.'fields/close_window')?></span></a>
                                <a href="javascript:void(0);" class="button1" id="recent_task_open_button" style="opacity:0.4;"><span><?php echo t($t_base.'fields/open')?></span></a>
                                <a href="javascript:void(0);" class="button1" id="recent_task_copy_button" style="opacity:0.4;"><span><?php echo t($t_base.'fields/copy')?></span></a>
                            </span> 
                        </td>
                    </tr> 
                </table> 
            </div>
        <?php } ?> 
</div>
<?php   
}  

function showSearchBookingTables($incompleteTaskAry,$iPageNumber=false,$mode=false,$idCRMEmailLogs=false)
{ 
    $t_base="management/pendingTray/";  
    if(!empty($incompleteTaskAry))
    {
        if($iPageNumber>0)
        {
            $ctr = (($iPageNumber-1) * __CONTENT_PER_PAGE_PENDING_TRAY_SEARCH__) + 1; 
        }
        else
        {
            $ctr = 1; 
        }
        
        $irowCount = count($incompleteTaskAry); 
        $kConfig = new cConfig();
        $fileStatusAry = array(); 
        $fileStatusAry = $kConfig->getAllActiveFileStatus();

        $transportModeListAry = array();
        $transportModeListAry = $kConfig->getAllTransportMode(false,false,true);
 
        foreach($incompleteTaskAry as $incompleteTaskArys)
        {   
            $szVolWeight = format_volume($incompleteTaskArys['fCargoVolume'])."cbm / ".number_format((float)$incompleteTaskArys['fCargoWeight'])."kg ";

            $szCustomerName = $incompleteTaskArys['szFirstName']." ".$incompleteTaskArys['szLastName'] ;
            //$szCustomerName = returnLimitData($szCustomerName,20) ;
            $szCustomerNameHover = $szCustomerName; //This text will displayed as mouse over string
            if(strlen($szCustomerName)>20)
            {
                $szCustomerName = substrwords($szCustomerName,17)."...";
            }   
            
            //$szCustomerCompanyName = returnLimitData($incompleteTaskArys['szCustomerCompanyName'],20) ;
            $szCustomerCompanyName = $incompleteTaskArys['szCustomerCompanyName'];
            $szCustomerCompanyNameHover = $szCustomerCompanyName; //This text will displayed as mouse over string
            if(strlen($szCustomerCompanyName)>20)
            {
                $szCustomerCompanyName = substrwords($szCustomerCompanyName,17)."...";
            }
            if($incompleteTaskArys['iQuickQuote']>0)
            {
                $szTask = $fileStatusAry[$incompleteTaskArys['szFileStatus']]['szQuickQuoteDescription'];
            }
            else
            {
                $szTask = $fileStatusAry[$incompleteTaskArys['szFileStatus']]['szDescription'];
            }  
            if($incompleteTaskArys['iQuickQuote']==__QUICK_QUOTE_DRAFT_QUOTE__)
            {
                $szTask = __S1_DRAFT_QUOTE_DESCRIPTION__;
            }
            $szFileReference = $incompleteTaskArys['szFileRef'];

            if($incompleteTaskArys['iCourierBooking']==1)
            {
                $szTransportMode="Courier";
                if($incompleteTaskArys['iLabelStatus']==__LABEL_SEND_FLAG__)
                {
                   $szTask="Labels sent"; 
                }
            }
            else
            {    
                //echo "<br> Mode: ".$incompleteTaskArys['idTransportMode'];
                if($incompleteTaskArys['idTransportMode']>0)
                {
                    $szTransportMode = $transportModeListAry[$incompleteTaskArys['idTransportMode']]['szShortName'] ; 
                }
                else
                {
                    $szTransportMode = __TRANSPORTECA_DEFAULT_TRANSPORT_MODE__;
                } 
            }
            $szBookingReference='';
            if($incompleteTaskArys['szBookingRef']!='' && ($incompleteTaskArys['idBookingStatus']=='3' || $incompleteTaskArys['idBookingStatus']=='4' || $incompleteTaskArys['idBookingStatus']=='7'))
            {
                $szBookingReference = $incompleteTaskArys['szBookingRef']; 
            }
            if($mode=='CRM_FILE_LINKS')
            {
                $szCrmOperationFormId = "crm_email_operations_form_".$idCRMEmailLogs;
                ?>
                <tr id="recent_task_popup_<?=$ctr?>" onclick="reassign_crm_email_to_file('<?php echo $incompleteTaskArys['idBookingFile']?>','<?php echo $szCrmOperationFormId; ?>');" style="cursor:pointer;">
                <?php
            }
            else
            {
                ?>
                <tr id="recent_task_popup_<?=$ctr?>" ondblclick="display_pending_task_overview('DISPLAY_PENDING_TASK_DETAILS','<?php echo $incompleteTaskArys['idBookingFile']?>','<?php echo $mode; ?>','','','<?php echo $incompleteTaskArys['iCourierBooking']?>','<?php echo $incompleteTaskArys['iClosed']?>','<?php echo $incompleteTaskArys['idBooking']?>');close_open_div('pending_task_main_list_container_div','pending_task_list_open_close_link','close');" onclick="activate_recent_task_details('recent_task_popup_<?=$ctr?>','<?php echo $incompleteTaskArys['idBookingFile']?>','<?php echo $mode; ?>','<?php echo $incompleteTaskArys['iBookingFlag']?>',true,'<?php echo $szFileReference?>')" style="cursor:pointer;">
                <?php
            }
            ?> 
                <td style="width:10%;"><?php echo $szFileReference; ?></td>
                <td style="width:10%;"><?php echo $szBookingReference; ?></td>
                <td style="width:13%;"><?php echo $szTask; ?></td>
                <td style="width:13%;" title="<?php echo $szCustomerNameHover; ?>"><?php echo $szCustomerName ; ?></td>
                <td style="width:10%;" title="<?php echo $szCustomerCompanyNameHover; ?>"><?php echo $szCustomerCompanyName ; ?></td>
                <td style="width:10%;"><?php echo $szTransportMode; ?></td> 
                <td style="width:10%;"><?php echo $incompleteTaskArys['szOriginCity']; ?></td>
                <td style="width:10%;"><?php echo $incompleteTaskArys['szDestinationCity'];; ?></td>
                <td style="width:14%;"><?php echo $szVolWeight; ?></td>
            </tr>
            <?php
            $ctr++;
        } 
    }
    else
    {
        ?> 
        <tr>
            <td colspan="9" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
        </tr> 
<?php }  
}
function display_delete_booking_file_confirmation($kBooking,$szFromPage='RECENT_TASK')
{
    $t_base="management/pendingTray/";   
    
    if($szFromPage=='RECENT_TASK')
    {
        $szMode = 'DISPLAY_RECENT_FILES_SEEN_BY_OPERATOR' ;
    }
    else
    { 
        $szMode = 'DISPLAY_SEARCH_PENDING_TASK_POPUP';
    }
    
    ?> 
    <div id="popup-bg" onclick="showHide('contactPopup')" style="cursor:pointer;"></div>
    <div id="popup-container">
        <div class="popup"> 
            <h5><?php echo t($t_base.'title/delete_file_heading')?></h5> 
            <p><?php echo t($t_base.'title/delete_file_message')?>?</p>
            <br>
            <div class="btn-container">
                <a href="javascript:void(0);" class="button1" onclick="display_recent_files_by_owner('<?php echo $szMode; ?>');"><span><?php echo t($t_base.'fields/no_popup')?></span></a>
                <a href="javascript:void(0);" class="button1" onclick="display_pending_task_overview('DELETE_PENDING_TASK_CONFIRM','<?php echo $kBooking->idBookingFile; ?>','<?php echo $szFromPage; ?>');"><span><?php echo t($t_base.'fields/yes_popup')?></span></a>
            </div>
        </div>
    </div>
    <?php
}
function display_pending_task_search_form($kBooking,$incompleteTaskAry=array())
{ 
    $t_base="management/pendingTray/"; 
    
    $kConfig = new cConfig();
    $kForwarder = new cForwarder();
    $kAdmin = new cAdmin();
    $kBooking_new = new cBooking(); 
    $kForwarderCont = new cForwarderContact();
    
    $fileStatusAry = array();
    $fileStatusAry = $kConfig->getAllActiveFileStatus();
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    
    $forwarderContactAry = array();
    if($idForwarder>0)
    { 
        $forwarderContactAry = $kForwarderCont->getAllForwardersContact($idForwarder,false);
    }
    else
    {
        $forwarderContactAry = $kForwarderCont->getAllForwardersContact(false,false);
    } 
    $szFieldWidth = "width:99%;";
    if(!empty($_POST['searchPendingTaskAry']))
    {
        $searchPendingTaskAry = $_POST['searchPendingTaskAry']; 
    }
    else
    {
        $searchPendingTaskAry = $incompleteTaskAry;
    }
    
    $szFreeText = $searchPendingTaskAry['szFreeText'];
    $szFileRef = $searchPendingTaskAry['szFileRef'];
    $szBookingRef = $searchPendingTaskAry['szBookingRef'];
    $szFileStatus = $searchPendingTaskAry['szFileStatus'];
    $szCustomerName = $searchPendingTaskAry['szCustomerName'];
    $idTransportMode = $searchPendingTaskAry['idTransportMode'];
    $szOriginCity = $searchPendingTaskAry['szOriginCity'];
    $szDestinationCity = $searchPendingTaskAry['szDestinationCity'];
    $fCargoVolume = $searchPendingTaskAry['fCargoVolume'];
    $fCargoWeight = $searchPendingTaskAry['fCargoWeight'];
    $iNumColli = $searchPendingTaskAry['iNumColli'];
    
    ?>    
<form id="pending_task_serch_form" id="pending_task_serch_form" action="" method="post">   
            
    <div class="file-search-form-container">
        <div class="clearfix accordion-container">
            <span class="accordian">Search</span>
        </div>
        <table cellpadding="0" class="task-search-form-container" cellspacing="0" width="100%">
            <tr>
                <td valign="top" style="width:100%;">
                    <div class="clearfix email-fields-container" style="width:100%;border-bottom:solid 1px #7f7f7f;"> 
                        <span class="quote-field-container" style="width:75%;">
                            <?=t($t_base.'fields/free_text')?><br>
                            <input type="text" name="searchPendingTaskAry[szFreeText]" class="search-form-element" id="szFreeText" value="<?php echo $szFreeText; ?>"  onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');">
                        </span> 
                        <span class="quote-field-container" style="width:25%;text-align: right;">&nbsp;<br>&nbsp;&nbsp;
                            <a href="javascript:void(0)" class="gray-btn" onclick="display_recent_files_by_owner('DISPLAY_SEARCH_PENDING_TASK_POPUP','','CLEAR_ALL')"><span>Clear All</span></a>
                            <a href="javascript:void(0)" class="button1" onclick="search_pending_task(0)" id="pending_task_search_popup_button"><span>Search</span></a>  
                        </span>
                    </div> 
                </td>  
            </tr>  
        </table>  
    </div>   
    <div style="padding-right:17px; border-bottom: 1px solid #7f7f7f;" class="clearfix">
        <table cellpadding="0" cellspacing="0" class="task-search-form-container" width="100%">
            <tr>
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/file')?><br>
                    <input type="text" name="searchPendingTaskAry[szFileRef]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szFileRef" value="<?php echo $szFileRef; ?>" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');">
                    <input type="hidden" name="searchPendingTaskAry[idCustomer]"  id="idCustomer" value="<?php echo $_POST['searchPendingTaskAry']['idCustomer']?>">
                    <input type="hidden" name="searchPendingTaskAry[iFlag]"  id="iFlag" value="<?php echo $_POST['searchPendingTaskAry']['iFlag']?>">
                </td> 
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/booking')?><br>
                    <input type="text" name="searchPendingTaskAry[szBookingRef]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szBookingRef" value="<?php echo $szBookingRef; ?>" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');">
                </td>
                <td valign="top" style="width:13%;">
                    <?=t($t_base.'fields/status')?><br>
                    <select name="searchPendingTaskAry[szFileStatus]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szFileStatus_popup">
                        <option value="">All</option>
                    <?php
                        if(!empty($fileStatusAry))
                        {
                           foreach($fileStatusAry as $fileStatusArys)
                           {
                            ?>
                            <option value="<?php echo $fileStatusArys['szShortDesc']; ?>" <?php echo (($fileStatusArys['szShortDesc']==$szFileStatus)?'selected':''); ?>><?php echo $fileStatusArys['szDescription']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </td>
                <td valign="top" style="width:13%;" >
                    <?=t($t_base.'fields/customer')?><br>
                    <input type="text" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');" name="searchPendingTaskAry[szCustomerName]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szCustomerName_popup" value="<?php echo $szCustomerName; ?>">
                </td> 
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/mode')?><br>
                    <select name="searchPendingTaskAry[idTransportMode]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="idTransportMode_popup">
                        <option value="">All</option>
                    <?php
                        if(!empty($transportModeListAry))
                        {
                           foreach($transportModeListAry as $transportModeListArys)
                           {
                            ?>
                            <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($transportModeListArys['id']==$idTransportMode)?'selected':''); ?>><?php echo $transportModeListArys['szShortName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </td>
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/from')?><br>
                    <input type="text" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');" name="searchPendingTaskAry[szOriginCity]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szOriginCity_popup" value="<?php echo $szOriginCity; ?>">
                </td>
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/to')?><br>
                    <input type="text" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');" name="searchPendingTaskAry[szDestinationCity]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szDestinationCity_popup" value="<?php echo $szDestinationCity; ?>">
                </td>
                <td valign="top" style="width:8%;">
                    <?=t($t_base.'fields/volume')?><br>
                    <input type="text" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');" name="searchPendingTaskAry[fCargoVolume]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="fCargoVolume_popup" value="<?php echo $fCargoVolume; ?>">
                </td>
                <td valign="top" style="width:8%;">
                    <?=t($t_base.'fields/weight')?><br>
                    <input type="text" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');" name="searchPendingTaskAry[fCargoWeight]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="fCargoWeight_popup" value="<?php echo $fCargoWeight; ?>">
                </td>
                <td valign="top" style="width:8%;">
                    <?=t($t_base.'fields/colli')?><br>
                    <input type="text" onkeypress="submitEnterKeySearchPopup(event,'SEARCH_POPUP_PENDINGTRAY');" name="searchPendingTaskAry[iNumColli]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="iNumColli_popup" value="<?php echo $iNumColli; ?>">
                </td>
            </tr> 
        </table> 
    </div> 
</form>
    <?php
}
function display_pending_task_search_form_backup($kBooking)
{ 
    $t_base="management/pendingTray/"; 
    
    $kConfig = new cConfig();
    $kForwarder = new cForwarder();
    $kAdmin = new cAdmin();
    $kBooking_new = new cBooking();
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(true); 
    
      $activeAdminAry = array();
//    $activeAdminAry = $kAdmin->getAllActiveStoreAdmin(); 

    $customerOwnerAry = array();
    $customerOwnerAry = $kAdmin->viewManagementDetails(false,true,true);
    
    $activeAdminAry = $customerOwnerAry; //From now Customer owner and File option are same. 
    
    $fileStatusAry = array();
    $fileStatusAry = $kConfig->getAllActiveFileStatus();
    
    $customerCompanyAry = array();
    $customerCompanyAry = $kBooking_new->getDistinctCompanyName('CUSTOMER_NAME_SEARCH');
    
    $kForwarderCont = new cForwarderContact();
    $forwarderContactAry = array();
    if($idForwarder>0)
    { 
        $forwarderContactAry = $kForwarderCont->getAllForwardersContact($idForwarder,false);
    }
    else
    {
        $forwarderContactAry = $kForwarderCont->getAllForwardersContact(false,false);
    } 
    $szFieldWidth = "width:99%;";
    ?>    
<form id="pending_task_serch_form" id="pending_task_serch_form" action="" method="post">   
        <script type="text/javascript"> 
        // Checks a string to see if it in a valid date format
        // of (D)D/(M)M/(YY)YY and returns true/false
        function isValidDate(s) {
            // format D(D)/M(M)/(YY)YY
            var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{1,4}$/;

            if (dateFormat.test(s)) {
                // remove any leading zeros from date values
                s = s.replace(/0*(\d*)/gi,"$1");
                var dateArray = s.split(/[\.|\/|-]/);

                // correct month value
                dateArray[1] = dateArray[1]-1;

                // correct year value
                if (dateArray[2].length<4) {
                    // correct year value
                    dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
                }

                var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
                if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
        $(document).ready(function(){ 
            // submiting form on enter key press
            $('.search-form-element').keyup(function(e) {
                if(e.keyCode == 13) 
                {
                    search_pending_task(0);
                }
            }); 
        }); 
        
        $("#dtRequestReceivedAfter").datepicker({
                onClose: function( selectedDate ) {
                    if(isValidDate(selectedDate))
                    {
                        var date_split = selectedDate.split('/');  
                        var _day = date_split[0];
                        var _month = date_split[1];
                        var _year = date_split[2];

                        var date_picker_argument = new Date(_year, _month-1,_day) ; 
                        $( "#dtRequestReceivedBefore" ).datepicker( "option", "minDate", date_picker_argument );
                    }
                }
           });   

            $("#dtRequestReceivedBefore").datepicker({
                onClose: function( selectedDate ) {
                    if(isValidDate(selectedDate))
                    {
                        var date_split = selectedDate.split('/');  
                        var _day = date_split[0];
                        var _month = date_split[1];
                        var _year = date_split[2];

                        var date_picker_argument = new Date(_year, _month-1,_day); 
                        $("#dtRequestReceivedAfter").datepicker("option","maxDate",date_picker_argument);
                    }
                }
            });   
    </script>     
    <div class="file-search-form-container">
        <div class="clearfix accordion-container">
            <span class="accordian">Search</span>
        </div>
        <table cellpadding="0" class="task-search-form-container" cellspacing="0" width="100%">
            <tr>
                <td valign="top" style="width:33%;">
                    <?=t($t_base.'fields/free_text')?><br>
                    <input type="text" name="searchPendingTaskAry[szFreeText]" class="search-form-element" id="szFreeText" value="<?php echo $_POST['searchPendingTaskAry']['szFreeText']?>">
                </td> 
                <td valign="top" style="width:33%;">
                    <?=t($t_base.'fields/forwarder')?><br>
                    <select name="searchPendingTaskAry[idForwarder]" class="search-form-element" id="idForwarder_popup" onchange="add_forwarder_contact_dropdown(this.value);">
                        <option value="">All</option>
                        <?php
                        if(!empty($forwarderListAry))
                        {
                           foreach($forwarderListAry as $forwarderListArys)
                           {
                            ?>
                            <option value="<?php echo $forwarderListArys['id']; ?>" <?php echo (($forwarderListArys['id']==$idForwarder)?'selected':''); ?>><?php echo $forwarderListArys['szDisplayName']; ?></option>
                            <?php
                           }
                        }
                        ?> 
                    </select>
                </td>
                <td valign="top" style="width:33%;">
                    <?=t($t_base.'fields/insurance')?><br>
                    <select name="searchPendingTaskAry[iInsuranceIncluded]" class="search-form-element" id="iInsuranceIncluded_popup">
                        <option value="10001">All</option>
                        <option value="1" <?php echo (($iInsuranceIncluded==1)?'selected':''); ?>>Yes</option>
                        <option value="2" <?php echo (($iInsuranceIncluded==2)?'selected':''); ?>>No</option>
                        <option value="3" <?php echo (($iInsuranceIncluded==3)?'selected':''); ?>>Optional</option> 
                    </select>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <?=t($t_base.'fields/file_owner')?><br>
                    <select name="searchPendingTaskAry[idFileOwner]" class="search-form-element" id="idFileOwner_popup">
                         <option value="10001">All</option>
                         <option value="10002"> Not Allocated</option>
                    <?php
                        if(!empty($activeAdminAry))
                        {
                           foreach($activeAdminAry as $activeAdminArys)
                           {
                            ?>
                            <option value="<?php echo $activeAdminArys['id']; ?>" <?php echo (($activeAdminArys['id']==$idFileOwner)?'selected':''); ?>><?php echo $activeAdminArys['szFirstName']." ".$activeAdminArys['szLastName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </td> 
                <td valign="top">
                    <?=t($t_base.'fields/forwarder_contact')?><br>
                    <span id="forwarder_contact_container_span">
                        <select name="searchPendingTaskAry[idForwarderContact]" class="search-form-element" id="idForwarderContact_popup">
                            <option value="">All</option>
                            <?php
                                if(!empty($forwarderContactAry))
                                {
                                   foreach($forwarderContactAry as $forwarderContactArys)
                                   {
                                        $szForwarderContactName = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
                                        $szForwarderContactName = trim($szForwarderContactName);
                                        if(!empty($szForwarderContactName))
                                        {
                                            ?>
                                            <option value="<?php echo $forwarderContactArys['id']; ?>" <?php echo (($forwarderContactArys['id']==$idForwarderContact)?'selected':''); ?>><?php echo $szForwarderContactName; ?></option>
                                            <?php
                                        } 
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </td>
                <td valign="top">
                    <?=t($t_base.'fields/request_received_after')?><br>
                    <input type="text" name="searchPendingTaskAry[dtRequestReceivedAfter]" class="search-form-element" readonly="" id="dtRequestReceivedAfter" value="<?php echo $_POST['searchPendingTaskAry']['dtRequestReceivedAfter']?>">
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <?=t($t_base.'fields/customer_owner')?><br>
                    <select name="searchPendingTaskAry[idCustomerOwner]" class="search-form-element" id="idCustomerOwner_popup">
                        <option value="">All</option>
                    <?php
                        if(!empty($customerOwnerAry))
                        {
                           foreach($customerOwnerAry as $customerOwnerArys)
                           {
                            ?>
                            <option value="<?php echo $customerOwnerArys['id']; ?>" <?php echo (($customerOwnerArys['id']==$idFileOwner)?'selected':''); ?>><?php echo $customerOwnerArys['szFirstName']." ".$customerOwnerArys['szLastName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </td> 
                <td valign="top">
                    <?=t($t_base.'fields/company')?><br> 
                    <select name="searchPendingTaskAry[szCompanyName]" class="search-form-element" id="szCompanyName_popup">
                        <option value="">All</option>
                    <?php
                        if(!empty($customerCompanyAry))
                        {
                           $szCompanyName =  $_POST['searchPendingTaskAry']['szCompanyName'] ; 
                           foreach($customerCompanyAry as $customerCompanyArys)
                           {
                            ?>
                            <option value="<?php echo $customerCompanyArys['szCompanyName']; ?>" <?php echo (($customerCompanyArys['szCompanyName']==$szCompanyName)?'selected':''); ?>><?php echo $customerCompanyArys['szCompanyName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </td>
                <td valign="top">
                    <?=t($t_base.'fields/request_received_before')?><br>
                    <input type="text" name="searchPendingTaskAry[dtRequestReceivedBefore]" class="search-form-element" readonly="" id="dtRequestReceivedBefore" value="<?php echo $_POST['searchPendingTaskAry']['dtRequestReceivedBefore']?>">
                </td>
            </tr>
        </table> 
        <div class="btn-container-pending-tray clearfix" style="text-align:right;">  
            <a href="javascript:void(0)" class="gray-btn" onclick="display_recent_files_by_owner('DISPLAY_SEARCH_PENDING_TASK_POPUP')"><span>Clear All</span></a>
            <a href="javascript:void(0)" class="button1" onclick="search_pending_task(0)" id="pending_task_search_popup_button"><span>Search</span></a>  
        </div>
    </div> 
    <div style="padding-right:17px; border-bottom: 1px solid #7f7f7f;" class="clearfix">
        <table cellpadding="0" cellspacing="0" class="task-search-form-container" width="100%">
            <tr>
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/file')?><br>
                    <input type="text" name="searchPendingTaskAry[szFileRef]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szFileRef" value="<?php echo $_POST['searchPendingTaskAry']['szFileRef']?>">
                </td> 
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/booking')?><br>
                    <input type="text" name="searchPendingTaskAry[szBookingRef]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szBookingRef" value="<?php echo $_POST['searchPendingTaskAry']['szBookingRef']?>">
                </td>
                <td valign="top" style="width:13%;">
                    <?=t($t_base.'fields/status')?><br>
                    <select name="searchPendingTaskAry[szFileStatus]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szFileStatus_popup">
                        <option value="">All</option>
                    <?php
                        if(!empty($fileStatusAry))
                        {
                           foreach($fileStatusAry as $fileStatusArys)
                           {
                            ?>
                            <option value="<?php echo $fileStatusArys['szShortDesc']; ?>" <?php echo (($fileStatusArys['szShortDesc']==$szFileStatus)?'selected':''); ?>><?php echo $fileStatusArys['szDescription']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </td>
                <td valign="top" style="width:13%;" >
                    <?=t($t_base.'fields/customer')?><br>
                    <input type="text" name="searchPendingTaskAry[szCustomerName]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szCustomerName_popup" value="<?php echo $_POST['searchPendingTaskAry']['szCustomerName']?>">
                </td> 
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/mode')?><br>
                    <select name="searchPendingTaskAry[idTransportMode]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="idTransportMode_popup">
                        <option value="">All</option>
                    <?php
                        if(!empty($transportModeListAry))
                        {
                           foreach($transportModeListAry as $transportModeListArys)
                           {
                            ?>
                            <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($transportModeListArys['id']==$idTransportMode)?'selected':''); ?>><?php echo $transportModeListArys['szShortName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </td>
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/from')?><br>
                    <input type="text" name="searchPendingTaskAry[szOriginCity]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szOriginCity_popup" value="<?php echo $_POST['searchPendingTaskAry']['szOriginCity']; ?>">
                </td>
                <td valign="top" style="width:10%;">
                    <?=t($t_base.'fields/to')?><br>
                    <input type="text" name="searchPendingTaskAry[szDestinationCity]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="szDestinationCity_popup" value="<?php echo $_POST['searchPendingTaskAry']['szDestinationCity']; ?>">
                </td>
                <td valign="top" style="width:8%;">
                    <?=t($t_base.'fields/volume')?><br>
                    <input type="text" name="searchPendingTaskAry[fCargoVolume]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="fCargoVolume_popup" value="<?php echo $_POST['searchPendingTaskAry']['fCargoVolume']; ?>">
                </td>
                <td valign="top" style="width:8%;">
                    <?=t($t_base.'fields/weight')?><br>
                    <input type="text" name="searchPendingTaskAry[fCargoWeight]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="fCargoWeight_popup" value="<?php echo $_POST['searchPendingTaskAry']['fCargoWeight']; ?>">
                </td>
                <td valign="top" style="width:8%;">
                    <?=t($t_base.'fields/colli')?><br>
                    <input type="text" name="searchPendingTaskAry[iNumColli]" class="search-form-element" style="<?php echo $szFieldWidth; ?>" id="iNumColli_popup" value="<?php echo $_POST['searchPendingTaskAry']['iNumColli']; ?>">
                </td>
            </tr> 
        </table> 
    </div> 
</form>
    <?php
}
function display_pending_task_validate_form($kBooking,$szSuccessType=false)
{
    $t_base="management/pendingTray/";    
    $formId = 'pending_task_tray_form';
            
    $idBooking = $kBooking->idBooking ; 
    $kBooking_new = new cBooking();
    $postSearchAry = $kBooking_new->getExtendedBookingDetails($idBooking);
            
    $kConfig = new cConfig();
    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
    
    $kVatApplication = new cVatApplication();
    $manualFeeCargoTypeArr = array();
    $manualFeeCargoTypeArr = $kVatApplication->getManualFeeCaroTypeList();
            
//    $disabalabelStatusAry = array();
//    $disabalabelStatusAry[0] = 'S7'; //Payment received
//    $disabalabelStatusAry[1] = 'S8'; //Booking sent
//    $disabalabelStatusAry[2] = 'S9'; //Booking confirmed
//    $disabalabelStatusAry[3] = 'S10'; //Booking advice sent
//    $disabalabelStatusAry[4] = 'S130'; //File closed  
//            
//    $iAlreadyPaidBooking = 0;  
//    if(in_array($szBookingFileStatus,$disabalabelStatusAry)|| $postSearchAry['idBookingStatus']==3 || $postSearchAry['idBookingStatus']==4)
//    {
//        $iAlreadyPaidBooking = 1;
//    } 
    $iAlreadyPaidBooking = checkalreadyPaidBooking($postSearchAry['idBookingStatus'],'',$szBookingFileStatus);
    
    if(!empty($_POST['updatedTaskAry']))
    {
        $updatedTaskAry = $_POST['updatedTaskAry'] ;
        $szFirstName = $updatedTaskAry['szFirstName'];
        $szLastName = $updatedTaskAry['szLastName'];
        $szCustomerCompanyName = $updatedTaskAry['szCustomerCompanyName'];
        $szCustomerPhoneNumber = $updatedTaskAry['szCustomerPhoneNumber'];
        $idCustomerDialCode = $updatedTaskAry['idCustomerDialCode'];
        $szEmail = $updatedTaskAry['szEmail']; 
        $isMoving = $updatedTaskAry['isMoving'];  
        $idUser = $updatedTaskAry['idUser'];
        
        $kUser = new cUser();
        $kUser->getUserDetails($idUser);
        
        $idBookingFile = $updatedTaskAry['idBookingFile'];
        $idTransportMode = $updatedTaskAry['idTransportMode'];
        $szOriginPostcode = $updatedTaskAry['szOriginPostcode'];
        $szOriginCity = $updatedTaskAry['szOriginCity'];
        $idOriginCountry = $updatedTaskAry['idOriginCountry']; 
        $szDestinationPostcode = $updatedTaskAry['szDestinationPostcode']; 
        $szDestinationCity = $updatedTaskAry['szDestinationCity'];
        $szOriginAddress = $updatedTaskAry['szOriginAddress'];
        $szDestinationAddress = $updatedTaskAry['szDestinationAddress'];
        $idDestinationCountry = $updatedTaskAry['idDestinationCountry'];  
        $fCargoWeight = $updatedTaskAry['fCargoWeight']; 
        $fCargoVolume = $updatedTaskAry['fCargoVolume'];
        $iNumColli = $updatedTaskAry['iNumColli'];  
        $idInsuranceCurrency = $updatedTaskAry['idInsuranceCurrency'];  
        $iInsuranceIncluded = $updatedTaskAry['iInsuranceIncluded'];  
        $szInternalComment = $updatedTaskAry['szInternalComment']; 
        $dtTimingDate = $updatedTaskAry['dtTimingDate'];  
        $fTotalInsuranceCostForBookingCustomerCurrency = $updatedTaskAry['fTotalInsuranceCostForBookingCustomerCurrency'];  
        
        $idCargo = $updatedTaskAry['idCargo'];    
        $idBooking = $updatedTaskAry['idBooking'];    
        $idServiceTerms = $updatedTaskAry['idServiceTerms']; 
        
        $iPrivateShipping = $updatedTaskAry['iPrivateShipping'];  
        $idCustomerCurrency = $updatedTaskAry['idCustomerCurrency'];  
        $iBookingLanguage = $updatedTaskAry['iBookingLanguage'];  
        $iAcceptNewsUpdate = $updatedTaskAry['iAcceptNewsUpdate'];  
        $idFileOwner = $updatedTaskAry['idFileOwner']; 
        $idCustomerOwner = $updatedTaskAry['idCustomerOwner'];  
        $idCustomerCountry = $updatedTaskAry['idCustomerCountry'];  
        
        if($isMoving==1)
        { 
            $szCargoDescription = $updatedTaskAry['szCargoDescriptionHidden'];   
        }
        else
        {
            $szCargoDescription = $updatedTaskAry['szCargoDescription'];   
        }
        if(!empty($updatedTaskAry['szHandoverCity']))
        {
            $szHandoverCity = $updatedTaskAry['szHandoverCity'] ;
        }
        else
        {
            $szHandoverCity = $updatedTaskAry['szHandoverCityHidden'];
        }
        
        $szCustomerAddress1 = $updatedTaskAry['szCustomerAddress1']; 
        $szCustomerPostCode = $updatedTaskAry['szCustomerPostCode']; 
        $szCustomerCity = $updatedTaskAry['szCustomerCity']; 
        $szCustomerCountry = $updatedTaskAry['szCustomerCountry']; 
        
        $szShipperCompanyName = $updatedTaskAry['szShipperCompanyName']; 
        $szShipperFirstName = $updatedTaskAry['szShipperFirstName']; 
        $szShipperLastName = $updatedTaskAry['szShipperLastName']; 
        $idShipperDialCode = $updatedTaskAry['idShipperDialCode']; 
        $szShipperPhoneNumber = $updatedTaskAry['szShipperPhoneNumber']; 
        $szShipperEmail = $updatedTaskAry['szShipperEmail']; 
        
        $szConsigneeCompanyName = $updatedTaskAry['szConsigneeCompanyName']; 
        $szConsigneeFirstName = $updatedTaskAry['szConsigneeFirstName']; 
        $szConsigneeLastName = $updatedTaskAry['szConsigneeLastName']; 
        $idConsigneeDialCode = $updatedTaskAry['idConsigneeDialCode']; 
        $szConsigneePhoneNumber = $updatedTaskAry['szConsigneePhoneNumber']; 
        $szConsigneeEmail = $updatedTaskAry['szConsigneeEmail'];
        $szCustomerCompanyRegNo = $updatedTaskAry['szCustomerCompanyRegNo'];
        
        $iShipperConsignee = $updatedTaskAry['iShipperConsignee'];
        $iSendTrackingUpdates = $updatedTaskAry['iSendTrackingUpdates']; 
        $szCargoType = $updatedTaskAry['szCargoType'];  
        $iCargoDescriptionLocked = $updatedTaskAry['iCargoDescriptionLocked'];  
         
        $kBooking_new->loadFile($idBookingFile);
        $szBookingFileStatus =  $kBooking_new->szFileStatus;
        $szBookingTaskStatus =  $kBooking_new->szTaskStatus;
        $iBookingSearchedType = $kBooking_new->iSearchType;
    }
    else
    {
        $szBookingFileStatus =  $kBooking->szFileStatus;
        $szBookingTaskStatus =  $kBooking->szTaskStatus;
        $iBookingSearchedType = $kBooking->iSearchType; 
        
        $cargoDetailsAry = $kBooking_new->getCargoComodityDeailsByBookingId($idBooking); 
        $szCargoDescription = '';
        if(!empty($cargoDetailsAry))
        {
            $szCargoDescription = utf8_decode($cargoDetailsAry['1']['szCommodity']); 
            $idCargo = utf8_decode($cargoDetailsAry['1']['id']); 
        }  
        $szFirstName = $postSearchAry['szFirstName'];
        $szLastName = $postSearchAry['szLastName'];
        $szCustomerCompanyName = $postSearchAry['szCustomerCompanyName'];
        $szCustomerPhoneNumber = $postSearchAry['szCustomerPhoneNumber'];
        $idCustomerDialCode = $postSearchAry['idCustomerDialCode']; 
        $szEmail = $postSearchAry['szEmail'];  
        $idUser = $postSearchAry['idUser'];
        $isMoving = $postSearchAry['isMoving']; 
        $szCargoType = $postSearchAry['szCargoType'];
        $iCargoDescriptionLocked = $postSearchAry['iCargoDescriptionLocked'];
        if(empty($szCargoType) && $isMoving==1)
        {
            $szCargoType = '__PERSONAL_EFFECTS__'; 
        }
        else if(empty($szCargoType))
        {
            $szCargoType = '__GENERAL_CODE__'; 
        }
        $szHandoverCity = $postSearchAry['szHandoverCity'];
        $szCustomerCompanyRegNo = $postSearchAry['szCustomerCompanyRegNo'];
        
            
        $kUser = new cUser();
        $kUser->getUserDetails($idUser); 
        $idBookingFile = $kBooking->idBookingFile; 
        $idTransportMode = $postSearchAry['idTransportMode'];
        $iSendTrackingUpdates = $postSearchAry['iSendTrackingUpdates'];
        
        $szOriginPostcode = $postSearchAry['szShipperPostCode'];
        $szOriginAddress = $postSearchAry['szShipperAddress'];
        
        if(!empty($postSearchAry['szShipperCity']))
        {
            $szOriginCity = $postSearchAry['szShipperCity'];
        }
        else
        {
            $szOriginCity = $postSearchAry['szOriginCity'];
        } 
        $idOriginCountry = $postSearchAry['idOriginCountry'];  
        $szDestinationPostcode = $postSearchAry['szConsigneePostCode']; 
        $szDestinationAddress = $postSearchAry['szConsigneeAddress']; 
        
        if(!empty($postSearchAry['szConsigneeCity']))
        {
            $szDestinationCity = $postSearchAry['szConsigneeCity'];
        }
        else
        {
            $szDestinationCity = $postSearchAry['szDestinationCity'];
        }
        
        $idDestinationCountry = $postSearchAry['idDestinationCountry'];  
        
        $fCargoWeight = '';
        $fCargoVolume = '';
        $iNumColli = '';
        if(!empty($postSearchAry['fCargoWeight']) && (float)$postSearchAry['fCargoWeight']>0.00)
        {
            $fCargoWeight = round_up($postSearchAry['fCargoWeight'],2); 
        }
        
        if(!empty($postSearchAry['fCargoVolume']) && (float)$postSearchAry['fCargoVolume']>0.00)
        {
            $fCargoVolume = format_volume($postSearchAry['fCargoVolume']); 
        }
        if(!empty($postSearchAry['iNumColli']))
        {
            $iNumColli = round((float)$postSearchAry['iNumColli']); 
        } 
            
        $idInsuranceCurrency = $postSearchAry['idGoodsInsuranceCurrency'];  
        $iInsuranceIncluded = $postSearchAry['iInsuranceChoice']; 
        
        $szInternalComment = str_replace("\r\n","<br>",$postSearchAry['szInternalComment']);
        $breaksAry = array("<br />","<br>","<br/>");  
        $szInternalComment = str_replace($breaksAry, "\r\n", $szInternalComment);  
        
        $idServiceTerms = $postSearchAry['idServiceTerms'];  
        
        $iPrivateShipping = $postSearchAry['iPrivateShipping'];  
        $idCustomerCurrency = $postSearchAry['idCustomerCurrency'];  
        
        if($iAlreadyPaidBooking==1)
        {
            $iBookingLanguage = $postSearchAry['iBookingLanguage'];  
        }
        else
        {
            $iBookingLanguage = $kUser->iLanguage;   
        }
        
        $iAcceptNewsUpdate = $kUser->iAcceptNewsUpdate;   
        $idCustomerCountry = $kUser->szCountry ;
        $idCustomerOwner = $kBooking->idCustomerOwner;   
        $idFileOwner = $kBooking->idFileOwner;    
        
//        if($idFileOwner<=0)
//        {
//            $idFileOwner = $idCustomerOwner; 
//        }
        $fTotalInsuranceCostForBookingCustomerCurrency = round((float)$postSearchAry['fValueOfGoods']);  
        
        if(empty($idInsuranceCurrency))
        {
            $idInsuranceCurrency = $postSearchAry['idCurrency']; 
        } 
        $dtTimingDate = date('d/m/Y',strtotime($postSearchAry['dtTimingDate'])); 
            
        $szCustomerAddress1 = $postSearchAry['szCustomerAddress1']; 
        $szCustomerPostCode = $postSearchAry['szCustomerPostCode']; 
        $szCustomerCity = $postSearchAry['szCustomerCity']; 
        $szCustomerCountry = $postSearchAry['szCustomerCountry']; 
        
        $szShipperCompanyName = $postSearchAry['szShipperCompanyName']; 
        $szShipperFirstName = $postSearchAry['szShipperFirstName']; 
        $szShipperLastName = $postSearchAry['szShipperLastName']; 
        $idShipperDialCode = $postSearchAry['idShipperDialCode']; 
        $szShipperPhoneNumber = $postSearchAry['szShipperPhoneNumber']; 
        $szShipperEmail = $postSearchAry['szShipperEmail']; 
        
        $szConsigneeCompanyName = $postSearchAry['szConsigneeCompanyName']; 
        $szConsigneeFirstName = $postSearchAry['szConsigneeFirstName']; 
        $szConsigneeLastName = $postSearchAry['szConsigneeLastName']; 
        $idConsigneeDialCode = $postSearchAry['idConsigneeDialCode']; 
        $szConsigneePhoneNumber = $postSearchAry['szConsigneePhoneNumber']; 
        $szConsigneeEmail = $postSearchAry['szConsigneeEmail'];
        $iShipperConsignee = $postSearchAry['iShipperConsignee']; 
            
        if($idShipperDialCode<=0)
        {
            $idShipperDialCode = $idOriginCountry;
        } 
        if($idConsigneeDialCode<=0)
        {
            $idConsigneeDialCode = $idDestinationCountry;
        }
    }  
    
    /*
    * Checking Insurance is available for this or not. By default we assume we will have Insurance untill shipper/consignee selected
    */
    $iTransportationInsuranceAvailable = 1;
    if($idOriginCountry>0 && $idDestinationCountry>0 && $idTransportMode>0)
    {
        $insuranceInputAry = array();
        $insuranceInputAry['idOriginCountry'] = $idOriginCountry;
        $insuranceInputAry['idDestinationCountry'] = $idDestinationCountry;
        $insuranceInputAry['idTransportMode'] = $idTransportMode;
        $insuranceInputAry['isMoving'] = $isMoving;
        $insuranceInputAry['szCargoType'] = $szCargoType; 
        $insuranceInputAry['fTotalBookingPriceUsd'] = 1;
        
        $kInsurance = new cInsurance();
        if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry))
        {
            /*
            * This means we do have Transportation Insurance for this Trade and should show all available option on this screen
            */ 
            $iTransportationInsuranceAvailable = 1;
        }
        else
        {
            /*
            * This means we do not have Transportation Insurance for this Trade and should show only 'Not available' option to select
            */  
            $iTransportationInsuranceAvailable = 2;
        }
    }
    
    $szMainContainerClass="overview-form";
    $szFirstSpanClass="field-name";
    $szSecondSpanClass="field-container";
    $szSecondSpanClass_name = "name-container";
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    
    //$serviceTermsAry = array();
    //$serviceTermsAry = $kConfig->getAllServiceTerms();
    
    $serviceTermsAry = array();
    $serviceTermsAry = $kConfig->getAllServiceTerms(false,false,true);
    
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);
        
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);
        
    $kAdmin_new = new cAdmin();
    $activeAdminAry = array();
//    $activeAdminAry = $kAdmin_new->getAllActiveStoreAdmin(); 
    
            
    $customerOwnerAry = array();
    $customerOwnerAry = $kAdmin_new->viewManagementDetails(false,false,true);
    
    $langArr=$kConfig->getLanguageDetails();
    
    $activeAdminAry = $customerOwnerAry ;
    
    $szVerfified_yes = 'style="display:none;"';
    $szVerfified_no = 'style="display:none;"';
    
    $szMouseOverText_yes = "Email verified";
    $szMouseOverText_no = "Email not verified";
    if($kUser->iConfirmed==1)
    { 
        $szVerfified_yes = 'style="display:inline-block;"';
        $iCustomerVerified = 1;
    } 
    else
    { 
        $szVerfified_no = 'style="display:inline-block;"';
        $iCustomerVerified = 0;
    }   
    
    if($postSearchAry['iSearchMiniVersion'] == 7 || $postSearchAry['iSearchMiniVersion'] == 8 || $postSearchAry['iSearchMiniVersion'] == 9)
    {
        if($iPrivateShipping == 1)
        {
            if(empty($szCustomerCompanyName))
            {
                $szCustomerCompanyName = $szFirstName." ".$szLastName;
            }
        }
    }
    if($kUser->iConfirmed!=1)
    {
        ?>
        <script type="text/javascript">
            check_verified_email('<?php echo $szEmail; ?>','<?php echo $idUser; ?>','<?php echo $idBookingFile; ?>',1)
        </script>
        <?php
    }  
    ?> 
    <script type="text/javascript"> 
      
        
        
        $().ready(function(){	
            $("#dtTimingDate").datepicker();  
            <?php
            if(!empty($szValidationErrorKey))
            {
                ?>		       	
                $('html, body').animate({ scrollTop: ($("#<?php echo $szValidationErrorKey; ?>").offset().top - 60) }, "7000");
                <?php
            }
        ?>  
            $("#szFirstName").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false,
                select: function( event, ui ) 
                { 
                    cosole.log("Selected");
                } 
            });
            
            $("#szLastName").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false
            }); 
            $("#szCustomerCompanyName").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false
            }); 
            $("#szEmail").autocomplete(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php", {
                width: 260,
                matchContains: true,
                mustMatch: true, 
                selectFirst: false
            });    
        });
    </script>
    <form id="pending_task_tray_form" name="pending_task_tray_form" method="post" action="">
<!--    <table cellpadding="5" cellspacing="0" class="format-4" width="100%" >
        <tr>
            <th style="width:50%;text-align:center;border-bottom:0px;" valign="top"><?php echo t($t_base.'fields/customer')?></th> 
            <th style="width:50%;text-align:center;border-bottom:0px;" valign="top"><?php echo t($t_base.'fields/file')?></th>  
        </tr>
    </table> -->
    <table cellpadding="5" cellspacing="0" class="task-table" width="100%" >
        <tr>
            <td style="width:50%;" valign="top" id="validate_pane_customer_container">
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/customer')?>:</span>
                    <span class="<?php echo $szSecondSpanClass_name; ?>">  
                        <input type="text" name="updatedTaskAry[szFirstName]" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" id="szFirstName" placeholder="First Name" value="<?php echo $szFirstName; ?>">
                        <input type="text" name="updatedTaskAry[szLastName]"  onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" id="szLastName" placeholder="Last Name" value="<?php echo $szLastName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/company')?>:</span>
                    <span class="single-field">
                        <input type="text" name="updatedTaskAry[szCustomerCompanyName]" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onblur="enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="szCustomerCompanyName" placeholder="Company Name" value="<?php echo $szCustomerCompanyName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/registration')?>:</span>
                    <span class="company-registration">
                        <input type="text" name="updatedTaskAry[szCustomerCompanyRegNo]" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="szCustomerCompanyRegNo" placeholder="Company Registration" value="<?php echo $szCustomerCompanyRegNo; ?>">
                    </span>
                    <span class="private-cb">    
                        <input type="checkbox" name="updatedTaskAry[iPrivateShipping]"  onclick="toggleCompanyField('iPrivateShipping');enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="iPrivateShipping" value="1" <?php echo (($iPrivateShipping==1)?'checked':''); ?>> <?php echo t($t_base.'fields/private')?>
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/phone')?>:</span>
                    <span class="phone-container">
                        <select size="1" name="updatedTaskAry[idCustomerDialCode]" onchange="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" id="idCustomerDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');">
                            <?php
                               if(!empty($dialUpCodeAry))
                               {
                                    $usedDialCode = array();
                                    foreach($dialUpCodeAry as $dialUpCodeArys)
                                    {
                                        if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                        {
                                            $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                            ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idCustomerDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                        }
                                    }
                                }
                            ?>
                        </select> 
                        <input type="text" name="updatedTaskAry[szCustomerPhoneNumber]" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" id="szCustomerPhoneNumber" value="<?php echo $szCustomerPhoneNumber; ?>" />
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/email')?>:</span>
                    <span class="pending-task-email"> 
                        <input type="text" name="updatedTaskAry[szEmail]" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');check_verified_email(this.value,'<?php echo $idUser; ?>','<?php echo $idBookingFile; ?>',1);" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);check_verified_email(this.value,'<?php echo $idUser; ?>','<?php echo $idBookingFile; ?>',1);enableAskforquoteButton('pending_task_tray_form');" id="szEmail" value="<?php echo $szEmail; ?>" >
                        <span id="confirmed_email_task_yes" <?php echo $szVerfified_yes; ?>> 
                            <a href="javascript:void(0);" class="verfied-yes" title="<?php echo $szMouseOverText_yes; ?>">&nbsp;</a> 
                        </span>
                        <span id="confirmed_email_task_no" <?php echo $szVerfified_no; ?>>
                            <a href="javascript:void(0);" class="verfied-no" title="<?php echo $szMouseOverText_no; ?>">&nbsp;</a> 
                        </span> 
                    </span>
                    <span class="verify-button">
                        <a href="javascript:void(0);" id="verify_email_button" class="gray-btn"><span><?php echo t($t_base.'fields/verify_email')?></span></a> 
                    </span>
                </div>  
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/billing')." ".t($t_base.'fields/address').":"?></span>
                    <span class="single-field">  
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onblur="enableAskforquoteButton('pending_task_tray_form');enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szCustomerAddress1]" id="szCustomerAddress1" placeholder="Billing Street Address" value="<?php echo $szCustomerAddress1; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="field-space">  
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onblur="check_postcode_field(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szCustomerPostCode]" id="szCustomerPostCode" placeholder="Billing Postcode" value="<?php echo $szCustomerPostCode; ?>">
                    </span>
                    <span class="field-space">
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szCustomerCity]" id="szCustomerCity" placeholder="Billing City" value="<?php echo $szCustomerCity; ?>">
                    </span>
                    <span class="field-nospace">
                        <select name="updatedTaskAry[szCustomerCountry]" id="szCustomerCountry" onchange = "autofill_billing_address__new_rfq();enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                   foreach($allCountriesArr as $allCountriesArrs)
                                   {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$szCustomerCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/currency')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="updatedTaskAry[idCustomerCurrency]" id="idCustomerCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableAskforquoteButton('pending_task_tray_form');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($currencyAry))
                                {
                                   foreach($currencyAry as $currencyArys)
                                   {
                                    ?>
                                    <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idCustomerCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select> 
                    </span>
                    <span class="label-language"><?php echo t($t_base.'fields/language')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="updatedTaskAry[iBookingLanguage]" id="iBookingLanguage" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableAskforquoteButton('pending_task_tray_form');">
                            <option value="">Select</option>
                            <?php
                            if(!empty($langArr))
                            {
                                foreach($langArr as $langArrs)
                                {?>
                                    <option value="<?php echo $langArrs['id']; ?>" <?php echo (($langArrs['id']==$iBookingLanguage)?'selected':''); ?>><?php echo ucfirst($langArrs['szName']); ?></option>
                                   <?php 
                                }
                            }?>
                            
<!--                            <option value="<?php echo __LANGUAGE_ID_ENGLISH__; ?>" <?php echo ((__LANGUAGE_ID_ENGLISH__==$iBookingLanguage)?'selected':''); ?>><?php echo ucfirst(__LANGUAGE_TEXT_ENGLISH__); ?></option>-->
                        </select> 
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/newsletter')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="updatedTaskAry[iAcceptNewsUpdate]" id="iAcceptNewsUpdate" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableAskforquoteButton('pending_task_tray_form');">
                            <option value="">Select</option>
                            <option value="1" <?php echo (($iAcceptNewsUpdate==1)?'selected':''); ?>>Yes</option>
                            <option value="2" <?php echo (($iAcceptNewsUpdate==2)?'selected':''); ?>>No</option> 
                        </select> 
                    </span>
                    <span class="label-language"><?php echo t($t_base.'fields/customer_owner')?>:</span>
                    <span class="currency-dropdown"> 
                        <select name="updatedTaskAry[idCustomerOwner]" id="idCustomerOwner">
                             <option value="">Not allocated</option> 
                            <?php
                                if(!empty($customerOwnerAry))
                                {
                                   foreach($customerOwnerAry as $activeAdminArys)
                                   {
                                    ?>
                                    <option value="<?php echo $activeAdminArys['id']; ?>" <?php echo (($activeAdminArys['id']==$idCustomerOwner)?'selected':''); ?>><?php echo $activeAdminArys['szFirstName']." ".$activeAdminArys['szLastName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </div> 
                <div class="dotted-line">&nbsp;</div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>" id="customer_role_radio_container"><?php echo t($t_base.'fields/customer_role')?>:</span>
                    <span class="field-nospace">
                        <input type="radio" name="updatedTaskAry[iShipperConsignee]" onclick="autofill_billing_address__new_rfq();check_insuarnce_availability('<?php echo $idBooking ?>');" id="iShipperConsignee_1" <?php if($iShipperConsignee==1){ echo "checked"; }?> value="1"> <?php echo t($t_base.'fields/shipper')?>
                    </span> 
                    <span class="field-nospace">
                        <input type="radio" name="updatedTaskAry[iShipperConsignee]" onclick="autofill_billing_address__new_rfq();check_insuarnce_availability('<?php echo $idBooking ?>');" id="iShipperConsignee_2" <?php if($iShipperConsignee==2){ echo "checked"; }?> value="2"> <?php echo t($t_base.'fields/consignee')?>
                    </span> 
                    <span class="field-nospace"> 
                        <input type="radio" name="updatedTaskAry[iShipperConsignee]" onclick="<?php  if($iAlreadyPaidBooking==1){ ?>enableCountrtDropForShipperConsignee('<?php echo $iShipperConsignee;?>'); <?php }?>autofill_billing_address__new_rfq();check_insuarnce_availability('<?php echo $idBooking ?>');" id="iShipperConsignee_3"  <?php if($iShipperConsignee==3){ echo "checked"; }?> value="3"> <?php echo t($t_base.'fields/billing')?>
                    </span> 
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/mode')?> / <?php echo t($t_base.'fields/terms')?> / <?php echo t($t_base.'fields/hand_over_city')?>:</span>
                    <span class="field-nospace">
                        <select name="updatedTaskAry[idTransportMode]" id="idTransportMode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" onchange="activate_colli_field(this.value);update_terms_down();check_insuarnce_availability('<?php echo $idBooking ?>');enableAskforquoteButton('pending_task_tray_form');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($transportModeListAry))
                                {
                                   foreach($transportModeListAry as $transportModeListArys)
                                   {
                                    ?>
                                    <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($transportModeListArys['id']==$idTransportMode)?'selected':''); ?>><?php echo $transportModeListArys['szShortName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                    <span class="separator">/</span>
                    <span class="field-nospace">
                        <select name="updatedTaskAry[idServiceTerms]"  id="idServiceTerms" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" onchange="update_handover_city();enableAskforquoteButton('pending_task_tray_form');">
                           <option value="">Select</option>
                            <?php
                                if(!empty($serviceTermsAry))
                                {
                                   foreach($serviceTermsAry as $serviceTermsArys)
                                   {
                                    ?>
                                    <option value="<?php echo $serviceTermsArys['id']; ?>" <?php echo (($serviceTermsArys['id']==$idServiceTerms)?'selected':''); ?>><?php echo $serviceTermsArys['szDisplayName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                        <input type="hidden" name="updatedTaskAry[iSingleOption]" id="iSingleOption" value="">
                        <input type="hidden" name="updatedTaskAry[idServiceTerms_hidden]" id="idServiceTerms_hidden" value="<?php echo $idServiceTerms; ?>">
                    </span>
                    <span class="separator">/</span>
                    <span class="field-nospace"> 
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szHandoverCity]" id="szHandoverCity" placeholder="City" value="<?php echo $szHandoverCity; ?>">
                        <input type="hidden" name="updatedTaskAry[szHandoverCityHidden]" id="szHandoverCityHidden" value="<?php echo $szHandoverCity; ?>">
                    </span>
                </div>  
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/cargo_text')?>:</span>
                    <span class="field-text-cbm">  
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[fCargoVolume]" id="fCargoVolume" placeholder="Volume" value="<?php echo $fCargoVolume; ?>">
                    </span>
                    <span class="text-cbm">cbm</span>
                    <span class="field-text-kg">
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[fCargoWeight]" id="fCargoWeight" placeholder="Weight" value="<?php echo $fCargoWeight; ?>">
                    </span>
                    <span class="text-kg">kg</span>
                    <span class="field-text-colli">
                        <input type="text" <?php if($idTransportMode==4){ ?><?php }else{?>readonly="readonly" class="disabled-input" <?php }?> onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onblur="enableAskforquoteButton('pending_task_tray_form');" value="<?php echo $iNumColli; ?>"  name="updatedTaskAry[iNumColli]" id="iNumColli" placeholder="Colli">
                    </span>
                    <span class="text-colli">colli</span>
                </div>
                <?php
                 $kWHSSearch=new cWHSSearch();
                $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($idBooking,false);
                //print_r($cargoDetailArr);
                $totalCargoLines=count($cargoDetailArr);
                $totalCargoWeight=0;
                $totalCargoColli=0;
                $fTotalCargoVolume=0;
                $checkWeightFlagForStandartFromTo=false;
                $checkVolumeFlagForStandartFromTo=false;
                
                if(!empty($cargoDetailArr))
                {
                    foreach($cargoDetailArr as $cargoDetailArrs)
                    {
                        $fWeight=0;
                        if($cargoDetailArrs['fWeight']==0)
                        {
                            $checkWeightFlagForStandartFromTo=true;
                        }
                        $fWeight=$cargoDetailArrs['fWeight']*$cargoDetailArrs['iQuantity'];
                        
                        
                        //$totalCargoWeight=$totalCargoWeight+$fWeight;
                        $totalCargoColli=$totalCargoColli+$cargoDetailArrs['iColli'];
                        
                        if($cargoDetailArrs['idWeightMeasure']==1)  // kg
                        {
                            $totalCargoWeight += ($fWeight);
                        }
                        else
                        {
                            $fKgFactor = $kWHSSearch->getWeightFactor($cargoDetailArrs['idWeightMeasure']);
                            if($fKgFactor>0)
                            {
                                $totalCargoWeight += ((($fWeight)/$fKgFactor ));
                            }	
                        }
                        
                        //if($postSearchAry['iSearchMiniVersion']==8)
                        //{
                            if($cargoDetailArrs['fVolume']==0)
                            {
                                $checkVolumeFlagForStandartFromTo=true;
                            }
                            if((float)$cargoDetailArrs['fVolume']>0)
                            {
                                $fTotalCargoVolume += $cargoDetailArrs['fVolume']* $cargoDetailArrs['iQuantity'];
                            }
                            else
                            {
                                if(($cargoDetailArrs['fLength']==0 || $cargoDetailArrs['fWidth']==0 || $cargoDetailArrs['fHeight']==0))
                                {
                                    $fTotalCargoVolume +=0;
                                }
                                else
                                {
                                    if($cargoDetailArrs['idCargoMeasure']==1)  // cm
                                    {
                                        $fTotalCargoVolume += ($cargoDetailArrs['fLength']/100) * ($cargoDetailArrs['fWidth']/100) * ($cargoDetailArrs['fHeight']/100) * $cargoDetailArrs['iQuantity'];
                                    }
                                    else
                                    {
                                        $fCmFactor = $kWHSSearch->getCargoFactor($cargoDetailArrs['idCargoMeasure']);
                                        if($fCmFactor>0)
                                        {
                                            $fTotalCargoVolume += (($cargoDetailArrs['fLength']/$fCmFactor )/100) * (($cargoDetailArrs['fWidth']/$fCmFactor)/100) * (($cargoDetailArrs['fHeight']/$fCmFactor)/100) * $cargoDetailArrs['iQuantity'];
                                        }	
                                    }
                                }
                            }
                        /*}
                        else
                        {
                            if($cargoDetailArrs['idCargoMeasure']==1)  // cm
                            {
                                $fTotalCargoVolume += ($cargoDetailArrs['fLength']/100) * ($cargoDetailArrs['fWidth']/100) * ($cargoDetailArrs['fHeight']/100) * $cargoDetailArrs['iQuantity'];
                            }
                            else
                            {
                                $fCmFactor = $kWHSSearch->getCargoFactor($cargoDetailArrs['idCargoMeasure']);
                                if($fCmFactor>0)
                                {
                                    $fTotalCargoVolume += (($cargoDetailArrs['fLength']/$fCmFactor )/100) * (($cargoDetailArrs['fWidth']/$fCmFactor)/100) * (($cargoDetailArrs['fHeight']/$fCmFactor)/100) * $cargoDetailArrs['iQuantity'];
                                }	
                            }
                        }*/
                        
                    }
                }
               
                $totalCargoWeight=round_up((float)$totalCargoWeight,2);
                $fTotalCargoVolume = format_volume($fTotalCargoVolume);
                 
                if($checkWeightFlagForStandartFromTo)
                {
                    $totalCargoWeight="-";
                }
                if($checkVolumeFlagForStandartFromTo)
                {    
                    $fTotalCargoVolume="-";
                }
                //echo $fTotalCargoVolume;
                //$totalCargoWeight=round_up((float)$totalCargoWeight,2);
                //$fTotalCargoVolume = format_volume($fTotalCargoVolume);
                
                ?>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"></span>
                    <?php $iNumColliText=t($t_base.'fields/cargo_details_line'); if((int)$totalCargoLines>1){ $iNumColliText=t($t_base.'fields/cargo_details_lines'); }?>
                    <span>
                    <a id="cargo_line_text" href="javascript:void(0);" style="cursor: pointer;" onclick="openCargoDetailUpdateForm('<?php echo $idBooking;?>','<?php echo $postSearchAry['iSearchMiniVersion'];?>','<?php echo $iAlreadyPaidBooking;?>');"><?php echo t($t_base.'fields/cargo_details')?>: <?php echo $totalCargoLines." ".$iNumColliText;?>, <?php echo $fTotalCargoVolume." cbm";?>, <?php echo $totalCargoWeight." kg";?>, <?php echo (int)$totalCargoColli." colli";?></a>
                    </span>
                    <span style="float:right;"><a href="javascript:void(0);" <?php if($iAlreadyPaidBooking==0){?> onclick="updateCargoLineConfirmed('<?php echo $idBooking;?>','<?php echo $postSearchAry['iSearchMiniVersion'];?>');" <?php }?>><?php echo  t($t_base.'fields/update_total');?></a></span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/date_of_shipment')?>:</span>
                    <span class="field-space">
                        <input type="text" name="updatedTaskAry[dtTimingDate]" readonly="" onfocus="check_form_field_not_required(this.form.id,this.id);" id="dtTimingDate" placeholder="Shipment date" value="<?php echo $dtTimingDate; ?>">
                    </span>
                    <span class="label-language" style="width:107px;"><?php echo t($t_base.'fields/email_tracking')?>:</span>
                    <span class="currency-dropdown" style="width:77px;">
                        <select name="updatedTaskAry[iSendTrackingUpdates]" id="iSendTrackingUpdates" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableAskforquoteButton('pending_task_tray_form');"> 
                            <option value="1" <?php echo (($iSendTrackingUpdates==1)?'selected':''); ?>>Yes</option>
                            <option value="2" <?php echo (($iSendTrackingUpdates!=1)?'selected':''); ?>>No</option> 
                        </select> 
                    </span> 
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/description')?>:</span>
                    <span class="cargo-desc-field">
                        <input type="text" name="updatedTaskAry[szCargoDescription]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');" onkeyup="toggleCargoDescriptionLockedField();enableAskforquoteButton('pending_task_tray_form');" id="szCargoDescription" placeholder="Cargo description" value="<?php echo $szCargoDescription; ?>">
                        <input type="hidden" name="updatedTaskAry[szCargoDescriptionHidden]" id="szCargoDescriptionHidden" value="<?php echo $szCargoDescription; ?>">
                        <input type="hidden" name="updatedTaskAry[idCargo]" id="idCargo" value="<?php echo $idCargo; ?>">
                    </span> 
                    <span class="cargo-desc-field middle">    
                        <input type="checkbox" name="updatedTaskAry[iCargoDescriptionLocked]"  onclick="toggleCargoDescriptionField();enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);"  id="iCargoDescriptionLocked" value="1" <?php echo (($iCargoDescriptionLocked==1)?'checked':''); ?>>
                    </span>
                    <span class="cargo-desc-field last">
                       <!--<input type="checkbox" name="updatedTaskAry[isMoving]" onclick="update_description(this.id,'<?php echo t($t_base.'title/moving_of_personal_effect'); ?>');check_insuarnce_availability('<?php echo $idBooking ?>');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');" id="isMoving" <?php echo ($isMoving==1?'checked':''); ?> value="1">-->
                       <select name="updatedTaskAry[szCargoType]" onchange="check_insuarnce_availability('<?php echo $idBooking ?>');enableAskforquoteButton('pending_task_tray_form');" id="szCargoType" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');" > 
                            <?php
                                if(!empty($manualFeeCargoTypeArr))
                                {
                                   foreach($manualFeeCargoTypeArr as $manualFeeCargoTypeArrs)
                                   {
                                      ?>
                                      <option value="<?php echo $manualFeeCargoTypeArrs['szCode']; ?>" <?php echo (($manualFeeCargoTypeArrs['szCode']==$szCargoType)?'selected':''); ?>><?php echo $manualFeeCargoTypeArrs['szName']; ?></option>
                                      <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span> 
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/insurance')?>:</span>
                    <span class="field-space" id="insurance_dropdown_container">
                       <?php echo display_insurance_options($iTransportationInsuranceAvailable,$iInsuranceIncluded); ?>
                    </span> 
                    <span class="field-space">
                        <select name="updatedTaskAry[idInsuranceCurrency]"  id="idInsuranceCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');check_insurance_required(this.form.id,this.id);" onchange="enableAskforquoteButton('pending_task_tray_form');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($currencyAry))
                                {
                                   foreach($currencyAry as $currencyArys)
                                   {
                                    ?>
                                    <option value="<?php echo $currencyArys['id']; ?>" <?php echo (($currencyArys['id']==$idInsuranceCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select> 
                    </span>
                    <span class="field-nospace">
                        <input type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');check_insurance_required(this.form.id,this.id);" onkeyup="enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[fTotalInsuranceCostForBookingCustomerCurrency]" id="fTotalInsuranceCostForBookingCustomerCurrency" value="<?php echo $fTotalInsuranceCostForBookingCustomerCurrency; ?>"/>  
                    </span>
                </div>
            </td> 
            <td style="width:50%;" valign="top"> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/shipper')?>:</span>
                    <span class="single-field">  
                        <input type="text" class="shipper-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onblur="enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szShipperCompanyName]" id="szShipperCompanyName" placeholder="Shipper Company" value="<?php echo $szShipperCompanyName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="<?php echo $szSecondSpanClass_name; ?>">  
                        <input type="text" class="shipper-fields" name="updatedTaskAry[szShipperFirstName]" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szShipperFirstName" placeholder="Shipper First Name" value="<?php echo $szShipperFirstName; ?>">
                        <input type="text" class="shipper-fields" name="updatedTaskAry[szShipperLastName]" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szShipperLastName" placeholder="Shipper Last Name" value="<?php echo $szShipperLastName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="phone-container">
                        <select size="1" name="updatedTaskAry[idShipperDialCode]" class="shipper-fields" onchange="enableAskforquoteButton('pending_task_tray_form');" id="idShipperDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <?php
                               if(!empty($dialUpCodeAry))
                               {
                                   $usedDialCode = array();
                                   foreach($dialUpCodeAry as $dialUpCodeArys)
                                   {
                                        if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                        {
                                            $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                            ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idShipperDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                        }
                                   }
                               }
                        ?>
                        </select> 
                        <input type="text" class="shipper-fields" name="updatedTaskAry[szShipperPhoneNumber]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" placeholder="Shipper Phone" id="szShipperPhoneNumber" value="<?php echo $szShipperPhoneNumber; ?>" />
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="single-field"> 
                        <input type="text" class="shipper-fields" name="updatedTaskAry[szShipperEmail]" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="Shipper Email" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szShipperEmail" value="<?php echo $szShipperEmail; ?>" > 
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="single-field">  
                        <input type="text" class="shipper-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szOriginAddress]" id="szOriginAddress" placeholder="Shipper Street Address" value="<?php echo $szOriginAddress; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="field-space">  
                        <input type="text" class="shipper-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_postcode_field(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szOriginPostcode]" id="szOriginPostcode" placeholder="Shipper Postcode" value="<?php echo $szOriginPostcode; ?>">
                    </span>
                    <span class="field-space">  
                        <input type="text" class="shipper-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');update_handover_city();" onkeyup="update_handover_city();" name="updatedTaskAry[szOriginCity]" id="szOriginCity" placeholder="Shipper City" value="<?php echo $szOriginCity; ?>">
                    </span>
                    <span class="field-nospace">  
                        <select name="updatedTaskAry[idOriginCountry]" class="shipper-fields" id="idOriginCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);check_insuarnce_availability('<?php echo $idBooking ?>');enableAskforquoteButton('pending_task_tray_form');" onchange="update_terms_down();update_dial_code('SHIPPER',this.value);check_insuarnce_availability('<?php echo $idBooking ?>');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                   foreach($allCountriesArr as $allCountriesArrs)
                                   {
                                      
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$idOriginCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/consignee')?>:</span>
                    <span class="single-field">  
                        <input type="text" class="consignee-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szConsigneeCompanyName]" id="szConsigneeCompanyName" placeholder="Consignee Company" value="<?php echo $szConsigneeCompanyName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="<?php echo $szSecondSpanClass_name; ?>">  
                        <input type="text" class="consignee-fields" name="updatedTaskAry[szConsigneeFirstName]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szConsigneeFirstName" placeholder="Consignee First Name" value="<?php echo $szConsigneeFirstName; ?>">
                        <input type="text" class="consignee-fields" name="updatedTaskAry[szConsigneeLastName]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szConsigneeLastName" placeholder="Consignee Last Name" value="<?php echo $szConsigneeLastName; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="phone-container">
                        <select size="1" class="consignee-fields" name="updatedTaskAry[idConsigneeDialCode]" id="idConsigneeDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onchange="enableAskforquoteButton('pending_task_tray_form');">
                            <?php
                               if(!empty($dialUpCodeAry))
                               {
                                   $usedDialCode = array();
                                   foreach($dialUpCodeAry as $dialUpCodeArys)
                                   {
                                        if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                        {
                                            $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ;
                                            ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$idConsigneeDialCode){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option><?php
                                        }
                                   }
                               }
                        ?>
                        </select> 
                        <input type="text" class="consignee-fields" name="updatedTaskAry[szConsigneePhoneNumber]" onkeyup="enableAskforquoteButton('pending_task_tray_form');" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" placeholder="Consignee Phone" id="szConsigneePhoneNumber" value="<?php echo $szConsigneePhoneNumber; ?>" />
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="single-field"> 
                        <input type="text" class="consignee-fields" name="updatedTaskAry[szConsigneeEmail]" onfocus="check_form_field_not_required(this.form.id,this.id);" placeholder="Consignee Email" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szConsigneeEmail" value="<?php echo $szConsigneeEmail; ?>" > 
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="single-field">  
                        <input type="text" class="consignee-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szDestinationAddress]" id="szDestinationAddress" placeholder="Consignee Street Address" value="<?php echo $szDestinationAddress; ?>">
                    </span>
                </div>
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>">&nbsp;</span>
                    <span class="field-space">  
                        <input type="text" class="consignee-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_postcode_field(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" name="updatedTaskAry[szDestinationPostcode]" id="szDestinationPostcode" placeholder="Consignee Postcode" value="<?php echo $szDestinationPostcode; ?>">
                    </span>
                    <span class="field-space">
                        <input type="text" class="consignee-fields" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');update_handover_city();" onkeyup="update_handover_city();" name="updatedTaskAry[szDestinationCity]" id="szDestinationCity" placeholder="Consignee City" value="<?php echo $szDestinationCity; ?>">
                    </span>
                    <span class="field-nospace">
                        <select name="updatedTaskAry[idDestinationCountry]" class="consignee-fields" id="idDestinationCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);check_insuarnce_availability('<?php echo $idBooking ?>');enableAskforquoteButton('pending_task_tray_form');" onchange="update_terms_down();enableAskforquoteButton('pending_task_tray_form');update_dial_code('CONSIGNEE',this.value);check_insuarnce_availability('<?php echo $idBooking ?>');">
                            <option value="">Select</option>
                            <?php
                                if(!empty($allCountriesArr))
                                {
                                   foreach($allCountriesArr as $allCountriesArrs)
                                   {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['id']; ?>" <?php echo (($allCountriesArrs['id']==$idDestinationCountry)?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/file_owner')?>:</span>
                    <span class="single-field"> 
                        <select name="updatedTaskAry[idFileOwner]" id="idFileOwner" onchange="enableAskforquoteButton('pending_task_tray_form');">
                             <option value="">Select</option> 
                            <?php
                                if(!empty($activeAdminAry))
                                {
                                   foreach($activeAdminAry as $activeAdminArys)
                                   {
                                    ?>
                                    <option value="<?php echo $activeAdminArys['id']; ?>" <?php echo (($activeAdminArys['id']==$idFileOwner)?'selected':''); ?>><?php echo $activeAdminArys['szFirstName']." ".$activeAdminArys['szLastName']; ?></option>
                                    <?php
                                   }
                                }
                            ?> 
                        </select>
                    </span>
                </div>
                <input type="hidden" name="updatedTaskAry[szFromPage]" id="szFromPage" value="VALIDATE">
                <div class="<?php echo $szMainContainerClass; ?>">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/internal_comments')?>:</span>
                    <span class="single-field"><textarea rows="5" onfocus="check_form_field_not_required(this.form.id,this.id);" name="updatedTaskAry[szInternalComment]" id="szInternalComment"><?php echo $szInternalComment; ?></textarea></span>
                </div> 
                <div class="clear-all"></div> 
                <?php 
                    if($iAlreadyPaidBooking==1)
                    {
                        $button_class = 'button2';
                    }
                    else
                    {
                        $button_class = 'button1';
                        $style= "style='opacity:0.4'" ;
                    }
                ?>
                <div class="btn-container" style="text-align:center;">
                    <?php
                        if($iAlreadyPaidBooking==1 && $postSearchAry['idBookingStatus']!='7') {
?>
                            <a href="javascript:void(0);" class="button2" id="booking_cancel_button" onclick="cancel_booking(<?php echo $postSearchAry['id']?>,'PENDING_TRAY');" ><span style="min-width:30%;"><?php echo 'CANCEL BOOKING'; ?></span></a>
                            <?php
                        }
                        if($iAlreadyPaidBooking==1 && $postSearchAry['idBookingStatus']=='7' && $postSearchAry['iCourierBooking']=='0') {
                            ?>
                            <a href="javascript:void(0);" class="button1" id="booking_copy_button" onclick="showCopyConfirmationPopUp('COPY_PENDING_TASK_POP_UP','<?php echo $postSearchAry['idFile']?>','0');" ><span style="min-width:30%;"><?php echo 'Copy'; ?></span></a>
                            <?php
                        }
                        if($iBookingSearchedType==1)
                        {
                            
                            if($iAlreadyPaidBooking!=1) {
?>
                            <a href="javascript:void(0);" class="button1" id="validate_button_convert_to_rfq" onclick="submit_pending_task_form('CONVERT_TO_RFQ');" ><span style="min-width:30%;"><?php echo 'CONVERT TO RFQ'; ?></span></a>
                            <?php
                        } }
                        else
                        {
                            if($iAlreadyPaidBooking!=1){
                            ?>
                            <a href="javascript:void(0);" class="<?php echo $button_class; ?>" id="validate_button_disqualify" <?php if($iAlreadyPaidBooking!=1){ ?> onclick="submit_pending_task_form('DISQUALIFY');" <?php } ?>><span style="min-width:30%;"><?php echo t($t_base.'fields/disqualify')?></span></a>
                            <a href="javascript:void(0);" class="<?php echo $button_class; ?>" id="validate_button_estimate" <?php echo $style; ?>><span style="min-width:30%;"><?php echo t($t_base.'fields/estimate')?></span></a>
                            <a href="javascript:void(0);" class="<?php echo $button_class; ?>" id="validate_button_ask_for_quote" <?php echo $style; ?>><span style="min-width:30%;"><?php echo t($t_base.'fields/request_quote')?></span></a> 
                            <?php
                            }
                        }
                    ?> 
                    <a href="javascript:void(0);" class="button1" id="validate_button_save"  onclick="submit_pending_task_form('SAVE')"><span><?php echo t($t_base.'fields/save')?></span></a>
                </div> 
                <div style="text-align:center;"> 
                    <p>
                        <?php
                            if(!empty($kBooking->arErrorMessages['idCustomerCurrencyMsg']))
                            {
                                echo '<span class="red_text">'.$kBooking->arErrorMessages['idCustomerCurrencyMsg']." </span> ";
                            }
                        ?>
                        <!--// All data saved to database -->
                        <?php  if($szSuccessType==1){ echo '<span style="color:green;"> Saved! </span>'; } ?> 
                        <?php  if($szSuccessType==2){ ?>  
                        <span> This file has been disqualified <a href="javascript:void(0);" onclick="display_pending_task_overview('UNDO_TASK_DISQUALIFICATION','<?php echo $idBookingFile; ?>');">Undo</a>
                        <?php } ?>
                    </p>
                </div>   
                <input type="hidden" name="updatedTaskAry[idUser]" id="idUser" value="<?php echo $idUser; ?>">
                <input type="hidden" name="updatedTaskAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">
                <input type="hidden" name="updatedTaskAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>">
                <input type="hidden" name="updatedTaskAry[szSpecialError]" id="szSpecialError" value=""> 
                <input type="hidden" name="updatedTaskAry[idCustomerCountry]" id="idCustomerCountry" value="<?php echo $idCustomerCountry; ?>">  
                <input type="hidden" name="updatedTaskAry[iAlreadyPaidBooking]" id="iAlreadyPaidBooking" value="<?php echo $iAlreadyPaidBooking; ?>" />
            </td> 
        </tr>
    </table> 
    </form>
    <?php
    
    if(!empty($kBooking->arErrorMessages))
    { 
        $szValidationErrorKey = '';
        foreach($kBooking->arErrorMessages as $errorKey=>$errorValue)
        {
            $szValidationErrorKey = $errorKey ;
            break;
        } 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
    $kCourierServices = new cCourierServices();
    $dtdTradeStr=$kCourierServices->getAllDTDTradesList(true);
    ?>
    <script type="text/javascript">
        <?php  if($iAlreadyPaidBooking==1){ ?>
            disabled_non_editable_fields_for_paid_booking('<?php echo $iShipperConsignee;?>');
        <?php } ?> 
            
        toggleCargoDescriptionField();
        toggleCargoDescriptionLockedField();
        autofill_billing_address__new_rfq();
        function update_handover_city()
        {
            var szOriginCity = $("#szOriginCity").val();
            var szDestinationCity = $("#szDestinationCity").val();
            var idServiceTerms = $("#idServiceTerms").val();  
            
            if(idServiceTerms==1) //1. EXW-DTD 
            {
                $("#szHandoverCity").val(szOriginCity);
                $("#szHandoverCity").attr('disabled','disabled');
                $("#szHandoverCityHidden").val(szOriginCity);
            }
            else if(idServiceTerms==3 || idServiceTerms==6) //3. DAP - Cleared 6. DAP - No CC
            {
                $("#szHandoverCity").val(szDestinationCity);
                $("#szHandoverCity").attr('disabled','disabled');
                $("#szHandoverCityHidden").val(szDestinationCity);
            }
            else if(idServiceTerms==2  || idServiceTerms==5) //FOB 5. EXW-WTD
            {
                $("#szHandoverCity").removeAttr('disabled'); 
            }
            else if(idServiceTerms==4) //CFR
            {
                $("#szHandoverCity").removeAttr('disabled'); 
            }
            enableAskforquoteButton('pending_task_tray_form');
        }
        
        function update_terms_down()
        {
            var customer_country = $("#idCustomerCountry").val();
            var origin_country = $("#idOriginCountry").val();
            var destination_country = $("#idDestinationCountry").val();
            var transport_mode = $("#idTransportMode").val(); 
            var iShipperConsignee = $('input:radio[name="updatedTaskAry[iShipperConsignee]"]:checked').val();  
            var iAlreadyPaidBooking = $("#iAlreadyPaidBooking").val(); 
            var dtdTradeStr = '<?php echo $dtdTradeStr;?>';
            var dtdTradeFlag=false;
            if(dtdTradeStr!='' && origin_country!='' && destination_country!='')
            {
                from_to_country_str=origin_country+"-"+destination_country;
                dtdTradeArr=dtdTradeStr.split(";");
                if(in_array(from_to_country_str,dtdTradeArr))
                {
                    dtdTradeFlag=true;
                }
            }
            customer_country = parseInt(customer_country);
            origin_country = parseInt(origin_country);
            destination_country = parseInt(destination_country);
            transport_mode = transport_mode;
             
            if(iAlreadyPaidBooking==1)
            { 
                $("#idServiceTerms").attr('disabled','disabled'); 
            }
            else
            {
                $("#idServiceTerms").removeAttr('style');
                $("#idServiceTerms").removeAttr('disabled');
                $("#iSingleOption").val('0');
            } 
            if(transport_mode!=3) //3. Road
            {
                $("#szOriginPostcode").removeClass('red_border');
                $("#szDestinationPostcode").removeClass('red_border');
            }
            var idServiceTermsVal = $("#idServiceTerms").val();  
            if(transport_mode==1 || transport_mode==2 || transport_mode==6 || transport_mode==10) // 1. AIR 2. LCL 6. FCL 10.Rail
            { 
                /*
                * Resetting Terms dropdown values
                */
                $("#idServiceTerms").removeAttr('selected');
                $("#idServiceTerms").val(' ');
                if(iShipperConsignee==1)
                { 
                    //export cargo 
                    $('#idServiceTerms option').each(function() {
                        var option_val = $(this).val(); 
                        if(option_val==1 || option_val==2 || option_val==5) // 1.EXW 2.FOB 5. EXW-WTD
                        { 
                            $(this).hide();
                            $(this).attr('disabled','disabled'); 
                        } 
                        else
                        { 
                            $(this).show(); 
                            $(this).removeAttr('disabled'); 
                            if(idServiceTermsVal==option_val)
                            {
                                $("#idServiceTerms").val(idServiceTermsVal); 
                                $(this).attr('selected','selected');
                            }
                            else if(option_val==3)
                            {
                                //$(this).attr('selected','selected');
                            }
                            else
                            {
                                $(this).removeAttr('selected');
                            }
                            if(idServiceTermsVal==1 || idServiceTermsVal==2 || idServiceTermsVal==5) // 1.EXW 2.FOB 5. EXW-WTD
                            {   
                                //if selected value got disabled then only we select new value for dropdown
                                //$(this).attr('selected','selected');
                            }
                        }
                    });  
                } 
                else if(iShipperConsignee==2)
                {
                    $('#idServiceTerms option').each(function() {
                        var option_val = $(this).val(); 
                        if(option_val==3 || option_val==4 || option_val==6) // 3.DAP 4.CFR 6. DAP-Non cleared
                        {
                            $(this).hide(); 
                            $(this).attr('disabled','disabled'); 
                        } 
                        else
                        {
                            $(this).show(); 
                            $(this).removeAttr('disabled');  
                            if(idServiceTermsVal==option_val)
                            {
                                $("#idServiceTerms").val(idServiceTermsVal); 
                                $(this).attr('selected','selected');
                            }
                            else if(option_val==1)
                            {
                                //$(this).attr('selected','selected');
                            }
                            else
                            {
                                $(this).removeAttr('selected');
                            }
                            
                            if(transport_mode==3 || transport_mode==4) // 3. Road 4. Courier
                            { 
                                
                            }
                            if(idServiceTermsVal==3 || idServiceTermsVal==4 || option_val==6)
                            {
                                //$(this).attr('selected','selected');
                            }
                        }
                    });
                }
                else
                {
                    //contains all option
                    $('#idServiceTerms option').each(function() {
                        var option_val = $(this).val();  
                        $(this).show();  
                        $(this).removeAttr('disabled');  
                        
                        if(idServiceTermsVal==option_val)
                        {
                            $("#idServiceTerms").val(idServiceTermsVal); 
                            $(this).attr('selected','selected');
                        }
                    });
                }
            }
            else if(transport_mode==3 || transport_mode==4 || transport_mode==7 || transport_mode==11) // 3. Road 4. Courier 7.FTL 11.Moving
            {
                /*
                * Resetting Terms dropdown values
                */
                $("#idServiceTerms").removeAttr('selected');
                $("#idServiceTerms").val(' ');
                if(iShipperConsignee==1)
                { 
                    //export cargo 
                    var single_value = '';
                    var idServiceTerms_hidden = '';
                    $('#idServiceTerms option').each(function() {
                        var option_val = $(this).val(); 
                        if(option_val==1 || option_val==2 || option_val==4 || option_val==5) // 1.EXW 2.FOB 4.CFR 5. EXW-WTD
                        {
                            $(this).hide(); 
                            $(this).attr('disabled','disabled'); 
                        } 
                        else
                        {
                            $(this).show(); 
                            $(this).removeAttr('disabled'); 
                            idServiceTerms_hidden =  $(this).val();  
                            if(idServiceTermsVal==option_val)
                            {
                                $("#idServiceTerms").val(idServiceTermsVal); 
                                $(this).attr('selected','selected');
                            }
                            else if(option_val==3)
                            {
                                /*
                                * If customer role is "Shipper" and Mode "LTL" or "Courier", we default select Terms – "DAP - Cleared" 
                                */
                                //$(this).attr('selected','selected');
                            }
                            else
                            {
                                $(this).removeAttr('selected');
                            }
                            //$(this).attr('selected','selected'); 
                        }
                    });  
                } 
                else if(iShipperConsignee==2)
                {
                    var idServiceTerms_hidden='';
                    //import cargo 
                    $('#idServiceTerms option').each(function() {
                        var option_val = $(this).val(); 
                        if(option_val==2 || option_val==3 || option_val==4 || option_val==6) // 2.FOB 3.DAP 4. CFR 6. DAP-Non cleared
                        {
                            $(this).hide(); 
                            $(this).attr('disabled','disabled');
                        } 
                        else
                        {
                            $(this).show(); 
                            $(this).removeAttr('disabled');  
                            if(idServiceTermsVal==option_val)
                            {
                                $("#idServiceTerms").val(idServiceTermsVal); 
                                $(this).attr('selected','selected');
                            }
                            else if(option_val==1)
                            {
                                /*
                                * If customer role is "Consignee" and Mode "LTL" or "Courier", we default select Terms – "EXW-DTD" 
                                */
                                //$(this).attr('selected','selected');
                            }
                            else
                            {
                                $(this).removeAttr('selected');
                            } 
                        }
                    }); 
                }
                else
                {
                    $('#idServiceTerms option').each(function() {
                        var option_val = $(this).val(); 
                        if(option_val==2 || option_val==4) // 1.FOB 4.CFR
                        {
                            $(this).hide(); 
                            $(this).attr('disabled','disabled');
                        } 
                        else
                        {
                            $(this).show();  
                            $(this).removeAttr('disabled'); 
                            if(idServiceTermsVal==option_val)
                            {
                                $("#idServiceTerms").val(idServiceTermsVal); 
                                $(this).attr('selected','selected');
                            }
                            else if(option_val==1)
                            {
                                /*
                                * If customer role is "Consignee" and Mode "LTL" or "Courier", we default select Terms – "EXW-DTD" 
                                */
                               // $(this).attr('selected','selected');
                            }
                            else
                            {
                                $(this).removeAttr('selected');
                            } 
                        }
                    });
                }
            }
            else
            {
                //contains all option
                $('#idServiceTerms option').each(function() {
                    var option_val = $(this).val();  
                    $(this).show();  
                    $(this).removeAttr('disabled');  

                    if(idServiceTermsVal==option_val)
                    {
                        $("#idServiceTerms").val(idServiceTermsVal); 
                        $(this).attr('selected','selected');
                    }
                });
            }
            update_handover_city();
        }
        
        toggleCompanyField('iPrivateShipping');
        enableAskforquoteButton('pending_task_tray_form'); 
        var iInsuranceIncluded = $("#iInsuranceIncluded").val();
        
        toggleInsuranceFields('<?php echo $iInsuranceIncluded; ?>');
        update_terms_down(); 
        
        $().ready(function(){	
            update_handover_city();
            <?php if($iTransportationInsuranceAvailable==2){ ?>
                toggleInsuranceFields(2);
            <?php }  ?> 
        });
    </script>
    <?php 
} 

function display_standard_cargo_quote_pricing_handler($idLandingPage)
{
    $kExplain = new cExplain();
    
    
    
    $standardHandlerAry = array();
    $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($idLandingPage,false,true);
    
    $standardQuotePricingAry = array();
    $standardQuotePricingAry = $kExplain->getStandardQuotePriceByPageID($idLandingPage,false,0,false,true);
    ?>
    <div id="standard_cargo_quotes_container">
        <?php echo display_standard_cargo_quotes_listing($idLandingPage,$standardQuotePricingAry); ?>
    </div> 
    <div id="standard_cargo_handler_container">
        <?php echo display_standard_cargo_handler($idLandingPage,$kExplain,$standardHandlerAry); ?>
    </div>
    <div id="add_standard_quote_pricing_try_id_container">
        <?php
            $automatedRfqResponseAry =$kExplain->getAutomatedRfqResponseList($idLandingPage);
            //$kExplain = new cExplain();
            //$landingPageDataArys = $kExplain->getAllNewLandingPageData(false,false,false,false,false,false,false,false,$automatedRfqResponseAry[0]['szCode']);
            echo display_add_standard_cargo_pricing_try_it_out_new($idLandingPage);
        ?>
    </div>
    <div id="standard_cargo_try_it_out_result_container" style="display:none;"></div>
    <?php
} 
function display_standard_cargo_quotes_listing($idLandingPage,$standardQuotesAry)
{
    $t_base = "management/uploadService/"; 
    
    ?>
    <script type="text/javascript">
        document.onclick=check;
        function check(e){
            
        var deselectFlag=false;    
        var t = (e && e.target) || (event && event.srcElement);
        while(t.parentNode){
            if(t==document.getElementById('standard_cargo_quotes_container')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('standard_cargo_up')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('standard_cargo_down')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('standard_cargo_add')){    
                deselectFlag=true;
            }
            else if(t==document.getElementById('standard_cargo_delete')){    
                deselectFlag=true;
            }
            
                t=t.parentNode
            }
            
            if(!deselectFlag)
            {
                //selectPricingServiceTradingData();
                
                $("#standardPricingTable .selected-row").removeClass("selected-row");
                var page_id=$("#idAutomatedRfqResponse").val();
                $("#standard_cargo_add").attr("onclick",'');
                $("#standard_cargo_add").unbind("click");
                $("#standard_cargo_add").html("<span style='min-width:50px;'>Add</span>");
                $("#standard_cargo_add").click(function(){display_add_quote_pricing_popup(page_id);});
                $("#standard_cargo_add").attr("style",'opacity:1;');
                
                $("#standard_cargo_up").unbind("click");                
                $("#standard_cargo_up").attr("style",'opacity:0.4;');
                
                $("#standard_cargo_down").unbind("click");                
                $("#standard_cargo_down").attr("style",'opacity:0.4;');
                
                $("#standard_cargo_delete").unbind("click");                
                $("#standard_cargo_delete").attr("style",'opacity:0.4;');
            }
        }
     </script>
    <hr> 
    <h4><strong><?php echo t($t_base.'title/quote'); ?></strong></h4>
    <p><?php echo t($t_base.'messages/quote_description'); ?></p>
    <table style="width:100%;" class="format-4" width="100%" cellspacing="0" cellpadding="0" id="standardPricingTable">
        <tr>
            <th style="width:18%"><?php echo t($t_base.'fields/forwarder'); ?></th>
            <th style="width:18%"><?php echo t($t_base.'fields/prouct'); ?></th>
            <th style="width:14%;text-align:right;"><?php echo t($t_base.'fields/transit_time'); ?></th>
            <th style="width:18%;text-align: right;"><?php echo t($t_base.'fields/per_booking'); ?></th>
            <th style="width:16%;text-align: right;"><?php echo t($t_base.'fields/markup'); ?></th>
            <th style="width:16%;text-align: right;"><?php echo t($t_base.'fields/minimum_markup'); ?></th>
        </tr>
        <?php
            $counter=count($standardQuotesAry);
            if(!empty($standardQuotesAry))
            {
                foreach($standardQuotesAry as $standardQuotesArys)
                {
                    if($standardQuotesArys['iBookingType']==2)
                    {
                        $szProductName = $standardQuotesArys['szCourierProviderName']." - ".$standardQuotesArys['szCourierProductName'];
                    }
                    else
                    {
                        $szProductName = 'LCL';
                    }
                    ?>
                    <tr id="sel_pricing_row_<?php echo $standardQuotesArys['id'];?>" onclick="select_standard_quote_pricing_row('<?php echo $standardQuotesArys['id'];?>',<?php echo $standardQuotesArys['iSequence'];?>,'<?php echo $counter;?>');">
                        <td><?php echo $standardQuotesArys['szForwarderDispName']." (".$standardQuotesArys['szForwarderAlias'].") "; ?></td>
                        <td><?php echo $szProductName; ?></td>
                        <td style="text-align: right;"><?php echo $standardQuotesArys['szTransitTime']." days"; ?></td>
                        <td style="text-align: right;"><?php echo $standardQuotesArys['szHandlingCurrencyName']." ".number_format((float)$standardQuotesArys['fHandlingFeePerBooking'],2); ?></td>
                        <td style="text-align: right;"><?php echo $standardQuotesArys['fHandlingMarkupPercentage']."%"; ?></td>
                        <td style="text-align: right;"><?php echo $standardQuotesArys['szHandlingMinMarkupCurrencyName']." ".number_format((float)$standardQuotesArys['fHandlingMinMarkupPrice'],2); ?></td>
                    </tr>
                    <?php
                }
            }
            else
            {
                ?>
                <tr>
                    <td colspan="6" style="text-align:center;">No record found<br><br></td>
                </tr> 
                <?php
            }
        ?>
    </table>
    <div class="btn-container">
        <span style="float:left;">
            <a href="javascript:void(0);" id="standard_cargo_up" style="opacity:0.4" class="button2"><span><?php echo t($t_base.'fields/up'); ?></span></a>
            <a href="javascript:void(0);" id="standard_cargo_down" style="opacity:0.4" class="button2"><span><?php echo t($t_base.'fields/down'); ?></span></a>
        </span>
        <span style="float:right;">
            <a href="javascript:void(0);" id="standard_cargo_add" onclick="display_add_quote_pricing_popup('<?php echo $idLandingPage; ?>');" class="button1"><span><?php echo t($t_base.'fields/add'); ?></span></a>
            <a href="javascript:void(0);" id="standard_cargo_delete" style="opacity:0.4" class="button2"><span><?php echo t($t_base.'fields/delete'); ?></span></a>
        </span>
    </div>
    <?php
}

function display_standard_cargo_handler($idLandingPage,$kExplain=false,$standardHandlerAry=false,$bSuccess=false)
{
    $t_base = "management/uploadService/";
    $kAdmin = new cAdmin();
    $customerOwnerAry = array();
    $customerOwnerAry = $kAdmin->viewManagementDetails(false,false,true);
    
    
    $automatedRfqResponseAry =$kExplain->getAutomatedRfqResponseList($idLandingPage);
    if(!empty($_POST['addStandardCargoHandler']))
    {
        $addStandardCargoHandler = $_POST['addStandardCargoHandler'];
        $idFileOwner = $addStandardCargoHandler['idFileOwner'];
        $iQuoteValidity = $addStandardCargoHandler['iQuoteValidity'];
        $szQuoteEmailSubject = $addStandardCargoHandler['szQuoteEmailSubject'];
        $szQuoteEmailBody = $addStandardCargoHandler['szQuoteEmailBody'];
        $idStandardCargoHandler = $addStandardCargoHandler['idStandardCargoHandler']; 
        $iActive = $addStandardCargoHandler['iActive'];  
    }
    else
    {
        if(!empty($standardHandlerAry))
        {
            $idFileOwner = $standardHandlerAry['idFileOwner'];
            $iQuoteValidity = $standardHandlerAry['iQuoteValidity'];
            $szQuoteEmailSubject = $standardHandlerAry['szQuoteEmailSubject'];
            $szQuoteEmailBody = $standardHandlerAry['szQuoteEmailBody'];
            $idStandardCargoHandler = $standardHandlerAry['id'];
            $iActive = $standardHandlerAry['iActive'];
        }
        else
        {
            $iQuoteValidity = 30; //Default value
        }
    } 
    ?> 
    <script type="text/javascript"> 
      
        NiceEditorInstance = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance("szQuoteEmailBody",{hasPanel : true}); 
        NiceEditorInstance.addEvent("blur", function() { enableHandlerSaveButton(); });
        $(".nicEdit-main").attr('style','margin: 4px; min-height: 200px; overflow: hidden;');
    </script>    
    <br><br>
    <div class="clear-all"></div> 
    <form method="post" action="javascript:void(0)" id="standard_cargo_handler_form" name="standard_cargo_handler_form">
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <span class="quote-field-container" style="width:25%;">File owner and e-mail sender</span>
            <span class="quote-field-container" style="width:30%;">
                <select name="addStandardCargoHandler[idFileOwner]" id="idFileOwner" onchange="enableHandlerSaveButton()">
                    <option>Select</option>
                    <?php
                        if(!empty($customerOwnerAry))
                        {
                            foreach($customerOwnerAry as $customerOwnerArys)
                            {
                                ?>
                                <option value="<?php echo $customerOwnerArys['id']; ?>" <?php echo (($customerOwnerArys['id']==$idFileOwner)?'selected':''); ?>><?php echo $customerOwnerArys['szFirstName']." ".$customerOwnerArys['szLastName']; ?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </span>
            <span class="quote-field-container" style="width:15%;text-align:right;"> 
                Quote validity
            </span>
            <span class="quote-field-container" style="width:30%;text-align:right;">
                <input type="text" name="addStandardCargoHandler[iQuoteValidity]" onkeyup="enableHandlerSaveButton();" id="iQuoteValidity" value="<?php echo $iQuoteValidity; ?>" style="width:80%;"> days
            </span>
        </div> 
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <span class="quote-field-container" style="width:60%;">Quote e-mail template</span>
            <span class="quote-field-container" style="width:40%;text-align: right;"><a href="javascript:void(0);" onclick="showHide('available_variables_popup')">See available variables</a></span>
        </div>
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <input type="text" name="addStandardCargoHandler[szQuoteEmailSubject]" onkeyup="enableHandlerSaveButton();" id="szQuoteEmailSubject" value="<?php echo $szQuoteEmailSubject; ?>">
        </div>
        <div class="clearfix email-fields-container" style="width:100%;"> 
            <textarea rows="10" name="addStandardCargoHandler[szQuoteEmailBody]" onkeyup="enableHandlerSaveButton();" id="szQuoteEmailBody"><?php echo $szQuoteEmailBody; ?></textarea>
        </div>
        <div class="btn-container clearfix">
            
                <div class=" email-fields-container" style="float:left;width:70%;"> 
                    <span class="quote-field-container" style="width:10%;">Active</span>
                    <span class="quote-field-container" style="width:10%;">
                        <select name="addStandardCargoHandler[iActive]" id="iActive" onchange="enableHandlerSaveButton();"> 
                            <option value="2" <?php echo (($iActive!=1)?'selected':'');?>>No</option>
                            <option value="1" <?php echo (($iActive==1)?'selected':'');?>>Yes</option>
                        </select>
                    </span>
                    <span class="quote-field-container" style="width:80%;padding-left:100px;">Automated RFQ Response Code: <?php echo $automatedRfqResponseAry[0]['szCode'];?></span>
                </div>
           
            <span style="float:right;">
                <?php
                    if($bSuccess>0)
                    {
                        if($bSuccess==1)
                        {
                            echo "<span class='green_text'>Saved</span>";
                        }
                        else
                        {
                            echo "<span class='red_text'>Not saved</span>";
                        }
                    }
                ?>
                <a href="javascript:void(0);" id="standard_cargo_save" style="opacity:0.4" class="button1"><span><?php echo t($t_base.'fields/save'); ?></span></a>
                <a href="javascript:void(0);" id="standard_cargo_cancel" class="button2"><span><?php echo t($t_base.'fields/cancel'); ?></span></a>
            </span>
        </div>
        <input type="hidden" name="addStandardCargoHandler[idLandingPage]" id="idLandingPage" value="<?php echo $idLandingPage; ?>" >
        <input type="hidden" name="addStandardCargoHandler[idStandardCargoHandler]" id="idStandardCargoHandler" value="<?php echo $idStandardCargoHandler; ?>" >
    </form>
     <script type="text/javascript"> 
      
       $().ready(function() {
            enableHandlerSaveButton();
        });
    </script>  
    <?php
    if(!empty($kExplain->arErrorMessages))
    { 
        $szValidationErrorKey = '';
        foreach($kExplain->arErrorMessages as $errorKey=>$errorValue)
        {
            $szValidationErrorKey = $errorKey ;
            break;
        } 
        display_form_validation_error_message($formId,$kExplain->arErrorMessages);
    }
}

function display_available_variables()
{
    $t_base = "management/uploadService/";
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup" style="width:430px;"> 
            <h3><?php echo t($t_base.'title/available_variables'); ?></h3> <br/>
            <div class="clearfix email-fields-container" style="width:100%;">szFromPostcode</div>
            <div class="clearfix email-fields-container" style="width:100%;">szFromCity</div>
            <div class="clearfix email-fields-container" style="width:100%;">szFromCountry</div>  
            <div class="clearfix email-fields-container" style="width:100%;">szToPostcode</div> 
            <div class="clearfix email-fields-container" style="width:100%;">szToCity</div> 
            <div class="clearfix email-fields-container" style="width:100%;">szToCountry</div>  
            <div class="clearfix email-fields-container" style="width:100%;">szCargoDescription</div> 
            <div class="clearfix email-fields-container" style="width:100%;">szVolume</div> 
            <div class="clearfix email-fields-container" style="width:100%;">szWeight</div> 
            <div class="clearfix email-fields-container" style="width:100%;">szFirstName</div>
            <div class="clearfix email-fields-container" style="width:100%;">szLastName</div>
            <div class="clearfix email-fields-container" style="width:100%;">szQuote</div>
            <div class="clearfix email-fields-container" style="width:100%;">szAdminName</div>
            <div class="clearfix email-fields-container" style="width:100%;">szOfficePhone</div>
            <div class="clearfix email-fields-container" style="width:100%;">szWebSiteUrl</div>
            <div class="clearfix email-fields-container" style="width:100%;">szValidTo</div> 
            <div class="btn-container" style="text-align:center;"> 
                <a href="javascript:void(0);" onclick="showHide('available_variables_popup')" class="button1"><span><?php echo t($t_base.'fields/close'); ?></span></a> 
            </div>
        </div>
    </div>  
    <?php
} 
function display_add_standard_cargo_pricing($idLandingPage,$standardQuotePricingAry=array(),$kExplain=false)
{
    $t_base = "management/uploadService/";
    $kForwarder = new cForwarder();
    $forwarderAry = array();
    $forwarderAry = $kForwarder->getAllForwarder(true);
    
    if(!empty($_POST['addStandardQuotePricingAry']))
    {
        $addStandardQuotePricingAry = $_POST['addStandardQuotePricingAry'];
        
        $idForwarder = $addStandardQuotePricingAry['idForwarder'];
        $iBookingType = $addStandardQuotePricingAry['iBookingType'];
        $idCourierProvider = $addStandardQuotePricingAry['idCourierProvider'];
        $idCourierProviderProduct = $addStandardQuotePricingAry['idCourierProviderProduct'];
        $szTransitTime = $addStandardQuotePricingAry['szTransitTime'];
        
        $idHandlingCurrency = $addStandardQuotePricingAry['idHandlingCurrency'];
        $fHandlingFeePerBooking = $addStandardQuotePricingAry['fHandlingFeePerBooking'];
        $fHandlingMarkupPercentage = $addStandardQuotePricingAry['fHandlingMarkupPercentage'];
        $idHandlingMinMarkupCurrency = $addStandardQuotePricingAry['idHandlingMinMarkupCurrency'];
        $fHandlingMinMarkupPrice = $addStandardQuotePricingAry['fHandlingMinMarkupPrice'];
        $idStandardQuotePricing = $addStandardQuotePricingAry['idStandardQuotePricing']; 
        
       
    }
    else if(!empty($standardQuotePricingAry))
    {
        $idForwarder = $standardQuotePricingAry['idForwarder'];
        $iBookingType = $standardQuotePricingAry['iBookingType'];
        $idCourierProvider = $standardQuotePricingAry['idCourierProvider'];
        $idCourierProviderProduct = $standardQuotePricingAry['idCourierProviderProduct'];
        $szTransitTime = $standardQuotePricingAry['szTransitTime'];
        
        $idHandlingCurrency = $standardQuotePricingAry['idHandlingCurrency'];
        $fHandlingFeePerBooking = $standardQuotePricingAry['fHandlingFeePerBooking'];
        $fHandlingMarkupPercentage = $standardQuotePricingAry['fHandlingMarkupPercentage'];
        $idHandlingMinMarkupCurrency = $standardQuotePricingAry['idHandlingMinMarkupCurrency'];
        $fHandlingMinMarkupPrice = $standardQuotePricingAry['fHandlingMinMarkupPrice']; 
        $idStandardQuotePricing = $standardQuotePricingAry['id']; 
    }
    
    //echo $idCourierProvider;
    
    if($iBookingType==2)
    {
        if($idForwarder>0)
        {
            $kCourierServices = new cCourierServices();
            $data['idForwarder'] = $idForwarder;
            $courierProviderListAry = $kCourierServices->getAllCourierAgreement($data,'',true);
            
            
            if((int)$idCourierProvider>0){
            $courierProviderProductListAry = $kCourierServices->getCourierProductByIdForwarderIdCourierProvider($idCourierProvider,$idForwarder);
            }
       
        }
    }
    
    if($idForwarder>0)
    {
        $disabled_str = '';
        if($iBookingType>0)
        {
            if($iBookingType==1) //LCL selected
            {
                $courier_disabled_str = ' disabled="disabled" ';
                $disabled_str='';
            }
            else
            {
                $courier_disabled_str = ''; //Courier selecetd
                if((int)$idCourierProvider==0)
                {
                    $disabled_str = ' disabled="disabled" ';
                }
                
                if((int)$idCourierProviderProduct==0)
                {
                    $disabled_str = ' disabled="disabled" ';
                }
            }
        }
        else
        {
            $disabled_str = ' disabled="disabled" ';
        }
    }
    else
    {
        $disabled_str = ' disabled="disabled" ';
        $courier_disabled_str = ' disabled="disabled" ';
    }
    $kConfig = new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);
    
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:430px;"> 
            <h3><?php echo t($t_base.'title/include_this_pricing'); ?></h3> <br/>
            <form name="add_standard_quote_pricing" id="add_standard_quote_pricing" method="post" action="javascription:void(0);">
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/forwarder'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addStandardQuotePricingAry[idForwarder]" id="idForwarder" onchange="enable_product_pricing(this.value);enableQuotePricingSubmitButton();" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value="">Select</option>
                            <?php
                                if(!empty($forwarderAry))
                                {
                                    foreach($forwarderAry as $forwarderArys)
                                    {
                                        ?>
                                        <option value="<?php echo $forwarderArys['id']?>" <?php echo (($forwarderArys['id']==$idForwarder)?'selected':''); ?>><?php echo $forwarderArys['szDisplayName']." (".$forwarderArys['szForwarderAlias'].")"?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                </div> 
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/prouct'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;">
                        <select name="addStandardQuotePricingAry[iBookingType]" id="iBookingType" onchange="enableProducOptions(this.value);enableQuotePricingSubmitButton();" <?php echo $disabled_str; ?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                            <option value="">Select</option>
                            <option value="1" <?php echo (($iBookingType==1)?'selected':'');?>>LCL</option>
                            <option value="2" <?php echo (($iBookingType==2)?'selected':'');?>>Courier</option>
                        </select>
                    </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/courier_provider'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;" id="idCourierProviderDiv">
                        <select name="addStandardQuotePricingAry[idCourierProvider]" onchange="enableProviderProducOptions(this.value);enableQuotePricingSubmitButton()" onfocus="check_form_field_not_required(this.form.id,this.id);" id="idCourierProvider" <?php echo $courier_disabled_str; ?> >
                            <option value=''>Select</option>
                            <?php 
                                if(!empty($courierProviderListAry))
                                {
                                    foreach($courierProviderListAry as $courierProviderListArys)
                                    {
                                        ?>
                                        <option  value='<?php echo $courierProviderListArys['idCourierProvider']?>' <?php echo (($courierProviderListArys['idCourierProvider']==$idCourierProvider)?'selected':''); ?>><?php echo $courierProviderListArys['szCourierProviderName']; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/courier_product'); ?></span>
                    <span class="quote-field-container" style="width:60%;text-align: right;" id="idCourierProviderProductDiv">
                        <select name="addStandardQuotePricingAry[idCourierProviderProduct]" id="idCourierProviderProduct" <?php echo $courier_disabled_str; ?> onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="enableQuotePricingFileds(this.value);">
                            <option value=''>Select</option>
                            <?php 
                                if(!empty($courierProviderProductListAry))
                                {
                                    foreach($courierProviderProductListAry as $courierProviderProductListArys)
                                    {
                                        ?>
                                        <option  value='<?php echo $courierProviderProductListArys['idProviderProduct']?>' <?php echo (($courierProviderProductListArys['idProviderProduct']==$idCourierProviderProduct)?'selected':''); ?>><?php echo $courierProviderProductListArys['szName']; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                    </span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/transit_time_for_quote'); ?></span>
                    <span class="quote-field-container" style="width:50%;">
                        <input type="text" <?php echo $disabled_str;?> name="addStandardQuotePricingAry[szTransitTime]" onkeyup="enableQuotePricingSubmitButton();" id="szTransitTime" value="<?php echo $szTransitTime; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                    </span> 
                    <span class="quote-field-container" style="width:10%;text-align: right;">days</span>
                </div>
                <div class="clearfix email-fields-container" style="width:100%;">&nbsp;</div>
                <div class="clearfix email-fields-container" style="width:100%;">Handling fee for Transporteca, added to tariff</div>
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/per_booking'); ?></span>
                    <span class="quote-field-container" style="width:60%;">
                        <span style="width:50%;float:left;">
                            <select <?php echo $disabled_str;?> name="addStandardQuotePricingAry[idHandlingCurrency]" id="idHandlingCurrency" style="width:95%;" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onchange="enableQuotePricingSubmitButton();setCurrencyValueForHandlingFee(this.value,'idHandlingMinMarkupCurrency');">
                                <option value="">Select</option> 
                                <?php
                                    if(!empty($currencyAry))
                                    {
                                        foreach($currencyAry as $currencyArys)
                                        {
                                            ?>
                                            <option value="<?php echo $currencyArys['id']?>" <?php echo (($currencyArys['id']==$idHandlingCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </span>
                        <span style="width:50%;float:right;">
                            <input <?php echo $disabled_str;?> type="text" name="addStandardQuotePricingAry[fHandlingFeePerBooking]" onkeyup="enableQuotePricingSubmitButton();" id="fHandlingFeePerBooking" value="<?php echo $fHandlingFeePerBooking; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        </span>
                    </span>  
                </div> 
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/markup'); ?></span> 
                    <span class="quote-field-container" style="width:27%;margin-left:30%">
                        <input <?php echo $disabled_str;?> type="text" name="addStandardQuotePricingAry[fHandlingMarkupPercentage]" onkeyup="enableQuotePricingSubmitButton();" id="fHandlingMarkupPercentage" value="<?php echo $fHandlingMarkupPercentage; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                    </span> 
                    <span class="quote-field-container" style="width:3%;text-align: right;">%</span> 
                </div>
                
                <div class="clearfix email-fields-container" style="width:100%;"> 
                    <span class="quote-field-container" style="width:40%;"><?php echo t($t_base.'fields/minimum_markup'); ?></span>
                    <span class="quote-field-container" style="width:60%;">
                        <span style="width:50%;float:left;">
                            <select <?php echo $disabled_str;?> name="addStandardQuotePricingAry[idHandlingMinMarkupCurrency]" id="idHandlingMinMarkupCurrency" style="width:95%;" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onchange="enableQuotePricingSubmitButton();setCurrencyValueForHandlingFee(this.value,'idHandlingCurrency');">
                                <option value="">Select</option> 
                                <?php
                                    if(!empty($currencyAry))
                                    {
                                        foreach($currencyAry as $currencyArys)
                                        {
                                            ?>
                                            <option value="<?php echo $currencyArys['id']?>" <?php echo (($currencyArys['id']==$idHandlingMinMarkupCurrency)?'selected':''); ?>><?php echo $currencyArys['szCurrency']; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </span>
                        <span style="width:50%;float:right;">
                            <input <?php echo $disabled_str;?> type="text" name="addStandardQuotePricingAry[fHandlingMinMarkupPrice]" onkeyup="enableQuotePricingSubmitButton();" id="fHandlingMinMarkupPrice" value="<?php echo $fHandlingMinMarkupPrice; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        </span>
                    </span>  
                </div> 
                <input type="hidden" name="addStandardQuotePricingAry[idLandingPage]" id="idLandingPage" value="<?php echo $idLandingPage; ?>">
                <input type="hidden" name="addStandardQuotePricingAry[idStandardQuotePricing]" id="idStandardQuotePricing" value="<?php echo $idStandardQuotePricing; ?>" > 
                <div class="btn-container" style="text-align:center;"> 
                    <a href="javascript:void(0);" onclick="showHide('add_standard_quote_pricing_form_container')" class="button2"><span><?php echo t($t_base.'fields/cancel'); ?></span></a> 
                    <a href="javascript:void(0);" style="opacity:0.4" id="standard_quote_pricing_submit_button" class="button1"><span><?php echo t($t_base.'fields/save'); ?></span></a> 
                </div>
            </form> 
        </div> 
    </div>  
    <?php 
    
    if(!empty($kExplain->arErrorMessages))
    { 
        $szValidationErrorKey = '';
        foreach($kExplain->arErrorMessages as $errorKey=>$errorValue)
        {
            $szValidationErrorKey = $errorKey ;
            break;
        } 
        display_form_validation_error_message($formId,$kExplain->arErrorMessages);
    }
}
function display_add_standard_cargo_pricing_try_it_out($idLandingPage,$kBooking=false)
{
    $t_base = "management/uploadService/"; 
    $kConfig=new cConfig();
    $allCountryAry = array();
    $allCountryAry = $kConfig->getAllCountries(true); 
    if(!empty($_POST['searchAry']))
    {
        $szDestinationPostCode = $_POST['searchAry']['szConsigneePostcode'];
        $szDestinationCity = $_POST['searchAry']['szConsigneeCity'];
        $idDestinationCountry = $_POST['searchAry']['idConsigneeCountry'];
        
        $szOriginCountryStr = $_POST['searchAry']['szOriginCountryStr'];
        $szDestinationCountryStr = $_POST['searchAry']['szDestinationCountryStr'];
    } 
    
    
    
    if($idLandingPage>0)
    {
        $kExplain = new cExplain();
        $landingPageDataArys = $kExplain->getAllNewLandingPageData($idLandingPage);
        $iVogaPageType = $landingPageDataArys[0]['iPageSearchType']; 
    }
    else
    {
        $iVogaPageType = __SEARCH_TYPE_VOGUE_AUTOMATIC_;
    } 
    
    ?>
    <br><br>
    <div class="clear-all"></div> 
    <hr>
    <script type="text/javascript" defer="">
        $().ready(function() {	 
            var autocomplete1,place ; 
            function initialize1() 
            {
                var input1 = document.getElementById('szOriginCountry');
                var options = {types: ['regions']};

                var autocomplete1 = new google.maps.places.Autocomplete(input1); 
                $(".pac-container:last").attr("id", 'szOriginCountryPacContainer' );
                google.maps.event.addListener(autocomplete1, 'place_changed', function() 
                {    
                    var szOriginAddress_js = $("#szOriginCountry").val();   
                }); 
                var input2 = document.getElementById('szDestinationCountry');
                var autocomplete2 = new google.maps.places.Autocomplete(input2); 
                $(".pac-container:last").attr("id", 'szDestinationCountryPacContainer' );
                google.maps.event.addListener(autocomplete2, 'place_changed', function() 
                {
                    var szDestinationAddress_js = $("#szDestinationCountry").val();   
                }); 

                $('#szOriginCountry').bind('paste focus', function (e){  
                    $('#szOriginCountry').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szOriginCountryPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks
                        }
                        else
                        {
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display'); 
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);
                                }
                            });
                        }
                        var key_code = e.keyCode || e.which;
                        if (key_code == 9 || key_code == 13) {
                            prefillAddressOnTabPress('szOriginCountry',pac_container_id);
                        }  
                    }); 
                });

                $('#szDestinationCountry').bind('paste focus', function (e){  
                    $('#szDestinationCountry').bind('keydown keyup keypress click', function (e) {  
                        var pac_container_id = 'szDestinationCountryPacContainer';
                        if($("#"+pac_container_id).length)
                        {
                            //This means id already there no need for further checks 
                            //console.log("div exist called");
                        }
                        else
                        {
                            var iFoundDiv = 1;
                            $(".pac-container").each(function(pac)
                            {
                                var disp = $(this).css('display');  
                                if(disp!='none')
                                {
                                    $(this).attr('id',pac_container_id);  
                                    iFoundDiv = 2;
                                } 
                            });

                            if(iFoundDiv==1)
                            {
                                $(".pac-container").each(function(pac)
                                {
                                    var container_id = $(this).attr('id');  
                                    if(container_id=='' || container_id === undefined)
                                    {
                                        $(this).attr('id',pac_container_id); 
                                        return true;
                                    }
                                });
                            }
                        } 
                        if (e.keyCode == 9 || e.keyCode == 13) {
                            prefillAddressOnTabPress('szDestinationCountry',pac_container_id);
                        }  
                    });
                }); 
            } 
            function prefillAddressOnTabPress(input_field_id,pac_container_id)
            {  
                $(".pac-container").each(function(pac){
                   var disp = $(this).css('display'); 
                   var contaier_id = $(this).attr('id');  

                   //console.log("Container: "+contaier_id + " pac id: "+pac_container_id);

                   if(contaier_id==pac_container_id)
                   {
                        var spanObj = $(this).children(".pac-item:first" );
                        var firstResult = ""; 
                        spanObj.children("span").each(function(){
                            var spanText = $(this).text(); 
                            spanText = jQuery.trim(spanText);
                            //("Span Text: "+spanText);
                            if(spanText!='')
                            {
                                if(firstResult=='')
                                {
                                    firstResult += spanText;
                                }
                                else
                                {
                                    firstResult += ", "+ spanText;
                                }
                                //$(this).children( ".pac-item:first" ).addClass("pac-selected");
                                //$(this).css("display","none");
                                $("#"+input_field_id).val(firstResult); 
                                $("#"+input_field_id).select();
                            }
                        }); 

                   }
                }); 
            } 
            function clean_pac_container()
            { 
                $("div").each(function(index){
                    if($( this ).hasClass("pac-container"))
                    {
                        $( this ).remove();
                    }
                }); 
            }
            initialize1();  
        }); 
   
    </script> 
    <h4><strong><?php echo t($t_base.'title/try_it_out'); ?></strong></h4>
    <form id="standardPricingTryItOutForm" method="post" name="standardPricingTryItOutForm" action="javascript:void(0);">
        <div class="clearfix email-fields-container" style="width:100%;">
            <?php if($iVogaPageType==__SEARCH_TYPE_VOGUE_AUTOMATIC_ || $iVogaPageType==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_){ ?>
                <span class="quote-field-container wd-34">
                    From<br>
                    <input type="text" name="searchAry[szOriginCountryStr]" placeholder="From explanation" id="szOriginCountry"  onblur="check_form_field_empty_standard(this.form.id,this.id);" value="<?php echo $szOriginCountryStr;?>" onkeyup="enable_voga_try_it_out_submit();" onfocus="check_form_field_not_required(this.form.id,this.id);" /> 
                </span>
                <span class="quote-field-container wds-35">
                    To<br>
                    <input type="text" name="searchAry[szDestinationCountryStr]" placeholder="To explanation" id="szDestinationCountry" value="<?php echo $szDestinationCountryStr;?>" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enable_voga_try_it_out_submit();" onfocus="check_form_field_not_required(this.form.id,this.id);" /> 
                </span>
            <?php } else { ?>
                <span class="quote-field-container wd-23" >
                    <?php echo t($t_base.'fields/delivery_postcode'); ?><br>
                    <input id="szConsigneePostcode" type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enable_voga_try_it_out_submit();" onblur="check_form_field_empty_standard(this.form.id,this.id);" value="<?php echo $szDestinationPostCode; ?>" name="searchAry[szConsigneePostcode]">
                </span>
                <span class="quote-field-container wds-23">
                    <?php echo t($t_base.'fields/delivery_city'); ?><br>
                    <input id="szConsigneeCity" type="text" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enable_voga_try_it_out_submit();" onblur="check_form_field_empty_standard(this.form.id,this.id);" value="<?php echo $szDestinationCity; ?>" name="searchAry[szConsigneeCity]">
                </span>
                <span class="quote-field-container wds-22" >
                    <?php echo t($t_base.'fields/country'); ?><br>
                    <select id="idConsigneeCountry" name="searchAry[idConsigneeCountry]" onchange="enable_voga_try_it_out_submit();" style="width:90%;">
                        <option value=''>Select</option> 
                        <?php 
                            if(!empty($allCountryAry))
                            {
                                foreach($allCountryAry as $allCountryArys)
                                {
                                    ?>
                                    <option  value='<?php echo $allCountryArys['id']?>' <?php if($idDestinationCountry==$allCountryArys['id']){?> selected <?php }?>><?php echo $allCountryArys['szCountryName'];?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            <?php } ?> 
            <span class="quote-field-container wds-31" style="text-align: right;"> <br>
                <a id="standard-cargo-test-button" class="button1" style="opacity:0.4"  href="javascript:void(0)">
                    <span style="min-width:50px;">Test</span>
                </a> 
                <a id="standard-cargo-clear-button" class="button2" onclick="reset_standard_tryit_out('<?php echo $idLandingPage; ?>');" href="javascript:void(0)">
                    <span style="min-width:50px;">Clear</span>
                </a>
            </span>
        </div> 
        <input type="hidden" name="searchAry[idLandingPage]" id="idLandingPage" value="<?php echo $idLandingPage; ?>">
        <input type="hidden" name="searchAry[iVogaPageType]" id="iVogaPageType" value="<?php echo $iVogaPageType; ?>">
        
        <div id="update_cargo_details_container_div" class="search-min-v7-cargo-field">
            <?php echo display_vog_cargo_fields_management($kBooking,$idLandingPage); ?>
        </div> 
    </form>
    <script type="text/javascript">
    </script>    
    <?php   
    if(!empty($kBooking->arErrorMessages))
    {  
        $formId = 'standardPricingTryItOutForm'; 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
} 
function display_vog_cargo_fields_management($kBooking,$idLandingPage)
{
    $cargoAry = array(); 
    $t_base = "LandingPage/";  
    
    $iLanguage = getLanguageId();  
    //$iSearchMiniPage = 7;
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__); 
    $szDanishClass=''; 
    $kCourierService = new cCourierServices();   
    
    $kExplain = new cExplain();
    $vogaPageDetailsAry = $kExplain->getAllNewLandingPageData($idLandingPage);
    $landingPageDataAry = $vogaPageDetailsAry[0];
    
    
    $iPageSearchType = $landingPageDataAry['iPageSearchType'];
    
    if($iPageSearchType==__SEARCH_TYPE_VOGUE__)
    {
        $iSearchMiniPage = 7;
    }
    else if($iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_)
    {
        $iSearchMiniPage = 8;
    }
    else if($iPageSearchType==__SEARCH_TYPE_VOGUE_AUTOMATIC_SIMPLE_)
    {
        $iSearchMiniPage = 9;
    }
    $szDivKey = "_".$iSearchMiniPage;  
    
    
                    
    $kConfig = new cConfig();
    $standardCargoCategoryAry = array();
    $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,false,$landingPageDataAry['idSearchMini']);
    
    /*
    * Building json data to pre-populate in category and subcategory menu
    */
    $standardSubCategoryAry = array();
    $standardVogaSubCategoryAry = array();
    if(!empty($standardCargoCategoryAry))
    {
        foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
        { 
            //echo "<br> Cat: ".$standardCargoCategoryArys['id']." Lang: ".$iSelectedLanguage." src Min: ".$landingPageDataAry['idSearchMini'];
            $standardCargoAry = array();
            $standardCargoAry = $kConfig->getStandardCargoList($standardCargoCategoryArys['id'],$iSelectedLanguage,'',$landingPageDataAry['idSearchMini']);  
            $idStandardCategory = $standardCargoCategoryArys['id'];
         //            print_r($standardCargoAry);
           //          die();
            $ctr = 0;
            if(!empty($standardCargoAry))
            {
                foreach($standardCargoAry as $standardCargoArys)
                {
                    $standardSubCategoryAry[$idStandardCategory][$ctr]['id'] = $standardCargoArys['id'];
                    $standardSubCategoryAry[$idStandardCategory][$ctr]['szLongName'] = stripslashes(addslashes(($standardCargoArys['szLongName'])));
                    $standardVogaSubCategoryAry[$standardCargoArys['id']]['iColli'] = $standardCargoArys['iColli'];
                    $standardVogaSubCategoryAry[$standardCargoArys['id']]['idSearchMini'] = $standardCargoArys['idSearchMini']; 
                    $ctr++;
                }
            } 
        }
    } 
    //print_r($standardSubCategoryAry);
    if(!empty($standardSubCategoryAry))
    {
        $standardSubCategoryJsonStr = json_encode($standardSubCategoryAry);
    }
    if(!empty($standardVogaSubCategoryAry))
    {
        $standardVogaSubCategoryJsonStr = json_encode($standardVogaSubCategoryAry);
    } 
    ?>  
    <script type="text/javascript">
        __JS_ONLY_VOGA_CATEGORY_LISTING__ = "";
        __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__ = "";
        
        __JS_ONLY_VOGA_CATEGORY_LISTING__ = <?php echo $standardSubCategoryJsonStr ?>; 
        __JS_ONLY_VOGA_SUB_CATEGORY_LISTING__ = <?php echo $standardVogaSubCategoryJsonStr ?>; 
    </script>
    <div id="horizontal-scrolling-div-id<?php echo $szDivKey;?>">
        <?php 
            $counter = 1; 
            if(count($_POST['cargodetailAry']['idCargoCategory'])>0)
            { 
                if(count($_POST['cargodetailAry']['idCargoCategory'])>0)
                {
                   $loop_counter = count($_POST['cargodetailAry']['idCargoCategory']);  
                   $postDataFlag = true;
                } 
                for($j=0;$j<$loop_counter;$j++)
                {
                    $counter = $j+1; 
                    ?>
                    <div id="add_booking_quote_container_<?php echo $j."".$szDivKey; ?>" class="request-quote-fields clearfix"> 
                        <?php
                            echo display_furniture_order_form_management($counter,$iSearchMiniPage,array(),$landingPageDataAry['idSearchMini']);
                        ?>
                    </div>
                    <?php
                }
            }
            else
            {   
                ?>
                <div id="add_booking_quote_container_1<?php echo $szDivKey;?>" class="request-quote-fields clearfix" > 
                    <?php
                       echo display_furniture_order_form_management($counter,$iSearchMiniPage,array(),$landingPageDataAry['idSearchMini']);
                    ?>
                </div>
                <?php 
            } 
        ?>
        <!--<div id="add_booking_quote_container_<?php echo $counter."".$szDivKey; ?>"></div>-->
    </div>     
    <?php
        if(!empty($kBooking->arErrorMessages['szSpecialError']))
        {
            echo '<div class="cargo-success-text">'.$kBooking->arErrorMessages['szSpecialError']."</div>" ;
        }
    ?>
    <input type="hidden" name="cargodetailAry[hiddenPosition]" id="hiddenPosition<?php echo $szDivKey;?>" value="<?=$counter?>" />
    <input type="hidden" name="cargodetailAry[hiddenPosition1]" id="hiddenPosition1<?php echo $szDivKey;?>" value="<?=$counter?>" />
    <input type="hidden" name="voga_sub_category_type_field" id="voga_sub_category_type_field" value="<?php echo t($t_base.'fields/furniture_type');?>" /> 
    <?php   
}

function display_furniture_order_form_management($number,$iSearchMiniPage=false,$cargoAry=array(),$idSearchMini=0)
{    
    if(!empty($_POST['cargodetailAry']))
    {
        $cargoAry = $_POST['cargodetailAry'] ; 
        $llop_counter = $number;
        $idCargoCategory = $cargoAry['idCargoCategory'][$llop_counter];
        $fWidth = $cargoAry['iWidth'][$llop_counter]; 
        $iQuantity = $cargoAry['iQuantity'][$llop_counter];
        $idCargo = $cargoAry['id'][$llop_counter]; 
        $idStandardCargoType = $cargoAry['idStandardCargoType'][$llop_counter];
    }
    else
    { 
        $idCargoCategory = $cargoAry[$number]['idCargoCategory'];
        $fWidth = $cargoAry[$number]['fWidth']; 
        $iQuantity = $cargoAry[$number]['iQuantity'];
        $idCargo = $cargoAry[$number]['id'];  
    }  
    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $idBooking = $postSearchAry['id'];
                    
    $t_base = "LandingPage/";  
    $szStandardClass="search-mini-cargo-fields"; 
    $idSuffix = "_V_".$iSearchMiniPage ; 
    $iLanguage = getLanguageId();
    
    $standardCargoCategoryAry = array();
    $standardCargoAry = array();
    
    $iSelectedLanguage = getLanguageActualID(__LANGUAGE__); 
    $standardCargoCategoryAry = $kConfig->getStandardCargoCategoryList($iSelectedLanguage,false,$idSearchMini);
    
    if($iSearchMiniPage==9){
        $idCargoCategory=$standardCargoCategoryAry[0]['id'];
    }
    if($idCargoCategory>0)
    { 
        $standardCargoAry = $kConfig->getStandardCargoList($idCargoCategory,$iSelectedLanguage,'',$idSearchMini); 
    }
?>   
    <div class="clearfix email-fields-container" style="width:100%;">
        <?php if($iSearchMiniPage!=9){?>
        <span class="quote-field-container" style="width:23%;">  
            <span id="idCargoCategory<?php echo $number."".$idSuffix."_container"; ?>"> 
                <select class="standard-cargo-category" onfocus="check_form_field_not_required(this.form.id,'idCargoCategory<?php echo $number."".$idSuffix; ?>');" name="cargodetailAry[idCargoCategory][<?=$number?>]" id="idCargoCategory<?php echo $number."".$idSuffix; ?>" style="width:90%;" onchange="update_cargo_type(this.value,'<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>','CATEGORY','','1');enable_voga_try_it_out_submit();">
                    <option value=""><?=t($t_base.'fields/furniture_category');?></option>
                    <?php
                        if(!empty($standardCargoCategoryAry))
                        {
                            foreach($standardCargoCategoryAry as $standardCargoCategoryArys)
                            {
                                ?>
                                <option value="<?php echo $standardCargoCategoryArys['id']?>" <?php if($standardCargoCategoryArys['id']==$idCargoCategory){?> selected <?php }?> ><?php echo $standardCargoCategoryArys['szCategoryName']; ?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </span> 
        </span>
        <?php }else{?>
        <input type="hidden" value="<?php echo $idCargoCategory;?>" name="cargodetailAry[idCargoCategory][<?=$number?>]" id="idCargoCategory<?php echo $number."".$idSuffix; ?>" />
        <?php }?>
        <span class="quote-field-container" style="<?php if($iSearchMiniPage==9){?>width:50%; <?php }else{ ?> width:23%; <?php }?>">
            <span class="input-fields" id="idStandardCargoType<?php echo $number."".$idSuffix."_container"; ?>"> 
                <?php echo display_cargo_type_dropdown_management($standardCargoAry,$number,$idSuffix,$idStandardCargoType); ?>
            </span> 
        </span>
        <?php if($iSearchMiniPage!=9){?>
        <span class="quote-field-container" style="width:10%;">
            <span class="input-box" id="iQuantity<?php echo $number."".$idSuffix."_container"; ?>">  
                <input type="text" onkeypress="return isNumberKey(event);" onkeyup="enable_voga_try_it_out_submit();" <?=$disabled_str?> pattern="[0-9]*" onblur="check_form_field_empty_standard_container(this.form.id,this.id,'iQuantity<?php echo $number."".$idSuffix; ?>');" onfocus="check_form_field_not_required(this.form.id,'iQuantity<?php echo $number."".$idSuffix."_container"; ?>');" placeholder="<?php echo ucfirst(t($t_base.'fields/furniture_qty')); ?>" name="cargodetailAry[iQuantity][<?=$number?>]" value="<?=$iQuantity?>" id="iQuantity<?php echo $number."".$idSuffix; ?>" class="first-cargo-fields<?php echo $idSuffix; ?>" />
            </span> 
        </span>
        <span class="quote-field-container" style="width:10.5%;text-align: right;">
            <a href="javascript:void(0);" onclick="add_more_cargo_details_management()" class="cargo-add-button"></a>  
            <a href="javascript:void(0);" class="cargo-remove-button" <?php echo $szMinusButtonId; ?> onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo $number."_".$iSearchMiniPage ?>','<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>');"></a>
        </span>
        <?php }?>
        <span class="quote-field-container" style="width:31%;">
<!--            <a href="javascript:void(0);" onclick="add_more_cargo_details_management()" class="cargo-add-button"></a>  
            <a href="javascript:void(0);" class="cargo-remove-button" <?php echo $szMinusButtonId; ?> onclick="remove_cargo_details_block('add_booking_quote_container_<?php echo $number."_".$iSearchMiniPage ?>','<?php echo $number; ?>','<?php echo $iSearchMiniPage; ?>');"></a>-->
        </span>
    </div> 
    <?php 
}

function display_cargo_type_dropdown_management($standardCargoAry,$number,$idSuffix,$idStandardCargoType=false)
{
    $t_base = "LandingPage/";   
?>
    <select onblur="check_form_field_empty_standard(this.form.id,this.id);" onfocus="check_form_field_not_required(this.form.id,'idStandardCargoType<?php echo $number."".$idSuffix."_container"; ?>');" name="cargodetailAry[idStandardCargoType][<?=$number?>]" id= "idStandardCargoType<?php echo $number."".$idSuffix; ?>" style="width:90%;" onchange="enable_voga_try_it_out_submit();">
        <option value=""><?=t($t_base.'fields/furniture_type');?></option>
        <?php
            if(!empty($standardCargoAry))
            {
                foreach($standardCargoAry as $standardCargoDetail)
                {
                    ?>
                    <option value="<?=$standardCargoDetail['id']?>" <?php if($standardCargoDetail['id']==$idStandardCargoType){?> selected <?php }?>><?=$standardCargoDetail['szLongName']?></option>
                    <?php
                }
            }
        ?>
    </select>  
    <?php
} 

function checkaddslashes($str)
{
    $szOriginalStr = $str;
    if(strpos(str_replace("\'",""," $str"),"'")!=false)
        return addslashes($szOriginalStr);
    else
        return $szOriginalStr;
}

function display_task_quote_container($kBooking,$edit_flag=false,$success_message=false,$edit_all_fields=false)
{    
    $t_base="management/pendingTray/";  
    $kBooking_error = new cBooking();
    $kBooking_error = $kBooking ;
    
    if(!empty($_POST['quotePricingAry']))
    {
        $idBooking = $_POST['quotePricingAry']['idBooking'] ;   
        $idBookingFile = $_POST['quotePricingAry']['idBookingFile'] ;   
        
        if($idBookingFile>0)
        {
            $kBooking = new cBooking();
            $kBooking->loadFile($idBookingFile);
            $idBooking = $kBooking->idBooking ;   
        }
    }
    else
    {
        $idBooking = $kBooking->idBooking ;
        $idBookingFile = $kBooking->idBookingFile ;
    }  
    
    $szMainContainerClass="overview-form afq";
    $szFirstSpanClass="field-name";
    $szSecondSpanClass="field-container";
    
    $kConfig = new cConfig();
    $kForwarder = new cForwarder();
    $kAdmin = new cAdmin(); 
    $kUser = new cUser();
    
    $kBooking_new = new cBooking();
    
    $iSectionEdited = 0;
    if($edit_flag)
    {
        $iSectionEdited =1;
    }
    $bookingDataArr =  $kBooking_new->getBookingDetails($idBooking); 
    $iBookingLanguage = $bookingDataArr['iBookingLanguage'];
    
    $idCustomer = $bookingDataArr['idUser'];
    if($idCustomer>0)
    {
        $kUser->getUserDetails($idCustomer);
        $iPrivateCustomer = $kUser->iPrivate;
    } 
    
    $fValueOfGoods = $bookingDataArr['fValueOfGoods'] ;
    $fValueOfGoodsUSD = (float)$bookingDataArr['fValueOfGoodsUSD'] ; 
    $idGoodsInsuranceCurrency = $bookingDataArr['idGoodsInsuranceCurrency'] ;
    $szGoodsInsuranceCurrency = $bookingDataArr['szGoodsInsuranceCurrency'] ;
    $fGoodsInsuranceExchangeRate = $bookingDataArr['fGoodsInsuranceExchangeRate'] ;
    $fTotalBookingPriceUsd = $bookingDataArr['fTotalPriceUSD'];
    $iInsuranceChoice = $bookingDataArr['iInsuranceChoice'];
    $iPrivateShipping = $bookingDataArr['iPrivateShipping'];
            
    if($idGoodsInsuranceCurrency=='1')//USD
    {
        $fGoodsInsuranceExchangeRate = 1;   
    }
    else
    {
        $kWarehouseSearch = new cWHSSearch();
        $resultAry = $kWarehouseSearch->getCurrencyDetails($idGoodsInsuranceCurrency);		 
        $fGoodsInsuranceExchangeRate = $resultAry['fUsdValue'];
    }
            
    $insuranceDataAry = array();
    $insuranceDataAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
    $insuranceDataAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry']; 
    $insuranceDataAry['szCargoType'] = $bookingDataArr['szCargoType'];
    $insuranceDataAry['iGetAllRecord'] = 1;
             
    $kInsurance = new cInsurance();
    $buyRateListAry = array();
    $buyRateListAry = $kInsurance->getBuyrateCombination($insuranceDataAry,true);
             
    if(!empty($buyRateListAry))
    {
        foreach($buyRateListAry as $idTransportModeBuyRate=>$buyRateListArys)
        {     
            $insuranceDataAry['idTransportMode'] = $idTransportModeBuyRate;
            
            $insuranceSellRateAry = array();
            $insuranceSellRateAry = $kInsurance->getBuyrateCombination($insuranceDataAry,__INSURANCE_SELL_RATE_TYPE__,true); 
            if(!empty($insuranceSellRateAry[$idTransportModeBuyRate]))
            {
                $buyRateListAry[$idTransportModeBuyRate]['sellRateAry'] = $insuranceSellRateAry[$idTransportModeBuyRate]; 
            } 
        }  
    } 
    
    $kConfig = new cConfig();
    $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);
    
    //print_r($configLangArr);
    $szOfferText = $configLangArr[1]['szOfferText_pt'] ;
    $szFromText =  $configLangArr[1]['szFromText_pt']  ;
    $szToText =  $configLangArr[1]['szToText_pt']  ;
    $szCargoText =  $configLangArr[1]['szCargoText_pt'] ;
    $szModeText =  $configLangArr[1]['szModeText_pt'] ;
    $szTransitTimeText =  $configLangArr[1]['szTransitTimeText_pt'];
    $szPriceText =  $configLangArr[1]['szPriceText_pt'];
    $OfferValidUntillText =  $configLangArr[1]['OfferValidUntillText_pt'];
    $szInsuranceText =  $configLangArr[1]['szInsuranceText_pt'];
    $szDaysText =  " ".$configLangArr[1]['szDaysText_pt'];
    $szNoVatOnBookingText =  $configLangArr[1]['szNoVatOnBookingText_pt'];
    $szVatExcludingText = $configLangArr[1]['szVatExcludingText_pt'];        
    $szOfferHeading =  $configLangArr[1]['szOfferHeading_pt'].":";
    $szMultipleOfferHeading = $configLangArr[1]['szMultipleOfferHeading_pt'].":";
    $szMultipleOfferHeading2 = $configLangArr[1]['szMultipleOfferHeading2_pt'].":";
    $szInsuranceValueUptoText = " ".$configLangArr[1]['szInsuranceValueUptoText_pt'];        
    $szVatTextExcluingTextWhenOneQuote = $configLangArr[1]['szVatTextExcluingTextWhenOneQuote_pt']; 
    $szInsuranceOptionalText = $configLangArr[1]['szInsuranceOptionalText']; 
    $szServiceText =  $configLangArr[1]['szService']  ;
    
    if($iInsuranceChoice==3)
    {
        //This will read as Insurance (optional): 
        $szInsuranceText = $szInsuranceText." (".trim($szInsuranceOptionalText).")";
    }
    $kConfig = new cConfig();
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false,$iBookingLanguage,true,true);
     
    $add_js_hour = false;
    if(!empty($_POST['quotePricingAry']))
    {
        $quoteReceivedAry = $_POST['quotePricingAry']; 
        $szEmailSubject = $quoteReceivedAry['szEmailSubject'];
        $szEmailBody = $quoteReceivedAry['szEmailBody'];
        $szQuotationBody = $quoteReceivedAry['szQuotationBody'];
        $dtReminderDate = $quoteReceivedAry['dtReminderDate'];
        $szReminderTime = $quoteReceivedAry['szReminderTime'];
        $idBookingFile = $quoteReceivedAry['idBookingFile'];  
        $idBooking = $quoteReceivedAry['idBooking'];  
        $idCustomerCurrency = $quoteReceivedAry['idCustomerCurrency'];   
        $fCustomerExchangeRateGlobal = $quoteReceivedAry['fCustomerExchangeRateGlobal'];    
        $fPriceMarkUpGlobal = $quoteReceivedAry['fPriceMarkUpGlobal'];     
        
        $szOriginCountryStr = $bookingDataArr['szOriginCountry'];
        $szDestinationCountryStr = $bookingDataArr['szDestinationCountry']; 
        $szCustomerCurrency = $bookingDataArr['szCurrency'];    
        $fCustomerExchangeRate = $bookingDataArr['fExchangeRate']; 
        
        $kBooking_new->loadFile($idBookingFile);
        $szBookingFileStatus =  $kBooking_new->szFileStatus;
        $szBookingTaskStatus =  $kBooking_new->szTaskStatus;
        $iBookingSearchedType = $kBooking_new->iSearchType;
    }
    else
    {  
        $szBookingFileStatus = $kBooking->szFileStatus;
        $szBookingTaskStatus =  $kBooking->szTaskStatus;
        $iBookingSearchedType = $kBooking->iSearchType;
            
        $szCustomerCurrency = $bookingDataArr['szCurrency'];   
        $idCustomerCurrency = $bookingDataArr['idCurrency'];   
        //$fCustomerExchangeRate = $bookingDataArr['fExchangeRate'];   
        //$fCustomerExchangeRateGlobal = $bookingDataArr['fExchangeRate']; 
        
        if($idCustomerCurrency=='1') //USD
        {
            $fCustomerExchangeRateGlobal = 1;  
            $fCustomerExchangeRate = 1;
        }
        else
        {
            $kWarehouseSearch = new cWHSSearch();
            $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency);		 
            $fCustomerExchangeRateGlobal = $resultAry['fUsdValue'];
            $fCustomerExchangeRate = $resultAry['fUsdValue'];
        }
        
        $kWhsSearch = new cWHSSearch();  
        $fPriceMarkUpGlobal = $kWhsSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__'); 
        
        $kAdmin->getAdminDetails($_SESSION['admin_id']);
        
        $Config= new cConfig();
        $languageArr=$kConfig->getLanguageDetails('',$bookingDataArr['iBookingLanguage']);
        if(!empty($languageArr))
        {
            $szCbmText = $languageArr[0]['szDimensionUnit'];
        }
        else
        {
            $szCbmText = 'cbm';
        }
        
        if($bookingDataArr['idTransportMode']==4) //courier 
        { 
            $szVolWeight = format_volume($bookingDataArr['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$bookingDataArr['fCargoWeight'],$iBookingLanguage)."kg,  ".number_format_custom((int)$bookingDataArr['iNumColli'],$iBookingLanguage)."col";
        }
        else
        {
            $szVolWeight = format_volume($bookingDataArr['fCargoVolume'],$iBookingLanguage).$szCbmText.",  ".number_format_custom((float)$bookingDataArr['fCargoWeight'],$iBookingLanguage)."kg ";
        }
        $cargoDetailsAry = $kBooking_new->getCargoComodityDeailsByBookingId($idBooking); 
        $szCargoDescription = '';
        if(!empty($cargoDetailsAry))
        {
            $szCargoDescription = utf8_decode($cargoDetailsAry['1']['szCommodity']);  
        }
        if(!empty($szCargoDescription))
        {
            $szVolWeight = trim($szVolWeight).", ".$szCargoDescription ;
        }
       
        $kConfig = new cConfig();
        $kConfig->loadCountry($kAdmin->idInternationalDialCode);
        $iInternationDialCode = $kConfig->iInternationDialCode;

        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry'],false,$bookingDataArr['iBookingLanguage']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry'],false,$bookingDataArr['iBookingLanguage']) ; 
        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	 
        $fCargoVolume = format_volume($bookingDataArr['fCargoVolume']);
        $fCargoWeight = number_format((float)$bookingDataArr['fCargoWeight']);

        if(!empty($bookingDataArr['szOriginPostCode']))
        {
            $szOriginPostCode = $bookingDataArr['szOriginPostCode'].", ";
        }
        if(!empty($bookingDataArr['szDestinationPostCode']))
        {
            $szDestinationPostCode = $bookingDataArr['szDestinationPostCode'].", ";
        }
        $szOriginCountryStr = $szOriginPostCode."".$bookingDataArr['szOriginCity'].", ".$szOriginCountry;
        $szDestinationCountryStr = $szDestinationPostCode."".$bookingDataArr['szDestinationCity'].", ".$szDestinationCountry; 
        
        $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone;

        $szBaseUrl_str = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);	
        $szBaseUrl = addhttp(__MAIN_SITE_HOME_PAGE_URL__);

        $szSiteLink = "<a href='".$szBaseUrl."' target='_blank'>".$szBaseUrl_str."</a>";

        $kWhsSearch=new cWHSSearch();
        $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$bookingDataArr['iBookingLanguage']);
        
        $iManualQuoteSendFlag=true;
        if((int)$bookingDataArr['iStandardPricing']==1)
        {
            $iManualQuoteSendFlag=false;
        }
        $szServiceDescriptionString = display_service_type_description($bookingDataArr,$bookingDataArr['iBookingLanguage'],true,false,$iManualQuoteSendFlag); 
        
        $replace_ary = array(); 
        $replace_ary['szOriginCity'] = $bookingDataArr['szOriginCity'];
        $replace_ary['szOriginCountry'] = $szOriginCountry;
        $replace_ary['szDestinationCity'] = $bookingDataArr['szDestinationCity'] ;
        $replace_ary['szDestinationCountry'] = $szDestinationCountry ;
        $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'] ;
        $replace_ary['fCargoVolume'] = $fCargoVolume;
        $replace_ary['fCargoWeight'] = $fCargoWeight; 
        $replace_ary['szQuoteUrl'] = $szQuoteUrl;  
        $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
        $replace_ary['szTitle'] = $kAdmin->szTitle ;
        $replace_ary['szOfficePhone'] = $szCustomerCareNumer ;
        $replace_ary['szMobile'] = $szMobile ;
        $replace_ary['zCargoVolWeidghtColli'] = $szVolWeight ;
        //$replace_ary['szSiteUrl'] =  $szSiteLink;
        //echo $bookingDataArr['iStandardPricing']."iStandardPricing";
        if((int)$bookingDataArr['iStandardPricing']==1)
        { 
            $replace_ary['szFromPostcode'] = $bookingDataArr['szOriginPostCode'];
            $replace_ary['szFromCity'] = $bookingDataArr['szOriginCity'];
            $replace_ary['szFromCountry'] = $szOriginCountry; 
            
            $replace_ary['szToPostcode'] = $bookingDataArr['szDestinationPostCode'];
            $replace_ary['szToCity'] = $bookingDataArr['szDestinationCity'];
            $replace_ary['szToCountry'] = $szDestinationCountry;
            
            
            $replace_ary['szCargoDescription'] = utf8_decode($bookingDataArr['szCargoDescription']);
            $replace_ary['szVolume'] = format_volume($bookingDataArr['fCargoVolume'])." cbm";
            $replace_ary['szWeight'] = round_up($bookingDataArr['fCargoWeight'],2)." kg";
            
            $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'];
            $replace_ary['szLastName'] = $bookingDataArr['szLastName'];
            $replace_ary['szWebSiteUrl'] = __MAIN_SITE_HOME_PAGE_SECURE_URL__;
            
            $iAdminFlag=true;
            if($bookingDataArr['szAutomatedRfqResponseCode']!='')
            {
                $iAdminFlag=false;
            }
            $kExplain = new cExplain();
            $standardHandlerAry = array();        
            $standardHandlerAry = $kExplain->getQuoteHandlerByPageID($bookingDataArr['idLandingPage'],true,$iAdminFlag);
            
            $dtHandlerValidity = date('Y-m-d',strtotime("+ ".$standardHandlerAry['iQuoteValidity']." days"));
            
            $iMonth = (int)date("m",strtotime($dtHandlerValidity));
            $szMonthName = getMonthName($iBookingLanguage,$iMonth,true);
                    
            $szValidTo = date("j.",strtotime($dtHandlerValidity))." ".$szMonthName;
            $replace_ary['szValidTo'] = $szValidTo;
            $szEmailBody=$standardHandlerAry['szQuoteEmailBody'];
            $szEmailSubject=$standardHandlerAry['szQuoteEmailSubject'];
            if (count($replace_ary) > 0)
            {
                foreach ($replace_ary as $replace_key => $replace_value)
                {
                    $szEmailBody = str_replace($replace_key, $replace_value, $szEmailBody);
                    $szEmailSubject= str_replace($replace_key, $replace_value, $szEmailSubject);
                }
            }
            //$szEmailBody = nl2br($szEmailBody);
            //$breaksAry = array("<br />","<br>","<br/>");  
            //$szEmailBody = str_replace($breaksAry, PHP_EOL, $szEmailBody); 

            //$szEmailBody = str_replace($breaksAry, "\r\n", $szEmailSubject);  
            //$szEmailSubject = $emailMessageAry['szEmailSubject']; 
        }
        else
        {
            $emailMessageAry = array(); 
            $emailMessageAry = createEmailMessage('__CUSTOMER_SEND_BOOKING_QUOTES__', $replace_ary);

            $breaksAry = array("<br />","<br>","<br/>");  
            $szEmailBody = str_replace($breaksAry, "\r\n", $emailMessageAry['szEmailBody']); 

            $szEmailBody = str_replace($breaksAry, "\r\n", $szEmailBody);  
            $szEmailSubject = $emailMessageAry['szEmailSubject'];   
        }
        
        $idBookingFile = $kBooking->idBookingFile ;
        $dtReminderDateTime = $kBooking->dtCustomerRemindDate ;
        
        if(!empty($dtReminderDateTime) && $dtReminderDateTime!='0000-00-00 00:00:00')
        {
            $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ; 
            $date = new DateTime($dtReminderDateTime); 
            $date->setTimezone(new DateTimeZone($szCustomerTimeZone));   
            $dtReminderDate = $date->format('d/m/Y');
            $szReminderTime = $date->format('H:i'); 
            
            $add_js_hour = false;
        }
        else
        {
            $kWHSSearch=new cWHSSearch();
            $iForwarderResponseTime = $kWHSSearch->getManageMentVariableByDescription('__CUSTOMER_RESPONSE_TIME__');
            
            $add_js_hour = true;
        } 
        
        $searchDataAry = array();
        $searchDataAry['iClosed']=1; 
        $quotesAry = array();  
        $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile,$searchDataAry);
        
        $languageArr=$kConfig->getLanguageDetails('',$bookingDataArr['iBookingLanguage']);
    
        if(!empty($languageArr))
        {
            $iBookingLan = $bookingDataArr['iBookingLanguage'] ;
        }
        else
        {
            $iBookingLan = __LANGUAGE_ID_ENGLISH__ ;
        } 
        $iQuickQuoteDescriptionArr=array();    
        $szQuotationBody = '';
        if(!empty($quotesAry))
        {
           
            $iQuoteCount = count($quotesAry); 
            
            if($iQuoteCount==1)
            { 
                if((int)$bookingDataArr['iStandardPricing']==0)
                {
                    $szQuotationBody = $szOfferHeading."<br><br>&#13;&#10;" ; 
                }   
            }
            else
            { 
                if((int)$bookingDataArr['iStandardPricing']==0)
                {
                    if((int)$bookingDataArr['iQuickQuote']>1)
                    {
                         $szQuotationBody = $szMultipleOfferHeading."<br><br>" ; 
                        $szQuotationBody .= "&#13;&#10;&#13;&#10;".$szFromText.': '.$bookingDataArr['szOriginCountry'].' <br>&#13;&#10; '
                                    . ' '.$szToText.': '.$bookingDataArr['szDestinationCountry'].' <br>&#13;&#10;'
                                    . ''.$szCargoText.': '.$szVolWeight.'  <br>&#13;&#10;';
                    }
                    else
                    {
                        $szQuotationBody = $szMultipleOfferHeading."<br><br>" ; 
                        $szQuotationBody .= "&#13;&#10;&#13;&#10;".$szFromText.': '.$bookingDataArr['szOriginCountry'].' <br>&#13;&#10; '
                                    . ' '.$szToText.': '.$bookingDataArr['szDestinationCountry'].' <br>&#13;&#10;'
                                    . ''.$szServiceText.': '.$szServiceDescriptionString.' <br>&#13;&#10;'
                                    . ''.$szCargoText.': '.$szVolWeight.'  <br>&#13;&#10;';
                    }

                    $szQuotationBody .= "<br><br>&#13;&#10;&#13;&#10;".$szMultipleOfferHeading2." <br><br>" ; 
                }
            }
            
            
            $counter = 1;
            $num_counter = 1;
            foreach($quotesAry as $quotesArys)
            {
                $idForwarder = $quotesArys['idForwarder'];
                
                 $kForwarder = new cForwarder();
                $kForwarder->load($idForwarder);

                
                $iProfitType = $kForwarder->iProfitType;
                 
                if($quotesArys['iClosed']==1)
                {
                   
                    if((int)$bookingDataArr['iStandardPricing']==1)
                    {
                        if($iProfitType==__FORWARDER_PROFIT_TYPE_MARK_UP__)
                        {
                            $fTotalPriceCustomerCurrencyHidden = $quotesArys['fTotalPriceCustomerCurrency'] + $quotesArys['fReferalAmount'] ;
                            $fTotalPriceCustomerCurrency = round((float)($quotesArys['fTotalPriceCustomerCurrency'] + $quotesArys['fReferalAmount']));
                            $fTotalVat = round((float)($quotesArys['fTotalVatCustomerCurrency'] + $quotesArys['fReferalAmount_vat']),2);
                        }
                        else
                        {
                            $fTotalPriceCustomerCurrencyHidden =  $quotesArys['fTotalPriceCustomerCurrency'] ;
                            $fTotalPriceCustomerCurrency = round((float)($quotesArys['fTotalPriceCustomerCurrency'])) ;
                            $fTotalVat = round((float)($quotesArys['fTotalVatCustomerCurrency']),2);
                        }  
                        
                        if($iPrivateCustomer==1)
                        { 
                            $iDecimalPlaces = false;
                            if(__float($fTotalVat))
                            {
                                $iDecimalPlaces = 2;
                            }
                            $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)($fTotalPriceCustomerCurrency + $fTotalVat),$iBookingLanguage,$iDecimalPlaces)." all-in" ;
                            $szVatExcludingText =  $szVatTextExcluingTextWhenOneQuote;
                        }
                        else
                        {
                            $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)$fTotalPriceCustomerCurrency,$iBookingLanguage)." all-in" ;
                        } 
                        
                        if($fTotalVat>0)
                        {
                            if((int)$bookingDataArr['iStandardPricing']==1)
                            {    
                                $szQuotePriceStr .=", ".$szVatExcludingText." ".$szCustomerCurrency." ".number_format_custom((float)$fTotalVat,$iBookingLanguage,2) ;
                            }
                            else
                            {
                                $szQuotePriceStr .=", ".$szVatExcludingText." ".$szCustomerCurrency." ".number_format_custom((float)$fTotalVat,$iBookingLanguage,2) ;
                            } 
                        }
                    }
                    else
                    {
                        if($quotesArys['fForwarderCurrencyExchangeRate']>0)
                        {
                            $fTotalPriceForwarderCurrency_usd = $quotesArys['fTotalPriceForwarderCurrency']*$quotesArys['fForwarderCurrencyExchangeRate'] ;
                            $fTotalVat_usd = $quotesArys['fTotalVat']*$quotesArys['fForwarderCurrencyExchangeRate'];
                        }   
                        if($fCustomerExchangeRate>0)
                        {
                            $fTotalPriceCustomerCurrency = round((float)($fTotalPriceForwarderCurrency_usd / $fCustomerExchangeRate));
                            $fTotalVat = round((float)($fTotalVat_usd / $fCustomerExchangeRate),2) ;
                        } 
                        
                        if($iPrivateCustomer==1)
                        { 
                            $iDecimalPlaces = false;
                            if(__float($fTotalVat))
                            {
                                $iDecimalPlaces = 2;
                            }
                            $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)($fTotalPriceCustomerCurrency + $fTotalVat),$iBookingLanguage,$iDecimalPlaces)." all-in" ;
                            $szVatExcludingText =  $szVatTextExcluingTextWhenOneQuote;
                        }
                        else
                        {
                            $szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)$fTotalPriceCustomerCurrency,$iBookingLanguage)." all-in" ;
                        }
                        //$szQuotePriceStr =  $szCustomerCurrency." ".number_format_custom((float)$fTotalPriceCustomerCurrency,$iBookingLanguage)." all-in" ;
                        if($fTotalVat>0)
                        {
                            $szQuotePriceStr .=", ".$szVatExcludingText." ".$szCustomerCurrency." ".number_format_custom((float)$fTotalVat,$iBookingLanguage,2) ;
                        }
                    }
                    
                    $dtOfferValidDay = date('d',strtotime($quotesArys['dtQuoteValidTo']));
                    
                    $dtOfferValidMonth = date('m',strtotime($quotesArys['dtQuoteValidTo']));
                    $dtOfferValidYear = date('Y',strtotime($quotesArys['dtQuoteValidTo']));
                    $szMonthName = getMonthName($iBookingLan,$dtOfferValidMonth,true);
                    $dtOfferValid=$dtOfferValidDay." ".$szMonthName." ".$dtOfferValidYear;
                    $szTransportMode = $transportModeListAry[$quotesArys['idTransportMode']]['szLongName'];
                   
                    if($iQuoteCount==1)
                    { 
                        if((int)$bookingDataArr['iStandardPricing']==1)
                        {
                            /* 
                            $szQuotationBody .= "&#13;&#10;&#13;&#10;".$szModeText.': '.$szTransportMode.' <br>&#13;&#10;'
                                . ''.$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText.' <br>&#13;&#10;'
                                . ''.$szPriceText.': '.$szQuotePriceStr.' <br><br>&#13;&#10;&#13;&#10;szBookingQuoteLink_'.$num_counter.' <br><br>';
                             * 
                             */ 
                            
                            $szQuotationBody .= $szModeText.': '.$szTransportMode.'<br>'. ''.$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText.'<br>'. ''.$szPriceText.': '.$szQuotePriceStr.'<br><br>'.'szBookingQuoteLink_'.$num_counter;
                        }
                        else
                        {
                            if((int)$bookingDataArr['iQuickQuote']>1)
                            {
                                $szServiceDescriptionString = display_service_type_description($bookingDataArr,$bookingDataArr['iBookingLanguage'],true,false,$iManualQuoteSendFlag,$quotesArys['szHandoverCity']); 
                                $iQuickQuoteDescriptionArr[$quotesArys['id']]=$szServiceDescriptionString;
                                $szQuotationBody .= "&#13;&#10;&#13;&#10;".$szFromText.': '.$bookingDataArr['szOriginCountry'].' <br>&#13;&#10; '
                                . ''.$szToText.': '.$bookingDataArr['szDestinationCountry'].' <br>&#13;&#10;'                                
                                . ''.$szCargoText.': '.$szVolWeight.'  <br>&#13;&#10;'
                                . ''.$szModeText.': '.$szTransportMode.' <br>&#13;&#10;'
                                . ''.$szServiceText.': '.$szServiceDescriptionString.' <br>&#13;&#10;'        
                                . ''.$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText.' <br>&#13;&#10;'
                                . ''.$szPriceText.': '.$szQuotePriceStr.' <br>&#13;&#10;'
                                . ''.$OfferValidUntillText.' '.$dtOfferValid.' <br><br>&#13;&#10;&#13;&#10;szBookingQuoteLink_'.$num_counter.'<br>&#13;&#10;&#13;&#10;
                            ';
                            }
                            else
                            {
                                $szQuotationBody .= "&#13;&#10;&#13;&#10;".$szFromText.': '.$bookingDataArr['szOriginCountry'].' <br>&#13;&#10; '
                                    . ''.$szToText.': '.$bookingDataArr['szDestinationCountry'].' <br>&#13;&#10;'
                                    . ''.$szServiceText.': '.$szServiceDescriptionString.' <br>&#13;&#10;'
                                    . ''.$szCargoText.': '.$szVolWeight.'  <br>&#13;&#10;'
                                    . ''.$szModeText.': '.$szTransportMode.' <br>&#13;&#10;'
                                    . ''.$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText.' <br>&#13;&#10;'
                                    . ''.$szPriceText.': '.$szQuotePriceStr.' <br>&#13;&#10;'
                                    . ''.$OfferValidUntillText.' '.$dtOfferValid.' <br><br>&#13;&#10;&#13;&#10;szBookingQuoteLink_'.$num_counter.'<br>&#13;&#10;&#13;&#10;
                                ';
                            }
                        } 
                    }
                    else
                    {
                        
                        if((int)$bookingDataArr['iStandardPricing']==1)
                        {
                            if($num_counter>1)
                            {
                                $szQuotationBody .= '<br><br>';
                            } 
                            $szQuotationBody .= "<strong><u>".$counter.'. '.$szOfferText.' </u></strong>'.'<br>'.$szModeText.': '.$szTransportMode.'<br>'. ''.$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText.'<br>'. ''.$szPriceText.': '.$szQuotePriceStr.'<br><br>'.'szBookingQuoteLink_'.$num_counter;
                        }
                        else
                        {
                            if((int)$bookingDataArr['iQuickQuote']>1)
                            {
                                $szServiceDescriptionString = display_service_type_description($bookingDataArr,$bookingDataArr['iBookingLanguage'],true,false,$iManualQuoteSendFlag,$quotesArys['szHandoverCity']);
                                $iQuickQuoteDescriptionArr[$quotesArys['id']]=$szServiceDescriptionString;
                                $szQuotationBody .= "<strong><u>".$counter.'. '.$szOfferText.' </u></strong><br>'. ' '.$szModeText.': '.$szTransportMode.' <br>&#13;&#10;'
                                    . ''.$szServiceText.': '.$szServiceDescriptionString.' <br>&#13;&#10;'
                                    . ''.$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText.' <br>&#13;&#10;'
                                    . ''.$szPriceText.': '.$szQuotePriceStr.' <br>&#13;&#10;'
                                    . ''.$OfferValidUntillText.' '.$dtOfferValid.' <br><br>&#13;&#10;&#13;&#10;szBookingQuoteLink_'.$num_counter.' <br><br>&#13;&#10;&#13;&#10;
                                ';
                            }
                            else
                            {
                                $szQuotationBody .= "<strong><u>".$counter.'. '.$szOfferText.' </u></strong><br>'. ' '.$szModeText.': '.$szTransportMode.' <br>&#13;&#10;'
                                    . ''.$szTransitTimeText.': '.$quotesArys['iTransitHours'].''.$szDaysText.' <br>&#13;&#10;'
                                    . ''.$szPriceText.': '.$szQuotePriceStr.' <br>&#13;&#10;'
                                    . ''.$OfferValidUntillText.' '.$dtOfferValid.' <br><br>&#13;&#10;&#13;&#10;szBookingQuoteLink_'.$num_counter.' <br><br>&#13;&#10;&#13;&#10;
                                ';
                            }
                        } 
                    } 
                    $counter++;
                    $num_counter++;
                }
            } 
        } 
        $quotesAry = array();  
        $quotesAry = $kBooking_new->getAllBookingQuotesForQuoteScreen($idBookingFile); 
    }   
    if((int)$bookingDataArr['iStandardPricing']==1)
    { 
        $szEmailBody = str_replace('szQuote', $szQuotationBody, $szEmailBody);
        //$breaksAry = array("<br>","<br />","<br/>");  
        //$szEmailBody = str_replace($breaksAry, PHP_EOL, $szEmailBody);  
        $szQuotationBody='';
    } 
    if(!empty($szQuotationBody))
    {
        $szQuotationBody = checkaddslashes($szQuotationBody);
    }
    $allCurrencyArr = array();
    $allCurrencyArr=$kConfig->getBookingCurrency(false,true);
     
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(true); 
    $kWhsSearch = new cWHSSearch();  
           
    $transportProductAry = array();
    $transportProductAry = $kConfig->getAllTransportMode();
    
    $szRowContainerClass = "quote-row-container clearfix";
    $szMainContainerClass="clearfix";
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container"; 
    
    $fInsuranceSellCurrencyExchangeRate = 0.1451;
            
    $iAlreadyPaidBooking = 0;    
    $iAlreadyPaidBooking = checkalreadyPaidBooking($postSearchAry['idBookingStatus'],$szBookingTaskStatus,$szBookingFileStatus,$iBookingSearchedType);
    $kVatApplication = new cVatApplication();
    
    $fApplicableVatRate = 0;
    if($bookingDataArr['iPrivateShipping']==1)
    {
        $vatInputAry = array();
        $vatInputAry['idCustomerCountry'] = $bookingDataArr['szCustomerCountry'];
        $vatInputAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
        $vatInputAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
        
        $fApplicableVatRate = $kVatApplication->getTransportecaVat($vatInputAry);   
    }
    $kReferralPricing = new cReferralPricing();
    ?>   
    <script type="text/javascript">
        var profitTypeAry = new Array();
        var referalPercentageAry = new Array();
        var currencyListAry = new Array();
        var exchangeRateListAry = new Array();
        var forwarderAccountCurrencyAry = new Array();
        var forwarderLogoAry = new Array();
        var forwarderVatAry = new Array();
        var iOffLineQuotesAry= new Array(); 
        var iQuickQuoteDescriptionArr = new Array();
        <?php
        if(!empty($quotesAry))
        {
            foreach($quotesAry as $quotesArys)
            {
                if((int)$bookingDataArr['iQuickQuote']>1)
                {
                    $szServiceDescriptionString = display_service_type_description($bookingDataArr,$bookingDataArr['iBookingLanguage'],true,false,$iManualQuoteSendFlag,$quotesArys['szHandoverCity']);
                    ?>
                    iQuickQuoteDescriptionArr['<?php echo $quotesArys['id'];?>']='<?php echo checkaddslashes($szServiceDescriptionString);?>';
                    <?php
                }
                             
            }
        }
       ?>
        <?php if($add_js_hour){ ?> 
            setDefaultReminderDate('<?php echo $iForwarderResponseTime; ?>');    
        <?php } 
            if(!empty($forwarderListAry))
            { 
                foreach($forwarderListAry as $forwarderListArys)
                {     
                    $idForwarderCountry = $forwarderListArys['idCountry']; 
                    
                    ?>
                    profitTypeAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $forwarderListArys['iProfitType'];  ?>'; 
                    forwarderAccountCurrencyAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $forwarderListArys['szCurrency'];  ?>';
                    forwarderLogoAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $forwarderListArys['szLogo'];  ?>';
                    forwarderVatAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $fApplicableVatRate;  ?>';
                    iOffLineQuotesAry['<?php echo $forwarderListArys['id']?>'] = '<?php echo $forwarderListArys['iOffLineQuotes'];  ?>';
                    
                    referalPercentageAry['<?php echo $forwarderListArys['id']?>'] = new Array();
                    <?php
                    if(!empty($transportProductAry))
                    {
                        $idForwarder = $forwarderListArys['id'];
                        
                        $forwarderReferralFeeAry = array();
                        $forwarderReferralFeeAry = $kReferralPricing->getAllReferralFeeByForwarder($idForwarder);
                       
                        foreach($transportProductAry as $transportModeArys)
                        {
                            $fReferralFee = (float)$forwarderReferralFeeAry[$transportModeArys['id']]['fReferralFee']; 
                            ?>
                            referalPercentageAry['<?php echo $idForwarder ?>']['<?php echo $transportModeArys['id']; ?>'] = '<?php echo $fReferralFee; ?>'
                            <?php 
                        } 
                    }
                }
            } 
            if(!empty($allCurrencyArr))
            {
                $currencyInputAry = array();
                foreach($allCurrencyArr as $allCurrencyArrs)
                {
                    $currencyInputAry[] = $allCurrencyArrs['id'];
                    ?>
                       currencyListAry['<?php echo $allCurrencyArrs['id']; ?>'] = '<?php echo $allCurrencyArrs['szCurrency'] ?>' 
                    <?php
                }
            }
            $currencyExchangeListAry = array();
            $currencyExchangeListAry = $kWhsSearch->getCurrencyDetails(false,$currencyInputAry);
            if(!empty($currencyExchangeListAry))
            {
                $alreadyCurrency[0] = 1;
                $ctr=1;
                foreach($currencyExchangeListAry as $currencyExchangeListArys)
                {
                    if($currencyExchangeListArys['fUsdValue']>0 && !in_array($currencyExchangeListArys['idCurrency'],$alreadyCurrency))
                    {
                        $fCurrencyExhang = round((float)$currencyExchangeListArys['fUsdValue'],4); 
                        $alreadyCurrency[$ctr] = $currencyExchangeListArys['idCurrency'];
                        $ctr++;
                        
                    ?>
                    exchangeRateListAry['<?php echo $currencyExchangeListArys['idCurrency']?>'] = '<?php echo $fCurrencyExhang; ?>';
                    <?php }
                }
            }
        ?> 
             
        exchangeRateListAry[1] = 1 ; //USD 
        function updateReferralPercentage(number)
        {
            var idForwarder = $("#idForwarder_"+number).val();
            if(idForwarder>0)
            {
                autofill_profit_type(idForwarder,number)
            }
        }
        function autofill_profit_type(forwarder_id,number)
        { 
            var idTransportMode = $("#idTransportMode_"+number).val();
            var iProfitType = profitTypeAry[forwarder_id];
            //alert(referalPercentageAry[forwarder_id]);
            if(idTransportMode>0)
            {
                var fReferralPercentage = referalPercentageAry[forwarder_id][idTransportMode]; 
            }
            else
            {
                var fReferralPercentage = 0; 
            } 
            var idForwarderAccountCurrency = forwarderAccountCurrencyAry[forwarder_id];  
            var szForwarderLogo = forwarderLogoAry[forwarder_id];
            var fApplicableVatRate = forwarderVatAry[forwarder_id];
            var iOffLineQuotes = iOffLineQuotesAry[forwarder_id];
            //parseInt($("#iManualFeeApplicable_"+number).val());
            var iManualFeeApplicable = parseInt($("#iManualFeeApplicable_"+number).val());;
             $("#iOffLineQuotes_"+number).attr('value',iOffLineQuotes);
            if(iProfitType==2)
            {
                $("#gross_profit_type_span_"+number).html('Markup');
                $("#gross_profit_title_span_"+number).html('Gross profit (%)');
                //$("#fTotalPriceCustomerCurrency_"+number).attr('readonly',false);
                $("#customer_price_text_container_"+number).attr('style','display:none;');
                $("#fTotalPriceCustomerCurrency_"+number).attr('style','display:block;')
                
                $("#quote_price_text_container_"+number).attr('style','display:none;');
                $("#fQuotePriceCustomerCurrency_"+number).attr('style','display:block;');

                //$("#customer_vat_price_text_container_"+number).attr('style','display:none;');
                //$("#fTotalVat_"+number).attr('style','display:block;');
                
                $("#iManualFeeApplicable_"+number).val(0)
            }
            else
            {
                $("#quote_price_text_container_"+number).attr('style','display:block;');
                $("#fQuotePriceCustomerCurrency_"+number).attr('style','display:none;');
                
                $("#customer_price_text_container_"+number).attr('style','display:block;');
                $("#fTotalPriceCustomerCurrency_"+number).attr('style','display:none;')
                
                //$("#customer_vat_price_text_container_"+number).attr('style','display:block;');
                //$("#fTotalVat_"+number).attr('style','display:none;')
                
                $("#gross_profit_type_span_"+number).html('Referral'); 
               if(iManualFeeApplicable==1)
               {
                   $("#gross_profit_title_span_"+number).html('Referral fee (%)');
               }
               else
               {
                   $("#gross_profit_title_span_"+number).html('Gross profit (%)');
               }
               $("#iManualFeeApplicable_"+number).val(1);
            } 
            
            $("#idForwarderAccountCurrency_"+number).val(idForwarderAccountCurrency);  
            $("#fReferalPercentage_"+number).val(fReferralPercentage); 
            $("#fReferalPercentageStandarad_"+number).val(fReferralPercentage); 
            $("#fApplicableVatRate_"+number).val(fApplicableVatRate); 
            $("#iProfitType_"+number).val(iProfitType); 
            
            if(szForwarderLogo.length>0)
            {
                var szImagePath = "<?php echo __BASE_STORE_INC_URL__; ?>/image.php?img="+szForwarderLogo+"&w=100&h=100" ;
                var img_str = '<img src="'+szImagePath+'">';
                
                $("#quote-forwarder-logo-container-"+number).html(img_str);
                $("#szForwarderLogoPath_"+number).val(szForwarderLogo);  
            }
            
            calculate_gross_profit(number,'PERCENTAGE'); 
            updateEmailBody(number);
        }
        
        function autofill_vat_currency(currency_id,number)
        {
            var szCurrency = currencyListAry[currency_id] ; 
            $("#vat_currency_span_"+number).html(szCurrency); 
        }
        function autofill_currency_exhchange_rate(currency_id,number,handling_fee)
        {
            var fExchangeRate = exchangeRateListAry[currency_id] ;
            if(handling_fee==1)
            {
                $("#fHandlingCurrencyExchangeRate_"+number).val(fExchangeRate); 
            }
            else
            {
                $("#fForwarderExchangeRate_"+number).val(fExchangeRate); 
            } 
        } 
        function calculateVatAmount(number)
        { 
            //alert("Vat calc called...");
            var fApplicableVatRate = $("#fApplicableVatRate_"+number).val();
            var fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrency_"+number).val(); 
            fApplicableVatRate = parseFloat(fApplicableVatRate); 
            fTotalPriceCustomerCurrency = parseFloat(fTotalPriceCustomerCurrency);    
            if(isNaN(fApplicableVatRate))
            {
                fApplicableVatRate = 0;
            } 
            if(isNaN(fTotalPriceCustomerCurrency))
            {
                fTotalPriceCustomerCurrency = 0;
            } 
            if(fApplicableVatRate>0)
            {
                var fTotalVat = fTotalPriceCustomerCurrency * fApplicableVatRate * .01 ; 
                fTotalVat = fTotalVat.toFixed(2); 
                var fTotalVatString = number_format(fTotalVat,'2',".",",");
                
                $("#vat_text_container_"+number).html(fTotalVatString);   
                $("#vat_text_container_"+number).css('display','block');  
                $("#fTotalVatHidden_"+number).val(fTotalVat);  
                $("#customer_vat_price_text_container_"+number).html(fTotalVatString);  
                $("#fTotalVat_"+number).attr('value',fTotalVat);  
                $("#iPickVatFromHidden_"+number).val(1); 
                $("#vat_input_container_"+number).css('display','none'); 
            }
            else
            {
                $("#fTotalVat_"+number).val('0.00');
                $("#fTotalVatHidden_"+number).val('0.00');
                $("#customer_vat_price_text_container_"+number).html('0.00');
                $("#iPickVatFromHidden_"+number).val('0'); 
                $("#vat_text_container_"+number).css('display','none');  
                $("#vat_input_container_"+number).css('display','block');  
            }
            
            var fTotalPriceForwarderCurrency = $("#fTotalPriceForwarderCurrency_"+number).val(); 
            var szForwarderQuoteCurrencyName = $("#szForwarderQuoteCurrencyName_"+number).val(); 
            
            fTotalPriceForwarderCurrency = parseFloat(fTotalPriceForwarderCurrency);    
            if(isNaN(fTotalPriceForwarderCurrency))
            {
                fTotalPriceForwarderCurrency = 0;
            } 
            if(fApplicableVatRate>0)
            {
                var fTotalVatForwarder = fTotalPriceForwarderCurrency * fApplicableVatRate * .01 ; 
                fTotalVatForwarder = fTotalVatForwarder.toFixed(2);
                
                var szForwarderVatString = szForwarderQuoteCurrencyName+" "+number_format(fTotalVatForwarder,'2',".",",");
                
                if($("#forwarder_vat_field_container_"+number).length)
                { 
                    $("#forwarder_vat_field_container_"+number).html(szForwarderVatString);
                } 
                //$("#fTotalVatForwarder_"+number).val(fTotalVatForwarder); 
                //$("#szTotalVatForwarderPrice_"+number).val(szForwarderVatString);  
            }
        }
        NiceEditorInstance = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance("szEmailBody",{hasPanel : true});
        
        NiceEditorInstance.addEvent('blur', function() {
            enableQuoteSendButton();
        });
    </script>
     
    <input type="hidden" name="szCustomerCurrency_email" id="szCustomerCurrency_email" value="<?php echo $szCustomerCurrency; ?>">
    <input type="hidden" name="szOriginCountry_email" id="szOriginCountry_email" value="<?php echo $szOriginCountryStr; ?>">
    <input type="hidden" name="szDestinationCountry_email" id="szDestinationCountry_email" value="<?php echo $szDestinationCountryStr; ?>">
    <input type="hidden" name="szCargoText_email" id="szCargoText_email" value="<?php echo $szVolWeight; ?>">  
    
    <input type="hidden" name="quotePricingAry[idGoodsInsuranceCurrency]" id="idGoodsInsuranceCurrency" value="<?php echo $idGoodsInsuranceCurrency; ?>">
    <input type="hidden" name="quotePricingAry[szGoodsInsuranceCurrency]" id="szGoodsInsuranceCurrency" value="<?php echo $szGoodsInsuranceCurrency; ?>">
    <input type="hidden" name="quotePricingAry[fGoodsInsuranceExchangeRate]" id="fGoodsInsuranceExchangeRate" value="<?php echo $fGoodsInsuranceExchangeRate; ?>">
    <input type="hidden" name="quotePricingAry[fValueOfGoodsUSD]" id="fValueOfGoodsUSD" value="<?php echo $fValueOfGoodsUSD; ?>"> 
    <input type="hidden" name="quotePricingAry[fValueOfGoods]" id="fValueOfGoods" value="<?php echo $fValueOfGoods; ?>"> 
    <input type="hidden" name="quotePricingAry[iAlreadyPaidBooking]" id="iAlreadyPaidBooking" value="<?php echo $iAlreadyPaidBooking; ?>" />
    <input type="hidden" name="quotePricingAry[iPrivateCustomer]" id="iPrivateCustomer" value="<?php echo $iPrivateCustomer; ?>" />
    
      
    <form spellcheck="false" id="pending_task_tray_form" name="pending_task_tray_form" method="post" action="">
       <div class="clearfix">  
        <div id="booking_quote_main_container_table" class="clearfix">  
            <div class="horizontal-scrolling-div-container">
                <div id="horizontal-scrolling-div-id">
                <?php 
                    $counter = 0; 
                    if(count($_POST['quotePricingAry']['idTransportMode'])>0 || !empty($quotesAry))
                    { 
                        if(count($_POST['quotePricingAry']['idTransportMode'])>0)
                        {
                           $loop_counter = count($_POST['quotePricingAry']['idTransportMode']);   
                        }
                        else
                        {
                            $loop_counter = count($quotesAry);   
                        }      
                        for($j=0;$j<$loop_counter;$j++)
                        {
                            $counter = $j;
                            $ary_ctr = $counter + 1 ;
                            ?>
                            <div id="add_booking_quote_container_<?php echo $j; ?>" class="quote-pricing-fields"> 
                                <?php
                                    echo display_booking_quote_pricing_details($kBooking,$counter,$quotesAry[$ary_ctr],$edit_flag,false,$edit_all_fields);
                                 ?> 
                            </div>
                            <?php
                        }
                    }
                    else
                    {  
                        ?>
                        <div id="add_booking_quote_container_0" class="quote-pricing-fields"> 
                             <?php
                                echo display_booking_quote_pricing_details($kBooking,$counter,$quotesAry,$edit_flag,false,$edit_all_fields);
                             ?>
                        </div>
                        <?php
                    }
                ?>
                    <div id="add_booking_quote_container_<?php echo $counter+1; ?>" style="display:none;"></div>
                </div>  
            </div>
            <div class="quote-button-container">
                <a class="button1" href="javascript:void(0);" onclick="add_more_booking_quote('ADD_MORE_BOOKING_QUOTES_PRICING','<?php echo $idBookingFile ?>');">
                    <span style="min-width: 50px;">+ Add</span>
                </a>
                <a class="button1" href="javascript:void(0);" onclick="display_task_menu_details('QUOTE','<?php echo $idBookingFile; ?>','DISPLAY_TASK_QUOTE_MANUAL_EDIT_FORM')">
                    <span style="min-width: 50px;">Edit</span>
                </a>
                <a class="button1" href="<?php echo __BASE_URL__."/checkPricings.php?file_id=".$idBookingFile; ?>" target="_blank">
                    <span style="min-width: 50px;">Check Pricing</span>
                </a>
            </div>
        </div>  
    </div> 
    
    <div class="clear-all"></div>  
    <div class="email-box-container"> 
        <div class="<?php echo $szMainContainerClass; ?> email-fields-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/subject')?></span>
            <span class="<?php echo $szSecondSpanClass; ?>">
                <input type="text" name="quotePricingAry[szEmailSubject]" onblur="enableQuoteSendButton();" id="szEmailSubject" value="<?php echo $szEmailSubject; ?>">
            </span>    
        </div>
        <div class="<?php echo $szMainContainerClass; ?> email-fields-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/email_body')?></span>
            <span class="<?php echo $szSecondSpanClass; ?>">
                <textarea name="quotePricingAry[szEmailBody]" id="szEmailBody" style="min-height: 100px;" onblur="enableQuoteSendButton();"><?php echo $szEmailBody; ?></textarea>
            </span>    
        </div>  
        <div class="<?php echo $szMainContainerClass; ?> email-fields-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/quote_body')?></span>
            <span class="<?php echo $szSecondSpanClass; ?>">
                <textarea style="min-height: 100px;" name="quotePricingAry[szQuotationBody]" id="szQuotationBody" onblur="enableQuoteSendButton();"><?php echo $szQuotationBody; ?></textarea>
            </span>    
        </div> 
        <div class="<?php echo $szMainContainerClass; ?> reminder-fields-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/remind'); ?></span>
            <span class="<?php echo $szSecondSpanClass; ?>">
                <input type="text" name="quotePricingAry[dtReminderDate]" id="dtReminderDate" readonly value="<?php echo $dtReminderDate; ?>" onblur="enableQuoteSendButton();">
                <input type="text" maxlength="5" name="quotePricingAry[szReminderTime]" id="szReminderTime" onblur="format_reminder_time(this.value);enableQuoteSendButton();" value="<?php echo $szReminderTime; ?>"> 
            </span>    
            <span class="quote-remider-time-format" style="float: left;color:#999;font-style: italic;padding: 2px 0 0;width: 5%;">(hh:mm)</span>
        </div>  
        <?php
        
        if($iAlreadyPaidBooking==1)
        {
            $button_class = 'button2';
        }
        else
        {
            $button_class = 'button1';
            $style= "style='opacity:0.4'" ;
        } 
        ?>
        <?php if($iAlreadyPaidBooking!=1){?>
            <div class="btn-container">
                <a href="javascript:void(0);" id="request_quote_send_button" class="<?php echo $button_class; ?>" <?php echo $style; ?>><span>Send</span></a> 
                <?php  if($success_message==3){ echo '<span style="color:green;"> Sent! </span>'; } ?> 
                <?php  if($success_message==2){ echo '<span style="float:left;"> All values have been reset </span>'; } ?>
                <span style="float:right;">
                    <a href="javascript:void(0);" id="request_quote_preview_button" class="<?php echo $button_class; ?>" <?php echo $style; ?>><span>Preview</span></a>
                    <a href="javascript:void(0);" id="request_quote_save_button" class="<?php echo $button_class; ?>" <?php echo $style; ?>><span>Save</span></a> 
                    <a href="javascript:void(0);" id="request_quote_reset_button" class="<?php echo $button_class; ?>" onclick="display_task_menu_details('QUOTE','<?php echo $idBookingFile; ?>','RESET_TASK_QUOTE_PANE')"><span>Reset</span></a>
                    <?php  if($success_message==1){ echo '<br><span style="color:green;"> Saved! </span>'; } 
                        if(!empty($kBooking_error->arErrorMessages))
                        {
                            echo '<br /> <span style="color:red;text-align:center;float:right">Not Saved! </span>'; 
                        }
                    ?>
                </span>
            </div> 
        <?php } ?>
    </div> 
    <div class="clear-all"></div>  
    <div id="customs_clearance_pop" class="help-pop right"></div>
    <br><br>
        <input type="hidden" name="quotePricingAry[hiddenPosition]" id="hiddenPosition" value="<?php echo $counter+1 ;?>" />
        <input type="hidden" name="quotePricingAry[hiddenPosition1]" id="hiddenPosition1" value="<?php echo $counter+1 ;?>" />
        <input type="hidden" name="quotePricingAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>" /> 
        <input type="hidden" name="quotePricingAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>" />  
        <input type="hidden" name="quotePricingAry[iSectionEdited]" id="iSectionEdited" value="<?php echo $iSectionEdited; ?>" />  
        <input type="hidden" name="quotePricingAry[idCustomerCurrency]" id="idCustomerCurrency" value="<?php echo $idCustomerCurrency; ?>" />  
        <input type="hidden" name="quotePricingAry[fCustomerExchangeRateGlobal]" id="fCustomerExchangeRateGlobal" value="<?php echo $fCustomerExchangeRateGlobal; ?>" />  
        <input type="hidden" name="quotePricingAry[fPriceMarkUpGlobal]" id="fPriceMarkUpGlobal" value="<?php echo $fPriceMarkUpGlobal; ?>" /> 
        <input type="hidden" name="quotePricingAry[iStandardPricing]" id="iStandardPricing" value="<?php echo $bookingDataArr['iStandardPricing']; ?>" />
        <input type="hidden" name="quotePricingAry[iQuickQuote]" id="iQuickQuote" value="<?php echo $bookingDataArr['iQuickQuote']; ?>" />
         
    </form>   
    <div class="clear-all"></div>  
    <div id="quote_email_previewer_div" style="display:none;"></div>
    <script type="text/javascript">
        $("#dtReminderDate").datepicker();
        var transportModeAry = new Array();
        <?php
            if(!empty($transportModeListAry))
            {
                foreach($transportModeListAry as $transportModeListArys)
                {
                    ?>
                    transportModeAry[<?php echo $transportModeListArys['id'] ?>] = '<?php echo $transportModeListArys['szLongName']?>'
                    <?php
                }
            }
          
            ?>
        enableQuoteSendButton();  
        
        function updateEmailBody(number)
        {
            var hiddenPosition = $("#hiddenPosition").val();
            var hiddenPosition1 = $("#hiddenPosition1").val(); 
            
            var szCustomerCurrency = $("#szCustomerCurrency_email").val();
            var szOriginCountry = $("#szOriginCountry_email").val(); 
            var szDestinationCountry = $("#szDestinationCountry_email").val(); 
            var szCargoText = $("#szCargoText_email").val();
            var szGoodsInsuranceCurrency = $("#szGoodsInsuranceCurrency").val();
            var fValueOfGoods = $("#fValueOfGoods").val();
            var iPrivateCustomer = $("#iPrivateCustomer").val();
            
            var szEmailBodyText = '';
            var counter = 1;
            var active_counter = 0;
            var iLanguage = '<?php echo $iBookingLanguage; ?>';  
            var active_counter=$('input.quoteActiveCb:checked').length 
            var iQuickQuote=$("#iQuickQuote").val();
            if(active_counter>0)
            {    
                $("#szEmailSubject").removeAttr('disabled','disabled');
                $("#szQuotationBody").removeAttr('disabled','disabled');
                $("#szEmailBody").removeAttr('disabled','disabled');
                
                if(iLanguage==2)
                {
                    var decimal_seprator = ",";
                    var thousand_seprator = ".";
                }
                else
                {
                    var decimal_seprator = ".";
                    var thousand_seprator = ",";
                }
                //number_format(67000, 0, ',', '.'); 
                if(active_counter==1)
                { 
                    szEmailBodyText = '<?php echo $szOfferHeading; ?>' + "<br><br>\n\n" ; 
                }
                else
                { 
                    if(parseInt(iQuickQuote)>1)
                    {
                        szEmailBodyText = '<?php echo $szMultipleOfferHeading ?>' + "<br><br>" ; 
                        szEmailBodyText += "\n\n" + '<?php echo $szFromText; ?>' + ': ' + szOriginCountry + ' <br>\n'
                                    + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + ' <br>\n'
                                    + '<?php echo $szCargoText; ?>' + ': '+ szCargoText + '  <br> \n ';
                    }
                    else
                    {
                        szEmailBodyText = '<?php echo $szMultipleOfferHeading ?>' + "<br><br>" ; 
                        szEmailBodyText += "\n\n" + '<?php echo $szFromText; ?>' + ': ' + szOriginCountry + ' <br>\n'
                                    + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + ' <br>\n'
                                    + '<?php echo $szServiceText; ?>' + ': '+ '<?php echo checkaddslashes($szServiceDescriptionString); ?>' +' <br>\n'
                                    + '<?php echo $szCargoText; ?>' + ': '+ szCargoText + '  <br> \n ';
                    }

                    szEmailBodyText += "<br><br>\n\n" + '<?php echo $szMultipleOfferHeading2; ?>' + " <br><br>\n\n" ; 
                }
                
                var number = 0;
                var num_counter = 1;
                for(number =0; number<=hiddenPosition;number++)
                { 
                    var cb_flag = false;
                    if($("#iActive_"+number).length)
                    {
                        cb_flag = $("#iActive_"+number).prop("checked");  
                    } 
                    if(cb_flag)
                    {
                        var szTransportMode = '';
                        var idTransportMode = $("#idTransportMode_"+number).val();
                        var idBookingQuote = $("#idBookingQuote_"+number).val();
                        if(idTransportMode>0)
                        {
                            szTransportMode = transportModeAry[idTransportMode] ;
                        }

                        var iTransitHours = $("#iTransitHours_"+number).val();
                        var fTotalPriceCustomerCurrency = $("#fTotalPriceCustomerCurrency_"+number).val(); 
                        var fTotalVat = parseFloat($("#fTotalVat_"+number).val());
                        
                        var szVatIncludingStr = '<?php echo checkaddslashes($szVatExcludingText); ?> '; 
                        var szQuotePriceStr = "";
                        if(iPrivateCustomer==1)
                        {
                            szVatIncludingStr = '<?php echo checkaddslashes($szVatTextExcluingTextWhenOneQuote); ?> ';
                            var fTotalPriceIncludingVat = (parseFloat(fTotalPriceCustomerCurrency) + fTotalVat);
                            szQuotePriceStr =  szCustomerCurrency+" "+number_format(fTotalPriceIncludingVat,'0',decimal_seprator,thousand_seprator)+" all-in" ;
                        }
                        else
                        {
                            szQuotePriceStr =  szCustomerCurrency+" "+number_format(fTotalPriceCustomerCurrency,'0',decimal_seprator,thousand_seprator)+" all-in" ;
                        }
                        
                        var dtOfferValid = $("#dtQuoteValidTo_"+number).val();
                        var fTotalInsuranceCostForBookingCustomerCurrency = $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).val();
                        var fInsuranceValueUptoPrice = $("#fInsuranceValueUpto_"+number).val();
                        
                        fInsuranceValueUptoPrice = parseFloat(fInsuranceValueUptoPrice);
                        var iInsuranceComments = $("#iInsuranceComments_"+number).prop("checked"); 
                        var szForwarderComment = $("#szForwarderComment_"+number).val(); 
                        var szForwarderCommentStr = '';
                        if(iInsuranceComments)
                        {
                            szForwarderCommentStr = szForwarderComment + ' <br>\n' ;
                        }
                        if(fTotalVat>0)
                        {
                            szQuotePriceStr += ", "+ szVatIncludingStr + szCustomerCurrency+" "+number_format(fTotalVat,'0',decimal_seprator,thousand_seprator) ;
                        }
                        else
                        {
                            szQuotePriceStr += ". "+'<?php echo checkaddslashes($szNoVatOnBookingText); ?>';
                        }
                        
                        var szInsuranceText = '';
                        if(fTotalInsuranceCostForBookingCustomerCurrency>0)
                        {
                            if(fValueOfGoods>0)
                            {
                                szInsuranceText = '<?php echo checkaddslashes($szInsuranceText); ?>' + ': ' + " " + szCustomerCurrency+" "+number_format(fTotalInsuranceCostForBookingCustomerCurrency,'0',decimal_seprator,thousand_seprator) + '<?php echo checkaddslashes($szInsuranceValueUptoText); ?>'+ " " + szGoodsInsuranceCurrency + " " + number_format(fValueOfGoods,'0',decimal_seprator,thousand_seprator) + ' <br>\n' ;
                            }
                            else
                            {
                                szInsuranceText = ' <?php echo checkaddslashes($szInsuranceText); ?>' + ': ' + " " + szCustomerCurrency+" "+number_format(fTotalInsuranceCostForBookingCustomerCurrency,'0',decimal_seprator,thousand_seprator) + ' <br>\n' ;
                            } 
                        } 
                        if(active_counter==1)
                        {
                            if(parseInt(iQuickQuote)>1)
                            {
                                szEmailBodyText += '<?php echo $szFromText; ?>' + ': '+szOriginCountry + ' <br>\n'
                                     + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + '<br>\n'                                     
                                     + '<?php echo $szCargoText; ?>' + ': ' + szCargoText + '<br>\n'
                                     +'<?php echo $szModeText; ?>'+': ' + szTransportMode + ' <br>\n'
                                    + '<?php echo $szServiceText; ?>' + ': '+ iQuickQuoteDescriptionArr[idBookingQuote] +' <br>\n'
                                     + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + '<?php echo $szDaysText; ?>' + ' <br>\n'
                                     + '<?php echo $szPriceText; ?>'+': '+szQuotePriceStr+' <br>\n'
                                     + szInsuranceText + szForwarderCommentStr + ''
                                     + '<?php echo checkaddslashes($OfferValidUntillText); ?>' + ' ' + dtOfferValid+ ' <br><br>\n\nszBookingQuoteLink_'+num_counter+' \n\n';
                            }
                            else
                            {
                                szEmailBodyText += '<?php echo $szFromText; ?>' + ': '+szOriginCountry + ' <br>\n'
                                     + '<?php echo $szToText; ?>' + ': ' + szDestinationCountry + '<br>\n'
                                     + '<?php echo $szServiceText; ?>' + ': ' + '<?php echo checkaddslashes($szServiceDescriptionString); ?>' + '<br>\n'
                                     + '<?php echo $szCargoText; ?>' + ': ' + szCargoText + '<br>\n'
                                     +'<?php echo $szModeText; ?>'+': ' + szTransportMode + ' <br>\n'
                                     + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + '<?php echo $szDaysText; ?>' + ' <br>\n'
                                     + '<?php echo $szPriceText; ?>'+': '+szQuotePriceStr+' <br>\n'
                                     + szInsuranceText + szForwarderCommentStr + ''
                                     + '<?php echo checkaddslashes($OfferValidUntillText); ?>' + ' ' + dtOfferValid+ ' <br><br>\n\nszBookingQuoteLink_'+num_counter+' \n\n';
                            }
                        }
                        else
                        {
                            if(counter>1)
                            {
                                szEmailBodyText +="<br><br>";
                            }
                            
                            if(parseInt(iQuickQuote)>1)
                            {
                                szEmailBodyText += "<strong><u>" + counter + '. ' + '<?php echo $szOfferText; ?>' + '</u> </strong> <br>\n'
                                    + '<?php echo $szModeText; ?>' + ': ' + szTransportMode + ' <br>\n'
                                    + '<?php echo $szServiceText; ?>' + ': '+ iQuickQuoteDescriptionArr[idBookingQuote] +' <br>\n'
                                    + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + '<?php echo $szDaysText; ?>' + ' <br>\n'
                                    + '<?php echo $szPriceText; ?>' + ': ' + szQuotePriceStr + ' <br>\n'
                                    + szInsuranceText + szForwarderCommentStr + ''
                                    + '<?php echo checkaddslashes($OfferValidUntillText); ?>' + ' ' + dtOfferValid + ' <br><br>  \n\nszBookingQuoteLink_'+num_counter+' \n\n';
                            }
                            else
                            {
                                szEmailBodyText += "<strong><u>" + counter + '. ' + '<?php echo $szOfferText; ?>' + '</u> </strong> <br>\n'
                                    + '<?php echo $szModeText; ?>' + ': ' + szTransportMode + ' <br>\n'
                                    + '<?php echo $szTransitTimeText; ?>' + ': ' + iTransitHours + '<?php echo $szDaysText; ?>' + ' <br>\n'
                                    + '<?php echo $szPriceText; ?>' + ': ' + szQuotePriceStr + ' <br>\n'
                                    + szInsuranceText + szForwarderCommentStr + ''
                                    + '<?php echo checkaddslashes($OfferValidUntillText); ?>' + ' ' + dtOfferValid + ' <br><br>  \n\nszBookingQuoteLink_'+num_counter+' \n\n';
                            }
                        } 
                        
                        counter++;
                        num_counter++;
                    }  
                } 
                var iStandardPricing=$("#iStandardPricing").val();
                if(parseInt(iStandardPricing)==0)
                {
                    //szEmailBodyText += "<br><br>";
                    $("#szQuotationBody").val(szEmailBodyText);
                }
            }
            else
            {
                $("#szQuotationBody").val(' ');
                $("#szEmailSubject").attr('disabled','disabled');
                $("#szQuotationBody").attr('disabled','disabled');
                $("#szEmailBody").attr('disabled','disabled');
                
                $("#request_quote_send_button").attr("style","opacity:0.4");
                $("#request_quote_send_button").unbind("click");  
            } 
        }
        
        updateEmailBody(1); 
        var szChangedField = ''; 
         
        function calculate_gross_profit(number,change_field)
        {
            szChangedField = change_field ;
            var fPrice = parseFloat($("#fTotalPriceCustomerCurrencyHidden_"+number).val());
            var fReferalAmount = parseFloat($("#fReferalAmountHidden_"+number).val());
            var fReferalPercentage = parseFloat($("#fReferalPercentageHidden_"+number).val());
            var idForwarderCurrency = $("#idForwarderCurrency_"+number).val();
            var fForwarderExchangeRate = $("#fForwarderExchangeRate_"+number).val();
            var fReferalAmountHidden2 = parseFloat($("#fReferalAmountHidden2_"+number).val());
            var fReferalPercentageHidden2 = parseFloat($("#fReferalPercentageHidden2_"+number).val());

            var iProfitType = parseInt($("#iProfitType_"+number).val());
            var fCustomerExchangeRate = parseFloat($("#fCustomerExchangeRate_"+number).val());
            var fForwarderExchangeRate = parseFloat($("#fForwarderExchangeRate_"+number).val());
            var fQuotePriceCustomerCurrency = parseFloat($("#fQuotePriceCustomerCurrency_"+number).val());
            var fTotalVatCustomerCurrency = parseFloat($("#fTotalVatCustomerCurrency_"+number).val());
            var fPriceMarkUp = parseFloat($("#fPriceMarkUp_"+number).val());
            
            var fTotalPriceForwarderCurrency = parseFloat($("#fTotalPriceForwarderCurrency_"+number).val());
            var fTotalVatForwarder = parseFloat($("#fTotalVatForwarder_"+number).val());
            var fTotalVat = 0; 
            var fTotalPriceForwarderCurrency_usd = parseFloat($("#fTotalPriceForwarderCurrency_usd_"+number).val());
            var fTotalVat_usd = parseFloat($("#fTotalVat_usd_"+number).val()); 
            
            var idForwarderAccountCurrency = parseFloat($("#idForwarderAccountCurrency_"+number).val()); 
            var idCustomerCurrency = parseFloat($("#idCustomerCurrency").val());  
            var iAddingNewQuote = parseInt($("#iAddingNewQuote_"+number).val());
            
            var iManualFeeApplicable = parseInt($("#iManualFeeApplicable_"+number).val());
            var idHandlingCurrency = parseInt($("#idHandlingCurrency_"+number).val());
            var szHandlingCurrency = $("#szHandlingCurrency_"+number).val();
            var fHandlingCurrencyExchangeRate = parseFloat($("#fHandlingCurrencyExchangeRate_"+number).val());
            var fTotalHandlingFee = parseFloat($("#fTotalHandlingFee_"+number).val());
  
            var fCustomerExchangeRate = parseFloat($("#fCustomerExchangeRateGlobal").val()); 
            var fPriceMarkUp = parseFloat($("#fPriceMarkUpGlobal").val());  
            var szPriceLogText = '';
            var szPriceLogFormula = '<h3>Formula</h3><hr>';
            var fHandlingFeeCustomerCurrency = 0;
            var fHandlingFeeUsd = 0; 
            
            if(iProfitType==1 && fTotalHandlingFee>0)
            {
                /*
                 * If forwarder type is referal and Handling fee>0 then we apply iManualFeeApplicable
                 */
                iManualFeeApplicable = 1;
                $("#iManualFeeApplicable_"+number).val(1);
            }
            if(iManualFeeApplicable==1)
            {
                fHandlingFeeUsd = fTotalHandlingFee * fHandlingCurrencyExchangeRate;
                if(idHandlingCurrency==idCustomerCurrency)
                {
                    fHandlingFeeCustomerCurrency = fTotalHandlingFee;
                }
                else if(fCustomerExchangeRate>0)
                {
                    fHandlingFeeCustomerCurrency = Math.round(fHandlingFeeUsd/fCustomerExchangeRate);
                }  
                if(idForwarderAccountCurrency!=idCustomerCurrency)
                {
                    fHandlingFeeCustomerCurrencyMarkup =(fHandlingFeeCustomerCurrency * fPriceMarkUp * 0.01)
                    fHandlingFeeCustomerCurrencyMarkup = fHandlingFeeCustomerCurrencyMarkup.toFixed(2);
                    fHandlingFeeCustomerCurrency = (fHandlingFeeCustomerCurrency + parseFloat(fHandlingFeeCustomerCurrencyMarkup));
                }  
                
                var fHandlingFeeForwarderCurrency =0;
                if(idHandlingCurrency==idForwarderCurrency)
                {
                    fHandlingFeeForwarderCurrency = fTotalHandlingFee;
                }
                else if(fForwarderExchangeRate>0)
                {
                    fHandlingFeeForwarderCurrency = Math.round(fHandlingFeeUsd/fForwarderExchangeRate);
                }  
            } 
            var idBookingQuote = parseInt($("#idBookingQuote_"+number).val()); 
            if(isNaN(fPrice))
            {
                fPrice = 0;
            }
            if(isNaN(fReferalAmount))
            {
                fReferalAmount = 0;
            }
            if(isNaN(fReferalPercentage))
            {
                fReferalPercentage = 0;
            }
            if(isNaN(fTotalVatForwarder))
            {
                fTotalVatForwarder = 0;
            }
            if(isNaN(fTotalVat_usd))
            {
                fTotalVat_usd = 0;
            }
            if(isNaN(fTotalPriceForwarderCurrency_usd))
            {
                fTotalPriceForwarderCurrency_usd = 0;
            }
            if(isNaN(fTotalPriceForwarderCurrency))
            {
                fTotalPriceForwarderCurrency = 0;
            }
            
            var iTakeDataFromHidden = 0;
            //alert(change_field);
            if(change_field=='PRICE')
            {
                var fTotalPriceCustomerCurrency_1 = $("#fTotalPriceCustomerCurrency_"+number).val();
                var fTotalPriceCustomerCurrency_2 = $("#fTotalPriceCustomerCurrencyHidden2_"+number).val();
                 
                if(fTotalPriceCustomerCurrency_1!=fTotalPriceCustomerCurrency_2)
                {
                    fPrice = fTotalPriceCustomerCurrency_1 ;
                    iTakeDataFromHidden = 1;
                }
                else
                {
                    iTakeDataFromHidden = 2;
                }
            }
            else if(change_field=='PERCENTAGE')
            { 
                var fReferalPercentage_1 = $("#fReferalPercentage_"+number).val();
                var fReferalPercentage_2 = $("#fReferalPercentageHidden2_"+number).val();
                 
                if(fReferalPercentage_1!=fReferalPercentage_2)
                {
                    fReferalPercentage = fReferalPercentage_1;
                    iTakeDataFromHidden = 1;
                }
                else
                {
                    iTakeDataFromHidden = 2;
                }
            }
            else if(change_field=='REFERAL_AMOUNT')
            {
                var fReferalAmount_1 = $("#fReferalAmount_"+number).val(); 
                var fReferalAmount_2 = $("#fReferalAmountHidden2_"+number).val(); 
                 
                if(fReferalAmount_1!=fReferalAmount_2)
                {
                    fReferalAmount = fReferalAmount_1;
                    iTakeDataFromHidden = 1;
                }
                else
                {
                    iTakeDataFromHidden = 2;
                }
            } 
            szPriceLogText = '<p>Customer Currency Exchange Rate: '+fCustomerExchangeRate+"</p>" ;
            szPriceLogText += '<p>Forwarder Currency Exchange Rate: '+fForwarderExchangeRate+"</p>" ;
            szPriceLogText += '<p>Forwarder Account Currency: '+idForwarderAccountCurrency+"</p>" ;
            szPriceLogText += '<p>Currency Mark-up: '+fPriceMarkUp+"</p><br><br>" ;
            
            var fApplicableVatRate = $("#fApplicableVatRate_"+number).val();
            fApplicableVatRate = parseFloat(fApplicableVatRate);
            if(fApplicableVatRate>0)
            {
            	var szForwarderQuoteCurrencyName = $("#szForwarderQuoteCurrencyName_"+number).val(); 
                var fTotalVatForwarder = '0.00' ; 
                //fTotalVatForwarder = fTotalVatForwarder.toFixed(2);
                
                var szForwarderVatString = szForwarderQuoteCurrencyName+" "+number_format(fTotalVatForwarder,'2',".",",");
                
                if($("#forwarder_vat_field_container_"+number).length)
                { 
                    $("#forwarder_vat_field_container_"+number).html(szForwarderVatString);
                } 
                //$("#fTotalVatForwarder_"+number).val(fTotalVatForwarder); 
                //$("#szTotalVatForwarderPrice_"+number).val(szForwarderVatString);  
            }
            
            if(fForwarderExchangeRate>0)
            {  
                szPriceLogFormula += '<p>Calculating Prices in to USD </p>'; 
                fTotalPriceForwarderCurrency_usd = (fTotalPriceForwarderCurrency * fForwarderExchangeRate);
                fTotalVat_usd = (fTotalVatForwarder * fForwarderExchangeRate);  
                
                szPriceLogFormula += "<p>Quote Price (USD) = (Forwarder Currency Price * Forwarder Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p>Quote Price (USD) = (" + fTotalPriceForwarderCurrency + "*"+fForwarderExchangeRate + ") = " + fTotalPriceForwarderCurrency_usd + "</p>";
                
                szPriceLogFormula += "<p> VAT Price (USD) = (VAT Price * Forwarder Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p>VAT Price (USD) = (" + fTotalVatForwarder + "*" + fForwarderExchangeRate + ") = " + fTotalVat_usd+"</p>" ;
            }   
            szPriceLogText += '<p>Quote Price (USD): '+fTotalPriceForwarderCurrency_usd + "</p>" ;
            szPriceLogText += '<p>Handling Fee (USD): '+fHandlingFeeUsd + "</p>" ;
            szPriceLogText += '<p>Vat Price (USD): ' + fTotalVat_usd +  "</p><br><br>" ;
            
            if(fCustomerExchangeRate>0)
            {
                fTotalPriceCustomerCurrency = (fTotalPriceForwarderCurrency_usd / fCustomerExchangeRate);
                fTotalVat = (fTotalVat_usd / fCustomerExchangeRate) ;
                
                if(isNaN(fTotalPriceCustomerCurrency))
                {
                    fTotalPriceCustomerCurrency = 0;
                }
                if(isNaN(fTotalVat))
                {
                    fTotalVat = 0;
                }
                szPriceLogFormula += '<br> <p> Calculating Prices in to Customer currency </p>';  
                szPriceLogFormula += "<p> Quote Price Customer currency = (Quote Price (USD) / Customer Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p>Quote Price Customer currency = (" + fTotalPriceForwarderCurrency_usd + "/" + fCustomerExchangeRate + ") = " + fTotalPriceCustomerCurrency+"</p>" ;
                szPriceLogFormula += "<p> VAT Price Customer currency = (VAT Price (USD) / Customer Currency Exchange Rate ) </p>";
                szPriceLogFormula += "<p> VAT Price Customer currency = (" + fTotalVat_usd + "/" + fCustomerExchangeRate + ") = " + fTotalVat+"</p>" ;
            }   
            szPriceLogText += '<p>Quote Price Customer currency : '+fTotalPriceCustomerCurrency+"</p>" ;
            szPriceLogText += '<p>Vat Price Customer currency: '+fTotalVat+"</p><br><br>" ; 
            var fTotalPriceCustomerCurrency_display = fTotalPriceCustomerCurrency ;
            if(idForwarderAccountCurrency!=idCustomerCurrency)
            {  
                fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency * ( 1+ fPriceMarkUp/100) ;
                fTotalVat = fTotalVat * ( 1+ fPriceMarkUp/100) ;
                
                szPriceLogText += '<p>Currency Mark-up Applied: Yes</p>'  ; 
                szPriceLogText += '<p>Prices After currency Mark-up: </p>'  ; 
                szPriceLogText += '<p>Quote: '+fTotalPriceCustomerCurrency+'</p>'  ; 
                szPriceLogText += '<p>VAT: '+fTotalVat+'</p> <br><br>' ;  
            } 
            else
            {
                szPriceLogText += '<p>Currency Mark-up Applied: No</p><br><br>'  ; 
                fPriceMarkUp = 0;
            } 
            
            fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency + fHandlingFeeCustomerCurrency
            //alert("change_field"+change_field);
            var iCalculateVatFlag=0; 
            if(change_field=='PRICE')
            { 
                var fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency;
                var fTempPrice = fTotalPriceCustomerCurrency+fTotalVat; 
                 
                var fAdjustedPriceUSD = fPrice * fCustomerExchangeRate ;
                if(iProfitType==2)
                {
                    var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd ; 
                    fReferalAmount = fPrice - fTotalPriceCustomerCurrency ;   
                    var fReferalAmountUSD = fReferalAmount * fCustomerExchangeRate;  
                    
                    if(fTotalAmountUsd!=0)
                    { 
                        fReferalPercentage = ((fReferalAmountUSD/(1+fPriceMarkUp/100)) / fTotalAmountUsd) * 100 ; 
                    } 
                    
                    szPriceLogFormula += "<hr><p>Adjusted Price: "+fPrice+" Adjusted Price(USD): "+fAdjustedPriceUSD+" Profit Type: Mark-up </p>";
                    szPriceLogFormula += "<p> Gross Profit = (Adjusted Price - Quote Price Customer currency) </p>";
                    szPriceLogFormula += "<p> Gross Profit = ("+ fPrice + " - " +fTotalPriceCustomerCurrency+") = " + fReferalAmount + " </p>";
                    
                    szPriceLogFormula += "<p> Gross Profit (%) = (GrossProfitUSD *100) / (QuotePriceUSD + VATPriceUSD)</p>";
                    szPriceLogFormula += "<p> Gross Profit (%) = ("+fReferalAmountUSD+" * 100) / ("+fTotalPriceForwarderCurrency_usd+" + " + fTotalVat_usd +") = "+fReferalPercentage+" </p>";
                    
                    if(fReferalPercentage>0)
                    {
                        var fReferalAmount_vat = (fTotalVat * fReferalPercentage)/100; 
                    }
                    var fTotalVatRounded = Math.round(fTotalVat) ;
                    var fTotalPriceCustomerCurrency_rounded = Math.round(fTotalPriceCustomerCurrency_display) ;
                    var fTotalDiplayedPriceRounded = Math.round(fPrice) ;
                    
                    fTotalVat = (fTotalVatRounded / fTotalPriceCustomerCurrency_rounded ) * fTotalDiplayedPriceRounded ;
                    
                    if(isNaN(fTotalPriceCustomerCurrency))
                    {
                        fTotalPriceCustomerCurrency = 0;
                    }
                    if(isNaN(fTotalVat))
                    {
                        fTotalVat = 0;
                    }
                    
                    szPriceLogFormula += "<p>Calculating New VAT </p>";
                    szPriceLogFormula += "<p> NEW VAT Price = Vat Price Customer currency + ((Vat Price Customer currency * Gross Profit (%))/100)  = "+ fTotalVat +"</p>";
                    
                    szPriceLogText += '<p>GP Type: Referral </p>'  ; 
                    szPriceLogText += '<p>Referral Percentage: '+fReferalPercentage+' </p>'  ; 
                    szPriceLogText += '<p>Referral Amount: '+fReferalAmount+' </p>'  ; 
                    szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                }
                else
                { 
                    szPriceLogFormula += "<hr><p>Adjusted Price: "+fPrice+" Profit Type: Referral </p>";
                    szPriceLogFormula += "<p>GP(USD): Price * Customer Currency Exchange rate / (1+CurrencyMarkup(ifApplicable)/100) – QuotePriceUSD*(1-CustomerStandardReferralFee/100) +  VATPriceUSD*CustomerStandardReferralFee/100</p>";
                    var CustomerStandardReferralFee = $("#fReferalPercentageStandarad_"+number).val() ;
                     
                    var fReferalAmountUSD = (fPrice * fCustomerExchangeRate / (1+fPriceMarkUp/100)) - (fTotalPriceForwarderCurrency_usd * (1 - CustomerStandardReferralFee/100)) + (fTotalVat_usd*CustomerStandardReferralFee/100) ;
                    
                    szPriceLogFormula += "<br> <p> GP(USD): "+fPrice+" * "+fCustomerExchangeRate+" / (1+"+fPriceMarkUp+"/100) – "+fTotalPriceForwarderCurrency_usd+"*(1-14.5/100) +  "+fTotalVat_usd+"*"+CustomerStandardReferralFee+"/100</p>";
                    szPriceLogFormula += "<br> <p> GP(USD)= "+fReferalAmountUSD ; 
                    
                    if(fCustomerExchangeRate>0)
                    {
                        fReferalAmount = fReferalAmountUSD/fCustomerExchangeRate * (1+fPriceMarkUp/100) ;
                    }
                    
                    szPriceLogFormula += "<br><br> <p>GP(Customer Currency): GrossProfitUSD / CustomerCurrencyExchangeRate * (1+CurrencyMarkup(ifApplicable)/100) </p>"; 
                    szPriceLogFormula += "<p>GP(Customer Currency): "+fReferalAmountUSD+" / "+fCustomerExchangeRate+" * (1+"+fPriceMarkUp+"/100) </p>";
                    szPriceLogFormula += "<p>GP(Customer Currency) = " + fReferalAmount ;
                    
                    var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd ;
                    if(fTotalAmountUsd>0)
                    {
                        fReferalPercentage = (fReferalAmountUSD / fTotalAmountUsd) * 100 ;
                    }
                    szPriceLogFormula += "<p>GP(%) = " + fReferalPercentage ;
                    
                    if(isNaN(fTotalPriceCustomerCurrency))
                    {
                        fTotalPriceCustomerCurrency = 0;
                    }
                    if(isNaN(fTotalVat))
                    {
                        fTotalVat = 0;
                    } 
                    szPriceLogText += '<p>GP Type: Mark-up </p>'  ; 
                    szPriceLogText += '<p>Referral Percentage: '+fReferalPercentage+' </p>'  ; 
                    szPriceLogText += '<p>Referral Amount: '+fReferalAmount+' </p>'  ; 
                    szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                } 
                
                $("#fReferalAmountHidden_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden_"+number).val(fReferalPercentage);
                $("#fTotalVatHidden_"+number).val(fTotalVat);
                $("#fTotalPriceCustomerCurrencyHidden_"+number).val(fPrice);
                 
                var fTotalInvoiceAmount = fPrice + fTotalVat ;
                
                fTotalVat = fTotalVat.toFixed(2);  
                var fReferalAmount = Math.round(fReferalAmount);
                var fReferalPercentage = fReferalPercentage.toFixed(2);
                
                fReferalAmount = fReferalAmount + fHandlingFeeCustomerCurrency;
                if(isNaN(fReferalAmount))
                {
                    fReferalAmount = 0;
                }
                if(isNaN(fTotalVat))
                {
                    fTotalVat = 0;
                }
                if(isNaN(fReferalPercentage))
                {
                    fReferalPercentage = 0;
                }
                
                $("#fReferalAmountHidden2_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage); 
                $("#fReferalAmount_"+number).val(fReferalAmount);
                $("#fReferalPercentage_"+number).val(fReferalPercentage); 
                
                
                var fTotalPriceCustomerCurrencyHidden2=$("#fTotalPriceCustomerCurrencyHidden2_"+number).val();
                var fTotalPriceCustomerCurrencyVal=$("#fTotalPriceCustomerCurrency_"+number).val(); 
                if(fTotalPriceCustomerCurrencyHidden2!=fTotalPriceCustomerCurrencyVal)
                { 
                   iCalculateVatFlag=1;             
                    if($("#vat_text_container_"+number).length)
                    { 
                        $("#vat_text_container_"+number).html(fTotalVat);  
                    }
                    $("#fTotalVat_"+number).val(fTotalVat); 
                    $("#customer_vat_price_text_container_"+number).html(fTotalVat);
                }
                
                if(iTakeDataFromHidden===1)
                {
                    $("#fTotalPriceCustomerCurrencyHidden2_"+number).val(fPrice); 
                }
                $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage);  
                $("#fReferalAmountHidden2_"+number).val(fReferalAmount); 
               
               calculateActualGP(number,fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fTotalInvoiceAmount,fCustomerExchangeRate,iProfitType,fPriceMarkUp,iTakeDataFromHidden,iCalculateVatFlag);
            }
            else if(change_field=='PERCENTAGE')
            { 
                fReferalAmount = 0;  
                var fReferalAmount_usd = 0;
                var fReferalAmount_vat_usd = 0; 
                //alert("fTotalVat_usd"+fTotalVat_usd);
                //alert("fReferalPercentage"+fReferalPercentage);
                //alert("fTotalPriceForwarderCurrency_usd"+fTotalPriceForwarderCurrency_usd);
                if(fReferalPercentage>0)
                {
                    fReferalAmount_usd = (((fTotalPriceForwarderCurrency_usd + fTotalVat_usd) * fReferalPercentage)/100);
                    fReferalAmount_vat_usd = ((fTotalVat_usd * fReferalPercentage)/100);
                }  
                //alert("fReferalAmount_usd"+fReferalAmount_usd);
                if(fCustomerExchangeRate>0)
                {
                    fReferalAmount = fReferalAmount_usd / fCustomerExchangeRate;
                    fReferalAmount_vat = fReferalAmount_vat_usd / fCustomerExchangeRate;
                     
                }
                //alert("fReferalAmount"+fReferalAmount);
                szPriceLogFormula += "<hr><p>Gross profit (%): "+fReferalPercentage+"</p>";
                szPriceLogFormula += "<p>Gross profit (USD) = ((Quote Price USD + Vat Price USD) * Gross profit (%) ) / 100  = "+fReferalAmount_usd+"</p>";
                szPriceLogFormula += "<p>Gross profit  = Gross profit (USD) / USD Exchange rate * (1+CurrencyMarkup/100) = "+fReferalAmount+"</p>";
                szPriceLogFormula += "<hr><p>Gross profit on VAT (USD) = ((Vat Price Customer currency) * Gross profit (%) ) / 100  = "+fReferalAmount_vat_usd+"</p>";
                szPriceLogFormula += "<p>Gross profit on VAT = Gross profit on VAT (USD) / USD Exchange rate * (1+CurrencyMarkup/100) = "+fReferalAmount_vat+"</p>";
                
                fTotalVatCustomerCurrency = fTotalVat ;  
                if(iProfitType===2)
                {
                    
                    fTotalPriceCustomerCurrency = (fTotalPriceCustomerCurrency + fReferalAmount);
                    //fTotalVat = (fTotalVat + fReferalAmount_vat);
                    var fTotalVat_1 = fTotalVat;
                    
                    var fTotalVatRounded = Math.round(fTotalVat) ;
                    var fTotalPriceCustomerCurrency_rounded = Math.round(fTotalPriceCustomerCurrency_display) ;
                    var fTotalDiplayedPriceRounded = Math.round(fTotalPriceCustomerCurrency) ;
                    
                    fTotalVat = (fTotalVatRounded / fTotalPriceCustomerCurrency_rounded ) * fTotalDiplayedPriceRounded ;
                    //fTotalVat = (fTotalVat / fTotalPriceCustomerCurrency_display ) * fTotalPriceCustomerCurrency ;
                    
                    //fReferalAmount = fReferalAmount + fReferalAmount_vat ;
                    
                    if(isNaN(fTotalPriceCustomerCurrency))
                    {
                        fTotalPriceCustomerCurrency = 0;
                    }
                    if(isNaN(fTotalVat))
                    {
                        fTotalVat = 0;
                    }
                    
                    szPriceLogText += '<p>GP Type: Mark-up </p>'; 
                    szPriceLogFormula += "<p>Profit Type: Mark-up</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency = (Quote Price Customer currency + Gross profit)  = "+fTotalPriceCustomerCurrency+"</p>";
                    //szPriceLogFormula += "<p>VAT Price Customer currency = (VAT Price Customer currency + Gross profit on VAT)  = "+fTotalVat+"</p>"; 
                    szPriceLogFormula += "<p>VAT Price Customer currency = (Total VAT / Display Price) * Price Customer Currency  = ("+fTotalVat_1+"/"+fTotalPriceCustomerCurrency_display+") * "+fTotalPriceCustomerCurrency+" = "+fTotalVat+" </p>"; 
                }
                else
                {
                    fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency;
                    fTotalVat = fTotalVat;
                    
                    if(isNaN(fTotalPriceCustomerCurrency))
                    {
                        fTotalPriceCustomerCurrency = 0;
                    }
                    if(isNaN(fTotalVat))
                    {
                        fTotalVat = 0;
                    }
                    
                    szPriceLogText += '<p>GP Type: Referral </p>'; 
                    
                    szPriceLogFormula += "<p>Profit Type: Referral</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency  = "+fTotalPriceCustomerCurrency+"</p>";
                    szPriceLogFormula += "<p>VAT Price Customer currency = "+fTotalVat+"</p>"; 
                }  
                //alert(fTotalPriceCustomerCurrency);
                szPriceLogText += '<p>Referral Amount: '+fReferalAmount+' </p>'  ; 
                szPriceLogText += '<p>Price: '+fTotalPriceCustomerCurrency+' </p>'  ; 
                szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                     
                $("#fReferalAmountHidden_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden_"+number).val(fReferalPercentage);
                $("#fTotalVatHidden_"+number).val(fTotalVat);
                $("#fTotalPriceCustomerCurrencyHidden_"+number).val(fTotalPriceCustomerCurrency);
                 
                var fTotalInvoiceAmount = fTotalPriceCustomerCurrency + fTotalVat ;
                // alert("handling fee"+fHandlingFeeCustomerCurrency);
                fTotalVat = fTotalVat.toFixed(2);   
                fReferalAmount = Math.round(fReferalAmount); 
                fTotalPriceCustomerCurrency = Math.round(fTotalPriceCustomerCurrency);
                fReferalAmount = fReferalAmount + fHandlingFeeCustomerCurrency;
                //alert("fReferalAmount + handling fee"+fReferalAmount);
                if(isNaN(fReferalAmount))
                {
                    fReferalAmount = 0;
                }
                if(isNaN(fTotalPriceCustomerCurrency))
                {
                    fTotalPriceCustomerCurrency = 0;
                }
                if(isNaN(fTotalVat))
                {
                    fTotalVat = 0;
                } 
                $("#fTotalPriceCustomerCurrency_"+number).val(fTotalPriceCustomerCurrency);
                var fTotalPriceCustomerCurrencyHidden2=$("#fTotalPriceCustomerCurrencyHidden2_"+number).val();
                var fTotalPriceCustomerCurrencyVal=$("#fTotalPriceCustomerCurrency_"+number).val(); 
                if(fTotalPriceCustomerCurrencyHidden2!=fTotalPriceCustomerCurrencyVal)
                { 
                   iCalculateVatFlag=1;
                   if($("#vat_text_container_"+number).length)
                    { 
                        $("#vat_text_container_"+number).html(number_format(fTotalVat));  
                    }
                     $("#fTotalVat_"+number).val(fTotalVat);  
                     $("#customer_vat_price_text_container_"+number).html(fTotalVat);
                }
                
                  
                
                if(iTakeDataFromHidden===1)
                {
                    $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage); 
                }
                $("#fReferalAmountHidden2_"+number).val(fReferalAmount); 
                $("#fTotalPriceCustomerCurrencyHidden2_"+number).val(fTotalPriceCustomerCurrency);
                
                $("#fReferalAmount_"+number).val(fReferalAmount); 
                if($("#customer_price_text_container_"+number).length)
                { 
                    $("#customer_price_text_container_"+number).html(number_format(fTotalPriceCustomerCurrency));  
                } 
                
                calculateActualGP(number,fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fTotalInvoiceAmount,fCustomerExchangeRate,iProfitType,fPriceMarkUp,iTakeDataFromHidden,iCalculateVatFlag)
            }
            else if(change_field=='REFERAL_AMOUNT')
            {   
                szPriceLogFormula += "<hr><p>Gross profit: "+fReferalAmount+"</p>";
                var fTempPrice = fTotalPriceForwarderCurrency_usd+fTotalVat_usd; 
                fTempPrice = fTempPrice / fCustomerExchangeRate;
                
                var fReferalAmountUSD = fReferalAmount * fCustomerExchangeRate;
                var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd;
                   
                if(fTotalAmountUsd>0)
                {
                    fReferalPercentage = (fReferalAmountUSD / fTotalAmountUsd) * 100 ;
                }   
                
                szPriceLogFormula += "<p> Gross Profit (%) = (GrossProfitUSD *100) / (QuotePriceUSD + VATPriceUSD)</p>";
                szPriceLogFormula += "<p> Gross Profit (%) = ("+fReferalAmountUSD+" * 100) / ("+fTotalPriceForwarderCurrency_usd+" + " + fTotalVat_usd +") = "+fReferalPercentage+" </p>";
                 
                fReferalPercentage = parseFloat(fReferalPercentage.toFixed(2)); 
                
                if(fReferalPercentage>0)
                {
                    fReferalAmount = ((fTempPrice * fReferalPercentage)/100);
                    fReferalAmount_vat = ((fTotalVat * fReferalPercentage)/100);
                } 
                szPriceLogFormula += "<hr><p>Gross profit on VAT = ((Vat Price Customer currency * Gross profit (%) ) / 100  = "+fReferalAmount_vat+"</p>";
                fTotalVatCustomerCurrency = fTotalVat ;  
                if(iProfitType==2)
                {
                    fTotalPriceCustomerCurrency = (fTotalPriceCustomerCurrency + fReferalAmount);
                    //fTotalVat = (fTotalVat + fReferalAmount_vat);
                    
                    var fTotalVatRounded = Math.round(fTotalVat) ;
                    var fTotalPriceCustomerCurrency_rounded = Math.round(fTotalPriceCustomerCurrency_display) ;
                    var fTotalDiplayedPriceRounded = Math.round(fTotalPriceCustomerCurrency) ;
                    
                    fTotalVat = (fTotalVatRounded / fTotalPriceCustomerCurrency_rounded ) * fTotalDiplayedPriceRounded ;
                    
                    //fTotalVat = (fTotalVat / fTotalPriceCustomerCurrency_display ) * fTotalPriceCustomerCurrency ;
                   // fReferalAmount = fReferalAmount + fReferalAmount_vat ;
                    szPriceLogText += '<p>GP Type: Mark-up </p>'  ;
                    
                    if(isNaN(fTotalPriceCustomerCurrency))
                    {
                        fTotalPriceCustomerCurrency = 0;
                    }
                    if(isNaN(fTotalVat))
                    {
                        fTotalVat = 0;
                    }
                    szPriceLogFormula += "<p>Profit Type: Mark-up</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency = (Quote Price Customer currency + Gross profit)  = "+fTotalPriceCustomerCurrency+"</p>";
                    szPriceLogFormula += "<p>VAT Price Customer currency = (VAT Price Customer currency + Gross profit on VAT)  = "+fTotalVat+"</p>"; 
                }
                else
                {
                    fTotalPriceCustomerCurrency = fTotalPriceCustomerCurrency ;
                    fTotalVat = fTotalVat;
                    
                    if(isNaN(fTotalPriceCustomerCurrency))
                    {
                        fTotalPriceCustomerCurrency = 0;
                    }
                    if(isNaN(fTotalVat))
                    {
                        fTotalVat = 0;
                    }
                    szPriceLogFormula += "<p>Profit Type: Referral</p>";
                    szPriceLogFormula += "<p>Quote Price Customer currency  = "+fTotalPriceCustomerCurrency+"</p>";
                    szPriceLogFormula += "<p>VAT Price Customer currency = "+fTotalVat+"</p>"; 
                    
                    szPriceLogText += '<p>GP Type: Referral </p>'  ;
                } 
                
                szPriceLogText += '<p>Referral Percentage: '+fReferalPercentage+' </p>'  ; 
                szPriceLogText += '<p>Price: '+fTotalPriceCustomerCurrency+' </p>'  ; 
                szPriceLogText += '<p>VAT: '+fTotalVat+' </p>'  ; 
                
                $("#fReferalAmountHidden_"+number).val(fReferalAmount);
                $("#fReferalPercentageHidden_"+number).val(fReferalPercentage);
                $("#fTotalVatHidden_"+number).val(fTotalVat);
                $("#fTotalPriceCustomerCurrencyHidden_"+number).val(fTotalPriceCustomerCurrency);
                
                var fTotalInvoiceAmount = fTotalPriceCustomerCurrency + fTotalVat;
                var fReferalPercentage = fReferalPercentage.toFixed(2);
                fTotalVat = fTotalVat.toFixed(2);  
                fTotalPriceCustomerCurrency = Math.round(fTotalPriceCustomerCurrency);
                if(isNaN(fReferalPercentage))
                {
                    fReferalPercentage = 0;
                }
                if(isNaN(fTotalPriceCustomerCurrency))
                {
                    fTotalPriceCustomerCurrency = 0;
                }
                if(isNaN(fTotalVat))
                {
                    fTotalVat = 0;
                } 
                $("#fReferalPercentage_"+number).val(fReferalPercentage);
                $("#fTotalPriceCustomerCurrency_"+number).val(fTotalPriceCustomerCurrency);
                if($("#customer_price_text_container_"+number).length)
                { 
                    $("#customer_price_text_container_"+number).html(number_format(fTotalPriceCustomerCurrency));  
                } 
                $("#fTotalPriceCustomerCurrency_"+number).val(fTotalPriceCustomerCurrency);
                var fTotalPriceCustomerCurrencyHidden2=$("#fTotalPriceCustomerCurrencyHidden2_"+number).val();
                var fTotalPriceCustomerCurrencyVal=$("#fTotalPriceCustomerCurrency_"+number).val();
              
                if(fTotalPriceCustomerCurrencyHidden2!=fTotalPriceCustomerCurrencyVal)
                { 
                   iCalculateVatFlag=1;
                    if($("#vat_text_container_"+number).length)
                    { 
                        $("#vat_text_container_"+number).html(number_format(fTotalVat));  
                    }
                    $("#fTotalVat_"+number).val(fTotalVat);  
                    $("#customer_vat_price_text_container_"+number).html(fTotalVat);
                } 
                if(iTakeDataFromHidden===1)
                {
                    $("#fReferalAmountHidden2_"+number).val(fReferalAmount);
                } 
                $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage); 
                $("#fTotalPriceCustomerCurrencyHidden2_"+number).val(fTotalPriceCustomerCurrency);
                
                calculateActualGP(number,fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fTotalInvoiceAmount,fCustomerExchangeRate,iProfitType,fPriceMarkUp,iTakeDataFromHidden,iCalculateVatFlag);
            }
            //console.log(szPriceLogFormula);
            if(number==1001)
            {
                szPriceLogText = szPriceLogText + "<br /><br />"+ szPriceLogFormula ;
                $("#price_details_text").html(szPriceLogText);
            }
            calculate_quote_insurance_price(number);
        }
        
        function calculateActualGP(number,fTotalPriceForwarderCurrency_usd,fTotalVat_usd,fPrice,fCustomerExchangeRate,iProfitType,fPriceMarkUp,iTakeDataFromHidden,iCalculateVatFlag)
        {   
            var fAdjustedPriceUSD = fPrice * fCustomerExchangeRate ; 
            if(iProfitType==2)
            {
                var fTotalAmountUsd = fTotalPriceForwarderCurrency_usd + fTotalVat_usd ;  
                var fReferalAmountUSD = fAdjustedPriceUSD - fTotalAmountUsd;

                var fReferalAmount = 0;
                if(fCustomerExchangeRate>0)
                {
                    fReferalAmount = (fReferalAmountUSD / fCustomerExchangeRate);
                }
                var fReferalPercentage = "";
                if(fTotalAmountUsd!=0)
                { 
                    fReferalPercentage = ((fReferalAmountUSD/(1+fPriceMarkUp/100)) / fAdjustedPriceUSD) * 100 ;
                }    
                
                //$("#fReferalAmountHidden_"+number).val(fReferalAmount);
               // $("#fReferalPercentageHidden_"+number).val(fReferalPercentage); 
                if(fReferalPercentage!="")
                {
                    fReferalPercentage = fReferalPercentage.toFixed('2');  
                    fReferalAmount = Math.round(fReferalAmount); 
                } 
                if(isNaN(fReferalAmount))
                {
                    fReferalAmount = 0;
                } 
                
                //$("#fReferalPercentage_"+number).val(fReferalPercentage);
               // $("#fReferalAmount_"+number).val(fReferalAmount);
                
                if(szChangedField=='PERCENTAGE')
                {
                   // $("#fReferalAmountHidden2_"+number).val(fReferalAmount);   
                }
                else if(szChangedField=='REFERAL_AMOUNT')
                {
                   // $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage);
                } 
                else
                {
                   // $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage);  
                   // $("#fReferalAmountHidden2_"+number).val(fReferalAmount);
                }
                
                if(iTakeDataFromHidden===1)
                {
                   // console.log("changed field: "+szChangedField);
                    
                    if(szChangedField=='REFERAL_AMOUNT')
                    {
                     //   $("#fReferalAmountHidden2_"+number).val(fReferalAmount);
                    } 
                    else if(szChangedField=='PERCENTAGE')
                    {
                       // $("#fReferalPercentageHidden2_"+number).val(fReferalPercentage);
                    }
                }  
                //$("#actual_gp_percentage_span_"+number).html(fReferalPercentage); 
                //$("#actual_gp_value_span_"+number).html(fReferalAmount);  
            }
            else
            {
                //$("#actual_gp_percentage_span_"+number).html(" "); 
                //$("#actual_gp_value_span_"+number).html(" ");  
            }  
            var iCalculateVatFlag = 1;
            if(parseInt(iCalculateVatFlag)==1)
            {
                calculateVatAmount(number);
            }
        }
         
        function manageHandlingFee(number)
        { 
            
            var iProfitType=$("#iProfitType_"+number).attr('value');
            
            if(iProfitType==1 || iProfitType==''){
                
                $("#idHandling_div_"+number).attr('style','width:100%;padding:5px;display:block;');
                //console.log("Manual Func called");
                var idTransportMode= $("#idTransportMode_"+number).val();
                var idForwarder= $("#idForwarder_"+number).val();  
                var idManualCurrency = '';
                if(parseInt(idForwarder)>0 && parseInt(idTransportMode)>0)
                {            
                    var manualFee =__JS_ONLY_MANUAL_FEE_LIST__;
                    var keyValue=idForwarder+"_"+idTransportMode;
                    if(manualFee[keyValue]!=undefined)
                    {
                        var szManualFeeValue = manualFee[keyValue]['szManualFee']; 
                        if(szManualFeeValue!='' && szManualFeeValue!=undefined)
                        {
                            var szManualFeeValueArr=szManualFeeValue.split("_");
                            idManualCurrency = szManualFeeValueArr[0];
                            $("#idHandlingCurrency_"+number).attr('value',szManualFeeValueArr[0]);
                            var manualFeeValue=parseFloat(szManualFeeValueArr[1]).toFixed(2);
                            $("#fTotalHandlingFee_"+number).attr('value',manualFeeValue); 
                        }
                        if(idManualCurrency>0)
                        {
                            //console.log("hello1");
                            autofill_currency_exhchange_rate(idManualCurrency,number,1);
                            $("#iManualFeeApplicable_"+number).attr('value','1');
                            $("#iHandlingFeeApplicable_"+number).attr('value','1');
                        }
                    }
                    else
                    {
                        var keyValue="0_"+idTransportMode;
                        if(manualFee[keyValue]!=undefined)
                        {
                            var szManualFeeValue = manualFee[keyValue]['szManualFee'];
                            if(szManualFeeValue!='' && szManualFeeValue!=undefined)
                            {
                                var szManualFeeValueArr=szManualFeeValue.split("_");
                                idManualCurrency = szManualFeeValueArr[0];
                                var manualFeeValue=parseFloat(szManualFeeValueArr[1]).toFixed(2);
                                $("#idHandlingCurrency_"+number).attr('value',szManualFeeValueArr[0]);
                                $("#fTotalHandlingFee_"+number).attr('value',manualFeeValue); 
                            }
                        }
                        else
                        {
                            var szManualFeeValue=__JS_ONLY_DEFAULT_MANUAL_FEE_LIST__;
                           // console.log(szManualFeeValue[0]);
                            var szManualFeeValueArr=szManualFeeValue[0].split("_");
                            idManualCurrency = szManualFeeValueArr[0];
                            var manualFeeValue=parseFloat(szManualFeeValueArr[1]).toFixed(2);
                            $("#idHandlingCurrency_"+number).attr('value',szManualFeeValueArr[0]);
                            $("#fTotalHandlingFee_"+number).attr('value',manualFeeValue); 
                        }
                        idManualCurrency = parseInt(idManualCurrency);
                       // console.log("Manual Curr: "+idManualCurrency);
                        if(idManualCurrency>0)
                        {
                            //console.log("hello");
                            autofill_currency_exhchange_rate(idManualCurrency,number,1);
                            $("#iManualFeeApplicable_"+number).attr('value','1');
                            $("#iHandlingFeeApplicable_"+number).attr('value','1');
                        }
                    } 
                    update_quote_price(number);
                    enableQuoteSendButton();
                }
                
            }
            else
            {
                $("#iManualFeeApplicable_"+number).attr('value','0');
                $("#iHandlingFeeApplicable_"+number).attr('value','0');
                $("#idHandling_div_"+number).attr('style','width:100%;padding:5px;display:none;');
                $("#fTotalHandlingFee_"+number).attr('value','0');
                $("#idHandlingCurrency_"+number).attr('value','');
                $("#fHandlingCurrencyExchangeRate_"+number).attr('value','');                
               // console.log("Manual Fee  Not appicable for Markup Forwarders");
                
                update_quote_price(number);
                enableQuoteSendButton();
            }
        }    
        function update_quote_price(number)
        { 
            var fPriceForwarderCurrency = $("#fTotalPriceForwarderCurrency_"+number).val();
            var fTotalVatForwarder = $("#fTotalVatForwarder_"+number).val();
            var idForwarderCurrency = $("#idForwarderCurrency_"+number).val();
            var fForwarderExchangeRate = $("#fForwarderExchangeRate_"+number).val();
            var fCustomerExchangeRate = $("#fCustomerExchangeRate_"+number).val();
            var idForwarderAccountCurrency = parseFloat($("#idForwarderAccountCurrency_"+number).val()); 
            var idCustomerCurrency = parseFloat($("#idCustomerCurrency").val()); 
            var fPriceMarkUp = parseFloat($("#fPriceMarkUpGlobal").val());  
            var iAddingNewQuote = parseInt($("#iAddingNewQuote_"+number).val());  
            var iProfitType = parseInt($("#iProfitType_"+number).val());
            
            var iManualFeeApplicable = parseInt($("#iManualFeeApplicable_"+number).val());
            var idHandlingCurrency = parseInt($("#idHandlingCurrency_"+number).val());
            var szHandlingCurrency = $("#szHandlingCurrency_"+number).val();
            var fHandlingCurrencyExchangeRate = parseFloat($("#fHandlingCurrencyExchangeRate_"+number).val());
            var fTotalHandlingFee = parseFloat($("#fTotalHandlingFee_"+number).val());
            
            var fCustomerExchangeRate = parseFloat($("#fCustomerExchangeRateGlobal").val()); 
            fTotalVatForwarder = parseFloat(fTotalVatForwarder);
            
            var fHandlingFeeCustomerCurrency = 0;
            var fHandlingFeeUsd = 0; 
            //console.log("Forwarder Price changed...");
            //forwarder_vat_field_container_
            /*
             * From now we have applied Handling fee for all RFQs
             */
            //iManualFeeApplicable = 1;
            if(iManualFeeApplicable==1)
            {
                fHandlingFeeUsd = fTotalHandlingFee * fHandlingCurrencyExchangeRate;
                if(idHandlingCurrency==idCustomerCurrency)
                {
                    fHandlingFeeCustomerCurrency = fTotalHandlingFee;
                }
                else if(fCustomerExchangeRate>0)
                {
                    fHandlingFeeCustomerCurrency = Math.round(fHandlingFeeUsd/fCustomerExchangeRate);
                }
            }
            fHandlingFeeCustomerCurrency = parseFloat(fHandlingFeeCustomerCurrency); 
            if(idForwarderCurrency==idCustomerCurrency)
            {
                var fQuotePriceCustomerCurrency = Math.round(fPriceForwarderCurrency);  
                var fVATCustomerCurrency = Math.round(fTotalVatForwarder);  
            }
            else
            { 
                if(fForwarderExchangeRate>0)
                {
                    //converting forwarder currency price to USD
                    var fQuotePriceCustomerCurrency = fPriceForwarderCurrency * fForwarderExchangeRate; 
                    var fVATCustomerCurrency = fTotalVatForwarder * fForwarderExchangeRate; 
                }
                else
                {
                    var fQuotePriceCustomerCurrency = 0;
                    var fVATCustomerCurrency = 0;
                } 
                if(fCustomerExchangeRate>0)
                {
                    //converting USD price to customer currency
                    var fQuotePriceCustomerCurrency = fQuotePriceCustomerCurrency/fCustomerExchangeRate; 
                    var fVATCustomerCurrency = fTotalVatForwarder/fCustomerExchangeRate; 
                }
                else
                {
                    var fQuotePriceCustomerCurrency = 0;
                } 
                var fQuotePriceCustomerCurrency = fQuotePriceCustomerCurrency
            } 
            
            if(idCustomerCurrency!=idForwarderAccountCurrency)
            {
                var fQuotePriceCustomerCurrency = fQuotePriceCustomerCurrency * (1+fPriceMarkUp/100);
                fQuotePriceCustomerCurrency = Math.round(fQuotePriceCustomerCurrency); 
            }
            else
            {
                fQuotePriceCustomerCurrency = Math.round(fQuotePriceCustomerCurrency); 
            } 
            //fQuotePriceCustomerCurrency = fQuotePriceCustomerCurrency - fHandlingFeeCustomerCurrency 
            $("#fQuotePriceCustomerCurrency_"+number).val(fQuotePriceCustomerCurrency); 
            $("#quote_price_text_container_"+number).html(fQuotePriceCustomerCurrency);
            calculate_gross_profit(number,'PERCENTAGE'); 
            updateEmailBody(number);
        } 
var insuranceCalculationAry = new Array();
insuranceCalculationAry['fLargetSellPrice'] = '<?php echo $fLargetSellPrice; ?>';
insuranceCalculationAry['fTotalBookingPriceUSD'] = '<?php echo $fTotalBookingPriceUsd; ?>';
insuranceCalculationAry['idBookingCurrency'] = '<?php echo $idCustomerCurrency; ?>'; 
insuranceCalculationAry['fBookingExchangeRate'] = '<?php echo $fCustomerExchangeRate; ?>'; 
insuranceCalculationAry['idGoodsInsuranceCurrency'] = '<?php echo $idGoodsInsuranceCurrency; ?>';
insuranceCalculationAry['szGoodsInsuranceCurrency'] = '<?php echo $szGoodsInsuranceCurrency; ?>';
insuranceCalculationAry['fGoodsInsuranceExchangeRate'] = '<?php echo $fGoodsInsuranceExchangeRate; ?>';
var fGoodsInsurancePriceUSD = '<?php echo (int)$fValueOfGoodsUSD ; ?>'; 
var iInsuranceChoice = '<?php echo (int)$iInsuranceChoice ; ?>'; 
var fTotalBookingPriceCustomerCurrency = '<?php echo $fTotalBookingPriceCustomerCurrency ?>'; 
var fInsuranceSellCurrencyExchangeRate = '<?php echo $fInsuranceSellCurrencyExchangeRate ?>';  

var idAllTransportMode = '<?php echo __ALL_WORLD_TRANSPORT_MODE_ID__ ?>';
var idRestTransportMode = '<?php echo __REST_TRANSPORT_MODE_ID__ ?>';
var iPrivateShipping = '<?php echo (int)$iPrivateShipping; ?>';
var fImaginaryProfitPercentage = '<?php echo __INSURANCE_IMAGINARY_PROFIT__; ?>'; 
var insuranceBuySellRateAry  = new Array();   

<?php  
    if(!empty($transportModeListAry))
    {
        foreach($transportModeListAry as $transportModeListArys)
        {
            ?>
            insuranceBuySellRateAry[<?php echo $transportModeListArys['id'] ?>] = new Array();
            <?php
        } 
        
        if(!empty($buyRateListAry))
        {
            foreach($buyRateListAry as $key=>$buyRateListArys)
            { 
                $insuranceSellRateAry = array(); 
                $insuranceSellRateAry = $buyRateListArys['sellRateAry'];  

                if(!empty($insuranceSellRateAry))
                {
                    $ctr=0;
                    foreach($insuranceSellRateAry as $insuranceDetailsArys)
                    {  
                        ?> 
                        var insuranceRateAry = [];
                        insuranceBuySellRateAry['<?php echo $key; ?>']['<?php echo $ctr ?>'] = new Array(); 
                        insuranceRateAry['idInsuranceBuyRate'] = '<?php echo $buyRateListArys['id']; ?>';
                        insuranceRateAry['idInsuranceSellRate'] = '<?php echo $insuranceDetailsArys['id']; ?>';
                        insuranceRateAry['idInsuranceCurrency'] = '<?php echo $insuranceDetailsArys['idInsuranceCurrency']; ?>';
                        insuranceRateAry['idInsuranceMinCurrency'] = '<?php echo $insuranceDetailsArys['idInsuranceMinCurrency']; ?>';
                        insuranceRateAry['fInsuranceUptoPrice'] = '<?php echo $insuranceDetailsArys['fInsuranceUptoPrice']; ?>';
                        insuranceRateAry['szInsuranceCurrency'] = '<?php echo $insuranceDetailsArys['szInsuranceCurrency']; ?>';
                        insuranceRateAry['fInsuranceExchangeRate'] = '<?php echo $insuranceDetailsArys['fLatestInsuranceExchangeRate']; ?>';
                        insuranceRateAry['fInsuranceRate'] = '<?php echo $insuranceDetailsArys['fInsuranceRate']; ?>';
                        insuranceRateAry['fInsuranceMinPrice'] = '<?php echo $insuranceDetailsArys['fInsuranceMinPrice']; ?>';
                        insuranceRateAry['szInsuranceMinCurrency'] = '<?php echo $insuranceDetailsArys['szInsuranceMinCurrency']; ?>';
                        insuranceRateAry['fInsuranceMinExchangeRate'] = '<?php echo $insuranceDetailsArys['fLatestInsuranceExchangeRate']; ?>'; 
                        insuranceRateAry['fInsurancePriceUSD'] = '<?php echo $insuranceDetailsArys['fInsurancePriceUSD']; ?>';
                        insuranceRateAry['fInsuranceMinPriceUSD'] = '<?php echo $insuranceDetailsArys['fInsuranceMinPriceUSD']; ?>';  
                        insuranceBuySellRateAry['<?php echo $key; ?>']['<?php echo $ctr ?>'].push(insuranceRateAry);    
                        <?php 
                        $ctr++;
                    }
                }
            }
        }
    }
?>  
    function calculate_quote_insurance_price(number)
    {        
        if(insuranceBuySellRateAry.length>0)
        {
            var idTransportMode = parseInt($("#idTransportMode_"+number).val());
            var insuranceSellRateAry = new Array();
            var arrayKeys = Object.keys(insuranceBuySellRateAry); 
             
            if(jQuery.inArray(idTransportMode,arrayKeys))
            {
                
                if(idTransportMode>0)
                { 
                    if(insuranceBuySellRateAry[idTransportMode].length>0)
                    {
                        //All ok no need to do any thing
                        insuranceSellRateAry = insuranceBuySellRateAry[idTransportMode] ;
                    }
                    else if(insuranceBuySellRateAry[idAllTransportMode].length>0)
                    {
                        insuranceSellRateAry = insuranceBuySellRateAry[idAllTransportMode] ;
                    } 
                }
            }    
            var iArrayLength = insuranceSellRateAry.length ; 
            var iNoMatchFound = 0;
            if(iArrayLength>0)
            {
                //From now we are picking customer currency price instead of forwarder currency price. 
                var fTotalPriceForwarderCurrency = $("#fTotalPriceCustomerCurrency_"+number).val();
                var fTotalVat = $("#fTotalVat_"+number).val();
                var fForwarderExchangeRate = $("#fCustomerExchangeRate_"+number).val();
                
                fTotalPriceForwarderCurrency = parseInt(fTotalPriceForwarderCurrency);
                var fTotalVatCustomerCurrency = parseFloat(fTotalVat);
                fForwarderExchangeRate = parseFloat(fForwarderExchangeRate);
                
                if(fForwarderExchangeRate>0)
                {
                    fForwarderExchangeRate = fForwarderExchangeRate ;
                }

                if(isNaN(fTotalPriceForwarderCurrency) || isNaN(fTotalVatCustomerCurrency))
                {
                    console.log("unrecognized character"); 
                }
                else
                { 
                    var fTotalBookingPriceUSD = 0;
                    if(fForwarderExchangeRate>0)
                    {
                        if(iPrivateShipping==1)
                        {
                            /*
                            * For private customer we calculate insurance price on total invoice value i.e (Price + Vat)
                            */
                            fTotalBookingPriceUSD = (fTotalPriceForwarderCurrency + fTotalVatCustomerCurrency) * fForwarderExchangeRate;  
                        }
                        else
                        {
                            fTotalBookingPriceUSD = fTotalPriceForwarderCurrency * fForwarderExchangeRate;  
                        }
                    } 
                    var szInsuranceLogText = "\n Booking Price(USD): "+fTotalBookingPriceUSD;
                    szInsuranceLogText += "\n Goods Price(USD): "+fGoodsInsurancePriceUSD;
                    //console.log("Price: "+fTotalPriceForwarderCurrency+" Vat: "+fTotalVatCustomerCurrency + " Goods "+fGoodsInsurancePriceUSD);
                    var fTotalBookingAmountToBeInsurancedUsd = parseInt(fTotalBookingPriceUSD) + parseInt(fGoodsInsurancePriceUSD);  

                    if(iPrivateShipping!=1 && fImaginaryProfitPercentage>0)
                    {
                        /* 
                        * For non-private customers we adds 10% imaginary profit on total value
                        */
                       var fImaginaryProfitAmount = (fTotalBookingAmountToBeInsurancedUsd * fImaginaryProfitPercentage *0.01);
                       szInsuranceLogText += "\n Imaginary Proit(USD): "+fImaginaryProfitAmount;
                       fTotalBookingAmountToBeInsurancedUsd = fTotalBookingAmountToBeInsurancedUsd + fImaginaryProfitAmount;
                    }
                    szInsuranceLogText += "\n Total Amount to be insured(USD): "+fTotalBookingAmountToBeInsurancedUsd
                    //console.log(szInsuranceLogText); 
                    
                    var insuranceDetailsAry = new Array();
                    insuranceDetailsAry = getNearestRecord(fTotalBookingAmountToBeInsurancedUsd,number); 
                    var iArrayLength = insuranceDetailsAry.length ;
 
                    if(insuranceDetailsAry['fInsuranceUptoPrice'] != null) 
                    {
                        var idCalculationInsuranceCurrency = insuranceDetailsAry['idInsuranceCurrency'];
                        var idInsuranceSellCurrency = $("#idInsuranceSellCurrency_"+number).val();
                        var fInsuranceSellCurrencyExchangeRate = $("#fInsuranceSellCurrencyExchangeRate_"+number).val();
                         
                        //console.log("Sell curr: "+idInsuranceSellCurrency+" XE: "+fInsuranceSellCurrencyExchangeRate);
                        var fInsuranceExchangeRate = insuranceDetailsAry['fInsuranceExchangeRate'];
                        var idInsuranceBuyRate = insuranceDetailsAry['idInsuranceBuyRate'];
                        var idInsuranceSellRate = insuranceDetailsAry['idInsuranceSellRate'];
                        
                        if(idInsuranceSellCurrency>0 && idInsuranceSellCurrency==insuranceDetailsAry['idInsuranceCurrency'] && fInsuranceSellCurrencyExchangeRate>0)
                        {
                            var fInsuranceExchangeRate = fInsuranceSellCurrencyExchangeRate ;
                        } 
                        var fTotalInsuranceCostUsd = (fTotalBookingAmountToBeInsurancedUsd * parseFloat(insuranceDetailsAry['fInsuranceRate']))/100;  
                        insuranceDetailsAry['fInsuranceExchangeRate'] = fInsuranceExchangeRate;
                          
                        var fTotalInsuranceMinCost = insuranceDetailsAry['fInsuranceMinPrice']; 
                        var fTotalInsuranceMinCostUsd = insuranceDetailsAry['fInsuranceMinPrice'] * insuranceDetailsAry['fInsuranceMinExchangeRate'] ;
                        var fInsuranceUptoPrice = insuranceDetailsAry['fInsuranceUptoPrice']
                        
                        var fTotalInsuranceCost = 0;
                        if(insuranceDetailsAry['fInsuranceExchangeRate']>0)
                        {
                            fTotalInsuranceCost  = Math.ceil(parseFloat(fTotalInsuranceCostUsd / parseFloat(insuranceDetailsAry['fInsuranceExchangeRate'])));
                        }  
                        if(fTotalInsuranceMinCostUsd>fTotalInsuranceCostUsd)
                        {
                            fTotalInsuranceCost = fTotalInsuranceMinCost ;
                            fTotalInsuranceCostUsd = fTotalInsuranceMinCostUsd ;
                            
                            idInsuranceSellCurrency = insuranceDetailsAry['fInsuranceMinPrice'];
                            fInsuranceSellCurrencyExchangeRate = fInsuranceExchangeRate ;
                        }
                        else
                        {
                            var idInsuranceSellCurrency = insuranceDetailsAry['idInsuranceCurrency'];
                            var fInsuranceSellCurrencyExchangeRate = insuranceDetailsAry['fInsuranceExchangeRate']; 
                        }  
                        var fTotalInsuranceCostForBookingCustomerCurrency = 0;
                        if(insuranceCalculationAry['fBookingExchangeRate']>0)
                        {
                            if((iInsuranceChoice==3 || iInsuranceChoice==1) && fGoodsInsurancePriceUSD==0)
                            {
                                /*
                                * When Insurance: optional or Yes and cargo value: 0, the Insurance (DKK) should default be 0
                                */
                                var fTotalInsuranceCostForBookingCustomerCurrency = 0;
                            }
                            else if(iInsuranceChoice!=2)
                            {   
                                if(insuranceCalculationAry['idBookingCurrency']==idCalculationInsuranceCurrency)
                                {
                                    fTotalInsuranceCostForBookingCustomerCurrency = fTotalInsuranceCost;
                                }
                                else if(insuranceCalculationAry['fBookingExchangeRate']>0)
                                {
                                    fTotalInsuranceCostForBookingCustomerCurrency  = Math.ceil(parseFloat(fTotalInsuranceCostUsd / parseFloat(insuranceCalculationAry['fBookingExchangeRate'])));
                                }
                                else 
                                {
                                    fTotalInsuranceCostForBookingCustomerCurrency = 0;
                                }
                            }  
                            $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).removeAttr('disabled');
                            $("#iInsuranceAvailabilityFlag_"+number).val(1);
                        }
                        else
                        {
                            var fTotalInsuranceCostForBookingCustomerCurrency = "No available" ; 
                            iNoMatchFound = 1;
                        }   
                        //console.log("Price customer currency: "+fTotalInsuranceCostForBookingCustomerCurrency);
                        
                        $("#insurance_value_container_span_"+number).html(fTotalInsuranceCostForBookingCustomerCurrency);
                        $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).val(fTotalInsuranceCostForBookingCustomerCurrency);
                        $("#fInsuranceValueUpto_"+number).val(fInsuranceUptoPrice);
                        
                        $("#idInsuranceBuyRate_"+number).val(idInsuranceBuyRate);
                        $("#idInsuranceSellRate_"+number).val(idInsuranceSellRate); 
                    }
                    else
                    { 
                        $("#fInsuranceValueUpto_"+number).val(fInsuranceUptoPrice);
                        iNoMatchFound = 1;
                    }
                }
            } 
            else
            {
                iNoMatchFound = 1;
            }
        }
        else
        {
            iNoMatchFound = 1;
        }
        
        if(iNoMatchFound==1)
        {
            
            var fTotalInsuranceCostForBookingCustomerCurrency = "No available" ;
            $("#insurance_value_container_span_"+number).html(fTotalInsuranceCostForBookingCustomerCurrency);
            $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).attr('disabled','disabled'); 
            $("#fTotalInsuranceCostForBookingCustomerCurrency_"+number).val(fTotalInsuranceCostForBookingCustomerCurrency);
            $("#iInsuranceAvailabilityFlag_"+number).val(2);
        }
    }

    function getNearestRecord(fMaxBookingAmountToBeInsuranced,number)
    {
        if(insuranceBuySellRateAry.length)
        {
            var idTransportMode = parseInt($("#idTransportMode_"+number).val());
            var insuranceSellRateAry = new Array();

            if(idTransportMode>0 && insuranceBuySellRateAry.length)
            { 
                if(insuranceBuySellRateAry[idTransportMode].length>0)
                {
                    //All ok no need to do any thing
                    insuranceSellRateAry = insuranceBuySellRateAry[idTransportMode] ;
                }
                else if(insuranceBuySellRateAry[idAllTransportMode].length>0)
                {
                    insuranceSellRateAry = insuranceBuySellRateAry[idAllTransportMode] ;
                }
                else if(insuranceBuySellRateAry[idRestTransportMode].length>0)
                {
                    insuranceSellRateAry = insuranceBuySellRateAry[idRestTransportMode] ;
                }
            } 
            var length = insuranceSellRateAry.length ; 
            var ret_ary = new Array();
            for (var i = 0; i < length; i++) 
            {   
                if(fMaxBookingAmountToBeInsuranced <= insuranceSellRateAry[i][0]['fInsurancePriceUSD'])
                {      
                    //console.log('fMaxBookingAmountToBeInsuranced: ' + fMaxBookingAmountToBeInsuranced + 'Found Insurance row ');
                    //console.log(insuranceSellRateAry[i]);
                    ret_ary = insuranceSellRateAry[i][0] ;
                    break;
                } 
            }
            return ret_ary; 
        } 
    }
</script>
    <?php 
    
    if((int)$bookingDataArr['iCustomerCurrencyChanged']==1 && $loop_counter>0)
    {
        for($j=0;$j<$loop_counter;$j++)
        {
            ?>
            <script>
                update_quote_price('<?php echo $j; ?>');
            </script>
            <?php
        }
    }
    $formId = 'pending_task_tray_form';
    if(!empty($kBooking_error->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking_error->arErrorMessages);
    }  
}
 
function display_booking_quote_pricing_details($kBooking,$number,$quotesAry=array(),$editable_flag=false,$added_new_flag=false,$edit_all_fields=false)
{   
    $t_base="management/pendingTray/";     
    $kBooking_new = new cBooking();
    $allow_add_quote_flag = false;
    if($added_new_flag)
    { 
        $iAddingNewQuote = 1;
        $allow_add_quote_flag = true;
    } 
    $idBooking = $kBooking->idBooking ;
    $bookingDataArr = $kBooking_new->getBookingDetails($idBooking); 
    $idCustomerCurrency = $bookingDataArr['idCurrency'];
    $szCustomerCurrency = $bookingDataArr['szCurrency'];
    $iQuickQuoteStatus = $bookingDataArr['iQuickQuote'];
    $iFinancialVersion = $bookingDataArr['iFinancialVersion']; 
    //$fCustomerExchangeRate = $bookingDataArr['fExchangeRate']; 
      
    $kVatApplication = new cVatApplication();
    $fApplicableVatRate = 0;
    if($bookingDataArr['iPrivateShipping']==1)
    {
        $vatInputAry = array();
        $vatInputAry['idCustomerCountry'] = $bookingDataArr['szCustomerCountry'];
        $vatInputAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
        $vatInputAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
        
        $fApplicableVatRate = $kVatApplication->getTransportecaVat($vatInputAry);   
    } 
    $bCalculateInsurance = false;
    
    if($idCustomerCurrency=='1') //USD
    {
        $fCustomerExchangeRate = 1;   
    }
    else
    {
        $kWarehouseSearch = new cWHSSearch();
        $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency);		 
        $fCustomerExchangeRate = $resultAry['fUsdValue'];
    }
    
    $fValueofGoodsUSD = $bookingDataArr['fValueOfGoodsUSD']; 
    $iInsuranceChoice = $bookingDataArr['iInsuranceChoice'];
    $fManualFeeCustomerCurrency = 0;
      // print_r($quotesAry);     
    if(!empty($_POST['quotePricingAry']))
    {
        $loop_number = $number ;
        $askForQuoteAry = $_POST['quotePricingAry'] ;
        $idTransportMode = $askForQuoteAry['idTransportMode'][$loop_number];
        $idForwarder = $askForQuoteAry['idForwarder'][$loop_number];
        $szForwarderContact = $askForQuoteAry['szForwarderContact'][$loop_number];
        $idBookingQuote = $askForQuoteAry['idBookingQuote'][$loop_number];
        $idBookingQuotePricing = $askForQuoteAry['idBookingQuotePricing'][$loop_number]; 
        $iActive = $askForQuoteAry['iActive'][$loop_number];
        $iOffLineQuotes = $askForQuoteAry['iOffLineQuotes'][$loop_number];

        $iRemovePriceGuarantee = $askForQuoteAry['iRemovePriceGuarantee'][$loop_number];
        $fManualFeeCustomerCurrency = $askForQuoteAry['fManualFeeCustomerCurrency'][$loop_number];

        
        //$szCustomerCurrency = $askForQuoteAry['szCustomerCurrency'];  
        
        $idTransportMode = $askForQuoteAry['idTransportMode'][$loop_number];
        $idForwarder = $askForQuoteAry['idForwarder'][$loop_number];
        $idForwarderCurrency = $askForQuoteAry['idForwarderCurrency'][$loop_number];
        $fTotalPriceForwarderCurrency = $askForQuoteAry['fTotalPriceForwarderCurrency'][$loop_number]; 
        $fTotalVatForwarder = $askForQuoteAry['fTotalVatForwarder'][$loop_number];
        $iTransitHours = $askForQuoteAry['iTransitHours'][$loop_number];
        $dtQuoteValidTo = $askForQuoteAry['dtQuoteValidTo'][$loop_number];
        $szForwarderComment = htmlentities($askForQuoteAry['szForwarderComment'][$loop_number]);
        
        $fTotalPriceCustomerCurrency = $askForQuoteAry['fTotalPriceCustomerCurrency'][$loop_number];
        $fReferalPercentage = $askForQuoteAry['fReferalPercentage'][$loop_number];
        $fReferalAmount = $askForQuoteAry['fReferalAmount'][$loop_number];
        $fTotalPriceCustomerCurrency = $askForQuoteAry['fTotalPriceCustomerCurrency'][$loop_number];
        $iPickVatFromHidden = $askForQuoteAry['iPickVatFromHidden'][$loop_number];
        
        if($iPickVatFromHidden==1)
        {
            $fTotalVat = $askForQuoteAry['fTotalVatHidden'][$loop_number];
        }
        else
        {
            $fTotalVat = $askForQuoteAry['fTotalVat'][$loop_number];
        }
        
        $fTotalInsuranceCostForBookingCustomerCurrency = $askForQuoteAry['fTotalInsuranceCostForBookingCustomerCurrency'][$loop_number];
        $iInsuranceComments = $askForQuoteAry['iInsuranceComments'][$loop_number];   
        $szTransportMode = $askForQuoteAry['szTransportMode'][$loop_number]; 
        $szForwarderDispName = $askForQuoteAry['szForwarderDispName'][$loop_number];
        $szQuotePrice = $askForQuoteAry['szQuotePrice'][$loop_number];
        $szTotalVatForwarderPrice = $askForQuoteAry['szTotalVatForwarderPrice'][$loop_number];
        $szTransitTime = $askForQuoteAry['szTransitTime'][$loop_number];
        $dtValidity = $askForQuoteAry['dtValidity'][$loop_number];
        $szForwarderComment = htmlentities($askForQuoteAry['szForwarderComment'][$loop_number]);
        $fQuotePriceCustomerCurrency = $askForQuoteAry['fQuotePriceCustomerCurrency'][$loop_number]; 
        $iAddingNewQuote = $askForQuoteAry['iAddingNewQuote'][$loop_number];  
        $iProfitType = $askForQuoteAry['iProfitType'][$loop_number];   
        
        $fPriceMarkUp = $askForQuoteAry['fPriceMarkUp'][$loop_number];   
        $fCustomerExchangeRate = $askForQuoteAry['fCustomerExchangeRate'][$loop_number];   
        $fForwarderExchangeRate = $askForQuoteAry['fForwarderExchangeRate'][$loop_number];   
        $fTotalVatCustomerCurrency = $askForQuoteAry['fTotalVatCustomerCurrency'][$loop_number];   
        $idForwarderAccountCurrency = $askForQuoteAry['idForwarderAccountCurrency'][$loop_number];   
        $szForwarderQuoteCurrencyName = $askForQuoteAry['szForwarderQuoteCurrencyName'][$loop_number];   
        $fReferalPercentageHidden = $askForQuoteAry['fReferalPercentageHidden'][$loop_number];   
        $fReferalAmountHidden = $askForQuoteAry['fReferalAmountHidden'][$loop_number];   
        $fReferalPercentageStandarad = $askForQuoteAry['fReferalPercentageStandarad'][$loop_number];    
        $fTotalPriceCustomerCurrencyHidden = $askForQuoteAry['fTotalPriceCustomerCurrencyHidden'][$loop_number];   
        $fQuotePriceCustomerCurrencyHidden = $askForQuoteAry['fTotalPriceForwarderCurrencyHidden'][$loop_number];   
        
        $fTotalVatHidden = $askForQuoteAry['fTotalVatHidden'][$loop_number]; 
        $fInsuranceValueUpto = $askForQuoteAry['fInsuranceValueUpto'][$loop_number]; 
        $szForwarderLogoPath = $askForQuoteAry['szForwarderLogoPath'][$loop_number]; 
        
        $iManualFeeApplicable = $askForQuoteAry['iManualFeeApplicable'][$loop_number]; 
        $idHandlingCurrency = $askForQuoteAry['idHandlingCurrency'][$loop_number]; 
        $szHandlingCurrency = $askForQuoteAry['szHandlingCurrency'][$loop_number]; 
        $fHandlingCurrencyExchangeRate = $askForQuoteAry['fHandlingCurrencyExchangeRate'][$loop_number];  
        $fTotalHandlingFee = $askForQuoteAry['fTotalHandlingFee'][$loop_number];  
        $fTotalHandlingFeeCustomerCurrency = $askForQuoteAry['fTotalHandlingFeeCustomerCurrency'][$loop_number]; 
            
        if($iActive==1)
        {
            $active_flag = true;
            $iQuoteReceived = 1;
        }
        else
        {
            $active_flag = false;
        }   
        
        $idInsuranceSellRate = $askForQuoteAry['idInsuranceSellRate'][$loop_number]; 
        $idInsuranceBuyRate = $askForQuoteAry['idInsuranceBuyRate'][$loop_number];  
    }
    else if(!empty($quotesAry))
    {       
        $idTransportMode = $quotesAry['idTransportMode'];
        $idForwarder = $quotesAry['idForwarder'];
        $idBooking = $quotesAry['idBooking'];
        $szForwarderContact = $quotesAry['szForwarderContact'];
        $idBookingQuote = $quotesAry['id'];
        $iActive = $quotesAry['iActive'];   
        $szTransportMode = $quotesAry['szTransportMode']; 
        $idForwarder = $quotesAry['idForwarder']; 
        $szTransitTime = $quotesAry['iTransitHours'];
        $iInsuranceComments = $quotesAry['iInsuranceComments'];
        $iOffLineQuotes = $quotesAry['iOffLine'];
        $iRemovePriceGuarantee = $quotesAry['iRemovePriceGuarantee'];
        
        $kForwarder = new cForwarder();
        $kForwarder->load($idForwarder);
        
        $szForwarderLogoPath = $kForwarder->szLogo;
        $idForwarderAccountCurrency = $kForwarder->szCurrency ;
        $szForwarderDispName = $kForwarder->szDisplayName." (".$kForwarder->szForwarderAlies.")" ;
        $iProfitType = $kForwarder->iProfitType;
        
        if($idForwarder>0 && $idTransportMode>0)
        {
            $referralFeeAry = array();
            $referralFeeAry['idForwarder'] = $idForwarder;
            $referralFeeAry['idTransportMode'] = $idTransportMode; 
            $kReferralPricing = new cReferralPricing();
            $fReferralFee = $kReferralPricing->getForwarderReferralFee($referralFeeAry);  
            if($fReferralFee>0)
            {
                $fReferalPercentage = round((float)$fReferralFee,2);

                $fReferalPercentageAry = explode(".",$fReferalPercentage);
                if(strlen($fReferalPercentageAry[1])==1)
                {
                    $fReferalPercentage = $fReferalPercentage."0";
                }
                else if(strlen($fReferalPercentageAry[1])==0)
                {
                    $fReferalPercentage = $fReferalPercentage.".00";
                }
            }
            else
            {
                $fReferalPercentage = 0;
            } 
        } 
        else
        {
            $fReferalPercentage = 0;
        }
        
        if($quotesAry['idQuotePriceDetails']>0 && $quotesAry['iClosed']==1)
        {
            $iQuoteReceived = 1; 
        }
        else
        {
            $iQuoteReceived = 0; 
        } 
           
        $kWhsSearch = new cWHSSearch();  
        $fPriceMarkUp = $kWhsSearch->getManageMentVariableByDescription('__MARK_UP_PRICING_PERCENTAGE__'); 
        
        if($quotesAry['idForwarderCurrency']=='1') //USD
        {
            $quotesAry['fForwarderCurrencyExchangeRate'] = 1;   
        }
        else
        {
            $kWarehouseSearch = new cWHSSearch();
            $resultAry = $kWarehouseSearch->getCurrencyDetails($quotesAry['idForwarderCurrency']);		 
            $quotesAry['fForwarderCurrencyExchangeRate'] = $resultAry['fUsdValue']; 
        }  
        
        if($iFinancialVersion==2)
        {
            /*
            * We are now always calculating VAT on load of Quote Screen 
            * @Ajay - 17th March 2017
            */ 
            if($fApplicableVatRate>0)
            {
                $fTotalVat = round((float)($quotesAry['fTotalPriceCustomerCurrency'] * $fApplicableVatRate * 0.01),2);
            }
            else
            {
                $fTotalVat = 0;
            }
            //Replacing DB values with calculated values
            $quotesAry['fTotalVat'] = $fTotalVat; 
            $quotesAry['fTotalVatCustomerCurrency_hidden'] = $fTotalVat; 
        } 
        else
        {
            $fTotalVat = $quotesAry['fTotalVatCustomerCurrency']; 
        }
        if($quotesAry['iCustomerPriceUpdated']>=1)
        {
            $fTotalPriceCustomerCurrency = round((float)$quotesAry['fTotalPriceCustomerCurrency']); 
            $fReferalPercentage = round((float)$quotesAry['fReferalPercentage'],2); 
             
            $fReferalAmount = round((float)$quotesAry['fReferalAmount'],2); 
            $fQuotePriceCustomerCurrency = round((float)$quotesAry['fQuotePriceCustomerCurrency']) ;
            //echo " Ins flg: ".$quotesAry['iQuoteSentToCustomer'];
            if($iQuickQuoteStatus==__QUICK_QUOTE_SEND_QUOTE__)
            {
                $bCalculateInsurance = true;
            }
            else if($quotesAry['iQuoteSentToCustomer']!=1)
            {
                $bCalculateInsurance = true;
            }
            else
            {
                $fTotalInsuranceCostForBookingCustomerCurrency = round((float)$quotesAry['fTotalInsuranceCostForBookingCustomerCurrency']);
            }
            
            
            if($quotesAry['fForwarderCurrencyExchangeRate']>0)
            {
                $fForwarderExchangeRate = round((float)$quotesAry['fForwarderCurrencyExchangeRate'],4);
            }
            else
            {
                $fForwarderExchangeRate = 0;
            }
           // echo $fForwarderExchangeRate."fForwarderExchangeRate--3";
            $fTotalVatCustomerCurrency = $fTotalVat ;
            
            if($quotesAry['fForwarderCurrencyExchangeRate']>0)
            { 
                $fForwarderCurrencyExchangeRate = round((float)$quotesAry['fForwarderCurrencyExchangeRate'],4);
                if($fForwarderCurrencyExchangeRate>0)
                {
                    $fTotalPriceForwarderCurrency_usd = round((float)($quotesAry['fTotalPriceForwarderCurrency'] * $fForwarderCurrencyExchangeRate),2) ;
                    $fTotalVat_usd = round((float)($quotesAry['fTotalVat'] * $fForwarderCurrencyExchangeRate),2); 
                } 
            } 
            
            $fReferalAmountHidden = $quotesAry['fReferalAmount_hidden'];
            $fReferalPercentageHidden = $quotesAry['fReferalPercentage_hidden']  ;
            $fReferalPercentageStandarad = $fReferalPercentage ;
            $fTotalPriceCustomerCurrencyHidden = $quotesAry['fTotalPriceCustomerCurrency_hidden'];
            $fTotalVatHidden = $quotesAry['fTotalVatCustomerCurrency_hidden'];
            $fQuotePriceCustomerCurrencyHidden = $quotesAry['fQuotePriceCustomerCurrency_hidden'];
            
            $idInsuranceSellRate = $quotesAry['idInsuranceRate']; 
            $idInsuranceBuyRate = $quotesAry['idInsuranceRate_buyRate']; 
        }
        else
        {   
            if($idCustomerCurrency=='1') //USD
            {
                $fCustomerExchangeRateGlobal = 1;   
            }
            else
            {
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency);		 
                $fCustomerExchangeRateGlobal = $resultAry['fUsdValue'];
            } 
            //echo $iProfitType."iProfitType";
            if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__)
            {
                $manualFeeSearchAry = array();
                $manualFeeSearchAry['idForwarder'] = $idForwarder; 
                $manualFeeSearchAry['szProduct'] = $idTransportMode;
                $kVatApplication = new cVatApplication();
                $manualFeeAry = array();
                $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                $manualFeeAry = $manualFeeAry[0];

                if(empty($manualFeeAry))
                {
                    /*
                    * If There is no Manual fee defined for the Forwarder then we calculate based on All other forwarder manual fee rates
                    */
                    $manualFeeSearchAry = array();
                    $manualFeeSearchAry['iAllForwarderFlag'] = '1'; 
                    $manualFeeSearchAry['szProduct'] = $idTransportMode;
                    $manualFeeAry = $kVatApplication->getManualFeeListing(false, false, $manualFeeSearchAry);
                    $manualFeeAry = $manualFeeAry[0];
                }

                if(empty($manualFeeAry))
                {
                    /*
                    * If There is no Manual fee defined for the Trade then we calculate based on default manual fee rates
                    */
                    $manualFeeAry = $kVatApplication->getDefaultManualFee();
                } 
                
                if(!empty($manualFeeAry)){
                    $kWarehouseSearch = new cWHSSearch();
                    $resultHandlingFeeAry = $kWarehouseSearch->getCurrencyDetails($manualFeeAry['idCurrency']);		 
                    $fHandlingFeeExchangeRate = $resultHandlingFeeAry['fUsdValue'];

                    $quotesAry['idHandlingCurrency'] = $manualFeeAry['idCurrency'];
                    $quotesAry['szHandlingCurrency']= $manualFeeAry['szCurrency']; 
                    $quotesAry['fHandlingCurrencyExchangeRate'] = $fHandlingFeeExchangeRate; 
                    $quotesAry['iManualFeeApplicable'] = 0;
                    $quotesAry['iHandlingFeeApplicable'] = 1;
                    $quotesAry['fTotalHandlingFee'] = round((float)$manualFeeAry['fManualFee'],2); 
                    $quotesAry['fTotalHandlingFeeUSD'] = round((float)$quotesAry['fTotalHandlingFee']*$fHandlingFeeExchangeRate,2);
                }               
            }
            else
            {
                $quotesAry['iManualFeeApplicable'] = 0;
                $quotesAry['iHandlingFeeApplicable'] = 0;
            }    
            if($quotesAry['fForwarderCurrencyExchangeRate']>0)
            { 
                $fForwarderCurrencyExchangeRate = round((float)$quotesAry['fForwarderCurrencyExchangeRate'],4);
                if($fForwarderCurrencyExchangeRate>0)
                {
                    $fTotalPriceForwarderCurrency_usd = (($quotesAry['fTotalPriceForwarderCurrency']) * $fForwarderCurrencyExchangeRate) ;
                    $fTotalVat_usd = ($quotesAry['fTotalVat'] * $fForwarderCurrencyExchangeRate); 
                }                 
            }   
            if($fCustomerExchangeRate>0)
            {
                $fTotalPriceCustomerCurrency = ($fTotalPriceForwarderCurrency_usd / $fCustomerExchangeRate);
                $fTotalVat = ($fTotalVat_usd / $fCustomerExchangeRate) ;
            }  
            if($idForwarderAccountCurrency!=$idCustomerCurrency)
            { 
                $fTotalPriceCustomerCurrency = (($fTotalPriceCustomerCurrency * (1 + ($fPriceMarkUp/100) )));  
                $fTotalVat = ($fTotalVat * (1 + ($fPriceMarkUp/100)));  
            }
            
            $fManualFeeForwarderCurrency=0;
            if($quotesAry['iHandlingFeeApplicable']==1)
            {
                if($quotesAry['idHandlingCurrency']==$quotesAry['idForwarderCurrency'])
                {
                    $fManualFeeForwarderCurrency =  round((float)$quotesAry['fTotalHandlingFee']);
                }
                else if($quotesAry['idForwarderCurrency']==1)
                {
                    $fManualFeeForwarderCurrency = round((float)$quotesAry['fTotalHandlingFeeUSD']);
                }
                else if($quotesAry['fForwarderCurrencyExchangeRate']>0)
                {
                    $fManualFeeForwarderCurrency = round((float)($quotesAry['fTotalHandlingFeeUSD']/$quotesAry['fForwarderCurrencyExchangeRate']));
                }
            }
            $fQuotePriceCustomerCurrency = round((float)$fTotalPriceCustomerCurrency+$fManualFeeForwarderCurrency); 
            
            $fReferalAmount = 0;
            $fReferalPercentageHidden = $fReferalPercentage; 
            $fReferalPercentageStandarad = $fReferalPercentage ;
            if($fReferalPercentage>0)
            {
                $fReferalAmountHidden = (($fTotalPriceForwarderCurrency_usd+$fTotalVat_usd) * $fReferalPercentage)/100; 
                if($fCustomerExchangeRate>0)
                {
                    $fReferalAmountHidden = ($fReferalAmountHidden / $fCustomerExchangeRate) ; 
                }
                else
                {
                    $fReferalAmountHidden = 0;
                }
                if($idForwarderAccountCurrency!=$idCustomerCurrency)
                {
                    $fReferalAmountHidden = $fReferalAmountHidden * (1 + ($fPriceMarkUp/100)) ;        
                }
                $fReferalAmount = $fReferalAmountHidden;
                $fReferalAmount_vat = (($fTotalVat * $fReferalPercentage)/100);
            } 
            $fManualFeeCustomerCurrency=0;
            if($quotesAry['iHandlingFeeApplicable']==1)
            {
                if($quotesAry['idHandlingCurrency']==$idCustomerCurrency)
                {
                    $fManualFeeCustomerCurrency =  round((float)$quotesAry['fTotalHandlingFee']);
                }
                else if($idCustomerCurrency==1)
                {
                    $fManualFeeCustomerCurrency = round((float)$quotesAry['fTotalHandlingFeeUSD']);
                }
                else if($fCustomerExchangeRate>0)
                {
                    $fManualFeeCustomerCurrency = round((float)($quotesAry['fTotalHandlingFeeUSD']/$fCustomerExchangeRate));
                }
            }
            //echo $fManualFeeCustomerCurrency."fManualFeeCustomerCurrency";
            $fTotalVatCustomerCurrency = $fTotalVat ;
            $fForwarderExchangeRate = $fForwarderCurrencyExchangeRate ;
            //echo $fForwarderExchangeRate."fForwarderExchangeRate--2";
            if($iProfitType==__FORWARDER_PROFIT_TYPE_MARK_UP__)
            {
                $fTotalPriceCustomerCurrencyHidden = $fTotalPriceCustomerCurrency + $fReferalAmount ;
                $fTotalPriceCustomerCurrency = round((float)($fTotalPriceCustomerCurrency + $fReferalAmount));
                $fTotalVat = round((float)($fTotalVat + $fReferalAmount_vat),2);
            }
            else
            {
                $fTotalPriceCustomerCurrencyHidden =  $fTotalPriceCustomerCurrency+$fManualFeeCustomerCurrency ;
                $fTotalPriceCustomerCurrency = round((float)($fTotalPriceCustomerCurrency+$fManualFeeCustomerCurrency)) ;
                $fTotalVat = round((float)($fTotalVat),2);
            } 
            
            $fReferalAmount = round((float)$fReferalAmountHidden);  
            $bCalculateInsurance = true;
        }   
        $bCalculateInsurance = true;
        if($bCalculateInsurance)
        {  
            $insuranceInputAry = $bookingDataArr ;
            $insuranceInputAry['idTransportMode'] = $idTransportMode ;
            $insuranceInputAry['fTotalBookingPriceUsd'] = $fTotalPriceForwarderCurrency_usd + $fValueofGoodsUSD; 
            
            $insuranceInputAry['idTransportMode'] = $idTransportMode ;
            $insuranceInputAry['fTotalBookingPriceUsd'] = $fTotalPriceUSD + $fValueofGoodsUSD;  
            
            $insuranceInputAry['fTotalPriceCustomerCurrency'] = $fTotalPriceCustomerCurrency;
            $insuranceInputAry['fTotalVat'] = $fTotalVat; 
             
            if($idCustomerCurrency==1)
            { 
                $szCustomerCurrency = 'USD';
                $fCustomerCurrencyExchangeRate = 1;   
            }
            else
            {
                $kWarehouseSearch = new cWHSSearch();
                $resultAry = $kWarehouseSearch->getCurrencyDetails($idCustomerCurrency);		
 
                $szCustomerCurrency = $resultAry['szCurrency'];
                $fCustomerCurrencyExchangeRate = $resultAry['fUsdValue']; 
            }
            $insuranceInputAry['idCurrency'] = $idCustomerCurrency;
            $insuranceInputAry['szCurrency'] = $szCustomerCurrency;
            $insuranceInputAry['fExchangeRate'] = $fCustomerCurrencyExchangeRate; 
            $insuranceInputAry['fValueOfGoods'] = $fValueofGoodsUSD;
            $insuranceInputAry['idInsuranceCurrency'] = 1; //USD
            
            $kInsurance = new cInsurance();
            if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry,true))
            {
                $fInsuranceValueUpto = $fValueUptoPrice = $kInsurance->fValueUptoPrice ;
                $iTransportationInsuranceAvailable = 1;
                /*
                 * If Insurance: Optional or Yes and Value of goods is equal to 0 then we show insurance sell price=0
                 */
                if($fValueofGoodsUSD<=0 && ($iInsuranceChoice==1 || $iInsuranceChoice==3))
                {
                    $fTotalInsuranceCostForBookingCustomerCurrency = 0;
                }  
                else if($iInsuranceChoice!=2) //
                { 
                    $fTotalInsuranceCostForBookingCustomerCurrency = round((float)$kInsurance->fTotalInsuranceCostForBookingCustomerCurrency) ; 
                }
                else
                {
                    $fTotalInsuranceCostForBookingCustomerCurrency = 0;
                }
            }
            else
            {
               $fTotalInsuranceCostForBookingCustomerCurrency = 'N/A' ;
               $iTransportationInsuranceAvailable = 2;
            } 
            $idInsuranceSellRate = $kInsurance->idInsuranceSellRate; 
            $idInsuranceBuyRate = $kInsurance->idInsuranceBuyRate;
        }
        $idForwarderCurrency = $quotesAry['idForwarderCurrency'] ;
        $fTotalPriceForwarderCurrency = '';
        $szQuotePrice =''; 
        if(!empty($quotesAry['fTotalPriceForwarderCurrency']))
        {
            $fTotalPriceForwarderCurrency = round((float)$quotesAry['fTotalPriceForwarderCurrency'],2) ; 
        }
        $szForwarderQuoteCurrencyName = $quotesAry['szForwarderCurrency'];
        if(!empty($quotesAry['fTotalPriceForwarderCurrency']))
        {
            if(__float($quotesAry['fTotalPriceForwarderCurrency']))
            {
                $szQuotePrice = $quotesAry['szForwarderCurrency']." ".number_format((float)$quotesAry['fTotalPriceForwarderCurrency'],2);
            } 
            else
            {
                $szQuotePrice = $quotesAry['szForwarderCurrency']." ".number_format((float)$quotesAry['fTotalPriceForwarderCurrency']);
            }
        }  
        $fTotalVatForwarder = '';
        if(!empty($quotesAry['fTotalVat']))
        {
            $fTotalVatForwarder = round((float)$quotesAry['fTotalVat'],2) ;
        }  
        $szTotalVatForwarderPrice = '';
        if(!empty($quotesAry['fTotalVat']))
        {
            $szTotalVatForwarderPrice = $quotesAry['szForwarderCurrency']." ".number_format((float)$quotesAry['fTotalVat'],2);
        }
        
        $iTransitHours = $quotesAry['iTransitHours'];
        if($quotesAry['iQuoteSentToCustomer']!=1 && $editable_flag)
        {
            /*
             *  When you make a manual RFQ, and you do not send a quote request to the forwarder, but go directly to QUOTE and click Edit, then the “Validity” field should be default updated with the date of today +1 month (similar to what we do for the additional lines we add when clicking ADD on the QUOTE screen)
             */
            $dtQuoteValidTo = date('d/m/Y',strtotime('+1 MONTH'));
        }
        else
        {
            if(!empty($quotesAry['dtQuoteValidTo']) && $quotesAry['dtQuoteValidTo']!='0000-00-00 00:00:00')
            {
                $dtValidity = date('d F',strtotime($quotesAry['dtQuoteValidTo']));
                $dtQuoteValidTo = date('d/m/Y',strtotime($quotesAry['dtQuoteValidTo']));
            } 
            else
            {
                $dtQuoteValidTo = date('d/m/Y',strtotime('+1 MONTH'));
            }
        }
        
        $szForwarderComment = htmlentities($quotesAry['szForwarderComment']);  
        $idBookingQuotePricing = $quotesAry['idQuotePriceDetails']; 
        
        //$fTotalPriceCustomerCurrencyHidden = $fTotalPriceCustomerCurrency ;
        $fTotalVatHidden = $fTotalVat ; 
        $fQuotePriceCustomerCurrencyHidden = $fQuotePriceCustomerCurrency ; 
        
        $idHandlingCurrency = $quotesAry['idHandlingCurrency'];
        $szHandlingCurrency = $quotesAry['szHandlingCurrency']; 
        $fHandlingCurrencyExchangeRate = $quotesAry['fHandlingCurrencyExchangeRate']; 
        $iManualFeeApplicable = $quotesAry['iManualFeeApplicable'];
        $iHandlingFeeApplicable = $quotesAry['iHandlingFeeApplicable'];
        /*
         *  1. $iHandlingFeeApplicable should be equals to 1 in case of Standard pricing(Voga) 
         *  2. $iManualFeeApplicable should be equals to 1 in case of Quick Quote
         *  In both case we have Handling fee applicable on bookings and so treat them same in both cases.
         */
        $subNoManualFee=false;
        if($iManualFeeApplicable ==0)
        {
            $subNoManualFee=true;
        }
        //echo $iHandlingFeeApplicable."iHandlingFeeApplicable";
        if($iHandlingFeeApplicable==1)
        {
            $iManualFeeApplicable = 1;
        }
        $fTotalHandlingFee = $quotesAry['fTotalHandlingFee']; 
        if($iManualFeeApplicable==1 && $fTotalHandlingFee<=0)
        {
            if($fHandlingCurrencyExchangeRate>0)
            {
                $fTotalHandlingFee = round((float)($quotesAry['fTotalHandlingFeeUSD']/$fHandlingCurrencyExchangeRate));
            } 
        } 
        if($iManualFeeApplicable==1)
        {
            if($idHandlingCurrency==$idCustomerCurrency)
            {
                $fManualFeeCustomerCurrency =  round((float)$fTotalHandlingFee,2);
            }
            else if($idCustomerCurrency==1)
            {
                $fManualFeeCustomerCurrency = round((float)$quotesAry['fTotalHandlingFeeUSD'],2);
            }
            else if($fCustomerExchangeRate>0)
            {
                $fManualFeeCustomerCurrency = round((float)($quotesAry['fTotalHandlingFeeUSD']/$fCustomerExchangeRate),2);
            }
            $fManualFeeCustomerCurrencyMarkup=round((float)$quotesAry['fTotalHandlingFeeCustomerCurrencyMarkup'],2);
            $fReferalAmount = $fReferalAmount + $fManualFeeCustomerCurrency+$fManualFeeCustomerCurrencyMarkup ;
            
        } 
        $fTotalHandlingFeeCustomerCurrency = $fManualFeeCustomerCurrency;
    }   
     
    if($idForwarder>0)
    {
        $kForwarder = new cForwarder();
        $kForwarder->load($idForwarder); 
        
        if((int)$idForwarderCurrency==0)
        {
            $idForwarderCurrency=$kForwarder->szCurrency;
            $kWarehouseSearch = new cWHSSearch();
            $resArr=$kWarehouseSearch->getCurrencyDetails($kForwarder->szCurrency);
            $fForwarderExchangeRate=$resArr['fUsdValue'];
            //echo $fForwarderExchangeRate."fForwarderExchangeRate--1";
        } 
    }  
    if($iAddingNewQuote==1)
    {
        $editable_flag = true;
        $dtQuoteValidTo = date('d/m/Y',strtotime('+1 MONTH'));
    }  
    if(empty($quotesAry) && (int)$iAddingNewQuote==0)
    {
        $dtQuoteValidTo = date('d/m/Y',strtotime('+1 MONTH'));
    }
    if($iQuoteReceived==1 || $edit_all_fields)
    {
        $szClassName = "class='active-box' " ;
    } 
    else
    {
        $szClassName = "class='disabled-box' " ;
    }
    
    if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__)
    {
        $szGrossProfitType = 'Referral';
    }
    else if($iProfitType==__FORWARDER_PROFIT_TYPE_MARK_UP__)
    {
        $szGrossProfitType = 'Mark-up';
    } 
    
    $kConfig = new cConfig();
    $kForwarder = new cForwarder(); 
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(true); 
     
    $allCurrencyArr = array();
    $allCurrencyArr=$kConfig->getBookingCurrency(false,true);
     
    $szRowContainerClass = "quote-row-container clearfix";
    $szMainContainerClass="overview-form";
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container"; 
    $szLevelValue ='';
    if(!$editable_flag)
    {
        $szLevelValue = 'label-value';
        $szLevelVatClass = ' label-vat';
    }      
    /*
    * Checking Insurance is available for this or not. By default we assume we will have Insurance untill shipper/consignee selected
    */
    $iTransportationInsuranceAvailable = 1;
    if($bookingDataArr['idOriginCountry']>0 && $bookingDataArr['idDestinationCountry']>0)
    {
        $fValueofGoodsUSD = $bookingDataArr['fValueOfGoodsUSD'];  
        $insuranceInputAry = array();
        $insuranceInputAry['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
        $insuranceInputAry['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];
        if($idTransportMode>0)
        {
            $insuranceInputAry['idTransportMode'] = $idTransportMode;
        }
        else
        {
            $insuranceInputAry['idTransportMode'] = $bookingDataArr['idTransportMode'];
        } 
        $insuranceInputAry['isMoving'] = $isMoving;
        $insuranceInputAry['fTotalBookingPriceUsd'] = 1;
        $insuranceInputAry['szCargoType'] = $bookingDataArr['szCargoType']; 
            
        $kInsurance = new cInsurance();
        if($kInsurance->getInsuranceDetailsFroBooking($insuranceInputAry))
        {
            /*
            * This means we do have Transportation Insurance for this Trade and should show all available option on this screen
            */ 
            $iTransportationInsuranceAvailable = 1;
        }
        else
        {
            /*
            * This means we do not have Transportation Insurance for this Trade and should show only 'Not available' option to select
            */  
            $iTransportationInsuranceAvailable = 2;
        }
    }  
   $kVatApplication= new cVatApplication();
    $manualFeeArr=$kVatApplication->getManualFeeListing();
    $ctr=0;
    if(!empty($manualFeeArr))
    {
        foreach($manualFeeArr as $manualFeeArrs)
        {
            $keyForwarderProduct=$manualFeeArrs['idForwarder']."_".$manualFeeArrs['szProduct'];
            $manualFeeNewArr[$keyForwarderProduct]['szManualFee']=$manualFeeArrs['idCurrency']."_".$manualFeeArrs['fManualFee'];
            ++$ctr;
        }
        
        $manualFeeNewJsonStr = json_encode($manualFeeNewArr);
       // echo $manualFeeNewJsonStr;
    }
    
    $manualFeeAry = $kVatApplication->getDefaultManualFee();
    $defaultManualFeeNewArr[0]=$manualFeeAry['idCurrency']."_".$manualFeeAry['fManualFee'];
    $defaultManualFeeStr=json_encode($defaultManualFeeNewArr);
    //echo $defaultManualFeeStr;
    ?>   
    <div id="add_booking_quote_sub_container_<?php echo $number; ?>" <?php echo $szClassName; ?>> 
        <script type="text/javascript">
            $(document).ready(function(){
                __JS_ONLY_MANUAL_FEE_LIST__ = <?php echo $manualFeeNewJsonStr ?>;
                __JS_ONLY_DEFAULT_MANUAL_FEE_LIST__ = <?php echo $defaultManualFeeStr ?>;
               $("#dtQuoteValidTo_<?php echo $number; ?>").datepicker();  
               autofill_vat_currency('<?php echo $idForwarderCurrency; ?>','<?php echo $number; ?>');  
               enableQuoteSendButton();  
               
               <?php if($iTransportationInsuranceAvailable==2){ ?> 
                    if($("#fTotalInsuranceCostForBookingCustomerCurrency_"+'<?php echo $number; ?>').length)
                    {
                        $("#fTotalInsuranceCostForBookingCustomerCurrency_"+'<?php echo $number; ?>').attr('disabled','disabled');
                        $("#fTotalInsuranceCostForBookingCustomerCurrency_"+'<?php echo $number; ?>').val('Not available');
                    }
               <?php } ?>
               <?php
               
            if(!empty($quotesAry) && (int)$quotesAry['iCustomerPriceUpdated']==0)
            {?>
            //update_quote_price('<?php echo $number; ?>');
            <?php }?>
            }); 
            
        </script> 
        <input type="hidden" value="<?php echo $fCustomerExchangeRate; ?>" name="quotePricingAry[fCustomerExchangeRate][<?php echo $number; ?>]" id="fCustomerExchangeRate_<?php echo $number; ?>">
        <input type="hidden" value="<?php echo $fForwarderExchangeRate; ?>" name="quotePricingAry[fForwarderExchangeRate][<?php echo $number; ?>]" id="fForwarderExchangeRate_<?php echo $number; ?>">
        <input type="hidden" value="<?php echo $fTotalVatCustomerCurrency; ?>" name="quotePricingAry[fTotalVatCustomerCurrency][<?php echo $number; ?>]" id="fTotalVatCustomerCurrency_<?php echo $number; ?>">
        <input type="hidden" value="<?php echo $fPriceMarkUp; ?>" name="quotePricingAry[fPriceMarkUp][<?php echo $number; ?>]" id="fPriceMarkUp_<?php echo $number; ?>">
        <input type="hidden" value="<?php echo $fTotalPriceForwarderCurrency_usd; ?>" name="quotePricingAry[fTotalPriceForwarderCurrency_usd][<?php echo $number; ?>]" id="fTotalPriceForwarderCurrency_usd_<?php echo $number; ?>">
        <input type="hidden" value="<?php echo $fTotalVat_usd; ?>" name="quotePricingAry[fTotalVat_usd][<?php echo $number; ?>]" id="fTotalVat_usd_<?php echo $number; ?>">
        <input type="hidden" value="<?php echo $idForwarderAccountCurrency; ?>" name="quotePricingAry[idForwarderAccountCurrency][<?php echo $number; ?>]" id="idForwarderAccountCurrency_<?php echo $number; ?>">
        <input type="hidden" value="<?php echo $fApplicableVatRate; ?>" name="quotePricingAry[fApplicableVatRate][<?php echo $number; ?>]" id="fApplicableVatRate_<?php echo $number; ?>"> 
        <input type="hidden" value="<?php echo $szForwarderQuoteCurrencyName; ?>" name="quotePricingAry[szForwarderQuoteCurrencyName][<?php echo $number; ?>]" id="szForwarderQuoteCurrencyName_<?php echo $number; ?>">    
        <div class="from-forwarder-box">
            
            <div class="vertical-text">
                <span>FROM FORWARDER</span>
            </div>
            <div class="<?php echo $szRowContainerClass; ?>"> 
                <div class=" <?php echo $szMainContainerClass; ?> quote-mode-container">
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/mode')?>:</span>
                    <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>"> 
                        <?php
                            if($editable_flag)
                            {
                                ?>
                                <select name="quotePricingAry[idTransportMode][<?php echo $number; ?>]" id="idTransportMode_<?php echo $number ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableQuoteSendButton();" onchange="updateReferralPercentage('<?php echo $number; ?>');calculate_quote_insurance_price('<?php echo $number; ?>');updateEmailBody('<?php echo $number; ?>');manageHandlingFee('<?php echo $number; ?>');">
                                    <option value="">Select</option>
                                <?php
                                    if(!empty($transportModeListAry))
                                    {
                                       foreach($transportModeListAry as $transportModeListArys)
                                       {
                                        ?>
                                        <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($transportModeListArys['id']==$idTransportMode)?'selected':''); ?>><?php echo $transportModeListArys['szShortName']; ?></option>
                                        <?php
                                       }
                                    }
                                ?> 
                                </select>
                                <?php
                            }
                            else
                            {
                                echo $szTransportMode;
                                ?>
                                <input type="hidden" name="quotePricingAry[idTransportMode][<?php echo $number; ?>]" id="idTransportMode_<?php echo $number ?>" value="<?php echo $idTransportMode; ?>">
                                <input type="hidden" name="quotePricingAry[szTransportMode][<?php echo $number; ?>]" id="szTransportMode_<?php echo $number ?>" value="<?php echo $szTransportMode; ?>">
                                <?php
                            }
                        ?> 
                    </span>
                </div> 
                <div class="<?php echo $szMainContainerClass; ?> quote-company-container"> 
                    <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/company')?>:</span>
                    <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>">  
                        <?php
                            if($editable_flag)
                            {
                                ?> 
                                <select name="quotePricingAry[idForwarder][<?php echo $number; ?>]" id="idForwarder_<?php echo $number ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableQuoteSendButton();" onchange="autofill_profit_type(this.value,'<?php echo $number; ?>');manageHandlingFee('<?php echo $number; ?>');">
                                    <option value="">Select</option>
                                <?php
                                    if(!empty($forwarderListAry))
                                    {
                                       foreach($forwarderListAry as $forwarderListArys)
                                       {
                                        ?>
                                        <option value="<?php echo $forwarderListArys['id']; ?>" <?php echo (($forwarderListArys['id']==$idForwarder)?'selected':''); ?>><?php echo $forwarderListArys['szForwarderAlias']; ?></option>
                                        <?php
                                       }
                                    }
                                ?> 
                                </select>
                                <?php
                            }
                            else
                            {
                                echo $szForwarderDispName;
                                ?>
                                <input type="hidden" name="quotePricingAry[idForwarder][<?php echo $number; ?>]" id="idForwarder_<?php echo $number ?>" value="<?php echo $idForwarder; ?>">
                                <input type="hidden" name="quotePricingAry[szForwarderDispName][<?php echo $number; ?>]" id="szForwarderDispName_<?php echo $number ?>" value="<?php echo $szForwarderDispName; ?>">
                                <?php
                            }
                        ?> 
                    </span>
                </div>
        <?php if($iQuoteReceived==1 || $editable_flag){ ?>
            <div class="<?php echo $szMainContainerClass; ?> quote-forwarder-price-container"> 
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/quote')?>:</span>
                <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>"> 
                     <?php 
                        if($editable_flag)
                        {
                            ?>  
                            <select name="quotePricingAry[idForwarderCurrency][<?php echo $number; ?>]" id="idForwarderCurrency_<?php echo $number ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableQuoteSendButton();" onchange="autofill_vat_currency(this.value,'<?php echo $number; ?>');autofill_currency_exhchange_rate(this.value,'<?php echo $number; ?>');update_quote_price('<?php echo $number; ?>'); ">
                                <option value=""><?=t($t_base.'fields/select');?></option>
                            <?php
                                if(!empty($allCurrencyArr))
                                {
                                    foreach($allCurrencyArr as $allCurrencyArrs)
                                    {
                                        ?><option value="<?=$allCurrencyArrs['id']?>" <?php echo ($idForwarderCurrency ==  $allCurrencyArrs['id']) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                                    }
                                }
                            ?>
                            </select>
                            <input type="text" onkeypress="return format_decimat(this,event)" name="quotePricingAry[fTotalPriceForwarderCurrency][<?php echo $number; ?>]" id="fTotalPriceForwarderCurrency_<?php echo $number; ?>" value="<?php echo $fTotalPriceForwarderCurrency; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);update_quote_price('<?php echo $number; ?>');">
                            <?php
                        }
                        else
                        {
                            echo $szQuotePrice;
                            ?>
                            <input type="hidden" name="quotePricingAry[idForwarderCurrency][<?php echo $number; ?>]" id="idForwarderCurrency_<?php echo $number ?>" value="<?php echo $idForwarderCurrency; ?>">
                            <input type="hidden" name="quotePricingAry[fTotalPriceForwarderCurrency][<?php echo $number; ?>]" id="fTotalPriceForwarderCurrency_<?php echo $number ?>" value="<?php echo $fTotalPriceForwarderCurrency; ?>">
                            <input type="hidden" name="quotePricingAry[szQuotePrice][<?php echo $number; ?>]" id="szQuotePrice_<?php echo $number ?>" value="<?php echo $szQuotePrice; ?>">
                            <?php
                        }
                    ?> 
                </span>
            </div> 
        <?php } else { $szDisabledCb = 'readonly="readonly"'; ?>
            <div class="<?php echo $szRowContainerClass; ?>"> 
                <div class="<?php echo $szMainContainerClass; ?> quote-remind-button-container"> 
                    <span class="<?php echo $szSecondSpanClass; ?>">
                        <a href="javascript:void(0);" onclick="display_task_menu_details('REMIND','3')" class="button1"><span>Remind</span></a>
                    </span>
                </div>
            </div>
        <?php } ?>
                    
        <?php if($number!=1001){  ?>
            <span class="quote-cb-container"><input type="checkbox" <?php echo $szDisabledCb; ?> class="quoteActiveCb" name="quotePricingAry[iActive][<?php echo $number; ?>]" onclick="toggleTaskQuoteContainer('<?php echo $number; ?>');updateEmailBody('<?php echo $number; ?>');enableQuoteSendButton();" <?php if($iQuoteReceived==1 || $active_flag || $allow_add_quote_flag){ ?>checked<?php }?> id="iActive_<?php echo $number; ?>" value="1">
                <?php if($quotesAry['iPriceAddedByAddButton']==1 || $iAddingNewQuote){?>
                <a class="close-button" onclick="deleteBookingQuote('<?php echo $number ?>','<?php echo $idBookingQuote; ?>','<?php echo $idBookingQuotePricing;?>','DISPLAY_DELETE_BOOKING_QUOTE_POPUP');" style="cursor:pointer;">
                   &nbsp;
                </a> 
                <?php }?>
            </span>
    <?php   } ?> 
    </div>
    <?php if($iQuoteReceived==1 || $editable_flag){ ?> <?php } else { ?>
            </div>
    <?php } ?> 
    <?php if($iQuoteReceived==1 || $editable_flag){ ?>
    <div class="<?php echo $szRowContainerClass; ?>">
        <div class="<?php echo $szMainContainerClass; ?> quote-transit-time-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/transit_time')?>:</span>
            <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>"> 
                <?php
                    if($editable_flag)
                    {
                        ?>   
                        <input type="text" name="quotePricingAry[iTransitHours][<?php echo $number; ?>]" id="iTransitHours_<?php echo $number; ?>" value="<?php echo $iTransitHours; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableQuoteSendButton();">
                        <?php
                    }
                    else
                    {
                        echo $szTransitTime;
                        ?>
                        <input type="hidden" name="quotePricingAry[iTransitHours][<?php echo $number; ?>]" id="iTransitHours_<?php echo $number ?>" value="<?php echo $szTransitTime; ?>">
                        <input type="hidden" name="quotePricingAry[szTransitTime][<?php echo $number; ?>]" id="szTransitTime_<?php echo $number ?>" value="<?php echo $szTransitTime; ?>">
                         <?php
                    }
                ?>
            </span>
            <span class="quote-transit-days-label">days</span>
        </div>
        <div class="<?php echo $szMainContainerClass; ?> quote-validity-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/validity')?>:</span>
            <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>"> 
                <?php
                    if($editable_flag)
                    {
                        ?>   
                        <input type="text" name="quotePricingAry[dtQuoteValidTo][<?php echo $number; ?>]" id="dtQuoteValidTo_<?php echo $number; ?>" value="<?php echo $dtQuoteValidTo; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="enableQuoteSendButton();">
                        <?php
                    }
                    else
                    {
                        echo $dtValidity ;
                        ?>
                        <input type="hidden" name="quotePricingAry[dtQuoteValidTo][<?php echo $number; ?>]" id="dtQuoteValidTo_<?php echo $number ?>" value="<?php echo $dtQuoteValidTo; ?>">
                        <input type="hidden" name="quotePricingAry[dtValidity][<?php echo $number; ?>]" id="dtValidity_<?php echo $number ?>" value="<?php echo $dtValidity; ?>">
                         <?php
                    }
                ?>
            </span>
        </div>
        <?php 
        if($iFinancialVersion==1)
        {
            ?>
            <div class="<?php echo $szMainContainerClass; ?> quote-forwarder-vat-container"> 
                <span class="<?php echo $szFirstSpanClass."".$szLevelVatClass; ?>"><?php echo t($t_base.'fields/vat')?>:</span> 
                <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>">  
                    <span id="forwarder_vat_field_container_<?php echo $number; ?>"><?php echo $szTotalVatForwarderPrice; ?></span>
                    <input type="hidden" onkeypress="return format_decimat(this,event)" name="quotePricingAry[fTotalVatForwarder][<?php echo $number; ?>]" id="fTotalVatForwarder_<?php echo $number ?>" value="<?php echo $fTotalVatForwarder; ?>">
                    <input type="hidden" onkeypress="return format_decimat(this,event)" name="quotePricingAry[szTotalVatForwarderPrice][<?php echo $number; ?>]" id="szTotalVatForwarderPrice_<?php echo $number ?>" value="<?php echo $szTotalVatForwarderPrice; ?>"> 
                </span>
            </div>  
            <?php
        }
        else
        { 
         
        /*
         * 
         * Going forward, since we will no longer invoice customer in the name of the forwarder, the forwarder quotes will always be with 0 VAT 
         * As they are effectively selling their transportation services to Transporteca, which is a Hong Kong entity, and hence they should not apply VAT
         * 
        <div class="<?php echo $szMainContainerClass; ?> quote-forwarder-vat-container"> 
            <span class="<?php echo $szFirstSpanClass."".$szLevelVatClass; ?>"><?php echo t($t_base.'fields/vat')?>:</span>
            <?php if($editable_flag){  ?>    
                <span  id="vat_currency_span_<?php echo $number; ?>" class="quote-vat-currency-conatier">&nbsp;</span>
            <?php } ?>
            <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>"> 
                <?php
                    if($editable_flag)
                    {
                        ?>    
                        <input type="text" name="quotePricingAry[fTotalVatForwarder][<?php echo $number; ?>]" style="width:100%;" id="fTotalVatForwarder_<?php echo $number; ?>" value="<?php echo $fTotalVatForwarder; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableQuoteSendButton();update_quote_price('<?php echo $number; ?>');"> 
                        <?php
                    }
                    else
                    { 
                        ?>
                        <span id="forwarder_vat_field_container_<?php echo $number; ?>"><?php echo $szTotalVatForwarderPrice; ?></span>
                        <input type="hidden" onkeypress="return format_decimat(this,event)" name="quotePricingAry[fTotalVatForwarder][<?php echo $number; ?>]" id="fTotalVatForwarder_<?php echo $number ?>" value="<?php echo $fTotalVatForwarder; ?>">
                        <input type="hidden" onkeypress="return format_decimat(this,event)" name="quotePricingAry[szTotalVatForwarderPrice][<?php echo $number; ?>]" id="szTotalVatForwarderPrice_<?php echo $number ?>" value="<?php echo $szTotalVatForwarderPrice; ?>">
                        <?php
                    }
                ?>  
            </span>
        </div>
          */ ?> 
            <input type="hidden" name="quotePricingAry[fTotalVatForwarder][<?php echo $number; ?>]" id="fTotalVatForwarder_<?php echo $number ?>" value="0.00">
    <?php
            }
    ?>
    </div>    
    <div class="<?php echo $szRowContainerClass; ?>">    
        <div class="<?php echo $szMainContainerClass; ?> quote-comment-box"> 
            <span class="<?php echo $szFirstSpanClass." ".$szLevelValue; ?>"><?php echo t($t_base.'fields/comments')?>:</span>
            <?php
                if($editable_flag)
                {
                    ?>   
                    <span class="<?php echo $szSecondSpanClass; ?>"> 
                        <textarea rows="1" name="quotePricingAry[szForwarderComment][<?php echo $number; ?>]" id="szForwarderComment_<?php echo $number; ?>"  onfocus="check_form_field_not_required(this.form.id,this.id);"><?php echo $szForwarderComment; ?></textarea>
                    </span>
                    <?php
                }
                else
                {  
                    /*
                     * If the comments are too long to be shown in the space available, then the last three spaces should be replaced with three periods (…) and at mouseover the entire comments should appear in a textbox as seen below. Currently, the system only allows one line of comments. 
                     * Please allow two lines as shown in the ppt. – Done. As can be seen below it cuts the line in the middle of a word (the word ‘and’). 
                     * Please ensure that line cuts only are made between words. Unless words are longer than, say, 10 characters
                     */ 
                    if(strlen($szForwarderComment)>185)
                    {
                        $szFirstStr = substr($szForwarderComment,0,100) ;
                        $iPosition = strrpos($szFirstStr, " ");
            
                        $iPosition2 = $iPosition ;
                        $szForwarderComment_display = substr($szForwarderComment,0,$iPosition)."<br>".substr($szForwarderComment,$iPosition2,120)."...";
                    }
                    else if(strlen($szForwarderComment)>120)
                    {
                        $szFirstStr = substr($szForwarderComment,0,120) ;
                        $iPosition = strrpos($szFirstStr, " "); 
                        $iPosition2 = $iPosition ; 
                        $szForwarderComment_display = substr($szForwarderComment,0,$iPosition)."<br>".substr($szForwarderComment,$iPosition2) ;
                    }
                    else
                    {
                        $szForwarderComment_display = $szForwarderComment ;
                    }
                    ?>
                    <span class="<?php echo $szSecondSpanClass; ?> label-value"> 		
                        <?php echo $szForwarderComment; ?>
                    </span> 
                    <input type="hidden" name="quotePricingAry[szForwarderComment][<?php echo $number; ?>]" id="szForwarderComment_<?php echo $number ?>" value="<?php echo $szForwarderComment; ?>">
            
                     <?php
                }
            ?> 
        </div> 
    </div>
    </div>
    <?php if($number!=1001){  ?>
    <div class="quote-forwarder-logo" id="quote-forwarder-logo-container-<?php echo $number; ?>"> 
        <?php if(!empty($szForwarderLogoPath)){ ?> 
            <img src="<?php echo __BASE_STORE_INC_URL__.'/image.php?img='.$szForwarderLogoPath."&w=100&h=100" ; ?>">
        <?php } ?> 
    </div>
    <?php } ?> 
    <div class="internal-task-box"> 
        <div class="vertical-text">
            <span>INTERNAL</span>
        </div>
        <?php if(!$editable_flag){ echo "<br>"; } ?>   
        <div class="<?php echo $szRowContainerClass; ?>">
            <div class="<?php echo $szMainContainerClass; ?> quote-customer-price-container">  
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/quote')." (".$szCustomerCurrency.")"; ?>:</span>
                <span class="<?php echo $szSecondSpanClass." ".$szLevelValue; ?>"> 
                    <?php
                        if($editable_flag)
                        {
                            if($subNoManualFee){
                                 ?>   
                            <span id="quote_price_text_container_<?php echo $number; ?>" <?php if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__){ ?> style="display:block" <?php }else{?> style="display:none;" <?php }?>><?php echo $fQuotePriceCustomerCurrency; ?></span>
                            <input type="text" <?php if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__){ ?> style="display:none" <?php }else{?> style="display:block;" <?php }?> name="quotePricingAry[fQuotePriceCustomerCurrency][<?php echo $number; ?>]" id="fQuotePriceCustomerCurrency_<?php echo $number; ?>" value="<?php echo $fQuotePriceCustomerCurrency; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);calculate_gross_profit('<?php echo $number; ?>','PERCENTAGE');enableQuoteSendButton();">
                            <?php
                            }else{
                            ?>   
                            <span id="quote_price_text_container_<?php echo $number; ?>" <?php if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__){ ?> style="display:block" <?php }else{?> style="display:none;" <?php }?>><?php echo $fQuotePriceCustomerCurrency-$fManualFeeCustomerCurrency; ?></span>
                            <input type="text" <?php if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__){ ?> style="display:none" <?php }else{?> style="display:block;" <?php }?> name="quotePricingAry[fQuotePriceCustomerCurrency][<?php echo $number; ?>]" id="fQuotePriceCustomerCurrency_<?php echo $number; ?>" value="<?php echo $fQuotePriceCustomerCurrency-$fManualFeeCustomerCurrency; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);calculate_gross_profit('<?php echo $number; ?>','PERCENTAGE');enableQuoteSendButton();">
                            <?php
                            }
                        }
                        else
                        {?>
                            <span id="quote_price_text_container_<?php echo $number; ?>">
                            <?php
                            if($subNoManualFee){
                                echo number_format((float)($fQuotePriceCustomerCurrency)) ;
                            }else{
                            echo number_format((float)($fQuotePriceCustomerCurrency-$fManualFeeCustomerCurrency)) ;
                            }
                            ?>
                            </span>
                            <input type="hidden" name="quotePricingAry[fQuotePriceCustomerCurrency][<?php echo $number; ?>]" id="fQuotePriceCustomerCurrency_<?php echo $number ?>" value="<?php echo $fQuotePriceCustomerCurrency; ?>">
                             <?php
                        }
                    ?>
                </span>
            </div>
            <div class="<?php echo $szMainContainerClass; ?> quote-gross-profit-container">  
                <span class="<?php echo $szFirstSpanClass; ?>">
                    <div id="gross_profit_title_span_<?php echo $number; ?>">
                    <?php
                        if($iManualFeeApplicable==1 && $iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__)
                        {
                            echo "Referral fee (%): ";
                        }
                        else
                        {
                            echo t($t_base.'fields/gross_profit')." (%): ";
                        }
                    ?> 
                    </div>
                    <span id="gross_profit_type_span_<?php echo $number; ?>"><?php echo $szGrossProfitType; ?></span>
                </span>
                <span class="<?php echo $szSecondSpanClass; ?>" >
                    <input type="text" onkeypress="return format_decimat(this,event)" name="quotePricingAry[fReferalPercentage][<?php echo $number; ?>]" id="fReferalPercentage_<?php echo $number; ?>" value="<?php echo $fReferalPercentage; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);calculate_gross_profit('<?php echo $number; ?>','PERCENTAGE');enableQuoteSendButton();updateEmailBody('<?php echo $number; ?>');"> 
                    <?php if($number==1001){  ?>
                        <span id="actual_gp_percentage_span_<?php echo $number; ?>"><?php echo $fReferalPercentage; ?></span>
                    <?php } ?>
                </span>
            </div>
            <div class="<?php echo $szMainContainerClass; ?> quote-gross-profit-amount-container">  
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/gross_profit')." (".$szCustomerCurrency.")"; ?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?>">
                    <input type="text" onkeypress="return format_decimat(this,event)" name="quotePricingAry[fReferalAmount][<?php echo $number; ?>]" id="fReferalAmount_<?php echo $number; ?>" value="<?php echo $fReferalAmount; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);calculate_gross_profit('<?php echo $number; ?>','REFERAL_AMOUNT');updateEmailBody('<?php echo $number; ?>');enableQuoteSendButton();">
                    <?php if($number==1001){  ?>
                        <span id="actual_gp_value_span_<?php echo $number; ?>"><?php echo $fReferalAmount; ?></span>
                    <?php } ?>
                </span>
            </div> 
        </div>  
       
        <div class="quote-row-container clearfix"  id="idHandling_div_<?php echo $number ?>" <?php if($iProfitType==__FORWARDER_PROFIT_TYPE_MARK_UP__){ ?> style="width:100%;padding:5px;display:none;" <?php }else{?> style="width:100%;padding:5px;" <?php }?>>  
            <span class="quote-field-container wd-12">
                Handling Fee:
            </span>
            <span class="quote-field-container wds-10">
                <select name="quotePricingAry[idHandlingCurrency][<?php echo $number; ?>]" id="idHandlingCurrency_<?php echo $number ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);update_quote_price('<?php echo $number; ?>');enableQuoteSendButton();" onchange="autofill_currency_exhchange_rate(this.value,'<?php echo $number; ?>',1);update_quote_price('<?php echo $number; ?>');">
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($allCurrencyArr))
                        {
                            foreach($allCurrencyArr as $allCurrencyArrs)
                            {
                                ?><option value="<?=$allCurrencyArrs['id']?>" <?php echo ($idHandlingCurrency ==  $allCurrencyArrs['id']) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php
                            }
                        }
                    ?>
                </select>
            </span>
            <span class="quote-field-container wds-10">
                <input type="text" onkeypress="return format_decimat(this,event)" name="quotePricingAry[fTotalHandlingFee][<?php echo $number; ?>]" id="fTotalHandlingFee_<?php echo $number; ?>" value="<?php echo $fTotalHandlingFee; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);update_quote_price('<?php echo $number; ?>');enableQuoteSendButton();"> 
            </span> 
        </div> 
        
        <input type="hidden" name="quotePricingAry[iManualFeeApplicable][<?php echo $number; ?>]" id="iManualFeeApplicable_<?php echo $number ?>" value="<?php echo $iManualFeeApplicable; ?>"> 
        <input type="hidden" name="quotePricingAry[iHandlingFeeApplicable][<?php echo $number; ?>]" id="iHandlingFeeApplicable_<?php echo $number ?>" value="<?php echo $iHandlingFeeApplicable; ?>"> 
        <input type="hidden" name="quotePricingAry[fHandlingCurrencyExchangeRate][<?php echo $number; ?>]" id="fHandlingCurrencyExchangeRate_<?php echo $number ?>" value="<?php echo $fHandlingCurrencyExchangeRate; ?>">
        
<!--    <input type="hidden" name="quotePricingAry[idHandlingCurrency][<?php echo $number; ?>]" id="idHandlingCurrency_<?php echo $number ?>" value="<?php echo $idHandlingCurrency; ?>">
        <input type="hidden" name="quotePricingAry[szHandlingCurrency][<?php echo $number; ?>]" id="szHandlingCurrency_<?php echo $number ?>" value="<?php echo $szHandlingCurrency; ?>">-->
        
<!--    <input type="hidden" name="quotePricingAry[fTotalHandlingFee][<?php echo $number; ?>]" id="fTotalHandlingFee_<?php echo $number ?>" value="<?php echo $fTotalHandlingFee; ?>">
        <input type="hidden" name="quotePricingAry[fTotalHandlingFee][<?php echo $number; ?>]" id="fTotalHandlingFee_<?php echo $number ?>" value="<?php echo $fTotalHandlingFee; ?>">
        <input type="hidden" name="quotePricingAry[fTotalHandlingFeeCustomerCurrency][<?php echo $number; ?>]" id="fTotalHandlingFeeCustomerCurrency_<?php echo $number ?>" value="<?php echo $fTotalHandlingFeeCustomerCurrency; ?>">-->
        
    </div>
    <div class="to-customer-box">
        <div class="vertical-text">
            <span>TO CUSTOMER</span>
        </div> 
        <div class="<?php echo $szRowContainerClass; ?> quote_to_customer_fields_container">
            <div class="<?php echo $szMainContainerClass; ?> quote-customer-price-container wd-18"> 
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/price')." (".$szCustomerCurrency.")"; ?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?> label-value"> 
                    <span id="customer_price_text_container_<?php echo $number; ?>" <?php if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__){ ?> style="display:block" <?php }else{?> style="display:none;" <?php }?>><?php echo number_format((float)($fTotalPriceCustomerCurrency)); ?></span>
                    <!--<input type="hidden" name="quotePricingAry[fTotalPriceCustomerCurrency][<?php echo $number; ?>]" id="fTotalPriceCustomerCurrency_<?php echo $number; ?>" value="<?php echo $fTotalPriceCustomerCurrency; ?>" >--> 
                    <input type="text" <?php if($iProfitType==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__){ ?> style="display:none" <?php }else{?> style="display:block;" <?php }?> onkeypress="return format_decimat(this,event)" name="quotePricingAry[fTotalPriceCustomerCurrency][<?php echo $number; ?>]" id="fTotalPriceCustomerCurrency_<?php echo $number; ?>" value="<?php echo $fTotalPriceCustomerCurrency; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);calculate_gross_profit('<?php echo $number; ?>','PRICE');updateEmailBody('<?php echo $number; ?>');enableQuoteSendButton();"> 
                </span>
            </div>
            <div class="<?php echo $szMainContainerClass; ?> quote-gross-profit-container wds-19">  
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/vat')." (".$szCustomerCurrency.")"; ?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?> label-value">  
                    <span id="customer_vat_price_text_container_<?php echo $number; ?>" style="display:block;"><?php echo $fTotalVat; ?></span>
                    <input type="text" onkeypress="return format_decimat(this,event)" name="quotePricingAry[fTotalVat][<?php echo $number; ?>]" id="fTotalVat_<?php echo $number; ?>" value="<?php echo $fTotalVat; ?>" style="display:none;" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);updateEmailBody('<?php echo $number; ?>');enableQuoteSendButton();">  
                </span>
            </div>
            <div class="<?php echo $szMainContainerClass; ?> quote-gross-profit-amount-container wds-21">  
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/insurance')." (".$szCustomerCurrency.")"; ?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?> label-value" id="insurance_value_container_span_<?php echo $number; ?>">
                    <?php echo $fTotalInsuranceCostForBookingCustomerCurrency; ?>
                </span>
                <input type="hidden" name="quotePricingAry[fTotalInsuranceCostForBookingCustomerCurrency][<?php echo $number; ?>]" id="fTotalInsuranceCostForBookingCustomerCurrency_<?php echo $number; ?>" value="<?php echo $fTotalInsuranceCostForBookingCustomerCurrency; ?>">
                <input type="hidden" name="quotePricingAry[iInsuranceAvailabilityFlag][<?php echo $number; ?>]" id="iInsuranceAvailabilityFlag_<?php echo $number; ?>" value="<?php echo $iTransportationInsuranceAvailable; ?>">
            </div>
            <div class="<?php echo $szMainContainerClass; ?> quote-insurance-cb-container wds-21"> 
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/include_comments'); ?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?>"><input type="checkbox" name="quotePricingAry[iInsuranceComments][<?php echo $number; ?>]" <?php if($iInsuranceComments==1){ ?>checked<?php }?> id="iInsuranceComments_<?php echo $number; ?>" value="1" onclick="updateEmailBody('<?php echo $number; ?>');enableQuoteSendButton();"></span>
            </div>  
            <div class="<?php echo $szMainContainerClass; ?> quote-insurance-cb-container wd-18"> 
                <span class="<?php echo $szFirstSpanClass; ?>">Price Guarantee:</span>
                <span class="<?php echo $szSecondSpanClass; ?>"><input type="checkbox" name="quotePricingAry[iRemovePriceGuarantee][<?php echo $number; ?>]" <?php if((int)$iRemovePriceGuarantee!=2){ ?>checked<?php }?> id="iRemovePriceGuarantee_<?php echo $number; ?>" value="1" onclick="enableQuoteSendButton();"></span>
            </div>
        </div>
    </div>
        
    <input type="hidden" name="quotePricingAry[fReferalPercentageHidden][<?php echo $number; ?>]" id="fReferalPercentageHidden_<?php echo $number; ?>" value="<?php echo $fReferalPercentageHidden; ?>"> 
    <input type="hidden" name="quotePricingAry[fReferalPercentageStandarad][<?php echo $number; ?>]" id="fReferalPercentageStandarad_<?php echo $number; ?>" value="<?php echo $fReferalPercentageStandarad; ?>"> 
    <input type="hidden" name="quotePricingAry[fReferalPercentageHidden2][<?php echo $number; ?>]" id="fReferalPercentageHidden2_<?php echo $number; ?>" value="<?php echo $fReferalPercentage; ?>"> 
    <input type="hidden" name="quotePricingAry[fReferalAmountHidden][<?php echo $number; ?>]" id="fReferalAmountHidden_<?php echo $number; ?>" value="<?php echo $fReferalAmountHidden; ?>">
    <input type="hidden" name="quotePricingAry[fReferalAmountHidden2][<?php echo $number; ?>]" id="fReferalAmountHidden2_<?php echo $number; ?>" value="<?php echo $fReferalAmount; ?>">
    <input type="hidden" name="quotePricingAry[fTotalPriceCustomerCurrencyHidden][<?php echo $number; ?>]" id="fTotalPriceCustomerCurrencyHidden_<?php echo $number; ?>" value="<?php echo $fTotalPriceCustomerCurrencyHidden; ?>">
    <input type="hidden" name="quotePricingAry[fTotalPriceCustomerCurrencyHidden2][<?php echo $number; ?>]" id="fTotalPriceCustomerCurrencyHidden2_<?php echo $number; ?>" value="<?php echo $fTotalPriceCustomerCurrency; ?>">
    <!--<input type="hidden" name="quotePricingAry[fTotalVatHidden][<?php echo $number; ?>]" id="fTotalVatHidden_<?php echo $number; ?>" value="<?php echo $fTotalVatHidden; ?>">-->
    
    <input type="hidden" name="quotePricingAry[fTotalPriceForwarderCurrencyHidden][<?php echo $number; ?>]" id="fTotalPriceForwarderCurrencyHidden_<?php echo $number; ?>" value="<?php echo $fQuotePriceCustomerCurrencyHidden; ?>">
    <input type="hidden" name="quotePricingAry[fTotalPriceForwarderCurrencyHidden2][<?php echo $number; ?>]" id="fTotalPriceForwarderCurrencyHidden2_<?php echo $number; ?>" value="<?php echo $fQuotePriceCustomerCurrency; ?>">
    
    <input type="hidden" name="quotePricingAry[szForwarderLogoPath][<?php echo $number; ?>]" id="szForwarderLogoPath_<?php echo $number; ?>" value="<?php echo $szForwarderLogoPath; ?>">
    
    
    <script type="text/javascript">
        $().ready(function(){  
            <?php if(!$edit_all_fields){ ?>
                var number = '<?php echo $number; ?>';
                toggleTaskQuoteContainer(number);
            <?php } 
                ?>                
        });
    </script>
    <?php  } else { ?> 
<!--        <div class="<?php echo $szRowContainerClass; ?>">
            <div class="<?php echo $szMainContainerClass; ?>"> 
                <span class="<?php echo $szSecondSpanClass; ?>">No Answer</span>
            </div>
            <div class="<?php echo $szMainContainerClass; ?>"> 
                <span class="<?php echo $szSecondSpanClass; ?>">
                    <a href="javascript:void(0);" onclick="display_task_menu_details('REMIND','3')" class="button1"><span>Remind</span></a>
                </span>
            </div>
        </div> -->
    <?php } ?> 
    <input type="hidden" value="<?php echo $iOffLineQuotes; ?>" name="quotePricingAry[iOffLineQuotes][<?php echo $number; ?>]" id="iOffLineQuotes_<?php echo $number; ?>">
    
    <input type="hidden" name="quotePricingAry[idBookingQuote][<?php echo $number; ?>]" id="idBookingQuote_<?php echo $number; ?>" value="<?php echo $idBookingQuote; ?>" >
    <input type="hidden" name="quotePricingAry[idBookingQuotePricing][<?php echo $number; ?>]" id="idBookingQuotePricing_<?php echo $number; ?>" value="<?php echo $idBookingQuotePricing; ?>" >
    <input type="hidden" name="quotePricingAry[iAddingNewQuote][<?php echo $number; ?>]" id="iAddingNewQuote_<?php echo $number; ?>" value="<?php echo $iAddingNewQuote; ?>" >
    <input type="hidden" name="quotePricingAry[iProfitType][<?php echo $number; ?>]" id="iProfitType_<?php echo $number; ?>" value="<?php echo $iProfitType; ?>" >
    <input type="hidden" name="quotePricingAry[fInsuranceValueUpto][<?php echo $number; ?>]" id="fInsuranceValueUpto_<?php echo $number; ?>" value="<?php echo $fInsuranceValueUpto; ?>" >
    
    <input type="hidden" name="quotePricingAry[idInsuranceSellRate][<?php echo $number; ?>]" id="idInsuranceSellRate_<?php echo $number; ?>" value="<?php echo $idInsuranceSellRate; ?>" >
    <input type="hidden" name="quotePricingAry[idInsuranceBuyRate][<?php echo $number; ?>]" id="idInsuranceBuyRate_<?php echo $number; ?>" value="<?php echo $idInsuranceBuyRate; ?>">
    <input type="hidden" name="quotePricingAry[idInsuranceSellCurrency][<?php echo $number; ?>]" id="idInsuranceSellCurrency_<?php echo $number; ?>" value="<?php echo $idInsuranceSellCurrency; ?>" >
    <input type="hidden" name="quotePricingAry[fInsuranceSellCurrencyExchangeRate][<?php echo $number; ?>]" id="fInsuranceSellCurrencyExchangeRate_<?php echo $number; ?>" value="<?php echo $fInsuranceSellCurrencyExchangeRate; ?>" >
  </div> 
    <?php
} 

function display_pending_task_header($kBooking,$idBookingFile)
{
    $szTaskStatus = trim($kBooking->szTaskStatus); 
    $szFileStatus = trim($kBooking->szFileStatus); 
    $iSearchType = trim($kBooking->iSearchType); 
    $iNoReferenceFound = $kBooking->iNoReferenceFound; 
    $idBooking  = $kBooking->idBooking;
      
    if($_SESSION['iShowFileLog_Tab']==1)
    {
        $szFileLog_class = ' active-th';
    }
    else
    {
        if($szTaskStatus=='T1')
        {
            $szValidate_class = ' active-th';
        }
        else if($szTaskStatus=='T2')
        {
            $szAskForQuote_class = ' active-th';
        }
        else if($szTaskStatus=='T3' && $szFileStatus=='S4')
        {
            $szQuote_class = ' active-th';
        }
        else if($szTaskStatus=='T3' && $szFileStatus=='S3')
        {
            $szAskForQuote_class = ' active-th';
        }
        else if($szTaskStatus=='T100' && $szFileStatus=='S2')
        {
            $szEstimate_class = ' active-th';
        }
        else if($szTaskStatus=='T200')
        {
            $szCustomerLog_class = ' active-th';
        } 
        else if($szTaskStatus=='T220')
        { 
            if($iNoReferenceFound>0)
            {
                $szCustomerLog_class = ' active-th';
            } 
            else
            {
                $szFileLog_class = ' active-th';
            } 
        }
        else if($szTaskStatus=='T210')
        {
            if($iNoReferenceFound>0)
            {
                $szCustomerLog_class = ' active-th';
            } 
            else
            {
                $szFileLog_class = ' active-th';
            } 
        } 
        else if($szTaskStatus=='T110' || $szTaskStatus=='T120' || $szTaskStatus=='T160' || $iSearchType==1)
        {
            $szRemind_class = ' active-th';
        }
        else if($szTaskStatus=='T170')
        {
            $szFileLog_class = ' active-th';
        }
        else
        {
            $szValidate_class = ' active-th';
        }
    }
    
    
    $t_base="management/pendingTray/";   
    $fFieldWidth = floor((float)(100/7)); 
    
    $kBooking_new = new cBooking();
    $bValidateScreenFilled = false;
    $bValidateScreenFilled = $kBooking_new->isValidateScreenFilled($idBooking); 
    $bDisableAllFields = false;
    $szDisabledCLass = "";
    if($iNoReferenceFound>0)
    {
        $bDisableAllFields = true;
        $szDisabledCLass = " disabled-tab"; 
    } 
    if($bValidateScreenFilled && !$bDisableAllFields)
    {
        $bEnableRequestPane = true; 
    }
    else
    {
        $szAskForQuote_class = " disabled-tab"; 
        $bEnableRequestPane = false;
    }   
    if($szTaskStatus!='T160' && $bValidateScreenFilled && !$bDisableAllFields)
    {
        $bEnableQuotePane = true; 
    }
    else
    {
        $szQuote_class = " disabled-tab"; 
        $bEnableQuotePane = false;
    }
    ?>
    <div class="task-tabs-heading">
        <table cellpadding="5" cellspacing="0" class="format-6" width="100%" >
            <tr>
                <th style="width:<?php echo $fFieldWidth; ?>%;text-align:center;" class="task-tray-header<?php echo $szValidate_class; ?>" id="task_tray_header_id_VALIDATE" valign="top" ><a href="javascript:void(0);" onclick="display_task_menu_details('VALIDATE','<?php echo $idBookingFile; ?>')" ><?php echo t($t_base.'fields/validate')?></a></th> 
                <th style="width:<?php echo $fFieldWidth; ?>%;text-align:center;" class="task-tray-header<?php echo $szAskForQuote_class."".$szDisabledCLass; ?>" id="task_tray_header_id_ASK_FOR_QUOTE" valign="top"><a href="javascript:void(0);" <?php if($bEnableRequestPane){ ?> onclick="display_task_menu_details('ASK_FOR_QUOTE','<?php echo $idBookingFile; ?>')" <?php } ?>><?php echo t($t_base.'fields/request_quote')?></a></th> 
                <th style="width:<?php echo $fFieldWidth; ?>%;text-align:center;" class="task-tray-header<?php echo $szQuote_class."".$szDisabledCLass; ?>" id="task_tray_header_id_QUOTE" valign="top" ><a href="javascript:void(0)" <?php if($bEnableQuotePane){ ?> onclick="display_task_menu_details('QUOTE','<?php echo $idBookingFile; ?>')" <?php } ?>><?php echo t($t_base.'fields/quote')?></a></th> 
                <th style="width:<?php echo $fFieldWidth; ?>%;text-align:center;" class="task-tray-header<?php echo $szEstimate_class."".$szDisabledCLass; ?>" id="task_tray_header_id_ESTIMATE" valign="top"><a href="javascript:void(0);" <?php if(!$bDisableAllFields){ ?> onclick="display_task_menu_details('ESTIMATE','<?php echo $idBookingFile; ?>')" <?php } ?>><?php echo t($t_base.'fields/estimate')?></a></th>   
                <th style="width:<?php echo $fFieldWidth; ?>%;text-align:center;" class="task-tray-header<?php echo $szRemind_class; ?>" id="task_tray_header_id_REMIND" valign="top"><a href="javascript:void(0)" onclick="display_task_menu_details('REMIND','<?php echo $idBookingFile; ?>')" ><?php echo t($t_base.'fields/remind')?></a></th> 
                <th style="width:<?php echo $fFieldWidth; ?>%;text-align:center;" class="task-tray-header<?php echo $szFileLog_class; ?>" id="task_tray_header_id_LOG" valign="top"><a href="javascript:void(0);" onclick="display_task_menu_details('LOG','<?php echo $idBookingFile; ?>')" ><?php echo t($t_base.'fields/file_log')?></a></th>
                <th style="width:<?php echo $fFieldWidth; ?>%;text-align:center;" class="task-tray-header<?php echo $szCustomerLog_class; ?>" id="task_tray_header_id_CUSTOMER_LOG" valign="top"><a href="javascript:void(0);" onclick="display_task_menu_details('CUSTOMER_LOG','<?php echo $idBookingFile; ?>')"><?php echo t($t_base.'fields/customer_log')?></a></th>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        <?php if($iNoReferenceFound==2){?>
            close_open_div('pending_task_overview_container','pending_task_overview_open_close_link','close');
        <?php } ?>
    </script>
    <?php
}


function display_insurance_options($iTransportationInsuranceAvailable,$iInsuranceIncluded=false)
{
    if($iTransportationInsuranceAvailable==1)
    { 
        ?>
        <select name="updatedTaskAry[iInsuranceIncluded]" id="iInsuranceIncluded" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" onchange="toggleInsuranceFields(this.value);enableAskforquoteButton('pending_task_tray_form');">
            <option value="">Select</option>
            <option value="1" <?php echo (($iInsuranceIncluded==1)?'selected':''); ?>>Yes</option>
            <option value="2" <?php echo (($iInsuranceIncluded==2)?'selected':''); ?>>No</option>
            <option value="3" <?php echo (($iInsuranceIncluded==3)?'selected':''); ?>>Optional</option> 
        </select> 
        <?php
    }
    else
    {
        ?>
        <select name="updatedTaskAry[iInsuranceIncluded]" id="iInsuranceIncluded" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableAskforquoteButton('pending_task_tray_form');" onchange="toggleInsuranceFields(this.value);enableAskforquoteButton('pending_task_tray_form');"> 
            <option value="2" <?php echo (($iInsuranceIncluded==2)?'selected':''); ?>>Not available</option> 
        </select> 
        <?php
    }
}


function display_forwarder_quote($kBooking,$number,$transportModeListAry,$forwarderListAry,$active_flag=false,$quotesAry=array(),$bookingDataArr=array())
{  
    $t_base="management/pendingTray/";    
    $szMainContainerClass="overview-form";
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container";
    $iOffLineFlag = false;
    if(!empty($_POST['askForQuoteAry']))
    {
        $askForQuoteAry = $_POST['askForQuoteAry'] ;
        $idTransportMode = $askForQuoteAry['idTransportMode'][$number];
        $idForwarder = $askForQuoteAry['idForwarder'][$number];
        $szForwarderContact = $askForQuoteAry['szForwarderContact'][$number];
        $idBookingQuote = $askForQuoteAry['idBookingQuote'][$number];
        $iActive = $askForQuoteAry['iActive'][$number];
        $iOffLine = $askForQuoteAry['iOffLine'][$number];
        
        
        if($iOffLine==1)
        {
            $iOffLineFlag = true;
        }
        else
        {
            $iOffLineFlag = false;
        }
        
        if($iActive==1)
        {
            $active_flag = true;
        }
        else
        {
            $active_flag = false;
        }
    }
    else
    {
        if($quotesAry['idTransportMode']>0)
        {
            $idTransportMode = $quotesAry['idTransportMode'];
        }
        else
        {
            $idTransportMode = $kBooking->idTransportMode ;
        }
        $iOffLine=$quotesAry['iOffLine']; 
        $idForwarder = $quotesAry['idForwarder'];
        $szForwarderContact = $quotesAry['szForwarderContact'];
        $idBookingQuote = $quotesAry['id'];
        $iActive = $quotesAry['iActive']; 
    }  
    if(!empty($bookingDataArr) && $idTransportMode>0)
    {
        $searchData = array(); 
        $searchData['idTransportMode'] = $idTransportMode;
        $searchData['idOriginCountry'] = $bookingDataArr['idOriginCountry'];
        $searchData['idDestinationCountry'] = $bookingDataArr['idDestinationCountry'];

        $kConfig = new cConfig();
        $forwarderContactAry = array();
        $forwarderContactAry = $kConfig->getServiceProviderCompanies($searchData);
     
        $numberCheckBox=$number-1; 
        $forwarderListAry = array();
        if(!empty($forwarderContactAry))
        { 
            $kForwarder = new cForwarder();
            $forwarderListAry = $kForwarder->getForwarderArr(true,$forwarderContactAry,true); 
            if($idBookingQuote==0){ 
            $iOffLine = $forwarderListAry[$idForwarder]['iOffLineQuotes']; 
            }
        }
    }  
    $idBookingStatus = $bookingDataArr['idBookingStatus'];
    $iAlreadyPaidBooking = 0;
    if($idBookingStatus==3 || $idBookingStatus==4)
    {
        $iAlreadyPaidBooking = 1;
    }
    $iProfitTypeidForwarderStr='';
    
    if($iOffLine==1)
    {
        $iOffLineFlag = true;
    }
    else
    {
        $iOffLineFlag = false;
    }
    //echo $forwarderListAry[$numberCheckBox]['iProfitType']."iProfitType";
    ?>  
    <div class="from-forwarder-box">
        <div class="quote-row-container clearfix">  
            <div class="<?php echo $szMainContainerClass; ?> quote-mode-container"> 
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/mode')?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?>" > 
                    <select name="askForQuoteAry[idTransportMode][<?php echo $number; ?>]" id="idTransportMode_<?php echo $number; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onchange="prefill_forwarder_contact('<?php echo $number; ?>','CHANGED_MODE');" class="pendingTaskRequestQuote">
                        <option value="">Select</option>
                    <?php
                        if(!empty($transportModeListAry))
                        {
                           foreach($transportModeListAry as $transportModeListArys)
                           {
                            ?>
                            <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($transportModeListArys['id']==$idTransportMode)?'selected':''); ?>> <?php echo $transportModeListArys['szShortName']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                </select>
                </span>
            </div>
            <div class="<?php echo $szMainContainerClass; ?> quote-company-container"> 
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/company')?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?>" id="forwarder_container_span_<?php echo $number; ?>">
                    <select name="askForQuoteAry[idForwarder][<?php echo $number; ?>]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableSendButton();" id="idForwarder_<?php echo $number; ?>" onchange="prefill_forwarder_contact('<?php echo $number; ?>');" >
                    <option value="">Select</option>
                    <?php
                        if(!empty($forwarderListAry))
                        {
                           foreach($forwarderListAry as $forwarderListArys)
                           {
                            ?>
                            <option value="<?php echo $forwarderListArys['id']; ?>" <?php echo (($forwarderListArys['id']==$idForwarder)?'selected':''); ?>><?php echo $forwarderListArys['szForwarderAlias']; ?></option>
                            <?php
                           }
                        }
                    ?> 
                    </select>
                </span>
            </div>
            <div class="<?php echo $szMainContainerClass; ?> quote-contact-container"> 
                <span class="<?php echo $szFirstSpanClass; ?>"><?php echo t($t_base.'fields/contact')?>:</span>
                <span class="<?php echo $szSecondSpanClass; ?>">
                    <input type="text" name="askForQuoteAry[szForwarderContact][<?php echo $number; ?>]" id="szForwarderContact_<?php echo $number; ?>" value="<?php echo $szForwarderContact; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableSendButton();" style="max-width:250px;">
                    <input style="margin-left:15px;" onclick="showHideSubjectBodyDiv('<?php echo $number; ?>');" type="checkbox" name="askForQuoteAry[iOffLine][<?php echo $number; ?>]" <?php if($iOffLineFlag){?> checked <?php } ?> id="iOffLine_<?php echo $number; ?>" value="1">&nbsp;Off-line
                </span>
            </div>
            <span class="quote-cb-container">
                <!--<input class="pendingTaskRequestQuote" type="checkbox" name="askForQuoteAry[iActive][<?php echo $number; ?>]" onclick="toggleQuoteFields('<?php echo $number; ?>');enableSendButton();" <?php if($active_flag){ ?>checked<?php }?> id="iActive_<?php echo $number; ?>" value="1">-->
                <a class="close-button" onclick="remove_booking_quote('add_booking_quote_container_<?php echo ($number-1) ?>','REMOVE_BOOKING_QUOTES','<?php echo $number; ?>','<?php echo $idBookingQuote; ?>');enableSendButton();" style="cursor:pointer;">
                    &nbsp;
                </a> 
            </span>
            <input type="hidden" name="askForQuoteAry[idBookingQuote][<?php echo $number; ?>]" id="idBookingQuote_<?php echo $number; ?>" value="<?php echo $idBookingQuote; ?>" >
            
            <script type="text/javascript">
                $().ready(function(){
                    var number = '<?php echo $number; ?>';
                    //toggleQuoteFields(number);
                    enableSendButton();
                });
            </script>
        </div>
    </div>
    <?php
}  

function display_task_customer_logs_container_backup($kBooking,$iPageNumber=1)
{
    $t_base="management/pendingTray/";    
    $formId = 'pending_task_tray_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    } 
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idOfflineChat = $kBooking->idOfflineChat;
    //echo "chat id: ".$idOfflineChat;
    $kBooking_new = new cBooking(); 
    $kChat = new cChat();
    $postSearchAry = $kBooking_new->getBookingDetails($idBooking);
    $idCustomer = $postSearchAry['idUser'];
            
    $kCrm = new cCRM();
    $kCrm->updateCrmSnoozeFlag();
    $logEmailsAry = array();
    $logEmailsAry = $kBooking_new->getAllFileEmailLogs($idBooking,$postSearchAry['szEmail'],false,false,$idCustomer);
             
    if($postSearchAry['idUser']>0)
    {
        $logReminderAry = array();
        $logReminderAry = $kBooking_new->getAllFileReminderLogs($idBooking,$idBookingFile,true,$postSearchAry['idUser']);
    }
            
    if(!empty($logReminderAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logReminderAry);
    }
    
    $logFileSearchAry = array();
    $logFileSearchAry = $kBooking_new->getAllFileSearchLogs(false,$idCustomer);
     
    if(!empty($logFileSearchAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logFileSearchAry);
    }
    
    $zopimChatLogsAry = array();
    $zopimChatLogsAry = $kChat->getAllZopimChatLogs($idCustomer);
    
    if(!empty($zopimChatLogsAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$zopimChatLogsAry);
    } 
    
    //$iTotalNumEmails = $kBooking_new->getAllFileEmailLogs($idBooking,$postSearchAry['szEmail'],false,true); 
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');  
    $iTotalNumEmails = count($logEmailsAry);
    
    $chunkedEmailAry = array_chunk($logEmailsAry, __CONTENT_PER_PAGE_CUSTOMER_LOG__);
    
    $logEmailsAry = $chunkedEmailAry[$iPageNumber-1]; 
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');  
    require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
    $kPagination  = new Pagination($iTotalNumEmails,__CONTENT_PER_PAGE_CUSTOMER_LOG__,10); 
    
    $standardTextAry = array('BOOKING_INVOICE','BOOKING_NOTICE','BOOKING_CONFIRMATION','BOOKING_INSURANCE_INVOICE');
    $kChat = new cChat();
    ?> 
    <div class="clearfix">
        <div id="booking_quote_main_container_table" class="clearfix"> 
            <?php
                if(!empty($logEmailsAry))
                {
                    foreach($logEmailsAry as $logEmailsArys)
                    { 
                        if(date('I')==1)
                        {  
                            //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
                            $logEmailsArys['dtSentOriginal'] = $logEmailsArys['dtSent'];
                            $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtSent'])));  
                        }
                        else
                        {
                            $logEmailsArys['dtSentOriginal'] = $logEmailsArys['dtSent'];
                            $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtSent'])));  
                        }
                        if($logEmailsArys['iBookingSearch']==1)
                        {
                            ?>
                            <div class="clearfix email-fields-container" style="width:100%;">
                                <span class="quote-field-container wd-10"><strong>From:</strong></span>
                                <span class="quote-field-container wd-40"><?php echo $logEmailsArys['szFromLocation']; ?></span>
                                <span class="quote-field-container wd-10"><strong>To:</strong></span>
                                <span class="quote-field-container wd-40"><?php echo $logEmailsArys['szToLocation']; ?></span>
                            </div>    
                            <?php
                                if($logEmailsArys['iQuickQuote']==1)
                                {
                                    ?>
                                    <div class="clearfix email-fields-container" style="width:100%;">
                                        <span class="quote-field-container wd-10"><strong>Type:</strong></span>
                                        <span class="quote-field-container wd-40"><?php echo "Quick quote by ".$logEmailsArys['szAdminFirstName']." ".$logEmailsArys['szAdminLastName']; ?></span>
                                        <span class="quote-field-container wd-10"><strong>Created:</strong></span>
                                        <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?></span>
                                    </div> 
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <div class="clearfix email-fields-container" style="width:100%;">
                                        <span class="quote-field-container wd-10"><strong>Date & Cargo:</strong></span>
                                        <span class="quote-field-container wd-40"><?php echo date('d. M Y',strtotime($logEmailsArys['dtTimingDate']))." - ".format_volume($logEmailsArys['fCargoVolume'])."cbm / ".number_format((float)$logEmailsArys['fCargoWeight'])."kg"; ?></span>
                                        <span class="quote-field-container wd-10"><strong>Searched:</strong></span>
                                        <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?></span>
                                    </div>  
                                    <?php
                                }
                            ?> 
                            <div class="clearfix email-fields-container" style="width:100%;"><hr></div>
                            <?php
                        }
                        else if($logEmailsArys['iChatLogs']==1)
                        {
                            $bOpenedChatBox = false;
                            if($idOfflineChat>0 && $idOfflineChat==$logEmailsArys['id'])
                            {
                                $bOpenedChatBox = true;
                            }
                            $szDivId = "zopim_chat_transcript_".$logEmailsArys['id'];
                            if($bOpenedChatBox){ ?>
                            <script type="text/javascript">
                                $("#zopim_chat_transcript_"+'<?php echo $logEmailsArys['id']; ?>'+"_link").unbind("click"); 
                                $("#zopim_chat_transcript_"+'<?php echo $logEmailsArys['id']; ?>'+"_link").click(function(){ showHidechatmessage('<?php echo $szDivId; ?>','HIDE'); });
                            </script>
                            <?php } ?> 
                            <div class="clearfix email-fields-container" style="width:100%;">
                                <span class="quote-field-container wd-10"><strong>Chat agent:</strong></span>
                                <span class="quote-field-container wd-40"><?php echo !empty($logEmailsArys['szAgentName'])?$logEmailsArys['szAgentName']:'N/A'; ?></span> 
                                <span class="quote-field-container wd-10"><strong>Date:</strong></span> 
                                <span class="quote-field-container wd-40">
                                    <?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?>
                                    &nbsp;&nbsp;<a href="javascript:void(0)" <?php if(!$bOpenedChatBox){ ?> onclick="display_chat_transcript('<?php echo $logEmailsArys['id']; ?>')" <?php } ?> id="zopim_chat_transcript_<?php echo $logEmailsArys['id']."_link"; ?>">View Detail</a>
                                    &nbsp;&nbsp;<a href="javascript:void(0)" onclick="display_task_menu_details('REMIND','<?php echo $idBookingFile; ?>','DISPLAY_TASK_FORM_MENU','<?php echo __OFFLINE_REMIND_TEMPLATE_ID__; ?>')" id="zopim_chat_transcript_reply_<?php echo $logEmailsArys['id']."_link"; ?>">Reply</a>
                                    &nbsp;&nbsp;<a href="javascript:void(0)" onclick="closeTask('<?php echo $idBookingFile; ?>')" id="zopim_chat_transcript_close_task_<?php echo $logEmailsArys['id']."_link"; ?>">Close task</a>
                                </span>
                            </div> 
                            <div class="clearfix email-fields-container" style="width:100%;" id="zopim_chat_transcript_<?php echo $logEmailsArys['id']; ?>">
                            <?php
                                if($bOpenedChatBox)
                                {
                                    $zopimChatTranscriptAry = array();
                                    $zopimChatTranscriptAry = $kChat->getAllZopimChatMessageLogs($idOfflineChat);

                                    $zopimChatAry = array();
                                    $zopimChatAry = $kChat->getAllZopimChatLogs(false,$idOfflineChat);
 
                                    echo display_zopim_chat_transcript($zopimChatTranscriptAry,$zopimChatAry[0]); 
                                }
                            ?>
                            </div> 
                            <div class="clearfix email-fields-container" style="width:100%;"><hr></div> 
                            <?php
                        }
                        else if($logEmailsArys['iOverviewTask']==1) 
                        {
                            echo display_overview_task_details($logEmailsArys,$kBooking,'PENDING_TRAY_CUSTOMER_LOG');
                        }
                        else if($logEmailsArys['iReminder']==1) 
                        {  
                            $szAdminAddedBy = ""; 
                            if(!empty($logEmailsArys['szAdminName']))
                            {
                                $szAdminAddedBy = " - ".$logEmailsArys['szAdminName'] ;
                            }  
                        ?>
                        <div class="clearfix email-fields-container" style="width:100%;">
                            <span class="quote-field-container wd-10"><strong>Note:</strong></span>
                            <span class="quote-field-container wd-40">
                                <?php if(strlen($logEmailsArys['szEmailBody'])>200){ ?>
                                <span id="display_less_content">
                                        <?php echo substr($logEmailsArys['szEmailBody'],0,197)."..."; ?><a href="javascript:void(0);" onclick="toggleReminderContent('display_full_content','display_less_content');">show more</a>
                                </span>
                                <span id="display_full_content" style="display:none;">
                                    <?php echo nl2br($logEmailsArys['szEmailBody']); ?>  <a href="javascript:void(0);" onclick="toggleReminderContent('display_less_content','display_full_content');">show less</a>
                                </span> 
                                <?php }else { echo nl2br($logEmailsArys['szEmailBody']); } ?> 
                            </span> 
                            <span class="quote-field-container wd-10"><strong>Added: </strong> <br><br> <strong>File: </strong></span>
                            <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])).$szAdminAddedBy; ?><br><br><?php echo $logEmailsArys['szBookingFileNumber']; ?></span>
                        </div>  
                        <div class="clearfix email-fields-container" style="width:100%;"><hr></div> 
                        <?php 
                        } 
                        else if($logEmailsArys['iIncomingEmail']==1) 
                        {  
                            echo display_crm_message_details($logEmailsArys,$kBooking,'PENDING_TRAY_CUSTOMER_LOG');
                        }  
                        else 
                        {
                            echo display_crm_outgoing_emails($logEmailsArys,$kBooking,'PENDING_TRAY_CUSTOMER_LOG');
                        }
                    } 
                }
                else
                {
                    ?>
                    <div class="clearfix email-fields-container" style="width:100%;"><strong>No email sent yet.</strong></div>
                    <?php
                }
            ?> 
        </div> 
        <?php
            if($iTotalNumEmails>0)
            {
                $kPagination->szMode = "DISPLAY_CUSTOMER_LOG_PAGINATION";
                $kPagination->idBookingFile = $idBookingFile;
                $kPagination->paginate('display_customer_log_pagination'); 
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }
            }
	?>
    </div> 
    <?php
}

function display_pending_quotes_overview_pane_courier_admin($bookingAry,$kBooking,$iSuccessCount = false,$disabled_flag=false,$kCourierServices=false)
{ 
    $t_base="management/pendingTray/"; 
     
    $kConfig = new cConfig();
    $kBooking_new = new cBooking();
    
    $taskStatusAry = array();    
    $postSearchAry = array();
    $fileStatusAry = array();
    $cargoDetailsAry = array();
     
    $taskStatusAry = $kConfig->getAllActiveTaskStatus();
    $fileStatusAry = $kConfig->getAllActiveFileStatus();
                     
    $postSearchAry = $bookingAry[1];
    $idBooking = $postSearchAry['idBooking'];
    //$idBookingQuote = $postSearchAry['id'];
    if($postSearchAry['idFile']>0)
    {
        $idBookingFile = $postSearchAry['idFile'];
    }
    else
    {
        $idBookingFile = $postSearchAry['idBookingFile'];
    }
    
    if($postSearchAry['idTransportMode']==4) //courier 
    {
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg / ".number_format((int)$postSearchAry['iNumColli'])."Col";
    }
    else
    {
        $szVolWeight = format_volume($postSearchAry['fCargoVolume'])."cbm / ".number_format((float)$postSearchAry['fCargoWeight'])."kg ";
    }
    $serviceTermsAry = array();
    $serviceTermsAry = $kConfig->getAllServiceTerms($postSearchAry['idServiceTerms']);
    $szServiceTerms = $serviceTermsAry[0]['szShortTerms'];
    
    $fTotalVolume = $postSearchAry['fCargoVolume'];
    $fTotalWeight = $postSearchAry['fCargoWeight']/1000 ;  // convrting weight from kg to meteric ton (mt)

    $cargoDetailsAry = $kBooking_new->getCargoComodityDeailsByBookingId($idBooking); 
    
    $szCargoDescription = '';
    if(!empty($cargoDetailsAry))
    {
        $szCargoDescription = utf8_decode($cargoDetailsAry['1']['szCommodity']); 
    }

    if($postSearchAry['idTransportMode']==4) //courier 
    {
    	if($szCargoDescription!='')
    	{
            $szCargoDescription= $szCargoDescription."/";
    	}
        $szCargoDescription = $szCargoDescription." ".number_format((int)$postSearchAry['iNumColli'])."Col";
    }
    
   
    $szDisabledStr = '';
    if($disabled_flag)
    {
        $szDisabledStr = 'disabled="disabled" ';
    }
    
    if($idBookingFile>0)
    {
        $kBookingFile = new cBooking();
        $kBookingFile->loadFile($idBookingFile);
        $szCommentToForwarder = $kBookingFile->szCommentToForwarder; 
    }
   // print_r($postSearchAry);
    
    $bookingDataArr=$kBooking->getExtendedBookingDetails($postSearchAry['idBooking']);
    //print_r($bookingDataArr);
    $kCourierServices= new cCourierServices();
    $newCourierBookingAry=$kCourierServices->getAllNewBookingWithCourier($postSearchAry['idBooking']);
    $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($postSearchAry['idBooking']);
    
    $courierBookingArr=$kCourierServices->getCourierBookingData($postSearchAry['idBooking']);
    if(empty($_POST['addBookingCourierLabelAry']))
    {
        $_POST['addBookingCourierLabelAry']['szMasterTrackingNumber']=$bookingDataArr['szTrackingNumber'];
        $_POST['addBookingCourierLabelAry']['idCourierProduct']=$bookingDataArr['idServiceProviderProduct']."-".$bookingDataArr['idServiceProvider'];
        $_POST['addBookingCourierLabelAry']['dtCollectionEndTime']=$courierBookingArr[0]['dtCollectionEndTime'];
        $_POST['addBookingCourierLabelAry']['dtCollectionStartTime']=$courierBookingArr[0]['dtCollectionStartTime'];
        $_POST['addBookingCourierLabelAry']['iCollection']=$courierBookingArr[0]['szCollection'];
        $_POST['addBookingCourierLabelAry']['filecount']=$courierBookingArr[0]['iTotalPage'];
        if($courierBookingArr[0]['dtCollection']!='0000-00-00'){ $_POST['addBookingCourierLabelAry']['dtCollection']=date('d/m/Y',strtotime($courierBookingArr[0]['dtCollection']));}
        
        $fileArr=$kCourierServices->getCourierLabelUploadedPdf($postSearchAry['idBooking']);
        $_POST['addBookingCourierLabelAry']['file_name']=$fileArr;
    } 
    
    $cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
    $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',','); 
    $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);                                    
    $total=count($cargoDetailArr);
    if(!empty($cargoDetailArr))
    {
        $szCargoFullDetails="";
        $ctr=0;
        $totalQuantity=0;
        foreach($cargoDetailArr as $cargoDetailArrs)
        { 
            $packingTypeArr=$kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1);
            $totalQuantity=$totalQuantity+$cargoDetailArrs['iColli'];
            $t=$total-1;
               if((int)$cargoDetailArrs['iQuantity']>1)
                    $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szPacking']);
                else
                    $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szSingle']);

                $lenWidthHeightStr='';
                if($cargoDetailArrs['fLength']>0 || $cargoDetailArrs['fWidth']>0 || $cargoDetailArrs['fHeight']>0)
                {
                    $lenWidthHeightStr=round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."".$cargoDetailArrs['cmdes']." (LxWxH), ";
                }
                
                if($ctr==0)
                {
                    $szCargoFullDetails .=$quantityText." ".$szOFText." ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                }
                else
                {
                    $szCargoFullDetails .="<br />".$quantityText." ".$szOFText." ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                }
            ++$ctr;	
        }

        if($totalQuantity>1)
        {
            $textPacking=strtolower($packingTypeArr[0]['szPacking']);
        }
        else
        {
             $textPacking=strtolower($packingTypeArr[0]['szSingle']);   
        }
        
        $szCargoFullDetails .= "<br />Total: ".number_format($totalQuantity)." ".$textPacking.", ".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity; 
    }
    else
    {
        $totalQuantity = $bookingDataArr['iNumColli'];
        $textPacking = 'colli';
        $szCargoFullDetails .= "Total: ".number_format($totalQuantity)." ".$textPacking.", ".$cargo_volume." cbm, ".$cargo_weight." kg, containing ".$szCargoCommodity; 
    }
    
    
    if(!empty($bookingDataArr['dtCutOff']) && $bookingDataArr['dtCutOff']!='0000-00-00 00:00:00')
    {
        $pickUpDate=date('d. F Y',strtotime($bookingDataArr['dtCutOff'])); 
    }
    else
    {
        $pickUpDate = "N/A";
    } 
    $shipperAddress = '';
    if(!empty($bookingDataArr['szShipperCompanyName']))
    {
        $shipperAddress.= html_entities_flag($bookingDataArr['szShipperCompanyName']);
    }  
    if(!empty($bookingDataArr['szShipperAddress_pickup']))
    {
        $shipperAddress.= "<br/> ".html_entities_flag($bookingDataArr['szShipperAddress_pickup']);
    }
    if(!empty($bookingDataArr['szShipperAddress2']))
    {
        $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress2']) ;
    }
    if(!empty($bookingDataArr['szShipperAddress3']))
    {
        $shipperAddress .=", ".html_entities_flag($bookingDataArr['szShipperAddress3']);
    }
    if(!empty($bookingDataArr['szShipperPostCode']) || !empty($bookingDataArr['szShipperCity']))
    {
        if(!empty($bookingDataArr['szShipperPostCode']) && !empty($bookingDataArr['szShipperCity']))
        {
            $shipperAddress.="<br/> ".html_entities_flag($bookingDataArr['szShipperPostCode'])." ".html_entities_flag($bookingDataArr['szShipperCity'])."";
        }
        else if(!empty($bookingDataArr['szShipperPostCode']))
        {
            $shipperAddress.="<br/> ".html_entities_flag($bookingDataArr['szShipperPostCode']);
        }
        else if(!empty($bookingDataArr['szShipperCity']))
        {
            $shipperAddress.="<br/> ".html_entities_flag($bookingDataArr['szShipperCity']);
        }
    }
    if(!empty($bookingDataArr['szShipperCountry']))
    {
          $shipperAddress.=", ".html_entities_flag($bookingDataArr['szShipperCountry']);
    }
    //echo $shipperAddress;
    $ConsigneeAddress = '';
    if(!empty($bookingDataArr['szConsigneeCompanyName']))
    {
        $ConsigneeAddress.= html_entities_flag($bookingDataArr['szConsigneeCompanyName']).", ";
    }  
    if(!empty($bookingDataArr['szConsigneeAddress']))
    {
        $ConsigneeAddress.= "<br/>".html_entities_flag($bookingDataArr['szConsigneeAddress']);
    }
    if(!empty($bookingDataArr['szConsigneeAddress2']))
    {
        $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress2']);
    }
    if(!empty($bookingDataArr['szConsigneeAddress3']))
    {
        $ConsigneeAddress .=", ".html_entities_flag($bookingDataArr['szConsigneeAddress3']);
    }
    if(!empty($bookingDataArr['szConsigneePostCode']) || !empty($bookingDataArr['szConsigneeCity']))
    {
        $ConsigneeAddress.="<br/> ".html_entities_flag($bookingDataArr['szConsigneePostCode'])." ".html_entities_flag($bookingDataArr['szConsigneeCity'])."";
    }
    if(!empty($bookingDataArr['szConsigneeCountry']))
    {
          $ConsigneeAddress.=", ".html_entities_flag($bookingDataArr['szConsigneeCountry']);
    }
     //   echo $ConsigneeAddress."<br/>";

    
    $kConfig = new cConfig();
    $kConfig->loadCountry($bookingDataArr['idShipperCountry']);
    
    $idShipperDialCode=$kConfig->iInternationDialCode;
    
    $kConfig->loadCountry($bookingDataArr['idConsigneeCountry']);
    
    $idConsigneeDialCode=$kConfig->iInternationDialCode;
    ?> 
<div id="courierDetail"> 
    <div class="clearfix accordion-container">
        <span class="accordian"><?=t($t_base.'title/courier_booking_details')?> <a href="javascript:void(0);" onclick="close_open_div('pending_task_overview_courier_container','pending_task_overview_open_close_link','close');" id="pending_task_overview_open_close_link" class="close-icon"></a></span>
        <?php echo display_overview_links($kBookingFile,$bookingDataArr,true);?>
    </div>
    <div id="pending_task_overview_container">
        <div id="pending_task_overview_courier_container">
        <table cellpadding="5" cellspacing="0" class="format-6" width="100%" >
            <tr>
                <td style="width:38%;border-right:none;" valign="top">
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/booking_reference')?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szBookingRef']; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/courier_provider')?>:</span>
                        <span class="field-value"><?php echo (!empty($newCourierBookingAry[0]['szProviderName'])?$newCourierBookingAry[0]['szProviderName']:'N/A'); ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/product'); ?>:</span> 
                        <span class="field-value"><?php echo (!empty($newCourierBookingAry[0]['szProviderProductName'])?$newCourierBookingAry[0]['szProviderProductName']:'N/A'); ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/sales_price'); ?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szCustomerCurrency']." ".number_format((float)$bookingDataArr['fTotalPriceCustomerCurrency'],2) ; ?></span>
                    </div>
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/requested_collection'); ?>:</span>
                        <span class="field-value"><?php echo $pickUpDate; ?></span>
                    </div>
                     
                   </td><td style="width:40%;" valign="top">
                       <div class="overview-detail">
                        <span class="field-name-cargo"><?php echo t($t_base.'fields/cargo_details'); ?>:<br/>
                        <?php echo $szCargoFullDetails ; ?></span>
                    </div>
                     
                </td> 
               </tr>
               <tr><td colspan="2" style="height:1px;background:#bfbfbf;padding:0;"></td></tr>
               <tr>
                   <td style="width:38%;border-right:none;" valign="top">
                        <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/shipper'); ?>:</span>
                             <span class="field-value"><?php echo $shipperAddress ; ?></span>
                        </div> 
                         <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/name'); ?>:</span>
                             <span class="field-value"><?php echo $bookingDataArr['szShipperFirstName']." ".$bookingDataArr['szShipperLastName']; ?></span>
                         </div> 
                         <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/email'); ?>:</span>
                             <span class="field-value"><?php echo $bookingDataArr['szShipperEmail'] ; ?></span>
                         </div>
                        <div class="overview-detail">
                             <span class="field-name"><?php echo t($t_base.'fields/phone_number'); ?>:</span>
                             <span class="field-value"><?php echo "+".$idShipperDialCode." ".$bookingDataArr['szShipperPhone'] ; ?></span>
                         </div>
                   </td>
                   <td style="width:40%;" valign="top">
                        <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/consignee'); ?>:</span>
                        <span class="field-value"><?php echo $ConsigneeAddress ; ?></span>
                    </div> 
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/name'); ?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szConsigneeFirstName']." ".$bookingDataArr['szConsigneeLastName'] ; ?></span>
                    </div> 
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/email'); ?>:</span>
                        <span class="field-value"><?php echo $bookingDataArr['szConsigneeEmail'] ; ?></span>
                    </div> 
                     
                    <div class="overview-detail">
                        <span class="field-name"><?php echo t($t_base.'fields/phone_number'); ?>:</span>
                        <span class="field-value">+<?php echo $idConsigneeDialCode ; ?> <?php echo $bookingDataArr['szConsigneePhone'] ; ?></span>
                    </div> 
                   </td>    
               </tr>
               <?php if($szCommentToForwarder!=''){?>
               <tr>
                    <td colspan="2" style="padding-bottom:0;"><?php echo t($t_base.'fields/comments_from_transporteca')?>:</td>
               </tr>
               <tr>
                    <td colspan="2" style="padding-top:0;"><?php echo $szCommentToForwarder; ?></td>
               </tr>
               <?php }?>
              </table><br/>
            </div>
            <div class="clearfix accordion-container">
                <span class="accordian"><?=t($t_base.'title/upload_shipping_labels_and_tracking_number')?> <a href="javascript:void(0);" onclick="close_open_div('pending_task_overview_courier_form_container','pending_task_overview_form_open_close_link','close');" id="pending_task_overview_form_open_close_link" class="close-icon"></a></span>
            </div>  
            <div id="pending_task_overview_courier_form_container">
            <form enctype="multipart/form-data" action="<?php echo __MANAGEMENT_URL__;?>/test.php" id="forwarder_courier_label_form" name="forwarder_courier_label_form" method="post" action="">
                <div id="forwarder_quote_main_container">
                <?php  
                    echo display_forwarder_quote_form_courier_admin($kBooking,$counter,array(),$disabled_flag,$totalQuantity);
                ?>
                <!--  <div id="add_booking_quote_container_<?php echo $counter; ?>" style="display:none;"></div>-->
                </div> 
                <input type="hidden" id="viewFlag" name="viewFlag" value="admin"/>  
                <input type="hidden" name="addBookingCourierLabelAry[hiddenPosition]" id="hiddenPosition" value="<?php echo $counter?>" />
                <input type="hidden" name="addBookingCourierLabelAry[hiddenPosition1]" id="hiddenPosition1" value="<?php echo $counter?>" />
                <input type="hidden" name="addBookingCourierLabelAry[idBookingQuote]" id="idBookingQuote" value="<?php echo $idBookingQuote?>" />
                <input type="hidden" name="addBookingCourierLabelAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>" />
                <input type="hidden" name="addBookingCourierLabelAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>" /> 
            </form> 
            <?php
            if((int)$iSuccessCount==0 && !$disabled_flag)
            {?>
            <br class="clear-all" /> 
             <div class="btn-container" align="center"> 
                <a href="javascript:void(0);" style="opacity:0.4" class="button1" id="create_admin_label_button" title="Create shipping label"><span><?=t($t_base.'fields/create_label');?></span></a> 
                <a href="javascript:void(0);" id="send_forwarder_quote_save_button" class="button2" title="Save your quote to Transporteca" style="opacity:0.4;"><span><?=t($t_base.'fields/save_data');?></span></a>
                <a href="javascript:void(0);" id="send_forwarder_quote_submit_button" class="button1" title="Submit your quote to Transporteca" style="opacity:0.4;"><span><?=t($t_base.'fields/submit');?></span></a> 
             </div>
             <?php }?>
        </div>
    </div>
</div> 
<?php 
    $formId = 'forwarder_courier_label_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }
    /*
    $szTrackingNumber = "GE329750264NL";
    $szLabelPdfFileName = "label__20161228.pdf"; 
    $kLabel = new cLabels(); 
    $splitedPdfLabelAry = array();
    $splitedPdfLabelAry = $kLabel->downloadSplitedPdfLabelFrom($szLabelPdfFileName,$szTrackingNumber);
    print_R($splitedPdfLabelAry);
     * 
     */
} 

function display_zopim_chat_transcript($zopimChatTranscriptAry,$zopimChatAry,$return_type='HTML')
{ 
    $szChatStringText = '';
    $szChatStringHtml = '';

    $szChatOpeningTag = '<div class="clearfix email-fields-container">';
    $szChatClosingTag = '</div>'; 
    $idZopimChat = $zopimChatAry['id']; 
     
    if(!empty($zopimChatTranscriptAry))
    {
        foreach($zopimChatTranscriptAry as $zopimChatTranscriptArys)
        { 
            $dtChattedOn = ''; 
            $zopimChatTranscriptArys['dtChatedOn'] = getDateCustom($zopimChatTranscriptArys['dtChatedOn']);
            /*
            if(date('I')==1)
            {  
                //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
                $zopimChatTranscriptArys['dtChatedOn'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($zopimChatTranscriptArys['dtChatedOn'])));  
            }
            else
            {
                $zopimChatTranscriptArys['dtChatedOn'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($zopimChatTranscriptArys['dtChatedOn'])));  
            } 
             * 
             */
            $dtChattedOn = date('h:i:s A',strtotime($zopimChatTranscriptArys['dtChatedOn']));
            if($zopimChatTranscriptArys['szType']=='__MEMBER_JOINED__')
            { 
                $szChatStringLogs = "(".$dtChattedOn.") *** ".$zopimChatTranscriptArys['szName']." has joined the chat ***".PHP_EOL;
            }
            else if($zopimChatTranscriptArys['szType']=='__CHAT_MESSAGE__')
            {
                $szChatStringLogs = "(".$dtChattedOn.") ".$zopimChatTranscriptArys['szName'].": ".$zopimChatTranscriptArys['szMessage'].PHP_EOL; 
            }
            else if($zopimChatTranscriptArys['szType']=='__MEMBER_LEFT__')
            {
               $szChatStringLogs = "(".$dtChattedOn.") *** ".$zopimChatTranscriptArys['szName']." has left ***".PHP_EOL;  
            }
            else if($zopimChatTranscriptArys['szType']=='__CHAT_RATING__')
            {
                if(!empty($zopimChatAry['szRating']))
                { 
                    $szChatStringLogs = "(".$dtChattedOn.") ".$zopimChatTranscriptArys['szName']." has rated the chat ".$zopimChatAry['szRating'].PHP_EOL; 
                }
                if(!empty($zopimChatAry['szComments']))
                {
                    $szChatStringLogs = "(".$dtChattedOn.") ".$zopimChatTranscriptArys['szName'].' commented: "'.$zopimChatAry['szComments'].'"'.PHP_EOL; 
                }
            }
            else if($zopimChatTranscriptArys['szType']=='__OFFLINE_MESSAGE__')
            {
                //$szChatStringLogs = "(".$dtChattedOn.") *** ".$zopimChatTranscriptArys['szName']." has joined the chat ***";
                $szChatStringLogs = "(".$dtChattedOn.") ".$zopimChatTranscriptArys['szName'].": ".$zopimChatTranscriptArys['szMessage'].PHP_EOL; 
            }
            $szChatStringText .= $szChatStringLogs;
            $szChatStringHtml .= $szChatOpeningTag. "".$szChatStringLogs."".$szChatClosingTag;
        }
    }
    else
    { 
        $szChatStringText = "No chat history found".PHP_EOL;
        $szChatStringHtml .= $szChatOpeningTag. "".$szChatStringText."".$szChatClosingTag;
    }
    if($return_type=='HTML')
    {
        ?>
        <div id="pending_task_list_container_popup"> 
            <div class="scrolling-div pending-tray-lists chat-transcript-container" style="max-height:300px;"> 
                <?php echo $szChatStringHtml; ?>
            </div>
        </div>
        <span id="customer_log_chat_history_details_<?php echo $idZopimChat; ?>"></span>
        <?php 
    }
    else
    {
        return $szChatStringText; 
    }  
}

function checkalreadyPaidBooking($idBookingStatus,$szBookingTaskStatus='',$szBookingFileStatus='',$iBookingSearchedType='')
{
    $disabalabelStatusAry = array();
    $disabalabelStatusAry[0] = 'S7'; //Payment received
    $disabalabelStatusAry[1] = 'S8'; //Booking sent
    $disabalabelStatusAry[2] = 'S9'; //Booking confirmed
    $disabalabelStatusAry[3] = 'S10'; //Booking advice sent
    $disabalabelStatusAry[4] = 'S130'; //File closed  
    $disabalabelStatusAry[4] = 'S160'; //File closed  
            
    $iAlreadyPaidBooking = 0;  
    if(in_array($szBookingFileStatus,$disabalabelStatusAry)|| $idBookingStatus==3 || $idBookingStatus==4 || $idBookingStatus==7 || $szBookingTaskStatus=='T160' || $iBookingSearchedType==1)
    {
        $iAlreadyPaidBooking = 1;
    } 
    
    return $iAlreadyPaidBooking;
}
function createLabelMailAddressInstructionsPopup($idBooking,$additionalParamsAry)
{
    $kBooking = new cBooking();
    $postSearchAry = array();
    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
    $bDisplayCargoDescField = true;
    $szCargoDescription = utf8_decode($postSearchAry['szCargoDescription']);
    $iDisplayCargoDescField = false;
    $iDisplayIndividualLineCargoDescField = false;
    $iDisplayIndividualLinePieceDescField = false;
    
    if(strlen($szCargoDescription)>=30)
    {
        $iDisplayCargoDescField = 1;
    }
    $szShipperName = $postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName'];
    $szConsigneeName = $postSearchAry['szConsigneeFirstName']." ".$postSearchAry['szConsigneeLastName'];
    
    $postSearchAry['szShipperFullName'] = $szShipperName;        
    $postSearchAry['szConsigneeFullName'] = $szConsigneeName;
    
    if(!empty($additionalParamsAry))
    {
        $postSearchAry['iCollection'] = $additionalParamsAry['iCollection'];
        $postSearchAry['dtCollection'] = $additionalParamsAry['dtCollection'];
        $postSearchAry['dtCollectionEndTime'] = $additionalParamsAry['dtCollectionEndTime'];
        $postSearchAry['dtCollectionStartTime'] = $additionalParamsAry['dtCollectionStartTime']; 
    }
    $kLabels = new cLabels();
    $kLabels->validateLabelInputFields($postSearchAry,true);
     
    $labelValidationErrorAry = array();
    if(!empty($kLabels->labelValidationErrorAry))
    {
        $labelValidationErrorAry = $kLabels->labelValidationErrorAry;
    } 
    
    $kConfig = new cConfig();
    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
    $szDestinationCountryCode=$kConfig->szCountryISO;
    
    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
    $szOriginCountryCode=$kConfig->szCountryISO;
     
    $originCityArr = array();
    $originCityArr = $kLabels->checkCityNameWithTNT($szOriginCountryCode,$postSearchAry['szShipperCity'],$postSearchAry['szShipperPostCode']);
    
    $destinationCityArr = array();
    $destinationCityArr = $kLabels->checkCityNameWithTNT($szDestinationCountryCode,$postSearchAry['szConsigneeCity'],$postSearchAry['szConsigneePostCode']);
    
    $iCheckShipperCityFlag = false;
    $iCheckConsigneeCityFlag=false;
    
    if(!empty($originCityArr))
    {
        $iCheckShipperCityFlag = 1;
    }
    
    if(!empty($destinationCityArr))
    {
        $iCheckConsigneeCityFlag = 1;
    }    
        
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:500px;"> 
            <h3>Shipping Instruction</h3> <br/>
            <?php
           
                if($labelValidationErrorAry['iCollectionTimeError']==1)
                {
                    ?>
                    <div class="clearfix email-fields-container" style="width:95%;"> 
                        <ul><?php echo $kLabels->szLabelApiError; ?></ul>
                    </div> 
                    <div class="btn-container" style="text-align:center;"> 
                        <a href="javascript:void(0);" onclick="showHide('contactPopup')" class="button2"><span>Close</span></a>  
                    </div>
                    <?php
                }
                else
                {
                    $bookingCargoAry = array();
                    $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true); 

                    if(!empty($bookingCargoAry))
                    {
                        foreach($bookingCargoAry as $bookingCargoArys)
                        {
                            if(strlen($bookingCargoArys['szCommodity'])>=30)
                            {
                                $iDisplayIndividualLineCargoDescField = 1;
                                break;
                            } 
                        }
                    } 
                    if(!empty($bookingCargoAry))
                    {
                        foreach($bookingCargoAry as $bookingCargoArys)
                        {
                            if(empty($bookingCargoArys['szSellerReference']))
                            {
                                $bookingCargoArys['szSellerReference'] = $bookingCargoArys['szCommodity'];
                            }
                            if(strlen($bookingCargoArys['szSellerReference'])>=24)
                            {
                                $iDisplayIndividualLinePieceDescField = 1;
                                break;
                            }
                        }
                    }
                     
                    ?>
                    <div class="scrolling-div-overview grey-container-div">
                        <form id="addDeliveryInstructionForm" name="addDeliveryInstructionForm" method="post">
                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                Message to sender <span id="char_counter_span_szMailAddress" style="float: right;">(0/60)</span><br> 
                                <textarea name="addMailAddressInstructionArr[szMailAddress]" rows="1" id="szMailAddress" onkeyup="display_char_counter(this.id,'60');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"></textarea> 
                            </div>
                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                Customer order reference <span id="char_counter_span_szDeliveryInstructions" style="float: right;">(0/60)</span><br> 
                                <textarea name="addMailAddressInstructionArr[szDeliveryInstructions]" rows="1" id="szDeliveryInstructions" onkeyup="display_char_counter(this.id,'60');showHidePieceInfoSection();enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="showHidePieceInfoSection();enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"></textarea> 
                            </div>
                            <?php if(!empty($originCityArr))
                            {?>
                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                 Shipper City
                                 <select id="szShipperCity" class="red_border" name="addMailAddressInstructionArr[szShipperCity]" onchange="showHidePieceInfoSection();enableDeliveryInstructionForm('<?php echo $idBooking; ?>');">
                                     <option value="">Select</option>
                                <?php 
                                    foreach($originCityArr as $originCityArrs)
                                    {?>
                                        <option value="<?php echo $originCityArrs?>" ><?php echo $originCityArrs?></option>
                                       <?php 
                                    }
                                 ?>
                                 </select>                                
                            </div>
                            <?php  } if(!empty($destinationCityArr)) { ?>
                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                 Consignee City
                                 <select id="szConsigneeCity" class="red_border" name="addMailAddressInstructionArr[szConsigneeCity]" onchange="showHidePieceInfoSection();enableDeliveryInstructionForm('<?php echo $idBooking; ?>');">
                                     <option value="">Select</option>
                                <?php 
                                    foreach($destinationCityArr as $destinationCityArrs)
                                    {?>
                                        <option value="<?php echo $destinationCityArrs?>" ><?php echo $destinationCityArrs?></option>
                                       <?php 
                                    }
                                 ?>
                                 </select>                                
                            </div>
                            <?php    
                            }?>
                            <?php
                                if(!empty($labelValidationErrorAry))
                                {
                                    foreach($labelValidationErrorAry as $errorField=>$labelValidationErrorArys)
                                    {
                                        if($labelValidationErrorArys==1 && !empty($errorField))
                                        {
                                            if($errorField=='iDisplayShipperNameField')
                                            {
                                                $szSectionTitle = 'Shipper Name';
                                                $szFieldName = 'szShipperName';
                                                $szFieldValue = $szShipperName;
                                                $iMaxLength = 25;
                                            }
                                            else if($errorField=='iDisplayShipperEmailField')
                                            {
                                                $szSectionTitle = 'Shipper Email';
                                                $szFieldName = 'szShipperEmail';
                                                $szFieldValue = $postSearchAry['szShipperEmail'];
                                                $iMaxLength = 30;
                                            }
                                            else if($errorField=='iDisplayShipperCompanyNameField')
                                            {
                                                $szSectionTitle = 'Shipper Company Name';
                                                $szFieldName = 'szShipperCompanyName';
                                                $szFieldValue = $postSearchAry['szShipperCompanyName'];
                                                $iMaxLength = 50;
                                            }
                                            else if($errorField=='iDisplayShipperAddressField')
                                            {
                                                $szSectionTitle = 'Shipper Address';
                                                $szFieldName = 'szShipperAddress';
                                                $szFieldValue = $postSearchAry['szShipperAddress'];
                                                $iMaxLength = 90;
                                            } 
                                            else if($errorField=='iDisplayShipperPostcodeField')
                                            {
                                                $szSectionTitle = 'Shipper Postcode';
                                                $szFieldName = 'szShipperPostCode';
                                                $szFieldValue = $postSearchAry['szShipperPostCode'];
                                                $iMaxLength = 9;
                                            }
                                            else if($errorField=='iDisplayShipperCityField')
                                            {
                                                $szSectionTitle = 'Shipper City';
                                                $szFieldName = 'szShipperCity';
                                                $szFieldValue = $postSearchAry['szShipperCity'];
                                                $iMaxLength = 30;
                                            }
                                            else if($errorField=='iDisplayConsigneeNameField')
                                            {
                                                $szSectionTitle = 'Consignee Name';
                                                $szFieldName = 'szConsigneeName';
                                                $szFieldValue = $szConsigneeName;
                                                $iMaxLength = 25;
                                            } 
                                            else if($errorField=='iDisplayConsigneeEmailField')
                                            {
                                                $szSectionTitle = 'Consignee Email';
                                                $szFieldName = 'szConsigneeEmail';
                                                $szFieldValue = $postSearchAry['szConsigneeEmail'];
                                                $iMaxLength = 30;
                                            }
                                            else if($errorField=='iDisplayConsigneeCompanyNameField')
                                            {
                                                $szSectionTitle = 'Consignee Company Name';
                                                $szFieldName = 'szConsigneeCompanyName';
                                                $szFieldValue = $postSearchAry['szConsigneeCompanyName'];
                                                $iMaxLength = 50;
                                            }
                                            else if($errorField=='iDisplayConsigneeAddressField')
                                            {
                                                $szSectionTitle = 'Consignee Address';
                                                $szFieldName = 'szConsigneeAddress';
                                                $szFieldValue = $postSearchAry['szConsigneeAddress'];
                                                $iMaxLength = 90;
                                            }
                                            else if($errorField=='iDisplayConsigneePostcodeField')
                                            {
                                                $szSectionTitle = 'Consignee Postcode';
                                                $szFieldName = 'szConsigneePostCode';
                                                $szFieldValue = $postSearchAry['szConsigneePostCode'];
                                                $iMaxLength = 9;
                                            }
                                            else if($errorField=='iDisplayConsigneeCityField')
                                            {
                                                $szSectionTitle = 'Consignee City';
                                                $szFieldName = 'szConsigneeCity';
                                                $szFieldValue = $postSearchAry['szConsigneeCity'];
                                                $iMaxLength = 30;
                                            } 
                                            else
                                            {
                                                continue;
                                            }
                                            ?>
                                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                                <?php echo $szSectionTitle; ?> <span id="char_counter_span_<?php echo $szFieldName; ?>" style="float: right;">(0/<?php echo $iMaxLength; ?>)</span><br> 
                                                <textarea name="labelShipConFields[<?php echo $szFieldName; ?>]" rows="1" id="<?php echo $szFieldName; ?>" onkeyup="display_char_counter(this.id,'<?php echo $iMaxLength; ?>');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"><?php echo $szFieldValue; ?></textarea> 
                                            </div>
                                            <script type="text/javascript">  
                                                display_char_counter('<?php echo $szFieldName; ?>','<?php echo $iMaxLength; ?>');   
                                            </script>
                                            <?php
                                        }
                                    }
                                }
                            ?>    
                            <?php if($iDisplayCargoDescField){ ?>
                                <div class="clearfix email-fields-container" style="width:95%;"> 
                                    Cargo Description <span id="char_counter_span_szCargoDescription" style="float: right;">(0/30)</span><br> 
                                    <textarea name="addMailAddressInstructionArr[szCargoDescription]" rows="1" id="szCargoDescription" onkeyup="display_char_counter(this.id,'30');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"><?php echo $szCargoDescription; ?></textarea> 
                                </div>
                            <?php  } ?> 
                            <?php 
                            if($iDisplayIndividualLineCargoDescField==1)
                            { 
                                if(!empty($bookingCargoAry))
                                {
                                    $ctr = 1;
                                    foreach($bookingCargoAry as $bookingCargoArys)
                                    { 
                                        ?>
                                        <div class="clearfix email-fields-container" style="width:95%;"> 
                                            Cargo Description Line <?php echo $ctr; ?><span id="char_counter_span_szCargoLineDescription_<?php echo $bookingCargoArys['id']; ?>" style="float: right;">(0/30)</span><br> 
                                            <textarea name="addMailAddressInstructionArr[szCargoLineDescription][<?php echo $bookingCargoArys['id']; ?>]" rows="1" id="szCargoLineDescription_<?php echo $bookingCargoArys['id']; ?>" onkeyup="display_char_counter(this.id,'30');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" class="field_name_key_INDIVIUAL_DESCRIPTION"><?php echo $bookingCargoArys['szCommodity']; ?></textarea> 
                                        </div> 
                                        <?php
                                        $ctr++;
                                    }
                                }
                            }
                            ?>
                                            
                            <div id="routing_label_piece_description_container">
                            <?php
                                if(!empty($bookingCargoAry) && $iDisplayIndividualLinePieceDescField==1)
                                {
                                    $ctr = 1;
                                    foreach($bookingCargoAry as $bookingCargoArys)
                                    {
                                        ?>
                                        <div class="clearfix email-fields-container" style="width:95%;" id="szSellerReference_container_<?php echo $bookingCargoArys['idCargo']; ?>"> 
                                            Piece Description Line <?php echo $ctr; ?><span id="char_counter_span_szSellerReference_<?php echo $bookingCargoArys['id']; ?>" style="float: right;">(0/24)</span><br> 
                                            <input type="text" name="addMailAddressInstructionArr[szSellerReference][<?php echo $bookingCargoArys['id']; ?>]" id="szSellerReference_<?php echo $bookingCargoArys['id']; ?>" onkeyup="display_char_counter(this.id,'24');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" class="field_name_key_SELLER_DESCRIPTION" value="<?php echo $bookingCargoArys['szSellerReference']; ?>" /> 
                                            <input type="hidden" name="addMailAddressInstructionArr[szActualSellerReference][<?php echo $bookingCargoArys['id']; ?>]" id="szActualSellerReference_<?php echo $bookingCargoArys['id']; ?>" value="<?php echo $bookingCargoArys['szSellerReference']; ?>" />  
                                            <input type="hidden" name="addMailAddressInstructionArr[iColli][<?php echo $bookingCargoArys['id']; ?>]" id="iColli_<?php echo $bookingCargoArys['id']; ?>" value="<?php echo $bookingCargoArys['iColli']; ?>" /> 
                                        </div>
                                        <?php $ctr++;
                                    }
                                }
                            ?>
                            </div> 
                            <input type="hidden" name="addMailAddressInstructionArr[iCheckShipperCityFlag]" id="iCheckShipperCityFlag" value="<?php echo $iCheckShipperCityFlag; ?>">
                            <input type="hidden" name="addMailAddressInstructionArr[iCheckConsigneeCityFlag]" id="iCheckConsigneeCityFlag" value="<?php echo $iCheckConsigneeCityFlag; ?>">
                            <input type="hidden" name="addMailAddressInstructionArr[iDisplayCargoDescField]" id="iDisplayCargoDescField" value="<?php echo $iDisplayCargoDescField; ?>">
                            <input type="hidden" name="addMailAddressInstructionArr[iDisplayIndividualLineCargoDescField]" id="iDisplayIndividualLineCargoDescField" value="<?php echo $iDisplayIndividualLineCargoDescField; ?>">    
                            <input type="hidden" name="addMailAddressInstructionArr[iDisplayIndividualLinePieceDescField]" id="iDisplayIndividualLinePieceDescField" value="<?php echo$iDisplayIndividualLinePieceDescField; ?>">    

                            <input type="hidden" name="labelErrorFields[iDisplayShipperNameField]" id="iDisplayShipperNameField" value="<?php echo $labelValidationErrorAry['iDisplayShipperNameField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperCompanyNameField]" id="iDisplayShipperCompanyNameField" value="<?php echo $labelValidationErrorAry['iDisplayShipperCompanyNameField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperEmailField]" id="iDisplayShipperEmailField" value="<?php echo $labelValidationErrorAry['iDisplayShipperEmailField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperAddressField]" id="iDisplayShipperAddressField" value="<?php echo $labelValidationErrorAry['iDisplayShipperAddressField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperPostcodeField]" id="iDisplayShipperPostcodeField" value="<?php echo $labelValidationErrorAry['iDisplayShipperPostcodeField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperCityField]" id="iDisplayShipperCityField" value="<?php echo $labelValidationErrorAry['iDisplayShipperCityField']; ?>"> 

                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeNameField]" id="iDisplayConsigneeNameField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeNameField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeCompanyNameField]" id="iDisplayConsigneeCompanyNameField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeCompanyNameField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeEmailField]" id="iDisplayConsigneeEmailField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeEmailField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeAddressField]" id="iDisplayConsigneeAddressField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeAddressField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeCityField]" id="iDisplayConsigneeCityField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeCityField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneePostcodeField]" id="iDisplayConsigneePostcodeField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneePostcodeField']; ?>">

                        </form> 
                    </div>
                    <div class="btn-container" style="text-align:center;"> 
                        <a href="javascript:void(0);" onclick="showHide('contactPopup')" class="button2"><span>Cancel</span></a> 
                        <a href="javascript:void(0);" id="add_shipping_label_instruction_continue_button" class="button1"><span>Continue</span></a> 
                    </div>
                    <script type="text/javascript"> 

                    function validate_label_shipper_consignee_fields()
                    {
                        var iDisplayShipperNameField = $("#iDisplayShipperNameField").val();
                        var iDisplayShipperCompanyNameField = $("#iDisplayShipperCompanyNameField").val();
                        var iDisplayShipperEmailField = $("#iDisplayShipperEmailField").val();
                        var iDisplayShipperAddressField = $("#iDisplayShipperAddressField").val();
                        var iDisplayShipperPostcodeField = $("#iDisplayShipperPostcodeField").val();
                        var iDisplayShipperCityField = $("#iDisplayShipperCityField").val();  

                        var iDisplayConsigneeNameField = $("#iDisplayConsigneeNameField").val();
                        var iDisplayConsigneeCompanyNameField = $("#iDisplayConsigneeCompanyNameField").val();
                        var iDisplayConsigneeEmailField = $("#iDisplayConsigneeEmailField").val();
                        var iDisplayConsigneeAddressField = $("#iDisplayConsigneeAddressField").val();
                        var iDisplayConsigneeCityField = $("#iDisplayConsigneeCityField").val();
                        var iDisplayConsigneePostcodeField = $("#iDisplayConsigneePostcodeField").val(); 
                        var iCheckShipperCityFlag = $("#iCheckShipperCityFlag").val();
                        var iCheckConsigneeCityFlag = $("#iCheckConsigneeCityFlag").val(); 

                        var error_counter = 1;
                        if(iDisplayShipperNameField==1 && validate_length('szShipperName',25))
                        { 
                            error_counter = 2;
                        }
                        else if(iDisplayShipperCompanyNameField==1 && validate_length('szShipperCompanyName',50))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperEmailField==1 && validate_length('szShipperEmail',30))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperAddressField==1 && validate_length('szShipperAddress',90))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperPostcodeField==1 && validate_length('szShipperPostCode',9))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperCityField==1 && validate_length('szShipperCity',30))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeNameField==1 && validate_length('szConsigneeName',25))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeEmailField==1 && validate_length('szConsigneeEmail',30))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeCompanyNameField==1 && validate_length('szConsigneeCompanyName',50))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeAddressField==1 && validate_length('szConsigneeAddress',90))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneePostcodeField==1 && validate_length('szConsigneePostCode',9))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeCityField==1 && validate_length('szConsigneeCity',30))
                        {
                            error_counter = 2;
                        }
                        
                        if(iCheckShipperCityFlag==1)
                        {
                            var szShipperCity = $("#szShipperCity").val();
                            if(szShipperCity!='')
                            {
                                $("#szShipperCity").removeClass("red_border");
                            }
                            else
                            {
                                $("#szShipperCity").addClass("red_border");
                                error_counter = 2;
                            }
                        } 
                        if(iCheckConsigneeCityFlag==1)
                        {
                            var szConsigneeCity = $("#szConsigneeCity").val();
                            if(szConsigneeCity!='')
                            {
                                $("#szConsigneeCity").removeClass("red_border");
                            }
                            else
                            {
                                $("#szConsigneeCity").addClass("red_border");
                                error_counter = 2;
                            }
                        } 
                        return error_counter;
                    } 

                    function validate_length(input_id,iMaxLength)
                    {
                       // console.log("Field: "+input_id+" Max len: "+iMaxLength);
                        var szFieldValue = $("#"+input_id).val();
                        var iLength = countUtf8Bytes(szFieldValue);
                        iLength = parseInt(iLength);
                        iMaxLength = parseInt(iMaxLength);

                        if(iLength>iMaxLength)
                        { 
                            return true;
                        }
                        else
                        { 
                            return false;
                        }
                    }
                    
                    function showHidePieceInfoSection()
                    { 
                        var error_counter = 1;
                        var szDeliveryInstruction = $("#szDeliveryInstructions").val(); 

                        $.each($(".field_name_key_SELLER_DESCRIPTION"), function(field_key){

                            var input_id = this.id; 
                            var SpltSry = input_id.split("szSellerReference_");  
                            var id_cargo = SpltSry[1];
                            
                            var szSellerReference = this.value;
                            var iColli = $("#iColli_"+id_cargo).val();
                            var iMaxLength = 24;
                            if(iColli>1)
                            {
                                iMaxLength = 20;
                            }
                            var szPieceDescription = szSellerReference; 
                            //console.log("Val: "+szPieceDescription+" coli: "+iColli);
                            var iLDesLen = countUtf8Bytes(szPieceDescription);  
                            if(iLDesLen>iMaxLength)
                            {
                                error_counter = 2;
                            }
                        }); 

                        if(error_counter==2)
                        {
                            //prefill_piece_value();
                            $("#iDisplayIndividualLinePieceDescField").val('1');
                            $("#routing_label_piece_description_container").attr('style','display:block;');
                        }
                        else
                        {
                            $("#iDisplayIndividualLinePieceDescField").val('0');
                            $("#routing_label_piece_description_container").attr('style','display:none;');
                        }
                    }
                    
                    function prefill_piece_value()
                    {
                        var szDeliveryInstruction = $("#szDeliveryInstructions").val(); 
                        $.each($(".field_name_key_SELLER_DESCRIPTION"), function(field_key){

                            var input_id = this.id; 
                            var SpltSry = input_id.split("szSellerReference_");  
                            var id_cargo = SpltSry[1];
                            
                            var szSellerReference = $("#szActualSellerReference_"+id_cargo).val(); 
                            var szPieceDescription = szDeliveryInstruction+" "+szSellerReference;  
                            this.value =  szPieceDescription;
                        }); 
                    }

                    $().ready(function(){   
                        display_char_counter("szMailAddress","60"); 
                        display_char_counter("szDeliveryInstructions","60"); 
                        <?php if($iDisplayCargoDescField){?>
                            display_char_counter("szCargoDescription","30"); 
                        <?php } ?> 
                        enableDeliveryInstructionForm('<?php echo $idBooking; ?>');
                    });   
                </script>
                    <?php
                }
            ?> 
        </div> 
    </div> 
   <?php 
}
function createLabelMailAddressInstructionsPopup_backup($idBooking,$additionalParamsAry)
{
    $kBooking = new cBooking();
    $postSearchAry = array();
    $postSearchAry = $kBooking->getExtendedBookingDetails($idBooking);
    $bDisplayCargoDescField = true;
    $szCargoDescription = utf8_decode($postSearchAry['szCargoDescription']);
    $iDisplayCargoDescField = false;
    $iDisplayIndividualLineCargoDescField = false;
    $iDisplayIndividualLinePieceDescField = false;
    
    if(strlen($szCargoDescription)>=30)
    {
        $iDisplayCargoDescField = 1;
    }
    $szShipperName = $postSearchAry['szShipperFirstName']." ".$postSearchAry['szShipperLastName'];
    $szConsigneeName = $postSearchAry['szConsigneeFirstName']." ".$postSearchAry['szConsigneeLastName'];
    
    $postSearchAry['szShipperFullName'] = $szShipperName;        
    $postSearchAry['szConsigneeFullName'] = $szConsigneeName;
    
    if(!empty($additionalParamsAry))
    {
        $postSearchAry['iCollection'] = $additionalParamsAry['iCollection'];
        $postSearchAry['dtCollection'] = $additionalParamsAry['dtCollection'];
        $postSearchAry['dtCollectionEndTime'] = $additionalParamsAry['dtCollectionEndTime'];
        $postSearchAry['dtCollectionStartTime'] = $additionalParamsAry['dtCollectionStartTime']; 
    }
    $kLabels = new cLabels();
    $kLabels->validateLabelInputFields($postSearchAry,true);
     
    $labelValidationErrorAry = array();
    if(!empty($kLabels->labelValidationErrorAry))
    {
        $labelValidationErrorAry = $kLabels->labelValidationErrorAry;
    } 
   
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container"> 
        <div class="popup" style="width:500px;"> 
            <h3>Shipping Instruction</h3> <br/>
            <?php
                if($labelValidationErrorAry['iCollectionTimeError']==1)
                {
                    ?>
                    <div class="clearfix email-fields-container" style="width:95%;"> 
                        <ul><?php echo $kLabels->szLabelApiError; ?></ul>
                    </div> 
                    <div class="btn-container" style="text-align:center;"> 
                        <a href="javascript:void(0);" onclick="showHide('contactPopup')" class="button2"><span>Close</span></a>  
                    </div>
                    <?php
                }
                else
                {
                    $bookingCargoAry = array();
                    $bookingCargoAry = $kBooking->getCargoDeailsByBookingId($idBooking,true); 

                    if(!empty($bookingCargoAry))
                    {
                        foreach($bookingCargoAry as $bookingCargoArys)
                        {
                            if(strlen($bookingCargoArys['szCommodity'])>=30)
                            {
                                $iDisplayIndividualLineCargoDescField = 1;
                                break;
                            } 
                        }
                    } 
                    if(!empty($bookingCargoAry))
                    {
                        foreach($bookingCargoAry as $bookingCargoArys)
                        {
                            if(empty($bookingCargoArys['szSellerReference']))
                            {
                                $bookingCargoArys['szSellerReference'] = $bookingCargoArys['szCommodity'];
                            }
                            if(strlen($bookingCargoArys['szSellerReference'])>=24)
                            {
                                $iDisplayIndividualLinePieceDescField = 1;
                                break;
                            }
                        }
                    }
                     
                    ?>
                    <div class="scrolling-div-overview grey-container-div">
                        <form id="addDeliveryInstructionForm" name="addDeliveryInstructionForm" method="post">
                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                Message to sender <span id="char_counter_span_szMailAddress" style="float: right;">(0/60)</span><br> 
                                <textarea name="addMailAddressInstructionArr[szMailAddress]" rows="1" id="szMailAddress" onkeyup="display_char_counter(this.id,'60');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"></textarea> 
                            </div>
                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                Customer order reference <span id="char_counter_span_szDeliveryInstructions" style="float: right;">(0/60)</span><br> 
                                <textarea name="addMailAddressInstructionArr[szDeliveryInstructions]" rows="1" id="szDeliveryInstructions" onkeyup="display_char_counter(this.id,'60');showHidePieceInfoSection();enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="showHidePieceInfoSection();enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"></textarea> 
                            </div>
                            <?php
                                if(!empty($labelValidationErrorAry))
                                {
                                    foreach($labelValidationErrorAry as $errorField=>$labelValidationErrorArys)
                                    {
                                        if($labelValidationErrorArys==1 && !empty($errorField))
                                        {
                                            if($errorField=='iDisplayShipperNameField')
                                            {
                                                $szSectionTitle = 'Shipper Name';
                                                $szFieldName = 'szShipperName';
                                                $szFieldValue = $szShipperName;
                                                $iMaxLength = 25;
                                            }
                                            else if($errorField=='iDisplayShipperEmailField')
                                            {
                                                $szSectionTitle = 'Shipper Email';
                                                $szFieldName = 'szShipperEmail';
                                                $szFieldValue = $postSearchAry['szShipperEmail'];
                                                $iMaxLength = 30;
                                            }
                                            else if($errorField=='iDisplayShipperCompanyNameField')
                                            {
                                                $szSectionTitle = 'Shipper Company Name';
                                                $szFieldName = 'szShipperCompanyName';
                                                $szFieldValue = $postSearchAry['szShipperCompanyName'];
                                                $iMaxLength = 50;
                                            }
                                            else if($errorField=='iDisplayShipperAddressField')
                                            {
                                                $szSectionTitle = 'Shipper Address';
                                                $szFieldName = 'szShipperAddress';
                                                $szFieldValue = $postSearchAry['szShipperAddress'];
                                                $iMaxLength = 90;
                                            } 
                                            else if($errorField=='iDisplayShipperPostcodeField')
                                            {
                                                $szSectionTitle = 'Shipper Postcode';
                                                $szFieldName = 'szShipperPostCode';
                                                $szFieldValue = $postSearchAry['szShipperPostCode'];
                                                $iMaxLength = 9;
                                            }
                                            else if($errorField=='iDisplayShipperCityField')
                                            {
                                                $szSectionTitle = 'Shipper City';
                                                $szFieldName = 'szShipperCity';
                                                $szFieldValue = $postSearchAry['szShipperCity'];
                                                $iMaxLength = 30;
                                            }
                                            else if($errorField=='iDisplayConsigneeNameField')
                                            {
                                                $szSectionTitle = 'Consignee Name';
                                                $szFieldName = 'szConsigneeName';
                                                $szFieldValue = $szConsigneeName;
                                                $iMaxLength = 25;
                                            } 
                                            else if($errorField=='iDisplayConsigneeEmailField')
                                            {
                                                $szSectionTitle = 'Consignee Email';
                                                $szFieldName = 'szConsigneeEmail';
                                                $szFieldValue = $postSearchAry['szConsigneeEmail'];
                                                $iMaxLength = 30;
                                            }
                                            else if($errorField=='iDisplayConsigneeCompanyNameField')
                                            {
                                                $szSectionTitle = 'Consignee Company Name';
                                                $szFieldName = 'szConsigneeCompanyName';
                                                $szFieldValue = $postSearchAry['szConsigneeCompanyName'];
                                                $iMaxLength = 50;
                                            }
                                            else if($errorField=='iDisplayConsigneeAddressField')
                                            {
                                                $szSectionTitle = 'Consignee Address';
                                                $szFieldName = 'szConsigneeAddress';
                                                $szFieldValue = $postSearchAry['szConsigneeAddress'];
                                                $iMaxLength = 90;
                                            }
                                            else if($errorField=='iDisplayConsigneePostcodeField')
                                            {
                                                $szSectionTitle = 'Consignee Postcode';
                                                $szFieldName = 'szConsigneePostCode';
                                                $szFieldValue = $postSearchAry['szConsigneePostCode'];
                                                $iMaxLength = 9;
                                            }
                                            else if($errorField=='iDisplayConsigneeCityField')
                                            {
                                                $szSectionTitle = 'Consignee City';
                                                $szFieldName = 'szConsigneeCity';
                                                $szFieldValue = $postSearchAry['szConsigneeCity'];
                                                $iMaxLength = 30;
                                            } 
                                            else
                                            {
                                                continue;
                                            }
                                            ?>
                                            <div class="clearfix email-fields-container" style="width:95%;"> 
                                                <?php echo $szSectionTitle; ?> <span id="char_counter_span_<?php echo $szFieldName; ?>" style="float: right;">(0/<?php echo $iMaxLength; ?>)</span><br> 
                                                <textarea name="labelShipConFields[<?php echo $szFieldName; ?>]" rows="1" id="<?php echo $szFieldName; ?>" onkeyup="display_char_counter(this.id,'<?php echo $iMaxLength; ?>');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"><?php echo $szFieldValue; ?></textarea> 
                                            </div>
                                            <script type="text/javascript">  
                                                display_char_counter('<?php echo $szFieldName; ?>','<?php echo $iMaxLength; ?>');   
                                            </script>
                                            <?php
                                        }
                                    }
                                }
                            ?>    
                            <?php if($iDisplayCargoDescField){ ?>
                                <div class="clearfix email-fields-container" style="width:95%;"> 
                                    Cargo Description <span id="char_counter_span_szCargoDescription" style="float: right;">(0/30)</span><br> 
                                    <textarea name="addMailAddressInstructionArr[szCargoDescription]" rows="1" id="szCargoDescription" onkeyup="display_char_counter(this.id,'30');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');"><?php echo $szCargoDescription; ?></textarea> 
                                </div>
                            <?php  } ?> 
                            <?php 
                            if($iDisplayIndividualLineCargoDescField==1)
                            { 
                                if(!empty($bookingCargoAry))
                                {
                                    $ctr = 1;
                                    foreach($bookingCargoAry as $bookingCargoArys)
                                    { 
                                        ?>
                                        <div class="clearfix email-fields-container" style="width:95%;"> 
                                            Cargo Description Line <?php echo $ctr; ?><span id="char_counter_span_szCargoLineDescription_<?php echo $bookingCargoArys['id']; ?>" style="float: right;">(0/30)</span><br> 
                                            <textarea name="addMailAddressInstructionArr[szCargoLineDescription][<?php echo $bookingCargoArys['id']; ?>]" rows="1" id="szCargoLineDescription_<?php echo $bookingCargoArys['id']; ?>" onkeyup="display_char_counter(this.id,'30');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" class="field_name_key_INDIVIUAL_DESCRIPTION"><?php echo $bookingCargoArys['szCommodity']; ?></textarea> 
                                        </div> 
                                        <?php
                                        $ctr++;
                                    }
                                }
                            }
                            ?>
                                            
                            <div id="routing_label_piece_description_container">
                            <?php
                                if(!empty($bookingCargoAry) && $iDisplayIndividualLinePieceDescField==1)
                                {
                                    $ctr = 1;
                                    foreach($bookingCargoAry as $bookingCargoArys)
                                    {
                                        ?>
                                        <div class="clearfix email-fields-container" style="width:95%;" id="szSellerReference_container_<?php echo $bookingCargoArys['idCargo']; ?>"> 
                                            Piece Description Line <?php echo $ctr; ?><span id="char_counter_span_szSellerReference_<?php echo $bookingCargoArys['id']; ?>" style="float: right;">(0/24)</span><br> 
                                            <input type="text" name="addMailAddressInstructionArr[szSellerReference][<?php echo $bookingCargoArys['id']; ?>]" id="szSellerReference_<?php echo $bookingCargoArys['id']; ?>" onkeyup="display_char_counter(this.id,'24');enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" onblur="enableDeliveryInstructionForm('<?php echo $idBooking; ?>');" class="field_name_key_SELLER_DESCRIPTION" value="<?php echo $bookingCargoArys['szSellerReference']; ?>" /> 
                                            <input type="hidden" name="addMailAddressInstructionArr[szActualSellerReference][<?php echo $bookingCargoArys['id']; ?>]" id="szActualSellerReference_<?php echo $bookingCargoArys['id']; ?>" value="<?php echo $bookingCargoArys['szSellerReference']; ?>" />  
                                            <input type="hidden" name="addMailAddressInstructionArr[iColli][<?php echo $bookingCargoArys['id']; ?>]" id="iColli_<?php echo $bookingCargoArys['id']; ?>" value="<?php echo $bookingCargoArys['iColli']; ?>" /> 
                                        </div>
                                        <?php $ctr++;
                                    }
                                }
                            ?>
                            </div>
                            <input type="hidden" name="addMailAddressInstructionArr[iDisplayCargoDescField]" id="iDisplayCargoDescField" value="<?php echo $iDisplayCargoDescField; ?>">
                            <input type="hidden" name="addMailAddressInstructionArr[iDisplayIndividualLineCargoDescField]" id="iDisplayIndividualLineCargoDescField" value="<?php echo $iDisplayIndividualLineCargoDescField; ?>">    
                            <input type="hidden" name="addMailAddressInstructionArr[iDisplayIndividualLinePieceDescField]" id="iDisplayIndividualLinePieceDescField" value="<?php echo$iDisplayIndividualLinePieceDescField; ?>">    

                            <input type="hidden" name="labelErrorFields[iDisplayShipperNameField]" id="iDisplayShipperNameField" value="<?php echo $labelValidationErrorAry['iDisplayShipperNameField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperCompanyNameField]" id="iDisplayShipperCompanyNameField" value="<?php echo $labelValidationErrorAry['iDisplayShipperCompanyNameField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperEmailField]" id="iDisplayShipperEmailField" value="<?php echo $labelValidationErrorAry['iDisplayShipperEmailField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperAddressField]" id="iDisplayShipperAddressField" value="<?php echo $labelValidationErrorAry['iDisplayShipperAddressField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperPostcodeField]" id="iDisplayShipperPostcodeField" value="<?php echo $labelValidationErrorAry['iDisplayShipperPostcodeField']; ?>">
                            <input type="hidden" name="labelErrorFields[iDisplayShipperCityField]" id="iDisplayShipperCityField" value="<?php echo $labelValidationErrorAry['iDisplayShipperCityField']; ?>"> 

                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeNameField]" id="iDisplayConsigneeNameField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeNameField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeCompanyNameField]" id="iDisplayConsigneeCompanyNameField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeCompanyNameField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeEmailField]" id="iDisplayConsigneeEmailField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeEmailField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeAddressField]" id="iDisplayConsigneeAddressField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeAddressField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneeCityField]" id="iDisplayConsigneeCityField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneeCityField']; ?>"> 
                            <input type="hidden" name="labelErrorFields[iDisplayConsigneePostcodeField]" id="iDisplayConsigneePostcodeField" value="<?php echo $labelValidationErrorAry['iDisplayConsigneePostcodeField']; ?>">

                        </form> 
                    </div>
                    <div class="btn-container" style="text-align:center;"> 
                        <a href="javascript:void(0);" onclick="showHide('contactPopup')" class="button2"><span>Cancel</span></a> 
                        <a href="javascript:void(0);" id="add_shipping_label_instruction_continue_button" class="button1"><span>Continue</span></a> 
                    </div>
                    <script type="text/javascript"> 

                    function validate_label_shipper_consignee_fields()
                    {
                        var iDisplayShipperNameField = $("#iDisplayShipperNameField").val();
                        var iDisplayShipperCompanyNameField = $("#iDisplayShipperCompanyNameField").val();
                        var iDisplayShipperEmailField = $("#iDisplayShipperEmailField").val();
                        var iDisplayShipperAddressField = $("#iDisplayShipperAddressField").val();
                        var iDisplayShipperPostcodeField = $("#iDisplayShipperPostcodeField").val();
                        var iDisplayShipperCityField = $("#iDisplayShipperCityField").val();  

                        var iDisplayConsigneeNameField = $("#iDisplayConsigneeNameField").val();
                        var iDisplayConsigneeCompanyNameField = $("#iDisplayConsigneeCompanyNameField").val();
                        var iDisplayConsigneeEmailField = $("#iDisplayConsigneeEmailField").val();
                        var iDisplayConsigneeAddressField = $("#iDisplayConsigneeAddressField").val();
                        var iDisplayConsigneeCityField = $("#iDisplayConsigneeCityField").val();
                        var iDisplayConsigneePostcodeField = $("#iDisplayConsigneePostcodeField").val();

                        var error_counter = 1;
                        if(iDisplayShipperNameField==1 && validate_length('szShipperName',25))
                        { 
                            error_counter = 2;
                        }
                        else if(iDisplayShipperCompanyNameField==1 && validate_length('szShipperCompanyName',50))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperEmailField==1 && validate_length('szShipperEmail',30))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperAddressField==1 && validate_length('szShipperAddress',90))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperPostcodeField==1 && validate_length('szShipperPostCode',9))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayShipperCityField==1 && validate_length('szShipperCity',30))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeNameField==1 && validate_length('szConsigneeName',25))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeEmailField==1 && validate_length('szConsigneeEmail',30))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeCompanyNameField==1 && validate_length('szConsigneeCompanyName',50))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeAddressField==1 && validate_length('szConsigneeAddress',90))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneePostcodeField==1 && validate_length('szConsigneePostCode',9))
                        {
                            error_counter = 2;
                        }
                        else if(iDisplayConsigneeCityField==1 && validate_length('szConsigneeCity',30))
                        {
                            error_counter = 2;
                        }
                        return error_counter;
                    } 

                    function validate_length(input_id,iMaxLength)
                    {
                       // console.log("Field: "+input_id+" Max len: "+iMaxLength);
                        var szFieldValue = $("#"+input_id).val();
                        var iLength = countUtf8Bytes(szFieldValue);
                        iLength = parseInt(iLength);
                        iMaxLength = parseInt(iMaxLength);

                        if(iLength>iMaxLength)
                        { 
                            return true;
                        }
                        else
                        { 
                            return false;
                        }
                    }
                    
                    function showHidePieceInfoSection()
                    { 
                        var error_counter = 1;
                        var szDeliveryInstruction = $("#szDeliveryInstructions").val(); 

                        $.each($(".field_name_key_SELLER_DESCRIPTION"), function(field_key){

                            var input_id = this.id; 
                            var SpltSry = input_id.split("szSellerReference_");  
                            var id_cargo = SpltSry[1];
                            
                            var szSellerReference = this.value;
                            var iColli = $("#iColli_"+id_cargo).val();
                            var iMaxLength = 24;
                            if(iColli>1)
                            {
                                iMaxLength = 20;
                            }
                            var szPieceDescription = szSellerReference; 
                            //console.log("Val: "+szPieceDescription+" coli: "+iColli);
                            var iLDesLen = countUtf8Bytes(szPieceDescription);  
                            if(iLDesLen>iMaxLength)
                            {
                                error_counter = 2;
                            }
                        }); 

                        if(error_counter==2)
                        {
                            //prefill_piece_value();
                            $("#iDisplayIndividualLinePieceDescField").val('1');
                            $("#routing_label_piece_description_container").attr('style','display:block;');
                        }
                        else
                        {
                            $("#iDisplayIndividualLinePieceDescField").val('0');
                            $("#routing_label_piece_description_container").attr('style','display:none;');
                        }
                    }
                    
                    function prefill_piece_value()
                    {
                        var szDeliveryInstruction = $("#szDeliveryInstructions").val(); 
                        $.each($(".field_name_key_SELLER_DESCRIPTION"), function(field_key){

                            var input_id = this.id; 
                            var SpltSry = input_id.split("szSellerReference_");  
                            var id_cargo = SpltSry[1];
                            
                            var szSellerReference = $("#szActualSellerReference_"+id_cargo).val(); 
                            var szPieceDescription = szDeliveryInstruction+" "+szSellerReference;  
                            this.value =  szPieceDescription;
                        }); 
                    }

                    $().ready(function(){   
                        display_char_counter("szMailAddress","60"); 
                        display_char_counter("szDeliveryInstructions","60"); 
                        <?php if($iDisplayCargoDescField){?>
                            display_char_counter("szCargoDescription","30"); 
                        <?php } ?>    
                        enableDeliveryInstructionForm('<?php echo $idBooking; ?>');
                    });   
                </script>
                    <?php
                }
            ?> 
        </div> 
    </div> 
   <?php 
}

function createOffEmailData($idBooking,$idLanguage)
{
        $kBooking = new cBooking();
        $bookingDataArr = array();
        $bookingDataArr =  $kBooking->getBookingDetails($idBooking);
        $shipperConsigneeAry = $kBooking->getBookingDetailsCountryName($idBooking);
        $kConfig = new cConfig();
        $kAdmin = new cAdmin();
        
        $idAdmin = $_SESSION['admin_id']; 
        $kAdmin = new cAdmin();
        $kAdmin->getAdminDetails($idAdmin);
        $szFromEmail = $kAdmin->szEmail;
        $szFromUserName = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
                        
        $bookingDataArr['iBookingLanguage']=$idLanguage;
        //$contactEmailAry = $quotesArys['szForwarderContact'];
        $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage'];
        
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$bookingDataArr['iBookingLanguage']);

        //print_r($configLangArr);
        $szContainingText = $configLangArr[1]['szContaining_txt'] ;
        $szEachText = $configLangArr[1]['szEach_txt'] ;
        $szLWHText = $configLangArr[1]['szLWH_txt'] ;
        $szOFText = $configLangArr[1]['szOF_txt'] ;
        $szColliText = $configLangArr[1]['szColli_txt'] ;

        $szShipperCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$shipperConsigneeAry['idShipperCountry'],false,$replace_ary['iBookingLanguage']);
        $szConsigneeCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$shipperConsigneeAry['idConsigneeCountry'],false,$replace_ary['iBookingLanguage']); 
        $szShipperCountry = $szShipperCountryAry[$shipperConsigneeAry['idShipperCountry']]['szCountryName'] ;
        $szConsigneeCountry = $szConsigneeCountryAry[$shipperConsigneeAry['idConsigneeCountry']]['szCountryName'] ;	 

        $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone;

        $kConfig->loadCountry($kAdmin->idInternationalDialCode);
        $iInternationDialCode = $kConfig->iInternationDialCode;

        $idInternationalDialCode = $bookingDataArr['idCustomerDialCode'];
        if(empty($idInternationalDialCode) && $bookingDataArr['idUser']>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($bookingDataArr['idUser']);
            $idInternationalDialCode = $kUser->idInternationalDialCode;
        } 
        $szCustomerPhoneNumber = $bookingDataArr['szCustomerPhoneNumber'];

        $szBaseUrl_str = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);	
        $szBaseUrl = addhttp(__MAIN_SITE_HOME_PAGE_URL__);

        $szSiteLink = "<a href='".$szBaseUrl."' target='_blank'>".$szBaseUrl_str."</a>";

        $volume =  format_volume($bookingDataArr['fCargoVolume']);
        $weight =  get_formated_cargo_measure($bookingDataArr['fCargoWeight']);

        $volume = round_up($volume,1);
        $weight = round_up($weight,0);

        $replace_ary['fTotalVolume'] = getPriceByLang($volume,$replace_ary['iBookingLanguage'],1)."cbm"; 
        $replace_ary['fTotalWeight'] = getPriceByLang($weight,$replace_ary['iBookingLanguage'],0)."kg"; 

        $serviceTermAry = $kConfig->getAllServiceTerms($bookingDataArr['idServiceTerms']);
        /*$kConfigNew = new cConfig();
        $transportModeListAry = array();
        $transportModeListAry = $kConfigNew->getAllTransportMode(false,$replace_ary['iBookingLanguage'],true,true);
        */
       // $replace_ary['szMode'] = $transportModeListAry[$quotesArys['idTransportMode']]['szShortName'];


        //$replace_ary['szForwarderMail'] = $quotesArys['szForwarderContact'];

        if($shipperConsigneeAry['szShipperPostCode']!=''){
            $replace_ary['szPostcodeShipper'] = $shipperConsigneeAry['szShipperPostCode'].", ";
        }else{
            $replace_ary['szPostcodeShipper'] ='';
        }

        if($shipperConsigneeAry['szConsigneePostCode']!=''){
            $replace_ary['szPostcodeConsignee'] = $shipperConsigneeAry['szConsigneePostCode'].", ";
        }else{
            $replace_ary['szPostcodeConsignee'] ='';
        }


           if($shipperConsigneeAry['szShipperCity']!=''){
            $replace_ary['szCityShipper'] = $shipperConsigneeAry['szShipperCity'].", ";
            }
            else{
                $replace_ary['szCityShipper'] = '';
            }                                    
            if($szShipperCountry!=''){
                $replace_ary['szCountryShipper'] = $szShipperCountry;
            }
            else{
                $replace_ary['szCountryShipper'] = '';
            } 

            if($shipperConsigneeAry['szConsigneeCity']!=''){
                $replace_ary['szCityConsignee'] = $shipperConsigneeAry['szConsigneeCity'].", ";
            }
            else{
                $replace_ary['szCityConsignee'] = '';
            }                                    
            if($szConsigneeCountry!=''){
                $replace_ary['szCountryConsignee'] = $szConsigneeCountry;
            }
            else{
                $replace_ary['szCountryConsignee'] = '';
            }

            $replace_ary['szFromName'] = $replace_ary['szPostcodeShipper']."".$replace_ary['szCityShipper']."".$replace_ary['szCountryShipper'];
            $replace_ary['szToName'] = $replace_ary['szPostcodeConsignee']."".$replace_ary['szCityConsignee']."".$replace_ary['szCountryConsignee'];

        $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($bookingDataArr['id']);
        $total=count($cargoDetailArr);
        $cargo_volume = getPriceByLang($volume,$replace_ary['iBookingLanguage'],1); 
        $cargo_weight = getPriceByLang($weight,$replace_ary['iBookingLanguage'],0); 
        $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);
            if($szCargoCommodity!='')
            {
                $szCargoCommodity = ", ".$szCargoCommodity;
            }
            else if($bookingDataArr['szCargoDescription']!='')
            {
                $szCargoCommodity = ", ".$bookingDataArr['szCargoDescription'];
            }                                    
        $total=count($cargoDetailArr);
        $kCourierServices = new cCourierServices();
        if(!empty($cargoDetailArr))
        {
            $szCargoFullDetails="";
            $ctr=0;
            $totalQuantity=0;
            foreach($cargoDetailArr as $cargoDetailArrs)
            {

                $totalQuantity=$totalQuantity+$cargoDetailArrs['iColli'];
                $t=$total-1;
                if($bookingDataArr['iSearchMiniVersion']==7 || $bookingDataArr['iSearchMiniVersion']==8 || $bookingDataArr['iSearchMiniVersion']==9)
                {
                    
                    $szVogaPackingType = ($cargoDetailArrs['szCommodity']);
                    $t=$total-1;  
                    $quantityText = number_format((int)$cargoDetailArrs['iQuantity'])." ".$szVogaPackingType;

                    $lenWidthHeightStr='';
                    if($cargoDetailArrs['fLength']>0 || $cargoDetailArrs['fWidth']>0 || $cargoDetailArrs['fHeight']>0)
                    {
                        $lenWidthHeightStr=round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."".$cargoDetailArrs['cmdes']." ".$szLWHText.", ";
                    }
                    if($ctr==0)
                    {
                        $szCargoFullDetails .=$quantityText." ".$szOFText." ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." ".$szEachText;
                    }
                    else
                    {
                        $szCargoFullDetails .="<br />".$quantityText." ".$szOFText." ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." ".$szEachText; 
                    }
                }
                else
                {
                    
                    if($cargoDetailArrs['szCommodity']!='')
                    {
                        
                        $packingTypeArr[0]['szPacking']=$cargoDetailArrs['szCommodity'];
                        $packingTypeArr[0]['szSingle']=$cargoDetailArrs['szCommodity'];
                    }
                    else
                    {
                        if((int)$bookingDataArr['idCourierPackingType']>0)
                        {
                            $packingTypeArr=$kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1);
                        }
                        else
                        {
                            $packingTypeArr[0]['szPacking']=$szColliText;
                            $packingTypeArr[0]['szSingle']=$szColliText;
                        }
                    }     
                   if((int)$cargoDetailArrs['iQuantity']>1)
                        $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szPacking']);
                    else
                        $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szSingle']);

                    $lenWidthHeightStr='';
                    if($cargoDetailArrs['fLength']>0 || $cargoDetailArrs['fWidth']>0 || $cargoDetailArrs['fHeight']>0)
                    {
                        $lenWidthHeightStr=round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."".$cargoDetailArrs['cmdes']." ".$szLWHText.", ";
                    }
                    if($ctr==0)
                    {
                        $szCargoFullDetails .=$quantityText." ".$szOFText." ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." ".$szEachText;
                    }
                    else
                    {
                        $szCargoFullDetails .="<br />".$quantityText." ".$szOFText." ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." ".$szEachText; 
                    }
                }
                ++$ctr;	
            }
             $textPacking='';
            if($totalQuantity>1)
            {
                $textPacking=number_format($totalQuantity)." ".strtolower($szColliText).", ";
            }
            else
            {
                 $textPacking=number_format($totalQuantity)." ".strtolower($szColliText).", ";   
            }

            $szCargoFullDetails .= "<br />Total: ".$textPacking."".$cargo_volume." cbm, ".$cargo_weight." kg".$szCargoCommodity; 
        }
        else
        {
            $totalQuantity = $bookingDataArr['iNumColli'];
            $textPacking='';
            if($totalQuantity>1)
            {
                $textPacking = $szColliText;
                $textPacking=number_format($totalQuantity)." ".strtolower($textPacking).", ";
            }

            $szCargoFullDetails .= "Total: ".$textPacking."".$cargo_volume." cbm, ".$cargo_weight." kg".$szCargoCommodity; 
        }    
        $replace_ary['szTerms'] = $serviceTermAry[0]['szShortTerms'];
        $replace_ary['szCargoDetail'] = $szCargoFullDetails;
        $replace_ary['szHandoverCity']= $bookingDataArr['szHandoverCity'];
        if(!empty($bookingDataArr['dtTimingDate']) && $bookingDataArr['dtTimingDate']!='0000-00-00 00:00:00')
        {
            $replace_ary['szCargoReady'] = date('d/m/Y',strtotime($bookingDataArr['dtTimingDate']));            
        }
        if(!empty($bookingDataArr['dtShipmentDate']) && $bookingDataArr['dtShipmentDate']!='0000-00-00 00:00:00')
        {
            $replace_ary['dtBooking'] = date('d/m/Y',strtotime($bookingDataArr['dtShipmentDate']));
        }
         $kWhsSearch=new cWHSSearch();
        $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$replace_ary['iBookingLanguage']);

        $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
        $replace_ary['szOfficePhone'] = $szCustomerCareNumer;
        $replace_ary['szMobile'] = $szMobile;
        $replace_ary['szWebSiteUrl'] = $szBaseUrl_str;
        $replace_ary['idBooking'] = $bookingDataArr['id'];
        $replace_ary['iAdminFlag'] = 1;
        
       // $emailMessageAry = array(); 
        //$emailMessageAry = createEmailMessage('__OFF_LINE_FORWARDER_NEW_PRICE_REQUEST__', $replace_ary);
        
        $buildTemplateAry = array();
        $buildTemplateAry = $replace_ary;
        $buildTemplateAry['szEmailTemplate'] = '__OFF_LINE_FORWARDER_NEW_PRICE_REQUEST__';
        
        $emailTemplateAry = array(); 
        $kSendEmail = new cSendEmail();
        $emailTemplateAry = $kSendEmail->createEmailTemplate($buildTemplateAry);
        
        
        $message = $emailTemplateAry['szEmailBody'];

        $subject = $emailTemplateAry['szEmailSubject'];
        
        if (count($replace_ary) > 0)
        {
            foreach ($replace_ary as $replace_key => $replace_value)
            {
                $message = str_replace($replace_key, $replace_value, $message);
                $subject= str_replace($replace_key, $replace_value, $subject);
            }
        }
        
        $mailBasicDetailsAry['szEmailSubject'] = $subject;
        $mailBasicDetailsAry['szEmailMessage'] = $message;
        
        return $mailBasicDetailsAry;
}
?>
