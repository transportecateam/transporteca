<?php
if( !isset( $_SESSION ) )
{
    session_start();
}
/**
 * This file contains the functions that are generic for the store and can be used by all the php files in the store website.
 *
 * functiions.php
 * @copyright Copyright (C) 2012 Trnasporteca, LLC
 * @author Anil
 * @package Trnasporteca Development
 */
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/pdf_functions.php" ); 
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH__ . "/inc/pendingTray_functions.php" );
require_once( __APP_PATH__ . "/inc/content_functions.php" );
require_once( __APP_PATH__ . "/inc/quick_quote_functions.php" );
require_once( __APP_PATH__ . "/inc/logs_functions.php" );

include_classes();
function showCompanyUserDetails($forwarderContactAry,$kForwardersContact,$idForwarder)
{
    $t_base = "ForwardersCompany/UserAccess/";
    ?>
    <div class="hsbody-2-right">
        <h4><strong><?=t($t_base.'titles/admin_profile');?></strong></h4>
            <p><?=t($t_base.'titles/admin_pref_text');?></p>
				<br>	
				<div class="oh">
				<?php
					if(!empty($forwarderContactAry))
					{
						$ctr=0;
						foreach($forwarderContactAry as $forwarderContactArys)
						{
							$name = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
							?>
							<div class="profile-box">
								<p class="title"><?=strlen($name)<=22?$name:mb_substr($name,0,20,'UTF8').'..'?></p>
								<p class="email"><span title="E-mail address">&nbsp;</span><?=strlen($forwarderContactArys['szEmail'])<=17?$forwarderContactArys['szEmail']:mb_substr($forwarderContactArys['szEmail'],0,15,'UTF8').".."?></p>
								<p class="date-setting">
								    <span class="padlock" title="Last login">&nbsp;</span>
									<span class="date"  title="Last login">
                                                                            <?php
										if($forwarderContactArys['iConfirmed']>0)
										{
											if(!empty($forwarderContactArys['dtLastLogin']) && ($forwarderContactArys['dtLastLogin']!='0000-00-00 00:00:00'))
											{
												echo date('d/m/Y',strtotime($forwarderContactArys['dtLastLogin']));
											}
											else
											{
												echo 'N/A';
											}
										}
										else
										{
											echo 'Pending';
										}
									
									?></span>
									<a href="javascript:void(0)"  title="Delete profile" alt="Delete profile" onclick="delete_forwarder_contacts('<?=$forwarderContactArys['id']?>','<?=$idForwarder?>')" class="delete">&nbsp;</a><a href="javascript:void(0)" alt="Edit profile" title="Edit profile" onclick="update_forwarder_users('<?=$forwarderContactArys['id']?>','<?=$idForwarder?>');" class="settings">&nbsp;</a>
								</p>
							</div>
					<?php
							$ctr++;
							if($ctr%4==0)
							{
								echo '</div><div class="oh">';
							}
						}
					}
				?>
					<div class="profile-box-link">
						<p><br /><a href="javascript:void(0)" onclick="addNewUer(1,'<?=$idForwarder?>')"><?=t($t_base.'titles/add_new_admin_profile');?></a></p>
					</div>
				</div>
				
				<br />
				
				<h4><strong><?=t($t_base.'titles/pricing_services');?></strong></h4>
				<p><?=t($t_base.'titles/pricing_services_msg');?></p>
				<br>
				<div class="oh">
				<?
					$idRoleAry = array();
					$idRoleAry[0]=3 ;
					$forwarderContactAry = $kForwardersContact->getAllForwardersContact($idForwarder,$idRoleAry);
					if(!empty($forwarderContactAry))
					{
						$ctr=0;
						foreach($forwarderContactAry as $forwarderContactArys)
						{
							$name = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
							?>
							<div class="profile-box" id="forwarder_contact_<?=$forwarderContactArys['id']?>">
								<p class="title"><?=strlen($name)<=22?$name:mb_substr($name,0,20,'UTF8').'..'?></p>
								<p class="email" alt="E-mail address"><span title="E-mail address">&nbsp;</span><?=strlen($forwarderContactArys['szEmail'])<=17?$forwarderContactArys['szEmail']:mb_substr($forwarderContactArys['szEmail'],0,15,'UTF8').".."?></p>
								<p class="date-setting">
									<span class="padlock" title="Last login">&nbsp;</span>
									<span class="date" alt="Last login"><?
										if($forwarderContactArys['iConfirmed']>0)
										{
											if(!empty($forwarderContactArys['dtLastLogin']) && ($forwarderContactArys['dtLastLogin']!='0000-00-00 00:00:00'))
											{
												echo date('d/m/Y',strtotime($forwarderContactArys['dtLastLogin']));
											}
											else
											{
												echo 'N/A';
											}
										}
										else
										{
											echo 'Pending';
										}
									
									?></span>
									<a href="javascript:void(0)" title="Delete profile" alt="Delete profile" onclick="delete_forwarder_contacts('<?=$forwarderContactArys['id']?>','<?=$idForwarder?>');" class="delete">&nbsp;</a>
									<a href="javascript:void(0)" title="Edit profile" alt="Edit profile" onclick="update_forwarder_users('<?=$forwarderContactArys['id']?>',<?=$idForwarder?>);" class="settings">&nbsp;</a>
								</p>
							</div>
							<?
							$ctr++;
							if($ctr%4==0)
							{
								echo '</div><div class="oh">';
							}
						}
					}
				?>
					<div class="profile-box-link">
						<p><br /><a href="javascript:void(0)" onclick="addNewUer(3,'<?=$idForwarder?>')"><?=t($t_base.'titles/add_new_pricing_services');?></a></p>
					</div>
				</div>
				<br />
				<h4><strong><?=t($t_base.'titles/booking_billing_profile');?></strong></h4>	
				<p><?=t($t_base.'titles/booking_billing_profile_msg');?></p>
				<br>
				<div class="oh">
					<?
					$idRoleAry = array();
					$idRoleAry[0]=5 ;
					$forwarderContactAry = $kForwardersContact->getAllForwardersContact($idForwarder,$idRoleAry);
					if(!empty($forwarderContactAry))
					{
						$ctr=0;
						foreach($forwarderContactAry as $forwarderContactArys)
						{
							$name = $forwarderContactArys['szFirstName']." ".$forwarderContactArys['szLastName'] ;
							?>
							<div class="profile-box" id="forwarder_contact_<?=$forwarderContactArys['id']?>">
								<p class="title"><?=strlen($name)<=22?$name:mb_substr($name,0,20,'UTF8').'..'?></p>
								<p class="email" alt="E-mail address"><span title="E-mail address">&nbsp;</span><?=strlen($forwarderContactArys['szEmail'])<=15?$forwarderContactArys['szEmail']:mb_substr($forwarderContactArys['szEmail'],0,13,'UTF8').".."?></p>
								<p class="date-setting">
									<span class="padlock" title="Last login">&nbsp;</span>
									<span class="date" alt="Last login"><?
										if($forwarderContactArys['iConfirmed']>0)
										{
											if(!empty($forwarderContactArys['dtLastLogin']) && ($forwarderContactArys['dtLastLogin']!='0000-00-00 00:00:00'))
											{
												echo date('d/m/Y',strtotime($forwarderContactArys['dtLastLogin']));
											}
											else
											{
												echo 'N/A';
											}
										}
										else
										{
											echo 'Pending';
										}
									?></span>
									<a href="javascript:void(0)" title="Delete profile" alt="delete profile" onclick="delete_forwarder_contacts('<?=$forwarderContactArys['id']?>','<?=$idForwarder?>')" class="delete">&nbsp;</a>
									<a href="javascript:void(0)" title="Edit profile" alt="Edit profile" onclick="update_forwarder_users('<?=$forwarderContactArys['id']?>','<?=$idForwarder?>');"  class="settings">&nbsp;</a>
								</p>
							</div>
							<?
							$ctr++;
							if($ctr%4==0)
							{
								echo '</div><div class="oh">';
							}
						}
					}
				?>
					<div class="profile-box-link">
						<p><br /><a href="javascript:void(0)" onclick="addNewUer(5,'<?=$idForwarder?>')"><?=t($t_base.'titles/add_new_booking_billing_profile');?></a></p>
					</div>
				</div>
	</div>
<?				
}
function showSettingAndPricing($t_base,$forwarderCP2)
{
?>		
<div class="hsbody-2-right">
<form>
	<h4 style="margin-top:5px;"><strong><?=t($t_base.'title/admin');?>  <?=t($t_base.'title/cannot_by_forwarder');?></strong></h4>							
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/codeForBooking');?></span>
			<span class="field-container"><input style="width:60px;" type="text" name="updatePrefControlArr[BookingRef]" value='<?=$forwarderCP2['szForwarderAlias']?>' > <?=t($t_base.'fields/two_cap_letter');?></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/urlDns');?></span>
			<span class="field-container" style="width:50%;" ><input style="width:181px;" type="text" name="updatePrefControlArr[dnsURL]" value=<?=$controlPanel;?>> <?=t($t_base.'fields/tenasporetca_com');?></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/referalFee');?></span>
			<span class="field-container"><input style="width:58px;" type="text" name="updatePrefControlArr[RefFee]" value='<?=$forwarderCP2['fReferalFee']?>'> <?=t($t_base.'fields/pctg_of_invoice_value');?></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/validto');?></span>
			<span class="field-container"><input type="text" style="width: 100px;" id="datepicker1" onblur="show_me(this.id,'dd/mm/yyyy')" onfocus="blank_me(this.id,'dd/mm/yyyy')" name="updatePrefControlArr[dtValidTo]" value='<?=(($forwarderCP2['dtReferealFeeValid']!='0000-00-00 00:00:00' && $forwarderCP2['dtReferealFeeValid']!=''  )?date('d/m/Y',strtotime($forwarderCP2['dtReferealFeeValid'])):date('d/m/Y'))?>'></span>
		</div>
		<div class="profile-fields">
			<span class="field-name"><?=t($t_base.'fields/automatic_pay');?></span>
			<span class="field-container">
			<select name="updatePrefControlArr[iAutomaticPaymentInvoicing]">
			<option value="1" <? echo ($forwarderCP2['iAutomaticPaymentInvoicing']?"selected=selected":'')?>>Yes</option>
			<option value="0" <? echo ($forwarderCP2['iAutomaticPaymentInvoicing']?'':"selected=selected")?>>No</option>
			</select>
		</div>
		<div class="profile-fields">
			<p><?=t($t_base.'fields/comments_for_pricing');?></p><br />
			<span class="field-container"><textarea name="updatePrefControlArr[szComment]" rows="10" cols="70" id="szCommentText" maxlength="400" onkeyup="checkLimit(this.id)"><?=str_replace('<br />',"",$forwarderCP2['szReferalFeeComment'])?></textarea></span>
		</div>
			<input type="hidden" name="flag" value="UPDATE_FORWARDER_PREFERENCES">							
			<input type="hidden" name="idForwarder" value="<?=$idForwarder;?>">	
		<div style="clear: both;"></div>
		<span style="font: italic;float: right;">You have <span id="char_left">400</span> characters left to write</span>			
		<p align="center"><br/><br/>
			<a href="javascript:void(0);" onclick="showDifferentProfile('forwarderComp');"  class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
			<a href="javascript:void(0);" onclick="submit_preference_controls_del()"  class="button1"><span><?=t($t_base.'fields/save');?></span></a>	
		</p>
	
</form>	
</div>
<?php
}
function viewSettingAndPricing($t_base,$forwarderCP2,$idForwarder)
{   
    if($forwarderCP2['iProfitType']==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__)
    {
        $szProfitType = __FORWARDER_PROFIT_TYPE_REFERRAL_FEE_TEXT__ ;
    }
    else if($forwarderCP2['iProfitType']==__FORWARDER_PROFIT_TYPE_MARK_UP__)
    {
        $szProfitType = __FORWARDER_PROFIT_TYPE_MARK_UP_TEXT__ ;
    } 
?>		
<div class="hsbody-2-right">
<div id="setting">
	<h4><strong><?=t($t_base.'title/settings');?> </strong> <a href="javascript:void(0);" onclick="setting_pricing(<?=$idForwarder?>,'setting','EDIT_SETTING')"><?=t($t_base.'fields/edit');?></a></h4>							
		<div class="ui-fields">
			<span class="fl-30"><?=t($t_base.'fields/codeForBooking');?></span>
			<span class="field-container"><?=$forwarderCP2['szForwarderAlias']?></span>
		</div>
		<div class="ui-fields">
			<span class="fl-30"><?=t($t_base.'fields/urlDns');?></span>
			<span class="field-container" style="width:50%;" ><?=$forwarderCP2['szControlPanelUrl'];?></span>
		</div>
		<div class="ui-fields">
                    <span class="fl-30"><?=t($t_base.'fields/available_formmanual_quotes');?></span>
                    <span class="field-container" style="width:50%;" ><?php echo (($forwarderCP2['iAvailableForManualQuotes']==1)?'Yes':'No');?></span>
		</div>
                <div class="ui-fields">
                    <span class="fl-30"><?=t($t_base.'fields/rfq_response');?></span>
                    <span class="field-container">
                    <?php echo ($forwarderCP2['iOffLineQuotes']?"By e-mail":'In system'); ?>
		</div>
                <div class="ui-fields">
                    <span class="fl-30"><?=t($t_base.'fields/quick_quote_available');?></span>
                    <span class="field-container" style="width:50%;" ><?php echo (($forwarderCP2['iDisplayQuickQuote']==1)?'Yes':'No');?></span>
		</div>
</div>		
		<br />
<div id="pricing">		
		<h4 style="margin-top:5px;"><strong><?=t($t_base.'title/transprteca_pricing');?> </strong> <a href="javascript:void(0);" onclick="setting_pricing(<?=$idForwarder?>,'pricing','EDIT_PRICING')"><?=t($t_base.'fields/edit');?></a></h4>							
		 
                    <div class="ui-fields">
                        <span class="fl-30"><?=t($t_base.'fields/gross_profit');?></span>  
                        <span class="field-container"><?php echo $szProfitType; ?></span>
                    </div>
                    <?php
                        echo displayForwarderReferralFeeDetails($idForwarder);
                    ?> 
                <div class="ui-fields">
                    <span class="fl-30"><?=t($t_base.'fields/credit_offered_transporteca');?></span>
                    <span class="field-container"><?=$forwarderCP2['iCreditDays']." days"; ?></span>
		</div> 
		<div class="ui-fields">
                    <span class="fl-30"><?=t($t_base.'fields/validto');?></span>
                    <span class="field-container"><?=(($forwarderCP2['dtReferealFeeValid']!='0000-00-00 00:00:00' && $forwarderCP2['dtReferealFeeValid']!='')?date('j. F Y',strtotime($forwarderCP2['dtReferealFeeValid'])):'')?></span>
		</div>
		<div class="ui-fields">
			<span class="fl-30"><?=t($t_base.'fields/automatic_pay');?></span>
			<span class="field-container">
			<?php echo ($forwarderCP2['iAutomaticPaymentInvoicing']?"Yes":'No')?>
		</div> 
</div>		
		<br />
<div id="comments" >			
		<div class="ui-fields" style="margin-top:0;">
		<h4><strong><?=t($t_base.'fields/comments_for_pricing');?> </strong> <a href="javascript:void(0);" onclick="setting_pricing(<?=$idForwarder?>,'comments','EDIT_COMMENTS')"><?=t($t_base.'fields/edit');?></a></h4>							
		
			<span class="fl-80" style="font-style: italic;"><?=$forwarderCP2['szReferalFeeComment']?></span>
		</div>
</div>			
<?php
}
function submission_awaiting_review_and_pricing($waitingForApprovalByManagement,$mode)
{
	$t_base = "management/uploadService/";
	?>
		<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="wating_for_cost_approved_by_management<?=$mode?>">
			<tr>
				<td style="width:11%;"><strong><?=t($t_base.'fields/submitted')?></strong></td>
				<td style="width:22%;"><strong><?=t($t_base.'fields/forwarder')?></strong></td>
				<td style="width:16%;"><strong><?=t($t_base.'fields/data_relate_to')?></strong></td>
				<td align="left" style="width:6%;"><strong><?=t($t_base.'fields/data')?></strong></td>
				<td align="left" style="width:13%;"><strong><?=t($t_base.'fields/records')?></strong></td>
				<td align="right" style="width:14%;"><strong><?=t($t_base.'fields/processing_cost')?></strong></td>
				<td style="width:13%;"><strong><?=t($t_base.'fields/submitted_by')?></strong></td>
				<td style="width:5%;">&nbsp;&nbsp;&nbsp;</td>
			</tr>
				<?
					if(!empty($waitingForApprovalByManagement))
					{
						$i=0;
						foreach($waitingForApprovalByManagement as $waitingForApprovalByManagements)
						{
						
							$submitted_by='';
							if(strlen($waitingForApprovalByManagements['szForwarderContactName'])>12)
							{
								$submitted_by=mb_substr($waitingForApprovalByManagements['szForwarderContactName'],0,10,'UTF8')."..";
							}
							else
							{
								$submitted_by=$waitingForApprovalByManagements['szForwarderContactName'];
							}
							
							$szDisplayName = '';
							if(strlen($waitingForApprovalByManagements['szDisplayName'])>25)
							{
								$szDisplayName=mb_substr($waitingForApprovalByManagements['szDisplayName'],0,22,'UTF8')."..";
							}
							else
							{
								$szDisplayName=$waitingForApprovalByManagements['szDisplayName'];
							}
							
							
				?>
							<tr id="wating_for_cost_<?=$mode.++$i?>" 
							<?if($mode=='__TOP__'){?>onclick="sel_line_waiting_for_cost_approval_admin('wating_for_cost_<?=$mode.$i?>','<?=$waitingForApprovalByManagements['id']?>','<?=t($t_base.'fields/cancel')?>','<?=t($t_base.'fields/edit')?>','<?=t($t_base.'fields/delete')?>');"<? }else if($mode=='__BOTTOM__'){ ?>
							onclick="bot_line_waiting_for_cost_approval_admin('wating_for_cost_<?=$mode.$i?>','<?=$waitingForApprovalByManagements['id']?>');"<? } ?>>
								<td><?php if($waitingForApprovalByManagements['dtSubmitted']!='' && $waitingForApprovalByManagements['dtSubmitted']!='0000-00-00 00:00:00')
										  {
												echo date('j. F',strtotime($waitingForApprovalByManagements['dtSubmitted']));
										  }	 				
								?></td>
								<td><?=$szDisplayName;?></td>
								<td><?=$waitingForApprovalByManagements['iFileType']?></td>
								<td align="left"><?=$waitingForApprovalByManagements['iFileCount']?> <? if($waitingForApprovalByManagements['iFileCount']>1){ echo t($t_base.'fields/files'); }else { echo t($t_base.'fields/file');;  }?></td>
								<td align="left">
									<?php
										if($waitingForApprovalByManagements['iTotalRecords']=='' || $waitingForApprovalByManagements['iTotalRecords']==0)
										{
											echo t($t_base.'messages/TBD');
										}
										else
										{
											echo number_format((int)$waitingForApprovalByManagements['iTotalRecords'])." ".$waitingForApprovalByManagements['szRecordType'];
										}
									?>
								</td>
								<td align="right">
									<?php
										if($waitingForApprovalByManagements['fProcessingCost']=='0.00')
										{
											echo t($t_base.'messages/TBD');
										}
										else
										{
											echo $waitingForApprovalByManagements['szCurrency'];
											echo " ";
											echo number_format((float)$waitingForApprovalByManagements['fProcessingCost'],2);
										}
									?>
								
								</td>
								<td><?=$submitted_by?></td>
								<td><a href="javascript:void(0)" id="comment_details_<?=$waitingForApprovalByManagements['id']?>" onclick="openCommentDescription('<?=$waitingForApprovalByManagements['id']?>');"><img src="<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>/images/forwarders/comments.png" alt="Comments" title="Comments"></a></td>
							</tr>
				
				<? }}else{?>
			<tr>
				<td colspan="8" align="center"><?=t($t_base.'messages/no_record_found')?></td>
				
			</tr>
			<? }?>
		</table>
		<br/>
		
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
				<td>&nbsp;</td>
				<td align="right">
				<? if($mode == '__TOP__')
				{?>
					<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="delete_files"><span id="delete_cancel"><?=t($t_base.'fields/delete')?></span></a>
					<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="view_files"><span><?=t($t_base.'fields/view_files')?></span></a>
					<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="edit_files"><span id="edit_name"><?=t($t_base.'fields/edit')?></span></a>
					<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="submit_files"><span><?=t($t_base.'fields/submit')?></span></a>
				<? }else if($mode =='__BOTTOM__'){ ?>
					<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="delete_files_forwarder_awaiting"><span><?=t($t_base.'fields/delete')?></span></a>
					<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="move_files"><span><?=t($t_base.'fields/move_back')?></span></a>
				<? } ?>
				</td>
			</tr>
		</table>
	<?
}
function submission_awaiting_review_and_pricing_waiting($waitingForApprovalByManagement,$mode)
{
	$t_base = "management/uploadService/";
	$kUploadBulkService = new cUploadBulkService();
	?>
		<div class="multi-payment-transfer-popup">
			<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%" id="wating_for_cost_approved_by_management<?=$mode?>">
				<tr>
					<? if($mode =='__WTOP__'){?><td style="width:12%;"><strong><?=t($t_base.'fields/approved')?></strong></td><?}?>
					<td style="width:12%;"><strong><? if($mode =='__WTOP__'){?>Complete by<?}else if($mode == '__WBOTTOM__'){?>Completed<?}?></strong></td>
					<? if($mode =='__WBOTTOM__'){?><td align="left" style="width:12%;"><strong><?=t($t_base.'fields/data')?></strong></td><? }?>
					<td style="width:25%;"><strong><?=t($t_base.'fields/forwarder')?></strong></td>
					<td style="width:19%;"><strong><?=t($t_base.'fields/data_relate_to')?></strong></td>
					<?if($mode=='__WBOTTOM__'){?>	
					<td align="left" style="width:13%;"><strong><?=t($t_base.'fields/records')?></strong></td>
					<? }if($mode =='__WTOP__'){ ?>
					<td align="left" style="width:13%;"><strong><?=t($t_base.'fields/records')?></strong></td>
					<? }?>
					<td align="right" style="width:14%;"><strong><?=t($t_base.'fields/processing_cost')?></strong></td>
					<!--  <td><strong><?=t($t_base.'fields/submitted_by')?></strong></td>-->
					<td style="width:5%;">&nbsp;&nbsp;&nbsp;</td>
				</tr>
					<?
						if(!empty($waitingForApprovalByManagement))
						{
							$i=0;
							foreach($waitingForApprovalByManagement as $waitingForApprovalByManagements)
							{
								
								$szDisplayName = '';
								if(strlen($waitingForApprovalByManagements['szDisplayName'])>25)
								{
									$szDisplayName=mb_substr($waitingForApprovalByManagements['szDisplayName'],0,22,'UTF8')."..";
								}
								else
								{
									$szDisplayName=$waitingForApprovalByManagements['szDisplayName'];
								}
							
								$filenameArr=$kUploadBulkService->bulkUploadServiceFilesByManagement($waitingForApprovalByManagements['id']);
									$filename=$filenameArr[0]['szFileName'];
					?>
								<tr id="wating_for_cost_<?=$mode.++$i?>" 
								<?if($mode=='__WTOP__'){?>onclick="waiting_line_waiting_for_cost_approval_admin('wating_for_cost_<?=$mode.$i?>','<?=$waitingForApprovalByManagements['id']?>','<?=$filename?>');"<? }else if($mode=='__WBOTTOM__'){ 
								
								?>
								onclick="comp_line_waiting_for_cost_approval_admin('wating_for_cost_<?=$mode.$i?>','<?=$waitingForApprovalByManagements['id']?>','<?=$filename?>');"<? } ?>>
									<? if($mode =='__WTOP__'){?>
									<td><?php if($waitingForApprovalByManagements['dtCostApproval']!='' && $waitingForApprovalByManagements['dtCostApproval']!='0000-00-00 00:00:00')
											  {
													echo date('j. F',strtotime($waitingForApprovalByManagements['dtCostApproval']));
											  }	 				
									?></td>
									<? } ?>
									<td><?php
										if($mode =='__WTOP__'){
											  if($waitingForApprovalByManagements['dtExpected']!='' && $waitingForApprovalByManagements['dtExpected']!='0000-00-00 00:00:00')
											  {
													echo date('j. F',strtotime($waitingForApprovalByManagements['dtExpected']));
											  }
										}else
										{
											  if($waitingForApprovalByManagements['dtCompleted']!='' && $waitingForApprovalByManagements['dtCompleted']!='0000-00-00 00:00:00')
											  {
													echo date('j. F',strtotime($waitingForApprovalByManagements['dtCompleted']));
											  }
										}	 				
									?></td>
									<?if($mode=='__WBOTTOM__'){?><td align="left"><?=$waitingForApprovalByManagements['iFileCount']?> <? if($waitingForApprovalByManagements['iFileCount']>1){ echo t($t_base.'fields/files'); }else { echo t($t_base.'fields/file');  }?></td><? } ?>
									<td><?=utf8_encode($szDisplayName);?></td>
									<td><?=$waitingForApprovalByManagements['iFileType']?></td>
									<? if($mode =='__WTOP__'){ ?>
									<td align="left">
										<?php
												echo number_format((int)$waitingForApprovalByManagements['iActualRecords'])." ".$waitingForApprovalByManagements['szRecordType'];
										?>
									</td>
									<? }?>
									<? if($mode =='__WBOTTOM__'){ ?>
									<td align="left">
										<?php
												echo number_format((int)$waitingForApprovalByManagements['iActualRecords'])." ".$waitingForApprovalByManagements['szRecordType'];
										?>
									</td>
									<? }?>
									<td align="right">
										<?php
											
												echo $waitingForApprovalByManagements['szCurrency'];
												echo " ";
												echo number_format((float)$waitingForApprovalByManagements['fProcessingCost'],2);
										?>
									
									</td>
									<!-- <td><?=$waitingForApprovalByManagements['szForwarderContactName']?></td>-->
									<td><a href="javascript:void(0)" id="comment_details_<?=$waitingForApprovalByManagements['id']?>" onclick="openCommentDescription('<?=$waitingForApprovalByManagements['id']?>');"><img src="<?=__MAIN_SITE_HOME_PAGE_SECURE_URL__?>/images/forwarders/comments.png" alt="Comments" title="Comments"></a></td>
								</tr>
					
					<? }}else{?>
				<tr>
					<td colspan="8" align="center"><?=t($t_base.'messages/no_record_found')?></td>
					
				</tr>
				<? }?>
			</table>
		</div>	
		
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
				
				<? if($mode == '__WTOP__')
				{?>
				<td>
				<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="view_files"><span><?=t($t_base.'fields/view_files')?></span></a>
				<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="view_records"><span><?=t($t_base.'fields/view_records')?></span></a>
				<form enctype="multipart/form-data" method="post" name="uploadFile" id="uploadFile" style="display: inline;position:relative;">						
						<a class="button1" id="button_upload" style="opacity:0.4;"><span style="cursor: pointer;"><?=t($t_base.'fields/upload')?></span></a><input type="file" class="file" name="file_upload" onchange="upload_working_file()"	size="4" id="upload">	
						<input type="hidden" name="mode" value="UPLOAD_FILE_SUBMISSION">
						<input type="hidden" name="id" id="fileID" value="">
				</form>
				</td>
				<td align="right">				
					<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="move_files"><span><?=t($t_base.'fields/move_back')?></span></a>
					<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="submit_files"><span>Complete</span></a>
				</td>
				<? }else if($mode =='__WBOTTOM__'){ ?>
				<td>
				<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="view_file"><span><?=t($t_base.'fields/view_files')?></span></a>
				<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="view_record"><span><?=t($t_base.'fields/view_records')?></span></a>
				</td>
				<td align="right">					
					<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="move_file"><span><?=t($t_base.'fields/move_back')?></span></a>
					<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="delete_file"><span><?=t($t_base.'fields/delete')?></span></a>
				</td>
				<? } ?>				
			</tr>
		</table>
	<?php
}
function validateManagement()
{
	if(isset($_SESSION))
	{
            if(!($_SESSION['admin_id']>0))
            {
                header('location:'.__MANAGEMENT_URL__);
                die;
            }
            else
            {
                $kAdmin = new cAdmin();
                $kAdmin->getAdminDetails($_SESSION['admin_id']);
                if((int)$kAdmin->iActive==0)
                {
                    $_SESSION['admin_id']='';
                    unset($_SESSION['admin_id']);
                    header('location:'.__MANAGEMENT_URL__);
                    die;
                }
            }
	}
	else
	{
		header('location:'.__MANAGEMENT_URL__);
		die;
	}
}
function validateManagement_ajax()
{
	if(isset($_SESSION))
	{
		if(!($_SESSION['admin_id']>0))
		{
			?>
			<script type="text/javascript">
			redirectUrl('<?=__MANAGEMENT_URL__?>');
			</script>
			<?php
			die;
		}
	}
	else
	{
		?>
		<script type="text/javascript">
		redirectUrl('<?=__MANAGEMENT_URL__?>');
		</script>
		<?php
		die;
	}
}

function viewTnc($data,$mode,$order)
{ 	
$t_base="management/textEditor/";
$mode=ucwords(trim($mode));
 ?>
 <table cellpadding="0" cellspacing="0" class="format-4" width="100%">
		<tr>
			<th width="4%" align="center" valign="top"></th>
			<th width="30%" align="left" valign="top"><?=t($t_base.'fields/current_version')?></th>
			<th width="30%" align="left" valign="top"><?=t($t_base.'fields/draft_version')?></th>
			<th width="10%" align="left" valign="top"><?=t($t_base.'fields/language')?></th>
			<th width="15%" align="left" valign="top" >&nbsp;</th>
			<th width="5%" align="left" valign="top" >&nbsp;</th>
		</tr>
 <?php
 //print_r($data);
 	if(!empty($data))
 	{	$kAdmin= new cAdmin();
 		$maxorder=$kAdmin->getMaxOrder($order);
 		$count=1;
                $kConfig = new cConfig();
	 	foreach($data as $terms)
	 	{ 
                        $langArr=$kConfig->getLanguageDetails('',$terms['iLanguage']);
	 ?>
		 	<tr>
			 	<td valign="top"><?=$count;?></td>
			 	<td valign="top"><?=$terms['szHeading'];?></td>
			 	<td valign="top"><?=$terms['szDraftHeading']?></td>
			 	<td valign="top"><?php echo $langArr[0]['szName']; ?></td>
			 	<td valign="top">
			 	 <?php if($terms['szDraftHeading']!=''){?>
			 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png" class="cursor-pointer" id="add" onclick='editTextEditor("<?=$mode?>TNC","<?=$terms['id']?>")' alt="edit">	
			 	<?php if($terms['iOrderByDraft']>1){ ?><img src="<?=__BASE_STORE_IMAGE_URL__?>/up-icon.png" class="cursor-pointer" onclick="orderChange('<?=$terms['id']?>','1','<?=$mode?>')" alt="up"><? }else{ ?><span style=" display: inline-block;  width: 19px;"></span><? } ?>
			 	<?php if($maxorder!=$terms['iOrderByDraft']){?><img src="<?=__BASE_STORE_IMAGE_URL__?>/down-icon.png" class="cursor-pointer" onclick="orderChange('<?=$terms['id']?>','2','<?=$mode?>')" alt="down"><? }else{ ?><span style=" display: inline-block;  width: 19px;"></span><? } ?>
			 	<img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png" id="deleteTextEditor" class="cursor-pointer" onclick="deleteTextData('DELETE_TERMS_<?=$mode?>','<?=$terms['id']?>')" alt="delete"></td> 	
				<?php } ?>
				<td valign="top"><?=($terms['iUpdated']?'<span class="info-icon">&nbsp;</span>':'')?></td>
			 </tr>
			 
	 <? $count++; }
	}
	elseif(empty($data))
	{
		echo "<tr align='center'><td colspan='5'><strong>NO DATA TO PREVIEW</strong></td></tr>";
	}
?>
</table>	
<?php
 }

function mainHeadingGetCOntent($textEditor,$saveMode)
{
	$t_base="management/textFaq/";
	$kConfig =new cConfig();
    $langArr=$kConfig->getLanguageDetails();
	
	?>
	<form method="post" id="textEditorSubmit" onkeypress="saveDetailsCheck(event);">
		<div style="float: left;padding: 0 5px 5px 5px;"><?=t($t_base.'fields/language');?>:</div> 
		<div id="headingDiv" style="display: block;padding: 0px 5px 5px 5px;">
			<select name="textEditArr[iLanguage]" id="iLanguage">
                <?php
                if(!empty($langArr))
                {
                    foreach($langArr as $langArrs)
                    {?>
                        <option value = '<?php echo $langArrs['id']?>' <?php if(isset($textEditor['iLanguage']) && $textEditor['iLanguage'] ==$langArrs['id']){echo "selected='selected'"; } ?>><?php echo ucfirst($langArrs['szName']);?></option>
                      <?php  
                    }
                }?>
               
        </select>
		</div>
        <div style="float: left;padding: 0 5px 5px 5px;"><?=($saveMode == 'CUSTOMER_FAQ_EDIT_DATA' || $saveMode == 'FORWARDER_FAQ_EDIT_DATA')?'Q':'Heading';?>:</div> <div id="headingDiv" style="display: block;padding: 0px 5px 5px 5px;"><input type="text" id="heading" name="textEditArr[heading]" value="<?=$textEditor['szDraftHeading'];?>" size="86" style="padding: 3px;"></div>
         <div style="float: left;padding: 0 5px 5px 5px;"><?=($saveMode == 'CUSTOMER_FAQ_EDIT_DATA' || $saveMode == 'FORWARDER_FAQ_EDIT_DATA')?'A':'Content';?>:</div>
        
        <textarea name="content" id="content" class="myTextEditor" style="width:90%;height:60%;"><?=$textEditor['szDraftDescription']?></textarea>
        <!-- <input type="hidden" id="checkMode" name="mode" value="SAVE_FORWARDER_FAQ"> -->
        <input type="hidden" id="checkId" name="id" value="<?=($textEditor['id'])?$textEditor['id']:0;?>">
        <br/>
        <div style="float: right;">
	        <a id="save" class="button1" style="opacity:" onclick="saveDetails(<?=($textEditor['id'])?$textEditor['id']:0;?>,'<?=$saveMode;?>');"><span><?=t($t_base.'fields/save');?></span></a>
			<a class="button1" onclick="cancel_text_edit(0)"><span><?=t($t_base.'fields/cancel');?></span></a>
		</div>
	</form>

	<?
}
function showTnCData($editData,$node)
{	
	if(isset($editData))
	{		
		if(!empty($editData))
		{
			foreach($editData as $key=>$value)
			{
?>	
	<div>
	<? /* 
		<!-- <a id="<?=$node."_".$value['id'];?>" onclick="editTextEditor('FORWARDERTNC','<?=$value['id'];?>');"><?=$value['szHeading'];?></a> -->
		*/ ?>
		<a id="<?=$node."_".$value['id'];?>" onclick="deleteTextData('FORWARDERTNC','<?=$value['id'];?>');"><?=$value['szHeading'];?></a>
	
	</div>			
<?			}
		}
		else if(empty($editData))
		{
			echo "NO DATA TO PREVIEW";
		}
	}
?>

<!-- <a class="button1" id="add" onclick="editTextEditor('FORWARDERTNC','0');"><span>ADD</span></a> -->
<?	
}

function viewGeneralTextEdit($data,$mode)
{
?>
		<form method="post" id="textEditorSubmit">
        	<textarea name="content" id="content" class="myTextEditor" style="width:100%;height:60%;"><?=$data['szValue'];?></textarea>
        	<input type="hidden" id="checkMode" name="mode" value="<?=$mode?>">
        	<input type="hidden" id="checkId" name="id" value="<?=$data['id'];?>">
        	<br/>
		</form>
<?
}
 function viewEachManageVarible($data)
 { $t_base="management/manageVar/";
 ?>
 <div id="popup-bg"></div>
			<div id="popup-container" >
				<div class="popup">
				<div id="error"></div>
					<?=t($t_base.'fields/heading');?>:<p><input type="text" id='heading' value="<?=$data['szFriendlyName']?>" size="30"></p><br/>
					<?=t($t_base.'fields/value');?>:<p><input type="text" id='value' value="<?=$data['szValue']?>" size="30"></p>
					<br/>
					<p align="center"><a href="javascript:void(0)" onclick="saveEditManagevar(<?=$data['id']?>)" class="button1" ><span><?=t($t_base.'fields/save');?></span></a> <a href="javascript:void(0)" class="button1" onclick="cancelEditMAnageVar(0)"><span><?=t($t_base.'fields/cancel');?></span></a></p>
			</div>
			</div>
 <?
 }
 
 function estimationContent($content)
 {
 
 //print_r($content);
 $t_base = "management/uploadService/";
 
 
 ?>
 <form id="EstimationComtent">
	 <div>
	    <div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'fields/estimated_number_of_records')?></p>
			<p class="field-container"><input type="text" style="width: 100px;" id="iRecords" name="updatePricingAry[iRecords]" value="<?=(isset($content['iTotalRecords'])?($content['iTotalRecords']==''?0:$content['iTotalRecords']):'')?>" disabled> <span id="szRecord_type"> <i><?=$content['szRecordType']?></i></span></p>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'fields/expected_processing_time')?></p>
			<p class="field-container"><input type="text" style="width: 100px;" id="szEstimatedTime" name="updatePricingAry[szEstimatedTime]" value="<?=$content['iProcessingTime']?>" disabled> <span id="duration" style="display:none;"> <i><?=t($t_base.'fields/working_days')?></i></span></p>
		</div>
		<div class="profile-fields">
			<p class="fl-30"><?=t($t_base.'fields/processing_cost_1')?></p>
			<p class="field-container"><input type="text" style="width: 100px;" id="szProcessingCost" name="updatePricingAry[szProcessingCost]" value="<?=$content['fProcessingCost']?>" disabled><span id="currency"> <span id="szcurrency_value"><i><?=(isset($content['szCurrency'])?$content['szCurrency']:'')?></i> </span></p>
		</div>
		<input type="hidden" name="id" id="id" value = "<?=(isset($content['id'])?$content['id']:'')?>">
		<input type="hidden" name="mode" id="mode" value ="EDIT_PRICE_DETAILS">
	</div>
</form>
<?php
 }
 function manageVAriables($manageVariable)
 {
    if(!empty($manageVariable))
    {	
        foreach($manageVariable as $terms)
        { 
            if($terms['szDescription']=='__CAPSULE_OWNER_OF_NEW_CUSTOMER__')
            {
                $kAdmin = new cAdmin();
                $kAdmin->getAdminDetails($terms['szValue']);
                $terms['szValue'] = $kAdmin->szEmail ;
            }
	 ?>
	 <label class="ui-full-fields">
            <span class="field-name"><?=$terms['szFriendlyName'];?> <?php if($terms['szDescription']=='__PAYPAL_STATUS__'){?>(<a href="javascript:void(0);" onclick="setPaypalCountries();">Set countries offered</a>) <?php }?></span>
            <span class="field-container">	
		<?php 
                if(is_numeric($terms['szValue']))
		{
                    if((int)$terms['szValue'] == $terms['szValue'])
                    {	
                        echo number_format($terms['szValue']);
                    }
                    else
                    {
                        echo number_format($terms['szValue'],1);
                    }
		}
		else
		{
                    echo returnLimitData($terms['szValue'],60);
		}		
		?>
            </span>
	</label> 		
			 
	 <?php $count++; }
	}
 
 } 
 function manageVAriablesEdit($manageVariable,$mode,$kAdmin)
 {
    $t_base="management/Error/";
    if(!empty($kAdmin->arErrorMessages)){ 
                ?>
        <div id="regError" class="errorBox ">
        <div class="header"><?=t($t_base.'fields/please_following');?></div>
        <div id="regErrorList">
        <ul>
        <?php
            foreach($kAdmin->arErrorMessages as $key=>$values)
            {
            ?><li><?=$values?></li>
            <?php	
            }
        ?>
        </ul>
        </div>
        </div>
        <?php  }
		
 	if(!empty($manageVariable))
 	{	$count = 0;
 		echo "<form method='post' id='manageVar' name='manageVar'>";
	 	foreach($manageVariable as $terms)
	 	{ 
		 ?>
		 <label class="ui-full-fields">
			<span class="field-name">
				<?=$terms['szFriendlyName']?>
			</span>
			<?php
				if($terms['szDescription']=='__PAYMENT_GATEWAY_STATUS__')
				{
                                    if(!empty($_POST['szValue'][$count]))
                                    {
                                            $szDescValue = $_POST['szValue'][$count] ;
                                    }
                                    else
                                    {
                                            $szDescValue = $terms['szValue'] ;
                                    }
                                    ?>
                                    <span class="field-container">	
                                        <select name="szValue[<?=$count?>]" style="width:105px;">
                                            <option value="ENABLED" <?php echo (($szDescValue=='ENABLED')?'selected':''); ?>>Enabled</option>
                                            <option value="DISABLED" <?php echo (($szDescValue=='DISABLED')?'selected':''); ?>>Disabled</option>
                                        </select>
                                    </span>
                                    <?php
				}
                                else if($terms['szDescription']=='__PAYPAL_STATUS__')
				{
                                    if(!empty($_POST['szValue'][$count]))
                                    {
                                            $szDescValue = $_POST['szValue'][$count] ;
                                    }
                                    else
                                    {
                                            $szDescValue = $terms['szValue'] ;
                                    }
                                    ?>
                                    <span class="field-container">	
                                        <select name="szValue[<?=$count?>]" style="width:105px;">
                                            <option value="ENABLED" <?php echo (($szDescValue=='ENABLED')?'selected':''); ?>>Enabled</option>
                                            <option value="DISABLED" <?php echo (($szDescValue=='DISABLED')?'selected':''); ?>>Disabled</option>
                                        </select>
                                    </span>
                                    <?php
				}
                                else if($terms['szDescription']=='__CAPSULE_OWNER_OF_NEW_CUSTOMER__')
				{
                                    $customerListAry = $kAdmin->viewManagementDetails(false,true);
                                    if(!empty($_POST['szValue'][$count]))
                                    {
                                        $idAdmin = $_POST['szValue'][$count] ;
                                    }
                                    else
                                    {
                                        $idAdmin = $terms['szValue'] ;
                                    } 
                                    ?>
                                    <span class="field-container">	
                                            <select name="szValue[<?=$count?>]" style="width:105px;">
                                                <?php 
                                                    if(!empty($customerListAry))  
                                                    {
                                                        foreach($customerListAry as $customerListArys)
                                                        {
                                                            ?>
                                                            <option value="<?php echo $customerListArys['id']; ?>" <?php echo (($customerListArys['id']==$idAdmin)?'selected':''); ?> > <?php echo $customerListArys['szEmail']; ?></option>
                                                            
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                    </span>
                                    <?php
				}
				else
				{
			?>
				<span class="field-container">	
					<input type="text" name="szValue[<?=$count?>]" value="<?=(isset($_POST['szValue'][$count])?$_POST['szValue'][$count]:$terms['szValue'])?>" width="12px">
				</span>
			<?php } ?>
			</label>
			<input type="hidden" name="id[<?=$count?>]" value="<?=$terms['id']?>">
 <?			$count++;
 		}
 		?>	<input type="hidden" name="flag" value="<?=$mode?>">
 			<input type="hidden" name="mode" value="<?=$mode?>">
 			<br/>
 			<p align="center">
 			<a class="button2" onClick="cancelVariablesDetails()"><span>Cancel</span></a>
 			<a class="button1" onClick="updateVariablesDetails()"><span>Save</span></a> 
 			</p>
 			</form>			
 		<?
	}
}
function postcodeDet($postcode,$count =1000)
{
	$t_base="management/postcode/";
	require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
	$kPagination  = new Pagination($count,__CONTENT_PER_PAGE_POSTCODE__,15);
	$kPagination->paginate();
?>
	<table cellpadding="0" id="headings"  cellspacing="0" border="0" class="format-4" width="100%">
		<tr >
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Postcode')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Latitude')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Longitude')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_1')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_2')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_3')?> </th>
			<th width="12%" align="center" valign="top"><?=t($t_base.'fields/Region_4')?> </th>
			<th align="center" valign="top"><?=t($t_base.'fields/City')?> </th>
		</tr>
<?php
			$count	=1;
			foreach($postcode as $post)
			{
			echo '
				<tr id=\'postcode_details_'.$count.'\' onclick="showPostCodeTable(this.id,\''.$post['id'].'\')">
					<td>'.$post['szPostCode'].'</td>
					<td>'.$post['szLat'].'</td>
					<td>'.$post['szLng'].'</td>
					<td>'.$post['szRegion1'].'</td>
					<td>'.$post['szRegion2'].'</td>
					<td>'.$post['szRegion3'].'</td>
					<td>'.$post['szRegion4'].'</td>
					<td>'.$post['szCity'].'</td>
				</tr>
			';
			$count++;
			}
			echo '</table>';
			echo '<div class="page_nav">';
			if($kPagination->links_per_page>1)
			{
				echo $kPagination->renderFullNav();
			}
			echo '</div>';

}
function showForwarderPendingPayment($billingDetails,$idAdmin)
{	
    $t_base="management/forwarderpayment/";
    $kAdmin = new cAdmin;
    $kBillig = new cBilling();

    $serviceUploadPaymentArr=$kAdmin->showForwarderPendingPaymentServiceUpload();
    $canelBookingPaymentArr=$kAdmin->showForwarderPendingPaymentCancelBooking();
    //print_r($canelBookingPaymentArr);
    $kForwarderNew = new cForwarder();
    $paymentFrequency = $kForwarderNew->getAllPaymentFrequency(); 
    ?>
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
		<tr>
                    <td width="35%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/forwarder')?></strong></td> 
                    <td width="7%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/auto')?></strong></td>
                    <td width="12%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/last_transfer')?></strong></td>
                    <td width="18%" style='text-align:right;' valign="top"><strong><?=t($t_base.'fields/amount')?></strong></td>
                    <td width="18%" style='text-align:right;' valign="top"><strong><?=t($t_base.'fields/balance')?></strong> </td>
                    <td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/action')?></strong> </td>
		</tr>
			<?php
                            if($billingDetails!=array())
                            {	
                                $ctr=1;
                                foreach($billingDetails as $bills)
                                {  
					//echo $totalAmount;
					$currencyValue='';	
					if($bills['szForwarderCurrency']!='')
					{
                                            $currencyValue=$bills['szForwarderCurrency'];
					}
					else
					{
                                            $currencyValue=$bills['szCurrency'];
					}	
					$totalAmount1=0;	
					$uploadPrice=0;
					$totalAmount=0;
					$cancelPrice=0; 
					
					$uploadPrice=$serviceUploadPaymentArr[$bills['id']][$currencyValue];
					$cancelPrice=$canelBookingPaymentArr[$bills['id']][$currencyValue];
            
					if((float)$uploadPrice>0)
					{ 
                                            $totalAmount1=$bills['fForwarderTotalPrice']-(2*$uploadPrice);
					}
					else
					{
                                            $totalAmount1=$bills['fForwarderTotalPrice'];
					}
            
					if((float)$cancelPrice>0)
					{
						$totalAmount=$totalAmount1-(2*$cancelPrice);
					}
					else
					{
						$totalAmount=$totalAmount1;
					}
					if((int)$totalAmount==0 || strtolower($bills['szDisplayName'])=='transporteca')
					{
                                            continue;
					}
					if($totalAmount<0)
					{
						$neg_value=str_replace("-","",$totalAmount);
						$total_value="(".$currencyValue." ".number_format((float)$neg_value,2).")";
					}
					else
					{
						$total_value=$currencyValue." ".number_format((float)$totalAmount,2);
					}
                                        $szTotalAmountLastTransferred = $kBillig->getLastTranserDetailsByForwarder($bills['id'],true);
                                        
   					echo "<tr align='center'>
								<td style='text-align:left;'>".returnLimitData($bills['szDisplayName'],38)." (".$bills['szForwarderAlias'].")</td> 
								<td style='text-align:left;'>".($bills['iAutomaticPaymentInvoicing']?'Yes':'No')."</td>
								<td style='text-align:left;'>".($kAdmin->lastTransactionToForwarderByAdmin($bills['id']))."</td>
                                                                <td style='text-align:right;'>".$szTotalAmountLastTransferred."</td>
								<td style='text-align:right;'>".$total_value."</td>";
								if((float)$totalAmount>0){?>
								<td style='text-align:left;cursor: pointer;' onclick="showPaymentConfirmationpopup('<?=$bills['id']?>','<?=$idAdmin?>','<?=$bills['idForwarderCurrency']?>')" ><a><?=t($t_base.'fields/pay_now')?></a></td>
								<?php }else {?>
								<td></td>
								<?php }?>
							</tr>	
								<?php
						$id='';
					}
				 }
				else
				{
					echo "<tr><td colspan='6'><CENTER><b>NO RECORD FOUND</b><CENTER></td></tr>";
				}
			?>
		</table>
			
<?php
}

function showVisualizationTopForm($mode='VISUALIZATION')
{
	$t_base="management/visualization/";
	$kExport_Import =  new cExport_Import;
	$kConfig = new cConfig;
	$kAdmin =  new cAdmin;
	$forwardCountriesArr = array();
	
	$forwardCountriesArr=$kConfig->getAllCountries();
	
	?>
		<script type="text/javascript">
			$().ready(function() {	
				$("#datepicker_from").datepicker();
				$("#datepicker_to").datepicker();
			});
		</script>
		<form name = "sales_funnel_visualization_form" id="sales_funnel_visualization_form">
				<p class="fl-15 f-size-12">
				<?=t($t_base.'fields/from_date');?>
				<br>
				<input id="datepicker_from" style="width:90px;" name="visualizationAry[dtFromDate]" type="text" value="<?php echo date('d/m/Y',strtotime("-1 Month"));?>" />
				</p>
				<p class="fl-15 f-size-12">
					<?=t($t_base.'fields/to_date');?>
					<br>
					<input id="datepicker_to" style="width:90px;" name="visualizationAry[dtToDate]" type="text" value="<?php echo date('d/m/Y');?>" />
				</p>
				<p class="fl-20 f-size-12">
				<?=t($t_base.'fields/from_countries');?>
				<br>
				<select name="visualizationAry[idOriginCountry]" id="idOriginCountry" style="width: 130px;">
					<option value=""><?=t($t_base.'fields/all');?></option>
					<?php
						if(!empty($forwardCountriesArr))
						{
							foreach($forwardCountriesArr as $forwardCountriesArrs)
							{
								?>
								<option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
								<?
							}
						}
					?>
				</select>
				</p>
				<p class="fl-20 f-size-12">
				<?=t($t_base.'fields/to_countries');?>
				<br>
				<select name="visualizationAry[idDestinationCountry]"  id="idDestinationCountry" style="width: 130px;">
					<option value=""><?=t($t_base.'fields/all');?></option>
					<?php
						if(!empty($forwardCountriesArr))
						{
							foreach($forwardCountriesArr as $forwardCountriesArrs)
							{
								?>
								<option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
								<?
							}
						}
					?>
				</select>
					<input type="hidden" value="<?=$mode?>" name="mode">
				</p>
				<p class="fl-25 f-size-12">
				&nbsp;
				<br>
					<a class="button1" onclick="submit_visualization_form('<?=$mode?>');" style="float: right;"><span><?=t($t_base.'fields/show');?></span></a>
				</p>
			</form>
			<div style="clear: both;"></div>
			<br />
		<?php
}
function showCurrentWtwTopForm($t_base,$iWarehouseType=false)
{
    $kExport_Import =  new cExport_Import;
    $kConfig = new cConfig;
    $kAdmin =  new cAdmin;
    $forwardWareHouseArr = array();
    $forwardCountriesArr = array();
    $forwardWareHouseCityAry = array();
    //GETTING ALL FORWARDER DETAILS
    $allForwarderList = $kAdmin->getAllForwarderNameList(); 
    $forwardCountriesArr=$kConfig->getAllCountries(); 
    
    ?>
	<form name = "currentWTW" id="currentWTW">
                <p class="fl-25 f-size-12">
                    <?=t($t_base.'fields/forwarder');?> <br>
                    <select name="currentWtw[idForwarder]" id="idForwarder" onchange="resetDetails('idForwarder',this.value,'<?php echo $iWarehouseType; ?>')" style="width: 150px;">
                        <option value=""><?=t($t_base.'fields/all');?></option>
                        <?php
                            if(!empty($allForwarderList))
                            {
                                foreach($allForwarderList as $forwarderList)
                                {
                                    ?>
                                    <option value="<?=$forwarderList['id']?>"><?=$forwarderList['szDisplayName']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </p>
                <p class="fl-25 f-size-12">
                    <?=t($t_base.'fields/from_countries');?> <br>
                    <select name="currentWtw[idOriginCountry]" id="idOriginCountry" onchange="resetDetails('idOriginCountry',this.value,'<?php echo $iWarehouseType; ?>')" style="width: 150px;">
                        <option value=""><?=t($t_base.'fields/all');?></option>
                        <?php
                            if(!empty($forwardCountriesArr))
                            {
                                foreach($forwardCountriesArr as $forwardCountriesArrs)
                                {
                                    ?>
                                    <option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </p>
                <p class="fl-25 f-size-12">
                    <?=t($t_base.'fields/to_countries');?><br>
                    <select name="currentWtw[idDestinationCountry]"  id="idDestinationCountry" onchange="resetDetails('idDestinationCountry',this.value,'<?php echo $iWarehouseType; ?>')" style="width: 150px;">
                        <option value=""><?=t($t_base.'fields/all');?></option>
                        <?php
                            if(!empty($forwardCountriesArr))
                            {
                                foreach($forwardCountriesArr as $forwardCountriesArrs)
                                {
                                    ?>
                                    <option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </p>
                <p class="fl-10 f-size-12">
                    <?=t($t_base.'fields/active');?><br>
                    <select name="currentWtw[iActive]"  id="idActive"  style="width: 100px;">
                        <option value="1"><?=t($t_base.'fields/yes');?></option>
                        <option value="0"><?=t($t_base.'fields/no');?></option>
                    </select>
                </p>
                <input type="hidden" name="currentWtw[iWarehouseType]" id="iWarehouseType" value="<?php echo $iWarehouseType;?>">
                <p class="fl-20 f-size-12" align="right">
                    &nbsp;<br>
                    <a class="button1" onclick="submit_currentWTW_form();"><span><?=t($t_base.'fields/show');?></span></a>
                </p>
            </form>
            <div style="clear: both;"></div><br />
	<?php
}
function currentWTWBottomForm($bottomContent = array())
{	
	$t_base="management/currentWTW/";
	?>
		<table cellpadding="0" id="usertable" cellspacing="0" border="0" class="format-4" width="99%" style="margin-bottom:20px;">
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th colspan="4"><strong><?=t($t_base.'fields/origin_charges')?></strong></th>
				<th colspan="4"><strong><?=t($t_base.'fields/port_to_port')?></strong></th>
				<th colspan="4"><strong><?=t($t_base.'fields/destination_charges')?></strong></th>
			</tr>
			<tr>
				<th><strong><?=t($t_base.'fields/forwarder')?></strong></th>
				<th><strong><?=t($t_base.'fields/from')?></strong></th>
				<th><strong><?=t($t_base.'fields/to')?></strong></th>
				<th><strong><?=t($t_base.'fields/curr')?></strong></th>			
				<th style="text-align: right;"><strong><?=t($t_base.'fields/rate')?></strong></th>	
				<th style="text-align: right;"><strong><?=t($t_base.'fields/min')?></strong></th>		
				<th style="text-align: right;"><strong><?=t($t_base.'fields/fix')?></strong></th>		
				<th><strong><?=t($t_base.'fields/curr')?></strong></th>			
				<th style="text-align: right;"><strong><?=t($t_base.'fields/rate')?></strong></th>	
				<th style="text-align: right;"><strong><?=t($t_base.'fields/min')?></strong></th>		
				<th style="text-align: right;"><strong><?=t($t_base.'fields/fix')?></strong></th>		
				<th><strong><?=t($t_base.'fields/curr')?></strong></th>			
				<th style="text-align: right;"><strong><?=t($t_base.'fields/rate')?></strong></th>	
				<th style="text-align: right;"><strong><?=t($t_base.'fields/min')?></strong></th>		
				<th style="text-align: right;"><strong><?=t($t_base.'fields/fix')?></strong></th>
			</tr>
			<?php
	if($bottomContent!= array())
	{
		$kAdmin = new cAdmin;
		$kConfig = new cConfig;
		$num = 0;
		$forwarderName = array();
		//$wtwForwarder = array();
		foreach($bottomContent as $content)
		{	
			//GETTING CURRENCY IN STRING FOR EACH TYPE
			if($content['szOriginRateCurrency'])
			{
				$originRateCurrency = $kConfig->getBookingCurrency($content['szOriginRateCurrency']);
			}
			else
			{
				$originRateCurrency = 0;
			}
			$freightCurrency = $kConfig->getBookingCurrency($content['szFreightCurrency']);
			if($content['szDestinationRateCurrency'])
			{
				$destinationRateCurrency = $kConfig->getBookingCurrency($content['szDestinationRateCurrency']);
			}
			else
			{
				$destinationRateCurrency = 0;
			}
			$wtwForwarder[$num][] = $content['id'];
			//$wtwForwarder[$num][] = $forwarderName[] = returnLimitData($kAdmin->findForwarderNameUsingWarehouse($content['idWarehouseFrom']),8);
			//$wtwForwarder[$num][] = $kAdmin->getAllWarehouseName($content['idWarehouseFrom']);
			//$wtwForwarder[$num][] = $kAdmin->getAllWarehouseName($content['idWarehouseTo']);
			$wtwForwarder[$num][] = returnLimitData($content['szDisplayName'],7);
			$wtwForwarder[$num][] = returnLimitData3($content['szCityFrom'],6).'('.$content['ISOCodeFrom'].')';
			$wtwForwarder[$num][] = returnLimitData3($content['szCityTo'],6).'('.$content['ISOCodeTo'].')';
			$wtwForwarder[$num][] = $originRateCurrency[0]['szCurrency'];
			$wtwForwarder[$num][] = $content['fOriginRateWM'];
			$wtwForwarder[$num][] = $content['fOriginMinRateWM'];
			$wtwForwarder[$num][] = $content['fOriginRate'];
			$wtwForwarder[$num][] = $freightCurrency[0]['szCurrency'];
			$wtwForwarder[$num][] = $content['fRateWM'];
			$wtwForwarder[$num][] = $content['fMinRateWM'];
			$wtwForwarder[$num][] = $content['fRate'];
			$wtwForwarder[$num][] = $destinationRateCurrency[0]['szCurrency'];
			$wtwForwarder[$num][] = $content['fDestinationRateWM'];
			$wtwForwarder[$num][] = $content['fDestinationMinRateWM'];
			$wtwForwarder[$num][] = $content['fDestinationRate'];
			/*$wtwForwarder[$num][] = ($content['dtExpiry']>date('Y-m-d h:i:s')?'Yes':'No');*/
			$num++;
			
		}
		/**
		**
		*  SORTING ON THE BASIS OF FORWARDER NAME, COUNTRY FROM ,COUNTRY TO
		*/
		//$wtwForwarder = sortArray($wtwForwarder, 1);
		
		//$wtwForwarder = sortArray($wtwForwarder, 1);
		//array_multisort($wtwForwarder, SORT_ASC, $forwarderName);
		$num = 1;
		foreach( $wtwForwarder as $wtwForwarderArr)
		{
			 echo "
				<tr id ='showEachContent".$num++."' onclick =\"showBottomTableToEditWTWContent(this.id,'".$wtwForwarderArr[0]."');\">
						<td>".$wtwForwarderArr[1]."</td>
						<td>".$wtwForwarderArr[2]."</td>
						<td>".$wtwForwarderArr[3]."</td>
						
						<td>".$wtwForwarderArr[4]."</td>
						<td style=\"text-align: right;\">".($wtwForwarderArr[4]!=''?$wtwForwarderArr[5]:'')."</td>
						<td style=\"text-align: right;\">".($wtwForwarderArr[4]!=''?$wtwForwarderArr[6]:'')."</td>
						<td style=\"text-align: right;\">".($wtwForwarderArr[4]!=''?$wtwForwarderArr[7]:'')."</td>
						
						<td>".$wtwForwarderArr[8]."</td>							
						<td style=\"text-align: right;\">".$wtwForwarderArr[9]."</td>
						<td style=\"text-align: right;\">".$wtwForwarderArr[10]."</td>
						<td style=\"text-align: right;\">".$wtwForwarderArr[11]."</td>
						
						<td>".($wtwForwarderArr[12]!=''?$wtwForwarderArr[12]:'')."</td>							
						<td style=\"text-align: right;\">".($wtwForwarderArr[12]!=''?$wtwForwarderArr[13]:'')."</td>
						<td style=\"text-align: right;\">".($wtwForwarderArr[12]!=''?$wtwForwarderArr[14]:'')."</td>
						<td style=\"text-align: right;\">".($wtwForwarderArr[12]!=''?$wtwForwarderArr[15]:'')."</td>
					</tr>
				";
		
		}
		/*
		 * echo "
			
				<tr id ='showEachContent".$num++."' onclick =\"showBottomTableToEditWTWContent(this.id,'".$content['id']."');\">
					<td>".returnLimitData($kAdmin->findForwarderNameUsingWarehouse($content['idWarehouseFrom']),10)."</td>
					<td>".$kAdmin->getAllWarehouseName($content['idWarehouseFrom'])."</td>
					<td>".$kAdmin->getAllWarehouseName($content['idWarehouseTo'])."</td>
					
					<td>".$originRateCurrency[0]['szCurrency']."</td>
					<td>".$content['fOriginRateWM']."</td>
					<td>".$content['fOriginMinRateWM']."</td>
					<td>".$content['fOriginRate']."</td>
					
					<td>".$freightCurrency[0]['szCurrency']."</td>							
					<td>".$content['fRateWM']."</td>
					<td>".$content['fMinRateWM']."</td>
					<td>".$content['fRate']."</td>
					
					<td>".$destinationRateCurrency[0]['szCurrency']."</td>							
					<td>".$content['fDestinationRateWM']."</td>
					<td>".$content['fDestinationMinRateWM']."</td>
					<td>".$content['fDestinationRate']."</td>
					
					<td>".($content['dtExpiry']>date('Y-m-d h:i:s')?'Yes':'No')."</td>
				</tr>
			";
		 * 
		 */
	}
	else
	{
		echo "<tr><td colspan = '16' align='center'><strong>NO CONTENT TO DISPLAY</strong></td></tr>";
	}
	echo "</table>";
		?>
			<div style="float: right;"><a class="button1" style="opacity:0.4;" id="editButton"><span><?=t($t_base.'fields/edit')?></span></a></div>
			<div style="clear: both;"></div>
		<?php
}
	function showPriorityPostcode($priority)
	{	
		$t_base	=	"management/postcode/";	
		?>
			<div id="priority" >
				<div id="popup-bg"></div>
				<div id="popup-container">	
					<div class="compare-popup popup" style="width: 500px;height: 400px;">
						<h4><b><?=t($t_base.'fields/biggest_no_gets_higher_priority')?></b></h4>
						<br />
						<div class="scroll-div" style="padding: 0px;height: 300px;" >
							<table cellpadding="0" cellspacing="0" class="format-4" width="100%" >
								<tr>
									<th width="50%" align="left" valign="top"><?=t($t_base.'fields/country')?><span id="country"><span onclick="sort_priority('szCountryName','DESC','country');" class="sort-arrow-down-grey">&nbsp;</span></span></th>
									<th width="30%" align="left" valign="top"><?=t($t_base.'fields/city')?><span id="city"><span onclick="sort_priority('szCity','ASC','city');" class="sort-arrow-up-grey">&nbsp;</span></span></th>
									<th width="10%" align="left" valign="top"><?=t($t_base.'fields/priority')?></th>
									<th width="10%"></th>
								</tr>
								<?php if($priority!= array())
									{
										foreach($priority as $priorityArr)
										{
											echo "
												<tr id='deletePriority".++$count."'>
													<td>".$priorityArr['1']."</td>
													<td>".$priorityArr['0']."</td>
													<td>".$priorityArr['2']."</td>
													<td><img src='".__BASE_STORE_IMAGE_URL__."/delete-icon-mgmt.png'  onclick=\"deletePriority('deletePriority".$count."','".$priorityArr['3']."','".$priorityArr['1']."','".$priorityArr['0']."')\" class='cursor-pointer' alt='delete'></td>
												</tr>
											";	
										}
									}else{
									?>
									<tr><td colspan="4" align="center"><b><?=t($t_base.'fields/no_data_found')?></b></td></tr>
									<?php
									}
								?>
								</table>
							</div>
							
							<p align="center"><a class="button1" onclick="closeConactPopUp(0)"><span><?=t($t_base.'fields/close')?></span></a></p>
					</div>
				</div>	
			</div>
		<?php
	}
	function showCustomClearenceNewArr($bottomContent = array() )
	{
		$count = 0;
		$arrData = array();
		if($bottomContent!= array())
		{
			$kAdmin = new cAdmin();	
			foreach($bottomContent as $content)
			{
					$arrData[$count]['idCCExport'] = $content['from_id'];
					$arrData[$count]['idCCImport'] = $content['to_id'];	
					
					$arrData[$count]['szDisplayName'] = $content['szDisplayName'];
						
					$arrData[$count]['wareHouseFrom'] = $content['wareHouseFrom'];	
					$arrData[$count]['cityFrom'] = $content['cityFrom'];	
					$arrData[$count]['countryFrom'] = $content['countryFrom'];	
					$arrData[$count]['wareHouseTo'] = $content['wareHouseTo'];	
					$arrData[$count]['cityTo'] = $content['cityTo'];	
					$arrData[$count]['countryTo'] = $content['countryTo'];
					
					$arrData[$count]['iExportRate'] 	= $content['export_value'];	
					$arrData[$count]['iExportCurrency'] = $kAdmin->currencyFinder($content['export_currency']);
					$arrData[$count]['iImportRate'] 	= $content['import_value'];	
					$arrData[$count]['iImportCurrency'] = $kAdmin->currencyFinder($content['import_currency']);	
					$arrData[$count]['iActiveFrom'] 	= $content['iActiveFrom'];	
					$arrData[$count]['iActiveTo'] = $content['iActiveTo'];	
					$count++;
			}
		}
		unset($bottomContent);
		return $arrData;
		exit;
	
	}
	function oboCCModelAdminFunction($bottomContent,$count)
	{
		$t_base = "management/customclearence/";
		require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
		$kPagination  = new Pagination($count,__CONTENT_PER_PAGE_CUSTOM_CLEARENCE__,15);
		$kPagination->paginate('pageCC');
	?>
		<table cellpadding="0" cellspacing="0" class="format-4" width="100%" id="customClearence">
			<tr>
				<th align="left" valign="top" width="16%"><?=t($t_base.'fields/forwarder')?></th>
				
				<th align="left" valign="top" width="12%"><?=t($t_base.'fields/from')?><span onclick="sort_cc_table('1','from','ASC')" id="from" class="cursor-pointer"><span class="sort-arrow-up-grey"></span></span></th>
				<th align="left" valign="top" width="10%"><?=t($t_base.'fields/cfs_name')?></th>
				
				<th align="left" valign="top" width="12%" ><?=t($t_base.'fields/to')?><span onclick="sort_cc_table('2','to','ASC')" id="to" class="cursor-pointer"><span class="sort-arrow-up-grey"></span></span></th>
				<th align="left" valign="top" width="10%"><?=t($t_base.'fields/cfs_name')?></th>
				<th align="center" valign="top" width="10%" colspan="2"><?=t($t_base.'fields/export_rate')?></th>

				<th align="center" valign="top" width="10%" colspan="2"><?=t($t_base.'fields/import_rate')?></th>
			
			</tr>
			<?php 
                        if($bottomContent!= array())
			{	
				?>
				<input type="hidden" name="sortBy" id="sortBy" value="">
				<input type="hidden" name="orderBy" id="orderBy" value="">				
				<?php
				$count = 1;
				foreach($bottomContent as $content)
				{	
				?>
				<tr id="cc_<?=$count++?>" onclick="showCustomClearence(this.id,'<?=$content['idCCExport']?>','<?=$content['idCCImport']?>')">
					<td><?=returnLimitData($content['szDisplayName'],15)?></td>
					
					<td><?=returnLimitData($content['cityFrom'],10)?>&nbsp;(<?=$content['countryFrom']?>)</td>
					<td><?=returnLimitData($content['wareHouseFrom'],8)?></td>
					
					<td><?=returnLimitData($content['cityTo'],10)?>&nbsp;(<?=$content['countryTo']?>)</td>
					<td><?=returnLimitData($content['wareHouseTo'],8)?></td>
					<td><?=($content['iActiveFrom']?$content['iExportCurrency']['szCurrency']:'')?></td>
					<td style="text-align: right;"><?=($content['iActiveFrom']?$content['iExportRate']:'')?></td>
					<td><?=($content['iActiveTo']?$content['iImportCurrency']['szCurrency']:'')?></td>
					<td style="text-align: right;"><?=($content['iActiveTo']?$content['iImportRate']:'')?></td>
				</tr>
				<?
				}
			}else{ echo "<tr><td colspan='11' align = 'center'><b>".t($t_base.'fields/no_content_to_dispay')."</b></td></tr>";}?>
			</table>
			
			<?
			if($bottomContent!= array())
			{
				echo '<div class="page_nav">';
					if($kPagination->links_per_page>1)
					{
						echo $kPagination->renderFullNav();
					}
				echo '</div>';
			}
			?>
			<br>
			<div style="float: right;"><a id="editCustomClearence" class="button1" style="opacity:0.4;"><span><?=t($t_base.'fields/edit')?></span></a></div>
		
	<?php
	}
	function findcityPriroity($cityPostCodeAry = '')
	{	
		$final_ary = array();
		$countr = 0;
		foreach($cityPostCodeAry as $cityPostCodeArys)
		{	
			if(!empty($cityPostCodeArys['szCity']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity'] ;
			}	
			else if(!empty($cityPostCodeArys['szCity1']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity1'] ;
			}	
			else if(!empty($cityPostCodeArys['szCity2']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity2'] ;
			}
			else if(!empty($cityPostCodeArys['szCity3']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity3'] ;
			}	
			else if(!empty($cityPostCodeArys['szCity4']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity4'] ;
			}
			else if(!empty($cityPostCodeArys['szCity5']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity5'] ;
			}	
			else if(!empty($cityPostCodeArys['szCity6']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity6'] ;
			}
			else if(!empty($cityPostCodeArys['szCity7']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity7'] ;
			}	
			else if(!empty($cityPostCodeArys['szCity8']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity8'] ;
			}
			else if(!empty($cityPostCodeArys['szCity9']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity9'] ;
			}	
			else if(!empty($cityPostCodeArys['szCity10'])) 
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szCity10'] ;
			}
			else if(!empty($cityPostCodeArys['szRegion1'])) 
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szRegion1'] ;
			}				
			else if(!empty($cityPostCodeArys['szRegion2'])) 
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szRegion2'] ;
			}				
			else if(!empty($cityPostCodeArys['szRegion3'])) 
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szRegion3'] ;
			}
			else if(!empty($cityPostCodeArys['szRegion4']))
			{
				$final_ary[$countr][0] = $cityPostCodeArys['szRegion4'] ;
			}
			$final_ary[$countr][1] = $cityPostCodeArys['szCountryName'];
			$final_ary[$countr][2] = $cityPostCodeArys['iPriority'];
			$final_ary[$countr][3]  = $cityPostCodeArys['id'];
			$countr++;
		}
		return $final_ary;
	}
function display_obo_lcl_bottom_admin_html($t_base,$disable_feilds,$pricingWtwAry=array(),$formData=false,$field_name=false)
{	
    $kConfig = new cConfig();
    $frequencyAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_FREQUENCY__);
    $weekDaysAry = $kConfig->getAllWeekDays();
    $cutoffLocalTimeAry = fColumnData();
    $allCurrencyArr=$kConfig->getBookingCurrency(false,false,false,true);
    $cuttOffAry = cutoff_key_value_pair();
    if($disable_feilds)
    {
        $disabled_str = 'disabled="disabled"';
        $opacity_str = 'style="opacity:0.4;"';
    }
    else
    {
        $textmss=t($t_base."title/get_data");
        $textmss1=t($t_base."fields/change_data");
        $save_onclick = ' onclick = "update_obo_lcl_data(\''.$textmss.'\',\''.$textmss1.'\');"';
        $cancel_onclick = ' onclick ="cancel_obo_lcl(\''.(isset($pricingWtwAry["id"])?$pricingWtwAry["id"]:'').'\')"';
        $clear_data_onclick = " onclick ='clear_all_data_obo_lcl()'";
    }
    if(!empty($pricingWtwAry))
    {
        $field_name="editOBOLCLData";
        $display = 'none';
        $submit_button_value = t($t_base.'title/save');
    }
    else
    {
        $field_name="addOBOLCLData";
        $submit_button_value = t($t_base.'title/save');
        $display = 'block';
    } 
    $idOriginWarehouse = $formData['idOriginWarehouse'];
    $idDestinationWarehouse = $formData['idDestinationWarehouse'];

    $em_style="style='font-style:normal;'" ;
    $kWHSSearch = new cWHSSearch();
    $illustration_text = '';
    if($formData['idOriginWarehouse']>0)
    {
        $kWHSSearch->load($formData['idOriginWarehouse']);
        $iWarehouseType = $kWHSSearch->iWarehouseType;
        $illustration_text .= " from ".$kWHSSearch->szWareHouseName ;
        if(strlen($kWHSSearch->szWareHouseName)>30)
        {
            $szOriginWhsName = mb_substr($kWHSSearch->szWareHouseName,0,27,'UTF8')."..";
        }
        else
        {
            $szOriginWhsName = $kWHSSearch->szWareHouseName ;
        }
    }

    if($formData['idDestinationWarehouse']>0)
    {
        $kWHSSearch->load($formData['idDestinationWarehouse']);
        if($iWarehouseType<=0)
        {
            //This situation never arise but still have added this code for security reasons.
            $iWarehouseType = $kWHSSearch->iWarehouseType;
        }
        $illustration_text .= " to ".$kWHSSearch->szWareHouseName ;
        if(strlen($kWHSSearch->szWareHouseName)>30)
        {
            $szDestinationWhsName = mb_substr($kWHSSearch->szWareHouseName,0,27,'UTF8').'..';
        }
        else
        {
            $szDestinationWhsName = $kWHSSearch->szWareHouseName ;
        }
    } 
    if($pricingWtwAry['iOriginChargesApplicable']!=1)
    {
        $pricingWtwAry['fOriginRateWM'] = (float)$pricingWtwAry['fOriginRateWM']>0?$pricingWtwAry['fOriginRateWM']:'';
        $pricingWtwAry['fOriginMinRateWM'] = (float)$pricingWtwAry['fOriginMinRateWM']>0?$pricingWtwAry['fOriginMinRateWM']:'';
        $pricingWtwAry['fOriginRate'] = (float)$pricingWtwAry['fOriginRate']>0?$pricingWtwAry['fOriginRate']:'';
    }	
    if($pricingWtwAry['iDestinationChargesApplicable']!=1)
    {
        $pricingWtwAry['fDestinationRateWM'] = (float)$pricingWtwAry['fDestinationRateWM']>0?$pricingWtwAry['fDestinationRateWM']:'';
        $pricingWtwAry['fDestinationMinRateWM'] = (float)$pricingWtwAry['fDestinationMinRateWM']>0?$pricingWtwAry['fDestinationMinRateWM']:'';
        $pricingWtwAry['fDestinationRate'] = (float)$pricingWtwAry['fDestinationRate']>0?$pricingWtwAry['fDestinationRate']:'';
    }
    
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    {
        $szPerWM = "Per KG";
        $szOriginChargeTipHeading = "";
        $szOriginChargeTipHeading = t($t_base.'messages/AIR_FREIGHT_ORIGIN_CHARGES_TOOL_TIP_HEADER_TEXT');
        $szFreightChargeTipHeading = t($t_base.'messages/AIR_FREIGHT_RATE_TOOL_TIP_HEADER_TEXT'); 
        $szDestinationChargeTipHeading = t($t_base.'messages/AIR_DESTINATION_CHARGES_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipHeading = t($t_base.'messages/AIR_SERVICE_FREQUENCY_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipText = t($t_base.'messages/AIR_SERVICE_FREQUENCY_TOOL_TIP_TEXT');
        $szCutOffTitle = t($t_base.'fields/airpot_cut_off_day');  
        $szCutoffTipHeading = t($t_base.'messages/AIRPORT_CUT_OFF_TOOL_TIP_HEADER_TEXT');
        $szTransitDaysTitle = t($t_base.'fields/airport_transit_hour'); 
        $szTransitDaysTipHeading = t($t_base.'messages/AIRPORT_TRANSIT_TOOL_TIP_HEADER_TEXT');
        $szAvailabelDaysTitle = t($t_base.'fields/airport_available_day'); 
        $szAvailableTipHeading = t($t_base.'messages/AIR_CFS_AVAILABLE_TOOL_TIP_HEADER_TEXT');
        $szIllustrationTitle = t($t_base.'title/illustration_of_air_service');
        $szImageName = "CargoFlowAirfreight.png"; 
        
        $szSectionTitle = t($t_base.'title/air_feright');
    }
    else
    {
        $szPerWM = t($t_base.'fields/per_wm'); 
        $szOriginChargeTipHeading = t($t_base.'messages/ORIGIN_CHARGES_TOOL_TIP_HEADER_TEXT');
        $szFreightChargeTipHeading = t($t_base.'messages/FRAIGHT_RATE_TOOL_TIP_HEADER_TEXT'); 
        $szDestinationChargeTipHeading = t($t_base.'messages/DESTINATION_CHARGES_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipHeading = t($t_base.'messages/SERVICE_FREQUENCY_TOOL_TIP_HEADER_TEXT'); 
        $szFrequencyTipText = "";
        $szCutOffTitle = t($t_base.'fields/cut_off_day'); 
        $szCutoffTipHeading = t($t_base.'messages/CFS_CUT_OFF_TOOL_TIP_HEADER_TEXT');

        $szTransitDaysTitle = t($t_base.'fields/transit_hour'); 
        $szTransitDaysTipHeading = t($t_base.'messages/CFS_TRANSIT_TOOL_TIP_HEADER_TEXT');
        $szAvailabelDaysTitle = t($t_base.'fields/available_day');  
        $szAvailableTipHeading = t($t_base.'messages/CFS_AVAILABLE_TOOL_TIP_HEADER_TEXT');
        $szIllustrationTitle = t($t_base.'title/illustration_of_lcl');
        $szImageName = "CargoFlowLCL.png"; 
        
        $szSectionTitle = t($t_base.'title/obo_lcl_middle_box_heading');
    }
        
    ?>	
<script type="text/javascript">
function initDatePicker() 
{
	$("#datepicker1").datepicker();
	$("#datepicker2").datepicker();
}
initDatePicker();
</script>
<?php if($pricingWtwAry != array())
{	
	$whFromName = $kWHSSearch->selectWarehouseName($pricingWtwAry['idWarehouseFrom']);
	$whToName = $kWHSSearch->selectWarehouseName($pricingWtwAry['idWarehouseTo']);
	echo "<h4><strong> ".$szSectionTitle." ".t($t_base.'title/from') ." ".$whFromName." ".t($t_base.'title/to')." ".$whToName."</strong></h4>";
}?>
<form name="obo_lcl_bottom_form" id="obo_lcl_bottom_form" method="post">
	<div id="obo_cc_no_record_found" style="display:none;">
		<ul id="message_ul" style="padding:0px 5px 5px 35px;">
			<li><?=t($t_base.'title/obo_lcl_no_service_found');?></li>
		</ul>
	</div>	
	
	<!-- 
	<div id="pointer_div" onclick="point_it(event)">
	<img src="<?=__BASE_STORE_IMAGE_URL__?>/CargoFlowLCL.png" id="cross" >
	</div>
	 -->
	<br/>
	<div id="obo_lcl_error"  class="errorBox" style="display:none;"></div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/origin_freight_rate');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szOriginChargeTipHeading;?>','<?=t($t_base.'messages/ORIGIN_CHARGES_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="<?=$field_name?>[fOriginRateWM]" id="fOriginRateWM" value="<?=$pricingWtwAry['fOriginRateWM']?>" type="text" onkeyup="check_applicable_charges_checkbox('ORIGIN');" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fOriginMinRateWM]" id="fOriginMinRateWM" value="<?=$pricingWtwAry['fOriginMinRateWM']?>" onkeyup="check_applicable_charges_checkbox('ORIGIN');" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fOriginRate]" id="fOriginRate" value="<?=$pricingWtwAry['fOriginRate']?>" size="10" onkeyup="check_applicable_charges_checkbox('ORIGIN');" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="<?=$field_name?>[szOriginFreightCurrency]" id="szOriginFreightCurrency" onchange="check_applicable_charges_checkbox('ORIGIN');" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($allCurrencyArr))
				{
					foreach($allCurrencyArr as $allCurrencyArrs)
					{
						?><option value="<?=$allCurrencyArrs['id']?>" <?=($pricingWtwAry['szOriginRateCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
		<input name="<?=$field_name?>[iOriginChargesApplicable]" id="iOriginChargesApplicable" value="<?=$pricingWtwAry['iDestinationChargesApplicable']?>" type="hidden" />
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/freight_rate');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szFreightChargeTipHeading;?>','<?=t($t_base.'messages/FRAIGHT_RATE_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="<?=$field_name?>[fRateWM]" id="fRateWM" value="<?=(isset($pricingWtwAry['fRateWM'])?$pricingWtwAry['fRateWM']:'')?>" type="text" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fMinRateWM]" id="fMinRateWM" value="<?=(isset($pricingWtwAry['fMinRateWM'])?$pricingWtwAry['fMinRateWM']:'')?>" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fRate]" id="fRate" value="<?=(isset($pricingWtwAry['fRate'])?$pricingWtwAry['fRate']:'')?>" size="10" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="<?=$field_name?>[szFreightCurrency]" id="szFreightCurrency" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($allCurrencyArr))
				{
					foreach($allCurrencyArr as $allCurrencyArrs)
					{
						?><option value="<?=$allCurrencyArrs['id']?>" <?=($pricingWtwAry['szFreightCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/destination_freight_rate');?> <a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szDestinationChargeTipHeading;?>','<?=t($t_base.'messages/DESTINATION_CHARGES_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=$szPerWM;?></span><br /><input <?=$disabled_str?> name="<?=$field_name?>[fDestinationRateWM]" id="fDestinationRateWM" value="<?=$pricingWtwAry['fDestinationRateWM']?>" type="text" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/minimum');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fDestinationMinRateWM]" id="fDestinationMinRateWM" value="<?=$pricingWtwAry['fDestinationMinRateWM']?>" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/per_booking');?></span><br /><input <?=$disabled_str?> type="text" name="<?=$field_name?>[fDestinationRate]" id="fDestinationRate" value="<?=$pricingWtwAry['fDestinationRate']?>" size="10" onkeyup="check_applicable_charges_checkbox('DESTINATION');" /></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="<?=$field_name?>[szDestinationFreightCurrency]" id="szDestinationFreightCurrency" onchange = "check_applicable_charges_checkbox('DESTINATION');">
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($allCurrencyArr))
				{
					foreach($allCurrencyArr as $allCurrencyArrs)
					{
						?><option value="<?=$allCurrencyArrs['id']?>" <?=($pricingWtwAry['szDestinationRateCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
		<input name="<?=$field_name?>[iDestinationChargesApplicable]" id="iDestinationChargesApplicable" value="<?=$pricingWtwAry['iDestinationChargesApplicable']?>" type="hidden" />
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/validity');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/VALIDITY_TOOL_TIP_HEADER_TEXT');?>','<?=t($t_base.'messages/VALIDITY_TOOL_TIP_TEXT');?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/from');?></span><br /><input size="10" <?=$disabled_str?> id="datepicker1" name="<?=$field_name?>[dtValidFrom]" type="text" value="<? if($pricingWtwAry['dtValidFrom']!='0000-00-00 00:00:00' && !empty($pricingWtwAry['dtValidFrom'])) {?><?=date('d/m/Y',strtotime($pricingWtwAry['dtValidFrom']))?><? }?>"></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/to');?></span><br /><input size="10" <?=$disabled_str?> id="datepicker2" name="<?=$field_name?>[dtExpiry]" type="text" value="<? if($pricingWtwAry['dtExpiry']!='0000-00-00 00:00:00' && !empty($pricingWtwAry['dtExpiry'])) {?><?=date('d/m/Y',strtotime($pricingWtwAry['dtExpiry']))?><? }?>"></p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/booking_cut_off');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/BOOKING_CUT_OFF_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-20"><span class="f-size-12">&nbsp;</span><br />
			<select size="1" style="width:90%;" <?=$disabled_str?> name="<?=$field_name?>[iBookingCutOffHours]" id="iBookingCutOffHours" >
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($cuttOffAry))
					{
						foreach($cuttOffAry as $key=>$cuttOffArys)
						{
							?><option value="<?=$key?>" <?=($pricingWtwAry['iBookingCutOffHours'] ==  $key ) ? "selected":""?>><?=$cuttOffArys?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=t($t_base.'fields/service_frequency');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szFrequencyTipHeading;?>','<?php echo $szFrequencyTipText; ?>','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-20"><span class="f-size-12">&nbsp;</span><br />
			<select size="1" style="width:90%;" <?=$disabled_str?> name="<?=$field_name?>[idFrequency]" id="idFrequency" >
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($frequencyAry))
					{
						foreach($frequencyAry as $frequencyArys)
						{
							?><option value="<?=$frequencyArys['id']?>" <?=($pricingWtwAry['idFrequency'] == $frequencyArys['id'] ) ? "selected":""?>><?=$frequencyArys['szDescription']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=$szCutOffTitle;?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szCutoffTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15" ><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/weekday');?></span><br />
			<select size="1" style="width:90%;" <?=$disabled_str?> name="<?=$field_name?>[idCutOffDay]" onchange="getAvailableDay();" id="idCutOffDay" >
				<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($weekDaysAry))
				{
					foreach($weekDaysAry as $weekDaysArys)
					{
						?><option value="<?=$weekDaysArys['id']?>" <?=($pricingWtwAry['idCutOffDay'] ==  $weekDaysArys['id'] ) ? "selected":""?>><?=$weekDaysArys['szWeekDay']?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/local_time');?></span><br />
			<select size="1" style="width:90%;" <?=$disabled_str?> name="<?=$field_name?>[szCutOffLocalTime]" id="szCutOffLocalTime" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($cutoffLocalTimeAry))
				{
					foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
					{
						?><option value="<?=$cutoffLocalTimeArys?>" <?=(trim($pricingWtwAry['szCutOffLocalTime']) ==  $cutoffLocalTimeArys) ? "selected":""?>><?=$cutoffLocalTimeArys?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=$szTransitDaysTitle;?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szTransitDaysTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/days');?></span><br /><input <?=$disabled_str?> id="iTransitHours"  name="<?=$field_name?>[iTransitHours]" value="<?=$pricingWtwAry['iTransitDays']?>" onblur="getAvailableDay();" type="text" size="10" /></p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field"><?=$szAvailabelDaysTitle;?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=$szAvailableTipHeading;?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<p class="fl-15" ><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/weekday');?></span><br />
			<input type="text" size="10" disabled name="<?=$field_name?>[idAvailableDay_dis]" value="<?=$pricingWtwAry['szAvailableDay']?>" id="idAvailableDay" />			
			<input type="hidden" name="<?=$field_name?>[idAvailableDay]" id="idAvailableDay_hidden" value="<?=$pricingWtwAry['idAvailableDay']?>" />
		</p>
		<p class="fl-15"><span class="f-size-12" <?=$em_style?>><?=t($t_base.'fields/local_time');?></span><br />
			<select size="1" style="width:90%;" <?=$disabled_str?> name="<?=$field_name?>[szAvailableLocalTime]" id="szAvailableLocalTime" >
			<option value=""><?=t($t_base.'fields/select');?></option>
			<?php
				if(!empty($cutoffLocalTimeAry))
				{
					foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
					{
						?><option value="<?=$cutoffLocalTimeArys?>" <?=($pricingWtwAry['szAvailableLocalTime'] ==  $cutoffLocalTimeArys) ? "selected":""?>><?=$cutoffLocalTimeArys?></option>
						<?php
					}
				}
			?>
			</select>
		</p>
	</div>
	
	<br />
	<div class="oh" style="float: right;">
		<p class="fl-40">
		<p class="fl-20" align="right"><a href="javascript:void(0);" <?=$save_onclick?> <?=$opacity_str?> class="button1" id="save_lcl"><span><?=$submit_button_value?></span></a></p>
	</div>
	<div style="clear: both"></div>
	<input type="hidden" name="<?=$field_name?>[idOriginWarehouse]" id="idOriginWarehouse_hidden" value="<?=$idOriginWarehouse?>">
	<input type="hidden" name="<?=$field_name?>[idDestinationWarehouse]" id="idDestinationWarehouse_hidden" value="<?=$idDestinationWarehouse?>">
	<input type="hidden" name="<?=$field_name?>[id]" value="<?=$pricingWtwAry['id']?>">
</form>	
<hr>
<?
}	
	function expiringWTW($bottomContent)
	{		
		$t_base = "management/expiringWTW/";
	?>
		<table cellpadding="0" cellspacing="0" class="format-4" width="100%" id="customClearence">
			<tr>
				<th align="left" valign="top" width="14%">
				<a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_expiring_table('fwd.szDisplayName','ASC','forwarder')" id="forwarder">
					<span class="sort-arrow-up-grey">&nbsp;</span>
				</a>
				<?=t($t_base.'fields/forwarder')?></th>
				<th align="left" valign="top" width="15%">
				<a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_expiring_table('w.szCity','ASC','warehouseFrom')" id="warehouseFrom">
					<span class="sort-arrow-up-grey">&nbsp;</span>
				</a>
				<?=t($t_base.'fields/from')?></th>
				<th align="left" valign="top" width="15%"><?=t($t_base.'fields/cfs_name')?></th>
				<th align="left" valign="top" width="15%">
				<a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_expiring_table('w2.szCity','ASC','warehouseTo')" id="warehouseTo">
					<span class="sort-arrow-up-grey">&nbsp;</span>
				</a>
				<?=t($t_base.'fields/to')?></th>
				<th align="left" valign="top" width="15%"><?=t($t_base.'fields/cfs_name')?></th>
				<th align="left" valign="top" width="10%">
				<a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_expiring_table('pw.dtExpiry','ASC','expiring')" id="expiring">
					<span class="sort-arrow-down">&nbsp;</span>
				</a>
				<?=t($t_base.'fields/expirint_wtw')?></th>
				<th align="left" valign="top" width="4%"><?=t($t_base.'fields/origin_haulage')?></th>
				<th align="left" valign="top" width="4%"><?=t($t_base.'fields/cc')?></th>
				<th align="left" valign="top" width="4%"><?=t($t_base.'fields/destination_haulage')?></th>
				<th align="left" valign="top" width="4%"><?=t($t_base.'fields/cc')?></th>
			</tr>
			<? if($bottomContent!= array())
			{	
				$count = 1;
				foreach($bottomContent as $content)
				{	
					/**
					*
					* FINDING WAREHOUSE DETAILS USING ID
					* 
					* */
					$kAdmin = new cAdmin;
					
				
				?>
				<tr id="cc_<?=$count++?>" onclick="showexpiryCC(this.id,'<?=$content['id']?>')">
					<td><?=returnLimitData($content['szDisplayName'],13)?></td>
					<td><?=returnLimitData($content['szCityFrom'],8)?> (<?=$content['szCountryISOFrom']?>)</td>
					
					<td><?=returnLimitData($content['szWarehouseFrom'],12)?></td>
					<td><?=returnLimitData($content['szCityTo'],8)?> (<?=$content['szCountryISOTo']?>)</td>
					
					<td><?=returnLimitData($content['szWarehouseTo'],12)?></td>
					<td><?=DATE('d/m/Y',strtotime($content['dtExpiry']))?></td>
					<td><?=($kAdmin->countHaulagePricing($content['idWarehouseFrom'])?'Yes':'No')?></td>
					<td><?=($kAdmin->countCCPricing($content['idWarehouseFrom'],$content['idWarehouseTo'],1)?'Yes':'No')?></td>
					<td><?=($kAdmin->countHaulagePricing($content['idWarehouseTo'])?'Yes':'No')?></td>
					<td><?=($kAdmin->countCCPricing($content['idWarehouseFrom'],$content['idWarehouseTo'],2)?'Yes':'No')?></td>
				</tr>
				<?
				}
			}else{ echo "<tr><td colspan='11' align = 'center'><b>".t($t_base.'fields/no_content_to_dispay')."</b></td></tr>";}?>
			</table>
	<?
	}
	function haulageListing($bottomContent)
	{	//print_r($bottomContent);
		$t_base = "management/haualgeListing/";
		$kAdmin = new cAdmin();
		?>
		<table cellpadding="0" cellspacing="0" class="format-4" width="100%" id="haulageDetails">
			<tr>
				<th align="left" valign="top" width="18%"><?=t($t_base.'fields/forwarder')?></th>
				<th align="left" valign="top" width="12%"><?=t($t_base.'fields/country')?></th>
				<th align="left" valign="top" width="10%"><?=t($t_base.'fields/city')?></th>
				<th align="left" valign="top" width="15%"><?=t($t_base.'fields/cfs')?></th>
				<th align="left" valign="top" width="10%"><?=t($t_base.'fields/direction')?></th>
				<th align="left" valign="top" width="15%"><?=t($t_base.'fields/pricing_model')?></th>
				<th align="left" valign="top" width="10%"><?=t($t_base.'fields/status')?></th>
				<th align="left" valign="top" width="8%"><?=t($t_base.'fields/content')?>/<?=t($t_base.'fields/active')?></th>
			</tr>
			<? if($bottomContent!= array())
			{	
				foreach($bottomContent as $contents)
				{	
				?>
				<?
						$total = 0;
						$active = 0;
						$total = $kAdmin->fetchTotalPricingHaulage($contents['idWarehouse'],$contents['iDirection'],$contents['idHaulage']);
						$active  = $kAdmin->fetchActivePricingHaulage($contents['idWarehouse'],$contents['iDirection'],$contents['idHaulage']);
				if(isset($_POST['currentHaulage']['iPricing']) && $_POST['currentHaulage']['iPricing'] ==1 )
				{
					if($total>0 && $active >0)
					{
						$activeMode = 1;
					}
					else
					{
						$activeMode = 0;
					}
				}
				else if(isset($_POST['currentHaulage']['iPricing']) && $_POST['currentHaulage']['iPricing'] ==2 )
				{
					
					if($total <= 0 || $active <= 0)
					{
						$activeMode = 1;
					}
					else
					{
						$activeMode = 0;
					}
				}
				else
				{
					$activeMode = 1;
				}	
					if($activeMode)
					{		
					?>
				<tr id="show_haulage_details_<?=++$count;?>" onclick="selectHaulageField(this.id,<?=$contents['id']?>,<?=$contents['iPriority']?>,<?=$contents['iActive']?>,<?=$contents['idCountry']?>,'<?=$contents['szCity']?>','<?=$contents['idWarehouse']?>',<?=$contents['iDirection']?>);">
					<td><?=returnLimitData($contents['szDisplayName'],17)?></td>
					<td><?=returnLimitData($contents['szCountryName'],10)?></td>
					<td><?=returnLimitData($contents['szCity'],8)?></td>
					<td><?=returnLimitData($contents['szWareHouseName'],12)?></td>
					<td><?=(($contents['iDirection']==1)?'Export':'Import')?></td>
					<td><?=$contents['szModelShortName']?></td>
					<td><?=($contents['iActive']?($contents['iPriority']." Priority"):'Inactive') ?></td>
					
					<td id="active"><?=$total."/".$active;?></td>
				</tr>
				<?
					}
				}
			}else{ echo "<tr><td colspan='11' align = 'center'><b>".t($t_base.'fields/no_content_to_dispay')."</b></td></tr>";}?>
			</table>
			<br />
			<!-- <p><?=t($t_base.'message/show_only')?> <input type="checkbox" checked="checked" id="chunchHaulage" onchange="haulageListing()"></p> -->
			
			<div style="float: left;">
				<?=t($t_base.'message/priority')?> 
				<a class="button2" style="opacity:0.4;" id="moveHauageUp"><span><?=t($t_base.'fields/up')?></span></a>
				<a class="button2" style="opacity:0.4;" id="moveHauageDown"><span><?=t($t_base.'fields/down')?></span></a>
			</div>
				<div style="float: right;">
				<a class="button1" style="opacity:0.4;" id="makeHaulageActivated"><span><?=t($t_base.'fields/activeate')?></span></a>
				<a class="button1" style="opacity:0.4;" id="haulageEdit"><span><?=t($t_base.'fields/edit')?></span></a>
			</div>
			<div style="clear: both;"></div>
<?php		
	}


function display_cancelled_booking_html($searchResult,$t_base,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)  
{	
	$table_width = (int)(100/10);
        $iGrandTotal=count($searchResult);
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
        //echo $limit;
        if($iPage==1)
        {
            $start=0;
            $end=$limit;
            if($iGrandTotal<$end)
            {
                $end=$iGrandTotal;
            }
        }
        else
        {
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            $startValue=$start+1;
            if($startValue>$iGrandTotal)
            {
                $iPage=$iPage-1;
                $start=$limit*($iPage-1);
                $end=$iPage*$limit;
                if($end>$iGrandTotal)
                {
                    $end=$iGrandTotal;
                }

            }
        }
	?>
	<table cellpadding="0" id="booking_table" cellspacing="0" class="format-4" width="100%">
            <tr>
                <th width="10%" valign="top"><?=t($t_base.'fields/date')?></th>
                <th width="10%" valign="top"><?=t($t_base.'fields/booking')?></th>
                <th width="10%" valign="top"><?=t($t_base.'fields/service');?></th>
                <th width="14%" valign="top"><?=t($t_base.'fields/from');?></th>
                <th width="14%" valign="top"><?=t($t_base.'fields/to');?></th>
                <th width="10%" valign="top"><?=t($t_base.'fields/wm');?> </th>
                <th width="13%" valign="top" style="text-align: right;"><?=t($t_base.'fields/value');?></th>
                <th width="21%" valign="top"><?=t($t_base.'fields/forwarder');?></th>
            </tr>
            <?php
            if(!empty($searchResult))
            {
                $ctr = 1;
                for($i=$start;$i<$end;++$i)
                {
                    $szServiceType='';
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
                    {
                            $szServiceType = "DTD" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTW__) //DTW
                    {
                            $szServiceType = "DTW" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTD__) //WTD
                    {
                            $szServiceType = "WTD" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTW__) //WTW
                    {
                            $szServiceType = "WTW" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTP__) //DTD
                    {
                            $szServiceType = "PTP" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTP__) //DTW
                    {
                            $szServiceType = "DTP" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTD__) //WTD
                    {
                            $szServiceType = "PTD" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTP__) //WTW
                    {
                            $szServiceType = "WTP" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTW__) //WTW
                    {
                            $szServiceType = "PTW" ;
                    }
                    
                    if($searchResult[$i]['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__ || $searchResult[$i]['iCourierBooking']==1)
                    {
                        $szWarehouseFromCity=createSubString($searchResult[$i]['szShipperCity'],0,10);
                        $szWarehouseToCity=createSubString($searchResult[$i]['szConsigneeCity'],0,10); 
                        $idWarehouseFromCountry = $searchResult[$i]['idOriginCountry'] ;
                        $idWarehouseToCountry = $searchResult[$i]['idDestinationCountry'] ;
                    }
                    else
                    {  
                        $szWarehouseFromCity=createSubString($searchResult[$i]['szWarehouseFromCity'],0,10); 
                        $szWarehouseToCity=createSubString($searchResult[$i]['szWarehouseToCity'],0,10); 
                        $idWarehouseFromCountry = $searchResult[$i]['idWarehouseFromCountry'] ;
                        $idWarehouseToCountry = $searchResult[$i]['idWarehouseToCountry'] ;
                    }
                    
                    if(!empty($searchResult[$i]['szTransportMode']))
                    {
                        $szServiceType = $searchResult[$i]['szTransportMode']." ".$szServiceType ;
                    }
                    else
                    {
                        $szServiceType = "Sea ".$szServiceType ;
                    }
                     if($searchResult[$i]['iCourierBooking']==1)
                     {
                            $szServiceType="Courier";
                    }
                    ?>						
                    <tr id="booking_data_<?=$ctr?>" onclick="select_cancelled_booking_tr('booking_data_<?=$ctr?>','<?=$searchResult[$i]['id']?>')">
                            <td><?=(!empty($searchResult[$i]['dtBookingCancelled']) && $searchResult[$i]['dtBookingCancelled']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResult[$i]['dtBookingCancelled'])):''?></td>
                            <td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $searchResult[$i]['szBookingRandomNum']."/";?>"><?=$searchResult[$i]['szBookingRef']?></a></td>
                            <td><?=$szServiceType; ?></td>
                            <td><?=$szWarehouseFromCity." (".isoidCountryCode($idWarehouseFromCountry).")"?></td>
                            <td><?=$szWarehouseToCity." (".isoidCountryCode($idWarehouseToCountry).")"?></td>
                            <td><?=(get_formated_cargo_measure($searchResult[$i]['fCargoWeight']/1000)) ." / ".format_volume($searchResult[$i]['fCargoVolume'])?></td>
                            <td style="text-align: right;"><?=($searchResult[$i]['szCustomerCurrency']?$searchResult[$i]['szCustomerCurrency']." ".number_format($searchResult[$i]['fTotalPriceCustomerCurrency'],2):'0.00')?></td>
                            <td><?=returnLimitData($searchResult[$i]['szDisplayName'],16)?></td>
                    </tr>
                    <?php
                    $ctr++;
                }
            }
            else
            {
                ?>
                <tr>
                    <td colspan="11" align="center"><h4><b><?=t($t_base.'messages/no_boooking_found');?></b></h4></td>
                </tr>
                <?php
            }
        ?>
	</table>
	<div id="invoice_comment_div" style="display:none">
	</div>
        <div class="page_limit clearfix">
        <div class="pagination">
            <?php

                $kPagination->szMode = "DISPLAY_BOOKING_LIST_PAGINATION";
                //$kPagination->idBookingFile = $idBookingFile;
                $kPagination->paginate('display_booking_list_pagination'); 
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }        
            ?>
               </div>
               <div class="limit">Records per page
                   <select id="limit" name="limit" onchange="display_booking_list_pagination('1')">
                       <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                        <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                        <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                        <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                   </select>
               </div>    
            </div>
	<br>
	<div>
            <span style="float: right;">
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_confirmation"><span><?=t($t_base.'fields/confirmation');?></span></a>&nbsp;
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_invoice"><span><?=t($t_base.'fields/invoice');?></span></a>&nbsp;
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_credit_note"><span><?=t($t_base.'fields/credit_note');?></span></a>&nbsp;
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_lcl_booking"><span><?=t($t_base.'fields/view_booking');?></span></a>
            </span>
	</div>
        <div class='clearfix'></div>
	<?php
        
}
function showUploadServiceCommentForAdmin($allCommentArr,$id)
{
$t_base="BulkUpload/";
$kForwarder = new  	cForwarder();
?>
<div id="popup-bg"></div>
<div id="popup-container">
	<div class="multi-bulk-upload-comment-popup popup" style="width:400px;">
		<strong><?=t($t_base.'title/comment')?></strong><br /><br />
		<? if(!empty($allCommentArr))
			{
				$kForwarder = new  	cForwarder();
			?>
		<div class="multi-comment-popup" style="border:1px solid #D0D0D0;height:150px;width:400px">
		<? 
				foreach($allCommentArr as $allCommentArrs)
				{
					$kForwarder->load($allCommentArrs['idForwarder']);
				?>
					<!-- <p style="margin-left:7px;"><?=$allCommentArrs['szForwarderContactName']?>, <?=$kForwarder->szDisplayName?>, <?=date('d.F Y',strtotime($allCommentArrs['dtComment']))?></p> -->
					<p><?=str_replace("<br/>","\n",$allCommentArrs['szComment'])?></p>
				<?	
				}
			?>		
		</div>
		<br/>
		<? }
		?>	
		<div>
		<p><?=t($t_base.'title/would_you_like_to_add_a_comment')?></p>
		<textarea cols="50" rows="5" id="szComment" name="szComment" style="width:396px;color:grey;font-style:italic;" onkeydown="blank_me_comment(this.id,'Type to add comments');" onkeyup="active_add_comment_button('<?=$id?>');" onblur="show_me(this.id,'Type to add comments');">Type to add comments</textarea>
		
		</div>
	<br/>
	<p align="center"><a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_comment_cost_processing"><span><?=t($t_base.'fields/add_comment')?></span></a><a href="javascript:void(0)" class="button1" onclick="cancelEditMAnageVar(0)" id="cancel_cost_processing"><span><?=t($t_base.'fields/close')?></span></a></p>
	</div>
</div>
<?}
function showForwarderPricingCurrencyUpdatePopup($idCurrency)
{
	$t_base = "management/currency/";
	$kConfig= new cConfig();
	$kForwarder= new cForwarder();
	$szCurrenyArr=$kConfig->getBookingCurrency($idCurrency);
	$kAdmin = new cAdmin();
	$forwarderUpdatedArr=$kAdmin->getForwarderIdForUpdatingCurrencyStatus($idCurrency);
?>
	<div id="inactivate_services">
		<div id="popup-bg"></div>
			<div id="popup-container">			
				<div class="popup abandon-popup" style="margin-top: -23px;">
					<form name="inActivateService" id="inActivateService" method="post">
					<h4><b><?=t($t_base.'messages/inactivate_pricing_currency');?> <?=$szCurrenyArr[0]['szCurrency']?></b></h4><br/>
					<p style="text-align:justify"><?=t($t_base.'messages/inactive_currency_popup_line_2')?></p>
					<br/>
					<div class="tableScroll" style="height:119px;overflow: auto;border: 1px solid #BFBFBF;padding: 0 3px;">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="position:relative;">
							<tr>
								<th style="width:50%"><b><?=t($t_base.'fields/forwarder')?></b></th>
								<th style="width:30%"><b><?=t($t_base.'fields/pricing_type')?></b></th>
								<th style="width:20%"><b><?=t($t_base.'fields/pricing_lines')?></b></th>
							</tr>
							<? if(!empty($forwarderUpdatedArr)){
									$forwardIdArr=array();
									foreach($forwarderUpdatedArr as $key=>$value)
									{
									
										$str_line='';
										$kForwarder->load($key);
										if((int)$value['customClearance']>0)
										{	
											$str_line .=$key."_c";							
										?>
											<tr>
												<td class="noborder"><?=$kForwarder->szDisplayName?></td>
												<td><?=t($t_base.'fields/customclearance');?></td>
												<td><?=$value['customClearance']?> <? if($value['customClearance']>1) { echo t($t_base.'fields/lines'); }else { echo t($t_base.'fields/line'); }?></td>
											</tr>
										<?
										}
										if((int)$value['lclService']>0)
										{	
											if($str_line!='')
											{
												$str_line .="_l";
											}
											else
											{
												$str_line .=$key."_l";
											}							
										?>
											<tr>
												<td class="noborder"><?=$kForwarder->szDisplayName?></td>
												<td><?=t($t_base.'fields/lclservice');?></td>
												<td><?=$value['lclService']?> <? if($value['lclService']>1) { echo t($t_base.'fields/lines'); }else { echo t($t_base.'fields/line'); }?></td>
											</tr>
										<?
										}
										if((int)$value['haulagePricing']>0)
										{	

											if($str_line!='')
											{
												$str_line .="_h";
											}
											else
											{
												$str_line .=$key."_h";
											}
										?>
											<tr>
												<td class="noborder"><?=$kForwarder->szDisplayName?> </td>
												<td><?=t($t_base.'fields/haulage');?></td>
												<td><?=$value['haulagePricing']?> <? if($value['haulagePricing']>1) { echo t($t_base.'fields/lines'); }else { echo t($t_base.'fields/line'); }?></td>
											</tr>
										<?
										}

										if(!in_array($key,$forwardIdArr))
										{
											$forwardIdArr[]=$key;
											$forwardStrArr[]=$str_line;
										}
									}
									
									if(!empty($forwardStrArr))
									{
										$forwarderArrStr=implode(";",$forwardStrArr);
									}
							}
							?>
						</table>
					</div>
					<br/>
					<p style="text-align:justify"><?=t($t_base.'messages/inactive_currency_popup_line_3')?></p>
					<br/>
					<p style="text-align:justify"><?=t($t_base.'messages/dear_forwarder')?>,</p>
					<p><textarea style="width:506px;" rows="2" cols="65" name="inactivateServiceArr[szText]" id="szText" onkeyup="update_confirm_button();"><?=$_POST['inactivateServiceArr']['szText']?></textarea></p><br/>
					<p style="text-align:justify"><em><?=t($t_base.'messages/links_to_upload_files_pasted_here')?></em></p><br/>
					<p style="text-align:justify"><?=t($t_base.'messages/inactive_currency_popup_line_4')?> <?=$szCurrenyArr[0]['szCurrency']?> <?=t($t_base.'messages/inactive_currency_popup_line_5')?></p><br/>
					<p style="text-align:justify"><?=t($t_base.'messages/kind_regards')?></p>
					<p style="text-align:justify"><?=t($t_base.'messages/transporteca')?></p><br/>
					<p align="center"> 
					<input type="hidden" name="inactivateServiceArr[idForwarderStr]" id="idForwarderStr" value="<?=$forwarderArrStr?>">
					<input type="hidden" name="inactivateServiceArr[idCurrency]" id="idCurrency" value="<?=$idCurrency?>">
					<a href="javascript:void(0)" id="confirm_button" style="opacity:0.4" class="button1"><span><?=t($t_base.'fields/confirm')?></span></a> <a href="javascript:void(0)" id="cancel_button" onclick="cancel_inactivate_curreny();" class="button2"><span><?=t($t_base.'fields/cancel')?></span></a></p>
					</form>
				</div>
			</div>
		</div>
		<script>
			$("#szText").focus();
		</script>	
<?php
} 
	function displayGmailSyncErrorMessage($gmailSyncErrorAry)
        {
            if(!empty($gmailSyncErrorAry))
            {
                echo '<div id="admin_header_gmail_sync_error_container">'; 
                foreach($gmailSyncErrorAry as $gmailSyncErrorArys)
                {
                    ?>
                    <div class="warning">
                        <p><?php echo $gmailSyncErrorArys['szErrorMessage'].". Sync with Gmail by clicking "; ?><a href="javascript:void(0);" onclick="manually_sync_with_gmail();">here</a></p>
                    </div>
                    <?php
                }
                echo "</div>";
            }
        }

function all_forwarder_booking_non_acknowledge_html($searchResult,$t_base,$flag=false,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__) 
{	
    $table_width = (int)(100/10);
    $iGrandTotal=count($searchResult);
    require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
    $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
    
    if($iPage==1)
    {
        $start=0;
        $end=$limit;
        if($iGrandTotal<$end)
        {
            $end=$iGrandTotal;
        }
    }
    else
    {
        $start=$limit*($iPage-1);
        $end=$iPage*$limit;
        if($end>$iGrandTotal)
        {
            $end=$iGrandTotal;
        }
        $startValue=$start+1;
        if($startValue>$iGrandTotal)
        {
            $iPage=$iPage-1;
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            
        }
    }
    ?>
    <table cellpadding="0" id="booking_table" cellspacing="0" class="format-4" width="100%">
        <tr>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/date')?></th>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/booking')?></th>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/service');?></th>
            <th width="<?=$table_width+4?>%" valign="top"><?=t($t_base.'fields/from');?></th>
            <th width="<?=$table_width+4?>%" valign="top"><?=t($t_base.'fields/to');?></th>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/wm');?> </th>
            <th width="<?=$table_width+3?>%" valign="top" style="text-align: right;"><?=t($t_base.'fields/value');?></th>
            <th width="<?=$table_width+9?>%" valign="top"><?=t($t_base.'fields/forwarder');?></th>
        </tr>
		<?php
			if(!empty($searchResult))
			{
				$ctr = 1;
                                $date5DayOld=date('Y-m-d',strtotime('- 4 DAY'));
				for($i=$start;$i<$end;++$i)
				{
					$szServiceType='';
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
					{
						$szServiceType = "DTD" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTW__) //DTW
					{
						$szServiceType = "DTW" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTD__) //WTD
					{
						$szServiceType = "WTD" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTW__) //WTW
					{
						$szServiceType = "WTW" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTP__) //DTD
					{
						$szServiceType = "PTP" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTP__) //DTW
					{
						$szServiceType = "DTP" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTD__) //WTD
					{
						$szServiceType = "PTD" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTP__) //WTW
					{
						$szServiceType = "WTP" ;
					}
					if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTW__) //WTW
					{
						$szServiceType = "PTW" ;
					}

					if($searchResult[$i]['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__ || $searchResult[$i]['iCourierBooking']==1)
                                        {
                                            $szWarehouseFromCity=createSubString($searchResult[$i]['szShipperCity'],0,10);
                                            $szWarehouseToCity=createSubString($searchResult[$i]['szConsigneeCity'],0,10); 
                                            $idWarehouseFromCountry = $searchResult[$i]['idOriginCountry'] ;
                                            $idWarehouseToCountry = $searchResult[$i]['idDestinationCountry'] ;
                                        }
                                        else
                                        {  
                                            $szWarehouseFromCity=createSubString($searchResult[$i]['szShipperCity'],0,10); 
                                            $szWarehouseToCity=createSubString($searchResult[$i]['szConsigneeCity'],0,10); 
                                            $idWarehouseFromCountry = $searchResult[$i]['idOriginCountry'] ;
                                            $idWarehouseToCountry = $searchResult[$i]['idDestinationCountry'] ;
                                        }
                                        
                                        if(!empty($searchResult[$i]['szTransportMode']))
                                        {
                                            $szServiceType = $searchResult[$i]['szTransportMode']." ".$szServiceType ;
                                        }
                                        else if($searchResult[$i]['iCourierBooking']==1)
                                        {
                                            $szServiceType="Courier";
                                        }
                                        else
                                        {
                                            $szServiceType = "Sea ".$szServiceType ;
                                        }
                                        $style='';
                                        
                                        if(strtotime(date('Y-m-d'))>strtotime($searchResult[$i]['dtSnooze'])){
                                            if(!empty($searchResult[$i]['dtBookingConfirmed']) && $searchResult[$i]['dtBookingConfirmed']!='0000-00-00 00:00:00')
                                            {

                                                if(strtotime($date5DayOld)>strtotime($searchResult[$i]['dtBookingConfirmed']))
                                                {
                                                    $style="class='snoozeDate'";
                                                }
                                            }
                                        }
					?>						
					<tr id="booking_data_<?=$ctr?>" onclick="select_forwarder_confirmation_tr('booking_data_<?=$ctr?>','<?=$searchResult[$i]['id']?>','NON_ACKNOWLEDGEMENT')">
                                                <td <?=$style;?> id="NONACKNOWLEDGE_<?=$searchResult[$i]['id']?>" <?php if($style!=''){?> onclick="openSnoozePopup('NONACKNOWLEDGE','<?=$searchResult[$i]['id']?>');" <?php }?>><?=(!empty($searchResult[$i]['dtBookingConfirmed']) && $searchResult[$i]['dtBookingConfirmed']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResult[$i]['dtBookingConfirmed'])):''?></td>
						<td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $searchResult[$i]['szBookingRandomNum'];?>/"><?=$searchResult[$i]['szBookingRef']?></a></td>
						<td><?=$szServiceType;?></td>
						<td><?=$szWarehouseFromCity." (".isoidCountryCode($idWarehouseFromCountry).")"?></td>
						<td><?=$szWarehouseToCity." (".isoidCountryCode($idWarehouseToCountry).")"?></td>
						<td><?=(get_formated_cargo_measure($searchResult[$i]['fCargoWeight']/1000)) ." / ".format_volume($searchResult[$i]['fCargoVolume'])?></td>
						<td style="text-align: right;"><?=($searchResult[$i]['fTotalPriceCustomerCurrency']?$searchResult[$i]['szCustomerCurrency']." ".number_format((float)($searchResult[$i]['fTotalPriceCustomerCurrency'] + $searchResults['fTotalVat']),2):'')?></td>
						<td><?=returnLimitData($searchResult[$i]['szDisplayName'],16)?></td>
					</tr>
					<?php
					$ctr++;
				}
			}
			else
			{
				?>
				<tr>
					<td colspan="11" align="center"><h4><b><?=t($t_base.'messages/no_boooking_found');?></b></h4></td>
				</tr>
				<?php
			}
		?>
	</table>
	<div class="map_note"><p><?=t($t_base.'fields/note');?>: <?=t($t_base.'fields/bookings_is_automatically');?></p></div>
	<div id="invoice_comment_div" style="display:none">
	</div>
        <div class="page_limit clearfix">
        <div class="pagination">
            <?php

                $kPagination->szMode = "DISPLAY_BOOKING_LIST_PAGINATION";
                //$kPagination->idBookingFile = $idBookingFile;
                $kPagination->paginate('display_booking_list_pagination'); 
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }        
            ?>
               </div>
               <div class="limit">Records per page
                   <select id="limit" name="limit" onchange="display_booking_list_pagination('1')">
                       <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                        <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                        <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                        <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                   </select>
               </div>    
            </div>
	<br>	
	<div align="right">
                <input type="hidden" id="page" name="page" value="<?php echo $iPage;?>" />    
		<span style="float:left;"><a href="javascript:void(0)" class="button2" style="opacity:0.4" id="booking_cancel" align="left"><span><?=t($t_base.'fields/cancel_booking');?></span></a></span>
		<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="resend_email_forwarder"><span><?=t($t_base.'fields/resend');?></span></a>&nbsp;
		<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_confirmation"><span><?=t($t_base.'fields/confirmation');?></span></a>&nbsp;
		<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_invoice"><span><?=t($t_base.'fields/invoice');?></span></a>&nbsp;
		<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_lcl_booking"><span><?=t($t_base.'fields/view_booking');?></span></a>
	</div>
        
	<?php
}
    function exchangeDetails($detail=array(),$flag=false)
    {  
        $t_base ='management/currency/';

        /*
        if((int)$detail['id']>0)
        {
                $idAdmin = $_SESSION['admin_id'];
                $kAdmin = new cAdmin();
                $detail_old=$kAdmin->excangeRates($idAdmin,$detail['id']);

        }*/ 
?>	
    <div style="clear: both;"></div>
    <label class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/currency');?></span>
        <span class="field-container">		
            <input type="text" name="arrExchange[currency]"  value="<?=(isset($detail['currency'])?$detail['currency']:$detail['szCurrency'])?>" size="6" style="width: 94px;"/> 
        </span>
    </label>			
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/should1');?></span>
	<span class="field-container">				
            <select name="arrExchange[pricing]" id="pricing">
                <option value="1" <?=(isset($detail['iPricing'])?$detail['iPricing']:$detail['pricing'])?'selected="selected"':'' ?>>Yes</option>
                <option value="0" <?=(isset($detail['iPricing'])?$detail['iPricing']:$detail['pricing'])?'':'selected="selected"' ?>>No</option>
            </select>
	</span>
    </label>			
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/should2');?></span>
	<span class="field-container">				
            <select name="arrExchange[booking]">
                <option value="1" <?=(isset($detail['iBooking'])?$detail['iBooking']:$detail['booking'])?'selected="selected"':'' ?>>Yes</option>
                <option value="0" <?=(isset($detail['iBooking'])?$detail['iBooking']:$detail['booking'])?'':'selected="selected"' ?>>No</option>
            </select>
	</span>
    </label>			
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/can');?></span>
	<span class="field-container">				
            <select name="arrExchange[settling]" id="settling">
                <option value="1" <?=(isset($detail['iSettling'])?$detail['iSettling']:$detail['settling'])?'selected="selected"':'' ?>>Yes</option>
                <option value="0" <?=(isset($detail['iSettling'])?$detail['iSettling']:$detail['settling'])?'':'selected="selected"' ?>>No</option>
            </select>
        </span>
    </label>			
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/google');?></span>
	<span class="field-container">			
            <input type="text" name="arrExchange[feed]" value="<?=!empty($detail['szFeed'])?$detail['szFeed']:$detail['feed']?>" />
	</span>
    </label>	 
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/bank');?></span>
	<span class="field-container">		
            <input type="text" name="arrExchange[szBankName]" id="szBankName"  value="<?php echo $detail['szBankName']; ?>" > 
	</span>
    </label>
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/sort_code');?></span>
	<span class="field-container">			
            <input type="text" name="arrExchange[szSortCode]" id="szSortCode" value="<?php echo $detail['szSortCode']; ?>">
	</span>
    </label>
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/account_number');?></span>
	<span class="field-container">			
            <input type="text" name="arrExchange[szAccountNumber]" id="szAccountNumber" value="<?php echo $detail['szAccountNumber']; ?>">
	</span>
    </label>
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/iban');?></span>
	<span class="field-container">			
            <input type="text" name="arrExchange[szIBANNumber]" id="szIBANNumber" value="<?php echo $detail['szIBANNumber']; ?>">
	</span>
    </label>
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/swift');?></span>
	<span class="field-container">			
            <input type="text" name="arrExchange[szSwiftNumber]" id="szSwiftNumber" value="<?php echo $detail['szSwiftNumber']; ?>" >
	</span>
    </label>
    <label class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/account_name');?></span>
	<span class="field-container">			
            <input type="text" name="arrExchange[szNameOnAccount]" id="szNameOnAccount" value="<?php echo $detail['szNameOnAccount']; ?>" >
	</span>
    </label> 
    <input type="hidden" name="mode" value="SAVE_DETAIL" />
    <input type="hidden" name="arrExchange[id]" id="id" value="<?=!empty($detail['id'])?$detail['id']:'';?>" />  
<?php
}	

function confirmBookingAfterPaymentResend($postSearchAry)
{
    $kForwarder = new cForwarder();
    $bookingRefLogAry = array();

    $kBooking = new cBooking();
    $updateAry['szInvoice'] = $postSearchAry['szInvoice'];
    $updateAry['dtBookingConfirmed'] = date('Y-m-d H:i:s');
    $updateAry['idBookingStatus'] = 3 ;

    if(!empty($updateAry))
    {
        foreach($updateAry as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }
    $update_query = rtrim($update_query,",");
    $kBooking->updateDraftBooking($update_query,$_SESSION['booking_id']);	
    $bookingDataArr=$kBooking->getExtendedBookingDetails($_SESSION['booking_id']);

    $kBooking->addrForwarderTranscaction($bookingDataArr);
    //print_r($bookingDataArr);
    //die();		
    $replace_ary = array();

    $idForwarder = $postSearchAry['idForwarder'] ;
    $customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder);
    $replace_ary['idUser'] = $postSearchAry['idUser'];
    $replace_ary['szFirstName'] = $postSearchAry['szFirstName'];
    $replace_ary['szLastName'] = $postSearchAry['szLastName'];
    $replace_ary['szEmail'] = $postSearchAry['szEmail'];
    $replace_ary['szForwarderDispName'] = $postSearchAry['szForwarderDispName'];
    $replace_ary['szForwarderCustServiceEmail'] = $customerServiceEmailAry[0];
    $replace_ary['szBookingRef'] = $postSearchAry['szBookingRef'];
    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
    createEmail(__BOOKING_CONFIRMATION_EMAIL__, $replace_ary,$replace_ary['szEmail'],' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,true);

    // creating email content for forwarder email 

    $idForwarder = $postSearchAry['idForwarder'];
    $kForwarder = new cForwarder();
    //$acceptedByUserAry = array();
    $acceptedByUserAry = $kForwarder->getTermsConditionAcceptedByUserDetails($idForwarder);

    $kForwarder->load($idForwarder);
    $iProfitType = $kForwarder->iProfitType;
    $kConfig = new cConfig();
    $szOriginCountryAry = array();
    $szDestinationCountryAry = array();
 
    if($bookingDataArr['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__ || $bookingDataArr['iQuotesStatus']==5)
    {
        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry']) ;

        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	

        $szOriginCountryStr = '';
        if(!empty($bookingDataArr['szShipperCity']))
        {
            $szOriginCountryStr = $bookingDataArr['szShipperCity'].", ";
        }
        $szOriginCountryStr .= $szOriginCountry ;

        $szDestinationCountryStr = '';
        if(!empty($bookingDataArr['szConsigneeCity']))
        {
            $szDestinationCountryStr = $bookingDataArr['szConsigneeCity'].", ";
        }
        $szDestinationCountryStr .= $szDestinationCountry ;
    }
    else
    {
        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseFromCountry']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idWarehouseToCountry']) ;

        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idWarehouseFromCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idWarehouseToCountry']]['szCountryName'] ;	
    
        $szOriginCountryStr = '';
        if(!empty($bookingDataArr['szWarehouseFromCity']))
        {
            $szOriginCountryStr = $bookingDataArr['szWarehouseFromCity'].", ";
        }
        $szOriginCountryStr .= $szOriginCountry ;
 
        $szDestinationCountryStr = '';
        if(!empty($bookingDataArr['szWarehouseToCity']))
        {
            $szDestinationCountryStr = $bookingDataArr['szWarehouseToCity'].", ";
        }
        $szDestinationCountryStr .= $szDestinationCountry ;
    }
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false,false,true);
    
    $replace_ary = array();
    $replace_ary['szForwarderDispName'] = $kForwarder->szDisplayName;

    $szControlPanelUrl = "http://".$kForwarder->szControlPanelUrl."/booking/".$_SESSION['booking_id']."__".md5(time())."/"; 

    $replace_ary['szLink'] = '<a href="'.$szControlPanelUrl.'"> CLICK HERE</a>';
    $replace_ary['szUrl'] = $szControlPanelUrl;
    $replace_ary['szTermsCoditionUrl'] = '<a href="http://'.$kForwarder->szControlPanelUrl.'/T&C/">Terms & Conditions</a>';	

    $replace_ary['szFirstName'] = utf8_encode($acceptedByUserAry['szFirstName']);
    $replace_ary['szLastName'] = utf8_encode($acceptedByUserAry['szLastName']);
    $replace_ary['dtAgreement'] = date('d/m/Y',strtotime($acceptedByUserAry['dtAgreement']));

    $replace_ary['szBookingRef'] = $postSearchAry['szBookingRef'];
    $replace_ary['supportEmail'] = __STORE_SUPPORT_EMAIL__;
    $replace_ary['szOriginCountry'] = $szOriginCountryStr;
    $replace_ary['szDestinationCountry'] = $szDestinationCountryStr;
    
    if($iProfitType==1)
    {
        $kConfig = new cConfig();
        $configLangArr=$kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',1);
        $szForwarderTerms_text1 = $configLangArr[1]['szForwarderTermsText1'];
        $szForwarderTerms_text2 = $configLangArr[1]['szForwarderTermsText2'];
        $szForwarderTerms_text3 = $configLangArr[1]['szForwarderTermsText3'];
        $iTermsConditionForReferralForwarder =$szForwarderTerms_text1." ".$replace_ary['szTermsCoditionUrl']."".$szForwarderTerms_text2." ".$replace_ary['szFirstName']." ".$replace_ary['szLastName']." ".$szForwarderTerms_text3." ".$replace_ary['dtAgreement'];
        $replace_ary['szTermsConditionForReferralForwarderText']="<br>".$iTermsConditionForReferralForwarder.".<br>";
    }
    else
    {
        $replace_ary['szTermsConditionForReferralForwarderText']='';
    }

    if($postSearchAry['idTransportMode']>0)
    {
        $replace_ary['szBookingType'] = $transportModeListAry[$postSearchAry['idTransportMode']]['szLongName'] ;
    }
    else
    {
        $replace_ary['szBookingType'] = 'Seafreight';
    }
    
    if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_AIR__)
    {
        $idForwarderContactRole = __AIR_FREIGHT_BOOKING_PROFILE_ID__ ;
    }
    else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_ROAD__)
    {
        $idForwarderContactRole = __ROAD_FREIGHT_BOOKING_PROFILE_ID__ ;
    }
    else if($postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__)
    {
        $idForwarderContactRole = __COURIER_FREIGHT_BOOKING_PROFILE_ID__ ;
    }
    else
    {
        $idForwarderContactRole = __CATCH_UP_ALL_BOOKING_QUOTE_PROFILE_ID__ ;
    }
    $email_address_forwarders_Arrs=$kForwarder->getForwarderCustomerServiceEmail($idForwarder,$idForwarderContactRole); 
            //$email_address_forwardersAry = format_fowarder_emails($email_address_forwarders_Arrs);
    //$forwarder_booking_email = $email_address_forwardersAry[0];
    createEmail('__FORWARDER_BOOKING_CONFIRMATION_REMINDER_EMAIL__', $replace_ary,$email_address_forwarders_Arrs ,' ', __STORE_SUPPORT_EMAIL__,$replace_ary['idUser'], __STORE_SUPPORT_EMAIL__,__FLAG_FOR_FORWARDER__,false);
    return true;        
}

function display_admin_obo_cc_bottom_html($t_base,$disable_feilds,$searchedDataAry=false,$formData=false,$field_name=false,$div_id = '')
{
	
	if($searchedDataAry['import']['iActive'] == 0)
	{
		$searchedDataAry['import']['szCurrency'] = null;
		$searchedDataAry['import']['fPrice'] = null;
	}
	if($searchedDataAry['export']['iActive'] == 0)
	{
		$searchedDataAry['export']['szCurrency'] = null;
		$searchedDataAry['export']['fPrice'] = null;
	}
	$disabled_str='';
	$display = 'none';
	if($disable_feilds)
	{
		$disabled_str = 'disabled="disabled"';
		$opacity_str = 'style="opacity:0.4;"';
	}
	else
	{
		$textmss=t($t_base."title/get_data");
		$textmss1=t($t_base."fields/change_data");
		$save_onclick = ' onclick = "update_obo_cc_data(\''.$textmss.'\',\''.$textmss1.'\',\''.$div_id.'\');"';
	}
	$kConfig = new cConfig();
	$allCurrencyArr=$kConfig->getBookingCurrency(false,false,false,true);
	if(!empty($searchedDataAry))
	{
		$field_name = 'editOBOCCData';
		
		//putting data to the variables
		$szOriginCountry = $searchedDataAry['export']['szCountryName'] ;
		$szDestinationCountry = $searchedDataAry['import']['szCountryName'] ;
		
	}
	elseif(!empty($formData))
	{
		$field_name = 'addOBOCCData';
		//$save_onclick = " onclick = 'update_obo_cc_data();'";
		$kConfi=new cConfig();
		$szOriginCountryAry = $kConfi->getAllCountryInKeyValuePair(false,$formData['idOriginCountry']);
		$szDestinationCountryAry = $kConfi->getAllCountryInKeyValuePair(false,$formData['idDestinationCountry']) ;
		
		$szOriginCountry = $szOriginCountryAry[$formData['idOriginCountry']]['szCountryName'] ;
		$szDestinationCountry = $szDestinationCountryAry[$formData['idDestinationCountry']]['szCountryName'] ;		
		$display = 'block';
	}
	$idOriginWarehouse = $formData['idOriginWarehouse'];
	$idDestinationWarehouse = $formData['idDestinationWarehouse'];
	
	$kWHSSearch=new cWHSSearch();
	$kWHSSearch->load($formData['idOriginWarehouse']);
	$fromWarehouseName=$kWHSSearch->szWareHouseName;
	
	$kWHSSearch->load($formData['idDestinationWarehouse']);
	$toWarehouseName=$kWHSSearch->szWareHouseName;
	
	if($formData['idDestinationWarehouse']>0 && $formData['idOriginWarehouse']>0)
	{
	?>
	<h5><strong><?=t($t_base.'messages/customs_clearance_rates_from')?> <?=t($t_base.'messages/rates_from')?> <?=$fromWarehouseName?> <?=t($t_base.'messages/to')?> <?=$toWarehouseName?></strong></h5>
	<? }else {?>
	<h5><strong><?=t($t_base.'messages/customs_clearance_rates_from')?></strong></h5>
	<? }?>
	<form id="update_obo_cc_form" method="post" action="">
	
	<div id="obo_cc_no_record_found" style="margin: 5px 0 2px 0;display:<?=$display?>">
			<ul id="message_ul" style="padding:5px 5px 5px 35px;">
				<li><?=t($t_base.'title/obo_cc_no_service_found');?></li></ul>
	</div>
	<?
		if($szOriginCountry=='' && empty($szOriginCountry))
		{
			$szOriginCountry="";
		}
		
		if($szDestinationCountry=='' && empty($szDestinationCountry))
		{
			$szDestinationCountry="";
		}
	?>
	<div id="obo_cc_error"  class="errorBox" style="display:none;"></div>
	<div class="oh">
		<p class="fl-40 gap-text-field" style="width:487px;"><?=t($t_base.'title/export_CC');?>&nbsp;<?=($szOriginCountry?'for '.$szOriginCountry:'')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/EXPORT_CC_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<!-- <p class="fl-15" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/country');?></span><br /><input disabled id="szOriginCountry" type="text" style="width: 107px;" name="<?=$field_name?>[szOriginCountry]" value="<?=$szOriginCountry?>" size="10" /></p>-->
		<p class="fl-15" style="width: 119px;"><span class="f-size-12"><?=t($t_base.'title/per_booking');?></span><br /><input <?=$disabled_str?> id="fOriginPrice" type="text" style="width: 107px;" name="<?=$field_name?>[fOriginPrice]" value="<?=$searchedDataAry['export']['fPrice']?>" size="10" /></p>
		<p class="fl-20" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="<?=$field_name?>[idOriginCurrency]" id="idOriginCurrency" >
			<option value=""><?=t($t_base.'fields/select');?></option>			
			<?php
					if(!empty($allCurrencyArr))
					{
						foreach($allCurrencyArr as $allCurrencyArrs)
						{
							?><option value="<?=$allCurrencyArrs['id']?>" <?=($searchedDataAry['export']['szCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<div class="oh">
		<p class="fl-40 gap-text-field" style="width:487px;"><?=t($t_base.'title/import_cc');?>&nbsp;<?=($szDestinationCountry?'for '.$szDestinationCountry:'')?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('<?=t($t_base.'messages/IMPORT_CC_TOOL_TIP_HEADER_TEXT');?>','','customs_clearance_pop',event);" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></p>
		<!--  <p class="fl-15" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/country');?></span><br /><input disabled id="szDestinationCountry" type="hidden" style="width: 107px;" name="<?=$field_name?>[szDestinationCountry]" value="<?=$szDestinationCountry?>"  size="10" /></p>-->
		<p class="fl-15" style="width: 119px;"><span class="f-size-12"><?=t($t_base.'title/per_booking');?></span><br /><input <?=$disabled_str?> id="fDestinationPrice" type="text" style="width: 107px;" name="<?=$field_name?>[fDestinationPrice]" value="<?=$searchedDataAry['import']['fPrice']?>" size="10" /></p>
		<p class="fl-20" style="width: 117px;"><span class="f-size-12"><?=t($t_base.'title/currency');?></span><br />
			<select size="1" <?=$disabled_str?> name="<?=$field_name?>[idDestinationCurrency]" id="idDestinationCurrency" >
				<option value=""><?=t($t_base.'fields/select');?></option>
				<?php
					if(!empty($allCurrencyArr))
					{
						foreach($allCurrencyArr as $allCurrencyArrs)
						{
							?><option value="<?=$allCurrencyArrs['id']?>" <?=($searchedDataAry['import']['szCurrency'] ==  $allCurrencyArrs['id'] ) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option>
							<?php
						}
					}
				?>
			</select>
		</p>
	</div>
	<br />
	<? if(isset($_SESSION['admin_id']) && (int)$_SESSION['admin_id']>0){?>
	<div class="oh" style="float: right;">
		<p class="fl-20" align="right"><a href="javascript:void(0);" <?=$save_onclick?> class="button1" id="updateData" <?=$opacity_str?>><span><?=t($t_base.'title/save');?></span></a></p>
	</div>
	<? }else{ ?>
	<div class="oh">
		<p class="fl-40"><a href="javascript:void(0)" onclick="cancel_obo_cc('<?=t($t_base.'fields/change_data');?>','<?=t($t_base.'title/get_data');?>')" class="button2" <?=$opacity_str?>><span><?=t($t_base.'title/cancel');?></span></a>
		 <a href="javascript:void(0);" onclick="clear_all_data_obo_cc();" class="button2" <?=$opacity_str?>><span><?=t($t_base.'title/clear_data');?></span></a></p>
		<p class="fl-40" align="right" style="padding-top:5px"><?=t($t_base.'title/click_to_update_changes');?></p>
		<p class="fl-20" align="right"><a href="javascript:void(0);" <?=$save_onclick?> class="button1" <?=$opacity_str?>><span><?=t($t_base.'title/save');?></span></a></p>
	</div>
	<? } ?>
	<input disabled id="szOriginCountry" type="hidden" style="width: 107px;" name="<?=$field_name?>[szOriginCountry]" value="<?=$szOriginCountry?>" size="10" />
	<input disabled id="szDestinationCountry" type="hidden" style="width: 107px;" name="<?=$field_name?>[szDestinationCountry]" value="<?=$szDestinationCountry?>"  size="10" />
	<input type="hidden" id="idOriginWarehouse" name="<?=$field_name?>[idOriginWarehouse]" value="<?=$idOriginWarehouse?>">
	<input type="hidden" id="idDestinationWarehouse" name="<?=$field_name?>[idDestinationWarehouse]" value="<?=$idDestinationWarehouse?>">
	<input type="hidden" id="idSelect" value="">
</form>	
	<?php
}
	
function try_it_out_haulage_admin($idWarehouse,$idDirection)
{
	if($idWarehouse>0)
	{
		$kWHSSearch = new cWHSSearch();
		$kWHSSearch->load($idWarehouse);
		$szWhsLatitude = $kWHSSearch->szLatitude ;
		$szWhsLongitude = $kWHSSearch->szLongitude ;
		
		$t_base = "HaulaugePricing/";
		if($idDirection=='1')
		{
			$direction=t($t_base.'fields/Export');
			$formto=t($t_base.'fields/try_from');
			
			$image_path = __BASE_STORE_IMAGE_URL__.'/haulagePricingBulk-2.png' ;
			$szOriginWhsName = $kWHSSearch->szWareHouseName ;
			$szDestinationWhsName='';
			$zone_type = 'HAULAGE_PRICING_EXPORT';
		}
		else
		{
			$direction=t($t_base.'fields/Import');
			$fromto=t($t_base.'fields/try_to');
			
			$image_path = __BASE_STORE_IMAGE_URL__.'/haulagePricingBulk-3.png' ;
			$szDestinationWhsName = $kWHSSearch->szWareHouseName ;
			$szOriginWhsName = '';
			$zone_type = 'HAULAGE_PRICING_IMPORT';
		}
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries();
		
		// geting all weight measure 
		//$weightMeasureAry=$kConfig->getConfigurationData(__DBC_SCHEMATA_WEIGHT_MEASURE__);
                
                $kConfig = new cConfig();
                $weightMeasureAry=$kConfig->getConfigurationLanguageData('__TABLE_WEIGHT_MEASURE__');
		
		
		?>
		<script type="text/javascript">
		$().ready(function() {	
			$("#szPostCodeCity").autocomplete(__JS_ONLY_SITE_BASE__+"/get_post_code_list.php", {
				width: 260,
				matchContains: true,
				mustMatch: true,
				//minChars: 0,
				//multiple: true,
				//highlight: false,
				//multipleSeparator: ",",
				selectFirst: false
			});
			
			//draw_canvas_image_obo('<?=$szOriginWhsName?>','<?=$szDestinationWhsName?>','<?=$image_path?>','<?=$zone_type?>');
		});
		</script>
		<hr/>
		<p align="left" style="font-size: 16px;"><b><?=t($t_base.'title/try_it_out')?></b></p>
		<p><?=t($t_base.'title/test_your')?> <?=$direction?> <?=t($t_base.'title/haulage_pricing_for')?> <?=$kWHSSearch->szWareHouseName?> <?=t($t_base.'title/by_completing_details_below')?>.</p>
		<br />
		<form id="obo_haulage_tryit_out_form" name="obo_haulage_tryit_out_form" method="post">
		<table cellpadding="0" cellspacing="0" border="0" class="tryhaulagepricing">
		<tr>
		<td width="112"><?=t($t_base.'fields/country')?></td>	
		<td colspan="2"><?=t($t_base.'fields/postcode')?></td>
		<td width="80"><?=t($t_base.'fields/cargo_weight')?></td>
		<td width="68" >&nbsp;</td>
		<td colspan="4"><?=t($t_base.'fields/cargo_volume')?></td>
		</tr>
			<tr>
			<td>
				<select size="1" name="tryoutArr[szCountry]" id="szCountry" onchange="clear_post_code()" style="width:122px;">
				<option value="">Select</option>
				<?php
					if(!empty($allCountriesArr))
					{ 
						foreach($allCountriesArr as $allCountriesArrs)
						{
						?><option value="<?=$allCountriesArrs['id']?>" <? if($kWHSSearch->idCountry==$allCountriesArrs['id']){?> selected <?}?>><?=$allCountriesArrs['szCountryName']?></option>
						<?php
						}
					}
				?>
				</td>
				<td width="81">
				<input style="width:75px;" type="text" value="<?=(($_POST['tryoutArr']['szPostCodeCity'])?$_POST['tryoutArr']['szPostCodeCity']:"")?>" id="szPostCodeCity" name="tryoutArr[szPostCodeCity]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
				<input type="hidden" id="idDirection" value="<?=$idDirection?>" name="tryoutArr[idDirection]">
				<input type="hidden" id="idWarehouse" value="<?=$idWarehouse?>" name="tryoutArr[idWarehouse]">				
				
				<input type="hidden" name="tryoutArr[szWhsLatitude]" id="szWhsLatitude" value="<?=$szWhsLatitude?>">
				<input type="hidden" name="tryoutArr[szWhsLongitude]" id="szWhsLongitude" value="<?=$szWhsLongitude?>">
				
				<input type="hidden" name="tryoutArr[szLatitude]" id="szLatitude" value="">
				<input type="hidden" name="tryoutArr[szLongitude]" id="szLongitude" value="">				
				
				</td>
				<td width="40" style="line-height:12px;">
				<a href="javascript:void(0);" onclick="open_google_help_popup()" style="font-size: 12px;"><?=t($t_base.'fields/Select');?><br/><?=t($t_base.'fields/on_map');?></a>
				</td>
				<td>
				<input style="width:73px;" type="text" value="<?=(($_POST['tryoutArr']['fTotalWeight'])?$_POST['tryoutArr']['fTotalWeight']:"")?>" id="szCargoWieght" name="tryoutArr[fTotalWeight]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
				
				</td>
				<td>
					<select size="1" name="tryoutArr[idWeightMeasure]" id="idWeightMeasure" style="width:68px;">
						 <?php
						 	if(!empty($weightMeasureAry))
						 	{
						 		foreach($weightMeasureAry as $weightMeasureArys)
						 		{
						 			?>
						 				<option value="<?=$weightMeasureArys['id']?>" <? if(strtolower($weightMeasureArys['szDescription'])=='kg'){?> selected <? }?>><?=$weightMeasureArys['szDescription']?></option>
						 			<?php
						 		}
						 	}
						 ?>
					</select>
				</td>
				<td width="80">
					<input style="width:70px;" type="text" value="<?=(($_POST['tryoutArr']['fVolume'])?$_POST['tryoutArr']['fVolume']:"")?>" id="szCargoVolume" name="tryoutArr[fVolume]" size="10" onkeyup="enable_add_edit_button_try_test(event);">
				</td>
				<td><span><?=t($t_base.'fields/cbm')?></span></td>
				<td colspan="2" style="padding:0;width:248px;" align="right"><a href="javascript:void(0)" style="opacity:0.4" id="test_try_out" class="button1"><span style="min-width:70px;"><?=t($t_base.'fields/test')?></span></a> <a href="javascript:void(0)" class="button2" onclick="clear_haulage_tryitout();"><span style="min-width:70px;"><?=t($t_base.'fields/clear')?></span></a></td>
			</tr>
		</table>
	</form>
	
	<div id="tryitout_result_div_error" style="display:none;margin-top:8px;" class="errorBox">
	</div>
	<div id="tryitout_result_div" style="display:none;">
	</div>		
	
	<?php	
	}
	else
	{
		$t_base = "HaulaugePricing/";
		?>
		
		<div id="tryitout_result_div_error" style="display:none;margin-top:8px;" class="errorBox">
		</div>
		
		<div id="tryitout_result_div" style="display:none;">
		</div>		
		
		<hr>
		<h5><?=t($t_base.'title/illustration_of_what_haulage_services_cover')?></h5>
		<canvas style="margin-left:-10px;" id="myCanvas" width="730" height="180"></canvas>
		<?
	}
}
function showForwarderCurrencyUpdatePopup($idCurrency)
{
	$t_base = "management/currency/";
	$kAdmin = new cAdmin();
	$forwarderUsingCurrencyArr=$kAdmin->checkForwarderCurrency($idCurrency);
	$forwardIdArr=array();
	?>
	<div id="inactivate_services">
		<div id="popup-bg"></div>
			<div id="popup-container">			
				<div class="popup abandon-popup" style="margin-top: -23px;">
					<form name="inActivateService" id="inActivateService" method="post">
					<h4><b><?=t($t_base.'messages/inactivate_forwarder_currency');?> <?=$szCurrenyArr[0]['szCurrency']?></b></h4><br/>
					<p style="text-align:justify"><?=t($t_base.'messages/inactive_forwarder_currency_popup_line_1')?></p>
					<br/>
					<div class="tableScroll" style="height:119px;overflow: auto;">
						<table class="format-2" width="100%" cellspacing="0" cellpadding="0" border="0" style="position:relative;top:-1px;">
							<tr>
								<td style="width:50%"><b><?=t($t_base.'fields/forwarder')?></b></td>
							</tr>
							<? if(!empty($forwarderUsingCurrencyArr)){
									foreach($forwarderUsingCurrencyArr as $forwarderUsingCurrencyArrs)
									{
										
									?>
									<tr>
										<td><?=$forwarderUsingCurrencyArrs['szDisplayName']?></td>
									</tr>
										
							<? 		
										if(!in_array($forwarderUsingCurrencyArrs['id'],$forwardIdArr))
										{
											$forwardIdArr[]=$forwarderUsingCurrencyArrs['id'];
											//$forwardStrArr[]=$str_line;
										}
									}
										$forwarderArrStr=implode(";",$forwardIdArr);
								}
							?>
						</table>
					</div>
					<p style="text-align:justify"><?=t($t_base.'messages/inactive_forwarder_currency_popup_line_2')?></p>
					<br/>
					<p align="center">
					<input type="hidden" name="inactivateForwarderArr[idForwarderCurrStr]" id="idForwarderCurrStr" value="<?=$forwarderArrStr?>">
					<input type="hidden" name="inactivateForwarderArr[idCurrency]" id="idCurrency" value="<?=$idCurrency?>">
					<a href="javascript:void(0)" id="confirm_button" onclick="confirm_update_forwarder_currency('<?=$idCurrency?>')" class="button1"><span><?=t($t_base.'fields/confirm')?></span></a> <a href="javascript:void(0)" id="cancel_button" onclick="cancel_inactivate_forwarder();" class="button2"><span><?=t($t_base.'fields/cancel')?></span></a></p>
					</form>
				</div>
			</div>
	</div>
					<?
}
function showCurrentHaulageTopForm($t_base)
{
	$kExport_Import =  new cExport_Import;
	$kConfig = new cConfig;
	$kAdmin =  new cAdmin;
	$forwardWareHouseArr = array();
	$forwardCountriesArr = array();
	$forwardWareHouseCityAry = array();
	//GETTING ALL FORWARDER DETAILS
	$allForwarderList = $kAdmin->getAllForwarderNameList();
	// getting all countries where forwarder has warehouse listed.
	//foreach($allForwarderList as $forwarderList)
	//{
		$forwardCountriesArr=$kConfig->getAllCountryInPair();
		//print_r($forwardCountriesArr);
//	}
	//print_R($forwardCountriesArr);
	/*if($forwardCountriesArr!= array())
	{
		foreach($forwardCountriesArr as $forwardCountriesArrs)
		{
			if($forwardCountriesArrs == array())
			{
				unset($forwardCountriesArrs);
			}
			else
			{
				$forwardArrs = $forwardCountriesArrs; 
			}
		}
	}*/
	?>
	<form name = "currentHaulage" id="currentHaulage">
			<p class="fl-25 f-size-12">
			<?=t($t_base.'fields/forwarder');?>
			<br>
			<select name="currentHaulage[idForwarder]" id="idForwarder" style="width: 150px;">
				<option value=""><?=t($t_base.'fields/all');?></option>
				<?php
					if(!empty($allForwarderList))
					{
						foreach($allForwarderList as $forwarderList)
						{
							?>
							<option value="<?=$forwarderList['id']?>"><?=$forwarderList['szDisplayName']?></option>
							<?
						}
					}
				?>
			</select>
			</p>
			<p class="fl-25 f-size-12">
			<?=t($t_base.'fields/country');?>
			<br>
			<select name="currentHaulage[idCountry]" id="idOriginCountry"  style="width: 150px;">
				<option value=""><?=t($t_base.'fields/all');?></option>
				<?php
					if(!empty($forwardCountriesArr))
					{
						foreach($forwardCountriesArr as $forwardCountriesArrs)
						{
							?>
							<option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
							<?
						}
					}
				?>
			</select>
			</p>
			<p class="fl-25 f-size-12">
			<?=t($t_base.'fields/direction');?>
			<br>
			<select name="currentHaulage[idDirection]"  id="idDirectionTop" style="width: 150px;">
				<option value=""><?=t($t_base.'fields/all');?></option>
				<option value="1"><?=t($t_base.'fields/export');?></option>
				<option value="2"><?=t($t_base.'fields/import');?></option>
			</select>
			</p>
			<p class="fl-10 f-size-12">
			<?=t($t_base.'fields/pricing');?>
			<br>
			<select name="currentHaulage[iPricing]"  id="iPricing"  style="width: 100px;">
				<option value="0"><?=t($t_base.'fields/all');?></option>
				<option value="1" selected="selected"><?=t($t_base.'fields/yes');?></option>
				<option value="2"><?=t($t_base.'fields/no');?></option>
			</select>
			</p>
			<p class="fl-20 f-size-12" align="right">
			&nbsp;
			<input type="hidden" name="mode" value="SHOW_HAULAGE_LISTING"> 
			<br>
			<a class="button1" onclick="submit_currentHaulage_form();"><span><?=t($t_base.'fields/show');?></span></a>
			</p>
		</form>
		<div style="clear: both;"></div>
		<br />
	<?
}
function showCurrentCCTopForm($t_base)
{
	$kExport_Import =  new cExport_Import;
	$kConfig = new cConfig;
	$kAdmin =  new cAdmin;
	$forwardWareHouseArr = array();
	$forwardCountriesArr = array();
	$forwardWareHouseCityAry = array();
	//GETTING ALL FORWARDER DETAILS
	$allForwarderList = $kAdmin->getAllForwarderNameList();
	// getting all countries where forwarder has warehouse listed.
	//foreach($allForwarderList as $forwarderList)
	//{
		$forwardCountriesArr=$kConfig->getAllCountries();
//	}
	//print_R($forwardCountriesArr);
	/*if($forwardCountriesArr!= array())
	{
		foreach($forwardCountriesArr as $forwardCountriesArrs)
		{
			if($forwardCountriesArrs == array())
			{
				unset($forwardCountriesArrs);
			}
			else
			{
				$forwardArrs = $forwardCountriesArrs; 
			}
		}
	}*/
	?>
	<form name = "currentCC" id="currentCC">
			<p class="fl-25 f-size-12">
			<?=t($t_base.'fields/forwarders');?>
			<br>
			<select name="currentCC[idForwarder]" id="idForwarder"  style="width: 150px;">
				<option value=""><?=t($t_base.'fields/all');?></option>
				<?php
					if(!empty($allForwarderList))
					{
						foreach($allForwarderList as $forwarderList)
						{
							?>
							<option value="<?=$forwarderList['id']?>"><?=$forwarderList['szDisplayName']?></option>
							<?
						}
					}
				?>
			</select>
			</p>
			<p class="fl-25 f-size-12">
			<?=t($t_base.'fields/from_countries');?>
			<br>
			<select name="currentCC[idOriginCountry]" id="idOriginCountry"  style="width: 150px;">
				<option value=""><?=t($t_base.'fields/all');?></option>
				<?php
					if(!empty($forwardCountriesArr))
					{
						foreach($forwardCountriesArr as $forwardCountriesArrs)
						{
							?>
							<option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
							<?
						}
					}
				?>
			</select>
			</p>
			<p class="fl-25 f-size-12">
			<?=t($t_base.'fields/to_countries');?>
			<br>
			<select name="currentCC[idDestinationCountry]"  id="idDestinationCountry"  style="width: 150px;">
				<option value=""><?=t($t_base.'fields/all');?></option>
				<?php
					if(!empty($forwardCountriesArr))
					{
						foreach($forwardCountriesArr as $forwardCountriesArrs)
						{
							?>
							<option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
							<?
						}
					}
				?>
			</select>
			</p>
			<input type="hidden" name="mode" value="SHOW_ACTIVE_CC">
			<p class="fl-15 f-size-12" style="float: right;">
			&nbsp;
			<br>
			<a class="button1" onclick="submit_currentCC_form();" style="float: right;"><span><?=t($t_base.'fields/show');?></span></a>
			</p>
		</form>
		<div style="clear: both;"></div>
		<br />
	<?php
}
function showCfsListing($t_base,$warehouseDetailArr,$idAdmin,$iWarehouseType=__WAREHOUSE_TYPE_CFS__)
{
	$kConfig 	= new cConfig;
	//$kBooking	= new cBooking;
	$kWHSSearch = new cWHSSearch;
	$allCountriesArr = $kConfig->getAllCountries(true);
	$dimension = 100/8; 
        
        if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
        {
            $szCfsName = "Airport warehouse";
        }
        else
        {
            $szCfsName = t($t_base."fields/cfs_name");
        }
?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-4" width="100%" id="Cfs_list">
		<tr>
			<th width="<?=$dimension+5;?>%"><?=t($t_base."fields/country")?></th>
			<th width="<?=$dimension;?>%"><?=t($t_base."fields/city")?></th>
			<th width="<?=$dimension+17;?>%"><?=$szCfsName;?></th>
			<th width="<?=$dimension-7;?>%" style="text-align: right;"><?=t($t_base."fields/origin")?></th>
			<th width="<?=$dimension-7;?>%" style="text-align: right;"><?=t($t_base."fields/destination")?></th>
			<th width="<?=$dimension-8;?>%"><?=t($t_base."fields/local_haulage")?></th>
			<th width="<?=$dimension-8;?>%"><?=t($t_base."fields/intl_haulage")?></th>
			<th ><?=t($t_base."fields/forwarder")?></th>
		</tr>
<?php if($warehouseDetailArr!=array())
	{
		$count = 0;
		foreach($warehouseDetailArr as $details)
		{		
			//one for all pricing CC having idirection = 1
		    //two for all pricing CC having idirection = 2
			$wareHouseFrom	=	$kWHSSearch->maximumPricingHaulage($details['id'],1);//export pricing lines
			$wareHouseTo	=	$kWHSSearch->maximumPricingHaulage($details['id'],2);//import pricing lines		

			$CFSListArr[$count][] = $details['id'];
			$CFSListArr[$count][] = returnLimitData($details['szCountryName'],15);
			$CFSListArr[$count][] = returnLimitData($details['szCity'],10);
			$CFSListArr[$count][] = returnLimitData($details['szWareHouseName'],28);
			$CFSListArr[$count][] = $kWHSSearch->getWareHouseCount($details['id'],'idWarehouseFrom');
			$CFSListArr[$count][] = $kWHSSearch->getWareHouseCount($details['id'],'idWarehouseTo');
			$CFSListArr[$count][] = ($wareHouseFrom?'Yes':'No');
			$CFSListArr[$count][] = ($wareHouseTo?'Yes':'No');
			$CFSListArr[$count][] = returnLimitData($details['szDisplayName'],20) ;
			$count++;
		}
			//$CFSListArr = sortArray($CFSListArr,1);
		
			$count = 0;
			foreach($CFSListArr as $CFSListAr)
			{			
			?>
		
				<tr id="Cfs_list_tr_<?=(++$count);?>" onclick="select_cfs_tr('Cfs_list_tr_<?=$count?>','<?=$CFSListAr[0]?>');">
				<td><?=$CFSListAr[1] ?></td>
				<td><?=$CFSListAr[2] ?></td>
				<td><?=$CFSListAr[3] ?></td>
				<td style="text-align: right;"><?=$CFSListAr[4] ?></td>
				<td style="text-align: right;"><?=$CFSListAr[5] ?></td>
				<td><?=$CFSListAr[6] ?></td>
				
				<td><?=$CFSListAr[7] ?></td>
				<td><?=$CFSListAr[8] ?></td>	
			</tr>
		<?php	}
		unset($CFSListArr);
	}
	else
	{
	
		echo "<tr><td colspan='8' align='center'><b>".t($t_base.'fields/no_data_to_display')."</b></td></tr>";
	}	
?>		
		</table>
		<br/>
		<div style="float: right;">
                    <a class="button1" id="editCfsLocation" style="opacity:0.4;"><span><?=t($t_base.'fields/edit')?></span></a>
                    <a class="button2" id="removeCfsLocation" style="opacity:0.4;"	><span><?=t($t_base.'fields/delete')?></span></a>
		</div>
		<br/>
		<?php
}
function addEditWarehouse($t_base,$idAdmin,$iWarehouseType=false)
{

    $kConfig  = new cConfig;
    $kBooking = new cBooking;
    $kWHSSearch = new cWHSSearch;
    $allCountriesArr = $kConfig->getAllCountries(true);
    $forwarderIdName = $kBooking->forwarderIdName($idAdmin);
    
    if($iWarehouseType==__WAREHOUSE_TYPE_AIR__)
    { 
        $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text_airport_cfs'); 
        $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text_airport_warehouse');
        $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text_airport_cfs');
        $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text_airport_cfs'); 
    }
    else
    { 
        $szAddressNameBluText = t($t_base.'messages/address_1_blue_box_text'); 
        $szPhoneNumberBluText = t($t_base.'messages/phone_number_blue_box_text');
        $szLatitudeBluText = t($t_base.'messages/latitude_blue_box_text'); 
        $szLongitudeBluText = t($t_base.'messages/longitude_blue_box_text');
    } 
    $szCfsNameBluText = t($t_base.'messages/cfs_name_blue_box_text'); 
?>

    <div id="Error-Log"></div>
    <div id='addedit_warehouse'>
            <form name="addEditWarehouse" id="addEditWarehouse" method="post" >
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/forwarder');?></span>
                <span class="field-container">
                    <select name="cfsAddEditArrs[idForwarder]" id="idForwarder" style="width:222px;" onchange="forwarderIdValue(this.value);" >
                        <option>Select</option>	
                        <?php
                            if($forwarderIdName!=array())
                            {
                                foreach($forwarderIdName as $fwdName)
                                {
                                    echo "<option value='".$fwdName['id']."'>".$fwdName['szDisplayName']."</option>";
                                }
                            }
                        ?>
                    </select>
                    <input type="hidden" name= "cfsAddEditArr[idForwarder]" value='0' id="idForwarderHidden">
                </span>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'fields/airport_warehouse'):t($t_base.'fields/cfs_name');?></span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_name');" onfocus="openTip('cfs_name');" name="cfsAddEditArr[szCFSName]" id="szCFSName" value="<?=$_POST['cfsAddEditArr']['szCFSName'] ? $_POST['cfsAddEditArr']['szCFSName'] : $kWHSSearch->szWareHouseName ?>"/></span>
                <div class="field-alert"><div id="cfs_name" style="display:none;"><?=$szCfsNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/address_line');?> 1</span>
                <span class="field-container"><input type="text" onblur="closeTip('address_1');" onfocus="openTip('address_1');" name="cfsAddEditArr[szAddressLine1]" id="szAddressLine1" value="<?=$_POST['cfsAddEditArr']['szAddressLine1'] ? $_POST['cfsAddEditArr']['szAddressLine1'] : $kWHSSearch->szAddress ?>"/></span>
                <div class="field-alert"><div id="address_1" style="display:none;"><?=$szAddressNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/address_line');?> 2&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input onblur="closeTip('address_2');" onfocus="openTip('address_2');"  type="text" name="cfsAddEditArr[szAddressLine2]" id="szAddressLine2" value="<?=$_POST['cfsAddEditArr']['szAddressLine2'] ? $_POST['cfsAddEditArr']['szAddressLine2'] : $kWHSSearch->szAddress2 ?>"/></span>
                <div class="field-alert"><div id="address_2" style="display:none;"><?=$szAddressNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/address_line');?> 3&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input type="text" onblur="closeTip('address_3');" onfocus="openTip('address_3');" name="cfsAddEditArr[szAddressLine3]" id="szAddressLine3" value="<?=$_POST['cfsAddEditArr']['szAddressLine3'] ? $_POST['cfsAddEditArr']['szAddressLine3'] : $kWHSSearch->szAddress3 ?>"/></span>
                <div class="field-alert"><div id="address_3" style="display:none;"><?=$szAddressNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/postcode');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_post_code');" onfocus="openTip('cfs_post_code');" name="cfsAddEditArr[szPostCode]" id="szOriginPostCode" onfocus="blank_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/optional');?>','<?=t($t_base.'fields/type_code');?>')"  value="<?=$_POST['cfsAddEditArr']['szPostCode'] ? $_POST['cfsAddEditArr']['szPostCode'] : $kWHSSearch->szPostCode ?>"/></span>
                <div class="field-alert"><div id="cfs_post_code" style="display:none;"><?=$szAddressNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/city');?></span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_city');" onfocus="openTip('cfs_city');" name="cfsAddEditArr[szCity]" onfocus="blank_me(this.id,'<?=t($t_base.'fields/type_name');?>')" onblur="show_me(this.id,'<?=t($t_base.'fields/type_name');?>')" id="szOriginCity" value="<?=$_POST['cfsAddEditArr']['szCity'] ? $_POST['cfsAddEditArr']['szCity'] : $kWHSSearch->szCity ?>"/></span>
                <div class="field-alert"><div id="cfs_city" style="display:none;"><?=$szAddressNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/province');?>&nbsp;<span class="optional">(<?=t($t_base.'fields/optional');?>)</span>	</span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_state');" onfocus="openTip('cfs_state');" name="cfsAddEditArr[szState]" id="szState" value="<?=$_POST['cfsAddEditArr']['szState'] ? $_POST['cfsAddEditArr']['szState'] : $kWHSSearch->szState ?>"/></span>
                <div class="field-alert"><div id="cfs_state" style="display:none;"><?=$szAddressNameBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/country');?></span>
                <span class="field-container">
                    <select size="1" onchange="showCountriesByCountryForManagement(this.value)" onblur="closeTip('cfs_country');" onfocus="openTip('cfs_country');" name="cfsAddEditArr[szCountry]" id="szCountry" style="max-width:211px;">
                        <option value="">Select Country</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?><option value="<?=$allCountriesArrs['id']?>" <?=((($_POST['cfsAddEditArr']['szCountry'])?$_POST['cfsAddEditArr']['szCountry']:$kWHSSearch->idCountry) ==  $allCountriesArrs['id'] ) ? "selected":""?>><?=$allCountriesArrs['szCountryName']?></option><?php
                                }
                            }
                        ?>
                    </select>
                </span>
                <div class="field-alert"><div id="cfs_country" style="display:none;"><?=$szAddressNameBluText;?></div></div>
            </label>  
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/phone_number');?></span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_phone');" onfocus="openTip('cfs_phone');" name="cfsAddEditArr[szPhoneNumber]" id="szPhoneNumber" value="<?=$_POST['cfsAddEditArr']['szPhoneNumber'] ? $_POST['cfsAddEditArr']['szPhoneNumber'] : $kWHSSearch->szPhone ?>"/></span>
                <div class="field-alert"><div id="cfs_phone" style="display:none;"><?=$szPhoneNumberBluText;?></div></div>
            </label>
            <br /> 
            <h5><strong><?=t($t_base.'fields/exact_location');?></strong> <a href="javascript:void(0);" onclick="open_help_me_select_popup()" class="f-size-14"><?=t($t_base.'fields/help_me_find_this');?></a></h5>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/latitude');?></span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_lat');" onfocus="openTip('cfs_lat');" name="cfsAddEditArr[szLatitude]" id="szLatitude" value="<?=$_POST['cfsAddEditArr']['szLatitude'] ? $_POST['cfsAddEditArr']['szLatitude'] : $kWHSSearch->szLatitude ?>"/></span>
                <div class="field-alert1"><div id="cfs_lat" style="display:none;"><?=$szLatitudeBluText;?></div></div>
            </label>
            <label class="profile-fields">
                <span class="field-name"><?=t($t_base.'fields/longitude');?></span>
                <span class="field-container"><input type="text" onblur="closeTip('cfs_long');" onfocus="openTip('cfs_long');" name="cfsAddEditArr[szLongitude]" id="szLongitude" value="<?=$_POST['cfsAddEditArr']['szLongitude'] ? $_POST['cfsAddEditArr']['szLongitude'] : $kWHSSearch->szLongitude ?>"/></span>
                <div class="field-alert1"><div id="cfs_long" style="display:none;"><?=$szLongitudeBluText;?></div></div>
            </label>  
            <div id='bottom-form'>
	
	<?php if($kWHSSearch->szWareHouseName==''){  ?>
	    	 <br />
	    	 <h4><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_this_airport_warehouse'):t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
	    	 <p style="margin-top: 5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/add_local_market_airport_cfs'):t($t_base.'messages/add_local_market');?></p>
	    	 <br />
	    	 <div style="direction: ltr;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td style="padding-top:5px;" valign="top" width="215"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_airport_warehouse'):t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			
	    	 </select>
	    	 <br />
	    	  <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?></span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	 </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;" ><?=t($t_base.'messages/add_from_list');?><br /><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/sorted_on_airport_cfs'):t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>	    	 
	    	 <select id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 </select>
	    	 <br />
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	    	 </div>
	<?php }else{
		$kConfig = new cConfig();
		$allCountriesArr=$kConfig->getAllCountries(true);
		//$idCountry = ;
		//$kWHSSearch->set_idCountry($idCountry);
		$countryList = $kWHSSearch->findWarehouseCountryList($kWHSSearch->idCountry,$kWHSSearch->szLatitude,$kWHSSearch->szLongitude);?>
	    	<div id="selectForCountrySelection">
	    	 <?php //$selectListCFS = array(1,2,3);
				if($kWHSSearch->id)
				{
					$selectListCFS  = $kWHSSearch->selectWarehouseCountries($kWHSSearch->id);
					 
				}
	    	 	elseif($kWHSSearch->idCountry!= NULL)
	    	 	{
	    	 		$selectListCFS = array($kWHSSearch->idCountry);
	    	 	}
	    	 ?>
	    	 <br />
	    	 <h4><b><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_this_airport_warehouse'):t($t_base.'title/countries_served_by_this_cfs');?></b></h4>
	    	 <p style="margin-top: 5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/add_local_market_airport_cfs'):t($t_base.'messages/add_local_market');?></p>
	    	 <br />
	    	<div style="direction: ltr;clear: both;">
	    	 <div style="float: left;width:434px;">
	    	 <table width="100%" cellspacing="0" cellpadding="0">
	    	 <tr>
	    	 <td valign="top" width="215" style="padding-top:5px;"><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'title/countries_served_by_airport_warehouse'):t($t_base.'title/countries_served_by_cfs');?></td>
	    	 <td>
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="selectedCountries" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple" >
			<?php
			if(!empty($allCountriesArr))
			{
				foreach($allCountriesArr as $allCountriesArrs)
				{
					if(in_array($allCountriesArrs['id'],$selectListCFS))
					{
						?><option value="<?=$allCountriesArrs['id']?>"><?=$allCountriesArrs['szCountryName']?></option>
						<?php
					}
				}
			}
		?>	
	    	 </select>
	    	  <br />
	    	   <a class="button1" onclick="addCfsCountries('addFormList')"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?> </span></a>
	    	 <a class="button2" onclick="removeCfsCountries('selectedCountries','<?=$kWHSSearch->idCountry?>','<?=$kWHSSearch->szLatitude?>','<?=$kWHSSearch->szLongitude?>')"><span style="min-width: 79px;"><?=t($t_base.'fields/remove');?></span></a>
	    	  </td>
	    	 </tr>
	    	 <tr><td valign="top">
	    	 <p style="margin-top: 5px;"><?=t($t_base.'messages/add_from_list');?><br /><?php echo ($iWarehouseType==__WAREHOUSE_TYPE_AIR__)?t($t_base.'messages/sorted_on_airport_cfs'):t($t_base.'messages/sorted_on');?> </p>
	    	 </td>
	    	<td>		    	
	    	 <select onfocus="onSelected('show');" onblur="onSelected('hide');" id="addFormList" style="width: 211px;height: 100px;margin-bottom:5px;margin: 5px 0 5px 2px;" multiple="multiple">
	    	 <?php
                    if($countryList!=array())
                    {
                        foreach($countryList as $key=>$value)
                        {
                            if(!in_array($value['id'],$selectListCFS))
                            {
                                echo "<option value ='".$value['id']."' >".$value['szCountryName']."</option>";
                            }
                        }
                    }
	    	 ?>
	    	 </select>
	    	  <br />
	    	
	    	</td>
	    	</tr>
	  	   </table>
	  	   </div>
	  	   <div class="profile-fields" style="margin-top: 14px;float:left;">
	    	 	<div class="field-alert" id="example" style="display:none;">
                            <div>
	    	 		<p><?=t($t_base.'messages/for_your_rotterdam');?></p>
	    	 		<br/>
	    	 		<p><?=t($t_base.'messages/for_your_hong_kong');?></p>
                            </div>
	    	 	</div>
	    	 </div>
		   </div>
	    	 <?php //getNewCfsCountriesList($allCountriesArr,$countryList,$selectListCFS);?>
	    </div>	
		<?php } ?>
		</div>	
		<div style="clear: both;"></div>
		<br />
		<div class="oh">
                    <p class="fl-30" align="right" style="padding-top:6px;">&nbsp;</p>
                    <p class="fl-40" align="left">
                    <input type="hidden" name="cfsAddEditArr[szPhoneNumberUpdate]" id="szPhoneNumberUpdate" value="">
                    <input type="hidden" name="cfsAddEditArr[idEditWarehouse]" id="idEditWarehouse" value="0">
                    <input type="hidden" name="cfsAddEditArr[iWarehouseType]" id="iWarehouseType" value="<?php echo $iWarehouseType; ?>">
                    
                    <a href="javascript:void(0)" class="button1" id="add_edit_warehouse_button" onclick="add_cfs_listing()"><span style="min-width: 79px;"><?=t($t_base.'fields/add');?></span></a>
                    <a href="javascript:void(0);" id="clear_cfs_location_button" onclick="clear_form_data_warehouse('addEditWarehouse')" class="button2"><span style="min-width: 79px;"><?=t($t_base.'fields/clear');?></span></a></p>
		</div> 
		</form>
                    <form method="post" id="google_map_hidden_form" action="<?=__MANAGEMENT_URL__?>/googleMap.php" target="google_map_target_1">
			<input type="hidden" name="cfsHiddenAry[szPostCode]" id="szPostCode_hidden" value="">
			<input type="hidden" name="cfsHiddenAry[szAddressLine1]" id="szAddressLine1_hidden" value="">
			<input type="hidden" name="cfsHiddenAry[szAddressLine2]" id="szAddressLine2_hidden" value="">
			<input type="hidden" name="cfsHiddenAry[szAddressLine3]" id="szAddressLine3_hidden" value="">
			<input type="hidden" name="cfsHiddenAry[szCity]" id="szCity_hidden" value="">
			
			<input type="hidden" name="cfsHiddenAry[szState]" id="szState_hidden" value="">
			<input type="hidden" name="cfsHiddenAry[szCountry]" id="szCountry_hidden" value="">
			<input type="hidden" name="cfsHiddenAry[szLatitude]" id="szLatitude_hidden" value="">
			<input type="hidden" name="cfsHiddenAry[szLongitude]" id="szLongitude_hidden" value="">
                        <input type="hidden" name="cfsHiddenAry[iWarehouseType]" id="iWarehouseType_hidden" value="<?php echo $iWarehouseType; ?>">
		</form>
		<!-- style="width:600px;height:380px;border:0px solid #fff;" -->
		<div id="popup_container_google_map" style="display:none;">
			<div id="popup-bg"></div><div id="popup-container" style="padding-bottom: 20px;">
			<iframe id="google_map_target_1" class="google_map_select popup" style="margin-top: -60px" name="google_map_target_1" src="#">
			</iframe>
	</div>
		
	</div>
</div>
<?php
}
function showCurrentCFSListTopForm($t_base)
{
	$kConfig = new cConfig;
	$kAdmin =  new cAdmin;
        $kService = new cServices();
        
	$forwardWareHouseArr = array();
	$forwardCountriesArr = array();
        $warehouseTypeAry = array();
        
	//GETTING ALL FORWARDER DETAILS
	$allForwarderList = $kAdmin->getAllForwarderNameList();
	$forwardCountriesArr = $kConfig->getAllCountryInPair();
        $warehouseTypeAry = $kService->getAllWarehouseType();
	?>
	<form name = "currentCFS" id="currentCFS">
            <p class="fl-25 f-size-12">
                <?=t($t_base.'fields/forwarders');?> <br>
                <select name="currentHaulage[idForwarder]" id="idForwarderList" style="width: 150px;" onchange="update_country_dropdown(this.value)">
                    <option value=""><?=t($t_base.'fields/all');?></option>
                    <?php
                        if(!empty($allForwarderList))
                        {
                            foreach($allForwarderList as $forwarderList)
                            {
                                ?>
                                <option value="<?=$forwarderList['id']?>"><?=$forwarderList['szDisplayName']?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </p>
            <p class="fl-25 f-size-12">
                <?=t($t_base.'fields/country');?><br>
                <span id="country_drop_down_span">
                    <select name="currentHaulage[idCountry]" id="idOriginCountry"  style="width: 150px;">
                        <option value=""><?=t($t_base.'fields/all');?></option>
                        <?php
                            if(!empty($forwardCountriesArr))
                            {
                                foreach($forwardCountriesArr as $forwardCountriesArrs)
                                {
                                    ?>
                                    <option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </span>
            </p>
            <p class="fl-25 f-size-12">
                Type<br> 
                <select name="currentHaulage[iWarehouseType]" id="iWarehouseType"  style="width: 150px;">
                    <?php
                        if(!empty($warehouseTypeAry))
                        {
                            foreach($warehouseTypeAry as $warehouseTypeArys)
                            {
                                ?>
                                <option value="<?=$warehouseTypeArys['id']?>"><?=$warehouseTypeArys['szFriendlyName']?></option>
                                <?php
                            }
                        }
                    ?>
                </select> 
            </p>
            <p class="fl-15 f-size-12" style="float: right;">
                &nbsp;
                <input type="hidden" name="mode" value="SHOW_CFS_LISTING"><br>
                <a class="button1" onclick="submit_currentCFS_form();" style="float: right;"><span><?=t($t_base.'fields/show');?></span></a>
            </p>
        </form>
        <div style="clear: both;"></div>
        <br />
	<?php
}

function showCurrentCFSMapTopForm($t_base)
{
    $kConfig = new cConfig;
    $kAdmin =  new cAdmin;
    $kService = new cServices();
        
    $forwardWareHouseArr = array();
    $forwardCountriesArr = array();
    $warehouseTypeAry = array();

    //GETTING ALL FORWARDER DETAILS
    $allForwarderList = $kAdmin->getAllForwarderNameList();
    $forwardCountriesArr=$kConfig->getAllCountryInPair();
    $warehouseTypeAry = $kService->getAllWarehouseType();
	
?>
    <form method="post" id="cfs_location_map_form" action="<?=__MANAGEMENT_URL__?>/cfsLocationMap.php" target="google_map_target_1">
        <p class="fl-25 f-size-12">
            <?=t($t_base.'fields/forwarders');?> <br>
            <select name="currentHaulage[idForwarder]" id="idForwarderList" style="width: 150px;" onchange="update_country_dropdown(this.value)">
                <option value=""><?=t($t_base.'fields/all');?></option>
                <?php
                    if(!empty($allForwarderList))
                    {
                        foreach($allForwarderList as $forwarderList)
                        {
                            ?>
                            <option value="<?=$forwarderList['id']?>"><?=$forwarderList['szDisplayName']?></option>
                            <?php
                        }
                    }
                ?>
            </select>
        </p>
        <p class="fl-25 f-size-12">
            <?=t($t_base.'fields/country');?> <br>
            <span id="country_drop_down_span">
                <select name="currentHaulage[idCountry]" id="idOriginCountry"  style="width: 150px;">
                    <option value=""><?=t($t_base.'fields/all');?></option>
                    <?php
                        if(!empty($forwardCountriesArr))
                        {
                            foreach($forwardCountriesArr as $forwardCountriesArrs)
                            {
                                ?>
                                <option value="<?=$forwardCountriesArrs['id']?>"><?=$forwardCountriesArrs['szCountryName']?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </span>
        </p>
        <p class="fl-25 f-size-12">
            Type<br> 
            <select name="currentHaulage[iWarehouseType]" id="iWarehouseType"  style="width: 150px;">
                <?php
                    if(!empty($warehouseTypeAry))
                    {
                        foreach($warehouseTypeAry as $warehouseTypeArys)
                        {
                            ?>
                            <option value="<?=$warehouseTypeArys['id']?>"><?=$warehouseTypeArys['szFriendlyName']?></option>
                            <?php
                        }
                    }
                ?>
            </select> 
        </p>
        <p class="fl-15 f-size-12" style="float: right;">
            &nbsp;
            <input type="hidden" name="mode" value="SHOW_CFS_LISTING"><br>
            <a class="button1" onclick="submit_CFS_map_form();" style="float: right;"><span><?=t($t_base.'fields/show');?></span></a>
        </p>
    </form>
    <div style="clear: both;"></div> <br />
    <?php
}

function showConfirmationPopUpMessage($t_base)
{
	echo '<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup"> 
							<h5><b>'.t($t_base.'title/resendBooking').'</b></h5>
							<p>'.t($t_base.'title/the_booking_email_send_successfully').'</p>
							<br>
							<p align="center"><a class="button1" onclick="cancelEditMAnageVar(0)"><span>'.t($t_base.'fields/close').'</span></a></p>
						</div>
					</div>';

}
function show_success_forwarder_creation_popup($t_base,$szEmail)
{?>
	<div id="popup-bg"></div>
		<div id="popup-container">	
			<div class="compare-popup popup">
				<h5><strong><?=t($t_base.'title/new_forwarder_created')?></strong></h5>
				<p><?=t($t_base.'title/new_forwarder_line_1')?> <?=$szEmail?></p><br />
				<p align="center"><a href="javascript:void(0)" onclick="close_create_forwarder_popup();" class="button1"><span><?=t($t_base.'fields/ok')?></span></a> </p>
			</div>
		</div>	
<?	
}
function display_haulage_tryitout_calculation_details_admin($searchedAry,$idForwarder)
{
	$t_base = "TryItOut/";
	
	if(!empty($searchedAry))
	{
		$kForwarder = new cForwarder();
		$kForwarder->load($idForwarder);
		if($kForwarder->szCurrency >0)
		{
			$idCurrency = $kForwarder->szCurrency ;
			$kWhsSearch = new cWHSSearch();
			if($idCurrency==1)
			{
				$forwarderCurrencyAry['idCurrency']=1;
				$forwarderCurrencyAry['szCurrency'] = 'USD';
				$forwarderCurrencyAry['fExchangeRate'] = 1;
			}
			else
			{
				$resultAry = $kWhsSearch->getCurrencyDetails($idCurrency);								
				$forwarderCurrencyAry['idCurrency']=$resultAry['idCurrency'];
				$forwarderCurrencyAry['szCurrency'] = $resultAry['szCurrency'];
				$forwarderCurrencyAry['fExchangeRate'] = $resultAry['fUsdValue'];
			}
			if($forwarderCurrencyAry['fExchangeRate']>0)
			{
				$fHaulageTotalRoe = number_format((float)((1/((1/$forwarderCurrencyAry['fExchangeRate'])/$searchedAry['fExchangeRate']))),4) ;
				//$fHaulageTotalRoe = number_format((float)($searchedAry['fExchangeRate']/$forwarderCurrencyAry['fExchangeRate']),4) ;
			}
			else
			{
				$fHaulageTotalRoe = 0;
			}
		}
		else
		{
			echo "Please update your currency then only we are able to display you Test result in your currency.";
			die;
		}
		
		if($searchedAry['iDirection']==1)
		{
			$direction_str = t($t_base.'fields/Export');
			$to_from = t($t_base.'fields/from');
		}
		else
		{
			$direction_str = t($t_base.'fields/Import');
			$to_from = t($t_base.'fields/to');
		}
		
		$kWhsSearch = new cWHSSearch();	
		$idWarehouse = $searchedAry['idWarehouse'];
		$kWhsSearch->load($idWarehouse);
		$szWhsName = $kWhsSearch->szWareHouseName ;
		
		$weight_bracket_distance = t($t_base.'fields/upto')." ".number_format((float)$searchedAry['iDistanceUptoKm'])." km ".t($t_base.'fields/and_weight')." ".t($t_base.'fields/upto')." ".number_format((float)$searchedAry['iUpToKg'])." kg ";
		
		$weight_bracket  = t($t_base.'fields/upto')." ".number_format((float)$searchedAry['iUpToKg'])." kg ";
		
		if($searchedAry['idHaulageModel']==__HAULAGE_ZONE_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/Pricing')." ".$to_from." ".t($t_base.'fields/zone')." ".$searchedAry['szModelName']." ".$weight_bracket." ".t($t_base.'fields/applies_to_this');
			//." ".$direction_str." ".t($t_base.'fields/haulage')
		}
		else if($searchedAry['idHaulageModel']==__HAULAGE_CITY_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/Pricing')." ".$to_from." ".$searchedAry['szModelName']." ".$weight_bracket." ".t($t_base.'fields/applies');
		}
		else if($searchedAry['idHaulageModel']==__HAULAGE_POSTCODE_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/Pricing')." ".$to_from." ".t($t_base.'fields/Postcode')." ".t($t_base.'fields/set')." ".$searchedAry['szModelName']." ".$weight_bracket." ".t($t_base.'fields/applies');
		}
		else if($searchedAry['idHaulageModel']==__HAULAGE_DISTANCE_PRICING_MODEL_ID__)
		{
			$header_text = t($t_base.'fields/pricing_based_on_distance')." ".$weight_bracket_distance." ".t($t_base.'fields/applies');
		}
		//class="td-border-bottom"
		?>
		<p style="margin-top:10px;margin-bottom:6px;"><?=$header_text?>: </p>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr >
		    <th style="width:28%" align="left">&nbsp;</th>
		    <th style="width:24%" align="right"><?=t($t_base.'fields/price');?>*</th>
		    <th style="width:24%" align="right"><?=t($t_base.'fields/roe');?></th>
		    <th style="width:24%" align="right"><?=t($t_base.'fields/price');?></th>
		  </tr>
		  <?
		  	if($searchedAry['iDirection']==1)
			{
				$szOriginAddress =  $searchedAry['szCityPostCode']."<br /> ".$searchedAry['szCountryName'];
		   	 	$szDestinationAddress = $searchedAry['szWhsPostcode']." - ".$searchedAry['szWhsCity']."<br />".$searchedAry['szWhsCountry'];
		    	$origin_lat_lang_pipeline = $searchedAry['szLatitude']."|".$searchedAry['szLongitude']."|".$searchedAry['szWhsLatitude']."|".$searchedAry['szWhsLongitude']."|".number_format((float)$searchedAry['iDistance'])."|".$szOriginAddress."|".$szDestinationAddress."|1";
		  ?>
		  <tr>
		    <td><?=t($t_base.'fields/export_haulage');?> <a href="javascript:void(0);" onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');">show on map</a></td>
		    <td align="right"><?=$searchedAry['szHaulageCurrency']?> <?=$searchedAry['fExchangeRate']>0 ? number_format((float)($searchedAry['fTotalHaulgePrice'] * $searchedAry['fExchangeRate']),2) : 0.00?></td>
		    <td align="right"><?=$fHaulageTotalRoe?></p></td>
		    <td align="right"> <?=$forwarderCurrencyAry['szCurrency']?> <?=number_format((float)($searchedAry['fTotalHaulgePrice']/$forwarderCurrencyAry['fExchangeRate']),2)?></td>
		  </tr>
		   <tr>
		     <td colspan="4" align="left" class="color f-size-12" valign="top"><em>*<?=t($t_base.'fields/calculation');?>: (<?=t($t_base.'fields/max');?>(ROUNDUP(<?=t($t_base.'fields/max');?>(<?=number_format((float)$searchedAry['fCargoWeight'])?>kg ; <?=get_formated_cargo_measure((float)$searchedAry['fCargoVolume'])?><i>cbm</i> x <?=number_format((float)$searchedAry['fWmFactor'])?>kg/cbm)/100kg) x <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePer100Kg'],2)?>) ; <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fMinimumPrice'],2)?>) + <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePerBooking'],2)?>) x (1 + <?=number_format((float)$searchedAry['fFuelPercentage'],1)?>%)  </em></td>
		  </tr>
		  <?
			}
			else 
			{
				$szOriginAddress =  $searchedAry['szCityPostCode']."<br /> ".$searchedAry['szCountryName'];
		   	 	$szDestinationAddress = $searchedAry['szWhsPostcode']." - ".$searchedAry['szWhsCity']."<br />".$searchedAry['szWhsCountry'];
		    	$origin_lat_lang_pipeline = $searchedAry['szLatitude']."|".$searchedAry['szLongitude']."|".$searchedAry['szWhsLatitude']."|".$searchedAry['szWhsLongitude']."|".number_format((float)$searchedAry['iDistance'])."|".$szOriginAddress."|".$szDestinationAddress."|2";
		  ?>
		  	<tr>
			    <td><?=t($t_base.'fields/import_haulage');?> <a href="javascript:void(0);" onclick="open_google_map_popup('<?=$origin_lat_lang_pipeline?>');">show on map</a></td>
			    <td align="right"><?=$searchedAry['szHaulageCurrency']?> <?=$searchedAry['fExchangeRate']>0 ? number_format((float)($searchedAry['fTotalHaulgePrice'] * $searchedAry['fExchangeRate']),2) : 0.00?></td>
			    <td align="right"><?=$fHaulageTotalRoe?></p></td>
			    <td align="right"><?=$forwarderCurrencyAry['szCurrency']?> <?=number_format((float)($searchedAry['fTotalHaulgePrice']/$forwarderCurrencyAry['fExchangeRate']),2)?></td>
			  </tr>
			  <tr>
			     <td colspan="4" align="left" class="color f-size-12" valign="top"><em>*<?=t($t_base.'fields/calculation');?>: (<?=t($t_base.'fields/max');?>(ROUNDUP(<?=t($t_base.'fields/max');?>(<?=number_format((float)$searchedAry['fCargoWeight'])?>kg ; <?=format_volume((float)$searchedAry['fCargoVolume'])?><i>cbm</i> x <?=number_format((float)$searchedAry['fWmFactor'])?>kg/cbm)/100kg) x <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePer100Kg'],2)?>) ; <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fMinimumPrice'],2)?>) + <?=$searchedAry['szHaulageCurrency']?> <?=number_format((float)$searchedAry['fPricePerBooking'],2)?>) x (1 + <?=number_format((float)$searchedAry['fFuelPercentage'],1)?>%)  </em></td>
			  </tr>
		 <? } ?>
		  </table>
		<?
	}
	else
	{
		?>
		<p style="margin-top:10px;"><?=t($t_base.'fields/no_active_haulage');?>.</p>		
		<?
	}
}
function returnLimitData2($data,$limit)
{
	if(strlen($data)>$limit)
	{
		$szDisplayName=substr_replace($data,'.',$limit);
	}
	else
	{
		$szDisplayName=$data;
	}
	return $szDisplayName;
}

function showMonthlyReportTopTable($monthlyBookingAry)
{
	$t_base="management/visualization/";
	?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
			<td style="width:30%"><strong><?=t($t_base.'titles/gate');?> (<?=t($t_base.'titles/nominal_count');?>)</strong></td>
			<?php 
				for($i=0;$i<6;$i++)
				{
					$currentMonthDate=date('Y-m-01');
					$newdate = strtotime ( "-".$i." MONTH" , strtotime ( $currentMonthDate ) ) ;
					?>
					<td align="right" valign="top" style="width:11%;"><strong><?php echo date('F',$newdate);?></strong></td>
					<?php 
				}
			?>
		</tr>
		<?php
			if(!empty($monthlyBookingAry))
			{
				for($counter=1;$counter<=14;$counter++)
				{
					?>
					<tr>
						<td ><?=$counter?>. <?=t($t_base.'titles/booking_step_'.$counter);?></td>
						<?php 
						for($i=0;$i<6;$i++)
						{
							$currentMonthDate=date('Y-m-01');
							$newdate = strtotime ( "-".$i." MONTH" , strtotime ( $currentMonthDate ) ) ;
							$szMonthName = date('F',$newdate);
							
							?>
							<td align="right" valign="top"><?php echo number_format((int)$monthlyBookingAry[$szMonthName]['icountStep'][$counter])?></td>
							<?php 
						}
					?>
					</tr>
					<?php
				}
			}
		?>
	</table>
<?
}

function showMonthlyReportBottomTable($monthlyBookingAry,$iIndexRow=1)
{
	$t_base="management/visualization/";
	?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
			<td style="width:30%"><strong><?=t($t_base.'titles/gate');?> (<?=t($t_base.'titles/indexed_count');?>)</strong></td>
			<?php 
				for($i=0;$i<6;$i++)
				{
					$currentMonthDate=date('Y-m-01');
					$newdate = strtotime ( "-".$i." MONTH" , strtotime ( $currentMonthDate ) ) ;
					?>
					<td align="right" valign="top" style="width:11%"><strong><?php echo date('F',$newdate);?></strong></td>
					<?php 
				}
			?>
		</tr>
		<?php
			if(!empty($monthlyBookingAry))
			{
				for($counter=1;$counter<=14;$counter++)
				{
					?>
					<tr>
						<td onclick="display_indexed_count_by_booking_step('<?=$counter?>');" style="cursor:pointer;"><?=$counter?>. <?=t($t_base.'titles/booking_step_'.$counter);?><?php if($counter==$iIndexRow){?>* <?php }?></td>
						<?php 
						for($i=0;$i<6;$i++)
						{
						
							$currentMonthDate=date('Y-m-01');
							$newdate = strtotime ( "-".$i." MONTH" , strtotime ( $currentMonthDate ) ) ;
							$szMonthName = date('F',$newdate);
							
							?>
							<td align="right" valign="top">
								<? 
									/*
									 * If the number in the cell is greater than or equal to 1000, they show with zero decimals and thousand separator.
									 */
									if($monthlyBookingAry[$szMonthName]['fPercentageStep'][$counter]>=1000)
									{
										echo number_format((float)$monthlyBookingAry[$szMonthName]['fPercentageStep'][$counter]);
									}
									else
									{
										echo number_format((float)$monthlyBookingAry[$szMonthName]['fPercentageStep'][$counter],2);
									}
									?>
							</td>
							<?php 
						}
					?>
					</tr>
					<?php
				}
			}
		?>
	</table>
	<p class="">* Index row - click any gate to change</p>
<?php
}

function showSellingTradeBottomTable($sellingTradeAry)
{
	$t_base="management/visualization/";
	?>
	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
			<td style="width:20%;"><strong><?=t($t_base.'titles/trade');?></strong></td>
			<td align="right" valign="top" style="width:10%;" ><strong><?=t($t_base.'titles/turn_over');?></strong></td>
			<td align="right" valign="top" style="width:10%;"><strong><?=t($t_base.'titles/revenue');?></strong></td>
			<td align="right" valign="top" style="width:10%;"><strong><?=t($t_base.'titles/bkg');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/ptp');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/ptw');?></strong></td>
			
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/wtp');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/wtw');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/ptd');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/wtd');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/dtp');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/dtw');?></strong></td>
			<td align="right" valign="top" style="width:5%;"><strong><?=t($t_base.'titles/dtd');?></strong></td>
		</tr>
		<?php
			if(!empty($sellingTradeAry))
			{
				foreach($sellingTradeAry as $sellingTradeArys)
				{
					?>
					<tr>
						<td><?php echo $sellingTradeArys['szTradeName'];?></td>
						<td align="right" valign="top"><?php echo number_format((float)$sellingTradeArys['fTotalTurnOver']);?></td>
						<td align="right" valign="top"><?php echo number_format((float)$sellingTradeArys['fTotalRevenue']);?></td>
						<td align="right" valign="top"><?php echo number_format((float)$sellingTradeArys['iBookingCounter']);?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iPTPCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iPTWCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iWTPCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iWTWCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iPTDCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iWTDCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iDTPCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iDTWCount'];?></td>
						<td align="right" valign="top"><?php echo $sellingTradeArys['iDTDCount'];?></td>
					</tr>
					<?php
				}
			}
			else
			{
				?>
				<tr>
					<td colspan="13" align="center"><strong>NO DATA TO SHOW REPORT</strong></td>
				</tr>
				<?php
			}
		?>
	</table>
<?php
}

function operationStat1($searchBookingEachForwarder,$kBooking,$t_base,$sortBy = 1)
{
    ?> 
    <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
        <tr>
            <td width="28%" align="center" valign="top"></td>
            <td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(1,'lastSevenOne','tableNoOne')"><?=t($t_base.'fields/last_7_days')?><span id="lastSevenOne" class="tableNoOne">*</span></strong></td>
            <td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(2,'lastThirtyOne','tableNoOne')"><?=t($t_base.'fields/last_30_days')?><span id="lastThirtyOne" class="tableNoOne"></span></strong></td>
            <td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(3,'lastYearOne','tableNoOne')"><?=t($t_base.'fields/last_year')?><span id="lastYearOne" class="tableNoOne"></span></strong></td>
        </tr>
        <tr>
            <td width="28%" align="left" valign="top"></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
        </tr>
        <?php

        $arrStat1 = array();
        $i = 0; 
        if(!empty($searchBookingEachForwarder))
        {
            foreach( $searchBookingEachForwarder as $EachForwarder)
            {
                $perForwarderBookingDetails = $EachForwarder['szPerForwarderBookingDetails'];
                $perYearForwarderBookingDetails = $EachForwarder['szPerYearForwarderBookingDetails'];

                $EachForwarder['fTotalPriceUSD'] = $EachForwarder['fTotalPriceUSD'] + $EachForwarder['fTotalInsuranceRevenue'] ;
                $perForwarderBookingDetails['fTotalPriceUSD'] = $perForwarderBookingDetails['fTotalPriceUSD'] + $perForwarderBookingDetails['fTotalInsuranceRevenue'] ;
                $perYearForwarderBookingDetails['fTotalPriceUSD'] = $perYearForwarderBookingDetails['fTotalPriceUSD'] + $perYearForwarderBookingDetails['fTotalInsuranceRevenue'] ;
            
                $j = 0;
                $arrStat1[$i][$j] = $EachForwarder['szDisplayName']?returnLimitData($EachForwarder['szDisplayName'],23)." (".$EachForwarder['szForwarderAlias'].")":'N/A';
                $arrStat1[$i][++$j] = (int)$EachForwarder['fTotalBookings']>0?(int)$EachForwarder['fTotalBookings']:'';
                $arrStat1[$i][++$j] = (float)$EachForwarder['fTotalPriceUSD']>0?number_format((float)$EachForwarder['fTotalPriceUSD'],0):'';
                $arrStat1[$i][++$j] = (int)$perForwarderBookingDetails['fTotalBookings']>0?(int)$perForwarderBookingDetails['fTotalBookings']:'';
                $arrStat1[$i][++$j] = (float)$perForwarderBookingDetails['fTotalPriceUSD']>0?number_format((float)$perForwarderBookingDetails['fTotalPriceUSD'],0):'';
                $arrStat1[$i][++$j] = (int)$perYearForwarderBookingDetails['fTotalBookings']>0?(int)$perYearForwarderBookingDetails['fTotalBookings']:'';
                $arrStat1[$i][++$j] = (float)$perYearForwarderBookingDetails['fTotalPriceUSD']>0?number_format((float)$perYearForwarderBookingDetails['fTotalPriceUSD'],0):'';
                $i++; 
            }
        }
        switch($sortBy)
        {
            case 1:
            {
                $sortField = 1;
                break;
            }
            case 2:
            {
                $sortField = 3;
                break;
            }
            case 3:
            {
                $sortField = 5;
                break;
            }
            default:
            {
                $sortField = 1;
                break;
            }
        }
        $arrayDataNew1 = array();
        $arrayDataNew1 = sortArray($arrStat1,$sortField,true);
        unset($arrStat1);
        if(!empty($arrayDataNew1))
        {
            foreach($arrayDataNew1 as $key=>$value)
            {
                $j=0;
        ?>
                <tr>
                    <td align="left" valign="top"><?=$value[$j]?></td>
                    <td align="right" valign="top"><?=$value[++$j]?></td>
                    <td align="right" valign="top"><?=$value[++$j]?></td>
                    <td align="right" valign="top"><?=$value[++$j]?></td>
                    <td align="right" valign="top"><?=$value[++$j]?></td>
                    <td align="right" valign="top"><?=$value[++$j]?></td>
                    <td align="right" valign="top"><?=$value[++$j]?></td>
                </tr>
        <?php
                }
        }
?>	
        </table>

<?php
}
function operationStat2($searchCountryStatisticsCountryCoustomer,$kBooking,$t_base,$sortBy = 1)
{ 
    ?>
    <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
        <tr>
            <td width="28%" align="left" valign="top"><strong><?=t($t_base.'fields/top_15_customer_locations')?></strong></td>
            <td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(1,'lastSevenTwo','tableNoTwo')"><?=t($t_base.'fields/last_7_days')?><span id="lastSevenTwo" class="tableNoTwo">*</span></strong></td>
            <td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(2,'lastThirtyTwo','tableNoTwo')"><?=t($t_base.'fields/last_30_days')?><span id="lastThirtyTwo" class="tableNoTwo"></span></strong></td>
            <td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(3,'lastYearTwo','tableNoTwo')"><?=t($t_base.'fields/last_year')?><span id="lastYearTwo" class="tableNoTwo"></span></strong></td>
        </tr>
        <tr>
            <td width="28%" align="center" valign="top"></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
            <td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
        </tr>
        <?php
				
            
            if(!empty($searchCountryStatisticsCountryCoustomer))
            {	
                $k = 0;	
                $i = 0;
                $j=0;
                foreach($searchCountryStatisticsCountryCoustomer as $eachCountryArr)
                {	  
                    $szCountryName = $eachCountryArr['szCountryName'];
                    
                    $searchCountryStatistics = $eachCountryArr['searchCountryStatistics'];
                    $perCountryBookingDetails = $eachCountryArr['perCountryBookingDetails'];
                    $perYearCountryBookingDetails = $eachCountryArr['perYearCountryBookingDetails'];	

                    $searchCountryStatistics['fTotalPriceUSD'] = $searchCountryStatistics['fTotalPriceUSD'] + $searchCountryStatistics['fTotalInsuranceRevenue'] ;
                    $perCountryBookingDetails['fTotalPriceUSD'] = $perCountryBookingDetails['fTotalPriceUSD'] + $perCountryBookingDetails['fTotalInsuranceRevenue'] ;
                    $perYearCountryBookingDetails['fTotalPriceUSD'] = $perYearCountryBookingDetails['fTotalPriceUSD'] + $perYearCountryBookingDetails['fTotalInsuranceRevenue'] ;
                
                    $j = 0;	
                    $k = 1;
                    $arrayData2[$i][$j++] = returnLimitData($szCountryName,25);
                    $arrayData2[$i][$j++] = (int)$searchCountryStatistics['fTotalBookings']>0?(int)$searchCountryStatistics['fTotalBookings']:'';
                    $arrayData2[$i][$j++] = (float)$searchCountryStatistics['fTotalPriceUSD']>0?number_format((float)$searchCountryStatistics['fTotalPriceUSD'],0):'';
                    $arrayData2[$i][$j++] = (int)$perCountryBookingDetails['fTotalBookings']>0?(int)$perCountryBookingDetails['fTotalBookings']:'';
                    $arrayData2[$i][$j++] = (float)$perCountryBookingDetails['fTotalPriceUSD']>0?number_format((float)$perCountryBookingDetails['fTotalPriceUSD'],0):'';
                    $arrayData2[$i][$j++] = (int)$perYearCountryBookingDetails['fTotalBookings']>0?(int)$perYearCountryBookingDetails['fTotalBookings']:'';
                    $arrayData2[$i][$j++] = (float)$perYearCountryBookingDetails['fTotalPriceUSD']>0?number_format((float)$perYearCountryBookingDetails['fTotalPriceUSD'],0):'';
                    $i++;  
                } 
            }
		
            if($k == 0)
            {
                echo '<tr>
                        <td align="center" valign="top" colspan = "7">
                                <b>'.t($t_base.'fields/no_record_found').'</b>
                        </td>
                    </tr>';
            }
            else
            { 
                $sortField = sortField($sortBy);

                $arrayDataNew2 = array(); 
                $arrayDataNew2 = sortArray($arrayData2,$sortField,true); 
                unset($arrayData2);
                $count = 1;
                if(!empty($arrayDataNew2))
                {
                        foreach($arrayDataNew2 as $key=>$value)
                        {	
                            $j = 0;

                        ?>
                        <tr>
                            <td align="left" valign="top"><?=$count++;?>.&nbsp;<?=$value[$j++]?></td>
                            <td align="right" valign="top"><?=$value[$j++]?></td>
                            <td align="right" valign="top"><?=$value[$j++]?></td>
                            <td align="right" valign="top"><?=$value[$j++]?></td>
                            <td align="right" valign="top"><?=$value[$j++]?></td>
                            <td align="right" valign="top"><?=$value[$j++]?></td>
                            <td align="right" valign="top"><?=$value[$j++]?></td>
                        </tr>
                        <?php	
                        }
                }
            }
		?>
			</table>
	<?php
}
function operationStat3($originDestinationCountry,$kBooking,$t_base,$sortBy = 1)
{
	?>
		<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
			<tr>
				<td width="28%" align="left" valign="top"><strong><?=t($t_base.'fields/top_15_trades')?></strong></td>
				<td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(1,'lastSevenThree','tableNoThree')"><?=t($t_base.'fields/last_7_days')?><span id="lastSevenThree" class="tableNoThree">*</span></strong></td>
				<td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(2,'lastThirtyThree','tableNoThree')"><?=t($t_base.'fields/last_30_days')?><span id="lastThirtyThree" class="tableNoThree"></span></strong></td>
				<td width="24%" align="center" valign="top" colspan="2" class="cursor-pointer"><strong onclick="CheckLastDataFirst(3,'lastYearThree','tableNoThree')"><?=t($t_base.'fields/last_year')?><span id="lastYearThree" class="tableNoThree"></span></strong></td>
			</tr>
			<tr>
				<td width="28%" align="center" valign="top"></td>
				<td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
				<td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
				<td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
				<td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
				<td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/bookings')?></strong></td>
				<td width="12%" align="right" valign="top"><strong><?=t($t_base.'fields/usd_value')?></strong></td>
			</tr>
			<?php 
				if($originDestinationCountry!=array())
				{
                                    $i = 0;
                                    $arrData3 = array();	
                                    foreach( $originDestinationCountry as $originDestinationStatistics)
                                    {	
                                        $j = 0;
                                        $countryFrom  = $originDestinationStatistics['szOriginCountry'];
                                        $countryTo  = $originDestinationStatistics['szDestinationCountry'];

                                        $originDestinationSevenDaysStatistics	= $originDestinationStatistics['originDestinationSevenDaysStatistics'];
                                        $OriginDestinationMonthBookingDetails	= $originDestinationStatistics['originDestinationMonthBookingDetails'];
                                        $OriginDestinationYearBookingDetails	= $originDestinationStatistics['originDestinationYearBookingDetails']; 
            
                                        $originDestinationSevenDaysStatistics['fTotalPriceUSD'] = $originDestinationSevenDaysStatistics['fTotalPriceUSD'] + $originDestinationSevenDaysStatistics['fTotalInsuranceRevenue'] ;
                                        $OriginDestinationMonthBookingDetails['fTotalPriceUSD'] = $OriginDestinationMonthBookingDetails['fTotalPriceUSD'] + $OriginDestinationMonthBookingDetails['fTotalInsuranceRevenue'] ;
                                        $OriginDestinationYearBookingDetails['fTotalPriceUSD'] = $OriginDestinationYearBookingDetails['fTotalPriceUSD'] + $OriginDestinationYearBookingDetails['fTotalInsuranceRevenue'] ;
                    
                                        $arrData3[$i][$j++] = returnLimitData($countryFrom,13)." to ". returnLimitData($countryTo,13);
                                        $arrData3[$i][$j++] = (int)$originDestinationSevenDaysStatistics['fTotalBookings']>0?(int)$originDestinationSevenDaysStatistics['fTotalBookings']:'';
                                        $arrData3[$i][$j++] = (float)$originDestinationSevenDaysStatistics['fTotalPriceUSD']>0?number_format((float)$originDestinationSevenDaysStatistics['fTotalPriceUSD'],0):'';
                                        $arrData3[$i][$j++] = (int)$OriginDestinationMonthBookingDetails['fTotalBookings']>0?(int)$OriginDestinationMonthBookingDetails['fTotalBookings']:'';
                                        $arrData3[$i][$j++] = (float)$OriginDestinationMonthBookingDetails['fTotalPriceUSD']>0?number_format((float)$OriginDestinationMonthBookingDetails['fTotalPriceUSD'],0):'';
                                        $arrData3[$i][$j++] = (int)$OriginDestinationYearBookingDetails['fTotalBookings']>0?(int)$OriginDestinationYearBookingDetails['fTotalBookings']:'';
                                        $arrData3[$i][$j++] = (float)$OriginDestinationYearBookingDetails['fTotalPriceUSD']>0?number_format((float)$OriginDestinationYearBookingDetails['fTotalPriceUSD'],0):'';
                                        $i++;	 
				?>	
				<?php
					}
				}
				
				if($originDestinationCountry!=array())
				{
                                    $sortField = sortField($sortBy);
                                    $arrayDataNew3 = array();
                                    $arrayDataNew3 = sortArray($arrData3,$sortField,true);
                                    unset($arrData3);
                                    if(!empty($arrayDataNew3))
                                    {
                                        foreach($arrayDataNew3 as $key=>$value)
                                        {
                                            $j=0;
                                            ?>
                                            <tr>
                                                <td align="left" valign="top"><?=$value[$j++]?></td>
                                                <td align="right" valign="top"><?=$value[$j++]?></td>
                                                <td align="right" valign="top"><?=$value[$j++]?></td>
                                                <td align="right" valign="top"><?=$value[$j++]?></td>
                                                <td align="right" valign="top"><?=$value[$j++]?></td>
                                                <td align="right" valign="top"><?=$value[$j++]?></td>
                                                <td align="right" valign="top"><?=$value[$j++]?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
				}
				else
				{
                                    echo '<tr>
                                            <td align="center" valign="top" colspan = "7">
                                                <b>'.t($t_base.'fields/no_record_found').'</b>
                                            </td>
                                        </tr>';
				}
		?>
			</table>
	<?php
}
function sortField($sortBy)
{
	switch($sortBy)
	{
            case 1:
            {
                    $sortField = 1;
                    break;
            }
            case 2:
            {
                    $sortField = 3;
                    break;
            }
            case 3:
            {
                    $sortField = 5;
                    break;
            }
            default:
            {
                    $sortField = 1;
                    break;
            }
	}
	return $sortField;
}


 
 function displaySearchedTradesData($searchedTradeAry,$sort_feild)
 {
 	$t_base="management/visualization/";
 	/*
 	if($sort_feild=='iSearched_30_days')
 	{
 		$iSearched_30_days_astric = "*";
 	}
 	else if($sort_feild=='iSearched_90_days')
 	{
 		$iSearched_90_days_astric = "*";
 	}
 	else if($sort_feild=='iSearched_365_days')
 	{
 		$iSearched_365_days_astric = "*";
 	}
 	else if($sort_feild=='iUnique_user_30_days')
 	{
 		$iUnique_user_30_days_astric = "*";
 	}
 	else if($sort_feild=='iUnique_user_90_days')
 	{
 		$iUnique_user_90_days_astric = "*";
 	}
 	else if($sort_feild=='iUnique_user_365_days')
 	{
 		$iUnique_user_365_days_astric = "*";
 	}
 	else if($sort_feild=='fConversionRate')
 	{
 		$fConversionRate_astric = "*";
 	}
 	else if($sort_feild=='iServiceAvailable')
 	{
 		$iServiceAvailable_astric = "*";
 	}
 	$iSearched_30_days_astric = "*";
 	*/
 	
 	$style_str = "style='cursor:pointer;'"
 ?>
 	<table cellspacing="0" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
			<td align="left"> <strong><?=t($t_base.'titles/searches');?> / 30 <?=t($t_base.'titles/days');?></strong>*</td>
			<td colspan="3" align="center"><strong><?=t($t_base.'titles/searches');?></strong></td>
			<td colspan="3" align="center"><strong><?=t($t_base.'titles/unique_users');?></strong></td>
			<td align="center"><strong><?=t($t_base.'titles/conversion');?></strong></td>
			<td align="center"><strong><?=t($t_base.'titles/currently');?></strong></td>
		</tr>
		<tr>
			<td onclick="sort_searched_trade('szTradeName');" <?php echo $style_str;?>><strong><?=t($t_base.'titles/trade');?></strong></td>
			<td align="center" onclick="sort_searched_trade('iSearched_30_days');" <?php echo $style_str;?>><strong>30 <?=t($t_base.'titles/days');?></strong><?=$iSearched_30_days_astric?></td>
			<td align="center" onclick="sort_searched_trade('iSearched_90_days');" <?php echo $style_str;?>><?=$iSearched_90_days_astric?><strong>90 <?=t($t_base.'titles/days');?></strong></td>
			<td align="center" onclick="sort_searched_trade('iSearched_365_days');" <?php echo $style_str;?>><?=$iSearched_365_days_astric?><strong>365 <?=t($t_base.'titles/days');?></strong></td>
			<td align="center" onclick="sort_searched_trade('iUnique_user_30_days');" <?php echo $style_str;?>><?=$iUnique_user_30_days_astric?><strong>30 <?=t($t_base.'titles/days');?></strong></td>
			<td align="center" onclick="sort_searched_trade('iUnique_user_90_days');" <?php echo $style_str;?>><?=$iUnique_user_90_days_astric?><strong>90 <?=t($t_base.'titles/days');?></strong></td>
			<td align="center" onclick="sort_searched_trade('iUnique_user_365_days');" <?php echo $style_str;?>><?=$iUnique_user_365_days_astric?><strong>365 <?=t($t_base.'titles/days');?></strong></td>
			<td align="center" onclick="sort_searched_trade('fConversionRate');" <?php echo $style_str;?>><?=$fConversionRate_astric?><strong>90 <?=t($t_base.'titles/days');?></strong></td>
			<td align="center" onclick="sort_searched_trade('iServiceAvailable');" <?php echo $style_str;?>><?=$iServiceAvailable_astric?><strong><?=t($t_base.'titles/available');?></strong></td>
		</tr>
		<?php
                    if(!empty($searchedTradeAry))
                    {
                        $ctr = 0; 
                        foreach($searchedTradeAry as $searchedTradeArys)
                        {
                            ?>
                            <tr>
                                <td><?php echo $searchedTradeArys['szTradeName'];?></td>
                                <td align="right" valign="top"><?php echo number_format((int)$searchedTradeArys['iSearched_30_days']);?></td>
                                <td align="right" valign="top"><?php echo number_format((int)$searchedTradeArys['iSearched_90_days']);?></td>
                                <td align="right" valign="top"><?php echo number_format((int)$searchedTradeArys['iSearched_365_days']);?></td>
                                <td align="right" valign="top"><?php echo number_format((int)$searchedTradeArys['iUnique_user_30_days']);?></td>
                                <td align="right" valign="top"><?php echo number_format((int)$searchedTradeArys['iUnique_user_90_days']);?></td>
                                <td align="right" valign="top"><?php echo number_format((int)$searchedTradeArys['iUnique_user_365_days']);?></td>
                                <td align="right" valign="top"><?php echo number_format((float)$searchedTradeArys['fConversionRate'],2)."%";?></td>
                                <td align="center" valign="top"><?php echo $searchedTradeArys['iServiceAvailable']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    else
                    {
                            ?>
                            <tr>
                                    <td colspan="13" align="center"><strong>NO DATA TO SHOW REPORT</strong></td>
                            </tr>
                            <?php
                    }
		?>
	</table>
	<input type="hidden" name="szOriginDestination" id="szOriginDestination" value="ORIGIN">
	<p>* Click a heading to sort table descending</p>
 	<?php 
 }
 
 
 
function createFeedBackExcelFile($feedbackDataGivenDateRange,$objPHPExcel,$kUser,$kNPS)
 {
 		$sheetindex=0;
 		date_default_timezone_set('UTC');	
		if($sheetindex>0)
		{
			$objPHPExcel->createSheet();
		}
		
		$objPHPExcel->setActiveSheetIndex($sheetindex);
		
		$styleArray = array(
				'font' => array(
					'bold' => true,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			
			$styleArray2 = array(
				'font' => array(
					'bold' => false,
					'size' =>12,
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
				'borders' => array(
					'bottom' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
					'left' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
					),
				),
			);
			
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50);

		$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setARGB('FFddd9c3');	
			
		$objPHPExcel->getActiveSheet()->setCellValue('A1','Date')->getStyle('A1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('B1','NPS')->getStyle('B1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('C1','Email')->getStyle('C1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('D1','First Name')->getStyle('D1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('E1','Last Name')->getStyle('E1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('F1','Country')->getStyle('F1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('G1','Searches (12m)')->getStyle('G1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('H1','Bookings (12m)')->getStyle('F1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
		
		$objPHPExcel->getActiveSheet()->setCellValue('I1','Suggestions')->getStyle('I1')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
		
		$rowcounter=2;
		if(!empty($feedbackDataGivenDateRange))
		{
			foreach($feedbackDataGivenDateRange as $res_arr)
			{
			
				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->applyFromArray($styleArray2);
				$objPHPExcel->getActiveSheet()->getStyle('I'.$rowcounter)->applyFromArray($styleArray2);
				
				$objPHPExcel->getActiveSheet()->getStyle('G'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0');
				$objPHPExcel->getActiveSheet()->getStyle('H'.$rowcounter)->getNumberFormat()->setFormatCode('#,##0');
									
				$colcounter=0;
				$kUser->getUserDetails($res_arr['idUser']);
				//echo $res_arr['dtComment'];
				$dtComment=' ';
	   			if(isset($res_arr['dtComment'])!='')
	   			{
		   			if($res_arr['dtComment']!='0000-00-00 00:00:00' && $res_arr['dtComment']!="")
		   			{
		   				$Comment_arr=explode('-',$res_arr['dtComment']);
						$dtComment = gmmktime(0,0,0,$Comment_arr[1],$Comment_arr[2],$Comment_arr[0]);
		   			}
	   			}
	   			
	   			$totalBooking=$kNPS->totalCountOfBookingForUser($res_arr['idUser']);
	   			
	   			$confirmedBooking=$kNPS->totalCountOfBookingForUser($res_arr['idUser'],true);
	   			
				
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($colcounter,$rowcounter)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,PHPExcel_Shared_Date::PHPToExcel($dtComment));

				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$res_arr['iScore']);
					
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$kUser->szEmail);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$kUser->szFirstName);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$kUser->szLastName);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$kUser->szCountryName);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$totalBooking);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$confirmedBooking);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colcounter++,$rowcounter,$res_arr['szComment']);
				
				
				$rowcounter++;					
			}
		}
		
		
		$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objPHPExcel->setActiveSheetIndex(0);
		//$ext = end(explode('.', $fileName));
		$fileName=__APP_PATH_ROOT__."/management/feedbacKData/Customer_Feedback_Data_".date('YmdHis').".xlsx";
		$objWriter->save($fileName);
		return $fileName;
 }
 
function returnLimitData3($data,$limit)
{
	if(strlen($data)>$limit)
	{
		$szDisplayName=substr_replace($data,'',$limit);
	}
	else
	{
		$szDisplayName=$data;
	}
	return $szDisplayName;
}

function adminForwarderDetails($t_base,$adminForwarderDetails,$bForwarderListing=false)
{
    require_once(__APP_PATH_CLASSES__."/forwarder.class.php");
    require_once(__APP_PATH_CLASSES__."/warehouseSearch.class.php");
    require_once(__APP_PATH_CLASSES__."/config.class.php");
    $kBooking   = new cBooking();
    $kForwarder	= new cForwarder();
    $kWHSSearch	= new cWHSSearch();
    $kExportImport = new cExport_Import();
    echo selectPageFormat('ForwarderDetail');

    if($bForwarderListing)
    { 
        $searchAry = $adminForwarderDetails ;   
        $iPage = $searchAry['iPage'] ;
        $kForwarder= new cForwarder(); 

        $adminForwarderDetails = array();
        $adminForwarderDetails = $kForwarder->getAllForwarderDetailsAdmin($iPage);
        $iGrandTotal = $kForwarder->getAllForwarderDetailsAdmin(false,true); 
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,__MAX_CUSTOMER_PER_PAGE__,10);
    } 
    
    ?>
    <div id="contentDetails">
        <table cellpadding="0" cellspacing="0" border="0" class="format-4" width="100%" id= "usertable" style="margin-bottom:5px;">
            <tr>
                <th width="24%" align="center" valign="top"><?=t($t_base.'fields/dispaly_name')?></th> 
                <th width="8%" align="center" valign="top"><?=t($t_base.'fields/country')?></th>
                <th width="6%"  style="text-align: center;" valign="top"><?=t($t_base.'fields/AO');?></th>
                <th width="12%" style="text-align: center;" valign="top"><?=t($t_base.'fields/user');?></th>
                <th width="12%" style="text-align: center;" valign="top"><?=t($t_base.'fields/lcl_haulage_cc');?></th>
                <th width="11%" style="text-align: right;" valign="top"><?=t($t_base.'fields/sales30');?></th> 
                <th width="11%" style="text-align: right;" valign="top"><?=t($t_base.'fields/sales90');?>	</th>
                <th width="12%" style="text-align: right;" valign="top"><?=t($t_base.'fields/sales365');?>	</th>
            </tr>
        <?php 
            $ctrl=1;
            if(!empty($adminForwarderDetails))
            {
                foreach($adminForwarderDetails as $forwarderCompanies)
                {
                    //****CALLING METHOD FOR GET COUNTRY NAME												****/
                    $countryName = $kBooking->findISOcode($forwarderCompanies['idCountry']);

                    //****CALLING METHOD FOR GETTING FORWARDER ONLINE STATUS								****/
                    $checkCompanyStatus = $kForwarder->checkForwarderComleteCompanyInfo((int)$forwarderCompanies['id']);
                    //$getStatusActive		= 	(($forwarderCompanies['iActive'] && $forwarderCompanies['iAgreeTNC'] && ($checkCompanyStatus==0))?'Yes':'No');//RETURNING COMPANY STATUS

                    //****CALLING METHOD FOR FINDING NUMBER OF CONTACT PERSON IN A CORRESPONDING FORWARDER	****/
                    $getCountForwarderRole = $kForwarder->countForwardersContactRole((int)$forwarderCompanies['id']);

                    //****CALLING METHOD FOR FINDING LCL OF A FORWARDER
                    $forwarderLclServices = $kWHSSearch->returnLcl((int)$forwarderCompanies['id']);

                    //****CALLING METHOD FOR FINDING CC OF A FORWARDER
                    $forwarderCC = $kWHSSearch->returnCC((int)$forwarderCompanies['id']);

                    //****CALLING METHOD FOR FINDING HAULAGE OF FORWARDER
                    $forwarderHaulage = $kExportImport->countForwaderHaulage((int)$forwarderCompanies['id']);
                    //****CALLING METHOD FOR FINDING SALES OF FORWARDER
                    $forwarderSales30 = null;
                    $forwarderSales90 = null;
                    $forwarderSales365 = null;
                    $forwarderSales30 = (($kForwarder->findForwarderCurrency((int)$forwarderCompanies['id']))?('USD '.number_format($kWHSSearch->returnSalesMonth((int)$forwarderCompanies['id'],30))):'N/A');
                    $forwarderSales90 = (($kForwarder->findForwarderCurrency((int)$forwarderCompanies['id']))?('USD '.number_format($kWHSSearch->returnSalesMonth((int)$forwarderCompanies['id'],90))):'N/A');
                    $forwarderSales365 = (($kForwarder->findForwarderCurrency((int)$forwarderCompanies['id']))?('USD '.number_format($kWHSSearch->returnSalesMonth((int)$forwarderCompanies['id'],365))):'N/A');

                    $activate_str = '';
                    if((int)$forwarderCompanies['iActive']==0)
                    {
                        $activate_str = 'activate';
                    } 
                    echo "
                        <tr onclick =select_to_Forwarder_companies('user_data_".$ctrl."','".$forwarderCompanies['id']."','".$activate_str."') id ='user_data_".$ctrl."'>
                        <td>".returnLimitData($forwarderCompanies['szDisplayName']?$forwarderCompanies['szDisplayName']." (".$forwarderCompanies['szForwarderAlias'].")":'URL: '.($kForwarder->findforwarderSubDomainName((int)$forwarderCompanies['id'])),24)."</td>
                        <td>".($countryName?$countryName:'N/A')."</td>
                        <td style=\"text-align: center;\">".($forwarderCompanies['iActive']?Y:N)."/".(($forwarderCompanies['isOnline'] && $forwarderCompanies['iActive'])?Y:N)."</td>
                        <td style=\"text-align: center;\">".$getCountForwarderRole['total']." (".$getCountForwarderRole['admin']."/".$getCountForwarderRole['pricing']."/".$getCountForwarderRole['billing'].")"."</td>
                        <td style=\"text-align: center;\">".$forwarderLclServices['lclServices'].'/'.$forwarderHaulage.'/'.$forwarderCC['pricingCC']."</td>
                        <td style=\"text-align: right;\"> ".$forwarderSales30."</td>
                        <td style=\"text-align: right;\">".$forwarderSales90."</td>
                        <td style=\"text-align: right;\">".$forwarderSales365."</td>
                        </tr>
                        ";
                    $ctrl++;
                }  
            } 
        ?>
        </table>
        <?php
            if($bForwarderListing && $iGrandTotal>0)
            { 
                $kPagination->idBookingFile = "'forwarderComp'"; 
                $kPagination->paginate("showProfilePagination");
                $kPagination->page = $iPage; 
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }
            } 
        ?>
        <p style="float: right;">
            <a class="button1" onclick ="addNewForwarder();" ><span><?=t($t_base.'fields/new')?></span></a>
            <a class="button1" id="admin_delete_button" style="opacity:0.4;"><span id="admin_delete_span"><?=t($t_base.'fields/delete')?></span></a>	
            <a class="button1" id="admin_edit_button" style="opacity:0.4;"><span><?=t($t_base.'fields/edit')?></span></a>			
        </p>
    </div>	
    <div style="clear: both;"></div> 
	<?php
}

function showForwarderDeleteConfirmation($idForwarder)
{
	$t_base="management/LandingPage/";	
	if($idForwarder>0)
	{
		$kForwarder = new cForwarder();
		$kForwarder->load($idForwarder);
		
		if(empty($kForwarder->szDisplayName))
		{
			$szForwarderDisName = "URL:".$kForwarder->szControlPanelUrl;
		}
		else
		{
			$szForwarderDisName = $kForwarder->szDisplayName;
		}
		
		$iForwarderHasBooking = $kForwarder->isForwarderHasBooking($idForwarder);
	}
	?>
 	<div id="popup-bg"></div>
	 	<div id="popup-container">	
			<div class="popup contact-popup" style="height: auto;">
				<h5><?php echo t($t_base.'title/delete_forwarder'); ?></h5>
				<?php 
					if((int)$iForwarderHasBooking>0)
					{
						if($iForwarderHasBooking==1)
						{
							$booking_str = t($t_base.'title/booking');
						}
						else 
						{
							$booking_str = t($t_base.'title/bookings');
						}
						?>
						<p><?php echo t($t_base.'title/you_can_delete')." ".$szForwarderDisName.". ".$iForwarderHasBooking." ".$booking_str." ".t($t_base.'title/booking_made_with')." ".$szForwarderDisName." ".t($t_base.'title/on_transporteca')."."; ?></p>
						<?php 
					}
					else
					{
				?>
					<p><?php echo t($t_base.'title/delete_forwarder_confirmation_message')." ".$szForwarderDisName."?"?></p>
				<?php } ?><br />
				<p align="center">	
				<?php 
					if((int)$iForwarderHasBooking==0)
					{
				?>
					<a href="javascript:void(0);" onclick="deleteFrorwarderConfirm('<?php echo $idForwarder; ?>')" class="button1"><span><?php echo t($t_base.'fields/confirm'); ?></span></a>
				<?php } else {?>
						<a href="javascript:void(0);" onclick="inactivateFrorwarderConfirm('<?php echo $idForwarder; ?>')" class="button1"><span><?php echo t($t_base.'fields/inactivate'); ?></span></a>
				<?php }?>			
					<a href="javascript:void(0);" onclick="showHide('confirmation_popup');" class="button2"><span><?php echo t($t_base.'fields/cancel');?></span></a>			
				</p>
			</div>
	 	</div>
	</div> 	
	<?php 
}

function adminForwarderProfileAccessHistory($t_base,$idForwarder)
{
	$sql="
		idForwarderContact='".(int)$idForwarder."'
	";
	$kForwarderContact = new cForwarderContact();
	$forwarderContactDetailAry = array();
	$forwarderContactDetailAry = $kForwarderContact->getForwarderContactFnLn($idForwarder);
	
	$kAdmin= new cAdmin();
	$forwarderProfileHistory=$kAdmin->getForwarderAccessHistory($sql,$forwarderContactDetailAry);
	?>
	<? ///forwarderProfileHeader('ForwarderHistory',$idForwarder);?>
	<div id="popup-bg"></div>
					<div id="popup-container">
						<div class="popup" style="width: 600px;height: 400px;"> 
					<h4><b>History for <?=$forwarderContactDetailAry['szFirstName']." ".$forwarderContactDetailAry['szLastName']?></b></h4>
					<div class="scroll-div" style="height: 300px;padding: 0px;">
						<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" >
							<tr>
								<td width="19%"><strong><?=t($t_base.'fields/created_date')?></strong></td>
								<td width="81%"><strong><?=t($t_base.'fields/activity')?></strong></td>
							</tr>	
							<?
								if(!empty($forwarderProfileHistory))
								{
									foreach($forwarderProfileHistory as $forwarderProfileHistorys)
									{
									?>
							<tr>
								<td ><?=((!empty($forwarderProfileHistorys['dtCreatedOn']) && $forwarderProfileHistorys['dtCreatedOn']!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($forwarderProfileHistorys["dtCreatedOn"])):"")?></td>
								<td ><?=$forwarderProfileHistorys['szTitle']?></td>
							</tr>	
							<? }}?>
							</table>
						</div>	
		<p align="center">
		<a href="javascript:void(0);" class="button2"  onclick="hideEditForwarderProfile();"><span><?=t($t_base.'fields/close');?></span></a>
		</p>
		</div>
	</div>	
<?php	
}
function adminProfileDetailsManagement($t_base,$allCustomerArr,$bCustomerListing=false)
{
    $kAdmin = new cAdmin(); 
    if($bCustomerListing)
    { 
        $searchAry = $allCustomerArr ; 
        $sortby = $searchAry['szSortBy'];  
        $sortOrder = $searchAry['szSortOrder'] ;
        $iPage = $searchAry['iPage'] ;
        
        $allCustomerArr = array();
        $allCustomerArr = $kAdmin->getAllCustomers($sortby,$sortOrder); 
//        $iGrandTotal = $kAdmin->getAllCustomers($sortby,$sortOrder,false,true);
//            
//        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
//	$kPagination  = new Pagination($iGrandTotal,__MAX_CUSTOMER_PER_PAGE__,10);
    } 
    echo selectPageFormat('user'); 
    ?> 
    <table cellpadding="0" cellspacing="0" border="0" class="format-4" width="100%" id="usertable" style="margin-bottom:5px;">
        <tr>
            <th width="10%" align="left" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('u.dtCreateOn','ASC','joined_date','cust')" id="joined_date"><span class="sort-arrow-down">&nbsp;</span></a><strong><?=t($t_base.'fields/joined')?></strong></th> 
            <th width="5%" align="left" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('c.szCountryISO','ASC','country','cust')" id="country"><span class="sort-arrow-up-grey">&nbsp;</span></a><strong><?=t($t_base.'fields/country_cc');?></strong></th>
            <th width="31%" align="left" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('u.szEmail','ASC','email','cust')" id="email"><span class="sort-arrow-up-grey">&nbsp;</span></a><strong><?=t($t_base.'fields/email');?></strong></th>
            <th width="7%" align="center" valign="top" style="text-align: center;"><strong>A/V</strong></th>
            <th width="10%" align="left" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('u.dtLastLogin','DESC','lastLogin','cust')" id="lastLogin"><span class="sort-arrow-down-grey">&nbsp;</span></a><strong><?=t($t_base.'fields/login');?></strong></th>
            <th width="7%" align="center" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('total','DESC','bookings','cust')" id="bookings"><span class="sort-arrow-down-grey">&nbsp;</span></a><strong><?=t($t_base.'fields/bookings');?></strong></th>
            <th width="7%" align="center" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('u.iLastestScore','DESC','nps','cust')" id="nps"><span class="sort-arrow-down-grey">&nbsp;</span></a><strong><?=t($t_base.'fields/nps');?></strong></th>
            <th width="12%" align="right" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('totalAmount','DESC','revenue','cust')" id="revenue"><span class="sort-arrow-down-grey">&nbsp;</span></a><strong><?=t($t_base.'fields/revenue');?></strong></th>
            <th width="11%" align="left" valign="top"><a href="javascript:void(0);" style="text-decoration:none;"  onclick="sort_customer_table('dtBookingConfirmed','DESC','last_date','cust')" id="last_date"><span class="sort-arrow-down-grey">&nbsp;</span></a><strong><?=t($t_base.'fields/last_booking');?></strong></th>
        </tr>
    <?php 
        if(!empty($allCustomerArr))
        {
            $i=1;	
            foreach($allCustomerArr as $allCustomerArrs)
            {
                //$totalBookingArr=array();
                //$totalBookingArr=$kAdmin->getAllBookingForUser($allCustomerArrs['id']); 
    ?>
            <tr id="user_data_<?=$i?>" onclick="select_to_edit_user('user_data_<?=$i?>','<?=$allCustomerArrs['id']?>');">
                <td><?=date('d/m/Y',strtotime($allCustomerArrs['dtCreateOn']));?></td>
                <td><?=$allCustomerArrs['szCountryISO']?></td>
                <td><?=returnLimitData($allCustomerArrs['szEmail'],30)?></td>
                <td align="center"><?php echo (int)$allCustomerArrs['iActive'] == 1 ? 'Y': 'N';
                    echo "/";
			echo (int)$allCustomerArrs['iConfirmed'] == 1 ? 'Y':'N';?></td>
                <td><?=($allCustomerArrs['dtLastLogin']!='0000-00-00 00:00:00'?date('d/m/Y', strtotime($allCustomerArrs['dtLastLogin'])):'N/A');?></td>
                <td align="center"><?=$allCustomerArrs['total']?></td>
                <td align="center"><?=($allCustomerArrs['iScore']?$allCustomerArrs['iScore']:'N/A')?></td>
                <td align="right">USD <?=$allCustomerArrs['totalAmount']>0 ? number_format((float)$allCustomerArrs['totalAmount']):'0'?></td>
                <td><?=($allCustomerArrs['dtBookingConfirmed']!='' &&$allCustomerArrs['dtBookingConfirmed']!='0000-00-00 00:00:00')  ? date('d/m/Y',strtotime($allCustomerArrs['dtBookingConfirmed'])):'N/A'?></td>
            </tr>
        <?php    	
            ++$i;
            }
        }
        ?>
    </table>			
        <div style="clear: both;"></div>
        <p align="right"><a href="javascript:void(0)" id="edit_button" style="opacity:0.4;" class="button1"><span>Edit</span></a></p>
    <?php
        if($bCustomerListing && $iGrandTotal>0)
        { 
//            $kPagination->idBookingFile = "'cust'"; 
//            $kPagination->paginate("showProfilePagination");
//            $kPagination->page = $iPage; 
//            if($kPagination->links_per_page>1)
//            {
//                echo $kPagination->renderFullNav();
//            }
        } 
    ?>
    </div>
	<?php
}
function sendMessageQueueLists()
{
	$kAdmin = new cAdmin();
	$t_base="management/AdminMessages/";
	$allSendMessageArr=$kAdmin->getAllQueueMessageSend();
	//print_r($allSendMessageArr);
	
	
?>
	<table cellspacing="0" id="send_message_list" cellpadding="0" border="0" class="format-2" width="100%">
		<tr>
			<td style="text-align:left;width:16%;"><strong><?=t($t_base.'fields/date_time')?></strong></td>
			<td style="text-align:left;width:60%;"><strong><?=t($t_base.'fields/subject')?></strong></td>
			<td style="text-align:left;width:17%;"><strong><?=t($t_base.'fields/recipient_total')?></strong></td>
			<td style="text-align:left;width:7%;"></td>
		</tr>
		<?
			if(!empty($allSendMessageArr))
			{
				$i=1;
				foreach($allSendMessageArr as $allSendMessageArrs)
				{
					//print_r($allSendMessageArrs['szSubject']);
					//die;
					?>
						<tr id="message_data_<?=$i?>" onclick="select_message_queue_send_tr('message_data_<?=$i?>','<?=$allSendMessageArrs['idBatch']?>')">
							<td><?=((!empty($allSendMessageArrs["dtCreated"]) && $allSendMessageArrs["dtCreated"]!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($allSendMessageArrs["dtCreated"])):"")?></td>
							<td><?=returnLimitData($allSendMessageArrs['szSubject'],50)?></td>
							<td><?=$allSendMessageArrs['total']?></td>
							<td><a href="javascript:void(0)" onclick="delete_messages('<?=$allSendMessageArrs['idBatch']?>')"><?=t($t_base.'fields/delete');?></a></td>
						</tr>
					<?
					++$i;
				}
			}
		?>
	</table>
	<br/>
	<p align="right"><a href="javascript:void(0)" id="preview_button" style="opacity:0.4;" class="button1"><span><?=t($t_base.'fields/preview');?></span></a></p>
	<div id="show_message_preview"></div>
<?
}
function newDetailLists()
{
	$t_base="management/AdminMessages/";
	$kRss = new cRSS();
	$rssFeedsAry = array();
	$rssFeedsAry = $kRss->getAllRssFeedsHtml(false,true);	
?>
	<table cellspacing="0" id="send_message_list" cellpadding="0" border="0" class="format-7" width="100%">
		<tr>
			<td style="text-align:left;width:16%;"><strong><?=t($t_base.'fields/date')?></strong></td>
			<td style="text-align:left;width:77%;"><strong><?=t($t_base.'fields/title')?></strong></td>
			<td style="text-align:left;width:7%;"></td>
		</tr>
		<?
			if(!empty($rssFeedsAry))
			{
				$i=1;
				foreach($rssFeedsAry as $rssFeedsArys)
				{
					//print_r($allSendMessageArrs['szSubject']);
					//die;
					?>
						<tr id="message_data_<?=$i?>">
							<td><?=((!empty($rssFeedsArys["dtCreatedOn"]) && $rssFeedsArys["dtCreatedOn"]!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($rssFeedsArys["dtCreatedOn"])):"")?></td>
							<td><?=returnLimitData($rssFeedsArys['szTitle'],100)?></td>
							<td><a href="javascript:void(0)" onclick="edit_news_feed('<?=$rssFeedsArys['id']?>')"><img src="<?=__BASE_STORE_IMAGE_URL__?>/Icon - Edit line.png"></a> <a href="javascript:void(0)" onclick="delete_news_feed('<?=$rssFeedsArys['id']?>')"><img src="<?=__BASE_STORE_IMAGE_URL__?>/delete-icon-mgmt.png"></a></td>
						</tr>
					<?
					++$i;
				}
			}
		?>
	</table>
	<br/>
	<div id="show_message_preview"></div>
<?php
}

function updateNewFeedForm($idNewFeed,$newFeedArray=array())
{

	$t_base="management/AdminMessages/";
	$kRss = new cRSS();
	$kRss->loadNews($idNewFeed);
	if(!empty($newFeedArray))
	{
		$_POST['newFeedArr']=$newFeedArray;
	}
?>
<script type="text/javascript">
		setup();
	</script>
<div>
		<form id="saveNewFeed" name="saveNewFeed" method="post">
			<table cellpadding="3"  cellspacing="3" border="0"  width="100%" style="margin-bottom:20px;">
			<!-- <tr>
			<td><?=t($t_base.'fields/szTitle')?>:</td><td><input type="text" name="newFeedArr[szTitle]"  id="szTitle" value="<?=(($_POST['newFeedArr']['szTitle'])?$_POST['newFeedArr']['szTitle']:$kRss->szTitle)?>" style='width:640px;'/></td>
			</tr> -->
			<tr valign="top">
			<td colspan="2"><textarea id="szDescription" class="myTextEditor" name="newFeedArr[szDescription]" rows="15" cols="94" style='width:700px;'><?=(($_POST['newFeedArr']['szDescription'])?$_POST['newFeedArr']['szDescription']:$kRss->szDescriptionHTML)?></textarea></td>
			</tr>
			<tr>
			<td align="right" colspan="2"><a href="javascript:void(0);" onclick="save_news();" class="button1"><span><?=t($t_base.'fields/save')?></span></a> <a href="javascript:void(0);" onclick="show_preview_text();" class="button1"><span><?=t($t_base.'fields/preview')?></span></a></td>
			
			</tr>
			<input type="hidden" name = "idNewFeed" value="<?=$idNewFeed?>" />
			<input type="hidden" name = "flag" value="SAVE_NEWS_FEED" />
			</table>
		</form>
		<div id="previewText">
		</div>
</div>
<?php	
}
function systemMessageLists($page=1)
{
    $kAdmin = new cAdmin();
    $t_base="management/AdminMessages/";
    $allSendMessageArr=$kAdmin->emailLogDetails($page);
    //print_r($allSendMessageArr);
    $gtotal=$kAdmin->emailLogtotalCount();

    require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
    $kPagination  = new Pagination($gtotal,__CONTENT_PER_PAGE_MESSAGES_REVIEWS__,15);
?>
    <table cellspacing="0" id="send_message_list" cellpadding="0" border="0" class="format-2" width="100%">
        <tr>
            <td style="text-align:left;width:16%;"><strong><?=t($t_base.'fields/date_time')?></strong></td>
            <td style="text-align:left;width:27%;"><strong><?=t($t_base.'fields/email_address')?></strong></td>
            <!--<td style="text-align:left;width:5%;"><strong><?=t($t_base.'fields/status')?></strong></td>-->
            <td style="text-align:left;width:45%;"><strong><?=t($t_base.'fields/subject')?></strong></td>
            <td style="text-align:left;width:7%;"></td>
        </tr>
        <?php
            if(!empty($allSendMessageArr))
            {
                $i=1;
                foreach($allSendMessageArr as $allSendMessageArrs)
                {
                    if(empty($allSendMessageArrs['szEmailStatus']))
                    {
                        $allSendMessageArrs['szEmailStatus'] = 'Sent';
                    }
                    if(!empty($allSendMessageArrs["dtSents"]) && $allSendMessageArrs["dtSents"]!='0000-00-00 00:00:00')
                    {
                        $allSendMessageArrs['dtSents'] = getDateCustom($allSendMessageArrs['dtSents']);
                        /*
                        if(date('I')==1)
                        { 
                            $allSendMessageArrs["dtSents"] =  date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($allSendMessageArrs['dtSents'])));  
                        } 
                        else
                        {
                            $allSendMessageArrs["dtSents"] =  date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($allSendMessageArrs['dtSents'])));  
                        }
                         * 
                         */
                    }
                                    
                        ?>
                        <tr id="message_data_<?=$i?>" onclick="select_message_system_send_tr('message_data_<?=$i?>','<?=$allSendMessageArrs['id']?>')">
                            <td><?=((!empty($allSendMessageArrs["dtSents"]) && $allSendMessageArrs["dtSents"]!="0000-00-00 00:00:00")?DATE("d/m/Y H:i",strtotime($allSendMessageArrs["dtSents"])):"")?></td>
                            <td><?=returnLimitData($allSendMessageArrs['szToAddress'],25)?></td>
                            <!--<td><?php echo ucfirst($allSendMessageArrs['szEmailStatus']); ?></td>-->
                            <td><?=substrwords(utf8_decode($allSendMessageArrs['szEmailSubject']),58)?></td>
                            <td><a href="javascript:void(0)" onclick="resend_system_messages('<?=$allSendMessageArrs['id']?>')"><?=t($t_base.'fields/resend');?></a></td>
                        </tr>
                        <?php
                        ++$i;
                    }
                }
            ?>
	</table>
	<?
		if($gtotal>0)
		{
			$kPagination->paginate('showPageSystemMessage');
			if($kPagination->links_per_page>1)
			{
				echo $kPagination->renderFullNav();
			}
		}
	?>
	<br/>
	<p align="right"><a href="javascript:void(0)" id="preview_button" style="opacity:0.4;" class="button1"><span><?=t($t_base.'fields/preview');?></span></a></p>
	<div id="show_message_preview"></div>
<?
}

function showLocationDescription($t_base,$iLanguage)
{
	$kWHSSearch = new cWHSSearch;
	$countryDetails=$kWHSSearch->LocationDescription(false,$iLanguage);
	
	if($countryDetails!= array())
	{
		foreach($countryDetails as $country)
		{
			?>
			<tr id="booking_table_<?=++$count;?>" style="height: 30px;">
				<td><?=$country['szCountryName']?></td>
				<td><?=($country['LocationDescriptionExact']?($kWHSSearch->getAvailableSelectionName($country['LocationDescriptionExact'])):'')?></td>
				<td><?=($country['LocationDescriptionNotExact']?($kWHSSearch->getAvailableSelectionName($country['LocationDescriptionNotExact'])):'')?></td>
				<td class='HideMe' onclick = "showMeLocation('<?=$country['id']?>','booking_table_<?=$count;?>','<?php echo $iLanguage; ?>')" style="cursor: pointer;"><a><?=t($t_base.'fields/edit')?></a></td>
			</tr>
		<?php
		}
	}
}

function countriesView($countryDetails)
{ $t_base="management/country/";
?>
    <table cellspacing="0" cellpadding="0" border="0" class="format-4" width="100%" id="excangeTable" >
       <!-- <t<!-r>
            <th colspan="7" valign="top"><!--<a href="#" <? if(!empty($countryDetails)){ ?> onclick="download_country_table(1)" <? } ?>><?=t($t_base.'fields/download')?></a>-->	</th> 
            <!--<th colspan="2" valign="top" ><?=t($t_base.'fields/registration_act')?></th>
            <th colspan="4" valign="top" ><?=t($t_base.'fields/services')?></th>
            <!-- <th colspan="3" valign="top" ><?=t($t_base.'fields/Max_intl')?></th> -->
        <!--</tr>-->
        <tr>
            <th width="35%"><span id="country" onclick="sortCountryDetails('country','szCountryName','DESC');" class="cursor-pointer"><?=t($t_base.'fields/name')?></span></th>
            <!--<th width="26%"><span id="country"><?=t($t_base.'fields/country_name_other_language')?></span></th>-->
            <th  width="13%" style="text-align:center"><span id="code" onclick="sortCountryDetails('code','szCountryISO','ASC');" class="cursor-pointer"><?=t($t_base.'fields/code')?></span></th>
            <th  width="13%" style="text-align:center"> <?=t($t_base.'fields/regions')?></th>
            <th width="13%" style="text-align:center"><span id="std" onclick="sortCountryDetails('std','fStandardTruckRate','ASC');" class="cursor-pointer"><?=t($t_base.'fields/standard')?></span></th>
<!--            <th width="6%" style="text-align:center"><span id="from" onclick="sortCountryDetails('from','iActiveFromDropdown','ASC');" class="cursor-pointer"><?=t($t_base.'fields/from')?></span></th>
            <th width="6%" style="text-align:center"><span id="to" onclick="sortCountryDetails('to','iActiveToDropdown','ASC');" class="cursor-pointer"><?=t($t_base.'fields/to')?></span></th>-->
            <th width="13%" style="text-align:center"><span id="act" onclick="sortCountryDetails('act','iActive','ASC');" class="cursor-pointer"><?=t($t_base.'fields/act')?></span></th>
            <th width="13%" style="text-align:center"><span id="sc" onclick="sortCountryDetails('sc','iSmallCountry','ASC');" class="cursor-pointer"><?=t($t_base.'fields/sc')?></span></th>
            <!--<th width="6%" style="text-align:right;min-width: 32px;"><span id="cust" onclick="sortCountryDetails('cust','0','ASC');" class="cursor-pointer"><?=t($t_base.'fields/cust')?></span></th>
            <th width="6%" style="text-align:right;min-width: 32px;"><span id="cfs" onclick="sortCountryDetails('cfs','1','ASC');" class="cursor-pointer"><?=t($t_base.'fields/cfs')?></span></th>
            <th width="6%" style="text-align:right;min-width: 32px;"><span id="service_from" onclick="sortCountryDetails('service_from','2','ASC');" class="cursor-pointer"><?=t($t_base.'fields/from')?></span></th>
            <th width="6%" style="text-align:right;min-width: 32px;"><span id="service_to" onclick="sortCountryDetails('service_to','3','ASC');" class="cursor-pointer"><?=t($t_base.'fields/to')?></span></th>-->
        </tr>
<?php
	 if(!empty($countryDetails))
	 {
		foreach($countryDetails as $key=>$value)
		{
                    $szTrClass = "";
                    if($value['fVATRate']>0 && empty($value['szVATRegistration']))
                    {
                        $szTrClass = " class='red_text'";
                    }
	?>
                    <tr class="countryDetals" id="<?=$value['id'];?>" onclick="selectExchangeCountries('<?=$value['id']."','".addslashes($value['szCountryName']);?>')" style="cursor: pointer;">
                        <td <?php echo $szTrClass;?>><?=$value['szCountryName'];?></td> 		
                        <td align="center"><?=$value['szCountryISO'];?></td>
                        <td align="center"><?=$value['szRegionShortName'];?></td>
                        <td align="center"><?=$value['fStandardTruckRate'];?></td> 
                        <td align="center"><?=($value['iActive'])?'Yes':'No';?></td>
                        <td align="center"><?=($value['iSmallCountry'])?'Yes':'No';?></td>
                        <!--<td align="right"><?=$value['0'];?></td>
                        <td align="right"><?=$value['1'];?></td>
                        <td align="right"><?=$value['2'];?></td>
                        <td align="right"><?=$value['3'];?></td>-->
                    </tr>
<?php
		}
	}	
	?></table><?php	

}

function countriesText()
{
    $t_base ='management/country/';

    $kAdmin = new cAdmin();
    $regionListAry = array(); 
    $searchAry = array();
    $searchAry['iActive'] = 1 ; 
    $regionListAry = array();  
    $regionListAry = $kAdmin->getAllRegions($searchAry); 
    
    
    $kConfig = new cConfig();
    $curreniesArr=$kConfig->getBookingCurrency('',true);
    $languageArr=$kConfig->getLanguageDetails('','',true);
    
    /*
    * Fetching language for Language dropdown
    */
    $languageAry = array();
    $languageAry = $kConfig->getLanguageDetails(false,false,false,false,false,true);
?>
<p class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/country_name');?></span>
	<span class="field-container">	
		<input type="text" name="arrCountry[szCountryName]" class="addExchangeOnFocus" size="25" onfocus="addExchangeRate();"/> 
	</span>
</p>	
<?php
if(!empty($languageArr))
{
   foreach($languageArr as $languageArrs)
   {?>
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/country_name')." (".$languageArrs['szName'].")";?></span>
            <span class="field-container">	
                <input type="text" name="arrCountry[szCountryDanishName_<?php echo $languageArrs['id'];?>]" class="addExchangeOnFocus" size="25" onfocus="addExchangeRate();"/> 
            </span>
        </p>
<?php
   }
}?>
<p class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/country_code');?></span>
	<span class="field-container">
		<input type="text" style="width:94px;" name="arrCountry[szCountryISO]" class="addExchangeOnFocus" size="2" onfocus="addExchangeRate();" />
	  <em><?=t($t_base.'fields/two_capital_letters');?></em>
	</span>
</p>
<p class="profile-fields">
    <span class="field-name"><?=t($t_base.'messages/vat_rate_on_transportation_services');?></span>
    <span class="field-container">
            <input type="text" style="width:94px;" name="arrCountry[fVATRate]" class="addExchangeOnFocus" size="2" onfocus="addExchangeRate();" />
      <em>%</em>
    </span>
</p>
<p class="profile-fields">
    <span class="field-name"><?=t($t_base.'messages/transporteca_vat_number');?></span>
    <span class="field-container">
        <input type="text" style="width:94px;" name="arrCountry[szVATRegistration]" class="addExchangeOnFocus" size="2" onfocus="addExchangeRate();" /> 
    </span>
</p>

<p class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/standard');?></span>
	<span class="field-container">	
		<input type="text" style="width:94px;" name="arrCountry[fStandardTruckRate]" class="addExchangeOnFocus" size="2" onfocus="addExchangeRate();" />
	  <em><?=t($t_base.'fields/usd_per_km');?></em>
	</span>
</p>	
<p class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/active');?></span>
	<span class="field-container">	
	<select name="arrCountry[iActive]" class="addExchangeOnChange" onchange="addExchangeRate();">
		<option value="1"><?=t($t_base.'fields/yes');?></option>
		<option value="0"><?=t($t_base.'fields/no');?></option>
	</select>
	</span>
</p>	
<p class="profile-fields">
    <span class="field-name"><?=t($t_base.'messages/region');?></span>
    <span class="field-container">	
        <select name="arrCountry[idRegion]" id="idRegion" class="addExchangeOnChange">
            <option value=""><?=t($t_base.'fields/select');?></option>
            <?php
                if(!empty($regionListAry))
                {
                    foreach($regionListAry as $regionListArys)
                    {
                        ?>
                        <option value="<?php echo $regionListArys['id']; ?>"><?php echo $regionListArys['szRegionName']; ?></option>    
                        <?php
                    }
                }
            ?> 
        </select>
    </span>
</p>
<!--
    <p class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/available1');?></span>
	<span class="field-container">	
	<select name="arrCountry[iActiveFromDropdown]" class="addExchangeOnChange" onchange="addExchangeRate();">
		<option value="2"><?=t($t_base.'fields/auto');?></option>
		<option value="1"><?=t($t_base.'fields/yes');?></option>
		<option value="0"><?=t($t_base.'fields/no');?></option>
	</select>
	</span>
    </p>	
    <p class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/available2');?></span>
	<span class="field-container">	
	<select name="arrCountry[iActiveToDropdown]" class="addExchangeOnChange" onchange="addExchangeRate();">
		<option value="2"><?=t($t_base.'fields/auto');?></option>
		<option value="1"><?=t($t_base.'fields/yes');?></option>
		<option value="0"><?=t($t_base.'fields/no');?></option>
	</select>
	</span>
    </p>
--> 

<input type="hidden" name="arrCountry[iActiveFromDropdown]" id="iActiveFromDropdown" value="1">
<input type="hidden" name="arrCountry[iActiveToDropdown]" id="iActiveToDropdown" value="1"> 

<p class="profile-fields">
	<span class="field-name"><?=t($t_base.'messages/small_country');?> (<?=t($t_base.'messages/small_country_description');?>)</span>
	<span class="field-container">	
	<select name="arrCountry[iSmallCountry]" id="iSmallCountry">
		<option value="1"><?=t($t_base.'fields/yes');?></option>
		<option value="0"><?=t($t_base.'fields/no');?></option>
	</select>
	</span>
</p>
    <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/cordinates_of_country');?></span>
        <span class="field-container">	
            <input type="text" name="arrCountry[szLatitude]" class="addExchangeOnFocus country-field" size="2" onfocus="addExchangeRate();" />
            <input type="text" name="arrCountry[szLongitude]" class="addExchangeOnFocus country-field" size="2" onfocus="addExchangeRate();" />
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/max_fob_distance');?></span>
        <span class="field-container">	
            <input type="text" name="arrCountry[fMaxFobDistance]" id="fMaxFobDistance" class="addExchangeOnFocus country-field" size="2" onfocus="addExchangeRate();" />&nbsp; <span><i>Km</i></span>
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/default_currency_of_users_from_this_country');?></span>
        <span class="field-container">	
            <select name="arrCountry[iDefaultCurrency]" id="iDefaultCurrency" class="addExchangeOnChange"> 
                <?php
                    if(!empty($curreniesArr))
                    {
                        foreach($curreniesArr as $curreniesArrs)
                        {
                            ?>
                            <option value="<?php echo $curreniesArrs['id']; ?>"><?php echo $curreniesArrs['szCurrency']; ?></option>    
                            <?php
                        }
                    }
                ?> 
            </select>
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/default_language_of_users_from_this_country');?></span>
        <span class="field-container">	
            <select name="arrCountry[iDefaultLanguage]" id="iDefaultLanguage" class="addExchangeOnChange"> 
                <?php
                    if(!empty($languageAry))
                    {
                        foreach($languageAry as $languageArys)
                        {
                            ?>
                            <option value="<?php echo $languageArys['id']; ?>"><?php echo $languageArys['szName']; ?></option>    
                            <?php
                        }
                    }
                ?> 
            </select>
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/default_postcode_and_city_name_for_courier_price_call');?></span>
        <span class="field-container">	
            <input type="text" name="arrCountry[szDefaultCourierPostcode]" class="country-field" size="10" placeholder="Postcode"/>
            <input type="text" name="arrCountry[szDefaultCourierCity]" class="country-field" size="10" placeholder="City"/>
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/users_postcode_collection_delivery');?></span>
        <span class="field-container">	
            <select name="arrCountry[iUsePostcodes]" id="iUsePostcodes" class="addExchangeOnChange">                 
                <option value="1">Yes</option> 
                <option value="0">No</option>                             
            </select>
        </span>
    </p>
	<input type="hidden" name="mode" id="SAVE_COUNTRIES" value="SAVE_COUNTRIES" />
	<input type="hidden" name="id" value="0" />
<?php 
}

function countriesDetails($detail=array())
{	
    $t_base ='management/country/'; 
    $kAdmin = new cAdmin();
    $regionListAry = array(); 
    $searchAry = array();
    $searchAry['iActive'] = 1 ; 
    $regionListAry = array();  
    $regionListAry = $kAdmin->getAllRegions($searchAry); 
    
    $kConfig = new cConfig();
    $curreniesArr=$kConfig->getBookingCurrency('',true);
    $languageArr=$kConfig->getLanguageDetails('','',true);
    
    /*
    * Fetching language for Language dropdown
    */
    $languageAry = array();
    $languageAry = $kConfig->getLanguageDetails(false,false,false,false,false,true);
?>
	<p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/country_name');?></span>
            <span class="field-container">	
                <input type="text" name="arrCountry[szCountryName]" class="addExchangeOnFocus" size="25" value="<?=isset($detail['szCountryName'])?$detail['szCountryName']:''?>" /> 
            </span>
	</p>
	<?php
        if(!empty($languageArr))
        {
           foreach($languageArr as $languageArrs)
           {?>
                <p class="profile-fields">
                    <span class="field-name"><?=t($t_base.'messages/country_name')." (".$languageArrs['szName'].")";?></span>
                    <span class="field-container">	
                        <input type="text" name="arrCountry[szCountryDanishName_<?php echo $languageArrs['id'];?>]" class="addExchangeOnFocus" size="25" value="<?=isset($detail['szCountryDanishName_'.$languageArrs['id'].''])?$detail['szCountryDanishName_'.$languageArrs['id'].'']:''?>"/> 
                    </span>
                </p>
        <?php
           }
        }?>
	<p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/country_code');?></span>
            <span class="field-container">	
                <input type="text" name="arrCountry[szCountryISO]" class="addExchangeOnFocus" size="2" style="width:94px;" value="<?=isset($detail['szCountryISO'])?$detail['szCountryISO']:''?>" />
                <em><?=t($t_base.'fields/two_capital_letters');?></em>
            </span>
	</p>
	<p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/vat_rate_on_transportation_services');?></span>
            <span class="field-container">
                    <input type="text" style="width:94px;" name="arrCountry[fVATRate]" onblur="checkVatValue();" id="fVATRate" class="addExchangeOnFocus" size="2" onblur="checkVatValue();" value="<?php echo ($detail['fVATRate']>0)?$detail['fVATRate']:''?>"/>
              <em>%</em>
            </span>
        </p>
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/transporteca_vat_number');?></span>
            <span class="field-container">
                <input type="text" style="width:94px;" name="arrCountry[szVATRegistration]" id="szVATRegistration" class="addExchangeOnFocus" value="<?php echo $detail['szVATRegistration']; ?>"/> 
            </span>
        </p>
	<p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/standard');?></span>
            <span class="field-container">	
                <input type="text" name="arrCountry[fStandardTruckRate]" class="addExchangeOnFocus" size="2" style="width:94px;" value="<?=isset($detail['fStandardTruckRate'])?$detail['fStandardTruckRate']:''?>"/>
                <em><?=t($t_base.'fields/usd_per_km');?></em>
            </span>
	</p>
	<p class="profile-fields">
		<span class="field-name"><?=t($t_base.'messages/active');?></span>
		<span class="field-container">	
			<select name="arrCountry[iActive]" class="addExchangeOnChange" >
				<option value="1" <?=(isset($detail['iActive'])?$detail['iActive']:'')?'selected="selected"':'' ?>><?=t($t_base.'fields/yes');?></option>
				<option value="0" <?=(isset($detail['iActive'])?$detail['iActive']:'')?'':'selected="selected"' ?>><?=t($t_base.'fields/no');?></option>
			</select>
		</span>
	</p>
	<p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/region');?></span>
            <span class="field-container">	
                <select name="arrCountry[idRegion]" id="idRegion" class="addExchangeOnChange">
                    <option value=""><?=t($t_base.'fields/select');?></option>
                    <?php
                        if(!empty($regionListAry))
                        {
                            foreach($regionListAry as $regionListArys)
                            {
                                ?>
                                <option value="<?php echo $regionListArys['id']; ?>" <?php echo (($regionListArys['id']==$detail['idRegion'])?'selected':''); ?>><?php echo $regionListArys['szRegionName']; ?></option>    
                                <?php
                            }
                        }
                    ?> 
                </select>
            </span>
	</p>
<!--        <p class="profile-fields">
		<span class="field-name"><?=t($t_base.'messages/available1');?></span>
		<span class="field-container">	
			<select name="arrCountry[iActiveFromDropdown]" class="addExchangeOnChange" >
				<option value="2" <?=(isset($detail['iActiveFromDropdown']) && $detail['iActiveFromDropdown']==2)?'selected="selected"':'' ?>><?=t($t_base.'fields/auto');?></option>
				<option value="1" <?=(isset($detail['iActiveFromDropdown']) && $detail['iActiveFromDropdown']==1)?'selected="selected"':'' ?>><?=t($t_base.'fields/yes');?></option>
				<option value="0" <?=(isset($detail['iActiveFromDropdown']) && $detail['iActiveFromDropdown']==0)?'selected="selected"':'' ?>><?=t($t_base.'fields/no');?></option>
			</select>
		</span>
	</p>
	<p class="profile-fields">
		<span class="field-name"><?=t($t_base.'messages/available2');?></span>
		<span class="field-container">	
			<select name="arrCountry[iActiveToDropdown]" class="addExchangeOnChange" >
				<option value="2" <?=(isset($detail['iActiveToDropdown']) && $detail['iActiveToDropdown']==2)?'selected="selected"':'' ?>><?=t($t_base.'fields/auto');?></option>
				<option value="1" <?=(isset($detail['iActiveToDropdown']) && $detail['iActiveToDropdown']==1)?'selected="selected"':'' ?>><?=t($t_base.'fields/yes');?></option>
				<option value="0" <?=(isset($detail['iActiveToDropdown']) && $detail['iActiveToDropdown']==0)?'selected="selected"':'' ?>><?=t($t_base.'fields/no');?></option>
			</select>
		</span>
	</p>	-->
        <input type="hidden" name="arrCountry[iActiveFromDropdown]" id="iActiveFromDropdown" value="1">
        <input type="hidden" name="arrCountry[iActiveToDropdown]" id="iActiveToDropdown" value="1">
	<p class="profile-fields">
		<span class="field-name"><?=t($t_base.'messages/small_country');?> (<?=t($t_base.'messages/small_country_description');?>)</span>
		<span class="field-container">	
		<select name="arrCountry[iSmallCountry]" id="iSmallCountry">
			<option value="1" <?=((isset($detail['iSmallCountry']) && $detail['iSmallCountry'])==1)?'selected="selected"':'' ?>><?=t($t_base.'fields/yes');?></option>
			<option value="0" <?=((isset($detail['iSmallCountry']) && $detail['iSmallCountry'])==0)?'selected="selected"':'' ?>><?=t($t_base.'fields/no');?></option>
		</select>
		</span>
	</p>
	<p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/cordinates_of_country');?> </span>
            <span class="field-container">	
                <input type="text" name="arrCountry[szLatitude]" class="addExchangeOnFocus country-field" size="2" value="<?=isset($detail['szLatitude'])?$detail['szLatitude']:''?>" />
                <input type="text" name="arrCountry[szLongitude]" class="addExchangeOnFocus country-field" size="2" value="<?=isset($detail['szLongitude'])?$detail['szLongitude']:''?>" />
            </span>
	</p>
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/max_fob_distance');?></span>
            <span class="field-container">	
                <input type="text" name="arrCountry[fMaxFobDistance]" id="fMaxFobDistance" class="addExchangeOnFocus country-field" size="2" value="<?php echo ($detail['fMaxFobDistance']>0)?$detail['fMaxFobDistance']:''?>" /> <span><i>Km</i></span>
            </span>
        </p>
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/default_currency_of_users_from_this_country');?></span>
            <span class="field-container">	
                <select name="arrCountry[iDefaultCurrency]" id="iDefaultCurrency" class="addExchangeOnChange"> 
                    <?php
                        if(!empty($curreniesArr))
                        {
                            foreach($curreniesArr as $curreniesArrs)
                            {
                                ?>
                                <option value="<?php echo $curreniesArrs['id']; ?>" <?php echo (($curreniesArrs['id']==$detail['iDefaultCurrency'])?'selected':''); ?>><?php echo $curreniesArrs['szCurrency']; ?></option>    
                                <?php
                            }
                        }
                    ?> 
                </select>
            </span>
        </p>
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/default_language_of_users_from_this_country');?></span>
            <span class="field-container">	
                <select name="arrCountry[iDefaultLanguage]" id="iDefaultLanguage" class="addExchangeOnChange"> 
                    <?php
                        if(!empty($languageAry))
                        {
                            foreach($languageAry as $languageArys)
                            {
                                ?>
                                <option value="<?php echo $languageArys['id']; ?>" <?php echo (($languageArys['id']==$detail['iDefaultLanguage'])?'selected':''); ?>><?php echo $languageArys['szName']; ?></option>    
                                <?php
                            }
                        }
                    ?> 
                </select>
            </span>
        </p> 
        <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/default_postcode_and_city_name_for_courier_price_call');?></span>
        <span class="field-container">	
            <input type="text" name="arrCountry[szDefaultCourierPostcode]" class="country-field" size="10" placeholder="Postcode" value="<?php echo $detail['szDefaultCourierPostcode'];?>"/>
            <input type="text" name="arrCountry[szDefaultCourierCity]" class="country-field" size="10" placeholder="City" value="<?php echo $detail['szDefaultCourierCity'];?>"/>
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"><?=t($t_base.'messages/users_postcode_collection_delivery');?></span>
        <span class="field-container">	
            <select name="arrCountry[iUsePostcodes]" id="iUsePostcodes" class="addExchangeOnChange">                 
                <option <?php if($detail['iUsePostcodes']==1){?> selected <?php } ?> value="1">Yes</option> 
                <option <?php if($detail['iUsePostcodes']==0){?> selected <?php } ?> value="0">No</option>                             
            </select>
        </span>
    </p>
        <input type="hidden" name="mode" id="SAVE_COUNTRIES" value="SAVE_COUNTRIES" />
        <input type="hidden" name="id" value="<?=!empty($detail['id'])?$detail['id']:'';?>" />
<?php
    }

function showAvailableSelection($t_base,$iLanguage)
{
    $kWHSSearch = new cWHSSearch;
    $availableSelections = $kWHSSearch->getAvailableSelection(false,$iLanguage);
    ?>
    <div id="show-Err"></div>
    <div style="float: right;padding-right:50px;"><?=t($t_base.'fields/avaliable_selection')?>
        <table class = "format-4" cellpadding="0" cellspacing="0" style="margin-bottom: 20px;" id="select_selection">  
            <?php 
                if($availableSelections!= array())
                {
                    foreach ($availableSelections as $selectionArr)
                    {
                        echo "<tr id='select_option".++$id."'><td style=\"min-width: 161px;\" onclick=\"selectAvailableSelection('".$selectionArr['id']."','select_option".$id."');\">".$selectionArr['szAvailableSelection']."</td></tr>";
                    }
                }
            ?> 
        </table>
        <input type="text" name="szSelectionArr" id="szSelectionAvailable" value="" onkeyup="addMe();">
        <input type="hidden" name="iSelection" id="iSelection" value="0">
        <input type="hidden" name="iLanguage" id="iLanguage" value="<?php echo $iLanguage; ?>">
        <br />
        <a href="javascript:void(0)" class="button1" id="saveEdit"  onclick="saveAvailableSelection()" style="opacity:0.4;"><span><?=t($t_base.'fields/edit');?></span></a>
    </div>
    <?php
}

function display_partner_logo_form($kExplain,$i,$partnerLogoAry=false)
{
	?>
	<table style="width:100%;">
		<tr>
			<td style="width:48%;" valign="top">
				<span class="field-name"><?php echo $szTitleText; ?></span>
			 	<span class="field-container">					 	
			 		<span class="field-title">Logo <?php echo $i; ?> image URL<br></span>
			 		<input type="text" style="width:90%;" id="szLogoImageURL_<?php echo $i; ?>" name="landingPageArr[szLogoImageURL][<?php echo $i; ?>]" value="<?php if(isset($partnerLogoAry['szLogoImageUrl'])){ echo $partnerLogoAry['szLogoImageUrl']; }?>">
			 		<input type="hidden" name="landingPageArr[idPartnerLogo][<?php echo $i; ?>]" id="idPartnerLogo_<?php echo $i; ?>" value="<?php echo $partnerLogoAry['id']; ?>">
			 	</span>
			 </td>
			 <td style="width:48%;" valign="top">
				<span class="field-name"><?php echo $szTitleText; ?></span>
			 	<span class="field-container">					 	
			 		<span class="field-title">Logo <?php echo $i; ?> image Title<br></span>
			 		<input type="text" style="width:90%;" id="szLogoImageTitle_<?php echo $i; ?>" name="landingPageArr[szLogoImageTitle][<?php echo $i; ?>]" value="<?php if(isset($partnerLogoAry['szImageTitle'])){ echo $partnerLogoAry['szImageTitle']; }?>">
			 	</span>
			 </td>
		</tr>
		<tr>
			<td valign="top">
				<span class="field-name"></span>
			 	<span class="field-container">					 	
			 		<span class="field-title">Logo <?php echo $i; ?> image description (150-160 characters)<span style="float:right;margin-right:30px;" id="szLogoImageDesc_<?php echo $i; ?>_span">(0)</span><br></span>
			 		<textarea style="width:90%;" id="szLogoImageDesc_<?php echo $i; ?>" onkeypress="limit_characters(this.id,160);" maxlength="160" name="landingPageArr[szLogoImageDesc][<?php echo $i; ?>]"><?php if(isset($partnerLogoAry['szImageDesc'])){ echo $partnerLogoAry['szImageDesc']; } ?></textarea>
			 	</span>
			</td>
			<td valign="top">
				<span class="field-name"></span>
			 	<span class="field-container">					 	
			 		<span class="field-title">Logo <?php echo $i; ?> tool tip text (150-160 characters)<span style="float:right;margin-right:30px;" id="szToolTipText_<?php echo $i; ?>_span">(0)</span><br></span>
			 		<textarea style="width:90%;" id="szToolTipText_<?php echo $i; ?>" onkeypress="limit_characters(this.id,160);" maxlength="160" name="landingPageArr[szToolTipText][<?php echo $i; ?>]"><?php if(isset($partnerLogoAry['szToolTipText'])){ echo $partnerLogoAry['szToolTipText']; } ?></textarea>
			 	</span>
			</td>
		</tr>
		
		<tr>
			<td valign="top" colspan="2">
				<span class="field-name"><?php echo $szTitleText; ?></span>
			 	<span class="field-container">					 	
			 		<span class="field-title">White Logo <?php echo $i; ?> image URL<br></span>
			 		<input type="text" style="width:90%;" id="szWhiteLogoImageURL_<?php echo $i; ?>" name="landingPageArr[szWhiteLogoImageURL][<?php echo $i; ?>]" value="<?php if(isset($partnerLogoAry['szWhiteLogoImageURL'])){ echo $partnerLogoAry['szWhiteLogoImageURL']; }?>">
			 	</span>
			 </td>
		</tr>
	</table> 
	<?php
} 

function all_forwarder_booking_html($searchResult,$t_base,$flag=false,$iNotConfirmed=false,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)  
{	
    $table_width = (int)(100/10); 
    $kBooking = new cBooking;
    $iBookingStatusPage = '';
    
    $iGrandTotal=count($searchResult);
    require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
    $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
    /*
    if($searchResult==__BOOKING_STATUS_CONFIRMED__)
    { 
        $iBookingStatusPage = __BOOKING_STATUS_CONFIRMED__ ; 
        $booking_searched_data = array();
        $searchResult = array();
        
        $booking_searched_data = $kBooking->search_all_forwarder_bookings(false,false,false,false,$iPage); 
        $searchResult = sortArray($booking_searched_data,'dtBookingConfirmed',true); 
        
        $grandTotalAry = array();
        $grandTotalAry = $kBooking->search_all_forwarder_bookings(false,false,true);  
        $iGrandTotal = $grandTotalAry[0]['iNumBookings'] ; 
        
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,__MAX_CONFIRMED_BOOKING_PER_PAGE__,15);
    }*/
    $bookingRefAry = array();
    if($iPage==1)
    {
        $start=0;
        $end=$limit;
        if($iGrandTotal<$end)
        {
            $end=$iGrandTotal;
        }
    }
    else
    {
        $start=$limit*($iPage-1);
        $end=$iPage*$limit;
        if($end>$iGrandTotal)
        {
            $end=$iGrandTotal;
        }
        $startValue=$start+1;
        if($startValue>$iGrandTotal)
        {
            $iPage=$iPage-1;
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            
        }
    }
    
    ?>
    <table cellpadding="0" id="booking_table" cellspacing="0" class="format-4" width="100%">
        <tr>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/date')?></th>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/booking')?></th>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/mode');?></th>
            <th width="<?=$table_width+4?>%" valign="top"><?=t($t_base.'fields/from');?></th>
            <th width="<?=$table_width+4?>%" valign="top"><?=t($t_base.'fields/to');?></th>
            <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/wm');?> </th>
            <th width="<?=$table_width+3?>%" valign="top" style="text-align: right;"><?=t($t_base.'fields/value');?></th>
            <th width="<?=$table_width+11 ?>%" valign="top"><?=t($t_base.'fields/forwarder');?></th>
        </tr>
        <?php
            if(!empty($searchResult))
            {
                $ctr = 1;
                $counter = 0;
                for($i=$start;$i<$end;++$i)
                { 
                    $szServiceType='';
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
                    {
                        $szServiceType = "DTD" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTW__) //DTW
                    {
                        $szServiceType = "DTW" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTD__) //WTD
                    {
                        $szServiceType = "WTD" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTW__) //WTW
                    {
                        $szServiceType = "WTW" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTP__) //DTD
                    {
                        $szServiceType = "PTP" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTP__) //DTW
                    {
                        $szServiceType = "DTP" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTD__) //WTD
                    {
                        $szServiceType = "PTD" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_WTP__) //WTW
                    {
                        $szServiceType = "WTP" ;
                    }
                    if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_PTW__) //WTW
                    {
                        $szServiceType = "PTW" ;
                    } 
                    if($searchResult[$i]['iQuotesStatus']==__BOOKING_QUOTES_STATUS_WON__ || $searchResult[$i]['iCourierBooking']==1)
                    {
                        $szWarehouseFromCity=createSubString($searchResult[$i]['szShipperCity'],0,10);
                        $szWarehouseToCity=createSubString($searchResult[$i]['szConsigneeCity'],0,10); 
                        $idWarehouseFromCountry = $searchResult[$i]['idOriginCountry'] ;
                        $idWarehouseToCountry = $searchResult[$i]['idDestinationCountry'] ;
                    }
                    else
                    {  
                        if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTD__) //DTD
                        { 
                            $szWarehouseFromCity=createSubString($searchResult[$i]['szShipperCity'],0,10); 
                        }
                        else
                        {
                            $szWarehouseFromCity=createSubString($searchResult[$i]['szWarehouseFromCity'],0,10); 
                        }
                        $szWarehouseToCity=createSubString($searchResult[$i]['szConsigneeCity'],0,10); 
                        $idWarehouseFromCountry = $searchResult[$i]['idOriginCountry'] ;
                        $idWarehouseToCountry = $searchResult[$i]['idDestinationCountry'] ;
                    }

                    $style='';
                    if($iNotConfirmed)
                    {
                        if($searchResult[$i]['idServiceType']==__SERVICE_TYPE_DTD__)
                        {
                            $szBankTransferDueDateTime = strtotime($searchResult[$i]['dtCutOff']) - ($searchResult[$i]['iBookingCutOffHours']*60*60);
                        }
                        else
                        {
                            $szBankTransferDueDateTime = strtotime($searchResult[$i]['dtWhsCutOff']) - ($searchResult[$i]['iBookingCutOffHours']*60*60);
                        }						

                        $dtTodayDateTime = strtotime(date('Y-m-d')); 
                        if($dtTodayDateTime>=$szBankTransferDueDateTime)
                        {
                            $style="color:red";
                        }
                    } 
                    if(!empty($searchResult[$i]['szTransportMode']))
                    {
                        $szServiceType = $searchResult[$i]['szTransportMode']." ".$szServiceType ;
                    }
                    else if($searchResult[$i]['iCourierBooking']==1)
                    {
                        $szServiceType = "Courier" ;
                    }
                    else
                    {
                        $szServiceType = "Sea ".$szServiceType ;
                    } 
                    $szStyle = '';
                    $dup_text = '';
                    if(!empty($bookingRefAry) && in_array($searchResult[$i]['szBookingRef'],$bookingRefAry))
                    {
//                        $szStyle = 'style="background:red;"';
//                        $dup_text = 'D';
                        //We are not showing duplicate bookings 
                        continue;
                    } 
                    else
                    {
                        $bookingRefAry[] = $searchResult[$i]['szBookingRef'];
                        $counter++;
                    }
                    $styleDate='';
                    $onclickEvent='';
                    if($iNotConfirmed)
                    {
                        if($searchResult[$i]['iTransferConfirmed']=='1' && $searchResult[$i]['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
                        {

                             if(strtotime(date('Y-m-d'))>strtotime($searchResult[$i]['dtSnooze'])){

                                $styleDate="class='snoozeDate'";
                                $onclickEvent="onclick=openSnoozePopup('OUTSTANDINGPAYMENT','".$searchResult[$i]['id']."');";

                            }
                        }
                    }
                    ?>						
                    <tr id="booking_data_<?=$ctr?>" onclick="select_forwarder_confirmation_tr('booking_data_<?=$ctr?>','<?=$searchResult[$i]['id']?>')" <?php echo $szStyle;?>>
                            <td id="OUTSTANDINGPAYMENT_<?=$searchResult[$i]['id']?>"<?=$styleDate;?> <?=$onclickEvent;?>><?=(!empty($searchResult[$i]['dtBookingConfirmed']) && $searchResult[$i]['dtBookingConfirmed']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResult[$i]['dtBookingConfirmed'])):''?></td>
                            <td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $searchResult[$i]['szBookingRandomNum']."/";?>"><?=$searchResult[$i]['szBookingRef']." ".$dup_text ;?></a></td>
                            <td><?=$szServiceType;?></td>
                            <td><?=$szWarehouseFromCity." (".isoidCountryCode($idWarehouseFromCountry).")"?></td>
                            <td><?=$szWarehouseToCity." (".isoidCountryCode($idWarehouseToCountry).")"?></td>
                            <td><?=(get_formated_cargo_measure($searchResult[$i]['fCargoWeight']/1000)) ." / ".format_volume($searchResult[$i]['fCargoVolume'])?></td>
                            <td style="text-align: right; <?php echo $style; ?>"><?=$searchResult[$i]['szCustomerCurrency'].' '.number_format($searchResult[$i]['fTotalPriceCustomerCurrency'],2)?></td>
                            <td><?=returnLimitData($searchResult[$i]['szDisplayName'],16)?></td>
                    </tr>
                    <?php
                    $ctr++;
                }
            }
            else
            {
                ?>
                <tr>
                        <td colspan="11" align="center"><h4><b><?=t($t_base.'messages/no_boooking_found');?></b></h4></td>
                </tr>
                <?php
            }
            ?>
    </table>
    <input type="hidden" id="page" name="page" value="<?php echo $iPage;?>">
    <div id="invoice_comment_div" style="display:none">
    </div>
    <div class="page_limit clearfix">
        <div class="pagination">
    <?php
        
        $kPagination->szMode = "DISPLAY_BOOKING_LIST_PAGINATION";
        //$kPagination->idBookingFile = $idBookingFile;
        $kPagination->paginate('display_booking_list_pagination'); 
        if($kPagination->links_per_page>1)
        {
            echo $kPagination->renderFullNav();
        }        
    ?>
       </div>
       <div class="limit">Records per page
           <select id="limit" name="limit" onchange="display_booking_list_pagination('1')">
               <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
           </select>
       </div>    
    </div>    
    <br>
    <div>
        <span align="left"><a href="javascript:void(0)" class="button2" style="opacity:0.4" id="booking_cancel" align="left"><span><?=t($t_base.'fields/cancel_booking');?></span></a></span>
        <span style="float: right;">
        <?php if($iNotConfirmed){?>
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_invoice"><span><?=t($t_base.'fields/invoice');?></span></a>
        <?php } else {?>
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_confirmation"><span><?=t($t_base.'fields/confirmation');?></span></a>&nbsp;
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="download_booking_lcl_booking"><span><?=t($t_base.'fields/view_booking');?></span></a>
        <?php }?>

        </span>
    </div>
    
	<?php
}

function display_insurance_buyer_listings($iType,$insuranceRateAry,$idInsurance=false,$all_buy_rate_deleted=false)
{ 
    if($iType==1)  
    { 
        $table_width = (int)(100/7);
    }
    else
    {
        $table_width = (int)(100/3);
    }
	
    $t_base="management/insurance/";
    $kInsurance = new cInsurance();  
    if($iType==__INSURANCE_SELL_RATE_TYPE__)
    {
        $szHeading = t($t_base.'fields/sell_rates') ;
    }
    else
    {
        $szHeading = t($t_base.'fields/buy_rates') ;
    }
    
    $kConfig = new cConfig();
    $countryRegionAry = array();
    $countryRegionAry = $kConfig->getAllCountriesForPreferencesKeyValuePair(); 
    
    /**
     * This adjustments are made because we don't want rebuild entire process of insurance. Following values are just to adjust column width
     */
    $diff=6;
    $rateDiff = 3;
    $minDiff = 0;
    ?> 
    <script src="<?php echo __BASE_STORE_SECURE_JS_URL__;?>/nanoslider.js?v=2" type="text/javascript"></script>
    <script type="text/javascript">
        $().ready(function(){	
            $(".nano").nanoScroller({scroll: 'top'});
        });
    </script> 
	<div id="insurance_buyer_list_container_<?php echo $iType; ?>"> 
            <table cellpadding="0" cellspacing="0" class="table-scroll-head" width="100%">
                <tr>
                <?php if($iType==1){    ?>
                    <th colspan="4"><?php echo "Product"; ?></th>
                    <th colspan="3"><?php echo $szHeading; ?></th>
                <?php } else {?>
                    <th colspan="3"><?php echo $szHeading; ?></th>
                <?php }?>
                </tr>
                <tr>
                <?php if($iType==1){ $rateDiff = 2;    ?>
                    <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/from')?></th>
                    <th width="<?=$table_width?>%" valign="top"><?=t($t_base.'fields/to')?></th>
                    <th width="<?=$table_width-3?>%" valign="top"><?=t($t_base.'fields/mode')?></th>
                    <th width="<?=$table_width-2?>%" valign="top"><?=t($t_base.'fields/moving')?></th>
                <?php } ?>
                    <th width="<?=$table_width+$diff;?>%" valign="top"><?=t($t_base.'fields/value_up_to')?></th>
                    <th width="<?=$table_width-$rateDiff; ?>%" valign="top"><?=t($t_base.'fields/rate')."(%)"; ?></th>
                    <th width="<?=$table_width- $minDiff ;?>%" valign="top"><?=t($t_base.'fields/minimum');?></th> 
                </tr>
            </table> 
            <div id="pending_task_list_container_div" class="pending-tray-scrollable-div"> 
                <div class="scrolling-div" style="min-height:300px;">
                    <table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="insurance_rate_table_<?php echo $iType; ?>" >
                    <?php 	
                    if(!empty($insuranceRateAry))
                    {
                        $ctr = 1;
                        $irowCount = count($insuranceRateAry);
                        $fLargetBuyPrice = 0;
                        if($iType==__INSURANCE_SELL_RATE_TYPE__)
                        {
                            $fLargetBuyPrice = $kInsurance->getLargestInsuranceValue() ; 
                        } 
                        $selected_tr_id = '';
                        foreach($insuranceRateAry as $insuranceRateArys)
                        {
                            $class_name="";
                            if($iType==__INSURANCE_SELL_RATE_TYPE__)
                            {  
                                if((int)$fLargetBuyPrice<(int)$insuranceRateArys['fInsurancePriceUSD'])
                                {
                                    $style = 'style="cursor:pointer;color:#D9000B;"';
                                    $class_name="class='red_text'";
                                }
                                else
                                {
                                    $style = 'style="cursor:pointer;"';
                                    $class_name="";
                                }
                            }
                            else
                            {
                                $style = 'style="cursor:pointer;"';
                            }
                            if($iType==1 && $idInsurance>0) 
                            {
                                if($idInsurance==$insuranceRateArys['id'])
                                {
                                    $selected_tr_id = 'insurance_rate_'.$iType."_".$insuranceRateArys['id'] ;
                                }
                            }
                            
                            if($iType==1) 
                            {
                                $szOriginCountry = $countryRegionAry[$insuranceRateArys['idOriginCountry']]['szCountryName'] ;
                                $szDestinationCountry = $countryRegionAry[$insuranceRateArys['idDestinationCountry']]['szCountryName'] ;
                                
                                $szOriginCountryFullName = $szOriginCountry ;
                                $szDestinationCountryFullName = $szDestinationCountry ; 
                                
                                if(strlen($szDestinationCountry)>8)
                                {
                                    $szDestinationCountry = substr($szDestinationCountry,0,6)."..";
                                } 
                                
                                if(strlen($szOriginCountry)>8)
                                {
                                    $szOriginCountry = substr($szOriginCountry,0,6)."..";
                                } 
                                if(strlen($insuranceRateArys['szTransportMode'])>6)
                                {
                                    $szTransportMode = substr($insuranceRateArys['szTransportMode'],0,4)."";
                                }
                                else
                                {
                                    $szTransportMode = $insuranceRateArys['szTransportMode'] ;
                                }
                            }
                            ?>						
                            <tr <?php echo $style; ?> <?php echo $class_name; ?> id="insurance_rate_<?php echo $iType."_".$insuranceRateArys['id']; ?>" onclick="select_insurance_rate_tr('insurance_rate_<?php echo $iType."_".$insuranceRateArys['id']; ?>','<?=$insuranceRateArys['id']?>','<?php echo $iType; ?>','<?php echo $insuranceRateArys['idInsurance']; ?>');" class="insurance_rate_tr_type_<?php echo $iType; ?>">
                              <?php  if($iType==1) { $rateDiff = 2; ?>
                                <td title="<?php echo $insuranceRateArys['szOriginCountry']; ?>" width="<?=$table_width?>%" valign="top" title="<?php echo $szOriginCountryFullName; ?>"><?php echo $szOriginCountry; ?></td>
                                <td title="<?php echo $insuranceRateArys['szDestinationCountry']; ?>" width="<?=$table_width?>%" valign="top" title="<?php echo $szDestinationCountryFullName; ?>"><?php echo $szDestinationCountry; ?></td>
                                <td width="<?=$table_width-3?>%" valign="top" title="<?php echo $insuranceRateArys['szTransportMode']; ?>"><?php echo $szTransportMode; ?></td>
                                <td width="<?=$table_width-2?>%" valign="top"><?php echo (($insuranceRateArys['iPrivate']==1)?'Yes':'No'); ?></td>
                              <?php }?>
                                <td width="<?=$table_width + $diff;?>%" valign="top" title="<?php echo "USD ".round($insuranceRateArys['fInsurancePriceUSD']); ?>"><?php echo $insuranceRateArys['szInsuranceCurrency']." ".number_format((float)$insuranceRateArys['fInsuranceUptoPrice']); ?></td>
                                <td width="<?=$table_width - $rateDiff;?>%" valign="top"><?php echo number_format((float)$insuranceRateArys['fInsuranceRate'],3); ?></td>
                                <td width="<?=$table_width - $minDiff;?>%" valign="top"><?php echo $insuranceRateArys['szInsuranceMinCurrency']." ".number_format((float)$insuranceRateArys['fInsuranceMinPrice']); ?></td>
                            </tr>
                            <?php
                            $ctr++;
                        } 		
                        if($irowCount<18)
                        {
                            $loop_counter = 18-$irowCount ; 
                            for($i=0;$i<=$loop_counter;$i++)
                            {
                                ?>
                                <tr>
                                    <?php  if($iType==1){ ?>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    <?php } ?>  
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <?php
                            } 
                        } 
                    }
                    else
                    {
                        ?>
                        <tr>
                        <?php  if($iType==1)   { ?>
                            <td colspan="7" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
                        <?php } else {?>
                            <td colspan="3" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
                        <?php } ?>  
                        </tr> 
                        <?php  for($i=0;$i<18;$i++) { ?>
                        <tr> 
                            <?php  if($iType==1)   { ?>
                                <td colspan="7">&nbsp;</td>
                            <?php } else {?>
                                <td colspan="3">&nbsp;</td>
                            <?php } ?> 
                        </tr>
                        <?php } 
                        }
                    ?>
                </table>  
            </div>
	</div>
	<div id="insurance_buyer_form_container_<?php echo $iType; ?>">
            <?php
                echo display_insurance_buy_rate_form($kInsurance,$iType,array(),$idInsurance);
            ?>	
	</div> 
	<br>
	<div>
            <span align="left">
                <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="insurance_rate_delete_button_<?php echo $iType;?>" align="left"><span style="min-width:50px;"><?=t($t_base.'fields/delete');?></span></a>
                <?php if($iType==1){ ?>
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="insurance_rate_reuse_button_<?php echo $iType;?>"><span style="min-width:50px;"><?=t($t_base.'fields/reuse');?></span></a>
                <?php  }?>
            </span>
            <span style="float: right;"> 
                <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="insurance_rate_add_edit_button_<?php echo $iType;?>"><span style="min-width:50px;"><?=t($t_base.'fields/add');?></span></a>
            </span>
	</div>
</div> 
    <?php
    if(($iType==1 && $idInsurance>0) || $all_buy_rate_deleted)
    {
        $iAllDeletedFlag = 0; 
        if($all_buy_rate_deleted)
        {
            $iAllDeletedFlag = 1;
        }
        ?>
        <script type="text/javascript">
            $(".insurance_rate_tr_type_1").attr('style','background:#ffffff;cursor:pointer;');
            $("#"+'<?php echo $selected_tr_id; ?>').attr('style','background:#DBDBDB;cursor:pointer;');
            display_sell_rate_listing('<?php echo $idInsurance; ?>',2,'<?php echo $iAllDeletedFlag; ?>');
        </script>
        <?php
    } 
}

function display_insurance_buy_rate_form($kInsurance,$iType=false,$insuranceAry=array(),$idInsuranceParent=false)
{
    $t_base="management/insurance/";
    if(!empty($kInsurance->arErrorMessages))
    {
        $formId = 'insurance_form_type_'.$iType;
        $szValidationErrorKey = '';
        foreach($kInsurance->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            if($errorKey=='szSpecialError')
            {
                $szSpecialErrorMessage = $errorValue ;
            }
            else
            {
                ?>
                <script type="text/javascript">
                        $("#"+'<?php echo $errorKey."_".$iType; ?>').addClass('red_border');
                </script>
            <?php }
        }
    }
    $kConfig=new cConfig();
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true); 
    $onfocus_func = "addInsuranceRate(".$iType.");";

    if(isset($_POST['addInsuranceAry'][$iType]['idInsuranceCurrency']))
    {
        $idInsuranceCurrency = $_POST['addInsuranceAry'][$iType]['idInsuranceCurrency'] ;
    }
    else
    {
        $idInsuranceCurrency = $kInsurance->idInsuranceCurrency;
    }
    if(isset($_POST['addInsuranceAry'][$iType]['fInsuranceUptoPrice']))
    {
        $fInsuranceUptoPrice = $_POST['addInsuranceAry'][$iType]['fInsuranceUptoPrice'] ;
    }
    else
    {
        if(!empty($kInsurance->fInsuranceUptoPrice))
        { 
            $fInsuranceUptoPrice = round((float)$kInsurance->fInsuranceUptoPrice);
        }
    }
    if(isset($_POST['addInsuranceAry'][$iType]['fInsuranceRate']))
    {
        $fInsuranceRate = $_POST['addInsuranceAry'][$iType]['fInsuranceRate'] ;
    }
    else
    {
        $fInsuranceRate = $kInsurance->fInsuranceRate;
    } 
    if(isset($_POST['addInsuranceAry'][$iType]['iInsuranceMinCurrency']))
    {
        $iInsuranceMinCurrency = $_POST['addInsuranceAry'][$iType]['iInsuranceMinCurrency'] ;
    }
    else
    {
        $iInsuranceMinCurrency = $kInsurance->idInsuranceMinCurrency;
    }

    if(isset($_POST['addInsuranceAry'][$iType]['fInsuranceMinPrice']))
    {
        $fInsuranceMinPrice = $_POST['addInsuranceAry'][$iType]['fInsuranceMinPrice'] ;
    }
    else
    {
        if(!empty($kInsurance->fInsuranceMinPrice))
        {
            $fInsuranceMinPrice = round((float)$kInsurance->fInsuranceMinPrice);
        }  
    }

    if(isset($_POST['addInsuranceAry'][$iType]['idInsuranceRates']))
    {
        $idInsuranceRates = $_POST['addInsuranceAry'][$iType]['idInsuranceRates'] ;
    }
    else
    {
        $idInsuranceRates = $kInsurance->id ;
    } 
    if(isset($_POST['addInsuranceAry'][$iType]['idOriginCountry']))
    {
        $idOriginCountry = $_POST['addInsuranceAry'][$iType]['idOriginCountry'] ;
    }
    else
    {
        $idOriginCountry = $kInsurance->idOriginCountry;
    }
    if(isset($_POST['addInsuranceAry'][$iType]['idDestinationCountry']))
    {
        $idDestinationCountry = $_POST['addInsuranceAry'][$iType]['idDestinationCountry'] ;
    }
    else
    {
        $idDestinationCountry = $kInsurance->idDestinationCountry ;
    }
    if(isset($_POST['addInsuranceAry'][$iType]['idTransportMode']))
    {
        $idTransportMode = $_POST['addInsuranceAry'][$iType]['idTransportMode'] ;
    }
    else
    {
        $idTransportMode = $kInsurance->idTransportMode ;
    }
    if(isset($_POST['addInsuranceAry'][$iType]['iPrivate']))
    {
        $iPrivate = $_POST['addInsuranceAry'][$iType]['iPrivate'] ;
    }
    else
    {
        $iPrivate = $kInsurance->iPrivate ;
    }
    if(isset($_POST['addInsuranceAry'][$iType]['idInsurance']))
    {
        $idInsurance = $_POST['addInsuranceAry'][$iType]['idInsurance'] ;
    }
    else
    {
        $idInsurance = $kInsurance->idInsurance ;
    }
    if(isset($_POST['addInsuranceAry'][$iType]['idInsuranceParent']))
    {
        $idInsuranceParent = $_POST['addInsuranceAry'][$iType]['idInsuranceParent'] ;
    } 
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false,false,true,true);

    $allCountriesArr = array();
    $allCountriesArr = $kConfig->getAllCountriesForPreferencesKeyValuePair(); 
    
    $select_style = 'style="width:100%;"' ; 
    
    if($iType==1)  
    { 
        $table_width = "width='".(int)(100/7)."%' ";
        $table_width_val = "width='".((int)(100/7)+2)."%' ";
        $table_width_rate = "width='".((int)(100/7)-3)."%' "; 
        
        $table_width_val_field = "width='".(int)((((int)(100/7)+2))/2)."%' ";
    }
    else
    {
        $table_width = "width='".(int)(100/3)."%' ";
        $table_width_val = "width='".((int)(100/3)+2)."%' ";
        $table_width_rate = "width='".((int)(100/3)-2)."%' ";
        $table_width_val_field = "width='".(int)((((int)(100/3)+2))/2)."%' ";
    }
?>
	
    <div id="isurance_buy_rate_form" style="width:100%;">
        <?php if(!empty($szSpecialErrorMessage)){ ?>
            <p style="font-style:italic;color:red;font-size:14px;"> <?php echo $szSpecialErrorMessage; ?></p>
        <?php  }  ?>
	<form action="javascript:void(0);" name="insurance_form_type_<?php echo $iType; ?>" id="insurance_form_type_<?php echo $iType; ?>">
            <table cellpadding="0" cellspacing="1" width="100%">
		<tr> 
                    <?php if($iType==1){ $diff=2 ?>
                    <td <?php echo $table_width; ?> class="font-14"><?=t($t_base.'fields/from')?></td>
                    <td <?php echo $table_width; ?> class="font-14"><?=t($t_base.'fields/to')?></td>
                    <td style="width:11%;" class="font-14"><?=t($t_base.'fields/mode')?></td>
                    <td style="width:12%;" class="font-14"><?=t($t_base.'fields/moving')?></td>
                    <?php }?>
                    <td style="width:20%;" class="font-14"><?=t($t_base.'fields/value_up_to')?></td>
                    <td style="width:12%;" class="font-14"><?=t($t_base.'fields/rate')?></td>
                    <td style="width:17%;" style="text-align:center;" class="font-14"><?=t($t_base.'fields/minimum')?></td>
		</tr>
		<tr>
                    <?php if($iType==1){ ?>
                    <td valign="top">
                        <select <?php echo $select_style; ?> class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idOriginCountry]" id="idOriginCountry_<?php echo $iType; ?>">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idOriginCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td  valign="top">
                        <select <?php echo $select_style; ?> class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idDestinationCountry]" id="idDestinationCountry_<?php echo $iType; ?>">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idDestinationCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td valign="top">
                        <select <?php echo $select_style; ?> class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idTransportMode]" id="idTransportMode_<?php echo $iType; ?>">
                            <option value="">Select</option>
                        <?php
                            if(!empty($transportModeListAry))
                            {
                                foreach($transportModeListAry as $transportModeListArys)
                                {
                                    ?>
                                    <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($idTransportMode==$transportModeListArys['id'])?'selected':''); ?>><?php echo $transportModeListArys['szShortName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td valign="top">
                        <select <?php echo $select_style; ?> class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][iPrivate]" id="iPrivate_<?php echo $iType; ?>">
                            <option value="1" <?php echo ($iPrivate==1?'selected':''); ?>>Yes</option> 
                            <option value="0" <?php echo ($iPrivate==0?'selected':''); ?>>No</option> 
                        </select>
                    </td>
                     <?php } ?>
                    <td>
                        <select <?php echo $select_style; ?> class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][idInsuranceCurrency]" id="idInsuranceCurrency_<?php echo $iType; ?>">
                        <?php
                            if(!empty($currencyAry))
                            {
                                foreach($currencyAry as $currencyArys)
                                {
                                    ?>
                                    <option value="<?=$currencyArys['id']?>" <?php echo (($idInsuranceCurrency==$currencyArys['id'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                        <br>
                        <input type="text" onkeypress="return format_decimat(this,event)" style="margin-top:2px;" class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][fInsuranceUptoPrice]" id="fInsuranceUptoPrice_<?php echo $iType; ?>" value="<?php echo $fInsuranceUptoPrice; ?>">
                    </td>
                    <td  style="text-align:center;" valign="bottom"> (%) <br> <input type="text" onkeypress="return format_decimat(this,event)" style="margin-top:4px;" class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][fInsuranceRate]" id="fInsuranceRate_<?php echo $iType; ?>" value="<?php echo $fInsuranceRate; ?>"></td>
                    <td >
                        <select <?php echo $select_style; ?> class="addInsurateRatesFields_<?php echo $iType; ?>" name="addInsuranceAry[<?php echo $iType; ?>][iInsuranceMinCurrency]" id="iInsuranceMinCurrency_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <?php
                            if(!empty($currencyAry))
                            {
                                foreach($currencyAry as $currencyArys)
                                {
                                    ?>
                                    <option value="<?=$currencyArys['id']?>" <?php echo (($iInsuranceMinCurrency==$currencyArys['id'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                    <?
                                }
                            }
                         ?>
                        </select>
                        <br>
                        <input type="text" style="margin-top:2px;" onkeypress="return format_decimat(this,event)" class="addInsurateRatesFields_<?php echo $iType; ?>" onfocus="<?php echo $onfocus_func; ?>check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="addInsuranceAry[<?php echo $iType; ?>][fInsuranceMinPrice]" id="fInsuranceMinPrice_<?php echo $iType; ?>" value="<?php echo $fInsuranceMinPrice; ?>">
                    </td>
		</tr> 
		</table>
		<input type="hidden" name="addInsuranceAry[<?php echo $iType; ?>][iInsuranceType]" id="iInsuranceType_<?php echo $iType; ?>" value="<?php echo $iType; ?>">
		<input type="hidden" name="addInsuranceAry[iType]" id="iType_<?php echo $iType; ?>" value="<?php echo $iType; ?>">
                
                <input type="hidden" name="addInsuranceAry[<?php echo $iType; ?>][idInsurance]" id="idInsurance_<?php echo $iType; ?>" value="<?php echo $idInsurance; ?>">
                <input type="hidden" name="addInsuranceAry[<?php echo $iType; ?>][idInsuranceParent]" id="idInsuranceParent_<?php echo $iType; ?>" value="<?php echo $idInsuranceParent; ?>">
		<input type="hidden" name="addInsuranceAry[<?php echo $iType; ?>][idInsuranceRates]" id="idInsuranceRates_<?php echo $iType; ?>" value="<?php echo $idInsuranceRates; ?>">
		
		
	</form>
	</div>
	<?php
}

function showCustomerAdminBilling($billingDetails,$idAdmin,$bExceptionFlag=false)
{	 
    $t_base="management/billings/";
    if($bExceptionFlag)
    {
        echo "<p class='red_text'>Critical: Exception occured in processing your request. Please ask your dev to look into asap. </p>";
    }
    $kAdmin = new cAdmin();
    $kAdmin->getAdminDetails($idAdmin);
   
    $updateStatusFlag = checkManagementPermission($kAdmin,"__UPDATE_STATUS__",3);
    $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'];  
?>
        <script type="text/javascript">
            $(document).ready(function() {
                var settings = {
                url: "<?php echo __MANAGEMENT_URL__?>/outstandingPayment/uploadStripePayments.php",
                method: "POST",
                allowedTypes:"csv",
                fileName: "myfile",
                multiple: false,
                dragDrop: false,
                uploadButtonClass: "button1 upload-stripe-csv",
                onSuccess:function(files,data,xhr)
                { 
                    var IS_JSON = true;
                    try
                    {
                        var json = $.parseJSON(data);
                    }
                    catch(err)
                    {
                        IS_JSON = false;
                    } 
                    if(IS_JSON)
                    { 
                        var obj = JSON.parse(data); 

                        var iFileCounter = 1;

                        //var filecount=$("#filecount").attr('value');

                        //$("#deleteArea").attr('style','display:block'); 
                        $("#upload_process_list").attr('style','display:none');
                        $("#upload_process_list").html('');
                        //$(".file_list_con").attr('style','display:block');

                        var newfilecount = iFileCounter;  
                        var uploadedFileName = obj[0]['name']; 

                        //$('#fileList').html('<br><strong>'+uploadedFileName+'</strong>');
                        var newvalue=files+"#####"+uploadedFileName;
                        $("#filecount").attr('value',newfilecount);
                        $("#file_name").attr('value',newvalue);

                        if(parseInt(newfilecount) >0)
                        {
                            var vaildator ="<?php echo $idAdmin;?>";
                            //$(".upload").attr('style','display:none');
                            //$('#submit_button').attr('style','opacity:1');
                            //$('#submit_button').attr('onclick','submitStripePaymentCsv('+vaildator+');');
                            //$(".button1 upload-stripe-csv").attr('style','display:none');
                            submitStripePaymentCsv(vaildator);
                        } 
                    }
                    else
                    {  
                        $("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='showHide(\"upload_process_list\")'>close</a></p>");
                    }

                },onSelect:function(s){
                    $("#upload_process_list").attr('style','display:block;top:93%;width:345px');
                    $("#upload_error_container").attr('style','display:none');
                },
                    afterUploadAll:function()
                    {
                        //alert("all images uploaded!!");
                    },
                    onError: function(files,status,errMsg)
                    {
                        //$("#status").html("<font color='red'>Upload is Failed</font>");
                       // alert('hi');
                    }
                }
                $("#upload_csv_file").uploadFile(settings);
            });
        </script>
	<table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
            <tr>
                <td width="18%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/booked')?></strong> </td>
                <td width="12%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/booking_reference')?></strong> </td>
                <td width="10%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/billing_invoice')?></strong> </td>
                <td width="10%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/paid_by')?></strong> </td>
                <td width="25%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/customer')?></strong> </td>
                <td width="15%" style="text-align:right" valign="top"><strong><?=t($t_base.'fields/price')?></strong> </td>
               <?php if($updateStatusFlag){?>
                <td width="10%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/update')?></strong> </td>
               <?php }?>  
            </tr>
			<?php
				if($billingDetails!=array())
				{	$ctr=1;
					foreach($billingDetails as $bills)
					{	
						$id=base64_encode($bills['id']);
						$szEmail='';
						if(strlen($bills['szEmail'])>25)
						{
                                                    $szEmail=substr_replace($bills['szEmail'],'..',25,strlen($bills['szEmail']));
						}
						else
						{
                                                    $szEmail=$bills['szEmail'];
						}
						$style="";
						$kAdmin = new cAdmin();
						if(!$kAdmin->checkBookAmountEqualAmountPaid($bills['BookingId']))
						{
                                                    $style="color:red";  
						}
                                                
                                                $style1='';
                                                $onclickEvent='';
						if(!empty($bills['dtSnooze']) && $bills['dtSnooze']!='0000-00-00 00:00:00' && strtotime(date('Y-m-d'))>strtotime($bills['dtSnooze'])){
                                                    if($bills['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__ && $bills['iTransferConfirmed']==1)
                                                    {
                                                        
                                                        if($bills['iBookingType']==__BOOKING_TYPE_RFQ__)
                                                        { 
                                                            $style="color:red"; 
                                                            $style1="class='snoozeDate'";
                                                            $onclickEvent="onclick=openSnoozePopup('OUTSTANDINGPAYMENT','".$bills['BookingId']."');";
                                                        }
                                                        else
                                                        {
                                                            
                                                            if($bills['iCourierBooking']==1)
                                                            {
                                                                $szBankTransferDueDateTime = strtotime($bills['dtCutOff']);
                                                            }
                                                            else
                                                            {
                                                                if($bills['idServiceType']==__SERVICE_TYPE_DTD__)
                                                                {
                                                                    $szBankTransferDueDateTime = strtotime($bills['dtCutOff']) - ($bills['iBookingCutOffHours']*60*60);
                                                                }
                                                                else
                                                                {
                                                                    $szBankTransferDueDateTime = strtotime($bills['dtWhsCutOff']) - ($bills['iBookingCutOffHours']*60*60);
                                                                }
                                                            }
                                                            
                                                            $dtTodayDateTime = strtotime(date('Y-m-d'));
                                                            if($dtTodayDateTime>$szBankTransferDueDateTime)
                                                            {
                                                                $style="color:red"; 
                                                                $style1="class='snoozeDate'";
                                                                $onclickEvent="onclick=openSnoozePopup('OUTSTANDINGPAYMENT','".$bills['BookingId']."');";

                                                            }
                                                        }
                                                    }
                                                }
//                                                
//                                                if($bills['iTransferConfirmed']=='1' && $bills['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__)
//                                                {
//                                                    
//                                                    if(strtotime(date('Y-m-d'))>strtotime($bills['dtSnooze'])){
//                                                        
//                                                           
//                                                    }
//                                                }
                                                
						$fInsurancePrice = 0;
						if($bills['iInsuranceIncluded']==1)
						{
                                                    $fInsurancePrice = $bills['fTotalInsuranceCostForBookingCustomerCurrency'];
						}
                                                if($bills['iQuickQuote']==__QUICK_QUOTE_MAKE_BOOKING__)
                                                {
                                                    $szPaymentType = "Quick quote";
                                                }
                                                else if($bills['iPartnerBooking']==1 && $bills['iQuickQuote']<=0)
						{ 
                                                    
                                                    if($bills['iPaymentTypePartner']==1)
                                                    {
                                                        $szPaymentType="Partner";
                                                    }else
                                                    {
                                                        $szPaymentType = $bills['iPaymentType']; 
                                                    }
                                                }
                                                else
                                                {
                                                    $szPaymentType = $bills['iPaymentType'];
                                                }
                                                
                                                if(date('I')==1)
                                                {
                                                    $iDayLightSavingTime = 1;  
                                                    //$bills['dtInvoiceOn'] = getDateCustom($bills['dtInvoiceOn']);  
                                                }   
                                                $date = new DateTime($bills['dtInvoiceOn']); 
                                                $date->setTimezone(new DateTimeZone($szCustomerTimeZone)); 
						$dtInvoicedOn = $date->format('d/m/Y H:i') ; //In local timezone
                                                ?>
						<tr align='center'  id='booking_data_<?php echo $ctr;?>' onclick=select_admin_tr('booking_data_<?php echo $ctr++;?>','<?php echo $bills['BookingId'];?>')>
								<td id='OUTSTANDINGPAYMENT_<?php echo $bills['BookingId'];?>' style='text-align:left' <?php echo $style1;?> <?php echo $onclickEvent;?>><?php echo $dtInvoicedOn;?></td>
								<td style='text-align:left'><a class='booking-ref-link' href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__."".$bills['szBookingRandomNum']."/";?>"><?php echo $bills['szBookingRef'];?></a></td>
								<td style='text-align:left'><?php echo $bills['szInvoice'];?></td>
								<td style='text-align:left'><?php echo $szPaymentType;?></td>
								<td style='text-align:left'><?php echo $szEmail;?></td>
								<td style='text-align:right;<?php echo $style;?>'><?php echo $bills['szCurrency']." ".number_format((float)($bills['fTotalAmount']+$fInsurancePrice),2);?></td>
								<?php if($updateStatusFlag){?>
                                                                <td onclick="showConfirmationpopup('<?=$id?>','<?=$idAdmin?>','<?=$bills['BookingId']?>')" style="cursor: pointer;text-align: left;"><a><?=t($t_base.'fields/received')?></a></td>
                                                                <?php }?>
							</tr>	
								<?php
						$id='';
					}
				 }
				else
				{
					echo "<tr><td colspan='7'><CENTER><b>".t($t_base.'fields/no_record_found')."</b><CENTER></td></tr>";
				}
			?>
		</table>
                        <div id="viewInvoiceComplete" style="float: right;">
				<a class="button1" id="download_invoice" style="opacity:0.4;"><span><?=t($t_base.'fields/view_invoice');?></span></a>  
				<a class="button1" id="download_booking"  style="opacity:0.4;"><span><?=t($t_base.'fields/view_booking');?></span></a>
			</div>
                        <?php if($updateStatusFlag){?>
                        <div id="upload_error_container" style="display:none;"></div>
                        <div id="upload_csv_file">
                              <span><?=t($t_base.'fields/upload');?></span>
                        </div>
                        <div id="upload_process_list" style="display:none;"></div>
                        <form name="submitStripePaymentCsvForm" id="submitStripePaymentCsvForm" method="post" action="javascript:void(0);">
                            <input id="filecount" name="filecount" value="0" type="hidden">
                            <input id="file_name" name="file_name" value="" type="hidden">
                        </form>
                        <?php }?>
<?php
}

function display_insurance_rate_delete_confirmation($kInsurance,$iType)
{ 
	$t_base="management/insurance/";
 ?>
 <div id="popup-bg"></div>
	<div id="popup-container" >
		<div class="popup">
		<h3><?=t($t_base.'fields/delete_insurance_rate');?></h3>
		<p><?=t($t_base.'fields/confirmation_message');?>?</p>
		<br/>
		<p align="center">
			<a href="javascript:void(0)" onclick="delete_insurance_rate('<?php echo $kInsurance->id; ?>','<?php echo $iType; ?>','DELETE_INSURANCE_RATE_CONFIRM')" class="button1" ><span><?=t($t_base.'fields/yes');?></span></a> 
			<a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup')"><span><?=t($t_base.'fields/no');?></span></a>
		</p>
	</div>
</div>
<?php 
}

function success_message_popup_admin($szTitle,$szMessage,$div_id=false,$iRedirect_flag=false,$szPageUrl=false,$buttonText=false)
{
    if(empty($div_id))
    {
        $div_id = 'success_message_div';
    }

    if($iRedirect_flag && !empty($szPageUrl))
    {
        $onclick_func = "redirect_url('".$szPageUrl."'); ";
    }
    else
    {
        $onclick_func = "showHide('".$div_id."'); ";
    }
    $t_base = "Users/AccountPage/";
?>  
<div id="popup-bg"></div>
<div id="popup-container">
    <div class="popup" style="width:550px;"> 
        <h2><?php echo $szTitle;?></h2>			
        <div style="padding:10px;">			
            <p> <br/><?php echo $szMessage;?>  </p>
        </div>
        <br />
        <p align="center" style="padding:10px;">
            <?php if($buttonText){?>
                <a class="button1" onclick="<?php echo $onclick_func;?>"><span>OK</span></a>
            <?php }else{?>
            <a class="button1" onclick="<?php echo $onclick_func;?>"><span><?=t($t_base.'fields/close')?></span></a> 
            <?php }?>
        </p>
    </div>
</div>
    <?php
}

function display_new_booking_listing_insurance($newInsuredBookingAry)
{   
    $t_base="management/insurance/";
    $kInsurance = new cInsurance(); 
    ?>   
    <form id="send_new_insured_booking" name="send_new_insured_booking" action="javascript:void(0);" method="post">
    <table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="insuranced_booking_table">
        <tr>
            <th style="width:5%;"><input type="checkbox" onclick="select_all_checkboxes(this.id)" name="select_all" id="select_all"></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/date')?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/booking')?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/customer');?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/insurance_value');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/sell');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/buy');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/gp');?></th> 
        </tr>
	<?php 
            if(!empty($newInsuredBookingAry))
            {
                $ctr = 1;
                $irowCount = count($newInsuredBookingAry); 

                foreach($newInsuredBookingAry as $searchResults)
                { 
                    $fInsuranceBuyPrice = $searchResults['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                    if($searchResults['fGoodsInsuranceExchangeRate']>0)
                    {
                        $fTotalAmountInsured = $searchResults['fTotalAmountInsured']/$searchResults['fGoodsInsuranceExchangeRate'] ;
                    }
                    else
                    {
                        $fTotalAmountInsured = 0;
                    }

                    $fTotalGrossProfit = round(($searchResults['fTotalInsuranceCostForBookingUSD'] - $searchResults['fTotalInsuranceCostForBookingUSD_buyRate']),2);

                    if($searchResults['iIncludeOnNewInsuranceScreen']>0)
                    {
                        $szInsuranceValueText = "(".$searchResults['szGoodsInsuranceCurrency']." ".number_format((float)$fTotalAmountInsured).")";
                        $szSellRateValueText = "(".$searchResults['szCurrency']." ".number_format((float)$searchResults['fTotalInsuranceCostForBookingCustomerCurrency']).")";
                        $szBuyRateValueText = "(".$searchResults['szCurrency']." ". number_format((float)$fInsuranceBuyPrice).")" ;
                        $szCustomerNameText = 'Cancelled';
                        $fTotalGrossProfit = abs((float)$fTotalGrossProfit);
                        $fTotalGrossProfit_str = "(USD ".number_format((float)$fTotalGrossProfit).")" ;
                    }
                    else
                    {
                        $szInsuranceValueText = $searchResults['szGoodsInsuranceCurrency']." ".number_format((float)$fTotalAmountInsured);
                        $szSellRateValueText = $searchResults['szCurrency']." ".number_format((float)$searchResults['fTotalInsuranceCostForBookingCustomerCurrency']);
                        $szBuyRateValueText = $searchResults['szCurrency']." ". number_format((float)$fInsuranceBuyPrice);
                        $szCustomerNameText = $searchResults['szFirstName']." ".$searchResults['szLastName'] ;

                        if($fTotalGrossProfit>0)
                        {				
                            $fTotalGrossProfit_str = "USD ".number_format((float)$fTotalGrossProfit) ;
                        }
                        else if((float)$fTotalGrossProfit==0)
                        {
                            $fTotalGrossProfit_str = "USD 0";
                        }
                        else
                        {
                            $fTotalGrossProfit_str = "(USD ".number_format((float)abs ($fTotalGrossProfit)).") ";
                        } 
                    }
                    $iFinancilaVersion = $searchResults['iFinancialVersion']; 
                        ?>				 
                        <tr id="booking_data_<?=$ctr?>" onclick="select_insured_booking_confirmation_tr('booking_data_<?=$ctr?>','<?php echo $searchResults['id']?>','NEW_INSURED_BOOKING','<?php echo $searchResults['iIncludeOnNewInsuranceScreen']; ?>','<?php echo $iFinancilaVersion; ?>')">
                            <td><input type="checkbox" onclick="enable_send_button();" class="insured-booking-check-boxes" name="sendBookingAry[iSendBooking][]" value="<?php echo $searchResults['id']; ?>" id="iSendBooking_<?php echo $ctr; ?>"></td>
                            <td><?php echo (!empty($searchResults['dtBookingConfirmed']) && $searchResults['dtBookingConfirmed']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtBookingConfirmed'])):''?></td>
                            <td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $searchResults['szBookingRandomNum'];?>/"><?php echo $searchResults['szBookingRef']?></a></td>
                            <td><?php echo $szCustomerNameText; ?></td>
                            <td><?php echo $szInsuranceValueText; ?></td>
                            <td><?php echo $szSellRateValueText; ?></td>
                            <td><?php echo $szBuyRateValueText; ?></td>
                            <td><?php echo $fTotalGrossProfit_str; ?></td>
                        </tr>
                        <?php
                            $ctr++;
                    } 
		}
		else
		{
                    ?> 
                    <tr>
                        <td colspan="8" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
                    </tr> 
                    <?php
		}
	?>
</table>  
</form>
<br>
<div>
	<span align="left"><a href="javascript:void(0)" class="button1" style="opacity:0.4" id="insured_booking_send_button" align="left"><span><?=t($t_base.'fields/send');?></span></a></span>
	<span style="float: right;"> 
		<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="insured_booking_invoice_button"><span><?=t($t_base.'fields/invoice');?></span></a>
		<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="insured_booking_cancel_button"><span><?=t($t_base.'fields/cancel');?></span></a>
	</span>
</div>
<?php 

}


function display_sent_insured_booking_listing($newInsuredBookingAry,$iInsuranceStatus=__BOOKING_INSURANCE_STATUS_SENT__,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)
{   
    $t_base="management/insurance/";
    $kInsurance = new cInsurance();  
    $iColspan="8"; 
    if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__)
    {
        $szFromPage = "SENT_INSURED_BOOKING"; 
    }
    else if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_CONFIRMED__)
    {
        $szFromPage = "CONFIRMED_INSURED_BOOKING"; 
    }
    else if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_CANCELLED__)
    {
        $szFromPage = "CANCELLED_INSURED_BOOKING"; 
    } 
    $newInsuredBookingAry = sortArray($newInsuredBookingAry,'dtBookingConfirmed','DESC');
    
       
    if($iInsuranceStatus!=__BOOKING_INSURANCE_STATUS_SENT__)
    {
        
        $iGrandTotal = count($newInsuredBookingAry); 
        $chunkedEmailAry = array();
        $chunkedEmailAry = array_chunk($newInsuredBookingAry, $limit); 
        $newInsuredBookingAry = $chunkedEmailAry[$iPage-1]; 
        $newInsuredBookingAry = sortArray($newInsuredBookingAry,'dtBookingConfirmed','DESC');
    
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__); 
    }
            
    /*
     * Calculating Exchange rate for DKK
     */
    $kWarehouseSearch = new cWHSSearch();
    $resultAry = $kWarehouseSearch->getCurrencyDetails(20);
    $fDkkExchangeRate = 0;
    if($resultAry['fUsdValue']>0)
    {
        $fDkkExchangeRate = $resultAry['fUsdValue'] ;						 
    }  
    
    ?>   
    <form id="confirm_insured_booking_form" name="confirm_insured_booking_form" action="javascript:void(0);" method="post">
    <table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="insuranced_booking_table">
        <tr>		
            <th style="width:5%;">
                <?php if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__){ ?>
                <input type="checkbox" onclick="select_all_checkboxes(this.id,'SENT_BOOKING');calculate_insurance_buy_rate();" name="select_all" id="select_all">
                <?php } ?>
            </th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/date')?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/booking')?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/customer');?></th>
            <th style="width:15%;" valign="top"><?=t($t_base.'fields/insurance_value');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/sell');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/buy');?></th>
            <th style="width:10%;" valign="top"><?=t($t_base.'fields/gp');?></th> 
        </tr>
	<?php 
        if(!empty($newInsuredBookingAry))
        {
            $ctr = 1;
            $irowCount = count($newInsuredBookingAry); 

            for($i=0;$i<$irowCount;++$i)
            {  
                
                $idInsuranceCurrency = $newInsuredBookingAry[$i]['idCurrency']; 
                $fInsuranceBuyPrice = $newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                if($newInsuredBookingAry[$i]['fGoodsInsuranceExchangeRate']>0)
                {
                    $fTotalAmountInsured = $newInsuredBookingAry[$i]['fTotalAmountInsured']/$newInsuredBookingAry[$i]['fGoodsInsuranceExchangeRate'] ;
                }
                else
                {
                    $fTotalAmountInsured = 0;
                }

                $fTotalGrossProfit = round(($newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingUSD'] - $newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingUSD_buyRate']),2);

                $szLogStr = "";
                $fInsuranceBuyRateDkk = 0;
                if($idInsuranceCurrency==20) //DKK
                {
                    $fInsuranceBuyRateDkk = round((float)$newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingCustomerCurrency_buyRate']); 
                }
                else if($newInsuredBookingAry[$i]['fDkkExchangeRate']>0)
                { 
                    $fInsuranceBuyRateDkk = round((float)($newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingUSD_buyRate']/$newInsuredBookingAry[$i]['fDkkExchangeRate']));  
                }
                else if($fDkkExchangeRate>0)
                { 
                    $fInsuranceBuyRateDkk = round((float)($newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingUSD_buyRate']/$fDkkExchangeRate));  
                }
                
                if(($newInsuredBookingAry[$i]['iIncludeOnNewInsuranceScreen']>0 || $newInsuredBookingAry[$i]['iInsuranceCancelledAfterSent']==1) && $iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__)
                {
                    
                    $iCheckValueFlag=3;
                    if(($newInsuredBookingAry[$i]['iIncludeOnNewInsuranceScreen']==2 && $newInsuredBookingAry[$i]['iInsuranceCancelledAfterSent']==1) && $iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__)
                    {
                        $iCheckValueFlag=5;
                    }
                    if($newInsuredBookingAry[$i]['iInsuranceCancelledAfterSent']==1)
                    {
                        $szInsuranceValueText = $newInsuredBookingAry[$i]['szGoodsInsuranceCurrency']." ".number_format((float)$fTotalAmountInsured);
                        $szSellRateValueText = $newInsuredBookingAry[$i]['szCurrency']." ".number_format((float)$newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingCustomerCurrency']);
                        $szBuyRateValueText = $newInsuredBookingAry[$i]['szCurrency']." ". number_format((float)$fInsuranceBuyPrice) ;
                        $szCustomerNameText = $newInsuredBookingAry[$i]['szFirstName']." ".$newInsuredBookingAry[$i]['szLastName'];
                        if($fTotalGrossProfit>0)
                        {				
                            $fTotalGrossProfit_str = "USD ".number_format((float)$fTotalGrossProfit) ;
                        }
                        else if((float)$fTotalGrossProfit==0)
                        {
                            $fTotalGrossProfit_str = "USD 0";
                        }
                        else
                        {
                            $fTotalGrossProfit_str = "(USD ".number_format((float)$fTotalGrossProfit).") ";
                        }
                        $iIncludeOnNewInsuranceScreen = 0;
                        
                    }
                    else
                    {
                        $szInsuranceValueText = "(".$newInsuredBookingAry[$i]['szGoodsInsuranceCurrency']." ".number_format((float)$fTotalAmountInsured).")";

                        $szSellRateValueText = "(".$newInsuredBookingAry[$i]['szCurrency']." ".number_format((float)$newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingCustomerCurrency']).")";
                        $szBuyRateValueText = "(".$newInsuredBookingAry[$i]['szCurrency']." ". number_format((float)$fInsuranceBuyPrice).")" ;
                        $szCustomerNameText = 'Cancelled';
                        $fTotalGrossProfit = abs((float)$fTotalGrossProfit);
                        $fTotalGrossProfit_str = "(USD ".number_format((float)$fTotalGrossProfit).")" ;
                        $iIncludeOnNewInsuranceScreen = 1; 
                        $fInsuranceBuyRateDkk = $fInsuranceBuyRateDkk * (-1);
                        $iCheckValueFlag=2;
                    }
                }
                else
                {
                    $szInsuranceValueText = $newInsuredBookingAry[$i]['szGoodsInsuranceCurrency']." ".number_format((float)$fTotalAmountInsured);
                    $szSellRateValueText = $newInsuredBookingAry[$i]['szCurrency']." ".number_format((float)$newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingCustomerCurrency']);
                    $szBuyRateValueText = $newInsuredBookingAry[$i]['szCurrency']." ". number_format((float)$fInsuranceBuyPrice) ;
                    $szCustomerNameText = $newInsuredBookingAry[$i]['szFirstName']." ".$newInsuredBookingAry[$i]['szLastName'];
                    if($fTotalGrossProfit>0)
                    {				
                        $fTotalGrossProfit_str = "USD ".number_format((float)$fTotalGrossProfit) ;
                    }
                    else if((float)$fTotalGrossProfit==0)
                    {
                        $fTotalGrossProfit_str = "USD 0";
                    }
                    else
                    {
                        $fTotalGrossProfit_str = "(USD ".number_format((float)$fTotalGrossProfit).") ";
                    }
                    $iIncludeOnNewInsuranceScreen = 0;
                    $iCheckValueFlag=1;
                } 
                $iFinancilaVersion = $newInsuredBookingAry[$i]['iFinancialVersion']; 
                if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_CANCELLED__)
                {
                    /*
                     * If only Insurance is cancelled then we display Insurance Credit Notes and if complete bookings is cancelled then we display Customer Credit Notes.
                     */
                    $iCancelledBooking = false;
                    if($newInsuredBookingAry[$i]['idBookingStatus']==7 && $newInsuredBookingAry[$i]['iInsuranceCancelledIndividually'])
                    {
                        //This means complete booking has been cancelled. We are displaying customer credit notes for this booking.
                        $iCancelledBooking = 1;
                    } 
                }
            ?>				 
                <tr id="booking_data_<?php echo $ctr?>" onclick="select_insured_booking_confirmation_tr('booking_data_<?php echo $ctr?>','<?php echo $newInsuredBookingAry[$i]['id']?>','<?php echo $szFromPage; ?>','<?php echo $iIncludeOnNewInsuranceScreen; ?>','<?php echo $iFinancilaVersion; ?>','<?php echo $iCancelledBooking; ?>')">
                    <td>
                        <?php if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__){ ?>
                            <input type="checkbox" onclick="enable_send_button('SENT_BOOKING');calculate_insurance_buy_rate('<?php echo $ctr?>');" class="insured-booking-check-boxes" name="confirmBookingAry[iConfirmBooking][]" value="<?php echo $newInsuredBookingAry[$i]['id']; ?>" id="iSendBooking_<?php echo $ctr; ?>">
                            <input type="hidden" value="<?php echo $fInsuranceBuyRateDkk; ?>" name="fInsuranceBuyRateDkk" id="fInsuranceBuyRateDkk_<?php echo $newInsuredBookingAry[$i]['id']; ?>">
                            <input type="checkbox" style="display:none;" value="<?php echo $iCheckValueFlag; ?>" name="confirmBookingAry[iCheckValueFlag][]" id="iCheckValueFlag_<?php echo $ctr; ?>">
                        <?php }?>
                    </td>
                    <td><?php echo (!empty($newInsuredBookingAry[$i]['dtBookingConfirmed']) && $searchResults['dtBookingConfirmed']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($newInsuredBookingAry[$i]['dtBookingConfirmed'])):''?></td>
                    <td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $newInsuredBookingAry[$i]['szBookingRandomNum'];?>/"><?php echo $newInsuredBookingAry[$i]['szBookingRef']?></a></td>
                    <td><?php echo $szCustomerNameText; ?></td>
                    <td><?php echo $szInsuranceValueText; ?></td>
                    <td><?php echo $szSellRateValueText; ?></td>
                    <td><?php echo $szBuyRateValueText; ?></td>
                    <td><?php echo $fTotalGrossProfit_str; ?></td>
                </tr>
                <?php
                $ctr++;
                if(($newInsuredBookingAry[$i]['iIncludeOnNewInsuranceScreen']==2 && $newInsuredBookingAry[$i]['iInsuranceCancelledAfterSent']==1) && $iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__)
                {
                    $szInsuranceValueText = "(".$newInsuredBookingAry[$i]['szGoodsInsuranceCurrency']." ".number_format((float)$fTotalAmountInsured).")";

                    $szSellRateValueText = "(".$newInsuredBookingAry[$i]['szCurrency']." ".number_format((float)$newInsuredBookingAry[$i]['fTotalInsuranceCostForBookingCustomerCurrency']).")";
                    $szBuyRateValueText = "(".$newInsuredBookingAry[$i]['szCurrency']." ". number_format((float)$fInsuranceBuyPrice).")" ;
                    $szCustomerNameText = 'Cancelled';
                    $fTotalGrossProfit = abs((float)$fTotalGrossProfit);
                    $fTotalGrossProfit_str = "(USD ".number_format((float)$fTotalGrossProfit).")" ;
                    $iIncludeOnNewInsuranceScreen = 1; 
                    $fInsuranceBuyRateDkk = $fInsuranceBuyRateDkk * (-1);
                    ?>
                    <tr id="booking_data_<?php echo $ctr?>" onclick="select_insured_booking_confirmation_tr('booking_data_<?php echo $ctr?>','<?php echo $newInsuredBookingAry[$i]['id']?>','<?php echo $szFromPage; ?>','<?php echo $iIncludeOnNewInsuranceScreen; ?>')">
                        <td>
                            <?php if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__){ ?>
                                <input type="checkbox" onclick="enable_send_button('SENT_BOOKING');calculate_insurance_buy_rate();" class="insured-booking-check-boxes" name="confirmBookingAry[iConfirmBooking][]" value="<?php echo $newInsuredBookingAry[$i]['id']; ?>" id="iSendBooking_<?php echo $ctr; ?>">
                                <input type="hidden" value="<?php echo $fInsuranceBuyRateDkk; ?>" name="fInsuranceBuyRateDkk" id="fInsuranceBuyRateDkk_<?php echo $newInsuredBookingAry[$i]['id']; ?>">
                                <input type="checkbox" style="display:none;" value="4" name="confirmBookingAry[iCheckValueFlag][]" id="iCheckValueFlag_<?php echo $ctr; ?>">
                            <?php }?>
                        </td>
                        <td><?php echo (!empty($newInsuredBookingAry[$i]['dtBookingConfirmed']) && $searchResults['dtBookingConfirmed']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($newInsuredBookingAry[$i]['dtBookingConfirmed'])):''?></td>
                        <td><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $newInsuredBookingAry[$i]['szBookingRandomNum'];?>/"><?php echo $newInsuredBookingAry[$i]['szBookingRef']?></a></td>
                        <td><?php echo $szCustomerNameText; ?></td>
                        <td><?php echo $szInsuranceValueText; ?></td>
                        <td><?php echo $szSellRateValueText; ?></td>
                        <td><?php echo $szBuyRateValueText; ?></td>
                        <td><?php echo $fTotalGrossProfit_str; ?></td>
                    </tr>
                    <?php
                    $ctr++;
                }
                
            }  
        }
        else
        {
            ?>
            <tr>
                <td colspan="<?php echo $iColspan; ?>" align="center">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="<?php echo $iColspan; ?>" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
            </tr>
            <tr>
                <td colspan="<?php echo $iColspan; ?>" align="center">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="<?php echo $iColspan; ?>" align="center">&nbsp;</td>
            </tr>
    <?php } ?>
    </table>  
    <?php if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__){ ?> 
        <div class="clearfix email-fields-container" style="width:100%;margin-top:5px;">
            <span class="quote-field-container wd-60" style="font-weight:bold;">Total insurance buy rate of selected bookings</span>
            <span class="quote-field-container wd-40" id="insurance_total_buy_rate_container_span" style="text-align:right;font-weight:bold;">None selected</span>
        </div>
    <?php }?>
</form>
<input type="hidden" id="page" name="page" value="<?php echo $iPage;?>">
<?php 
    if($iInsuranceStatus!=__BOOKING_INSURANCE_STATUS_SENT__)
    {?>
<div class="page_limit clearfix">
        <div class="pagination">
            <?php

                $kPagination->szMode = "DISPLAY_BOOKING_LIST_PAGINATION";
                //$kPagination->idBookingFile = $idBookingFile;
                $kPagination->paginate('display_booking_list_pagination'); 
                if($kPagination->links_per_page>1)
                {
                    echo $kPagination->renderFullNav();
                }        
            ?>
               </div>
               <div class="limit">Records per page
                   <select id="limit" name="limit" onchange="display_booking_list_pagination('1')">
                       <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option> 
                        <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                        <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                        <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                   </select>
               </div>    
            </div>
    <?php }?>
<br>
<div>
<?php if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_SENT__){ ?>
	<span align="left"><a href="javascript:void(0)" class="button1" style="opacity:0.4" id="insured_booking_send_button" align="left"><span><?=t($t_base.'fields/confirmed');?></span></a></span>
<?php } ?>
	<span style="float: right;"> 
		<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="insured_booking_invoice_button"><span><?=t($t_base.'fields/invoice');?></span></a>
		<?php if($iInsuranceStatus==__BOOKING_INSURANCE_STATUS_CANCELLED__)	{?>
			<a href="javascript:void(0)" class="button1" style="opacity:0.4" id="insured_booking_cancel_button"><span><?=t($t_base.'fields/credit_note');?></span></a>
		<?php } else {?>
			<a href="javascript:void(0)" class="button2" style="opacity:0.4" id="insured_booking_cancel_button"><span><?=t($t_base.'fields/cancel');?></span></a>
		<?php }?>
	</span>
</div>
<?php  
} 
function display_insuredbooking_cancel_confirmation($postSearchAry,$szPageName)
{ 
	$t_base="management/insurance/";
 ?>
 <div id="popup-bg"></div>
	<div id="popup-container" >
		<div class="popup">
		<h3><?=t($t_base.'fields/insurance_notification');?></h3> <br><br>
		<p><?=t($t_base.'fields/cancel_booking_confirmation');?>?</p> <br>
		<p><input type="checkbox" name="iSendCreditNote" id="iSendCreditNote" value="1"> <?=t($t_base.'fields/send_credit_note');?></p>
		<br/>
		<p align="center">
			<a href="javascript:void(0)" onclick="cancel_insured_booking('<?php echo $postSearchAry['id']; ?>','CANCEL_NEW_INSURED_BOOKING_CONFIRM','<?php echo $szPageName; ?>')" class="button1" ><span><?=t($t_base.'fields/yes');?></span></a> 
			<a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup')"><span><?=t($t_base.'fields/no');?></span></a>
		</p>
	</div>
</div>
<?php }

function display_insured_booking_send_confirmation($sendBookingAry)
{
    $t_base="management/insurance/";
    $szSendBookingSerializedStr = implode("XXXX",$sendBookingAry); 
    $iNumSendBooking = count($sendBookingAry);
    if(!empty($_COOKIE['__INSURANCE_VENDOR_EMAIL__']))
    {
        $szEmail = $_COOKIE['__INSURANCE_VENDOR_EMAIL__'] ;
    }
 ?> 
<div id="popup-bg"></div>
<div id="popup-container">
    <form action="javascript:void(0);" name="send_isurance_vendor_email" id="send_isurance_vendor_email" method="post">
        <div class="popup" style="width:400px;">
            <h3><?=t($t_base.'fields/send_to_insurance_company');?></h3> <br><br>
            <p><?php echo t($t_base.'fields/details_text_1')." ".$iNumSendBooking." ".t($t_base.'fields/details_text_2');?></p> <br>
            <p><?=t($t_base.'fields/email_address');?>: <input style="width:210px;" onfocus="check_insurace_send_email(event,this.form.id,this.id);" onkeyup="check_insurace_send_email(event,this.form.id,this.id);" type="text" name="sendInsuranceEmail[szInsuranceVendorEmail]" id="szInsuranceVendorEmail" value="<?php echo $szEmail; ?>"> </p>
            <br/>
            <p align="center">
                <input type="hidden" name="sendInsuranceEmail[idBookingPipeLine]" id="idBookingPipeLine" value="<?php echo $szSendBookingSerializedStr; ?>">
                <a href="javascript:void(0)"  <?php if($szEmail==''){?> style="opacity:0.4;" <?php }else{?> onclick="send_insurance_email();" <?php }?> id="email_vendor_send_button" class="button1" ><span><?=t($t_base.'fields/send');?></span></a> 
                <a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup')"><span><?=t($t_base.'fields/cancel');?></span></a> 
            </p>
            <br>
            <p><a href="javascript:void(0)" onclick="preview_insurance_sheet_before_sending();">Click here</a> to see preview of insurance sheet before sending to insurance vendor.</p>
        </div>
    </form>
</div> 
<?php 
} 

function display_confirm_insured_booking_confirmation($confirmedBookingAry,$iCheckValueFlagAry)
{ 
	$t_base="management/insurance/";
	
	$szSendBookingSerializedStr = implode("XXXX",$confirmedBookingAry); 
	$iNumSendBooking = count($confirmedBookingAry);
        
        $iCheckValueFlagStr = implode("XXXX",$iCheckValueFlagAry); 
        
        
	
 ?>
 <div id="popup-bg"></div>
	<div id="popup-container" >
	<form action="javascript:void(0);" name="confirm_insured_booking_form_popup" id="confirm_insured_booking_form_popup" method="post">
		<div class="popup">
		<h3><?=t($t_base.'fields/insurance_confirmed');?></h3> <br><br>
		<p><?php echo t($t_base.'fields/confirm_insurance_text1')." ".$iNumSendBooking." ".t($t_base.'fields/confirm_insurance_text2');?></p> <br> 
		<br/>
		<p align="center">
			<input type="hidden" name="confirmInsuranceBooking[idBookingPipeLine]" id="idBookingPipeLine" value="<?php echo $szSendBookingSerializedStr; ?>">
                        <input type="hidden" name="confirmInsuranceBooking[iCheckValueFlagStr]" id="iCheckValueFlagStr" value="<?php echo $iCheckValueFlagStr; ?>">
			<a href="javascript:void(0)" onclick="confirm_insurance_booking_submit();" class="button1" ><span><?=t($t_base.'fields/yes');?></span></a> 
			<a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup')"><span><?=t($t_base.'fields/no');?></span></a>
		</p>
	</div>
	</form>
</div>
<?php }
function display_new_search_form_admin($kBooking,$postSearchAry,$hidden_search_form=false,$service_page_flag=false,$hide_search_image=false,$szDefaultFromField=false)
{
	$iHiddenValue = 0;
	if($hidden_search_form)
	{
		$id_suffix = "_hiden";
		$iHiddenValue = 1;
	}
	$kBooking_new = new cBooking();
	
	//following sesssion will used only for multitabing issue 
	if(!empty($_REQUEST['booking_random_num']))
	{
		$szBookingRandomNum = $_REQUEST['booking_random_num'] ;
	}
	else if(!empty($postSearchAry['szBookingRandomNum']))
	{
		$szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
	}
	else
	{
		$szBookingRandomNum = $kBooking_new->getUniqueBookingKey(10);
		$szBookingRandomNum = $kBooking_new->isBookingRandomNumExist($szBookingRandomNum);
	} 
	if(!empty($kBooking->arErrorMessages))
	{
		foreach($kBooking->arErrorMessages as $errorKey=>$errorValue)
		{
			if($errorKey=='szOriginCountry')
			{
				$errorKey = 'szOriginCountryStr'.$id_suffix.'_container';
			} 
			if($errorKey=='szDestinationCountry')
			{
				$errorKey = 'szDestinationCountryStr'.$id_suffix.'_container';
			} 
			if($errorKey=='szDestinationCountry')
			{
				$errorKey = 'szDestinationCountryStr'.$id_suffix.'_container';
			}
			if($errorKey=='fCargoWeight_landing_page')
			{
				$errorKey = 'iWeight'.$id_suffix.'_container';
			}
			if($errorKey=='iVolume')
			{
				$errorKey = 'iVolume'.$id_suffix.'_container';
			}
			if($errorKey=='dtTiming')
			{
				$errorKey = 'dtTiming'.$id_suffix.'_container';
			}
			
			?>
			<script type="text/javascript">
				$("#"+'<?php echo $errorKey; ?>').addClass('red_border');
			</script>
			<?php
		}
	}
	
	$button_flag=true; 
	if(!empty($_COOKIE['__SIMPLE_SEARCH_COOKIE__']) && empty($postSearchAry))
	{	
		$today=date('Y-m-d');
		
		//print_r($_COOKIE['__SIMPLE_SEARCH_COOKIE__']);
		$simpleSearchCookieArr=explode("||||",$_COOKIE['__SIMPLE_SEARCH_COOKIE__']);
		
		$dtTiming=date('Y-m-d',strtotime($simpleSearchCookieArr[7]));
		if(strtotime($dtTiming)<strtotime($today))
		{
			$postSearchAry['dtTimingDate']=$today;
		}
		else
		{
			$postSearchAry['dtTimingDate']=$dtTiming;
		}
		
		$postSearchAry['idServiceType']=$simpleSearchCookieArr[4];
		$postSearchAry['szOriginCountryStr']=ucwords(strtolower($simpleSearchCookieArr[0]));
		$postSearchAry['szOriginCountry']=ucwords(strtolower($simpleSearchCookieArr[0]));
		$postSearchAry['szDestinationCountryStr']=ucwords(strtolower($simpleSearchCookieArr[2]));
		$postSearchAry['szDestinationCountry']=ucwords(strtolower($simpleSearchCookieArr[2]));
		
		$postSearchAry['fCargoVolume']=$simpleSearchCookieArr[5];
		$postSearchAry['fCargoWeight']=$simpleSearchCookieArr[6]; 
	}
	else if(empty($_COOKIE['__SIMPLE_SEARCH_COOKIE__']) && empty($postSearchAry))
	{			
		$postSearchAry['szOriginCountryStr'] = "Shanghai, China";
		$postSearchAry['szOriginCountry'] = "Shanghai, China";
		$postSearchAry['fCargoVolume']=__DEFAULT_CARGO_VOLUME__;
		$postSearchAry['fCargoWeight']=__DEFAULT_CARGO_WEIGHT__; 
	} 
	
	if(isset($_POST['searchAry']['szOriginCountryStr']))
	{
		$szOriginCountryStr = ucwords(strtolower(sanitize_all_html_input($_POST['searchAry']['szOriginCountryStr'])));
	}
	else
	{
		$szOriginCountryStr = ucwords(strtolower($postSearchAry['szOriginCountry'])) ;
	} 
			
	if(isset($_POST['searchAry']['szDestinationCountryStr']))
	{
		$szDestinationCountryStr = ucwords(strtolower(sanitize_all_html_input($_POST['searchAry']['szDestinationCountryStr'])));
	}
	else
	{
		$szDestinationCountryStr = ucwords(strtolower($postSearchAry['szDestinationCountry'])) ;
	}
	
	if(isset($_POST['searchAry']['iVolume']))
	{
		$fCargoVolume = sanitize_all_html_input($_POST['searchAry']['iVolume']);
	}
	else if($postSearchAry['fCargoVolume']!=0)
	{
		$fCargoVolume = round((float)$postSearchAry['fCargoVolume'],1);
	}
	else
	{
		$fCargoVolume = __DEFAULT_CARGO_VOLUME__ ;
	}
	
	if(isset($_POST['searchAry']['iWeight']))
	{
		$iWeight = sanitize_all_html_input($_POST['searchAry']['iWeight']);
	}
	else if($postSearchAry['fCargoWeight']!=0)
	{
		$iWeight = round((float)$postSearchAry['fCargoWeight'],1);
	}
	else
	{
		$iWeight = __DEFAULT_CARGO_WEIGHT__ ;
	}
	
	if(isset($_POST['searchAry']['idServiceType']))
	{
            $idServiceType = sanitize_all_html_input($_POST['searchAry']['idServiceType']);
	}
	else if(isset($postSearchAry['idServiceType']))
	{
            $idServiceType = (int)$postSearchAry['idServiceType'];
	}
	else
	{
            $idServiceType = __SERVICE_TYPE_PTD__ ;
	} 
	if($postSearchAry['dtTimingDate']!='0000-00-00 00:00:00' && !empty($postSearchAry['dtTimingDate']))
	{
            $dtTimingDate = date('d/m/Y',strtotime($postSearchAry['dtTimingDate']));
            $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
	}
	else
	{
            $dtTimingDate = date('d/m/Y');
            $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
	}
	 
	$t_base = "home/homepage/";
	
	$kWHSSearch=new cWHSSearch();
	$maxCargoLength = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_LENGTH__');
	$maxCargoWidth = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_WIDTH__');
	$maxCargoHeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_DIMENSION_HEIGHT__');
	$maxCargoWeight = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_WEIGHT__');
	$maxCargoQty = $kWHSSearch->getManageMentVariableByDescription('__MAX_CARGO_QUANTITY__');
	
	$maxVolume=($maxCargoLength*$maxCargoWidth*$maxCargoHeight)/1000000;
	
	$szClass_update='';
	if($service_page_flag)
	{
            $szClass_update = "update-btn";
	} 
?>
<div id="service-search-form-container">
<form id="landing_page_form<?php echo $id_suffix; ?>" name="landing_page_form<?php echo $id_suffix; ?>" method="post" action="javascript:void(0);">  
	<div class="search clearfix" id="search_header">	
<script type="text/javascript">
$().ready(function() {	

	var autocomplete1,place ;
	function initialize1() 
	{
        var input1 = document.getElementById('szOriginCountryStr<?php echo $id_suffix; ?>');
         
        var autocomplete1 = new google.maps.places.Autocomplete(input1); 
        
  		google.maps.event.addListener(autocomplete1, 'place_changed', function() 
  		{ 
		    var szOriginAddress_js = $("#szOriginCountryStr<?php echo $id_suffix; ?>").val();  
		    checkFromAddress(szOriginAddress_js,'FROM_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');
		});
        
        
        var input2 = document.getElementById('szDestinationCountryStr<?php echo $id_suffix; ?>');
        var autocomplete2 = new google.maps.places.Autocomplete(input2);
        
        google.maps.event.addListener(autocomplete2, 'place_changed', function() 
  		{
		    var szDestinationAddress_js = $("#szDestinationCountryStr<?php echo $id_suffix; ?>").val(); 
		     
		    checkFromAddress(szDestinationAddress_js,'TO_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');
		});
    }
      
    Custom.init();
    initialize1();
    
   // google.maps.event.addDomListener(window, 'load', initialize1);
    $("#datepicker1_search_services<?php echo $id_suffix; ?>")
                    .bind('keydown', function (event) {  
                    if (event.which == 13) {
                        var e = jQuery.Event("keydown");
                        e.which = 9;//tab 
                        e.keyCode = 9;
                        $(this).trigger(e);
                        return false;
                    }
    }).datepicker({ <?=$date_picker_argument?> });
   });
</script>	



	<?php if(!$hidden_search_form && !$service_page_flag && !$hide_search_image){ ?>
		<div class="location-pointer"><img src="<?php echo __BASE_STORE_IMAGE_URL__?>/search-from-to-arrow.png" alt=""></div>
	<?php } ?>
	<div class="location-container">
		<div class="from">
			<label><?=t($t_base.'fields/from');?></label>
			<div id="szOriginCountryStr<?php echo $id_suffix; ?>_container"><input type="text" name="searchAry[szOriginCountryStr]" id="szOriginCountryStr<?php echo $id_suffix; ?>"  tabindex="1" onblur="check_location(this.id,'FROM_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');check_form_field_empty_standard_search(this.form.id,this.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/from');?>');" value="<?php echo $szOriginCountryStr;?>" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event);" onfocus="reste_search_flags('FROM_COUNTRY');check_form_field_not_required(this.form.id,'szOriginCountryStr<?php echo $id_suffix; ?>_container');"></div>
		</div>
		<div class="to">
			<label><?=t($t_base.'fields/to');?></label>
			<div id="szDestinationCountryStr<?php echo $id_suffix; ?>_container" ><input type="text" name="searchAry[szDestinationCountryStr]" id="szDestinationCountryStr<?php echo $id_suffix; ?>" value="<?php echo $szDestinationCountryStr;?>" tabindex="2" onblur="check_location(this.id,'TO_COUNTRY','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');check_form_field_empty_standard_search(this.form.id,this.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/to');?>');" onkeyup="enableDisableSearchButton('<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?=$_SESSION['user_id']?>','<?php echo $iHiddenValue; ?>',event);" onfocus="reste_search_flags('TO_COUNTRY');check_form_field_not_required(this.form.id,'szDestinationCountryStr<?php echo $id_suffix; ?>_container');"></div>
		</div>
	</div>
	<div class="dimensions-container">
		<div class="pickup">
			<label> <?php echo t($t_base.'fields/pick_up_new');?><a href="javascript:void(0);" class="help" onmouseover="show_mainpage_tooltip('','<?=t($t_base.'messages/PICKUP_TOOL_TIP_TEXT');?>','customs_clearance_pop',event,'1');" onmouseout="hide_tool_tip('customs_clearance_pop')">&nbsp;</a></label>
			<div style="text-align:center;">				
				<select name="searchAry[iPickup]" class="styled" id="iPickup<?php echo $id_suffix; ?>" onfocus="change_dropdown_value(this.id);validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" onblur="change_dropdown_value(this.id,1);change_service_type(this.value,'<?php echo __SERVICE_TYPE_PTD__; ?>','<?php echo __SERVICE_TYPE_DTD__; ?>');" tabindex="3"> 
					<option value="0" <?php echo (($idServiceType==__SERVICE_TYPE_PTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/fob');?></option>
					<option value="1" <?php echo (($idServiceType==__SERVICE_TYPE_DTD__)?'selected':'');?>><?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/exw');?></option>
				</select>  
			</div>
		</div>
		<div class="date">
			<label><?php echo t($t_base.'fields/date');?></label>
			<div id="dtTiming<?php echo $id_suffix; ?>_container">
				<input tabindex="4" id="datepicker1_search_services<?php echo $id_suffix; ?>" name="searchAry[dtTiming]"  type="text" value="<?php echo $dtTimingDate; ?>" >
				<input type="hidden" name="searchAry[idTimingType]" id="szTiming" value="1">
			</div>
		</div>
		<div class="cbm">
			<label><?=t($t_base.'fields/cm');?></label>
			<div id="iVolume<?php echo $id_suffix; ?>_container"><input type="text"  onkeypress="return isNumberKeySearch(event);" tabindex="5" onfocus="check_form_field_not_required(this.form.id,'iVolume<?php echo $id_suffix; ?>_container');validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');" name="searchAry[iVolume]"  onblur="check_form_field_empty_standard_search(this.form.id,this.id,'iVolume<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/volume');?>');" onkeyup="checkFromAddressWeightVolume(this.value,'VOLUME','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?php echo t('Error/volume')." ".t('Error/must_be_more');?>','<?php echo t('Error/cbm');?>','<?php echo $iHiddenValue; ?>',event);" id="iVolume<?php echo $id_suffix; ?>" value="<?php echo $fCargoVolume;?>"></div>
		</div>
		<div class="kg">
			<label><?php echo t($t_base.'fields/kg');?></label>
			<div id="iWeight<?php echo $id_suffix; ?>_container">
				<input id="iWeight<?php echo $id_suffix; ?>" tabindex="6" type="text"  onkeypress="return isNumberKeySearch(event);" onblur="check_form_field_empty_standard_search(this.form.id,this.id,'iWeight<?php echo $id_suffix; ?>_container','<?=t($t_base.'fields/weight');?>','1');" onkeyup="checkFromAddressWeightVolume(this.value,'WEIGHT','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>','<?php echo t('Error/weight')." ".t('Error/must_be_more');?>','<?php echo t('Error/kg');?>','<?php echo $iHiddenValue; ?>',event);" value="<?php echo $iWeight; ?>" name="searchAry[iWeight]" onfocus="check_form_field_not_required(this.form.id,'iWeight<?php echo $id_suffix; ?>_container');validateaddress('<?php echo $iHiddenValue; ?>','<?=$_SESSION['user_id']?>','<?=__SELECT_SIMPLE_SERVICES_URL__?>','<?php echo $id_suffix; ?>','<?php echo $service_page_flag;?>');">
				<input type="hidden" name="searchAry[szPageLoaction]" id="szPageLoaction" value="1" />
				<input type="hidden" name="searchAry[szPageName]" id="szPageName" value="NEW_LANDING_PAGE">
				<input type="hidden" name="searchAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?php echo $szBookingRandomNum?>" />
				<input type="hidden" name="searchAry[idServiceType]" id="idServiceType<?php echo $id_suffix; ?>" value="<?php echo $idServiceType; ?>">
				<input type="hidden" name="searchAry[iHiddenChanged]" id="iHiddenChanged" value="2">
				<input type="hidden" name="searchAry[iHiddenValue]" id="iHiddenValue" value="<?php echo $iHiddenValue; ?>" />
				<input type="hidden" name="searchAry[iServiceFlag]" id="iServiceFlag" value="<?php echo $service_page_flag; ?>" />
				
				<input type="hidden" name="iPickupDefaultValue" id="iPickupDefaultValue" value="<?php echo t($t_base.'fields/pick_no');?>">
				<input type="hidden" name="iPickupDefaultValue_option1" id="iPickupDefaultValue_option1" value="<?php echo t($t_base.'fields/pick_no')." - ".t($t_base.'fields/fob');?>">
				<input type="hidden" name="iPickupDefaultValue_option2" id="iPickupDefaultValue_option2" value="<?php echo t($t_base.'fields/pick_yes')." - ".t($t_base.'fields/exw');?>">
				
				<input type="hidden" name="iPickupDropdownValue_option1" id="iPickupDropdownValue_option1" value="<?php echo t($t_base.'fields/pick_no');?>">
				<input type="hidden" name="iPickupDropdownValue_option2" id="iPickupDropdownValue_option2" value="<?php echo t($t_base.'fields/pick_yes');?>">
				 
				<input type="hidden" name="fMaxVolume" id="fMaxVolume" value="<?php echo number_format($maxVolume,2);?>">
				<input type="hidden" name="fMaxWeight" id="fMaxWeight" value="<?php echo round($maxCargoWeight);?>">
				<input type="hidden" name="searchAry[szReuquestPageUrl]" id="szReuquestPageUrl" value="<?php echo $_SERVER['REQUEST_URI'];?>">
				<input type="hidden" name="iCheckedForFromField" id="iCheckedForFromField" value="">
				<input type="hidden" name="iCheckedToFromField" id="iCheckedToFromField" value=""> 
			</div>
		</div>
	</div>
	
		<div <?php if($button_flag){?> class="btn <?php echo $szClass_update; ?>" <?php }else{?> class="btn-grey" <?php }?> id="search-btn-container<?php echo $id_suffix; ?>">
			<?php if($service_page_flag){ ?>
				<!-- <input type="button" tabindex="7" id="update_search<?php echo $id_suffix; ?>" onclick="update_service_listing('landing_page_form<?php echo $id_suffix; ?>','<?php echo $iHiddenValue; ?>');" value="<?php echo t($t_base.'fields/update');?>"> -->
				<a href="javascript:void(0);" tabindex="7" id="update_search<?php echo $id_suffix; ?>" onclick="update_service_listing('landing_page_form<?php echo $id_suffix; ?>','<?php echo $iHiddenValue; ?>');"> <?php echo t($t_base.'fields/update');?></a>
			<?php }else{ ?>
				<!-- <input type="button" tabindex="7" id="search_listing<?php echo $id_suffix; ?>" onclick="return validateNewLandingPageForm('landing_page_form<?php echo $id_suffix; ?>','<?php echo $_SESSION['user_id']; ?>','<?php echo __NEW_SERVICES_PAGE_URL__?>','<?php echo $iHiddenValue; ?>');" value="<?=t($t_base.'fields/search');?>"> -->
				<a href="javascript:void(0);" tabindex="7" id="search_listing<?php echo $id_suffix; ?>" onclick="return validateNewLandingPageForm('landing_page_form<?php echo $id_suffix; ?>','<?php echo $_SESSION['user_id']; ?>','<?php echo __NEW_SERVICES_PAGE_URL__?>','<?php echo $iHiddenValue; ?>');"><?=t($t_base.'fields/search');?></a>
			<?php }?>
		</div>
	</div>
</form>
</div>
<?php
} 

function display_link_builder($szLinkUrl)
{
    ?>  
    <div class="link-builder-div clearfix">
        <table style="width:100%;" class="tableFormat1" >
            <tr>
                <td style="width:88%;"><input type="text" name="ZeroClipboardMovie_1" style="width:100%;" id="ZeroClipboardMovie_1" value="<?php echo $szLinkUrl; ?>"></td>
                <td style="width:10%;"><a href="javascript:void(0);" onclick="select_all()" class="button1" id="url_copy_button"><span>copy</span></a></td>
            </tr>
        </table>
    </div>
    <?php
}

function display_new_booking_quotes_listing($newBookingQuotesAry)
{ 
    $t_base="management/insurance/";
    $kInsurance = new cInsurance(); 
    ?>   
                        
    <form id="send_new_insured_booking" name="send_new_insured_booking" action="javascript:void(0);" method="post">
	<table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="insuranced_booking_table">
            <tr>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/date')?></th> 
                <th style="width:25%;" valign="top"><?=t($t_base.'fields/customer');?></th>
                <th style="width:15%;" valign="top"><?=t($t_base.'fields/shipping_date');?></th>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/trade');?></th> 
                <th style="width:20%;" valign="top"><?=t($t_base.'fields/vol_height');?></th>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/ready');?></th>
            </tr>
	<?php 
		if(!empty($newBookingQuotesAry))
		{
                    $ctr = 1;
                    $irowCount = count($newBookingQuotesAry); 
                    $kConfig = new cConfig();
                    foreach($newBookingQuotesAry as $searchResults)
                    {  
                         $kConfig->loadCountry($searchResults['idOriginCountry']);
                         $szOriginCountryName = $kConfig->szCountryISO ;
                         
                         $kConfig->loadCountry($searchResults['idDestinationCountry']);
                         $szDestinationCountryName = $kConfig->szCountryISO ;
                         
                         $szVolWeight = format_volume($searchResults['fCargoVolume'])."cbm / ".number_format((float)$searchResults['fCargoWeight'])."kg ";
                         
                         $szCustomerName = $searchResults['szFirstName']." ".$searchResults['szLastName'] ;
                         $szCustomerName = returnLimitData($szCustomerName,20) ;
                         
                         $szClass = '';
                         if($searchResults['dtQuoteValidTo']!='0000-00-00 00:00:00' && strtotime($searchResults['dtQuoteValidTo'])<time())
                         {
                             //$szClass = 'class="red_background" ';
                         }
                        ?>				 
                        <tr  id="booking_data_<?=$ctr?>" onclick="select_booking_quote_confirmation_tr('booking_data_<?=$ctr?>','<?php echo $searchResults['id']?>','NEW_INSURED_BOOKING','<?php echo $searchResults['iQuotationReady']; ?>')">
                            <td <?php echo $szClass;?> ><?php echo (!empty($searchResults['dtBookingInitiated']) && $searchResults['dtBookingInitiated']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtBookingInitiated'])):''?></td>
                            <td><?php echo $szCustomerName ; ?></td>
                            <td><?php echo (!empty($searchResults['dtTimingDate']) && $searchResults['dtTimingDate']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtTimingDate'])):''?></td>
                            <td><?php echo $szOriginCountryName."-".$szDestinationCountryName; ?></td> 
                            <td><?php echo $szVolWeight; ?></td>
                            <td><?php echo (($searchResults['iQuotationReady']==1)?'Yes':'No'); ?></td>
                        </tr>
                        <?php
                        $ctr++;
                    }
                    if($irowCount<3)
                    {
                        $loop_counter = 3-$irowCount ;
                        for($i=0;$i<=$loop_counter;$i++)
                        {
                            ?>
                            <tr> 
                                <td>&nbsp;</td>
                                <td>&nbsp;</td> 
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td> 
                            </tr>
                            <?php
                        } 
                    } 
		}
		else
		{
                    ?>
                    <tr>
                        <td colspan="6" align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">&nbsp;</td>
                    </tr>
	<?php }	?>
    </table>  
    </form>
    <br>
    <div>
	<span align="left">
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_edit_button" align="left"><span><?=t($t_base.'fields/edit');?></span></a>
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_send_button"><span><?=t($t_base.'fields/send');?></span></a>
            <a href="javascript:void(0)" class="button1" onclick="display_manual_quote_form('','DISAPLAY_SHIPPING_DATE_POPUP','')"  id="booking_quote_new_button"><span><?=t($t_base.'fields/new');?></span></a>
       </span>
	<span style="float: right;">  
            <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="booking_quote_delete_button"><span><?=t($t_base.'fields/delete');?></span></a>
	</span>
    </div>
<?php  

}

function display_manual_quotes_form($kRegisterShipCon,$idBooking,$szFromPage=false,$iViewOnly=false,$iCreateNewQuote=false)
{
    $t_base="management/insurance/";
    
    $kBooking = new cBooking();
    $kConfig = new cConfig();
    $kRegisterShipCon_new = new cRegisterShipCon(); 
    if(!empty($kRegisterShipCon->arErrorMessages))
    {
        $formId = 'manual_booking_quote_details_form';
        $szValidationErrorKey = '';
        foreach($kRegisterShipCon->arErrorMessages as $errorKey=>$errorValue)
        {
            if(empty($szValidationErrorKey))
            {
                $szValidationErrorKey = $errorKey;
            }
            ?>
            <script type="text/javascript">
                 $("#"+'<?php echo $errorKey ; ?>').addClass('red_border');
            </script>
            <?php
        }
    }
    
    $szReadOnly ='';
    if($iViewOnly==1)
    {
        $szReadOnly = "readonly" ;
    }
    
    $bookingIdAry = array();
    $bookingIdAry[0] = $idBooking ;
    $newInsuredBookingAry = array();
    $newBookingQuotesAry = $kBooking->getAllBookingQuotes(false,$bookingIdAry);
    
    if(!empty($_POST['shipperConsigneeAry']))
    {
        $shipperConsigneeAry = $_POST['shipperConsigneeAry'] ;
        $shipperConsigneeAry['szShipperPhone'] = urldecode($shipperConsigneeAry['szShipperPhone']); 
    } 
    else
    { 
        $shipperConsigneeAry = $kRegisterShipCon_new->getShipperConsigneeDetails($idBooking); 
        if(empty($shipperConsigneeAry) && !$iCreateNewQuote )
        { 
            $origionGeoCountryAry = array();
            $origionGeoCountryAry = reverse_geocode($newBookingQuotesAry[0]['szOriginCountry']); 
            
            $szShipperAddressGoe = trim($origionGeoCountryAry['szStreetNumber']);

            $postcodeCheckAry=array();
            $postcodeResultAry = array();

            $postcodeCheckAry['idCountry'] = $newBookingQuotesAry[0]['idOriginCountry'] ;
            $postcodeCheckAry['szLatitute'] = $origionGeoCountryAry['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $origionGeoCountryAry['szLng'] ;
            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

            if(!empty($postcodeResultAry))
            {
                if(empty($origionGeoCountryAry['szCityName']))
                {
                    $origionGeoCountryAry['szCityName'] = $postcodeResultAry['szCity'];
                }
                if(empty($origionGeoCountryAry['szPostCode']))
                {
                    $origionGeoCountryAry['szPostCode'] = $postcodeResultAry['szPostCode'];
                }
            }
            if(!empty($szShipperAddressGoe) && !empty($origionGeoCountryAry['szRoute']))
            {
                $szShipperAddressGoe = ", ".$origionGeoCountryAry['szRoute'] ;
            }
            else
            {
                $szShipperAddressGoe = $origionGeoCountryAry['szRoute'] ;
            }

            if(!empty($szShipperAddressGoe))
            {
                $shipperConsigneeAry['iGetShipperAddressFromGoogle'] = 1 ;
                $shipperConsigneeAry['szShipperAddress'] = $szShipperAddressGoe ;
            }
            if(!empty($origionGeoCountryAry['szCityName']))
            {
                $shipperConsigneeAry['szShipperCity'] = $origionGeoCountryAry['szCityName'];
                $shipperConsigneeAry['iGetShipperCityFromGoogle'] = 1 ;
            }
            if(!empty($origionGeoCountryAry['szPostCode']))
            {
                $shipperConsigneeAry['szShipperPostCode'] = $origionGeoCountryAry['szPostCode'];	
                $shipperConsigneeAry['iGetShipperPostcodeFromGoogle'] = 1 ;		  
            } 
            $shipperConsigneeAry['szShipperLatitute'] = $origionGeoCountryAry['szLat']; 
            $shipperConsigneeAry['szShipperLongitute'] = $origionGeoCountryAry['szLng'];		
            $shipperConsigneeAry['idShipperDialCode'] = $newBookingQuotesAry[0]['idOriginCountry'];
            $shipperConsigneeAry['idShipperCountry'] = $newBookingQuotesAry[0]['idOriginCountry']; 
               
            $fetchAddressFromGoogle = true ; 

            $destinationGeoCountryAry = array();
            $destinationGeoCountryAry = reverse_geocode($newBookingQuotesAry[0]['szDestinationCountry']);

            $postcodeCheckAry=array();
            $postcodeResultAry = array();

            $postcodeCheckAry['idCountry'] = $newBookingQuotesAry[0]['idDestinationCountry'] ;
            $postcodeCheckAry['szLatitute'] = $destinationGeoCountryAry['szLat'] ;
            $postcodeCheckAry['szLongitute'] = $destinationGeoCountryAry['szLng'] ;
            $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

            if(!empty($postcodeResultAry))
            {
                if(empty($destinationGeoCountryAry['szCityName']))
                {
                    $destinationGeoCountryAry['szCityName'] = trim($postcodeResultAry['szCity']);
                }
                if(empty($destinationGeoCountryAry['szPostCode']))
                {
                    $destinationGeoCountryAry['szPostCode'] = trim($postcodeResultAry['szPostCode']);
                }
            }

            $szShipperAddressGoe = trim($destinationGeoCountryAry['szStreetNumber']);

            if(!empty($szShipperAddressGoe) && !empty($destinationGeoCountryAry['szRoute']))
            {
                $szShipperAddressGoe .= ", ".$destinationGeoCountryAry['szRoute'] ;
            }
            else
            {
                $szShipperAddressGoe .= $destinationGeoCountryAry['szRoute'] ;
            }

            $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 0;
            $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 0;
            $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 0;

            if(!empty($szShipperAddressGoe))
            {
                $shipperConsigneeAry['szConsigneeAddress'] = $szShipperAddressGoe ;
                $shipperConsigneeAry['iGetConsigneeAddressFromGoogle'] = 1 ;
            }
            if(!empty($destinationGeoCountryAry['szPostCode']))
            {
                $shipperConsigneeAry['szConsigneePostCode'] = $destinationGeoCountryAry['szPostCode'];
                $shipperConsigneeAry['iGetConsigneePostcodeFromGoogle'] = 1 ;
            }
            if(!empty($destinationGeoCountryAry['szCityName']))
            {
                $shipperConsigneeAry['szConsigneeCity'] = $destinationGeoCountryAry['szCityName']; 
                $shipperConsigneeAry['iGetConsigneeCityFromGoogle'] = 1 ;
            }  

            $shipperConsigneeAry['szConsigneeLatitute'] = $destinationGeoCountryAry['szLat']; 
            $shipperConsigneeAry['szConsigneeLongitute'] = $destinationGeoCountryAry['szLng']; 

            $shipperConsigneeAry['idConsigneeDialCode'] = $newBookingQuotesAry[0]['idDestinationCountry']; 
            $shipperConsigneeAry['idConsigneeCountry'] = $newBookingQuotesAry[0]['idDestinationCountry']; 
        }
        $cargoDetailsAry = $kBooking->getCargoComodityDeailsByBookingId($idBooking);
        $shipperConsigneeAry['szShipperPostcode'] = $shipperConsigneeAry['szShipperPostCode'] ;
        $shipperConsigneeAry['szConsigneePostcode'] = $shipperConsigneeAry['szConsigneePostCode'] ;
        
        $shipperConsigneeAry['iInsuranceMandatory'] = $newBookingQuotesAry[0]['iInsuranceMandatory'];
        $shipperConsigneeAry['szBillingCompanyName'] = $newBookingQuotesAry[0]['szCustomerCompanyName'];
        $shipperConsigneeAry['szBillingFirstName'] = $newBookingQuotesAry[0]['szFirstName'];
        $shipperConsigneeAry['szBillingLastName'] = $newBookingQuotesAry[0]['szLastName'];
        $shipperConsigneeAry['szBillingEmail'] = $newBookingQuotesAry[0]['szEmail'];
        $shipperConsigneeAry['idBillingDialCode'] = $newBookingQuotesAry[0]['idCustomerDialCode'];
        $shipperConsigneeAry['szBillingPhone'] = $newBookingQuotesAry[0]['szCustomerPhoneNumber'];
        $shipperConsigneeAry['szBillingAddress'] = $newBookingQuotesAry[0]['szCustomerAddress1'];
        $shipperConsigneeAry['szBillingPostcode'] = $newBookingQuotesAry[0]['szCustomerPostCode'];
        $shipperConsigneeAry['szBillingCity'] = $newBookingQuotesAry[0]['szCustomerCity'];
        $shipperConsigneeAry['idBillingCountry'] = $newBookingQuotesAry[0]['szCustomerCountry'];
        $shipperConsigneeAry['fCargoVolume'] = format_volume($newBookingQuotesAry[0]['fCargoVolume']);
        $shipperConsigneeAry['fCargoWeight'] = round($newBookingQuotesAry[0]['fCargoWeight']);
        $shipperConsigneeAry['idServiceType'] = $newBookingQuotesAry[0]['idServiceType'];
        $shipperConsigneeAry['idCustomerCurrency'] = $newBookingQuotesAry[0]['idCustomerCurrency'];
        $shipperConsigneeAry['iBookingLanguage'] = $newBookingQuotesAry[0]['iBookingLanguage'];
        
        if(empty($shipperConsigneeAry['iShipperConsignee']))
        {
            $shipperConsigneeAry['iShipperConsignee'] = 3;
        }
        
        /*
        if($shipperConsigneeAry['iShipperConsignee']==1)
        {
            $shipperConsigneeAry['iShipperFlag'] = 1;
        }
        else if($shipperConsigneeAry['iShipperConsignee']==2)
        {
            $shipperConsigneeAry['iConsigneeFlag'] = 1;
        }*/
        
        if(empty($shipperConsigneeAry['szShipperHeading']))
        {
            $shipperConsigneeAry['szShipperHeading'] = 'Shipper';
        }
        if(empty($shipperConsigneeAry['szConsigneeHeading']))
        {
            $shipperConsigneeAry['szConsigneeHeading'] = 'Consignee';
        } 
        
        $shipperConsigneeAry['szServiceDescription'] = $newBookingQuotesAry[0]['szServiceDescription'];
        
        $shipperConsigneeAry['szServiceDescription'] = $newBookingQuotesAry[0]['szServiceDescription'];
        $shipperConsigneeAry['idServiceType'] = $newBookingQuotesAry[0]['idServiceType'];
        $shipperConsigneeAry['idForwarder'] = $newBookingQuotesAry[0]['idForwarder'];
        $shipperConsigneeAry['dtQuotesValidTo'] = $newBookingQuotesAry[0]['dtQuotesValidTo'];
        $shipperConsigneeAry['idTransportMode'] = $newBookingQuotesAry[0]['idTransportMode'];
        $shipperConsigneeAry['iTransitHours'] = $newBookingQuotesAry[0]['iTransitHours'];
        
        $shipperConsigneeAry['szFrequency'] = $newBookingQuotesAry[0]['szFrequency'];
        $shipperConsigneeAry['idCustomerCurrency'] = $newBookingQuotesAry[0]['idCustomerCurrency'];
        $shipperConsigneeAry['fBookingAmount'] = (int)$newBookingQuotesAry[0]['fBookingAmount'];
        $shipperConsigneeAry['fReferalFee'] = round((float)$newBookingQuotesAry[0]['fReferalFee'],4);
        $shipperConsigneeAry['fTotalVat'] = $newBookingQuotesAry[0]['fTotalVat'];
        
        $shipperConsigneeAry['fInsuranceSellPrice'] = round((float)$newBookingQuotesAry[0]['fInsuranceSellPrice'],2);
        $shipperConsigneeAry['fInsuranceBuyPrice'] = $newBookingQuotesAry[0]['fInsuranceBuyPrice'];
        $shipperConsigneeAry['fInsuranceValue'] = (int)$newBookingQuotesAry[0]['fInsuranceValue'];
        $shipperConsigneeAry['fReferalFee'] = $newBookingQuotesAry[0]['fReferalFee'];
        $shipperConsigneeAry['szOtherComments'] = $newBookingQuotesAry[0]['szOtherComments'];
        $shipperConsigneeAry['idGoodsInsuranceCurrency'] = $newBookingQuotesAry[0]['idGoodsInsuranceCurrency'];
        
        if(empty($shipperConsigneeAry['idShipperDialCode']))
        {
            $shipperConsigneeAry['idShipperDialCode'] = $newBookingQuotesAry[0]['idOriginCountry'];
        }
        if(empty($shipperConsigneeAry['idConsigneeDialCode']))
        {
            $shipperConsigneeAry['idConsigneeDialCode'] = $newBookingQuotesAry[0]['idDestinationCountry'];
        }
        if(empty($shipperConsigneeAry['idBillingDialCode']))
        {
            $shipperConsigneeAry['idBillingDialCode'] = $newBookingQuotesAry[0]['idOriginCountry'];
        }
        
        if(empty($shipperConsigneeAry['idShipperCountry']))
        {
            $shipperConsigneeAry['idShipperCountry'] = $newBookingQuotesAry[0]['idOriginCountry'];
        }
        if(empty($shipperConsigneeAry['idConsigneeCountry']))
        {
            $shipperConsigneeAry['idConsigneeCountry'] = $newBookingQuotesAry[0]['idDestinationCountry'];
        }
        if(empty($shipperConsigneeAry['idBillingCountry']))
        {
            $shipperConsigneeAry['idBillingCountry'] = $newBookingQuotesAry[0]['idOriginCountry'];
        }
        
        if(!empty($cargoDetailsAry))
        {
            $shipperConsigneeAry['szCargoCommodity'] = utf8_decode($cargoDetailsAry['1']['szCommodity']);
            $shipperConsigneeAry['idCargo'] = $cargoDetailsAry['1']['id'] ;
        }  
        if(!empty($shipperConsigneeAry['dtQuotesValidTo']) && $shipperConsigneeAry['dtQuotesValidTo']!='0000-00-00 00:00:00')
        {
            $shipperConsigneeAry['dtQuotesValidTo'] = date('d/m/Y',strtotime($shipperConsigneeAry['dtQuotesValidTo']));
        }
        else
        {
            $shipperConsigneeAry['dtQuotesValidTo'] = date('d/m/Y',strtotime("+1 MONTH"));
        }
    }  
    if($shipperConsigneeAry['iShipperConsignee']==1 || $shipperConsigneeAry['iShipperConsignee']==2)
    {
        $szBillingReadOnly = "readonly style='background-color:#D3D3D3;'" ; 
        $szBillingPhoneReadOnly = "readonly style='background-color:#D3D3D3;width:120px;'" ;
    }
    else
    {
        $szBillingPhoneReadOnly = "style='width:120px;'" ;
    }
    
    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true,$iLanguage);

    $dialUpCodeAry = array();
    $dialUpCodeAry = $kConfig->getAllCountries(true,$iLanguage,true);
      
    $kConfig->loadCountry($newBookingQuotesAry[0]['idOriginCountry']);
    
    $szOriginCountryName = $kConfig->szCountryName ;
    $kConfig->loadCountry($newBookingQuotesAry[0]['idDestinationCountry']);
    $szDestinationCountryName = $kConfig->szCountryName ; 
    $kForwarder = new cForwarder();
    // geting all service type 
    $serviceTypeAry = array();
    $serviceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
    
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(true); 
    
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false);
    
    $currencyAry = array();
    $currencyAry = $kConfig->getBookingCurrency(false,true);
    
    if(isset($_POST['shipperConsigneeAry']['iCreateNewQuote']))
    {
        $iCreateNewQuote = $_POST['shipperConsigneeAry']['iCreateNewQuote'] ;
    } 
    $kReferralPricing = new cReferralPricing();
    $transportProductAry = array();
    $transportProductAry = $kConfig->getAllTransportMode();
   ?>
    <script>
     var referalFeeAry = new Array();
     <?php
        if(!empty($forwarderListAry))
        {
            foreach($forwarderListAry as $forwarderListArys)
            {
                if($forwarderListArys['id']>0)
                {
                    $idForwarder = $forwarderListArys['id'];
                   ?>
                    referalFeeAry[<?php echo $forwarderListArys['id']; ?>] = new Array(); 
                   <?php
                    if(!empty($transportProductAry))
                    {
                        $idForwarder = $forwarderListArys['id'];
                        
                        $forwarderReferralFeeAry = array();
                        $forwarderReferralFeeAry = $kReferralPricing->getAllReferralFeeByForwarder($idForwarder);
                       
                        foreach($transportProductAry as $transportModeArys)
                        {
                            $fReferralFee = (float)$forwarderReferralFeeAry[$transportModeArys['id']]['fReferralFee']; 
                            ?>
                            referalFeeAry['<?php echo $idForwarder ?>']['<?php echo $transportModeArys['id']; ?>'] = '<?php echo $fReferralFee; ?>'
                            <?php 
                        } 
                    }
                }
            }
        }
        $countryListingAry = array();
        $countryListingAry = sortArray($dialUpCodeAry,'szCountryName');
        
        $kConfig=new cConfig();
        $currencyAry = array();
        $currencyAry = $kConfig->getBookingCurrency(false,true);
        
        $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".(date('d')+1).")" ;
     ?>
    function prefill_referal_fee(fwd_id)
    {
       var fReferalFee = referalFeeAry[fwd_id];
       $("#fReferalFee").val(fReferalFee);
    } 
    $().ready(function() {	
        $("#dtQuotesValidTo").datepicker({<?php echo $date_picker_argument; ?>}); 
    });
    </script> 
    <form id="manual_booking_quote_details_form" name="manual_booking_quote_details_form" action="" method="post">
        <?php if($iCreateNewQuote){ ?>
            <h4><?php echo "Create new quote"; ?></h4>
        <?php } else {?>
            <h4><?php echo t($t_base.'title/edit_manaul_quote')." ".$szOriginCountryName." ".t($t_base.'title/to')." ".$szDestinationCountryName; ?></h4>
        <?php }?>
        <table cellpadding="5" cellspacing="0" class="format-3" width="100%" id="insuranced_booking_table">
           <tr>
                <td style="width:10%;">Heading</td>
                <td style="width:30%;"><?php echo t($t_base.'title/shipper') ?></td>
                <td style="width:30%;"><?php echo t($t_base.'title/consignee') ?></td>
<!--                <td style="width:30%;"><input type="hidden" <?php echo $szReadOnly; ?> tabindex="1" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperHeading]" id="szShipperHeading" value="<?=$shipperConsigneeAry['szShipperHeading']?>"/>&nbsp;<input tabindex="2" name="shipperConsigneeAry[iShipperFlag]" id="iShipperFlag" onclick="autofill_billing_address(this.id,1);" <?php if($shipperConsigneeAry['iShipperConsignee']==1 || $shipperConsigneeAry['iShipperFlag']==1){?>checked<?php } ?> type="checkbox" value="1"></td>
                <td style="width:30%;"><input type="hidden" <?php echo $szReadOnly; ?> tabindex="3" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeHeading]" id="szConsigneeHeading" value="<?=$shipperConsigneeAry['szConsigneeHeading']?>"/>&nbsp;<input tabindex="4" name="shipperConsigneeAry[iConsigneeFlag]" id="iConsigneeFlag" onclick="autofill_billing_address(this.id,2)" <?php if($shipperConsigneeAry['iShipperConsignee']==2 || $shipperConsigneeAry['iConsigneeFlag']==1){?>checked<?php } ?> type="checkbox" value="1"></td>-->
                <td style="width:30%;">
                    Billing
                    <select size="1" name="shipperConsigneeAry[iShipperConsignee]" id="iShipperConsignee" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="autofill_billing_address(this.value);">
                        <option value="1" <?php if($shipperConsigneeAry['iShipperConsignee']==1){?> selected <?php }?>>Shipper</option>
                        <option value="2" <?php if($shipperConsigneeAry['iShipperConsignee']==2){?> selected <?php }?>>Consignee</option>
                        <option value="3" <?php if($shipperConsigneeAry['iShipperConsignee']==3){?> selected <?php }?>>Billing</option>
		   </select>
                </td>
            </tr> 
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/company_name');?></span></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?>  tabindex="9" onblur="autofill_billing_address('iShipperFlag',1);" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperCompanyName]" id="szShipperCompanyName" value="<?=$shipperConsigneeAry['szShipperCompanyName']?>"/></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?>  tabindex="18" onblur="autofill_billing_address('iConsigneeFlag',2);" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeCompanyName]" id="szConsigneeCompanyName" value="<?php echo $shipperConsigneeAry['szConsigneeCompanyName']?>" ></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>  tabindex="28" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingCompanyName]" id="szBillingCompanyName" value="<?=$shipperConsigneeAry['szBillingCompanyName']?>"/></td>
            </tr>
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/first_name');?></span></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?>  tabindex="10" onblur="autofill_billing_address('iShipperFlag',1);" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperFirstName]" id="szShipperFirstName" value="<?=$shipperConsigneeAry['szShipperFirstName']?>"/></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?>  tabindex="19" onblur="autofill_billing_address('iConsigneeFlag',2);" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeFirstName]" id="szConsigneeFirstName" value="<?php echo $shipperConsigneeAry['szConsigneeFirstName']?>" ></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>   tabindex="29" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingFirstName]" id="szBillingFirstName" value="<?=$shipperConsigneeAry['szBillingFirstName']?>"/></td>
            </tr>
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/last_name');?></span></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> tabindex="11" onblur="autofill_billing_address('iShipperFlag',1);" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperLastName]" id="szShipperLastName" value="<?=$shipperConsigneeAry['szShipperLastName']?>"/></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?>  tabindex="20" onblur="autofill_billing_address('iConsigneeFlag',2);" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeLastName]" id="szConsigneeLastName" value="<?php echo $shipperConsigneeAry['szConsigneeLastName']?>" ></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>  tabindex="30" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingLastName]" id="szBillingLastName" value="<?=$shipperConsigneeAry['szBillingLastName']?>"/></td>
            </tr>
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/email');?></span></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?>  tabindex="12" onblur="autofill_billing_address('iShipperFlag',1);" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperEmail]" id="szShipperEmail" value="<?=$shipperConsigneeAry['szShipperEmail']?>"/></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?>  tabindex="21" onblur="autofill_billing_address('iConsigneeFlag',2);"  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeEmail]" id="szConsigneeEmail" value="<?php echo $shipperConsigneeAry['szConsigneeEmail']?>" ></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>   tabindex="31" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingEmail]" id="szBillingEmail" value="<?=$shipperConsigneeAry['szBillingEmail']?>"/></td>
            </tr>
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/phone_number');?></span></td>
                <td style="width:30%;">
                    <select size="1"  tabindex="13" <?php echo $szReadOnly; ?>  name="shipperConsigneeAry[idShipperDialCode]" id="idShipperDialCode" onblur="autofill_billing_address('iShipperFlag',1);" onfocus="check_form_field_not_required(this.form.id,this.id);">
			<?php
                            if(!empty($dialUpCodeAry))
                            { 
                                $usedDialCode = array();
                                foreach($dialUpCodeAry as $dialUpCodeArys)
                                {
                                    if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                    {
                                        $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ; 
                                        ?>
                                        <option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$shipperConsigneeAry['idShipperDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                        <?php
                                    }
                                }
                            }
			?>
		   </select>
		   <input type="text" tabindex="14" onblur="autofill_billing_address('iShipperFlag',1);" style="width:120px;" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperPhone]" id="szShipperPhone" value="<?=$shipperConsigneeAry['szShipperPhone']?>"/>
                </td>
                <td style="width:30%;">
                    <select size="1"  tabindex="22" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iConsigneeFlag',2);"   name="shipperConsigneeAry[idConsigneeDialCode]" id="idConsigneeDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);">
			<?php
                            if(!empty($dialUpCodeAry))
                            {
                                 $usedDialCode = array();
                                foreach($dialUpCodeAry as $dialUpCodeArys)
                                { 
                                    if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                    {
                                        $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ; 
                                        ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$shipperConsigneeAry['idConsigneeDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                        <?php
                                    }
                                }
                            }
			?>
		   </select>
		   <input type="text" tabindex="23" <?php echo $szReadOnly; ?>  onblur="autofill_billing_address('iConsigneeFlag',2);"  style="width:120px;" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneePhone]" id="szConsigneePhone" value="<?php echo $shipperConsigneeAry['szConsigneePhone']?>" >
                </td>
                <td style="width:30%;">
                    <select size="1"  tabindex="32" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>   name="shipperConsigneeAry[idBillingDialCode]" id="idBillingDialCode" onfocus="check_form_field_not_required(this.form.id,this.id);" >
			<?php
                            if(!empty($dialUpCodeAry))
                            {
                                $usedDialCode = array();
                                foreach($dialUpCodeAry as $dialUpCodeArys)
                                {
                                    if(empty($usedDialCode) || !in_array($dialUpCodeArys['iInternationDialCode'],$usedDialCode))
                                    {
                                        $usedDialCode[] = $dialUpCodeArys['iInternationDialCode'] ; 
                                        ?><option value="<?=$dialUpCodeArys['id']?>" <?php if($dialUpCodeArys['id']==$shipperConsigneeAry['idBillingDialCode']){?> selected <?php }?>>+<?=$dialUpCodeArys['iInternationDialCode']?></option>
                                        <?php
                                    }
                                }
                            }
			?>
		   </select>
		   <input type="text" tabindex="33" <?php echo $szReadOnly; ?> <?php echo $szBillingPhoneReadOnly; ?> class="wd120" onfocus="check_form_field_not_required(this.form.id,this.id);"  name="shipperConsigneeAry[szBillingPhone]" id="szBillingPhone" value="<?=$shipperConsigneeAry['szBillingPhone']?>"/>
                </td>
            </tr> 
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/address');?></span></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iShipperFlag',1);"   tabindex="15" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperAddress]" id="szShipperAddress1" value="<?=$shipperConsigneeAry['szShipperAddress']?>"/></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iConsigneeFlag',2);"  tabindex="24" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeAddress]" id="szConsigneeAddress1" value="<?php echo $shipperConsigneeAry['szConsigneeAddress']; ?>" ></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>   tabindex="34" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingAddress]" id="szBillingAddress1" value="<?=$shipperConsigneeAry['szBillingAddress']?>"/></td>
            </tr>
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/postcode');?></span></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iShipperFlag',1);"   tabindex="16" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperPostcode]" id="szShipperPostcode"  value="<?php echo $shipperConsigneeAry['szShipperPostcode']; ?>"/></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iConsigneeFlag',2);"  tabindex="25" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneePostcode]" id="szConsigneePostcode" value="<?php echo $shipperConsigneeAry['szConsigneePostcode']; ?>" ></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>   tabindex="35" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingPostcode]" id="szBillingPostcode"  value="<?php echo (!empty($shipperConsigneeAry['szBillingPostCode'])?$shipperConsigneeAry['szBillingPostCode']:$shipperConsigneeAry['szBillingPostcode']); ?>"/></td>
            </tr>
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/city');?></span></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iShipperFlag',1);"  tabindex="17" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szShipperCity]" id="szShipperCity" value="<?php echo $shipperConsigneeAry['szShipperCity'];?>"/></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iConsigneeFlag',2);"  tabindex="26" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szConsigneeCity]" id="szConsigneeCity" value="<?php echo $shipperConsigneeAry['szConsigneeCity']; ?>" ></td>
                <td style="width:30%;"><input type="text" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>   tabindex="36" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szBillingCity]" id="szBillingCity" value="<?php echo $shipperConsigneeAry['szBillingCity'];?>"/></td>
            </tr>
            <tr>
                <td style="width:10%;"><span><?=t($t_base.'fields/country');?></span></td>
                <td style="width:30%;">
                     <select size="1"  tabindex="16" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iShipperFlag',1);" name="shipperConsigneeAry[idShipperCountry]" id="idShipperCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" >
			<?php 
                            if(!empty($countryListingAry))
                            { 
                                foreach($countryListingAry as $countryListingArys)
                                {
                                    ?><option value="<?=$countryListingArys['id']?>" <?php if($countryListingArys['id']==$shipperConsigneeAry['idShipperCountry']){?> selected <?php }?>><?=$countryListingArys['szCountryName']?></option>
                                    <?php
                                }
                            }
			?>
		   </select>
                </td>
                <td style="width:30%;">
                    <select size="1"  tabindex="16" <?php echo $szReadOnly; ?> onblur="autofill_billing_address('iConsigneeFlag',2);"  name="shipperConsigneeAry[idConsigneeCountry]" id="idConsigneeCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" >
			<?php 
                            if(!empty($countryListingAry))
                            { 
                                foreach($countryListingAry as $countryListingArys)
                                {
                                    ?><option value="<?=$countryListingArys['id']?>" <?php if($countryListingArys['id']==$shipperConsigneeAry['idConsigneeCountry']){?> selected <?php }?>><?=$countryListingArys['szCountryName']?></option>
                                    <?php
                                }
                            }
			?>
		   </select>
                </td>
                <td style="width:30%;">
                    <select size="1"  tabindex="16" <?php echo $szReadOnly; ?> <?php echo $szBillingReadOnly; ?>   name="shipperConsigneeAry[idBillingCountry]" id="idBillingCountry" onfocus="check_form_field_not_required(this.form.id,this.id);" >
			<?php 
                            if(!empty($countryListingAry))
                            { 
                                foreach($countryListingAry as $countryListingArys)
                                {
                                    ?><option value="<?=$countryListingArys['id']?>" <?php if($countryListingArys['id']==$shipperConsigneeAry['idBillingCountry']){?> selected <?php }?>><?=$countryListingArys['szCountryName']?></option>
                                    <?php
                                }
                            }
			?>
		   </select>
                </td>
            </tr>
        </table>
        <br>
        <table cellpadding="5" cellspacing="0" class="format-3" width="100%" id="insuranced_booking_table">
            <tr>                
                <td>Volume</td>
                <td><input type="text" tabindex="37" <?php echo $szReadOnly; ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fCargoVolume]" id="fCargoVolume" value="<?php echo $shipperConsigneeAry['fCargoVolume'];?>"/></td>
                <td>Weight</td>
                <td><input type="text" tabindex="38" <?php echo $szReadOnly; ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fCargoWeight]" id="fCargoWeight" value="<?php echo $shipperConsigneeAry['fCargoWeight'];?>"/></td>
                <td>Cargo description</td>
                <td>
                    <input type="text" tabindex="39" <?php echo $szReadOnly; ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szCargoCommodity]" id="fCargoWeight_landing_page" value="<?php echo $shipperConsigneeAry['szCargoCommodity'];?>"/>
                    <input type="hidden" name="shipperConsigneeAry[idCargo]" id="idCargo" value="<?php echo $shipperConsigneeAry['idCargo'];?>"/>
                    
                </td>
            </tr>
            <tr>
                <td colspan="6">Service description (for overview screen, invoice, booking confirmation and forwarder booking)</td>
            </tr>
            <tr>
                <td colspan="4"><input type="text" <?php echo $szReadOnly; ?>  style="width:440px;" tabindex="40" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szServiceDescription]" id="szServiceDescription" value="<?php echo $shipperConsigneeAry['szServiceDescription'];?>"/></td>
                <td>Type</td>
                <td>
                    <select name="shipperConsigneeAry[idServiceType]" <?php echo $szReadOnly; ?>  style="min-width:100px;" tabindex="41" id="idServiceType" onfocus="check_form_field_not_required(this.form.id,this.id);">
                        <?php
                            if(!empty($serviceTypeAry))
                            {
                               foreach($serviceTypeAry as $serviceTypeArys)
                               {
                                ?>
                                <option value="<?php echo $serviceTypeArys['id']; ?>" <?php echo (($serviceTypeArys['id']==$shipperConsigneeAry['idServiceType'])?'selected':''); ?>><?php echo $serviceTypeArys['szShortName']; ?></option>
                                <?php
                               }
                            }
                        ?> 
                    </select>
                </td>
            </tr>
            <tr>
                <td>Forwarder</td>
                <td>
                    <select name="shipperConsigneeAry[idForwarder]" <?php echo $szReadOnly; ?>  style="min-width:140px;" tabindex="42" id="idForwarder" onfocus="check_form_field_not_required(this.form.id,this.id);" onchange="prefill_referal_fee(this.value)">
                        <option value="">Select</option>
                        <?php
                            if(!empty($forwarderListAry))
                            {
                               foreach($forwarderListAry as $forwarderListArys)
                               {
                                ?>
                                <option value="<?php echo $forwarderListArys['id']; ?>" <?php echo (($forwarderListArys['id']==$shipperConsigneeAry['idForwarder'])?'selected':''); ?>><?php echo $forwarderListArys['szDisplayName']; ?></option>
                                <?php
                               }
                            }
                        ?> 
                    </select>
                </td>
                <td>Valid To</td>
                <td><input type="text" tabindex="43" <?php echo $szReadOnly; ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[dtQuotesValidTo]" id="dtQuotesValidTo" value="<?php echo $shipperConsigneeAry['dtQuotesValidTo']; ?>"/></td>
                <td>Mode</td>
                <td>
                    <select name="shipperConsigneeAry[idTransportMode]" <?php echo $szReadOnly; ?>  style="min-width:100px;" tabindex="44" id="idTransportMode" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);">
                        <option value="">Select</option>
                        <?php
                            if(!empty($transportModeListAry))
                            {
                               foreach($transportModeListAry as $transportModeListArys)
                               {
                                ?>
                                <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($transportModeListArys['id']==$shipperConsigneeAry['idTransportMode'])?'selected':''); ?>><?php echo $transportModeListArys['szShortName']; ?></option>
                                <?php
                               }
                            }
                        ?> 
                    </select>
                </td> 
            </tr> 
            <tr>
                <td>Transit time</td>
                <td><input type="text" tabindex="45" <?php echo $szReadOnly; ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[iTransitHours]" id="iTransitHours" value="<?php echo $shipperConsigneeAry['iTransitHours']; ?>"/></td>
                <td>Frequency</td>
                <td><input type="text" tabindex="46" <?php echo $szReadOnly; ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[szFrequency]" id="szFrequency" value="<?php echo $shipperConsigneeAry['szFrequency']; ?>"/></td>
                <td colspan="2">&nbsp;</td> 
            </tr>
            <tr>
                <td></td>
                <td colspan="2" style="text-align:center;padding:4px 20px;">All-In</td>
                <td>Referral fee</td>
                <td>VAT</td>
                <td >&nbsp;</td> 
            </tr>
            <tr>
                <td>Transportation quote</td>
                <td>
                    <select name="shipperConsigneeAry[idCustomerCurrency]" <?php echo $szReadOnly; ?>  style="min-width:100px;" tabindex="47" id="idCustomerCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);" >
                        <option value="">Select</option>
                        <?php
                            if(!empty($currencyAry))
                            {
                                foreach($currencyAry as $currencyArys)
                                {
                                    ?> 
                                    <option value="<?=$currencyArys['id']?>" <?php echo (($shipperConsigneeAry['idCustomerCurrency']==$currencyArys['id'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                    <?php 
                                }
                            }
                         ?> 
                    </select>
                </td> 
                <td><input type="text" style="width:50px;"  <?php echo $szReadOnly; ?> tabindex="48" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fBookingAmount]" id="fBookingAmount" value="<?php echo $shipperConsigneeAry['fBookingAmount']; ?>"/></td>
                <td><input type="text" style="width:140px;" <?php echo $szReadOnly; ?>  tabindex="49" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fReferalFee]" id="fReferalFee" value="<?php echo $shipperConsigneeAry['fReferalFee']; ?>"/>%</td>
                <td colspan="2"><input type="text" style="width:150px;" <?php echo $szReadOnly; ?>  tabindex="50" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fTotalVat]" id="fTotalVat" value="<?php echo $shipperConsigneeAry['fTotalVat']; ?>"/> </td> 
            </tr> 
            <tr>
                <td>Insurance</td> 
                <td>Sell<br>  
                    <input type="text" onkeyup="update_mandatory_field(this.value)" style="width:50px;" <?php echo $szReadOnly; ?>  tabindex="51" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fInsuranceSellPrice]" id="fInsuranceSellPrice" value="<?php echo $shipperConsigneeAry['fInsuranceSellPrice']; ?>"/>
                    <input type="checkbox" name="shipperConsigneeAry[iInsuranceMandatory]" id="iInsuranceMandatory" value="1" <?php echo (($shipperConsigneeAry['iInsuranceMandatory']==1)?'checked':''); ?> > <span>Mandatory</span>
                </td>
                <td>Buy<br>
                    <input type="text" style="width:50px;" <?php echo $szReadOnly; ?>  tabindex="52" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fInsuranceBuyPrice]" id="fInsuranceBuyPrice" value="<?php echo $shipperConsigneeAry['fInsuranceBuyPrice']; ?>"/></td>
                <td>Value<br>
                    <select name="shipperConsigneeAry[idInsuranceCurrency]" id="idInsuranceCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);">
                            <?php
                                if(!empty($currencyAry))
                                {
                                    foreach($currencyAry as $currencyArys)
                                    {
                                        ?>
                                        <option value="<?php echo $currencyArys['id']?>" <?php echo (($currencyArys['id']==$shipperConsigneeAry['idGoodsInsuranceCurrency'])?'selected':''); ?>><?=$currencyArys['szCurrency']?></option>
                                        <?php
                                    }
                                }
                             ?>
                    </select>
                    <input type="text" style="width:50px;" <?php echo $szReadOnly; ?>  tabindex="53" onfocus="check_form_field_not_required(this.form.id,this.id);" name="shipperConsigneeAry[fInsuranceValue]" id="fInsuranceValue" value="<?php echo $shipperConsigneeAry['fInsuranceValue']; ?>"/></td> 
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6">Other comments for customer<br>
                    <textarea cols="80" rows="3" tabindex="54" <?php echo $szReadOnly; ?>  name="shipperConsigneeAry[szOtherComments]" id="szOtherComments"><?php echo trim($shipperConsigneeAry['szOtherComments']); ?></textarea>
                </td> 
            </tr>
        </table>
        <input type="hidden" name="shipperConsigneeAry[idBooking]" id="idBooking" value="<?php echo $idBooking; ?>">
        <input type="hidden" name="shipperConsigneeAry[szFromPage]" id="szFromPage" value="<?php echo $szFromPage; ?>">
        <input type="hidden" name="shipperConsigneeAry[iCreateNewQuote]" id="iCreateNewQuote" value="<?php echo $iCreateNewQuote; ?>">
        
        <input type="hidden" name="shipperConsigneeAry[iBookingLanguage]" id="iBookingLanguage" value="<?php echo $shipperConsigneeAry['iBookingLanguage']; ?>">
        
        
        
        <div class="btn-container" style="padding:20px;text-align: center"> 
            <?php if($iViewOnly==1){ ?>
                <a href="javascript:void(0)" class="button1" id="booking_quote_edit_button" onclick="display_manual_quote_form('<?php echo $idBooking; ?>','BACK_TO_MANUAL_QUEOTS_LISTING','<?php echo $szFromPage; ?>')" align="left"><span><?=t($t_base.'fields/back');?></span></a>
            <?php } else {?>
                <a href="javascript:void(0)" class="button1" id="booking_quote_edit_button" onclick="submit_manual_quote_form();" align="left"><span><?=t($t_base.'fields/save');?></span></a>
                <a href="javascript:void(0)" class="button1" onclick="display_manual_quote_listings('<?php echo $szFromPage; ?>');"><span><?=t($t_base.'fields/cancel');?></span></a>
            <?php }?>
        </div>
    </form>
    <?php
}

function display_testimonial_form($kExplain,$i,$testimonialAry=array())
{ 
    if($i==1)
    {
        $szTitleText = 'K';
    }
    else
    {
        $szTitleText = '';
    }
    ?> 
    <input type="hidden" name="landingPageArr[idTestimonial][<?php echo $i; ?>]" id="idTestimonial_<?php echo $i; ?>" value="<?php echo $testimonialAry[$i]['id']; ?>">
    <p class="profile-fields">
        <span class="field-name"><?php echo $szTitleText; ?></span>
        <span class="field-container">					 	
            <span class="field-title">Testimonial <?php echo $i; ?> image URL<br></span>
            <input type="text" id="szImageURL_<?php echo $i; ?>" name="landingPageArr[szImageURL][<?php echo $i; ?>]" value="<?php if(isset($testimonialAry[$i]['szImageURL'])){ echo $testimonialAry[$i]['szImageURL']; }?>">
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"></span>
        <span class="field-container">					 	
            <span class="field-title">Testimonial <?php echo $i; ?> image description (150-160 characters)<span style="float:right;" id="szImageDesc_<?php echo $i; ?>_span">(0)</span><br></span>
            <textarea id="szImageDesc_<?php echo $i; ?>" onkeypress="limit_characters(this.id,160);" maxlength="160" name="landingPageArr[szImageDesc][<?php echo $i; ?>]"><?php if(isset($testimonialAry[$i]['szImageDesc'])){ echo $testimonialAry[$i]['szImageDesc']; } ?></textarea>
        </span>
    </p>						
    <p class="profile-fields">
        <span class="field-name"></span>
        <span class="field-container">					 	
            <span class="field-title">Testimonial <?php echo $i; ?> image title (48 characters)<span style="float:right;" id="szImageTitle_<?php echo $i; ?>_span">(0)</span><br></span>
            <input type="text" id="szImageTitle_<?php echo $i; ?>" onkeypress="limit_characters(this.id,48);" maxlength="48" name="landingPageArr[szImageTitle][<?php echo $i; ?>]" value="<?php if(isset($testimonialAry[$i]['szImageTitle'])){ echo $testimonialAry[$i]['szImageTitle']; }?>">
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"></span>
        <span class="field-container">					 	
            <span class="field-title">Testimonial  <?php echo $i; ?> heading<br></span>
            <input type="text" id="szHeading_<?php echo $i; ?>" name="landingPageArr[szHeading][<?php echo $i; ?>]" value="<?php if(isset($testimonialAry[$i]['szHeading'])){ echo $testimonialAry[$i]['szHeading']; }?>">
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"></span>
        <span class="field-container">					 	
            <span class="field-title">Testimonial <?php echo $i; ?> text<br></span>
            <textarea id="szTextDesc_<?php echo $i; ?>" name="landingPageArr[szTextDesc][<?php echo $i; ?>]"><?php if(isset($testimonialAry[$i]['szTextDesc'])){ echo $testimonialAry[$i]['szTextDesc']; } ?></textarea>
        </span>
    </p>
    <p class="profile-fields">
        <span class="field-name"></span>
        <span class="field-container">					 	
            <span class="field-title">Testimonial  <?php echo $i; ?> name and title<br></span>
            <input type="text" id="szTitleName_<?php echo $i; ?>" name="landingPageArr[szTitleName][<?php echo $i; ?>]" value="<?php if(isset($testimonialAry[$i]['szTitleName'])){ echo $testimonialAry[$i]['szTitleName']; }?>">
        </span>
    </p>
    <br>
    <?php
}
function display_send_quotes_popup($idBooking,$szFromPage=false)
{
    $t_base="management/insurance/";
  
    $kAdmin = new cAdmin();
    $idAdmin = $_SESSION['admin_id']; 
    $kAdmin->getAdminDetails($idAdmin);
    $szAdminEmail = $kAdmin->szEmail ;
	$kConfig =new cConfig();
    $langArr=$kConfig->getLanguageDetails();
 ?>
<div id="popup-bg"></div>
<div id="popup-container" >
    <div class="popup"> 
        <h2><?php echo t($t_base.'fields/send_quote');?></h2>
        <p><?php echo t($t_base.'fields/send_quotes_details');?>:</p>
        <form id="send_manual_quotes_popup_form" name="send_manual_quotes_popup_form" action="javascript:void(0);">
            <p><input type="text" name="sendQuotesAry[szUserEmail]" onfocus="check_form_field_not_required(this.form.id,this.id);" id="szUserEmail" value="<?php echo $szAdminEmail; ?>" size="30"></p><br/> 
            <p> <?php echo t($t_base.'fields/language');?>: 
                <select name="sendQuotesAry[iLanguage]" id="iLanguage">
                    <?php
                    if(!empty($langArr))
                    {
                        foreach($langArr as $langArrs)
                        {?>
                            <option value="<?php echo $langArrs['id']; ?>"><?php echo ucfirst($langArrs['szName']); ?></option>
                          <?php  
                        }
                    }?>                 
                </select>
            </p>
            <br/>
            <input type="hidden" name="sendQuotesAry[idBooking]" id="idBooking" value="<?php echo $idBooking?>">
        </form>
        <p align="center"><a href="javascript:void(0)" onclick="validate_send_booking_quotes('send_manual_quotes_popup_form','<?php echo $szFromPage; ?>');" class="button1" ><span><?=t($t_base.'fields/send');?></span></a> <a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup');"><span><?=t($t_base.'fields/cancel');?></span></a></p>
    </div>
</div>
 <?php
 
}

function display_shipping_date_popup($kRegisterShipCon,$szFromPage)
{
     $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".date('d').")" ;
     
     if(!empty($_POST['addManulQuotesAry']['dtTimingDate']))
     {
         $dtTimingDate = $_POST['addManulQuotesAry']['dtTimingDate'] ;
     }
     else
     {
         $dtTimingDate = date('d/m/Y');
     }
     if(!empty($kRegisterShipCon->arErrorMessages['dtTimingDate']))
     {
         $szClassName = 'class="red_border"';
     }
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container" >
        <div class="popup"> 
    <script type="text/javascript">          
        $().ready(function() {	
            $("#dtTimingDate").datepicker({<?php echo $date_picker_argument; ?>}); 
        });
    </script> 
            <h2>Shipping Date</h2>
            <p>What is the expected shipping date?</p>
            <form id="add_new_manual_quotes_popup_form" name="add_new_manual_quotes_popup_form" action="javascript:void(0);">
                <p><input type="text" <?php echo $szClassName; ?> name="addManulQuotesAry[dtTimingDate]" onfocus="check_form_field_not_required(this.form.id,this.id);" id="dtTimingDate" value="<?php echo $dtTimingDate; ?>" size="30"></p><br/> 
                 
                <br/>
                <input type="hidden" name="addManulQuotesAry[szFromPage]" id="szFromPage" value="<?php echo $szFromPage?>">
                <input type="hidden" name="mode" id="mode" value="ADD_NEW_MANUAL_QUEOTS">
            </form>
            <p align="center"><a href="javascript:void(0)" onclick="validate_send_booking_quotes('add_new_manual_quotes_popup_form','<?php echo $szFromPage; ?>','1');" class="button1" ><span>Create New Quote</span></a> <a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup');"><span>Cancel</span></a></p>
        </div>
    </div>
 <?php
}

function display_verify_user_confirmation($idUser,$szFromPage=false,$idBookingFile=false)
{
    $t_base="management/insurance/"; 
 ?>
<div id="popup-bg"></div>
<div id="popup-container" >
    <div class="popup"> 
        <h2><?php echo t($t_base.'title/verify_email');?></h2>
        <p><?php echo t($t_base.'title/veify_email_text');?></p>   
        <p align="center">
            <a href="javascript:void(0)" onclick="display_verify_popup('<?php echo $idUser; ?>','VERIFY_USER_EMAIL_CONFIRM','<?php echo $szFromPage; ?>','<?php echo $idBookingFile; ?>')" class="button1" ><span><?=t($t_base.'fields/confirm');?></span></a> 
            <a href="javascript:void(0)" class="button1" onclick="showHide('contactPopup');"><span><?=t($t_base.'fields/cancel');?></span></a>
        </p>
    </div>
</div>
 <?php 
}
function display_delete_quotes_popup($idBooking,$szEmail,$szFromPage=false)
{
     $t_base="management/insurance/"; 
 ?>
<div id="popup-bg"></div>
<div id="popup-container" >
    <div class="popup"> 
        <h2><?php echo t($t_base.'fields/delete_quote');?></h2>
        <p><?php echo t($t_base.'fields/delete_quotes_details')." ".$szEmail."?";?></p>  <br><br>
        <p align="center"><a href="javascript:void(0)" onclick="display_manual_quote_form(<?php echo $idBooking; ?>,'DELETE_MANUAL_QUEOTS_CONFIRM','<?php echo $szFromPage; ?>')" class="button1" ><span><?=t($t_base.'fields/delete');?></span></a> <a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup');"><span><?=t($t_base.'fields/cancel');?></span></a></p>
    </div>
</div>
 <?php
 
}
function display_expire_quotes_popup($idBooking,$quotesBookingAry)
{
    $t_base="management/insurance/"; 
 ?>
<div id="popup-bg"></div>
<div id="popup-container" >
    <div class="popup"> 
        <h2><?php echo t($t_base.'fields/expire_quote');?></h2>
        <p><?php echo t($t_base.'fields/expire_quote_details')." ".$quotesBookingAry['szEmail']." on ".date('d. M, Y',strtotime($quotesBookingAry['dtQuotationSent']))."? ".t($t_base.'fields/expire_quote_details_2')." ".date('d. M, Y',strtotime($quotesBookingAry['dtQuoteValidTo']));?></p>  <br><br>
        <p align="center"><a href="javascript:void(0)" onclick="display_manual_quote_form(<?php echo $idBooking; ?>,'EXPIRE_MANUAL_QUEOTS_CONFIRM')" class="button1" ><span>Yes</span></a> <a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup');"><span>No</span></a></p>
    </div>
</div>
 <?php
} 
function display_sent_booking_quotes_listing($newBookingQuotesAry)
{ 
    $t_base="management/insurance/";
    $kInsurance = new cInsurance(); 
    ?>   
                        
    <form id="send_new_insured_booking" name="send_new_insured_booking" action="javascript:void(0);" method="post">
	<table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="insuranced_booking_table">
            <tr>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/date')?></th> 
                <th style="width:25%;" valign="top"><?=t($t_base.'fields/customer');?></th>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/price');?></th>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/trade');?></th> 
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/mode');?></th>
                <th style="width:20%;" valign="top"><?=t($t_base.'fields/vol_height');?></th>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/requested');?></th>
            </tr>
	<?php 
		if(!empty($newBookingQuotesAry))
		{
                    $ctr = 1;
                    $irowCount = count($newBookingQuotesAry); 
                    $kConfig = new cConfig();
                    foreach($newBookingQuotesAry as $searchResults)
                    {  
                        $kConfig->loadCountry($searchResults['idOriginCountry']);
                        $szOriginCountryName = $kConfig->szCountryISO ;

                        $kConfig->loadCountry($searchResults['idDestinationCountry']);
                        $szDestinationCountryName = $kConfig->szCountryISO ;

                        $szVolWeight = format_volume($searchResults['fCargoVolume'])."cbm / ".number_format((float)$searchResults['fCargoWeight'])."kg ";
                        
                        $szCustomerName = $searchResults['szFirstName']." ".$searchResults['szLastName'] ;
                        $szCustomerName = returnLimitData($szCustomerName,20) ;
                        
                        $fInsuranceBuyPrice = $searchResults['fTotalInsuranceCostForBookingCustomerCurrency_buyRate'];
                        if($searchResults['fGoodsInsuranceExchangeRate']>0)
                        {
                            $fTotalAmountInsured = $searchResults['fTotalAmountInsured']/$searchResults['fGoodsInsuranceExchangeRate'] ;
                        }
                        else
                        {
                            $fTotalAmountInsured = 0;
                        }
                        ?>				 
                        <tr id="booking_data_<?=$ctr?>" onclick="select_booking_quote_sent_tr('booking_data_<?=$ctr?>','<?php echo $searchResults['id']?>','SEND_INSURED_BOOKING')">
                            <td><?php echo (!empty($searchResults['dtQuotationSent']) && $searchResults['dtQuotationSent']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtQuotationSent'])):''?></td>
                            <td><?php echo $szCustomerName ; ?></td>
                            <td><?php echo $searchResults['szCustomerCurrency']." ".number_format($searchResults['fBookingAmount']); ?></td>
                            <td><?php echo $szOriginCountryName."-".$szDestinationCountryName; ?></td> 
                            <td><?php echo $searchResults['szTransportMode']; ?></td> 
                            <td><?php echo $szVolWeight; ?></td>
                            <td><?php echo (!empty($searchResults['dtBookingInitiated']) && $searchResults['dtBookingInitiated']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtBookingInitiated'])):''?></td>
                        </tr>
                        <?php
                        $ctr++;
                    }  
		}
		else
		{
                    ?> 
                    <tr>
                        <td colspan="7" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
                    </tr> 
	<?php }	?>
    </table>  
    </form>
    <br>
    <div>
	<span align="left">
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_edit_button" align="left"><span><?=t($t_base.'fields/edit');?></span></a>
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_send_button"><span><?=t($t_base.'fields/resend');?></span></a>
       </span>
	<span style="float: right;"> 
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_copy_button"><span><?=t($t_base.'fields/copy');?></span></a>
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_expire_button"><span><?=t($t_base.'fields/expire');?></span></a>
            <a href="javascript:void(0)" class="button2" style="opacity:0.4" id="booking_quote_delete_button"><span><?=t($t_base.'fields/delete');?></span></a>
	</span>
    </div>
<?php 
} 

function display_expired_booking_quotes_listing($iQuoteStatus,$iPage=1)
{ 
    $t_base="management/insurance/";
    $kInsurance = new cInsurance(); 
    $kBooking = new cBooking();
    $iBookingStatusPage = __BOOKING_STATUS_CONFIRMED__ ; 
    $booking_searched_data = array();
    $searchResult = array();
            
    $newInsuredBookingAry = array();
    $newBookingQuotesAry = $kBooking->getAllBookingQuotes($iQuoteStatus,false,false,$iPage);  
    $iGrandTotal = $kBooking->getAllBookingQuotes($iQuoteStatus,false,false,false,true);   

    require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
    $kPagination  = new Pagination($iGrandTotal,__MAX_CONFIRMED_BOOKING_PER_PAGE__,15);
            
    if($iQuoteStatus==__BOOKING_QUOTES_STATUS_WON__)
    {
        $szFromPage = "WON_QUOTE_PAGE";
    }
    else
    {
        $szFromPage = "EXPIRED_QUOTE_PAGE";
    }
    ?>   
                        
    <form id="send_new_insured_booking" name="send_new_insured_booking" action="javascript:void(0);" method="post">
	<table cellpadding="5" cellspacing="0" class="format-4" width="100%" id="insuranced_booking_table">
            <tr>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/requested');?></th> 
                <th style="width:25%;" valign="top"><?=t($t_base.'fields/customer');?></th>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/price');?></th>
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/trade');?></th> 
                <th style="width:10%;" valign="top"><?=t($t_base.'fields/mode');?></th>
                <th style="width:25%;" valign="top"><?=t($t_base.'fields/vol_height');?></th>
                <?php if($iQuoteStatus==__BOOKING_QUOTES_STATUS_WON__){?>
                    <th style="width:10%;" valign="top"><?=t($t_base.'fields/references')?></th> 
                <?php } else {?>
                    <th style="width:10%;" valign="top"><?=t($t_base.'fields/expired')?></th>
                <?php }?>
            </tr>
	<?php 
		if(!empty($newBookingQuotesAry))
		{
                    $ctr = 1;
                    $irowCount = count($newBookingQuotesAry); 
                    $kConfig = new cConfig();
                    foreach($newBookingQuotesAry as $searchResults)
                    {  
                        $kConfig->loadCountry($searchResults['idOriginCountry']);
                        $szOriginCountryName = $kConfig->szCountryISO ;

                        $kConfig->loadCountry($searchResults['idDestinationCountry']);
                        $szDestinationCountryName = $kConfig->szCountryISO ;

                        $szVolWeight = format_volume($searchResults['fCargoVolume'])."cbm / ".number_format((float)$searchResults['fCargoWeight'])."kg ";
                        
                        $szCustomerName = $searchResults['szFirstName']." ".$searchResults['szLastName'] ;
                        $szCustomerName = returnLimitData($szCustomerName,20) ;
                        
                        ?>				 
                        <tr id="booking_data_<?=$ctr?>" onclick="select_booking_quote_sent_tr('booking_data_<?=$ctr?>','<?php echo $searchResults['id']?>','<?php echo $szFromPage; ?>','1')">
                            <td><?php echo (!empty($searchResults['dtBookingInitiated']) && $searchResults['dtBookingInitiated']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtBookingInitiated'])):''?></td>
                            <td><?php echo $szCustomerName ; ?></td>
                            <td><?php echo $searchResults['szCustomerCurrency']." ".number_format($searchResults['fBookingAmount']) ; ?></td>
                            <td><?php echo $szOriginCountryName."-".$szDestinationCountryName; ?></td> 
                            <td><?php echo $searchResults['szTransportMode']; ?></td> 
                            <td><?php echo $szVolWeight; ?></td>
                            <?php if($iQuoteStatus==__BOOKING_QUOTES_STATUS_WON__){?>
                                <td><?php echo $searchResults['szBookingRef']; ?></td>
                            <?php }else { ?>
                                <td><?php echo (!empty($searchResults['dtQuotationExpired']) && $searchResults['dtQuotationExpired']!='0000-00-00 00:00:00')?date('d/m/Y',strtotime($searchResults['dtQuotationExpired'])):''?></td>
                            <?php }?>
                        </tr>
                        <?php
                        $ctr++;
                    } 
		}
		else
		{
                    ?> 
                    <tr>
                        <td colspan="7" align="center"><h4><b><?=t($t_base.'fields/no_rate_found');?></b></h4></td>
                    </tr> 
	<?php }	?>
    </table>  
    </form>
    <br>
    <div >
        <span align="right" style="float:right;">
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_view_button"><span><?=t($t_base.'fields/view');?></span></a>
            <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="booking_quote_copy_button"><span><?=t($t_base.'fields/copy');?></span></a>
       </span>  
    </div>
    <?php 
     
    if($iGrandTotal>0)
    { 
        $kPagination->page = $iPage;
        $kPagination->idBookingFile = "'".$szFromPage."'";
        $kPagination->paginate('showBookingQuotePagination'); 
        if($kPagination->links_per_page>1)
        {
            echo $kPagination->renderFullNav();
        }
    }  
} 

function display_overview_links($kBooking,$postSearchAry,$backToTask=false,$bQuickQuote=false)
{
    $idBooking = $kBooking->idBooking ;
    $idBookingFile = $kBooking->idBookingFile ; 
    $bUploadLabel = false;
    $iLabelStatus = $kBooking->iLabelStatus ;  
    /*
     * ($kBooking->szFileStatus=='S130' || $kBooking->szTaskStatus=='T220' || $kBooking->szTaskStatus=='T210') && 
     */
    if($kBooking->szForwarderBookingTask=='T150' || $kBooking->szForwarderBookingTask=='T140' || $kBooking->szTaskStatus=='T140' || $iLabelStatus==__LABEL_NOT_SEND_FLAG__ || $iLabelStatus==__LABEL_SEND_FLAG__ || $iLabelStatus==__LABEL_DOWNLOAD_FLAG__ || ( ($postSearchAry['idBookingStatus']=='3'|| $postSearchAry['idBookingStatus']=='4' || $postSearchAry['idBookingStatus']=='7') && $postSearchAry['idTransportMode']==__BOOKING_TRANSPORT_MODE_COURIER__))
    {
        $bUploadLabel = true;
    }
    
    $kTracking = new cTracking();
    $idCourierProvider = $kTracking->getIdCourierProviderByIdServiceProvider($postSearchAry['idServiceProvider']);
    
    if($idCourierProvider==__TNT_API__)
    {
        /*
        * For TNT courier packages we are adding country name at the end of the Tracking URL
        */
        $kConfig = new cConfig();
        if($postSearchAry['idDestinationCountry']>0)
        {
            $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
            if(!empty($kConfig->szCountryISO3Code))
            {
                $szCountryISO3Code = ":".$kConfig->szCountryISO3Code;
            } 
        }
    } 
?>
    <div class="pending-tray-overview-links" style="float:right;padding:6px 0 0 0px;">  
        <?php  
        if($bQuickQuote)
        {
            ?>
            <!--<a href="javascript:void(0);" style="font-size:10px;" onclick="showCopyConfirmationPopUp('COPY_QUICK_QUOTE_BOOKING','<?php echo $idBookingFile; ?>','OVERVIEW_PANE');" target="_blank">Quick Quote</a> -->
            <?php
        }
        else
        {
            
            if($postSearchAry['idBookingStatus']!='7')
            {
                if($backToTask)
                {
                    ?>
                    <!--<a href="javascript:void(0);" onclick="download_admin_label_pdf('<?php echo $idBooking;?>');">View Label</a> |-->
                    <a href="javascript:void(0);" style="font-size:10px;" onclick="display_pending_task_overview('DISPLAY_PENDING_TASK_DETAILS','<?php echo $idBookingFile; ?>');">Back to remind</a> |
                    <?php
                }
                else if($bUploadLabel && $postSearchAry['iTransferConfirmed']==0) 
                { 
                    ?>
                    <!--<a href="javascript:void(0);" onclick="download_admin_label_pdf('<?php echo $idBooking;?>');">View Label</a> |-->
                    <a href="javascript:void(0);" style="font-size:10px;" onclick="display_pending_task_overview('DISPLAY_PENDING_TASK_COURIER_LABEL_UPLOAD_FORM','<?php echo $idBookingFile; ?>');">Label upload screen</a> |
                    <?php
                }

                if(!empty($postSearchAry['szTrackingNumber'])){ ?>
                    <a style="font-size:10px;" href="<?php echo __AFTER_SHIP_TRACKING_URL__."/".$postSearchAry['szTrackingNumber']."".$szCountryISO3Code; ?>" target="_blank">Track</a> |
                <?php } 
            }
            if($postSearchAry['idBookingStatus']=='7'){
                
            if($postSearchAry['iFinancialVersion']=='2' && $postSearchAry['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__ && $postSearchAry['iTransferConfirmed']==0){
            ?> 
            <a style="font-size:10px;" href="javascript:void(0);" onclick="download_admin_self_credit_note_pdf('<?php echo $idBooking;?>');">Self-Billing Credit Note</a> |    
            <?php }?>
            <a style="font-size:10px;" href="javascript:void(0);" onclick="download_admin_credit_note_pdf('<?php echo $idBooking;?>');">Credit Note</a> |
            <?php }if($postSearchAry['iTransferConfirmed']==0){ ?>
            <a style="font-size:10px;" href="javascript:void(0);" onclick="download_admin_booking_pdf('<?php echo $idBooking;?>');">Booking Notice</a> |
            <?php }
            if($postSearchAry['iFinancialVersion']=='2' && $postSearchAry['iForwarderGPType']==__FORWARDER_PROFIT_TYPE_REFERRAL_FEE__ && $postSearchAry['iTransferConfirmed']==0){
            ?>
            <a style="font-size:10px;" href="javascript:void(0);" onclick="download_admin_self_invoice_pdf('<?php echo $idBooking;?>');">Self-Billing Invoice</a> |
            <?php }?>
            <a style="font-size:10px;" href="javascript:void(0);" onclick="download_admin_invoice_pdf('<?php echo $idBooking;?>');">Customer Invoice</a>  
            <?php if($postSearchAry['iTransferConfirmed']==0){ ?>
            | <a style="font-size:10px;" href="javascript:void(0);" onclick="download_booking_confirmation_pdf('<?php echo $idBooking;?>');">Booking Confirmation</a>
            <?php   }} ?> 
    </div> 
    <?php
}


 /*
function display_pending_task_tray_container($kBooking,$iMessageType=false)
{
    $t_base="management/pendingTray/";  
   
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->idBookingFile ;  
    $szTaskStatus = trim($kBooking->szTaskStatus);
    $szFileStatus =  $kBooking->szFileStatus ; 
    $iSearchType =  $kBooking->iSearchType ; 
    ?>
    <?php if($szTaskStatus!='T140' && $szTaskStatus!='T150'){  ?>            
        <div class="clearfix accordion-container-task">
            <span class="accordian"><?=t($t_base.'title/task')?></span>
        </div>
    <?php } ?>
    <div class="scrolling-div-task-pane grey-container-div">
        <?php
            if($szTaskStatus!='T140' && $szTaskStatus!='T150')
            {
                echo '<div id="pending_tray_task_heading_container">';
                echo display_pending_task_header($kBooking,$idBookingFile); 
                echo '</div>';
            }
        ?>
        <div id="pending_task_tray_container">
        <?php 
            if($szTaskStatus=='T140' ||  $szTaskStatus=='T150')
            {
                $bookingQuotesAry[1]['idBooking']=$kBooking->idBooking;
                $bookingQuotesAry[1]['idFile']=$kBooking->idBookingFile;
                
                if((int)$_REQUEST['iQuoteClosed']==1 || $szTaskStatus=='T150')
                { 
                    display_pending_quotes_overview_pane_courier_admin($bookingQuotesAry,$kBooking,false,true); 
                }
                else
                {
                    display_pending_quotes_overview_pane_courier_admin($bookingQuotesAry,$kBooking);
                }
            }
            else
            {    
                if($szTaskStatus=='T1')
                {
                    echo display_pending_task_validate_form($kBooking,$iMessageType);
                }
                else if($szTaskStatus=='T2')
                {
                    echo display_task_ask_for_quote_container($kBooking);
                } 
                else if($szTaskStatus=='T3' && $szFileStatus=='S3')
                {
                    echo display_task_ask_for_quote_container($kBooking);
                }
                else if($szTaskStatus=='T3' && $szFileStatus=='S4')
                {
                    echo display_task_quote_container($kBooking);
                }
                else if($szTaskStatus=='T100' && $szFileStatus=='S2')
                {
                    echo display_task_estimate_container($kBooking);
                }
                else if($szTaskStatus=='T110' || $szTaskStatus=='T120' || $szTaskStatus=='T160' || $iSearchType==1)
                {
                    echo display_task_reminder_container($kBooking);
                }
                else if($szTaskStatus=='T170')
                {
                    echo display_task_logs_container($kBooking);
                }
                else
                {
                    echo display_pending_task_validate_form($kBooking,$iMessageType);
                }
            }
        ?> 
        </div>
    </div> 
    <?php
}  
  * 
  */


function display_task_book_container($kBooking)
{ 
    $t_base="management/pendingTray/";    
    $formId = 'pending_task_tray_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    } 
    $idBooking = $kBooking->idBooking ;  
    
    ?>
    <div><h3>BOOK CONTAINER COMING SOON</h3></div>
    <?php
}
function display_task_reminder_container_backup($kBooking)
{  
    $idBooking = $kBooking->idBooking ;    
    $t_base="management/pendingTray/";    
    ?> 
    <script type="text/javascript">  
        var _day = '<?php echo date('d'); ?>';
        var _month = '<?php echo date('m'); ?>';
        var _year = '<?php echo date('Y'); ?>';

        var date_picker_argument = new Date(_year, _month-1,_day) ; 
        $( "#dtReminderDate").datepicker({ minDate:date_picker_argument}); 
    </script> 
    <div class="clearfix" id="rfq-reminder-notes-container">
        <?php echo displayAddRFQReminderNotes($kBooking); ?>
    </div>  
    <div class="clearfix" style="width:100%;">
        <div style="width:40%;float:left;padding: 5px 0 0;" id="snooze-form-container">
            <?php echo displayAddSnoozeForm($kBooking); ?>
        </div>
        <div class="btn-container" style="float:right;width:59%;text-align:right;">
            <a href="javascript:void(0);" id="request_quote_save_button" style="float:left;" class="button1" onclick="submit_snooze_form();"><span>Snooze</span></a>
            <a href="javascript:void(0);" class="button1" onclick="close_pending_task('<?php echo $kBooking->idBookingFile ?>','CONFIRM_CLOSE_BOOKING_FILE')"><span><?php echo t($t_base.'fields/close_task'); ?></span></a> 
            <a href="javascript:void(0);" id="request_quote_save_button" class="button1" onclick="submit_rfq_note_form();"><span>Save</span></a> 
            <?php if($iSuccess){?>
                <span style="color:green;"> Saved! </span>
            <?php }?>
        </div> 
    </div>
    <?php
}
function display_task_reminder_container($kBooking,$iSuccess=false,$idTemplate=false,$iLanguage=false,$szEmailClicked=false)
{   
    $idBooking = $kBooking->idBooking ;    
    $t_base="management/pendingTray/";    
    $kConfig = new cConfig();
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container";   
  
    $replace_ary = array();   
    //$kBooking_new  = new cBooking();
    //$kBooking_new = $kBooking; //Creating object clone
    if(!empty($_POST['addSnoozeAry']))
    { 
        $addSnoozeAry = $_POST['addSnoozeAry'];  
        $szReminderNotes = $addSnoozeAry['szReminderNotes'];  
        $idBookingFile = $addSnoozeAry['idBookingFile'];
        $dtReminderDate = $addSnoozeAry['dtReminderDate'];
        $szReminderTime = $addSnoozeAry['szReminderTime'];
        $emailTemplate_ID = $addSnoozeAry['idTemplate'];
        $szCustomerEmail = $addSnoozeAry['szCustomerEmail'];
        $szReminderSubject = $addSnoozeAry['szReminderSubject'];
        $szReminderEmailBody = $addSnoozeAry['szReminderEmailBody'];
        $dtEmailReminderDate = $addSnoozeAry['dtEmailReminderDate'];
        $szEmailReminderTime = $addSnoozeAry['szEmailReminderTime'];
        $replace_ary['iBookingLanguage'] = $addSnoozeAry['iRemindTemplateLanguage'];
        
        $szCustomerCCEmail = $addSnoozeAry['szCustomerCCEmail'];
        $szCustomerBCCEmail = $addSnoozeAry['szCustomerBCCEmail'];
        
        $kBooking->loadFile($idBookingFile);
        $bookingDataArr = $kBooking->getBookingDetails($idBooking);
        $szRemindEmailTemplates = $kConfig->getRemindCmsSectionsList();
    }  
    else
    {
        
        $idBookingFile = $kBooking->idBookingFile;
        $idBooking = $kBooking->idBooking; 
        //$dtReminderDateTime = $kBooking->dtSnoozeDate;
        $idOfflineChat = $kBooking->idOfflineChat;
        $bookingDataArr = $kBooking->getBookingDetails($idBooking);
        
        if(!empty($szEmailClicked))
        {
            $szCustomerEmail = $szEmailClicked;
        }
        else
        {
            $szCustomerEmail = $bookingDataArr['szEmail'];
        } 
        $remindDateAry = format_reminder_date_time($dtReminderDateTime);
        
        $dtReminderDate = $remindDateAry['dtReminderDate'];
        $szReminderTime = $remindDateAry['szReminderTime']; 
            
        if(!empty($dtReminderDateTime) && $dtReminderDateTime!='0000-00-00 00:00:00')
        { 
            if(date('I')==1)
            {
                //$dtReminderDateTime = getDateCustom($dtReminderDateTime);  
            } 
            $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ; 
            $date = new DateTime($dtReminderDateTime); 
            $date->setTimezone(new DateTimeZone($szCustomerTimeZone));    
            
            $dtEmailReminderDate = $date->format('d/m/Y');
            $szEmailReminderTime = $date->format('H:i');  
        }
        else
        {
            $iWeekDay = date('w');  
            //Default snooze time for automatic booking is 2 hours and for RFQ 1 days.
            
            if($iWeekDay==7 || $iWeekDay==6) //5: Friday, 6: Saturday
            {
                $dtEmailReminderDate = date("d/m/Y",strtotime("NEXT MONDAY")); 
            }
            else
            { 
                $dtEmailReminderDate = date("d/m/Y",strtotime("+1 DAY")); 
            }  
            
            if(date('I')==1)
            {
                $szEmailReminderTime = date("H:i",strtotime("+1 HOUR")); 
            }
            else
            {
                $szEmailReminderTime = date("H:i"); 
            }
        }
        
        if($idTemplate>0)
        {
            $emailTemplate_ID = $idTemplate;
        }
        else
        {
            $id_ReminderTemplate = $kConfig->getReminderTemplateIdByTaskStatus($kBooking->szTaskStatus);

            if($id_ReminderTemplate>0)
            {
                $emailTemplate_ID = $id_ReminderTemplate;
            }
            else
            {
                $emailTemplate_ID =0;
            }
        }
        

        $kAdmin = new cAdmin();
        $kAdmin->getAdminDetails($_SESSION['admin_id']);
        
        $kUser = new cUser();
        $kUser->getUserDetails($bookingDataArr['idUser']);
        
        $kConfig->loadCountry($kAdmin->idInternationalDialCode);
        $iInternationDialCode = $kConfig->iInternationDialCode;
        
        $idInternationalDialCode = $bookingDataArr['idCustomerDialCode'];
        if(empty($idInternationalDialCode) && $bookingDataArr['idUser']>0)
        {
            
            $idInternationalDialCode = $kUser->idInternationalDialCode;
        } 
        $szCustomerPhoneNumber = $bookingDataArr['szCustomerPhoneNumber'];
        
        $kConfig = new cConfig();
        $kConfig->loadCountry($idInternationalDialCode);
        $iCustomerInternationDialCode = "+".$kConfig->iInternationDialCode;
            
        $iAlreadyPaidBooking = 0;  

        if($bookingDataArr['idBookingStatus']==3 || $bookingDataArr['idBookingStatus']==4)
        {
            $iAlreadyPaidBooking = 1;
        }  

        if($iAlreadyPaidBooking==1)
        {
            if($iLanguage>0)
            {
                $replace_ary['iBookingLanguage'] = $iLanguage;
            }
            else
            {
                $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage']; 
            }  
        }
        else
        {
            if($iLanguage>0)
            {
                $replace_ary['iBookingLanguage'] = $iLanguage;
            }
            else
            {
                if($kUser->iLanguage>0)
                {
                    $replace_ary['iBookingLanguage'] = $kUser->iLanguage;
                }
                else
                {
                    $replace_ary['iBookingLanguage'] = 2;//Danish
                }
            }   
        }
        
        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry'],false,$replace_ary['iBookingLanguage']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry'],false,$replace_ary['iBookingLanguage']); 
        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	 

        $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone;

        $szBaseUrl_str = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);	
        $szBaseUrl = addhttp(__MAIN_SITE_HOME_PAGE_URL__);

        $szSiteLink = "<a href='".$szBaseUrl."' target='_blank'>".$szBaseUrl_str."</a>";

        $kWhsSearch=new cWHSSearch();
        $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__');

        $szRemindEmailTemplates = $kConfig->getRemindCmsSectionsList();
        
        $kConfigNew = new cConfig();
        $transportModeListAry = array();
        $transportModeListAry = $kConfigNew->getAllTransportMode(false,$bookingDataArr['iBookingLanguage'],true,true);
        $shipperConsigneeAry = $kBooking->getBookingDetailsCountryName($idBooking);
        
        
        $szShipperCountryAry = $kConfigNew->getAllCountryInKeyValuePair(false,$shipperConsigneeAry['idShipperCountry'],false,$bookingDataArr['iBookingLanguage']);
        $szConsigneeCountryAry = $kConfigNew->getAllCountryInKeyValuePair(false,$shipperConsigneeAry['idConsigneeCountry'],false,$bookingDataArr['iBookingLanguage']); 
        $szShipperCountry = $szShipperCountryAry[$shipperConsigneeAry['idShipperCountry']]['szCountryName'] ;
        $szConsigneeCountry = $szConsigneeCountryAry[$shipperConsigneeAry['idConsigneeCountry']]['szCountryName'] ;	 
        
        $serviceTermAry = $kConfigNew->getAllServiceTerms($bookingDataArr['idServiceTerms']);
        
        
        $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($bookingDataArr['id']);
        $total=count($cargoDetailArr);
        $cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
        $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',','); 
        $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);
        if($szCargoCommodity!='')
        {
            $szCargoCommodity = ", ".$szCargoCommodity;
        }
        else if($bookingDataArr['szCargoDescription']!='')
        {
            $szCargoCommodity = ", ".$bookingDataArr['szCargoDescription'];
        }
        $total=count($cargoDetailArr);
        $kCourierServices =  new cCourierServices();
        if(!empty($cargoDetailArr))
        {
            $szCargoFullDetails="";
            $ctr=0;
            $totalQuantity=0;
            foreach($cargoDetailArr as $cargoDetailArrs)
            { 
                if($cargoDetailArrs['szCommodity']!='')
                {
                    $packingTypeArr[0]['szPacking']=$cargoDetailArrs['szCommodity'];
                    $packingTypeArr[0]['szSingle']=$cargoDetailArrs['szCommodity'];
                }
                else
                {
                    if($bookingDataArr['idCourierPackingType']>0)
                    {
                        $packingTypeArr=$kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1);
                    }
                    else
                    {
                        $packingTypeArr[0]['szPacking']='colli';
                        $packingTypeArr[0]['szSingle']='colli';
                    }
                }
                $totalQuantity=$totalQuantity+$cargoDetailArrs['iColli'];
                $t=$total-1;
                   if((int)$cargoDetailArrs['iQuantity']>1)
                        $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szPacking']);
                    else
                        $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szSingle']);

                    $lenWidthHeightStr='';
                    if($cargoDetailArrs['fLength']>0 || $cargoDetailArrs['fWidth']>0 || $cargoDetailArrs['fHeight']>0)
                    {
                        $lenWidthHeightStr=round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."".$cargoDetailArrs['cmdes']." (LxWxH), ";
                    }
                    if($ctr==0)
                    {
                        $szCargoFullDetails .=$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                    }
                    else
                    {
                        $szCargoFullDetails .="<br />".$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                    }
                ++$ctr;	
            }

            if($totalQuantity>1)
            {
                $textPacking='colli';
            }
            else
            {
                $textPacking='colli';   
            }
            
            $coliiText='';
            if($totalQuantity>0)
            {
                $coliiText=number_format($totalQuantity)." ".$textPacking.", ";
            }

            $szCargoFullDetails .= "<br />Total: ".$coliiText."".$cargo_volume." cbm, ".$cargo_weight." kg".$szCargoCommodity; 
        }
        else
        {
            $totalQuantity = $bookingDataArr['iNumColli'];
            $textPacking = 'colli';
            $coliiText='';
            if($totalQuantity>0)
            {
                $coliiText=number_format($totalQuantity)." ".$textPacking.", ";
            }
            $szCargoFullDetails .= "Total: ".$coliiText."".$cargo_volume." cbm, ".$cargo_weight." kg".$szCargoCommodity; 
        }
                                    
        
        if($shipperConsigneeAry['szShipperPostCode']!=''){
            $replace_ary['szPostcodeShipper'] = $shipperConsigneeAry['szShipperPostCode'].", ";
        }else{
            $replace_ary['szPostcodeShipper'] ='';
        }
        if($shipperConsigneeAry['szConsigneePostCode']!=''){
            $replace_ary['szPostcodeConsignee'] = $shipperConsigneeAry['szConsigneePostCode'].", ";
        }else{
            $replace_ary['szPostcodeConsignee'] ='';
        }
        
        if($shipperConsigneeAry['szShipperCity']!=''){
        $replace_ary['szCityShipper'] = $shipperConsigneeAry['szShipperCity'].", ";
        }
        else{
            $replace_ary['szCityShipper'] = '';
        }                                    
        if($szShipperCountry!=''){
            $replace_ary['szCountryShipper'] = $szShipperCountry;
        }
        else{
            $replace_ary['szCountryShipper'] = '';
        } 

        if($shipperConsigneeAry['szConsigneeCity']!=''){
            $replace_ary['szCityConsignee'] = $shipperConsigneeAry['szConsigneeCity'].", ";
        }
        else{
            $replace_ary['szCityConsignee'] = '';
        }                                    
        if($szConsigneeCountry!=''){
            $replace_ary['szCountryConsignee'] = $szConsigneeCountry;
        }
        else{
            $replace_ary['szCountryConsignee'] = '';
        }

        $replace_ary['szFromString'] = $replace_ary['szPostcodeShipper']."".$replace_ary['szCityShipper']."".$replace_ary['szCountryShipper'];
        $replace_ary['szToString'] = $replace_ary['szPostcodeConsignee']."".$replace_ary['szCityConsignee']."".$replace_ary['szCountryConsignee'];                               
        
        $replace_ary['szCargoDetail'] = $szCargoFullDetails ;
        $replace_ary['szCargoReady'] = date('d/m/Y',strtotime($bookingDataArr['dtTimingDate']));
        $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'] ;
        $replace_ary['szLastName'] = $bookingDataArr['szLastName'];
        $replace_ary['szCountryCode'] = $iCustomerInternationDialCode;
        $replace_ary['szPhoneNumber'] = $szCustomerPhoneNumber;
        $replace_ary['szFromCountry'] = $szOriginCountry ;
        $replace_ary['szToCountry'] = $szDestinationCountry ;
        $replace_ary['szFromCity'] = $bookingDataArr['szOriginCity'];
        $replace_ary['szToCity'] = $bookingDataArr['szDestinationCity'] ;
        
        
        
        $replace_ary['szMode'] = $transportModeListAry[$bookingDataArr['idTransportMode']]['szShortName'];
        $replace_ary['szTerms'] = $serviceTermAry[0]['szShortTerms'];
            
        $volume =  format_volume($bookingDataArr['fCargoVolume']);
        $weight =  get_formated_cargo_measure($bookingDataArr['fCargoWeight']);
        
        $volume = round_up($volume,1);
        $weight = round_up($weight,0);
        
        $replace_ary['szVol'] = getPriceByLang($volume,$replace_ary['iBookingLanguage'],1); 
        $replace_ary['szWeight'] = getPriceByLang($weight,$replace_ary['iBookingLanguage'],0); 
            
        $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
        $replace_ary['szTitle'] = $kAdmin->szTitle ;
        $replace_ary['szOfficePhone'] = $szCustomerCareNumer ;
        $replace_ary['szMobile'] = $szMobile ; 
        $replace_ary['szWebSiteUrl'] = $szBaseUrl_str;
        
        if($idOfflineChat>0)
        {
            $kChat = new cChat();
            $zopimChatTranscriptAry = array();
            $zopimChatTranscriptAry = $kChat->getAllZopimChatMessageLogs($idOfflineChat);

            $zopimChatAry = array();
            $zopimChatAry = $kChat->getAllZopimChatLogs(false,$idOfflineChat); 
            $szChatMessage = display_zopim_chat_transcript($zopimChatTranscriptAry,$zopimChatAry[0],'TEXT'); 
            $szChatDateTime = $zopimChatAry[0]['dtChatDateTime'];

            $iMonth = (int)date("m",strtotime($szChatDateTime));
            $szMonthName = getMonthName($iBookingLanguage,$iMonth,true);
			
            $szChatDate = (int)date("d",strtotime($szChatDateTime)).". ".$szMonthName." ".date("Y",strtotime($szChatDateTime));

            $replace_ary['szChatDate'] = $szChatDate ; 
            $replace_ary['szChatMessage'] = $szChatMessage;
        }
        $kCRM = new cCRM();
        $remindDraftArr=$kCRM->getDraftEmailRemindData($bookingDataArr['id']);
        $draftEmailData=false;
        if($remindDraftArr['szRemindDraftEmailBody']!='')
        {
            $draftEmailData=true;
            $emailTemplate_ID=$remindDraftArr['idRemindEmailTemplate'];
            $szCustomerEmail=$remindDraftArr['szCustomerEmail'];
            $szCustomerCCEmail=$remindDraftArr['szCustomerCCEmail'];
            if($szCustomerCCEmail!='')
            {
                $iAddCCEmail=1;
            }
            $szCustomerBCCEmail=$remindDraftArr['szCustomerBCCEmail'];
            if($szCustomerBCCEmail!='')
            {
                $iAddBCCEmail=1;
            }
        }
        else
        {
            $emailMessageAry = array(); 
            $emailMessageAry = createEmailMessage($szEmailTemplate,$replace_ary,true,$emailTemplate_ID);

            $breaksAry = array("<br/>","<br>","<br><br>","<br/><br/>");

            //$szReminderEmailBody = htmlentities($emailMessageAry['szEmailBody'], ENT_NOQUOTES);  
            //$szReminderEmailBody = str_replace(array("\n", "\r"), '', $szReminderEmailBody);
            $szReminderEmailBody = str_ireplace($breaksAry, PHP_EOL, $emailMessageAry['szEmailBody']);   
            $szReminderSubject = $emailMessageAry['szEmailSubject']; 
        }
        
    } 
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('','','','','',true);
    
    $szFormAction = __BASE_MANAGEMENT_URL__."/uploadAttachment.php";
    
    if((int)$kBooking->iLabelStatus==1){
    if($kBooking->szTaskStatus=='T140')
    {
        $szTaskStatus=$kBooking->szTaskStatus;
    }
    else
    {
        if($kBooking->szForwarderBookingTask=='T140'){
        $szTaskStatus=$kBooking->szForwarderBookingTask;
        }
    }}
    ?> 
    <script type="text/javascript">  
        var _day = '<?php echo date('d'); ?>';
        var _month = '<?php echo date('m'); ?>';
        var _year = '<?php echo date('Y'); ?>';

        var date_picker_argument = new Date(_year, _month-1,_day) ; 
        $("#dtReminderDate").datepicker({minDate:date_picker_argument});
        $("#dtEmailReminderDate").datepicker({minDate:date_picker_argument}); 
        
        
        <?php if(!empty($szCustomerCCEmail)){ ?>
            display_email_fields('SHOW_CC','<?php echo $idCrmEmail; ?>');
        <?php } if(!empty($szCustomerBCCEmail)){ ?>
            display_email_fields('SHOW_BCC','<?php echo $idCrmEmail; ?>');
        <?php } ?>
    </script>   
    <form spellcheck="false" action="<?php echo $szFormAction; ?>" method="post" id="rfq-reminder-notes-form" name="rfq-reminder-notes-form"> 
        <div id="save_reminder_notes_container">
            <div class="reminder-fields-container"> 
                <span class="<?php echo $szSecondSpanClass; ?>" style="width:100%;">
                    <textarea style="min-height: 100px;" name="addSnoozeAry[szReminderNotes]" onkeyup="enable_reminder_tab_buttons();" onblur="enable_reminder_tab_buttons();" placeholder="Reminder Notes" id="szReminderNotes"><?php echo $szReminderNotes; ?></textarea>
                </span>  
            </div>   
            <div class="clearfix" style="width:100%;">
                <div style="width:40%;float:left;padding: 5px 0 0;" id="snooze-form-container"> 
                    <div class="reminder-fields-container clearfix"> 
                        <span class="<?php echo $szSecondSpanClass; ?>" style="width:89%;">
                            <input type="text" name="addSnoozeAry[dtReminderDate]" id="dtReminderDate" onkeyup="enable_reminder_tab_buttons();" onblur="enable_reminder_tab_buttons();" readonly value="<?php echo $dtReminderDate; ?>">
                            <input type="text" maxlength="5" name="addSnoozeAry[szReminderTime]" onkeyup="enable_reminder_tab_buttons();" id="szReminderTime" onblur="format_reminder_time(this.value);enable_reminder_tab_buttons();" value="<?php echo $szReminderTime; ?>">  
                            <input type="hidden" name="addSnoozeAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>"> 
                            <input type="hidden" name="addSnoozeAry[iLabelStatus]" id="iLabelStatus" value="<?php echo $kBooking->iLabelStatus; ?>"> 
                            <input type="hidden" name="addSnoozeAry[szTaskStatusRemind]" id="szTaskStatusRemind" value="<?php echo $szTaskStatus; ?>"> 
                        </span> 
                        <span class="quote-remider-time" style="float: left;color:#999;font-style: italic;padding: 2px 0 0;width: 5%;">(hh:mm)</span>   
                    </div>  
                </div>
                <div class="btn-container" style="float:right;width:59%;text-align:right;">
                    <a href="javascript:void(0);" id="request_reminder_snooze_button" style="float:left;opacity:0.4;" class="button1"><span>Save Snooze</span></a>
                    <a href="javascript:void(0);" id="request_reminder_test_search_button" class="button1" onclick="open_test_search('<?php echo $idBookingFile; ?>')"><span>Test Search</span></a>
                    <a href="javascript:void(0);" id="request_reminder_close_task_button" class="button1" <?php if($kBooking->iLabelStatus=='1' && $szTaskStatus=='T140'){?> style="opacity:0.4" <?php }else{ ?> onclick="submit_snooze_form('CLOSE_TASK');" <?php }?>><span><?php echo t($t_base.'fields/save_n_close'); ?></span></a> 
                    <a href="javascript:void(0);" id="request_reminder_save_button" style="opacity:0.4;" class="button1" ><span>Save</span></a> 
                    <?php if($iSuccess==1){ ?>
                        <span style="color:green;"> Saved! </span>
                    <?php }
                    if($kBooking->arErrorMessages['szTaskStatusCheckingFlag']!='')
                    {
                    ?>
                        <span style="color:red;"><br /><?php echo $kBooking->arErrorMessages['szTaskStatusCheckingFlag'];?></span>
                    <?php }?>
                </div> 
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="change_reminder_email_template_container">
        <div class="clearfix email-fields-container"> 
            <span class="<?php echo $szFirstSpanClass; ?>">Template</span>
            <span class="<?php echo $szSecondSpanClass; ?>" style="width:40%;" >
                <select name="addSnoozeAry[idTemplate]" id="idTemplate" onchange="changeRemindEmailTemplateBody('CHANGE_REMIND_EMAIL_TEMPLATE_BODY','<?php echo $idBookingFile; ?>',this.value,'<?php echo $replace_ary['iBookingLanguage'];?>','CHANGE_TEMPLATE');enable_reminder_tab_buttons();">
                    <?php 
                    if(!empty($szRemindEmailTemplates))
                    {

                        if($emailTemplate_ID == 0)
                        {?>
                            <option value="0" <?php if($emailTemplate_ID==0){?>selected="selected"<?php }?> >Select</option>
                        <?php
                        }
                        foreach($szRemindEmailTemplates as $szRemindEmailTemplateDetail)
                        {?>
                            <option value="<?php echo $szRemindEmailTemplateDetail['id'];?>" <?php if($szRemindEmailTemplateDetail['id'] == $emailTemplate_ID){?>selected="selected"<?php }?> ><?php echo $szRemindEmailTemplateDetail['szFriendlyName'];?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </span>
            <span class="<?php echo $szSecondSpanClass; ?>" style="width:10%;margin-left:5px;">
                <select name="addSnoozeAry[iRemindTemplateLanguage]" id="iRemindTemplateLanguage" onchange="changeRemindEmailTemplateBody('CHANGE_REMIND_EMAIL_TEMPLATE_BODY','<?php echo $idBookingFile; ?>','<?php echo $emailTemplate_ID;?>',this.value,'CHANGE_LANGUAGE');">
                    <?php
                    if(!empty($languageArr))
                    {
                        foreach($languageArr as $languageArrs)
                        {?>
                    <option value="<?php echo $languageArrs['id'];?>" <?php if($replace_ary['iBookingLanguage'] == $languageArrs['id']){ ?> selected="selected"<?php }?>><?php echo ucfirst($languageArrs['szName']);?></option>
                           <?php 
                        }
                    }?>


                </select>
            </span>
            <span class="<?php echo $szSecondSpanClass; ?>" style="width:25%;float:right;text-align:right;"> 
                <a href="javascript:void(0);" id="reminder_template_edit_button" class="button1" onclick="add_edit_RemindEmailTemplate('REMIND','<?php echo $idBookingFile; ?>','edit_template');"><span>Edit</span></a> 
                <a href="javascript:void(0);" id="reminder_template_new_button" class="button1" onclick="add_edit_RemindEmailTemplate('REMIND','<?php echo $idBookingFile; ?>','new_template');"><span>New</span></a> 
            </span> 
        </div>
        <div class="clearfix email-fields-container"> 
            <span class="quote-field-container wd-14">To</span>
            <span class="quote-field-container wd-40">
                <input type="text" name="addSnoozeAry[szCustomerEmail]" onkeyup="enable_reminder_tab_buttons();" onblur="enable_reminder_tab_buttons();" placeholder="Email Addresses, separate with comma" id="szCustomerEmail" value="<?php echo $szCustomerEmail; ?>"> 
            </span>  
            <span class="quote-field-container wds-45">
                <a href="javascript:void(0);" onclick="display_remind_pane_add_popup('CUSTOMER_EMAIL','<?php echo $idBookingFile; ?>','CRM_OPERATION','<?php echo $idCrmEmail; ?>');">Add</a>
                <span id="crm_operation_cc_link_<?php echo $idCrmEmail; ?>">&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('SHOW_CC','<?php echo $idCrmEmail; ?>')">Cc</a></span>
                <span id="crm_operation_bcc_link_<?php echo $idCrmEmail; ?>">&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('SHOW_BCC','<?php echo $idCrmEmail; ?>')">Bcc</a></span>
            </span>
        </div>
        <div class="clearfix email-fields-container" id="crm_operation_cc_fields_container_<?php echo $idCrmEmail; ?>" <?php if($iAddCCEmail==1){ ?>style="display:block;"<?php }else{?> style="display:none;"<?php } ?>> 
            <span class="quote-field-container wd-14">Cc</span>
            <span class="quote-field-container wd-40">
                <input type="text" name="addSnoozeAry[szCustomerCCEmail]" onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" placeholder="Email Addresses, separate with comma" id="szCustomerCCEmail" value="<?php echo $szCustomerCCEmail; ?>"> 
            </span>  
            <span class="quote-field-container wds-45">
                <a href="javascript:void(0);" onclick="display_remind_pane_add_popup('CUSTOMER_CC_EMAIL','<?php echo $idBookingFile; ?>','CRM_OPERATION','<?php echo $idCrmEmail; ?>');">Add</a> 
                &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('HIDE_CC','<?php echo $idCrmEmail; ?>')" class="red_text"><strong>X</strong></a>
            </span>
        </div>
        <div class="clearfix email-fields-container" id="crm_operation_bcc_fields_container_<?php echo $idCrmEmail; ?>" <?php if($iAddBCCEmail==1){ ?>style="display:block;"<?php }else{?> style="display:none;"<?php } ?>> 
            <span class="quote-field-container wd-14">Bcc</span>
            <span class="quote-field-container wd-40">
                <input type="text" name="addSnoozeAry[szCustomerBCCEmail]" onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" placeholder="Email Addresses, separate with comma" id="szCustomerBCCEmail" value="<?php echo $szCustomerBCCEmail; ?>"> 
            </span>  
            <span class="quote-field-container wds-45">
                <a href="javascript:void(0);" onclick="display_remind_pane_add_popup('CUSTOMER_BCC_EMAIL','<?php echo $idBookingFile; ?>','CRM_OPERATION','<?php echo $idCrmEmail; ?>');">Add</a> 
                &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_email_fields('HIDE_BCC','<?php echo $idCrmEmail; ?>')" class="red_text"><strong>X</strong></a>
            </span>
        </div>
        <input type="hidden" name="addSnoozeAry[iAddCCEmail]" id="iAddCCEmail_<?php echo $idCrmEmail; ?>" value="<?php echo $iAddCCEmail; ?>">
        <input type="hidden" name="addSnoozeAry[iAddBCCEmail]" id="iAddBCCEmail_<?php echo $idCrmEmail; ?>" value="<?php echo $iAddBCCEmail; ?>">
    </div>
    <div id="send_reminder_email_container">
        <?php echo display_email_template_body($kBooking,$emailTemplate_ID,$replace_ary['iBookingLanguage'],$iSuccess);?>
    </div>
    <div id="pending_task_reminder_email_preview_container" style="display:none;">
    </div>
    </form> 
    
    <?php 
    $formId = 'rfq-reminder-notes-form'; 
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }  
}
function displayAddRFQReminderNotes($kBooking,$iSuccess=false)
{
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container";  
    
    $t_base="management/pendingTray/";    
            
    if(!empty($_POST['addReminderNotesAry']))
    {
        $idBookingFile = $_POST['addReminderNotesAry']['idBookingFile'];
        $szReminderNotes = $_POST['addReminderNotesAry']['szReminderNotes']; 
    } 
    else
    {
        $idBookingFile = $kBooking->idBookingFile; 
    }
    ?>
    <form action="javascript:void(0);" method="post" id="rfq-reminder-notes-form" name="rfq-reminder-notes-form">
        <div class="email-box-container">
            <div class="reminder-fields-container"> 
                <span class="<?php echo $szSecondSpanClass; ?>" style="width:100%;">
                    <textarea style="min-height: 250px;" name="addReminderNotesAry[szReminderNotes]" id="szReminderNotes"><?php echo $szReminderNotes; ?></textarea>
                    <input type="hidden" name="addReminderNotesAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">  
                </span>  
            </div>  
        </div> 
    </form>
    <?php
    $formId = 'rfq-reminder-notes-form'; 
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }  
}
function displayAddSnoozeForm($kBooking,$iSuccess=false)
{
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container";  
    
    $t_base="management/pendingTray/";    
    $formId = 'task-snooze-form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    }  
    
    if(!empty($_POST['addSnoozeAry']))
    {
        $idBookingFile = $_POST['addSnoozeAry']['idBookingFile'];
        $dtReminderDate = $_POST['addSnoozeAry']['dtReminderDate'];
        $szReminderTime = $_POST['addSnoozeAry']['szReminderTime'];
    }
    else
    {
        $idBookingFile = $kBooking->idBookingFile; 
        //$dtReminderDateTime = $kBooking->dtSnoozeDate ;
        
        if(!empty($dtReminderDateTime) && $dtReminderDateTime!='0000-00-00 00:00:00')
        {
            $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ; 
            $date = new DateTime($dtReminderDateTime); 
            $date->setTimezone(new DateTimeZone($szCustomerTimeZone));   
            $dtReminderDate = $date->format('d/m/Y');
            $szReminderTime = $date->format('H:i');  
        }
        else
        {
            $iWeekDay = date('w');
            if($iWeekDay==5 || $iWeekDay==6) //5: Friday, 6: Saturday
            {
                $dtReminderDate = date("d/m/Y",strtotime("NEXT MONDAY"));
            }
            else
            {
                $dtReminderDate = date("d/m/Y",strtotime("+1 DAY"));
            }
            $szReminderTime = date("H:i");
        }
    } 
    $kAdmin = new cAdmin();
    ?>
    <form action="javascript:void(0);" method="post" id="task-snooze-form" name="task-snooze-form"> 
        <div class="reminder-fields-container clearfix"> 
            <span class="<?php echo $szSecondSpanClass; ?>" style="width:89%;">
                <input type="text" name="addSnoozeAry[dtReminderDate]" id="dtReminderDate" readonly value="<?php echo $dtReminderDate; ?>">
                <input type="text" maxlength="5" name="addSnoozeAry[szReminderTime]" id="szReminderTime" onblur="format_reminder_time(this.value);" value="<?php echo $szReminderTime; ?>">  
                <input type="hidden" name="addSnoozeAry[idBookingFile]" id="idBookingFile" value="<?php echo $idBookingFile; ?>">  
            </span> 
            <span class="quote-remider-time" style="float: left;color:#999;font-style: italic;padding: 2px 0 0;width: 5%;">(hh:mm)</span>   
        </div> 
        <?php if($iSuccess){?>
            <div style="color:green;float:right;" class="<?php echo $szSecondSpanClass; ?>"> Saved! </div>
        <?php }?>
    </form>
    <?php
}
function display_task_logs_container_backup($kBooking)
{ 
    $t_base="management/pendingTray/";    
    $formId = 'pending_task_tray_form';
    if(!empty($kBooking->arErrorMessages))
    { 
        display_form_validation_error_message($formId,$kBooking->arErrorMessages);
    } 
    $idBooking = $kBooking->idBooking ;  
    $idBookingFile = $kBooking->id ;
    $idEmailLogs = $kBooking->idEmailLogs;
    
    $kBooking_new = new cBooking(); 
    $postSearchAry = $kBooking_new->getBookingDetails($idBooking);
            
    $logEmailsAry = array();
    $logEmailsAry = $kBooking_new->getAllFileEmailLogs($idBooking);
    
    $logReminderAry = array();
    $logReminderAry = $kBooking_new->getAllFileReminderLogs($idBooking);
    
    if(!empty($logReminderAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logReminderAry);
    }
    
    $logFileSearchAry = array();
    $logFileSearchAry = $kBooking_new->getAllFileSearchLogs($idBookingFile);
           
    if(!empty($logFileSearchAry))
    {
        $logEmailsAry = array_merge($logEmailsAry,$logFileSearchAry);
    }
    $logEmailsAry = sortArray($logEmailsAry,'dtSent','DESC');
    $kAdmin = new cAdmin();
    ?> 
    <div class="clearfix">
        <div id="booking_quote_main_container_table" class="clearfix"> 
            <?php
                if(!empty($logEmailsAry))
                {
                    foreach($logEmailsAry as $logEmailsArys)
                    {
                        if(date('I')==1)
                        {  
                            //Our server's My sql time is 2 hours behind so that's why added 2 hours to sent date 
                            $logEmailsArys['dtSentOriginal'] = $logEmailsArys['dtSent']; 
                            $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtSent'])));  
                            if(!empty($logEmailsArys['dtRemindDate']) && $logEmailsArys['dtRemindDate']!='0000-00-00 00:00:00')
                            {
                                $logEmailsArys['dtRemindDate'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtRemindDate'])));  
                            }
                            if(!empty($logEmailsArys['dtClosedOn']) && $logEmailsArys['dtClosedOn']!='0000-00-00 00:00:00')
                            {
                                $logEmailsArys['dtClosedOn'] = date('Y-m-d H:i:s',strtotime("+2 HOUR",strtotime($logEmailsArys['dtRemindDate'])));  
                            } 
                        }  
                        else
                        {
                            $logEmailsArys['dtSentOriginal'] = $logEmailsArys['dtSent']; 
                            $logEmailsArys['dtSent'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtSent'])));  
                            if(!empty($logEmailsArys['dtRemindDate']) && $logEmailsArys['dtRemindDate']!='0000-00-00 00:00:00')
                            {
                                $logEmailsArys['dtRemindDate'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtRemindDate'])));    
                            }
                            if(!empty($logEmailsArys['dtClosedOn']) && $logEmailsArys['dtClosedOn']!='0000-00-00 00:00:00')
                            {
                                $logEmailsArys['dtClosedOn'] = date('Y-m-d H:i:s',strtotime("+1 HOUR",strtotime($logEmailsArys['dtClosedOn']))); 
                            }
                        }
                        
                        if($logEmailsArys['iBookingSearch']==1)
                        {
                            ?>
                            <div class="clearfix email-fields-container" style="width:100%;">
                                <span class="quote-field-container wd-10"><strong>From:</strong></span> 
                                <span class="quote-field-container wd-40"><?php echo $logEmailsArys['szFromLocation']; ?></span> 
                                <span class="quote-field-container wd-10"><strong>To: </strong></span>
                                <span class="quote-field-container wd-40"><?php echo $logEmailsArys['szToLocation']; ?></span>
                            </div> 
                            <?php
                                if($logEmailsArys['iQuickQuote']==1)
                                {
                                    ?>
                                    <div class="clearfix email-fields-container" style="width:100%;">
                                        <span class="quote-field-container wd-10"><strong>Type:</strong></span>
                                        <span class="quote-field-container wd-40"><?php echo "Quick quote by ".$logEmailsArys['szAdminFirstName']." ".$logEmailsArys['szAdminLastName']; ?></span> 
                                        <span class="quote-field-container wd-10"><strong>Created: </strong></span>
                                        <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?></span>
                                    </div>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <div class="clearfix email-fields-container" style="width:100%;">
                                        <span class="quote-field-container wd-10"><strong>Date & Cargo:</strong> </span>
                                        <span class="quote-field-container wd-40"><?php echo date('d. M Y',strtotime($logEmailsArys['dtTimingDate']))." - ".format_volume($logEmailsArys['fCargoVolume'])."cbm / ".number_format((float)$logEmailsArys['fCargoWeight'])."kg"; ?></span> 
                                        <span class="quote-field-container wd-10"><strong>Searched: </strong></span>
                                        <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])); ?></span>
                                    </div>
                                    <?php
                                }
                            ?> 
                            <div class="clearfix email-fields-container" style="width:100%;"><hr></div>
                            <?php
                        } 
                        else if($logEmailsArys['iIncomingEmail']==1) 
                        {  
                            echo display_crm_message_details($logEmailsArys,$kBooking,'PENDING_TRAY_LOG');
                        } 
                        else  if($logEmailsArys['iOverviewTask']==1) 
                        {  
                            echo display_overview_task_details($logEmailsArys,$kBooking,'PENDING_TRAY_LOG');
                        }
                        else  if($logEmailsArys['iReminder']==1) 
                        {    
                            $szAdminAddedBy = ""; 
                            if(!empty($logEmailsArys['szAdminName']))
                            {
                                $szAdminAddedBy = " - ".$logEmailsArys['szAdminName'] ;
                            }  
                        ?>
                        <div class="clearfix email-fields-container" style="width:100%;">
                            <span class="quote-field-container wd-10"><strong>Note:</strong> </span>
                            <span class="quote-field-container wd-40">
                                <?php if(strlen($logEmailsArys['szEmailBody'])>200){ ?>
                                <span id="display_less_content">
                                    <?php echo substr($logEmailsArys['szEmailBody'],0,197)."..."; ?><a href="javascript:void(0);" onclick="toggleReminderContent('display_full_content','display_less_content');">show more</a>
                                </span>
                                <span id="display_full_content" style="display:none;">
                                        <?php echo nl2br($logEmailsArys['szEmailBody']); ?>  <a href="javascript:void(0);" onclick="toggleReminderContent('display_less_content','display_full_content');">show less</a>
                                </span> 
                                <?php }else { echo nl2br($logEmailsArys['szEmailBody']); } ?> 
                            </span> 
                            <span class="quote-field-container wd-10"><strong>Added: </strong></span>
                            <span class="quote-field-container wd-40"><?php echo date('d-M-Y H:i',strtotime($logEmailsArys['dtSent'])).$szAdminAddedBy; ?></span>
                        </div> 
                        <div class="clearfix email-fields-container" style="width:100%;"><hr></div>
                    <?php  
                        } 
                        else 
                        {
                            echo display_crm_outgoing_emails($logEmailsArys,$kBooking,'PENDING_TRAY_LOG');
                        } 
                    }
                }
                else
                {
                    ?>
                    <div class="clearfix email-fields-container" style="width:100%;"><strong>No email sent yet.</strong></div>
                    <?php
                }
            ?> 
        </div> 
    </div> 
    <?php
} 

function regionsListingView($countryDetails)
{ 
    $t_base="management/country/";
?>
    <table cellspacing="0" cellpadding="0" border="0" class="format-4" width="99%" id="regionTable" > 
        <tr>
            <th width="30%"><span class="cursor-pointer"><?=t($t_base.'fields/region_name')?></span></th>
            <th width="30%"><span class="cursor-pointer"><?=t($t_base.'fields/short_name')?></span></th>
            <th width="10%" style="text-align:center"><span class="cursor-pointer"><?=t($t_base.'fields/act')?></span></th>
        </tr>
        <?php
	 if(!empty($countryDetails))
	 {
             
            foreach($countryDetails as $key=>$value)
            {
	?>
            <tr class="countryDetals" id="<?=$value['id'];?>" onclick="select_region_tr('<?=$value['id']."','".$value['id'];?>')" style="cursor: pointer;">
                <td><?=$value['szRegionName'];?></td>
                <td><?=$value['szRegionShortName'];?></td> 
                <td align="center"><?=($value['iActive'])?'Yes':'No';?></td> 
            </tr>
<?php
            }
	}
        else
        {
            ?>
            <tr>
                <td colspan="3" style="text-align: center;padding:10px;"><strong><?php echo t($t_base.'fields/no_record_found'); ?></strong></td>
            </tr>
            <?php
        }
	?>
    </table>
    <div style="float: right;padding-top: 10px;"  class="btn-container">
        <span id="cancel"></span>
        <a class="button2" id="deleteExchangeRates" style="opacity:0.4;"><span><?=t($t_base.'fields/cancel');?></span></a>
       <a class="button1" id="editExchangeRates" style="opacity:0.4;"><span><?=t($t_base.'fields/edit');?></span></a>
    </div>
<?php	  
}

function addRegionForm($kAdmin,$arrRegion = array())
{
    $t_base ='management/country/';
    $formId = 'add_edit_region_form';
    if(!empty($kAdmin->arErrorMessages))
    {  
        display_form_validation_error_message($formId,$kAdmin->arErrorMessages);
    }  
    if(!empty($_POST['arrRegion']))
    {
        $arrRegion = $_POST['arrRegion'] ;
        
        $szRegionName = $arrRegion['szRegionName'] ;
        $szRegionShortName = $arrRegion['szRegionShortName'] ;
        $iActive = $arrRegion['iActive'] ;  
         $idRegion = $arrRegion['idRegion'] ; 
    }
    else
    {
        $szRegionName = $arrRegion['szRegionName'] ;
        $szRegionShortName = $arrRegion['szRegionShortName'] ;
        $iActive = $arrRegion['iActive'] ; 
        $idRegion = $arrRegion['id'] ; 
    }
?>
    <form action="" mathod="post" id="add_edit_region_form" name="add_edit_region_form">
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'fields/region_name');?></span>
            <span class="field-container">	
                <input type="text" name="arrRegion[szRegionName]" id="szRegionName" value="<?php echo $szRegionName; ?>" class="addExchangeOnFocus" size="25" onfocus="addRegion();"/> 
            </span>
        </p>	
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'fields/region_short_name');?></span>
            <span class="field-container">	
                <input type="text" name="arrRegion[szRegionShortName]" id="szRegionShortName" value="<?php echo $szRegionShortName; ?>" class="addExchangeOnFocus" size="25" onfocus="addRegion();"/> 
            </span>
        </p> 
        <p class="profile-fields">
            <span class="field-name"><?=t($t_base.'messages/active');?></span>
            <span class="field-container">	
                <select name="arrRegion[iActive]" id="iActive" class="addExchangeOnChange" onchange="addRegion();">
                    <option value="1" <?php echo (($iActive==1)?'selected':''); ?>><?=t($t_base.'fields/yes');?></option>
                    <option value="0" <?php echo ((isset($iActive) && $iActive==0)?'selected':''); ?> ><?=t($t_base.'fields/no');?></option>
                </select>
            </span>
        </p>	 
        <input type="hidden" name="mode" id="SAVE_REGION" value="SAVE_REGION" />
        <input type="hidden" name="arrRegion[idRegion]" id="idRegion" value="<?php echo $idRegion; ?>" />
    </form>
<?php 
}  

function display_insurance_rate_reuse_confirmation($kInsurance,$insuranceAry=array())
{ 
    $t_base="management/insurance/";
    
    if(isset($_POST['reuseInsuranceRateAry']['idOriginCountry']))
    {
        $idOriginCountry = $_POST['reuseInsuranceRateAry']['idOriginCountry'] ;
    }
    else
    {
        $idOriginCountry = $insuranceAry['idOriginCountry'] ;
    }
    if(isset($_POST['reuseInsuranceRateAry']['idDestinationCountry']))
    {
        $idDestinationCountry = $_POST['reuseInsuranceRateAry']['idDestinationCountry'] ;
    }
    else
    {
        $idDestinationCountry = $insuranceAry['idDestinationCountry'] ;
    }
    if(isset($_POST['reuseInsuranceRateAry']['idTransportMode']))
    {
        $idTransportMode = $_POST['reuseInsuranceRateAry']['idTransportMode'] ;
    }
    else
    {
        $idTransportMode = $insuranceAry['idTransportMode'] ;
    }
    if(isset($_POST['reuseInsuranceRateAry']['iPrivate']))
    {
        $iPrivate = $_POST['reuseInsuranceRateAry']['iPrivate'] ;
    }
    else
    {
        $iPrivate = $insuranceAry['iPrivate'] ; ;
    }
    if(isset($_POST['reuseInsuranceRateAry']['idInsurance']))
    {
        $idInsurance = $_POST['reuseInsuranceRateAry']['idInsurance'] ;
    }
    else
    {
        $idInsurance = $insuranceAry['id'] ;
    }
            
    $kConfig = new cConfig();
    $transportModeListAry = array();
    $transportModeListAry = $kConfig->getAllTransportMode(false,false,true);

    $allCountriesArr = array();
    $allCountriesArr=$kConfig->getAllCountries(true); 
    $select_style = 'style="width:90%;"' ;
    
    $kInsurance_new = new cInsurance();
    $insuranceBuyRateAry = array();
    $insuranceBuyRateAry = $kInsurance_new->getInsuranceDetails(); 
 ?>
 <div id="popup-bg"></div>
<div id="popup-container" >
    <div class="popup" style="width:600px;">  
        <h3 align="center">
            <?php echo t($t_base.'title/Please_change'); ?>
        </h3>
        <h4 align="center">
            <?php echo t($t_base.'title/reason').": ".t($t_base.'title/reason_text')."<br>".t($t_base.'title/all_buy_sell_rate'); ?>
        </h4>
        <br/>
        <div id="reuse_form_container">
            <form action="javascript:void(0);" name="reuse_insurance_rate_form" id="reuse_insurance_rate_form">
            <table cellpadding="0" cellspacing="1" width="100%">
		<tr> 
                    <th style="width:15%;"><?=t($t_base.'fields/from')?></th>
                    <th style="width:15%;"><?=t($t_base.'fields/to')?></th>
                    <th style="width:15%;"><?=t($t_base.'fields/mode')?></th>
                    <th style="width:15%;"><?=t($t_base.'fields/private')?></th> 
                    <th style="width:30%;">&nbsp;</th>
		</tr>
		<tr> 
                    <td>
                        <select <?php echo $select_style; ?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="reuseInsuranceRateAry[idOriginCountry]" id="idOriginCountry" onchange="enable_save_button();">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idOriginCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td>
                        <select <?php echo $select_style; ?>  onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="reuseInsuranceRateAry[idDestinationCountry]" id="idDestinationCountry" onchange="enable_save_button();">
                            <option value="">Select</option>
                        <?php
                            if(!empty($allCountriesArr))
                            {
                                foreach($allCountriesArr as $allCountriesArrs)
                                {
                                    ?>
                                    <option value="<?php echo $allCountriesArrs['idCountry']; ?>" <?php echo (($idDestinationCountry==$allCountriesArrs['idCountry'])?'selected':''); ?>><?php echo $allCountriesArrs['szCountryName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td>
                        <select <?php echo $select_style; ?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="reuseInsuranceRateAry[idTransportMode]" id="idTransportMode" onchange="enable_save_button();">
                            <option value="">Select</option>
                        <?php
                            if(!empty($transportModeListAry))
                            {
                                foreach($transportModeListAry as $transportModeListArys)
                                {
                                    ?>
                                    <option value="<?php echo $transportModeListArys['id']; ?>" <?php echo (($idTransportMode==$transportModeListArys['id'])?'selected':''); ?>><?php echo $transportModeListArys['szShortName']?></option>
                                    <?php
                                }
                            }
                         ?>
                        </select>
                    </td>
                    <td>
                        <select <?php echo $select_style; ?> onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" name="reuseInsuranceRateAry[iPrivate]" id="iPrivate" onchange="enable_save_button();">
                            <option value="1" <?php echo ($iPrivate==1?'selected':''); ?>>Yes</option> 
                            <option value="0" <?php echo ($iPrivate==0?'selected':''); ?>>No</option> 
                        </select>
                    </td>
                    <td>
                        <a href="javascript:void(0)" class="button2" id="reuse_popup_save_button" ><span style="min-width:50px;"><?=t($t_base.'fields/save');?></span></a> 
                        <a href="javascript:void(0)" class="button1" onclick="showHide('insurance_rate_popup')"><span style="min-width:50px;"><?=t($t_base.'fields/cancel');?></span></a>
                    </td>
		</tr>
            </table> 
            <input type="hidden" name="reuseInsuranceRateAry[idInsurance]" id="idInsurance" value="<?php echo $idInsurance; ?>"> 
	</form>
            
            <br><br>
        <script type="text/javascript">
            var insuranceBuyRateAry = new Array();
            var i=0;
            <?php
                if(!empty($insuranceBuyRateAry))
                {
                    foreach($insuranceBuyRateAry as $insuranceBuyRateArys)
                    {
                        ?>
                        insuranceBuyRateAry[i] = '<?php echo $insuranceBuyRateArys['idOriginCountry']."|".$insuranceBuyRateArys['idDestinationCountry']."|".$insuranceBuyRateArys['idTransportMode']."|".$insuranceBuyRateArys['iPrivate']; ?>'
                        i++;
                        <?php
                    }
                }
            ?>
                function enable_save_button()
                {
                    var  formId = 'reuse_insurance_rate_form';
                    var error_count = 1;
                    if(isFormInputElementEmpty(formId, 'idOriginCountry')) 
                    {
                        error_count++;
                    }
                    else if(isFormInputElementEmpty(formId, 'idDestinationCountry')) 
                    {
                        error_count++;
                    }
                    else if(isFormInputElementEmpty(formId, 'idTransportMode')) 
                    {
                        error_count++;
                    }
                    else if(isFormInputElementEmpty(formId, 'iPrivate')) 
                    {
                        error_count++;
                    }
                    
                    if(error_count==1)
                    {
                        var idOriginCountry = parseInt($("#idOriginCountry").val()); 
                        var idDestinationCountry = parseInt($("#idDestinationCountry").val()); 
                        var idTransportMode = parseInt($("#idTransportMode").val()); 
                        var iPrivate = parseInt($("#iPrivate").val()); 
                        
                        var value_pipeline = idOriginCountry +"|"+idDestinationCountry+"|"+idTransportMode+"|"+iPrivate ;
                        
                        var iRecordFound = 1; 
                        for(var j=0;j<insuranceBuyRateAry.length;j++)
                        { 
                            if(insuranceBuyRateAry[j]===value_pipeline)
                            {
                                iRecordFound = 2;
                                break;
                            }
                        }
                        
                        if(iRecordFound==1)
                        {
                            $("#reuse_popup_save_button").attr("class",'button1'); 
                            $("#reuse_popup_save_button").unbind("click"); 
                            $("#reuse_popup_save_button").click(function(){submit_reuse_popup_form()});
                        }
                        else
                        {
                            $("#reuse_popup_save_button").attr("class",'button2'); 
                            $("#reuse_popup_save_button").unbind("click"); 
                        } 
                    }
                    else
                    {
                        $("#reuse_popup_save_button").attr("class",'button2'); 
                        $("#reuse_popup_save_button").unbind("click");  
                    }
                } 
                enable_save_button();
        </script>
            
        </div>  
    </div>
</div>
<?php 
}


function display_reset_quote_confirmation_popup($kBooking)
{
    $t_base="management/pendingTray/";    
    $szMode = 'CONFIRM_RESET_TASK_QUOTE_PANE'; 
    
    ?> 
    <div id="popup-bg" onclick="showHide('contactPopup')" style="cursor:pointer;"></div>
    <div id="popup-container">
        <div class="popup"> 
            <h5><?php echo t($t_base.'title/reset_quote_heading')?></h5> 
            <p><?php echo t($t_base.'title/reset_quote_message')?>?</p>
            <br>
            <div class="btn-container">
                <a href="javascript:void(0);" class="button1" onclick="showHide('contactPopup');"><span><?php echo t($t_base.'fields/cancel')?></span></a>
                <a href="javascript:void(0);" class="button1" onclick="display_task_menu_details('QUOTE','<?php echo $kBooking->idBookingFile; ?>','CONFIRM_RESET_TASK_QUOTE_PANE');"><span><?php echo t($t_base.'fields/confirm')?></span></a>
            </div>
        </div>
    </div>
    <?php
}

function display_close_task_confirmation_popup($kBooking)
{
    $t_base="management/pendingTray/";    
    $szMode = 'CONFIRM_RESET_TASK_QUOTE_PANE'; 
    
    ?> 
    <div id="popup-bg" onclick="showHide('contactPopup')" style="cursor:pointer;"></div>
    <div id="popup-container">
        <div class="popup"> 
            <h5><?php echo t($t_base.'title/close_task_heading')?></h5> 
            <p><?php echo t($t_base.'title/close_task_meassage')?>?</p>
            <br>
            <div class="btn-container">
                <a href="javascript:void(0);" class="button1" onclick="showHide('contactPopup');"><span><?php echo t($t_base.'fields/cancel')?></span></a>
                <a href="javascript:void(0);" class="button1" onclick="close_pending_task('<?php echo $kBooking->idBookingFile; ?>','CONFIRM_CLOSE_BOOKING_FILE');"><span><?php echo t($t_base.'fields/confirm')?></span></a>
            </div>
        </div>
    </div>
    <?php
}

function display_delete_quote_pricing_confirmation($idBookingQuote,$idBookingQuotePricing,$iRecordNumber)
{
    $t_base="management/pendingTray/";    
    $szMode = 'CONFIRM_DELETE_QUOTE_PRICING';  
    ?> 
    <div id="popup-bg" onclick="showHide('contactPopup')" style="cursor:pointer;"></div>
    <div id="popup-container">
        <div class="popup"> 
            <h5><?php echo t($t_base.'title/delete_quote_pricing_heading')?></h5> 
            <p><?php echo t($t_base.'title/delete_quote_pricing')?>?</p>
            <br>
            <div class="btn-container" style="text-align:center;">
                <a href="javascript:void(0);" class="button1" onclick="showHide('contactPopup');"><span><?php echo t($t_base.'fields/no_popup')?></span></a>
                <a href="javascript:void(0);" class="button1" onclick="deleteBookingQuote('<?php echo $iRecordNumber; ?>','<?php echo $idBookingQuote; ?>','<?php echo $idBookingQuotePricing; ?>','CONFIRM_DELETE_QUOTE_PRICING')"><span><?php echo t($t_base.'fields/yes_popup')?></span></a>
            </div>
        </div>
    </div>
    <?php
}


function transportecaInvestorsDetails($t_base,$transportecaManagement,$idAdmin)
{ 
	if($idAdmin>0)
	{
?>
        <?php echo selectPageFormat('Investors'); ?>
		
            <table cellpadding="0" id="management" cellspacing="0" border="0" class="format-4" width="100%">
		<tr>
                    <th width="25%" align="center" valign="top"><?=t($t_base.'fields/name')?></th> 
                    <th width="25%" align="center" valign="top"><?=t($t_base.'fields/l_name')?></th>
                    <th width="25%" align="center" valign="top"><?=t($t_base.'fields/email');?></th>
                    <th width="15%" align="center" valign="top"><?=t($t_base.'fields/last_login');?></th>
                    <th width="10%" align="center" valign="top"><?=t($t_base.'fields/verified');?></th>
		</tr>
		<?php
		$i=1;
		if(!empty($transportecaManagement))
		{
                    foreach($transportecaManagement as $admins)
                    {
                        if(!empty($admins['dtLastLogin']) && $admins['dtLastLogin']!='0000-00-00 00:00:00')
                        {
                            $dtLastLogin = date('d/m/Y',strtotime($admins['dtLastLogin']));
                        }
                        else
                        {
                            $dtLastLogin = 'N/A';
                        }
                       ?>
                        <tr id="admin<?php echo $i; ?>" onclick="changeManagementActiveStatus('<?php echo $admins['id']; ?>','<?php echo $admins['iActive'] ?>','<?php echo $admins['iMode']; ?>',this.id,'1');">
                            <td><?php echo $admins['szFirstName']; ?></td>
                            <td><?php echo $admins['szLastName']; ?></td>
                            <td><?php echo $admins['szEmail'] ?></td>
                            <td><?php echo $dtLastLogin; ?> </td>
                            <td><?php echo ($admins['iActive']?'Active':'Inactive') ?> </td>
                        </tr>
                        <?php
                       $i++;
                    }
		}
		?>
		</table>
            <br />
            <div style="float: right;">
                <a class="button1" id="new_admin" onclick="createNewAdminProfile('<?=$idAdmin?>','CREATE_NEW_INVESTOR_PROFILE')" style="opacity:1;"><span><?=t($t_base.'fields/new');?></span></a>
                <a class="button2" id="changeStatus" style="opacity:0.4;"><span><?=t($t_base.'fields/inactive');?></span></a>	
            </div> 
            <div style="clear: both;"></div>
	</div>
	<?php
    }
}

function createNewInvestorProfile($id,$t_base)
{ 
    $t_base=trim($t_base);
    $id=(int)sanitize_all_html_input($id);
    if($id>0)
    {
        ?>
        <div id="popup-bg"></div>
        <div id="popup-container">
            <div class="popup"> 
                <div id="Error"></div>		
                <h5><?=t($t_base.'title/create_new_investor');?></h5>
                <form id="createNewAdminProfile">
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/f_name');?></span>
                        <span class="field-container">
                            <input type="text" name="createProfile[fName]" style="width: 190px;">
                        </span>
                    </label>
                    <div style="clear: both;"></div>
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/l_name');?></span>
                        <span class="field-container">
                            <input type="text" name="createProfile[lName]" style="width: 190px;">
                        </span>
                    </label>
                    <div style="clear: both;"></div>
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/email');?></span>
                        <span class="field-container">
                            <input type="text" name="createProfile[eMail]" style="width: 190px;">
                        </span>
                    </label>
                    <div style="clear: both;"></div>
                    <label class="profile-fields">
                        <span class="field-name"><?=t($t_base.'fields/password');?></span>
                        <span class="field-container">
                            <input type="password" name="createProfile[password]" size="26" style="width: 190px;">
                        </span>
                    </label> 
                    <input type="hidden" name="flag" value="CREATE_NEW_INVESTORS_PROFILE_CONFIRM">
                    <div style="clear: both;"></div>
                </form>
                <br />
                <div class="oh">
                    <p align="center">	
                        <a href="javascript:void(0);" onclick="submitAdminProfileDetails(<?=$id?>)" class="button1"><span><?=t($t_base.'fields/confirm');?></span></a>			
                        <a href="javascript:void(0);" onclick="cancelEditMAnageVar(0)" class="button2"><span><?=t($t_base.'fields/cancel');?></span></a>			
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
} 
function display_forwarder_quote_form_courier_admin($kBooking,$counter,$quotesAry=array(),$disabled_flag=false,$totalQuantity)
{ 
    $t_base="management/pendingTray/";  
    $kConfig=new cConfig();
    $kCourierServices = new cCourierServices();
    $szDisabledStr = '';
    if($disabled_flag)
    {
        $szDisabledStr = 'disabled="disabled" ';
    }
    
    $szDisabled_str = 'disabled="disabled" ';
    $counterNew=$counter-1;
    $mon=date('m')-1;
    $date_picker_argument = "minDate: new Date(".date('Y').", ".$mon.", ".(date('d')).")" ;
    //echo $date_picker_argument;
     if((int)$kBooking->idBooking>0)
     {
         $idBooking=$kBooking->idBooking;
     }
     else
     {
         $idBooking=$_POST['addBookingCourierLabelAry']['idBooking'];
     }
    $kCourierServices = new cCourierServices();
    $cutoffLocalTimeAry = fColumnData();
    $bookingDataArr = $kBooking->getExtendedBookingDetails($idBooking);
    $idForwarder = $bookingDataArr['idForwarder'];
            
    if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__ && $bookingDataArr['isManualCourierBooking']=='1')
    { 
        $courierProductArr = array();
        $kLabels = new cLabels();
        $courierProductArr = $kLabels->getAvailableCourierProductByForwarder($idForwarder);
        $iProductsAvailableForCreateLabel = $kLabels->iProductsAvailableForCreateLabel; 
        if(!empty($courierProductArr))
        {
            $courierProductArr = sortArray($courierProductArr,'szName');
        }
        $div_width = "18%";
        $div_width_small = "10%";
    }
    else
    {
        $div_width = "22%";
        $div_width_small = "12%";
        $newCourierBookingAry = $kCourierServices->getAllNewBookingWithCourier($idBooking); 
        $idCourierProviderCompany = $newCourierBookingAry[0]['idProvider']; 
    }
            
    //echo $szDisabledStr;
   if($szDisabledStr=='')
   { 
?>  
    <script type="text/javascript">
	$(document).ready(function() {
  	var settings = {
	url: "<?php echo __MANAGEMENT_URL__?>/courierLabel/uploadLabel.php",
	method: "POST",
	allowedTypes:"pdf",
	fileName: "myfile",
	multiple: true,
        dragDropStr:'<span><b>Click to upload your PDF labels or drop them here!</b></span>',
	onSuccess:function(files,data,xhr)
 	{ 
//            console.log("Files");
//            console.log(files);
//            console.log("data");
//            console.log(data);
//            console.log("xhr");
//            console.log(xhr);
//            
            var IS_JSON = true;
            try
            {
                var json = $.parseJSON(data);
            }
            catch(err)
            {
                IS_JSON = false;
            } 
            
            if(IS_JSON)
            { 
                var obj = JSON.parse(data); 
                var iFileCounter = 1;

                var filecount=$("#filecount").attr('value');
                var filecountOld=$("#filecount").attr('value');
                
                $("#deleteArea").attr('style','display:block'); 
                $("#upload_process_list").attr('style','display:none');
                $("#upload_process_list").html(' ');
                $(".file_list_con").attr('style','display:block');

                for (var i = 0, len = obj.length; i < len; i++)
                { 
                    var newfilecount = parseInt(filecount) + iFileCounter;  
                    var uploadedFileName = obj[i]['name']; 
                    var FileUrl="<?php echo __BASE_STORE_URL__?>/courierLabel/"+uploadedFileName; 

                    var container_li_id = "id_"+newfilecount;

                    $('#fileList #namelist').append( '<li id="'+container_li_id+'" onclick="openPdfLightBox(\''+FileUrl+'\')" >&nbsp;</li>');  
                    $('#'+container_li_id).html( '<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=Page.png&temp=2&w=50&h=50'?>" border="0" /><span class="page">'+newfilecount+'</span>');
  
                    iFileCounter++; 
                    
                    var filename_all=$("#file_name").val();  
                    if(filename_all!='')
                    { 
                        var newvalue=filename_all+";"+files+"#####"+uploadedFileName;
                        $("#fileuploadlink").attr('style','display:block');
                        $("#file_uploade_plus_link").attr('style','display:block');
                    }
                    else
                    { 
                        var newvalue=files+"#####"+uploadedFileName;
                        $("#fileuploadlink").attr('style','display:block');	
                        $("#file_uploade_plus_link").attr('style','display:block');
                    } 

                    var textpage='page';
                    if(parseInt(newfilecount)>1)
                    {
                        textpage='pages';
                    }
                    textpage=newfilecount+" "+textpage; 
                    $("#iTotalPage").attr('value',textpage); 
                    $("#file_name").attr('value',newvalue);
                } 
                $("#filecount").attr('value',newfilecount); 

                if(parseInt(newfilecount)=='<?php echo $totalQuantity; ?>')
                {
                    $("#fileuploadlink").attr('style','display:none');
                    $("#file_uploade_plus_link").attr('style','display:none');
                } 
                $('div.ajax-upload-dragdrop:gt(0)').hide (); 
                
            }
            else
            {  
                //$("#upload_process_list").append("<p style='color:red;'>"+data+" ... <a hre='javascript:void(0);' onclick='showHide(\"upload_process_list\")'>close</a></p>");
            }
            enableSendForwarderCourierLabelButton();
            
	},onSelect:function(s){
            $("#upload_process_list").attr('style','display:block');
            $("#upload_error_container").attr('style','display:none');            
        },
        afterUploadAll:function()
        {
            //alert("all images uploaded!!");
        },
        onError: function(files,status,errMsg)
        {		
            //$("#status").html("<font color='red'>Upload is Failed</font>");
        }
    }
    <?php if($szDisabledStr==''){?>
    $("#mulitplefileuploader").uploadFile(settings);
            <?php }?>
    $("#drop").uploadFile(settings);
    <?php    if($_POST['addBookingCourierLabelAry']['filecount']>0){?> $('div.ajax-upload-dragdrop:gt(0)').hide();<?php }?>
  
});
enableSendForwarderCourierLabelButton();
</script>
<?php
   }
$counterNew=$counter-1;
    $mon=date('m')-1;
    $date_picker_argument = "minDate: new Date(".date('Y').", ".$mon.", ".(date('d')).")" ;
    //echo $date_picker_argument;
    $textpages="page";
    if($_POST['addBookingCourierLabelAry']['filecount']>1)
    {
        $textpages="pages";
    }
    $textpages=$_POST['addBookingCourierLabelAry']['filecount']." ".$textpages;
if($szDisabledStr=='')
   {
?> 
    <script type="text/javascript">
        jQuery(document).ready(function($){
         $("#namelist").sortable({
         connectWith: '#deleteArea'
         });
         $("#deleteArea").droppable({
         accept: '#namelist > li',
         hoverClass: 'dropAreaHover',
         drop: function(event, ui) {
         deleteImage(ui.draggable,ui.helper);
         },
         activeClass: 'dropAreaHover'
         });
         function deleteImage($draggable,$helper){
         params = $draggable.attr('id');
         var paramsArr=params.split("_");
         var idBooking=$("#idBooking").val();
         var idBookingFile=$("#idBookingFile").val();
         var file_name=$("#file_name").val();
         var value=$("#forwarder_courier_label_form").serialize();
	 var newValue=value+'&mode=DELETE_COURIER_LABEL_PDF&file_name='+file_name+'&counter='+paramsArr[1];
         $.post(__JS_ONLY_SITE_BASE__+"/ajax_pendingTray.php",newValue,function(result){
                  $("#pending_task_tray_container").html(result);
                 //$("#fileuploadlink").attr('style','display:block');
                  //$("#file_uploade_plus_link").attr('style','display:block');
          });
         $helper.effect('transfer', { to: '#deleteArea', className: 'ui-effects-transfer' },500);
         $draggable.remove();
         }
        });
    </script>
   <?php }
   ?>  

    <script type="text/javascript">
        $().ready(function(){
           $("#dtCollection").datepicker({<?php echo $date_picker_argument;?>}); 
        });
    </script>
    <div id="add_booking_quote_container_<?php echo $counterNew; ?>">
     <table class="format-6" width="100%" cellspacing="0" cellpadding="5">
    <tr>
    <td style="width:9%;padding:15px 0px 0px;border-right:none;text-align:center;max-width: 80%;" valign="top">
   	<img src="<?=__BASE_STORE_INC_URL__.'/image.php?img=label.png&temp=2&w=100&h=100'?>" border="0" />
    </td><td style="width:91%;padding:10px;" valign="top" id="labelformDiv">
   	<div class="clearfix submit-quote-form">
            <div class="all-price" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/master_tracking_number'); ?><br/>
                <input type="text" style="text-transform: uppercase;" class="send-forwarder-quote-class" <?php echo $szDisabledStr;?> name="addBookingCourierLabelAry[szMasterTrackingNumber]" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" id="szMasterTrackingNumber" value="<?php echo $_POST['addBookingCourierLabelAry']['szMasterTrackingNumber']; ?>" onkeyup="enableSendForwarderCourierLabelButton();">
            </div>
            <div class="vat" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/upload_labels_pdf'); ?><br/>
            <?php
            if($szDisabledStr=='')
            {
                //echo $_POST['addBookingCourierLabelAry']['filecount']."----".$totalQuantity;
                ?>      
            <div  id="drop" class="upload_labe_plus">
                
            </div>
            <?php }?> <input <?php echo $szDisabled_str; ?> type="text" class="send-forwarder-quote-class" readonly name="addBookingCourierLabelAry[iTotalPage]"  id="iTotalPage" value="<?php echo $textpages; ?>">
            </div>
            <?php 
            $iManualCourierBooking = 0;
            if($bookingDataArr['iBookingType']==__BOOKING_TYPE_RFQ__ && $bookingDataArr['isManualCourierBooking']=='1')
            { 
                $iManualCourierBooking = 1;
                $szTntServicesPipeLine = "";
                ?>
                <div class="transit-time" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/courier_product'); ?><br/>
                    <select id="idCourierProduct" name="addBookingCourierLabelAry[idCourierProduct]" <?php echo $szDisabledStr;?> onchange="enableSendForwarderCourierLabelButton();showCourierProductAttentionPopup(this.value,'<?php echo $_POST['addBookingCourierLabelAry']['idCourierProduct'];?>');">
                        <option value="">Select</option>
                        <?php
                            if(!empty($courierProductArr))
                            {
                                foreach($courierProductArr as $courierProductArrs)
                                {
                                    $idCourierProduct = $courierProductArrs['idCourierProviderProductid']."-".$courierProductArrs['idCourierAgreement']; 
                                    if($courierProductArrs['idCourierProvider']==3 && $iProductsAvailableForCreateLabel==1)
                                    {
                                        $szTntServicesPipeLine .= $idCourierProduct.";";
                                    }
                                    ?>
                                    <option value="<?php echo $idCourierProduct;?>" <?php if($_POST['addBookingCourierLabelAry']['idCourierProduct']==$idCourierProduct){?> selected <?php }?>><?php echo $courierProductArrs['szName'];?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select> 
                </div>
            <?php } ?>
            <input type="hidden" name="addBookingCourierLabelAry[iManualCourierBooking]" id="iManualCourierBooking" value="<?php echo $iManualCourierBooking; ?>">
            <input type="hidden" name="addBookingCourierLabelAry[idCourierProviderCompany]" id="idCourierProviderCompany" value="<?php echo $idCourierProviderCompany; ?>">
            <input type="hidden" name="addBookingCourierLabelAry[szTntServicesPipeLine]" id="szTntServicesPipeLine" value="<?php echo $szTntServicesPipeLine; ?>">
            
            <div class="transit-time" style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/collection'); ?><br/>
            	<select id="iCollection" name="addBookingCourierLabelAry[iCollection]" <?php echo $szDisabledStr;?> onchange="enableDisableDateTimeField(this.value);enableSendForwarderCourierLabelButton();">
                    <option value="">Select</option>
                    <option value="Scheduled" <?php if($_POST['addBookingCourierLabelAry']['iCollection']=='Scheduled'){?> selected <?php }?>>Scheduled</option>
                    <option value="Not-scheduled" <?php if($_POST['addBookingCourierLabelAry']['iCollection']=='Not-scheduled'){?> selected <?php }?>>Not scheduled</option>
                </select>
            </div>
            <div class="valid" style="width:<?php echo $div_width_small;?>"><?php echo t($t_base.'fields/date'); ?><br/>
            	<input type="text" class="send-forwarder-quote-class" <?php if($_POST['addBookingCourierLabelAry']['iCollection']!='Scheduled' && $szDisabledStr==''){ echo $szDisabled_str; }?> <?php echo $szDisabledStr;?>  readonly="" name="addBookingCourierLabelAry[dtCollection]" onfocus="check_form_field_not_required(this.form.id,this.id);" id="dtCollection" value="<?php echo !empty($_POST['addBookingCourierLabelAry']['dtCollection'])?$_POST['addBookingCourierLabelAry']['dtCollection']:'N/A'; ?>">
            </div>
            <div class="currency"  style="width:<?php echo $div_width;?>"><?php echo t($t_base.'fields/time_between'); ?><br/>
                <div class="clearfix">
            	<select <?php if($_POST['addBookingCourierLabelAry']['iCollection']!='Scheduled' && $szDisabledStr==''){ echo $szDisabled_str; }?>  id="dtCollectionStartTime" <?php echo $szDisabledStr;?> onchange="changeEndTimeValue(this.value,'<?php echo implode(";",$cutoffLocalTimeAry)?>');enableSendForwarderCourierLabelButton();" name="addBookingCourierLabelAry[dtCollectionStartTime]" onchange="changeEndTimeValue(this.value,'<?php echo implode(";",$cutoffLocalTimeAry);?>')">
                    <option value=""></option>
                    <?php 
                    $t=0;
                    if(!empty($cutoffLocalTimeAry)){
                        foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
                        {?>    
                            <option value="<?php echo $t;?>"  <?php if($_POST['addBookingCourierLabelAry']['dtCollectionStartTime']!='' && $_POST['addBookingCourierLabelAry']['dtCollectionStartTime']==$t){?> selected <?php }?>><?php echo $cutoffLocalTimeArys;?></option>
                    <?php 
                    
                        ++$t;
                        }}?>                    
                </select><span>-</span><select <?php echo $szDisabledStr;?> <?php if($_POST['addBookingCourierLabelAry']['iCollection']!='Scheduled' && $szDisabledStr==''){ echo $szDisabled_str; }?>  onchange="enableSendForwarderCourierLabelButton();" id="dtCollectionEndTime" name="addBookingCourierLabelAry[dtCollectionEndTime]">
                    <option value=""></option>
                    <?php 
                    if(!empty($cutoffLocalTimeAry)){
                        $k=$_POST['addBookingCourierLabelAry']['dtCollectionStartTime'];
                        foreach($cutoffLocalTimeAry as $cutoffLocalTimeArys)
                        {   
                            if($t1>=$k)
                            {
                            ?>    
                            <option value="<?php echo $t1;?>" <?php if($_POST['addBookingCourierLabelAry']['dtCollectionEndTime']!='' && $_POST['addBookingCourierLabelAry']['dtCollectionEndTime']==$t1){?> selected <?php }?>><?php echo $cutoffLocalTimeArys;?></option>
                    <?php 
                            }
                        ++$t1;
                        }}?>                    
                </select>
                </div>
            </div>
            
        </div>
                        <?php
                         if($szDisabledStr=='' )
                         {?>
                       <!-- <div class="upload_label" id="fileuploadlink">
                            <div class="clearfix">

                                        <span id="nofile-selected"><?=t($t_base.'fields/click_to_upload_your_pdf');?></span>
                            <input id="file_upload" width="120" type="file" height="30" name="file_upload"  />
			
                            </div>
			</div>-->
                         <?php }?>
                       <?php
                         if($szDisabledStr=='')
                         {?>
                        <div id="upload_error_container" style="display:none;"></div>
                        <div id="mulitplefileuploader" class="upload_label" >
				Upload

                            
			</div>
                        <?php }?>
                       <div id="upload_process_list" style="display:none;"></div>
                       <div class="file_list_con" <?php if($_POST['addBookingCourierLabelAry']['filecount']==0){?>style="display:none;" <?php }?>>
                        <div id="fileList">
			 <ul id="namelist" class="ui-sortable clearfix"> 
                             
			<?php 
				if(!empty($_POST['addBookingCourierLabelAry']['file_name']))
				{
					$allUploadedFileArr=explode(";",$_POST['addBookingCourierLabelAry']['file_name']);
					if(!empty($allUploadedFileArr))
					{
                                                $t=1;    
						for($i=0;$i<count($allUploadedFileArr);$i++)
						{
							$uploadFileArr=explode("#####",$allUploadedFileArr[$i]);
                                                        $fileUrl=__BASE_STORE_URL__."/courierLabel/".$uploadFileArr[1];    
							echo "<li id='id_".$t."_".$uploadFileArr[2]."' onclick='openPdfLightBox(\"".$fileUrl."\");'><img src='".__BASE_STORE_INC_URL__."/image.php?img=Page.png&temp=2&w=50&h=50' ><span class='page'>".$t."</span></span>";							
                                                        ++$t;
						}
					}
				}else{
			?>	
				
			
			<?php
				}
			?>
                         </ul>
                           
			</div>  
                         <?php
                            if($szDisabledStr=='')
                            {
                                //echo $_POST['addBookingCourierLabelAry']['filecount']."----".$totalQuantity;
                               ?> 
                           <div id="deleteArea" <?php if($_POST['addBookingCourierLabelAry']['filecount']==0){?>style="display:none;"<?php }?>>
                                <img src="<?php echo __BASE_STORE_INC_URL__?>/image.php?img=Trash.png&temp=2&w=50&h=50" title="Drag here to delete." alert="Drag here to delete."/>
                            </div>
                           <?php
                            }?>
                       </div>
                        <input type="hidden" name="addBookingCourierLabelAry[filecount]" id="filecount" value="<?=(($_POST['addBookingCourierLabelAry']['filecount'])?$_POST['addBookingCourierLabelAry']['filecount']:"0")?>">			
			<input type="hidden" name="addBookingCourierLabelAry[file_name]" id="file_name" value="<?=$_POST['addBookingCourierLabelAry']['file_name']?>">
			
    </table>
    </div>
   <?php
} 


function displayPayForwarderInvoices($unpaidMarkupBookingsAry,$iPaidInvoices=false,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)
{	
    $t_base="management/forwarderpayment/";
    $kAdmin = new cAdmin; 
    $iGrandTotal=count($unpaidMarkupBookingsAry);
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
        //echo $limit;
        if($iPage==1)
        {
            $start=0;
            $end=$limit;
            if($iGrandTotal<$end)
            {
                $end=$iGrandTotal;
            }
        }
        else
        {
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            $startValue=$start+1;
            if($startValue>$iGrandTotal)
            {
                $iPage=$iPage-1;
                $start=$limit*($iPage-1);
                $end=$iPage*$limit;
                if($end>$iGrandTotal)
                {
                    $end=$iGrandTotal;
                }

            }
        }
    ?>
    <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
        <tr>
            <td width="35%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/forwarder')?></strong></td>
            <td width="8%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/booking')?></strong></td>
            <td width="8%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/date')?></strong></td>
            <td width="12%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/quote')?></strong></td>
            <td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/vat')?></strong> </td>
            <td width="10%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/action')?></strong> </td>
        </tr>
        <?php
            if(!empty($unpaidMarkupBookingsAry))
            {	
                $ctr=1;
                for($i=$start;$i<$end;++$i)
                { 
                ?>
                    <tr align='center'>
                        <td style='text-align:left;'><?php echo returnLimitData($unpaidMarkupBookingsAry[$i]['szDisplayName'],38)." (".$unpaidMarkupBookingsAry[$i]['szForwarderAlias'].")"; ?></td>
                        <td style='text-align:left;'><a class="booking-ref-link" href="<?php echo __MANAGEMENT_OPERATIONS_PENDING_TRAYS__;?><?php echo $unpaidMarkupBookingsAry[$i]['szBookingRandomNum'];?>/"><?php echo $unpaidMarkupBookingsAry[$i]['szBookingRef']; ?></a></td>
                        <td style='text-align:left;'><?php echo date('d/m/Y',strtotime($unpaidMarkupBookingsAry[$i]['dtBookingConfirmed'])); ?></td>
                        <td style='text-align:left;'><?php echo $unpaidMarkupBookingsAry[$i]['szForwarderQuoteCurrency']." ".number_format((float)$unpaidMarkupBookingsAry[$i]['fForwarderTotalQuotePrice'],2); ?></td>
                        <td style='text-align:left;'><?php echo $unpaidMarkupBookingsAry[$i]['szForwarderQuoteCurrency']." ".number_format((float)$unpaidMarkupBookingsAry[$i]['fTotalVatForwarderCurrency'],2); ?></td>
                        <td style='text-align:left;'>
                            <?php if($iPaidInvoices) {?>
                                <a href="javascript:void(0)" onclick="pay_forwarder_invoice('<?php echo $unpaidMarkupBookingsAry[$i]['idBooking']; ?>');"><?=t($t_base.'fields/changes')?></a>
                            <?php } else {?>
                                <a href="javascript:void(0)" onclick="pay_forwarder_invoice('<?php echo $unpaidMarkupBookingsAry[$i]['idBooking']; ?>');"><?=t($t_base.'fields/pay_now')?></a> 
                            <?php }?>
                        </td>
                    </tr>
                    <?php
                }	  
            }
            else
            {
                echo "<tr><td colspan='6'><CENTER><b>NO RECORD FOUND</b><CENTER></td></tr>";
            }
        ?>
    </table> 
    <div class="page_limit clearfix">
        <div class="pagination">
    <?php

        $kPagination->szMode = "DISPLAY_BOOKING_LIST_PAGINATION";
        //$kPagination->idBookingFile = $idBookingFile;
        $kPagination->paginate('display_booking_list_pagination'); 
        if($kPagination->links_per_page>1)
        {
            echo $kPagination->renderFullNav();
        }        
    ?>
       </div>
       <div class="limit">Records per page
           <select id="limit" name="limit" onchange="display_booking_list_pagination('1')">
               <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
           </select>
       </div>    
    </div>

    <input type="hidden" id="page" name="page" value="<?php echo $iPage;?>">
<?php
}

function display_pay_forwarder_invoice_popup($postSearchAry,$kBilling=false)
{
    $t_base="management/forwarderpayment/"; 
    $kforwarder = new cForwarder();
    $kConfig = new cConfig();
    $kforwarder->load($postSearchAry['idForwarder']); 
    
    $allCurrencyArr = array();
    $allCurrencyArr = $kConfig->getBookingCurrency(false,true,false,false,false,true);
    
    $date_picker_argument = "minDate: new Date(".date('Y').", ".date('m')."-1, ".(date('d')+1).")" ;
    
    if(!empty($_POST['confirmForwarderPayment']))
    {
        $confirmForwarderPayment = $_POST['confirmForwarderPayment'];
        $idForwarderQuoteCurrency = $confirmForwarderPayment['idForwarderQuoteCurrency'];
        $fForwarderTotalQuotePrice = $confirmForwarderPayment['fForwarderTotalQuotePrice'];
        $fTotalVatForwarderCurrency = $confirmForwarderPayment['fTotalVatForwarderCurrency'];
        $dtMarkupPaid = $confirmForwarderPayment['dtMarkupPaid'];
        $iMarkupPaid = $confirmForwarderPayment['iMarkupPaid'];
        $fForwarderQuoteExchangeRate = $confirmForwarderPayment['fForwarderQuoteExchangeRate'];
        $szMarkupPaymentComments = $confirmForwarderPayment['szMarkupPaymentComments'];
    }
    else
    {
        $idForwarderQuoteCurrency = $postSearchAry['idForwarderQuoteCurrency'];
        $fForwarderTotalQuotePrice = $postSearchAry['fForwarderTotalQuotePrice'];
        $fTotalVatForwarderCurrency = $postSearchAry['fTotalVatForwarderCurrency'];
        $iMarkupPaid = round((float)$postSearchAry['iMarkupPaid']); 
        if(!empty($postSearchAry['dtMarkupPaid']) && $postSearchAry['dtMarkupPaid']!='0000-00-00 00:00:00')
        { 
            $dtMarkupPaid = date('d/m/Y',strtotime($postSearchAry['dtMarkupPaid']));
        }
        else
        {
            $dtMarkupPaid = date('d/m/Y');
        }
        $fForwarderQuoteExchangeRate = $postSearchAry['fForwarderQuoteCurrencyExchangeRate'];
        $szMarkupPaymentComments = $postSearchAry['szMarkupPaymentComments']; 
    }
    
    $fTotalPriceUSD = $postSearchAry['fTotalPriceUSD'];
    $idQuotePricingDetails = $postSearchAry['idQuotePricingDetails'];
    $quotePricingAry = array();
    $kBooking_new = new cBooking(); 
    if($idQuotePricingDetails>0)
    {  
        $priceSearchAry = array();
        $priceSearchAry['idBookingQuotePricing'] = $idQuotePricingDetails;
        $quotePricingAry = $kBooking_new->getAllBookingQuotesPricingDetails($priceSearchAry);
        if(!empty($quotePricingAry[1]))
        {
            $fTotalPriceForwarderCurrency = $quotePricingAry[1]['fTotalPriceForwarderCurrency'];
            $fForwarderCurrencyExchangeRate = $quotePricingAry[1]['fForwarderCurrencyExchangeRate'];
            
            $fActualQuoteByForwarderUSD = ($fTotalPriceForwarderCurrency * $fForwarderCurrencyExchangeRate);
            $fTotalBudgetReferalAmount = $fTotalPriceUSD - $fActualQuoteByForwarderUSD ; 
            if($fActualQuoteByForwarderUSD>0)
            {
                $fTotalBudgetReferalPerentage = ($fTotalBudgetReferalAmount * 100 ) / $fActualQuoteByForwarderUSD ;
                $fTotalBudgetReferalPerentage = round((float)$fTotalBudgetReferalPerentage);
            }
            $fTotalBudgetReferalAmount = number_format((float)$fTotalBudgetReferalAmount);
        } 
    }
    
    $bookingDataAry = array();
    $bookingDataAry = $kBooking_new->getExtendedBookingDetails($postSearchAry['id']);
    $bookingCurrencyAry = array();
    ?>
    <div id="popup-bg"></div>
    <div id="popup-container">
        <script type="text/javascript">
            $().ready(function() {	
                $("#dtMarkupPaid").datepicker(); 
                enableConfirmButton();
            }); 
        </script>
        <div class="popup" style="width:550px;" id="task-popup-id-recent"> 
            <form name="payForwarderInvoiceForm" id="payForwarderInvoiceForm" method="post" action="javascript:void(0);">	
                <h5><?php echo t($t_base.'messages/pay')." ".ucfirst($kforwarder->szDisplayName)." (".$kforwarder->szForwarderAlies.") ".t($t_base.'messages/invoice'); ?></h5>  
                <p class="oh">
                    <span class="fl-60" style="width:100%;">
                        <?php echo $postSearchAry['szTransportMode']." ".t($t_base.'messages/from')." ".$bookingDataAry['szShipperCity']." ".t($t_base.'messages/to')." ".$bookingDataAry['szConsigneeCity']; ?>
                        <?php echo t($t_base.'messages/for')." ".$bookingDataAry['szFirstName']." ".$bookingDataAry['szLastName'].", ".t($t_base.'messages/booked_under')." ".$bookingDataAry['szBookingRef']." ".date('d M. Y',strtotime($bookingDataAry['dtBookingConfirmed']))."."; ?>
                    </span> 
                </p>  
                <p class="oh">
                    <span class="fl-60" style="width:100%;"><?php echo t($t_base.'messages/quote')." ".$postSearchAry['szForwarderQuoteCurrency']." ".number_format((float)$postSearchAry['fForwarderTotalQuotePrice'])." ".t($t_base.'messages/plus_vat_of')." ".$postSearchAry['szForwarderCurrency']." ".number_format((float)$postSearchAry['fTotalVatForwarderCurrency']);?></span> 
                </p>  
                <p class="oh">
                    <span class="fl-60" style="width:100%;">
                        <table class="format-6" style="width:100%" cellpadding="0"  cellspacing="0" border="0">
                            <tr>
                                <td style="width:40%" colspan="2"><?php echo t($t_base.'fields/invoice_value') ?></td>
                                <td style="width:30%"><?php echo t($t_base.'fields/vat') ?></td>
                                <td style="width:30%"><?php echo t($t_base.'fields/payment_date') ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="confirmForwarderPayment[idForwarderQuoteCurrency]" id="idForwarderQuoteCurrency" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableConfirmButton();" onchange="updateActualProfit();">
                                        <option value=""><?=t($t_base.'fields/select');?></option>
                                        <?php
                                            if(!empty($allCurrencyArr))
                                            {
                                                foreach($allCurrencyArr as $allCurrencyArrs)
                                                {
                                                    ?><option value="<?=$allCurrencyArrs['id']?>" <?php echo ($idForwarderQuoteCurrency ==  $allCurrencyArrs['id']) ? "selected":""?>><?=$allCurrencyArrs['szCurrency']?></option><?php 
                                                    $bookingCurrencyAry[$allCurrencyArrs['id']] = $allCurrencyArrs['fUsdValue']; 
                                                }
                                            }
                                        ?>
                                    </select> 
                                </td>
                                <td>
                                    <input type="text" onkeyup="updateActualProfit();" onkeypress="return format_decimat(this,event)" name="confirmForwarderPayment[fForwarderTotalQuotePrice]" id="fForwarderTotalQuotePrice" value="<?php echo $fForwarderTotalQuotePrice; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableConfirmButton();">
                                </td>
                                <td>
                                    <input type="text" onkeyup="updateActualProfit();" onkeypress="return format_decimat(this,event)" name="confirmForwarderPayment[fTotalVatForwarderCurrency]" id="fTotalVatForwarderCurrency" value="<?php echo $fTotalVatForwarderCurrency; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableConfirmButton();">
                                </td>
                                <td>
                                    <input type="text" name="confirmForwarderPayment[dtMarkupPaid]" id="dtMarkupPaid" value="<?php echo $dtMarkupPaid; ?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);enableConfirmButton();" readonly >
                                </td>
                            </tr>
                        </table>
                    </span> 
                </p>  
                <br/>
                <p class="oh budget-text-container"> 
                    <span>Budget GP USD <?php echo $fTotalBudgetReferalAmount; ?> (<?php echo $fTotalBudgetReferalPerentage; ?>%)</span><br>
                    <span>Actual GP <span id="actual-gp-container-span"></span></span>
                </p>
                <p class="oh">
                    <textarea name="confirmForwarderPayment[szMarkupPaymentComments]" id="szMarkupPaymentComments" cols="200" rows="3" placeholder="Please add your comments"><?php echo $szMarkupPaymentComments; ?></textarea>
                </p>
                <br/>
                <?php if($kBilling->iConfirmError==1){ ?>
                    <p class="oh red_text" id="error_message_conatiner_p"><?php echo $kBilling->szConfirmationSpecialError; ?></p>
                    <input type="hidden" name="confirmForwarderPayment[iConfirmError]" id="iConfirmError" value="1">
                <?php }?>
                <p align="center">
                    <a href="javascript:void(0)" class="button1" id="confirm_referral_payment_admin"><span><?=t($t_base.'fields/confirm');?></span></a>
                    <a href="javascript:void(0)" class="button2" onclick="showHide('contactPopup');"><span><?=t($t_base.'fields/cancel');?></span></a> 
                </p>
                <input type="hidden" name="confirmForwarderPayment[idBooking]" id="idBooking" value="<?php echo $postSearchAry['id']; ?>">
                <input type="hidden" name="confirmForwarderPayment[iMarkupPaid]" id="iMarkupPaid" value="<?php echo $iMarkupPaid; ?>"> 
                <input type="hidden" name="confirmForwarderPayment[fTotalPriceUSD]" id="fTotalPriceUSD" value="<?php echo $fTotalPriceUSD; ?>">
                <input type="hidden" name="confirmForwarderPayment[fForwarderQuoteExchangeRate]" id="fForwarderQuoteExchangeRate" value="<?php echo $fForwarderQuoteExchangeRate; ?>">
            </form>  
            <script type="text/javascript"> 
                var currencyListAry = new Array(); 
                <?php 
                    if(!empty($bookingCurrencyAry))
                    { 
                        foreach($bookingCurrencyAry as $idCurrency=>$fExchangeRate)
                        { 
                            if($idCurrency>0)
                            { 
                            ?>
                            currencyListAry['<?php echo $idCurrency; ?>'] = '<?php echo $fExchangeRate; ?>' 
                            <?php
                            }
                        }
                    }
                ?> 
                var fTotalPriceUSD = '<?php echo $fTotalPriceUSD; ?>';   
                function updateActualProfit(first_load)
                {  
                    if(first_load!=1)
                    {
                        $("#iConfirmError").val('0');
                        $("#error_message_conatiner_p").attr('style','display:none;');
                    } 
                    var fForwarderTotalQuotePrice = $("#fForwarderTotalQuotePrice").val();
                    var idForwarderQuoteCurrency = $("#idForwarderQuoteCurrency").val();

                    fForwarderTotalQuotePrice = parseFloat(fForwarderTotalQuotePrice);
                    idForwarderQuoteCurrency = parseInt(idForwarderQuoteCurrency);
                    if(idForwarderQuoteCurrency==1)
                    {
                        var fForwarderQuoteExchangeRate = 1; 
                    }
                    else
                    {
                        var fForwarderQuoteExchangeRate = currencyListAry[idForwarderQuoteCurrency];
                    }
                    //console.log(currencyListAry); 
                    
                    var fForwarderTotalQuotePriceUSD = 0; 
                    fForwarderTotalQuotePriceUSD = fForwarderTotalQuotePrice * fForwarderQuoteExchangeRate ; 
                    
                    //fForwarderTotalQuotePriceUSD = Math.round(fForwarderTotalQuotePriceUSD);
                    console.log("Inv: "+fTotalPriceUSD+" Qt: "+fForwarderTotalQuotePriceUSD+" curr: "+idForwarderQuoteCurrency+" Exchange rate: "+fForwarderQuoteExchangeRate);
                    var fReferralAmountUSD = fTotalPriceUSD - fForwarderTotalQuotePriceUSD ; 
                    var fReferalPercentage = 0;
                    if(fForwarderTotalQuotePriceUSD>0)
                    {
                        fReferalPercentage = (fReferralAmountUSD * 100 ) / fForwarderTotalQuotePriceUSD;
                    } 
                    fReferralAmountUSD = Math.round(fReferralAmountUSD,2);
                    fReferalPercentage = Math.round(fReferalPercentage,2); 
                    
                    fReferralAmountUSD = parseFloat(fReferralAmountUSD);
                    var szActualPriceString = "USD "+fReferralAmountUSD+" ("+fReferalPercentage+"%)";
                    $("#actual-gp-container-span").html(szActualPriceString);
                    
                    if(fReferralAmountUSD<0)
                    {
                        $("#actual-gp-container-span").addClass('red_text');
                    } 
                    else
                    {
                        $("#actual-gp-container-span").removeClass('red_text');
                    }
                    console.log("USD: "+fReferralAmountUSD+" Val: "+fReferalPercentage+" %");
                    enableConfirmButton();
                }
                updateActualProfit(1);
            </script>
        </div>
    </div>	
    <?php
}
/**
 * This function contains the  scripts to generate graph
 * @param Array, string, int
 * @author Ajay
 */
function showAdminDashboardGraph($graph_data_ary,$szGraphName,$div_id=1)
{	 
    if(!empty($graph_data_ary))
    {
        $loop_counter = count($graph_data_ary);
        $counter_1=0;
        $szMonthKeyStr=''; 
        
        foreach($graph_data_ary as $key=>$value)
        {  
            $szMonthKeyStr .= "'".$key."'" ;  
            $szDataStr .= $value; 
            
            $counter_1++;
            if($loop_counter != $counter_1)
            {
                $szMonthKeyStr .=", ";
                $szDataStr .=',';
            }   
        }
        
        $szSearieText = " { 
            name: '".$szGraphName."', 
            data: [".$szDataStr."]
         } ";
    }     
    
    ?>
	 
    <script type="text/javascript"> 
    $().ready(function() { 
        $('#container_'+<?php echo $div_id; ?>).highcharts({ 
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                x: -20 //center
            }, 
            xAxis: {
                categories: [<?php echo $szMonthKeyStr;?>]
            },
            credits: {
                enabled: false
            },
            yAxis: { 
                title: {
                    text: '<?php echo $szGraphName; ?>'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            }, 
            series: [<?php echo $szSearieText; ?>] 
        }); 
    });     
    </script> 
    <div id="container_<?php echo $div_id; ?>" style="width: 100%; height: 300px; margin: 0 auto"></div>
    <script type="text/javascript">
     $(".highcharts-button").attr('style','display:none;');
     //document.getElementsByTagName("rect").style='display:none'; 
    </script> 
    <?php 
}


/**
 * This function contains the  scripts to generate graph
 * @param Array, string, int
 * @author Ajay
 */
function showAdminDashboardSaleRevenueGraph($graph_data_ary,$graph_turnoverdata_ary,$szGraphName,$div_id=1)
{	 
    if(!empty($graph_data_ary))
    {
        $loop_counter = count($graph_data_ary);
        $counter_1=0;
        $szMonthKeyStr=''; 
        
        foreach($graph_data_ary as $key=>$value)
        {  
            $szMonthKeyStr .= "'".$key."'" ;  
            $szDataStr .= "['".$key."',".($value*1000)."]"; 
            
            $counter_1++;
            if($loop_counter != $counter_1)
            {
                $szMonthKeyStr .=", ";
                $szDataStr .=',';
            }   
        }
        
        $szSearieText = " { 
            name: 'Turnover', 
            data: [".$szDataStr."]
         }";
    }     
    if($graph_turnoverdata_ary)
    {
        $szDataStr = ''; 
        $loop_counter = count($graph_turnoverdata_ary);
        $counter_1=0;
        foreach($graph_turnoverdata_ary as $key=>$value)
        {   
            //$szDataStr .= $value; 
            $szDataStr .= "['".$key."',".$value."]"; 
            
            $counter_1++;
            if($loop_counter != $counter_1)
            { 
                $szDataStr .=',';
            }   
        }
        $szSearieText_1 = ", { 
            name: 'Sale Revenue', 
            data: [".$szDataStr."]
         } ";
    }
    ?>
	 
    <script type="text/javascript"> 
    $().ready(function() { 
        $('#container_'+<?php echo $div_id; ?>).highcharts({ 
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                x: -20 //center
            }, 
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }, 
            yAxis: {
                min: 0,
                title: {
                    text: 'Values (USD)'
                }
            }, 
            legend: {
                enabled: true
            }, 
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>$ {point.y:.1f}</b>'
            },
            series: [<?php echo $szSearieText."".$szSearieText_1; ?>], 
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }); 
    });     
    </script> 
    <div id="container_<?php echo $div_id; ?>" style="width: 100%; height: 300px; margin: 0 auto"></div>
    <script type="text/javascript">
     $(".highcharts-button").attr('style','display:none;');
     //document.getElementsByTagName("rect").style='display:none'; 
    </script> 
    <?php 
} 
 
function deleteSearchAidWordsConfirmation($idSearchAdwords)
{
   $t_base="management/landingPageData/";  
   ?>
   <div id="popup-bg"></div>
   <div id="popup-container">	
       <div class="popup contact-popup" style="height: auto;">
           <?php echo t($t_base.'title/are_you_sure_to_delete_aid_words'); ?>
           <br /><br />
           <p align="center">	
               <a href="javascript:void(0);" onclick="update_aid_words('<?php echo $idSearchAdwords; ?>','DELETE_SEARCH_AID_WORDS_CONFIRM')" class="button1"><span><?php echo t($t_base.'fields/ok'); ?></span></a>			
               <a href="javascript:void(0);" onclick="closeConactPopUp(0)" class="button2"><span><?php echo t($t_base.'fields/cancel');?></span></a>			
           </p>
       </div>
   </div> 
   <?php 
} 
 
 function display_email_template_body($kBooking,$idTemplate=false,$iLanguage=false,$iSuccess=false,$szFromPage=false,$logEmailsArys=array(),$szAction=false)
 { 
    $idBooking = $kBooking->idBooking ; 
    $idOfflineChat = $kBooking->idOfflineChat ;
    $kConfig = new cConfig();
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container";
    $t_base="management/pendingTray/";
    $replace_ary = array();
    $kBooking_new  = new cBooking();
    $kBooking_new = $kBooking; //Creating object clone
            
    $bAttachmentAvailable = false;
    if($szFromPage=='CRM_TEMPLATE')
    {
        $addSnoozeAry = $_POST['addCrmOperationAry'];  
        $szArrayName = 'addCrmOperationAry';
        
        $szFormId = "crm_email_operations_form_".$logEmailsArys['id'];  
        $idCrmEmail = $logEmailsArys['id']; 
        $szIdSuffix = "_".$idCrmEmail;
    }
    else
    {
        $szFormId = "rfq-reminder-notes-form";   
        $addSnoozeAry = $_POST['addSnoozeAry'];  
        $szArrayName = 'addSnoozeAry'; 
        $szIdSuffix = "";
    } 
    $bookingDataArr = $kBooking->getBookingDetails($idBooking);
    $iAlreadyPaidBooking = 0;   
    if($bookingDataArr['idBookingStatus']==3 || $bookingDataArr['idBookingStatus']==4 || $bookingDataArr['idBookingStatus']==7)
    {
        $iAlreadyPaidBooking = 1;
    }  
    
    if(!empty($addSnoozeAry))
    {   
        $szReminderNotes = $addSnoozeAry['szReminderNotes'];  
        $idBookingFile = $addSnoozeAry['idBookingFile'];
        $dtReminderDate = $addSnoozeAry['dtReminderDate'];
        $szReminderTime = $addSnoozeAry['szReminderTime'];
        $emailTemplate_ID = $addSnoozeAry['idTemplate'];
        $szCustomerEmail = $addSnoozeAry['szCustomerEmail'];
        $szReminderSubject = $addSnoozeAry['szReminderSubject'];
        $szReminderEmailBody = $addSnoozeAry['szReminderEmailBody'];
        $dtEmailReminderDate = $addSnoozeAry['dtEmailReminderDate'];
        $szEmailReminderTime = $addSnoozeAry['szEmailReminderTime'];
        $replace_ary['iBookingLanguage'] = $addSnoozeAry['iRemindTemplateLanguage'];

        $forwarderdingAttachmentAry = $addSnoozeAry['forwardedAttachments'];
        $uploadedFileName = $addSnoozeAry['uploadedFileName'];
        $originalFileName = $addSnoozeAry['originalFileName'];
        $originalFileSize = $addSnoozeAry['originalFileSize'];
        
        $kBooking->loadFile($idBookingFile);
        $bookingDataArr = $kBooking->getBookingDetails($idBooking);
    }  
    else
    {
        $idBookingFile = $kBooking->idBookingFile;
        $idBooking = $kBooking->idBooking; 
        $idOfflineChat = $kBooking->idOfflineChat ;
        //$dtReminderDateTime = $kBooking->dtSnoozeDate; //MAN-37 06-Feb-2017
        
        
            
        $szCustomerEmail = $bookingDataArr['szEmail'];

        if(!empty($dtReminderDateTime) && $dtReminderDateTime!='0000-00-00 00:00:00')
        { 
            if(date('I')==1)
            {
                //$dtReminderDateTime = getDate($dtReminderDateTime);  
            } 
            $szCustomerTimeZone = $_SESSION['szCustomerTimeZoneGlobal'] ; 
            $date = new DateTime($dtReminderDateTime); 
            $date->setTimezone(new DateTimeZone($szCustomerTimeZone));   
            $dtReminderDate = $date->format('d/m/Y');
            $szReminderTime = $date->format('H:i');  

            $dtEmailReminderDate = $date->format('d/m/Y');
            $szEmailReminderTime = $date->format('H:i');  
        }
        else
        {
            $iWeekDay = date('w');  
            //Default snooze time for automatic booking is 2 hours and for RFQ 1 days.

            if($iWeekDay==0 || $iWeekDay==6) //0: Sunday, 6: Saturday
            {
                $dtReminderDate = date("d/m/Y",strtotime("NEXT MONDAY"));
                $szReminderTime = date("H:i");
            }
            else
            { 
                if(date('I')==1)
                {
                    $dtReminderDate = date("d/m/Y");
                    $szReminderTime = date("H:i",strtotime("+3 HOUR")); 
                } 
                else
                {
                    $dtReminderDate = date("d/m/Y");
                    $szReminderTime = date("H:i",strtotime("+2 HOUR")); 
                } 
            }  

            if($iWeekDay==7 || $iWeekDay==6) //5: Friday, 6: Saturday
            {
                $dtEmailReminderDate = date("d/m/Y",strtotime("NEXT MONDAY")); 
            }
            else
            { 
                $dtEmailReminderDate = date("d/m/Y"); 
            }  

            if(date('I')==1)
            {
                $szEmailReminderTime = date("H:i",strtotime("+3 HOUR")); 
            }
            else
            {
                $szEmailReminderTime = date("H:i",strtotime("+2 HOUR")); 
            }
        } 
        
        $iReminderTemplateFlag=false;
        if($idTemplate>0)
        {
            $emailTemplate_ID = $idTemplate;
        }
        else
        {
            $id_ReminderTemplate = $kConfig->getReminderTemplateIdByTaskStatus($kBooking->szTaskStatus);

            if($id_ReminderTemplate>0)
            {
                
                $iReminderTemplateFlag=true;
                $emailTemplate_ID = $id_ReminderTemplate;
            }
            else
            {
                $emailTemplate_ID =0;
            }
        }
            
        $kAdmin = new cAdmin();
        $kAdmin->getAdminDetails($_SESSION['admin_id']);
            
        $kConfig->loadCountry($kAdmin->idInternationalDialCode);
        $iInternationDialCode = $kConfig->iInternationDialCode;

        $idInternationalDialCode = $bookingDataArr['idCustomerDialCode'];
        if(empty($idInternationalDialCode) && $bookingDataArr['idUser']>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($bookingDataArr['idUser']);
            $idInternationalDialCode = $kUser->idInternationalDialCode;
        } 
        $szCustomerPhoneNumber = $bookingDataArr['szCustomerPhoneNumber'];
            
        if($iAlreadyPaidBooking==1)
        {
            if($iLanguage>0)
            {
                $replace_ary['iBookingLanguage'] = $iLanguage;
            }
            else
            {
                $replace_ary['iBookingLanguage'] = $bookingDataArr['iBookingLanguage']; 
            }  
        }
        else
        {
            if($iLanguage>0)
            {
                $replace_ary['iBookingLanguage'] = $iLanguage;
            }
            else
            {
                if($kUser->iLanguage>0)
                {
                    $replace_ary['iBookingLanguage'] = $kUser->iLanguage;
                }
                else
                {
                    $replace_ary['iBookingLanguage'] = 2;//Danish
                }
            }   
        }

        $kConfig = new cConfig();
        $kConfig->loadCountry($idInternationalDialCode);
        $iCustomerInternationDialCode = "+".$kConfig->iInternationDialCode;

        $szOriginCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idOriginCountry'],false,$replace_ary['iBookingLanguage']);
        $szDestinationCountryAry = $kConfig->getAllCountryInKeyValuePair(false,$bookingDataArr['idDestinationCountry'],false,$replace_ary['iBookingLanguage']); 
        $szOriginCountry = $szOriginCountryAry[$bookingDataArr['idOriginCountry']]['szCountryName'] ;
        $szDestinationCountry = $szDestinationCountryAry[$bookingDataArr['idDestinationCountry']]['szCountryName'] ;	 

        $szMobile = "+".$iInternationDialCode." ".$kAdmin->szPhone;

        $szBaseUrl_str = remove_http_from_url(__MAIN_SITE_HOME_PAGE_URL__);	
        $szBaseUrl = addhttp(__MAIN_SITE_HOME_PAGE_URL__);

        $szSiteLink = "<a href='".$szBaseUrl."' target='_blank'>".$szBaseUrl_str."</a>";
        $iBookingLanguage = $replace_ary['iBookingLanguage'];

        $kConfig = new cConfig();
        $configLangArr = $kConfig->getConfigurationLanguageData('__SITE_STANDARD_TEXT__',$iBookingLanguage);
        $szCbmText = $configLangArr[1]['szCbm']; 

        if($bookingDataArr['idTransportMode']==4)//courier 
        { 
            $szVolWeight = format_volume($bookingDataArr['fCargoVolume'],$iBookingLanguage).$szCbmText.", ".number_format_custom((float)$bookingDataArr['fCargoWeight'],$iBookingLanguage)."kg,  ".number_format_custom((int)$bookingDataArr['iNumColli'],$iBookingLanguage)."col";
        }
        else
        {
            $szVolWeight = format_volume($bookingDataArr['fCargoVolume'],$iBookingLanguage).$szCbmText.",  ".number_format_custom((float)$bookingDataArr['fCargoWeight'],$iBookingLanguage)."kg ";
        }

       // $kWhsSearch=new cWHSSearch();
        //$szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__');

        $szRemindEmailTemplates = $kConfig->getRemindCmsSectionsList();

        $kWhsSearch=new cWHSSearch();
        $szCustomerCareNumer = $kWhsSearch->getManageMentVariableByDescription('__TRANSPORTECA_CUSTOMER_CARE__',$replace_ary['iBookingLanguage']);
        
        
        $kConfigNew = new cConfig();
        $transportModeListAry = array();
        $transportModeListAry = $kConfigNew->getAllTransportMode(false,$bookingDataArr['iBookingLanguage'],true,true);
        $shipperConsigneeAry = $kBooking_new->getBookingDetailsCountryName($idBooking);
        
        
        $szShipperCountryAry = $kConfigNew->getAllCountryInKeyValuePair(false,$shipperConsigneeAry['idShipperCountry'],false,$bookingDataArr['iBookingLanguage']);
        $szConsigneeCountryAry = $kConfigNew->getAllCountryInKeyValuePair(false,$shipperConsigneeAry['idConsigneeCountry'],false,$bookingDataArr['iBookingLanguage']); 
        $szShipperCountry = $szShipperCountryAry[$shipperConsigneeAry['idShipperCountry']]['szCountryName'] ;
        $szConsigneeCountry = $szConsigneeCountryAry[$shipperConsigneeAry['idConsigneeCountry']]['szCountryName'] ;	 
        
        $serviceTermAry = $kConfigNew->getAllServiceTerms($bookingDataArr['idServiceTerms']);
        
        $cargoDetailArr=$kBooking->getCargoDeailsByBookingId($bookingDataArr['id']);
        $total=count($cargoDetailArr);
        $cargo_volume = format_volume($bookingDataArr['fCargoVolume']); 
        $cargo_weight = number_format((float)$bookingDataArr['fCargoWeight'],0,'.',','); 
        $szCargoCommodity = html_entities_flag(utf8_decode($cargoDetailsAry['1']['szCommodity']),$utf8Flag);
        if($szCargoCommodity!='')
        {
            $szCargoCommodity = ", ".$szCargoCommodity;
        }
        else if($bookingDataArr['szCargoDescription']!='')
        {
            $szCargoCommodity = ", ".$bookingDataArr['szCargoDescription'];
        }
        $total=count($cargoDetailArr);
        $kCourierServices = new cCourierServices();
        if(!empty($cargoDetailArr))
        {
            $szCargoFullDetails="";
            $ctr=0;
            $totalQuantity=0;
            foreach($cargoDetailArr as $cargoDetailArrs)
            { 
                
                if($cargoDetailArrs['szCommodity']!='')
                {
                    $packingTypeArr[0]['szPacking']=$cargoDetailArrs['szCommodity'];
                    $packingTypeArr[0]['szSingle']=$cargoDetailArrs['szCommodity'];
                }
                else
                {
                    if($bookingDataArr['idCourierPackingType']>0)
                    {
                        $packingTypeArr=$kCourierServices->selectProviderPackingList($bookingDataArr['idCourierPackingType'],1);
                    }
                    else
                    {
                        $packingTypeArr[0]['szPacking']='colli';
                        $packingTypeArr[0]['szSingle']='colli';
                    }
                }
                $totalQuantity=$totalQuantity+$cargoDetailArrs['iColli'];
                $t=$total-1;
                   if((int)$cargoDetailArrs['iQuantity']>1)
                        $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szPacking']);
                    else
                        $quantityText=number_format((int)$cargoDetailArrs['iQuantity'])." ".strtolower($packingTypeArr[0]['szSingle']);
                    
                    $lenWidthHeightStr='';
                    if($cargoDetailArrs['fLength']>0 || $cargoDetailArrs['fWidth']>0 || $cargoDetailArrs['fHeight']>0)
                    {
                        $lenWidthHeightStr=round_up($cargoDetailArrs['fLength'],1)." x ".round_up($cargoDetailArrs['fWidth'],1)." x ".round_up($cargoDetailArrs['fHeight'],1)."".$cargoDetailArrs['cmdes']." (LxWxH), ";
                    }
                    if($ctr==0)
                    {
                        $szCargoFullDetails .=$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each";
                    }
                    else
                    {
                        $szCargoFullDetails .="<br />".$quantityText." of ".$lenWidthHeightStr."".round_up($cargoDetailArrs['fWeight'],1)." ".strtolower($cargoDetailArrs['wmdes'])." each"; 
                    }
                ++$ctr;	
            }

            if($totalQuantity>1)
            {
                $textPacking='colli';
            }
            else
            {
                 $textPacking='colli';   
            }

            $coliiText='';
            if($totalQuantity>0)
            {
                $coliiText=number_format($totalQuantity)." ".$textPacking.", ";
            }

            $szCargoFullDetails .= "<br />Total: ".$coliiText."".$cargo_volume." cbm, ".$cargo_weight." kg".$szCargoCommodity; 
        }
        else
        {
            $totalQuantity = $bookingDataArr['iNumColli'];
            $textPacking = 'colli';
            $coliiText='';
            if($totalQuantity>0)
            {
                $coliiText=number_format($totalQuantity)." ".$textPacking.", ";
            }
            $szCargoFullDetails .= "Total: ".$coliiText."".$cargo_volume." cbm, ".$cargo_weight." kg".$szCargoCommodity; 
        }
                                    
        
        if($shipperConsigneeAry['szShipperPostCode']!=''){
            $replace_ary['szPostcodeShipper'] = $shipperConsigneeAry['szShipperPostCode'].", ";
        }else{
            $replace_ary['szPostcodeShipper'] ='';
        }
        if($shipperConsigneeAry['szConsigneePostCode']!=''){
            $replace_ary['szPostcodeConsignee'] = $shipperConsigneeAry['szConsigneePostCode'].", ";
        }else{
            $replace_ary['szPostcodeConsignee'] ='';
        }
        
        if($shipperConsigneeAry['szShipperCity']!=''){
        $replace_ary['szCityShipper'] = $shipperConsigneeAry['szShipperCity'].", ";
        }
        else{
            $replace_ary['szCityShipper'] = '';
        }                                    
        if($szShipperCountry!=''){
            $replace_ary['szCountryShipper'] = $szShipperCountry;
        }
        else{
            $replace_ary['szCountryShipper'] = '';
        } 

        if($shipperConsigneeAry['szConsigneeCity']!=''){
            $replace_ary['szCityConsignee'] = $shipperConsigneeAry['szConsigneeCity'].", ";
        }
        else{
            $replace_ary['szCityConsignee'] = '';
        }                                    
        if($szConsigneeCountry!=''){
            $replace_ary['szCountryConsignee'] = $szConsigneeCountry;
        }
        else{
            $replace_ary['szCountryConsignee'] = '';
        }

        $replace_ary['szFromString'] = $replace_ary['szPostcodeShipper']."".$replace_ary['szCityShipper']."".$replace_ary['szCountryShipper'];
        $replace_ary['szToString'] = $replace_ary['szPostcodeConsignee']."".$replace_ary['szCityConsignee']."".$replace_ary['szCountryConsignee'];                               
                                    
       
        $replace_ary['szFirstName'] = $bookingDataArr['szFirstName'] ;
        $replace_ary['szLastName'] = $bookingDataArr['szLastName'];
        $replace_ary['szCountryCode'] = $iCustomerInternationDialCode;
        $replace_ary['szPhoneNumber'] = $szCustomerPhoneNumber;
        $replace_ary['szFromCountry'] = $szOriginCountry ;
        $replace_ary['szToCountry'] = $szDestinationCountry ;
        $replace_ary['szFromCity'] = $bookingDataArr['szOriginCity'];
        $replace_ary['szToCity'] = $bookingDataArr['szDestinationCity'] ;
        $replace_ary['szBookingRef'] = $bookingDataArr['szBookingRef'];  
        $replace_ary['szCargoDescription'] = $bookingDataArr['szCargoDescription']; 
        $replace_ary['szCargoDetail'] = $szCargoFullDetails;
        $replace_ary['szCargoReady'] = date('d/m/Y',strtotime($bookingDataArr['dtTimingDate']));
        $replace_ary['szCargo'] = $szVolWeight;
        $replace_ary['szMode'] = $transportModeListAry[$bookingDataArr['idTransportMode']]['szShortName'];
        $replace_ary['szTerms'] = $serviceTermAry[0]['szShortTerms'];
        
        
        

        $volume =  format_volume($bookingDataArr['fCargoVolume']);
        $weight =  get_formated_cargo_measure($bookingDataArr['fCargoWeight']);

        $volume = round_up($volume,1);
        $weight = round_up($weight,0);

        $replace_ary['szVol'] = getPriceByLang($volume,$replace_ary['iBookingLanguage'],1); 
        $replace_ary['szWeight'] = getPriceByLang($weight,$replace_ary['iBookingLanguage'],0); 

        $replace_ary['szAdminName'] = $kAdmin->szFirstName." ".$kAdmin->szLastName ;
        $replace_ary['szTitle'] = $kAdmin->szTitle ;
        $replace_ary['szOfficePhone'] = $szCustomerCareNumer ;
        $replace_ary['szMobile'] = $szMobile ; 
        $replace_ary['szWebSiteUrl'] = $szBaseUrl_str;

        $szChatDate = '';
        $szChatDate = '';
        if($idOfflineChat>0)
        {
            $kChat = new cChat();
            $zopimChatTranscriptAry = array();
            $zopimChatTranscriptAry = $kChat->getAllZopimChatMessageLogs($idOfflineChat);

            $zopimChatAry = array();
            $zopimChatAry = $kChat->getAllZopimChatLogs(false,$idOfflineChat); 
            $szChatMessage = display_zopim_chat_transcript($zopimChatTranscriptAry,$zopimChatAry[0],'TEXT'); 
            $szChatDateTime = $zopimChatAry[0]['dtChatDateTime'];

            $iMonth = (int)date("m",strtotime($szChatDateTime));
            $szMonthName = getMonthName($iBookingLanguage,$iMonth,true);

            $szChatDate = (int)date("d",strtotime($szChatDateTime)).". ".$szMonthName." ".date("Y",strtotime($szChatDateTime)); 
        }
        $replace_ary['szChatDate'] = $szChatDate ; 
        $replace_ary['szChatMessage'] = $szChatMessage;
        $emailMessageAry = array(); 
        $emailMessageAry = createEmailMessage($szEmailTemplate,$replace_ary,true,$emailTemplate_ID); 
        $szReminderEmailBody = $emailMessageAry['szEmailBody'];   
           
        if($emailTemplate_ID>0)
        {
            $templateDetail = array();
            $templateDetail = $kConfig->getRemindCmsSectionsList($iBookingLanguage,$emailTemplate_ID,$szFromPage); 
            
            $attachmentOriginalFileNameAry = array();
            $attachmentFileNameAry = array(); 
            if(!empty($templateDetail[0]['szAttachmentOriginalFileName']))
            {
                $originalFileName = unserialize($templateDetail[0]['szAttachmentOriginalFileName']);
                $bAttachmentAvailable = true; 
            } 
            if(!empty($templateDetail[0]['szAttachmentFileName']))
            {
                $uploadedFileName = unserialize($templateDetail[0]['szAttachmentFileName']);
            }  
            $iAddInvoice = $templateDetail[0]['iAddInvoice'];
            $iAddBookingNotice = $templateDetail[0]['iAddBookingNotice'];
            $iAddBookingConfirmation = $templateDetail[0]['iAddBookingConfirmation'];

            if((int)$iAddInvoice==0 && (int)$iAddInvoice==0 && (int)$iAddInvoice==0)
            {
                $bAttachmentAvailable = true;
            }
        }
            
        if($szFromPage=='CRM_TEMPLATE')
        {   
            if(!empty($logEmailsArys['szToUserName']))
            {
                $szToEmailAddressStr = $logEmailsArys['szToUserName']." <".$logEmailsArys['szToAddress'].">";
            }
            else
            {
                $szToEmailAddressStr = $logEmailsArys['szToAddress'];
            }
            
            $kQuote = new cQuote();
            $crmEmailAry = array();
            $crmEmailAry = $kQuote->getReplyEmailText($logEmailsArys['id'],$iBookingLanguage); 
            if(!empty($crmEmailAry['szEmailSubject']))
            {
                $szReminderSubject = $crmEmailAry['szEmailSubject'];
            }
            
            if($iReminderTemplateFlag && $crmEmailAry['szDraftFlag'])
            {
                $szReminderEmailBody='';
            }
            if(!empty($crmEmailAry['szEmailBody']) && !empty($szReminderEmailBody))
            {
                
                $szReminderEmailBody = $szReminderEmailBody."<br><br>".$crmEmailAry['szEmailBody']; 
                
            }  
            else if(!empty($crmEmailAry['szEmailBody']))
            {
                
                if($crmEmailAry['szDraftFlag'])
                {
                    $szReminderEmailBody = $crmEmailAry['szEmailBody'];
                }
                else
                {
                    //$szReminderEmailBody = PHP_EOL.PHP_EOL.PHP_EOL.$crmEmailAry['szEmailBody'];
                    $szReminderEmailBody = "<br><br>".$crmEmailAry['szEmailBody']; 
                }
            } 
            $forwarderdingAttachmentAry = array(); 
            if($szAction=='CRM_FORWARD')
            {
                if($logEmailsArys['iIncomingEmail']==1)
                {
                    if($logEmailsArys['iHasAttachments']==1 && !empty($logEmailsArys['szAttachmentFiles']))
                    {
                        $forwarderdingAttachmentAry = unserialize($logEmailsArys['szAttachmentFiles']);
                        $iAttachmentCount = count($attachmentAry);
                    } 
                }
                else if(!empty($logEmailsArys['szAttachmentFiles']))
                {
                    $outgoingEmailAttachmentAry = array();
                    $outgoingEmailAttachmentAry = unserialize($logEmailsArys['szAttachmentFiles']);
                    if(!empty($outgoingEmailAttachmentAry))
                    {
                        $ctr_email = 1; 
                        foreach($outgoingEmailAttachmentAry as $outgoingEmailAttachmentArys)
                        { 
                            if(file_exists($outgoingEmailAttachmentArys))
                            {
                                $originalFileName[$ctr_email] = basename($outgoingEmailAttachmentArys); 
                                $uploadedFileName[$ctr_email] = basename($outgoingEmailAttachmentArys); 
                                $bAttachmentAvailable = true; 
                                $ctr_email++;
                            }
                        }
                    }
                } 
            }
            else if($szAction=='CRM_NEW_EMAIL')
            {
                $szReminderEmailBody = '';
            }
        }
        else
        {
            $szReminderSubject = $emailMessageAry['szEmailSubject'];  
        } 
        
        if($szFromPage!='CRM_TEMPLATE' && $_POST['mode']=='DISPLAY_TASK_FORM_MENU')
        {
            $kCRM = new cCRM();
            $remindDraftArr=$kCRM->getDraftEmailRemindData($idBooking);
            $draftEmailData=false;
            if($remindDraftArr['szRemindDraftEmailBody']!='')
            {
                $szReminderSubject = utf8_decode($remindDraftArr['szRemindDraftSubject']);  
                $szReminderEmailBody = utf8_decode($remindDraftArr['szRemindDraftEmailBody']);
            }
        }
    }  
    ?>
    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__."/jquery.form.crm.js"; ?>"></script>
     
    <script type="text/javascript"> 
        var _day = '<?php echo date('d'); ?>';
        var _month = '<?php echo date('m'); ?>';
        var _year = '<?php echo date('Y'); ?>';

        var date_picker_argument = new Date(_year, _month-1,_day) ; 
        
        
             
            $("#dtReminderDate"+'<?php echo $szIdSuffix; ?>').datepicker({minDate:date_picker_argument});
            $("#dtEmailReminderDate"+'<?php echo $szIdSuffix; ?>').datepicker({minDate:date_picker_argument});  
              
            function sendFileToServer(formData,status)
            {
                var uploadURL ="<?php echo __MANAGEMENT_URL__?>/uploadAttachment.php"; //Upload URL
                var extraData ={}; //Extra Data.
                var jqXHR=$.ajax({
                        xhr: function() {
                        var xhrobj = $.ajaxSettings.xhr();
                        if (xhrobj.upload) {
                                xhrobj.upload.addEventListener('progress', function(event) {
                                    var percent = 0;
                                    var position = event.loaded || event.position;
                                    var total = event.total;
                                    if (event.lengthComputable) {
                                        percent = Math.ceil(position / total * 100);
                                    } 
                                }, false);
                            }
                        return xhrobj;
                    },
                url: uploadURL,
                type: "POST",
                contentType:false,
                processData: false,
                cache: false,
                data: formData,
                success: function(data){  
                    $('.uploading').hide();
                    var iCreateUl = 0;
                    var szHtmlString = '';
                    if($(".add-edit-popup-ul"+'<?php echo $szIdSuffix; ?>').length)
                    { 
                        //@To Do
                    }
                    else
                    { 
                        iCreateUl = 1; 
                        szHtmlString = '<ul class="reorder_ul reorder-photos-list add-edit-popup-ul'+'<?php echo $szIdSuffix; ?>'+'">';
                    } 
                    szHtmlString += data;
                    if(iCreateUl===1)
                    { 
                        szHtmlString += '<input id="iAttachmentCounter'+'<?php echo $szIdSuffix; ?>'+'" name="'+'<?php echo $szArrayName; ?>'+'[iAttachmentCounter]" value="1" type="hidden">';
                        szHtmlString += '</ul>';
                        $("#images_preview"+'<?php echo $szIdSuffix; ?>').html(szHtmlString);
                    }
                    else
                    {
                        var iAttachmentCtr = $("#iAttachmentCounter"+'<?php echo $szIdSuffix; ?>').val();
                        iAttachmentCtr = parseInt(iAttachmentCtr); 
                        if(iAttachmentCtr>0)
                        {
                            iAttachmentCtr = iAttachmentCtr + 1;
                        }
                        else
                        {
                            iAttachmentCtr = 1;
                        } 
                        $(".add-edit-popup-ul"+'<?php echo $szIdSuffix; ?>').append(szHtmlString);
                        $("#iAttachmentCounter"+'<?php echo $szIdSuffix; ?>').val(iAttachmentCtr);
                    } 
                    $("#dragdrop_file"+'<?php echo $szIdSuffix; ?>').removeAttr('style');
                }
            });  
        }
        
        function handleFileUpload(files,obj)
        {
            for (var i = 0; i < files.length; i++) 
            {
                var iAttachmentCtr = $("#iAttachmentCounter"+'<?php echo $szIdSuffix; ?>').val();
                iAttachmentCtr = parseInt(iAttachmentCtr); 
                if(iAttachmentCtr>0)
                {
                    iAttachmentCtr = iAttachmentCtr + 1;
                }
                else
                {
                    iAttachmentCtr = 1;
                }
                var fd = new FormData(); 
                fd.append('filesToUpload', files[i]);
                fd.append('iUploadedByDragDrop', '1');
                fd.append('iAttachmentCounter', iAttachmentCtr); 
                fd.append('formAryName', '<?php echo $szArrayName; ?>'); 
                fd.append('idCrmEmail', '<?php echo $idCrmEmail; ?>'); 
                /*
                    var status = new createStatusbar(obj); //Using this we can set progress.
                    status.setFileNameSize(files[i].name,files[i].size);
                */
                $('.uploading').show();
                sendFileToServer(fd,status); 
            }
        }
            
        $(document).ready(function()
        { 
            $('#filesToUpload'+'<?php echo $szIdSuffix; ?>').on('change',function(){ 
                 
                $('#'+'<?php echo $szFormId; ?>').ajaxForm({
                    target:'#images_preview<?php echo $szIdSuffix; ?>',
                        beforeSubmit:function(e){
                            $('.uploading').show();
                        },
                        success:function(e){
                            $('.uploading').hide();
                            $("#reminder_attachment_container"+'<?php echo $szIdSuffix; ?>').css('display','none');
                        },
                        error:function(e){
                    }
                }).submit();
            });

            /*
            *  Opening file upload window on click of +Attachment button
            */
            $('#request_reminder_email_attachment_button'+'<?php echo $szIdSuffix; ?>').click(function() {
                $('#filesToUpload'+'<?php echo $szIdSuffix; ?>').click(); 
            }); 
                
            var obj = $("#dragdrop_file"+'<?php echo $szIdSuffix; ?>');
            obj.on('dragenter', function (e) 
            {
                e.stopPropagation();
                e.preventDefault();
                $(this).css('border', '2px solid #0B85A1');
            });
            obj.on('dragover', function (e) 
            {
                 e.stopPropagation();
                 e.preventDefault();
            });
            obj.on('drop', function (e) 
            {

                $(this).css('border', '2px dotted #0B85A1');
                e.preventDefault();
                var files = e.originalEvent.dataTransfer.files;

                //We need to send dropped files to Server
                handleFileUpload(files,obj);
            });
            $(document).on('dragenter', function (e) 
            {
                e.stopPropagation();
                e.preventDefault();
            });
            $(document).on('dragover', function (e) 
            {
                e.stopPropagation();
                e.preventDefault();
                obj.css('border', '2px dotted #0B85A1');
            });
            $(document).on('drop', function (e) 
            {
                e.stopPropagation();
                e.preventDefault();
            });
            <?php
            if($szFromPage=='CRM_TEMPLATE')
            {?>
                
                NiceEditorInstanceCRM[<?=$idCrmEmail?>] = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance("szReminderEmailBody<?php echo $szIdSuffix; ?>",{hasPanel : true});
                <?php
                if($crmEmailAry['szDraftFlag'])
                {?>
                var container_div_id_text ="crm_buttons_operations_main_container_<?=$idCrmEmail?>";
                $('#'+container_div_id_text+' .nicEdit-main #email_body_indent_span:first').attr('style','');
                <?php }?>
            <?php            
            }else{?>
                NiceEditorInstance = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance("szReminderEmailBody<?php echo $szIdSuffix; ?>",{hasPanel : true});
                
            <?php    
            }?>
             <?php if($szFromPage=='CRM_TEMPLATE'){ ?>
            enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>'); 
         <?php } else { ?>
            $(".nicEdit-main").keyup(function () { enable_reminder_tab_buttons();  });
            enable_reminder_tab_buttons();
         <?php } ?>
        });  
    </script>  
    <div class="clearfix email-fields-container"> 
        <span class="<?php echo $szFirstSpanClass; ?>">Subject</span>
        <span class="<?php echo $szSecondSpanClass; ?>">
            <input type="text" name="<?php echo $szArrayName; ?>[szReminderSubject]" <?php if($szFromPage=='CRM_TEMPLATE') { ?> onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" <?php } else {?> onkeyup="enable_reminder_tab_buttons();" onblur="enable_reminder_tab_buttons();"<?php }?>placeholder="Subject" id="szReminderSubject<?php echo $szIdSuffix; ?>" value="<?php echo $szReminderSubject; ?>"> 
        </span>  
    </div> 
    <div class="clearfix email-fields-container"> 
        <div class="<?php echo $szFirstSpanClass; ?>" >Email Body</div>
        <div class="<?php echo $szSecondSpanClass; ?>" id="dragdrop_file<?php echo $szIdSuffix; ?>">
            <textarea spellcheck="false" style="min-height: 200px;width:99.7%;" name="<?php echo $szArrayName; ?>[szReminderEmailBody]" <?php if($szFromPage=='CRM_TEMPLATE') { ?>onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" <?php } else {?>onkeyup="enable_reminder_tab_buttons();" onblur="enable_reminder_tab_buttons();"<?php }?>placeholder="Email Body" id="szReminderEmailBody<?php echo $szIdSuffix; ?>"><?php echo $szReminderEmailBody; ?></textarea>
            
            <div style="height:5px;text-align:right;"><span id="draft_email_msg<?php echo $szIdSuffix; ?>" style="display:none;">Draft saved</span></div>
            
        </div>  
    </div>
    <span class="clearfix email-fields-container" style="width:100%;" id="email_attachment_listing_container<?php echo $szIdSuffix; ?>"> 
        <span class="<?php echo $szFirstSpanClass; ?>" >&nbsp;</span>
            <span class="<?php echo $szSecondSpanClass; ?>">
                <span style="display:none;">
                    <input type="hidden" name="image_form_submit" value="1"/> 
                    <input name="filesToUpload[]" id="filesToUpload<?php echo $szIdSuffix; ?>" type="file" />
                </span> 
                <div class="uploading none">
                    <label>&nbsp;</label>
                    <img src="<?php echo __BASE_STORE_IMAGE_URL__."/uploading.gif"; ?>" alt="uploading..."/>
                </div>  
                <?php
                    if(!empty($forwarderdingAttachmentAry))
                    {
                        $count = 1; 
                        ?>
                        <span id="forwarded_attachment_container">
                            <ul class="reorder_ul reorder-photos-list">
                            <?php
                                foreach($forwarderdingAttachmentAry as $attachmentArys)
                                {
                                    ?>
                                    <li id="forwarded_attachment_li_<?php echo $count; ?>" class="ui-sortable-handle"> 
                                        <a href="<?php echo __BASE_URL_GMAIL_ATTACHMENT__."/".$logEmailsArys['iEmailNumber']."/".$attachmentArys; ?>" target="_blank"><?php echo $attachmentArys; ?></a>                                           
                                        &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="closeFile('<?php echo $count; ?>','FORWARD_EMAIL')" class="red_text" style="text-decoration:none;"><strong>X </strong></a>  
                                        <input type="hidden" name="<?php echo $szArrayName; ?>[forwardedAttachments][]" id="forwardedAttachments_<?php echo $count;?>" value="<?php echo $attachmentArys; ?>">
                                    </li>  
                                    <?php
                                    $count++;
                                }
                            ?>
                            </ul>
                        </span>
                        <?php
                    }
                        
                ?>
                <span id="images_preview<?php echo $szIdSuffix; ?>"> 
                <?php 
                    if(!empty($uploadedFileName))
                    { 
                        $count = 1;  
                        echo '<ul class="reorder_ul reorder-photos-list add-edit-popup-ul'.$szIdSuffix.'">';
                        foreach($uploadedFileName as $key=>$uploadedFiles)
                        {   
                            $szBasePath = __BASE_URL_EMAIL_ATTACHMENT__."/".$uploadedFiles;
                            if($uploadedFiles=='BOOKING_INVOICE' || $uploadedFiles=='BOOKING_NOTICE' || $uploadedFiles=='BOOKING_CONFIRMATION')
                            {
                                if($iAlreadyPaidBooking!=1)
                                {
                                    //If booking is not paid then we don't add booking documents to the emails
                                    continue;
                                } 
                                if($uploadedFiles==='BOOKING_INVOICE')
                                {
                                    $szAttachmentLinkText = "Booking Invoice";
                                }
                                else if($uploadedFiles==='BOOKING_NOTICE')
                                {
                                    $szAttachmentLinkText = "Booking Notice";
                                }
                                else if($uploadedFiles==='BOOKING_CONFIRMATION')
                                {
                                    $szAttachmentLinkText = "Booking Confirmation";
                                }
                                $szAttachmentLink = "Attachment ".$count.": ".$szAttachmentLinkText;
                                $szBasePath = "javascript:void(0);";
                            }
                            else
                            {
                                $szAttachmentLink = "Attachment ".$count.": ".$originalFileName[$key];
                            }
	?>
                            <li id="image_li_<?php echo $count; ?>" class="ui-sortable-handle">  
                                <a href="<?php echo $szBasePath; ?>" target="_blank" style="float:none;" class="image_link"><?php echo $szAttachmentLink; ?></a> 
                                &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="closeFile('<?php echo $count; ?>')" class="red_text" style="text-decoration:none;"><strong>X </strong></a> 
                                <input type="hidden" name="<?php echo $szArrayName; ?>[uploadedFileName][]" id="uploadedFileName_<?php echo $count;?>" value="<?php echo $uploadedFiles; ?>">
                                <input type="hidden" name="<?php echo $szArrayName; ?>[originalFileName][]" id="originalFileName_<?php echo $count;?>" value="<?php echo $originalFileName[$key]; ?>">
                                <input type="hidden" name="<?php echo $szArrayName; ?>[originalFileSize][]" id="originalFileSize_<?php echo $count;?>" value="<?php echo $originalFileSize[$key]; ?>"> 
                            </li>
                            <?php
                            $count++; 
                        }
			echo '</ul><br>';
                    }
                ?>   
                    <input type="hidden" name="<?php echo $szArrayName; ?>[iAttachmentCounter]" id="iAttachmentCounter<?php echo $szIdSuffix; ?>" value="<?php echo $count; ?>"> 
                    </span> 
                </span>
            </span>
            <?php 
                $crmClass='';
//                if($szFromPage=='CRM_TEMPLATE')
//                {
                    
                    $crmClass="crm";
                    $wd_28="wd-14 crm";
                    $wds_72="wds-86 crm";
                    $wds_20="wds-17 crm";
                    $wds_60="wds-68 crm";
                    $wds_time_20="wds-15";
               /* }
                else
                {
                    $wd_28="wd-28";
                    $wds_72="wds-72";
                    $wds_20="wds-20";
                    $wds_60="wds-60";
                    $wds_time_20="wds-20";
                }*/
            ?>
            <div class="clearfix email-fields-container" style="width:100%;">
                <span class="quote-field-container wd-14">&nbsp;</span>
                <span class="quote-field-container wd-86 <?php echo $crmClass;?>">
                    <div class="clearfix email-fields-container" style="width:100%;">
                        <span class="quote-field-container <?php echo $wd_28;?>">
                            <a href="javascript:void(0);" id="request_reminder_email_attachment_button<?php echo $szIdSuffix; ?>" class="button2"><span>+ Attachment</span></a>  
                        </span>
                        <span class="quote-field-container <?php echo $wds_72;?>" style="text-align:right;"> 
                            <span class="quote-field-container <?php echo $wds_20;?>">
                                <input type="text" name="<?php echo $szArrayName; ?>[dtEmailReminderDate]" onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" onblur="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" id="dtEmailReminderDate<?php echo $szIdSuffix; ?>" readonly value="<?php echo $dtEmailReminderDate; ?>">
                            </span>
                            <span class="quote-field-container <?php echo $wds_time_20;?>">
                                <input type="text" maxlength="5" name="<?php echo $szArrayName; ?>[szEmailReminderTime]" onkeyup="enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');"  id="szEmailReminderTime<?php echo $szIdSuffix; ?>" onblur="<?php if($szFromPage=='CRM_TEMPLATE'){ ?>format_reminder_time(this.value,'4','<?php echo $idCrmEmail; ?>');<?php } else {?>format_reminder_time(this.value,1);<?php }?>enable_crm_operation_reply_tab_buttons('<?php echo $szFormId; ?>','<?php echo $idCrmEmail; ?>');" value="<?php echo $szEmailReminderTime; ?>">   
                            </span> 
                            <span class="quote-field-container <?php echo $wds_60;?>" style="text-align:right;">
                                <a href="javascript:void(0);" id="request_reminder_email_snooze_button<?php echo $szIdSuffix; ?>" style="opacity:0.4;" class="button1" ><span>Send Snooze</span></a>  
                                <a href="javascript:void(0);" id="request_reminder_email_send_close_task_button<?php echo $szIdSuffix; ?>" class="button1" style="opacity:0.4;"><span>Send <?php echo t($t_base.'fields/close_task'); ?></span></a> 
                                
                                <a href="javascript:void(0);" id="request_reminder_email_save_draft_button<?php echo $szIdSuffix; ?>" class="button1" style="opacity:0.4;"><span>Save Draft</span></a>
                                  
                                <a href="javascript:void(0);" id="request_reminder_email_send_button<?php echo $szIdSuffix; ?>" class="button1" style="opacity:0.4;"><span>Send</span></a>
                               <?php if($iSuccess==2){ ?>
                                    <span style="color:green;"> Sent! </span>
                                <?php } 
                                ?>    
                            </span> 
                        </span>
                    </div>
                </span> 
            </div>  
        <input type="hidden" name="<?php echo $szArrayName; ?>[idRemindTemplate]" id="idRemindTemplate" value="<?php echo $idTemplate; ?>">
    <?php
 }
 function add_edit_Reminder_Email_Template($kConfig,$idBookingFile=false,$idTemplate=false,$szTaskStatus=false,$iRemindEmailLanguage=false,$idCrmEmailLogs=false,$szFromPage=false,$szAction=false)
 {
    $t_base="management/pendingTray/";     
    
    $szFirstSpanClass="quote-field-label";
    $szSecondSpanClass="quote-field-container"; 
    
    if(!empty($iRemindEmailLanguage))
    {
        $iRemindEmailLanguage = $iRemindEmailLanguage;
    }
    else
    {
        $iRemindEmailLanguage = 1;
    }
    
    if(empty($szTaskStatus))
    {
        $szTaskStatus = "REMIND";
    } 
    if(!empty($idTemplate))
    {
        $mode = "UPDATE_REMIND_EMAIL_TEMPLATE"; 
        $templateDetail = $kConfig->getRemindCmsSectionsList($iRemindEmailLanguage,$idTemplate);  
        
        if(empty($templateDetail))
        {
            $templateDetailArr = $kConfig->getRemindCmsSectionsList(false,$idTemplate);
            $templateDetail[0]['szFriendlyName']=$templateDetailArr[0]['szFriendlyName']; 
        }
        $heading = "Edit e-mail template: ".$templateDetail[0]['szFriendlyName']; 
        $operationType = "edit_template"; 
            
        if(empty($_POST['addEditRemindEmailTemplate']))
        {
            $_POST['addEditRemindEmailTemplate']['szFromPage'] = $szFromPage; 
            $_POST['addEditRemindEmailTemplate']['szAction'] = $szAction; 
            
            if(mb_detect_encoding($templateDetail[0]['section_description'], "UTF-8", true) == "UTF-8")
            { 
                $message = utf8_decode($templateDetail[0]['section_description']);	 
            }
            else
            { 
                $message = $templateDetail[0]['section_description']; 
            } 
            if(!empty($templateDetail[0]['subject']))
            { 
                if(mb_detect_encoding($templateDetail[0]['subject'], "UTF-8", true) == "UTF-8")
                {
                    $subject = utf8_decode($templateDetail[0]['subject']);	
                }
                else
                {
                    $subject = $templateDetail[0]['subject'];	
                }
            }   
            $_POST['addEditRemindEmailTemplate']['szFriendlyName'] = $templateDetail[0]['szFriendlyName'];
            $_POST['addEditRemindEmailTemplate']['szReminderSubject'] = $subject;
            $_POST['addEditRemindEmailTemplate']['szReminderEmailBody'] = $message;
            $_POST['addEditRemindEmailTemplate']['iRemindEmailLanguage'] = $iRemindEmailLanguage; 
            
            $_POST['addEditRemindEmailTemplate']['iAddInvoice'] = $templateDetail[0]['iAddInvoice'];
            $_POST['addEditRemindEmailTemplate']['iAddBookingNotice'] = $templateDetail[0]['iAddBookingNotice'];
            $_POST['addEditRemindEmailTemplate']['iAddBookingConfirmation'] = $templateDetail[0]['iAddBookingConfirmation'];
            $_POST['addEditRemindEmailTemplate']['iRemindScreen'] = $templateDetail[0]['iRemindScreen'];
            $_POST['addEditRemindEmailTemplate']['iFileLog'] = $templateDetail[0]['iFileLog'];
            $_POST['addEditRemindEmailTemplate']['iCustomerLog'] = $templateDetail[0]['iCustomerLog'];
            
            if(!empty($templateDetail[0]['szAttachmentFileName']))
            {
                $_POST['addEditRemindEmailTemplate']['uploadedFileName'] = unserialize($templateDetail[0]['szAttachmentFileName']);
            } 
            if(!empty($templateDetail[0]['szAttachmentOriginalFileName']))
            {
                $_POST['addEditRemindEmailTemplate']['originalFileName'] = unserialize($templateDetail[0]['szAttachmentOriginalFileName']);
            } 
        } 
    }
    else
    {
        $mode = "SAVE_NEW_REMIND_EMAIL_TEMPLATE"; 
        $heading = "New e-mail template"; 
        $operationType = "new_template";
        $_POST['addEditRemindEmailTemplate']['iRemindScreen'] = 1;
        $_POST['addEditRemindEmailTemplate']['iFileLog'] = 1;
        $_POST['addEditRemindEmailTemplate']['iCustomerLog'] = 1;
        
        $_POST['addEditRemindEmailTemplate']['szFromPage'] = $szFromPage; 
        $_POST['addEditRemindEmailTemplate']['szAction'] = $szAction;
        $_POST['addEditRemindEmailTemplate']['iRemindEmailLanguage'] = $iRemindEmailLanguage;
    } 
    
    if($_POST['addEditRemindEmailTemplate']['idCrmEmailLogs']>0)
    {
        $idCrmEmailLogs = (int)$_POST['addEditRemindEmailTemplate']['idCrmEmailLogs'];
    } 
    $uploadedFileName = $_POST['addEditRemindEmailTemplate']['uploadedFileName'];
    $originalFileName = $_POST['addEditRemindEmailTemplate']['originalFileName']; 
            
    $formId = 'addEditRemindEmailTemplateForm';
    
    if(!empty($kConfig->arErrorMessages))
    { 
        $szValidationErrorKey = '';
        foreach($kConfig->arErrorMessages as $errorKey=>$errorValue)
        {
            $szValidationErrorKey = $errorKey ;
            break;
        } 
        display_form_validation_error_message($formId,$kConfig->arErrorMessages);
    }
    $kConfig = new cConfig();
    $languageArr=$kConfig->getLanguageDetails('','');
    ?> 
    <script type="text/javascript" src="<?php echo __BASE_STORE_JS_URL__."/jquery.form.crm.js"; ?>"></script>
    <script type="text/javascript">  
        enable_reminder_template_tab_buttons('<?php echo $mode;?>','<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>','<?php echo $iRemindEmailLanguage;?>');
        $(document).ready(function(){
            $('#filesToUpload_popup').on('change',function(){
                $('#'+'<?php echo $formId; ?>').ajaxForm({
                    target:'#images_preview_popup',
                        beforeSubmit:function(e){
                            $('.uploading_popup').show();
                            $("#iFileUploadInProgress").val('1');
                        },
                        success:function(e){
                            $('.uploading_popup').hide();
                            //$("#reminder_attachment_container_popup").css('display','none');
                            $("#iFileUploadInProgress").val('0');
                        },
                        error:function(e){
                    }
                }).submit();
            });
            $('#add_attachment_reminder_popup').click(function() {
                $('#filesToUpload_popup').click(); 
            });
        });
        
        NiceEditorInstance_popup = new nicEditor({fullPanel : true,maxHeight : 300,minHeight : 300}).panelInstance("szReminderEmailBodyPopup",{hasPanel : true}); 
        $(".nicEdit-main").attr('style','margin: 4px; min-height: 200px; overflow: hidden;');
    </script>
    <div id="popup-bg"></div>
    <div id="popup-container">
        <div class="popup" style="width:670px;margin-top:-30px;"> 
            <div class="scroll-div remind-email-popup" style="height: 450px;" >
                <div id="delete-msg" style="display:none;"></div>
                <h3><?php echo $heading;?></h3> <br/>
                <br/>
                <form spellcheck="false" action="<?php echo __BASE_MANAGEMENT_URL__."/uploadAttachment.php"; ?>" id="<?php echo $formId;?>"  name="<?php echo $formId;?>" method="post" enctype="multipart/form-data">

                    <div class="clearfix"></div>
                    <div class="clearfix email-fields-container"> 
                        <span class="<?php echo $szFirstSpanClass; ?>">Name</span>
                        <span class="<?php echo $szSecondSpanClass; ?>" style="width:70%;" >
                            <input type="text" name="addEditRemindEmailTemplate[szFriendlyName]"  onkeyup="enable_reminder_template_tab_buttons('<?php echo $mode;?>','<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>','<?php echo $iRemindEmailLanguage;?>');" onblur="enable_reminder_template_tab_buttons('<?php echo $mode;?>','<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>','<?php echo $iRemindEmailLanguage;?>');" placeholder="Friendly Name" id="szFriendlyName" value="<?php echo $_POST['addEditRemindEmailTemplate']['szFriendlyName']; ?>"> 
                        </span>
                        <span class="<?php echo $szSecondSpanClass; ?>" style="width:15%;float:right;">
                            <select name="addEditRemindEmailTemplate[iRemindEmailLanguage]" id="iRemindEmailLanguage" <?php if($idTemplate>0){ ?>onchange="add_edit_RemindEmailTemplate('<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $operationType;?>',this.value);" <?php }?>>
                                <?php 
                                if(!empty($languageArr))
                                {
                                    foreach($languageArr as $languageArrs)
                                    {?>
                                        <option value="<?php echo $languageArrs['id'];?>" <?php if($_POST['addEditRemindEmailTemplate']['iRemindEmailLanguage'] == $languageArrs['id']){ ?> selected="selected"<?php }?>><?php echo ucfirst($languageArrs['szName']);?></option>
                                      <?php  
                                    }
                                }?> 
                            </select>
                        </span>
                    </div>
                    <div class="clearfix email-fields-container"> 
                        <span class="<?php echo $szFirstSpanClass; ?>">Subject</span>
                        <span class="<?php echo $szSecondSpanClass; ?>" style="width:86%;">
                            <input type="text" name="addEditRemindEmailTemplate[szReminderSubject]" placeholder="Subject" id="szReminderSubject" value="<?php echo $_POST['addEditRemindEmailTemplate']['szReminderSubject']; ?>"> 
                        </span>  
                    </div>
                    <div class="clearfix email-fields-container"> 
                        <span class="<?php echo $szFirstSpanClass; ?>" >Body</span>
                        <span class="<?php echo $szSecondSpanClass; ?>" >
                            <textarea style="min-height: 150px;width:98.7%" name="addEditRemindEmailTemplate[szReminderEmailBody]" placeholder="Email Body" id="szReminderEmailBodyPopup"><?php echo $_POST['addEditRemindEmailTemplate']['szReminderEmailBody'];?></textarea>
                        </span>  
                    </div>
                    <div class="clearfix email-fields-container">
                        <span class="<?php echo $szFirstSpanClass; ?>" >&nbsp;</span>
                        <span class="<?php echo $szSecondSpanClass; ?>" style="opacity:0.4;">
                            Available variables: szFirstName, szLastName, szCountryCode, szPhoneNumber, szFromCountry, szToCountry, szFromCity, szToCity, szFromString, szToString, szMode,  szTerms,  szVol, szWeight, szAdminName, szOfficePhone, szMobile, szWebSiteUrl, szBookingRef, szCargo, szCargoDescription, szCargoDetail, szCargoReady
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container">
                        <span class="<?php echo $szFirstSpanClass; ?>">Attachments</span>
                        <span class="<?php echo $szSecondSpanClass; ?>">
                            <span style="display:none;" id="reminder_attachment_container_popup">  
                                <input type="hidden" name="image_form_submit" value="1"/> 
                                <input name="filesToUpload[]" id="filesToUpload_popup" type="file" multiple /> 
                            </span>
                            <div class="uploading_popup none">
                                <label>&nbsp;</label>
                                <img src="<?php echo __BASE_STORE_IMAGE_URL__."/uploading.gif"; ?>" alt="uploading..."/>
                            </div> 
                            <span id="images_preview_popup"> 
                                <?php
                                    $count = 0; 
                                    $bBookingNotice = false;
                                    $bBookingInvoice = false;
                                    $bBookingConfirmation = false;
                                    if(!empty($uploadedFileName))
                                    {  
                                        $count = 1; 
                                        echo '<ul class="reorder_ul reorder-photos-list add-edit-popup-ul">';
                                        foreach($uploadedFileName as $key=>$uploadedFiles)
                                        {  
                                            if($uploadedFiles=='BOOKING_INVOICE' || $uploadedFiles=='BOOKING_NOTICE' || $uploadedFiles=='BOOKING_CONFIRMATION')
                                            {
                                                if($uploadedFiles==='BOOKING_INVOICE')
                                                {
                                                    $szAttachmentLinkText = "Booking Invoice (<i>if applicable</i>)";
                                                    $bBookingInvoice = true;
                                                }
                                                else if($uploadedFiles==='BOOKING_NOTICE')
                                                {
                                                    $szAttachmentLinkText = "Booking Notice (<i>if applicable</i>)";
                                                    $bBookingNotice = true;
                                                }
                                                else if($uploadedFiles==='BOOKING_CONFIRMATION')
                                                {
                                                    $szAttachmentLinkText = "Booking Confirmation (<i>if applicable</i>)";
                                                    $bBookingConfirmation = true;
                                                }
                                                $szAttachmentLink = "Attachment ".$count.": ".$szAttachmentLinkText;
                                                $szBasePath = "javascript:void(0);";
                                            }
                                            else
                                            {
                                                $szAttachmentLink = "Attachment ".$count.": ".$originalFileName[$key];
                                                $szBasePath = __MAIN_SITE_HOME_PAGE_URL__."/attachments/".$uploadedFiles; 
                                            } 
    ?>
                                            <li id="image_li_<?php echo $count; ?>" class="ui-sortable-handle">  
                                                <a href="<?php echo $szBasePath; ?>" target="_blank" style="float:none;" class="image_link"><?php echo $szAttachmentLink; ?></a> 
                                                &nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="closeFile('<?php echo $count; ?>')" class="red_text" style="text-decoration:none;"><strong>X </strong></a> 
                                                
                                                <input type="hidden" name="addEditRemindEmailTemplate[uploadedFileName][]" id="uploadedFileName_<?php echo $count;?>" value="<?php echo $uploadedFiles; ?>">
                                                <input type="hidden" name="addEditRemindEmailTemplate[originalFileName][]" id="originalFileName_<?php echo $count;?>" value="<?php echo $originalFileName[$key]; ?>">
                                                <input type="hidden" name="addEditRemindEmailTemplate[originalFileSize][]" id="originalFileSize_<?php echo $count;?>" value="<?php echo $originalFileSize[$key]; ?>">
                                            </li>
                                            <?php
                                            $count++; 
                                        }
                                        echo '</ul><br>';
                                    }
                                ?>   
                                <input type="hidden" name="addEditRemindEmailTemplate[iAttachmentCounter]" id="iAttachmentCounter" value="<?php echo $count; ?>"> 
                            </span> 
                            <a href="javascript:void(0);" id="add_attachment_reminder_popup">Add attachments</a>
                            <span id="add_document_attachment_popup_BOOKING_INVOICE" <?php if($bBookingInvoice){?> style="display:none;" <?php }?>>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_document_attachments('BOOKING_INVOICE');">Add Invoice</a></span>
                            <span id="add_document_attachment_popup_BOOKING_CONFIRMATION" <?php if($bBookingConfirmation){?> style="display:none;" <?php }?>>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_document_attachments('BOOKING_CONFIRMATION');" >Add Booking Confirmation</a></span>
                            <span id="add_document_attachment_popup_BOOKING_NOTICE" <?php if($bBookingNotice){?> style="display:none;" <?php }?>>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="display_document_attachments('BOOKING_NOTICE');">Add Booking Notice</a></span>
                        </span>
                    </div> 
                    <div class="clearfix email-fields-container">
                        <span class="quote-field-container wd-14">Available on</span>
                        <span class="quote-field-container wd-18">
                            <input type="checkbox" name="addEditRemindEmailTemplate[iRemindScreen]" id="iRemindScreen" value="1" <?php if($_POST['addEditRemindEmailTemplate']['iRemindScreen']==1){?>checked<?php } ?> onclick="enable_reminder_template_tab_buttons('<?php echo $mode;?>','<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>','<?php echo $iRemindEmailLanguage;?>');"> Remind Screen 
                        </span>
                        <span class="quote-field-container wds-13">
                            <input type="checkbox" name="addEditRemindEmailTemplate[iFileLog]" id="iFileLog" value="1" <?php if($_POST['addEditRemindEmailTemplate']['iFileLog']==1){?>checked<?php } ?> onclick="enable_reminder_template_tab_buttons('<?php echo $mode;?>','<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>','<?php echo $iRemindEmailLanguage;?>');"> File Log
                        </span>
                        <span class="quote-field-container wds-20">
                            <input type="checkbox" name="addEditRemindEmailTemplate[iCustomerLog]" id="iCustomerLog" value="1" <?php if($_POST['addEditRemindEmailTemplate']['iCustomerLog']==1){?>checked<?php } ?> onclick="enable_reminder_template_tab_buttons('<?php echo $mode;?>','<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>','<?php echo $iRemindEmailLanguage;?>');"> Customer Log 
                        </span> 
                        <span class="quote-field-container wds-35">&nbsp;</span>
                    </div>
                    <div class="clearfix" style="width:100%;margin-top:10px;">
                        <div class="btn-container"> 
                            <p align="center">
                            <?php 
                                if($mode != "SAVE_NEW_REMIND_EMAIL_TEMPLATE")
                                {
                            ?>
                                <a href="javascript:void(0)" class="button2" onclick="deleteRemindEmailTemplate('<?php echo $szTaskStatus;?>','<?php echo $idBookingFile;?>','<?php echo $idTemplate;?>')">
                                    <span>DELETE</span>
                                </a>
                            <?php
                                }
                            ?>
                                <a href="javascript:void(0)" id="request_reminder_email_template_save_button" class="button1" style="opacity:0.4;">
                                    <span>SAVE</span>
                                </a>
                                <a href="javascript:void(0)"  id="request_reminder_email_template_save_close_button" class="button1" style="opacity:0.4;">
                                    <span>SAVE & CLOSE</span>
                                </a>
                                <a href="javascript:void(0)"  class="button2" onclick="closeRemindEmailPopUp('0');">
                                    <span>CLOSE</span>
                                </a>
                            </p>
                        </div> 
                    </div>  
                    <input type="hidden" name="addEditRemindEmailTemplate[idCrmEmailLogs]" id="idCrmEmailLogs" value="<?php echo $idCrmEmailLogs; ?>">
                    <input type="hidden" name="iFileUploadInProgress" id="iFileUploadInProgress" value="0">
                    <input type="hidden" name="addEditRemindEmailTemplate[szFromPage]" id="szFromPage" value="<?php echo $_POST['addEditRemindEmailTemplate']['szFromPage']; ?>">
                    <input type="hidden" name="addEditRemindEmailTemplate[szAction]" id="szAction" value="<?php echo $_POST['addEditRemindEmailTemplate']['szAction']; ?>"> 
                </form>
            </div>
        </div>
    </div>
    <?php
 }
 
 
 function pendingTrayCargoLineHtml($i,$iSearchMiniVersion)
 {
     ?>
        <td width="6%">
            <input type="hidden" id="idCargoFlag<?php echo $i;?>" value="1" name="cargodetailAry[idCargoFlag][<?php echo $i;?>]">
            <input type="hidden" id="idCargo<?php echo $i;?>" value="<?php echo $_POST['cargodetailAry']['idCargo'][$i];?>" name="cargodetailAry[idCargo][<?php echo $i;?>]">
            <input id="iLength<?php echo $i;?>" class="first-cargo-fields" type="text" name="cargodetailAry[iLength][<?php echo $i;?>]" placeholder="Length" value="<?php echo  $_POST['cargodetailAry']['iLength'][$i];?>" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableSaveButtonForAddingCargoLine();calculatePerColliVolume('<?php echo $i;?>');" onkeypress="return isNumberKey(event);">
        </td>
        <td width="6%">
            <input id="iWidth<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['iWidth'][$i];?>" name="cargodetailAry[iWidth][<?php echo $i;?>]" placeholder="Width" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableSaveButtonForAddingCargoLine();calculatePerColliVolume('<?php echo $i;?>');" onkeypress="return isNumberKey(event);">
        </td>
        <td width="6%">
            <input id="iHeight<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['iHeight'][$i];?>" name="cargodetailAry[iHeight][<?php echo $i;?>]" placeholder="Height" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableSaveButtonForAddingCargoLine();calculatePerColliVolume('<?php echo $i;?>');" onkeypress="return isNumberKey(event);">
        </td>
        <td width="6%">
        <select id="idCargoMeasure<?php echo $i;?>"  name="cargodetailAry[idCargoMeasure][<?php echo $i;?>]" onchange="calculatePerColliVolume('<?php echo $i;?>');">
            <option <?php if($_POST['cargodetailAry']['idCargoMeasure'][$i]==1){?> selected <?php } ?>value="1">cm</option>
            <option <?php if($_POST['cargodetailAry']['idCargoMeasure'][$i]==2){?> selected <?php } ?> value="2">Inch</option>
        </select>
        </td>
        <?php 
        //$width="30%";
        //if($iSearchMiniVersion==8){
            $width="24%";
        ?>
        <td width="6%">
            <input id="iVolume<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['iVolume'][$i];?>" name="cargodetailAry[iVolume][<?php echo $i;?>]" placeholder="Volume" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableSaveButtonForAddingCargoLine();" onkeypress="return isNumberKey(event);">
        </td>
        <?php //}?>
        <td width="6%">
            <input id="iWeight<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['iWeight'][$i];?>" name="cargodetailAry[iWeight][<?php echo $i;?>]" placeholder="Weight" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableSaveButtonForAddingCargoLine();update_total_cargo_details_text();" onkeypress="return isNumberKey(event);">
        </td>
        <td width="6%">                        
            <select id="idWeightMeasure<?php echo $i;?>"  name="cargodetailAry[idWeightMeasure][<?php echo $i;?>]" size="1">
            <option <?php if($_POST['cargodetailAry']['idWeightMeasure'][$i]==1){?> selected <?php } ?> value="1">Kg</option>
            <option <?php if($_POST['cargodetailAry']['idWeightMeasure'][$i]==2){?> selected <?php } ?> value="2">Pounds</option>
            </select>
        </td>
        <td width="6%">
        <input id="iQuantity<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['iQuantity'][$i];?>" name="cargodetailAry[iQuantity][<?php echo $i;?>]" placeholder="Quantity" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" pattern="[0-9]*" onkeypress="return isNumberKey(event);" onkeyup="enableSaveButtonForAddingCargoLine();update_total_cargo_details_text();">
        </td>
        <td width="<?php echo $width;?>">
        <input id="szCommodity<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['szCommodity'][$i];?>" name="cargodetailAry[szCommodity][<?php echo $i;?>]" placeholder="Description" onfocus="check_form_field_not_required(this.form.id,this.id);" onblur="check_form_field_empty_standard(this.form.id,this.id);" onkeyup="enableSaveButtonForAddingCargoLine();"/>
        </td>
        <td width="8%">
        <input id="iColli<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['iColli'][$i];?>" name="cargodetailAry[iColli][<?php echo $i;?>]" placeholder="Colli" onfocus="check_form_field_not_required(this.form.id,this.id);" pattern="[0-9]*" onkeypress="return isNumberKey(event);" onkeyup="enableSaveButtonForAddingCargoLine();">
        </td>
        <td width="12%">
        <input id="szSellerReference<?php echo $i;?>" type="text" value="<?php echo  $_POST['cargodetailAry']['szSellerReference'][$i];?>" name="cargodetailAry[szSellerReference][<?php echo $i;?>]" placeholder="Seller reference" onfocus="check_form_field_not_required(this.form.id,this.id);" onkeyup="enableSaveButtonForAddingCargoLine();">
        </td>
        <td class="input-title">
        <?php if((int)$_POST['iAlreadyPaidBooking']==0){?>    
        <a id="pending_task_cargo_line_add_more_link" class="cargo-add-button" href="javascript:void(0);" onclick="addMoreCargoLinePendingTray('<?php echo $iSearchMiniVersion;?>')"></a>    
        <a id="pending_task_cargo_line_remove_link" class="cargo-remove-button" href="javascript:void(0);" onclick="removeCargoLinePendingTray('<?php echo $i;?>')"></a>
        <?php }?>
        </td>
    <?php 
    
 }
 
function LCLExcludedTradesList()
{ 
    $t_base="management/providerProduct/";  
    $kCourierServiceNew = new cCourierServices();
    $excludedTradeServiceAry = array();
    $excludedTradeServiceAry = $kCourierServiceNew->getAllExcludedLCLTrades();
           
    ?> 
    <script type="text/javascript">
        $(function () {
            $('#couier_excluded_trades_list_table').multiSelect({
                actcls: 'selected_row', //Select Styles
                selector: 'tbody tr', //Row element selected
                except: ['tbody'], //Not after removing the effect of multiple-choice elements selected queue
                statics: ['#table-header', '[data-no="1"]'], //Conditions are excluded row element
                callback: function (items) {
                    var res_ary_new = new Array();
                    var ctr_1 = 0;
                    $(".selected_row").each(function(input_obj){
                        var tr_id = this.id;
                        console.log(tr_id);
                        var tr_id_ary = tr_id.split("courier_excluded_trades_");
                        console.log(tr_id_ary);
                        var idExcludedService = parseInt(tr_id_ary[1]);
                        
                        if(!isNaN(idExcludedService))
                        {
                            res_ary_new[ctr_1] = tr_id_ary[1];
                            ctr_1++;
                        }
                        console.log(ctr_1);
                    });  
                    if(res_ary_new.length)
                    {
                        var excluded_trade_id = res_ary_new.join(';'); 
                        $("#idExcludedServiceType").attr('value',excluded_trade_id);
                        
                        if(ctr_1==1)
                        {
                            $("#add_edit_excluded_trades_data").unbind("click");
                            $("#add_edit_excluded_trades_data").click(function(){
                                update_lcl_excluded_trades(excluded_trade_id,'EDIT_EXCLUDED_LCL_TRADES');
                            });
                            $("#add_edit_excluded_trades_data").attr('style','opacity:1');
                            $("#add_edit_excluded_trades_data_span").html('Edit');
                        }
                        else
                        {
                            $("#add_edit_excluded_trades_data").unbind("click"); 
                            $("#add_edit_excluded_trades_data").attr('style','opacity:0.4');
                            $("#add_edit_excluded_trades_data_span").html('Add');
                        }
                        $("#detete_excluded_trades_data").unbind("click");
                        $("#detete_excluded_trades_data").click(function(){
                            update_lcl_excluded_trades(excluded_trade_id,'DELETE_MULTIPLE_EXCLUDED_LCL_TRADES_CONFIRM');
                        });
                        $("#detete_excluded_trades_data").attr("style",'opacity:1'); 
                    }
                    else
                    {
                        var szModeOfTransIds = '';
                        $("#idExcludedServiceType").attr('value',szModeOfTransIds);
                        $("#detete_excluded_trades_data").unbind("click");
                        $("#detete_excluded_trades_data").attr("style",'opacity:0.4');
                    } 
                }
            });
        })
    </script> 
    <div id="couier_excluded_trade_list_container">    
        <h4><strong><?php echo t($t_base.'title/excluded_service');?></strong></h4>
        <div class="clearfix courier-provider courier-provider-scroll" style="max-height:450px;"> 
            <table cellpadding="5" cellspacing="0" class="format-2" width="100%" id="couier_excluded_trades_list_table">
                <tr>
                    <td width="30%" align="left"><strong><?php echo t($t_base.'fields/forwarder');?></strong></td>
                    <td width="25%" style="text-align:left;"><strong><?php echo t($t_base.'fields/from')." ".t($t_base.'fields/country');?></strong></td>
                    <td width="25%" style="text-align:left;"><strong><?php echo t($t_base.'fields/to')." ".t($t_base.'fields/country');?></strong></td>
                    <td width="20%" style="text-align:left;"><strong><?php echo t($t_base.'fields/type');?></strong></td>
                </tr>
                <?php
                    if(!empty($excludedTradeServiceAry))
                    {
                        foreach($excludedTradeServiceAry as $excludedTradeServiceArys)
                        { 
                            $szCustomerType = '';
                            if($excludedTradeServiceArys['iCustomerType']==1)
                            {
                                $szCustomerType = 'Business only';
                            }
                            else if($excludedTradeServiceArys['iCustomerType']==2)
                            {
                                $szCustomerType = 'Private only';
                            }
                            else if($excludedTradeServiceArys['iCustomerType']==3)
                            {
                                $szCustomerType = 'Business & Private';
                            }
                            ?>
                            <tr class="excluded-service-tr" id="courier_excluded_trades_<?php echo $excludedTradeServiceArys['id'];?>" <?php if($excludedTradeServiceArys['id']==$idCargoLimit){?> style="background:#DBDBDB;cursor:pointer;" <?php }?> onclick="show_edit_excluded_trades_list('<?php echo $excludedTradeServiceArys['id'];?>');">
                                
                                <td><?php echo $excludedTradeServiceArys['szForwarderDisName']." (".$excludedTradeServiceArys['szForwarderAlies'].") ";?></td>
                                <td><?php echo $excludedTradeServiceArys['szOriginCountry']; ?></td>
                                <td><?php echo $excludedTradeServiceArys['szDestinationCountry']; ?></td>
                                <td><?php echo $szCustomerType; ?></td>
                            </tr>
                        <?php		
                        } 		 
                    }  ?> 
                </table>
            </div> 
    </div>   
    <div class="clear-all"></div>
    <br> 
    <div id="courier_excluded_trades_addedit_data">
        <?php
           echo display_lcl_excluded_trades_add_edit_form($kCourierServices);
        ?>	
    </div>   
    <?php 
}
function display_lcl_excluded_trades_add_edit_form($kCourierServices,$courierExcludedTradesAry=array())
{ 
    $t_base="management/providerProduct/";
    if(!empty($kCourierServices->arErrorMessages))
    {
        $szValidationErrorKey = '';
        foreach($kCourierServices->arErrorMessages as $errorKey=>$errorValue)
        { 
            if($errorKey=='szSpecialError')
            {
                $szSpecialErrorMessage = $errorValue ;
            }
            else
            {
                ?>
                <script type="text/javascript">
                    $("#<?php echo $errorKey?>").addClass('red_border');
                </script>
                <?php 
            }
        }
    } 
    
    $kCourierServices_new  = new cCourierServices();
    $kForwarder = new cForwarder();
    $kConfig = new cConfig(); 
    
    
    $forwarderListAry = array();
    $forwarderListAry = $kForwarder->getForwarderArr(); 
    //print_r($forwarderListAry);
    //$forwarderListAry = sortArray($forwarderListAry,'szDisplayName');
    
    $allCountryAry = array();
    $allCountryAry = $kConfig->getAllCountries(true); 
     
    if(!empty($courierExcludedTradesAry))
    {
        $idForwarder = $courierExcludedTradesAry['idForwarder'];
        $idOriginCountry = $courierExcludedTradesAry['idOriginCountry'];
        $idDestinationCountry = $courierExcludedTradesAry['idDestinationCountry'];
        $idExludedTrades = $courierExcludedTradesAry['id'];
        $iCustomerType = $courierExcludedTradesAry['iCustomerType'];
    }
    else
    {
       
        $idForwarder = $_POST['addLCLExcludedTradesAry']['idForwarder'] ;
        $idOriginCountry = $_POST['addLCLExcludedTradesAry']['idOriginCountry'] ;
        $idDestinationCountry = $_POST['addLCLExcludedTradesAry']['idDestinationCountry'] ; 
        $idExludedTrades = $_POST['addLCLExcludedTradesAry']['idExludedTrades'];  
        $iCustomerType = $_POST['addLCLExcludedTradesAry']['iCustomerType']; 
    } 
    if($idExludedTrades>0)
    {
        $regionArr = array();
    }
    else
    {
        $regionArr = $kCourierServices_new->getAllRegion();
    }
    
    $excludedTradeServiceAry = array();
    $excludedTradeServiceAry = $kCourierServices_new->getAllExcludedTrades(); 
    
    //$forwarderListAry = array();
    if($idCourierProvider>0)
    {
        $data = array();
        //$data['idCourierProvider'] = $idCourierProvider;
       // $forwarderListAry = $kCourierServices_new->getAllCourierAgreement($data,true); 
    }
    
    $customerTypeAry = array();
    $customerTypeAry[0]['id'] = 1;
    $customerTypeAry[0]['szType'] = 'Business only';
    $customerTypeAry[1]['id'] = 2;
    $customerTypeAry[1]['szType'] = 'Private only';
    $customerTypeAry[2]['id'] = 3;
    $customerTypeAry[2]['szType'] = 'Business & Private';
?>             
<script type="text/javascript">
    
    var serviceListAry = new Array();
    <?php
        if(!empty($excludedTradeServiceAry))
        { /*
            foreach($excludedTradeServiceAry as $excludedTradeServiceArys)
            {
                $szServiceString = $excludedTradeServiceArys['idCourierProvider']."_".$excludedTradeServiceArys['idForwarder']."_".$excludedTradeServiceArys['idOriginCountry']."_".$excludedTradeServiceArys['idDestinationCountry'];
                ?>
                    //serviceListAry['<?php echo $excludedTradeServiceArys['id']; ?>'] = '<?php echo $szServiceString; ?>';
                <?php
            } */
        }
    ?>  
        function check_duplicate_values_1()
        {
           
            var idForwarder = $("#idForwarder").val();
            var idOriginCountry = $("#idOriginCountry").val();
            var idDestinationCountry = $("#idDestinationCountry").val(); 
            
            var service_tring = idForwarder+"_"+idOriginCountry+"_"+idDestinationCountry ;
              
            if ($.inArray(service_tring, serviceListAry) > -1)
            { 
                return 2;
            } 
            else
            {
                return 1;
            }
        } 
</script> 
<div style="width:100%;">
    <?php if(!empty($szSpecialErrorMessage)){ ?>
        <p style="font-style:italic;color:red;font-size:14px;"> <?php echo $szSpecialErrorMessage; ?></p>
    <?php  }  ?>
    <form action="javascript:void(0);" name="courierExcludeTradesForm" method="post" id="courierExcludeTradesForm">
        <table cellpadding="0" cellspacing="1" width="100%">
            <tr>  
                <td class="font-12" width="30%" style="text-align:left; padding:0 4px;"><?php echo t($t_base.'fields/forwarder');?></td>
                <td class="font-12" width="25%" style="text-align:left; padding:0 4px;"><?php echo t($t_base.'fields/from')." ".t($t_base.'fields/country'); ?></td>
                <td class="font-12" width="25%" style="text-align:left;"><?php echo t($t_base.'fields/to')." ".t($t_base.'fields/country');?></td> 
                <td class="font-12" width="20%" style="text-align:left;"><?php echo t($t_base.'fields/type');?></td> 
            </tr>
            <tr>
                
                <td style="padding:0 4px;" id="excluded_trade_forwarder_container">
                    <select id="idForwarder" name="addLCLExcludedTradesAry[idForwarder]" onchange="checkLCLExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($forwarderListAry))
                            {
                                foreach($forwarderListAry as $forwarderListArys)
                                {
                                    ?>
                                    <option  value='<?php echo $forwarderListArys['id']?>' <?php if($idForwarder==$forwarderListArys['id']){?> selected <?php }?>><?php echo $forwarderListArys['szDisplayName']." (".$forwarderListArys['szForwarderAlias'].")"; ?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </td> 
                <td style="padding:0 4px;">
                    <select id="idOriginCountry" name="addLCLExcludedTradesAry[idOriginCountry]" onchange="checkLCLExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                                ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addLCLExcludedTradesAry']['idOriginCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                <?php
                                }
                            }?>
                        <?php 
                            if(!empty($allCountryAry))
                            {
                                foreach($allCountryAry as $allCountryArys)
                                {
                                    ?>
                                    <option  value='<?php echo $allCountryArys['id']?>' <?php if($idOriginCountry==$allCountryArys['id']){?> selected <?php }?>><?php echo $allCountryArys['szCountryName'];?></option>
                                    <?php
                                }
                            }?>
                    </select>
                </td> 
                <td>
                    <select id="idDestinationCountry" name="addLCLExcludedTradesAry[idDestinationCountry]" onchange="checkLCLExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($regionArr))
                            {
                                foreach($regionArr as $regionArrs)
                                {
                                    $regionValue=$regionArrs['id']."_r";
                                ?>
                                    <option  value='<?php echo $regionValue?>' <?php if($_POST['addLCLExcludedTradesAry']['idDestinationCountry']==$regionValue){?> selected <?php }?>><?php echo $regionArrs['szRegionName'];?></option>
                                <?php
                                }
                            }?>
                        <?php 
                            if(!empty($allCountryAry))
                            {
                                foreach($allCountryAry as $allCountryArys)
                                {
                                    ?>
                                    <option  value='<?php echo $allCountryArys['id']?>' <?php if($idDestinationCountry==$allCountryArys['id']){?> selected <?php }?>><?php echo $allCountryArys['szCountryName'];?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </td>  
                <td>
                    <select id="iCustomerType" name="addLCLExcludedTradesAry[iCustomerType]" onchange="checkLCLExludedTradesData();">
                        <option value=''>Select</option>
                        <?php 
                            if(!empty($customerTypeAry))
                            {
                                foreach($customerTypeAry as $customerTypeArys)
                                {
                                    ?>
                                    <option  value='<?php echo $customerTypeArys['id']?>' <?php if($iCustomerType==$customerTypeArys['id']){?> selected <?php }?>><?php echo $customerTypeArys['szType'];?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select> 
                </td>
            </tr>
            <tr>
                <td colspan="5" style="text-align:right;padding-top:5px;">   
                    <a href="javascript:void(0)" class="button1" style="opacity:0.4" id="add_edit_excluded_trades_data"><span style="min-width:50px;" id="add_edit_excluded_trades_data_span"><?php if($idExludedTrades>0){?><?=t($t_base.'fields/save');?> <?php }else{?><?=t($t_base.'fields/add');?><?php }?></span></a>
                    <a href="javascript:void(0)" class="button2" <?php if($_POST['addLCLExcludedTradesAry']['id']==0){?>style="opacity:0.4"<?php }?> id="detete_excluded_trades_data" align="left"><span style="min-width:50px;" id="detete_excluded_trades_data_span"><?php if($idExludedTrades>0){?><?=t($t_base.'fields/cancel');?> <?php }else{?><?=t($t_base.'fields/delete');?><?php }?></span></a>
                </td>
            </tr>
        </table>
        <input type="hidden" name="addLCLExcludedTradesAry[idExludedTrades]" id="idExludedTrades" value="<?php echo $idExludedTrades;?>"> 
    </form>
</div>
    <?php
}

function displayForwarderTurnover($forwarderTurnoverAry)
{	 
    $t_base="management/billings/";
?>
    <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
        <tr>
            <td width="25%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/forwarder')?></strong> </td>
            <td width="25%" style="text-align:right" valign="top"><strong><?=t($t_base.'fields/total_sales')?></strong> </td>
            <td width="25%" style="text-align:right" valign="top"><strong><?=t($t_base.'fields/vat')?></strong> </td>
            <td width="25%" style="text-align:right" valign="top"><strong><?=t($t_base.'fields/gross_proit')?></strong> </td> 
        </tr>
    <?php
        if(!empty($forwarderTurnoverAry))
        {	
            $ctr=1;
            foreach($forwarderTurnoverAry as $forwarderTurnoverArys)
            {	 
                ?> 		
                <tr align='center' >
                    <td style='text-align:left'><?php echo $forwarderTurnoverArys['szForwarderDispName']." (".$forwarderTurnoverArys['szForwarderAlias'].")"; ?></td>
                    <td style='text-align:right;'><?php echo "USD ".number_format((float)$forwarderTurnoverArys['fTotalSalesUSD'],2); ?></td>
                    <td style='text-align:right'><?php echo "USD ".number_format((float)$forwarderTurnoverArys['fTotalVatUSD'],2); ?></td> 
                    <td style='text-align:right'><?php echo "USD ".number_format((float)$forwarderTurnoverArys['fTotalGrossProfitUSD'],2); ?></td>
                </tr>	
                <?php 
            }
        }
        else
        {
           // echo "<tr><td colspan='4'><CENTER><b>".t($t_base.'fields/no_record_found')."</b><CENTER></td></tr>";
            ?>
            <tr align='center' >
                <td colspan="4" style="height:40px;"></td>
            </tr> 
            <?php
        } 
    ?>
    </table> 
    <div class="btn-container" style="text-align:right;">
        <a href="javascript:void(0)" class="button2" onclick="submit_forwarder_turn_over_form('DOWNLOAD_FORWARDER_TURNOVER_DATA')"><span>Download</span></a>
    </div>
<?php
}

function display_turnover_search_form($searchDataAry)
{
    if(!empty($_POST['forwarderTurnoverAry']['dtFromDate']))
    {
        $dtFromDate = $_POST['forwarderTurnoverAry']['dtFromDate'];
    }
    else
    {
        $dtFromDate = $searchDataAry['dtFromDate'];
    } 
    
    if(!empty($_POST['forwarderTurnoverAry']['dtToDate']))
    {
        $dtToDate = $_POST['forwarderTurnoverAry']['dtToDate'];
    }
    else
    {
        $dtToDate = $searchDataAry['dtToDate'];
    } 
?>
    <script type="text/javascript">
        $().ready(function(){	 
            $("#dtFromDate").datepicker({ 
                onClose: function( selectedDate ) {
                   var dateAry = selectedDate.split("/");
                   var date = dateAry[0];
                   var month = dateAry[1];
                   var year = dateAry[2]; 
                   var dtMinDate = new Date(year, month-1,date) ; 
                   $( "#dtToDate" ).datepicker("option", "minDate", dtMinDate);
                }
            });
            
            $("#dtToDate").datepicker({ 
                onClose: function( selectedDate ){
                    var dateAry = selectedDate.split("/");
                    var date = dateAry[0];
                    var month = dateAry[1];
                    var year = dateAry[2];
                    var dtMaxDate = new Date(year, month-1,date) ;
                   $( "#dtFromDate" ).datepicker("option", "maxDate", dtMaxDate );
               }
           }); 
        });
    </script>
    <form method="post" id="forwarder_turnover_form">
        <table class="format-3" id="forwarder_turnover_table" width="100%">
            <tr>
                <td style="width:10%;">From date</td>
                <td style="width:30%"><input type="text" readonly="readonly" name="forwarderTurnoverAry[dtFromDate]" id="dtFromDate" value="<?php echo $dtFromDate; ?>"></td> 
                <td style="width:10%;text-align:right;"> To date</td>
                <td style="width:30%"><input type="text" readonly="readonly" name="forwarderTurnoverAry[dtToDate]" id="dtToDate" value="<?php echo $dtToDate; ?>"></td>
                <td style="width:10%;text-align:right;"><a href="javascript:void(0)" class="button1" onclick="submit_forwarder_turn_over_form()"><span>Generate</span></a></td>
            </tr>
        </table> 
    </form>
    <?php
}
function forwarderTransportecaInvoiceListHtml($paymentDetails,$t_base,$iPage=1,$limit=__MAX_CONFIRMED_BOOKING_NEW_PER_PAGE__)
{
    
    $iGrandTotal=count($paymentDetails);
        require_once (__APP_PATH__ ."/inc/classes/pagination.class.php");
        $kPagination  = new Pagination($iGrandTotal,$limit,__LINK_CONFIRMED_BOOKING_NEW_PER_PAGE__);
        //echo $limit;
        if($iPage==1)
        {
            $start=0;
            $end=$limit;
            if($iGrandTotal<$end)
            {
                $end=$iGrandTotal;
            }
        }
        else
        {
            $start=$limit*($iPage-1);
            $end=$iPage*$limit;
            if($end>$iGrandTotal)
            {
                $end=$iGrandTotal;
            }
            $startValue=$start+1;
            if($startValue>$iGrandTotal)
            {
                $iPage=$iPage-1;
                $start=$limit*($iPage-1);
                $end=$iPage*$limit;
                if($end>$iGrandTotal)
                {
                    $end=$iGrandTotal;
                } 
            }
        } 
?>
    <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
		<tr>
			<td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/invoice_date')?></strong> </td>
			<td width="15%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/reference')?></strong> </td>
			<td width="20%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/invoice_party')?></strong></td>
			<td width="35%" style='text-align:left;' valign="top"><strong><?=t($t_base.'fields/description')?></strong></td>
			<td width="15%" style='text-align:right;' valign="top"><strong><?=t($t_base.'fields/amount')?></strong> </td>
		</tr>
		<?php	
			//print_r($paymentDetails);
			if($paymentDetails!=array())
			{	
				$count=1; 
				for($i=$start;$i<$end;++$i)
				{ 
                                    $dtPaymentConfirmed = "";
                                    if(!empty($paymentDetails[$i]['dtPaymentConfirmed']) && $paymentDetails[$i]['dtPaymentConfirmed']!='0000-00-00 00:00:00')
                                    {
                                        $dtPaymentConfirmed = $paymentDetails[$i]['dtPaymentConfirmed'];
                                    }
                                    else if(!empty($paymentDetails[$i]['dtInvoiceOn']) && $paymentDetails[$i]['dtInvoiceOn']!='0000-00-00 00:00:00')
                                    {
                                        $dtPaymentConfirmed = $paymentDetails[$i]['dtInvoiceOn'];
                                    }
                                    else if(!empty($paymentDetails[$i]['dtPaymentScheduled']) && $paymentDetails[$i]['dtPaymentScheduled']!='0000-00-00 00:00:00')
                                    {
                                        $dtPaymentConfirmed = $paymentDetails[$i]['dtPaymentScheduled'];
                                    }
                                    $paymentDetails[$i]['dtPaymentConfirmed'] = $dtPaymentConfirmed;
                                    
                                    if($paymentDetails[$i]['iDebitCredit']=='1' || $paymentDetails[$i]['iDebitCredit']=='6')
                                    {  
                                        if($paymentDetails[$i]['iPrivateShipping']==1)
                                        {
                                            $szCustomerName = $paymentDetails[$i]['szFirstName']." ".$paymentDetails[$i]['szFirstName'];
                                        }
                                        else
                                        {
                                            $szCustomerName = $paymentDetails[$i]['szCustomerCompanyName'];
                                        }
                                        $iInsuranceIncluded = $paymentDetails[$i]['iInsuranceIncluded'];
                                        $iInsuranceCancelledIndividually = $paymentDetails[$i]['iInsuranceCancelledIndividually'];
                                        $fTotalInvoiceAmount = $paymentDetails[$i]['fTotalPriceCustomerCurrency'] + $paymentDetails[$i]['fTotalVat'];
                                        
                                        if($iInsuranceIncluded==1 && $iInsuranceCancelledIndividually!=1)
                                        {
                                            $fTotalInvoiceAmount += $paymentDetails[$i]['fTotalInsuranceCostForBookingCustomerCurrency'];
                                        }
                                        
                                        if($paymentDetails[$i]['iDebitCredit']=='6')
                                        {
                                            $szBookingRef = "Cancelled:  ".$paymentDetails[$i]['szBookingRef'];
                                            $iType = 2;
                                            $szAmountStr = "(".$paymentDetails[$i]['szCustomerCurrency']." ".number_format($fTotalInvoiceAmount,2).")";
                                        }
                                        else
                                        {
                                            $szBookingRef = $paymentDetails[$i]['szBookingRef'];
                                            $iType = 1;
                                            $szAmountStr = $paymentDetails[$i]['szCustomerCurrency']." ".number_format($fTotalInvoiceAmount,2);
                                        }  
                                        ?>
                                        <tr id="booking_data_<?=$count;?>" onclick="viewNoticeTransactionDetails('booking_data_<?=$count;?>','<?=$paymentDetails[$i]["idBooking"]?>','<?=$paymentDetails[$i]["idForwarder"]?>','2','<?php echo $iType; ?>')">
                                            <td style='text-align:left;'><?=date('d/m/Y',strtotime($dtPaymentConfirmed))?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szInvoice']?></td>
                                            <td style='text-align:left;'><?=returnLimitData($szCustomerName,20)?></td>
                                            <td style='text-align:left;'><?=$szBookingRef;?></td>
                                            <td style='text-align:right;'><?php echo $szAmountStr; ?></td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    if(($paymentDetails[$i]['iDebitCredit']=='3' && $paymentDetails[$i]['iDebitCredit']!='1'))
                                    { 
                                        ?>
                                        <tr id="booking_data_<?=$count;?>" onclick="viewNoticeTransactionDetails('booking_data_<?=$count;?>','<?=$paymentDetails[$i]["idBooking"]?>','<?=$paymentDetails[$i]["idForwarder"]?>','2')">
                                            <td style='text-align:left;'><?=date('d/m/Y',strtotime($paymentDetails[$i]['dtPaymentConfirmed']))?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szInvoice']?></td>
                                            <td style='text-align:left;'><?=returnLimitData($paymentDetails[$i]['szDisplayName'],20)?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szBooking']?></td>
                                            <td style='text-align:right;'><?=$paymentDetails[$i]['szCurrency']." ".number_format($paymentDetails[$i]['fTotalPriceForwarderCurrency'],2)?></td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    else if(($paymentDetails[$i]['iDebitCredit']=='8'))
                                    {
                                        if($paymentDetails[$i]['szBooking']=='Courier Booking and Labels Invoice' && $paymentDetails[$i]['fTotalPriceForwarderCurrency']<0)
                                        {
                                            $paymentDetails[$i]['szBooking'] = 'Courier Booking and Labels Credit Note';
                                            $szAmountStr = " (".$paymentDetails[$i]['szCurrency']." ".number_format((float)abs($paymentDetails[$i]['fTotalPriceForwarderCurrency']),2).")";
                                        }
                                        else
                                        {
                                            $szAmountStr = $paymentDetails[$i]['szCurrency']." ".number_format($paymentDetails[$i]['fTotalPriceForwarderCurrency'],2);
                                        }
                                        ?>
                                        <tr id="booking_data_<?=$count;?>" onclick="viewCourierLabelDetails('booking_data_<?=$count;?>','<?=$paymentDetails[$i]["idBooking"]?>','<?=$paymentDetails[$i]["idForwarder"]?>','2')">
                                            <td style='text-align:left;'><?=date('d/m/Y',strtotime($paymentDetails[$i]['dtPaymentConfirmed']))?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szInvoice']?></td>
                                            <td style='text-align:left;'><?=returnLimitData($paymentDetails[$i]['szDisplayName'],20)?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szBooking']?></td>
                                            <td style='text-align:right;'><?=$szAmountStr?></td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    else if(($paymentDetails[$i]['iDebitCredit']=='12'))
                                    {
                                        if($paymentDetails[$i]['szBooking']==__HANDLING_FEE_INVOICE_TEXT__ && $paymentDetails[$i]['fTotalPriceForwarderCurrency']<0)
                                        {
                                            $paymentDetails[$i]['szBooking'] = __HANDLING_FEE_CREDIT_NOTE_TEXT__;
                                            $szAmountStr = " (".$paymentDetails[$i]['szCurrency']." ".number_format((float)abs($paymentDetails[$i]['fTotalPriceForwarderCurrency']),2).")";
                                        }
                                        else
                                        {
                                            $szAmountStr = $paymentDetails[$i]['szCurrency']." ".number_format($paymentDetails[$i]['fTotalPriceForwarderCurrency'],2);
                                        }
                                        ?>
                                        <tr id="booking_data_<?=$count;?>" onclick="viewCourierLabelDetails('booking_data_<?=$count;?>','<?=$paymentDetails[$i]["szInvoice"]?>','<?=$paymentDetails[$i]["idForwarder"]?>','2','HANDLING_FEE')">
                                            <td style='text-align:left;'><?=date('d/m/Y',strtotime($paymentDetails[$i]['dtPaymentConfirmed']))?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szInvoice']?></td>
                                            <td style='text-align:left;'><?=returnLimitData($paymentDetails[$i]['szDisplayName'],20)?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szBooking']?></td>
                                            <td style='text-align:right;'><?=$szAmountStr?></td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    else if($paymentDetails[$i]['iDebitCredit']=='5')
                                    {
                                        ?>
                                        <tr id="booking_data_<?=$count;?>" onclick="viewNoticeTransactionDetailsUploadServic('booking_data_<?=$count;?>','<?=$paymentDetails[$i]["szInvoice"]?>','<?=$paymentDetails[$i]["idForwarder"]?>','2')">
                                            <td style='text-align:left;'><?=date('d/m/Y',strtotime($paymentDetails[$i]['dtPaymentConfirmed']))?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szInvoice']?></td>
                                            <td style='text-align:left;'><?=returnLimitData($paymentDetails[$i]['szDisplayName'],20)?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szBooking']?></td>
                                            <td style='text-align:right;'><?=$paymentDetails[$i]['szForwarderCurrency']." ".number_format($paymentDetails[$i]['fTotalPriceForwarderCurrency'],2)?></td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
                                    else if($paymentDetails[$i]['iDebitCredit']=='7')
                                    {
                                        $szFromPage='';
                                        if($paymentDetails[$i]['iInsuranceStatus']==__BOOKING_INSURANCE_STATUS_CANCELLED__)
                                        {
                                            $szFromPage = 'CANCELLED_INSURED_BOOKING';
                                            $szAmountStr = "(".$paymentDetails[$i]['szCurrency']." ".number_format($paymentDetails[$i]['fTotalPriceForwarderCurrency'],2).")";
                                        }
                                        else
                                        {
                                            $szAmountStr = $paymentDetails[$i]['szCurrency']." ".number_format($paymentDetails[$i]['fTotalPriceForwarderCurrency'],2) ;
                                        } 
                                        ?>
                                        <tr id="booking_data_<?=$count;?>" onclick="viewNoticeTransactionDetailsUploadServic('booking_data_<?=$count;?>','<?=$paymentDetails[$i]["idBooking"]?>','','3','<?php echo $szFromPage; ?>')">
                                            <td style='text-align:left;'><?=date('d/m/Y',strtotime($paymentDetails[$i]['dtPaymentConfirmed']))?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szInvoice']?></td>
                                            <td style='text-align:left;'><?=returnLimitData($paymentDetails[$i]['szDisplayName'],20)?></td>
                                            <td style='text-align:left;'><?=$paymentDetails[$i]['szBooking']?></td>
                                            <td style='text-align:right;'><?=$szAmountStr; ?></td>
                                        </tr>
                                        <?php
                                        $count++;
                                    }
				}
			}
			else
			{
				echo "<tr><td colspan='6'><CENTER><b>".t($t_base.'fields/no_record_found')."</b><CENTER></td></tr>";
			}
		?>
		</table>
                <div class="page_limit clearfix">
                <div class="pagination">
                    <?php

                        $kPagination->szMode = "DISPLAY_BOOKING_LIST_PAGINATION";
                        //$kPagination->idBookingFile = $idBookingFile;
                        $kPagination->paginate('display_booking_list_pagination'); 
                        if($kPagination->links_per_page>1)
                        {
                            echo $kPagination->renderFullNav();
                        }        
                    ?>
                       </div>
                       <div class="limit">Records per page
                           <select id="limit" name="limit" onchange="display_booking_list_pagination('1')">
                               <option <?php if($limit=='100' || $_POST['limit']==''){?> selected <?php }?> value="100">100</option>
                                <option <?php if($limit=='250'){?> selected <?php }?> value="250">250</option>
                                <option <?php if($limit=='500'){?> selected <?php }?> value="500">500</option>
                                <option <?php if($limit=='1000'){?> selected <?php }?> value="1000">1000</option>
                           </select>
                       </div>    
                    </div>

                    <input type="hidden" id="page" name="page" value="<?php echo $iPage;?>">
		<a class="button1" id='view_notice' style="opacity:0.4;float: right;"><span><?=t($t_base."fields/view_invoice")?></span></a>
                <?php
}

function display_transporteca_turnover_search_form($searchDataAry)
{
    if(!empty($_POST['transportecaTurnoverAry']['dtFromDate']))
    {
        $dtFromDate = $_POST['transportecaTurnoverAry']['dtFromDate'];
    }
    else
    {
        $dtFromDate = $searchDataAry['dtFromDate'];
    } 
    
    if(!empty($_POST['transportecaTurnoverAry']['dtToDate']))
    {
        $dtToDate = $_POST['transportecaTurnoverAry']['dtToDate'];
    }
    else
    {
        $dtToDate = $searchDataAry['dtToDate'];
    } 
?>
    <script type="text/javascript">
        $().ready(function(){	 
            $("#dtFromDate").datepicker({ 
                onClose: function( selectedDate ) {
                   var dateAry = selectedDate.split("/");
                   var date = dateAry[0];
                   var month = dateAry[1];
                   var year = dateAry[2]; 
                   var dtMinDate = new Date(year, month-1,date) ; 
                   $( "#dtToDate" ).datepicker("option", "minDate", dtMinDate);
                }
            });
            
            $("#dtToDate").datepicker({ 
                onClose: function( selectedDate ){
                    var dateAry = selectedDate.split("/");
                    var date = dateAry[0];
                    var month = dateAry[1];
                    var year = dateAry[2];
                    var dtMaxDate = new Date(year, month-1,date) ;
                   $( "#dtFromDate" ).datepicker("option", "maxDate", dtMaxDate );
               }
           }); 
        });
    </script>
    <form method="post" id="transporteca_turnover_form">
        <table class="format-3" id="forwarder_turnover_table" width="100%">
            <tr>
                <td style="width:10%;">From date</td>
                <td style="width:30%"><input type="text" readonly="readonly" name="transportecaTurnoverAry[dtFromDate]" id="dtFromDate" value="<?php echo $dtFromDate; ?>"></td> 
                <td style="width:10%;text-align:right;"> To date</td>
                <td style="width:30%"><input type="text" readonly="readonly" name="transportecaTurnoverAry[dtToDate]" id="dtToDate" value="<?php echo $dtToDate; ?>"></td>
                <td style="width:10%;text-align:right;"><a href="javascript:void(0)" class="button1" onclick="submit_transporteca_turn_over_form()"><span>Generate</span></a></td>
            </tr>
        </table> 
    </form>
    <?php
}


function displayTransportecaTurnover($transportecaTurnoverAry)
{	 
    $t_base="management/billings/";
?>
    <table cellpadding="0"  cellspacing="0" border="0" class="format-2" width="100%" style="margin-bottom:20px;" id="booking_table">
        <tr>
            <td width="60%" style="text-align:left" valign="top"><strong><?=t($t_base.'fields/revenue_source')?></strong> </td>
            <td width="40%" style="text-align:right" valign="top"><strong><?=t($t_base.'fields/total')?></strong> </td>
        </tr>
    <?php
        $totalReferralFee=0;
        $totalHandlingFee=0;
        $totalInsurance=0;
        $totalLabelFee=0;
        $totalfCurrencyMarkup=0;
        $totaluploadServiceFee=0;
        if(!empty($transportecaTurnoverAry))
        {	
            $ctr=1;
            foreach($transportecaTurnoverAry as $transportecaTurnoverArys)
            {	
                $referralFee=0;
                $handlingFee=0;
                $Insurance=0;
                $labelfee=0;
                
                $totalfCurrencyMarkup=$totalfCurrencyMarkup+$transportecaTurnoverArys['fCurrencyMarkup'];
                if(($transportecaTurnoverArys['iDebitCredit']=='3' && $transportecaTurnoverArys['iDebitCredit']!='1'))
                {
                    if($transportecaTurnoverArys['fExchangeRate']!='')
                    {
                        $referralFee=$transportecaTurnoverArys['fTotalPriceForwarderCurrency']*$transportecaTurnoverArys['fExchangeRate'];
                    }
                    
                    $totalReferralFee=$totalReferralFee+$referralFee;
                }
                else if($transportecaTurnoverArys['iDebitCredit']=='8')
                {
                                       
                    if($transportecaTurnoverArys['fExchangeRate']!='')
                    {
                        $labelfee=$transportecaTurnoverArys['fTotalPriceForwarderCurrency']*$transportecaTurnoverArys['fExchangeRate'];
                    }
                   
                    if($transportecaTurnoverArys['szBooking']=='Courier Booking and Labels Invoice' && $transportecaTurnoverArys['fTotalPriceForwarderCurrency']<0)
                    {
                        $totalLabelFee=$totalLabelFee-$labelfee;
                    }
                    else
                    {
                        $totalLabelFee=$totalLabelFee+$labelfee;
                    }
                    
                     
                }
                else if(($transportecaTurnoverArys['iDebitCredit']=='12'))
                {
                    
                    if($transportecaTurnoverArys['fExchangeRate']!='')
                    {
                        $handlingFee=$transportecaTurnoverArys['fTotalPriceForwarderCurrency']*$transportecaTurnoverArys['fExchangeRate'];
                    }
                    if($forwarderTurnoverArys['szBooking']==__HANDLING_FEE_INVOICE_TEXT__ && $forwarderTurnoverArys['fTotalPriceForwarderCurrency']<0)
                    {
                        $totalHandlingFee=$totalHandlingFee-$handlingFee;
                    }
                    else
                    {
                        $totalHandlingFee=$totalHandlingFee+$handlingFee;
                    }                    
                }
                else if($transportecaTurnoverArys['iDebitCredit']=='7')
                {
                    
                    if($transportecaTurnoverArys['fExchangeRate']!='')
                    {
                        $Insurance=$transportecaTurnoverArys['fTotalPriceForwarderCurrency']*$transportecaTurnoverArys['fExchangeRate'];
                    }
                    $szFromPage='';
                    if($forwarderTurnoverArys['iInsuranceStatus']==__BOOKING_INSURANCE_STATUS_CANCELLED__)
                    {
                        $totalInsurance=$totalInsurance-$Insurance;
                    }
                    else
                    {
                        $totalInsurance=$totalInsurance+$Insurance;
                    } 
                }
                else if($transportecaTurnoverArys['iDebitCredit']=='5')
                {
                    if($transportecaTurnoverArys['fExchangeRate']!='')
                    {
                        $uploadServiceFee=$transportecaTurnoverArys['fTotalPriceForwarderCurrency']*$transportecaTurnoverArys['fExchangeRate'];
                    }
                    
                    $totaluploadServiceFee=$totaluploadServiceFee+$uploadServiceFee;
                }
                
            }
        }
        
        if($totaluploadServiceFee<0)
        {
            $totaluploadServiceFee=  str_replace("-","",$totaluploadServiceFee);
            $totaluploadServiceFeeStr="(USD ".number_format((float)$totaluploadServiceFee,2).")";
        }
        else
        {
            $totaluploadServiceFeeStr="USD ".number_format((float)$totaluploadServiceFee,2);
        }
        
        if($totalReferralFee<0)
        {
            $totalReferralFee=  str_replace("-","",$totalReferralFee);
            $totalReferralFeeStr="(USD ".number_format((float)$totalReferralFee,2).")";
        }
        else
        {
            $totalReferralFeeStr="USD ".number_format((float)$totalReferralFee,2);
        }
        
        if($totalLabelFee<0)
        {
            $totalLabelFee=  str_replace("-","",$totalLabelFee);
            $totalLabelFeeStr="(USD ".number_format((float)$totalLabelFee,2).")";
        }
        else
        {
            $totalLabelFeeStr="USD ".number_format((float)$totalLabelFee,2);
        }
        
        
        if($totalHandlingFee<0)
        {
            $totalHandlingFee=  str_replace("-","",$totalHandlingFee);
            $totalHandlingFeeStr="(USD ".number_format((float)$totalHandlingFee,2).")";
        }
        else
        {
            $totalHandlingFeeStr="USD ".number_format((float)$totalHandlingFee,2);
        }
        
        if($totalInsurance<0)
        {
            $totalInsurance=  str_replace("-","",$totalInsurance);
            $totalInsuranceStr="(USD ".number_format((float)$totalInsurance,2).")";
        }
        else
        {
            $totalInsuranceStr="USD ".number_format((float)$totalInsurance,2);
        }
        
        
        if($totalfCurrencyMarkup<0)
        {
            $totalfCurrencyMarkup=  str_replace("-","",$totalfCurrencyMarkup);
            $totalfCurrencyMarkupStr="(USD ".number_format((float)$totalfCurrencyMarkup,2).")";
        }
        else
        {
            $totalfCurrencyMarkupStr="USD ".number_format((float)$totalfCurrencyMarkup,2);
        }
    ?>
           	
    <tr align='center' >
        <td style='text-align:left'><?php echo "Referral fee"; ?></td>
        <td style='text-align:right;'><?php echo $totalReferralFeeStr; ?></td>
    </tr>	
    <tr align='center' >
        <td style='text-align:left'><?php echo "Handling fee"; ?></td>
        <td style='text-align:right;'><?php echo $totalHandlingFeeStr; ?></td>
    </tr> 
    <tr align='center' >
        <td style='text-align:left'><?php echo "Label fee"; ?></td>
        <td style='text-align:right;'><?php echo $totalLabelFeeStr; ?></td>
    </tr> 
    <tr align='center' >
        <td style='text-align:left'><?php echo "Transporteca Upload Service"; ?></td>
        <td style='text-align:right;'><?php echo $totaluploadServiceFeeStr; ?></td>
    </tr>
    <tr align='center' >
        <td style='text-align:left'><?php echo "Insurance"; ?></td>
        <td style='text-align:right;'><?php echo $totalInsuranceStr; ?></td>
    </tr>
    <tr align='center' >
        <td style='text-align:left'><?php echo "Currency markup (not included in download)"; ?></td>
        <td style='text-align:right;'><?php echo $totalfCurrencyMarkupStr; ?></td>
    </tr>
    </table> 
    <div class="btn-container" style="text-align:right;">
        <a href="javascript:void(0)" class="button2" onclick="submit_transporteca_turn_over_form('DOWNLOAD_TRANSPORTECA_TURNOVER_DATA')"><span>Download</span></a>
    </div>
<?php
}
function getCustomerTimeZone()
{
    $ret_ary = array();
    $ret_ary = getCountryCodeByIPAddress();
    if(empty($ret_ary))
    {
        $ret_ary['szCountryCode'] = 'DK';
    } 
    $szCustomerTimeZone = get_time_zone($ret_ary['szCountryCode'],$ret_ary['szRegion']); 
    $_SESSION['szCustomerTimeZoneGlobal'] = $szCustomerTimeZone ; 
    return $szCustomerTimeZone;
}
function is_base64_encoded($data)
{
    if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
       return TRUE;
    } else {
       return FALSE;
    }
}
?>
