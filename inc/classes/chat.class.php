<?php
/**
 * This file is the container for partner api related functionality. 
 *
 * api.class.php
 *
 * @copyright Copyright (C) 2015 Transporteca
 * @author Ajay
 */
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" ); 

class cChat extends cDatabase
{    
    function fetchChatTrascript()
    {
        $szUserName = __ZOPIM_CHAT_USER_NAME__;
        $szPassword = __ZOPIM_CHAT_PASSWORD__;
 
        $headers = array(
            'Content-Type:application/json',
            'Authorization: Basic '. base64_encode($szUserName.":".$szPassword)
        );
        
        /*
        $dtLastRunDate = $this->getLastEmailReceivedDate(); 
        if(empty($dtLastRunDate))
        {
            $dtLastRunDate = date("Y-m-d",strtotime("-15 days"))."T00:00:00";
        }
        * 
        */

        //$dtLastRunDate = date("Y-m-d",strtotime("-2 HOURS"))."T".date("h:i:s",strtotime("-2 HOURS")); 
          
        $dtLastRunDate = date("Y-m-d",strtotime("-7 days"))."T00:00:00";
        $szUrl = __ZOPIM_CHAT_BASE_URL__."?q=timestamp:[".$dtLastRunDate." TO *]";
        //$szUrl = urlencode($szUrl);
         
        $ch = curl_init();                 
        curl_setopt($ch, CURLOPT_URL,$szUrl);      
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch); 
        $infoAry = curl_getinfo($ch);
        
        $filename = __APP_PATH_ROOT__."/logs/debug_zopim_test_".date('Y-m-d').".log";
        
        $resultAry = array();
        if($infoAry['http_code']==200)
        {
            $resultAry = json_decode($result,true);  
        }
        else
        {
            $strdata=array();
            $strdata[0]= "\n\n Response ary \n\n".print_R($result,true)." \n\n ";
            $strdata[1]= "\n\n Information Ary \n\n".print_R($infoAry,true)." \n\n ";
            $strdata[1]= "\n\n Curl Erorr: \n\n".  curl_error($ch)." \n\n ";
            file_log_session(true, $strdata, $filename);
        }
         
        if(!empty($resultAry['chats']))
        {
            $kUser = new cUser(); 
            $chatLogsAry = $resultAry['chats'];
            foreach($chatLogsAry as $chatLogsArys)
            {
                $szChatId = $chatLogsArys['id']; 
                if($this->isChatAlreadyAdded($szChatId))
                {
                    /*
                    $idZopimChat = $this->idZopimChatLogs;
                    $zopimChatTranscriptAry = array();
                    $zopimChatTranscriptAry = $kChat->getAllZopimChatMessageLogs($idZopimChat);
                    if(empty($zopimChatTranscriptAry))
                    {
                        
                    } */ 
                    /* If the chat id is already added into the system we just continue to next record
                    */
                    continue; 
                }  
                
                $szChatType = $chatLogsArys['type'];
                 
                $kBooking = new cBooking();
                $kUser = new cUser(); 
                $szAgentName = $chatLogsArys['agent_names'][0];
                $szAgentId = $chatLogsArys['agent_ids'][0];
                
                $visitorAry = array();
                $visitorAry = $chatLogsArys['visitor'];
                if(!empty($visitorAry))
                {
                    $szVisitorId = $visitorAry['id'];
                    $szVisitorName = $visitorAry['name'];
                    $szVisitorEmail = $visitorAry['email'];
                    $szVisitorPhone = $visitorAry['phone']; 
                }
                /*
                * To add a chat transcript to pending tray a chat must have a valid customer email
                */  
                if(empty($szVisitorEmail))
                {
                    continue;
                }
                else
                {
                    echo "<br> Adding Chat ID: ".$szChatId."<br><br>";  
                }  
                
                $szForwarderTag = strtolower(__FORWARDER_CHAT_TAG_KEYWORD__);
                if(!empty($chatLogsArys['tags']) && in_array($szForwarderTag,$chatLogsArys['tags']))
                {
                    /*
                    * If we find 'forwarder' in chat tag then we don't consider that chat transcript in management section
                    */ 
                    continue;
                } 
                $szWebPath = serialize($chatLogsArys['webpath']);
                $szTags = serialize($chatLogsArys['tags']);
                $szUserSession = serialize($chatLogsArys['session']);
                
                $idCustomer = 0;
                $bCreateBookingFile = false; 
                if(empty($szAgentName))
                {
                    /*
                    * If agent name is empty for a chat then we add that as task in pending tray
                    */
                    $bCreateBookingFile = true;
                }
                if(!empty($szVisitorEmail))
                {
                    $kUser->idIncompleteUser = 0;
                    $kUser->isUserEmailExist($szVisitorEmail); 
                    $idCustomer = $kUser->idIncompleteUser; 
                    
                    if($idCustomer<=0)
                    {
                        /*
                        *  If we don't find cutomers email in our database then we add this email as a new user
                        */
                        if(!empty($szVisitorName))
                        {
                            $szVisitorNameAry = explode(" ",$szVisitorName);
                        }
                        $kRegisterShipCon = new cRegisterShipCon();
                        $szPassword = mt_rand(0,99)."".$kRegisterShipCon->generateUniqueToken(8);
                        
                        $userChatSessionAry = array();
                        $userChatSessionAry = $chatLogsArys['session'];
                        
                        if(!empty($userChatSessionAry['country_code']))
                        {
                            $kConfigNew = new cConfig();
                            $kConfigNew->loadCountry(false,$userChatSessionAry['country_code']);
                            $idCustomerCountry = $kConfigNew->idCountry;
                            $idCustomerCurrency = $kConfigNew->iDefaultCurrency;
                            $idCustomerLanguage = $kConfigNew->iDefaultLanguage; 
                            
                            if(!empty($szVisitorPhone))
                            {
                                /*
                                 * As per system we expacting phone number shoud be like +91 9999999999
                                 */
                                $szVisitorPhoneAry = explode(" ", $szVisitorPhone);
                                $szCustomerPhone = $szVisitorPhoneAry[1];
                                if(empty($szCustomerPhone))
                                {
                                    $szCustomerPhone = $szVisitorPhone;
                                }
                            }
                        } 
                        $addCustomerAry = array();
                        $addCustomerAry['szFirstName'] = $szVisitorNameAry[0];
                        $addCustomerAry['szLastName'] = $szVisitorNameAry[1]; 
                        $addCustomerAry['szEmail'] = $szVisitorEmail ;
                        $addCustomerAry['szPassword'] = $szPassword ;
                        $addCustomerAry['szConfirmPassword'] = $szPassword ;
                        $addCustomerAry['szPostCode'] = "" ;
                        $addCustomerAry['szCity'] = $userChatSessionAry['city'] ;  
                        $addCustomerAry['szCountry'] = $idCustomerCountry; 
                        $addCustomerAry['szPhoneNumberUpdate'] = $szCustomerPhone ; 
                        $addCustomerAry['szAddress1'] = $userChatSessionAry['region'] ;
                        $addCustomerAry['szCurrency'] = $idCustomerCurrency; 
                        $addCustomerAry['iConfirmed'] = 0; 
                        $addCustomerAry['iFirstTimePassword'] = 1;  
                        $addCustomerAry['idInternationalDialCode'] = $idCustomerCountry ;
                        $addCustomerAry['iLanguage'] = $idCustomerLanguage;
                        $addCustomerAry['iSendUpdate'] = 1;
                         
                        if($kUser->createAccount($addCustomerAry,'QUICK_QUOTE'))
                        {
                            $idCustomer = $kUser->id ;   
                            $kRegisterShipCon->updateCapsuleCrmDetails($idCustomer);  
                            $bCreateBookingFile = true;
                        }
                        echo "<br> User ID: ".$idCustomer;
                    }
                }  
                $dtChatTimeStamp = $chatLogsArys['timestamp'];
                $szCustomerTimeZoneGlobal = 'Europe/London';

                $date = new DateTime($dtChatTimeStamp,new DateTimeZone($szCustomerTimeZoneGlobal));   
                $dtChatDateTime = $date->format('Y-m-d H:i:s');
                
                $addChatLogsAry = array();
                $addChatLogsAry['idUser'] = $idCustomer;
                $addChatLogsAry['szChatUniqueID'] = $szChatId;
                $addChatLogsAry['szAgentName'] = $szAgentName;
                $addChatLogsAry['szAgentId'] = $szAgentId;
                $addChatLogsAry['szVisitorId'] = $szVisitorId;
                $addChatLogsAry['szVisitorName'] = $szVisitorName;
                $addChatLogsAry['szVisitorEmail'] = $szVisitorEmail;
                $addChatLogsAry['szVisitorPhone'] = $szVisitorPhone;
                $addChatLogsAry['szWebPath'] = $szWebPath;
                $addChatLogsAry['szTags'] = $szTags;
                $addChatLogsAry['szRating'] = $chatLogsArys['rating'];
                $addChatLogsAry['szUserSession'] = $szUserSession;
                $addChatLogsAry['dtChatDateTime'] = $dtChatDateTime;
                $addChatLogsAry['szComments'] = $chatLogsArys['comment'];
                            
                $idZopimChat = $this->addZopimChat($addChatLogsAry);  
                 
                if($szChatType=='offline_msg')
                {
                    $dtChatTimeStamp = $chatLogsArys['timestamp'];
                    $szCustomerTimeZoneGlobal = 'Europe/London';

                    $date = new DateTime($dtChatTimeStamp,new DateTimeZone($szCustomerTimeZoneGlobal));   
                    $dtChatedOn = $date->format('Y-m-d H:i:s');
                    
                    $addChatHistoryAry = array();
                    $addChatHistoryAry['idUser'] = $idCustomer;
                    $addChatHistoryAry['idZopimChat'] = $idZopimChat;
                    $addChatHistoryAry['szType'] = "__OFFLINE_MESSAGE__";
                    $addChatHistoryAry['szName'] = $szVisitorName;
                    $addChatHistoryAry['szMessage'] = $chatLogsArys['message'];
                    $addChatHistoryAry['szMessageId'] = $chatLogsArys['id'];
                    $addChatHistoryAry['szChannel'] = 'N/A';
                    $addChatHistoryAry['dtChatedOn'] = $dtChatedOn; 
                    
                    $idZompimMessageLogs = $this->addZopimChatMessageLogs($addChatHistoryAry);  
                }
                else
                {
                    $chatHistoryAry = $chatLogsArys['history'];
                    if(!empty($chatHistoryAry) && $idZopimChat>0)
                    {
                        foreach($chatHistoryAry as $chatHistoryArys)
                        {
                            $dtChatTimeStamp = $chatHistoryArys['timestamp'];
                            $szCustomerTimeZoneGlobal = 'Europe/London';

                            $date = new DateTime($dtChatTimeStamp,new DateTimeZone($szCustomerTimeZoneGlobal));   
                            $dtChatedOn = $date->format('Y-m-d H:i:s');

                            $szMessageType = $this->getZopimMessageType($chatHistoryArys['type']);
                            $addChatHistoryAry = array();
                            $addChatHistoryAry['idUser'] = $idCustomer;
                            $addChatHistoryAry['idZopimChat'] = $idZopimChat;
                            $addChatHistoryAry['szType'] = $szMessageType;
                            $addChatHistoryAry['szName'] = $chatHistoryArys['name'];
                            $addChatHistoryAry['szMessage'] = $chatHistoryArys['msg'];
                            $addChatHistoryAry['szMessageId'] = $chatHistoryArys['msg_id'];
                            $addChatHistoryAry['szChannel'] = $chatHistoryArys['channel'];
                            $addChatHistoryAry['dtChatedOn'] = $dtChatedOn;
                              
                            $idZompimMessageLogs = $this->addZopimChatMessageLogs($addChatHistoryAry);  
                        }
                    }
                }
                if($bCreateBookingFile)
                {
                    $idFileOwner = 0;
                    if($idCustomer>0)
                    {
                        $kUser->getUserDetails($idCustomer);
                        $idFileOwner = $kUser->idCustomerOwner;
                    }
                    $chatDetailsAry = array();
                    $chatDetailsAry['idCustomer'] = $idCustomer;
                    $chatDetailsAry['idFileOwner'] = $idFileOwner;
                    $chatDetailsAry['idOfflineChat'] = $idZopimChat;
                     
                    $kBooking->addNewBookingFile(true,$chatDetailsAry);
                    $idBookingFile = $kBooking->idBookingFile;
                    echo "<br> File ID: ".$idBookingFile;
                }
                
                /*
                * If Agent has already added to a chat then we close the booking
                */
                if($idBookingFile>0 && $szAgentId>0)
                {
                    //echo "<br> Close Chat file: ".$idBookingFile;
                    $kBooking->closeTask($idBookingFile); 
                } 
            }
        }
        return $resultAry; 
    }
    
    function getZopimMessageType($szType)
    {
        if($szType=='chat.memberjoin')
        {
            $szMessageType = '__MEMBER_JOINED__';
        }
        else if($szType=='chat.msg')
        {
            $szMessageType = '__CHAT_MESSAGE__';
        }
        else if($szType=='chat.memberleave')
        {
            $szMessageType = '__MEMBER_LEFT__';
        }
        else if($szType=='chat.rating')
        {
            $szMessageType = '__CHAT_RATING__';
        }
        else if($szType=='offline_msg')
        {
            $szMessageType = '__OFFLINE_MESSAGE__';
        }
        return $szMessageType;
    } 
    function getAllZopimChatLogs($idCustomer=false,$idZopimChat=false,$iHistoricalData=false)
    { 
        if($idCustomer>0 ||  $idZopimChat>0)
        {
            if($idZopimChat>0)
            {
                $query_and = " id = '".$idZopimChat."' ";
            }
            else if($idCustomer>0)
            {
                $query_and = " idUser = '".(int)$idCustomer."'  ";
            }
            else
            {
                return false;
            }
            
            if($iHistoricalData)
            {
                $dtSent = date('Y-m-d',strtotime("-6 MONTH"));
                $query_and .= " AND DATE(dtChatDateTime) < '".mysql_escape_custom($dtSent)."' ";
            }
            
            $query="
                SELECT 
                    id,
                    idUser, 
                    szChatUniqueID,
                    szAgentName,
                    szAgentId, 
                    szVisitorId,
                    szVisitorName, 
                    szVisitorEmail,
                    szVisitorPhone,
                    szWebPath,
                    szTags,
                    szRating,
                    szUserSession,
                    dtChatDateTime as dtSent,
                    dtChatDateTime,
                    szComments,
                    dtCreatedOn
                FROM
                    ".__DBC_SCHEMATA_ZOPIM_LOGS__."	
                WHERE
                    $query_and
                ORDER BY
                    dtChatDateTime DESC
            ";
            //echo "<br><br>".$query ;
            if($result = $this->exeSQL($query))
            { 
                $retAry = array(); 
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $retAry[$ctr] = $row;
                    $retAry[$ctr]['iChatLogs'] = 1;
                    $ret_ary[$ctr]['idCustomerLogDataType'] = __CUSTOMER_LOG_SNIPPET_TYPE_ZOPIM_CHAT_LOGS__; 
                    $ctr++;
                }
                return $retAry;
            }
        } 
    }
    
    function getAllZopimChatMessageLogs($idZopimChat)
    { 
        if($idZopimChat>0)
        {
            $query="
                SELECT 
                    id,
                    idUser, 
                    idZopimChat,
                    szType,
                    szName, 
                    szMessage,
                    szMessageId, 
                    szChannel,
                    dtChatedOn,
                    dtCreatedOn
                FROM
                    ".__DBC_SCHEMATA_ZOPIM_CHAT_HISTORY_LOGS__."	
                WHERE
                    idZopimChat = '".(int)$idZopimChat."'  
                ORDER BY
                    dtChatedOn ASC
            ";
            //echo "<br><br>".$query ;
            if($result = $this->exeSQL($query))
            { 
                $retAry = array(); 
                $ctr = 0;
                while($row = $this->getAssoc($result))
                {
                    $retAry[$ctr] = $row; 
                    $ctr++;
                }
                return $retAry;
            }
        } 
    }
    
    function isChatAlreadyAdded($szChatUniqueID)
    {
        if(!empty($szChatUniqueID))
        {
            $query="
                SELECT 
                    id 
                FROM
                    ".__DBC_SCHEMATA_ZOPIM_LOGS__."	
                WHERE
                    szChatUniqueID = '".mysql_escape_custom($szChatUniqueID)."'  
            ";
            //echo "<br><br>".$query ;
            if($result = $this->exeSQL($query))
            { 
                $retAry = array(); 
                $row = $this->getAssoc($result); 
                if($row['id']>0)
                {
                    $this->idZopimChatLogs = $row['id'];
                    return true;
                }
                else
                {
                    return false;
                } 
            }
        } 
    }
    
    function addZopimChat($addChatLogsAry)
    { 
        if(!empty($addChatLogsAry))
        {       
            $query="	
                INSERT INTO
                    ".__DBC_SCHEMATA_ZOPIM_LOGS__."
                (
                    idUser, 
                    szChatUniqueID,
                    szAgentName,
                    szAgentId, 
                    szVisitorId,
                    szVisitorName, 
                    szVisitorEmail,
                    szVisitorPhone,
                    szWebPath,
                    szTags,
                    szRating,
                    szUserSession,
                    dtChatDateTime,
                    szComments,
                    dtCreatedOn
                )
                VALUES
                (
                    '".mysql_escape_custom(trim($addChatLogsAry['idUser']))."',
                    '".mysql_escape_custom(trim($addChatLogsAry['szChatUniqueID']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szAgentName']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szAgentId']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szVisitorId']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szVisitorName']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szVisitorEmail']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szVisitorPhone']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szWebPath']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szTags']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szRating']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szUserSession']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['dtChatDateTime']))."', 
                    '".mysql_escape_custom(trim($addChatLogsAry['szComments']))."', 
                    now()
                )  
            "; 
            //echo $query."<br><br>"; 
            if($result=$this->exeSQL($query))
            { 
                $idZompimLogs = $this->iLastInsertID;
                $strdata=array();
                $bAddLogs = false;
                if($idZompimLogs>0)
                {
                    /*
                    * Adding reminder logs data to customer log snippet
                    */
                    $kCustomerLogs = new cCustomerLogs();
                    $zopimChatLogsAry = array();
                    $zopimChatLogsAry = $this->getAllZopimChatLogs(false,$idZompimLogs); 
                    if(!empty($zopimChatLogsAry[0]))
                    {
                        $kCustomerLogs->addZopimChatDataToCustomerLogSnippet($zopimChatLogsAry[0]);
                    }
                    else 
                    {
                        $bAddLogs = true; 
                        $szErrorMessage = " \n\n *** No record found for ID: ".$idZompimLogs."\n\n";
                    } 
                }
                else
                {
                    $bAddLogs = true; 
                    $szErrorMessage = " \n\n *** Not found Last inserted Id for Query:\n\n".$query;
                }  
                if($bAddLogs)
                {
                    $filename = __APP_PATH_ROOT__."/logs/customer_log_snippet.log"; 
                    $strdata[0]=" \n\n *** Adding Zopim Chat data *** \n";
                    $strdata[1] = $szErrorMessage;
                    file_log_session(true, $strdata, $filename);
                }
                return $idZompimLogs;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
    }
    
    function addZopimChatMessageLogs($addChatHistoryAry)
    { 
        if(!empty($addChatHistoryAry))
        {       
            $query="	
                INSERT INTO
                    ".__DBC_SCHEMATA_ZOPIM_CHAT_HISTORY_LOGS__."
                (
                    idUser, 
                    idZopimChat,
                    szType,
                    szName, 
                    szMessage,
                    szMessageId, 
                    szChannel,
                    dtChatedOn,
                    dtCreatedOn
                )
                VALUES
                (
                    '".mysql_escape_custom(trim($addChatHistoryAry['idUser']))."',
                    '".mysql_escape_custom(trim($addChatHistoryAry['idZopimChat']))."', 
                    '".mysql_escape_custom(trim($addChatHistoryAry['szType']))."', 
                    '".mysql_escape_custom(trim($addChatHistoryAry['szName']))."', 
                    '".mysql_escape_custom(trim($addChatHistoryAry['szMessage']))."', 
                    '".mysql_escape_custom(trim($addChatHistoryAry['szMessageId']))."', 
                    '".mysql_escape_custom(trim($addChatHistoryAry['szChannel']))."', 
                    '".mysql_escape_custom(trim($addChatHistoryAry['dtChatedOn']))."',  
                    now()
                )  
            "; 
            //echo $query."<br><br>"; 
            if($result=$this->exeSQL($query))
            { 
                $idZompimMessageLogs = $this->iLastInsertID;
                return $idZompimMessageLogs;
            }
            else
            { 
                $this->error = true;
                $szErrorMessage = __CLASS__ . "::" . __FUNCTION__ .  "() failed to load because of a mysql error. SQL: " . $query . " MySQL Error: " . mysql_error_custom();
                $this->logError( "mysql", $szErrorMessage, "PHP", __CLASS__, __FUNCTION__, __LINE__, "critical");
                return false;
            } 
        }
    } 
    
    function getLastEmailReceivedDate()
    {
        $query="
            SELECT 
                dtReceivedOn 
            FROM
                ".__DBC_SCHEMATA_ZOPIM_LOGS__."	  
            ORDER BY 
                dtReceivedOn DESC
            LIMIT 0,1
        ";
        //echo "<br><br>".$query ;
        if($result = $this->exeSQL($query))
        { 
            $retAry = array(); 
            $row=$this->getAssoc($result); 
            return $row['dtReceivedOn'];
        }
    }
    
    function getOfflineMessageTest()
    { 
        $szUserName = "morten.laerkholm@transporteca.com";
        $szPassword = "a5P8t6102j";
 
        $headers = array(
            'Content-Type:application/json',
            'Authorization: Basic '. base64_encode($szUserName.":".$szPassword)
        );
         
        //$dtLastRunDate = date("Y-m-d",strtotime("-2 HOURS"))."T".date("h:i:s",strtotime("-2 HOURS")); 
          
        $dtLastRunDate = date("Y-m-d",strtotime("-7 days"))."T00:00:00";
        //$szUrl = __ZOPIM_CHAT_BASE_URL__."?q=timestamp:[".$dtLastRunDate." TO *]";
        $szUrl = __ZOPIM_CHAT_BASE_URL__."?ids=1610.578077.Q0WgUnD7BLQxg"; 
         
        $ch = curl_init();                 
        curl_setopt($ch, CURLOPT_URL,$szUrl);      
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);      
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch); 
        $infoAry = curl_getinfo($ch);
           
        $resultAry = array();
        if($infoAry['http_code']==200)
        {
            $resultAry = json_decode($result,true);  
        } 
          
        if(!empty($resultAry['chats']))
        {
            $kUser = new cUser(); 
            $chatLogsAry = $resultAry['chats'];
            foreach($chatLogsAry as $chatLogsArys)
            {
                $szChatId = $chatLogsArys['id'];  
            }
        }
        return $resultAry; 
    }
    
    function set_szEmail( $value,$flag=true)
    {
        $this->szEmail = $this->validateInput( $value, __VLD_CASE_EMAIL__, "szEmail", t($this->t_base.'fields/email'), false, 255, $flag );
    }
}
?>