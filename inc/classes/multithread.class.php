<?php

/**
 * database.class.php
 *
 * @copyright Copyright (C) 2012 Transporteca, LLC
 * @author Anil
 * @package Transporteca Development
 */
if( !defined( "__APP_PATH__" ) )
    define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/../../" ) );
 
require_once( __APP_PATH__ . "/inc/constants.php" ); 
require_once( __APP_PATH_INC__ . "/functions.php" );
require_once( __APP_PATH_INC__ . "/courier_functions.php" );
require_once( __APP_PATH_CLASSES__ . "/database.class.php");
require_once( __APP_PATH_CLASSES__ . "/courierServices.class.php" );
 
/**
 * cCourierAPI is interfaced used to execute sql statements
 * 
 */
class cCourierAPI extends Worker
{  
    //  thread responses
    static $gThread_Responses = array(); 
    var $idCourierProvider = 0;
    var $courierRequestAry = array();

    function __construct($idCourierProvider,$courierRequestAry)
    { 
        $this->idCourierProvider = $idCourierProvider;
        $this->courierRequestAry = $courierRequestAry;  
    }    
    function run()
    {
        $this->runScript();
    }
    
    function runScript()
    {
        $idCourierProvider = $this->idCourierProvider;
        $courierRequestAry = $this->courierRequestAry;
          
        if($idCourierProvider==1)
        {
            //$kCourierServices = new cCourierServices();  
            $this->szApiResponseText = "<br><br> calling FedEx<br><br>";
//            $kCourierServices->calculateShippingDetails($courierRequestAry);
//            $this->szApiResponseText = $kCourierServices->szApiResponseText;
            //self::$gThread_Responses .= $kCourierServices->szApiResponseText;
        }
        else if($idCourierProvider==2)
        {
            //$kCourierServices_2 = new cCourierServices();  
            $this->szApiResponseText = "<br><br> calling UPS <br><br>";
//            $kCourierServices->calculateShippingDetails($courierRequestAry);
//            $this->szApiResponseText = $kCourierServices->szApiResponseText;
            //self::$gThread_Responses .= $kCourierServices->szApiResponseText;
        }
        else if($idCourierProvider==3)
        {
            //$kWebServices = new cWebServices();
            //$this->szApiResponseText = "<br><br> calling TNT <br><br>";
            $kWebServices->calculateShippingDetails($courierRequestAry);
            $this->szApiResponseText = $kWebServices->szApiResponseText;
           // self::$gThread_Responses .= $this->szApiResponseText;
        }
    }
}
?>
